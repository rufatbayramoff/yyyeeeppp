<?php

use common\models\Ps;
use frontend\widgets\PsPrintServiceReviewStarsWidget;
use frontend\widgets\UploadWidget;

use kartik\select2\Select2Asset;
use kartik\select2\ThemeKrajeeAsset;
use yii\bootstrap\ActiveForm;

$ps = Ps::findByPk(1);

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

Select2Asset::register($this);
ThemeKrajeeAsset::register($this);

$additionalParams = [
    'psId' => $this->params['psId'] ?? ''
];

?>

    <div class="hero-banner hero-banner--2col">
        <div class="container container--wide">

            <h1 class="p-b30 animated text-center">
                <?= _t('site.page', 'Treatstock Online 3D Printing Service for'); ?>
            </h1>

            <div class="row">
                <div class="col-sm-6">

                    <ul class="nav nav-tabs nav-tabs--secondary hero-banner__tab">
                        <li class="active">
                            <a href="#business" data-toggle="tab"><?= _t('site.page', 'small business'); ?></a>
                        </li>
                        <li>
                            <a href="#hobby" data-toggle="tab"><?= _t('site.page', 'hobby'); ?></a>
                        </li>
                        <li>
                            <a href="#medical" data-toggle="tab"><?= _t('site.page', 'medical testing'); ?></a>
                        </li>
                        <li>
                            <a href="#engineering" data-toggle="tab"><?= _t('site.page', 'engineering'); ?></a>
                        </li>
                        <li>
                            <a href="#prototyping" data-toggle="tab"><?= _t('site.page', 'rapid prototyping'); ?></a>
                        </li>
                        <li>
                            <a href="#you" data-toggle="tab"><?= _t('site.page', 'you'); ?></a>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane fade active in" id="business">
                            <img class="hero-banner__tab-img img-responsive"
                                 src="https://static.treatstock.com/static/images/materials/Glass-filled-Nylon.jpg"
                                 alt="">
                        </div>

                        <div class="tab-pane fade" id="hobby">
                            <img class="hero-banner__tab-img img-responsive"
                                 src="https://static.treatstock.com/static/images/materials/Generic-Resin.jpg"
                                 alt="">
                        </div>

                        <div class="tab-pane fade" id="medical">
                            <img class="hero-banner__tab-img img-responsive"
                                 src="https://static.treatstock.com/static/images/materials/Resin-Dental.jpg"
                                 alt="">
                        </div>

                        <div class="tab-pane fade" id="engineering">
                            <img class="hero-banner__tab-img img-responsive"
                                 src="https://static.treatstock.com/static/fxd/wikiMaterials/PLA/photos/pla_printed_parts_2_1_720x540.jpg?date=1569488617"
                                 alt="">
                        </div>

                        <div class="tab-pane fade" id="prototyping">
                            <img class="hero-banner__tab-img img-responsive"
                                 src="https://static.treatstock.com/static/fxd/wikiMaterials/ASA/photos/asa2_1_720x540.jpg?date=1543934534"
                                 alt="">
                        </div>

                        <div class="tab-pane fade" id="you">
                            <img class="hero-banner__tab-img img-responsive"
                                 src="https://static.treatstock.com/static/images/materials/Resin-Ceramic.jpg"
                                 alt="">
                        </div>

                    </div>

                </div>

                <div class="col-sm-6 text-center">

                    <h3 class="m-t30">
                        <?= _t('site.page', 'Upload your 3D design for Instant price comparison'); ?>
                    </h3>

                    <a href="/order-upload?utm_source=3d_printing" target="_blank" class="btn btn-danger m-t10 m-b10">
                        <?= _t('site.page', 'Get 3D Printing'); ?>
                    </a>

                    <!-- TrustBox script -->
                    <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
                    <!-- End TrustBox script -->

                    <!-- TrustBox widget - Micro Review Count -->
                    <div class="trustpilot-widget trustpilot-widget--promo" data-locale="en-US" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="598c29210000ff0005a88bed" data-style-height="24px" data-style-width="100%" data-theme="light">
                        <a href="https://www.trustpilot.com/review/treatstock.com" target="_blank" rel="noopener">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->

                </div>
            </div>
        </div>
    </div>

    <div class="promo-page-section promo-page-section--dark">
        <div class="container container--wide">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="text-center m-b30 animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'Compare 3D Printing Technologies'); ?>
                    </h2>

                    <div class="overflow-auto m-t20">
                        <table class="table table-bordered mat-compare-table">
                            <tr>
                                <th class="mat-compare-table__head">
                                    <?= _t('front', 'Technology'); ?>
                                </th>
                                <th class="mat-compare-table__head">
                                    <b class="m-r5">FDM</b>
                                    <span class="tsi tsi-question-c" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fused deposition modeling"></span>
                                </th>
                                <th class="mat-compare-table__head">
                                    <b class="m-r5">DUP</b>
                                    <span class="tsi tsi-question-c" data-toggle="tooltip" data-placement="top" title="" data-original-title="Direct UV printing"></span>
                                </th>
                                <th class="mat-compare-table__head">
                                    <b class="m-r5">DLP & SLA</b>
                                    <span class="tsi tsi-question-c" data-toggle="tooltip" data-placement="top" title="" data-original-title="Stereolithography"></span>
                                </th>
                                <th class="mat-compare-table__head">
                                    <b class="m-r5">MJ & Polyjet</b>
                                    <span class="tsi tsi-question-c" data-toggle="tooltip" data-placement="top" title="" data-original-title="Material jetting"></span>
                                </th>
                                <th class="mat-compare-table__head">
                                    <b class="m-r5">SLS</b>
                                    <span class="tsi tsi-question-c" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selective laser sintering"></span>
                                </th>
                                <th class="mat-compare-table__head">
                                    <b class="m-r5">SLM & DMLS</b>
                                    <span class="tsi tsi-question-c" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selective laser melting"></span>
                                </th>
                            </tr>
                            <tr>
                                <th class="mat-compare-table__head-side"><?= _t('front', 'Tip'); ?></th>
                                <td><?= _t('front', 'An affordable way to get rough prototypes, educational and hobby projects.'); ?></td>
                                <td><?= _t('front', 'Budget friendly printing with resins, great for figurines, minis and scale models.'); ?></td>
                                <td><?= _t('front', 'High-precision resin printing for medical applications and jewelry master models.'); ?></td>
                                <td><?= _t('front', 'High-definition resin printing with multiple materials and color support. Provides accurate structure, texture and visual appearance.'); ?></td>
                                <td><?= _t('front', 'Works best for low batch productions, snap-fit prototypes and mechanical parts.'); ?></td>
                                <td><?= _t('front', 'Produces metal parts and components, suitable for replacements and rapid metal prototypes.'); ?></td>
                            </tr>
                            <tr>
                                <th class="mat-compare-table__head-side"><?= _t('front', 'Average cost'); ?></th>
                                <td><?= _t('front', '$'); ?></td>
                                <td><?= _t('front', '$$'); ?></td>
                                <td><?= _t('front', '$$$'); ?></td>
                                <td><?= _t('front', '$$$'); ?></td>
                                <td><?= _t('front', '$$$'); ?></td>
                                <td><?= _t('front', '$$$$'); ?></td>
                            </tr>
                            <tr>
                                <th class="mat-compare-table__head-side"><?= _t('front', 'Average turnaround* (including shipping)'); ?></th>
                                <td><?= _t('front', '4 days'); ?></td>
                                <td><?= _t('front', '8 days'); ?></td>
                                <td><?= _t('front', '10 days'); ?></td>
                                <td><?= _t('front', '10 days'); ?></td>
                                <td><?= _t('front', '10 days'); ?></td>
                                <td><?= _t('front', '15 days'); ?></td>
                            </tr>
                            <tr>
                                <th class="mat-compare-table__head-side"><?= _t('front', 'How it works'); ?></th>
                                <td><?= _t('front', 'Produces objects from string plastic or rubber, which is heated and extruded through a nozzle to form an object from the bottom up.'); ?></td>
                                <td><?= _t('front', 'Uses LED lights and a masking screen to selectively cure UV resins. While similar to stereolithography, LED-based 3D printers are more prone to voxelization, resulting in parts with more visible layers. On the bright side, DUP printing is much quicker than common SLA.'); ?></td>
                                <td><?= _t('front', 'With a more powerful and precise light source (projector in DLP and a laser in SLA) these technologies are more accurate and smooth. '); ?></td>
                                <td><?= _t('front', 'Operates several nozzles to drop mix and combine UV resins with each layer instantly cured by the source on the printing head. '); ?></td>
                                <td><?= _t('front', 'Using a laser fuses plastic powder grains layer by layer. This technology is most cost-efficient when many parts are nested and printed in one run. Parts are recommended to be post-processed or coated as they are highly porous.'); ?></td>
                                <td><?= _t('front', 'Like SLS, uses a laser, however, not just sinters the powder grains but melts them together. This method allows getting complex metal parts and even pieces from precious metals.'); ?></td>
                            </tr>
                            <tr>
                                <th class="mat-compare-table__head-side"><?= _t('front', 'Compatible materials'); ?></th>
                                <td><?= _t('front', '100+ Plastics, rubbers, polyesters, polyamides, exotic and engineering composites'); ?></td>
                                <td><?= _t('front', 'UV & Daylight Resins, Castable, Tough, Flexible types'); ?></td>
                                <td><?= _t('front', 'UV Resins, Engineering, Dental, Medical, Castable and Flexible types'); ?></td>
                                <td><?= _t('front', 'UV Resins, Wide range of properties available through combinations, matte & glossy finish possible'); ?></td>
                                <td><?= _t('front', 'Polyamides, Rubbers and Nylon-based composites with special properties (esd-safe, medical, etc)'); ?></td>
                                <td><?= _t('front', 'Metals'); ?></td>
                            </tr>
                        </table>
                    </div>

                    <p>* — <?= _t('front', 'an average value is presented, more accurate lead time is shown in the widget'); ?></p>

                    <a href="/3d-printing-services?location=all-locations" target="_blank" class="btn btn-primary m-t10 m-b10">
                        <?= _t('site.page', 'More 3D Printing Methods'); ?>
                    </a>

                </div>
            </div>
        </div>
    </div>

    <div class="p-t30 p-b30">

        <h2 class="animated text-center m-b30" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
            <?= _t('front', 'Find your 3D Printing Material'); ?>
        </h2>

        <ul class="nav nav-tabs nav-tabs--secondary text-center">
            <li class="active">
                <a href="#plastics" data-toggle="tab"><?= _t('front', 'Plastics'); ?></a>
            </li>
            <li>
                <a href="#flexibles" data-toggle="tab"><?= _t('front', 'Flexibles'); ?></a>
            </li>
            <li>
                <a href="#exotics" data-toggle="tab"><?= _t('front', 'Exotics'); ?></a>
            </li>
            <li>
                <a href="#resins" data-toggle="tab"><?= _t('front', 'Resins'); ?></a>
            </li>
            <li>
                <a href="#powder" data-toggle="tab"><?= _t('front', 'Powder Thermoplastics'); ?></a>
            </li>
            <li>
                <a href="#metals" data-toggle="tab"><?= _t('front', 'Metals & Alloys'); ?></a>
            </li>

        </ul>
        <div id="myTabContent" class="tab-content m-t10">
            <div class="tab-pane fade active in" id="plastics">

                <div class="customer-reviews__slider swiper-container">
                    <div class="swiper-wrapper customer-reviews__gallery">

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/pla" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/fxd/wikiMaterials/PLA/photos/pla_printed_parts_2_1_720x540.jpg?date=1569488617"
                                     alt="PLA ">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'PLA'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Versatile but brittle FDM plastic with great detail transfer. Wide range of types and colors.'); ?>
                                    <br>
                                    <?= _t('front', 'Withstands up to 122 – 176 °F'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/abs-plastic" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/fxd/wikiMaterials/abs-plastic/photos/abs_filament-4_720x540.jpg?date=1582026464"
                                     alt="ABS ">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'ABS'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Rigid and strong plastic suitable for cases and enclosures. Tricky at small details. Good hot temperature stability.'); ?>
                                    <br>
                                    <?= _t('front', 'Withstands up to 220 °F'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/petg" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/fxd/wikiMaterials/PETG/photos/petg_filament-2_720x540.jpg?date=1582131559"
                                     alt="PETG">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'PETG'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Strong and durable plastic with great chemical resistance. A great alternative to ABS in terms of accurate results. Withstands moisture and cold temperatures.'); ?>
                                    <br>
                                    <?= _t('front', 'Withstands up to 176 °F'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/asa" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/fxd/wikiMaterials/ASA/photos/asa2_1_720x540.jpg?date=1543934534"
                                     alt="ASA">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'ASA'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Engineering plastic with decent strength and surface quality.  It can be machined. Good resistance to UV, heat and cold.'); ?>
                                    <br>
                                    <?= _t('front', 'Withstands up to 194 °F'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/polycarbonate" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/fxd/wikiMaterials/polycarbonate/photos/polycarbonate-sheet-construction_720x540.jpg?date=1638463770"
                                     alt="PC (Polycarbonate)">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'PC (Polycarbonate)'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Durable and impact resistant plastic. Works well for mechanical components and helps to reduce the weight of the parts.'); ?>
                                    <br>
                                    <?= _t('front', 'Withstands up to 302 °F'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/carbon-fiber" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/fxd/wikiMaterials/carbon-fiber/photos/3d-printed-carbon-fiber-tools_720x540.jpeg?date=1638465750"
                                     alt="Carbon Fiber">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Carbon Fiber'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Composite material made with a combination of Nylon and Carbon Fibers. Strong and lightweight with impressive stress resistance.'); ?>
                                    <br>
                                    <?= _t('front', 'Withstands up to 221 °F'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/nylon-fdm" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/fxd/wikiMaterials/nylon-FDM/photos/nylon_filament-4_720x540.jpg?date=1582131207"
                                     alt="Nylon">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Nylon'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Flexible in thin areas and rigid in thick structures. Good resistance to UV and temperature but high moisture absorption rates.'); ?>
                                    <br>
                                    <?= _t('front', 'Withstands up to 158 – 176 °F'); ?>
                                </div>
                            </a>
                        </div>

                    </div>

                    <div class="customer-reviews__scrollbar swiper-scrollbar"></div>
                </div>

            </div>

            <div class="tab-pane fade" id="flexibles">

                <div class="customer-reviews__slider swiper-container">
                    <div class="swiper-wrapper customer-reviews__gallery">

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/tpu" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/fxd/wikiMaterials/TPU/photos/tpu_material_720x540.jpg?date=1523358480"
                                     alt="TPU">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'TPU'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Rubber-like material is available in different hardness types, which impact how flexible parts turn out.'); ?>
                                    <br>
                                    <?= _t('front', 'Withstands up to 170 °F'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/FLEX-TPE.jpg"
                                     alt="FLEX (TPE)">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'FLEX (TPE)'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'A more elastic and soft type of flexible filament.'); ?>
                                    <br>
                                    <?= _t('front', 'Withstands up to 140 – 266 °F'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/sbs" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/SBS.jpg"
                                     alt="SBS ">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'SBS'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Flexible filament, which can be treated with solvent for great translucency. It is durable and resistant to temperature changes.'); ?>
                                    <br>
                                    <?= _t('front', 'Withstands up to 168 °F'); ?>
                                </div>
                            </a>
                        </div>

                    </div>

                    <div class="customer-reviews__scrollbar swiper-scrollbar"></div>
                </div>

            </div>

            <div class="tab-pane fade" id="exotics">

                <div class="customer-reviews__slider swiper-container">
                    <div class="swiper-wrapper customer-reviews__gallery">

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Wood-filled-Plastics.jpg"
                                     alt="Wood-filled Plastics ">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Wood-filled Plastics'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Wood-filled plastics combine the properties of the base plastic material (PLA, PETG, ABS, depending on the blend) and the appearance of wood. They are great for decor, interior and memorabilia parts.'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Glow.jpg"
                                     alt="Glow in the Dark">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Glow in the Dark'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'These plastics are made with the addition of chemicals that allow a printed part to glow in the dark after some exposure to UV light beforehand. Good for use in toys, gaming pieces and custom signs.'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Metal-Filled-Plastics.jpg"
                                     alt="Metal-Filled Plastics">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Metal-Filled Plastics'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Plastics with metal powders are more dense and heavy. They have some visual and textural likeness to the metal powder used in the blend. Some of them are also magnetic and can be rusted.'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Marble-Plastics.jpg"
                                     alt="Marble Plastics">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Marble Plastics'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'These plastics have tiny grains and powders in them to imitate marble. They help to create figurines and elegant decorations.'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Foaming-PLA.jpg"
                                     alt="Foaming PLA">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Foaming PLA'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Great for RC planes and models that need to weigh less. After printing, the material foams and becomes more porous on the inside.'); ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="customer-reviews__scrollbar swiper-scrollbar"></div>
                </div>

            </div>

            <div class="tab-pane fade" id="resins">

                <div class="customer-reviews__slider swiper-container">
                    <div class="swiper-wrapper customer-reviews__gallery">

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/resin" target="_blank"  class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Generic-Resin.jpg"
                                     alt="Generic or all-purpose Resin">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Generic or all-purpose Resin'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'The basic material for resin printing. It is affordable and had medium detail transfer. The finished parts are relatively brittle. Works great for medium-detailed miniatures, prototypes and concept models. '); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/resin" target="_blank"  class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/High-Detail-Resin.jpg"
                                     alt="High-Detail Resin">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'High-Detail Resin'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'A special composition of this resin allows to rich the maximum printing resolution and detail transfer. It is a trusted material for the smallest and intricate pieces like figurines and jewelry concepts.'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/resin" target="_blank"  class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Castable-Resins.jpg"
                                     alt="Castable Resins">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Castable Resins'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Similar to jewelry wax, used for creating master models in casting. They allow clean burnout and fine detail transfer.'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/resin" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Resin-Dental.jpg"
                                     alt="Dental Resin">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Dental'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'A special range of resins for use in the dental field. Allow for precise structure and compatibility.'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Resin-Ceramic.jpg"
                                     alt="Ceramic Resin">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Ceramic'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Ceramic resins are unique materials for decorative and pottery products. They have lower detail transfer. Their special property is that the end part can be burned in a kiln to reveal a porcelain-like piece.'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Resin-Flexible.jpg"
                                     alt="Flexible Resin">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Flexible'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Used for bendable prototypes. Finished parts are accurate and precise and can be bent. Depending on the grade, these resins help create objects of various hardness from really soft and elastic to tough and rubber-like.'); ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="customer-reviews__scrollbar swiper-scrollbar"></div>
                </div>

            </div>

            <div class="tab-pane fade" id="powder">

                <div class="customer-reviews__slider swiper-container">
                    <div class="swiper-wrapper customer-reviews__gallery">

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/PA-12-Nylon.jpg"
                                     alt="PA 12 Nylon ">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'PA 12 Nylon'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'One of the most common grades of polyamide. It produces strong and mechanically resistant parts. Results are precise and have a good surface finish. Porous and absorb moisture.'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Glass-filled-Nylon.jpg"
                                     alt="Glass-filled Nylon ">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Glass-filled Nylon'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Nylon with the addition of glass beads. Produces stronger and stiffer results with good resistance to heat. '); ?>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <div class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/TPU-Powder.jpg"
                                     alt="TPU Powder ">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'TPU Powder'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Used for flexible and elastic parts. Available in different Shore hardness for various results.'); ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="customer-reviews__scrollbar swiper-scrollbar"></div>
                </div>

            </div>

            <div class="tab-pane fade" id="metals">

                <div class="customer-reviews__slider swiper-container">
                    <div class="swiper-wrapper customer-reviews__gallery">

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/aluminum" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Aluminum.jpg"
                                     alt="Aluminum">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Aluminum'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Ductile and lightweight material. It is an affordable option for metal parts that are conductive and withstand low temperatures.'); ?>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide" style="max-width: 280px;">
                            <a href="/material/titanium" target="_blank" class="mat-cat-card m-l15 m-r15 m-b0">
                                <img class="mat-cat-card__pic"
                                     src="https://static.treatstock.com/static/images/materials/Titanium.jpg"
                                     alt="Titanium ">
                                <h4 class="m-l15 m-r15"><?= _t('front', 'Titanium'); ?></h4>
                                <div class="m-l15 m-r15 m-b10">
                                    <?= _t('front', 'Strong and resistant metal that can be used for an implant and mechanical parts. It withstands corrosion, chemical and heat impact.'); ?>
                                </div>
                            </a>
                        </div>

                    </div>

                    <div class="customer-reviews__scrollbar swiper-scrollbar"></div>
                </div>

            </div>
        </div>

        <div class="text-center">
            <a href="/materials" target="_blank" class="btn btn-primary m-t10 m-b10">
                <?= _t('front', 'More Materials'); ?>
            </a>
        </div>

    </div>

    <div class="promo-page-section promo-page-section--grey">
        <div class="container container--wide">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="animated text-center m-b30" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'Order 3D Print Online'); ?>
                    </h2>

                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <p class="lead m-b0 animated"  data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                                <?= _t('front', 'Simply upload your file(s) to compare 3D printing prices across different technologies and vendors. If you don’t have a 3D model, our widget will direct you to service catalog, where you can request a quote with the information you have on hand.'); ?>
                            </p>
                        </div>
                    </div>

                    <div class="upload-widget-container">

                        <?= UploadWidget::widget(
                            [
                                'additionalParams' => $additionalParams,
                                'needNewWindow'    => true,
                                'isWidget'         => 0
                            ]
                        ); ?>

                        <div class="upload-block__input" style="display: none">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="https://www.thingiverse.com/thing:XXXXXX">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button"><?= _t('site.printModel3d', 'Go') ?></button>
                                </span>
                            </div>
                            <span class="help-block">
                                <?= _t('site.printModel3d', 'Put model link from thingiverse.com and print it') ?>
                            </span>
                        </div>
                    </div>

                    <div class="upload-widget-how-it-works m-t0 m-b0">

                        <h3 class="upload-widget-how-it-works__title"><?= _t('site.printModel3d', 'How it works') ?></h3>

                        <div class="upload-block__explainer">

                            <div class="upload-block__explainer-item">
                                <span class="tsi tsi-upload-l upload-block__explainer-ico"></span>
                                <div class="upload-block__explainer-count">1</div>
                                <?= _t('site.printModel3d', 'Upload files') ?>
                            </div>

                            <div class="upload-block__explainer-item">
                                <span class="tsi tsi-cogwheel upload-block__explainer-ico"></span>
                                <div class="upload-block__explainer-count">2</div>
                                <?= _t('site.printModel3d', 'Get it manufactured') ?>
                            </div>

                            <div class="upload-block__explainer-item">
                                <span class="tsi tsi-truck upload-block__explainer-ico"></span>
                                <div class="upload-block__explainer-count">3</div>
                                <?= _t('site.printModel3d', 'Get it delivered') ?>
                            </div>

                        </div>

                        <div class="modal fade upload-block__modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <h3 class="modal-title"><?= _t('site.printModel3d', 'How it works') ?></h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="video-container m-b0">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/CYijJ4rrktw?rel=0" frameborder="0" allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row m-l0 m-r0 p-t20 p-b10">
                            <div class="col-sm-3 p-t5">
                                <h4 class="upload-widget-how-it-works__benefits">
                                    <i class="tsi tsi-checkmark m-r10 text-success"></i>
                                    <?= _t('site.printModel3d', 'Save time') ?>
                                </h4>
                            </div>
                            <div class="col-sm-3 p-t5">
                                <h4 class="upload-widget-how-it-works__benefits">
                                    <i class="tsi tsi-checkmark m-r10 text-success"></i>
                                    <?= _t('site.printModel3d', 'Avoid spam') ?>
                                </h4>
                            </div>
                            <div class="col-sm-3 p-t5">
                                <h4 class="upload-widget-how-it-works__benefits">
                                    <i class="tsi tsi-checkmark m-r10 text-success"></i>
                                    <?= _t('site.printModel3d', 'Get the best prices') ?>
                                </h4>
                            </div>
                            <div class="col-sm-3 p-t5">
                                <h4 class="upload-widget-how-it-works__benefits">
                                    <i class="tsi tsi-checkmark m-r10 text-success"></i>
                                    <?= _t('site.printModel3d', 'Personal approach') ?>
                                </h4>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="promo-page-section">

        <h2 class="animated text-center m-b30" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
            <?= _t('front', 'Why choose 3D printing On-Demand'); ?>
        </h2>

        <div class="container container--wide overflow-hidden">
            <div class="row">
                <div class="col-xs-12">

                    <div class="row fb-grid p-t20">
                        <div class="col-sm-4 fb-grid__item m-b30">
                            <div class="m-b0 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                <img class="svg-ico m-b15"
                                     style="width: 80px;height: 80px;"
                                     src="https://static.treatstock.com/static/images/common/big-icons/save-money.svg"
                                     alt="">
                                <h4 class="designer-card__title">
                                    <?= _t('front', 'Accessible Low Batch Runs'); ?>
                                </h4>

                                <div class="">
                                    <?= _t('front', 'On-demand manufacturing allows creating components in small runs, scaling the number of the stock based on consumer request. It is an affordable way to get your small business running and selling your very own products.'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 fb-grid__item m-b30">
                            <div class="m-b0 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                <img class="svg-ico m-b15"
                                     style="width: 80px;height: 80px;"
                                     src="https://static.treatstock.com/static/images/common/big-icons/hobby.svg"
                                     alt="">
                                <h4 class="designer-card__title">
                                    <?= _t('front', 'Educational & Hobby Projects'); ?>
                                </h4>

                                <div class="">
                                    <?= _t('front', 'With 3D printing, you can explore hidden talents, try new things and turn the ideas into custom objects. '); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 fb-grid__item m-b30">
                            <div class="m-b0 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                <img class="svg-ico m-b15"
                                     style="width: 80px;height: 80px;"
                                     src="https://static.treatstock.com/static/images/common/big-icons/eco.svg"
                                     alt="">
                                <h4 class="designer-card__title">
                                    <?= _t('front', 'Better Approach to Consumption'); ?>
                                </h4>

                                <div class="">
                                    <?= _t('front', 'By producing only the parts needed, you can save space and reduce material waste. 3D printing also helps get replacement parts that are no longer available to fix machinery and tools, allowing for more sustainable use.'); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="promo-page-section promo-page-section--grey">
        <div class="container container--wide overflow-hidden">
            <div class="row fb-grid fb-grid--va-center">
                <div class="col-xs-12 col-sm-8 col-md-6 col-lg-5 col-lg-offset-2 fb-grid__item">
                    <h2 class="animated m-b30" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'Earn Up to 32% back from your Order'); ?>
                    </h2>
                    <p class="lead animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'On Treatstock you can get Bonuses from using 3D printing or any other manufacturing service. Depending on a vendor’s offer, you will earn a certain number of Bonuses, which can be later used to cover your purchase.'); ?>
                    </p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-6 col-lg-4 fb-grid__item text-center">
                    <a href="/order-upload?utm_source=3d_printing"
                       class="hero-banner__features-img animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                        <img style="border-radius: 5px" src="https://static.treatstock.com/static/fxd/product_main_promobar/images/promo_bar_bonus_400x600.jpg?date=1638540200">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="promo-page-section overflow-hidden">
        <div class="container container--wide">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                    <h2 class="animated m-b30 text-center" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'Frequently Asked Questions'); ?>
                    </h2>

                    <h3 class="animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft"><?= _t('front', 'How to check a 3D printing service price?'); ?></h3>
                    <p class="m-b30 p-b10 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft"><?= _t('front', 'You can get a 3D printing instant quote by uploading a file to our widget. It is calculating the cost of materials and work required to manufacture your parts, including support structures under the overhanging areas. Aside from the volume, the price may be influenced by excessive supports removal (lots of small pieces with lots of supports) or material changes in between the runs (many parts ordered in different colors).'); ?></p>

                    <h3 class="animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft"><?= _t('front', 'How is my IP protected?'); ?></h3>
                    <p class="m-b30 p-b10 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                        <?php
                        $wmLink = "<a href='https://www.watermark3d.com' target='_blank'>Watermark 3D</a>";
                        echo  _t('front', 'It\'s the manufacturer\'s responsibility to destroy all digital copies of files contained in the order, including all derivative work and trace logs, after completing the order, or if the order is canceled. The second layer of protection, which also covers 3D models that are not published on Treatstock, is that we use an innovative system called {Watermark3D} to automatically insert a secret watermark into all STL files downloaded by the manufacturer. The watermark, which is practically invisible and undetectable without the correct password, causes no structural changes to the design and contains information pertinent to a particular order and its manufacturer.', ['Watermark3D'=>$wmLink]);
                        ?>
                    </p>

                    <h3 class="animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft"><?= _t('front', 'Can we sign an NDA?'); ?></h3>
                    <p class="m-b30 p-b10 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft"><?= _t('front', 'Our Terms & Conditions broadly cover intellectual property and private information safety for all users and orders that go through the platform. It serves as NDA for all the files and order information by default. However, if you need an additional document, it is absolutely possible to get that after you select a vendor.'); ?></p>

                    <h3 class="animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft"><?= _t('front', 'Can you 3D print my CAD files?'); ?></h3>
                    <p class="m-b30 p-b10 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft"><?= _t('front', 'Yes, we can 3D print STL and STEP formats straight away. For other CAD models you may need to proceed with a manual quote to get them printable first, however, we’ll do our best to help you get from an early stage design to a finished product as quickly as possible.'); ?></p>

                    <div class="animated text-center" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <a href="/help" class="btn btn-primary" target="_blank"><?= _t('front', 'Check Help Center'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="promo-page-section promo-page-section--grey" style="margin-bottom: -40px;">
        <div class="container container--wide overflow-hidden">

            <h2 class="text-center animated m-b30" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                <?= _t('front', 'Other Manufacturing Services we Offer'); ?>
            </h2>

            <div class="row m-t30 p-t20 p-b30">
                <div class="col-xs-6 col-sm-3">
                    <div class="designer-card m-b30 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                        <a href="/company-service/cnc-manufacturing" target="_blank" class="display-b text-info">
                            <img class="m-b15" width="60px" height="60px" src="https://static.treatstock.com/static/images/common/service-cat/cnc-machining.svg" alt="<?= _t('front', 'CNC Machining'); ?>">

                            <h4 class="m-t0 m-b0">
                                <?= _t('front', 'CNC Machining'); ?>
                            </h4>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="designer-card m-b30 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                        <a href="/company-service/cutting" target="_blank" class="display-b text-info">
                            <img class="m-b15" width="60px" height="60px" src="https://static.treatstock.com/static/images/common/service-cat/cutting-laser.svg" alt="<?= _t('front', 'Milling, Engraving & Laser Cutting'); ?>">

                            <h4 class="m-t0 m-b0">
                                <?= _t('front', 'Milling, Engraving & Laser Cutting'); ?>
                            </h4>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="designer-card m-b30 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                        <a href="/company-service/injection-molding" target="_blank" class="display-b text-info">
                            <img class="m-b15" width="60px" height="60px" src="https://static.treatstock.com/static/images/common/service-cat/injection-molding.svg" alt="<?= _t('front', 'Injection Molding'); ?>">

                            <h4 class="m-t0 m-b0">
                                <?= _t('front', 'Injection Molding'); ?>
                            </h4>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="designer-card m-b30 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                        <a href="/company-service/designing-services" target="_blank" class="display-b text-info">
                            <img class="m-b15" width="60px" height="60px" src="https://static.treatstock.com/static/images/common/service-cat/hd-prototyping.svg" alt="<?= _t('front', 'CAD & 3D Modeling'); ?>">

                            <h4 class="m-t0 m-b0">
                                <?= _t('front', 'CAD & 3D Modeling'); ?>
                            </h4>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        <?php $this->beginBlock('js1', false); ?>

        function initPrints() {
            //Init slider for .customer-reviews__slider
            var swiperPrints = new Swiper('.customer-reviews__slider', {
                observer: true,
                observeParents: true,
                scrollbar: '.customer-reviews__scrollbar',
                scrollbarHide: true,
                slidesPerView: 'auto',
                centeredSlides: true,
                spaceBetween: 0,
                grabCursor: true
            });
        }

        initPrints();
        $(window).resize(function () {initPrints()});

        $('a[data-toggle="tab"]').click(function () {initPrints()});

        $('a[data-toggle="tab"]').click(function () {
            setTimeout(initPrints(), 100);
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
            setTimeout(initPrints(), 100);
        })


        <?php $this->endBlock(); ?>
    </script>
<?php $this->registerJs($this->blocks['js1']); ?>