<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>


<div class="guides-page">
    <div class="guides-page__nav">

        <div class="guides-page__search">
            <form method="get" action="/help/search/" class="ng-pristine ng-valid">
                <div class="input-group">
                    <input type="text" name="q" class="form-control input-sm" placeholder="<?= _t('site.guide', 'Search in Guides...')?>">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <span class="tsi tsi-search"></span>
                        </button>
                    </span>
                </div>
            </form>
        </div>

        <a href="#guidesMainPage" class="guides-page__title"><?= _t('site.guide', 'Manufacturing Guides')?></a>

        <div class="side-nav-mobile">
            <div class="side-nav-mobile__title" data-toggle="collapse" data-target="#navMobile">
                <?= _t('site.guide', 'Guides Menu')?>
                <span class="tsi tsi-down"></span>
            </div>

            <ul class="side-nav help-nav" id="navMobile">
                <li>
                    <a href="#guides/tech">Technology <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                        <li><a href="#tech1">3D Printing </a></li>
                        <li><a href="#tech2">CNC</a></li>
                        <li><a href="#tech3">Injection</a></li>
                    </ul>
                </li>
                <li class="active open">
                    <a href="#guides/materials">Materials <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                        <li><a href="#materials">ABS</a></li>
                        <li><a href="#materials">PLA</a></li>
                        <li><a href="#materials">Resin</a></li>
                        <li><a href="#materials">Nylon</a></li>
                        <li><a href="#materials">VisiJet</a></li>
                        <li><a href="#materials">Sandstone</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#guides/tips&tricks">Tips & Tricks <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                    </ul>
                </li>
                <li>
                    <a href="#guides/how-to">How To <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                    </ul>
                </li>
                <li>
                    <a href="#guides/application">Application & Usage <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                    </ul>
                </li>
                <li>
                    <a href="#guides/3d-modelling">3D Modelling <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                    </ul>
                </li>
            </ul>

        </div>

    </div>
    <div class="guides-page__body">
        <div class="container guides-page__container">

            <h1>Manufacturing Guides</h1>

            <div class="guides-themes">
                <a href="#guide/tech" class="guides-themes__item">
                    <img class="guides-themes__pic" src="https://static.treatstock.com/static/files/1e/3a/97627_15_3a1eb4a98af040e32f3298a8ea523a68.jpg" alt="">
                    <span class="guides-themes__title">Technology</span>
                </a>
                <a href="#guide/materials" class="guides-themes__item">
                    <img class="guides-themes__pic" src="https://static.treatstock.com/static/files/66/23/90100_21_57b9d72a4a5a9aaf418fdc30597d2a80.jpg" alt="">
                    <span class="guides-themes__title">Materials</span>
                </a>
                <a href="#guide/tips-and-tricks" class="guides-themes__item">
                    <img class="guides-themes__pic" src="https://static.treatstock.com/static/files/13/ee/56535_15_43fffbf48c456373240bdd3054f17e15.jpg" alt="">
                    <span class="guides-themes__title">Tips & Tricks</span>
                </a>
                <a href="#guide/how-to" class="guides-themes__item">
                    <img class="guides-themes__pic" src="https://static.treatstock.com/static/files/94/dc/50146_15_4a562f5e4d177d092e098de835d3cf05.jpg" alt="">
                    <span class="guides-themes__title">How To</span>
                </a>
                <a href="#guide/application" class="guides-themes__item">
                    <img class="guides-themes__pic" src="https://static.treatstock.com/static/files/72/af/29937_15_06017da5b7ac48081736cca4d5955357.jpg" alt="">
                    <span class="guides-themes__title">Application & Usage</span>
                </a>
                <a href="#guide/3d-modelling-tips" class="guides-themes__item">
                    <img class="guides-themes__pic" src="https://static.treatstock.com/static/files/d1/9b/24884_15_60cd69dc8ad1dc4b4b52bbce7bd266e4.jpg" alt="">
                    <span class="guides-themes__title">3D Modelling </span>
                </a>
            </div>

            <h2 class="m-t30 m-b20">How to</h2>

            <div class="guides-articles">
                <a href="#guide/tech/article.html" class="guides-articles__item" style="background-image: url('/static/images/ps-pic3.jpg')">
                    <span class="guides-articles__title">How to set up the 3D printer </span>
                </a>
                <a href="#guide/materials/article.html" class="guides-articles__item" style="background-image: url('/static/images/howto-biz-network.jpg')">
                    <span class="guides-articles__title">5 best price CNC machines for machine shop</span>
                </a>
                <a href="#guide/tips-and-tricks/article.html" class="guides-articles__item" style="background-image: url('/static/images/howto-biz-store.jpg')">
                    <span class="guides-articles__title">How to fast and easy make CNC calculations </span>
                </a>
                <a href="#guide/how-to/article.html" class="guides-articles__item" style="background-image: url('/static/images/howto-order-bg.jpg')">
                    <span class="guides-articles__title">How to earn money with 3D Printing </span>
                </a>
            </div>

            <h2 class="m-t30 m-b20">3D printing tips and tricks</h2>

            <div class="guides-articles">
                <a href="#guide/tech/article.html" class="guides-articles__item" style="background-image: url('https://static.treatstock.com/static/files/1e/3a/97627_15_3a1eb4a98af040e32f3298a8ea523a68.jpg')">
                    <span class="guides-articles__title">How has 3D Printing changed the jewelry industry?</span>
                </a>
                <a href="#guide/materials/article.html" class="guides-articles__item" style="background-image: url('https://static.treatstock.com/static/files/66/23/90100_21_57b9d72a4a5a9aaf418fdc30597d2a80.jpg')">
                    <span class="guides-articles__title">3D printing with metal</span>
                </a>
                <a href="#guide/tips-and-tricks/article.html" class="guides-articles__item" style="background-image: url('https://static.treatstock.com/static/files/13/ee/56535_15_43fffbf48c456373240bdd3054f17e15.jpg?date=1493388972')">
                    <span class="guides-articles__title">The Whole Tooth about 3D Printing in Dentistry</span>
                </a>
                <a href="#guide/how-to/article.html" class="guides-articles__item" style="background-image: url('https://static.treatstock.com/static/files/94/dc/50146_15_4a562f5e4d177d092e098de835d3cf05.jpg?date=1492691449')">
                    <span class="guides-articles__title">Application for Metal 3D printing </span>
                </a>
            </div>

        </div>
    </div>
</div>