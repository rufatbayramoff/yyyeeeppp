<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>

<link rel="stylesheet" href="/css/print-model3d/print-model3d.css" type="text/css">

<div class="widget-choose">
    <button class="btn widget-choose__btn widget-choose__btn--active">
        <svg class="svg-ico m-r10" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 11 16" width="11" height="16">
            <path fill="currentColor" d="M9.99998 6H7.86898L10.832 1.555C10.9325 1.4044 10.9902 1.22935 10.999 1.04852C11.0077 0.867687 10.9673 0.687865 10.8819 0.528235C10.7964 0.368605 10.6693 0.235156 10.514 0.142126C10.3587 0.049095 10.181 -2.70837e-05 9.99998 1.12028e-08H4.99998C4.82372 5.68653e-05 4.6506 0.0467025 4.49817 0.135211C4.34574 0.22372 4.21941 0.350946 4.13198 0.504L0.131983 7.504C0.045124 7.65606 -0.00028081 7.82826 0.000310886 8.00338C0.000902581 8.1785 0.04747 8.35039 0.135354 8.50186C0.223239 8.65333 0.349358 8.77906 0.501095 8.86648C0.652831 8.9539 0.824864 8.99994 0.999983 9H4.27698L1.13198 14.504C1.01228 14.7143 0.972861 14.9608 1.02104 15.1979C1.06923 15.435 1.20174 15.6466 1.39401 15.7935C1.58629 15.9403 1.82529 16.0125 2.06672 15.9966C2.30815 15.9807 2.53562 15.8778 2.70698 15.707L10.707 7.707C10.8468 7.56715 10.942 7.38898 10.9806 7.19503C11.0191 7.00108 10.9993 6.80005 10.9237 6.61735C10.848 6.43465 10.7199 6.27848 10.5554 6.1686C10.391 6.05871 10.1977 6.00004 9.99998 6Z"></path>
        </svg>
        Instant quote for manufacturing
    </button>
    <button class="btn widget-choose__btn widget-choose__btn--or">
        or
    </button>
    <button class="btn widget-choose__btn">
        <i class="tsi tsi-search m-r10"></i>
        Find product
    </button>
</div>

<div class="aff-widget">
    <div class="aff-widget-content clearfix">
        <!-- uiView: -->
        <ui-view class=" ">
            <div class="tab-pane  ">
                <div>

                    <div class="aff-widget__lead">
                        Upload a CAD model to get an online quote for manufacturing services.
                        <br>
                        On Treatstock you can find suitable technology like 3D printing, CNC machining, Injection Molding, Casting, Sheet metal
                    </div>

                    <form class="upload-drop-file-zone upload-block ng-pristine ng-valid">

                        <h3 class="m-t0 text-muted">Drag and drop or</h3>

                        <div class="btn btn-danger upload-block__btn t-widget-upload-button"
                             ng-click="uploader.addFileClick();">
                            Upload Files
                        </div>


                        <div class="upload-block__hint">
                            STL, PLY, OBJ, 3MF, CDR, DXF, EPS, PDF or SVG files are supported
                        </div>
                        <div class="upload-block__hint-links">
                            <div class="upload-block__hint-links-body">
                                <span class="tsi tsi-lock m-r10"></span>
                                <span>
                                    All files are protected by <a href="https://www.treatstock.com/" target="_blank" class="upload-block__hint-links-ts">Treatstock</a> security and <a
                                    href="https://www.watermark3d.com/" target="_blank">Watermark 3D</a>
                                </span>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </ui-view>
    </div>
</div>

<div class="upload-widget-how-it-works">

    <h2 class="m-b30">Get over 6000 price offers for your order in seconds!</h2>

    <h3 class="upload-widget-how-it-works__title">How it works</h3>

    <div class="upload-block__explainer">

        <div class="upload-block__explainer-item">
            <span class="tsi tsi-upload-l upload-block__explainer-ico"></span>
            <div class="upload-block__explainer-count">1</div>
            Upload files
        </div>

        <div class="upload-block__explainer-item">
            <span class="tsi tsi-cogwheel upload-block__explainer-ico"></span>
            <div class="upload-block__explainer-count">2</div>
            Get it manufactured
        </div>

        <div class="upload-block__explainer-item">
            <span class="tsi tsi-truck upload-block__explainer-ico"></span>
            <div class="upload-block__explainer-count">3</div>
            Get delivery
        </div>

    </div>

    <div class="p-t15 p-b30">
        <span class="m-l10 m-r10">watch video for more information</span>
        <button class="btn btn-default btn-ghost p-l15 p-r15 m-t10 m-b10"
                data-toggle="modal" data-target=".upload-block__modal">
            <i class="tsi tsi-video m-r10"></i>How it works (5:24)
        </button>
    </div>

    <div class="modal fade upload-block__modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="modal-title">How it works</h3>
                </div>
                <div class="modal-body">
                    <div class="video-container m-b0">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/CYijJ4rrktw?rel=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-l0 m-r0 p-b10">
        <div class="col-sm-3">
            <h4>
                <i class="tsi tsi-checkmark m-r10 text-success"></i>
                Save time
            </h4>
        </div>
        <div class="col-sm-3">
            <h4>
                <i class="tsi tsi-checkmark m-r10 text-success"></i>
                Avoid spam
            </h4>
        </div>
        <div class="col-sm-3">
            <h4>
                <i class="tsi tsi-checkmark m-r10 text-success"></i>
                Get the best prices
            </h4>
        </div>
        <div class="col-sm-3">
            <h4>
                <i class="tsi tsi-checkmark m-r10 text-success"></i>
                All deals are insured by Treatstock
            </h4>
        </div>
    </div>

</div>

<hr class="m-t30 m-b30">

<div class="aff-widget " id="print-navigation">
    <!-- Nav tabs -->
    <ul class="aff-widget-tabs clearfix">
        <li class="active" ui-sref="upload"><a href="#upload" aria-controls="upload"><span
                        class="aff-widget-tabs__step">1</span>
                Upload Files </a></li>
        <li class="" ui-sref="printers" ui-sref-active="active"><a href="#print" aria-controls="print"><span
                        class="aff-widget-tabs__step">2</span>
                Customize Order </a></li>
        <li class="" ui-sref="delivery" ui-sref-active="active"><a href="#delivery" aria-controls="delivery"><span
                        class="aff-widget-tabs__step">3</span>
                Delivery Options </a></li>
        <li class="" ui-sref="checkout" ui-sref-active="active"><a href="#payment" aria-controls="payment"><span
                        class="aff-widget-tabs__step">4</span>
                Checkout </a></li>
    </ul>
    <div class="aff-widget-content clearfix"><!-- uiView: -->
        <ui-view class="">
            <div class="tab-pane ">
                <div class="">

                    <h3>Choosen technology</h3>

                    <div class="row aff-widget-tech fb-grid">
                        <div class="col-xs-12 col-sm-6 m-b15">
                            <button class=" btn btn-default aff-widget-tech__item aff-widget-tech__item--active">
                                <img alt="3D Printing" class="aff-widget-tech__icon"
                                     src="https://static.treatstock.com/static/images/common/service-cat/3dprinting.svg">
                                <span class="aff-widget-tech__text">
                                    <span class="aff-widget-tech__heading">
                                        3D Printing
                                        <span class="aff-widget-tech__heading-hint">SLA, SLS, FDM, MFG</span>
                                    </span>
                                    <span class="aff-widget-tech__hint">
                                        Some text for description technology and other information bla bla bla and other text and words.
                                    </span>
                                </span>
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6 m-b15">
                            <button class=" btn btn-default aff-widget-tech__item">
                                <img alt="3D Printing" class="aff-widget-tech__icon"
                                     src="https://static.treatstock.com/static/images/common/service-cat/cnc-machining.svg">
                                <span class="aff-widget-tech__text">
                                    <span class="aff-widget-tech__heading">
                                        CNC Machining
                                        <span class="aff-widget-tech__heading-hint">Milling, Routing, Turning</span>
                                    </span>
                                    <span class="aff-widget-tech__hint">
                                        Some text for description technology and other information bla bla bla and other text and words.
                                    </span>
                                </span>
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6 m-b15">
                            <button class=" btn btn-default aff-widget-tech__item">
                                <img alt="3D Printing" class="aff-widget-tech__icon"
                                     src="https://static.treatstock.com/static/images/common/service-cat/cutting-laser.svg">
                                <span class="aff-widget-tech__text">
                                    <span class="aff-widget-tech__heading">
                                        Cutting
                                        <span class="aff-widget-tech__heading-hint">Laser Cutting & CNC Cutting</span>
                                    </span>
                                    <span class="aff-widget-tech__hint">
                                        Some text for description technology and other information bla bla bla and other text and words.
                                    </span>
                                </span>
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6 m-b15">
                            <button class=" btn btn-default aff-widget-tech__item">
                                <img alt="3D Printing" class="aff-widget-tech__icon"
                                     src="https://static.treatstock.com/static/images/common/service-cat/3dprinting.svg">
                                <span class="aff-widget-tech__text">
                                    <span class="aff-widget-tech__heading">
                                        3D Printing
                                        <span class="aff-widget-tech__heading-hint">SLA, SLS, FDM, MFG</span>
                                    </span>
                                    <span class="aff-widget-tech__hint">
                                        Some text for description technology and other information bla bla bla and other text and words.
                                    </span>
                                </span>
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-6 m-b15">
                            <button class=" btn btn-default aff-widget-tech__item">
                                <img alt="3D Printing" class="aff-widget-tech__icon"
                                     src="https://static.treatstock.com/static/images/common/service-cat/3dprinting.svg">
                                <span class="aff-widget-tech__text">
                                    <span class="aff-widget-tech__heading">
                                        3D Printing
                                        <span class="aff-widget-tech__heading-hint">SLA, SLS, FDM, MFG</span>
                                    </span>
                                    <span class="aff-widget-tech__hint">
                                        Some text for description technology and other information bla bla bla and other text and words.
                                    </span>
                                </span>
                            </button>
                        </div>
                    </div>

                    <p class="tagline">
                        Set the required quantities for each file and if their sizes appear too small, change the unit
                        of measurement to inches. Click on 3D Viewer for a 360° preview of your files.
                    </p>

                    <div class="aff-widget__upload-controls">

                        <div class="units">
                            <span class="units__label">
                                Unit of measurement:
                            </span>
                            <div class="btn-group btn-switcher" data-toggle="buttons"><label
                                        class="btn btn-default btn-xs active"
                                        ng-class="{'active':model3d.modelUnits=='mm'}"
                                        ng-click="changeScale('mm')">
                                    <input type="radio" name="optionsPrice"
                                           id="optionsPrice1" autocomplete="off">
                                    MM
                                </label>
                                <label class="btn btn-default btn-xs"
                                       ng-class="{'active':model3d.modelUnits=='in'}"
                                       ng-click="changeScale('in')">
                                    <input type="radio" name="optionsPrice"
                                           id="optionsPrice2"
                                           autocomplete="off">
                                    IN
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="models"><!-- ngRepeat: model3dPart in model3d.parts -->
                        <div  class="">
                            <!-- ngIf: !model3dPart.isCanceled -->
                            <div class="models__row " >
                                <div class="models__data">
                                    <div class="models__pic"><img
                                                ng-src="https://static.treatstock.com/static/render/06dd768bd8bddcbae3f842b6f74fa030_color_ffffff_ambient_40_ax_0_ay_30_az_0_pb_0.png"
                                                main-src="https://static.treatstock.com/static/render/06dd768bd8bddcbae3f842b6f74fa030_color_ffffff_ambient_40_ax_0_ay_30_az_0_pb_0.png"
                                                alt-src="/static/images/preloader80.gif"
                                                onerror="commonJs.checkAltSrcImage(this);"
                                                src="https://static.treatstock.com/static/render/06dd768bd8bddcbae3f842b6f74fa030_color_ffffff_ambient_40_ax_0_ay_30_az_0_pb_0.png">
                                        <div class="models__pic-qty ng-binding" title="Quantity: 1">×1</div>
                                    </div>
                                    <div class="models__info"><h4 class="models__title ng-binding">255.stl</h4>
                                        <!-- ngIf: model3dPart.uploadPercent>0 && model3dPart.uploadPercent<100 -->
                                        <!-- ngIf: model3dPart.uploadFailedReason -->
                                        <!-- ngIf: !model3dPart.uploadFailedReason -->
                                        <div class="models__size ">
                                            <!-- ngIf: model3dPart.isParsed -->
                                            <div class="">
                                                <!-- ngIf: model3dPart.isCalculated() -->
                                                <div class="js_models_parsed ng-binding ">
                                                    Size: 105.62 x 96.91 x 123.43 mm (4.16 x 3.82 x 4.86 in)
                                                </div><!-- end ngIf: model3dPart.isCalculated() -->
                                                <!-- ngIf: !model3dPart.isCalculated() -->
                                            </div>
                                            <!-- end ngIf: model3dPart.isParsed --><!-- ngIf: !model3dPart.isParsed -->
                                        </div><!-- end ngIf: !model3dPart.uploadFailedReason -->
                                    </div>
                                    <!-- ngIf: model3d.isCalculated() && !model3d.isKit() -->
                                    <div class="models__scaling ">
                                        <div model="model3d" model-state="model3dTextureState" class="ng-isolate-scope">
                                            <div class="model-zoom-title">
                                                <span class="model-zoom-title__text collapsed"
                                                        data-toggle="collapse"
                                                        data-target="#modelZoom"
                                                        aria-expanded="false"
                                                        aria-controls="modelZoom">
                                                    Scale<span class="tsi tsi-down"></span>
                                                </span>
                                            </div><!-- ngIf: isSmall && !model.isKit() -->
                                            <!-- ngIf: isLarge && !model.isKit() -->
                                            <div class="collapse" id="modelZoom">
                                                <div class="model-zoom">
                                                    <div class="model-zoom__sizes">
                                                        <div class="model-zoom__sizes-item model-zoom__sizes-item--type">
                                                            <label class="control-label" for="">Scale in</label>
                                                            <div class="dropdown model-zoom__measure">
                                                                <button class="btn btn-default btn-sm btn-block dropdown-toggle dropdown-menu-right-toggle ng-binding"
                                                                        type="button" id="dropdownMenu2"
                                                                        data-toggle="dropdown" aria-haspopup="true"
                                                                        aria-expanded="false">
                                                                    Dimensions
                                                                    <span class="tsi tsi-down"></span></button>
                                                                <ul class="dropdown-menu dropdown-menu-right"
                                                                    aria-labelledby="dropdownMenu2">
                                                                    <!-- ngRepeat: scale in scaleTypes -->
                                                                    <li ng-repeat="scale in scaleTypes"
                                                                        class=""><a
                                                                                ng-click="scaleTypeChange(scale)"
                                                                                class="ng-binding">Dimensions</a></li>
                                                                    <!-- end ngRepeat: scale in scaleTypes -->
                                                                    <li ng-repeat="scale in scaleTypes"
                                                                        class=""><a
                                                                                ng-click="scaleTypeChange(scale)"
                                                                                class="ng-binding">Percentage</a></li>
                                                                    <!-- end ngRepeat: scale in scaleTypes -->
                                                                    <li ng-repeat="scale in scaleTypes"
                                                                        class=""><a
                                                                                ng-click="scaleTypeChange(scale)"
                                                                                class="ng-binding">Ratio</a></li>
                                                                    <!-- end ngRepeat: scale in scaleTypes --></ul>
                                                            </div>
                                                        </div><!-- ngIf: scaleType.type == 'dimension' -->
                                                        <div
                                                             class="model-zoom__sizes-item model-zoom__sizes-item--measure js-measure-types ">
                                                            <label class="control-label" for="">&nbsp;</label>
                                                            <div class="dropdown model-zoom__measure">
                                                                <button class="btn btn-default btn-sm btn-block dropdown-toggle dropdown-menu-right-toggle ng-binding"
                                                                        type="button" id="dropdownMenu1"
                                                                        data-toggle="dropdown" aria-haspopup="true"
                                                                        aria-expanded="false">
                                                                    mm
                                                                    <span class="tsi tsi-down"></span></button>
                                                                <ul class="dropdown-menu dropdown-menu-right"
                                                                    aria-labelledby="dropdownMenu1">
                                                                    <!-- ngRepeat: measure in allowedMeasures -->
                                                                    <li ng-repeat="measure in allowedMeasures"
                                                                        class=""><a href="#"
                                                                                            ng-click="setCurrentMeasure(measure)"
                                                                                            class="ng-binding">mm</a>
                                                                    </li>
                                                                    <!-- end ngRepeat: measure in allowedMeasures -->
                                                                    <li ng-repeat="measure in allowedMeasures"
                                                                        class=""><a href="#"
                                                                                            ng-click="setCurrentMeasure(measure)"
                                                                                            class="ng-binding">in</a>
                                                                    </li>
                                                                    <!-- end ngRepeat: measure in allowedMeasures -->
                                                                </ul>
                                                            </div>
                                                        </div><!-- end ngIf: scaleType.type == 'dimension' -->
                                                        <!-- ngIf: scaleType.type == 'dimension' -->
                                                        <div class="model-zoom__sizes-item "><label
                                                                    class="control-label" for="zoomLength">length <span
                                                                        class="tsi tsi-arrow-right-l model-zoom__sizes-arrow"></span></label><input
                                                                    type="number" step="0.1"
                                                                    class="form-control ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min"
                                                                    id="zoomLength" ng-model="size.width"
                                                                    ng-change="onChangeWidth()" min="0.1"></div>
                                                        <!-- end ngIf: scaleType.type == 'dimension' -->
                                                        <!-- ngIf: scaleType.type == 'dimension' -->
                                                        <div class="model-zoom__sizes-item "><label
                                                                    class="control-label" for="zoomWidth">width <span
                                                                        class="tsi tsi-arrow-right-l model-zoom__sizes-arrow model-zoom__sizes-arrow--rotate"></span></label><input
                                                                    type="number" step="0.1"
                                                                    class="form-control ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min"
                                                                    id="zoomWidth" ng-model="size.height"
                                                                    ng-change="onChangeHeight()" min="0.1"></div>
                                                        <!-- end ngIf: scaleType.type == 'dimension' -->
                                                        <!-- ngIf: scaleType.type == 'dimension' -->
                                                        <div class="model-zoom__sizes-item "><label
                                                                    class="control-label" for="zoomHeight">height <span
                                                                        class="tsi tsi-arrow-up-l model-zoom__sizes-arrow"></span></label><input
                                                                    type="number" step="0.1"
                                                                    class="form-control ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min"
                                                                    id="zoomHeight" ng-model="size.length"
                                                                    ng-change="onChangeLength()" min="0.1"></div>
                                                        <!-- end ngIf: scaleType.type == 'dimension' -->
                                                        <!-- ngIf: scaleType.type == 'percent' -->
                                                        <!-- ngIf: scaleType.type == 'ration' --></div>
                                                    <div class="model-zoom__btns">
                                                        <button class="btn btn-primary btn-sm" ng-click="apply()">
                                                            <!-- ngIf: showProgress --><!-- ngIf: !showProgress --><span
                                                                    ng-if="!showProgress" class="">
                        Apply                    </span><!-- end ngIf: !showProgress --></button>
                                                    </div><!-- ngIf: isSizeChanged() -->
                                                    <div class="model-zoom__original-sizes ng-binding ">
                                                        <button ng-click="revertSize()" type="button"
                                                                class="btn btn-link btn-sm p-l0 p-r0">Revert to original
                                                            size:
                                                        </button>
                                                        105.62 x 96.91 x 123.43 x mm
                                                    </div><!-- end ngIf: isSizeChanged() --></div>
                                            </div>
                                        </div>
                                    </div><!-- end ngIf: model3d.isCalculated() && !model3d.isKit() --></div>
                                <div class="models__qty"><label class="control-label"
                                                                for="getprintedform-qty">Quantity</label><input
                                            type="number"
                                            class="form-control input-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max"
                                            name="qty" style="-moz-appearance: textfield;"
                                            ng-blur="updateModel3dPartQty(model3dPart)" min="0" max="1000000" value="1"></div>
                                <button ng-disabled="!model3dPart.isCalculated()" ng-click="duplicate(model3dPart)"
                                        class="models__duplicate" title="Duplicate"><i class="tsi tsi-duplicate"></i>
                                </button>
                                <button ng-click="cancelUploadItem(model3dPart)" class="models__del" title="Delete">×
                                </button>
                            </div><!-- end ngIf: !model3dPart.isCanceled -->
                        </div>
                        <!-- end ngRepeat: model3dPart in model3d.parts -->
                        <!-- ngRepeat: model3dImg in model3d.images -->
                        <div class="row m-l0 m-r0">
                            <div class="col-sm-12 p-l0">
                                <div class="models__controls">
                                    <button class="btn btn-primary btn-sm models__add-btn" ng-click="uploader.addFileClick()">
                                        <span class="tsi tsi-plus m-r10"></span>
                                        Upload Files
                                    </button>
                                    <button class="btn btn-default btn-sm models__add-btn" ng-click="preview3d()"
                                            ng-disabled="!model3d.hasPreview3d()"><span class="tsi tsi-cube m-r10"></span>
                                        3D Viewer
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="form-group p-t10">
                                    <label class="control-label" for="preorderDescr">Description of your orders (It can help vendor to make everything perfect)</label>
                                    <textarea class="form-control " rows="3" id="preorderDescr" required="required"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="aff-widget-bottom">
                    <button ng-click="nextStep()" class="btn btn-primary">
                        Next <span class="tsi tsi-right"></span>
                    </button>
                </div>
            </div>
        </ui-view>
    </div>
</div>

<hr class="m-t30 m-b30">


<div class="aff-widget  " id="print-navigation">
    <!-- Nav tabs -->
    <ul class="aff-widget-tabs clearfix">
        <li class="active" >
            <a href="#delivery" aria-controls="delivery">
                <span class="aff-widget-tabs__step">1</span>
                Project Description </a>
        </li>
    </ul>
    
    <div class="aff-widget-content clearfix">
        <!-- uiView: -->
        <ui-view class=" ">
            <div class="tab-pane  " id="delivery">

                <div class="form-horizontal m-t15">
                <div class="form-group row">
                    <label class="control-label col-sm-3 col-md-2" for="preorderProduct">Company</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12 ugc-content">
                                <div class="preorder-service-ava"><img
                                            src="https://static.treatstock.com/static/user/9fd263afbd2487f25d35774e974c4a13/ps_logo_circle_1611892588.jpg"
                                            alt="Jinan BCAMCNC Machinery Co., Ltd"></div>
                                <div class="preorder-service-name">
                                    <h4 class="m-t0 m-b0">Jinan BCAMCNC Machinery Co., Ltd</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- ngIf: isShowName() --><!-- ngIf: isShowSelectClient() --><!-- ngIf: psShowName() -->
                <!-- ngIf: isShowProduct() -->
                <div class="form-group row ng-scope">
                    <label class="control-label col-sm-3 col-md-2" for="preorderProduct">Product</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12 ugc-content"><h4 class="ng-binding">8mm Carbon Steel YAG
                                    fiberLaser Cutting Machine</h4></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><img
                                        ng-src="https://static.treatstock.com/static/files/b9/22/2664790_109465_3baa7713f666a1d1e15369050dabe5c2_720X540.jpg?date=1615445306"
                                        class="preorder-product-image"
                                        src="https://static.treatstock.com/static/files/b9/22/2664790_109465_3baa7713f666a1d1e15369050dabe5c2_720X540.jpg?date=1615445306">
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <label
                                            class="control-label col-sm-4 col-md-3 p-t0">
                                        Ship from </label>
                                    <div class="col-sm-8 m-b10 ng-binding">
                                        Qingdao, Shandong&nbsp;
                                    </div>
                                </div>
                                <div class="row">
                                    <label
                                            class="control-label col-sm-4 col-md-3 p-t0">
                                        Incoterms </label>
                                    <div class="col-sm-8 m-b10 ng-binding">
                                        FOB&nbsp;
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="control-label col-sm-4 col-md-3 p-t0">
                                        Price </label>
                                    <div class="col-sm-8">
                                        $37,000.00
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row ng-scope">
                    <label class="control-label col-sm-3 col-md-2" for="preorderQuantity">Project Description<span
                                class="form-required">*</span></label>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3" id="preorderDescr" required="required"></textarea>
                    </div>
                </div>
                <div class="form-group row ng-scope">
                    <label class="control-label col-sm-3 col-md-2" for="preorderQuantity">Quantity<span
                                class="form-required">*</span></label>
                    <div class="col-sm-5"><input type="number"
                                                 class="form-control pull-left ng-valid-min ng-not-empty ng-dirty ng-valid-number ng-valid ng-valid-required ng-touched"
                                                 id="preorderQuantity" ng-model="preorder.quantity"
                                                 required="required" style="max-width: 90px" min="1" step="any">
                        <div class="form-control-static pull-left m-l10 ng-binding">
                            pieces
                        </div>
                    </div>
                </div>
                <div class="form-group row ng-scope">
                    <label
                            class="control-label col-sm-3 col-md-2" for="preorderTotal">Total price </label>
                    <div class="col-sm-9">
                        <div class="form-control-static ng-binding">
                            USD 74000.00
                        </div>
                    </div>
                </div><!-- end ngIf: isShowTotalPrice() -->
                <div class="form-group row">
                    <label class="control-label col-sm-3 col-md-2" for="preorderCompany">
                        Company (optional) </label>
                    <div class="col-sm-6 col-md-5"><input
                                class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                ng-model="preorder.shipTo.company" id="preorderCompany" placeholder="Company"></div>
                </div><!-- ngIf: isShowProjectDescription() --><!-- ngIf: isShowAttachFiles() -->
                <!-- ngIf: isShowBudget() -->
                <div class="form-group row">
                    <label class="control-label col-sm-3 col-md-2" for="preorderTime">Deadline</label>
                    <div class="col-sm-6 col-md-5">
                        <div class="input-group"><span class="input-group-addon"><span
                                        class="tsi tsi-alarm-clock"></span></span><input
                                    class="form-control ng-pristine ng-untouched ng-valid ng-empty ng-valid-min"
                                    type="number" id="preorderTime" placeholder="Days" min="0"
                                    ng-model="preorder.estimateTime"></div>
                    </div>
                </div><!-- ngIf: isGuest -->
                <div class="form-group row">
                    <label class="control-label col-sm-3 col-md-2"
                                                   for="shipTo">Location<span class="form-required">*</span></label>
                    <div class="col-sm-9 p-t10"><input type="hidden"
                                                       class="form-control ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required"
                                                       rows="3" ng-model="preorder.shipTo" id="preorderShipTo"
                                                       required="required">
                        <div class="UserLocationWidget"><span class="tsi tsi-map-marker"></span>
                            <button type="button" class="ts-user-location ts-ajax-modal btn-link"
                                    title="Shipping Address" data-url="/geo/change-location"
                                    data-target="#changelocation">Kazan, RU
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-sm-3 col-md-2"
                                                   for="preorderShippingAddress">Shipping Address </label>
                    <div class="col-sm-6 col-md-5"><input
                                class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                ng-model="preorder.shipTo.address" id="preorderShippingAddress"
                                placeholder="Shipping Address"></div>
                </div><!-- ngIf: isShowMessageToManufacturer() -->
                <div class="form-group row ng-scope">
                    <label
                            class="control-label col-sm-3 col-md-2" for="preorderMsg">Message to
                        manufacturer</label>
                    <div class="col-sm-6 col-md-5"><textarea
                                class="form-control ng-pristine ng-untouched ng-valid ng-empty" rows="3"
                                id="preorderMsg" ng-model="preorder.message"></textarea></div>
                </div><!-- end ngIf: isShowMessageToManufacturer() -->
                <div class="row">
                    <div class="col-sm-3 col-md-2"></div>
                    <div class="col-sm-6 col-md-5 m-b10">
                        <script src="https://www.google.com/recaptcha/api.js"></script>
                        <div class="g-recaptcha" data-sitekey="6LddAjUUAAAAAK6syp59F28KU8COgLof9D727OlY">
                            <div style="width: 304px; height: 78px;">
                                <div>
                                    <iframe title="reCAPTCHA"
                                            src="https://www.google.com/recaptcha/api2/anchor?ar=2&amp;k=6LddAjUUAAAAAK6syp59F28KU8COgLof9D727OlY&amp;co=aHR0cHM6Ly93d3cudHJlYXRzdG9jay5jb206NDQz&amp;hl=ru&amp;v=RDRwZ7RcROX_wCxEJ01WeqEX&amp;size=normal&amp;cb=3syco75eaefl"
                                            width="304" height="78" role="presentation" name="a-cbk1eaig5g84"
                                            frameborder="0" scrolling="no"
                                            sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe>
                                </div>
                                <textarea id="g-recaptcha-response" name="g-recaptcha-response"
                                          class="g-recaptcha-response"
                                          style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea>
                            </div>
                            <iframe style="display: none;"></iframe>
                        </div>
                    </div>
                </div>
                <div class="row m-b20"><!-- ngIf: !preorder.createdByPs -->
                    <div ng-if="!preorder.createdByPs" class="col-sm-5 col-sm-offset-3 col-md-offset-2 ng-scope">
                        The supplier will review your request and respond with a quote shortly. We thank you for
                        your patience.
                    </div>
                </div>

                <div class="aff-widget-bottom">
                    <button ng-click="prevStep()" class="btn btn-default">
                        <span class="tsi tsi-left"></span>
                        Back
                    </button>
                    <button class="btn btn-primary" ng-click="nextStep()" ng-disabled="showProgress">
                        <span ng-if="!showProgress" class=" ">
                            Next
                        </span>
                    </button>
                </div>
            </div>
        </ui-view>
    </div>
</div>