<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

$this->registerCssFile('@web/css/cutting-widget.css');
?>

<div class="m-t30 p-b20">
<!-- Распорка для отступа от шапки. В дальнейшем убрать или оставить только на нужной странице внутри сайта ТС -->
</div>

<div class="container">

    <div class="row">

        <div class="col-md-12 ps-steps ps-steps--cutting">

            <h2 class="ps-steps__title">
                New 2D cutting service
            </h2>

            <?php /*

            ### Temp disible inner navigation

            <ul id="tabs" class="nav nav-tabs nav-tabs--secondary">
                <li><a href="#">Printer</a></li>
                <li class="active"><a href="#">MaterialOptions</a></li>
                <li><a href="#">Delivery Details</a></li>
                <li><a href="#">Certification</a></li>
            </ul>
            */ ?>

            <div id="my-tab-content" class="tab-content">

                <div class="p-t20 m-b20">
                    <span class="m-r10">Unit of measurement:</span>
                    <div class="btn-group btn-switcher" data-toggle="buttons">
                        <label class="btn btn-default btn-sm" ng-class="{'active':csWindow.measurement=='ft'}" ng-click="csWindow.setMeasurement('ft')">
                            <input type="radio" value="ft" ng-model="csWindow.measurement" id="optionsPrice1" autocomplete="off" class="ng-pristine ng-untouched ng-valid ng-not-empty" name="3">
                            ft
                        </label>
                        <label class="btn btn-default btn-sm active" ng-class="{'active':csWindow.measurement=='m'}" ng-click="csWindow.setMeasurement('m')">
                            <input type="radio" value="m" id="optionsPrice2" autocomplete="off" ng-model="csWindow.measurement" class="ng-pristine ng-untouched ng-valid ng-not-empty" name="4">
                            m
                        </label>
                    </div>
                </div>

                <div class="row cutting-manufacturer">

                    <div class="col-sm-4 wide-padding--right">

                        <select class="form-control">
                            <option label="Plastic" value="Plastic">Plastic</option>
                            <option label="Metal" value="Metal">Metal</option>
                            <option label="Something" value="Something">Something</option>
                        </select>

                        <br>

                        <select class="form-control">
                            <option label="ABS" value="number:3">ABS</option>
                            <option label="FLEX" value="number:14">FLEX</option>
                            <option label="PETG" value="number:62">PETG</option>
                            <option label="PLA" value="number:2" selected="selected">PLA</option>
                            <option label="Rubber (TPU)" value="number:43">Rubber (TPU)</option>
                        </select>

                        <br>

                        <button type="button" class="btn btn-sm btn-danger btn-ghost m-b30" title="Delete Material">
                            <span class="tsi tsi-bin m-r10"></span>Delete Material
                        </button>

                    </div>

                    <div class="col-sm-8">
                        <div class="cutting-manufacturer__settings">
                            <div class="form-group">
                                <label for="input1">Width</label>
                                <input type="number" class="form-control" id="input1">
                                <div class="cutting-manufacturer__value">mm</div>
                            </div>

                            <div class="form-group">
                                <label for="input2">Cut price</label>
                                <input type="number" class="form-control" id="input2">
                                <div class="cutting-manufacturer__value">/m</div>
                            </div>

                            <div class="form-group">
                                <label for="input3">Material price</label>
                                <input type="number" class="form-control" id="input3">
                                <div class="cutting-manufacturer__value">m<sup>2</sup></div>
                            </div>

                            <div class="form-group">
                                <label for="input4">Max length</label>
                                <input type="number" class="form-control" id="input4">
                                <div class="cutting-manufacturer__value">mm</div>
                            </div>

                            <div class="form-group">
                                <label for="input5">Max height</label>
                                <input type="number" class="form-control" id="input5">
                                <div class="cutting-manufacturer__value">mm</div>
                            </div>

                            <button type="button" class="btn btn-primary btn-circle" title="Delete Color">
                                <span class="tsi tsi-bin"></span>
                            </button>
                        </div>

                        <div class="cutting-manufacturer__settings">
                            <div class="form-group">
                                <label for="input1">Width</label>
                                <input type="number" class="form-control" id="input1">
                                <div class="cutting-manufacturer__value">mm</div>
                            </div>

                            <div class="form-group">
                                <label for="input2">Cut price</label>
                                <input type="number" class="form-control" id="input2">
                                <div class="cutting-manufacturer__value">/m</div>
                            </div>

                            <div class="form-group">
                                <label for="input3">Material price</label>
                                <input type="number" class="form-control" id="input3">
                                <div class="cutting-manufacturer__value">m<sup>2</sup></div>
                            </div>

                            <div class="form-group">
                                <label for="input4">Max length</label>
                                <input type="number" class="form-control" id="input4">
                                <div class="cutting-manufacturer__value">mm</div>
                            </div>

                            <div class="form-group">
                                <label for="input5">Max height</label>
                                <input type="number" class="form-control" id="input5">
                                <div class="cutting-manufacturer__value">mm</div>
                            </div>

                            <button type="button" class="btn btn-primary btn-circle" title="Delete Color">
                                <span class="tsi tsi-bin"></span>
                            </button>
                        </div>

                        <button type="button" class="btn btn-default btn-sm btn-ghost m-b30">
                            <span class="tsi tsi-plus m-r10"></span>Add New Width
                        </button>
                    </div>
                </div>

                <div class="row cutting-manufacturer">

                    <div class="col-sm-4 wide-padding--right">

                        <select class="form-control">
                            <option label="Metal" value="Metal">Metal</option>
                            <option label="Plastic" value="Plastic">Plastic</option>
                            <option label="Something" value="Something">Something</option>
                        </select>

                        <br>

                        <select class="form-control">
                            <option label="Чугуний" value="number:3">Чугуний</option>
                            <option label="Алюминий" value="number:14">Алюминий</option>
                            <option label="Сталь" value="number:62">Сталь</option>
                        </select>

                        <br>

                        <button type="button" class="btn btn-sm btn-danger btn-ghost m-b30" title="Delete Material">
                            <span class="tsi tsi-bin m-r10"></span>Delete Material
                        </button>

                    </div>

                    <div class="col-sm-8">
                        <div class="cutting-manufacturer__settings">
                            <div class="form-group">
                                <label for="input1">Width</label>
                                <input type="number" class="form-control" id="input1">
                                <div class="cutting-manufacturer__value">mm</div>
                            </div>

                            <div class="form-group">
                                <label for="input2">Cut price</label>
                                <input type="number" class="form-control" id="input2">
                                <div class="cutting-manufacturer__value">/m</div>
                            </div>

                            <div class="form-group">
                                <label for="input3">Material price</label>
                                <input type="number" class="form-control" id="input3">
                                <div class="cutting-manufacturer__value">m<sup>2</sup></div>
                            </div>

                            <div class="form-group">
                                <label for="input4">Max length</label>
                                <input type="number" class="form-control" id="input4">
                                <div class="cutting-manufacturer__value">mm</div>
                            </div>

                            <div class="form-group">
                                <label for="input5">Max height</label>
                                <input type="number" class="form-control" id="input5">
                                <div class="cutting-manufacturer__value">mm</div>
                            </div>

                            <button type="button" class="btn btn-primary btn-circle" title="Delete Color">
                                <span class="tsi tsi-bin"></span>
                            </button>
                        </div>

                        <button type="button" class="btn btn-default btn-sm btn-ghost m-b30">
                            <span class="tsi tsi-plus m-r10"></span>Add New Width
                        </button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-primary m-b10 m-r30 pull-left">
                            <span class="tsi tsi-plus m-r10"></span>Add New Material
                        </button>
                        <a class="m-t10 pull-left" href="/site/contact" target="_blank">Material not on the list? Contact us</a>
                    </div>
                </div>

                <div class="row p-t20">
                    <div class="col-sm-6 col-md-5 col-lg-4 additional-price-div m-b20">
                        <div class="checkbox">
                            <input type="checkbox">
                            <label for="additionalWorkCost">Minimum order charge:</label>
                        </div>

                        <div class="additional-price-div__help">
                            <a class="ts-ajax-modal"
                               href="/site/help?alias=ps.add-printer.mater-opt.min-charge-for-print"
                               title="Minimum Order Charge"><i class="tsi tsi-question"></i>
                            </a>
                        </div>

                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input class="form-control" autocomplete="off">
                        </div>
                    </div>
                </div>

            </div>


            <div class="ps-steps__footer row">

                <div class="col-xs-12">
                    <div class="ps-steps__footer-save-btn">
                        <a class="btn btn-link" href="/mybusiness/services">Cancel</a>
                        <button type="button" class="btn btn-primary js-save-printer">
                            Save
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>