<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

$this->registerCssFile('@web/css/cutting-widget.css');
?>

<div class="m-t30 p-b20">
<!-- Распорка для отступа от шапки. В дальнейшем убрать или оставить только на нужной странице внутри сайта ТС -->
</div>

<div class="cutting-widget">

    <div class="cutting-widget__header row">
        <h2 class="cutting-widget__title ugc-content">
            <a target="_blank" class="cutting-widget__avatar" href="#company">
                <img src="http://static.h5.tsdev.work/static/user/4a08142c38dbe374195d41c04562d9f8/ps_logo_circle_1529932395_64x64.png">
            </a>
            <a target="_blank" href="#company">
                Laser 5000 UberCutting service
            </a>
        </h2>
        <a href="https://treatstock.com" class="cutting-widget__copy" target="_blank">
            <span>Powered by</span>
            <img src="https://static.treatstock.com/static/images/ts-logo.svg" width="120px" height="15px">
        </a>
    </div>

    <div class="cutting-widget__body">
        <div class="row">

            <div class="col-sm-6 wide-padding wide-padding--right">
                <!-- Функцию загрузки вешайте сразу на окно .cutting-widget__upload, а не только на кнопку -->
                <div class="cutting-widget__upload">
                    <svg class="cutting-widget__upload-ico" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 491.52 491.52">
                        <path d="M430.08 245.76v-61.44h-61.44v20.48h-92.16v-20.48h-61.44v20.48h-92.16v-20.48H61.44v61.44h61.44v-20.48h43.7c-47.88 26.6-80.62 76.4-84.18 133.12H61.44v61.44h61.44V358.4h-20c4.53-63.16 50.5-116.28 112.16-129.76v17.12h61.44v-17.12c61.66 13.48 107.63 66.6 112.16 129.76h-20v61.44h61.44V358.4h-20.96c-3.56-56.71-36.3-106.53-84.17-133.12h43.69v20.48h61.44zm-40.96-40.96h20.48v20.48h-20.48V204.8zM102.4 225.28H81.92V204.8h20.48v20.48zm0 174.08H81.92v-20.48h20.48v20.48zM256 225.28h-20.48V204.8H256v20.48zm133.12 153.6h20.48v20.48h-20.48v-20.48z" fill="currentColor"/>
                        <path d="M352.4 0H30.72v491.52H460.8V108.4L352.4 0zm6 34.96l67.44 67.44H358.4V34.96zm81.92 436.08H51.2V20.48h286.72v102.4h102.4v348.16z" fill="currentColor"/>
                    </svg>
                    <button class="btn btn-primary cutting-widget__upload-btn">Upload File</button>
                    <div class="cutting-widget__upload-hint">
                        <strong>Upload SVG file</strong>
                        <br>
                        <small>All files are protected by Treatstock security</small>
                    </div>
                </div>

                <div class="cutting-widget__img-container">
                    <img src="/static/images/logo-pie.svg" alt="filename.svg">
                </div>

                <div class="cutting-widget__data">
                    <div class="cutting-widget__info">
                        <h4 class="cutting-widget__filename">templateFile.svg</h4>
                        <div class="cutting-widget__size">
                            <table class="table cutting-widget__size-table">
                                <tr>
                                    <th>
                                        Size
                                    </th>
                                    <td>
                                        215 x 109 x 22mm <span style="display: inline-block;">(9.2 x 4.6 x 0.9 in)</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Cut length
                                    </th>
                                    <td>
                                        2150mm (921 in)
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="cutting-widget__qty">
                        <label class="control-label" for="cutting-widget-qty">Quantity</label>
                        <input type="number" class="form-control input-sm" name="cutting-widget-qty" min="0" max="1000000">
                    </div>
                    <button class="cutting-widget__del" title="Delete">
                        <span class="tsi tsi-bin"></span>
                    </button>
                </div>

                <div class="cutting-widget__units">
                    <span class="cutting-widget__units-label">
                        Unit of measurement:
                    </span>
                    <div class="btn-group btn-switcher" data-toggle="buttons">
                        <label class="btn btn-default btn-xs active" ng-class="{'active':model3d.modelUnits=='mm'}" ng-click="changeScale('mm')">
                            <input type="radio" name="optionsPrice" id="optionsPrice1" autocomplete="off">
                            MM
                        </label>
                        <label class="btn btn-default btn-xs" ng-class="{'active':model3d.modelUnits=='in'}" ng-click="changeScale('in')">
                            <input type="radio" name="optionsPrice" id="optionsPrice2" autocomplete="off">
                            IN
                        </label>
                    </div>
                </div>

                <div class="m-b30">
                    <button class="btn-sm btn btn-info collapsed cutting-widget__scale-btn" data-toggle="collapse" data-target="#modelZoom"
                            aria-expanded="false" aria-controls="modelZoom">
                        Scale
                        <span class="tsi tsi-down"></span>
                    </button>
                    <div class="collapse " id="modelZoom" aria-expanded="false">
                        <div class="model-zoom model-zoom--cutting-widget">
                            <div class="model-zoom__sizes">
                                <div class="model-zoom__sizes-item">
                                    <input type="number" step="0.1" class="form-control ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min" id="zoomWidth" ng-model="size.height" ng-change="onChangeHeight()" min="0.1">
                                    <label class="control-label" for="zoomWidth">width <span class="tsi tsi-arrow-right-l model-zoom__sizes-arrow"></span></label>
                                </div>

                                <div class="model-zoom__sizes-item">
                                    <input type="number" step="0.1" class="form-control ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min" id="zoomHeight" ng-model="size.length" ng-change="onChangeLength()" min="0.1">
                                    <label class="control-label" for="zoomHeight">height <span class="tsi tsi-arrow-up-l model-zoom__sizes-arrow"></span></label>
                                </div>

                                <div class="model-zoom__sizes-item model-zoom__sizes-item--measure">
                                    <b>mm</b>
                                </div>

                                <div class="model-zoom__original-sizes">
                                    <button type="button" class="btn btn-link btn-sm p-l0 p-r0">Revert to original size:</button>
                                    7.28 x 1.82 x 0.10 x in
                                </div>

                            </div>

                            <div class="model-zoom__btns">
                                <button class="btn btn-primary btn-sm">
                                    Apply
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-sm-6">

                <h4 class="cutting-widget__material-title">Material</h4>

                <div class="material-switcher cutting-widget__material">
                    <div class="material-switcher__item material-switcher__item--active" title="Wood">
                        Wood
                    </div>
                    <div class="material-switcher__item" title="Metal">
                        Metal
                    </div>
                    <div class="material-switcher__item" title="ABS">
                        ABS
                    </div>
                    <div class="material-switcher__item" title="Steel">
                        Steel
                    </div>
                    <div class="material-switcher__item" title="Aluminium">
                        Aluminium
                    </div>
                    <div class="material-switcher__item" title="Plywood">
                        Plywood
                    </div>
                    <div class="material-switcher__item" title="Desk">
                        Desk
                    </div>
                </div>

                <h4 class="cutting-widget__material-title">Thickness</h4>

                <div class="input-group cutting-widget__select-group">
                    <select class="form-control input-sm">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <span class="input-group-addon" >mm</span>
                </div>

                <div class="cutting-widget__price">
                    <h3 class="cutting-widget__price-value">Cost $100500</h3>
                    <button class="btn btn-danger">Proceed to checkout</button>
                </div>

            </div>

        </div>
    </div>
</div>