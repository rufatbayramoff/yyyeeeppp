<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>

<div class="container invoice">
    <div class="row">
        <div class="col-sm-12">
            <div class="invoice__header">
                <img src="/static/images/ts-logo.svg" alt="Tratstock" class="invoice__logo" width="190px" height="24px">
                Invoice #100500
            </div>
        </div>
    </div>
    <div class="row m-b20">
        <div class="col-sm-4">
            <h4 class="m-b0">Bill To:</h4>
            <p>
                FULLNAME Buzname<br>
                11 W 53RD ST<br>
                NEW YORK NY 10019<br>
                US Phone: 79503222522
            </p>
        </div>
        <div class="col-sm-4">
            <h4 class="m-b0">Ship To:</h4>
            <p>
                FULLNAME Buzname<br>
                11 W 53RD ST<br>
                NEW YORK NY 10019<br>
                US Phone: 79503222522
            </p>
        </div>
        <div class="col-sm-4">
            <div class="table-responsive">
                <table class="table--invoice-data">
                    <tbody>
                    <tr>
                        <th>Creation Date</th>
                        <td>10/10/2017</td>
                    </tr>
                    <tr>
                        <th>Due Date</th>
                        <td>20/10/2017</td>
                    </tr>
                    <tr>
                        <th>Currency</th>
                        <td>USD</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="invoice__table">
                    <tbody>
                        <tr>
                            <th>Store unit id</th>
                            <th>Desсription</th>
                            <th>Qty</th>
                            <th>Unit Price</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td>#1</td>
                            <td>Kakaya-to_detal.stl #3rd_revision</td>
                            <td>1</td>
                            <td>$20</td>
                            <td>$20</td>
                        </tr>
                        <tr>
                            <td>#2</td>
                            <td>Ftoraya_detal.stl</td>
                            <td>100</td>
                            <td>$30</td>
                            <td>$3000</td>
                        </tr>
                        <tr>
                            <td>#3</td>
                            <td>Number_tree_detal.stl</td>
                            <td>10</td>
                            <td>$50</td>
                            <td>$500</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">Subtotal</td>
                            <td>$3520</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">Shipping fee</td>
                            <td>$200</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">Service fee</td>
                            <td>$200</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">
                                <strong>Total</strong>
                            </td>
                            <th>$3920</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <p>Please pay amount (within 10 days) to:</p>
            Treatstock Inc<br>
            Address 40 east Main street Suite 900<br>
            Newark, DE 19711<br>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <hr>
            <h4 class="m-b0">Bank details</h4>
            Bank of America<br>

            Account number: 325059041986<br>
            Routing number: 121000358 (paper & electronic), 026009593 (wires)<br>
        </div>
    </div>
</div>
