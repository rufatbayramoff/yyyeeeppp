<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

use common\models\Ps;
use frontend\widgets\PsPrintServiceReviewStarsWidget;

use kartik\select2\Select2Asset;
use kartik\select2\ThemeKrajeeAsset;
use yii\bootstrap\ActiveForm;

$ps = Ps::findByPk(1);

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

Select2Asset::register($this);
ThemeKrajeeAsset::register($this);

?>

    <div class="hero-banner hero-banner--2col promo-page-section--dark">
        <div class="hero-banner__pic-big" style="background-image: url(https://static.treatstock.com/static/images/main-page-temp/cnc-bg.jpg);">
        </div>

        <div class="container p-t30 p-b30">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="hero-banner__title">
                        Heading H1
                    </h1>
                    <p class="hero-banner__lead lead m-b30">
                        Description text block. Can delete it.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="promo-page-section promo-page-section--grey1">
        <div class="container container--wide">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Widget place -->
                    <div class="quote-widget">

                        <div class="quote-widget__header">
                            Request a Quote    </div>

                        <div class="quote-widget__body">
                            <form name="preorderForm" id="preorderForm" class="form-horizontal ng-pristine ng-valid-min ng-invalid ng-invalid-required">

                                <div class="form-group row ng-scope">
                                    <label class="control-label col-sm-3" for="preorderName">Your name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid-required ng-empty ng-invalid ng-invalid-required" id="preorderName" placeholder="Client name" ng-model="preorder.name" required="required">
                                    </div>
                                </div>

                                <div class="form-group row ng-scope">
                                    <label class="control-label col-sm-3" for="preorderDescr">Project Description</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" rows="3" id="preorderDescr" ng-model="preorder.description" required="required"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row ng-scope">
                                    <label class="control-label col-sm-3">Attach Files</label>
                                    <div class="col-sm-9">
                                        <button type="button" class="btn btn-primary ng-isolate-scope dz-clickable" dropzone-button="preorder.files" accepted-files=".pdf, .doc, .docx, .txt, .odt, .xls, .xlsx, .csv, .jpg, .jpeg, .gif, .png, .mp4, .webm, .ogg, .ogv, .avi, .mng, .mov, .qt, .3gp, .3g2, .stl, .ply, .step, .iges, .stp, .igs, .3mf, .obj, .cdr, .dxf, .eps, .svg, .zip, .dxf, .prt, .neu, .asm, .dft, .psm, .pwd, .par, .sldasm, .slddrw, .sldprt, .edrw, .svd, .dwg, .dws, .dwt, .dxe, .pwt, .fbx, .123c, .123d, .123dx, .ipj, .ipn, .ipt, .iam, .idv, .idw, .model, .mdl, .catdrawing, .catpart, .catproduct, .cgr, .dlv, .x_b, .x_t, .fz, .fzb, .fzm, .fzp, .fzz, .fcstd, .g, .scad, .xpk, .cdw, .dwf, .jt, .cad, .dgk, .cnc, .nc, .nc1, .3ds, .prj, .shp, .blend, .dae, .obj, .off, .sc1, .skp, .wrl, .vrml, .x3d, .xsi, .ztl, .xyz, .cdr, .dxf, .eps, .svg" max-files="20"><i class="tsi tsi-upload-l dz-clickable"></i> Browse files
                                        </button>

                                        <p class="help-block">Maximum limit of 20 files. Max size: 200 Mb.</p>

                                        <div class="quote-widget__hint-links">
                                            <span class="tsi tsi-lock m-r10"></span>
                                            <span>All files are protected by                                <a href="https://www.treatstock.com/" target="_blank" class="upload-block__hint-links-ts">Treatstock</a>
                                security                        </span>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="shipTo">Location</label>
                                    <div class="col-sm-9 p-t5">
                                        <input type="hidden" class="form-control ng-pristine ng-untouched ng-valid-required ng-empty ng-invalid ng-invalid-required" rows="3" ng-model="preorder.shipTo" id="preorderShipTo" required="required">
                                        <div class="UserLocationWidget">
                                            <span class="tsi tsi-map-marker"></span>
                                            <button type="button" class="ts-user-location ts-ajax-modal btn-link" title="Shipping Address" data-url="/geo/change-location" data-target="#changelocation">New York, US
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="quote-widget__hint m-t20 m-b20">
                                    We will review your request and respond with a quote shortly. We thank you for your patience.            </div>

                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-primary" loader-click="submitForm()">Submit</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="promo-page-section promo-page-section--dark">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                    <h2 class="m-t0 m-b15 text-center animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'Heading H2 text block'); ?>
                    </h2>
                    <div style="font-size: 16px" class="animated"  data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <!-- Text container -->
                        <p><?= _t('front', 'Seo text goes here.'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="promo-page-section promo-page-section--grey p-b30">
        <div class="container container-wide">
            <div class="picture-slider swiper-container">
                <div class="swiper-wrapper">
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-animals.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-animals.png" alt="">
                    </a>
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-art.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-fashion.png" alt="">
                    </a>
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-games.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-games.png" alt="">
                    </a>
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-home.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-home.png" alt="">
                    </a>
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-sport.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-sport.png" alt="">
                    </a>
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-technology.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-technology.png" alt="">
                    </a>
                </div>
                <div class="picture-slider__scrollbar swiper-scrollbar"></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center p-t30">
                <h3 class="h2 m-b30 animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                    Some heading
                </h3>
                <a href="#кнопка со ссылкой на категорию в каталоге сервисов" class="btn btn-lg btn-danger m-b20 animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                    Services
                </a>
            </div>
        </div>
    </div>




    <script>
        <?php $this->beginBlock('js1', false); ?>

        function initPics() {
            //Init Picture Slider
            var swiperPictureSlider = new Swiper('.picture-slider', {
                scrollbar: '.picture-slider__scrollbar',
                scrollbarHide: true,
                slidesPerView: 'auto',
                grabCursor: true
            });
        }

        initPics();
        $(window).resize(function () {initPics()});

        <?php $this->endBlock(); ?>
    </script>
<?php $this->registerJs($this->blocks['js1']); ?>