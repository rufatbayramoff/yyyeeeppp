<?php

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);


Yii::$app->angular
    ->service(['notify', 'router', 'user'])
    ->controller('promo/PromoContactUsController');
?>

<style>
    .promo-head {
        position: relative;
        padding: 40px 0;
        color: #ffffff;
        background-color: #351159;
        background-image: linear-gradient(135deg, #351159 0%, #b64a5a 50%, #f8a24d 100%);

    }
    .promo-head__title {
        margin-top: 0;
        font-size: 50px;
        line-height: 55px;
    }
    .promo-head__subtitle {
        margin-bottom: 30px;
        font-size: 18px;
        line-height: 25px;
    }
    .promo-head .btn-default {
        background: transparent;
        color: #ffffff;
        border-color: #ffffff;
    }
    @media (max-width: 550px) {
        .promo-head {
            padding: 50px 0;
        }
        .promo-head__title {
            font-size: 30px;
            line-height: 40px;
        }
    }

</style>

<section class="promo-head">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="promo-head__title"><?= _t('site.page', 'Treatstock Widget Demo'); ?></h1>
                <p class="promo-head__subtitle"><?= _t('site.page', 'Contact us for more information about our rewards program.'); ?></p>
                <a href="/l/partnership#contactForm" class="btn btn-primary m-b10"><?= _t('site.page', 'Contact Us'); ?></a>
            </div>
        </div>
    </div>
</section>


<div class="container m-t30">
    <a href="/l/partnership" class="btn btn-default m-b10"><span class="tsi tsi-left m-r10"></span><?= _t('site.page', 'Back'); ?></a>
</div>

<div class="container m-t30">
    <!-- UserWidget: asIs8y -->
    <link href="https://www.treatstock.com/css/embed-user.css" rel="stylesheet">
    <div class="ts-embed-tslink"><a href="https://www.treatstock.com/" target="_blank">Powered by Treatstock</a></div>
    <iframe class="ts-embed-userwidget" frameborder="0" height="650px" src="https://www.treatstock.com/my/print-model3d/widget?userUid=asIs8y" width="100%"></iframe>
</div>

<script>
    <?php $this->beginBlock('js1', false); ?>

    $('a[data-target]').click(function () {
        var scroll_elTarget = $(this).attr('data-target');
        if ($(scroll_elTarget).length != 0) {
            $('html, body').animate({scrollTop: $(scroll_elTarget).offset().top - 105}, 350);
        }
        return false;
    });

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>