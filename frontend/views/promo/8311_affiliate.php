<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>
<div class="container">

    <div class="row">
        <div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 m-t30">
            <div class="panel panel-default box-shadow border-0">
                <div class="panel-body">

                    <h1 class="text-center m-t0 m-b10">
                        <?= _t('front.user', 'Become a Partner'); ?>
                    </h1>

                    <p class="text-center m-b20">Если вы хотите быть supplier, перейдите в раздел <a href="#" target="_blank">Create Business</a></p>

                    <form id="request-password-reset-form" class="form-horizontal ng-pristine ng-valid"
                          action="/user/forgot-password" method="post">
                        <input type="hidden" name="_frontendCSRF"
                               value="XKqNSKRgP6XbYB09Y77ZQoSzVtEJU59slQ8roOWBJQAK78ME0gdOz6M3fAQX3LwJ0_Uxjkwdr1WjYkfXjPsUdw==">

                        <div class="form-group">
                            <label class="control-label col-sm-4 m-b0" for="partner"><?= _t('front.user', 'Partnership type'); ?><span class="form-required">*</span></label>
                            <div class="col-sm-8">
                                <div class="checkbox checkbox-primary p-t10">
                                    <input id="checkbox11" type="checkbox">
                                    <label for="checkbox11">
                                        <?= _t('front.user', 'Affiliate'); ?>
                                    </label>
                                </div>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox22" type="checkbox">
                                    <label for="checkbox22">
                                        <?= _t('front.user', 'API Partner'); ?>
                                    </label>
                                </div>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox33" type="checkbox">
                                    <label for="checkbox33">
                                        <?= _t('front.user', 'Media Partner'); ?>
                                    </label>
                                </div>
                                <p class="help-block help-block-error "></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="partner"><?= _t('front.user', 'Website'); ?></label>
                            <div class="col-sm-8">
                                <input type="text" id="partner" class="form-control" aria-required="true" placeholder="example.com">
                                <p class="help-block help-block-error "></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="partner"><?= _t('front.user', 'Full Name'); ?><span class="form-required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="partner" class="form-control"
                                       name="PasswordResetRequestForm[email]" aria-required="true">
                                <p class="help-block help-block-error "></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="partner"><?= _t('front.user', 'Company'); ?><span class="form-required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="partner" class="form-control"
                                       name="PasswordResetRequestForm[email]" aria-required="true" placeholder="<?= _t('front.user', 'Name'); ?>">
                                <p class="help-block help-block-error "></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="partner"><?= _t('front.user', 'About you'); ?><span class="form-required">*</span></label>
                            <div class="col-sm-8">
                                <textarea class="form-control" rows="3" id="textArea"></textarea>
                                <p class="help-block help-block-error "></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4 p-t5" for="partner"><?= _t('front.user', 'Location'); ?><span class="form-required">*</span></label>
                            <div class="col-sm-8">
                                <div class="UserLocationWidget">
                                    <span class="tsi tsi-map-marker"></span>
                                    <button type="button" class="ts-user-location ts-ajax-modal btn-link"
                                            title="Shipping Address" data-url="/geo/change-location"
                                            data-target="#changelocation">New York, US
                                    </button>
                                </div>
                                <p class="help-block help-block-error "></p>
                            </div>
                        </div>

                        <div class="text-center m-t30 m-b10">
                            <button type="submit" class="btn btn-primary"><?= _t('front.user', 'Become a Partner'); ?></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
