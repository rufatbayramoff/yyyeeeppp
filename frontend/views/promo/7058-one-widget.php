<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>

<link rel="stylesheet" href="/css/print-model3d/print-model3d.css" type="text/css">

<div class="aff-widget">
    <!-- Nav tabs -->
    <ul class="aff-widget-tabs clearfix">
        <li class="active" ui-sref="upload">
            <a href="#upload" aria-controls="upload">
                <span class="aff-widget-tabs__step">1</span>
                Upload Files </a>
        </li>
        <li class="" ui-sref="printers" ui-sref-active="active">
            <a href="#print" aria-controls="print">
                <span class="aff-widget-tabs__step">2</span>
                Customize Order </a>
        </li>
        <li class="" ui-sref="delivery" ui-sref-active="active">
            <a href="#delivery" aria-controls="delivery">
                <span class="aff-widget-tabs__step">3</span>
                Delivery Options </a>
        </li>
        <li class="" ui-sref="checkout" ui-sref-active="active">
            <a href="#payment" aria-controls="payment">
                <span class="aff-widget-tabs__step">4</span>
                Checkout </a>
        </li>
    </ul>
    <div class="aff-widget-content clearfix">
        <!-- uiView: -->
        <ui-view class=" ">
            <div class="tab-pane  ">
                <div  >

                    <form class="upload-drop-file-zone upload-block ng-pristine ng-valid">
                        <h2 class="m-t0 m-b20">Upload files and get instant prices</h2>

                        <div class="upload-block__explainer">

                            <div class="upload-block__explainer-item">
                                <a class="t-widget-upload-link" ng-click="uploader.addFileClick();">
                                    <span class="tsi tsi-upload-l upload-block__explainer-ico"></span>
                                    <div class="upload-block__explainer-count">1</div>
                                    Upload files </a>
                            </div>

                            <div class="upload-block__explainer-item">
                                <span class="tsi tsi-printer3d upload-block__explainer-ico"></span>
                                <div class="upload-block__explainer-count">2</div>
                                Get it manufactured
                            </div>

                            <div class="upload-block__explainer-item">
                                <span class="tsi tsi-truck upload-block__explainer-ico"></span>
                                <div class="upload-block__explainer-count">3</div>
                                Delivery in 5-7 days
                            </div>

                        </div>

                        <div class="btn btn-danger upload-block__btn t-widget-upload-button"
                             ng-click="uploader.addFileClick();">
                            Upload Files...
                        </div>

                        <div class="btn upload-block__btn">
                            or
                        </div>

                        <div class="btn btn-info upload-block__btn t-widget-upload-button"
                             ng-click="uploader.addFileClick();">
                            I don't have files
                        </div>


                        <div class="upload-block__hint">
                            STL, PLY, OBJ, 3MF, CDR, DXF, EPS, PDF or SVG files are supported.
                            <br>
                            <small>
                                All files are protected by Treatstock security and <a
                                        href="https://www.watermark3d.com/" target="_blank">Watermark 3D</a>
                            </small>
                        </div>

                        <div class="upload-block__input">
                            <div class="input-group">
                                <input type="text" class="form-control"
                                       placeholder="https://www.thingiverse.com/thing:XXXXXX">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button"><?= _t('site.printModel3d', 'Go') ?></button>
                                </span>
                            </div>
                            <span class="help-block">
                                <?= _t('site.printModel3d', 'Put model link from thingiverse.com and print it') ?>
                            </span>
                        </div>

                    </form>

                </div>

            </div>

        </ui-view>
    </div>
</div>


<div class="aff-widget   " id="print-navigation">
    <!-- Nav tabs -->
    <ul class="aff-widget-tabs clearfix">
        <li class="" ui-sref="upload" ui-sref-active="active">
            <a href="#upload" aria-controls="upload">
                <span class="aff-widget-tabs__step">1</span>
                Upload Files </a>
        </li>
        <li class="active" ui-sref="printers">
            <a href="#print" aria-controls="print">
                <span class="aff-widget-tabs__step">2</span>
                Choose Manufacturer</a>
        </li>
        <li class="" ui-sref="delivery" ui-sref-active="active">
            <a href="#delivery" aria-controls="delivery">
                <span class="aff-widget-tabs__step">3</span>
                Project Description </a>
        </li>
    </ul>

    <div class="aff-widget-content clearfix">
        <!-- uiView: -->
        <ui-view class="">
            <div class="tab-pane " id="print">


                <div class="row m-t20">
                    <div class="col-sm-5 col-md-4">

                        <div class="preview" style="display: none;">

                            <div class="preview__hint">
                                Select a file to change its material or color
                            </div>

                            <!-- Slider main container -->
                            <div class="preview__slider">
                                <span class="n">
                                    <div class="preview__slide preview_slider-clickable ">
                                        <div class="preview__material n" title="White PLA">
                                            PLA
                                            <div class="preview__material-color" style="background-color: #ffffff"></div>
                                        </div>
                                        <img class="preview__pic" title=""
                                             src="http://static.h5.tsdev.work/static/render/06dd768bd8bddcbae3f842b6f74fa030_color_ffffff_ambient_40_ax_0_ay_30_az_0_pb_0.png">
                                        <div class="preview__qty n" title="Quantity: 1">×1</div>
                                        <div class="n">
                                            1
                                        </div>
                                    </div>
                                </span><!-- end ngRepeat: model3dPart in model3d.parts -->
                            </div>
                            <button class="btn btn-default btn-sm preview__view3d" name="view3d">
                                <span class="tsi tsi-cube m-r5"></span>
                                3D Viewer
                            </button>
                        </div>

                        <h4 class="material__title clearfix">
                            Product type
                        </h4>

                        <select class="form-control color-switcher__material-selector">
                            <option value="0">Any</option>
                            <option value="1">Perviy 1</option>
                            <option value="11">&nbsp;&nbsp;&nbsp;Vlozhenniy perviy 1</option>
                            <option value="111">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vlozhenniy perviy 1 1</option>
                            <option value="112">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vlozhenniy perviy 1 2</option>
                            <option value="12">&nbsp;&nbsp;&nbsp;Vlozhenniy perviy 2</option>
                            <option value="2">Vtoroy</option>
                            <option value="21">&nbsp;&nbsp;&nbsp;Vtoroy 1</option>
                            <option value="211">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vtoroy 1 1</option>
                            <option value="22">&nbsp;&nbsp;&nbsp;Vtoroy 2</option>
                            <option value="3">Tretiy</option>
                        </select>

                        <h4 class="material__title clearfix">
                            Materials &amp; Colors <a class="material__guide-btn" href="#materialsGuideModal">Need
                                help?</a>
                        </h4>
                        <div class="material-switcher">
                            <!-- ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                            >
        <!-- ngIf: materialColorItem.materialGroup --><div
                                        class="material-switcher__item "
                                        ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"

                                        title="Resin">
            Resin
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                                    class="  ">
        <!-- ngIf: materialColorItem.materialGroup --><div
                                        class="material-switcher__item     "
                                        title="Nylons">
            Nylons
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                                    class="  ">
        <!-- ngIf: materialColorItem.materialGroup --><div
                                        class="material-switcher__item "
                                        title="Full-Color Sandstone">
            Full-Color Sandstone
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                                    class="  ">
        <!-- ngIf: materialColorItem.materialGroup --><div ng-if="materialColorItem.materialGroup"
                                                           class="material-switcher__item "
                                                           title="Wood">
            Wood
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                                    class="  ">
        <!-- ngIf: materialColorItem.materialGroup --><div ng-if="materialColorItem.materialGroup"
                                                           class="material-switcher__item "
                                                           title="Polyesters/PETG">
            Polyesters/PETG
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                                    class="  ">
        <!-- ngIf: materialColorItem.materialGroup --><div ng
                                                           class="material-switcher__item     "
                                                           ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
                                                           title="Flexible (TPU/TPE)">
            Flexible (TPU/TPE)
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                                    class="  ">
        <!-- ngIf: materialColorItem.materialGroup --><div
                                        class="material-switcher__item      material-switcher__item--active"
                                        ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
                                        title="PLA">
            PLA
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                                    class="  ">
        <!-- ngIf: materialColorItem.materialGroup --><div
                                        class="material-switcher__item     "
                                        ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
                                        title="Polycarbonates">
            Polycarbonates
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                                    class="  ">
        <!-- ngIf: materialColorItem.materialGroup --><div
                                        class="material-switcher__item     "
                                        ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"

                                        title="ABS">
            ABS
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                                    class="  ">
        <!-- ngIf: materialColorItem.materialGroup --><div
                                        class="material-switcher__item     "
                                        ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"

                                        title="Nylon Powders">
            Nylon Powders
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                                    class="  ">
        <!-- ngIf: materialColorItem.materialGroup --><div
                                        class="material-switcher__item     "
                                        ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"

                                        title="High-Performance Plastics">
            High-Performance Plastics
        </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list -->
                        </div>

                        <!-- ngIf: isShowMaterialColors() -->
                        <select
                                class="form-control color-switcher__material-selector                 "
                                id="materialSelector">
                            <!-- ngRepeat: materialColorItem in getMaterialsColors() -->
                            <option value="0"
                                    class="    ">All
                            </option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
                            <option value="2"
                                    class="    ">PLA
                            </option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
                            <option value="122"
                                    class="    ">PLA+
                            </option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
                            <option value="143"
                                    class="    ">PLA-CF
                            </option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
                            <option value="144"
                                    class="    ">HPLA-CF
                            </option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
                            <option value="181"
                                    class="    ">HTPLA
                            </option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
                            <option value="202"
                                    class="    ">Matte Fiber HTPLA
                            </option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
                        </select><!-- end ngIf: isShowMaterialColors() -->

                        <div class="color-switcher__material-list m-b10">
                            <input type="checkbox" class="showhideColorSwitcher"
                                   id="color-switcher__aff-widget-switcher">
                            <label class="color-switcher__mobile" for="color-switcher__aff-widget-switcher">
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: #ffffff"></div>
                                    <div class="material-item__label  ">White</div>
                                </div>
                                <span class="tsi tsi-down"></span>
                            </label>

                            <!-- ngIf: getColors() -->
                            <div class="  ">
                                <!-- ngIf: isShortColors() || !longColorsNeed() -->
                                <div class="color-switcher__color-list   ">
                                    <!-- ngRepeat: printerColor in getColors().shortColorsList --><span class="  ">
            <div class="material-item"
                 title="Black">
                <div class="material-item__color"
                     style="background-color: rgb(63, 63, 63);"></div>
                <div class="material-item__label  ">Black</div>
            </div>
        </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span class="  ">
            <div ng-click="changeColor(printerColor.id)" class="material-item material-item--active"
                 ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
                 title="White">
                <div class="material-item__color" ng-style="{'background-color': '#ffffff'}"
                     style="background-color: rgb(255, 255, 255);"></div>
                <div class="material-item__label  ">White</div>
            </div>
        </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span class="  ">
            <div ng-click="changeColor(printerColor.id)" class="material-item"
                 title="Red">
                <div class="material-item__color" ng-style="{'background-color': '#ff465b'}"
                     style="background-color: rgb(255, 70, 91);"></div>
                <div class="material-item__label  ">Red</div>
            </div>
        </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span class="  ">
            <div ng-click="changeColor(printerColor.id)" class="material-item"
                 title="Blue">
                <div class="material-item__color" ng-style="{'background-color': '#0066cc'}"
                     style="background-color: rgb(0, 102, 204);"></div>
                <div class="material-item__label  ">Blue</div>
            </div>
        </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span class="  ">
            <div ng-click="changeColor(printerColor.id)" class="material-item"
                 title="Green">
                <div class="material-item__color" ng-style="{'background-color': '#00cc33'}"
                     style="background-color: rgb(0, 204, 51);"></div>
                <div class="material-item__label  ">Green</div>
            </div>
        </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span class="  ">
            <div ng-click="changeColor(printerColor.id)" class="material-item"
                 title="Gray">
                <div class="material-item__color" ng-style="{'background-color': '#bebebe'}"
                     style="background-color: rgb(190, 190, 190);"></div>
                <div class="material-item__label  ">Gray</div>
            </div>
        </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span class="  ">
            <div ng-click="changeColor(printerColor.id)" class="material-item"
                 title="Yellow">
                <div class="material-item__color" ng-style="{'background-color': '#f2d70e'}"
                     style="background-color: rgb(242, 215, 14);"></div>
                <div class="material-item__label  ">Yellow</div>
            </div>
        </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span class="  ">
            <div ng-click="changeColor(printerColor.id)" class="material-item"
                 title="Orange">
                <div class="material-item__color" ng-style="{'background-color': '#ff8000'}"
                     style="background-color: rgb(255, 128, 0);"></div>
                <div class="material-item__label  ">Orange</div>
            </div>
        </span><!-- end ngRepeat: printerColor in getColors().shortColorsList -->

                                    <div class="material-item   "
                                         ng-click="setColorsShortMode(0)">
                                        <span class="material-item__spoiler">
                                            <span class="tsi tsi-plus"></span>
                                            More            </span>
                                    </div><!-- end ngIf: isShortColors() && longColorsNeed() -->
                                </div><!-- end ngIf: isShortColors() || !longColorsNeed() -->

                            </div><!-- end ngIf: getColors() -->
                        </div>

                        <!-- ngIf: getInfill() -->
                        <div class="  ">
                            <div class="infill-switcher">
                                <label for="materialitem-infill" class="infill-switcher__label">
                                    Infill </label>
                                <select class="form-control infill-switcher__select              "
                                        id="materialitem-infill" name="materialitem-infill"
                                        ng-options="infillItem for infillItem in getInfill().list">
                                    <option label="20" value="number:20" selected="selected">20</option>
                                    <option label="25" value="number:25">25</option>
                                    <option label="30" value="number:30">30</option>
                                    <option label="35" value="number:35">35</option>
                                    <option label="40" value="number:40">40</option>
                                    <option label="45" value="number:45">45</option>
                                    <option label="50" value="number:50">50</option>
                                    <option label="55" value="number:55">55</option>
                                    <option label="60" value="number:60">60</option>
                                    <option label="65" value="number:65">65</option>
                                    <option label="70" value="number:70">70</option>
                                    <option label="75" value="number:75">75</option>
                                    <option label="80" value="number:80">80</option>
                                    <option label="85" value="number:85">85</option>
                                    <option label="90" value="number:90">90</option>
                                    <option label="95" value="number:95">95</option>
                                    <option label="100" value="number:100">100</option>
                                </select>
                            </div>
                        </div><!-- end ngIf: getInfill() -->
                    </div>
                    <div class="col-sm-7 col-md-8">
                        <div class="choose-technology">
                            <div class="choose-technology__label">
                                Technology:
                            </div>
                            <div class="choose-technology__vars" data-toggle="buttons">
                                <label class="btn btn-sm active">
                                    <input type="radio" name="options" id="option1" autocomplete="off"
                                           checked="checked">
                                    3D printing </label>
                                <label class="btn btn-sm">
                                    <input type="radio" name="options" id="option2" autocomplete="off">
                                    CNC </label>
                                <label class="btn btn-sm">
                                    <input type="radio" name="options" id="option3" autocomplete="off">
                                    Injection Molding </label>
                                <label class="btn btn-sm">
                                    <input type="radio" name="options" id="option4" autocomplete="off">
                                    Any</label>
                            </div>
                        </div>
                        <div class="printers">
                            <div class="printers__control">
                                <div class="printers__control-left">
                                    <!-- ngIf: !offersBundleData.fixedLocation -->
                                    <div class="printers__loc   ">
                                        Location:
                                        <div class="UserLocationWidget">
                                            <span class="tsi tsi-map-marker"></span>
                                            <button type="button" class="ts-user-location ts-ajax-modal btn-link"
                                                    title="Shipping Address" data-url="/geo/change-location"
                                                    data-target="#changelocation">LOS ANGELES, US
                                            </button>
                                        </div>
                                    </div><!-- end ngIf: !offersBundleData.fixedLocation -->

                                    <!-- ngIf: !psIdOnly -->
                                    <div class="printers__delivery printers__intl_only   ">
                                        <label class="checkbox-switch checkbox-switch--xs">
                                            <input type="checkbox" ng-model="offersBundleData.intlOnly"
                                                   ng-change="onChangeWithInternationalDelivery()"
                                                   class="         ng-empty">
                                            <span class="slider"></span>
                                            <span class="text">International delivery</span>
                                        </label>
                                    </div><!-- end ngIf: !psIdOnly -->
                                </div>

                                <div class="printers__sort">
                                    <label>Sort by:</label>
                                    <select ng-options="key as value for (key , value) in sortOptions "
                                            ng-model="offersBundleData.sortPrintersBy"
                                            ng-change="onChangePrintersListSort()"
                                            class="form-control input-sm              ">
                                        <option label="Default" value="string:default" selected="selected">Default
                                        </option>
                                        <option label="Best Rating" value="string:rating">Best Rating</option>
                                        <option label="Lowest print price" value="string:price">Lowest print price
                                        </option>
                                        <option label="Nearest to me" value="string:distance">Nearest to me</option>
                                    </select>
                                </div>
                            </div>
                            <!-- ngIf: (startedUpdatePrintersRequest || model3d.isCalculating()) -->
                            <!-- ngIf: !(startedUpdatePrintersRequest || model3d.isCalculating()) -->
                            <div class="  ">
                                <!-- ngIf: offersBundleData.offersBundle.messages -->
                                <!-- ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
                                <div class="  ">
                                    <div>
                                        <div class="model-printers-available model-printers-available--choosen"

                                             style="display: block;">
                                            <div class="model-printers-available__wrap">
                                                <div class="model-printers-available__info">
                                                    <div class="model-printers-available__info__img">
                                                        <a href="/c/3dprintdirect?selectedPrinterId=5945&amp;model3dViewedStateId=1440784"
                                                           target="_blank" data-pjax="0"><img
                                                                    ng-src="http://static.h5.tsdev.work/static/files/ef/63/348269_0_7ea16e92d31c7a5ac5e402668bef76ec_64x64.png?date=1534967947"
                                                                    width="30" height="30" alt="" align="left"
                                                                    src="http://static.h5.tsdev.work/static/files/ef/63/348269_0_7ea16e92d31c7a5ac5e402668bef76ec_64x64.png?date=1534967947"></a>
                                                        <!-- ngIf: offerItem.psPrinter.isStatusPro -->
                                                        <div
                                                                class="cert-label cert-label--pro   "
                                                                data-toggle="tooltip" data-placement="bottom" title=""
                                                                data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                            Pro
                                                        </div><!-- end ngIf: offerItem.psPrinter.isStatusPro -->

                                                        <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                                                    </div>

                                                    <div class="model-printers-available__info__ps">
                                                        <div class="model-printers-available__info__ps-title">
                                                            <a href="/c/3dprintdirect?selectedPrinterId=5945&amp;model3dViewedStateId=1440784"
                                                               target="_blank" data-pjax="0"><span class=" ">3dPrint.Direct</span></a>
                                                        </div>

                                                        <div>
                                                            <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                            <div
                                                                    rating="offerItem.psPrinter.ps.reviewsInfo"
                                                                    class="   ng-isolate-scope">
                                                                <!-- ngIf: showRating -->
                                                                <div
                                                                        class="star-rating-block   "
                                                                        itemprop="aggregateRating" itemscope=""
                                                                        itemtype="http://schema.org/AggregateRating">
                                                                    <meta itemprop="ratingValue" content="4.9">
                                                                    <meta itemprop="worstRating" content="1">
                                                                    <meta itemprop="bestRating" content="5">
                                                                    <div class="star-rating rating-xs rating-disabled">
                                                                        <div class="rating-container tsi"
                                                                             data-content="">
                                                                            <div class="rating-stars"
                                                                                 data-content=""
                                                                                 style="width: 98%;"></div>
                                                                            <input value="4.9" type="text"
                                                                                   class="star-rating form-control hide"
                                                                                   data-min="0" data-max="5"
                                                                                   data-step="0.1" data-size="xs"
                                                                                   data-symbol=""
                                                                                   data-glyphicon="false"
                                                                                   data-rating-class="tsi"
                                                                                   data-readonly="true"></div>
                                                                    </div>
                                                                </div><!-- end ngIf: showRating --><span
                                                                        class="star-rating-count  ">(<span
                                                                            itemprop="reviewCount"
                                                                            class=" ">35</span> reviews)</span>
                                                            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                        </div>


                                                        <div class="model-printers-available__info__ps-printer  ">
                                                            3D Printer: Creality Ender 3 -2
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="model-printers-available__delivery">
                                                    <div class="model-printers-available__delivery-title  ">
                                                        Denton, TX
                                                    </div>
                                                    <div class="model-printers-available__price-delivery">
                                                        <span class=" ">Pickup, Delivery</span>
                                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span

                                                                class="  ">: <strong class=" "><span
                                                                        class="tsi tsi-truck"
                                                                        title="Delivery Price"></span> $7.80</strong>
                </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
                                                        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
                                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="model-printers-available__price-btn">

                                                <div class="model-printers-available__print-btn">
                                                    <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                                                            title="Select manufacturer" ng-click="printHere(offerItem)"
                                                            oncontextmenu="return false;">Get a Quote
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
                                <div class="  ">
                                    <div>
                                        <div class="model-printers-available"
                                             ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
                                             style="display: block;">
                                            <div class="model-printers-available__wrap">
                                                <div class="model-printers-available__info">
                                                    <div class="model-printers-available__info__img">
                                                        <a href="/c/mr-print?selectedPrinterId=4684&amp;model3dViewedStateId=1440784"
                                                           target="_blank" data-pjax="0"><img
                                                                    ng-src="http://static.h5.tsdev.work/static/user/3f9f75fb7d4ef2d22d068799252c2e34/ps_logo_circle_1571035958_64x64.jpg"
                                                                    width="30" height="30" alt="" align="left"
                                                                    src="http://static.h5.tsdev.work/static/user/3f9f75fb7d4ef2d22d068799252c2e34/ps_logo_circle_1571035958_64x64.jpg"></a>
                                                        <!-- ngIf: offerItem.psPrinter.isStatusPro -->
                                                        <div
                                                                class="cert-label cert-label--pro   "
                                                                data-toggle="tooltip" data-placement="bottom" title=""
                                                                data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                            Pro
                                                        </div><!-- end ngIf: offerItem.psPrinter.isStatusPro -->

                                                        <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                                                    </div>

                                                    <div class="model-printers-available__info__ps">
                                                        <div class="model-printers-available__info__ps-title">
                                                            <a href="/c/mr-print?selectedPrinterId=4684&amp;model3dViewedStateId=1440784"
                                                               target="_blank" data-pjax="0"><span
                                                                        class=" ">M.R. Print</span></a>
                                                        </div>

                                                        <div>
                                                            <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                            <div
                                                                    rating="offerItem.psPrinter.ps.reviewsInfo"
                                                                    class="   ng-isolate-scope">
                                                                <!-- ngIf: showRating -->
                                                                <div
                                                                        class="star-rating-block   "
                                                                        itemprop="aggregateRating" itemscope=""
                                                                        itemtype="http://schema.org/AggregateRating">
                                                                    <meta itemprop="ratingValue" content="4.8">
                                                                    <meta itemprop="worstRating" content="1">
                                                                    <meta itemprop="bestRating" content="5">
                                                                    <div class="star-rating rating-xs rating-disabled">
                                                                        <div class="rating-container tsi"
                                                                             data-content="">
                                                                            <div class="rating-stars"
                                                                                 data-content=""
                                                                                 style="width: 96%;"></div>
                                                                            <input value="4.8" type="text"
                                                                                   class="star-rating form-control hide"
                                                                                   data-min="0" data-max="5"
                                                                                   data-step="0.1" data-size="xs"
                                                                                   data-symbol=""
                                                                                   data-glyphicon="false"
                                                                                   data-rating-class="tsi"
                                                                                   data-readonly="true"></div>
                                                                    </div>
                                                                </div><!-- end ngIf: showRating --><span
                                                                        class="star-rating-count  ">(<span
                                                                            itemprop="reviewCount"
                                                                            class=" ">49</span> reviews)</span>
                                                            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                        </div>


                                                        <div class="model-printers-available__info__ps-printer  ">
                                                            3D Printer: Ender 3 Pro
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="model-printers-available__delivery">
                                                    <div class="model-printers-available__delivery-title  ">
                                                        Frederick, Maryland
                                                    </div>
                                                    <div class="model-printers-available__price-delivery">
                                                        <span class=" ">Delivery</span>
                                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span

                                                                class="  ">: <strong class=" "><span
                                                                        class="tsi tsi-truck"
                                                                        title="Delivery Price"></span> $6.80</strong>
                </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
                                                        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
                                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="model-printers-available__price-btn">

                                                <div class="model-printers-available__print-btn">
                                                    <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                                                            title="Select manufacturer" ng-click="printHere(offerItem)"
                                                            oncontextmenu="return false;">Get a Quote
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
                                <div class="  ">
                                    <div>
                                        <div class="model-printers-available"
                                             ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
                                             style="display: block;">
                                            <div class="model-printers-available__wrap">
                                                <div class="model-printers-available__info">
                                                    <div class="model-printers-available__info__img">
                                                        <a href="/c/capeolive?selectedPrinterId=1901&amp;model3dViewedStateId=1440784"
                                                           target="_blank" data-pjax="0"><img
                                                                    ng-src="http://static.h5.tsdev.work/static/user/7436b0dc99f8aed11026252aeade1a3a/ps_logo_circle_1583014731_64x64.png"
                                                                    width="30" height="30" alt="" align="left"
                                                                    src="http://static.h5.tsdev.work/static/user/7436b0dc99f8aed11026252aeade1a3a/ps_logo_circle_1583014731_64x64.png"></a>
                                                        <!-- ngIf: offerItem.psPrinter.isStatusPro -->
                                                        <div
                                                                class="cert-label cert-label--pro   "
                                                                data-toggle="tooltip" data-placement="bottom" title=""
                                                                data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                            Pro
                                                        </div><!-- end ngIf: offerItem.psPrinter.isStatusPro -->

                                                        <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                                                    </div>

                                                    <div class="model-printers-available__info__ps">
                                                        <div class="model-printers-available__info__ps-title">
                                                            <a href="/c/capeolive?selectedPrinterId=1901&amp;model3dViewedStateId=1440784"
                                                               target="_blank" data-pjax="0"><span
                                                                        class=" ">Cape_Olive</span></a>
                                                        </div>

                                                        <div>
                                                            <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                            <div
                                                                    rating="offerItem.psPrinter.ps.reviewsInfo"
                                                                    class="    ">
                                                                <!-- ngIf: showRating -->
                                                                <div
                                                                        class="star-rating-block   "
                                                                        itemprop="aggregateRating" itemscope=""
                                                                        itemtype="http://schema.org/AggregateRating">
                                                                    <meta itemprop="ratingValue" content="5.0">
                                                                    <meta itemprop="worstRating" content="1">
                                                                    <meta itemprop="bestRating" content="5">
                                                                    <div class="star-rating rating-xs rating-disabled">
                                                                        <div class="rating-container tsi"
                                                                             data-content="">
                                                                            <div class="rating-stars"
                                                                                 data-content=""
                                                                                 style="width: 100%;"></div>
                                                                            <input value="5.0" type="text"
                                                                                   class="star-rating form-control hide"
                                                                                   data-min="0" data-max="5"
                                                                                   data-step="0.1" data-size="xs"
                                                                                   data-symbol=""
                                                                                   data-glyphicon="false"
                                                                                   data-rating-class="tsi"
                                                                                   data-readonly="true"></div>
                                                                    </div>
                                                                </div><!-- end ngIf: showRating --><span
                                                                        class="star-rating-count  ">(<span
                                                                            itemprop="reviewCount" class=" ">225</span> reviews)</span>
                                                            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                        </div>


                                                        <div class="model-printers-available__info__ps-printer  ">
                                                            3D Printer: Original Prusa i3 MK2.5S
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="model-printers-available__delivery">
                                                    <div class="model-printers-available__delivery-title  ">
                                                        Round Rock, TX
                                                    </div>
                                                    <div class="model-printers-available__price-delivery">
                                                        <span class=" ">Delivery</span>
                                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
                                                                class="  ">: <strong class=" "><span
                                                                        class="tsi tsi-truck"
                                                                        title="Delivery Price"></span> $4.80</strong>
                </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
                                                        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
                                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="model-printers-available__price-btn">

                                                <div class="model-printers-available__print-btn">
                                                    <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                                                            title="Select manufacturer" ng-click="printHere(offerItem)"
                                                            oncontextmenu="return false;">Get a Quote
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
                                <div class="  ">
                                    <div>
                                        <div class="model-printers-available"
                                             ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
                                             style="display: block;">
                                            <div class="model-printers-available__wrap">
                                                <div class="model-printers-available__info">
                                                    <div class="model-printers-available__info__img">
                                                        <a href="/c/crk-tech?selectedPrinterId=4717&amp;model3dViewedStateId=1440784"
                                                           target="_blank" data-pjax="0"><img
                                                                    ng-src="http://static.h5.tsdev.work/static/user/fd534ba27cc20651b56d18ee2317f2fd/ps_logo_circle_1571593648_64x64.png"
                                                                    width="30" height="30" alt="" align="left"
                                                                    src="http://static.h5.tsdev.work/static/user/fd534ba27cc20651b56d18ee2317f2fd/ps_logo_circle_1571593648_64x64.png"></a>
                                                        <!-- ngIf: offerItem.psPrinter.isStatusPro -->
                                                        <div ng-if="offerItem.psPrinter.isStatusPro"
                                                             class="cert-label cert-label--pro   "
                                                             data-toggle="tooltip" data-placement="bottom" title=""
                                                             data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                            Pro
                                                        </div><!-- end ngIf: offerItem.psPrinter.isStatusPro -->

                                                        <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                                                    </div>

                                                    <div class="model-printers-available__info__ps">
                                                        <div class="model-printers-available__info__ps-title">
                                                            <a href="/c/crk-tech?selectedPrinterId=4717&amp;model3dViewedStateId=1440784"
                                                               target="_blank" data-pjax="0"><span class=" ">crK Technologies</span></a>
                                                        </div>

                                                        <div>
                                                            <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                            <div
                                                                    rating="offerItem.psPrinter.ps.reviewsInfo"
                                                                    class="   ng-isolate-scope">
                                                                <!-- ngIf: showRating -->
                                                                <div ng-if="showRating"
                                                                     class="star-rating-block   "
                                                                     itemprop="aggregateRating" itemscope=""
                                                                     itemtype="http://schema.org/AggregateRating">
                                                                    <meta itemprop="ratingValue" content="5.0">
                                                                    <meta itemprop="worstRating" content="1">
                                                                    <meta itemprop="bestRating" content="5">
                                                                    <div class="star-rating rating-xs rating-disabled">
                                                                        <div class="rating-container tsi"
                                                                             data-content="">
                                                                            <div class="rating-stars"
                                                                                 data-content=""
                                                                                 style="width: 100%;"></div>
                                                                            <input value="5.0" type="text"
                                                                                   class="star-rating form-control hide"
                                                                                   data-min="0" data-max="5"
                                                                                   data-step="0.1" data-size="xs"
                                                                                   data-symbol=""
                                                                                   data-glyphicon="false"
                                                                                   data-rating-class="tsi"
                                                                                   data-readonly="true"></div>
                                                                    </div>
                                                                </div><!-- end ngIf: showRating --><span
                                                                        class="star-rating-count  ">(<span
                                                                            itemprop="reviewCount" class=" ">116</span> reviews)</span>
                                                            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                        </div>


                                                        <div class="model-printers-available__info__ps-printer  ">
                                                            3D Printer: Creality CR-10S
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="model-printers-available__delivery">
                                                    <div class="model-printers-available__delivery-title  ">
                                                        Plantation, Florida
                                                    </div>
                                                    <div class="model-printers-available__price-delivery">
                                                        <span class=" ">Delivery</span>
                                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
                                                                ng-if="offerItem.estimateDeliveryCost.amount"
                                                                class="  ">: <strong class=" "><span
                                                                        class="tsi tsi-truck"
                                                                        title="Delivery Price"></span> $6.30</strong>
                </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
                                                        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
                                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="model-printers-available__price-btn">

                                                <div class="model-printers-available__print-btn">
                                                    <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                                                            title="Select manufacturer" ng-click="printHere(offerItem)"
                                                            oncontextmenu="return false;">Get a Quote
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
                                <div class="  ">
                                    <div>
                                        <div class="model-printers-available"
                                             ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
                                             style="display: block;">
                                            <div class="model-printers-available__wrap">
                                                <div class="model-printers-available__info">
                                                    <div class="model-printers-available__info__img">
                                                        <a href="/c/moldy-miniatures?selectedPrinterId=6234&amp;model3dViewedStateId=1440784"
                                                           target="_blank" data-pjax="0"><img
                                                                    ng-src="http://static.h5.tsdev.work/static/user/da442cb1a3877a9688991ca4880bf573/ps_logo_circle_1597110077_64x64.jpg"
                                                                    width="30" height="30" alt="" align="left"
                                                                    src="http://static.h5.tsdev.work/static/user/da442cb1a3877a9688991ca4880bf573/ps_logo_circle_1597110077_64x64.jpg"></a>
                                                        <!-- ngIf: offerItem.psPrinter.isStatusPro -->
                                                        <div
                                                                class="cert-label cert-label--pro   "
                                                                data-toggle="tooltip" data-placement="bottom" title=""
                                                                data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                            Pro
                                                        </div><!-- end ngIf: offerItem.psPrinter.isStatusPro -->

                                                        <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                                                    </div>

                                                    <div class="model-printers-available__info__ps">
                                                        <div class="model-printers-available__info__ps-title">
                                                            <a href="/c/moldy-miniatures?selectedPrinterId=6234&amp;model3dViewedStateId=1440784"
                                                               target="_blank" data-pjax="0"><span class=" ">Moldy Miniatures LLC</span></a>
                                                        </div>

                                                        <div>
                                                            <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                            <div
                                                                    rating="offerItem.psPrinter.ps.reviewsInfo"
                                                                    class="   ng-isolate-scope">
                                                                <!-- ngIf: showRating -->
                                                                <div ng-if="showRating"
                                                                     class="star-rating-block   "
                                                                     itemprop="aggregateRating" itemscope=""
                                                                     itemtype="http://schema.org/AggregateRating">
                                                                    <meta itemprop="ratingValue" content="5.0">
                                                                    <meta itemprop="worstRating" content="1">
                                                                    <meta itemprop="bestRating" content="5">
                                                                    <div class="star-rating rating-xs rating-disabled">
                                                                        <div class="rating-container tsi"
                                                                             data-content="">
                                                                            <div class="rating-stars"
                                                                                 data-content=""
                                                                                 style="width: 100%;"></div>
                                                                            <input value="5.0" type="text"
                                                                                   class="star-rating form-control hide"
                                                                                   data-min="0" data-max="5"
                                                                                   data-step="0.1" data-size="xs"
                                                                                   data-symbol=""
                                                                                   data-glyphicon="false"
                                                                                   data-rating-class="tsi"
                                                                                   data-readonly="true"></div>
                                                                    </div>
                                                                </div><!-- end ngIf: showRating --><span
                                                                        class="star-rating-count  ">(<span
                                                                            itemprop="reviewCount"
                                                                            class=" ">3</span> reviews)</span>
                                                            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                        </div>


                                                        <div class="model-printers-available__info__ps-printer  ">
                                                            3D Printer: Upgraded Creality Ender 3
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="model-printers-available__delivery">
                                                    <div class="model-printers-available__delivery-title  ">
                                                        Milwaukee, WI
                                                    </div>
                                                    <div class="model-printers-available__price-delivery">
                                                        <span class=" ">Pickup, Delivery</span>
                                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
                                                                ng-if="offerItem.estimateDeliveryCost.amount"
                                                                class="  ">: <strong class=" "><span
                                                                        class="tsi tsi-truck"
                                                                        title="Delivery Price"></span> $7.80</strong>
                </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
                                                        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
                                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="model-printers-available__price-btn">

                                                <div class="model-printers-available__print-btn">
                                                    <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                                                            title="Select manufacturer" ng-click="printHere(offerItem)"
                                                            oncontextmenu="return false;">Get a Quote
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
                                <div ng-repeat="offerItem in offersBundleData.offersBundle.offers" class="  ">
                                    <div>
                                        <div class="model-printers-available"
                                             ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
                                             style="display: block;">
                                            <div class="model-printers-available__wrap">
                                                <div class="model-printers-available__info">
                                                    <div class="model-printers-available__info__img">
                                                        <a href="/c/mr-anything-tech?selectedPrinterId=2583&amp;model3dViewedStateId=1440784"
                                                           target="_blank" data-pjax="0"><img
                                                                    ng-src="http://static.h5.tsdev.work/static/user/bafe5a19bbef4152f217efc037c1be59/ps_logo_circle_1525182732_64x64.jpg"
                                                                    width="30" height="30" alt="" align="left"
                                                                    src="http://static.h5.tsdev.work/static/user/bafe5a19bbef4152f217efc037c1be59/ps_logo_circle_1525182732_64x64.jpg"></a>
                                                        <!-- ngIf: offerItem.psPrinter.isStatusPro -->
                                                        <div ng-if="offerItem.psPrinter.isStatusPro"
                                                             class="cert-label cert-label--pro   "
                                                             data-toggle="tooltip" data-placement="bottom" title=""
                                                             data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                            Pro
                                                        </div><!-- end ngIf: offerItem.psPrinter.isStatusPro -->

                                                        <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                                                    </div>

                                                    <div class="model-printers-available__info__ps">
                                                        <div class="model-printers-available__info__ps-title">
                                                            <a href="/c/mr-anything-tech?selectedPrinterId=2583&amp;model3dViewedStateId=1440784"
                                                               target="_blank" data-pjax="0"><span class=" ">Mr Anything Tech</span></a>
                                                        </div>

                                                        <div
                                                        ="psIdOnly">
                                                        <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                        <div ng-if="offerItem.psPrinter.ps.reviewsInfo"
                                                             rating="offerItem.psPrinter.ps.reviewsInfo"
                                                             class="   ng-isolate-scope">
                                                            <!-- ngIf: showRating -->
                                                            <div ng-if="showRating"
                                                                 class="star-rating-block   "
                                                                 itemprop="aggregateRating" itemscope=""
                                                                 itemtype="http://schema.org/AggregateRating">
                                                                <meta itemprop="ratingValue" content="4.9">
                                                                <meta itemprop="worstRating" content="1">
                                                                <meta itemprop="bestRating" content="5">
                                                                <div class="star-rating rating-xs rating-disabled">
                                                                    <div class="rating-container tsi"
                                                                         data-content="">
                                                                        <div class="rating-stars"
                                                                             data-content=""
                                                                             style="width: 98%;"></div>
                                                                        <input value="4.9" type="text"
                                                                               class="star-rating form-control hide"
                                                                               data-min="0" data-max="5"
                                                                               data-step="0.1" data-size="xs"
                                                                               data-symbol=""
                                                                               data-glyphicon="false"
                                                                               data-rating-class="tsi"
                                                                               data-readonly="true"></div>
                                                                </div>
                                                            </div><!-- end ngIf: showRating --><span
                                                                    class="star-rating-count  ">(<span
                                                                        itemprop="reviewCount"
                                                                        class=" ">25</span> reviews)</span>
                                                        </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                    </div>
                                                    <div ng-show="psIdOnly" class="     ">
                                                        <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
                                                        <div ng-if="offerItem.psPrinter.reviewsInfo"
                                                             rating="offerItem.psPrinter.reviewsInfo"
                                                             class="   ng-isolate-scope">
                                                            <!-- ngIf: showRating -->
                                                            <div ng-if="showRating"
                                                                 class="star-rating-block   "
                                                                 itemprop="aggregateRating" itemscope=""
                                                                 itemtype="http://schema.org/AggregateRating">
                                                                <meta itemprop="ratingValue" content="4.9">
                                                                <meta itemprop="worstRating" content="1">
                                                                <meta itemprop="bestRating" content="5">
                                                                <div class="star-rating rating-xs rating-disabled">
                                                                    <div class="rating-container tsi"
                                                                         data-content="">
                                                                        <div class="rating-stars"
                                                                             data-content=""
                                                                             style="width: 98%;"></div>
                                                                        <input value="4.9" type="text"
                                                                               class="star-rating form-control hide"
                                                                               data-min="0" data-max="5"
                                                                               data-step="0.1" data-size="xs"
                                                                               data-symbol=""
                                                                               data-glyphicon="false"
                                                                               data-rating-class="tsi"
                                                                               data-readonly="true"></div>
                                                                </div>
                                                            </div><!-- end ngIf: showRating --><span
                                                                    class="star-rating-count  ">(<span
                                                                        itemprop="reviewCount"
                                                                        class=" ">23</span> reviews)</span>
                                                        </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
                                                    </div>

                                                    <div class="model-printers-available__info__ps-printer  ">
                                                        3D Printer: Creality CR-10S
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="model-printers-available__delivery">
                                                <div class="model-printers-available__delivery-title  ">
                                                    Iowa City, Iowa
                                                </div>
                                                <div class="model-printers-available__price-delivery">
                                                    <span class=" ">Pickup, Delivery</span>
                                                    <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
                                                            ng-if="offerItem.estimateDeliveryCost.amount"
                                                            class="  ">: <strong class=" "><span
                                                                    class="tsi tsi-truck"
                                                                    title="Delivery Price"></span> $5.80</strong>
                </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
                                                    <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
                                                    <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="model-printers-available__price-btn">

                                            <div class="model-printers-available__print-btn">
                                                <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                                                        title="Select manufacturer" ng-click="printHere(offerItem)"
                                                        oncontextmenu="return false;">Get a Quote
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
                            <div ng-repeat="offerItem in offersBundleData.offersBundle.offers" class="  ">
                                <div>
                                    <div class="model-printers-available"
                                         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
                                         style="display: block;">
                                        <div class="model-printers-available__wrap">
                                            <div class="model-printers-available__info">
                                                <div class="model-printers-available__info__img">
                                                    <a href="/c/shellback-workshop?selectedPrinterId=4752&amp;model3dViewedStateId=1440784"
                                                       target="_blank" data-pjax="0"><img
                                                                ng-src="http://static.h5.tsdev.work/static/user/c75c5bfeffe5a3d847c98f9fd32b9d9e/ps_logo_circle_1566413076_64x64.png"
                                                                width="30" height="30" alt="" align="left"
                                                                src="http://static.h5.tsdev.work/static/user/c75c5bfeffe5a3d847c98f9fd32b9d9e/ps_logo_circle_1566413076_64x64.png"></a>
                                                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->
                                                    <div ng-if="offerItem.psPrinter.isStatusPro"
                                                         class="cert-label cert-label--pro   "
                                                         data-toggle="tooltip" data-placement="bottom" title=""
                                                         data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                        Pro
                                                    </div><!-- end ngIf: offerItem.psPrinter.isStatusPro -->

                                                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                                                </div>

                                                <div class="model-printers-available__info__ps">
                                                    <div class="model-printers-available__info__ps-title">
                                                        <a href="/c/shellback-workshop?selectedPrinterId=4752&amp;model3dViewedStateId=1440784"
                                                           target="_blank" data-pjax="0"><span class=" ">Shellback Workshop</span></a>
                                                    </div>

                                                    <div
                                                    ="psIdOnly">
                                                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                    <div ng-if="offerItem.psPrinter.ps.reviewsInfo"
                                                         rating="offerItem.psPrinter.ps.reviewsInfo"
                                                         class="   ng-isolate-scope">
                                                        <!-- ngIf: showRating -->
                                                        <div ng-if="showRating"
                                                             class="star-rating-block   "
                                                             itemprop="aggregateRating" itemscope=""
                                                             itemtype="http://schema.org/AggregateRating">
                                                            <meta itemprop="ratingValue" content="4.9">
                                                            <meta itemprop="worstRating" content="1">
                                                            <meta itemprop="bestRating" content="5">
                                                            <div class="star-rating rating-xs rating-disabled">
                                                                <div class="rating-container tsi"
                                                                     data-content="">
                                                                    <div class="rating-stars"
                                                                         data-content=""
                                                                         style="width: 98%;"></div>
                                                                    <input value="4.9" type="text"
                                                                           class="star-rating form-control hide"
                                                                           data-min="0" data-max="5"
                                                                           data-step="0.1" data-size="xs"
                                                                           data-symbol=""
                                                                           data-glyphicon="false"
                                                                           data-rating-class="tsi"
                                                                           data-readonly="true"></div>
                                                            </div>
                                                        </div><!-- end ngIf: showRating --><span
                                                                class="star-rating-count  ">(<span
                                                                    itemprop="reviewCount" class=" ">168</span> reviews)</span>
                                                    </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                </div>
                                                <div ng-show="psIdOnly" class="     ">
                                                    <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
                                                    <div ng-if="offerItem.psPrinter.reviewsInfo"
                                                         rating="offerItem.psPrinter.reviewsInfo"
                                                         class="   ng-isolate-scope">
                                                        <!-- ngIf: showRating -->
                                                        <div ng-if="showRating"
                                                             class="star-rating-block   "
                                                             itemprop="aggregateRating" itemscope=""
                                                             itemtype="http://schema.org/AggregateRating">
                                                            <meta itemprop="ratingValue" content="4.9">
                                                            <meta itemprop="worstRating" content="1">
                                                            <meta itemprop="bestRating" content="5">
                                                            <div class="star-rating rating-xs rating-disabled">
                                                                <div class="rating-container tsi"
                                                                     data-content="">
                                                                    <div class="rating-stars"
                                                                         data-content=""
                                                                         style="width: 98%;"></div>
                                                                    <input value="4.9" type="text"
                                                                           class="star-rating form-control hide"
                                                                           data-min="0" data-max="5"
                                                                           data-step="0.1" data-size="xs"
                                                                           data-symbol=""
                                                                           data-glyphicon="false"
                                                                           data-rating-class="tsi"
                                                                           data-readonly="true"></div>
                                                            </div>
                                                        </div><!-- end ngIf: showRating --><span
                                                                class="star-rating-count  ">(<span
                                                                    itemprop="reviewCount"
                                                                    class=" ">35</span> reviews)</span>
                                                    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
                                                </div>

                                                <div class="model-printers-available__info__ps-printer  ">
                                                    3D Printer: Ender 3
                                                </div>
                                            </div>
                                        </div>
                                        <div class="model-printers-available__delivery">
                                            <div class="model-printers-available__delivery-title  ">
                                                Pataskala, OH
                                            </div>
                                            <div class="model-printers-available__price-delivery">
                                                <span class=" ">Delivery</span>
                                                <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
                                                        ng-if="offerItem.estimateDeliveryCost.amount"
                                                        class="  ">: <strong class=" "><span
                                                                class="tsi tsi-truck"
                                                                title="Delivery Price"></span> $6.30</strong>
                </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
                                                <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
                                                <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="model-printers-available__price-btn">

                                        <div class="model-printers-available__print-btn">
                                            <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                                                    title="Select manufacturer" ng-click="printHere(offerItem)"
                                                    oncontextmenu="return false;">Get a Quote
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
                        <div ng-repeat="offerItem in offersBundleData.offersBundle.offers" class="  ">
                            <div>
                                <div class="model-printers-available"
                                     ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
                                     style="display: block;">
                                    <div class="model-printers-available__wrap">
                                        <div class="model-printers-available__info">
                                            <div class="model-printers-available__info__img">
                                                <a href="/c/baysingers-additive-manufacturing?selectedPrinterId=3268&amp;model3dViewedStateId=1440784"
                                                   target="_blank" data-pjax="0"><img
                                                            ng-src="http://static.h5.tsdev.work/static/user/cf1420a1b265012ab70e3e275f812180/ps_logo_circle_1536848977_64x64.jpg"
                                                            width="30" height="30" alt="" align="left"
                                                            src="http://static.h5.tsdev.work/static/user/cf1420a1b265012ab70e3e275f812180/ps_logo_circle_1536848977_64x64.jpg"></a>
                                                <!-- ngIf: offerItem.psPrinter.isStatusPro -->
                                                <div ng-if="offerItem.psPrinter.isStatusPro"
                                                     class="cert-label cert-label--pro   "
                                                     data-toggle="tooltip" data-placement="bottom" title=""
                                                     data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                    Pro
                                                </div><!-- end ngIf: offerItem.psPrinter.isStatusPro -->

                                                <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                                            </div>

                                            <div class="model-printers-available__info__ps">
                                                <div class="model-printers-available__info__ps-title">
                                                    <a href="/c/baysingers-additive-manufacturing?selectedPrinterId=3268&amp;model3dViewedStateId=1440784"
                                                       target="_blank" data-pjax="0"><span class=" ">Baysinger's Additive Manufacturing</span></a>
                                                </div>

                                                <div
                                                ="psIdOnly">
                                                <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                                <div ng-if="offerItem.psPrinter.ps.reviewsInfo"
                                                     rating="offerItem.psPrinter.ps.reviewsInfo"
                                                     class="   ng-isolate-scope">
                                                    <!-- ngIf: showRating -->
                                                    <div ng-if="showRating"
                                                         class="star-rating-block   "
                                                         itemprop="aggregateRating" itemscope=""
                                                         itemtype="http://schema.org/AggregateRating">
                                                        <meta itemprop="ratingValue" content="5.0">
                                                        <meta itemprop="worstRating" content="1">
                                                        <meta itemprop="bestRating" content="5">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi"
                                                                 data-content="">
                                                                <div class="rating-stars"
                                                                     data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5.0" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5"
                                                                       data-step="0.1" data-size="xs"
                                                                       data-symbol=""
                                                                       data-glyphicon="false"
                                                                       data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>
                                                    </div><!-- end ngIf: showRating --><span
                                                            class="star-rating-count  ">(<span
                                                                itemprop="reviewCount"
                                                                class=" ">241</span> reviews)</span>
                                                </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                            </div>
                                            <div ng-show="psIdOnly" class="     ">
                                                <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
                                                <div ng-if="offerItem.psPrinter.reviewsInfo"
                                                     rating="offerItem.psPrinter.reviewsInfo"
                                                     class="   ng-isolate-scope">
                                                    <!-- ngIf: showRating -->
                                                    <div ng-if="showRating"
                                                         class="star-rating-block   "
                                                         itemprop="aggregateRating" itemscope=""
                                                         itemtype="http://schema.org/AggregateRating">
                                                        <meta itemprop="ratingValue" content="5">
                                                        <meta itemprop="worstRating" content="1">
                                                        <meta itemprop="bestRating" content="5">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi"
                                                                 data-content="">
                                                                <div class="rating-stars"
                                                                     data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5"
                                                                       data-step="0.1" data-size="xs"
                                                                       data-symbol=""
                                                                       data-glyphicon="false"
                                                                       data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>
                                                    </div><!-- end ngIf: showRating --><span
                                                            class="star-rating-count  ">(<span
                                                                itemprop="reviewCount"
                                                                class=" ">66</span> reviews)</span>
                                                </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
                                            </div>

                                            <div class="model-printers-available__info__ps-printer  ">
                                                3D Printer: Original Prusa i3 MK3
                                            </div>
                                        </div>
                                    </div>
                                    <div class="model-printers-available__delivery">
                                        <div class="model-printers-available__delivery-title  ">
                                            DALLAS, TX
                                        </div>
                                        <div class="model-printers-available__price-delivery">
                                            <span class=" ">Pickup, Delivery</span>
                                            <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
                                                    ng-if="offerItem.estimateDeliveryCost.amount"
                                                    class="  ">: <strong class=" "><span
                                                            class="tsi tsi-truck"
                                                            title="Delivery Price"></span> $4.80</strong>
                </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
                                            <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
                                            <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
                                        </div>
                                    </div>
                                </div>
                                <div class="model-printers-available__price-btn">

                                    <div class="model-printers-available__print-btn">
                                        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                                                title="Select manufacturer" ng-click="printHere(offerItem)"
                                                oncontextmenu="return false;">Get a Quote
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
                    <div ng-repeat="offerItem in offersBundleData.offersBundle.offers" class="  ">
                        <div>
                            <div class="model-printers-available"
                                 ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
                                 style="display: block;">
                                <div class="model-printers-available__wrap">
                                    <div class="model-printers-available__info">
                                        <div class="model-printers-available__info__img">
                                            <a href="/c/1304-stone-street?selectedPrinterId=4336&amp;model3dViewedStateId=1440784"
                                               target="_blank" data-pjax="0"><img
                                                        ng-src="http://static.h5.tsdev.work/static/user/7ad0732636df966fa9dbf2ced5eb721d/ps_logo_circle_1591482122_64x64.jpeg"
                                                        width="30" height="30" alt="" align="left"
                                                        src="http://static.h5.tsdev.work/static/user/7ad0732636df966fa9dbf2ced5eb721d/ps_logo_circle_1591482122_64x64.jpeg"></a>
                                            <!-- ngIf: offerItem.psPrinter.isStatusPro -->
                                            <div ng-if="offerItem.psPrinter.isStatusPro"
                                                 class="cert-label cert-label--pro   "
                                                 data-toggle="tooltip" data-placement="bottom" title=""
                                                 data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                Pro
                                            </div><!-- end ngIf: offerItem.psPrinter.isStatusPro -->

                                            <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                                        </div>

                                        <div class="model-printers-available__info__ps">
                                            <div class="model-printers-available__info__ps-title">
                                                <a href="/c/1304-stone-street?selectedPrinterId=4336&amp;model3dViewedStateId=1440784"
                                                   target="_blank" data-pjax="0"><span class=" ">GC-Tech 3D</span></a>
                                            </div>

                                            <div
                                            ="psIdOnly">
                                            <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                            <div ng-if="offerItem.psPrinter.ps.reviewsInfo"
                                                 rating="offerItem.psPrinter.ps.reviewsInfo"
                                                 class="   ng-isolate-scope">
                                                <!-- ngIf: showRating -->
                                                <div ng-if="showRating"
                                                     class="star-rating-block   "
                                                     itemprop="aggregateRating" itemscope=""
                                                     itemtype="http://schema.org/AggregateRating">
                                                    <meta itemprop="ratingValue" content="4.9">
                                                    <meta itemprop="worstRating" content="1">
                                                    <meta itemprop="bestRating" content="5">
                                                    <div class="star-rating rating-xs rating-disabled">
                                                        <div class="rating-container tsi"
                                                             data-content="">
                                                            <div class="rating-stars"
                                                                 data-content=""
                                                                 style="width: 98%;"></div>
                                                            <input value="4.9" type="text"
                                                                   class="star-rating form-control hide"
                                                                   data-min="0" data-max="5"
                                                                   data-step="0.1" data-size="xs"
                                                                   data-symbol=""
                                                                   data-glyphicon="false"
                                                                   data-rating-class="tsi"
                                                                   data-readonly="true"></div>
                                                    </div>
                                                </div><!-- end ngIf: showRating --><span
                                                        class="star-rating-count  ">(<span
                                                            itemprop="reviewCount"
                                                            class=" ">33</span> reviews)</span>
                                            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                        </div>
                                        <div ng-show="psIdOnly" class="     ">
                                            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
                                            <div ng-if="offerItem.psPrinter.reviewsInfo"
                                                 rating="offerItem.psPrinter.reviewsInfo"
                                                 class="   ng-isolate-scope">
                                                <!-- ngIf: showRating -->
                                                <div ng-if="showRating"
                                                     class="star-rating-block   "
                                                     itemprop="aggregateRating" itemscope=""
                                                     itemtype="http://schema.org/AggregateRating">
                                                    <meta itemprop="ratingValue" content="4.9">
                                                    <meta itemprop="worstRating" content="1">
                                                    <meta itemprop="bestRating" content="5">
                                                    <div class="star-rating rating-xs rating-disabled">
                                                        <div class="rating-container tsi"
                                                             data-content="">
                                                            <div class="rating-stars"
                                                                 data-content=""
                                                                 style="width: 98%;"></div>
                                                            <input value="4.9" type="text"
                                                                   class="star-rating form-control hide"
                                                                   data-min="0" data-max="5"
                                                                   data-step="0.1" data-size="xs"
                                                                   data-symbol=""
                                                                   data-glyphicon="false"
                                                                   data-rating-class="tsi"
                                                                   data-readonly="true"></div>
                                                    </div>
                                                </div><!-- end ngIf: showRating --><span
                                                        class="star-rating-count  ">(<span
                                                            itemprop="reviewCount"
                                                            class=" ">23</span> reviews)</span>
                                            </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
                                        </div>

                                        <div class="model-printers-available__info__ps-printer  ">
                                            3D Printer: Ender 5
                                        </div>
                                    </div>
                                </div>
                                <div class="model-printers-available__delivery">
                                    <div class="model-printers-available__delivery-title  ">
                                        JACKSONVILLE, Arkansas
                                    </div>
                                    <div class="model-printers-available__price-delivery">
                                        <span class=" ">Pickup, Delivery</span>
                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
                                                ng-if="offerItem.estimateDeliveryCost.amount"
                                                class="  ">: <strong class=" "><span
                                                        class="tsi tsi-truck"
                                                        title="Delivery Price"></span> $6.30</strong>
                </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
                                        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
                                        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
                                    </div>
                                </div>
                            </div>
                            <div class="model-printers-available__price-btn">

                                <div class="model-printers-available__print-btn">
                                    <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                                            title="Select manufacturer" ng-click="printHere(offerItem)"
                                            oncontextmenu="return false;">Get a Quote
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
                <div ng-repeat="offerItem in offersBundleData.offersBundle.offers" class="  ">
                    <div>
                        <div class="model-printers-available"
                             ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
                             style="display: block;">
                            <div class="model-printers-available__wrap">
                                <div class="model-printers-available__info">
                                    <div class="model-printers-available__info__img">
                                        <a href="/c/chris-l-3d?selectedPrinterId=4271&amp;model3dViewedStateId=1440784"
                                           target="_blank" data-pjax="0"><img
                                                    ng-src="http://static.h5.tsdev.work/static/files/bc/be/461050_21_d74ae0a3c16d83b0e0cd07796160d85d_64x64.png?date=1543226641"
                                                    width="30" height="30" alt="" align="left"
                                                    src="http://static.h5.tsdev.work/static/files/bc/be/461050_21_d74ae0a3c16d83b0e0cd07796160d85d_64x64.png?date=1543226641"></a>
                                        <!-- ngIf: offerItem.psPrinter.isStatusPro -->
                                        <div ng-if="offerItem.psPrinter.isStatusPro"
                                             class="cert-label cert-label--pro   "
                                             data-toggle="tooltip" data-placement="bottom" title=""
                                             data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                            Pro
                                        </div><!-- end ngIf: offerItem.psPrinter.isStatusPro -->

                                        <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                                    </div>

                                    <div class="model-printers-available__info__ps">
                                        <div class="model-printers-available__info__ps-title">
                                            <a href="/c/chris-l-3d?selectedPrinterId=4271&amp;model3dViewedStateId=1440784"
                                               target="_blank" data-pjax="0"><span class=" ">Chris L3d</span></a>
                                        </div>

                                        <div
                                        ="psIdOnly">
                                        <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                        <div ng-if="offerItem.psPrinter.ps.reviewsInfo"
                                             rating="offerItem.psPrinter.ps.reviewsInfo"
                                             class="   ng-isolate-scope">
                                            <!-- ngIf: showRating -->
                                            <div ng-if="showRating"
                                                 class="star-rating-block   "
                                                 itemprop="aggregateRating" itemscope=""
                                                 itemtype="http://schema.org/AggregateRating">
                                                <meta itemprop="ratingValue" content="4.9">
                                                <meta itemprop="worstRating" content="1">
                                                <meta itemprop="bestRating" content="5">
                                                <div class="star-rating rating-xs rating-disabled">
                                                    <div class="rating-container tsi"
                                                         data-content="">
                                                        <div class="rating-stars"
                                                             data-content=""
                                                             style="width: 98%;"></div>
                                                        <input value="4.9" type="text"
                                                               class="star-rating form-control hide"
                                                               data-min="0" data-max="5"
                                                               data-step="0.1" data-size="xs"
                                                               data-symbol=""
                                                               data-glyphicon="false"
                                                               data-rating-class="tsi"
                                                               data-readonly="true"></div>
                                                </div>
                                            </div><!-- end ngIf: showRating --><span
                                                    class="star-rating-count  ">(<span
                                                        itemprop="reviewCount" class=" ">212</span> reviews)</span>
                                        </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                                    </div>
                                    <div ng-show="psIdOnly" class="     ">
                                        <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
                                        <div ng-if="offerItem.psPrinter.reviewsInfo"
                                             rating="offerItem.psPrinter.reviewsInfo"
                                             class="   ng-isolate-scope">
                                            <!-- ngIf: showRating -->
                                            <div ng-if="showRating"
                                                 class="star-rating-block   "
                                                 itemprop="aggregateRating" itemscope=""
                                                 itemtype="http://schema.org/AggregateRating">
                                                <meta itemprop="ratingValue" content="4.9">
                                                <meta itemprop="worstRating" content="1">
                                                <meta itemprop="bestRating" content="5">
                                                <div class="star-rating rating-xs rating-disabled">
                                                    <div class="rating-container tsi"
                                                         data-content="">
                                                        <div class="rating-stars"
                                                             data-content=""
                                                             style="width: 98%;"></div>
                                                        <input value="4.9" type="text"
                                                               class="star-rating form-control hide"
                                                               data-min="0" data-max="5"
                                                               data-step="0.1" data-size="xs"
                                                               data-symbol=""
                                                               data-glyphicon="false"
                                                               data-rating-class="tsi"
                                                               data-readonly="true"></div>
                                                </div>
                                            </div><!-- end ngIf: showRating --><span
                                                    class="star-rating-count  ">(<span
                                                        itemprop="reviewCount"
                                                        class=" ">18</span> reviews)</span>
                                        </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
                                    </div>

                                    <div class="model-printers-available__info__ps-printer  ">
                                        3D Printer: Creality Ender 3
                                    </div>
                                </div>
                            </div>
                            <div class="model-printers-available__delivery">
                                <div class="model-printers-available__delivery-title  ">
                                    Bay Shore, New York
                                </div>
                                <div class="model-printers-available__price-delivery">
                                    <span class=" ">Pickup, Delivery</span>
                                    <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
                                            ng-if="offerItem.estimateDeliveryCost.amount"
                                            class="  ">: <strong class=" "><span
                                                    class="tsi tsi-truck"
                                                    title="Delivery Price"></span> $6.80</strong>
                </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
                                    <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
                                    <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
                                </div>
                            </div>
                        </div>
                        <div class="model-printers-available__price-btn">

                            <div class="model-printers-available__print-btn">
                                <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                                        title="Select manufacturer" ng-click="printHere(offerItem)"
                                        oncontextmenu="return false;">Get a Quote
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
    </div><!-- end ngIf: !(startedUpdatePrintersRequest || model3d.isCalculating()) -->

    <!-- ngIf: printersPageInfo.numPages>1 -->
    <nav class="  ">
        <ul class="pagination">
            <!-- ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
            <li
                    ng-class="{'active' : printersPageInfo.currentPage===$index}"
                    class="   active">
                <a href="javascript:;" ng-click="selectPage($index)" class=" ">1</a>
            </li>
            <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
            <li
                    ng-class="{'active' : printersPageInfo.currentPage===$index}" class="  ">
                <a href="javascript:;" ng-click="selectPage($index)" class=" ">2</a>
            </li>
            <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
            <li
                    ng-class="{'active' : printersPageInfo.currentPage===$index}" class="  ">
                <a href="javascript:;" ng-click="selectPage($index)" class=" ">3</a>
            </li>
            <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
            <li
                    ng-class="{'active' : printersPageInfo.currentPage===$index}" class="  ">
                <a href="javascript:;" ng-click="selectPage($index)" class=" ">4</a>
            </li>
            <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
            <li
                    ng-class="{'active' : printersPageInfo.currentPage===$index}" class="  ">
                <a href="javascript:;" ng-click="selectPage($index)" class=" ">5</a>
            </li>
            <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        </ul>
    </nav><!-- end ngIf: printersPageInfo.numPages>1 -->
</div>
</div>
</div>
    <div class="aff-widget-bottom">
        <button ng-click="prevStep()" class="btn btn-default">
            <span class="tsi tsi-left"></span>
            Back
        </button>
        <button ng-click="nextStep()" class="btn btn-primary">
            Next <span class="tsi tsi-right"></span>
        </button>
    </div>
</div>

</ui-view>
</div>
</div>


<div class="aff-widget  " id="print-navigation">
    <!-- Nav tabs -->
    <ul class="aff-widget-tabs clearfix">
        <li class="" ui-sref="upload" ui-sref-active="active">
            <a href="#upload" aria-controls="upload">
                <span class="aff-widget-tabs__step">1</span>
                Upload Files </a>
        </li>
        <li class="" ui-sref="printers">
            <a href="#print" aria-controls="print">
                <span class="aff-widget-tabs__step">2</span>
                Choose Manufacturer</a>
        </li>
        <li class="active" >
            <a href="#delivery" aria-controls="delivery">
                <span class="aff-widget-tabs__step">3</span>
                Project Description </a>
        </li>
    </ul>
    
    <div class="aff-widget-content clearfix">
        <!-- uiView: -->
        <ui-view class=" ">
            <div class="tab-pane  " id="delivery">
                <div style="margin: 15px 0 0 0;">
                    <div >
                        <div class="row js-type-delivery  ">

                            <div class="row">
                                <div class="col-lg-12 p-l30">
                                    <h3 class="delivery-details__title">Project Description</h3>
                                </div>
                                <div class="col-lg-12 p-l30">
                                    <p>
                                        <strong>Manufacturer:</strong>
                                        <img width="30" height="30" alt=""
                                             src="https://static.treatstock.com/static/user/a7e8ce2715799ff27bab9c684d389652/ps_logo_circle_1604445040_64x64.png"
                                             style="border-radius: 50%;">
                                        SomeFactoryName
                                    </p>

                                </div>
                            </div>
                            
                            <div class="col-sm-5 delivery-details wide-padding wide-padding--right">

                                <div class="row">
                                    <div class="form-group col-sm-12 field-deliveryform-email">
                                        <label class="control-label" for="deliveryform-email">Email <span
                                                    class="form-required">*</span></label>
                                        <input name="deliveryform-email"
                                               class="form-control ng-pristine ng-untouched ng-valid ng-not-empty"
                                               ng-model="deliveryForm.email">
                                        <p class="help-block help-block-error" validation-for="deliveryform-email"></p>
                                    </div>
                                    <div class="form-group col-sm-12 field-deliveryform-first_name">
                                        <label class="control-label" for="deliveryform-first_name">Your name<span
                                                    class="form-required">*</span></label>
                                        <input name="deliveryform-first_name"
                                               class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                               ng-model="deliveryForm.firstName">
                                        <p class="help-block help-block-error"
                                           validation-for="deliveryform-first_name"></p>
                                    </div>

                                    <div class="form-group col-sm-12 field-deliveryform-company">
                                        <label class="control-label" for="deliveryform-company">Company</label>
                                        <input name="deliveryform-company"
                                               class="form-control ng-pristine ng-untouched ng-valid ng-not-empty"
                                               ng-model="deliveryForm.company">
                                        <p class="help-block help-block-error"
                                           validation-for="deliveryform-company"></p>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-7 ">
                                <div class="row ">

                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label class="control-label m-r10" for="shipTo">Location<span
                                                        class="form-required">*</span></label>
                                            <div style="margin: 5px 0 25px">
                                                <input type="hidden"
                                                       class="form-control ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required"
                                                       rows="3" ng-model="preorder.shipTo" id="preorderShipTo"
                                                       required="required">
                                                <div class="UserLocationWidget">
                                                    <span class="tsi tsi-map-marker"></span>
                                                    <button type="button"
                                                            class="ts-user-location ts-ajax-modal btn-link"
                                                            title="Shipping Address" data-url="/geo/change-location"
                                                            data-target="#changelocation">Los Angeles, US
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="control-label"
                                                   for="preorderShippingAddress">Shipping Address </label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   ng-model="preorder.shipTo.address" id="preorderShippingAddress"
                                                   placeholder="Shipping Address">
                                        </div>

                                        <div class="form-group ">
                                            <label class="control-label" for="preorderDescr">Project
                                                Description<span class="form-required">*</span></label>
                                            <textarea
                                                    class="form-control "
                                                    rows="3" id="preorderDescr"
                                                    required="required"></textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-12 p-b10">
                                <span class="form-required">*</span> Required
                            </div>


                        </div>
                        <!-- end ngIf: deliveryForm.deliveryType == DELIVERY_TYPE_DELIVERY || deliveryForm.deliveryType == DELIVERY_TYPE_INTL -->
                    </div><!-- end ngIf: deliveryForm.loaded -->
                </div>

                <div class="aff-widget-bottom">
                    <button ng-click="prevStep()" class="btn btn-default">
                        <span class="tsi tsi-left"></span>
                        Back
                    </button>
                    <button class="btn btn-primary" ng-click="nextStep()" ng-disabled="showProgress">
                        <span ng-if="!showProgress" class=" ">
                            Submit
                        </span>
                    </button>
                </div>
            </div>
        </ui-view>
    </div>
</div>