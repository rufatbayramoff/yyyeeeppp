<?php

use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>

<div class="over-nav-tabs-header m-b0">
    <div class="container guides-page__container">
        <ol class="breadcrumb m-t10 m-b10">
            <li><a href="#cat">Manufacturing Guides</a></li>
            <li><a href="#cat/plastic">Materials</a></li>
            <li><a href="#cat/plastic">Metal</a></li>
        </ol>
    </div>
</div>

<div class="guides-page">
    <div class="guides-page__nav">

        <div class="guides-page__search">
            <form method="get" action="/help/search/" class="ng-pristine ng-valid">
                <div class="input-group">
                    <input type="text" name="q" class="form-control input-sm" placeholder="<?= _t('site.guide', 'Search in Guides...')?>">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <span class="tsi tsi-search"></span>
                        </button>
                    </span>
                </div>
            </form>
        </div>

        <a href="#guidesMainPage" class="guides-page__title"><?= _t('site.guide', 'Manufacturing Guides')?></a>

        <div class="side-nav-mobile">
            <div class="side-nav-mobile__title" data-toggle="collapse" data-target="#navMobile">
                <?= _t('site.guide', 'Guides Menu')?>
                <span class="tsi tsi-down"></span>
            </div>

            <ul class="side-nav help-nav" id="navMobile">
                <li>
                    <a href="#guides/tech">Technology <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                        <li><a href="#tech1">3D Printing </a></li>
                        <li><a href="#tech2">CNC</a></li>
                        <li><a href="#tech3">Injection</a></li>
                    </ul>
                </li>
                <li class="open">
                    <a href="#guides/materials">Materials <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                        <li><a href="#materials">ABS</a></li>
                        <li><a href="#materials">PLA</a></li>
                        <li><a href="#materials">Resin</a></li>
                        <li class="active"><a href="#materials">Metal</a></li>
                        <li><a href="#materials">Nylon</a></li>
                        <li><a href="#materials">VisiJet</a></li>
                        <li><a href="#materials">Sandstone</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#guides/tips&tricks">Tips & Tricks <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                    </ul>
                </li>
                <li>
                    <a href="#guides/how-to">How To <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                    </ul>
                </li>
                <li>
                    <a href="#guides/application">Application & Usage <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                    </ul>
                </li>
                <li>
                    <a href="#guides/3d-modelling">3D Modelling <span class="tsi tsi-right"></span></a>
                    <ul class="side-nav">
                    </ul>
                </li>
            </ul>

        </div>

    </div>
    <div class="guides-page__body guides-page__body--inner-page">

        <div class="post-hero">

            <div class="row post-hero__text">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h1 class="post-hero__title">
                        Application for Metal 3D printing: Case studies
                    </h1>
                    <div class="post__footer post__footer--hero">
                        <a href="#comments" class="post__comments post__comments--hero">
                            <span class="tsi tsi-comment"></span> <span class="fb-comments-count fb_comments_count_zero" data-href="http://ts5.vcap.me/blog/Must-Have-3D-print" fb-xfbml-state="rendered"><span class="fb_comments_count">0</span></span> comments                    </a>

                        <div class="post__date post__date--hero">
                            <span class="tsi tsi-calendar"></span>Aug 31, 2017                    </div>
                    </div>

                    <div class="m-t30 text-center">
                        <p class="post-share__label m-b10">Share with friends:</p>

                        <div class="model-share__list js-social-likes post-share__list social-likes social-likes_visible social-likes_ready">

                            <div class="model-share__btn"><a href="/site/sendemail" title="Send email" class="ts-ajax-modal">
                                    <span class="social-likes__icon social-likes__icon_mail"></span></a>
                            </div>

                            <div class="model-share__btn sharelink" id="sharelink" data-placement="bottom" data-container="body" data-toggle="popover" data-original-title="Share this link">
                                <span class="social-likes__icon social-likes__icon_link" title="Share this link"></span>
                            </div>

                            <div class="model-share__btn facebook-send" title="Facebook Messenger">
                                <span class="social-likes__icon social-likes__icon_facebook-m"></span> Facebook Send
                            </div>

                            <div class="model-share__btn social-likes__widget social-likes__widget_facebook" title="Share this link" data-display="popup"><span class="social-likes__button social-likes__button_facebook"><span class="social-likes__icon social-likes__icon_facebook"></span>Facebook</span><span class="social-likes__counter social-likes__counter_facebook social-likes__counter_empty"></span></div>
                            <div class="model-share__btn social-likes__widget social-likes__widget_twitter" title="Tweet this link" data-via="treatstock"><span class="social-likes__button social-likes__button_twitter"><span class="social-likes__icon social-likes__icon_twitter"></span> Twitter</span></div>
                            <div class="model-share__btn social-likes__widget social-likes__widget_pinterest" title="Pin this model" data-media="http://ts5.vcap.me/static/files/15/98/113879_15_de56ea67eadda21371bae310ff3d8d81.jpg?date=1504157919"><span class="social-likes__button social-likes__button_pinterest"><span class="social-likes__icon social-likes__icon_pinterest"></span> Pinterest</span><span class="social-likes__counter social-likes__counter_pinterest social-likes__counter_empty"></span></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="post-hero__bg" style="background-image: url('https://static.treatstock.com/static/files/25/67/44359_15_aabc3083acf5cfa351b68e281db6ffe6.jpg?date=1491832152')"></div>

        </div>

        <div class="container guides-page__container">

            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 post-tags">
                    <a href="#tag" class="post-tags__item">Metal</a>
                    <a href="#tag" class="post-tags__item">DSLM</a>
                    <a href="#tag" class="post-tags__item">How to</a>
                    <a href="#tag" class="post-tags__item">Usage</a>
                </div>
            </div>

            <div class="post-content">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 post-content__body">
                        <p class="lead">Being a maker means getting inspiration from the world around you and wanting to simplify things. This is how Jim Brown came up with his 3D printed automated GemCreator. He got this idea from his parents’ stories about their hand faceting hobby.</p>
                        <p>The application of metal 3D printing is widespread and continues to rapidly evolve. Numerous industries
                            now use 3D printing to support additive manufacturing for enhanced and more efficient production. EY’s
                            Global 3D printing <a
                                href="http://www.ey.com/Publication/vwLUAssets/ey-global-3d-printing-report-2016-full-report/$FILE/ey-global-3d-printing-report-2016-full-report.pdf"
                                target="_blank">Report&nbsp;</a>states that the top 3 industrial applications of metal 3D printing
                            are mechanical and plant engineering, electronics, and automotive and aerospace. In addition, metal 3D
                            printing has proven utility in the medical fields, such as for manufacturing prosthetics and implants.
                            Here, we explore the most exciting applications of metal 3D printing to demonstrate how this innovative
                            technology could be useful to you.</p>
                        <p>
                            <a href="http://www.ey.com/Publication/vwLUAssets/ey-global-3d-printing-report-2016-full-report/$FILE/ey-global-3d-printing-report-2016-full-report.pdf" target="_blank">
                                <img alt="Top 3 demanded industries for metal 3D printing" src="https://static.treatstock.com/static/uploads/ck_20171219_Demand for metal 3DP.JPG">
                            </a>
                        </p>
                        <p class="post-img-caption">
                            Some description for picture or copyrights or something else
                        </p>
                        <h2>3D printing for mechanical engineering</h2>
                        <p>Additive manufacturing provides countless opportunities for designers and engineers to create more
                            complex types of structures.</p>
                        <h3>1. Industrial repair by Siemens</h3>
                        <p>German Siemens has leveraged the power of 3D printing to dramatically shorten lead times for mechanical
                            repairs. For instance, the replacement of burner tips in gas turbines is both a time-consuming and
                            cost-intensive process. The entire repairing process can take up to nine months, and the cost of spare
                            burner tips is excessive. The application of 3D printing has helped Siemens extend the operation of
                            turbines, while concurrently improving burner tip design. The process involves removing the worn-out
                            burner tip, metal printing a new tip, and then welding it back into place. Ultimately, additive
                            manufacturing reduces repair time by as much as 90% – from up to 44 weeks to just 4 weeks.</p>
                        <div class="post-video">
                            <div class="video-container">
                                <iframe allow="encrypted-media" allowfullscreen="" frameborder="0" gesture="media" height="315"
                                        src="https://www.youtube.com/embed/8vtLtu4ZYsQ" width="560"></iframe>
                            </div>
                        </div>
                        <h3>2. Blades for gas-turbine engines</h3>
                        <p>Mechanical and plant engineering, as well as automotive industry, have intensively used metal 3D printing
                            for creating fully functional prototypes, parts, and tools. Companies such as General Electric (GE), in
                            it’s Aviation subdivision and avio S.p.A. utilize 3D printing for manufacturing turbine blades for
                            gas-turbine engines. Moreover, GE has developed the world’s largest laser-powered 3D printer for metal.
                            It is reported that this printer will be able to create parts that fit inside a 1 cubic-meter space.
                            Vice president and general manager of GE Additive, Mohammad Ehteshami, claims that this machine will be
                            capable of 3D printing jet engine structural components suitable for manufacturers in the power,
                            automotive, oil and gas industries.</p>
                        <blockquote>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                            <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                        </blockquote>
                        <h3>3. Increase in performance of hydraulics</h3>
                        <p>A quite unusual application of metal 3D printing was recently described in the Metal Additive
                            Manufacturing <a href="http://www.metal-am.com/introduction-to-metal-additive-manufacturing-and-3d-printing/case-study-hydraulic-crossing/" target="_blank">magazine</a>. With the help of Selective Laser Melting, engineers were able to
                            increase the performance of hydraulics by optimizing the functionality of the internal channel geometry.
                            <!-- Your previous sentence was confusing. I have simplified the text by merging the two sentences, but it needs to be confirmed for accuracy.  -->
                            It was written that the dimensions of the 3D printed part were 80 x 80 x 50 mm and weighed less than 1
                            kilogram. Overall, the mass of the part was significantly reduced, while at the same time its
                            performance increased. For instance, a test pressure (1400 bar) of the part revealed no plastic
                            deformation or liquid leakage. These benefits came at no additional expense associated with
                            post-processing.</p>
                        <p>
                            <a href="https://www.flickr.com/photos/spacexphotos/16789102495/" target="_blank">
                                <img
                                    alt="The SuperDraco is an engine used by SpaceX’s Cargo Dragon spacecraft"
                                    src="https://static.treatstock.com/static/uploads/ck_20171220_SuperDraco_rocket_engines_at_SpaceX_Hawthorne_facility_(16789102495).jpg"
                                    style="width: 100%;">
                            </a>
                        </p>
                        <h2>Metal 3D printing for Aerospace</h2>
                        <p>The popularity of 3D printing in the creation of aircraft parts continues to grow rapidly because energy
                            costs and waste during production are significantly reduced. The finished 3D print can also be much
                            lighter compared to the original parts, thus resulting in reducing the weight of planes and leading to
                            fuel saving.</p>
                        <h3>1. Fuel nozzles for jet engines</h3>
                        <p>GE Aviation uses metal 3D printing for manufacturing fuel nozzles for Boeing and Airbus planes. These new
                            designs integrated approximately 20 details of the traditionally manufactured part into a single one,
                            resulting in savings of about $1.6 million per flight.</p>
                        <h4>Table with some numbers</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td>Min. Layer Thickness：</td>
                                    <td>0.1 mm (in some cases 0.05 mm)</td>
                                </tr>
                                <tr>
                                    <td>Printing Precision：</td>
                                    <td>±0.1mm</td>
                                </tr>
                                <tr>
                                    <td>Nozzle Diameters：</td>
                                    <td>0.2mm, 0.3mm, 0.4 mm</td>
                                </tr>
                                <tr>
                                    <td>Printing Speed:</td>
                                    <td>200 mm/s</td>
                                </tr>
                                <tr>
                                    <td>Compatible filaments：</td>
                                    <td>PLA, TPU, Wood, Copper, Rubber</td>
                                </tr>
                                <tr>
                                    <td>Standard Print Size:</td>
                                    <td>300 x 300 x 400 mm</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <h3>2. Structural components for Dreamliners</h3>
                        <p>Boeing now orders 3D printed parts of titanium alloy for production of its new 787 Dreamliner. These
                            bone-like structures reduce weight and increases overall performance. Boeing reports that the usage of
                            metal 3D printing can reduce the cost of each Dreamliner by $2 - $3 million, and are considered to be
                            the first printed structural components that bear the stress of an airframe in flight.</p>
                        <h3>3. Space X and NASA</h3>
                        <p>NASA has successfully tested a jet engine containing 3D printed parts. The device withstood the same
                            stress as a regular rocket engine where fuel is burned at temperatures greater than 6,000 ℉
                            <!-- Formatting error.  -->. Meanwhile, Elon Musk’s SpaceX also uses additive manufacturing for
                            producing rocket ship parts at a reduced cost and improved performance.</p>
                        <div class="post-video">
                            <div class="video-container">
                                <iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/gSkEu9_Wg7Y"
                                        width="560"></iframe>
                            </div>
                        </div>
                        <h2>Metal 3D printing in healthcare</h2>
                        <p>Additive manufacturing has also proven to be beneficial for medical purposes. Both healthcare and dental
                            industry actively deploy metal 3D printing for creating hips, knees, shoulder implants, shoe insoles,
                            hearing aids, and prosthetics. Presently, medical implants are very commonly manufactured using 3D
                            printing. It is reported that customized products provide superior comfort that contributes to a faster
                            patient recovery. Risks of implant reject, as well as costs, are similarly reduced. Metal 3D printing
                            has also improved&nbsp;the <a href="/guide/3dprinting-dentistry" target="_blank">dental industry</a>. In
                            particular, 3D printing has emerged as a powerful technology in the field of orthodontics. Dental
                            technicians, and their patients, are now reaping the benefits resulting from 3D fabrication that is
                            replacing manual and time-consuming processes. Ultimately, this is making treatments more efficient and
                            also much more affordable.</p>

                        <div class="post-video">
                            <div class="video-container">
                                <iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/Xt0CiUDVPHk"
                                        width="560"></iframe>
                            </div>
                        </div>
                        <p><a href="<?= CatalogPrintingUrlHelper::printing3dCatalog(usageCode: 'metal') ?>"
                              target="_blank"><strong>3D Print with Metal</strong></a></p>

                        <h1>How has 3D Printing changed the jewelry industry?</h1>
                        <p>There are many misconceptions about the use of 3D printing for jewelry. One thing can be said for
                            sure – this technology has changed the rules of the game for everyone who has anything to do with
                            this industry. Learn why jewelers and goldsmiths from all over the world have fallen in love with
                            this technology and why it is considered even better than CNC machining in some cases.</p>
                        <h3>Manufacturing precious objects</h3>
                        <p>You may have heard that 3D printing is widely used in the manufacture of jewelry but are you familiar
                            with the reasons why though?</p>
                        <p>
                            <script src="//cdn.playbuzz.com/widget/feed.js"></script>
                        </p>
                        <div class="pb_feed pb_feed_anim pb_feed_rendered" data-comments="false"
                             data-embed-by="3dcc6f1f-997c-4dc5-aeb6-e77aaf37cfc8" data-game-info="false"
                             data-item="2fe12046-a2f7-407f-95d6-88be705950a2" data-shares="false" data-version="2"
                             element-index="0" render-status="rendered" style="position: relative; min-height: auto;">
                            <div class="pb_top_content_container pb_feed_anim"
                                 style="clear: both; padding-bottom: 10px; opacity: 1;">
                                <div>
                                    <iframe
                                        src="https://www.playbuzz.com/item/2fe12046-a2f7-407f-95d6-88be705950a2?feed=true&amp;comments=undefined&amp;divId=div0&amp;articleCanonicalUrl=https%3A%2F%2Fwww.treatstock.com%2F&amp;width=640&amp;height=auto&amp;useShares=false&amp;useComments=false&amp;gameInfo=false&amp;embedBy=3dcc6f1f-997c-4dc5-aeb6-e77aaf37cfc8&amp;item=2fe12046-a2f7-407f-95d6-88be705950a2&amp;version=2&amp;parentPageLoadUid=db90903d-77ac-49ea-b830-1abc1def330e&amp;social=true&amp;socialReferrer=false&amp;inFixedDialog=false&amp;pageLoadTrackerId=39741560475752480&amp;shouldTrackLoading=false&amp;parentHost=ru.treatstock.com&amp;parentUrl=https%3A%2F%2Fru.treatstock.com%2Fguide%2Fjewelry&amp;referral=https%3A%2F%2Fru.treatstock.com%2Fguide"
                                        name="pb_feed_iframe" class="pb_feed_iframe" frameborder="0" scrolling="no"
                                        height="100%" iniframe="false"
                                        style="min-width: 100%; width: 1px; max-width: 640px; height: 557px; border: none; margin: auto; display: table; max-height: 557px !important;"
                                        allowfullscreen="true"></iframe>
                                </div>
                            </div>
                            <div class="pb_feed_placeholder_container" style="display: none;">
                                <div class="pb_feed_placeholder_inner">
                                    <div class="pb_feed_placeholder_content">
                                        <div class="pb_feed_placeholder_preloader">
                                            <div
                                                style="position:absolute;width:90px;height:38px;transform:translateX(-60px);-webkit-transform:translateX(-60px);                -webkit-animation:pb_color_trans 5s;                -moz-animation:pb_color_trans 5s;                -ms-animation:pb_color_trans 5s;                animation:pb_color_trans 5s;                -webkit-animation-fill-mode:forwards;-moz-animation-fill-mode:forwards;-ms-animation-fill-mode:forwards;animation-fill-mode:forwards;">
                                                <div style="width:33%;height:100%;float:left;background-color:#bc49ff"></div>
                                                <div style="width:33%;height:100%;float:left;background-color:#009cff"></div>
                                                <div style="width:34%;height:100%;float:left;background-color:#bdbdbd"></div>
                                            </div>
                                            <img
                                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABUCAQAAACYJ/nYAAADp0lEQVR4Ae3aA7QkSRCF4Xjm2rZt27Zt27Zt27Zt27Z3u/Pe7DFi96jGUdWVLydXfxwrv3I1hJp2/oGAGh7DsX4jt2CvyX/olL+qjFWZEou4TXESHiMjAvAjz+Hi2ixG2syleDZ+6HEAnsWa2ijDZDLW4Us9B3gJi0qJ/LJ8MRiAn90m2iAl0wZsjd8CALjDTyiBYTzeUwqAgdhHeiRtwD7oXy/AuRWlB8Mq9PUAHBaWHs4tgN+KAuAWkAhhIfYqAMBArCmR8suiXz5gT4mY2yMHgLskatqAOwwAfuH4Ejk3Dn4aJYDbS8Fq82A/XoPn8RbfwDO4nLtipsKEzUYFeK3I4+anLuyLj6gjmdexrbZKgfDUSAF+uSJ6/Eg15jO3suTGxUcGeE1y+qUbt1ALzHnaIjnh6REAfsMc9fh8lVpwHv2hMwew2nAA/GIfvR86+TK1jrlHm+xXFvw8LOACMeN11PoGJ4kZTx8G4FYQI65PrRswGIvmnYgZAH21w9hd7fiOWmJet96ltIW1DICnTesu1HKD1cSID2YAnipGeLMsgPeIEY7JANjCuPVMSy076PdTl/Fw3iADWKcLt6eWH+vFzs2XAarTGDvq/BAA9jMeaJNkgMpYxh64NwTA86yrKwOIEZ4LAlwtRqMBgEvNO0F8gHWBu3HjA8x3rMpUowHg5zT2wILRAfjJehr4jaMDeJYY4fjIAAyoTmM/jGIDLhQjbSGiAvCjG0eM/DLUiAAM9EuLGS6ICMBgbC5m2sY/ogHQz20qOfkNqbEAn7v5JTe+FgfgcLR2SG5uZWogAM+Sw17zeNbtnr1XmGkjXwsE4Glt0ha3INf2u/ndsBUW+aVbjKy37DKAamUqKZ2fiNVAgNtUSqdNeJwaBrheAuJR1DDAV7+PKaVzm2JgEAADuXjot4RhgONDlmeNGgZ4VVukZFwffahhAO+ml1JpI47FYGooYDspVXXq7AupEADulBJpKw+ip4YDHCeo/5bjNs2+0AwGKD/FvtWxpWBuXO7Cz+2Fy1wFNdzsN8R4YlSb2G2C29HXXjToVoxBeA838RCuxSX8nJWpqtP42bEw1+MhvAof2IuVALhx8CM1YEIBuJWaEMDtqAkBmIm1hABt5RvUhACeTk0IcCthcEKAnwg/UZMBtIEPUBMCsBc1IaA2F/omBPzQiQ+pCQG4mJoQwHWpCQG9p+AfCQHahKepCQE8nJoQgIUxICHg9zH5JTUhgDf8/4fGdPMnaCBzQc6JVDQAAAAASUVORK5CYII="
                                                class="pb_feed_anim_mask"></div>
                                        <div class="pb_ie_fixer"></div>
                                        <div class="pb_feed_loading_text">Loading...</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>It actually has nothing to do with direct metal 3D printing technologies as most people would think.
                            Although it’s possible to use direct metal 3D printing, the costs are simply too high and therefore
                            majority of goldsmiths prefer using other options. So, let’s find out how additive manufacturing can
                            help. First of all, let’s take a close look at the traditional ways of manufacturing precious items
                            that are used by jewelers from all over the world. We will skip the technical details describing the
                            primary processes. The frequency of actions can be seen in the infographic below.</p>
                        <p><img alt=""
                                src="https://static.treatstock.com/static/uploads/ck_20171218_ck_20171107_Jewelry 3D printing.png"
                                style="width: 100%;"></p>
                        <h4>1.Master design (Jewelry model)</h4>
                        <p>First, the master model is made. Usually it is made manually by modeling clay or special wax.</p>
                        <h4>2. Making the press form</h4>
                        <p>Using the master model, a press form is created. The master model is covered with a liquid. After
                            it’s hardened, the object is halved. Now it is possible to create intermediate wax models for
                            casting.</p>
                        <h4>3. Intermediate model</h4>
                        <p>Using a special wax and press form, the intermediate model is made.</p>
                        <h4>4. Making the casting mold</h4>
                        <p>The intermediate model is covered with gypsum, that’s how the casting mold is made.</p>
                        <h4>5. Casting the piece</h4>
                        <p>After hardening the gypsum inside the mold, the liquid metal is poured. It replaces the wax.</p>
                        <h4>6. Post processing</h4>
                        <p>The final item is carefully taken out, mechanically handled and polished.</p>
                        <p>So, what does 3D printing have to do with this complicated process? Well, some of the stages are
                            simplified with the help of additive manufacturing. New technology has made the process much more
                            faster and cheaper. In cases of complex objects, it can be considered revolutionary.</p>

                        <div class="post-video">
                            <div class="video-container">
                                <iframe allow="encrypted-media" allowfullscreen="" frameborder="0" gesture="media" height="315"
                                        src="https://www.youtube.com/embed/EpiJOMENWNc" width="560"></iframe>
                            </div>
                        </div>
                        <h3>Direct 3D printing of the master model</h3>
                        <p>Businesses using high precision 3D printing technologies such as SLA (Stereolithography) and DLP
                            (Digital Light Processing) gain a competitive advantage over their rivals who continue to stick with
                            traditional manufacturing methods. With 3D printing, making the master model has become easier than
                            ever before. There is a special resin that can be molded as a regular wax. This helps to save time
                            as it is much simpler to make a digital 3D model and then 3D print it. SLA and DLP 3D printers use
                            laser/light projections that can form tiny elements which are difficult to even near impossible to
                            achieve manually. Such machines solidify resin layer-by-layer, eventually forming amazing and
                            high-detailed objects. With further advances in technology, such devices are evolving to be even
                            more precise, efficient and cost-effective. In addition, customers can see and hold 3D printed
                            prototypes and request changes before jewelers commit to the manufacture of the final product.</p>

                        <h3>3D printing the wax model for casting</h3>
                        <p>3D printers that are capable of making a model directly with a castable material like wax are
                            extremely popular amongst jewelers. Making such intermediate models can save time and money by
                            shortening the technological process. Thus, one needs less time for the finished product. Sometimes,
                            the item can be so complicated that making a press form and intermediate model is impossible. In
                            such cases, only 3D printing can help.</p>

                        <h3>Laser sintering for gold</h3>
                        <p>As mentioned already, 3D printing serves to optimize the casting process and should not be seen as a
                            total replacement. The process of making jewelry with the help of 3D printing shouldn’t be confused
                            with direct 3D printing in metal such as DMLS (Direct metal laser sintering) for example. In the
                            latter, the object is formed using metal powder and using gold or other precious alloys is much too
                            expensive. Furthermore, the minimal size of elements for direct metal 3D printing shouldn’t be less
                            than 1-2 mm whereas jewelry usually contains many small elements. However, 3D printing in precious
                            metals is still practiced. For example, designer Lionel Theodore Dean made a series of golden
                            adornments with 3D printer for metal.</p>

                        <p>To sum it up, 3D printing makes jewelry much more affordable. This technology can be supplemented by
                            the traditional methods of manufacturing that are already in use by jewelers such as CNC machining
                            and milling. Many customers like buying unique pieces that were made on request rather than regular
                            goods from the shops. Some jewelry entrepreneurs make all their products on demand. Before the
                            customer chooses the item on the site and clicks the “Order” button, the product doesn’t exist. So,
                            the jeweler doesn’t have to spend extra money on showrooms or expensive storage facilities.
                            Meanwhile, the cost of SLA and DLP 3D printers capable of 3D printing in castable resin continue to
                            become less expensive every year, whilst the quality becomes higher and higher. It’s important to
                            note that it’s not just the machines that you need, but a high-skilled operator too. In case you are
                            not going to spend money on a new 3D printer and expand your staff, you can order that particular
                            piece that is made as a wax model through Treatstock. Our new 3D printing services directory, among
                            other options, makes searching for local 3D print shops capable of such work easy and
                            convenient.</p>
                        <div class="post-video">
                            <div class="video-container">
                                <iframe allow="encrypted-media" allowfullscreen="" frameborder="0" gesture="media" height="315"
                                        src="https://www.youtube.com/embed/lOavkEcg_z4" width="560"></iframe>
                            </div>
                        </div>
                        <h3>Using CNC and 3D printing for Jewelry</h3>
                        <p>Jewelers all over the world use different methods and technologies in their work. Both CNC processing
                            and 3D printing can be beneficial for goldsmiths. Let’s consider the benefits and advantages of
                            each.</p>

                        <h4>CNC for Jewelry</h4>
                        <ul>
                            <li>Desktop CNC machines cost less than SLA/DLP printers</li>
                            <li>Solid wax is cheaper than liquid polymers used in 3D printing</li>
                            <li>Special skills are not required</li>
                        </ul>

                        <h4>3D printing for Jewelry</h4>
                        <ul>
                            <li>3D printers can produce extremely complicated shapes and geometric forms that cannot be produced
                                using only CNC
                            </li>
                            <li>SLA and DLP 3D printers can “grow” several wax patterns at the same time</li>
                            <li>Speed of manufacturing is much higher in comparison to other methods</li>
                        </ul>
                        <p>In summary, the choice of CNC machining or 3D printing depends on what kind of product you need to
                            create. If you need to make relatively simple shapes like rings or bracelets, CNC is usually
                            suffice. Goldsmiths state that 4- axis and 5-axis machines generally can fulfill most functions of a
                            3D printer. Besides, solid wax that is used for machining costs less than special castable
                            photopolymers for 3D printing. However, when one considers very small and delicate work that can be
                            only be done manually, you can use all the potential of SLA and DLP printers. We have professional
                            service providers that are able to help deliver your project at extremely affordable prices. Search
                            and compare prices using our user-friendly directory with filters designed to help you find the best
                            manufacturer for your needs.</p>
                        <p>
                            <a class="btn btn-primary" href="<?= CatalogPrintingUrlHelper::printing3dCatalog(usageCode: 'jewelry');?>">3Dprint for Jewelry</a>
                        </p>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>