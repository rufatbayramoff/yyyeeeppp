<?php

use common\models\user\UserIdentityProvider;
use common\modules\affiliate\helpers\AffiliateUrlHelper;

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);

$this->title = "Affiliates";
$userIdentityProvider = Yii::createObject(UserIdentityProvider::class);
$url = AffiliateUrlHelper::register();
?>

<?= $this->render('/common/ie-alert.php'); ?>

<div class="affiliates-land affiliates-land--grad affiliates-land-top">
    <div class="container">
        <div class="row relative">
            <div class="col-sm-6">
                <h1 class="affiliates-land-top__heading animated" data-animation-in="fadeInDown" data-animation-out="">
                    <?= _t('front.user', 'Affiliate Reward Program'); ?>
                </h1>

                <h3 class="affiliates-land-top__slogan animated" data-animation-in="fadeInDown" data-animation-out="">
                    <?= _t('front.user', 'Join our affiliate program. Average earning of our referrals is $1260 per month'); ?>
                </h3>
                <a href="<?= is_guest()?'#': $url; ?>" class="btn btn-lg btn-danger m-b20 animated"
                   data-animation-in="fadeInDown" data-animation-out="fadeOutDown"
                    <?php if (is_guest()): ?>
                        onclick="TS.Visitor.loginForm('<?=$url?>','signUp');"
                    <?php endif; ?>>
                    <?= _t('site.ps', 'Join Now'); ?>
                </a>
            </div>
            <div class="col-sm-5 col-md-offset-1">
                <div class="affiliates-land-top__pie">
                    <div class="affiliates-land-top__pie-percent">50<span>%</span></div>
                    <div class="affiliates-land-top__pie-text">
                        <?= _t('front.user', 'of our earning is yours'); ?>
                    </div>
                    <div class="affiliates-land-top__pie-label">
                        <?= _t('front.user', 'Your reward!'); ?><span>$</span>
                    </div>
                </div>
            </div>

            <!--
            <div class="affiliates-land-top__pic">$</div>
            -->
        </div>
    </div>
</div>

<div class="container-fliud affiliates-land affiliates-land-why">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 wide-padding wide-padding--right">
                <h2 class="affiliates-land__title animated" data-animation-in="fadeInDown" data-animation-out="">
                    <?= _t('site.ps', 'How it works?'); ?>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 m-b30 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <div class="affiliates-land-why__icon">
                    <img src="https://static.treatstock.com/static/images/affpage/place.svg" alt="Affiliate rewards for orders">
                </div>
                <p class="affiliates-land-why__text">
                    <?= _t('site.ps', 'You place a referral link to our platform where customers will see and click it'); ?>
                </p>
                <div class="tsi tsi-arrow-right-l affiliates-land-why__arrow"></div>
            </div>
            <div class="col-sm-4 m-b30 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <div class="affiliates-land-why__icon">
                    <img src="https://static.treatstock.com/static/images/affpage/cart.svg" alt="Affiliate cabinet">
                </div>
                <p class="affiliates-land-why__text">
                    <?= _t('site.ps', 'Customers place orders on Treatstock. We get a service fee from each order and share half of it with you!'); ?>
                </p>
                <div class="tsi tsi-arrow-right-l affiliates-land-why__arrow"></div>
            </div>
            <div class="col-sm-4 m-b30 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <div class="affiliates-land-why__icon">
                    <img src="https://static.treatstock.com/static/images/affpage/repay.svg" alt="Affiliate tracking">
                </div>
                <p class="affiliates-land-why__text">
                    <?= _t('site.ps', 'Customers return back and place new orders. You get 50% of our fee again!'); ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container-fliud affiliates-land affiliates-land-gray affiliates-land-promo">
    <div class="container">
        <h2 class="affiliates-land__title animated" data-animation-in="fadeInDown" data-animation-out="">
            <?= _t('site.ps', 'Where can I promote Treatstock?'); ?>
        </h2>
        <div class="row p-t20">
            <div class="col-sm-3 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <div class="designer-card affiliates-land-promo__card">
                    <div class="affiliates-land-promo__icon">
                        <img src="https://static.treatstock.com/static/images/affpage/promote-web.svg" alt="">
                    </div>
                    <h4 class="affiliates-land-promo__title">
                        <?= _t('site.ps', 'On your news website or blog about technologies'); ?>
                    </h4>
                </div>
            </div>
            <div class="col-sm-3 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <div class="designer-card affiliates-land-promo__card">
                    <div class="affiliates-land-promo__icon">
                        <img src="https://static.treatstock.com/static/images/affpage/promote-yt.svg" alt="">
                    </div>
                    <h4 class="affiliates-land-promo__title">
                        <?= _t('site.ps', 'Youtube videos'); ?>
                    </h4>
                </div>
            </div>
            <div class="col-sm-3 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <div class="designer-card affiliates-land-promo__card">
                    <div class="affiliates-land-promo__icon">
                        <img src="https://static.treatstock.com/static/images/affpage/promote-sm.svg" alt="">
                    </div>
                    <h4 class="affiliates-land-promo__title">
                        <?= _t('site.ps', 'Social media'); ?>
                    </h4>
                </div>
            </div>
            <div class="col-sm-3 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <div class="designer-card affiliates-land-promo__card">
                    <div class="affiliates-land-promo__icon">
                        <img src="https://static.treatstock.com/static/images/affpage/promote-forum.svg" alt="">
                    </div>
                    <h4 class="affiliates-land-promo__title">
                        <?= _t('site.ps', 'Forums where you place articles or expert opinion'); ?>
                    </h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fliud affiliates-land ">
    <div class="container">
        <h2 class="affiliates-land__title animated" data-animation-in="fadeInDown" data-animation-out="">
            <?= _t('site.ps', 'Why your visitors will love Treatstock?'); ?>
        </h2>
        <div class="affiliates-land-widget__list">
            <div class="affiliates-land-widget__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <img class="affiliates-land-widget__icon" src="https://static.treatstock.com/static/images/common/big-icons/materials-colors.svg" alt="">
                <h4 class="affiliates-land-widget__title">
                    <?= _t('site.ps', 'Customize files by choosing from different materials and colors'); ?>
                </h4>
            </div>
            <div class="affiliates-land-widget__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <img class="affiliates-land-widget__icon" src="https://static.treatstock.com/static/images/common/big-icons/save.svg" alt="">
                <h4 class="affiliates-land-widget__title">
                    <?= _t('site.ps', 'Real-time quotes from local manufacturers'); ?>
                </h4>
            </div>
            <div class="affiliates-land-widget__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <img class="affiliates-land-widget__icon" src="https://static.treatstock.com/static/images/common/big-icons/compare-services.svg" alt="">
                <h4 class="affiliates-land-widget__title">
                    <?= _t('site.ps', 'Compare prices and reviews to get the best deals online'); ?>
                </h4>
            </div>
            <div class="affiliates-land-widget__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                <img class="affiliates-land-widget__icon" src="https://static.treatstock.com/static/images/common/big-icons/instant-security.svg" alt="">
                <h4 class="affiliates-land-widget__title">
                    <?= _t('site.ps', 'Place orders instantly with secure online payments'); ?>
                </h4>
            </div>
        </div>
    </div>
</div>

<div class="affiliates-land affiliates-land--grad affiliates-land-reviews">
    <div class="container">
        <h2 class="affiliates-land-reviews__title animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
            <?= _t('site.ps', 'What our partners are saying'); ?>
        </h2>
    </div>
    <div class="affiliates-land-reviews__swiper swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="affiliates-land-reviews__info">
                    <p class="affiliates-land-reviews__text">
                        "<?= _t('site.ps', 'Being a Treatstock affiliate opened up new possibilities for my visitors, and it can be seen in both traffic and feedback. The communication with the guys over at Treatstock is great - they are always helpful, always listening, and provided solutions to all the issues I had. In all honesty, it’s been a real pleasure working with them!'); ?>"
                    </p>
                    <!--<noindex>-->
                    <a class="affiliates-land-reviews__logo" href="http://www.viewstl.com/" rel="nofollow" target="_blank">
                        <img src="https://static.treatstock.com/static/images/affpage/aff-partners-viewstl.png" alt="">ViewSTL.com
                    </a>
                    <!--</noindex>-->
                </div>
            </div>
            <div class="swiper-slide">
                <div class="affiliates-land-reviews__info">
                    <p class="affiliates-land-reviews__text">
                        "<?= _t('site.ps', 'Treatstock is great to work with and has allowed Yeggi to provide its users with access to high-quality and fast-working manufacturers.'); ?>"
                    </p>
                    <!--<noindex>-->
                    <a class="affiliates-land-reviews__logo" href="http://www.yeggi.com/" rel="nofollow" target="_blank">
                        <img src="https://static.treatstock.com/static/images/affpage/aff-partners-yeggi.png" alt="">
                    </a>
                    <!--</noindex>-->
                </div>
            </div>
            <div class="swiper-slide">
                <div class="affiliates-land-reviews__info">
                    <p class="affiliates-land-reviews__text">
                        "<?= _t('site.ps', 'Thanks to the affiliate partnership with Treatstock, our users can now find a design from our repository and get it 3D printed in next to no time. They\'ve made placing an order fun and easy to do, and the best part is we get paid for every order made by our referral for up to six months.'); ?>"
                    </p>
                    <!--<noindex>-->
                    <a class="affiliates-land-reviews__logo" href="https://www.yobi3d.com/" rel="nofollow" target="_blank">
                        <img src="https://static.treatstock.com/static/images/affpage/aff-partners-yobi3d.png" alt="">
                    </a>
                    <!--</noindex>-->
                </div>
            </div>
        </div>
        <div class="affiliates-land-reviews__scrollbar swiper-scrollbar"></div>
    </div>
</div>

<div class="container-fliud affiliates-land affiliates-land-gray">
    <div class="container">
        <h2 class="affiliates-land__title animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
            <?= _t('site.ps', 'User friendly and very informative cabinet'); ?>
        </h2>
        <div class="row affiliates-land-screens__row">
            <div class="col-sm-7">
                <img class="affiliates-land-screens__pic animated"
                     data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft"
                     src="https://static.treatstock.com/static/images/affpage/screen1.png" alt="">
            </div>
            <div class="col-sm-5">
                <h3 class="affiliates-land-screens__title animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <?= _t('site.ps', 'Quick link building'); ?>
                </h3>
            </div>
        </div>
        <div class="row affiliates-land-screens__row affiliates-land-screens__row--2nd">
            <div class="col-sm-7">
                <img class="affiliates-land-screens__pic animated"
                     data-animation-in="fadeInRight" data-animation-out="fadeOutRight"
                     src="https://static.treatstock.com/static/images/affpage/screen2.png" alt="">
            </div>
            <div class="col-sm-5">
                <h3 class="affiliates-land-screens__title animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                    <?= _t('site.ps', 'Detailed statistics of all orders from your referrals. 100% transparent and open partnership.'); ?>
                </h3>
            </div>
        </div>
        <div class="row affiliates-land-screens__row">
            <div class="col-sm-7">
                <img class="affiliates-land-screens__pic animated"
                     data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft"
                     src="https://static.treatstock.com/static/images/affpage/screen3.png" alt="">
            </div>
            <div class="col-sm-5">
                <h3 class="affiliates-land-screens__title animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <?= _t('site.ps', 'Withdraw via Paypal'); ?>
                </h3>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h2 class="affiliates-land-reviews__join animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                <?= _t('site.ps', 'Join Our Affiliate Reward Program'); ?>
            </h2>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h3 class="affiliates-land-reviews__join-sub animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                        <?= _t('site.ps', 'Register on Treatstock as a referral and join our affiliate reward program to start earning $1260 per month.'); ?>
                    </h3>
                </div>
            </div>
            <a href="<?= is_guest()?'#':$url ?>" class="btn btn-lg btn-danger m-b20 animated"
               data-animation-in="fadeInDown" data-animation-out="fadeOutDown"
                <?php if (is_guest()): ?>
                    onclick="TS.Visitor.loginForm('<?=$url;?>','signUp');"
                <?php endif; ?>>
                <?= _t('site.ps', 'Start Now'); ?>
            </a>
        </div>
    </div>
</div>

<script>
    <?php $this->beginBlock('js1', false); ?>

    function swiperAffReviews() {
        //Init slider for .main-page-prints__slider
        var swiperAffReviews = new Swiper('.affiliates-land-reviews__swiper', {
            scrollbar: '.affiliates-land-reviews__scrollbar',
            scrollbarHide: true,
            slidesPerView: 'auto',
            centeredSlides: true,
            spaceBetween: 30,
            grabCursor: true,
            breakpoints: {
                767: {
                    slidesPerView: 1.15,
                    centeredSlides: false
                }
            }
        });
    }

    swiperAffReviews();
    $(window).resize(function () {swiperAffReviews()});


    <?php $this->endBlock(); ?>
</script>
<?php $this->registerJs($this->blocks['js1']); ?>