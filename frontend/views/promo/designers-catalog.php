<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

$this->title = "Custom 3D design";

?>


<div class="container">
    <h1 class="text-center m-t30 m-b20"><?= _t('site.custom3d', 'Custom 3D Design'); ?></h1>

    <h3 class="designers-cat-2heading">
        <?= _t('site.custom3d', 'If you want to create something unique or need a prototype for your invention, hire a designer to exclusively create your 3D model idea.'); ?>
    </h3>
</div>

<div class="container">
    <div class="responsive-container-list responsive-container-list--3">

        <div class="responsive-container">
            <div class="designer-card">

                <div class="designer-card__userinfo">

                    <a class="designer-card__avatar" href="https://www.treatstock.com/u/i.mark">
                        <img src="https://static.treatstock.com/static/user/fba9d88164f3e2d9109ee770223212a0/avatar_128x128.jpeg" alt="" align="left">
                    </a>

                    <div class="designer-card__username">
                        i.mark
                    </div>

                </div>

                <div class="designer-card__models-list">
                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/4496-3d-printable-reaper-mask-from-overwatch" title="3D Printable Reaper Mask from Overwatch">
                        <img src="https://static.treatstock.com/static/user/fba9d88164f3e2d9109ee770223212a0/model/13568_358x269.jpg" alt="3D Printable Reaper Mask from Overwatch">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/1093-3d-printable-katana-mask-from-suicide-squad" title="3D Printable Katana Mask from Suicide Squad">
                        <img src="https://static.treatstock.com/static/user/fba9d88164f3e2d9109ee770223212a0/model/6395_358x269.png" alt="3D Printable Katana Mask from Suicide Squad">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/324-clover-pendant" title="Clover pendant">
                        <img src="https://static.treatstock.com/static/user/fba9d88164f3e2d9109ee770223212a0/model324/1927_358x269.png" alt="Clover pendant">
                    </a>
                </div>

                <div class="designer-card__about">
                    I am experienced in 3D modeling and design with my specialty being cosplay and items from<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                    <span class="tsrm-content hide"> movies and games. I'm really passionate about what I do and if you need a 3D printable model designed for you within a short turnaround, then send me a message and I will create it for you.</span>
                </div>

                <div class="designer-card__btn-block">
                    <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="https://www.treatstock.com/3d-printable-models-store/1002">Hire Designer</a>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card">

                <div class="designer-card__userinfo">

                    <a class="designer-card__avatar" href="https://www.treatstock.com/u/sergey-%223dpicasso%22-kolesnik">
                        <img src="https://static.treatstock.com/static/user/google_105184706932840335390/avatar_128x128.jpg" alt="" align="left">
                    </a>

                    <div class="designer-card__username">
                        Sergey Kolesnik
                    </div>

                </div>

                <div class="designer-card__models-list">
                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/127-nn-14-blaster-pistol" title="NN-14 BLASTER PISTOL">
                        <img src="https://static.treatstock.com/static/user/31857b449c407203749ae32dd0e7d64a/model127/400_358x269.jpg" alt="NN-14 BLASTER PISTOL">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/222-smg-from-wolfenstein-the-new-order" title="SMG from Wolfenstein: The New Order">
                        <img src="https://static.treatstock.com/static/user/31857b449c407203749ae32dd0e7d64a/model222/932_358x269.jpg" alt="SMG from Wolfenstein: The New Order">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/123-dookus-lightsaber" title="Dookus Lightsaber">
                        <img src="https://static.treatstock.com/static/user/31857b449c407203749ae32dd0e7d64a/model123/330_358x269.png" alt="Dookus Lightsaber">
                    </a>

                </div>

                <div class="designer-card__about">
                    3D graphics / 3D Art for games,3d for casual gaming. Concept 3d .Toy 3d concepts
                </div>

                <div class="designer-card__btn-block">
                    <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="https://www.treatstock.com/3d-printable-models-store/1067">Hire Designer</a>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card">

                <div class="designer-card__userinfo">

                    <a class="designer-card__avatar" href="https://www.treatstock.com/u/tomislav-veg">
                        <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/avatar_128x128.jpg" alt="" align="left">
                    </a>

                    <div class="designer-card__username">
                        Tomislav
                    </div>

                </div>

                <div class="designer-card__models-list">
                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/753-leonardo-di-caprio" title="Leonardo  Di Caprio">
                        <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/model/7763_358x269.jpg" alt="Leonardo  Di Caprio">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/1763-donald-trump-sandwich-prick" title="Donald Trump Sandwich Prick">
                        <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/model/7787_358x269.jpg" alt="Donald Trump Sandwich Prick">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/1764-hillary-clinton-sandwich-prick" title="Hillary Clinton Sandwich Prick">
                        <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/model/7791_358x269.jpg" alt="Hillary Clinton Sandwich Prick">
                    </a>
                </div>

                <div class="designer-card__about">
                    I'm self learned digital sculptor and 3D artist. Sculpting and designing models for 3D print
                </div>

                <div class="designer-card__btn-block">
                    <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="https://www.treatstock.com/3d-printable-models-store/1453">Hire Designer</a>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card">

                <div class="designer-card__userinfo">

                    <a class="designer-card__avatar" href="https://www.treatstock.com/u/XXYiceMAN">
                        <img src="https://static.treatstock.com/static/user/cefab442b1728a7c1b49c63f1a55781c/avatar_1468511999_128x128.jpg" alt="" align="left">
                    </a>

                    <div class="designer-card__username">
                        tonghui li
                    </div>

                </div>

                <div class="designer-card__models-list">
                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/1193-sexy-girl-lying-scale-in-110" title="Sexy girl lying Scale in 1/10">
                        <img src="https://static.treatstock.com/static/user/cefab442b1728a7c1b49c63f1a55781c/model1193/6096_358x269.jpg" alt="Sexy girl lying Scale in 1/10">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/1242-kneeling-girl-003-scale-in-110" title="Kneeling Girl 003 Scale in 1/10">
                        <img src="https://static.treatstock.com/static/user/cefab442b1728a7c1b49c63f1a55781c/model/6255_358x269.jpg" alt="Kneeling Girl 003 Scale in 1/10">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/1192-king-kong-scale-in-130" title="King  Kong Scale in 1:30">
                        <img src="https://static.treatstock.com/static/user/cefab442b1728a7c1b49c63f1a55781c/model1192/6093_358x269.jpg" alt="King  Kong Scale in 1:30">
                    </a>

                </div>

                <div class="designer-card__about">
                    Hello! I come from Beijing, China, I am a 3D printing designer . I like to make sexy<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                    <span class="tsrm-content hide"> female characters. I made a lot of beautiful women.I have my own 3D print shop, if you are interested you can go to print your favorite works.</span>
                </div>

                <div class="designer-card__btn-block">
                    <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="https://www.treatstock.com/3d-printable-models-store/1756">Hire Designer</a>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card">

                <div class="designer-card__userinfo">

                    <a class="designer-card__avatar" href="https://www.treatstock.com/u/3dartdigital">
                        <img src="https://static.treatstock.com/static/user/a424ed4bd3a7d6aea720b86d4a360f75/avatar_128x128.jpg" alt="" align="left">
                    </a>

                    <div class="designer-card__username">
                        3dartdigital
                    </div>

                </div>

                <div class="designer-card__models-list">
                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/724-pirate-money-bank" title="Pirate money bank">
                        <img src="https://static.treatstock.com/static/user/a424ed4bd3a7d6aea720b86d4a360f75/model724/3507_358x269.jpg" alt="Pirate money bank">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/726-bird-bad-piggy-bank" title="Bird Bad  piggy bank">
                        <img src="https://static.treatstock.com/static/user/a424ed4bd3a7d6aea720b86d4a360f75/model726/3547_358x269.jpg" alt="Bird Bad  piggy bank">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/727-sheriff-bird-piggy-bank" title="Sheriff Bird piggy bank">
                        <img src="https://static.treatstock.com/static/user/a424ed4bd3a7d6aea720b86d4a360f75/model727/3565_358x269.jpg" alt="Sheriff Bird piggy bank">
                    </a>
                </div>

                <div class="designer-card__about">
                    3dartdigital is a design studio specializing in the development of exclusive 3D models for 3D printing.
                </div>

                <div class="designer-card__btn-block">
                    <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="https://www.treatstock.com/3d-printable-models-store/1425">Hire Designer</a>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card">

                <div class="designer-card__userinfo">

                    <a class="designer-card__avatar" href="https://www.treatstock.com/u/EnterpriseXDDesign">
                        <img src="https://static.treatstock.com/static/user/82965d4ed8150294d4330ace00821d77/avatar_128x128.jpg" alt="" align="left">
                    </a>

                    <div class="designer-card__username">
                        Enterprise XD Design
                    </div>

                </div>

                <div class="designer-card__models-list">
                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/1273-pokeball-ring-box" title="Pokèball Ring Box">
                        <img src="https://static.treatstock.com/static/user/82965d4ed8150294d4330ace00821d77/model/6382_358x269.jpg" alt="Pokèball Ring Box">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/844-treasure-chest-jewellery-box" title="Treasure Chest Jewellery Box">
                        <img src="https://static.treatstock.com/static/user/82965d4ed8150294d4330ace00821d77/model844/4266_358x269.jpg" alt="Treasure Chest Jewellery Box">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/507-bob-the-alien-robolamp" title="Bob, the Alien Robolamp">
                        <img src="https://static.treatstock.com/static/user/82965d4ed8150294d4330ace00821d77/model507/2648_358x269.jpg" alt="Bob, the Alien Robolamp">
                    </a>
                </div>

                <div class="designer-card__about">
                    I've been a traditional 2D designer for 25 years, and over the last couple of years<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                    <span class="tsrm-content hide"> I've been getting into the world of 3D design and printing. Eventually I realised it's too much fun to not take seriously and decided to make a business of it.  I hope you enjoy my designs, and if there's anything you'd specifically like designed just for you please send me a message and I'll be happy to discuss your needs with you!</span>
                </div>

                <div class="designer-card__btn-block">
                    <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="https://www.treatstock.com/3d-printable-models-store/1346">Hire Designer</a>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card">

                <div class="designer-card__userinfo">

                    <a class="designer-card__avatar" href="https://www.treatstock.com/u/khaled-alkayed">
                        <img src="https://static.treatstock.com/static/user/eaa32c96f620053cf442ad32258076b9/avatar_128x128.jpg" alt="" align="left">
                    </a>

                    <div class="designer-card__username">
                        Khaled Alkayed
                    </div>

                </div>

                <div class="designer-card__models-list">
                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/733-body-kylo-ren" title="Body Kylo Ren">
                        <img src="https://static.treatstock.com/static/user/eaa32c96f620053cf442ad32258076b9/model733/3667_358x269.jpg" alt="Body Kylo Ren">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/736-kylo-ren-key-chain" title="Kylo Ren Key Chain">
                        <img src="https://static.treatstock.com/static/user/eaa32c96f620053cf442ad32258076b9/model736/3677_358x269.jpg" alt="Kylo Ren Key Chain">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/739-kylo-ren-head" title="Kylo Ren Head">
                        <img src="https://static.treatstock.com/static/user/eaa32c96f620053cf442ad32258076b9/model739/3691_358x269.jpg" alt="Kylo Ren Head">
                    </a>
                </div>

                <div class="designer-card__about">
                    3D Artist at Mixed Dimensions
                </div>

                <div class="designer-card__btn-block">
                    <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="https://www.treatstock.com/3d-printable-models-store/1436">Hire Designer</a>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card">

                <div class="designer-card__userinfo">

                    <a class="designer-card__avatar" href="https://www.treatstock.com/u/laughing-monkey-labs">
                        <img src="https://static.treatstock.com/static/user/google_106616681868710278376/avatar_128x128.jpg" alt="" align="left">
                    </a>

                    <div class="designer-card__username">
                        Thomas Owsiany
                    </div>

                </div>

                <div class="designer-card__models-list">
                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/5384-batman-cookie-cutter" title="Batman Cookie Cutter">
                        <img src="https://static.treatstock.com/static/files/25/b2/16546_2474_fa1522012eb46e9f86766df3d2730aa7_720x540.jpg" alt="Batman Cookie Cutter">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/6079-avengers-cookie-cutter" title="Avengers Cookie Cutter">
                        <img src="https://static.treatstock.com/static/files/25/b2/16547_2474_1245fdba968a6d7bb58cd00bd234677f_720x540.jpg" alt="Avengers Cookie Cutter">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/6084-deadpool-cookie-cutter" title="Deadpool Cookie Cutter">
                        <img src="https://static.treatstock.com/static/files/25/b2/16549_2474_c569dfea65091db3f85f6ea359ca10d6_720x540.jpg" alt="Deadpool Cookie Cutter">
                    </a>
                </div>

                <div class="designer-card__about">
                </div>

                <div class="designer-card__btn-block">
                    <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="https://www.treatstock.com/3d-printable-models-store/2474">Hire Designer</a>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card">

                <div class="designer-card__userinfo">

                    <a class="designer-card__avatar" href="https://www.treatstock.com/u/TanyaAkinora">
                        <img src="https://static.treatstock.com/static/user/facebook_1023706591011911/avatar_128x128.jpg" alt="" align="left">
                    </a>

                    <div class="designer-card__username">
                        Tatyana Bulgakova
                    </div>

                </div>

                <div class="designer-card__models-list">
                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/958-quilling-tulips" title="Quilling 'Tulips'">
                        <img src="https://static.treatstock.com/static/user/3c1e4bd67169b8153e0047536c9f541e/model958/4959_358x269.jpg" alt="Quilling 'Tulips'">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/962-cinderella-carriage" title="Cinderella Carriage">
                        <img src="https://static.treatstock.com/static/user/3c1e4bd67169b8153e0047536c9f541e/model962/4985_358x269.jpg" alt="Cinderella Carriage">
                    </a>

                    <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/992-pill-dispenser" title="Pill Dispenser">
                        <img src="https://static.treatstock.com/static/user/3c1e4bd67169b8153e0047536c9f541e/model992/5262_358x269.jpg" alt="Pill Dispenser">
                    </a>
                </div>

                <div class="designer-card__about">
                    All my models were created in the program 123D Design.
                </div>

                <div class="designer-card__btn-block">
                    <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="https://www.treatstock.com/3d-printable-models-store/1639">Hire Designer</a>
                </div>

            </div>
        </div>

    </div>
</div>