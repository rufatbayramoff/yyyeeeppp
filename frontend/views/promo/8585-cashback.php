<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>
<div class="over-nav-tabs-header">
    <div class="container container--wide"><h1>My Business</h1></div>
</div>

<div class="nav-tabs__container">
    <div class="container container--wide">
        <ul id="w0" class="nav nav-tabs" role="tablist">
            <li><a href="/mybusiness/company">About</a></li>
            <li><a href="/mybusiness/products">Shop</a></li>
            <li><a href="/mybusiness/services">Services</a></li>
            <li><a href="/mybusiness/taxes">Taxes</a></li>
            <li class="active"><a href="/mybusiness/widgets">Business Tools</a></li>
        </ul>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="side-nav-mobile">
                <div class="side-nav-mobile__title" data-toggle="collapse" data-target="#navMobile">
                    Business Tools Menu <span class="tsi tsi-down"></span>
                </div>
                <ul id="navMobile" class="side-nav help-nav">
                    <li class="text-divider">Services</li>
                    <li><a href="/mybusiness/widgets/printers">My Services Widget</a></li>
                    <li><a href="/mybusiness/widgets/get-quote">Get a Quote Widget</a></li>
                    <li class="active"><a href="#cash">Cashback</a></li>
                    <li class="text-divider">Products</li>
                    <li><a href="/mybusiness/widgets/store-products">Products Store</a></li>
                    <li class="text-divider">3D Models</li>
                    <li><a href="/mybusiness/widgets/store">3D Models Store</a></li>
                    <li><a href="/mybusiness/widgets/models">3D Model Widget</a></li>
                    <li class="text-divider">Partnership</li>
                    <li><a href="/mybusiness/affiliates">Affiliate</a></li>
                    <li><a href="/mybusiness/widgets/affiliate-widget">Affiliate widget</a></li>
                    <li><a href="/mybusiness/widgets/api">API</a></li>
                </ul>
            </div>
        </div>

        <div class="col-sm-9 wide-padding wide-padding--left">


            <div class="row">
                <div class="col-lg-12">
                    <div class="designer-card">
                        <h2 class="designer-card__title">
                            <?= _t('site.ps', 'Cashback'); ?>
                        </h2>
                        <p class="designer-card__about">
                            <?= _t('site.ps', 'Описание Cashbackа и все такое. Текстовый блок для информации и буковок со словами. По факту тут дофига всего можно написать, но желательно кратко и информативно. Люди не любят читать.'); ?>
                        </p>
                        <div class="row">
                            <div class="col-md-8 wide-padding wide-padding--right">
                                <h3 class="m-t0 m-b20 text-center"><?= _t('site.ps', ' User\'s Total Cashback'); ?> = 10%</h3>
                                <div class="cashback-towers">
                                    <div class="cashback-towers__tower">
                                        <div class="cashback-towers__header">
                                            <?= _t('site.ps', 'Cashback from'); ?>
                                            <br>
                                            <b><?= _t('site.ps', 'you'); ?></b>
                                        </div>
                                        <div class="cashback-towers__body">
                                            <div class="cashback-towers__fill" style="height: 50%">
                                                <div class="cashback-towers__counter">5%</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cashback-towers__tower cashback-towers__tower--treat">
                                        <div class="cashback-towers__header">
                                            <?= _t('site.ps', 'Cashback from'); ?>
                                            <br>
                                            <b><?= _t('site.ps', 'Treatstock'); ?></b>
                                        </div>
                                        <div class="cashback-towers__body">
                                            <div class="cashback-towers__fill" style="height: 50%">
                                                <div class="cashback-towers__counter">10%</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h3 class="m-t0"><?= _t('site.ps', 'Order types'); ?></h3>
                                <table class="form-table form-table--top-label m-b0">
                                    <tbody>
                                    <tr class="form-table__row">
                                        <td class="p-b15 p-r5"><b><?= _t('site.ps', 'Instant Order'); ?></b></td>
                                        <td class="order-types">
                                            <div class="order-types__bonus">5%</div>
                                        </td>
                                    </tr>
                                    <tr class="form-table__row">
                                        <td class="p-b15 p-r5"><b><?= _t('site.ps', 'Instant Order on Direct Service'); ?></b></td>
                                        <td class="order-types">
                                            <div class="order-types__bonus">5%</div> ×2
                                        </td>
                                    </tr>
                                    <tr class="form-table__row">
                                        <td class="p-b15 p-r5"><b><?= _t('site.ps', 'Quote and Instant Payment'); ?></b></td>
                                        <td class="order-types">
                                            <div class="order-types__bonus">5%</div> ×2
                                        </td>
                                    </tr>
                                    <tr class="form-table__row">
                                        <td class="p-b15 p-r5"><b><?= _t('site.ps', 'Additional Service'); ?></b></td>
                                        <td class="order-types">
                                            <div class="order-types__bonus">5%</div> ×2
                                        </td>
                                    </tr>
                                    <tr class="form-table__row">
                                        <td class="p-b15 p-r5"><b><?= _t('site.ps', 'Product sales'); ?></b></td>
                                        <td class="order-types">
                                            <div class="order-types__bonus">5%</div> ×2
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>