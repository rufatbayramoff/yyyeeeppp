<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}


$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);


Yii::$app->angular
    ->service(['notify', 'router', 'user'])
    ->controller('promo/PromoContactUsController')
    ->controllerParam('type', 'business')
?>

    <style>
        .become-sup-head {
            position: relative;
            padding: 50px 0;
            background: rgba(0, 0, 0, 0.7);
            color: #ffffff;
            overflow: hidden;
        }
        .become-sup-head:after {
            content: '';
            position: absolute;
            z-index: -1;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background: url(https://static.treatstock.com/static/images/covid/covid.jpg?30032020) no-repeat 50% 50% transparent;
            background-size: cover;
        }
        .become-sup-head__title,
        .become-sup-head__subtitle,
        .become-sup-head__btn {
            position: relative;
            z-index: 1;
            margin-bottom: 40px;
        }
        .become-sup-head__title {
            margin-top: 10px;
            font-size: 50px;
            line-height: 50px;
        }
        .become-sup-head__subtitle {
            margin-top: 0;
            font-weight: normal;
        }
        .become-sup-head__btn {
            margin-bottom: 20px;
            padding: 15px 30px;
            font-size: 20px;
        }
        @media (max-width: 767px) {
            .become-sup-head {
                padding: 40px 0;
            }
            .become-sup-head__title {
                font-size: 40px;
                line-height: 40px;
            }
            .become-sup-head__subtitle {
                font-size: 20px;
                line-height: 1.4;
            }
        }

        .become-sup-section {
            position: relative;
            padding: 60px 0;
        }
        @media (max-width: 767px) {
            .become-sup-section {
                padding: 40px 0;
            }
        }

        .become-sup-section--grey {
            background-color: #f6f8fa;
        }

        .become-sup-img {
            width: 100%;
            max-width: 100%;
            margin-bottom: 50px;
            /*border-radius: 5px;*/
            /*box-shadow: 0 10px 30px rgba(0, 54, 153, 0.15);*/
        }

        .become-sup-h2,
        .become-sup-h3 {
            font-weight: 100;
            margin: 0 0 40px;
        }
        .become-sup-h2 {
            font-size: 40px;
            line-height: 40px;
        }
        .become-sup-h3 {
            font-size: 26px;
            line-height: 30px;
        }

        .become-sup-li {
            font-weight: bold;
            font-size: 20px;
            line-height: 1.2;
        }
        .become-sup-li li {
            margin-bottom: 1.5rem;
        }

        .promo-contact .btn-info {
            color: #1E88E5;
            background-color: #ffffff;
            border-color: transparent;
        }
        .promo-contact .btn-info:hover {
            background-color: rgba(255, 255, 255, 0.85);
        }

        .mat-cat-card {
            margin-bottom: 30px;
            border-radius: 5px;
            overflow: hidden;
            background: #ffffff;
        }
        .mat-cat-card:hover {
            transform: none;
            box-shadow: 0 10px 30px rgba(0, 54, 153, 0.15);
        }
        .mat-cat-card__title {
            margin: 10px 10px 5px;
            padding: 0;
            font-size: 14px;
            line-height: 20px;
            font-weight: 700;
            height: 40px;
            display: -webkit-box;
            overflow: hidden;
            text-overflow: ellipsis;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 2;
        }
        .mat-cat-card__btn {
            max-width: calc(100% - 20px);
            margin: 10px 10px 5px;
            padding-left: 0;
            padding-right: 0;
        }
        .product-card__pic {
            height: 120px;
        }
        .become-sup-link {
            margin: 20px 0 0;
            font-size: 20px;
            line-height: 1.2;
        }
        .become-sup-link a {
            border-bottom: 1px dashed;
            text-decoration: none !important;
        }

        .ceo-review {
            max-width: 600px;
            margin: 30px auto;
            padding: 15px 30px;
            box-shadow: 0 10px 30px rgba(0, 54, 153, 0.15);
            border-radius: 5px;
            background: #ffffff;
        }
        .ceo-review__text {
            font-size: 20px;
            line-height: 30px;
        }
        .ceo-review__info {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            margin-top: 20px;
            text-align: right;
        }
        .ceo-review__info-text {
            display: block;
            font-size: 18px;
            padding-right: 20px;
        }
        .ceo-review__ava {
            width: 64px;
            height: 64px;
            border-radius: 50%;
        }

        .warning-text {
            margin: 40px 0 0;
            font-weight: 100;
            font-size: 32px;
            line-height: 1.2;
            text-align: center;
        }

        .ts-embed-userwidget {
            width: 100%;
            max-width: 100%;
            min-height: 900px;
            margin-top: 30px;
        }
    </style>

    <section class="become-sup-head text-center">

        <div class="container">
            <h1 class="become-sup-head__title"><?= _t('site.page', 'Be Ready for Coronavirus — 3D printed Respirators and Valves'); ?></h1>
            <p style="    font-size: 18px;line-height: 1.4;">
                <?= _t('site.page', 'When the world is at risk of a global pandemic it is especially important to protect yourself. 3D printed alternatives to respirators and face masks can balance out a shortage of necessary medical equipment and protective devices. These projects are open to use, so you can make some for yourself and your family members too. Don’t own a 3D printer? You can order necessary parts printed and safely delivered on demand. As our company contribution, all the proceeds* from orders will go to the global fund for treatment of COVID-2019.'); ?>
            </p>
        </div>

    </section>

    <section class="become-sup-section">

        <div class="container">

            <h3 class="m-b30 text-center"><?= _t('site.page', 'Featured 3D Models'); ?></h3>

            <div class="row m-t30">
                <div class="col-xs-6 col-sm-4 col-md-2">
                    <a href="https://www.treatstock.com/my/print-model3d?model3d=M:4113196" class="mat-cat-card" target="_blank">
                        <span class="product-card__pic">
                            <img src="https://static.treatstock.com/static/files/a8/c2/1719382_1026_7923ae1d339d39cfad82e2cbb03fb6fb_720x540.png?date=1587965748"
                                 alt="Mask FMP Imalize security COVID-19">
                        </span>
                        <h3 class="mat-cat-card__title"><?= _t('site.page', 'Mask FMP Imalize security COVID-19'); ?> </h3>
                        <p class="mat-cat-card__btn btn btn-primary btn-block btn-sm">
                            <?= _t('site.page', 'Print'); ?>
                        </p>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-2">
                    <a href="https://www.treatstock.com/my/print-model3d?model3d=M:4113165" class="mat-cat-card" target="_blank">
                        <span class="product-card__pic">
                            <img src="https://static.treatstock.com/static/files/d9/c2/1719353_1026_488594d88735139011d6c6591befeea4_720x540.png?date=1587964929"
                                 alt="Surgical Mask Band for Ear Comfort">
                        </span>
                        <h3 class="mat-cat-card__title"><?= _t('site.page', 'Surgical Mask Band for Ear Comfort'); ?> </h3>
                        <p class="mat-cat-card__btn btn btn-primary btn-block btn-sm">
                            <?= _t('site.page', 'Print'); ?>
                        </p>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-2">
                    <a href="https://www.treatstock.com/my/print-model3d?model3d=M:4113122" class="mat-cat-card" target="_blank">
                        <span class="product-card__pic">
                            <img src="https://static.treatstock.com/static/files/e0/13/1719317_1026_068dcbfe0a5436dd96ce39b9bed007be_720x540.png?date=1587963969"
                                 alt="Prusa Protective Face Shield RC3 US version">
                        </span>
                        <h3 class="mat-cat-card__title"><?= _t('site.page', 'Prusa Protective Face Shield RC3 US version'); ?> </h3>
                        <p class="mat-cat-card__btn btn btn-primary btn-block btn-sm">
                            <?= _t('site.page', 'Print'); ?>
                        </p>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-2">
                    <a href="https://www.treatstock.com/my/print-model3d?model3d=M:4113073" class="mat-cat-card" target="_blank">
                        <span class="product-card__pic">
                            <img src="https://static.treatstock.com/static/files/be/76/1719259_720x540.png?date=1587962977"
                                 alt="COVID 19 MASK v2">
                        </span>
                        <h3 class="mat-cat-card__title"><?= _t('site.page', 'COVID 19 MASK v2'); ?> </h3>
                        <p class="mat-cat-card__btn btn btn-primary btn-block btn-sm">
                            <?= _t('site.page', 'Print'); ?>
                        </p>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-2">
                    <a href="https://www.treatstock.com/my/print-model3d?model3d=M:4113045" class="mat-cat-card" target="_blank">
                        <span class="product-card__pic">
                            <img src="https://static.treatstock.com/static/files/ac/e2/1719221_720x540.jpg?date=1587962333"
                                 alt="Double+Filter+Adapter+for+the+Makers+Modul">
                        </span>
                        <h3 class="mat-cat-card__title"><?= _t('site.page', 'Double+Filter+Adapter+for+the+Makers+Modul'); ?> </h3>
                        <p class="mat-cat-card__btn btn btn-primary btn-block btn-sm">
                            <?= _t('site.page', 'Print'); ?>
                        </p>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-2">
                    <a href="https://www.treatstock.com/my/print-model3d?model3d=M:4112984" class="mat-cat-card" target="_blank">
                        <span class="product-card__pic">
                            <img src="https://static.treatstock.com/static/files/0e/17/1719326_1026_99588549f4c547cbbb554b50da487615_720x540.png?date=1587964263"
                                 alt="Maker Mask V 4 6 3D Printable Respirator S">
                        </span>
                        <h3 class="mat-cat-card__title"><?= _t('site.page', 'Maker Mask V 4 6 3D Printable Respirator S'); ?> </h3>
                        <p class="mat-cat-card__btn btn btn-primary btn-block btn-sm">
                            <?= _t('site.page', 'Print'); ?>
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="become-sup-section become-sup-section--grey" id="additive">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="become-sup-h2">
                        <strong><?= _t('site.page', 'NanoHack 2.0 by Copper3D'); ?></strong>
                    </h2>
                    <p class="m-b30" style="font-size: 18px;line-height: 1.4;">
                        <?= _t('site.page', 'NanoHack 2.0 is a 3D printable alternative to the N95 mask developed by filament manufacturer Copper3D. It requires some minor post-processing and assembly but the end result allows for a good fit and customizable structure.'); ?>
                    </p>
                    <p class="m-b30 p-b30">
                        <a href="https://www.treatstock.com/my/print-model3d?model3d=M:3987832" target="_blank" class="btn btn-primary m-r20" ><?= _t('site.page', 'Print Now'); ?></a>
                        <!--<noindex>-->
                        <a href="https://copper3d.com/stl/g_facemask_nanohack.zip" target="_blank" rel="nofollow" class="btn btn-primary btn-ghost" ><?= _t('site.page', 'View'); ?></a>
                        <!--</noindex>-->
                    </p>

                    <div class="alert alert-danger m-t30 m-b0">
                        <?= _t('site.page', '3D printed respirators aren’t officially certified and thus may not be considered a legitimate medical device'); ?>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <img class="become-sup-img m-b0" src="https://static.treatstock.com/static/images/covid/mask.jpg?01042020" alt="">
                </div>
            </div>
            <!--
            <h3 class="become-sup-h3 text-center m-t30">
                <strong><?= _t('site.page', 'Check out other 3D printable respiratory protection parts'); ?></strong>
            </h3>
            -->
        </div>
    </section>


    <section class="p-t30 p-b30">

        <div class="container">

            <div class="ceo-review">
                <p class="ceo-review__text">
                    “We do everything we  can to support the global fight against COVID-2019. All proceeds* are going to fund to support the treatment of coronavirus patients.”
                </p>
                <div class="ceo-review__info">
                    <div class="ceo-review__info-text">
                        Tim Arno
                        <br>
                        CEO of Treatstock
                    </div>
                    <img class="ceo-review__ava" src="https://static.treatstock.com/static/images/covid/tim-mask.jpg" alt="">
                </div>
            </div>

            <div class="row m-t30 p-t30">
                <div class="col-sm-8 col-sm-offset-2">
                    <p style="font-size: 18px;line-height: 1.4;">
                        <?= _t('site.page', '*Proceeds include our platform fees as well as rewards from vendors that agreed to participate in donations.'); ?>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="become-sup-section">

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="become-sup-h2 m-t0 m-b30 p-b30 text-center">
                        <strong>
                            <?= _t('site.page', 'Do you have you own 3D model? Print it now!'); ?>
                        </strong>
                    </h2>
                    <h3 class="become-sup-h3 m-t0 m-b20 text-center">
                        <strong>
                            <?= _t('site.page', 'How it works'); ?>
                        </strong>
                    </h3>
                    <ol class="become-sup-li">
                        <li><?= _t('site.page', 'Find and download 3D model(s) in .stl format'); ?></li>
                        <li><?= _t('site.page', 'Upload the model on Treatstock to get it printed'); ?></li>
                        <li><?= _t('site.page', 'Place an order step-by-step paying attention to model’s instructions (if any)'); ?></li>
                    </ol>
                </div>
            </div>
        </div>

        <div class="container-fluid" style="width: 100%;max-width: 100%;">
            <iframe class="ts-embed-userwidget" frameborder="0" height="470px" src="https://www.treatstock.com/my/print-model3d/widget" width="100%"></iframe>
        </div>

    </section>

    <section class="p-t30 p-b30">

        <div class="container">
            <div class="row m-t30">
                <div class="col-sm-8 col-sm-offset-2">
                    <p class="warning-text">
                        <strong><?= _t('site.page', 'Warning'); ?> </strong>
                        <br>
                        <?= _t('site.page', 'Custom 3D printable parts do not undergo any clinical testing, the choice of product for any particular intended use is the sole and exclusive responsibility of the customer.'); ?>
                    </p>
                </div>
            </div>

            <div class="row m-t30 p-t30">
                <div class="col-sm-8 col-sm-offset-2">
                    <h3 class="m-b20"><?= _t('site.page', 'Recommendations for choosing and using 3D models of protective parts properly'); ?></h3>
                    <p style="font-size: 18px;line-height: 1.4;">
                        <?= _t('site.page', '3D printed protective masks and parts can be useful and effective but you must keep in mind several details:'); ?>
                    </p>

                    <ol>
                        <li>
                            <?= _t('site.page', 'Make sure you select a model that can deliver a good fit. For example, some 3D printable masks are allowed to be heated and formed to fit better. Others will require additional cushioning.'); ?>
                        </li>
                        <li>
                            <?= _t('site.page', 'If you select a customisable model, double check all the measurements and scale it, so the part fits well.'); ?>
                        </li>
                        <li>
                            <?= _t('site.page', 'In case you are unsure how to scale a 3D file to fit you, try asking a vendor to help you out.'); ?>
                        </li>
                        <li>
                            <?= _t('site.page', 'Be aware of 3D printing accuracy rates and account for clearance.'); ?>
                        </li>
                        <li>
                            <?= _t('site.page', 'Compare dimensions of the model during Step 1 of the ordering process to ensure there are no mistakes in the size. '); ?>
                        </li>
                        <li>
                            <?= _t('site.page', 'Seal the parts to ensure they won’t be any holes and gaps in between the layers. If you are unsure how to do this, ask a vendor to post-process the part for you accordingly.'); ?>
                        </li>
                        <li>
                            <?= _t('site.page', 'Remember, like any surface, plastic can be a home for bacteria. Thus, you need to clean and disinfect the part more frequently as well as change the filter parts regularly (at least every day).'); ?>
                        </li>
                    </ol>

                </div>
            </div>

        </div>
    </section>


    <script>
        <?php $this->beginBlock('js1', false); ?>

        $('a[data-target]').click(function () {
            var scroll_elTarget = $(this).attr('data-target');
            if ($(scroll_elTarget).length != 0) {
                $('html, body').animate({scrollTop: $(scroll_elTarget).offset().top - 105}, 350);
            }
            return false;
        });

        //Init slider for material examples
        var swiperPsPortfolio = new Swiper('.designer-card__ps-portfolio', {
            scrollbar: '.designer-card__ps-portfolio-scrollbar',
            scrollbarHide: true,
            slidesPerView: 'auto',
            grabCursor: true
        });

        //Init slider for .main-page-promo
        var swiperPsLand = new Swiper('.ps-land-reviews__swiper', {
            pagination: '.ps-land-reviews__pagination',
            paginationClickable: true,
            slidesPerView: 1,
            spaceBetween: 30,
            grabCursor: true,
            autoplay: 5000,
            speed: 700,
            loop: true,

            breakpoints: {
                600: {
                    slidesPerView: 1
                }
            }
        });

        <?php $this->endBlock(); ?>
    </script>

<?php $this->registerJs($this->blocks['js1']); ?>
