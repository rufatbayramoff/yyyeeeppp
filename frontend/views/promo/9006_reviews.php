<?php

if (YII_ENV == 'prod') {
    return '';
}

use common\models\Ps;
use frontend\widgets\PsPrintServiceReviewStarsWidget;

use kartik\select2\Select2Asset;
use kartik\select2\ThemeKrajeeAsset;
use yii\bootstrap\ActiveForm;

$ps = Ps::findByPk(1);

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

Select2Asset::register($this);
ThemeKrajeeAsset::register($this);

?>

    <style>
        .star-rating-block,
        .star-rating-count {
            display: inline-block;
        }
        .star-rating-block {
            position: relative;
        }
        .star-rating-block .star-rating {
            position: relative;
            top: -2px;
            display: inline-block;
            margin-right: 5px;
        }
        .star-rating-range {
            padding: 0 5px;
            font-size: 14px;
            font-weight: normal;
        }
        .star-rating-count {
            font-size: 14px;
            color: #909498;
            font-weight: normal;
        }
    </style>

    <div class="over-nav-tabs-header">
        <div class="container container--wide">
            <div class="row">
                <div class="col-sm-8">

                    <h1 class="animated m-b20">
                        <?= _t('site.page', 'Reviews'); ?>
                    </h1>

                    <p class="m-b20">
                        <?= _t('site.page', 'Newcomer to 3D printing? See the latest reviews for 3D printed parts. People share how their custom 3D designs turned out.'); ?>
                    </p>

                </div>
            </div>
        </div>
    </div>

    <div class="nav-tabs__container">
        <div class="container container--wide">
            <div class="nav-filter nav-filter--many nav-filter--ps-cat">
                <form id="w0" class="form-inline"
                      action="/3d-printing-services/Los-Angeles--California--US/technology-SLA?sort=distance"
                      method="post" onsubmit="return false; ">
                    <input type="hidden" name="_frontendCSRF"
                           value="ksKNNvg_OaQoRvikLxuEk_2vZTZn0i2KlMqnLXBFCsPIq_tZgGwO0k5yy-oCK93AyJo6QyCdWdvQldJcIS9-tw==">
                    <input type="checkbox" id="showhideNavFilter">
                    <label class="nav-filter__mobile-label" for="showhideNavFilter">
                        Filters <span class="tsi tsi-down"></span>
                    </label>
                    <div class="nav-filter__mobile-container">

                        <div class="nav-filter__group">
                            <div class="form-group field-category">
                                <label for="">Category</label>
                                <select class="form-control js-category-select input-sm select2-hidden-accessible"
                                        ng-model="categorySlug" ng-init="initCategories()" data-select2-id="1"
                                        tabindex="-1" aria-hidden="true">
                                    <option value="Any" data-select2-id="3">Any</option>
                                    <option value="manufacturing-services" data-select2-id="4">Manufacturing
                                        Services
                                    </option>
                                    <option value="designing-services" data-select2-id="5">3D Design</option>
                                    <option value="cnc-manufacturing" data-select2-id="6">CNC Machining</option>
                                    <option value="injection-molding" data-select2-id="7">Injection Molding</option>
                                    <option value="signage-services" data-select2-id="8">Signage</option>
                                    <option value="electronics-manufacturing-service" data-select2-id="9">
                                        Electronics Manufacturing Service
                                    </option>
                                    <option value="3d-scanning" data-select2-id="10">3D Scanning</option>
                                    <option value="painting" data-select2-id="11">Painting</option>
                                    <option value="other" data-select2-id="12">Other</option>
                                    <option value="cutting" data-select2-id="13">Cutting</option>
                                    <option value="Urethane-Casting" data-select2-id="14">Urethane Casting</option>
                                    <option value="Sheet-Fabrication" data-select2-id="15">Sheet Fabrication
                                    </option>
                                    <option value="Metal-Casting" data-select2-id="16">Metal Casting</option>
                                    <option value="Vacuum-Forming" data-select2-id="17">Vacuum Forming</option>
                                    <option value="Mold-Making" data-select2-id="18">Mold Making</option>
                                    <option value="Metal-Stamping" data-select2-id="19">Metal Stamping</option>
                                    <option value="CNC-turning" data-select2-id="20">CNC Turning</option>
                                </select>
                                <span class="select2 select2-container select2-container--krajee" dir="ltr"
                                      data-select2-id="2" style="width: 100%;"><span
                                            class="selection"><span
                                                class="select2-selection select2-selection--single form-control js-category-select input-sm"
                                                role="combobox" aria-haspopup="true" aria-expanded="false"
                                                tabindex="0" aria-disabled="false"
                                                aria-labelledby="select2-srfb-container"><span
                                                    class="select2-selection__rendered" id="select2-srfb-container"
                                                    role="textbox" aria-readonly="true" title="Any">Any</span><span
                                                    class="select2-selection__arrow" role="presentation"><b
                                                        role="presentation"></b></span></span></span><span
                                            class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                        </div>

                        <div class="nav-filter__group">
                            <div class="form-group field-technology">
                                <label class="" for="technology">Technology</label>
                                <select id="technology"
                                        class="form-control input-sm nav-filter__input select2-hidden-accessible"
                                        name="technology"
                                        onchange="TsCatalogSearch.changeLocation(this.form)"
                                        data-s2-options="s2options_21667806"
                                        data-krajee-select2="select2_51d09493"
                                        style="width: 1px; height: 1px; visibility: hidden;"
                                        data-select2-id="technology"
                                        tabindex="-1"
                                        aria-hidden="true">
                                    <option value="">Any</option>
                                    <option value="0">Any</option>
                                    <option value="FDM">FDM</option>
                                    <option value="MJP">MJP</option>
                                    <option value="SLA" selected="" data-select2-id="23">SLA</option>
                                    <option value="CJP">CJP</option>
                                    <option value="DLP">DLP</option>
                                    <option value="MJF">MJF</option>
                                    <option value="DUP">DUP</option>
                                </select>
                                <span class="select2 select2-container select2-container--krajee input-sm"
                                      dir="ltr" data-select2-id="22" style="width: 100%;"><span
                                            class="selection"><span
                                                class="select2-selection select2-selection--single" role="combobox"
                                                aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                aria-disabled="false"
                                                aria-labelledby="select2-technology-container"><span
                                                    class="select2-selection__rendered"
                                                    id="select2-technology-container" role="textbox"
                                                    aria-readonly="true" title="SLA">SLA</span><span
                                                    class="select2-selection__arrow" role="presentation"><b
                                                        role="presentation"></b></span></span></span><span
                                            class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                        </div>

                        <div class="nav-filter__group">
                            <div class="form-group field-material">
                                <label class="" for="material">Material</label>
                                <span id="parent-s2-togall-material"
                                      style="display:none"><span
                                            id="s2-togall-material" class="s2-togall-button s2-togall-select"><span
                                                class="s2-select-label"><i
                                                    class="glyphicon glyphicon-unchecked"></i>Select all</span><span
                                                class="s2-unselect-label"><i class="glyphicon glyphicon-check"></i>Unselect all</span></span></span><input
                                        type="hidden" name="material" value="">
                                <select id="material"
                                        class="form-control input-sm nav-filter__input select2-hidden-accessible"
                                        name="material[]" multiple=""
                                        size="4"
                                        onchange="TsCatalogSearch.changeLocation(this.form)"
                                        data-s2-options="s2options_8cca79c2"
                                        data-krajee-select2="select2_b0512bd7"
                                        style="width: 1px; height: 1px; visibility: hidden;"
                                        data-select2-id="material"
                                        tabindex="-1"
                                        aria-hidden="true">
                                    <option value="abs-like-resin">ABS-like Resin</option>
                                    <option value="accura-clearvue">Accura ClearVue</option>
                                    <option value="accura-e-stone">Accura e-Stone</option>
                                    <option value="alumina">Alumina</option>
                                    <option value="alumina-zirconia">AluminaZirconia</option>
                                    <option value="aluminium-nitride">AluminiumNitride</option>
                                    <option value="biomed-resin">BioMed-Resin</option>
                                    <option value="castable-resin">Castable Resin</option>
                                    <option value="ceramic-resin">Ceramic Resin</option>
                                    <option value="clear-biomed-resin">ClearBiomedResin</option>
                                    <option value="cordierite">Cordierite</option>
                                    <option value="dental-lt-resin">Dental LT Resin</option>
                                    <option value="dental-sg-resin">Dental SG Resin</option>
                                    <option value="draft-resin">Draft Resin</option>
                                    <option value="durable-pp-like-resin">Durable (PP-like) Resin</option>
                                    <option value="elastic-resin-soft-and-flexible">Elastic Resin (Soft and
                                        flexible)
                                    </option>
                                    <option value="flexible-resin">Flexible Resin</option>
                                    <option value="fused-silica">FusedSilica</option>
                                    <option value="glass-reinforced-resin">Glass-reinforced Resin</option>
                                    <option value="grey-pro-resin">Grey Pro Resin</option>
                                    <option value="high-temp-resin">High Temp Resin</option>
                                    <option value="hydroxyapatite">Hydroxyapatite</option>
                                    <option value="solidscape-3z-model">Jewelry Wax (Solidscape 3Z Model)</option>
                                    <option value="nanoceramic-resin">Nanoceramic Resin</option>
                                    <option value="resin">Resin</option>
                                    <option value="rigid-opaque-resin">Rigid opaque Resin</option>
                                    <option value="rubber-like-resin">Rubber-like Resin</option>
                                    <option value="silicon-nitride">SiliconNitride</option>
                                    <option value="silicore">Silicore</option>
                                    <option value="somos-evolve-128">Somos EvoLVe 128</option>
                                    <option value="somos-next">Somos NeXt</option>
                                    <option value="somos-watershed-xc-11122">Somos WaterShed XC 11122</option>
                                    <option value="standard-acrylic-resin">Standard acrylic Resin</option>
                                    <option value="tough-2000-resin">Tough2000</option>
                                    <option value="tough-resin">Tough Resin</option>
                                    <option value="tricalcium-phosphate">TricalciumPhosphate</option>
                                    <option value="visijet-ftx-cast">VisiJet FTX Cast</option>
                                    <option value="visijet-ftx">VisiJet FTX resin</option>
                                    <option value="visijet-sl-clear">VisiJet SL Clear</option>
                                    <option value="visijet-sl-e-stone">VisiJet SL e-Stone</option>
                                    <option value="waterclear-resin">WaterClear Resin</option>
                                    <option value="wax-like-resin">Wax-like Resin</option>
                                    <option value="zirconia-3y">Zirconia3Y</option>
                                    <option value="zirconia-8y">Zirconia8Y</option>
                                </select>
                                <span class="select2 select2-container select2-container--krajee input-sm"
                                      dir="ltr" data-select2-id="26" style="width: 1px;"><span
                                            class="selection"><span
                                                class="select2-selection select2-selection--multiple"
                                                role="combobox" aria-haspopup="true" aria-expanded="false"
                                                tabindex="-1" aria-disabled="false"><ul
                                                    class="select2-selection__rendered"><li
                                                        class="select2-search select2-search--inline"><input
                                                            class="select2-search__field" type="search" tabindex="0"
                                                            autocomplete="off" autocorrect="off"
                                                            autocapitalize="none" spellcheck="false"
                                                            role="searchbox" aria-autocomplete="list"
                                                            placeholder="Any"
                                                            style="width: 158px;"></li></ul></span></span><span
                                            class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="reviewList" class="container container--wide">
        <div class="row reviews-cat-list">
            <div id="w1" class="list-view">
                <div class="summary">Showing <b>1-20</b> of <b>23,281</b> items.</div>
                <div class="col-sm-6 col-md-4 reviews-cat-list__cell">
                    <div class="reviews-cat">

                            <div class="reviews-cat__service">
                                <a href="https://ts.h5.tsdev.work/c/shellback-workshop" target="_blank"
                                   class="reviews-cat__service-data" title="Shellback Workshop">
                                    <img src="https://static.treatstock.com/static/user/182fe53e6c20be432aa0277667511ae2/ps_logo_circle_1622138427.png"
                                         alt="Shellback Workshop" title="Shellback Workshop">
                                    <div>Shellback Workshop</div>
                                </a>
                                <div class="reviews-cat__materials">
                                    <span class="text-muted m-r10">Material &amp; Color</span>
                                    <strong>
                                        <span class="m-r10">Resin</span>
                                        <div title="Resin" class="material-item">
                                            <div class="material-item__color"
                                                 style="background-color: rgb(190,190,190)"></div>
                                            <div class="material-item__label">Gray</div>
                                        </div>
                                    </strong>
                                </div>
                            </div>
                            <div class="reviews-cat__body" id="user-review-26702">
                                <div class="reviews-cat__rate">
                                    <div id="star-rating-block-39340" class="star-rating-block">
                                        <div class="star-rating rating-xs rating-disabled">
                                            <div class="rating-container tsi" data-content="">
                                                <div class="rating-stars" data-content=""
                                                     style="width: 100%;"></div>
                                                <input value="5" type="text" class="star-rating form-control hide"
                                                       data-min="0" data-max="5" data-step="0.1" data-size="xs"
                                                       data-symbol="" data-glyphicon="false" data-rating-class="tsi"
                                                       data-readonly="true"></div>
                                        </div>

                                    </div>
                                    <button type="button" class="btn btn-info btn-xs star-rating__dropdown collapsed"
                                            data-toggle="collapse" data-target="#rating-26702" aria-expanded="false">
                                        <span class="tsi tsi-down"></span>
                                    </button>

                                    <div id="rating-26702" class="star-rating__dropdown-body collapse">
                                        <table class="form-table">

                                            <tbody>
                                            <tr class="form-table__row">
                                                <td class="form-table__label">
                                                    Speed
                                                </td>
                                                <td class="form-table__data">
                                                    <div id="star-rating-block-3266" class="star-rating-block">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi" data-content="">
                                                                <div class="rating-stars" data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5.00" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5" data-step="0.1"
                                                                       data-size="xs" data-symbol=""
                                                                       data-glyphicon="false" data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>


                                            <tr class="form-table__row">
                                                <td class="form-table__label">
                                                    Quality
                                                </td>
                                                <td class="form-table__data">
                                                    <div id="star-rating-block-96272" class="star-rating-block">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi" data-content="">
                                                                <div class="rating-stars" data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5.00" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5" data-step="0.1"
                                                                       data-size="xs" data-symbol=""
                                                                       data-glyphicon="false" data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>


                                            <tr class="form-table__row">
                                                <td class="form-table__label">
                                                    Communication
                                                </td>
                                                <td class="form-table__data">
                                                    <div id="star-rating-block-27743" class="star-rating-block">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi" data-content="">
                                                                <div class="rating-stars" data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5.00" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5" data-step="0.1"
                                                                       data-size="xs" data-symbol=""
                                                                       data-glyphicon="false" data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="ps-profile-user-review__data">
                                    <div id="w2"
                                         class="ps-profile-user-review__user-models swiper-container swiper-container-horizontal"
                                         style="cursor: grab;">
                                        <div class="swiper-wrapper"><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide swiper-slide-active"
                                                    href="https://ts.h5.tsdev.work/static/files/0a/b9/3031726_61271_c79f0ecade30fb0be55cf5fef8fe0be9_1200x1200.jpg?date=1626225054"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/29/f1/3292866_87060_243cca673c4f847a10bb7b79cffe94cb_120x120.jpg?date=1634219862"
                                                        alt="Review image #3031726"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide swiper-slide-next"
                                                    href="https://ts.h5.tsdev.work/static/files/9d/8b/3031727_61271_857d381b25e92668df3ba30bdaeced85_1200x1200.jpg?date=1626225054"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/13/f7/3292864_87060_91a560ee9c52c84a54a8440ed8a73ed2_120x120.jpg?date=1634219714"
                                                        alt="Review image #3031727"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide"
                                                    href="https://ts.h5.tsdev.work/static/files/94/97/3031725_61271_eda05c806a28cc52145128fe731fede3_1200x1200.jpg?date=1626225054"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/c6/a5/3289062_76335_40d087f9f38e6c867fbdbba08ede88bc_120x120.jpg?date=1634096281"
                                                        alt="Review image #3031725"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide"
                                                    href="https://ts.h5.tsdev.work/static/files/a5/c5/3031724_61271_518bae8dcc46279730ae1f62c2f4e67f_1200x1200.jpg?date=1626225053"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/d3/a5/3289061_76335_e73989c7e9d1002c56735507885c672a_120x120.jpg?date=1634096281"
                                                        alt="Review image #3031724"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide"
                                                    href="https://ts.h5.tsdev.work/static/files/d3/e3/3031723_61271_cdfb536643f84394008d8030bef7e591_1200x1200.jpg?date=1626225051"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/1f/06/3289063_76335_749ba8c3793a54091c513e60f9098463_120x120.jpg?date=1634096281"
                                                        alt="Review image #3031723"></a></div>
                                        <div class="ps-profile-user-review__user-models-scrollbar swiper-scrollbar"
                                             style="opacity: 0;">
                                            <div class="swiper-scrollbar-drag" style="width: 181.24px;"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="reviews-cat__user">
                                <div class="ps-profile-user-review__user-info">
                                    <div class="ps-profile-user-review__user-avatar">
                                        <img src="https://secure.gravatar.com/avatar/b1af51a35ae2a96c5a0f024f7acb9842?r=g&s=64&d=retro"
                                             alt=""></div>
                                    <div class="ps-profile-user-review__user-name">
                                        CharityЧ
                                    </div>
                                </div>
                                <div class="ps-profile-user-review__user-date">
                                    Jul 20, 2021
                                </div>
                            </div>

                    </div>
                </div>


                <div class="col-sm-6 col-md-4 reviews-cat-list__cell">
                    <div class="reviews-cat">

                            <div class="reviews-cat__service">
                                <a href="https://ts.h5.tsdev.work/c/mourne-3d-solutions" target="_blank"
                                   class="reviews-cat__service-data" title="mourne 3d solutions">
                                    <img src="https://static.treatstock.com/static/user/9dec88b3772c35708f47db386b2f487e/ps_logo_circle_1559697273.png"
                                         alt="mourne 3d solutions" title="mourne 3d solutions">
                                    <div>mourne 3d solutions</div>
                                </a>
                                <div class="reviews-cat__materials">
                                    <span class="text-muted m-r10">Material &amp; Color</span>
                                    <strong>
                                        <span class="m-r10">PLA</span>
                                        <div title="PLA" class="material-item">
                                            <div class="material-item__color"
                                                 style="background-color: rgb(0,204,51)"></div>
                                            <div class="material-item__label">Green</div>
                                        </div>
                                    </strong>
                                </div>
                            </div>
                            <div class="reviews-cat__body" id="user-review-26692">
                                <div class="reviews-cat__rate">
                                    <div id="star-rating-block-2640" class="star-rating-block">
                                        <div class="star-rating rating-xs rating-disabled">
                                            <div class="rating-container tsi" data-content="">
                                                <div class="rating-stars" data-content=""
                                                     style="width: 100%;"></div>
                                                <input value="5" type="text" class="star-rating form-control hide"
                                                       data-min="0" data-max="5" data-step="0.1" data-size="xs"
                                                       data-symbol="" data-glyphicon="false" data-rating-class="tsi"
                                                       data-readonly="true"></div>
                                        </div>

                                    </div>
                                    <button type="button" class="btn btn-info btn-xs star-rating__dropdown collapsed"
                                            data-toggle="collapse" data-target="#rating-26692" aria-expanded="false">
                                        <span class="tsi tsi-down"></span>
                                    </button>

                                    <div id="rating-26692" class="star-rating__dropdown-body collapse">
                                        <table class="form-table">

                                            <tbody>
                                            <tr class="form-table__row">
                                                <td class="form-table__label">
                                                    Speed
                                                </td>
                                                <td class="form-table__data">
                                                    <div id="star-rating-block-34322" class="star-rating-block">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi" data-content="">
                                                                <div class="rating-stars" data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5.00" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5" data-step="0.1"
                                                                       data-size="xs" data-symbol=""
                                                                       data-glyphicon="false" data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>


                                            <tr class="form-table__row">
                                                <td class="form-table__label">
                                                    Quality
                                                </td>
                                                <td class="form-table__data">
                                                    <div id="star-rating-block-38273" class="star-rating-block">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi" data-content="">
                                                                <div class="rating-stars" data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5.00" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5" data-step="0.1"
                                                                       data-size="xs" data-symbol=""
                                                                       data-glyphicon="false" data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>


                                            <tr class="form-table__row">
                                                <td class="form-table__label">
                                                    Communication
                                                </td>
                                                <td class="form-table__data">
                                                    <div id="star-rating-block-3561" class="star-rating-block">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi" data-content="">
                                                                <div class="rating-stars" data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5.00" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5" data-step="0.1"
                                                                       data-size="xs" data-symbol=""
                                                                       data-glyphicon="false" data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="ps-profile-user-review__data">
                                    <div id="w12"
                                         class="ps-profile-user-review__user-models swiper-container swiper-container-horizontal"
                                         style="cursor: grab;">
                                        <div class="swiper-wrapper"><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide swiper-slide-active"
                                                    href="https://ts.h5.tsdev.work/static/files/0a/b9/3031726_61271_c79f0ecade30fb0be55cf5fef8fe0be9_1200x1200.jpg?date=1626225054"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/29/f1/3292866_87060_243cca673c4f847a10bb7b79cffe94cb_120x120.jpg?date=1634219862"
                                                        alt="Review image #3031726"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide swiper-slide-next"
                                                    href="https://ts.h5.tsdev.work/static/files/9d/8b/3031727_61271_857d381b25e92668df3ba30bdaeced85_1200x1200.jpg?date=1626225054"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/13/f7/3292864_87060_91a560ee9c52c84a54a8440ed8a73ed2_120x120.jpg?date=1634219714"
                                                        alt="Review image #3031727"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide"
                                                    href="https://ts.h5.tsdev.work/static/files/94/97/3031725_61271_eda05c806a28cc52145128fe731fede3_1200x1200.jpg?date=1626225054"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/c6/a5/3289062_76335_40d087f9f38e6c867fbdbba08ede88bc_120x120.jpg?date=1634096281"
                                                        alt="Review image #3031725"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide"
                                                    href="https://ts.h5.tsdev.work/static/files/a5/c5/3031724_61271_518bae8dcc46279730ae1f62c2f4e67f_1200x1200.jpg?date=1626225053"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/d3/a5/3289061_76335_e73989c7e9d1002c56735507885c672a_120x120.jpg?date=1634096281"
                                                        alt="Review image #3031724"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide"
                                                    href="https://ts.h5.tsdev.work/static/files/d3/e3/3031723_61271_cdfb536643f84394008d8030bef7e591_1200x1200.jpg?date=1626225051"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/1f/06/3289063_76335_749ba8c3793a54091c513e60f9098463_120x120.jpg?date=1634096281"
                                                        alt="Review image #3031723"></a></div>
                                        <div class="ps-profile-user-review__user-models-scrollbar swiper-scrollbar"
                                             style="display: none; opacity: 0;">
                                            <div class="swiper-scrollbar-drag" style="width: 0px;"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="ps-profile-user-review__text">
                                    Mourne 3d solutions is a print service from Northern Ireland, GB. They work with PLA
                                    in different colors. Our item was printed in Green PLA, and the result is quite
                                    impressive. The bridges and holes look very well, the speed of printing was very
                                    high. The vendor was on the line, they have replied quickly.
                                </div>

                            </div>

                            <div class="reviews-cat__user">
                                <div class="ps-profile-user-review__user-info">
                                    <a href="https://ts.h5.tsdev.work/u/support"
                                       class="ps-profile-user-review__user-avatar">
                                        <img src="https://static.treatstock.com/static/user/c4ca4238a0b923820dcc509a6f75849b/avatar_1484318770_64x64.png"
                                             alt=""> </a>
                                    <a href="https://ts.h5.tsdev.work/u/support"
                                       class="ps-profile-user-review__user-name">
                                        Treatstock </a>
                                </div>
                                <div class="ps-profile-user-review__user-date">
                                    Jul 20, 2021
                                </div>
                            </div>

                    </div>
                </div>


                <div class="col-sm-6 col-md-4 reviews-cat-list__cell">
                    <div class="reviews-cat">

                            <div class="reviews-cat__service">
                                <a href="https://ts.h5.tsdev.work/c/rhino-prints" target="_blank"
                                   class="reviews-cat__service-data" title="Rhino Prints">
                                    <img src="https://static.treatstock.com/static/user/421661a0bac457f0449d84cb360660e1/ps_logo_circle_1625630450.png"
                                         alt="Rhino Prints" title="Rhino Prints">
                                    <div>Rhino Prints</div>
                                </a>
                                <div class="reviews-cat__materials">
                                    <span class="text-muted m-r10">Material &amp; Color</span>
                                    <strong>
                                        <span class="m-r10">PLA</span>
                                        <div title="PLA" class="material-item">
                                            <div class="material-item__color"
                                                 style="background-color: rgb(230,232,230)"></div>
                                            <div class="material-item__label">Silver</div>
                                        </div>
                                    </strong>
                                </div>
                            </div>
                            <div class="reviews-cat__body" id="user-review-26683">
                                <div class="reviews-cat__rate">
                                    <div id="star-rating-block-21007" class="star-rating-block">
                                        <div class="star-rating rating-xs rating-disabled">
                                            <div class="rating-container tsi" data-content="">
                                                <div class="rating-stars" data-content=""
                                                     style="width: 100%;"></div>
                                                <input value="5" type="text" class="star-rating form-control hide"
                                                       data-min="0" data-max="5" data-step="0.1" data-size="xs"
                                                       data-symbol="" data-glyphicon="false" data-rating-class="tsi"
                                                       data-readonly="true"></div>
                                        </div>

                                    </div>
                                    <button type="button" class="btn btn-info btn-xs star-rating__dropdown collapsed"
                                            data-toggle="collapse" data-target="#rating-26683" aria-expanded="false">
                                        <span class="tsi tsi-down"></span>
                                    </button>

                                    <div id="rating-26683" class="star-rating__dropdown-body collapse">
                                        <table class="form-table">

                                            <tbody>
                                            <tr class="form-table__row">
                                                <td class="form-table__label">
                                                    Speed
                                                </td>
                                                <td class="form-table__data">
                                                    <div id="star-rating-block-2969" class="star-rating-block">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi" data-content="">
                                                                <div class="rating-stars" data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5.00" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5" data-step="0.1"
                                                                       data-size="xs" data-symbol=""
                                                                       data-glyphicon="false" data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>


                                            <tr class="form-table__row">
                                                <td class="form-table__label">
                                                    Quality
                                                </td>
                                                <td class="form-table__data">
                                                    <div id="star-rating-block-44494" class="star-rating-block">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi" data-content="">
                                                                <div class="rating-stars" data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5.00" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5" data-step="0.1"
                                                                       data-size="xs" data-symbol=""
                                                                       data-glyphicon="false" data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>


                                            <tr class="form-table__row">
                                                <td class="form-table__label">
                                                    Communication
                                                </td>
                                                <td class="form-table__data">
                                                    <div id="star-rating-block-12579" class="star-rating-block">
                                                        <div class="star-rating rating-xs rating-disabled">
                                                            <div class="rating-container tsi" data-content="">
                                                                <div class="rating-stars" data-content=""
                                                                     style="width: 100%;"></div>
                                                                <input value="5.00" type="text"
                                                                       class="star-rating form-control hide"
                                                                       data-min="0" data-max="5" data-step="0.1"
                                                                       data-size="xs" data-symbol=""
                                                                       data-glyphicon="false" data-rating-class="tsi"
                                                                       data-readonly="true"></div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="ps-profile-user-review__data">
                                    <div id="w21"
                                         class="ps-profile-user-review__user-models swiper-container swiper-container-horizontal"
                                         style="cursor: grab;">
                                        <div class="swiper-wrapper"><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide swiper-slide-active"
                                                    href="https://ts.h5.tsdev.work/static/files/0a/b9/3031726_61271_c79f0ecade30fb0be55cf5fef8fe0be9_1200x1200.jpg?date=1626225054"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/29/f1/3292866_87060_243cca673c4f847a10bb7b79cffe94cb_120x120.jpg?date=1634219862"
                                                        alt="Review image #3031726"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide swiper-slide-next"
                                                    href="https://ts.h5.tsdev.work/static/files/9d/8b/3031727_61271_857d381b25e92668df3ba30bdaeced85_1200x1200.jpg?date=1626225054"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/13/f7/3292864_87060_91a560ee9c52c84a54a8440ed8a73ed2_120x120.jpg?date=1634219714"
                                                        alt="Review image #3031727"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide"
                                                    href="https://ts.h5.tsdev.work/static/files/94/97/3031725_61271_eda05c806a28cc52145128fe731fede3_1200x1200.jpg?date=1626225054"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/c6/a5/3289062_76335_40d087f9f38e6c867fbdbba08ede88bc_120x120.jpg?date=1634096281"
                                                        alt="Review image #3031725"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide"
                                                    href="https://ts.h5.tsdev.work/static/files/a5/c5/3031724_61271_518bae8dcc46279730ae1f62c2f4e67f_1200x1200.jpg?date=1626225053"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/d3/a5/3289061_76335_e73989c7e9d1002c56735507885c672a_120x120.jpg?date=1634096281"
                                                        alt="Review image #3031724"></a><a
                                                    class="ps-profile-user-review__user-models-pic swiper-slide"
                                                    href="https://ts.h5.tsdev.work/static/files/d3/e3/3031723_61271_cdfb536643f84394008d8030bef7e591_1200x1200.jpg?date=1626225051"
                                                    data-lightbox="w2"><img
                                                        src="https://static.treatstock.com/static/files/1f/06/3289063_76335_749ba8c3793a54091c513e60f9098463_120x120.jpg?date=1634096281"
                                                        alt="Review image #3031723"></a></div>
                                        <div class="ps-profile-user-review__user-models-scrollbar swiper-scrollbar"
                                             style="opacity: 0;">
                                            <div class="swiper-scrollbar-drag" style="width: 226.55px;"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="ps-profile-user-review__text">
                                    Rhino Prints is a 3D company in northern Indiana. The item was printed in Silver PLA
                                    and was done in a very short time. As well as with PLA the company also works in
                                    PETG. The size, the quality of the printed piece are as good as they were expected.
                                    Little parts were done very well, there are some insignificant issues which are not
                                    affecting the quality and can be removed by the customer's wish.
                                </div>

                            </div>

                            <div class="reviews-cat__user">
                                <div class="ps-profile-user-review__user-info">
                                    <a href="https://ts.h5.tsdev.work/u/support"
                                       class="ps-profile-user-review__user-avatar">
                                        <img src="https://static.treatstock.com/static/user/c4ca4238a0b923820dcc509a6f75849b/avatar_1484318770_64x64.png"
                                             alt=""> </a>
                                    <a href="https://ts.h5.tsdev.work/u/support"
                                       class="ps-profile-user-review__user-name">
                                        Treatstock </a>
                                </div>
                                <div class="ps-profile-user-review__user-date">
                                    Jul 20, 2021
                                </div>
                            </div>

                    </div>
                </div>

            </div>
        </div>
    </div>




    <script>
        <?php $this->beginBlock('js1', false); ?>

        function initPrints() {
            //Init slider for .customer-reviews__slider
            var swiperPrints = new Swiper('.customer-reviews__slider', {
                scrollbar: '.customer-reviews__scrollbar',
                scrollbarHide: true,
                slidesPerView: 'auto',
                grabCursor: true
            });
        }

        initPrints();
        $(window).resize(function () {initPrints()});

        function initPrints() {
            //Init slider for .customer-reviews__slider
            var swiperPrints = new Swiper('.ps-profile-user-review__user-models', {
                scrollbar: '.ps-profile-user-review__user-models-scrollbar',
                scrollbarHide: true,
                slidesPerView: 'auto',
                grabCursor: true
            });
        }

        initPrints();
        $(window).resize(function () {initPrints()});


        <?php $this->endBlock(); ?>
    </script>
<?php $this->registerJs($this->blocks['js1']); ?>