<?php

use common\models\Ps;

$ps = Ps::findByPk(1);

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->controller('store/common-models')
    ->controller('preorder/preorder-models')
    ->controller('preorder/preorder-service')
    ->controller([
        'ps/PsCatalogController',
        'product/productForm',
        'product/productModels',
    ])->controllerParams(['z' => 'y']);

echo $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-modal.php');

$this->title = "Suppliers";

?>


    <div class="over-nav-tabs-header">
        <div class="container"><h1><?= _t('site.ps', 'Suppliers'); ?></h1></div>
    </div>


    <div class="container" ng-controller="PsCatalogController">

        <div class="responsive-container-list responsive-container-list--3">

            <div class="responsive-container">
                <div class="designer-card designer-card--ps-cat">

                    <div class="designer-card__userinfo">
                        <div class="designer-card__avatar">
                            <img src="https://static.treatstock.com/static/images/common/suppliers/1-logo.jpg" alt="" align="left">
                        </div>

                        <h3 class="designer-card__username">
                            American Precision Gear Co., Inc.
                        </h3>
                    </div>

                    <div class="designer-card__ps-pics">

                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/1-1b.jpg"
                                        data-lightbox="sup1">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/1-1b.jpg" alt="">
                                </a>
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/1-2b.jpg"
                                        data-lightbox="sup1">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/1-2b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/1-3b.jpg"
                                   data-lightbox="sup1">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/1-3b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/1-4b.jpg"
                                   data-lightbox="sup1">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/1-4b.jpg" alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>

                    </div>

                    <div class="designer-card__about">
                        I am experienced in 3D modeling and design with my specialty being cosplay and items from<a class="tsrm-toggle" href="#" style="display:inline">...Show
                            more</a>
                        <span class="tsrm-content hide"> Valley, California. Serving the needs of a variety of traditional industrial and emerging high-tech industries that require precision gears including medical device gears, military component gears, instrumentation gears, aerospace gears, semi-conductor gears, and robotics gears.</span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="designer-card__btn-block">
                                <button type="button" class="btn btn-danger ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                                        loader-click="openCreatePreorder(1518)">
                                    <?= _t('site.ps', 'Get a Quote'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="responsive-container">
                <div class="designer-card designer-card--ps-cat">

                    <div class="designer-card__userinfo">
                        <div class="designer-card__avatar">
                            <img src="https://static.treatstock.com/static/images/common/suppliers/2-logo.jpg" alt="" align="left">
                        </div>

                        <h3 class="designer-card__username">
                            American Gear, Inc.
                        </h3>
                    </div>

                    <div class="designer-card__ps-pics">

                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/2-1b.jpg"
                                        data-lightbox="sup2">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/2-1b.jpg" alt="">
                                </a>
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/2-2b.jpg"
                                        data-lightbox="sup2">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/2-2b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/2-3b.jpg"
                                   data-lightbox="sup2">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/2-3b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/2-4b.jpg"
                                   data-lightbox="sup2">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/2-4b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/2-5b.jpg"
                                   data-lightbox="sup2">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/2-5b.jpg" alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>

                    </div>

                    <div class="designer-card__about">
                        Consistency of quality and 25 years of expertise and experience produce AMERICAN GEARS<a class="tsrm-toggle" href="#" style="display:inline">...Show
                            more</a>
                        <span class="tsrm-content hide"> fine to medium pitch custom gears. We meet the specifications of industry and military standards for a variety of products that are within inches of your finger tips everywhere.</span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="designer-card__btn-block">
                                <button type="button" class="btn btn-danger ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                                        loader-click="openCreatePreorder(1518)">
                                    <?= _t('site.ps', 'Get a Quote'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="responsive-container">
                <div class="designer-card designer-card--ps-cat">

                    <div class="designer-card__userinfo">
                        <div class="designer-card__avatar">
                            <img src="https://static.treatstock.com/static/images/common/suppliers/3-logo.jpg" alt="" align="left">
                        </div>

                        <h3 class="designer-card__username">
                            Gear Motions, Inc
                        </h3>
                    </div>

                    <div class="designer-card__ps-pics">

                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/3-1b.jpg"
                                        data-lightbox="sup3">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/3-1b.jpg" alt="">
                                </a>
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/3-2b.jpg"
                                        data-lightbox="sup3">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/3-2b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/3-3b.jpg"
                                   data-lightbox="sup3">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/3-3b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/3-4b.jpg"
                                   data-lightbox="sup3">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/3-4b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/3-5b.jpg"
                                   data-lightbox="sup3">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/3-5b.jpg" alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>

                    </div>

                    <div class="designer-card__about">
                        Gear Motions is a leading gear manufacturer specializing in supplying custom cut<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                        <span class="tsrm-content hide"> and ground gears for OEMs all around the world. Whether we’re manufacturing your gear complete from start to finish or working from your blanks, our wide range of precision gear manufacturing capabilities and services deliver quality gears to meet almost any application – and the most demanding specifications.</span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="designer-card__btn-block">
                                <button type="button" class="btn btn-danger ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                                        loader-click="openCreatePreorder(1518)">
                                    <?= _t('site.ps', 'Get a Quote'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="responsive-container">
                <div class="designer-card designer-card--ps-cat">

                    <div class="designer-card__userinfo">
                        <div class="designer-card__avatar">
                            <img src="https://static.treatstock.com/static/images/common/suppliers/4-logo.jpg" alt="" align="left">
                        </div>

                        <h3 class="designer-card__username">
                            Atlanta Gear Works
                        </h3>
                    </div>

                    <div class="designer-card__ps-pics">

                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/4-1b.jpg"
                                        data-lightbox="sup4">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/4-1b.jpg" alt="">
                                </a>
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/4-2b.jpg"
                                        data-lightbox="sup4">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/4-2b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/4-3b.jpg"
                                   data-lightbox="sup4">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/4-3b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/4-4b.jpg"
                                   data-lightbox="sup4">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/4-4b.jpg" alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>

                    </div>

                    <div class="designer-card__about">
                        Established in 1988, Atlanta Gear Works began repairing supply line shaft power<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                        <span class="tsrm-content hide">  transmission equipment for the paper industry. Since, we have expanded to service a variety of industries.</span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="designer-card__btn-block">
                                <button type="button" class="btn btn-danger ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                                        loader-click="openCreatePreorder(1518)">
                                    <?= _t('site.ps', 'Get a Quote'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="responsive-container">
                <div class="designer-card designer-card--ps-cat">

                    <div class="designer-card__userinfo">
                        <div class="designer-card__avatar">
                            <img src="https://static.treatstock.com/static/images/common/suppliers/5-logo.jpg" alt="" align="left">
                        </div>

                        <h3 class="designer-card__username">
                            PSM Industries, Inc.
                        </h3>
                    </div>

                    <div class="designer-card__ps-pics">

                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/5-1b.jpg"
                                        data-lightbox="sup5">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/5-1b.jpg" alt="">
                                </a>
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/5-2b.jpg"
                                        data-lightbox="sup5">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/5-2b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/5-3b.jpg"
                                   data-lightbox="sup5">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/5-3b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/5-4b.jpg"
                                   data-lightbox="sup5">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/5-4b.jpg" alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>

                    </div>

                    <div class="designer-card__about">
                        Ferro-TiC® SBC is a specialized manufacturer of parts and components using Steel<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                        <span class="tsrm-content hide"> Bonded Titanium Carbide (TiC). Steel Bonded TiC is renowned for its durability, resistance to heat, corrosion and hostile environments, and its light weight.</span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="designer-card__btn-block">
                                <button type="button" class="btn btn-danger ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                                        loader-click="openCreatePreorder(1518)">
                                    <?= _t('site.ps', 'Get a Quote'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="responsive-container">
                <div class="designer-card designer-card--ps-cat">

                    <div class="designer-card__userinfo">
                        <div class="designer-card__avatar">
                            <img src="https://static.treatstock.com/static/images/common/suppliers/6-logo.jpg" alt="" align="left">
                        </div>

                        <h3 class="designer-card__username">
                            Reliance Gear Corporation
                        </h3>
                    </div>

                    <div class="designer-card__ps-pics">

                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/6-1b.jpg"
                                        data-lightbox="sup6">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/6-1b.jpg" alt="">
                                </a>
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/6-2b.jpg"
                                        data-lightbox="sup6">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/6-2b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/6-3b.jpg"
                                   data-lightbox="sup6">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/6-3b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/6-4b.jpg"
                                   data-lightbox="sup6">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/6-4b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/6-5b.jpg"
                                   data-lightbox="sup6">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/6-5b.jpg" alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>

                    </div>

                    <div class="designer-card__about">
                        Welcome to Reliance Gear Corporation, a manufacturer of custom and precision<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                        <span class="tsrm-content hide"> gearing since 1965, a subsidiary of Ashot Ashkelon Industries. We supply gearing for many industries such as Aerospace, Defense, Agriculture, Mining, and Construction. Building upon our rich heritage, the commitment to supplying the best possible gears still lives on.</span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="designer-card__btn-block">
                                <button type="button" class="btn btn-danger ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                                        loader-click="openCreatePreorder(1518)">
                                    <?= _t('site.ps', 'Get a Quote'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="responsive-container">
                <div class="designer-card designer-card--ps-cat">

                    <div class="designer-card__userinfo">
                        <div class="designer-card__avatar">
                            <img src="https://static.treatstock.com/static/images/common/suppliers/7-logo.jpg" alt="" align="left">
                        </div>

                        <h3 class="designer-card__username">
                            HM Manufacturing
                        </h3>
                    </div>

                    <div class="designer-card__ps-pics">

                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/7-1b.jpg"
                                        data-lightbox="sup7">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/7-1b.jpg" alt="">
                                </a>
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/7-2b.jpg"
                                        data-lightbox="sup7">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/7-2b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/7-3b.jpg"
                                   data-lightbox="sup7">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/7-3b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/7-4b.jpg"
                                   data-lightbox="sup7">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/7-4b.jpg" alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>

                    </div>

                    <div class="designer-card__about">
                        HM Manufacturing is a manufacturer of custom gears produced to meet all your<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                        <span class="tsrm-content hide"> specifications and requirements. HM gears, are a reliable product to transmit power and to change the speed, torque, and direction of a power source for all your individual applications and needs.</span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="designer-card__btn-block">
                                <button type="button" class="btn btn-danger ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                                        loader-click="openCreatePreorder(1518)">
                                    <?= _t('site.ps', 'Get a Quote'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="responsive-container">
                <div class="designer-card designer-card--ps-cat">

                    <div class="designer-card__userinfo">
                        <div class="designer-card__avatar">
                            <img src="https://static.treatstock.com/static/images/common/suppliers/8-logo.jpg" alt="" align="left">
                        </div>

                        <h3 class="designer-card__username">
                            Circle Gear & Machine Co., Inc.
                        </h3>
                    </div>

                    <div class="designer-card__ps-pics">

                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/8-1b.jpg"
                                        data-lightbox="sup8">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/8-1b.jpg" alt="">
                                </a>
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/8-2b.jpg"
                                        data-lightbox="sup8">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/8-2b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/8-3b.jpg"
                                   data-lightbox="sup8">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/8-3b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/8-4b.jpg"
                                   data-lightbox="sup8">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/8-4b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/8-5b.jpg"
                                   data-lightbox="sup8">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/8-5b.jpg" alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>

                    </div>

                    <div class="designer-card__about">
                        Since 1951 Circle Gear has stood out as a leader in the Chicago area gear<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                        <span class="tsrm-content hide"> market. Specializing in small lot production, Circle Gear has grown from a solid reputation of satisfied customers.</span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="designer-card__btn-block">
                                <button type="button" class="btn btn-danger ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                                        loader-click="openCreatePreorder(1518)">
                                    <?= _t('site.ps', 'Get a Quote'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="responsive-container">
                <div class="designer-card designer-card--ps-cat">

                    <div class="designer-card__userinfo">
                        <div class="designer-card__avatar">
                            <img src="https://static.treatstock.com/static/images/common/suppliers/9-logo.jpg" alt="" align="left">
                        </div>

                        <h3 class="designer-card__username">
                            The Adams Company
                        </h3>
                    </div>

                    <div class="designer-card__ps-pics">

                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/9-1b.jpg"
                                        data-lightbox="sup9">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/9-1b.jpg" alt="">
                                </a>
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/9-2b.jpg"
                                        data-lightbox="sup9">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/9-2b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/9-3b.jpg"
                                   data-lightbox="sup9">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/9-3b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/9-4b.jpg"
                                   data-lightbox="sup9">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/9-4b.jpg" alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>

                    </div>

                    <div class="designer-card__about">
                        Closely held private company established in 1883. Custom manufactured<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                        <span class="tsrm-content hide"> gears and shafts are sold to original equipment manufacturers of agriculture and construction machinery, oil recovery equipment, pumps, winches, wind turbines and many other industries</span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="designer-card__btn-block">
                                <button type="button" class="btn btn-danger ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                                        loader-click="openCreatePreorder(1518)">
                                    <?= _t('site.ps', 'Get a Quote'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="responsive-container">
                <div class="designer-card designer-card--ps-cat">

                    <div class="designer-card__userinfo">
                        <div class="designer-card__avatar">
                            <img src="https://static.treatstock.com/static/images/common/suppliers/10-logo.jpg" alt="" align="left">
                        </div>

                        <h3 class="designer-card__username">
                            Pleasant Gardens Machine, Inc.
                        </h3>
                    </div>

                    <div class="designer-card__ps-pics">

                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/10-1b.jpg"
                                        data-lightbox="sup10">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/10-1b.jpg" alt="">
                                </a>
                                <a
                                        class="designer-card__ps-portfolio-item swiper-slide"
                                        href="https://static.treatstock.com/static/images/common/suppliers/10-2b.jpg"
                                        data-lightbox="sup10">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/10-2b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/10-3b.jpg"
                                   data-lightbox="sup10">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/10-3b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/10-4b.jpg"
                                   data-lightbox="sup10">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/10-4b.jpg" alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/images/common/suppliers/10-5b.jpg"
                                   data-lightbox="sup10">
                                    <img src="https://static.treatstock.com/static/images/common/suppliers/10-5b.jpg" alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>

                    </div>

                    <div class="designer-card__about">
                        PG Machine is a family owned and operated Machine Shop since incorporation<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                        <span class="tsrm-content hide"> 1964. With our broad skill set and equipment, we can turn anything from 1/2 inch to 10 feet. Our skilled and experienced shop personnel are setup capable and pride themselves on quick turnaround and quality workmanship. We can handle small lot production or large lot production.</span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="designer-card__btn-block">
                                <button type="button" class="btn btn-danger ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                                        loader-click="openCreatePreorder(1518)">
                                    <?= _t('site.ps', 'Get a Quote'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    </div>

    <script>
        <?php $this->beginBlock('js1', false); ?>

        //init PS card portfolio pics
        var swiperMachCards = new Swiper('.designer-card__ps-portfolio', {
            scrollbar: '.designer-card__ps-portfolio-scrollbar',
            scrollbarHide: true,
            slidesPerView: 'auto',
            grabCursor: true
        });

        <?php $this->endBlock(); ?>
    </script>

<?php $this->registerJs($this->blocks['js1']); ?>