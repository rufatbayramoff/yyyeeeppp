<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

/** @var \yii\web\View $this */
$this->title = "Category page";

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);

?>

<div class="cat-page-header" style='background-image: url("https://test.treatstock.com/static/images/profile-default-bg.jpg")'>
    <div class="container">
        <h1 class="cat-page-header__title">Category_title</h1>

        <div class="row cat-page-info">
            <div class="col-xs-12 col-sm-3 cat-page-info__pic">
                <img src="/static/images/cat-art.png" alt="Category_name">
            </div>
            <div class="col-xs-12 col-sm-9 cat-page-info__text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquam aliquid asperiores dolorum minima, numquam perferendis quae quis reiciendis sapiente sequi totam vel vero. Animi officiis porro possimus rem rerum.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquam aliquid asperiores dolorum minima, numquam perferendis quae quis reiciendis sapiente sequi totam vel vero. Animi officiis porro possimus rem rerum.
            </div>
        </div>

    </div>
</div>

<div class="container cat-page-models">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <h2 class="cat-page-title">Featured 3D Models</h2>

            <div class="model-card-list swiper-container">

                <div class="model-card-swiper-wrapper swiper-wrapper">

                    <div class="model-card">
                        <div class="model-card__pic">
                            <div class="model-card__like js-store-item-like">
                                <div class="model-card__like-count">2k</div>
                                <div class="model-card__like-icon"></div>
                            </div>
                            <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                <img class="model-card__img" src="/static/images/cat1.png" alt="35mm spacer">
                            </a>
                        </div>

                        <div class="model-card__footer">
                            <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                            <div class="model-card__price">
                                <div class="model-card__price-label">From</div>
                                $1,426.00
                            </div>

                            <?php if(false): #TODO, rating and shipping? ?>
                                <div class="model-card__rating">
                                    <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                    <div class="model-card__rating-count">
                                        359
                                    </div>
                                </div>
                                <div class="model-card__shipping">
                                    free shipping avail
                                    <span class="tsi tsi-truck"></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="model-card">
                        <div class="model-card__pic">
                            <div class="model-card__like  is-liked js-store-item-like">
                                <div class="model-card__like-count">2k</div>
                                <div class="model-card__like-icon"></div>
                            </div>
                            <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                <img class="model-card__img" src="/static/images/cat2.png" alt="35mm spacer">
                            </a>
                        </div>

                        <div class="model-card__footer">
                            <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                            <div class="model-card__price">
                                <div class="model-card__price-label">From</div>
                                $1,426.00
                            </div>

                            <?php if(false): #TODO, rating and shipping? ?>
                                <div class="model-card__rating">
                                    <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                    <div class="model-card__rating-count">
                                        359
                                    </div>
                                </div>
                                <div class="model-card__shipping">
                                    free shipping avail
                                    <span class="tsi tsi-truck"></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="model-card">
                        <div class="model-card__pic">
                            <div class="model-card__like js-store-item-like">
                                <div class="model-card__like-count">2k</div>
                                <div class="model-card__like-icon"></div>
                            </div>
                            <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                <img class="model-card__img" src="/static/images/cat3.png" alt="35mm spacer">
                            </a>
                        </div>

                        <div class="model-card__footer">
                            <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                            <div class="model-card__price">
                                <div class="model-card__price-label">From</div>
                                $1,426.00
                            </div>

                            <?php if(false): #TODO, rating and shipping? ?>
                                <div class="model-card__rating">
                                    <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                    <div class="model-card__rating-count">
                                        359
                                    </div>
                                </div>
                                <div class="model-card__shipping">
                                    free shipping avail
                                    <span class="tsi tsi-truck"></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="model-card">
                        <div class="model-card__pic">
                            <div class="model-card__like  is-liked js-store-item-like">
                                <div class="model-card__like-count">2k</div>
                                <div class="model-card__like-icon"></div>
                            </div>
                            <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                <img class="model-card__img" src="/static/images/cat1.png" alt="35mm spacer">
                            </a>
                        </div>

                        <div class="model-card__footer">
                            <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                            <div class="model-card__price">
                                <div class="model-card__price-label">From</div>
                                $1,426.00
                            </div>

                            <?php if(false): #TODO, rating and shipping? ?>
                                <div class="model-card__rating">
                                    <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                    <div class="model-card__rating-count">
                                        359
                                    </div>
                                </div>
                                <div class="model-card__shipping">
                                    free shipping avail
                                    <span class="tsi tsi-truck"></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="model-card">
                        <div class="model-card__pic">
                            <div class="model-card__like js-store-item-like">
                                <div class="model-card__like-count">2k</div>
                                <div class="model-card__like-icon"></div>
                            </div>
                            <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                <img class="model-card__img" src="/static/images/cat1.png" alt="35mm spacer">
                            </a>
                        </div>

                        <div class="model-card__footer">
                            <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                            <div class="model-card__price">
                                <div class="model-card__price-label">From</div>
                                $1,426.00
                            </div>

                            <?php if(false): #TODO, rating and shipping? ?>
                                <div class="model-card__rating">
                                    <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                    <div class="model-card__rating-count">
                                        359
                                    </div>
                                </div>
                                <div class="model-card__shipping">
                                    free shipping avail
                                    <span class="tsi tsi-truck"></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="model-card">
                        <div class="model-card__pic">
                            <div class="model-card__like  is-liked js-store-item-like">
                                <div class="model-card__like-count">2k</div>
                                <div class="model-card__like-icon"></div>
                            </div>
                            <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                <img class="model-card__img" src="/static/images/cat2.png" alt="35mm spacer">
                            </a>
                        </div>

                        <div class="model-card__footer">
                            <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                            <div class="model-card__price">
                                <div class="model-card__price-label">From</div>
                                $1,426.00
                            </div>

                            <?php if(false): #TODO, rating and shipping? ?>
                                <div class="model-card__rating">
                                    <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                    <div class="model-card__rating-count">
                                        359
                                    </div>
                                </div>
                                <div class="model-card__shipping">
                                    free shipping avail
                                    <span class="tsi tsi-truck"></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="model-card">
                        <div class="model-card__pic">
                            <div class="model-card__like js-store-item-like">
                                <div class="model-card__like-count">2k</div>
                                <div class="model-card__like-icon"></div>
                            </div>
                            <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                <img class="model-card__img" src="/static/images/cat3.png" alt="35mm spacer">
                            </a>
                        </div>

                        <div class="model-card__footer">
                            <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                            <div class="model-card__price">
                                <div class="model-card__price-label">From</div>
                                $1,426.00
                            </div>

                            <?php if(false): #TODO, rating and shipping? ?>
                                <div class="model-card__rating">
                                    <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                    <div class="model-card__rating-count">
                                        359
                                    </div>
                                </div>
                                <div class="model-card__shipping">
                                    free shipping avail
                                    <span class="tsi tsi-truck"></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="model-card">
                        <div class="model-card__pic">
                            <div class="model-card__like  is-liked js-store-item-like">
                                <div class="model-card__like-count">2k</div>
                                <div class="model-card__like-icon"></div>
                            </div>
                            <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                <img class="model-card__img" src="/static/images/cat1.png" alt="35mm spacer">
                            </a>
                        </div>

                        <div class="model-card__footer">
                            <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                            <div class="model-card__price">
                                <div class="model-card__price-label">From</div>
                                $1,426.00
                            </div>

                            <?php if(false): #TODO, rating and shipping? ?>
                                <div class="model-card__rating">
                                    <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                    <div class="model-card__rating-count">
                                        359
                                    </div>
                                </div>
                                <div class="model-card__shipping">
                                    free shipping avail
                                    <span class="tsi tsi-truck"></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>

                <div class="model-card-list__scrollbar swiper-scrollbar"></div>

            </div>
        </div>
    </div>
</div>

<div class="cat-page-models cat-page-models--grey">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <h2 class="cat-page-title">Popular 3D Models</h2>
                <div class="model-card-list swiper-container">

                    <div class="model-card-swiper-wrapper swiper-wrapper">

                        <div class="model-card">
                            <div class="model-card__pic">
                                <div class="model-card__like js-store-item-like">
                                    <div class="model-card__like-count">2k</div>
                                    <div class="model-card__like-icon"></div>
                                </div>
                                <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                    <img class="model-card__img" src="/static/images/cat1.png" alt="35mm spacer">
                                </a>
                            </div>

                            <div class="model-card__footer">
                                <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                                <div class="model-card__price">
                                    <div class="model-card__price-label">From</div>
                                    $1,426.00
                                </div>

                                <?php if(false): #TODO, rating and shipping? ?>
                                    <div class="model-card__rating">
                                        <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                        <div class="model-card__rating-count">
                                            359
                                        </div>
                                    </div>
                                    <div class="model-card__shipping">
                                        free shipping avail
                                        <span class="tsi tsi-truck"></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="model-card">
                            <div class="model-card__pic">
                                <div class="model-card__like  is-liked js-store-item-like">
                                    <div class="model-card__like-count">2k</div>
                                    <div class="model-card__like-icon"></div>
                                </div>
                                <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                    <img class="model-card__img" src="/static/images/cat2.png" alt="35mm spacer">
                                </a>
                            </div>

                            <div class="model-card__footer">
                                <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                                <div class="model-card__price">
                                    <div class="model-card__price-label">From</div>
                                    $1,426.00
                                </div>

                                <?php if(false): #TODO, rating and shipping? ?>
                                    <div class="model-card__rating">
                                        <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                        <div class="model-card__rating-count">
                                            359
                                        </div>
                                    </div>
                                    <div class="model-card__shipping">
                                        free shipping avail
                                        <span class="tsi tsi-truck"></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="model-card">
                            <div class="model-card__pic">
                                <div class="model-card__like js-store-item-like">
                                    <div class="model-card__like-count">2k</div>
                                    <div class="model-card__like-icon"></div>
                                </div>
                                <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                    <img class="model-card__img" src="/static/images/cat3.png" alt="35mm spacer">
                                </a>
                            </div>

                            <div class="model-card__footer">
                                <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                                <div class="model-card__price">
                                    <div class="model-card__price-label">From</div>
                                    $1,426.00
                                </div>

                                <?php if(false): #TODO, rating and shipping? ?>
                                    <div class="model-card__rating">
                                        <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                        <div class="model-card__rating-count">
                                            359
                                        </div>
                                    </div>
                                    <div class="model-card__shipping">
                                        free shipping avail
                                        <span class="tsi tsi-truck"></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="model-card">
                            <div class="model-card__pic">
                                <div class="model-card__like  is-liked js-store-item-like">
                                    <div class="model-card__like-count">2k</div>
                                    <div class="model-card__like-icon"></div>
                                </div>
                                <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                    <img class="model-card__img" src="/static/images/cat1.png" alt="35mm spacer">
                                </a>
                            </div>

                            <div class="model-card__footer">
                                <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                                <div class="model-card__price">
                                    <div class="model-card__price-label">From</div>
                                    $1,426.00
                                </div>

                                <?php if(false): #TODO, rating and shipping? ?>
                                    <div class="model-card__rating">
                                        <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                        <div class="model-card__rating-count">
                                            359
                                        </div>
                                    </div>
                                    <div class="model-card__shipping">
                                        free shipping avail
                                        <span class="tsi tsi-truck"></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="model-card">
                            <div class="model-card__pic">
                                <div class="model-card__like js-store-item-like">
                                    <div class="model-card__like-count">2k</div>
                                    <div class="model-card__like-icon"></div>
                                </div>
                                <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                    <img class="model-card__img" src="/static/images/cat1.png" alt="35mm spacer">
                                </a>
                            </div>

                            <div class="model-card__footer">
                                <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                                <div class="model-card__price">
                                    <div class="model-card__price-label">From</div>
                                    $1,426.00
                                </div>

                                <?php if(false): #TODO, rating and shipping? ?>
                                    <div class="model-card__rating">
                                        <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                        <div class="model-card__rating-count">
                                            359
                                        </div>
                                    </div>
                                    <div class="model-card__shipping">
                                        free shipping avail
                                        <span class="tsi tsi-truck"></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="model-card">
                            <div class="model-card__pic">
                                <div class="model-card__like  is-liked js-store-item-like">
                                    <div class="model-card__like-count">2k</div>
                                    <div class="model-card__like-icon"></div>
                                </div>
                                <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                    <img class="model-card__img" src="/static/images/cat2.png" alt="35mm spacer">
                                </a>
                            </div>

                            <div class="model-card__footer">
                                <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                                <div class="model-card__price">
                                    <div class="model-card__price-label">From</div>
                                    $1,426.00
                                </div>

                                <?php if(false): #TODO, rating and shipping? ?>
                                    <div class="model-card__rating">
                                        <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                        <div class="model-card__rating-count">
                                            359
                                        </div>
                                    </div>
                                    <div class="model-card__shipping">
                                        free shipping avail
                                        <span class="tsi tsi-truck"></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="model-card">
                            <div class="model-card__pic">
                                <div class="model-card__like js-store-item-like">
                                    <div class="model-card__like-count">2k</div>
                                    <div class="model-card__like-icon"></div>
                                </div>
                                <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                    <img class="model-card__img" src="/static/images/cat3.png" alt="35mm spacer">
                                </a>
                            </div>

                            <div class="model-card__footer">
                                <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                                <div class="model-card__price">
                                    <div class="model-card__price-label">From</div>
                                    $1,426.00
                                </div>

                                <?php if(false): #TODO, rating and shipping? ?>
                                    <div class="model-card__rating">
                                        <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                        <div class="model-card__rating-count">
                                            359
                                        </div>
                                    </div>
                                    <div class="model-card__shipping">
                                        free shipping avail
                                        <span class="tsi tsi-truck"></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="model-card">
                            <div class="model-card__pic">
                                <div class="model-card__like  is-liked js-store-item-like">
                                    <div class="model-card__like-count">2k</div>
                                    <div class="model-card__like-icon"></div>
                                </div>
                                <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
                                    <img class="model-card__img" src="/static/images/cat1.png" alt="35mm spacer">
                                </a>
                            </div>

                            <div class="model-card__footer">
                                <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
                                <div class="model-card__price">
                                    <div class="model-card__price-label">From</div>
                                    $1,426.00
                                </div>

                                <?php if(false): #TODO, rating and shipping? ?>
                                    <div class="model-card__rating">
                                        <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                        <div class="model-card__rating-count">
                                            359
                                        </div>
                                    </div>
                                    <div class="model-card__shipping">
                                        free shipping avail
                                        <span class="tsi tsi-truck"></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>

                    <div class="model-card-list__scrollbar swiper-scrollbar"></div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12">

            <div class="cat-page-show-more">
                <button class="btn btn-primary btn-lg btn-ghost">Show More 3D Models</button>
            </div>

        </div>

    </div>
</div>

<script>
    <?php $this->beginBlock('js1', false); ?>

    // Turn on sliders on mobile devices
    function initCatMobileSliders() {
        if ( $('html').hasClass('mobile') ) {

            //Init slider for .model-card-list
            var swiperCatModels = new Swiper('.model-card-list', {
                scrollbar: '.model-card-list__scrollbar',
                slideClass: 'model-card',
                scrollbarHide: true,
                slidesPerView: 1.1,
                spaceBetween: 0,
                grabCursor: true
            });

        }
    }

    initCatMobileSliders();
    $(window).resize(function () {initCatMobileSliders()});

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>