<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

use common\models\Ps;
use frontend\widgets\PsPrintServiceReviewStarsWidget;

$ps=Ps::findByPk(1);

$this->title = "Material Catalog";

?>


<div class="over-nav-tabs-header">
    <div class="container"><h1><?= _t('site.material', 'Material Catalog'); ?></h1></div>
</div>
<div class="nav-tabs__container">
    <div class="container">
        <div class="nav-filter">
            <div class="nav-filter__group nav-filter__group--search">
                <label for=""><?= _t('site.material', 'Search:'); ?></label>
                <div class="nav-filter__input nav-filter__search">
                    <input class="form-control nav-filter__search-input" placeholder="Search">
                    <button class="btn btn-default nav-filter__search-btn" type="button"><span class="tsi tsi-search"></span></button>
                </div>
            </div>
            <?php /*
            <input type="checkbox" id="showhideNavFilter">
            <label  class="nav-filter__mobile-label" for="showhideNavFilter">
                <?= _t('site.material', 'Filters'); ?>
                <span class="tsi tsi-down"></span>
            </label>
            <div class="nav-filter__mobile-right-block">
                <div class="form-group field-sort">
                    <label class="" for="sort">Sort by</label> <select id="sort" class="form-control input-sm nav-filter__input" name="sort" onchange="TsCatalogSearch.changeLocation(this.form)">
                        <option value="">Distance</option>
                        <option value="price">Lowest Price</option>
                        <option value="rating">Best Rating</option>
                    </select>
                </div>
            </div>
            <div class="nav-filter__mobile-container">
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.material', 'Material Group:'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option value="">Any</option>
                        <option value="prototyping">Plastic</option>
                        <option value="hq-prototyping">Metal</option>
                        <option value="jewelry">Sandstone</option>
                        <option value="dental">Resin</option>
                    </select>
                </div>
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.material', 'Technology:'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option value="">Any</option>
                        <option value="FDM">FDM</option>
                        <option value="DMLS">DMLS</option>
                        <option value="EBM">EBM</option>
                        <option value="SLS">SLS</option>
                        <option value="3DP">3DP</option>
                        <option value="PJP">PJP</option>
                        <option value="MJP">MJP</option>
                        <option value="SLA">SLA</option>
                        <option value="LPD">LPD</option>
                        <option value="CJP">CJP</option>
                        <option value="LOM">LOM</option>
                        <option value="SCP">SCP</option>
                        <option value="DLP">DLP</option>
                        <option value="CLIP">CLIP</option>
                        <option value="LC">LC</option>
                        <option value="MJF">MJF</option>
                    </select>
                </div>
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.material', 'Product Application:'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option value="">Any</option>
                        <option value="prototyping">Prototype</option>
                        <option value="hq-prototyping">HD Prototype</option>
                        <option value="jewelry">Jewelry</option>
                        <option value="dental">Dental</option>
                        <option value="colored">Multi-Color</option>
                        <option value="metal">Metal</option>
                    </select>
                </div>
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.material', 'Manufacturer:'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option value="">Any</option>
                        <option value="prototyping">Rec3D</option>
                        <option value="hq-prototyping">3D Filament Pro</option>
                        <option value="jewelry">Filament Inc.</option>
                    </select>
                </div>
                <div class="nav-filter__group nav-filter__group--right hidden-xs">
                    <label for=""><?= _t('site.material', 'Sort by:'); ?></label>
                    <select id="model3d-printers-sort" name="Model3dEditForm[currentPrintersSort]" class="selectpicker form-control input-sm nav-filter__input" onchange="model3dItemFormObj.reloadPrintersList();">
                        <option selected="" value="default">Default</option>
                        <option value="distance">
                            Best quality
                        </option>
                        <option value="price">
                            Lowest price
                        </option>
                    </select>
                </div>
            </div>
            */?>
        </div>
    </div>
</div>

<div class="container">


    <div class="responsive-container-list responsive-container-list--3">

        <div class="responsive-container">
            <a href="#PLA" class="mat-cat-card">
                <img class="mat-cat-card__pic" src="https://static.treatstock.com/static/files/19/2d/130475_13298_c81041e069bd5da875cb586667d1002c_720x540.jpg?date=1507222380" alt="PLA Plastic">
                <h3 class="mat-cat-card__title"><?= _t('site.material', 'PLA'); ?></h3>
            </a>
        </div>

        <div class="responsive-container">
            <a href="#Resin" class="mat-cat-card">
                <img class="mat-cat-card__pic" src="https://static.treatstock.com/static/files/4a/5b/138725_14099_e12ad04fd90a291a11abf3cfac176eb7_720x540.jpg?date=1508523648" alt="PLA Plastic">
                <h3 class="mat-cat-card__title"><?= _t('site.material', 'Resin'); ?></h3>
            </a>
        </div>

        <div class="responsive-container">
            <a href="#Resin" class="mat-cat-card">
                <img class="mat-cat-card__pic" src="https://static.treatstock.com/static/files/25/73/138738_14099_5c264a7597b0a91bba5d0b244437bbf6_720x540.jpg?date=1508525559" alt="PLA Plastic">
                <h3 class="mat-cat-card__title"><?= _t('site.material', 'Eshe 4eto'); ?></h3>
            </a>
        </div>

    </div>

</div>