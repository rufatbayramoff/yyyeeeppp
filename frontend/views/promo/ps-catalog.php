<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

use common\models\Ps;
use frontend\widgets\PsPrintServiceReviewStarsWidget;

$ps=Ps::findByPk(1);

$this->title = "Printer Services";

?>


<div class="over-nav-tabs-header">
    <div class="container"><h1><?= _t('site.ps', 'Print Services'); ?></h1></div>
</div>
<div class="nav-tabs__container">
    <div class="container">
        <div class="nav-filter nav-filter--many">
            <input type="checkbox" id="showhideNavFilter">
            <label  class="nav-filter__mobile-label" for="showhideNavFilter">
                <?= _t('site.ps', 'Filters'); ?>
                <span class="tsi tsi-down"></span>
            </label>
            <div class="nav-filter__mobile-right-block">
                <div class="form-group field-sort">
                    <label class="" for="sort">Sort by</label> <select id="sort" class="form-control input-sm nav-filter__input" name="sort" onchange="TsCatalogSearch.changeLocation(this.form)">
                        <option value="">Distance</option>
                        <option value="price">Lowest Price</option>
                        <option value="rating">Best Rating</option>
                    </select>
                </div>
            </div>
            <div class="nav-filter__mobile-container">
                <div class="nav-filter__group">
                    <div class="form-group field-international">
                        <label class="" for="international">International</label> <label class="checkbox-switch checkbox-switch--xs">
                            <input type="hidden" name="international" value="0"><input type="checkbox" id="international" name="international" value="1" checked="" onchange="TsCatalogSearch.changeLocation(this.form)">
                            <span class="slider"></span>
                        </label>
                    </div>
                </div>
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'Country:'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option selected="" value="">Any</option>
                        <option value="">USA</option>
                        <option value="">Germany</option>
                        <option value="">Russia</option>
                        <option value="">France</option>
                    </select>
                </div>
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'City:'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option selected="" value="">Any</option>
                        <option value="">Washington DC</option>
                        <option value="">Chicage</option>
                        <option value="">New York</option>
                        <option value="">Los Angeles</option>
                    </select>
                </div>
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'Material:'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option selected="" value="">Any</option>
                        <option value="">PLA</option>
                        <option value="">ABS</option>
                        <option value="">Resin</option>
                        <option value="">Composite</option>
                    </select>
                </div>
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'Certification:'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option selected="" value="">Any</option>
                        <option value="">Professional</option>
                        <option value="">Common</option>
                    </select>
                </div>
                <div class="nav-filter__group nav-filter__group--right hidden-xs">
                    <label for=""><?= _t('site.ps', 'Sort by:'); ?></label>
                    <select id="model3d-printers-sort" name="Model3dEditForm[currentPrintersSort]" class="selectpicker form-control input-sm nav-filter__input" onchange="model3dItemFormObj.reloadPrintersList();">
                        <option selected="" value="default">Default</option>
                        <option value="distance">
                            Nearest to me            </option>
                        <option value="price">
                            Lowest price            </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <ul class="pagination m-t0">
                <li class="prev disabled"><span><span class="tsi tsi-left"></span></span></li>
                <li class="active"><a href="#page1" data-page="0">1</a></li>
                <li><a href="#page2" data-page="1">2</a></li>
                <li><a href="#page3" data-page="2">3</a></li>
                <li><a href="#page4" data-page="3">4</a></li>
                <li><a href="#page5" data-page="4">5</a></li>
                <li><a href="#page6" data-page="5">6</a></li>
                <li><a href="#page7" data-page="6">7</a></li>
                <li><a href="#page8" data-page="7">8</a></li>
                <li><a href="#page9" data-page="8">9</a></li>
                <li><a href="#page10" data-page="9">10</a></li>
                <li class="next"><a href="#page2" data-page="1"><span class="tsi tsi-right"></span></a></li>
            </ul>
        </div>
    </div>

    <?php /*
    <div class="ps-cat">

        <div class="ps-cat__userinfo">

            <a class="ps-cat__avatar" href="#to-ps-page" target="_blank">
                <img src="https://static.treatstock.com/static/user/0bf727e907c5fc9d5356f11e4c45d613/ps_logo_1479155239_64x64.jpg" alt="" align="left">
            </a>

            <h4 class="ps-cat__username">
                <a href="#to-ps-page" target="_blank">
                    Laughing Monkey Labs LLC
                </a>
            </h4>

            <a href="#show-about"
               class="ps-cat__about"
               data-container="body"
               data-toggle="popover"
               data-placement="bottom"
               data-content="I am experienced in 3D modeling and design with my specialty being cosplay and items from movies and games. I'm really passionate about what I do and if you need a 3D printable model designed for you within a short turnaround, then send me a message and I will create it for you."
               data-original-title=""
               title="">
                <?= _t('site.ps', 'Show about'); ?>
            </a>

        </div>

        <div class="ps-cat__ps-rating">


            <?= PsPrintServiceReviewStarsWidget::widget(['ps' => $ps]); ?>


        </div>

        <div class="ps-cat__cert">
            <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                Pro
            </div>
        </div>

        <div class="ps-cat__loc">
            Harrison Charter Township,
            <br>
            Michigan, US
        </div>

        <div class="ps-cat__materials">
            <span class="ps-cat__data-label"><?= _t('site.ps', 'Materials:'); ?></span> PLA, ABS, SomeShit
        </div>

        <div class="ps-cat__ps-pics">

            <div class="ps-cat__ps-pics-wrap">
                <div class="fotorama"
                     data-width="100%"
                     data-height="auto"
                     data-arrows="true"
                     data-click="true"
                     data-nav="thumbs"
                     data-thumbmargin="0"
                     data-thumbwidth="80px"
                     data-thumbheight="60px"
                     data-allowfullscreen="true">
                    <img src="/static/images/cat1.png" alt="">
                    <img src="/static/images/cat2.png" alt="">
                    <img src="/static/images/cat3.png" alt="">
                </div>
            </div>

        </div>

        <div class="ps-cat__btn-block">
            <span class="ps-cat__data-label"><?= _t('site.ps', 'Min. Print Price:'); ?></span>
            <span class="ps-cat__price">$10</span>
            <a class="btn btn-primary btn-sm btn-ghost ps-cat__print-btn ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="#to-ps-page">
                <span class="tsi tsi-printer3d"></span>
                <?= _t('site.ps', 'Buy Here'); ?>
            </a>
        </div>

    </div>
    */?>


    <div class="responsive-container-list responsive-container-list--3">

        <div class="responsive-container">
            <div class="designer-card designer-card--ps-cat">
                <div class="designer-card__ps-rating">

                    <!-- temp off
                        <?= PsPrintServiceReviewStarsWidget::widget(['ps' => $ps]); ?>
                        -->

                    <div id="star-rating-block-55146" class="star-rating-block" itemprop="aggregateRating"
                         itemscope="" itemtype="http://schema.org/AggregateRating">
                        <meta itemprop="ratingValue" content="5">
                        <meta itemprop="worstRating" content="1">
                        <meta itemprop="bestRating" content="5">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                       step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                       data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                    <span class="star-rating-count">(<span itemprop="reviewCount">23</span> reviews)</span>
                </div>

                <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                    Pro
                </div>

                <div class="designer-card__userinfo">
                    <a class="designer-card__avatar" href="#to-ps-page" target="_blank">
                        <img src="https://static.treatstock.com/static/user/fcdf25d6e191893e705819b177cddea0/avatar_1478208742_64x64.jpg" alt="" align="left">
                    </a>

                    <h3 class="designer-card__username">
                        <a href="#to-ps-page" target="_blank" title="Ed's Printing">
                            Ed's Printing
                        </a>
                    </h3>

                    <div class="designer-card__ps-visit">
                        <?= _t('site.ps', 'Last online'); ?> 02.10.2017
                    </div>
                </div>

                <div class="designer-card__ps-pics">

                    <div class="fotorama"
                         data-width="100%"
                         data-height="auto"
                         data-arrows="true"
                         data-click="true"
                         data-nav="thumbs"
                         data-thumbmargin="10"
                         data-thumbwidth="120px"
                         data-thumbheight="90px"
                         data-allowfullscreen="true">
                        <img src="/static/images/cat2.png" alt="">
                        <img src="/static/images/cat3.png" alt="">
                        <img src="/static/images/cat1.png" alt="">
                    </div>

                </div>

                <div class="designer-card__ps-loc" title="Harrison Charter Township, Michigan, US">
                    <span class="tsi tsi-map-marker"></span>
                    Harrison Charter Township, Michigan, US
                </div>

                <div class="designer-card__about">
                    I am experienced in 3D modeling and design with my specialty being cosplay and items from<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                    <span class="tsrm-content hide"> movies and games. I'm really passionate about what I do and if you need a 3D printable model designed for you within a short turnaround, then send me a message and I will create it for you.</span>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Materials:'); ?></span> PLA, ABS, Reisin +5 materials
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Min. Print Price:'); ?></span> $10
                        </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Shipping:'); ?></span> $5
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__btn-block">
                            <a class="btn btn-danger btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="#to-ps-page">
                                <span class="tsi tsi-printer3d"></span>
                                <?= _t('site.ps', 'Buy Here'); ?>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card designer-card--ps-cat">
                <div class="designer-card__ps-rating">
                    <span class="text-muted">Be the first to review</span>
                </div>

                <div class="designer-card__userinfo">
                    <a class="designer-card__avatar" href="#to-ps-page" target="_blank">
                        <img src="https://static.treatstock.com/static/images/defaultPrinter.png" alt="" align="left">
                    </a>

                    <h3 class="designer-card__username">
                        <a href="#to-ps-page" target="_blank" title="Candyskull Cupcake Designs 3D Printing Service">
                            Candyskull Cupcake Designs 3D Printing Service
                        </a>
                    </h3>
                    <div class="designer-card__ps-visit designer-card__ps-visit--online">
                        <?= _t('site.ps', 'Online now'); ?>
                    </div>
                </div>

                <div class="designer-card__ps-pics">

                    <div class="designer-card__ps-pics-empty">
                        No images have been uploaded yet
                    </div>

                </div>

                <div class="designer-card__ps-loc" title="Harrison Charter Township, Michigan, US">
                    <span class="tsi tsi-map-marker"></span>
                    Harrison Charter Township, Michigan, US
                </div>

                <div class="designer-card__about">
                    I am experienced in 3D modeling and design with my specialty being cosplay and items from<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                    <span class="tsrm-content hide"> movies and games. I'm really passionate about what I do and if you need a 3D printable model designed for you within a short turnaround, then send me a message and I will create it for you.</span>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Materials'); ?></span>: PLA
                        </div>

                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Min. Print Price'); ?></span>: $10
                        </div>
                        <div class="designer-card__data">
                            Customer Pickup
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">

                        <div class="designer-card__btn-block">
                            <a class="btn btn-danger btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="#to-ps-page">
                                <span class="tsi tsi-printer3d"></span>
                                <?= _t('site.ps', 'Buy Here'); ?>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card designer-card--ps-cat">
                <div class="designer-card__ps-rating">
                    <div id="star-rating-block-55146" class="star-rating-block" itemprop="aggregateRating"
                         itemscope="" itemtype="http://schema.org/AggregateRating">
                        <meta itemprop="ratingValue" content="5">
                        <meta itemprop="worstRating" content="1">
                        <meta itemprop="bestRating" content="5">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                       step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                       data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                    <span class="star-rating-count">(<span itemprop="reviewCount">23</span> reviews)</span>
                </div>

                <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                    Pro
                </div>

                <div class="designer-card__userinfo">
                    <a class="designer-card__avatar" href="#to-ps-page" target="_blank">
                        <img src="https://static.treatstock.com/static/user/0e087ec55dcbe7b2d7992d6b69b519fb/ps_logo_1479425253_64x64.jpg" alt="" align="left">
                    </a>

                    <h3 class="designer-card__username">
                        <a href="#to-ps-page" target="_blank" title="Dylco">
                            Dylco
                        </a>
                    </h3>

                    <div class="designer-card__ps-visit">
                        <?= _t('site.ps', 'Last visit'); ?> 02.10.2017
                    </div>
                </div>

                <div class="designer-card__ps-pics">

                    <div class="fotorama"
                         data-width="100%"
                         data-height="auto"
                         data-arrows="true"
                         data-click="true"
                         data-nav="thumbs"
                         data-thumbmargin="10"
                         data-thumbwidth="120px"
                         data-thumbheight="90px"
                         data-allowfullscreen="true">
                        <img src="/static/images/cat3.png" alt="">
                        <img src="/static/images/cat1.png" alt="">
                        <img src="/static/images/cat2.png" alt="">
                    </div>

                </div>

                <div class="designer-card__ps-loc" title="Harrison Charter Township, Michigan, US">
                    <span class="tsi tsi-map-marker"></span>
                    Harrison Charter Township, Michigan, US
                </div>

                <div class="designer-card__about">
                    I am experienced in 3D modeling and design with my specialty being cosplay and items from<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                    <span class="tsrm-content hide"> movies and games. I'm really passionate about what I do and if you need a 3D printable model designed for you within a short turnaround, then send me a message and I will create it for you.</span>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Materials:'); ?></span> PLA, ABS
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Min. Print Price:'); ?></span> $10
                        </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Shipping:'); ?></span> $5
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__btn-block">
                            <a class="btn btn-danger btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="#to-ps-page">
                                <span class="tsi tsi-printer3d"></span>
                                <?= _t('site.ps', 'Buy Here'); ?>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card designer-card--ps-cat">
                <div class="designer-card__ps-rating">
                    <div id="star-rating-block-55146" class="star-rating-block" itemprop="aggregateRating"
                         itemscope="" itemtype="http://schema.org/AggregateRating">
                        <meta itemprop="ratingValue" content="5">
                        <meta itemprop="worstRating" content="1">
                        <meta itemprop="bestRating" content="5">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                       step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                       data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                    <span class="star-rating-count">(<span itemprop="reviewCount">23</span> reviews)</span>
                </div>

                <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                    Pro
                </div>

                <div class="designer-card__userinfo">
                    <a class="designer-card__avatar" href="#to-ps-page" target="_blank">
                        <img src="https://static.treatstock.com/static/user/0bf727e907c5fc9d5356f11e4c45d613/ps_logo_1479155239_64x64.jpg" alt="" align="left">
                    </a>

                    <h3 class="designer-card__username">
                        <a href="#to-ps-page" target="_blank" title="Laughing Monkey Labs LLC">
                            Laughing Monkey Labs LLC
                        </a>
                    </h3>

                    <div class="designer-card__ps-visit">
                        <?= _t('site.ps', 'Last visit'); ?> 02.10.2017
                    </div>
                </div>

                <div class="designer-card__ps-pics">

                    <div class="fotorama"
                         data-width="100%"
                         data-height="auto"
                         data-arrows="true"
                         data-click="true"
                         data-nav="thumbs"
                         data-thumbmargin="10"
                         data-thumbwidth="120px"
                         data-thumbheight="90px"
                         data-allowfullscreen="true">
                        <img src="/static/images/cat1.png" alt="">
                        <img src="/static/images/cat2.png" alt="">
                        <img src="/static/images/cat3.png" alt="">
                    </div>

                </div>

                <div class="designer-card__ps-loc" title="Harrison Charter Township, Michigan, US">
                    <span class="tsi tsi-map-marker"></span>
                    Harrison Charter Township, Michigan, US
                </div>

                <div class="designer-card__about">
                    I am experienced in 3D modeling and design with my specialty being cosplay and items from<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                    <span class="tsrm-content hide"> movies and games. I'm really passionate about what I do and if you need a 3D printable model designed for you within a short turnaround, then send me a message and I will create it for you.</span>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Materials:'); ?></span> ABS
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Min. Print Price:'); ?></span> $10
                        </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Shipping:'); ?></span> $5
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__btn-block">
                            <a class="btn btn-danger btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="#to-ps-page">
                                <span class="tsi tsi-printer3d"></span>
                                <?= _t('site.ps', 'Buy Here'); ?>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-sm-12">
            <ul class="pagination">
                <li class="prev disabled"><span><span class="tsi tsi-left"></span></span></li>
                <li class="active"><a href="#page1" data-page="0">1</a></li>
                <li><a href="#page2" data-page="1">2</a></li>
                <li><a href="#page3" data-page="2">3</a></li>
                <li><a href="#page4" data-page="3">4</a></li>
                <li><a href="#page5" data-page="4">5</a></li>
                <li><a href="#page6" data-page="5">6</a></li>
                <li><a href="#page7" data-page="6">7</a></li>
                <li><a href="#page8" data-page="7">8</a></li>
                <li><a href="#page9" data-page="8">9</a></li>
                <li><a href="#page10" data-page="9">10</a></li>
                <li class="next"><a href="#page2" data-page="1"><span class="tsi tsi-right"></span></a></li>
            </ul>
        </div>
    </div>

</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <h2>Seo text</h2>
            <p>Some SEO bullshit text. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid cumque delectus fugit illo maxime quasi reiciendis, repellat ullam. Consequatur corporis earum eum eveniet facilis, iure magnam mollitia. Expedita, sit, voluptas.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid assumenda at deleniti distinctio earum hic in iste iusto laboriosam magnam maxime minus modi non odio officia, quia soluta, veniam voluptate.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid assumenda at deleniti distinctio earum hic in iste iusto laboriosam magnam maxime minus modi non odio officia, quia soluta, veniam voluptate. Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid amet assumenda distinctio dolore ducimus impedit, itaque, iusto laudantium maxime nisi nulla placeat quaerat quod recusandae reprehenderit sapiente tempore, ut?</p>
        </div>
    </div>
</div>