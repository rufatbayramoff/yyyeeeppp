<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

use common\models\Ps;
use frontend\widgets\PsPrintServiceReviewStarsWidget;

use kartik\select2\Select2Asset;
use kartik\select2\ThemeKrajeeAsset;
use yii\bootstrap\ActiveForm;

$ps = Ps::findByPk(1);

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

Select2Asset::register($this);
ThemeKrajeeAsset::register($this);

?>

    <style>
        .star-rating-block,
        .star-rating-count {
            display: inline-block;
        }
        .star-rating-block {
            position: relative;
        }
        .star-rating-block .star-rating {
            position: relative;
            top: -2px;
            display: inline-block;
            margin-right: 5px;
        }
        .star-rating-range {
            padding: 0 5px;
            font-size: 14px;
            font-weight: normal;
        }
        .star-rating-count {
            font-size: 14px;
            color: #909498;
            font-weight: normal;
        }
    </style>

    <div class="over-nav-tabs-header m-b0">
        <div class="container container--wide p-t20 p-b30">
            <div class="row">
                <div class="col-sm-6">

                    <h1 class="animated m-b20">
                        <?= _t('site.page', 'SLA 3D Printing'); ?>
                    </h1>

                    <p class="lead">
                        <?= _t('site.page', 'Subtitle text block'); ?>
                    </p>

                    <p>
                        <?= _t('site.page', 'Describing body text block for more text content and some bla bla bla words.'); ?>
                    </p>

                    <ul class="checked-list">
                        <li>Choose from over 100 materials and colors</li>
                        <li>Compare instant prices to find the best deals online</li>
                        <li>Fast turnarounds with orders generally delivered within 5 days</li>
                        <li>All machines are tested and orders pass a quality control check</li>
                    </ul>

                    <a href="#go-to-upload" class="btn btn-danger m-t10 m-b10">
                        <?= _t('site.page', 'Call to action'); ?>
                    </a>

                </div>
                <div class="col-sm-6 wide-padding--left">
                    <img class="img-responsive m-t30 m-b10"
                         src="http://www.papaya3dp.com/uploads/personal/C001830/images/papaya3dp-3d-printing-service-sla-electronics-case-fit-test-full.gif"
                         alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="">

        <div class="nav-tabs__container">
            <div class="container container--wide">
                <div class="nav-filter nav-filter--many nav-filter--ps-cat">
                    <form id="w0" class="form-inline"
                          action="/3d-printing-services/Los-Angeles--California--US/technology-SLA?sort=distance"
                          method="post" onsubmit="return false; ">
                        <input type="hidden" name="_frontendCSRF"
                               value="ksKNNvg_OaQoRvikLxuEk_2vZTZn0i2KlMqnLXBFCsPIq_tZgGwO0k5yy-oCK93AyJo6QyCdWdvQldJcIS9-tw==">
                        <input type="checkbox" id="showhideNavFilter">
                        <label class="nav-filter__mobile-label" for="showhideNavFilter">
                            Filters <span class="tsi tsi-down"></span>
                        </label>
                        <div class="nav-filter__mobile-right-block">
                            <div class="form-group field-sort">
                                <label class="" for="sort">Sort by</label>
                                <select id="sort"
                                        class="form-control input-sm nav-filter__input"
                                        name="sort"
                                        onchange="TsCatalogSearch.changeLocation(this.form)">
                                    <option value="distance" selected="">Distance</option>
                                    <option value="price">Lowest Price</option>
                                    <option value="rating">Best Rating</option>
                                </select>
                            </div>
                        </div>
                        <div class="nav-filter__mobile-container">

                            <div class="nav-filter__group">
                                <div class="form-group field-category">
                                    <label for="">Category</label>
                                    <select class="form-control js-category-select input-sm select2-hidden-accessible"
                                            ng-model="categorySlug" ng-init="initCategories()" data-select2-id="1"
                                            tabindex="-1" aria-hidden="true">
                                        <option value="Any" data-select2-id="3">Any</option>
                                        <option value="manufacturing-services" data-select2-id="4">Manufacturing
                                            Services
                                        </option>
                                        <option value="designing-services" data-select2-id="5">3D Design</option>
                                        <option value="cnc-manufacturing" data-select2-id="6">CNC Machining</option>
                                        <option value="injection-molding" data-select2-id="7">Injection Molding</option>
                                        <option value="signage-services" data-select2-id="8">Signage</option>
                                        <option value="electronics-manufacturing-service" data-select2-id="9">
                                            Electronics Manufacturing Service
                                        </option>
                                        <option value="3d-scanning" data-select2-id="10">3D Scanning</option>
                                        <option value="painting" data-select2-id="11">Painting</option>
                                        <option value="other" data-select2-id="12">Other</option>
                                        <option value="cutting" data-select2-id="13">Cutting</option>
                                        <option value="Urethane-Casting" data-select2-id="14">Urethane Casting</option>
                                        <option value="Sheet-Fabrication" data-select2-id="15">Sheet Fabrication
                                        </option>
                                        <option value="Metal-Casting" data-select2-id="16">Metal Casting</option>
                                        <option value="Vacuum-Forming" data-select2-id="17">Vacuum Forming</option>
                                        <option value="Mold-Making" data-select2-id="18">Mold Making</option>
                                        <option value="Metal-Stamping" data-select2-id="19">Metal Stamping</option>
                                        <option value="CNC-turning" data-select2-id="20">CNC Turning</option>
                                    </select>
                                    <span class="select2 select2-container select2-container--krajee" dir="ltr"
                                          data-select2-id="2" style="width: 100%;"><span
                                                class="selection"><span
                                                    class="select2-selection select2-selection--single form-control js-category-select input-sm"
                                                    role="combobox" aria-haspopup="true" aria-expanded="false"
                                                    tabindex="0" aria-disabled="false"
                                                    aria-labelledby="select2-srfb-container"><span
                                                        class="select2-selection__rendered" id="select2-srfb-container"
                                                        role="textbox" aria-readonly="true" title="Any">Any</span><span
                                                        class="select2-selection__arrow" role="presentation"><b
                                                            role="presentation"></b></span></span></span><span
                                                class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                            </div>

                            <div class="nav-filter__group">
                                <label for="">Location</label>
                                <div class="UserLocationWidget">
                                    <span class="tsi tsi-map-marker"></span>
                                    <button type="button" class="ts-user-location ts-ajax-modal btn-link"
                                            title="Shipping Address" data-url="/geo/change-location"
                                            data-target="#changelocation">Los Angeles, US
                                    </button>
                                </div>
                            </div>
                            <div class="nav-filter__group">
                                <div class="form-group field-international">
                                    <label class="" for="international">International</label>
                                    <label
                                            class="checkbox-switch checkbox-switch--xs" for="international"><input
                                                type="hidden" name="international" value="0">
                                        <input type="checkbox"
                                               id="international"
                                               name="international"
                                               value="1"
                                               onchange="TsCatalogSearch.changeLocation(this.form)"><span
                                                class="slider"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="nav-filter__group">
                                <div class="form-group field-technology">
                                    <label class="" for="technology">Technology</label>
                                    <select id="technology"
                                            class="form-control input-sm nav-filter__input select2-hidden-accessible"
                                            name="technology"
                                            onchange="TsCatalogSearch.changeLocation(this.form)"
                                            data-s2-options="s2options_21667806"
                                            data-krajee-select2="select2_51d09493"
                                            style="width: 1px; height: 1px; visibility: hidden;"
                                            data-select2-id="technology"
                                            tabindex="-1"
                                            aria-hidden="true">
                                        <option value="">Any</option>
                                        <option value="0">Any</option>
                                        <option value="FDM">FDM</option>
                                        <option value="MJP">MJP</option>
                                        <option value="SLA" selected="" data-select2-id="23">SLA</option>
                                        <option value="CJP">CJP</option>
                                        <option value="DLP">DLP</option>
                                        <option value="MJF">MJF</option>
                                        <option value="DUP">DUP</option>
                                    </select>
                                    <span class="select2 select2-container select2-container--krajee input-sm"
                                          dir="ltr" data-select2-id="22" style="width: 100%;"><span
                                                class="selection"><span
                                                    class="select2-selection select2-selection--single" role="combobox"
                                                    aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                    aria-disabled="false"
                                                    aria-labelledby="select2-technology-container"><span
                                                        class="select2-selection__rendered"
                                                        id="select2-technology-container" role="textbox"
                                                        aria-readonly="true" title="SLA">SLA</span><span
                                                        class="select2-selection__arrow" role="presentation"><b
                                                            role="presentation"></b></span></span></span><span
                                                class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                            </div>

                            <div class="nav-filter__group">
                                <div class="form-group field-usage">
                                    <label class="" for="usage">Product Application</label>
                                    <select id="usage"
                                            class="form-control input-sm nav-filter__input select2-hidden-accessible"
                                            name="usage"
                                            onchange="TsCatalogSearch.changeLocation(this.form)"
                                            data-s2-options="s2options_21667806"
                                            data-krajee-select2="select2_51d09493"
                                            style="width: 1px; height: 1px; visibility: hidden;"
                                            data-select2-id="usage"
                                            tabindex="-1"
                                            aria-hidden="true">
                                        <option value="" data-select2-id="25">Any</option>
                                        <option value="0">Any</option>
                                        <option value="hq-prototyping">HD Prototype</option>
                                        <option value="jewelry">Jewelry</option>
                                        <option value="dental">Dental</option>
                                    </select>
                                    <span class="select2 select2-container select2-container--krajee input-sm"
                                          dir="ltr" data-select2-id="24" style="width: 100%;"><span
                                                class="selection"><span
                                                    class="select2-selection select2-selection--single" role="combobox"
                                                    aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                    aria-disabled="false"
                                                    aria-labelledby="select2-usage-container"><span
                                                        class="select2-selection__rendered" id="select2-usage-container"
                                                        role="textbox" aria-readonly="true"><span
                                                            class="select2-selection__placeholder">Any</span></span><span
                                                        class="select2-selection__arrow" role="presentation"><b
                                                            role="presentation"></b></span></span></span><span
                                                class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                            </div>

                            <div class="nav-filter__group">
                                <div class="form-group field-material">
                                    <label class="" for="material">Material</label>
                                    <span id="parent-s2-togall-material"
                                          style="display:none"><span
                                                id="s2-togall-material" class="s2-togall-button s2-togall-select"><span
                                                    class="s2-select-label"><i
                                                        class="glyphicon glyphicon-unchecked"></i>Select all</span><span
                                                    class="s2-unselect-label"><i class="glyphicon glyphicon-check"></i>Unselect all</span></span></span><input
                                            type="hidden" name="material" value="">
                                    <select id="material"
                                            class="form-control input-sm nav-filter__input select2-hidden-accessible"
                                            name="material[]" multiple=""
                                            size="4"
                                            onchange="TsCatalogSearch.changeLocation(this.form)"
                                            data-s2-options="s2options_8cca79c2"
                                            data-krajee-select2="select2_b0512bd7"
                                            style="width: 1px; height: 1px; visibility: hidden;"
                                            data-select2-id="material"
                                            tabindex="-1"
                                            aria-hidden="true">
                                        <option value="abs-like-resin">ABS-like Resin</option>
                                        <option value="accura-clearvue">Accura ClearVue</option>
                                        <option value="accura-e-stone">Accura e-Stone</option>
                                        <option value="alumina">Alumina</option>
                                        <option value="alumina-zirconia">AluminaZirconia</option>
                                        <option value="aluminium-nitride">AluminiumNitride</option>
                                        <option value="biomed-resin">BioMed-Resin</option>
                                        <option value="castable-resin">Castable Resin</option>
                                        <option value="ceramic-resin">Ceramic Resin</option>
                                        <option value="clear-biomed-resin">ClearBiomedResin</option>
                                        <option value="cordierite">Cordierite</option>
                                        <option value="dental-lt-resin">Dental LT Resin</option>
                                        <option value="dental-sg-resin">Dental SG Resin</option>
                                        <option value="draft-resin">Draft Resin</option>
                                        <option value="durable-pp-like-resin">Durable (PP-like) Resin</option>
                                        <option value="elastic-resin-soft-and-flexible">Elastic Resin (Soft and
                                            flexible)
                                        </option>
                                        <option value="flexible-resin">Flexible Resin</option>
                                        <option value="fused-silica">FusedSilica</option>
                                        <option value="glass-reinforced-resin">Glass-reinforced Resin</option>
                                        <option value="grey-pro-resin">Grey Pro Resin</option>
                                        <option value="high-temp-resin">High Temp Resin</option>
                                        <option value="hydroxyapatite">Hydroxyapatite</option>
                                        <option value="solidscape-3z-model">Jewelry Wax (Solidscape 3Z Model)</option>
                                        <option value="nanoceramic-resin">Nanoceramic Resin</option>
                                        <option value="resin">Resin</option>
                                        <option value="rigid-opaque-resin">Rigid opaque Resin</option>
                                        <option value="rubber-like-resin">Rubber-like Resin</option>
                                        <option value="silicon-nitride">SiliconNitride</option>
                                        <option value="silicore">Silicore</option>
                                        <option value="somos-evolve-128">Somos EvoLVe 128</option>
                                        <option value="somos-next">Somos NeXt</option>
                                        <option value="somos-watershed-xc-11122">Somos WaterShed XC 11122</option>
                                        <option value="standard-acrylic-resin">Standard acrylic Resin</option>
                                        <option value="tough-2000-resin">Tough2000</option>
                                        <option value="tough-resin">Tough Resin</option>
                                        <option value="tricalcium-phosphate">TricalciumPhosphate</option>
                                        <option value="visijet-ftx-cast">VisiJet FTX Cast</option>
                                        <option value="visijet-ftx">VisiJet FTX resin</option>
                                        <option value="visijet-sl-clear">VisiJet SL Clear</option>
                                        <option value="visijet-sl-e-stone">VisiJet SL e-Stone</option>
                                        <option value="waterclear-resin">WaterClear Resin</option>
                                        <option value="wax-like-resin">Wax-like Resin</option>
                                        <option value="zirconia-3y">Zirconia3Y</option>
                                        <option value="zirconia-8y">Zirconia8Y</option>
                                    </select>
                                    <span class="select2 select2-container select2-container--krajee input-sm"
                                          dir="ltr" data-select2-id="26" style="width: 1px;"><span
                                                class="selection"><span
                                                    class="select2-selection select2-selection--multiple"
                                                    role="combobox" aria-haspopup="true" aria-expanded="false"
                                                    tabindex="-1" aria-disabled="false"><ul
                                                        class="select2-selection__rendered"><li
                                                            class="select2-search select2-search--inline"><input
                                                                class="select2-search__field" type="search" tabindex="0"
                                                                autocomplete="off" autocorrect="off"
                                                                autocapitalize="none" spellcheck="false"
                                                                role="searchbox" aria-autocomplete="list"
                                                                placeholder="Any"
                                                                style="width: 158px;"></li></ul></span></span><span
                                                class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                            </div>

                            <div class="nav-filter__group  hidden-xs">
                                <div class="form-group field-sort2">
                                    <label class="" for="sort2">Sort by</label>
                                    <select id="sort2"
                                            class="form-control input-sm nav-filter__input select2-hidden-accessible"
                                            name="sort"
                                            onchange="TsCatalogSearch.changeLocation(this.form)"
                                            data-s2-options="s2options_21667806"
                                            data-krajee-select2="select2_5afcdf75"
                                            style="width: 1px; height: 1px; visibility: hidden;"
                                            data-select2-id="sort2"
                                            tabindex="-1"
                                            aria-hidden="true">
                                        <option value="">Any</option>
                                        <option value="distance" selected="" data-select2-id="28">Distance</option>
                                        <option value="price">Lowest Price</option>
                                        <option value="rating">Best Rating</option>
                                    </select>
                                    <span class="select2 select2-container select2-container--krajee input-sm"
                                          dir="ltr" data-select2-id="27" style="width: 100%;"><span
                                                class="selection"><span
                                                    class="select2-selection select2-selection--single" role="combobox"
                                                    aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                    aria-disabled="false"
                                                    aria-labelledby="select2-sort2-container"><span
                                                        class="select2-selection__rendered" id="select2-sort2-container"
                                                        role="textbox" aria-readonly="true"
                                                        title="Distance">Distance</span><span
                                                        class="select2-selection__arrow" role="presentation"><b
                                                            role="presentation"></b></span></span></span><span
                                                class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                            </div>

                            <div class="nav-filter__group hidden-xs">
                                <div class="form-group field-text">
                                    <label class="" for="text">Search</label>
                                    <input type="text" id="text"
                                           class="form-control nav-filter__input input-sm"
                                           name="text"
                                           onchange="TsCatalogSearch.changeLocation(this.form)">
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="container container--wide">
            <!-- Items -->
            <div class="responsive-container-list catalog-listview">
                <div data-key="6496">
                    <div class="responsive-container">
                        <div class="designer-card designer-card--ps-cat">
                            <div class="designer-card__ps-rating">
                                <div id="star-rating-block-98939" class="star-rating-block" itemprop="aggregateRating"
                                     itemscope="" itemtype="http://schema.org/AggregateRating">
                                    <meta itemprop="ratingValue" content="4.9">
                                    <meta itemprop="worstRating" content="1">
                                    <meta itemprop="bestRating" content="5">
                                    <div class="hidden" itemprop="itemReviewed" itemscope=""
                                         itemtype="https://schema.org/Organization"><img itemprop="image"
                                                                                         src="https://static.treatstock.com/static/user/dc2ff5f1be7effb525b145bed225e336/ps_logo_circle_1570252425_720x540.jpg"
                                                                                         alt="Prozix"><span itemprop="name">Prozix </span>
                                    </div>
                                    <div class="star-rating rating-xs rating-disabled">
                                        <div class="rating-container tsi" data-content="">
                                            <div class="rating-stars" data-content="" style="width: 98%;"></div>
                                            <input value="4.9" type="text" class="star-rating form-control hide" data-min="0"
                                                   data-max="5" data-step="0.1" data-size="xs" data-symbol=""
                                                   data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>
                                    </div>
                                    <span class="star-rating-range"><strong>4.9</strong>/5</span><span
                                            class="star-rating-count"><a href="/c/prozix/reviews">(<span itemprop="reviewCount">115</span> reviews)</a></span>
                                </div>
                            </div>

                            <div class="designer-card__userinfo"><a class="designer-card__avatar" href="/c/prozix"
                                                                    target="_blank"><img
                                            src="https://static.treatstock.com/static/user/dc2ff5f1be7effb525b145bed225e336/ps_logo_circle_1570252425_64x64.jpg"
                                            alt="Prozix" title="Prozix"></a>
                                <h3 class="designer-card__username"><a href="/c/prozix" target="_blank" title="Prozix">
                                        Prozix </a></h3></div>
                            <div class="designer-card__ps-pics">
                                <div id="w2" class="designer-card__ps-portfolio swiper-container swiper-container-horizontal"
                                     style="cursor: grab;">
                                    <div class="swiper-wrapper"><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active"
                                                href="https://static.treatstock.com/static/files/2d/b4/115097_11240_466aa5ed506cf8ba6f561e425f271e71_1200x1200.png?date=1504429507"
                                                data-lightbox="w2"><img
                                                    src="https://static.treatstock.com/static/files/2d/b4/115097_11240_466aa5ed506cf8ba6f561e425f271e71_160x90.png?date=1504429507"
                                                    alt="Prozix 3D printing photo" title="Prozix 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next"
                                                href="https://static.treatstock.com/static/files/01/99/115098_11240_50bb877476c4a89e1e384db85564b5bd_1200x1200.png?date=1504429521"
                                                data-lightbox="w2"><img
                                                    src="https://static.treatstock.com/static/files/01/99/115098_11240_50bb877476c4a89e1e384db85564b5bd_160x90.png?date=1504429521"
                                                    alt="Prozix 3D printing photo" title="Prozix 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide"
                                                href="https://static.treatstock.com/static/files/c7/e4/115099_11240_5a8000cf2526a2976de6c71f4ac8cc98_1200x1200.png?date=1504429528"
                                                data-lightbox="w2"><img
                                                    src="https://static.treatstock.com/static/files/c7/e4/115099_11240_5a8000cf2526a2976de6c71f4ac8cc98_160x90.png?date=1504429528"
                                                    alt="Prozix 3D printing photo" title="Prozix 3D printing photo"></a></div>
                                    <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;">
                                        <div class="swiper-scrollbar-drag" style="width: 194.832px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="designer-card__ps-loc" title="Long Beach, CA, US"><span
                                        class="tsi tsi-map-marker"></span>
                                Long Beach, CA, US
                            </div>
                            <div class="designer-card__about">
                                Professional 3d printing with more than 7 years of printing experience in architecture model...
                            </div>
                            <div class="row" style="min-height: 75px">
                                <div class="col-sm-12">
                                    <div class="designer-card__data m-b10"><h4 class="m-b0 m-t0">Manufacturing Service</h4><span
                                                class="badge designer-card__data-badge">3D Printing</span><span
                                                class="badge designer-card__data-badge"></span></div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Materials:</span>
                                        Resin
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Shipping:</span>
                                        Standard Flat Rate
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="designer-card__btn-block"><a class="btn btn-danger btn-sm"
                                                                             href="/my/print-model3d?utm_source=public_ps&amp;posUid=fixedPs&amp;psId=989">Instant
                                            order </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-key="5248">
                    <div class="responsive-container">
                        <div class="designer-card designer-card--ps-cat">
                            <div class="designer-card__ps-rating">
                                <div id="star-rating-block-15702" class="star-rating-block" itemprop="aggregateRating"
                                     itemscope="" itemtype="http://schema.org/AggregateRating">
                                    <meta itemprop="ratingValue" content="4.8">
                                    <meta itemprop="worstRating" content="1">
                                    <meta itemprop="bestRating" content="5">
                                    <div class="hidden" itemprop="itemReviewed" itemscope=""
                                         itemtype="https://schema.org/Organization"><img itemprop="image"
                                                                                         src="https://static.treatstock.com/static/user/d1cb4bd2c18fe96d6f82f7b60d4aa0a0/ps_logo_circle_1563996729_720x540.jpg"
                                                                                         alt="RexRoi"><span itemprop="name">RexRoi </span>
                                    </div>
                                    <div class="star-rating rating-xs rating-disabled">
                                        <div class="rating-container tsi" data-content="">
                                            <div class="rating-stars" data-content="" style="width: 96%;"></div>
                                            <input value="4.8" type="text" class="star-rating form-control hide" data-min="0"
                                                   data-max="5" data-step="0.1" data-size="xs" data-symbol=""
                                                   data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>
                                    </div>
                                    <span class="star-rating-range"><strong>4.8</strong>/5</span><span
                                            class="star-rating-count"><a href="/c/rexroi/reviews">(<span itemprop="reviewCount">259</span> reviews)</a></span>
                                </div>
                            </div>

                            <div class="designer-card__userinfo"><a class="designer-card__avatar" href="/c/rexroi"
                                                                    target="_blank"><img
                                            src="https://static.treatstock.com/static/user/d1cb4bd2c18fe96d6f82f7b60d4aa0a0/ps_logo_circle_1563996729_64x64.jpg"
                                            alt="RexRoi" title="RexRoi"></a>
                                <h3 class="designer-card__username"><a href="/c/rexroi" target="_blank" title="RexRoi">
                                        RexRoi </a></h3></div>
                            <div class="designer-card__ps-pics">
                                <div id="w3" class="designer-card__ps-portfolio swiper-container swiper-container-horizontal"
                                     style="cursor: grab;">
                                    <div class="swiper-wrapper"
                                         style="transform: translate3d(-31px, 0px, 0px); transition-duration: 0ms;"><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active"
                                                href="https://static.treatstock.com/static/files/4c/57/492206_37203_ecf8e49057fc5dd2638f4951ee04400e_1200x1200.jpg?date=1545212222"
                                                data-lightbox="w3"><img
                                                    src="https://static.treatstock.com/static/files/4c/57/492206_37203_ecf8e49057fc5dd2638f4951ee04400e_160x90.jpg?date=1545212222"
                                                    alt="RexRoi 3D printing photo" title="RexRoi 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next"
                                                href="https://static.treatstock.com/static/files/b9/33/492209_37203_a9d9efd2912be2fe546fb4972f009132_1200x1200.jpeg?date=1545212222"
                                                data-lightbox="w3"><img
                                                    src="https://static.treatstock.com/static/files/b9/33/492209_37203_a9d9efd2912be2fe546fb4972f009132_160x90.jpeg?date=1545212222"
                                                    alt="RexRoi 3D printing photo" title="RexRoi 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide"
                                                href="https://static.treatstock.com/static/files/e3/22/492207_37203_7bea0d634f6eb09603df289aa020ecba_1200x1200.jpeg?date=1545212222"
                                                data-lightbox="w3"><img
                                                    src="https://static.treatstock.com/static/files/e3/22/492207_37203_7bea0d634f6eb09603df289aa020ecba_160x90.jpeg?date=1545212222"
                                                    alt="RexRoi 3D printing photo" title="RexRoi 3D printing photo"></a></div>
                                    <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"
                                         style="opacity: 0; transition-duration: 400ms;">
                                        <div class="swiper-scrollbar-drag"
                                             style="transform: translate3d(28.4636px, 0px, 0px); transition-duration: 0ms; width: 274.536px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="designer-card__ps-loc" title="Los Angeles, California, US"><span
                                        class="tsi tsi-map-marker"></span>
                                Los Angeles, California, US
                            </div>
                            <div class="designer-card__about">
                                Here at RexRoi, We do everything we can to make sure you are happy...
                            </div>
                            <div class="row" style="min-height: 75px">
                                <div class="col-sm-12">
                                    <div class="designer-card__data m-b10"><h4 class="m-b0 m-t0">Manufacturing Service</h4><span
                                                class="badge designer-card__data-badge">3D Printing</span><span
                                                class="badge designer-card__data-badge"></span></div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Materials:</span>
                                        Resin, Durable (PP-like) Resin, Glass-reinforced Resin +1 material
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Shipping:</span>
                                        Standard Flat Rate
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="designer-card__btn-block"><a class="btn btn-danger btn-sm"
                                                                             href="/my/print-model3d?utm_source=public_ps&amp;posUid=fixedPs&amp;psId=2083">Instant
                                            order </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-key="3128">
                    <div class="responsive-container">
                        <div class="designer-card designer-card--ps-cat">
                            <div class="designer-card__ps-rating">
                                <div id="star-rating-block-94841" class="star-rating-block" itemprop="aggregateRating"
                                     itemscope="" itemtype="http://schema.org/AggregateRating">
                                    <meta itemprop="ratingValue" content="4.7">
                                    <meta itemprop="worstRating" content="1">
                                    <meta itemprop="bestRating" content="5">
                                    <div class="hidden" itemprop="itemReviewed" itemscope=""
                                         itemtype="https://schema.org/Organization"><img itemprop="image"
                                                                                         src="https://static.treatstock.com/static/user/3fc2c60b5782f641f76bcefc39fb2392/ps_logo_circle_1482253158_720x540.png"
                                                                                         alt="Jinxbot 3D Printing"><span
                                                itemprop="name">Jinxbot 3D Printing </span></div>
                                    <div class="star-rating rating-xs rating-disabled">
                                        <div class="rating-container tsi" data-content="">
                                            <div class="rating-stars" data-content="" style="width: 94%;"></div>
                                            <input value="4.7" type="text" class="star-rating form-control hide" data-min="0"
                                                   data-max="5" data-step="0.1" data-size="xs" data-symbol=""
                                                   data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>
                                    </div>
                                    <span class="star-rating-range"><strong>4.7</strong>/5</span><span
                                            class="star-rating-count"><a href="/c/jinxbot-3d-printing/reviews">(<span
                                                    itemprop="reviewCount">3</span> reviews)</a></span></div>
                            </div>

                            <div class="designer-card__userinfo"><a class="designer-card__avatar" href="/c/jinxbot-3d-printing"
                                                                    target="_blank"><img
                                            src="https://static.treatstock.com/static/user/3fc2c60b5782f641f76bcefc39fb2392/ps_logo_circle_1482253158_64x64.png"
                                            alt="Jinxbot 3D Printing" title="Jinxbot 3D Printing"></a>
                                <h3 class="designer-card__username"><a href="/c/jinxbot-3d-printing" target="_blank"
                                                                       title="Jinxbot 3D Printing">
                                        Jinxbot 3D Printing </a></h3></div>
                            <div class="designer-card__ps-pics">
                                <div id="w4" class="designer-card__ps-portfolio swiper-container swiper-container-horizontal"
                                     style="cursor: grab;">
                                    <div class="swiper-wrapper"><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active"
                                                href="https://static.treatstock.com/static/files/25/b2/19434_2825_ce18b7a9763afeb2005dac2e37e115c4_1200x1200.jpg?date=1482253158"
                                                data-lightbox="w4"><img
                                                    src="https://static.treatstock.com/static/files/25/b2/19434_2825_ce18b7a9763afeb2005dac2e37e115c4_160x90.jpg?date=1482253158"
                                                    alt="Jinxbot 3D Printing 3D printing photo"
                                                    title="Jinxbot 3D Printing 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next"
                                                href="https://static.treatstock.com/static/files/25/b2/19435_2825_c729766b6c3954bc62b0edb223c1b9b6_1200x1200.jpg?date=1482253158"
                                                data-lightbox="w4"><img
                                                    src="https://static.treatstock.com/static/files/25/b2/19435_2825_c729766b6c3954bc62b0edb223c1b9b6_160x90.jpg?date=1482253158"
                                                    alt="Jinxbot 3D Printing 3D printing photo"
                                                    title="Jinxbot 3D Printing 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide"
                                                href="https://static.treatstock.com/static/files/25/b2/19436_2825_5e8a0a5a460f52534d11117ede8bb16c_1200x1200.jpg?date=1482253158"
                                                data-lightbox="w4"><img
                                                    src="https://static.treatstock.com/static/files/25/b2/19436_2825_5e8a0a5a460f52534d11117ede8bb16c_160x90.jpg?date=1482253158"
                                                    alt="Jinxbot 3D Printing 3D printing photo"
                                                    title="Jinxbot 3D Printing 3D printing photo"></a></div>
                                    <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;">
                                        <div class="swiper-scrollbar-drag" style="width: 247.195px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="designer-card__ps-loc" title="MOUNTAIN VIEW, California, US"><span
                                        class="tsi tsi-map-marker"></span>
                                MOUNTAIN VIEW, California, US
                            </div>
                            <div class="designer-card__about">
                                Quality is my priority. I run a top of the line Makerbot Replicator 5th...
                            </div>
                            <div class="row" style="min-height: 75px">
                                <div class="col-sm-12">
                                    <div class="designer-card__data m-b10"><h4 class="m-b0 m-t0">Manufacturing Service</h4><span
                                                class="badge designer-card__data-badge">3D Printing</span><span
                                                class="badge designer-card__data-badge"></span></div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Materials:</span>
                                        Resin, Tough Resin, High Temp Resin
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Shipping:</span>
                                        Standard Flat Rate
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="designer-card__btn-block"><a class="btn btn-danger btn-sm"
                                                                             href="/my/print-model3d?utm_source=public_ps&amp;posUid=fixedPs&amp;psId=473">Instant
                                            order </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-key="2934">
                    <div class="responsive-container">
                        <div class="designer-card designer-card--ps-cat">
                            <div class="designer-card__ps-rating">
                                <div id="star-rating-block-30836" class="star-rating-block" itemprop="aggregateRating"
                                     itemscope="" itemtype="http://schema.org/AggregateRating">
                                    <meta itemprop="ratingValue" content="5.0">
                                    <meta itemprop="worstRating" content="1">
                                    <meta itemprop="bestRating" content="5">
                                    <div class="hidden" itemprop="itemReviewed" itemscope=""
                                         itemtype="https://schema.org/Organization"><img itemprop="image"
                                                                                         src="https://static.treatstock.com/static/user/95c14ff9ca2c5d8ead4a18b1e830d504/ps_logo_circle_1531354056_720x540.png"
                                                                                         alt="ODMS"><span
                                                itemprop="name">ODMS </span></div>
                                    <div class="star-rating rating-xs rating-disabled">
                                        <div class="rating-container tsi" data-content="">
                                            <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                            <input value="5.0" type="text" class="star-rating form-control hide" data-min="0"
                                                   data-max="5" data-step="0.1" data-size="xs" data-symbol=""
                                                   data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>
                                    </div>
                                    <span class="star-rating-range"><strong>5</strong>/5</span><span
                                            class="star-rating-count"><a href="/c/odms/reviews">(<span
                                                    itemprop="reviewCount">1</span> review)</a></span></div>
                            </div>
                            <div class="cert-label cert-label--not-tested" data-toggle="tooltip" data-placement="bottom"
                                 title="" data-original-title="This printer is not certified yet"></div>
                            <div class="designer-card__userinfo"><a class="designer-card__avatar" href="/c/odms"
                                                                    target="_blank"><img
                                            src="https://static.treatstock.com/static/user/95c14ff9ca2c5d8ead4a18b1e830d504/ps_logo_circle_1531354056_64x64.png"
                                            alt="ODMS" title="ODMS"></a>
                                <h3 class="designer-card__username"><a href="/c/odms" target="_blank" title="ODMS">
                                        ODMS </a></h3></div>
                            <div class="designer-card__ps-pics">
                                <div id="w5" class="designer-card__ps-portfolio swiper-container swiper-container-horizontal"
                                     style="cursor: grab;">
                                    <div class="swiper-wrapper"><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active"
                                                href="https://static.treatstock.com/static/files/7c/de/306876_32932_65b1a6e4302d6ea845e6efc082652841_1200x1200.png?date=1531452443"
                                                data-lightbox="w5"><img
                                                    src="https://static.treatstock.com/static/files/7c/de/306876_32932_65b1a6e4302d6ea845e6efc082652841_160x90.png?date=1531452443"
                                                    alt="ODMS 3D printing photo" title="ODMS 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next"
                                                href="https://static.treatstock.com/static/files/6d/d4/306877_32932_fdc09408537e87066d1925628198a0cf_1200x1200.jpg?date=1531452443"
                                                data-lightbox="w5"><img
                                                    src="https://static.treatstock.com/static/files/6d/d4/306877_32932_fdc09408537e87066d1925628198a0cf_160x90.jpg?date=1531452443"
                                                    alt="ODMS 3D printing photo" title="ODMS 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide"
                                                href="https://static.treatstock.com/static/files/05/ae/306878_32932_9ed71795a93747202b7d8454f8a21b84_1200x1200.jpg?date=1531452443"
                                                data-lightbox="w5"><img
                                                    src="https://static.treatstock.com/static/files/05/ae/306878_32932_9ed71795a93747202b7d8454f8a21b84_160x90.jpg?date=1531452443"
                                                    alt="ODMS 3D printing photo" title="ODMS 3D printing photo"></a></div>
                                    <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;">
                                        <div class="swiper-scrollbar-drag" style="width: 171.337px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="designer-card__ps-loc" title="San Leandro, California, US"><span
                                        class="tsi tsi-map-marker"></span>
                                San Leandro, California, US
                            </div>
                            <div class="designer-card__about">
                                Parts On-Demand Platform
                                ODMS' PRINT-TO-CAST PLATFORM PRODUCES PARTS IN BRONZE, BRASS, ALUMINUM AND STAINLESS...
                            </div>
                            <div class="row" style="min-height: 75px">
                                <div class="col-sm-12">
                                    <div class="designer-card__data m-b10"><h4 class="m-b0 m-t0">Manufacturing Service</h4><span
                                                class="badge designer-card__data-badge">3D Printing</span><span
                                                class="badge designer-card__data-badge">Manufacturing Services</span><span
                                                class="badge designer-card__data-badge"></span></div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Materials:</span>
                                        Resin
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Shipping:</span>
                                        Standard Flat Rate
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="designer-card__btn-block"><a class="btn btn-danger btn-sm"
                                                                             href="/my/print-model3d?utm_source=public_ps&amp;posUid=fixedPs&amp;psId=1883">Instant
                                            order </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-key="3513">
                    <div class="responsive-container">
                        <div class="designer-card designer-card--ps-cat">
                            <div class="designer-card__ps-rating"><span class="text-muted">Be the first to leave a review</span>
                            </div>
                            <div class="cert-label cert-label--not-tested" data-toggle="tooltip" data-placement="bottom"
                                 title="" data-original-title="This printer is not certified yet"></div>
                            <div class="designer-card__userinfo"><a class="designer-card__avatar" href="/c/bd3dcustoms"
                                                                    target="_blank"><img
                                            src="https://static.treatstock.com/static/user/87876ca731b4c56b3f48110c953e0401/ps_logo_circle_1543126762_64x64.jpg"
                                            alt="BD3DCUSTOMS" title="BD3DCUSTOMS"></a>
                                <h3 class="designer-card__username"><a href="/c/bd3dcustoms" target="_blank"
                                                                       title="BD3DCUSTOMS">
                                        BD3DCUSTOMS </a></h3></div>
                            <div class="designer-card__ps-pics">
                                <div id="w6" class="designer-card__ps-portfolio swiper-container swiper-container-horizontal"
                                     style="cursor: grab;">
                                    <div class="swiper-wrapper"><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active"
                                                href="https://static.treatstock.com/static/files/a3/d9/467975_42155_113ea78d84888ee5f00f1e1a15a9b3ab_1200x1200.jpg?date=1543593487"
                                                data-lightbox="w6"><img
                                                    src="https://static.treatstock.com/static/files/a3/d9/467975_42155_113ea78d84888ee5f00f1e1a15a9b3ab_160x90.jpg?date=1543593487"
                                                    alt="BD3DCUSTOMS 3D printing photo"
                                                    title="BD3DCUSTOMS 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next"
                                                href="https://static.treatstock.com/static/files/bc/f6/467976_42155_6970393d321dfc10856fbfc128cb1fac_1200x1200.jpg?date=1543593487"
                                                data-lightbox="w6"><img
                                                    src="https://static.treatstock.com/static/files/bc/f6/467976_42155_6970393d321dfc10856fbfc128cb1fac_160x90.jpg?date=1543593487"
                                                    alt="BD3DCUSTOMS 3D printing photo"
                                                    title="BD3DCUSTOMS 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide"
                                                href="https://static.treatstock.com/static/files/f0/cb/467977_42155_050aaeeb1288032f66709ec7da78d12b_1200x1200.jpg?date=1543593487"
                                                data-lightbox="w6"><img
                                                    src="https://static.treatstock.com/static/files/f0/cb/467977_42155_050aaeeb1288032f66709ec7da78d12b_160x90.jpg?date=1543593487"
                                                    alt="BD3DCUSTOMS 3D printing photo"
                                                    title="BD3DCUSTOMS 3D printing photo"></a></div>
                                    <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"
                                         style="display: none; opacity: 0;">
                                        <div class="swiper-scrollbar-drag" style="width: 0px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="designer-card__ps-loc" title="Bear River City, Utah, US"><span
                                        class="tsi tsi-map-marker"></span>
                                Bear River City, Utah, US
                            </div>
                            <div class="designer-card__about">
                                3d printing and rapid prototyping
                            </div>
                            <div class="row" style="min-height: 75px">
                                <div class="col-sm-12">
                                    <div class="designer-card__data m-b10"><h4 class="m-b0 m-t0">Manufacturing Service</h4><span
                                                class="badge designer-card__data-badge">3D Printing</span><span
                                                class="badge designer-card__data-badge"></span></div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Materials:</span>
                                        Resin
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Shipping:</span>
                                        $10.00
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="designer-card__btn-block"><a class="btn btn-danger btn-sm"
                                                                             href="/my/print-model3d?utm_source=public_ps&amp;posUid=fixedPs&amp;psId=2324">Instant
                                            order </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-key="3923">
                    <div class="responsive-container">
                        <div class="designer-card designer-card--ps-cat">
                            <div class="designer-card__ps-rating">
                                <div id="star-rating-block-10170" class="star-rating-block" itemprop="aggregateRating"
                                     itemscope="" itemtype="http://schema.org/AggregateRating">
                                    <meta itemprop="ratingValue" content="5.0">
                                    <meta itemprop="worstRating" content="1">
                                    <meta itemprop="bestRating" content="5">
                                    <div class="hidden" itemprop="itemReviewed" itemscope=""
                                         itemtype="https://schema.org/Organization"><img itemprop="image"
                                                                                         src="https://static.treatstock.com/static/user/18018116065e7e68946ed1d42c0fde2f/ps_logo_circle_1558501520_720x540.png"
                                                                                         alt="Bespoke"><span itemprop="name">Bespoke </span>
                                    </div>
                                    <div class="star-rating rating-xs rating-disabled">
                                        <div class="rating-container tsi" data-content="">
                                            <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                            <input value="5.0" type="text" class="star-rating form-control hide" data-min="0"
                                                   data-max="5" data-step="0.1" data-size="xs" data-symbol=""
                                                   data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>
                                    </div>
                                    <span class="star-rating-range"><strong>5</strong>/5</span><span
                                            class="star-rating-count"><a href="/c/bespoke/reviews">(<span
                                                    itemprop="reviewCount">2</span> reviews)</a></span></div>
                            </div>

                            <div class="designer-card__userinfo"><a class="designer-card__avatar" href="/c/bespoke"
                                                                    target="_blank"><img
                                            src="https://static.treatstock.com/static/user/18018116065e7e68946ed1d42c0fde2f/ps_logo_circle_1558501520_64x64.png"
                                            alt="Bespoke" title="Bespoke"></a>
                                <h3 class="designer-card__username"><a href="/c/bespoke" target="_blank" title="Bespoke">
                                        Bespoke </a></h3></div>
                            <div class="designer-card__ps-pics">
                                <div id="w7" class="designer-card__ps-portfolio swiper-container swiper-container-horizontal"
                                     style="cursor: grab;">
                                    <div class="swiper-wrapper"><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active"
                                                href="https://static.treatstock.com/static/files/64/f9/646660_51898_6cda6a7206b0bcf1e90a3609c5695275_1200x1200.jpg?date=1554209357"
                                                data-lightbox="w7"><img
                                                    src="https://static.treatstock.com/static/files/64/f9/646660_51898_6cda6a7206b0bcf1e90a3609c5695275_160x90.jpg?date=1554209357"
                                                    alt="Bespoke 3D printing photo" title="Bespoke 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next"
                                                href="https://static.treatstock.com/static/files/40/57/646661_51898_04c85ba8524c1a7c5f2621290e0b0168_1200x1200.jpg?date=1554209357"
                                                data-lightbox="w7"><img
                                                    src="https://static.treatstock.com/static/files/40/57/646661_51898_04c85ba8524c1a7c5f2621290e0b0168_160x90.jpg?date=1554209357"
                                                    alt="Bespoke 3D printing photo" title="Bespoke 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide"
                                                href="https://static.treatstock.com/static/files/7b/8c/646671_51898_1f3adca3aa651fe45032fa2c5229ea6a_1200x1200.jpg?date=1554210694"
                                                data-lightbox="w7"><img
                                                    src="https://static.treatstock.com/static/files/7b/8c/646671_51898_1f3adca3aa651fe45032fa2c5229ea6a_160x90.jpg?date=1554210694"
                                                    alt="Bespoke 3D printing photo" title="Bespoke 3D printing photo"></a></div>
                                    <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;">
                                        <div class="swiper-scrollbar-drag" style="width: 209.329px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="designer-card__ps-loc" title="Bend, Oregon, US"><span class="tsi tsi-map-marker"></span>
                                Bend, Oregon, US
                            </div>
                            <div class="designer-card__about">
                                Bespoke is a 3d printing service located in Bend, Oregon. We leverage SLA technology...
                            </div>
                            <div class="row" style="min-height: 75px">
                                <div class="col-sm-12">
                                    <div class="designer-card__data m-b10"><h4 class="m-b0 m-t0">Manufacturing Service</h4><span
                                                class="badge designer-card__data-badge">3D Printing</span><span
                                                class="badge designer-card__data-badge"></span></div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Materials:</span>
                                        Resin, Tough Resin, Durable (PP-like) Resin +1 material
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Shipping:</span>
                                        Standard Flat Rate
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="designer-card__btn-block"><a class="btn btn-danger btn-sm"
                                                                             href="/my/print-model3d?utm_source=public_ps&amp;posUid=fixedPs&amp;psId=2658">Instant
                                            order </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-key="2305">
                    <div class="responsive-container">
                        <div class="designer-card designer-card--ps-cat">
                            <div class="designer-card__ps-rating">
                                <div id="star-rating-block-22397" class="star-rating-block" itemprop="aggregateRating"
                                     itemscope="" itemtype="http://schema.org/AggregateRating">
                                    <meta itemprop="ratingValue" content="5.0">
                                    <meta itemprop="worstRating" content="1">
                                    <meta itemprop="bestRating" content="5">
                                    <div class="hidden" itemprop="itemReviewed" itemscope=""
                                         itemtype="https://schema.org/Organization"><img itemprop="image"
                                                                                         src="https://static.treatstock.com/static/files/19/84/348044_0_c2415a1450ca9debcf9bbe20ea8a2261_720x540.png?date=1534951211"
                                                                                         alt="The Boulder Protoshop"><span
                                                itemprop="name">The Boulder Protoshop </span></div>
                                    <div class="star-rating rating-xs rating-disabled">
                                        <div class="rating-container tsi" data-content="">
                                            <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                            <input value="5.0" type="text" class="star-rating form-control hide" data-min="0"
                                                   data-max="5" data-step="0.1" data-size="xs" data-symbol=""
                                                   data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>
                                    </div>
                                    <span class="star-rating-range"><strong>5</strong>/5</span><span
                                            class="star-rating-count"><a href="/c/brians-print-shop/reviews">(<span
                                                    itemprop="reviewCount">4</span> reviews)</a></span></div>
                            </div>

                            <div class="designer-card__userinfo"><a class="designer-card__avatar" href="/c/brians-print-shop"
                                                                    target="_blank"><img
                                            src="https://static.treatstock.com/static/files/19/84/348044_0_c2415a1450ca9debcf9bbe20ea8a2261_64x64.png?date=1534951211"
                                            alt="The Boulder Protoshop" title="The Boulder Protoshop"></a>
                                <h3 class="designer-card__username"><a href="/c/brians-print-shop" target="_blank"
                                                                       title="The Boulder Protoshop">
                                        The Boulder Protoshop </a></h3></div>
                            <div class="designer-card__ps-pics">
                                <div id="w8" class="designer-card__ps-portfolio swiper-container swiper-container-horizontal"
                                     style="cursor: grab;">
                                    <div class="swiper-wrapper"><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active"
                                                href="https://static.treatstock.com/static/files/c9/04/204421_17541_f2111a2f2f769b57c9a69d8b1f44fd16_1200x1200.jpg?date=1518039238"
                                                data-lightbox="w8"><img
                                                    src="https://static.treatstock.com/static/files/c9/04/204421_17541_f2111a2f2f769b57c9a69d8b1f44fd16_160x90.jpg?date=1518039238"
                                                    alt="The Boulder Protoshop 3D printing photo"
                                                    title="The Boulder Protoshop 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next"
                                                href="https://static.treatstock.com/static/files/95/75/204423_17541_7630a92a8e0002da9e9d28e2a220fe33_1200x1200.jpg?date=1518039369"
                                                data-lightbox="w8"><img
                                                    src="https://static.treatstock.com/static/files/95/75/204423_17541_7630a92a8e0002da9e9d28e2a220fe33_160x90.jpg?date=1518039369"
                                                    alt="The Boulder Protoshop 3D printing photo"
                                                    title="The Boulder Protoshop 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide"
                                                href="https://static.treatstock.com/static/files/bc/c5/204424_17541_82fc014056257f888a83b720acd7b29a_1200x1200.jpg?date=1518039369"
                                                data-lightbox="w8"><img
                                                    src="https://static.treatstock.com/static/files/bc/c5/204424_17541_82fc014056257f888a83b720acd7b29a_160x90.jpg?date=1518039369"
                                                    alt="The Boulder Protoshop 3D printing photo"
                                                    title="The Boulder Protoshop 3D printing photo"></a></div>
                                    <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"
                                         style="display: none; opacity: 0;">
                                        <div class="swiper-scrollbar-drag" style="width: 0px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="designer-card__ps-loc" title="Lafayette, Colorado, US"><span
                                        class="tsi tsi-map-marker"></span>
                                Lafayette, Colorado, US
                            </div>
                            <div class="designer-card__about">
                                DLP, Laser SLA, FDM, Laser cutting, Laser engraving, CNC routing, Design, and other services...
                            </div>
                            <div class="row" style="min-height: 75px">
                                <div class="col-sm-12">
                                    <div class="designer-card__data m-b10"><h4 class="m-b0 m-t0">Manufacturing Service</h4><span
                                                class="badge designer-card__data-badge">3D Printing</span><span
                                                class="badge designer-card__data-badge"></span><span
                                                class="badge designer-card__data-badge">Cutting</span></div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Materials:</span>
                                        Resin, Flexible Resin
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Shipping:</span>
                                        $15.00
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="designer-card__btn-block"><a class="btn btn-danger btn-sm"
                                                                             href="/my/print-model3d?utm_source=public_ps&amp;posUid=fixedPs&amp;psId=1285">Instant
                                            order </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-key="1394">
                    <div class="responsive-container">
                        <div class="designer-card designer-card--ps-cat">
                            <div class="designer-card__ps-rating"><span class="text-muted">Be the first to leave a review</span>
                            </div>
                            <div class="cert-label cert-label--common" data-toggle="tooltip" data-placement="bottom" title=""
                                 data-original-title="This printer has a 'Common Certificate' that confirms it is capable of printing average models to a high quality.">
                                <span class="tsi tsi-checkmark"></span></div>
                            <div class="designer-card__userinfo"><a class="designer-card__avatar"
                                                                    href="/c/professional-3d-printing-with-3dv" target="_blank"><img
                                            src="https://static.treatstock.com/static/user/dff70232bcd2396f4a6c7bb57dab3e87/ps_logo_circle_1501527955_64x64.jpg"
                                            alt="Professional 3D Printing with 3DV"
                                            title="Professional 3D Printing with 3DV"></a>
                                <h3 class="designer-card__username"><a href="/c/professional-3d-printing-with-3dv"
                                                                       target="_blank"
                                                                       title="Professional 3D Printing with 3DV">
                                        Professional 3D Printing with 3DV </a></h3></div>
                            <div class="designer-card__ps-pics">
                                <div id="w9" class="designer-card__ps-portfolio swiper-container swiper-container-horizontal"
                                     style="cursor: grab;">
                                    <div class="swiper-wrapper"><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active"
                                                href="https://static.treatstock.com/static/files/a8/97/99787_10167_972201f662153513f9f6f161aa801d64_1200x1200.jpg?date=1501553155"
                                                data-lightbox="w9"><img
                                                    src="https://static.treatstock.com/static/files/a8/97/99787_10167_972201f662153513f9f6f161aa801d64_160x90.jpg?date=1501553155"
                                                    alt="Professional 3D Printing with 3DV 3D printing photo"
                                                    title="Professional 3D Printing with 3DV 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next"
                                                href="https://static.treatstock.com/static/files/2b/3d/99788_10167_530b331d7d7f14a4a3043f0143869e7d_1200x1200.jpg?date=1501553155"
                                                data-lightbox="w9"><img
                                                    src="https://static.treatstock.com/static/files/2b/3d/99788_10167_530b331d7d7f14a4a3043f0143869e7d_160x90.jpg?date=1501553155"
                                                    alt="Professional 3D Printing with 3DV 3D printing photo"
                                                    title="Professional 3D Printing with 3DV 3D printing photo"></a><a
                                                class="designer-card__ps-portfolio-item swiper-slide"
                                                href="https://static.treatstock.com/static/files/7c/42/99789_10167_60254131aa6c4783f203cee2a30d356e_1200x1200.jpg?date=1501553156"
                                                data-lightbox="w9"><img
                                                    src="https://static.treatstock.com/static/files/7c/42/99789_10167_60254131aa6c4783f203cee2a30d356e_160x90.jpg?date=1501553156"
                                                    alt="Professional 3D Printing with 3DV 3D printing photo"
                                                    title="Professional 3D Printing with 3DV 3D printing photo"></a></div>
                                    <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;">
                                        <div class="swiper-scrollbar-drag" style="width: 231.115px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="designer-card__ps-loc" title="Portland, Oregon, US"><span
                                        class="tsi tsi-map-marker"></span>
                                Portland, Oregon, US
                            </div>
                            <div class="designer-card__about">
                                We offer everything from rough prints to fully sanded and primed pieces. If you...
                            </div>
                            <div class="row" style="min-height: 75px">
                                <div class="col-sm-12">
                                    <div class="designer-card__data m-b10"><h4 class="m-b0 m-t0">Manufacturing Service</h4><span
                                                class="badge designer-card__data-badge">3D Printing</span><span
                                                class="badge designer-card__data-badge"></span></div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Materials:</span>
                                        Resin
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="designer-card__data"><span class="designer-card__data-label">Shipping:</span>
                                        Standard Flat Rate
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="designer-card__btn-block"><a class="btn btn-danger btn-sm"
                                                                             href="/my/print-model3d?utm_source=public_ps&amp;posUid=fixedPs&amp;psId=880">Instant
                                            order </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <a href="#showMore" class="btn btn-primary m-b30">
                        <?= _t('front', 'Show more'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="customer-reviews">
        <div class="container container--wide">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="customer-reviews__title animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'What our customers are saying'); ?>
                    </h2>
                </div>
            </div>
        </div>

        <div class="customer-reviews__slider swiper-container">
            <div class="swiper-wrapper customer-reviews__gallery">

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                    </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                        The print was an excellent job. A fast print, but more importantly are really good quality. I am a happy customer and would buy again.
                    </span>
                    <img class="customer-reviews__avatar" src="https://secure.gravatar.com/avatar/e83e2496ad31501002461f799301c8bb?r=g&s=64&d=retro">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">mazzemotto</span>
                        <span class="customer-reviews__user-date">Jan 11, 2021</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                    <div id="star-rating-block-13955" class="star-rating-block">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                    Like always, the quality is unreal. Great communication, open and forthright, definitely one of the best!
                </span>
                    <img class="customer-reviews__avatar" src="https://secure.gravatar.com/avatar/75d9273ff688b23a26baeb1d242bf61b?r=g&s=64&d=retro">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">helgish</span>
                        <span class="customer-reviews__user-date">Jan 6, 2021</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                    <div id="star-rating-block-13955" class="star-rating-block">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                    Very happy with the delivered parts, exactly as ordered.
                </span>
                    <img class="customer-reviews__avatar" src="https://static.treatstock.com/static/user/google_114025802608639125611/avatar_64x64.jpg">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">m</span>
                        <span class="customer-reviews__user-date">Jan 6, 2021</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                    <div id="star-rating-block-13955" class="star-rating-block">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                    Really good quality and quick shipping. Would highly recommend!
                </span>
                    <img class="customer-reviews__avatar" src="https://static.treatstock.com/static/user/google_104722062749600656013/avatar_64x64.jpg">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">blacksadify</span>
                        <span class="customer-reviews__user-date">Dec 29, 2020</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                    <div id="star-rating-block-13955" class="star-rating-block">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                    Excellent work. Completely satisfied and would hire this printer again. Bravo.
                </span>
                    <img class="customer-reviews__avatar" src="https://static.treatstock.com/static/user/0095699549f68634cbf32d574ebb27aa/avatar_1589347905_64x64.jpg">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">shintownalle</span>
                        <span class="customer-reviews__user-date">Dec 1, 2020</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                    </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                        Great as always
                    </span>
                    <img class="customer-reviews__avatar" src="https://secure.gravatar.com/avatar/a7b114b45308910612758f50301bd9fe?r=g&s=64&d=retro">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">E150</span>
                        <span class="customer-reviews__user-date">Dec 1, 2020</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                    </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                        Very knowledgeable and great pricing. Fast shipping and fast communication. Will use for future projects.
                    </span>
                    <img class="customer-reviews__avatar" src="https://secure.gravatar.com/avatar/34a794a7208a92504c91e24c991d5f73?r=g&s=64&d=retro">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">mazzemotto</span>
                        <span class="customer-reviews__user-date">Nov 24, 2020</span>
                    </div>
                </div>

            </div>

            <div class="customer-reviews__scrollbar swiper-scrollbar"></div>
        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="m-t30">
                    <?= _t('front', 'SEO text block'); ?>
                </h2>
                <p><?= _t('front', 'Seo text goes here.'); ?></p>
            </div>
        </div>
    </div>


    <script>
        <?php $this->beginBlock('js1', false); ?>

        function initPrints() {
            //Init slider for .customer-reviews__slider
            var swiperPrints = new Swiper('.customer-reviews__slider', {
                scrollbar: '.customer-reviews__scrollbar',
                scrollbarHide: true,
                slidesPerView: 'auto',
                grabCursor: true
            });
        }

        initPrints();
        $(window).resize(function () {initPrints()});


        <?php $this->endBlock(); ?>
    </script>
<?php $this->registerJs($this->blocks['js1']); ?>