<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

$this->title = "Independence Day";

?>

<style>
    .promopage-header {
        width: 100%;
        max-height: 540px;
        margin: 0 0 10px;
        padding: 10% 0;
        background: url("https://static.treatstock.com/static/uploads/ck_20160622_4thjuly.jpg") no-repeat bottom center transparent;
        background-size: cover;
    }
    .promopage-header__title {
        margin: 0;
        color: #ffffff;
        font-size: 50px;
        line-height: 50px;
        font-weight: bold;
    }
    .catalog-item__category,
    .catalog-item__controls,
    .catalog-item__footer__stats__likes {
        display: none !important;
    }
    .second-header {
        text-align: center;
    }
    .second-header:last-child{
        margin-bottom: 30px;
    }
    @media (max-width: 400px) {
        .promopage-header__title {
            font-size: 20px;
            line-height: 30px;
        }
        .second-header {
            margin: 0 0 10px;
            font-size: 14px;
            line-height: 20px;
            text-align: left;
        }
        .second-header:last-child{
            margin: 0 0 10px;
        }
    }
</style>

<div class="promopage-header">
    <div class="container">
        <h1 class="promopage-header__title">Happy Birthday America!</h1>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
            <blockquote class="m-t30">
                <p>It ought to be solemnized with pomp and parade, with shows, games, sports, guns, bells, bonfires, and illuminations, from one end of this continent to the other, from this time forward forever more.</p>

                <small><cite title="Source Title">John Adams</cite></small>
            </blockquote>
        </div>
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <h3 class="second-header">Let 3D printing help you to make these words come true.<br>Happy Independence Day!</h3>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="841" data-model3d-id="986" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="https://www.treatstock.com/3d-printable-models?category=11">
                            Art                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="https://www.treatstock.com/u/tomislav-veg" class="catalog-item__author__avatar">
                            <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/avatar_64x64.jpg" alt="">                </a>
                        <a href="https://www.treatstock.com/u/tomislav-veg" class="catalog-item__author__name">
                            Tomislav                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>





                    </div>

                    <a class="catalog-item__pic__img" href="https://www.treatstock.com/3dmodels/986" title="Obama &quot;not bad&quot; meme sculpture">
                        <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/model986/5213_412x309.jpg" alt="Obama &quot;not bad&quot; meme sculpture">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="https://www.treatstock.com/3dmodels/986">Obama "not bad" meme sculpture</a></h4>

                    <div class="catalog-item__footer__price">
                        $17.20            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count js-store-item-likes-first">Like it first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="218" data-model3d-id="301" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="https://www.treatstock.com/3d-printable-models?category=21">
                            Miniatures                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="https://www.treatstock.com/u/MiniWorld" class="catalog-item__author__avatar">
                            <img src="https://static.treatstock.com/static/user/4122cb13c7a474c1976c9706ae36521d/avatar_64x64.png" alt="">                </a>
                        <a href="https://www.treatstock.com/u/MiniWorld" class="catalog-item__author__name">
                            MiniWorld                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>





                    </div>

                    <a class="catalog-item__pic__img" href="https://www.treatstock.com/3dmodels/301" title="Colorado State Capitol, USA">
                        <img src="https://static.treatstock.com/static/user/4122cb13c7a474c1976c9706ae36521d/model301/1771_412x309.jpg" alt="Colorado State Capitol, USA">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="https://www.treatstock.com/3dmodels/301">Colorado State Capitol, USA</a></h4>

                    <div class="catalog-item__footer__price">
                        $7.69            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">1 like</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="497" data-model3d-id="623" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="https://www.treatstock.com/3d-printable-models?category=15">
                            Games                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="https://www.treatstock.com/u/StyMakc" class="catalog-item__author__avatar">
                            <img src="https://static.treatstock.com/static/user/299a23a2291e2126b91d54f3601ec162/avatar_64x64.jpg" alt="">                </a>
                        <a href="https://www.treatstock.com/u/StyMakc" class="catalog-item__author__name">
                            Styazhkin                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>





                    </div>

                    <a class="catalog-item__pic__img" href="https://www.treatstock.com/3dmodels/623" title="Captain America's shield">
                        <img src="https://static.treatstock.com/static/user/299a23a2291e2126b91d54f3601ec162/model623/3154_412x309.png" alt="Captain America's shield">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="https://www.treatstock.com/3dmodels/623">Captain America's shield</a></h4>

                    <div class="catalog-item__footer__price">
                        $1.00            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">5 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="94" data-model3d-id="113" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="https://www.treatstock.com/3d-printable-models?category=21">
                            Miniatures                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="https://www.treatstock.com/u/Rasih" class="catalog-item__author__avatar">
                            <img src="https://secure.gravatar.com/avatar/ddb060bd4bbdbe9b8c3742b2840d5e73?r=g&amp;s=64&amp;d=retro" alt="Gravatar image">                </a>
                        <a href="https://www.treatstock.com/u/Rasih" class="catalog-item__author__name">
                            Rasih                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>





                    </div>

                    <a class="catalog-item__pic__img" href="https://www.treatstock.com/3dmodels/113" title="Statue of Liberty">
                        <img src="https://static.treatstock.com/static/user/456ac9b0d15a8b7f1e71073221059886/model113/5277_720x540.jpg" alt="Statue of Liberty">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="https://www.treatstock.com/3dmodels/113">Statue of Liberty</a></h4>

                    <div class="catalog-item__footer__price">
                        $2.99            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">2 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="729" data-model3d-id="877" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="https://www.treatstock.com/3d-printable-models?category=12">
                            Fashion                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="https://www.treatstock.com/u/MaxCooper" class="catalog-item__author__avatar">
                            <img src="https://static.treatstock.com/static/user/82b8a3434904411a9fdc43ca87cee70c/avatar_64x64.jpg" alt="">                </a>
                        <a href="https://www.treatstock.com/u/MaxCooper" class="catalog-item__author__name">
                            Max                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>





                    </div>

                    <a class="catalog-item__pic__img" href="https://www.treatstock.com/3dmodels/877" title="Earring for Independence Day">
                        <img src="https://static.treatstock.com/static/user/82b8a3434904411a9fdc43ca87cee70c/model877/4477_412x309.png" alt="Earring for Independence Day">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="https://www.treatstock.com/3dmodels/877">Earring for Independence Day</a></h4>

                    <div class="catalog-item__footer__price">
                        $1.00            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">4 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="728" data-model3d-id="876" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="https://www.treatstock.com/3d-printable-models?category=11">
                            Art                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="https://www.treatstock.com/u/MaxCooper" class="catalog-item__author__avatar">
                            <img src="https://static.treatstock.com/static/user/82b8a3434904411a9fdc43ca87cee70c/avatar_64x64.jpg" alt="">                </a>
                        <a href="https://www.treatstock.com/u/MaxCooper" class="catalog-item__author__name">
                            Max                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>





                    </div>

                    <a class="catalog-item__pic__img" href="https://www.treatstock.com/3dmodels/876" title="Statue of Liberty Crown">
                        <img src="https://static.treatstock.com/static/user/82b8a3434904411a9fdc43ca87cee70c/model876/4474_412x309.png" alt="Statue of Liberty Crown">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="https://www.treatstock.com/3dmodels/876">Statue of Liberty Crown</a></h4>

                    <div class="catalog-item__footer__price">
                        $13.80            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">5 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="605" data-model3d-id="751" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="https://www.treatstock.com/3d-printable-models?category=11">
                            Art                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="https://www.treatstock.com/u/tomislav-veg" class="catalog-item__author__avatar">
                            <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/avatar_64x64.jpg" alt="">                </a>
                        <a href="https://www.treatstock.com/u/tomislav-veg" class="catalog-item__author__name">
                            Tomislav                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>





                    </div>

                    <a class="catalog-item__pic__img" href="https://www.treatstock.com/3dmodels/751" title="Hillary  Clinton">
                        <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/model751/3850_412x309.jpg" alt="Hillary  Clinton">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="https://www.treatstock.com/3dmodels/751">Hillary  Clinton</a></h4>

                    <div class="catalog-item__footer__price">
                        $10.08            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">1 like</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="585" data-model3d-id="727" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="https://www.treatstock.com/3d-printable-models?category=11">
                            Art                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="https://www.treatstock.com/u/3dartdigital" class="catalog-item__author__avatar">
                            <img src="https://static.treatstock.com/static/user/a424ed4bd3a7d6aea720b86d4a360f75/avatar_64x64.jpg" alt="">                </a>
                        <a href="https://www.treatstock.com/u/3dartdigital" class="catalog-item__author__name">
                            3dartdigit                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>





                    </div>

                    <a class="catalog-item__pic__img" href="https://www.treatstock.com/3dmodels/727" title="Sheriff Bird piggy bank">
                        <img src="https://static.treatstock.com/static/user/a424ed4bd3a7d6aea720b86d4a360f75/model727/3565_412x309.jpg" alt="Sheriff Bird piggy bank">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="https://www.treatstock.com/3dmodels/727">Sheriff Bird piggy bank</a></h4>

                    <div class="catalog-item__footer__price">
                        $50.78            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">4 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="604" data-model3d-id="750" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="https://www.treatstock.com/3d-printable-models?category=11">
                            Art                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="https://www.treatstock.com/u/tomislav-veg" class="catalog-item__author__avatar">
                            <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/avatar_64x64.jpg" alt="">                </a>
                        <a href="https://www.treatstock.com/u/tomislav-veg" class="catalog-item__author__name">
                            Tomislav                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>





                    </div>

                    <a class="catalog-item__pic__img" href="https://www.treatstock.com/3dmodels/750" title="Donald  Trump">
                        <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/model750/3853_412x309.jpg" alt="Donald  Trump">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="https://www.treatstock.com/3dmodels/750">Donald  Trump</a></h4>

                    <div class="catalog-item__footer__price">
                        $10.97            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">2 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>