<?php

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);

$this->title = "Start 3D Print Service";

?>

<?= $this->render('/common/ie-alert.php'); ?>

<div class="container-fliud ps-land ps-land-top">
    <div class="container">
        <div class="row relative">
            <div class="col-xs-12">
                <h3 class="h1 ps-land-top__heading">
                    <?= _t('front.user', 'Do you own a 3D printer, CNC or Cutting machine?'); ?>
                </h3>
            </div>
            <div class="col-sm-12 col-lg-8">
                <h1 class="ps-land-top__slogan"><?= _t('front.user', 'Start your service and receive orders to print 3D models, CAD files and cutting service.'); ?></h1>
                <a href="<?= is_guest()?'#':\Yii::$app->urlManager->createUrl("/mybusiness/company/create-ps"); ?>" class="btn btn-primary m-b20"
                    <?php if (is_guest()): ?>
                        onclick="TS.Visitor.loginForm('<?=\Yii::$app->urlManager->createUrl("/mybusiness/company/create-ps");?>');"
                    <?php endif; ?>>
                    <?= _t('site.ps', 'Start Service'); ?>
                </a>
            </div>
            <div class="col-sm-7">
                <p class="ps-land-top__desk"><?= _t('front.user', 'Register your print service and join our global 3D printing network on Treatstock.'); ?></p>
            </div>
            <img class="ps-land-top__pic col-sm-5" src="/static/images/ps-welcome-service.png">
        </div>
    </div>
</div>

<div class="container-fliud ps-land ps-land-why">
    <div class="container">
        <h2 class="ps-land-why__title">
            <?= _t('site.ps', 'Why Treatstock?'); ?>
        </h2>
        <div class="responsive-container-list">
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/feefree.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'No Transaction Fees'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'You as a service pay a 0% transaction fee on payouts for your orders.'); ?>
                </p>
            </div>
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/security.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'Financial Security'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'Customers pay for orders using our online payment system, giving you the financial security to know that you will be paid for your work.'); ?>
                </p>
            </div>
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/receipt.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'Order Receipts'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'All orders come with receipts in the form of e-statements which can be downloaded by your customers.'); ?>
                </p>
            </div>
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/time.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'Save Time'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'Orders are processed automatically and all communication is only with paying customers, allowing you to save time.'); ?>
                </p>
            </div>
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/money.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'Save Money'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'The automated process of the widget allows your business to run more efficiently.'); ?>
                </p>
            </div>
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/calc.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'Price Calculation'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'Customers will have access to fast and accurate information about the cost of printing their 3D model with your print service.'); ?>
                </p>
            </div>
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/instant.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'Instant Orders'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'Orders and payments are made directly through the widget without the need for emails, phone calls or confirmations.'); ?>
                </p>
            </div>
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/customer.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'Customer Tools'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'Customers can upload and view their files as a 3D render in different materials, colors and sizes to gain a better understanding of what they want to order.'); ?>
                </p>
            </div>
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/formats.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'Multiple Formats'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'Various formats of 3D models are supported, including the most popular - STL and PLY.'); ?>
                </p>
            </div>
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/tech.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'Range of Technologies'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'Our website works with a wide range of technologies including all your 3D printers.'); ?>
                </p>
            </div>
            <div class="responsive-container">
                <img class="ps-land-why__icon" src="/static/images/widget-page-icons/promo.svg" alt="">
                <h3 class="ps-land-why__subtitle">
                    <?= _t('site.ps', 'Promotional Tool'); ?>
                </h3>
                <p class="ps-land-why__text">
                    <?= _t('site.ps', 'Your current and potential customers will get the latest information about your print service and your professional achievements.'); ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container-fliud ps-land ps-land-reviews">
    <div class="container">
        <h2 class="ps-land-reviews__title">
            <?= _t('site.ps', 'What Print Services are Saying'); ?>
        </h2>
        <div class="row">
            <div class="col-xs-12">
                <div class="ps-land-reviews__swiper swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img class="ps-land-reviews__avatar" src="https://static.treatstock.com/static/user/fcdf25d6e191893e705819b177cddea0/avatar_1478208742_240x240.jpg">
                            <div class="ps-land-reviews__info">
                                <h4 class="ps-land-reviews__ps-name">
                                    <a href="/c/eds-printing" target="_blank">Ed's Printing PS</a>
                                </h4>
                                <div class="ps-land-reviews__ps-owner">
                                    Edward Fries
                                </div>
                                <p class="ps-land-reviews__text">
                                    "<?= _t('site.ps', 'I have had a wonderful experience printing on Treatstock! I average about 3-4 print jobs per week and their support services have been very helpful when I have had issues printing a model. Most importantly, what I earn for my services goes directly into my Treastock account. There are never any extra service fees from Treastock, my earnings are mine!'); ?>"
                                </p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <img class="ps-land-reviews__avatar" src="https://static.treatstock.com/static/user/c3614206a443012045cfd75d2600af2d/ps_logo_1476310929_240x240.jpg">
                            <div class="ps-land-reviews__info">
                                <h4 class="ps-land-reviews__ps-name">
                                    <a href="/c/jwcproductions" target="_blank">Jwcproductions PS</a>
                                </h4>
                                <div class="ps-land-reviews__ps-owner">
                                    Jim Carter
                                </div>
                                <p class="ps-land-reviews__text">
                                    "<?= _t('site.ps', 'Treatstock is great to deal with and have lots of great local printers! The registration process is much better than the competitors out there and I also like that there is a "Pro" level of printer certification.'); ?>"
                                </p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <img class="ps-land-reviews__avatar" src="https://static.treatstock.com/static/user/0e087ec55dcbe7b2d7992d6b69b519fb/ps_logo_1479425253_240x240.jpg">
                            <div class="ps-land-reviews__info">
                                <h4 class="ps-land-reviews__ps-name">
                                    <a href="/c/dylco3d" target="_blank">DYLCO3D PS</a>
                                </h4>
                                <div class="ps-land-reviews__ps-owner">
                                    Dylan Wallis
                                </div>
                                <p class="ps-land-reviews__text">
                                    "<?= _t('site.ps', 'I like the fact that treatstock.com can connect my print service with customers all over the world, from just a few states away to other countries.'); ?>"
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="ps-land-reviews__pagination swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h2 class="ps-land-reviews__join">
                <?= _t('site.ps', 'Join Us Now'); ?>
            </h2>
            <a href="<?= is_guest()?'#':\Yii::$app->urlManager->createUrl("/mybusiness/company/create-ps"); ?>" class="btn btn-primary btn-lg m-b20"
                <?php if (is_guest()): ?>
                    onclick="TS.Visitor.loginForm('<?=\Yii::$app->urlManager->createUrl("/mybusiness/company/create-ps");?>');"
                <?php endif; ?>>
                <?= _t('site.ps', 'Get Started'); ?>
            </a>
        </div>
    </div>
</div>

<script>
    <?php $this->beginBlock('js1', false); ?>

    //Init slider for .main-page-promo
    var swiperPsLand = new Swiper('.ps-land-reviews__swiper', {
        pagination: '.ps-land-reviews__pagination',
        paginationClickable: true,
        slidesPerView: 1,
        spaceBetween: 30,
        grabCursor: true,
        autoplay: 5000,
        speed: 700,
        loop: true,

        breakpoints: {
            600: {
                slidesPerView: 1
            }
        }
    });

    <?php $this->endBlock(); ?>
</script>
<?php $this->registerJs($this->blocks['js1']); ?>