<?php

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);


Yii::$app->angular
    ->service(['notify', 'router', 'user'])
    ->controller('promo/PromoContactUsController')
    ->controllerParam('type', 'business')
?>

<style>
    .biz-page-head {
        position: relative;
        margin-bottom: 20px;
        padding: 90px 0 90px;
        color: #ffffff;
        background: rgba(43, 43, 51, 0.8);

    }
    .biz-page-head:after {
        content: '';
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: url("https://static.treatstock.com/static/images/common/business-page/head-bg-min.jpg") no-repeat 50% 50% transparent;
        background-size: cover;
    }
    .biz-page-head__title {
        margin-top: 0;
        line-height: 45px;
    }
    .biz-page-head__subtitle {
        margin-bottom: 30px;
        font-size: 18px;
        line-height: 25px;
    }
    @media (max-width: 550px) {
        .biz-page-head {
            padding: 50px 0;
        }
        .biz-page-head__title {
            font-size: 30px;
            line-height: 40px;
        }
    }

    .biz-page-collaborate {
        padding: 60px 0;
    }
    .biz-page-collaborate__list {
        padding: 0;
        list-style: none;
        display: flex;
        flex-flow: wrap;
    }
    .biz-page-collaborate__list > li {
        position: relative;
        margin-bottom: 5px;
        padding-left: 25px;
        font-size: 18px;
        line-height: 25px;
        flex: 1 0 auto;
    }
    .biz-page-collaborate__list > li > .tsi {
        position: absolute;
        top: 3px;
        left: 0;
        display: block;
        color: #00bf00;
    }
    @media (max-width: 991px) {
        .biz-page-collaborate__list > li {
            width: 50%;
        }
    }
    @media (max-width: 700px) {
        .biz-page-collaborate__list > li {
            width: 100%;
        }
    }
    .biz-page-collaborate__text {
        margin-top: 40px;
        margin-bottom: 15px;
        font-style: italic;
        font-size: 16px;
        line-height: 25px;
    }
    .biz-page-collaborate__avatar {
        float: left;
        width: 50px;
        height: 50px;
        margin-right: 20px;
        border-radius: 50%;
    }
    .biz-page-collaborate__username {
        font-size: 16px;
        font-weight: bold;
        line-height: 25px;
        overflow: hidden;
    }
    .biz-page-collaborate__company {
        font-size: 16px;
        color: #707478;
        overflow: hidden;
    }

    .biz-page-manager {
        padding: 80px 0;
        background: #f6f8fa;
    }
    .biz-page-manager__card {
        position: relative;
        height: 400px;
        box-shadow: 0 10px 30px rgba(0, 54, 153, 0.15);
        border-radius: 5px;
        overflow: hidden;
        background: url("https://static.treatstock.com/static/images/common/business-page/manager-min.jpg") no-repeat left bottom #ffffff;
    }
    .biz-page-manager__text {
        position: absolute;
        top: 140px;
        right: 0;
        width: 100%;
        max-width: 500px;
        padding: 0 20px;
        font-size: 18px;
        line-height: 30px;
    }
    .biz-page-manager__text h3 {
        margin: 0 0 15px;
        font-size: 30px;
    }
    @media (max-width: 991px) {
        .biz-page-manager__text h3 {
            margin-bottom: 10px;
        }
        .biz-page-manager__text {
            top: auto;
            bottom: 0;
            left: 0;
            max-width: 100%;
            padding: 30px 15px 0;
            background-image: linear-gradient(0deg, rgba(242, 242, 255, 0.85) 0%, rgba(242, 242, 255, 0) 100%);
        }
    }
    @media (max-width: 767px) {
        .biz-page-manager__text {
            font-size: 14px;
            line-height: 20px;
        }
    }
    @media (max-width: 500px) {
        .biz-page-manager__card {
            height: 320px;
            background-size: cover;
        }
    }

    .biz-page-benefits {
        padding: 60px 0 50px;
    }
    .biz-page-title {
        margin: 0 0 40px;
        padding: 0;
        text-align: center;
    }
    .biz-page-list {
        margin: 0 -10px;
        font-size: 0;
    }
    .biz-page-card {
        position: relative;
        display: inline-block;
        vertical-align: top;
        width: calc(33.3333% - 20px);
        margin: 0 10px 30px;
        border-radius: 5px;
        font-size: 14px;
        text-align: left;
    }
    .biz-page-icon {
        width: 75px;
        height: 75px;
    }
    .biz-page-card__title {
        margin: 15px 0;
        font-size: 20px;
        font-weight: 100;
    }
    .biz-page-card__text {}
    @media (max-width: 550px) {
        .biz-page-list {
            margin: 0 -15px;
            padding: 10px 5px;
            white-space: nowrap;
            overflow: auto;
            -webkit-overflow-scrolling: touch;
        }
        .biz-page-card {
            width: calc(80% - 20px);
            margin-bottom: 0;
            white-space: normal;
        }
    }
    @media (min-width: 550px) and (max-width: 991px) {
        .biz-page-card {
            width: calc(50% - 20px);
        }
    }
    .biz-page-cta {}

    .biz-page-feedback {}

    .biz-page-vs {
        padding: 80px 0 40px;
    }
    .biz-page-vs__block {
        max-width: 430px;
        margin: 0 auto;
    }
    .biz-page-vs h4 {
        margin: 20px 0;
        font-size: 20px;
        line-height: 25px;
    }
    .biz-page-vs__block > ul {
        margin-bottom: 40px;
        font-size: 18px;
        color: #606468;
    }
    .biz-page-vs__block > ul > li {
        position: relative;
        margin-bottom: 15px;
        padding: 10px 0 10px 55px;
    }
    .biz-page-vs__block > ul > li:last-child {
        margin-bottom: 0;
    }
    .biz-page-vs__block p {
        margin: 15px 0 0;
        font-size: 14px;
    }
    .biz-page-vs__block > ul > li ul {
        list-style: disc;
        margin: 15px 0 0;
        padding: 0;
        font-size: 14px;
    }
    .biz-page-vs__block > ul > li ul li {
        margin-bottom: 10px;
    }
    .biz-page-vs__block > ul > li ul li:last-child {
        margin-bottom: 0;
    }
    .biz-page-vs__mark {
        position: absolute;
        left: 0;
        top: 0;
        display: inline-block;
        vertical-align: middle;
        width: 40px;
        height: 40px;
        margin-right: 15px;
        line-height: 40px;
        text-align: center;
        border-radius: 50%;
        box-shadow: 0 3px 10px rgba(0, 54, 153, 0.2);
    }
    .biz-page-vs__mark--poz {
        color: #00bf00;
    }
    .biz-page-vs__mark--neg {
        font-size: 14px;
        color: #ff5967;
    }
    .biz-page-vs__mark > .tsi {
        font-weight: bold;
    }

    .biz-page-suppliers {
        padding: 80px 0 40px;
        background: #f0f4f8;
    }
    .designer-card--suplier {
        margin-bottom: 40px;
        background: #ffffff;
        border-radius: 5px;
    }
    @media (max-width: 767px) {
        .designer-card--suplier .designer-card__about > br {
            display: none;
        }
    }

    .biz-page-contact {
        padding: 80px 0 70px;
        margin-bottom: -40px;
        color: #fff;
        background-color: #0068b3;
        background-image: linear-gradient(20deg, #006a80 0%, #550080 100%);
    }
    .biz-page-contact__form {
        margin-top: 30px;
    }
    .biz-page-contact__form .btn {
        margin: 10px 0 0;
    }
    .biz-page-contact__form .form-control {
        background: rgba(0, 0, 0, 0.4);
        box-shadow: none;
        border-color: transparent;
        color: #ffffff;
        transition: all 0.3s ease;
    }
    .biz-page-contact__form .form-control:focus {
        border-color: rgba(0, 0, 0, 0);
        box-shadow: none;
        background: #fff;
        color: #404448;
    }
    .biz-page-contact__form .form-control::-webkit-input-placeholder {color: #868fbd;}
    .biz-page-contact__form .form-control::-moz-placeholder {color: #868fbd;}
    .biz-page-contact__form .form-control:-ms-input-placeholder {color: #868fbd;}
    .biz-page-contact__form .form-control:-moz-placeholder {color: #868fbd;}

    .biz-page-contact__form label {
        color: rgba(255, 255, 255, 0.8);
    }
    @media (max-width: 500px) {
        .biz-page-contact {
            padding: 60px 0;
        }
    }

    .biz-page-contact .btn-info {
        color: #1E88E5;
        background-color: #ffffff;
        border-color: transparent;
    }
    .biz-page-contact .btn-info:hover {
        background-color: rgba(255, 255, 255, 0.85);
    }
</style>

<section class="biz-page-head">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-md-8">
                <h1 class="biz-page-head__title"><?= _t('site.page', 'Rapid Manufacturing Solutions for Business'); ?></h1>
                <p class="biz-page-head__subtitle"><?= _t('site.page', 'Treatstock is a B2B marketplace where manufacturing companies provide a personalized approach to producing quality products for corporate clients and a global customer base'); ?></p>
                <a href="#contactForm" data-target="#contactForm" class="btn btn-primary"><?= _t('site.page', 'Contact Sales'); ?></a>
            </div>
        </div>
    </div>
</section>

<section class="biz-page-collaborate">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="m-t0"><?= _t('site.page', 'Among our clients are'); ?>:</h2>
                <ul class="biz-page-collaborate__list">
                    <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Science labs'); ?></li>
                    <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Universities'); ?></li>
                    <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Architects and design services'); ?></li>
                    <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Small & medium business'); ?></li>
                </ul>
            </div>
            <?php /*
            <div class="col-sm-6">
                <div class="biz-page-collaborate__text">
                    Мы используем Третсток и пипец как довольны. Печатаем через сервисы всякие штуки в огромных количествах.
                    Постоянно есть поддержка, которая быстро решает все вопросы.
                </div>
                <img class="biz-page-collaborate__avatar" src="https://randomuser.me/api/portraits/men/82.jpg" alt="">
                <div class="biz-page-collaborate__username">Kakoyto Kakoytovich</div>
                <div class="biz-page-collaborate__company">CEO in Uber Lab Technology Inc.</div>
            </div>
            */?>
        </div>
    </div>
</section>

<section class="biz-page-benefits">
    <div class="container">
        <h2 class="biz-page-title"><?= _t('site.page', 'How can Treatstock help your business?'); ?></h2>

        <div class="biz-page-list">
            <div class="biz-page-card">
                <img class="biz-page-icon" src="https://static.treatstock.com/static/images/common/big-icons/instant.svg" alt="">
                <h4 class="biz-page-card__title">
                    <?= _t('front', 'Instant Quotes'); ?>
                </h4>
                <p class="biz-page-card__text">
                    <?= _t('front', 'At a touch of a button you can instantly view thousands of manufacturing offers. Compare prices and reviews to help find the best deals online.'); ?>
                </p>
            </div>
            <div class="biz-page-card">
                <img class="biz-page-icon" src="https://static.treatstock.com/static/images/common/big-icons/wallet.svg" alt="">
                <h4 class="biz-page-card__title">
                    <?= _t('front', 'Save Money'); ?>
                </h4>
                <p class="biz-page-card__text">
                    <?= _t('front', 'Competition between vendors leads to lower prices and can provide savings of more than 50% on quotes sourced elsewhere.'); ?>
                </p>
            </div>
            <div class="biz-page-card">
                <img class="biz-page-icon" src="https://static.treatstock.com/static/images/common/big-icons/save.svg" alt="">
                <h4 class="biz-page-card__title">
                    <?= _t('front', 'Save Time'); ?>
                </h4>
                <p class="biz-page-card__text">
                    <?= _t('front', 'Find professional manufacturers in the shortest time and place orders quickly and easily with minimal fuss.'); ?>
                </p>
            </div>
            <div class="biz-page-card">
                <img class="biz-page-icon" src="https://static.treatstock.com/static/images/common/big-icons/guarantee.svg" alt="">
                <h4 class="biz-page-card__title">
                    <?= _t('front', 'Quality Assurance'); ?>
                </h4>
                <p class="biz-page-card__text">
                    <?= _t('front', 'All manufacturers are reliable and have passed stringent quality testing. Order results are uploaded for inspection before products are shipped.'); ?>
                </p>
            </div>
            <div class="biz-page-card">
                <img class="biz-page-icon" src="https://static.treatstock.com/static/images/common/big-icons/financial-security.svg" alt="">
                <h4 class="biz-page-card__title">
                    <?= _t('front', 'Financial Security'); ?>
                </h4>
                <p class="biz-page-card__text">
                    <?= _t('front', 'Place orders using our secure online payment system where payment will only be released after the dispute period has elapsed.'); ?>
                </p>
            </div>
            <div class="biz-page-card">
                <img class="biz-page-icon" src="https://static.treatstock.com/static/images/common/big-icons/business-tools.svg" alt="">
                <h4 class="biz-page-card__title">
                    <?= _t('front', 'Business Tools'); ?>
                </h4>
                <p class="biz-page-card__text">
                    <?= _t('front', 'Gain access to privileges such as intellectual property protection, financial tools (assistance) and commercial invoices for large orders.'); ?>
                </p>
            </div>
        </div>

        <p class="text-center">
            <a href="#contactForm" data-target="#contactForm" class="btn btn-danger biz-page-cta m-b30"><?= _t('front', 'Contact Sales'); ?></a>
        </p>
    </div>
</section>

<section class="biz-page-manager">
    <div class="container">
        <div class="biz-page-manager__card">
            <div class="biz-page-manager__text">
                <h3><?= _t('site.page', 'Personal Manager'); ?></h3>
                <p><?= _t('site.page', 'You will be assigned a personal manager to control the production process of your order. They are always on hand to answer your questions and help in any other way.'); ?></p>
            </div>
        </div>
    </div>
</section>

<section class="biz-page-vs">
    <div class="container">
        <h2 class="biz-page-title"><?= _t('site.page', 'Regular vendors vs Treatstock'); ?></h2>

        <div class="row">
            <div class="col-sm-6">
                <div class="biz-page-vs__block">
                    <h4><?= _t('site.page', 'Regular Vendors'); ?></h4>
                    <ul class="list-unstyled">
                        <li><span class="biz-page-vs__mark biz-page-vs__mark--neg"><span class="tsi tsi-remove"></span></span>Long Quote Times
                            <p>Long wait times for a quote</p>
                        </li>
                        <li><span class="biz-page-vs__mark biz-page-vs__mark--neg"><span class="tsi tsi-remove"></span></span>Poor Quality
                            <p>No guarantees on quality</p>
                        </li>
                        <li><span class="biz-page-vs__mark biz-page-vs__mark--neg"><span class="tsi tsi-remove"></span></span>Loss of Money
                            <p>Lack of competition means services are overpriced</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="biz-page-vs__block">
                    <h4><?= _t('site.page', 'Treatstock Manufacturing Marketplace'); ?></h4>
                    <ul class="list-unstyled">
                        <li><span class="biz-page-vs__mark biz-page-vs__mark--poz"><span class="tsi tsi-checkmark"></span></span>Freedom of Choice
                            <ul>
                                <li>Get instant offers from a large number of vendors. Compare prices and order options.</li>
                                <li>Orders are placed and processed fast and efficiently</li>
                            </ul>
                        </li>
                        <li><span class="biz-page-vs__mark biz-page-vs__mark--poz"><span class="tsi tsi-checkmark"></span></span>Quality/Purchase Protection
                            <ul>
                                <li>Decide on reliability of services with the help of reviews​</li>
                                <li>Online support helps to check the quality of the finished product​</li>
                                <li>Refunds possible if the results are proven to not meet acceptable quality standards</li>
                            </ul>
                        </li>
                        <li><span class="biz-page-vs__mark biz-page-vs__mark--poz"><span class="tsi tsi-checkmark"></span></span>Save Money
                            <ul>
                                <li>Competition leads to lower prices, sometimes by more than 50%</li>
                                <li>Payment is carried out via escrow and transferred only after customer approval</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="biz-page-suppliers">
    <div class="container">
        <h2 class="biz-page-title"><?= _t('site.page', 'Professional Manufacturers'); ?></h2>

        <div class="row">
            <div class="col-sm-4">
                <div class="designer-card designer-card--suplier">
                    <div class="designer-card__userinfo">
                        <a class="designer-card__avatar" href="https://www.treatstock.com/c/sokar-mechanics" target="_blank">
                            <img src="https://static.treatstock.com/static/user/9acf1b76f369030270de8f98f84d6707/ps_logo_circle_1504003522_100x100.jpg" alt="SOKAR MECHANICS">
                        </a>
                        <h3 class="designer-card__username">
                            <a href="https://www.treatstock.com/c/sokar-mechanics" target="_blank" title="SOKAR MECHANICS">
                                SOKAR MECHANICS
                            </a>
                        </h3>
                    </div>

                    <div class="designer-card__about">
                        Specialist in prototyping and small series. We use the Selective Laser Melting process (Metals: Inox, &, Aluminum). Quick delivery time. Customers from automotive and aerospace sector.
                    </div>

                    <div class="designer-card__ps-pics">
                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/files/b9/bf/113091_10319_d9862ed8431d166904f80ae34353ccd0_720x540.png?date=1503992722"
                                   data-lightbox="s1">
                                    <img src="https://static.treatstock.com/static/files/b9/bf/113091_10319_d9862ed8431d166904f80ae34353ccd0_160x90.png?date=1503992722"
                                        alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                    href="https://static.treatstock.com/static/files/d7/e7/113092_10319_fd73837ecced1aa1628bb89e1dea0963_720x540.png?date=1503992722"
                                    data-lightbox="s1">
                                    <img src="https://static.treatstock.com/static/files/d7/e7/113092_10319_fd73837ecced1aa1628bb89e1dea0963_160x90.png?date=1503992722"
                                        alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                    href="https://static.treatstock.com/static/files/f2/be/113093_10319_9a74a39b21897afc48d0f8fc3335d857_720x540.png?date=1503992722"
                                    data-lightbox="s1">
                                    <img src="https://static.treatstock.com/static/files/f2/be/113093_10319_9a74a39b21897afc48d0f8fc3335d857_160x90.png?date=1503992722"
                                        alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                    href="https://static.treatstock.com/static/files/d5/f0/113094_10319_60cc1b11a0b4d6d3da45c55f15bd7d4a_720x540.jpg?date=1503992722"
                                    data-lightbox="s1">
                                    <img src="https://static.treatstock.com/static/files/d5/f0/113094_10319_60cc1b11a0b4d6d3da45c55f15bd7d4a_160x90.jpg?date=1503992722"
                                        alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>
                    </div>

                    <div class="designer-card__ps-loc" title="Canton, Ohio, US">
                        <span class="tsi tsi-map-marker"></span> Camas, Andalucía, ES
                    </div>

                    <div class="row designer-card__data">
                        <div class="col-sm-12 col-md-3 designer-card__data-label">Materials:</div>
                        <div class="col-sm-12 col-md-9">
                            Aluminium Alloy AlSi12, Stainless Steel 316L, Cobalt-Chrome CoCr
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="designer-card designer-card--suplier">
                    <div class="designer-card__userinfo">
                        <a class="designer-card__avatar" href="https://www.treatstock.com/c/make-it-3d-manufacturing-center" target="_blank">
                            <img src="https://static.treatstock.com/static/user/95f8d9901ca8878e291552f001f67692/ps_logo_circle_1476089404_100x100.png" alt="Make it 3D Manufacturing Center">
                        </a>
                        <h3 class="designer-card__username">
                            <a href="https://www.treatstock.com/c/make-it-3d-manufacturing-center" target="_blank" title="Make it 3D Manufacturing Center">
                                Make it 3D Manufacturing Center
                            </a>
                        </h3>
                    </div>

                    <div class="designer-card__about">
                        We specialize in industrial 3D printing, CNC machining and Casting and Molding Service with a quite competitive quality and price!
                        <br>
                        <br>
                    </div>

                    <div class="designer-card__ps-pics">
                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/files/71/d0/111513_2274_b40b6d172f4c7b1a4d03dd6ac8c309cf_720x540.jpg?date=1503614600"
                                   data-lightbox="s2">
                                    <img src="https://static.treatstock.com/static/files/71/d0/111513_2274_b40b6d172f4c7b1a4d03dd6ac8c309cf_160x90.jpg?date=1503614600"
                                         alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/files/a4/fd/111519_2274_789036aebb0c8da4d51aca21874d479c_720x540.jpg?date=1503614600"
                                   data-lightbox="s2">
                                    <img src="https://static.treatstock.com/static/files/a4/fd/111519_2274_789036aebb0c8da4d51aca21874d479c_160x90.jpg?date=1503614600"
                                         alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/files/01/a9/111520_2274_30006fab33aeeea589d18a3af6986a8e_720x540.jpg?date=1503614600"
                                   data-lightbox="s2">
                                    <img src="https://static.treatstock.com/static/files/01/a9/111520_2274_30006fab33aeeea589d18a3af6986a8e_160x90.jpg?date=1503614600"
                                         alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/files/0d/b9/111524_2274_d418daa1689d5b93a2f82897711af893_720x540.jpg?date=1503614600"
                                   data-lightbox="s2">
                                    <img src="https://static.treatstock.com/static/files/0d/b9/111524_2274_d418daa1689d5b93a2f82897711af893_160x90.jpg?date=1503614600"
                                         alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>
                    </div>

                    <div class="designer-card__ps-loc" title="Canton, Ohio, US">
                        <span class="tsi tsi-map-marker"></span> Dongguan Shi, Guangdong Sheng, CN
                    </div>

                    <div class="row designer-card__data">
                        <div class="col-sm-12 col-md-3 designer-card__data-label">Materials:</div>
                        <div class="col-sm-12 col-md-9">
                            Stainless Steel 316L, Resin, zp 150 High Performance Composite, Polyamide, PLA
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="designer-card designer-card--suplier">
                    <div class="designer-card__userinfo">
                        <a class="designer-card__avatar" href="https://www.treatstock.com/c/evolv3d" target="_blank">
                            <img src="https://static.treatstock.com/static/user/b4baaff0e2f11b5356193849021d641f/ps_logo_circle_1481573557_100x100.png" alt="Evolv3D">
                        </a>
                        <h3 class="designer-card__username">
                            <a href="https://www.treatstock.com/c/evolv3d" target="_blank" title="Evolv3D">
                                Evolv3D
                            </a>
                        </h3>
                    </div>

                    <div class="designer-card__about">
                        Build parts in many different metals and plastics. Processes include Selective Laser Sintering (SLS), ColorJet Printing (CJP), Stereolithography (SLA) & MultiJet (MJP) 3D Printing.
                    </div>

                    <div class="designer-card__ps-pics">
                        <div class="designer-card__ps-portfolio swiper-container">
                            <div class="swiper-wrapper">
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/files/5b/a2/47856_2484_743da437d7e59463c922bf3a54121872_720x540.jpg?date=1492421127"
                                   data-lightbox="s3">
                                    <img src="https://static.treatstock.com/static/files/5b/a2/47856_2484_743da437d7e59463c922bf3a54121872_160x90.jpg?date=1492421127"
                                         alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/files/cb/70/47858_2484_20eb5ea646fa0d175a5812cd0a2609aa_720x540.png?date=1492421214"
                                   data-lightbox="s3">
                                    <img src="https://static.treatstock.com/static/files/cb/70/47858_2484_20eb5ea646fa0d175a5812cd0a2609aa_160x90.png?date=1492421214"
                                         alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/files/08/49/47859_2484_275c5bd395ae5d7e371335f291458349_720x540.jpg?date=1492421214"
                                   data-lightbox="s3">
                                    <img src="https://static.treatstock.com/static/files/08/49/47859_2484_275c5bd395ae5d7e371335f291458349_160x90.jpg?date=1492421214"
                                         alt="">
                                </a>
                                <a class="designer-card__ps-portfolio-item swiper-slide"
                                   href="https://static.treatstock.com/static/files/7c/82/47853_2484_ffaf3a12259240d16b4cda98da223e27_720x540.jpg?date=1492420990"
                                   data-lightbox="s3">
                                    <img src="https://static.treatstock.com/static/files/7c/82/47853_2484_ffaf3a12259240d16b4cda98da223e27_160x90.jpg?date=1492420990"
                                         alt="">
                                </a>
                            </div>
                            <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                        </div>
                    </div>

                    <div class="designer-card__ps-loc" title="Canton, Ohio, US">
                        <span class="tsi tsi-map-marker"></span> Aurora, Ontario, CA
                    </div>

                    <div class="row designer-card__data">
                        <div class="col-sm-12 col-md-3 designer-card__data-label">Materials:</div>
                        <div class="col-sm-12 col-md-9">
                            VisiJet® M2 Rigid, VisiJet FTX, DuraForm PA Plastic, VisiJet M3, VisiJet M2 Elastomeric
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="biz-page-contact" id="contactForm" ng-controller="PromoContactUsController">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2 class="m-t0"><?= _t('site.page', 'Contact Us'); ?></h2>
                <p class="m-b30"><?= _t('site.page', 'A personal manager will be in touch with you as soon as possible'); ?></p>
            </div>
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <form class="row biz-page-contact__form">
                    <div class="col-sm-4 form-group">
                        <label class="control-label" for="contactform-name"><?= _t('site.page', 'Name'); ?></label>
                        <input ng-model="contactForm.name"
                               type="text" id="contactform-name" class="form-control" placeholder="<?= _t('site.page', 'Your name'); ?>">
                    </div>
                    <div class="col-sm-4 form-group">
                        <label class="control-label" for="contactform-email"><?= _t('site.page', 'Email'); ?></label>
                        <input ng-model="contactForm.email"
                               type="email" id="contactform-email" class="form-control" placeholder="<?= _t('site.page', 'Your work email'); ?>">
                    </div>
                    <div class="col-sm-4 form-group">
                        <label class="control-label" for="contactform-tel"><?= _t('site.page', 'Phone'); ?></label>
                        <input ng-model="contactForm.phone"
                               type="tel" id="contactform-tel" class="form-control" placeholder="<?= _t('site.page', 'Your work phone number'); ?>">
                    </div>
                    <div class="col-sm-12 form-group">
                        <label class="control-label" for="contactform-text"><?= _t('site.page', 'Details'); ?></label>
                        <textarea ng-model="contactForm.details"
                                  id="contactform-text" class="form-control" rows="3" placeholder="<?= _t('site.page', 'Tell us more about your needs'); ?>"></textarea>
                    </div>
                    <div class="col-sm-12 form-group m-b0">
                        <button
                                loader-click="sendContactForm()"
                                type="button" class="btn btn-info" name="contact-button"><?= _t('site.page', 'Send'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<script>
    <?php $this->beginBlock('js1', false); ?>

    $('a[data-target]').click(function () {
        var scroll_elTarget = $(this).attr('data-target');
        if ($(scroll_elTarget).length != 0) {
            $('html, body').animate({scrollTop: $(scroll_elTarget).offset().top - 105}, 350);
        }
        return false;
    });

    //Init slider for material examples
    var swiperPsPortfolio = new Swiper('.designer-card__ps-portfolio', {
        scrollbar: '.designer-card__ps-portfolio-scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>