<?php

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);


Yii::$app->angular
    ->service(['notify', 'router', 'user'])
    ->controller('promo/PromoContactUsController')
    ->controllerParam('type', 'partnership');
?>

<style>
    .promo-head {
        position: relative;
        padding: 90px 0 70px;
        color: #ffffff;
        background-color: #351159;
        background-image: linear-gradient(135deg, #351159 0%, #b64a5a 50%, #f8a24d 100%);
        overflow: hidden;
    }
    .promo-head__title {
        margin-top: 0;
        font-size: 50px;
        line-height: 55px;
    }
    .promo-head__subtitle {
        margin-bottom: 30px;
        font-size: 18px;
        line-height: 30px;
    }
    .promo-head__video {
        margin: 0 auto;
        max-width: 500px;
        box-shadow: 0 10px 30px rgba(0, 54, 153, 0.15);
        transform: matrix3d(1,-.07321269660432293,0,-.00058709452830785784,-.02185444687068156,.9628510184921463,0,-.00009375824129455574,0,0,1,0,-60,20,0,1);
        transition: all 0.3s ease;
    }
    .promo-head__video:hover {
        transform: scale(1);
    }
    @media (max-width: 620px) {
        .promo-head__video {
            transform: matrix3d(1,-.07321269660432293,0,-.00058709452830785784,-.02185444687068156,.9628510184921463,0,-.00009375824129455574,0,0,1,0,-20,20,0,1);
        }
    }
    .promo-head .btn-default {
        background: transparent;
        color: #ffffff;
        border-color: #ffffff;
    }
    @media (max-width: 550px) {
        .promo-head {
            padding: 50px 0;
        }
        .promo-head__title {
            margin-bottom: 20px;
            font-size: 30px;
            line-height: 40px;
        }
    }

    .promo-widget {
        padding: 60px 0 30px;
    }

    .promo-code {
        padding: 60px 0;
        background: #f6f8fa;
    }
    .promo-code .designer-card {
        margin-bottom: 20px;
        background: #ffffff;
        border-radius: 5px;
    }
    .promo-code textarea {
        background: #ffffff !important;
    }
    .promo-code__text {
        font-size: 18px;
        line-height: 30px;
    }

    .promo-benefits {
        padding: 0 0 50px;
    }
    .promo-title {
        margin: 0 0 40px;
        padding: 0;
        font-size: 40px;
        line-height: 40px;
    }
    .promo-list {
        margin: 0 -10px;
        font-size: 0;
    }
    .promo-card {
        position: relative;
        display: inline-block;
        vertical-align: top;
        width: calc(25% - 20px);
        margin: 0 10px 30px;
        border-radius: 5px;
        font-size: 14px;
        text-align: left;
    }
    .promo-icon {
        width: 75px;
        height: 75px;
    }
    .promo-card__title {
        margin: 20px 0;
        font-size: 16px;
        line-height: 25px;
        font-weight: 400;
    }
    @media (max-width: 550px) {
        .promo-list {
            margin: 0 -15px;
            padding: 10px 5px;
            white-space: nowrap;
            overflow: auto;
            -webkit-overflow-scrolling: touch;
        }
        .promo-card {
            width: calc(80% - 20px);
            margin-bottom: 0;
            white-space: normal;
        }
    }
    @media (min-width: 550px) and (max-width: 991px) {
        .promo-card {
            width: calc(50% - 20px);
        }
    }

    .promo__list {
        padding: 0;
        list-style: none;
        display: flex;
        flex-flow: wrap;
    }
    .promo__list > li {
        position: relative;
        width: 33.3333%;
        margin-bottom: 15px;
        padding: 0 15px 0 25px;
        font-size: 16px;
        line-height: 25px;
        flex: 0 0 auto;
    }
    .promo__list > li > .tsi {
        position: absolute;
        top: 3px;
        left: 0;
        display: block;
        color: #00bf00;
    }
    @media (max-width: 991px) {
        .promo__list > li {
            width: 50%;
        }
    }
    @media (max-width: 700px) {
        .promo__list > li {
            width: 100%;
        }
    }

    .promo-contact {
        padding: 80px 0 70px;
        margin-bottom: -40px;
        color: #fff;
        background-color: #0068b3;
        background-image: linear-gradient(20deg, #006a80 0%, #550080 100%);
    }
    .promo-contact__form {
        margin-top: 30px;
    }
    .promo-contact__form .btn {
        margin: 10px 0 0;
    }
    .promo-contact__form .form-control {
        background: rgba(0, 0, 0, 0.4);
        box-shadow: none;
        border-color: transparent;
        color: #ffffff;
        transition: all 0.3s ease;
    }
    .promo-contact__form .form-control:focus {
        border-color: rgba(0, 0, 0, 0);
        box-shadow: none;
        background: #fff;
        color: #404448;
    }
    .promo-contact__form .form-control::-webkit-input-placeholder {color: #868fbd;}
    .promo-contact__form .form-control::-moz-placeholder {color: #868fbd;}
    .promo-contact__form .form-control:-ms-input-placeholder {color: #868fbd;}
    .promo-contact__form .form-control:-moz-placeholder {color: #868fbd;}

    .promo-contact__form label {
        color: rgba(255, 255, 255, 0.8);
    }
    @media (max-width: 500px) {
        .promo-contact {
            padding: 60px 0;
        }
    }

    .promo-contact .btn-info {
        color: #1E88E5;
        background-color: #ffffff;
        border-color: transparent;
    }
    .promo-contact .btn-info:hover {
        background-color: rgba(255, 255, 255, 0.85);
    }

    .ts-embed-tslink {
        max-width: 970px;
        margin: 10px auto 0;
        padding: 0 15px;
        font: 14px/20px "proxima-nova", "Helvetica Neue", "Arial", sans-serif;
        text-decoration-skip: ink;
        box-sizing: content-box;
    }
    .ts-embed-userwidget {
        width: 100%;
        min-height: 520px;
    }
</style>

<section class="promo-head">
    <div class="container">
        <div class="row relative">
            <div class="col-md-7 wide-padding wide-padding--right">
                <h1 class="promo-head__title"><?= _t('site.page', 'Treatstock Widget'); ?></h1>
                <p class="promo-head__subtitle"><?= _t('site.page', 'Convert your website traffic into sales and earn money by providing your users with the opportunity to upload their files, calculate and compare real-time quotes from manufacturers and place orders instantly with our powerful Treatstock Widget. Contact us for more information about our rewards program.'); ?></p>
                <a href="#promo-widget" data-target="#promo-widget" class="btn btn-primary m-b30 m-r10" target="_blank"><?= _t('site.page', 'Try Widget'); ?></a>
                <a href="#promo-code" data-target="#promo-code" class="btn btn-warning m-b30 m-r10" target="_blank"><?= _t('site.page', 'Get HTML Code'); ?></a>
                <a href="#contactForm" data-target="#contactForm" class="btn btn-default m-b30"><?= _t('site.page', 'Contact Us'); ?></a>
            </div>
            <div class="col-md-5">
                <div class="promo-head__video">
                    <div class="video-container m-b0">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/bbUos_nOzs4?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="promo-widget" id="promo-widget">
    <div class="container">
        <h2 class="promo-title"><?= _t('site.page', 'Try Widget'); ?></h2>
        <div class="ts-embed-tslink"><a href="https://www.treatstock.com/" target="_blank">Powered by Treatstock</a></div>
        <iframe class="ts-embed-userwidget" frameborder="0" height="470px" src="https://www.treatstock.com/my/print-model3d/widget?userUid=asIs8y" width="100%"></iframe>
    </div>
</section>

<section class="promo-benefits">
    <div class="container">
        <h2 class="promo-title"><?= _t('site.page', 'Features'); ?></h2>

        <div class="promo-list">
            <div class="promo-card">
                <img class="promo-icon" src="https://static.treatstock.com/static/images/common/big-icons/materials-colors.svg" alt="">
                <h4 class="promo-card__title">
                    <?= _t('front', 'Customize files by choosing from different materials and colors'); ?>
                </h4>
            </div>
            <div class="promo-card">
                <img class="promo-icon" src="https://static.treatstock.com/static/images/common/big-icons/save.svg" alt="">
                <h4 class="promo-card__title">
                    <?= _t('front', 'Real-time quotes from local manufacturers'); ?>
                </h4>
            </div>
            <div class="promo-card">
                <img class="promo-icon" src="https://static.treatstock.com/static/images/common/big-icons/compare-services.svg" alt="">
                <h4 class="promo-card__title">
                    <?= _t('front', 'Compare prices and reviews to get the best deals online'); ?>
                </h4>
            </div>
            <div class="promo-card">
                <img class="promo-icon" src="https://static.treatstock.com/static/images/common/big-icons/instant-security.svg" alt="">
                <h4 class="promo-card__title">
                    <?= _t('front', 'Place orders instantly with secure online payments'); ?>
                </h4>
            </div>
        </div>

        <p class="text-center">
            <a href="#promo-benefits__more" data-toggle="collapse" class="btn btn-primary btn-ghost m-b30"><?= _t('front', 'Show More Features'); ?></a>
        </p>

        <div class="promo-benefits__more collapse" id="promo-benefits__more" aria-expanded="true">
            <ul class="promo__list">
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'STL and PLY files supported'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Exotic materials and colors available'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Wide range of 3D printing technologies'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'All 3D printers are quality tested'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Professionally certified 3D printers available'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Pick-up, domestic and international shipping options'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Customers can track the order process'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', '24/7 online support service'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Private messaging between client and manufacturer'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'All 3D prints are quality checked before they are shipped'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'Receipts available for all orders'); ?></li>
                <li><span class="tsi tsi-checkmark-c"></span><?= _t('site.page', 'All files are protected by Treatstock security'); ?></li>
            </ul>
        </div>
    </div>
</section>

<section class="promo-code" id="promo-code">
    <div class="container">
        <h2 class="promo-title"><?= _t('site.page', 'HTML Sample code'); ?></h2>
        <div class="row">
            <div class="col-sm-6 col-lg-5">
                <div class="designer-card">
                    <textarea id="publicLink" class="form-control" name="publicLink" rows="8" readonly="readonly"><!-- UserWidget: asIs8y --><link href="https://www.treatstock.com/css/embed-user.css" rel="stylesheet"><div class="ts-embed-tslink"><a href="https://www.treatstock.com/" target="_blank">Powered by Treatstock</a></div><iframe class="ts-embed-userwidget" frameborder="0" height="650px" src="https://www.treatstock.com/my/print-model3d/widget?userUid=asIs8y" width="100%"></iframe></textarea>
                </div>
            </div>
            <div class="col-sm-6 col-lg-6 col-lg-offset-1 wide-padding wide-padding--left">
                <p class="promo-code__text">
                    <?= _t(
                        'site.page',
                        'Simply copy and paste the HTML code into your site to embed the innovative widget powered by Treatstock.'
                    ); ?>
                    <br>
                    <br>
                    <?= _t(
                        'site.page',
                        'If you want to join our affiliate program and gain rewards for each order, get your own unique embed code.'
                    ); ?>
                    <br>
                    <br>
                    <a href="/mybusiness/widgets/affiliate" class="btn btn-primary" target="_blank">
                        <?= _t(
                            'site.page',
                            'Get unique embed code'
                        ); ?>
                    </a>
                    
                </p>
            </div>
        </div>
    </div>
</section>

<section class="promo-contact" id="contactForm" ng-controller="PromoContactUsController">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2 class="m-t0 m-b30"><?= _t('site.page', 'Contact Us'); ?></h2>
            </div>
            <div class="col-sm-12 col-md-6 col-md-offset-3">
                <form class="row promo-contact__form">
                    <div class="col-sm-6 form-group">
                        <label class="control-label" for="contactform-name"><?= _t('site.page', 'Name'); ?></label>
                        <input ng-model="contactForm.name"
                               type="text" id="contactform-name" class="form-control" placeholder="<?= _t('site.page', 'Your name'); ?>">
                    </div>
                    <div class="col-sm-6 form-group">
                        <label class="control-label" for="contactform-email"><?= _t('site.page', 'Email'); ?></label>
                        <input ng-model="contactForm.email"
                               type="email" id="contactform-email" class="form-control" placeholder="<?= _t('site.page', 'Your email'); ?>">
                    </div>
                    <div class="col-sm-12 form-group">
                        <label class="control-label" for="contactform-text"><?= _t('site.page', 'Message'); ?></label>
                        <textarea ng-model="contactForm.details"
                                  id="contactform-text" class="form-control" rows="3" placeholder="<?= _t('site.page', 'Tell us more about your needs'); ?>"></textarea>
                    </div>
                    <div class="col-sm-12 form-group m-b0">
                        <button
                                loader-click="sendContactForm()"
                                type="button" class="btn btn-info" name="contact-button"><?= _t('site.page', 'Submit'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<script>
    <?php $this->beginBlock('js1', false); ?>

    $('a[data-target]').click(function () {
        var scroll_elTarget = $(this).attr('data-target');
        if ($(scroll_elTarget).length != 0) {
            $('html, body').animate({scrollTop: $(scroll_elTarget).offset().top - 105}, 350);
        }
        return false;
    });

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>