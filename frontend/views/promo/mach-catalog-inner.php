<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

use common\models\Ps;
use frontend\widgets\PsPrintServiceReviewStarsWidget;

$ps=Ps::findByPk(1);

$this->title = "Manufacturing Machines Guide";

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

?>

<div class="container">
    <ul class="breadcrumb m-t10">
        <li><a href="#">Manufacturing Machines Guide</a></li>
        <li><a href="#">Maker Bot</a></li>
        <li><a href="#">Maker bot Replicator Mini</a></li>
    </ul>
</div>

<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h1 class="m-t0">Maker bot Replicator Mini</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="designer-card p-t0 p-l0 p-r0 p-b0 m-b30">
                <div class="fotorama"
                     data-width="100%"
                     data-height="auto"
                     data-arrows="true"
                     data-click="true"
                     data-nav="thumbs"
                     data-thumbmargin="10"
                     data-thumbwidth="80px"
                     data-thumbheight="60px"
                     data-allowfullscreen="true">
                    <img src="/static/images/cat2.png" alt="">
                    <img src="/static/images/cat3.png" alt="">
                    <img src="/static/images/cat1.png" alt="">
                </div>
            </div>
        </div>
        <div class="col-sm-8 wide-padding wide-padding--left">
            <div class="designer-card m-b30">
                <p>
                    <strong>Machine type:</strong> 3D Printer<br>
                    <strong>Tehnology:</strong> FDM<br>
                    <strong>Materials:</strong> PLA, ABS, Wood PLA, Bronze PLA, перечислить все материалы<br>
                </p>
                <p>
                    <strong>Layer resolution Low:</strong> 200 micron (0,2 mm)<br>
                    <strong>Layer resolution High:</strong> 40 micron (0,04 mm)<br>
                    <strong>Work Area:</strong> 252 х 199 х 150 mm
                </p>
                <p class="m-b0">
                    <strong>Brand:</strong> Maker Bot<br>
                    <strong>Website:</strong> <noindex><a rel="nofollow" href="https://www.makerbot.com/replicator-mini/">https://www.makerbot.com/replicator-mini</a></noindex><br>
                </p>
            </div>

            <h2>About Maker Bot Replicator</h2>
            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>

            <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
        </div>
    </div>
</div>

    <script>
        <?php $this->beginBlock('js1', false); ?>

        //init PS card portfolio pics
        var swiperMachCards = new Swiper('.designer-card__ps-portfolio', {
            scrollbar: '.designer-card__ps-portfolio-scrollbar',
            scrollbarHide: true,
            slidesPerView: 'auto',
            grabCursor: true
        });

        <?php $this->endBlock(); ?>
    </script>

<?php $this->registerJs($this->blocks['js1']); ?>