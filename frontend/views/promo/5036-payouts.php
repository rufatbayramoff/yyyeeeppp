<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>


<div class="user-profile ng-scope" ng-controller="ProfileHeaderController">

    <div class="user-profile__cover" style="background-image: url('http://ts.vcap.me/static/user/4a08142c38dbe374195d41c04562d9f8/cover.jpg')" title="Profile image"></div>

    <div class="container relative">
        <div class="row">
            <div class="col-sm-9">
                <div class="user-profile__userinfo">
                    <a class="user-profile__avatar" href="/user-profile">
                        <img class="img-circle" src="http://ts.vcap.me/static/user/4a08142c38dbe374195d41c04562d9f8/avatar_128x128.jpg?mcache=" alt="" align="left">                    </a>
                    <div class="user-profile__username">
                        SomeOneTwoThree                    </div>
                    <div class="user-profile__user-rating">
                    </div>
                    <div class="user-profile__btns">
                        <div class="user-profile__send-msg">
                            <a class="btn btn-primary" href="/workbench/messages"><span class="tsi tsi-message"></span></a>
                        </div>

                        <div class="user-profile__nav user-profile__nav--desktop btn-group">

                            <a href="/user-profile" class="btn btn-default ">
                                Profile                                                                    </a>
                            <a href="/mybusiness/company" class="btn btn-default ">
                                My Services                                                                            <span class="user-profile__nav-badge">1</span>
                            </a>
                            <a href="/my/store" class="btn btn-default ">
                                My Store                                                                            <span class="user-profile__nav-badge">30</span>
                            </a>

                        </div>

                        <div class="user-profile__nav user-profile__nav--mobile">
                            <select id="w0" class="user-profile__nav-select form-control select2-hidden-accessible" name="user-profile__nav--mobile" data-s2-options="s2options_d6851687" data-krajee-select2="select2_f83af4cc" style="display:none" tabindex="-1" aria-hidden="true">
                                <option value="/user-profile">Profile</option>
                                <option value="/mybusiness/company">My Service</option>
                                <option value="/my/store">My Store</option>
                            </select><span class="select2 select2-container select2-container--krajee" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-w0-container"><span class="select2-selection__rendered" id="select2-w0-container" title="Profile">Profile</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>                        </div>
                    </div>
                </div>
            </div>
        </div>

        <a class="ts-ajax-modal change-cover user-profile__controls__item user-profile__change-cover-btn" href="/user-profile/change-cover" title="" data-target="#modal_uploadcover" data-toggle="tooltip" data-placement="bottom" data-original-title="Change cover picture"><span class="tsi tsi-picture"></span></a>
        <div class="user-profile__controls__item user-profile__change-avatar-btn" ng-click="showChangeAvatarModal()" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Change avatar">
            <span class="tsi tsi-camera"></span>
        </div>

        <a class="user-profile__controls__item user-profile__change-pass-btn" href="/user-profile/change-password" data-toggle="tooltip" data-placement="bottom" data-original-title="Change password"><span class="tsi tsi-lock"></span></a>
    </div>
</div>

<div class="nav-tabs__container">
    <div class="container">
        <ul id="w1" class="nav nav-tabs">
            <li><a href="/user-profile">Profile</a></li>
            <li><a href="/my/delivery">Delivery</a></li>
            <li class="active"><a href="/my/payments/transactions">Earnings</a></li>
            <li><a href="/my/taxes">Taxes</a></li>
            <li><a href="/my/settings">Settings</a></li>
            <li><a href="/my/notifications">Notifications</a></li>
            <li><a href="/my/widgets">Widgets</a></li>
        </ul>
    </div>
</div>

<style type="text/css">/*!reset via github.com/premasagar/cleanslate*/
    .LIwPP,
    .LIwPP b,
    .LIwPP img,
    .LIwPP svg {
        -webkit-box-sizing: content-box !important;
        -moz-box-sizing: content-box !important;
        box-sizing: content-box !important;
        background-attachment: scroll!important;
        background-color: transparent!important;
        background-image: none!important;
        background-position: 0 0!important;
        background-repeat: repeat!important;
        border-color: black!important;
        border-color: currentColor!important;
        border-radius: 0!important;
        border-style: none!important;
        border-width: medium!important;
        direction: inherit!important;
        display: inline!important;
        float: none!important;
        font-size: inherit!important;
        height: auto!important;
        letter-spacing: normal!important;
        line-height: inherit!important;
        margin: 0!important;
        max-height: none!important;
        max-width: none!important;
        min-height: 0!important;
        min-width: 0!important;
        opacity: 1!important;
        outline: invert none medium!important;
        overflow: visible!important;
        padding: 0!important;
        position: static!important;
        text-align: inherit!important;
        text-decoration: inherit!important;
        text-indent: 0!important;
        text-shadow: none!important;
        text-transform: none!important;
        unicode-bidi: normal!important;
        vertical-align: baseline!important;
        visibility: inherit!important;
        white-space: normal!important;
        width: auto!important;
        word-spacing: normal!important;
    }
    .LIwPP *[dir=rtl] {
        direction: rtl!important;
    }
    .LIwPP {
        direction: ltr!important;
        font-style: normal!important;
        text-align: left!important;
        text-decoration: none!important;
    }
    .LIwPP {
        background-color: #bfbfbf !important;
        background-image: -webkit-linear-gradient(#e0e0e0 20%, #bfbfbf) !important;
        background-image: linear-gradient(#e0e0e0 20%, #bfbfbf) !important;
        background-repeat: no-repeat !important;
        border: 1px solid #bbb !important;
        border-color: #cbcbcb #b2b2b2 #8b8b8b !important;
        -webkit-box-shadow: inset 0 1px #ececec !important;
        box-shadow: inset 0 1px #ececec !important;
        -webkit-box-sizing: border-box !important;
        -moz-box-sizing: border-box !important;
        box-sizing: border-box !important;
        border-radius: 5px !important;
        font-size: 16px !important;
        height: 2em !important;
        margin: 0 !important;
        overflow: hidden !important;
        padding: 0 !important;
        position: relative !important;
        text-align: center !important;
        width: auto !important;
        white-space: nowrap !important;
    }
    .LIwPP,
    .LIwPP b,
    .LIwPP i {
        cursor: pointer !important;
        display: inline-block !important;
        -webkit-user-select: none !important;
        -moz-user-select: none !important;
        -ms-user-select: none !important;
        user-select: none !important;
    }
    .LIwPP b {
        border-left: 1px solid #b2b2b2 !important;
        -webkit-box-shadow: inset 1px 0 rgba(255,255,255,.5) !important;
        box-shadow: inset 1px 0 rgba(255,255,255,.5) !important;
        color: #333333 !important;
        font: normal 700 0.6875em/1.5 "Helvetica Neue", Arial, sans-serif !important;
        height: 2.25em !important;
        padding: .625em .66667em 0 !important;
        text-shadow: 0 1px 0 #eee !important;
        vertical-align: baseline !important;
    }
    .LIwPP .PPTM,
    .LIwPP i {
        height: 100% !important;
        margin: 0 .4em 0 .5em !important;
        vertical-align: middle !important;
        width: 1em !important;
    }
    .LIwPP i {
        background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAhCAMAAADnC9tbAAAA81BMVEX///////+ozeUAecEARXyozeUARXwAecH///8AecGozeX///8ARXz///+ozeUAecEARXzg7fYAbrPx9/v///8AecFGntKozeUAZqgARXwARXy11OmFv+EAecEAaav///8AecGozeUARXwAW5rZ6fTg7fb5+/3V5/LA2+z///8ARXyozeW92euy0+j5+/0AecEAc7kAbLAASIDR5fH///+ozeUAY6MAT4oARXwAecEARXz///+ozeX///8ARXwAecGozeUARXyozeX///8AecH///8AecEARXyozeWozeX///8ARXwAecGozeUAecH///8ARXwU3q5BAAAATXRSTlMAERERESIiIiIzMzMzRERERERERFVVVVVVVWZmZmZmZnd3d3d3d3d3d4iIiIiIiIiIiIiImZmZmZmqqqqqu7u7u8zMzMzd3d3d7u7u7nVO37gAAAEZSURBVHheTY5rX4IwGEf/aFhWoJV2sRugXcDu2QUMDA0QbdPv/2naw9B13jzb2X5nA1CbLyVxBwWd5RoPhLdU1EjEQjiAQ+K9DWysTmh2f4CmmBnKG89cJrLmySftI1+ISCXnx9wH1D7Y/+UN7JLIoih4uRhxfi5bsTUapZxzvw4goA/y1LKsRhVERq/zNkpk84lXlXC8j26aQoG6qD3iP5uHZwaxg5KtRcnM1UBcLtYMQQzoMJwUZo9EIkQLMEi82oBGC62cvSnQEjMB4JK4Y3KRuG7RGHznQKgemZyyN2ChvnHPcl3Gw9B9uOpPWb4tE8MvRkztCoChENdsfGSaOgpmQtwwE2uo1mcVJYyD3m0+hgJ6zpitxB/jvlwEGmtLjgAAAABJRU5ErkJggg==') no-repeat !important;
        height: 1em !important;
    }
    .LIwPP img.PPTM {
        height: auto !important;
    }
    .LIwPP:hover,
    .LIwPP:active {
        background-color: #a5a5a5 !important;
        background-image: -webkit-linear-gradient(#e0e0e0 20%, #a5a5a5) !important;
        background-image: linear-gradient(#e0e0e0 20%, #a5a5a5) !important;
        background-repeat: no-repeat !important;
    }
    .LIwPP:active {
        border-color: #8c8c8c #878787 #808080 !important;
        -webkit-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.2) !important;
        box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.2) !important;
        outline: 0 !important;
    }
    .LIwPP:hover b {
        -webkit-box-shadow: inset 1px 0 rgba(255,255,255,.3) !important;
        box-shadow: inset 1px 0 rgba(255,255,255,.3) !important;
    }
    .PPBlue {
        background-color: #0079c1 !important;
        background-image: -webkit-linear-gradient(#00a1ff 20%, #0079c1) !important;
        background-image: linear-gradient(#00a1ff 20%, #0079c1) !important;
        background-repeat: no-repeat !important;
        border-color: #0079c1 #00588b #004b77 !important;
        -webkit-box-shadow: inset 0 1px #4dbeff !important;
        box-shadow: inset 0 1px #4dbeff !important;
    }
    .PPBlue b {
        -webkit-box-shadow: inset 1px 0 rgba(77, 190, 255, .5) !important;
        box-shadow: inset 1px 0 rgba(77, 190, 255, .5) !important;
        border-left-color: #00588b !important;
        color: #f9fcff !important;
        -webkit-font-smoothing: antialiased !important;
        font-smoothing: antialiased !important;
        text-shadow: 0 -1px 0 #00629c !important;
    }
    .PPBlue i {
        background-position: 0 100% !important;
    }
    .PPBlue .PPTM-btm {
        fill: #a8cde5 !important;
    }
    .PPBlue .PPTM-top {
        fill: #fff !important;
    }
    .PPBlue:hover,
    .PPBlue:active {
        background-color: #005282 !important;
        background-image: -webkit-linear-gradient(#0083cf 20%, #005282) !important;
        background-image: linear-gradient(#0083cf 20%, #005282) !important;
        background-repeat: no-repeat !important;
        border-color: #006699 #004466 #003355 !important;
    }
    .PPBlue:hover b,
    .PPBlue:active b {
        text-shadow: 0 -1px 0 #004466 !important;
    }
</style>

<div class="container">
    <div class="row wide-padding">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="payments-balance">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4 class="payments-balance__label">Balance</h4>
                                <div class="payments-balance__value">$0.00</div>
                                <hr>
                                <div>
                                    You can use your income for purchases on Treatstock. <a
                                        href="https://www.treatstock.com/help/article/80-what-can-i-do-with-my-income-on-treatstock"
                                        target="_blank">
                                        Learn more </a>
                                </div>
                            </div>

                            <div class="payments-balance__divider"></div>

                            <div class="col-sm-8">

                                <h4 class="payments-balance__label">Withdrawn </h4>

                                <div class="payments-payout">
                                    <p>
                                        <span id="lippButton" style="display: inline-block; margin: 0 10px 10px 0;">
                                            <button id="LIwPP14036958" class="LIwPP PPBlue">
                                            <svg class="PPTM" xmlns="http://www.w3.org/2000/svg" version="1.1"
                                                 width="16px" height="17px" viewBox="0 0 16 17">
                                                <path class="PPTM-btm" fill="#0079c1"
                                                      d="m15.603 3.917c-0.264-0.505-0.651-0.917-1.155-1.231-0.025-0.016-0.055-0.029-0.081-0.044 0.004 0.007 0.009 0.014 0.013 0.021 0.265 0.506 0.396 1.135 0.396 1.891 0 1.715-0.712 3.097-2.138 4.148-1.425 1.052-3.418 1.574-5.979 1.574h-0.597c-0.45 0-0.9 0.359-1.001 0.798l-0.719 3.106c-0.101 0.438-0.552 0.797-1.002 0.797h-1.404l-0.105 0.457c-0.101 0.438 0.184 0.798 0.633 0.798h2.1c0.45 0 0.9-0.359 1.001-0.798l0.718-3.106c0.101-0.438 0.551-0.797 1.002-0.797h0.597c2.562 0 4.554-0.522 5.979-1.574 1.426-1.052 2.139-2.434 2.139-4.149 0-0.755-0.132-1.385-0.397-1.891z"></path>
                                                <path class="PPTM-top" fill="#00457c"
                                                      d="m9.27 6.283c-0.63 0.46-1.511 0.691-2.641 0.691h-0.521c-0.45 0-0.736-0.359-0.635-0.797l0.628-2.72c0.101-0.438 0.552-0.797 1.002-0.797h0.686c0.802 0 1.408 0.136 1.814 0.409 0.409 0.268 0.611 0.683 0.611 1.244 0 0.852-0.315 1.507-0.944 1.97zm3.369-5.42c-0.913-0.566-2.16-0.863-4.288-0.863h-4.372c-0.449 0-0.9 0.359-1.001 0.797l-2.957 12.813c-0.101 0.439 0.185 0.798 0.634 0.798h2.099c0.45 0 0.901-0.358 1.003-0.797l0.717-3.105c0.101-0.438 0.552-0.797 1.001-0.797h0.598c2.562 0 4.554-0.524 5.979-1.575 1.427-1.051 2.139-2.433 2.139-4.148-0.001-1.365-0.439-2.425-1.552-3.123z"></path>
                                            </svg>
                                            <b>Log In with PayPal</b>
                                            </button>
                                        </span>
                                        to withdraw your funds
                                    </p>
                                    <div class="payments-payout__form">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Amount" disabled="disabled">
                                            <span class="input-group-btn">
                                                <button class="btn btn-success" type="button" disabled="disabled">Request payout!</button>
                                            </span>
                                        </div>
                                        <span class="help-block">The minimum payout amount is $10</span>
                                    </div>
                                </div>

                                <!-- When PayPal authorized -->
                                <hr>
                                <div class="payments-payout">
                                    <div class="payments-payout__form">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Amount" disabled="disabled">
                                            <span class="input-group-btn">
                                                <button class="btn btn-success" type="button" disabled="disabled">Request payout!</button>
                                            </span>
                                        </div>
                                        <span class="help-block">The minimum payout amount is $10</span>
                                    </div>
                                    <p class="m-b10">
                                        Current PayPal account: <b>username@gmail.com</b>
                                    </p>
                                    <p>
                                        <a class="payments-payout__change" href="#changePaypal">
                                            Change PayPal account
                                        </a>
                                    </p>
                                </div>

                                <div class="alert alert-info alert--text-normal m-b0">
                                    After you request a payout from your Treatstock account, you will receive the payout
                                    amount in your PayPal account within 10 working days. The minimum payout amount from
                                    your Treatstock account to your PayPal account is $10. If you experience any
                                    problems with payout to your PayPal account, please contact us.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="payments-balance">
                        <h4 class="payments-balance__label">Statistics </h4>
                        <div class="row payments-balance__stats">
                            <div class="col-sm-3">
                                <h4>Total Income</h4>
                                <span>$0.00</span>
                            </div>
                            <div class="col-sm-3">
                                <h4>Completed Print Requests</h4>
                                <span>0</span>
                            </div>
                            <div class="col-sm-3">
                                <h4>Total Files Printed</h4>
                                <span>0</span>
                            </div>
                            <div class="col-sm-3">
                                <h4>Models Sold</h4>
                                <span>0</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default payments-create-report">
                <div class="panel-body">
                    <p>You have been a Treatstock user since Feb 1, 2016</p>

                    <form id="w2" class="form-inline ng-pristine ng-valid" action="/my/payments/report" method="get"
                          target="_blank">
                        <div class="form-group">
                            <label class="payments-create-report__label">From</label>
                            <input type="date" name="from" class="form-control input-sm payments-create-report__input"
                                   value="2016-02-01">
                        </div>
                        <div class="form-group">
                            <label class="payments-create-report__label">to</label>
                            <input type="date" name="to" class="form-control input-sm payments-create-report__input"
                                   value="2017-11-24">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-sm btn-primary btn-ghost payments-create-report__btn">Create report
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <div class="table-responsive m-b20">
                <div id="w3" class="grid-view">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th><a href="/my/payments/transactions?sort=id" data-sort="id">Payment ID</a></th>
                            <th><a class="desc" href="/my/payments/transactions?sort=created_at" data-sort="created_at">Created
                                    On</a></th>
                            <th><a href="/my/payments/transactions?sort=original_amount" data-sort="original_amount">Amount</a>
                            </th>
                            <th><a href="/my/payments/transactions?sort=type" data-sort="type">Transaction</a></th>
                            <th><a href="/my/payments/transactions?sort=description"
                                   data-sort="description">Description</a></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="5">
                                <div class="empty">No results found.</div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>