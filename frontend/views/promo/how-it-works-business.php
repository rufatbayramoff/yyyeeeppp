<?php
use yii\helpers\Html;
$this->title = _t('site.page', 'How it Works');

$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="container">
        <div class="row wide-padding">

            <div class="col-xs-12 site-how-to">
                <h1><?= \H($this->title) ?></h1>

                <div id="how-to-accordion">



                    <div class="panel how-to__panel">

                        <h2 class="how-to__title" data-toggle="collapse" data-parent="#how-to-accordion" data-target="#how-to__2">
                            <span class="tsi tsi-printer3d"></span>
                            <span class="tsi tsi-up-c"></span>
                            <?=_t('site.page', 'Join our manufacturing platform')?>
                        </h2>

                        <div class="how-to__pane collapse in" id="how-to__2">

                            <div class="how-to__pic how-to__pic--network">
                                <h3 class="how-to__subtitle">&nbsp;</h3>
                            </div>

                            <div class="how-to-steps-wrap" data-width="100%" data-height="200" data-arrows="true" data-click="true">
                                <div class="how-to-steps">
                                    <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-ps-1.png" alt="<?=_t('site.page', 'Put your 3D printers to work')?>" title="<?=_t('site.page', 'Put your 3D printers to work')?>">
                                    */?>
                                    <div class="how-to-steps__text">
                                        <div class="how-to-steps__count how-to-steps__count--one">1</div>
                                        <h3>
                                            <a href="/mybusiness/company" onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/company');return false;" : "return true;" ?>">
                                                <?=_t('site.page', 'Register your machines')?>
                                            </a>
                                        </h3>
                                        <p><?=_t('site.page', 'Register your manufacturing equipment with all the materials and colors that you offer, set your prices and your delivery options. Be sure to upload images of your previous work and get your machines certified to show customers that you can produce high-quality products.')?></p>
                                    </div>
                                </div>

                                <div class="how-to-steps">
                                    <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-ps-2.png" alt="<?=_t('site.page', 'Connect with customers')?>" title="<?=_t('site.page', 'Connect with customers')?>">
                                    */?>
                                    <div class="how-to-steps__text">
                                        <div class="how-to-steps__count">2</div>
                                        <h3><?=_t('site.page', 'Connect with customers')?></h3>
                                        <p><?=_t('site.page', 'Respond to new orders, requests for a quote and customer inquiries as fast as possible. Use our free business tools to increase your points of sale across different platforms and maximize your revenue.')?></p>
                                    </div>
                                </div>

                                <div class="how-to-steps">
                                    <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-ps-3.png" alt="<?=_t('site.page', 'Earn & Enjoy!')?>" title="<?=_t('site.page', 'Earn & Enjoy!')?>">
                                    */?>
                                    <div class="how-to-steps__text">
                                        <div class="how-to-steps__count">3</div>
                                        <h3><?=_t('site.page', 'Earn & Enjoy!')?></h3>
                                        <p><?=_t('site.page', 'Deliver high-quality products on-demand, build a reputation with satisfied customers from all over the world and earn money while helping to make the world around us unique and special, one order at a time!')?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="panel how-to__panel">

                        <h2 class="how-to__title collapsed" data-toggle="collapse" data-parent="#how-to-accordion" data-target="#how-to__1">
                            <span class="tsi tsi-shopping-cart"></span>
                            <span class="tsi tsi-up-c"></span>
                            <?=_t('site.page', 'OPEN A 3D MODEL STORE')?>
                        </h2>

                        <div class="how-to__pane collapse" id="how-to__1">

                            <div class="how-to__pic how-to__pic--open-store">
                                <h3 class="how-to__subtitle">&nbsp;</h3>
                            </div>

                            <div class="how-to-steps-wrap" data-width="100%" data-height="200" data-arrows="true" data-click="true">
                                <div class="how-to-steps">
                                    <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-1.png" alt="<?=_t('site.page', 'Create a digital model')?>" title="<?=_t('site.page', 'Create a digital model')?>">
                                    */?>
                                    <div class="how-to-steps__text">
                                        <div class="how-to-steps__count how-to-steps__count--one">1</div>
                                        <h3><?=_t('site.page', 'Create a digital model')?></h3>
                                        <p><?=_t('site.page', 'CAD designers and engineers can create digital models with many functions such as mechanical parts, prototypes, jewelry, robotics and gadgets, home goods, and much more.')?></p>
                                    </div>
                                </div>

                                <div class="how-to-steps">
                                    <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-2.png" alt="<?=_t('site.page', 'Publish your model')?>" title="<?=_t('site.page', 'Publish your model')?>">
                                    */?>
                                    <div class="how-to-steps__text">
                                        <div class="how-to-steps__count">2</div>
                                        <h3>
                                            <a href="/upload" data-method="get">
                                                <?=_t('site.page', 'Publish your model')?>
                                            </a>
                                        </h3>
                                        <p><?=_t('site.page', 'Upload your 3D model with photos of the finished product. Write a short description, assign tags, recommend a material and color, and set your price. If you want it to be free, just leave the price as zero. Then, simply hit the "Publish" button!')?></p>
                                    </div>
                                </div>

                                <div class="how-to-steps">
                                    <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-3.png" alt="<?=_t('site.page', 'Earn & Enjoy!')?>" title="<?=_t('site.page', 'Earn & Enjoy!')?>">
                                    */?>
                                    <div class="how-to-steps__text">
                                        <div class="how-to-steps__count">3</div>
                                        <h3><?=_t('site.page', 'Earn & Enjoy!')?></h3>
                                        <p><?=_t('site.page', 'Have your designs transformed into high-quality tangible products with the help of our global manufacturing platform, get paid each time your 3D model is made and let people all around the world enjoy your work!')?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>

    <script>
        <?php $this->beginBlock('js1', false); ?>

        function makeSliders() {
            var isMobile = window.matchMedia("only screen and (max-width: 767px)");

            if (isMobile.matches) {
                $('.how-to-steps-wrap').addClass('fotorama');
                $('.fotorama').fotorama();
            }
        }

        makeSliders();

        $(function () {

            var hash = window.location.hash;

            if (hash){
                var hashQueryPos = hash.indexOf('?');
                if(hashQueryPos != -1){
                    hash = hash.substr(0, hashQueryPos);
                }
            }

            if(hash != null && hash != ""){
                $('.collapse').removeClass('in');
                $('.how-to__title').addClass('collapsed');
                $(hash + '.collapse').collapse('toggle');

                $(".how-to__title").click(function (e) {

                    if (window.location.hash != $(this).attr("data-target")) {
                        $('.collapse').removeClass('in');
                        $('.how-to__title').addClass('collapsed');
                        $('.collapse').collapse('hide');
                    }
                    window.location.hash = $(this).attr("data-target");
                });
            }

            $(".how-to__title").click(function (e) {
                window.location.hash = $(this).attr("data-target");
            });
        });

        $(function () {
            $('#how-to-accordion').on('shown.bs.collapse', function (e) {
                var offset = $('.how-to__panel > .collapse.in').offset();
                if (offset) {
                    $('html,body').animate({
                        scrollTop: $('.collapse.in').siblings('.how-to__title').offset().top - 120
                    }, 300);
                }
            });
        });

        <?php $this->endBlock(); ?>
    </script>

<?php $this->registerJs($this->blocks['js1']); ?>