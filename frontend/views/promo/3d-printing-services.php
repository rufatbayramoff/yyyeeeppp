<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}
$this->title = "Printer Services";

?>

<div class="ps-cat-head">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="h2 ps-cat-head__title"><?= _t('site.ps', 'Print Services'); ?></h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="promo-page-title m-t30"><?= _t('site.ps', 'Featured 3D Print Services'); ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="designer-card designer-card--ps-cat">
                <div class="designer-card__ps-rating">

                    <div id="star-rating-block-65968" class="star-rating-block" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                        <meta itemprop="ratingValue" content="5">
                        <meta itemprop="worstRating" content="1">
                        <meta itemprop="bestRating" content="5">
                        <div class="star-rating rating-xs rating-disabled"><div class="rating-container tsi" data-content=""><div class="rating-stars" data-content="" style="width: 100%;"></div><input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div></div>

                        <span class="star-rating-range"><strong>5</strong>/5</span>
                        <span class="star-rating-count">(<span itemprop="reviewCount">5</span> reviews)</span></div>

                </div>

                <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                    Pro        </div>

                <div class="designer-card__userinfo">
                    <a class="designer-card__avatar" href="#" target="_blank"><img src="http://ts.vcap.me/static/user/a9b7ba70783b617e9998dc4dd82eb3c5/ps_logo_circle_1487765230_64x64.png"></a>
                    <h3 class="designer-card__username">
                        <a href="#" target="_blank" title="SF Test PS">
                            SF Test PS                </a>
                    </h3>
                </div>
                <div class="designer-card__ps-pics">
                    <div class="designer-card__ps-portfolio swiper-container swiper-container-horizontal" style="cursor: -webkit-grab;">
                        <div class="swiper-wrapper">
                            <a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active" href="http://ts.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_720x540.png" data-lightbox="ps1"><img src="http://ts.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_160x90.png" alt="Designer33.png"></a><a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next" href="http://ts.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_720x540.png" data-lightbox="ps1"><img src="http://ts.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_160x90.png" alt="PrintServiceS1.png"></a><a class="designer-card__ps-portfolio-item swiper-slide" href="http://ts.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_720x540.png" data-lightbox="ps1"><img src="http://ts.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_160x90.png" alt="PrintServiceS2.png"></a>                        </div>
                        <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;"><div class="swiper-scrollbar-drag" style="width: 0px;"></div></div>
                    </div>
                </div>
                <div class="designer-card__ps-loc" title="NEW YORK, NY, US">
                    <span class="tsi tsi-map-marker"></span>
                    NEW YORK, NY, US                </div>

                <div class="designer-card__about">
                    You can become one of our 3D printing services worldwide. It doesn’t m<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a><span class="tsrm-content hide">atter if you have many devices or only one. The registration is free, but we ask you to give precise information about you, your 3D printer and your company as we need it to comply with all the laws.</span>        </div>

                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label">Materials:</span>
                            Gypsum, PLA, Bronze PLA                                    </div>
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label">Min. Print Price:</span>
                            $30.00                                    </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label">Shipping:</span>
                            Standard                </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__btn-block">
                            <a class="btn btn-danger btn-ghost ts-ga" data-categoryga="PublicPs" data-actionga="PrintHere" href="#">
                                <span class="tsi tsi-printer3d"></span>
                                Buy Here                    </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-4">
            <div class="designer-card designer-card--ps-cat">
                <div class="designer-card__ps-rating">

                    <div id="star-rating-block-65968" class="star-rating-block" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                        <meta itemprop="ratingValue" content="5">
                        <meta itemprop="worstRating" content="1">
                        <meta itemprop="bestRating" content="5">
                        <div class="star-rating rating-xs rating-disabled"><div class="rating-container tsi" data-content=""><div class="rating-stars" data-content="" style="width: 100%;"></div><input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div></div>

                        <span class="star-rating-range"><strong>5</strong>/5</span>
                        <span class="star-rating-count">(<span itemprop="reviewCount">5</span> reviews)</span></div>

                </div>

                <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                    Pro        </div>

                <div class="designer-card__userinfo">
                    <a class="designer-card__avatar" href="#" target="_blank"><img src="http://ts.vcap.me/static/user/a9b7ba70783b617e9998dc4dd82eb3c5/ps_logo_circle_1487765230_64x64.png"></a>
                    <h3 class="designer-card__username">
                        <a href="#" target="_blank" title="SF Test PS">
                            SF Test PS                </a>
                    </h3>
                </div>
                <div class="designer-card__ps-pics">
                    <div class="designer-card__ps-portfolio swiper-container swiper-container-horizontal" style="cursor: -webkit-grab;">
                        <div class="swiper-wrapper">
                            <a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active" href="http://ts.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_720x540.png" data-lightbox="ps1"><img src="http://ts.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_160x90.png" alt="Designer33.png"></a><a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next" href="http://ts.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_720x540.png" data-lightbox="ps1"><img src="http://ts.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_160x90.png" alt="PrintServiceS1.png"></a><a class="designer-card__ps-portfolio-item swiper-slide" href="http://ts.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_720x540.png" data-lightbox="ps1"><img src="http://ts.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_160x90.png" alt="PrintServiceS2.png"></a>                        </div>
                        <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;"><div class="swiper-scrollbar-drag" style="width: 0px;"></div></div>
                    </div>
                </div>
                <div class="designer-card__ps-loc" title="NEW YORK, NY, US">
                    <span class="tsi tsi-map-marker"></span>
                    NEW YORK, NY, US                </div>

                <div class="designer-card__about">
                    You can become one of our 3D printing services worldwide. It doesn’t m<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a><span class="tsrm-content hide">atter if you have many devices or only one. The registration is free, but we ask you to give precise information about you, your 3D printer and your company as we need it to comply with all the laws.</span>        </div>

                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label">Materials:</span>
                            Gypsum, PLA, Bronze PLA                                    </div>
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label">Min. Print Price:</span>
                            $30.00                                    </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label">Shipping:</span>
                            Standard                </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__btn-block">
                            <a class="btn btn-danger btn-ghost ts-ga" data-categoryga="PublicPs" data-actionga="PrintHere" href="#">
                                <span class="tsi tsi-printer3d"></span>
                                Buy Here                    </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-4">
            <div class="designer-card designer-card--ps-cat">
                <div class="designer-card__ps-rating">

                    <div id="star-rating-block-65968" class="star-rating-block" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                        <meta itemprop="ratingValue" content="5">
                        <meta itemprop="worstRating" content="1">
                        <meta itemprop="bestRating" content="5">
                        <div class="star-rating rating-xs rating-disabled"><div class="rating-container tsi" data-content=""><div class="rating-stars" data-content="" style="width: 100%;"></div><input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div></div>

                        <span class="star-rating-range"><strong>5</strong>/5</span>
                        <span class="star-rating-count">(<span itemprop="reviewCount">5</span> reviews)</span></div>

                </div>

                <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                    Pro        </div>

                <div class="designer-card__userinfo">
                    <a class="designer-card__avatar" href="#" target="_blank"><img src="http://ts.vcap.me/static/user/a9b7ba70783b617e9998dc4dd82eb3c5/ps_logo_circle_1487765230_64x64.png"></a>
                    <h3 class="designer-card__username">
                        <a href="#" target="_blank" title="SF Test PS">
                            SF Test PS                </a>
                    </h3>
                </div>
                <div class="designer-card__ps-pics">
                    <div class="designer-card__ps-portfolio swiper-container swiper-container-horizontal" style="cursor: -webkit-grab;">
                        <div class="swiper-wrapper">
                            <a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active" href="http://ts.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_720x540.png" data-lightbox="ps1"><img src="http://ts.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_160x90.png" alt="Designer33.png"></a><a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next" href="http://ts.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_720x540.png" data-lightbox="ps1"><img src="http://ts.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_160x90.png" alt="PrintServiceS1.png"></a><a class="designer-card__ps-portfolio-item swiper-slide" href="http://ts.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_720x540.png" data-lightbox="ps1"><img src="http://ts.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_160x90.png" alt="PrintServiceS2.png"></a>                        </div>
                        <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;"><div class="swiper-scrollbar-drag" style="width: 0px;"></div></div>
                    </div>
                </div>
                <div class="designer-card__ps-loc" title="NEW YORK, NY, US">
                    <span class="tsi tsi-map-marker"></span>
                    NEW YORK, NY, US                </div>

                <div class="designer-card__about">
                    You can become one of our 3D printing services worldwide. It doesn’t m<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a><span class="tsrm-content hide">atter if you have many devices or only one. The registration is free, but we ask you to give precise information about you, your 3D printer and your company as we need it to comply with all the laws.</span>        </div>

                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label">Materials:</span>
                            Gypsum, PLA, Bronze PLA                                    </div>
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label">Min. Print Price:</span>
                            $30.00                                    </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label">Shipping:</span>
                            Standard                </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-12 col-lg-6">
                    </div>
                    <div class="col-sm-6 col-md-12 col-lg-6">
                        <div class="designer-card__btn-block">
                            <a class="btn btn-danger btn-ghost ts-ga" data-categoryga="PublicPs" data-actionga="PrintHere" href="#">
                                <span class="tsi tsi-printer3d"></span>
                                Buy Here                    </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="ps-cat-links ps-cat-links--bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="promo-page-title ps-cat-links__title"><?= _t('site.ps', 'Featured Locations'); ?></h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-4">
                <ul>
                    <li><a href="#city">New York</a></li>
                    <li><a href="#city">Los Angeles</a></li>
                    <li><a href="#city">Chicago</a></li>
                    <li><a href="#city">Mexico</a></li>
                    <li><a href="#city">Moscow</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-4">
                <ul>
                    <li><a href="#city">Chicago</a></li>
                    <li><a href="#city">Los Angeles</a></li>
                    <li><a href="#city">New York</a></li>
                    <li><a href="#city">Moscow</a></li>
                    <li><a href="#city">Mexico</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-4">
                <ul>
                    <li><a href="#city">Moscow</a></li>
                    <li><a href="#city">Los Angeles</a></li>
                    <li><a href="#city">Mexico</a></li>
                    <li><a href="#city">Chicago</a></li>
                    <li><a href="#city">New York</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="ps-cat-links">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="promo-page-title ps-cat-links__title"><?= _t('site.ps', 'Top Technologies'); ?></h2>
                <div class="row">
                    <div class="col-xs-6">
                        <ul>
                            <li><a href="#city">FDM</a></li>
                            <li><a href="#city">SLA</a></li>
                            <li><a href="#city">LOM</a></li>
                            <li><a href="#city">DLP</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6">
                        <ul>
                            <li><a href="#city">SLM</a></li>
                            <li><a href="#city">EBM</a></li>
                            <li><a href="#city">SLS</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <h2 class="promo-page-title ps-cat-links__title"><?= _t('site.ps', 'Featured Materials'); ?></h2>
                <div class="row">
                    <div class="col-xs-6">
                        <ul>
                            <li><a href="#city">ABS</a></li>
                            <li><a href="#city">PLA</a></li>
                            <li><a href="#city">Wood</a></li>
                            <li><a href="#city">Full-Color Sandstone</a></li>
                            <li><a href="#city">Polyester</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6">
                        <ul>
                            <li><a href="#city">Resin</a></li>
                            <li><a href="#city">Flexible Plastic</a></li>
                            <li><a href="#city">Nylon</a></li>
                            <li><a href="#city">High-Performance Plastics</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
