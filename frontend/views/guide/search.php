<?php

/** @var \yii\web\View $this */
/** @var $urlStrategy HelpUrlStrategy */
/** @var $q string */


use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use frontend\views\guide\HelpUrlStrategy;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ListView;

$this->params['hidesearch'] = true;
$this->title = _t('front.site', 'Guides');
?>

<div class="site-help">
    <div class="border-b m-b10">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div class="site-help__title">
                        <?=  _t('front.site', 'Guides'); ?>
                    </div>
                </div>
                <div class="col-sm-8 col-md-9 wide-padding--left">
                    <div class="site-help__search">
                        <form method="get" action="<?= $urlStrategy->search()?>">
                            <div class="input-group">
                                <input type="text" name="q" value="<?= H($q);?>" class="form-control" placeholder="<?= _t('front.site', 'Search for guides...'); ?>">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><?= _t('front.site', 'Search'); ?></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3 sidebar">
                <a class="btn btn-default btn-block help-nav-back" href="<?= $urlStrategy->indexUrl()?>">
                    <span class="tsi tsi-arrow-left-l"></span>
                    <?= _t('front.site', 'Back to Guides'); ?>
                </a>
            </div>

            <div class="col-sm-8 col-md-9 wide-padding--left">
                <h1><?= _t('front.site', 'Search results'); ?></h1>
                <ol>
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => function($helpItem) use ($urlStrategy) {
                        if($helpItem instanceof SiteHelp){
                            $link = $urlStrategy->articleUrl($helpItem);
                            $shortInfo = StringHelper::truncate(strip_tags($helpItem->content), 150);
                            $result = sprintf('<li><h3>%s</h3><p class="text-muted">%s</p>
                                <p class="text-success">Link: %s</li>', Html::a(H($helpItem->title), $link), $shortInfo, Html::a($link, $link, ['class'=>'text-success']));
                        }else if($helpItem instanceof SiteHelpCategory){
                            $link = $urlStrategy->categoryUrl($helpItem);
                            $shortInfo = StringHelper::truncate(strip_tags($helpItem->info), 150);
                            $result = sprintf('<li><h3>%s</h3><p class="text-muted">%s</p>
                                <p class="text-success">Link: %s</li>', Html::a(H($helpItem->title), $link), $shortInfo, Html::a($link, $link, ['class'=>'text-success']));

                        }
                        return $result;
                    },
                ]); ?>
                </ol>

            </div>
        </div>
    </div>
</div>