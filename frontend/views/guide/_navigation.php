<?php

/** @var $currentArticle SiteHelp|null Current article */
/** @var $currentCategory SiteHelpCategory|null Current category */
/** @var $urlStrategy HelpUrlStrategy */
/** @var $topCategory SiteHelpCategory */

use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use frontend\views\guide\HelpUrlStrategy;
use yii\helpers\Url;


$helpCategories = $topCategory->getSiteHelpCategories()
    ->orderByPriority()
    ->all();

$currentArticle = $currentArticle ?? null;
$currentCategory = $currentCategory ?? ($currentArticle ? $currentArticle->category : null);


/*$itemsFn = function (SiteHelpCategory $category, HelpUrlStrategy $urlStrategy, $itemsFn) use ($currentArticle)
{

    $item = [
        'label' => $category->title,
        'url' => $urlStrategy->categoryUrl($category),
        'items' => [],
    ];

    foreach ($category->siteHelpCategories as $child) {
        $item['items'][] = $itemsFn($child, $urlStrategy, $itemsFn);
    }

    foreach ($category->siteHelps as $article) {

        $item['items'][] = [
            'label' => $article->title,
            'url' => $urlStrategy->articleUrl($article),
            'visible' => $article->is_active,
            'active' => $currentArticle->id == $article->id
        ];
    }

    return $item;
};

$menuItems = $itemsFn($topCategory, $urlStrategy, $itemsFn)['items'];*/
?>
<ul class="side-nav help-nav" id="navMobile">
    <?php foreach($helpCategories as $category):
        $class = '';
        $innerNavToggle = '';
        $innerNavMenu = '';
        if($currentArticle && $currentCategory && $currentCategory->equals($category)){
            $class = 'class="open"';
            $innerNavToggle = 'aria-expanded="true"';
            $innerNavMenu = 'in';
        }else if($currentCategory && $currentCategory->equals($category)){
            $class = 'class="active open"';
        }
        ?>
        <li <?=$class;?>>
            <a <?php ?>
                    data-toggle="collapse"
                    data-parent=".side-nav-mobile"
                    href="#navMobileInner<?php echo $category->id; ?>"
                    role="button"
                <?=$innerNavToggle;?>
                    class="collapsed">
                <?php echo $category->title; ?> <span class="tsi tsi-down"></span>
            </a>
            <ul class="side-nav collapse <?=$innerNavMenu;?>" id="navMobileInner<?php echo $category->id; ?>">
                <?php

                $areticles = SiteHelp::find()
                    ->active()
                    ->forCategory($category)
                    ->orderByProprity()
                    ->all();

                foreach($areticles as $article): ?>
                    <li <?= $currentArticle && $currentArticle->equals($article) ?'class="active"':'';?>><a href="<?= $urlStrategy->articleUrl($article); ?>"><?php echo $article->title; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </li>
    <?php endforeach; ?>
</ul>