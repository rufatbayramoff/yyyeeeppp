<?php

/** @var \yii\web\View $this */
/** @var SiteHelpCategory $category */
/** @var $urlStrategy HelpUrlStrategy */
/** @var $topCategory SiteHelpCategory */

use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use frontend\components\image\ImageHtmlHelper;
use frontend\views\guide\HelpUrlStrategy;

echo $this->render('_metatags', ['urlStrategy' => $urlStrategy, 'category' => $category]);
echo $this->render('_breadcrumbs', ['category' => $category, 'urlStrategy' => $urlStrategy]);

/**
 * Return thumbial image of article or category
 * @param SiteHelpCategory|SiteHelp $obj
 * @return string
 */
$thumbFn = function ($obj) {
    if (!$obj->coverFile) {
        return '';
    }
    return ImageHtmlHelper::getThumbUrlForFile($obj->coverFile, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
};

$articles = $category->getSiteHelps()
    ->active()
    ->orderByProprity()
    ->all();

?>


<div class="guides-page">

    <?= $this->render('_sidebar', compact('urlStrategy', 'topCategory', 'category')); ?>

    <div class="guides-page__body">
        <div class="container guides-page__container">

            <h1 class="h1"><?= _t('site.guide', $category->title)?></h1>

            <?php if($category->info): ?>
                <p>
                    <?= $category->info?>
                </p>
            <?php endif; ?>

            <div class="guides-articles">
                <?php foreach ($articles as $article) : ?>
                    <a href="<?= $urlStrategy->articleUrl($article) ?>" class="guides-articles__item" style="background-image: url('<?= $thumbFn($article) ?>')">
                        <span class="guides-articles__title"><?= $article->title?></span>
                    </a>
                <?php endforeach; ?>
            </div>

            <div class="p-l0 p-r0 p-0">
                <div class="tsadelement tsadelement--horizontal">
                    <div id="amzn-assoc-ad-36ecd97a-d7c3-42bd-b069-67da508d2ac4"></div>
                    <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=36ecd97a-d7c3-42bd-b069-67da508d2ac4"></script>
                </div>
            </div>

        </div>
    </div>
</div>
