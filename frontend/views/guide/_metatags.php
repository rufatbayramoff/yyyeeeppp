<?php

use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use frontend\views\guide\HelpUrlStrategy;
use yii\helpers\StringHelper;

/** @var $title string Page title */
/** @var $article SiteHelp|null Current article */
/** @var $category SiteHelpCategory|null Current category */
/** @var $urlStrategy HelpUrlStrategy */

$article = $article ?? null;
$category = $category ?? null;

$this->params['hidesearch'] = true;

$this->title = _t('front.site', 'Guides');

$this->params['meta_keywords'] = '';
$this->params['meta_description'] = $this->title;

if($article){
    $this->title = "{$this->title} {$article->title}";
    $this->params['meta_description'] = StringHelper::truncateWords(strip_tags($article->content), 21);
    $this->registerLinkTag(['rel' => 'canonical', 'href' => $urlStrategy->articleUrl($article)]);
}
elseif($category) {
    $this->params['meta_description'] = StringHelper::truncateWords(strip_tags($category->info), 21);
    $this->title = "{$this->title} {$category->title}";
    $this->registerLinkTag(['rel' => 'canonical', 'href' => $urlStrategy->categoryUrl($category)]);
}

$this->registerMetaTag([
    'property' => 'fb:app_id',
    'content' => param('facebook_app_id')
]);

$ogTags = [
    'og:url' => param('server') . app('request')->url,
    'og:title' => $this->title,
    'og:description' => \H(strip_tags($this->params['meta_description'])),
    'og:type' => 'website'
];

$this->params['ogtags'] = $ogTags;

$this->registerJs("var facebook_app_id='".param('facebook_app_id')."'", \yii\web\View::POS_HEAD);