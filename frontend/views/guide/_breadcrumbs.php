<?php

/** @var $article SiteHelp|null Current article */
/** @var $category SiteHelpCategory|null Current category */
/** @var $urlStrategy HelpUrlStrategy */

use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use frontend\views\guide\HelpUrlStrategy;
use yii\base\Exception;
use yii\widgets\Breadcrumbs;

$article = $article ?? null;
$category = $category ?? null;

$linksFn = function(SiteHelp $article = null, SiteHelpCategory $category = null, HelpUrlStrategy $urlStrategy)
{
    if (!$article && !$category) {
        throw new Exception("Category or article must be setted.");
    }

    $links = [];

    $parentCategory = null;

    if ($article) {
        $links[] = [
            'label' => $article->title,
            'url' => $urlStrategy->articleUrl($article)
        ];
        $parentCategory = $article->category;
    }
    elseif ($category) {
        $links[] = [
            'label' => $category->title,
            'url' => $urlStrategy->categoryUrl($category)
        ];
        $parentCategory = $category->parent;
    }

    while ($parentCategory) {
        array_unshift($links, [
            'label' => $parentCategory->title,
            'url' => $urlStrategy->categoryUrl($parentCategory)
        ]);
        $parentCategory = $parentCategory->parent;
    }

    if (isset($links[0])) {
        $links[0]['url'] = $urlStrategy->indexUrl();
    }

    $breadcrumps = [];
    foreach ($links as $key=>$link) {
        $breadcrumps[$link['label']] = $link['url'];
    }
    return $breadcrumps;
};


$breadcrumps = $linksFn($article, $category, $urlStrategy);

?>

<?php if($breadcrumps) : ?>

    <div class="over-nav-tabs-header m-b0">
        <div class="container guides-page__container">
            <ol class="breadcrumb m-t10 m-b0">
                <?php
                foreach ($breadcrumps as $breadcrumpText => $breadcrumpUrl) {
                    echo '<li>';
                    echo $breadcrumpUrl ? '<a href="' . HL($breadcrumpUrl) . '">' : '';
                    echo H($breadcrumpText);
                    echo $breadcrumpUrl ? '</a>' : '';
                    echo '</li>';
                }
                ?>
            </ol>
            <?php
            echo \frontend\widgets\BreadcrumbsSchemaWidget::widget([
                'breadcrumpsItems' => $breadcrumps
            ]);
            ?>
        </div>
    </div>
<?php endif ?>



