<?php

use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use frontend\components\image\ImageHtmlHelper;
use frontend\views\guide\HelpUrlStrategy;

/** @var \yii\web\View $this */
/** @var SiteHelpCategory $topCategory */
/** @var $urlStrategy HelpUrlStrategy */
/** @var $dataProvider \yii\data\ActiveDataProvider */
/** @var $tag string */


/**
 * Return thumbial image of article or category
 * @param SiteHelpCategory|SiteHelp $obj
 * @return string
 */
$thumbFn = function ($obj) {

    if (!$obj->coverFile) {
        return '';
    }

    return ImageHtmlHelper::getThumbUrlForFile($obj->coverFile, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
};

$this->render('_metatags', ['urlStrategy' => $urlStrategy]);

?>

<div class="guides-page">

    <?= $this->render('_sidebar', compact('urlStrategy', 'topCategory')); ?>

    <div class="guides-page__body">
        <div class="container guides-page__container">

            <h2 class="h1"><?= H(_t('site.guide', '{tag}', ['tag' => $tag])) ?></h2>

            <div class="guides-articles">

                <?php foreach ($dataProvider->getModels() as $article) : ?>

                    <a href="<?= $urlStrategy->articleUrl($article) ?>" class="guides-articles__item" style="background-image: url('<?= $thumbFn($article) ?>')">
                        <span class="guides-articles__title"><?= $article->title?></span>
                    </a>

                <?php endforeach; ?>
            </div>

        </div>
    </div>
</div>