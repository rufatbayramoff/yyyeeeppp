<?php

use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use frontend\components\image\ImageHtmlHelper;
use frontend\views\guide\HelpUrlStrategy;

/** @var \yii\web\View $this */
/** @var SiteHelpCategory $topCategory */
/** @var $urlStrategy HelpUrlStrategy */
/** @var SiteHelpCategory[] $categories */
$categories = $topCategory->getSiteHelpCategories()
    ->withCoverFile()
    ->orderByPriority()
    ->all();


/** @var SiteHelpCategory[] $popularCategories */
$popularCategories = $topCategory->getSiteHelpCategories()
    ->hasPopularArticles()
    ->withCoverFile()
    ->orderByPriority()
    ->all();

/**
 * Return thumbial image of article or category
 *
 * @param SiteHelpCategory|SiteHelp $obj
 * @return string
 */
$thumbFn = function ($obj) {

    if (!$obj->coverFile) {
        return '';
    }

    return ImageHtmlHelper::getThumbUrlForFile($obj->coverFile, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
};


$this->render('_metatags', ['urlStrategy' => $urlStrategy, 'category' => $topCategory]);

echo $this->render('@frontend/views/seo/seoHeader.php');
?>
<div class="guides-page">

    <?= $this->render('_sidebar', compact('urlStrategy', 'topCategory')); ?>

    <div class="guides-page__body">
        <div class="container guides-page__container">

            <h2 class="h1"><?= $topCategory->title ?></h2>

            <?php if ($topCategory->info): ?>
                <p>
                    <?= $topCategory->info ?>
                </p>
            <?php endif; ?>


            <div class="guides-themes">

                <?php foreach ($categories as $category) : ?>

                    <a href="<?= $urlStrategy->categoryUrl($category) ?>" class="guides-themes__item">
                        <img class="guides-themes__pic" src="<?= $thumbFn($category) ?>" alt="">
                        <span class="guides-themes__title"><?= $category->title ?></span>
                    </a>

                <?php endforeach; ?>

            </div>

            <?php foreach ($popularCategories as $category) : ?>

                <h3 class="h2 m-t30 m-b20"><?= $category->title ?></h3>

                <div class="guides-articles">

                    <?php foreach ($category->getSiteHelps()->active()->popular()->orderByProprity()->all() as $article) : ?>

                        <a href="<?= $urlStrategy->articleUrl($article) ?>" class="guides-articles__item" style="background-image: url('<?= $thumbFn($article) ?>')">
                            <span class="guides-articles__title"><?= $article->title ?></span>
                        </a>

                    <?php endforeach; ?>

                </div>

            <?php endforeach; ?>

            <div class="p-l0 p-r0 p-0">
                <div class="tsadelement tsadelement--horizontal">
                    <div id="amzn-assoc-ad-36ecd97a-d7c3-42bd-b069-67da508d2ac4"></div>
                    <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=36ecd97a-d7c3-42bd-b069-67da508d2ac4"></script>
                </div>
            </div>

        </div>
    </div>
</div>