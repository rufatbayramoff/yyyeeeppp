<?php

/** @var \yii\web\View $this */
/** @var SiteHelp|null $article */
/** @var SiteHelpCategory|null $category */
/** @var $urlStrategy HelpUrlStrategy */
/** @var $topCategory SiteHelpCategory */

use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use frontend\views\guide\HelpUrlStrategy;

$article = $article ?? null;
$category = $category ?? null;

?>

<div class="guides-page__nav">

    <div class="guides-page__search">
        <form method="get" action="<?= $urlStrategy->search()?>" class="ng-pristine ng-valid">
            <div class="input-group">
                <input type="text" name="q" class="form-control input-sm" placeholder="<?= _t('site.guide', 'Search in Guides...')?>">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-sm" type="submit">
                        <span class="tsi tsi-search"></span>
                    </button>
                </span>
            </div>
        </form>
    </div>

    <a href="/guide" class="guides-page__title"><?= _t('site.guide', 'Manufacturing Guides')?></a>

    <div class="side-nav-mobile">
        <div class="side-nav-mobile__title" data-toggle="collapse" data-target="#navMobile">
            <?= _t('site.guide', 'Guides Menu')?>
            <span class="tsi tsi-down"></span>
        </div>

        <?=
            $this->render('_navigation', [
                'currentCategory' => $category,
                'currentArticle' => $article,
                'urlStrategy' => $urlStrategy,
                'topCategory' => $topCategory
            ]);
        ?>

    </div>

    <div class="hidden-xs">
        <hr>
        <div class="tsadelement">
            <div id="amzn-assoc-ad-a62ceb71-6f15-49a6-93a9-810eeb3bdbc2"></div>
            <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=a62ceb71-6f15-49a6-93a9-810eeb3bdbc2"></script>
        </div>
    </div>

</div>
<?php
$this->registerJs("
$('.side-nav a[data-toggle=\"collapse\"]').click(function() {
  $('.side-nav .collapse.in').collapse('hide')
});
");