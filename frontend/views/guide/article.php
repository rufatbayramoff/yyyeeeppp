<?php

/** @var \yii\web\View $this */
/** @var SiteHelp $article */
/** @var $urlStrategy HelpUrlStrategy */
/** @var $topCategory SiteHelpCategory */

use common\components\JsObjectFactory;
use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use frontend\assets\SocialButtonsAsset;
use frontend\views\guide\HelpUrlStrategy;
use yii\helpers\Url;


echo $this->render('_metatags', ['urlStrategy' => $urlStrategy, 'article' => $article]);
echo $this->render('_breadcrumbs', ['article' => $article, 'urlStrategy' => $urlStrategy]);

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

$coverImgUrl = $article->coverFile ? $article->coverFile->getFileUrl() : null;
?>

<div class="guides-page">

    <?= $this->render('_sidebar', compact('urlStrategy', 'topCategory', 'article')); ?>

    <div class="guides-page__body guides-page__body--inner-page">

        <div class="post-hero">

            <div class="row post-hero__text">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h1 class="post-hero__title">
                        <?= $article->title ?>
                    </h1>
                    <div class="post__footer post__footer--hero">
                        <a href="#comments" class="post__comments post__comments--hero">
                            <span class="tsi tsi-comment"></span> <span class="fb-comments-count fb_comments_count_zero" data-href="http://ts5.vcap.me/blog/Must-Have-3D-print" fb-xfbml-state="rendered"><span class="fb_comments_count">0</span></span> comments                    </a>
                    </div>

                    <div class="m-t30 text-center">





                        <p class="post-share__label"><?= _t('site.blog', 'Share with friends')?>:</p>

                        <div class="model-share__list js-social-likes post-share__list">

                            <div class="model-share__btn"><a href="<?= Url::toRoute(['/site/sendemail'])?>" title="<?= _t("site.email", "Send email"); ?>" class="ts-ajax-modal">
                                    <span class="social-likes__icon social-likes__icon_mail"></span></a>
                            </div>

                            <div class="model-share__btn sharelink" id="sharelink" data-placement="bottom" data-container="body" data-toggle="popover" data-original-title="<?= _t("site.share", "Share this link"); ?>">
                                <span class="social-likes__icon social-likes__icon_link" title="<?= _t("site.email", "Share this link"); ?>"></span>
                            </div>

                            <div class="model-share__btn facebook-send"  title="<?= _t("site.email", "Facebook Messenger"); ?>" >
                                <span class="social-likes__icon social-likes__icon_facebook-m"></span> Facebook Send
                            </div>

                            <div class="model-share__btn facebook" title="<?= _t("site.email", "Share this link"); ?>" data-display="popup">Facebook</div>
                            <div class="model-share__btn twitter" title="<?= _t("site.email", "Tweet this link"); ?>" data-via="treatstock"> Twitter</div>
                            <div class="model-share__btn pinterest" title="<?= _t("site.email", "Pin this model"); ?>" data-media="<?= $coverImgUrl ?>"> Pinterest</div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="post-hero__bg" style="background-image: url('<?= $coverImgUrl?>')"></div>

        </div>

        <div class="container guides-page__container">

            <?php if($article->getTags()): ?>

                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 post-tags">

                        <?php foreach ($article->getTags() as $tag) : ?>

                            <a href="<?= $urlStrategy->searchByTag($tag) ?>" class="post-tags__item"><?= $tag ?></a>

                        <?php endforeach ?>
                    </div>
                </div>

            <?php endif; ?>

            <div class="post-content">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 post-content__body">
                        <?= $article->content ?>

                        <div class="p-l0 p-r0 p-0">
                            <div class="tsadelement tsadelement--horizontal">
                                <div id="amzn-assoc-ad-36ecd97a-d7c3-42bd-b069-67da508d2ac4"></div>
                                <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=36ecd97a-d7c3-42bd-b069-67da508d2ac4"></script>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs("var facebook_app_id = '".param('facebook_app_id')."';", \yii\web\View::POS_HEAD);
$this->registerAssetBundle(SocialButtonsAsset::class);
JsObjectFactory::createJsObject(
    'socialButtonsClass',
    'socialButtonsObj',
    []
    ,
    $this
);
?>

<script>
    <?php $this->beginBlock('js1', false); ?>

    //Init Picture Slider
    var swiperPictureSlider = new Swiper('.picture-slider', {
        scrollbar: '.picture-slider__scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>

