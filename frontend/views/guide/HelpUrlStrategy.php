<?php
/**
 * Created by mitaichik
 */

namespace frontend\views\guide;


use common\components\ArrayHelper;
use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use yii\helpers\Url;

/**
 * Class HelpUrlStrategy
 * @package frontend\views\guide
 */
class HelpUrlStrategy
{
    /**
     * @var array
     */
    private $controllerRoute;

    /**
     * HelpUrlStrategy constructor.
     * @param $controllerRoute mixed Route to controller. May be array or string.
     */
    public function __construct($controllerRoute)
    {
        $this->controllerRoute = (array)$controllerRoute;
    }

    /**
     * Return url of index page.
     * @return string
     */
    public function indexUrl(): string
    {
        return Url::toRoute($this->controllerRoute);
    }

    /**
     * Return url of category.
     * @param SiteHelpCategory $category
     * @return string
     */
    public function categoryUrl(SiteHelpCategory $category): string
    {
        $route    = $this->controllerRoute;
        $route[0] = $route[0] . '/category';
        $route    = ArrayHelper::merge($route, ['slug' => mb_strtolower($category->slug)]);
        return Url::toRoute($route, true);
    }

    /**
     * Return url of article.
     * @param SiteHelp $article
     * @return string
     */
    public function articleUrl(SiteHelp $article): string
    {
        $route    = $this->controllerRoute;
        $route[0] = $route[0] . '/article';
        $route    = ArrayHelper::merge($route, ['id' => $article->id, 'slug' => $article->getSlug()]);
        return Url::toRoute($route, true);
    }

    /**
     * Return url for view all articles for tag.
     * @param string $tag
     * @return string
     */
    public function searchByTag(string $tag): string
    {
        $route        = $this->controllerRoute;
        $route[0]     = $route[0] . '/tag';
        $route['tag'] = $tag;

        return Url::toRoute($route);
    }

    /**
     * Return url for serarch form.
     * @return string
     */
    public function search(): string
    {
        $route    = $this->controllerRoute;
        $route[0] = $route[0] . '/search';
        return Url::toRoute($route);
    }
}