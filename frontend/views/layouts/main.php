<?php

use common\models\PaymentCurrency;
use common\models\SystemLang;
use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use common\modules\googleAnalitics\widget\GoogleRecaptchaV3Widget;
use frontend\assets\AppAsset;
use frontend\components\FrontendWebView;
use frontend\components\UserSessionFacade;
use frontend\models\community\MessageFacade;
use frontend\models\user\FrontUser;
use frontend\models\user\LoginForm;
use frontend\models\user\SignupForm;
use frontend\models\user\UserFacade;
use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;
use frontend\widgets\Alert;
use frontend\widgets\Currency;
use frontend\widgets\GoogleAnalyticsWidget;
use frontend\widgets\Lang;
use frontend\widgets\TopFindServiceWidget;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\Menu;

/* @var $this FrontendWebView */
/* @var $content string */
AppAsset::register($this);
Yii::$app->angular->registerScripts($this);

/** @var FrontUser|null $user */
$user                    = Yii::$app->user->getIdentity();
$companyExistOrNotActive = is_guest() ? false : $user->hasActiveCompany();
$siteUser                = new FrontUser();
$mainPage                = app('request')->pathInfo == '';
$spaceless               = YII_ENV === 'prod';
$isErrorPage             = $this->context->action ? $this->context->action->id == 'error' : false;
$gatrackId               = ''; // google analytics track id
$description             = ''; //Innovative network for 3d designers and printservices.';
$keywords                = ''; //3d printing services, 3d printable models, 3d printers, 3d prints';
$isSecurePage            = !empty($this->params['is_secure']);
if (!empty($this->params['meta_title'])) {
    $this->title = $this->params['meta_title'];
}
if (!empty($this->params['meta_keywords'])) {
    $keywords = H($this->params['meta_keywords']);
}
if (!empty($this->params['meta_description'])) {
    $description = $this->params['meta_description'];
}

if ($this->context->seo && !$isErrorPage) {
    if (!empty($this->context->seo->title)) {
        $this->title = $this->context->seo->title;
    }
    if (!empty($this->context->seo->meta_description)) {
        $description = H($this->context->seo->meta_description);
    }
    if (!empty($this->context->seo->meta_keywords)) {
        $keywords = H($this->context->seo->meta_keywords);
    }
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="title" content="<?= H($this->title) ?>"/>
    <meta name="description" content="<?= $description; ?>"/>
    <meta name="keywords" content="<?= $keywords; ?>"/>
    <?= Html::csrfMetaTags() ?>
    <?php
    if (!empty($this->params['ogtags'])):
        foreach ($this->params['ogtags'] as $ogKey => $ogValue) {
            $this->registerMetaTag(
                [
                    'property' => $ogKey,
                    'content'  => $ogValue
                ],
                true
            );
            echo '<meta property="' . $ogKey . '" content="' . str_replace('"', '\'', $ogValue) . '" />' . "\n";
        }
    else:

        ?>
        <meta property="og:url" content="https://www.treatstock.com/"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="Treatstock"/>
        <meta property="og:description" content="Professional manufacturing services for business"/>
        <meta property="og:image"
              content="https://static.treatstock.com/static/images/common/treatstock-soc-img-5min.jpg"/>
        <meta property="og:image:secure_url"
              content="https://static.treatstock.com/static/images/common/treatstock-soc-img-5min.jpg"/>
        <meta itemprop="image" content="https://static.treatstock.com/static/images/common/treatstock-soc-img-5min.jpg">
        <meta property="og:image:type" content="image/jpeg"/>
        <meta property="og:image:width" content="1200"/>
        <meta property="og:image:height" content="630"/>
        <link rel="image_src" href="https://static.treatstock.com/static/images/common/treatstock-soc-img-5min.jpg"/>
        <?php if ($mainPage) { ?>
        <?php if (in_array($_SERVER['HTTP_HOST'], ['www.treatstock.com'])): ?>
            <link href="https://www.treatstock.com" rel="canonical"/>
        <?php endif; ?>

        <?php if (in_array($_SERVER['HTTP_HOST'], ['www.treatstock.co.uk'])): ?>
            <link href="https://www.treatstock.co.uk" rel="canonical"/>
        <?php endif; ?>
    <?php } ?>

    <?php endif; ?>
    <meta name="msapplication-config" content="/static/images/favicon/browserconfig.xml"/>
    <?php if (!empty($this->params['script'])) {
        $scriptType    = $this->params['script']['type'];
        $scriptContent = $this->params['script']['content'];
        if ($scriptType == 'application/ld+json') {
            $scriptContent = json_encode($scriptContent, JSON_PRETTY_PRINT);
        }
        echo "\n<script type=\"" . $scriptType . "\">\n" . $scriptContent . "\n</script>\n";
    } ?>
    <?php if (!empty($this->params['noindex'])) { ?>
        <meta name="robots" content="noindex, nofollow"/>
    <?php } ?>

    <title><?= H($this->title) ?></title>
    <link rel="preload" href="/fonts/ts-icons.ttf?5u760k" as="font" type="font/ttf" crossorigin>
    <link rel="apple-touch-icon" sizes="57x57" href="/static/images/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/static/images/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/static/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/static/images/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/static/images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/static/images/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/static/images/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/static/images/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/static/images/favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/static/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/static/images/favicon/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="/static/images/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/static/images/favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/static/images/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon/faviconie.ico"/>
    <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon/favicon-32x32.ico"/>
    <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon/favicon-48x48.ico"/>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
    <link rel="manifest" href="/static/images/favicon/manifest.json">
    <link rel="mask-icon" href="/static/images/favicon/safari-pinned-tab.svg" color="#e00457">
    <meta name="apple-mobile-web-app-title" content="Treatstock">
    <meta name="application-name" content="Treatstock">
    <meta name="msapplication-TileColor" content="#e00457">
    <meta name="msapplication-TileImage" content="/static/images/favicon/mstile-144x144.png">
    <meta name="theme-color" content="#e00457">

    <?php

    $currentUrl = Yii::$app->request->hostInfo . Yii::$app->request->url;
    $mainUrl    = Yii::$app->getModule('intlDomains')->domainManager->getUrlWithLangDomain($currentUrl, 'en-US', false);
    ?>
    <link href="<?= $mainUrl ?>" rel="alternate" hreflang="x-default"/>
    <?php

    foreach (SystemLang::getAllActive() as $lang) {
        $langDomain = Yii::$app->getModule('intlDomains')->domainManager->getDomainForLang($lang->iso_code);
        if (!empty($this->params['alternateLang'][$lang->iso_code])) {
            $langUrl = $this->params['alternateLang'][$lang->iso_code];
        } else {
            $langUrl = Yii::$app->getModule('intlDomains')->domainManager->getUrlWithLangDomain($currentUrl, $lang->iso_code, false);
        }
        ?>
        <link href="<?= $langUrl ?>" rel="alternate" hreflang="<?= $lang->url; ?>"/>
        <?php

    }
    ?>
    <?= GoogleAnalyticsWidget::widget(['isSecurePage' => $isSecurePage]); ?>

    <!--[if lte IE 9]>
    <link href="<?= param('server') . '/css/ie9.css?v=' . @filemtime(Yii::getAlias('@webroot/css/ie9.css')); ?>"
          rel="stylesheet" type="text/css"/>
    <![endif]-->

    <style type="text/css">
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
    </style>

    <!-- Facebook Pixel Code -->
    <?php
    if (param('facebookPixelEnabled')) {
        ?>
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1189389281172887');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1189389281172887&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->
        <?php
    }
    ?>

    <?php $this->head() ?>
</head>
<body class="body-site<?php if ($mainPage) {
    echo ' main-page';
} else {
    if ($isErrorPage) {
        echo ' error-page';
    }
} ?><?php echo($this->getHeaderSearchFormDisabledClass() ? ' ' . $this->getHeaderSearchFormDisabledClass() : ''); ?>"
      ng-app="app">
<?php
if ($spaceless) {
    yii\widgets\Spaceless::begin();
} ?>
<?php $this->beginBody() ?>
<header class="header-container">
    <div class="adaptive-search-block">
    </div>

    <div id="header-bar" class="header-bar">
        <div class="navbar-header">
            <?php
            if (empty($this->params['hidenav'])):
                $topNotifyCount = $user ? UserFacade::getTopNotify($user) : 0;
                ?>
                <button type="button" class="navbar-toggle slidemenu__btn">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <?php if ($topNotifyCount): ?>
                        <span class="slidemenu__notification-label"></span>
                    <?php endif; ?>
                </button>
            <?php endif; //hidenav
            ?>
            <a class="header-logo" href="/" title="Treatstock 3D Printing Services">Treatstock</a>
            <?php if (!is_guest()): ?>
                <div class="header-bar__chat-mobile">
                    <?php $unreaded = (int)MessageFacade::getUnreadedTotal($user); ?>
                    <a class="btn btn-default header-bar__chat-btn visible-xs" href="<?php echo Url::toRoute(['/workbench/messages/index']) ?>"
                       title="<?= _t('site.main', 'Chat') ?>">
                        <?php if ($unreaded > 0): ?>
                            <span class="header-bar__chat-badge"><?php echo $unreaded ?></span>
                        <?php endif; ?>

                        <span class="tsi tsi-message"></span>
                        <span class="header-bar__chat-text">
                        <?= _t('site.main', 'Chat'); ?>
                    </span>
                    </a>
                </div>
            <?php endif; ?>
            <?php echo \frontend\widgets\TopMenuWidget::widget() ?>
        </div>

        <div id="header-bar__collapse" class="slidemenu__menu navbar-collapse">
            <div class="slidemenu__menu-closer"></div>

            <?php
            /*
             * Turn off Search block #4391
            if (empty($this->params['hidesearch'])) {
                echo \frontend\widgets\Search::widget();
            } else {
                \frontend\widgets\Search::widget();
            }
            */

            if (is_guest()) {
                $menuItems = frontend\models\user\UserFacade::getGuestMenu();
                $nav       = Nav::widget(
                    [
                        'options' => ['class' => 'header-bar__menu'],
                        'items'   => $menuItems,
                    ]
                );
                $nav       = str_replace("header-bar__menu nav", "header-bar__menu", $nav);
                #echo $nav;
                ?>

                <ul class="header-bar__menu">

                    <li class="navbar-toggle slidemenu__btn">
                        <!--  data-toggle="collapse" data-target="#header-bar__collapse" -->
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </li>


                    <?php if (!$companyExistOrNotActive): ?>

                        <li class="header-bar__become hidden-xs">
                            <a href="/l/become-supplier" class="btn btn-link t-header-bar__become">
                                <?= _t('site.main', 'Create a Business'); ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <li class="header-bar__auth">
                        <div class="header-bar__auth-icon">
                            <a class="header-bar__auth-icon-btn t-header-bar__auth-icon-btn--signin-mobile ts-ga"
                               data-categoryga="User" data-actionga="Signin" data-labelga="registration" href="#"
                               onclick="TS.Visitor.signUpForm('<?= app('request')->url; ?>');return false;"><span
                                        class="tsi tsi-user"></span></a>
                        </div>
                        <div class="header-bar__auth-block">
                            <a class="btn btn-link header-bar__auth-btn t-header-bar__auth-icon-btn--signin ts-ga"
                               data-categoryga="User" data-actionga="Signin" href="#"
                               onclick="TS.Visitor.loginForm('<?= app('request')->url; ?>', 'login');return false;"><?= _t('site.main', 'Sign in'); ?></a>
                        </div>
                    </li>
                    <li class="dropdown dropdown-onhover header-bar__auth header-bar__auth--in">
                        <ul id="tabs" class="header-nav nav nav-tabs visible-xs">
                            <li><a href="/products"><?= _t('site.main', 'Products'); ?></a></li>
                            <li>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle header-nav-drop__btn" type="button" id="dropdownMenuTop" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="true">
                                        <?= _t('site.main', 'Manufacturing services'); ?> <span class="tsi tsi-down"></span>
                                    </button>
                                    <div class="dropdown-menu header-nav-drop" aria-labelledby="dropdownMenuTop">
                                        <div class="header-nav-drop__content">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="header-nav-drop__big-list">
                                                        <a class="header-nav-drop__big" href="<?= CatalogPrintingUrlHelper::landingPage() ?>">
                                                            <img class="header-nav-drop__big-icon" src="https://static.treatstock.com/static/images/common/service-cat/3dprinting.svg">
                                                            <span class="header-nav-drop__big-text"><?= _t('site.main', '3D Printing'); ?></span>
                                                        </a>
                                                        <a class="header-nav-drop__big" href="/company-service/cnc-manufacturing">
                                                            <img class="header-nav-drop__big-icon" src="https://static.treatstock.com/static/images/common/service-cat/cnc-machining.svg">
                                                            <span class="header-nav-drop__big-text"><?= _t('site.main', 'CNC Machining'); ?></span>
                                                        </a>
                                                        <a class="header-nav-drop__big" href="/company-service/cutting">
                                                            <img class="header-nav-drop__big-icon" src="https://static.treatstock.com/static/images/common/service-cat/cutting-laser.svg">
                                                            <span class="header-nav-drop__big-text"><?= _t('site.main', 'Cutting'); ?></span>
                                                        </a>
                                                        <a class="header-nav-drop__big" href="<?= CatalogPrintingUrlHelper::printing3dCatalog(usageCode: 'hq-prototyping', sort: 'distance') ?>">
                                                            <img class="header-nav-drop__big-icon" src="https://static.treatstock.com/static/images/common/service-cat/hd-prototyping.svg">
                                                            <span class="header-nav-drop__big-text"><?= _t('site.main', 'HD Prototyping'); ?></span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="header-nav-drop__title">Services</div>
                                                    <div class="header-nav-drop__list">
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Manufacturing Services</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">CNC Turning</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">3D Design</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Injection Molding</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Signage</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Electronics Manufacturing Service</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">3D Scanning</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Painting</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Urethane Casting</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Sheet Fabrication</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Metal Casting</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Vacuum Forming</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Mold Making</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Metal Stamping</a>
                                                        </div>
                                                        <div class="header-nav-drop__list-item">
                                                            <a href="#">Other</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <?php
            } else {
                $businessMenu    = UserFacade::getTopBusinessMenuLinks($user);
                $workbenchMenu   = UserFacade::getTopWorkbenchLinks($user);
                $topNotifyCount  = UserFacade::getTopNotify($user);
                $displayUserName = H(frontend\models\user\UserFacade::getFormattedUserName($user));

                if (empty($this->params['hidenav'])):
                    ?>

                    <ul class="header-bar__menu">

                        <li type="button" class="navbar-toggle slidemenu__btn">
                            <!--  data-toggle="collapse" data-target="#header-bar__collapse" -->
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </li>

                        <?php if (!$companyExistOrNotActive): ?>
                            <li class="header-bar__become hidden-xs">
                                <a href="/l/become-supplier" class="btn btn-link t-header-bar__become">
                                    <?= _t('site.main', 'Create a Business'); ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php $unreaded = (int)MessageFacade::getUnreadedTotal($user); ?>
                        <li class="header-bar__chat">
                            <a class="btn btn-default header-bar__chat-btn" href="<?php echo Url::toRoute(['/workbench/messages/index']) ?>"
                               title="<?= _t('site.main', 'Chat') ?>">
                                <?php if ($unreaded > 0): ?>
                                    <span class="header-bar__chat-badge"><?php echo $unreaded; ?></span>
                                <?php endif; ?>
                                <span class="tsi tsi-message"></span>
                                <span class="header-bar__chat-text">
                                <?= _t('site.main', 'Chat'); ?>
                            </span>
                            </a>
                        </li>

                        <li class="dropdown dropdown-onhover header-bar__auth header-bar__auth--in">
                            <a class="btn btn-default header-bar__menu-btn dropdown-toggle dropdown-menu-right-toggle t-header-bar__menu-btn--profile-btn"
                               href="/user-profile" data-toggle="dropdown">
                                <div class="header-bar__avatar">
                                    <?php if ($topNotifyCount): ?>
                                        <div class="header-bar__notification-label"></div>
                                    <?php endif; ?>
                                    <?php echo frontend\components\UserUtils::getAvatarForUser($user, 'top-profile'); ?>
                                </div>

                                <span class="header-bar__username">
                                        <?php echo $displayUserName; ?>
                                </span>
                                <span class="tsi tsi-down"></span>
                            </a>

                            <?php echo \frontend\widgets\TopMenuWidget::widget(['navClass' => 'visible-xs']) ?>

                            <ul class="dropdown-menu dropdown-menu-right">

                                <li class="text-divider"><?= _t('site.menu', 'Dashboard'); ?></li>
                                <?php foreach ($workbenchMenu as $k => $v): ?>
                                    <li><a href="<?php echo yii\helpers\Url::toRoute($v['link']); ?>"
                                           tabindex="-1"><span
                                                    class="tsi <?php echo $v['icon']; ?>"></span><?php echo $v['title']; ?>
                                        </a></li>
                                <?php endforeach; ?>
                                <li class="text-divider"><?= _t('site.menu', 'My Business'); ?></li>
                                <?php if (!$companyExistOrNotActive): ?>
                                    <li>
                                        <a class="t-header-bar__store-service--start-ps" href="/mybusiness/company"
                                           onclick="<?= is_guest() ? "TS.Visitor.signUpForm('/mybusiness/company'); return false;" : ""; ?>">
                                            <span class="tsi tsi-printer3d"></span>
                                            <?= _t("site.main", "Create a Business"); ?>
                                        </a>
                                    </li>
                                <?php else: ?>
                                    <?php foreach ($businessMenu as $k => $v): ?>
                                        <li><a href="<?php echo yii\helpers\Url::toRoute($v['link']); ?>" tabindex="-1"><span
                                                        class="tsi <?php echo $v['icon']; ?>"></span><?php echo $v['title']; ?>
                                            </a></li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <li class="divider"></li>
                                <li><a class="t-header-bar__menu-btn--signout" href="/profile" tabindex="-1">
                                        <span class="tsi tsi-user"></span><?php echo _t("site", "Profile"); ?></a>

                                </li>


                                <li class="divider"></li>
                                <li><a class="t-header-bar__menu-btn--signout" href="/user/logout" data-method="post"
                                       tabindex="-1">
                                        <span class="tsi tsi-sign-out"></span><?php echo _t("site", "Sign Out"); ?></a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                <?php
                endif; // hidenav
            }
            // NavBar::end();
            ?>
        </div>
    </div>

    <?php if (!$this->isHeaderSearchFormDisabled()): ?>
        <div class="header-bottom clearfix">
            <?php echo TopFindServiceWidget::widget([]); ?>

            <div class="header-bottom__lang t-header-bottom__lang">
                <?php echo Lang::widget(); ?>
            </div>
        </div>
    <?php endif; ?>
</header>

<div class="wrap">
    <?php
    if (!is_guest()) {
        /** @var FrontUser $userModel */
        $userModel = Yii::$app->user->getIdentity();
        if ($userModel->needConfirm() && !(app('request')->url == '/store/delivery')) {
            echo frontend\widgets\UserConfirm::widget(['user' => $userModel]);
        }
    } ?>

    <?php if ($mainPage): ?>
        <div id="notify-message" class="m-t10">
            <?= Alert::widget(['options' => ['class' => '']]) ?>
        </div>
        <?= $content ?>
    <?php else: ?>
        <div id="notify-message">
            <?= Alert::widget(['options' => ['class' => '']]) ?>
        </div>


        <?= $content ?>

    <?php endif; ?>
</div>

<footer class="footer <?php if ($this->context->seo && $this->context->seo->footer_text) {
    echo 'footer--extra-info';
} ?> hide-full-screen">
    <div class="container container--wide">
        <div class="footer__extra-info">
            <?php if ($this->context->seo && $this->context->seo->footer_text) {
                echo $this->context->seo->footer_text;
            } ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="footer__lang">
                    <div class="footer__lang__title"><?= _t('front.site', 'Language') ?>:</div>
                    <div class="footer__lang__drop t-footer-lang"><?php echo Lang::widget(); ?></div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-xs-6 col-sm-3">

                <h4 class="footer__title"><?= _t('front.site', 'Company') ?></h4>

                <div class="footer__menu">
                    <ul class="footer__list">
                        <li><a href="/site/about"
                               class="t-footer-company__about"><?= _t('front.site', 'About Us') ?></a></li>
                        <li><a href="/site/how-it-works"
                               class="t-footer-company__how"><?= _t('front.site', 'How It Works'); ?></a></li>
                        <li><a href="/blog" class="t-footer-company__blog"><?= _t('front.site', 'Blog') ?></a></li>
                        <li><a href="/site/contact"
                               class="t-footer-company__contact"><?= _t('front.site', 'Contact Us') ?></a></li>
                        <li><a href="/press" class="t-footer-company__press"><?= _t('front.site', 'Press') ?></a></li>
                        <li><a href="/help" class="t-footer-resources__help"><?= _t('front.site', 'Help Center') ?></a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-6 col-sm-3">

                <h4 class="footer__title"><?= _t('front.site', 'Resources') ?></h4>

                <div class="footer__menu">
                    <ul class="footer__list">
                        <li><a href="/guide"
                               class="t-footer-resources__manufacturing-guide"><?= _t('front.site', 'Manufacturing Guide') ?></a>
                        </li>
                        <li><a href="/machines/3d-printers"
                               class="t-footer-resources__machines"><?= _t('front.site', '3D Printers Guide') ?></a>
                        </li>
                        <li><a href="/materials"
                               class="t-footer-resources__materials-guide"><?= _t('front.site', 'Materials Guide') ?></a>
                        </li>
                        <li><a href="/apps" class="t-footer-resources__apps"><?= _t('front.site', 'Apps') ?></a></li>
                        <li>
                            <a class="t-footer__service--what-is-new" href="/about/what-is-new">
                                <?php echo _t("site", "What's New"); ?>
                            </a>
                        </li>
                        <li>
                            <a class="t-footer__service--online-3D-printing" href="/l/3d-printing-service-online-order">
                                <?php echo _t("site", "Online 3D Printing"); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3">

                <h4 class="footer__title">
                    <?php echo _t("site", "Join Treatstock"); ?>
                </h4>

                <div class="footer__menu">
                    <ul class="footer__list">
                        <li>
                            <a class="t-footer__service--start-ps" href="/mybusiness/company"
                               onclick="<?php echo is_guest() ? "TS.Visitor.loginForm('/mybusiness/company', 'signUp');return false;" : "return true;"; ?>">
                                <?= is_guest() || !frontend\models\user\UserFacade::hasPrinter($user) ? _t("site", "Offer Your Services") : _t(

                                    "site",
                                    "Edit Business"
                                ); ?>
                            </a>
                        </li>
                        <li>
                            <a href="/mybusiness/products/product/create" class="t-footer__service--sell-products"
                               onclick="<?php echo is_guest() ? "TS.Visitor.loginForm('/mybusiness/product/create', 'signUp');return false;" : "return true;" ?>">
                                <?php echo _t("site", "Sell Products"); ?>
                            </a>
                        </li>
                        <li>
                            <a href="/about/supplier-tips" class="t-footer__service--become-supp">
                                <?php echo _t("site", "How to Create a Business"); ?>
                            </a>
                        </li>
                        <li>
                            <a class="t-footer__service--api" href="/help/article/113-become-an-api-partner">
                                <?php echo _t("site", "API Partner"); ?>
                            </a>
                        </li>
                        <li>
                            <a class="t-footer__service--affliate" href="/l/affiliates">
                                <?php echo _t("site", "Become a Partner"); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3">

                <h4 class="footer__title"><?= _t('front.site', 'Follow Us') ?></h4>

                <div class="footer__social">
                    <a class="social-ico-fb footer__social-item t-footer-follow__fb"
                       href="https://www.facebook.com/treatstock/" target="_blank" title="Facebook">
                        <svg class="soc-ico" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" width="96.124px" height="96.123px"
                             viewBox="0 0 96.124 96.123">
                            <path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803   c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654   c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246   c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z"></path>
                        </svg>
                        <!-- Facebook-->
                    </a>
                    <a class="social-ico-tw footer__social-item t-footer-follow__twit"
                       href="https://twitter.com/Treatstock" target="_blank" title="Twitter">
                        <svg class="soc-ico" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 612 612">
                            <path d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411    c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513    c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101    c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104    c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194    c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485    c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z"></path>
                        </svg>
                        <!-- Twitter-->
                    </a>
                    <a class="social-ico-insta footer__social-item t-footer-follow__inst"
                       href="https://www.instagram.com/treatstock/" target="_blank" title="Instagram">
                        <svg class="soc-ico" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="169.063px"
                             height="169.063px" viewBox="0 0 169.063 169.063">
                            <path d="M122.406,0H46.654C20.929,0,0,20.93,0,46.655v75.752c0,25.726,20.929,46.655,46.654,46.655h75.752   c25.727,0,46.656-20.93,46.656-46.655V46.655C169.063,20.93,148.133,0,122.406,0z M154.063,122.407   c0,17.455-14.201,31.655-31.656,31.655H46.654C29.2,154.063,15,139.862,15,122.407V46.655C15,29.201,29.2,15,46.654,15h75.752   c17.455,0,31.656,14.201,31.656,31.655V122.407z"></path>
                            <path d="M84.531,40.97c-24.021,0-43.563,19.542-43.563,43.563c0,24.02,19.542,43.561,43.563,43.561s43.563-19.541,43.563-43.561   C128.094,60.512,108.552,40.97,84.531,40.97z M84.531,113.093c-15.749,0-28.563-12.812-28.563-28.561   c0-15.75,12.813-28.563,28.563-28.563s28.563,12.813,28.563,28.563C113.094,100.281,100.28,113.093,84.531,113.093z"></path>
                            <path d="M129.921,28.251c-2.89,0-5.729,1.17-7.77,3.22c-2.051,2.04-3.23,4.88-3.23,7.78c0,2.891,1.18,5.73,3.23,7.78   c2.04,2.04,4.88,3.22,7.77,3.22c2.9,0,5.73-1.18,7.78-3.22c2.05-2.05,3.22-4.89,3.22-7.78c0-2.9-1.17-5.74-3.22-7.78   C135.661,29.421,132.821,28.251,129.921,28.251z"></path>
                        </svg>
                        <!-- Instagram-->
                    </a>

                    <a class="social-ico-yt footer__social-item t-footer-follow__youtube"
                       href="https://www.youtube.com/channel/UC0arVuO5CcFt619Vj0CZ1uQ/videos"
                       target="_blank" title="YouTube">
                        <svg class="soc-ico" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1"
                             viewBox="0 0 310 310">
                            <path id="XMLID_823_"
                                  d="M297.917,64.645c-11.19-13.302-31.85-18.728-71.306-18.728H83.386c-40.359,0-61.369,5.776-72.517,19.938   C0,79.663,0,100.008,0,128.166v53.669c0,54.551,12.896,82.248,83.386,82.248h143.226c34.216,0,53.176-4.788,65.442-16.527   C304.633,235.518,310,215.863,310,181.835v-53.669C310,98.471,309.159,78.006,297.917,64.645z M199.021,162.41l-65.038,33.991   c-1.454,0.76-3.044,1.137-4.632,1.137c-1.798,0-3.592-0.484-5.181-1.446c-2.992-1.813-4.819-5.056-4.819-8.554v-67.764   c0-3.492,1.822-6.732,4.808-8.546c2.987-1.814,6.702-1.938,9.801-0.328l65.038,33.772c3.309,1.718,5.387,5.134,5.392,8.861   C204.394,157.263,202.325,160.684,199.021,162.41z"></path>
                        </svg>
                        <!-- YouTube-->
                    </a>

                    <?php /*
                    <a class="social-ico-pin footer__social-item t-footer-follow__pint"
                       href="https://pinterest.com/treatstock/" target="_blank" title="Pinterest">
                        <svg class="soc-ico" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 310.05 310.05">
                            <path id="XMLID_799_"
                                  d="M245.265,31.772C223.923,11.284,194.388,0,162.101,0c-49.32,0-79.654,20.217-96.416,37.176   c-20.658,20.9-32.504,48.651-32.504,76.139c0,34.513,14.436,61.003,38.611,70.858c1.623,0.665,3.256,1,4.857,1   c5.1,0,9.141-3.337,10.541-8.69c0.816-3.071,2.707-10.647,3.529-13.936c1.76-6.495,0.338-9.619-3.5-14.142   c-6.992-8.273-10.248-18.056-10.248-30.788c0-37.818,28.16-78.011,80.352-78.011c41.412,0,67.137,23.537,67.137,61.425   c0,23.909-5.15,46.051-14.504,62.35c-6.5,11.325-17.93,24.825-35.477,24.825c-7.588,0-14.404-3.117-18.705-8.551   c-4.063-5.137-5.402-11.773-3.768-18.689c1.846-7.814,4.363-15.965,6.799-23.845c4.443-14.392,8.643-27.985,8.643-38.83   c0-18.55-11.404-31.014-28.375-31.014c-21.568,0-38.465,21.906-38.465,49.871c0,13.715,3.645,23.973,5.295,27.912   c-2.717,11.512-18.865,79.953-21.928,92.859c-1.771,7.534-12.44,67.039,5.219,71.784c19.841,5.331,37.576-52.623,39.381-59.172   c1.463-5.326,6.582-25.465,9.719-37.845c9.578,9.226,25,15.463,40.006,15.463c28.289,0,53.73-12.73,71.637-35.843   c17.367-22.418,26.932-53.664,26.932-87.978C276.869,77.502,265.349,51.056,245.265,31.772z"></path>
                        </svg>
                        <!-- Pinterest-->
                    </a>
                    */ ?>

                    <a class="social-ico-linkedin footer__social-item t-footer-follow__linkedin"
                       href="https://www.linkedin.com/company/treatstock/" target="_blank" title="LinkedIn">
                        <svg class="soc-ico" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 430.117 430.117">
                            <path d="M430.117 261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477V420.56h-92.219s1.242-251.285 0-277.32h92.21v39.309c-.187.294-.43.611-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zM52.183 9.558C20.635 9.558 0 30.251 0 57.463c0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zM5.477 420.56h92.184V143.24H5.477v277.32z"
                                  style="fill: rgb(255, 255, 255);"></path>
                        </svg>
                        <!-- LinkedIn-->
                    </a>

                    <?php /*
                    <br>
                    <a class="social-ico-trustp footer__social-item t-footer-follow__trustpilot" href="https://www.trustpilot.com/review/treatstock.com" target="_blank" title="Trustpilot">
                        <img src="/static/images/trustpilot-rating.svg" alt="Treatstock Reviews" width="150px">
                        <!-- Trustpilot -->
                    </a>
                    */ ?>
                </div>
                <?php if (UserFacade::canTranslate()):
                    if (app('session')->get('translateMode', false)) {
                        echo Html::a('Translate Mode: On', ['site/set-translate-mode', 'action' => 'off']);
                    } else {
                        echo Html::a('Translate Mode: Off', ['site/set-translate-mode', 'action' => 'on']);
                    }
                    //    <div>Translate mode</div>
                endif; ?>

            </div>
        </div>

        <div class="row m-t20">
            <div class="col-xs-12 col-sm-5">
                <div class="footer__copyrights" itemscope itemtype="http://schema.org/Organization">
                    <span class="m-r10" itemprop="name">Treatstock &copy; <?php echo date("Y"); ?></span>
                    <div class="footer__address" itemprop="address" itemscope
                         itemtype="http://schema.org/PostalAddress">
                        <span itemprop="streetAddress">40 East Main Street Suite 900</span>,&nbsp;
                        <span itemprop="addressLocality">Newark</span>,&nbsp;
                        <span itemprop="addressRegion">DE</span>,&nbsp;
                        <span itemprop="postalCode">19711</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7">
                <div class="footer__bottom-links">
                    <a href="/sitemap" class="t-footer-menu__sitemap"><?= _t('front.site', 'Sitemap') ?></a> /
                    <a href="/site/policy" class="t-footer-menu__policy"><?= _t('front.site', 'Privacy Policy') ?></a> /
                    <a href="/site/terms" class="t-footer-menu__terms"><?= _t('front.site', 'Terms of Use') ?></a> /
                    <a href="/site/return-policy"
                       class="t-footer-menu__return"><?= _t('front.site', 'Return Policy') ?></a>
                </div>
            </div>
        </div>
        <div class="row m-t20">
            <div class="col-xs-12 col-sm-5">
                <?= GoogleRecaptchaV3Widget::widget() ?>
            </div>
        </div>
    </div>
</footer>

<?php if (empty($_COOKIE['accepted_cookie'])): ?>
    <div class="cookie-modal">
        <div class="container clearfix">
            <div class="cookie-modal__text">
                <?php
                $policyLink = yii\helpers\Html::a("Privacy Policy", ['/site/policy']);
                echo _t('front.site', 'Our site uses cookies. By continuing to use our site you are agreeing to our {policy}', ['policy' => $policyLink]);
                ?>
            </div>
            <button type="button" onclick="return TS.acceptCookie();" class="btn btn-primary cookie-modal__btn">
                <?= _t('front.site', 'Accept'); ?>
            </button>
        </div>
    </div>
<?php endif; ?>

<?php
$this->registerJs("jQuery('#loginModal').modal({'show':false});");
echo ' <div id="loginModal" class="fade modal" role="dialog" tabindex="-1">'; //loginAjax-modal-dialog
echo $this->renderFile(
    '@app/views/user/loginAjax.php',
    [
        'model'       => new LoginForm(),
        'modelSignup' => new SignupForm()
    ]
);
echo '</div>';
?>

<?php $this->endBody() ?>

<?php if (!UserFacade::isSecuredPage()): ?>
    <script src="https://use.typekit.net/anh4ohv.js"></script>
    <script>try {
            Typekit.load({async: true});
        } catch (e) {
        }</script>
<?php endif; ?>

<script>
    <?php echo $this->registerJs(
        "
        

        //Activate header search block on mobile view
        function activateMobileSearchBlock() {
            var searchBlock = $(\".adaptive-search\"),
                searchInput = $(\"#explore-input-adaptive\"),
                searchBtn = $(\"#adaptive-search__btn\");

            $(searchBtn).on(\"click\", function(e){                
                if( (searchBlock).hasClass('is-shown') ) {
                    searchBlock.submit();
                    console.log('Has class');
                } else {
                    e.preventDefault();
                    searchBlock.addClass(\"is-shown\");
                    searchInput.focus();
                    console.log('Has not class');
                }
            });
            
            $(searchInput).on(\"blur\", function(e){
                if(!$(e.relatedTarget).is(searchBtn)){
                   searchBlock.removeClass(\"is-shown\");
                }       
            });
        }

        activateMobileSearchBlock();
        
        //Activate header search block on mobile view
        function openHeaderFindPS() {
            var fakeBlock = $(\"#header-bar__fake-findps\"),
                findPS = $(\".header-bar__findps\"),
                closeFindPSBtn = $(\".header-bar__findps-close\");

            $(fakeBlock).click(function(e)
            {
                e.preventDefault();
                $(findPS).toggleClass('is-open');
            });
            
            $(closeFindPSBtn).click(function(e)
            {
                e.preventDefault();
                $(findPS).toggleClass('is-open');
            });
        }

        openHeaderFindPS();

        /* Activate dropdown onhover on desktop */
        function onhoverDropdown() {
            var isBig = window.matchMedia(\"screen and (min-width: 1025px)\");

            if (isBig.matches) {
                $('.dropdown-onhover').hover(function() {
                    $(this).addClass('open');
                    $('.dropdown-onhover .dropdown-toggle').click(function() {
                        return false;
                    });
                },
                function() {
                    $(this).removeClass('open');
                });
            }
        }
        onhoverDropdown();
        function openSlidemenu() {
            $('.slidemenu__btn').click(function()
            {
                $('body').toggleClass('slidemenu--is-open');
                $('body').removeClass('howto-popup-open');
            });

            $('.slidemenu__menu-closer').click(function()
            {
                $('body').removeClass('slidemenu--is-open');
                $('body').removeClass('howto-popup-open');
            });
        }
        openSlidemenu();
        
        ( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling;
                var labelVal = label ?  label.innerHTML : undefined;
        
                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\\' ).pop();
        
                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });
        
                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));
        
        $(document).on('click', '.dropdown-menu', function(e) {
            if ($(this).hasClass('keep-open')) { e.stopPropagation(); }
        });
        
        $('[data-toggle=\"popover\"]').popover();
        
        try{
            $('[data-toggle=\"tooltip\"]').tooltip();

            //Close tooltip on iPad
            function closeTooltip() {
                var isIPad = window.matchMedia(\"only screen and (min-width: 768px) and (max-width: 1024px)\");
                if (isIPad.matches) {
                    if ('ontouchstart' in document.documentElement) {
                        $('body').css('cursor', 'pointer');
                    }
                }
            }

            closeTooltip();
            $(window).resize(function () {closeTooltip()});
            }catch(e){}
        if(!navigator.cookieEnabled){
            new TS.Notify({ type : 'error', text: '" . str_replace(["\n", "\r"], '', _t('site.error', 'Please enable cookies to work with this site')) . "',  automaticClose: false});
        }
        "
    ); ?>

    <?php
    $userData = is_guest()
        ? ['isGuest' => true]
        : ['isGuest' => false, 'userId' => $userModel->id, 'username' => UserFacade::getFormattedUserName($userModel)];
    ?>
    _.extend(TS.User, <?= Json::encode($userData)?>);
    <?php if (app('request')->get('showlogin', false)) {
        echo $this->registerJs("jQuery('#loginModal').modal({'show':true});$('#loginModal .nav-tabs a[href=\"#user-loginAjax-signup\"]').tab('show');");
    } ?>
</script>
<?php
if ($spaceless) {
    yii\widgets\Spaceless::end();
} ?>
</body>
</html>
<?php $this->endPage() ?>
