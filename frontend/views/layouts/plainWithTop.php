<?php
use frontend\assets\AppAsset;
use frontend\models\user\UserFacade;
use frontend\widgets\GoogleAnalyticsWidget;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;

AppAsset::register($this);
Yii::$app->angular->registerScripts($this);
BootstrapPluginAsset::register($this);
$user = UserFacade::getCurrentUser();
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= \H($this->title) ?></title>
    <?php $this->head() ?>
    <?=GoogleAnalyticsWidget::widget();?>

    <style type="text/css">
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
    </style>

</head>
<body ng-app="app" class="body-plain <?= (array_key_exists(
        'additionalBodyClass',
        Yii::$app->params
    ) && Yii::$app->params['additionalBodyClass']) ? Yii::$app->params['additionalBodyClass'] : '' ?>">
<?php $this->beginBody() ?>

<div>
    <div id="header-bar" class="header-bar">
        <div id="header-bar__collapse" class="slidemenu__menu navbar-collapse">
            <div class="slidemenu__menu-closer"></div>
            <?php
            if(true) {
                #$userTopMenu = \frontend\models\user\UserFacade::getTopMenuLinks($user);
                $businessMenu = \frontend\models\user\UserFacade::getTopBusinessMenuLinks($user);
                $workbenchMenu = \frontend\models\user\UserFacade::getTopWorkbenchLinks($user);
                $topNotifyCount = \frontend\models\user\UserFacade::getTopNotify($user);
                $displayUserName = H(frontend\models\user\UserFacade::getFormattedUserName($user));

                if (empty($this->params['hidenav'])):
                    ?>
                    <a class="header-logo" href="/" title="Treatstock 3D Printing Services" style="position:absolute;">Treatstock</a>
                    <ul class="header-bar__menu">

                        <li type="button" class="navbar-toggle slidemenu__btn"> <!--  data-toggle="collapse" data-target="#header-bar__collapse" -->
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </li>

                        <?php if(!$user || !$user->ps): ?>
                            <li class="header-bar__become hidden-xs">
                                <a href="/l/become-supplier" class="btn btn-link t-header-bar__become">
                                    <?= _t('site.main', 'Create a Business'); ?>
                                </a>
                            </li>
                        <?php endif; ?>


                        <li class="dropdown dropdown-onhover header-bar__auth header-bar__auth--in">
                            <a class="btn btn-default header-bar__menu-btn dropdown-toggle dropdown-menu-right-toggle t-header-bar__menu-btn--profile-btn"
                               href="/user-profile" data-toggle="dropdown">
                                <div class="header-bar__avatar">
                                    <?php if ($topNotifyCount): ?>
                                        <div class="header-bar__notification-label"></div>
                                    <?php endif; ?>
                                    <?php echo frontend\components\UserUtils::getAvatarForUser($user, 'top-profile'); ?>
                                </div>

                                <span class="header-bar__username">
                                        <?php echo $displayUserName; ?>
                                </span>
                                <span class="tsi tsi-down"></span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li class="text-divider"><?=_t('site.menu', 'Dashboard');?></li>
                                <?php foreach ($workbenchMenu as $k => $v): ?>
                                    <li><a href="<?php echo yii\helpers\Url::toRoute($v['link']); ?>" tabindex="-1"><span
                                                    class="tsi <?php echo $v['icon']; ?>"></span><?php echo $v['title']; ?></a></li>
                                <?php endforeach; ?>
                                <li class="text-divider"><?=_t('site.menu', 'My Business');?></li>
                                <?php if(!$user || !$user->ps): ?>
                                    <li>
                                        <a class="t-header-bar__store-service--start-ps" href="/mybusiness/company" onclick="<?= is_guest()?"TS.Visitor.signUpForm('/mybusiness/company'); return false;":"";?>">
                                            <span class="tsi tsi-printer3d"></span>
                                            <?= _t("site.main","Create a Business"); ?>
                                        </a>
                                    </li>
                                <?php else: ?>
                                    <?php foreach ($businessMenu as $k => $v): ?>
                                        <li><a href="<?php echo yii\helpers\Url::toRoute($v['link']); ?>" tabindex="-1"><span
                                                        class="tsi <?php echo $v['icon']; ?>"></span><?php echo $v['title']; ?></a></li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <li class="divider"></li>
                                <li><a class="t-header-bar__menu-btn--signout" href="/profile" tabindex="-1">
                                        <span class="tsi tsi-user"></span><?php echo _t("site", "Profile"); ?></a>

                                </li>
                                <li class="divider"></li>
                                <li><a class="t-header-bar__menu-btn--signout" href="/user/logout" data-method="post" tabindex="-1">
                                        <span class="tsi tsi-sign-out"></span><?php echo _t("site", "Sign Out"); ?></a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                <?php
                endif; // hidenav
            }
            // NavBar::end();
            ?>
        </div>
    </div>
</div>
<div id="notify-message"></div>
<?= \dmstr\widgets\Alert::widget(['options' => ['class' => 'alertpanel']]) ?>
<?= $content ?>

<?php $this->endBody() ?>
</body>
<footer class="footer hidden">
</footer>
<script>
    <?php
    $userData = is_guest()
        ? ['isGuest' => true]
        : ['isGuest' => false, 'userId' => UserFacade::getCurrentUser()->id,
           'username' => UserFacade::getFormattedUserName(UserFacade::getCurrentUser())];
    ?>
    _.extend(TS.User, <?= \yii\helpers\Json::encode($userData)?>);
</script>
</html>
<?php $this->endPage() ?>

