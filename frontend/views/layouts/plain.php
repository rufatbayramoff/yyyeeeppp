<?php
use frontend\assets\AppAsset;
use frontend\models\user\UserFacade;
use frontend\widgets\GoogleAnalyticsWidget;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;

AppAsset::register($this);
Yii::$app->angular->registerScripts($this);
BootstrapPluginAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="<?= H($this->title) ?>"/>
    <meta name="robots" content="noindex, nofollow" />
    <?= Html::csrfMetaTags() ?>
    <title><?= \H($this->title) ?></title>
    <?php $this->head() ?>
    <?=GoogleAnalyticsWidget::widget(['widgetMode'=> $this->isWidgetMode()]);?>

    <style type="text/css">
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
    </style>

</head>
<body ng-app="app" class="body-plain <?= (array_key_exists(
        'additionalBodyClass',
        Yii::$app->params
    ) && Yii::$app->params['additionalBodyClass']) ? Yii::$app->params['additionalBodyClass'] : '' ?>">
<?php $this->beginBody() ?>
<div id="notify-message" class="no-print"></div>
<?= \dmstr\widgets\Alert::widget(['options' => ['class' => 'alertpanel']]) ?>
<?= $content ?>

<?php $this->endBody() ?>
</body>
<footer class="footer hidden">
</footer>
<script>
    <?php
    $userData = is_guest()
        ? ['isGuest' => true]
        : ['isGuest' => false, 'userId' => UserFacade::getCurrentUser()->id,
                               'username' => UserFacade::getFormattedUserName(UserFacade::getCurrentUser())];
    ?>
    _.extend(TS.User, <?= \yii\helpers\Json::encode($userData)?>);
</script>
</html>
<?php $this->endPage() ?>

