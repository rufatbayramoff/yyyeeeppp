<?php
use yii\helpers\Html;

?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= \H($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body ng-app="app" class="body-plain">
<?php $this->beginBody() ?>
<header>
header
</header>
<article>
<?= $content ?>
</article>

<footer class="footer ">footer
</footer>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>

