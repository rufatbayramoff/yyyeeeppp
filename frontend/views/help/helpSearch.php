<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

$this->params['hidesearch'] = true;
$this->title = _t('front.site', 'Help Center');
?>

<div class="site-help">
    <div class="border-b m-b10">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div class="site-help__title">
                        <?= _t('front.site', 'Help Center') ?>
                    </div>
                </div>
                <div class="col-sm-8 col-md-9 wide-padding--left">
                    <div class="site-help__search">
                        <form method="get" action="/help/search/">
                            <div class="input-group">
                                <input type="text" name="q" value="<?php echo H($q);?>" class="form-control" placeholder="<?= _t('site.help', 'Search for help...'); ?>">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><?= _t('site.help', 'Search'); ?></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3 sidebar">
                <a class="btn btn-default btn-block help-nav-back" href="/help">
                    <span class="tsi tsi-arrow-left-l"></span>
                    <?php echo _t('site.help', 'Back to help center'); ?>
                </a>
            </div>

            <div class="col-sm-8 col-md-9 wide-padding--left">
                <h1>Search results</h1>
                <ol>
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => function($helpItem){
                        if($helpItem instanceof \common\models\SiteHelp){
                            $link = Url::toRoute(['help/article', 'id'=>$helpItem->id, 'alias'=>$helpItem->alias], true);
                            $shortInfo = \yii\helpers\StringHelper::truncate(strip_tags($helpItem->content), 150);
                            $result = sprintf('<li><h3>%s</h3><p class="text-muted">%s</p>
                                <p class="text-success">Link: %s</li>', Html::a(H($helpItem->title), $link), $shortInfo, Html::a($link, $link, ['class'=>'text-success']));
                        }else if($helpItem instanceof \common\models\SiteHelpCategory){
                            $link = Url::toRoute(['help/category', 'id'=>$helpItem->id], true);
                            $shortInfo = \yii\helpers\StringHelper::truncate(strip_tags($helpItem->info), 150);
                            $result = sprintf('<li><h3>%s</h3><p class="text-muted">%s</p>
                                <p class="text-success">Link: %s</li>', Html::a(H($helpItem->title), $link), $shortInfo, Html::a($link, $link, ['class'=>'text-success']));

                        }
                        return $result;
                    },
                ]); ?>
                </ol>

            </div>
        </div>
    </div>
</div>