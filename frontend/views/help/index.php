<?php

use common\models\SiteHelpCategory;
use yii\helpers\Url;

/** @var $currentArticle \common\models\SiteHelp */
$this->params['hidesearch'] = true;

$this->title = _t('front.site', 'Help Center');
/** @var SiteHelpCategory $topCategory */
$topCategory       = SiteHelpCategory::tryFind(\Yii::$app->setting->get('settings.site_help_category_id'));
$helpCategories    = $topCategory->getSiteHelpCategories()->orderByPriority()->all();
$currentCategoryId = isset($currentCategory) ? $currentCategory->id : false;
$currentArticleId  = isset($currentArticle) ? $currentArticle->id : false;

$this->params['meta_keywords']    = '';
$this->params['meta_description'] = $this->title;
if ($currentArticleId) {
    $this->title = $currentArticle->title . ' ' . $this->title;
    $listTopics  = $currentArticle->getRelatedHelps()->all();
    $currentArticle->updateViews();
    $this->params['meta_description'] = \yii\helpers\StringHelper::truncateWords(strip_tags($currentArticle->content), 21);
    $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::toRoute(['help/article', 'id' => $currentArticle->id, 'alias' => $currentArticle->getSlug()], true)]);
} else {
    if ($currentCategoryId) {
        $this->params['meta_description'] = \yii\helpers\StringHelper::truncateWords(strip_tags($currentCategory->info), 21);
        $this->title                      = $currentCategory->title . ' ' . $this->title;
        $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::toRoute(['help/category', 'id' => $currentCategory->id, 'slug' => $currentCategory->getSlug()], true)]);
    }
    $listTopics = $currentCategoryId && !$currentArticleId ? $currentCategory->siteHelps : [];
}
$this->registerMetaTag([
    'property' => 'fb:app_id',
    'content'  => param('facebook_app_id')
]);

$ogTags = [
    'og:url'         => param('server') . app('request')->url,
    'og:title'       => $this->title,
    'og:description' => \H(strip_tags($this->params['meta_description'])),
    'og:type'        => 'website'
];

$this->params['ogtags'] = $ogTags;
$this->params['script'] = [
    'type'    => 'application/ld+json',
    'content' => [
        '@context'   => 'https://schema.org',
        '@type'      => 'FAQPage',
        'mainEntity' => [
            [
                '@type'          => 'Question',
                'name'           => 'What is the return policy?',
                'acceptedAnswer' => [
                    '@type' => 'Answer',
                    'text'  => '<p>Most unopened items in new condition and returned within <strong>90 days</strong> will receive a refund or exchange. Some items have a modified return policy noted on the receipt or packing slip. Items that are opened or damaged or do not have a receipt may be denied a refund or exchange. Items purchased online or in-store may be returned to any store.</p><p>Online purchases may be returned via a major parcel carrier. <a href=http://example.com/returns> Click here </a> to initiate a return.</p>'
                ]
            ]
        ]
    ]
];

$this->registerJs("var facebook_app_id='" . param('facebook_app_id') . "'", \yii\web\View::POS_HEAD);
?>

    <div class="site-help">

        <div class="border-b m-b10">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-3">
                        <div class="site-help__title">
                            <?= _t('front.site', 'Help Center') ?>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-9 wide-padding--left">
                        <div class="site-help__search">
                            <form method="get" action="/help/search/">
                                <div class="input-group">
                                    <input type="text" name="q" class="form-control" placeholder="<?= _t('site.help', 'Search for help...'); ?>">
                                    <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><?= _t('site.help', 'Search'); ?></button>
                            </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">

                <div class="col-sm-4 col-md-3 sidebar">
                    <?php if ($currentCategoryId): ?>
                        <a class="btn btn-default btn-block help-nav-back" href="/help">
                            <span class="tsi tsi-arrow-left-l"></span>
                            <?php echo _t('site.help', 'Back to help center'); ?>
                        </a>
                    <?php endif; ?>

                    <!-- Nav -->
                    <div class="side-nav-mobile">
                        <div class="side-nav-mobile__title" data-toggle="collapse" data-target="#navMobile">
                            <?php echo _t('site.help', 'Help Center Menu'); ?>
                            <span class="tsi tsi-down"></span>
                        </div>

                        <ul class="side-nav help-nav" id="navMobile">
                            <?php foreach ($helpCategories as $category):
                                $class = '';
                                $innerNavToggle = '';
                                $innerNavMenu = '';
                                if ($currentArticleId && $currentCategoryId == $category->id) {
                                    $class          = 'class="open"';
                                    $innerNavToggle = 'aria-expanded="true"';
                                    $innerNavMenu   = 'in';
                                } else if ($currentCategoryId == $category->id) {
                                    $class = 'class="active open"';
                                }
                                ?>
                                <li <?= $class; ?>>
                                    <a <?php /*href="<?=Url::toRoute(['help/category', 'id'=>$category->id, 'slug'=>$category->getSlug()]);?>" */
                                    ?>
                                            data-toggle="collapse"
                                            data-parent=".side-nav-mobile"
                                            href="#navMobileInner<?php echo $category->id; ?>"
                                            role="button"
                                        <?= $innerNavToggle; ?>
                                            class="collapsed">
                                        <?php echo $category->title; ?> <span class="tsi tsi-down"></span>
                                    </a>
                                    <ul class="side-nav collapse <?= $innerNavMenu; ?>" id="navMobileInner<?php echo $category->id; ?>">
                                        <?php

                                        $siteHelps = \common\models\SiteHelp::find()
                                            ->where(['is_active' => 1, 'category_id' => $category->id])
                                            ->orderBy('priority asc, id asc')->all();
                                        foreach ($siteHelps as $article): ?>
                                            <li <?= $currentArticleId == $article->id ? 'class="active"' : ''; ?>><a
                                                        href="<?= Url::toRoute(['help/article', 'id' => $article->id, 'alias' => $article->getSlug()]); ?>"><?php echo $article['title']; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php endforeach; ?>
                            <li class="divider"></li>
                            <!-- <li><a href="#">Reviews</a></li> -->
                            <li><a href="/site/contact"><?php echo _t("site.help", "Didn't find an answer"); ?></a></li>
                        </ul>

                    </div>

                </div>

                <div class="col-sm-8 col-md-9 wide-padding--left">

                    <?php if (isset($currentArticle)): ?>
                        <?php if (!empty($currentCategory)): ?>
                            <ol class="breadcrumb help-breadcrumb">

                                <li><a href="<?= Url::toRoute(['help/category',
                                        'id' => $currentCategory->id]); ?>"><?php echo $currentCategory->title; ?></a>
                                </li>

                                <li class="active"><?php echo $currentArticle->title; ?></li>
                            </ol>
                        <?php endif; ?>
                        <h1><?php echo $currentArticle->title; ?></h1>
                        <?php echo $currentArticle->content;
                        $currentArticle->updateClicks(); ?>

                        <?php if (!$isVoted): ?>

                            <div class="help-helpful">
                                <?php echo _t('site.help', 'Was this article helpful?'); ?>
                                <div class="help-helpful__btn-block">
                                    <button class="btn btn-sm btn-link ts-ajax" href="<?= Url::toRoute(['help/vote',
                                        'id' => $currentArticle->id, 'flag' => 'yes']); ?>" jsCallback="$('.help-helpful').html(result.message);">
                                        <?= _t("app", "Yes"); ?>
                                    </button> |
                                    <button class="btn btn-sm btn-link" onClick="$('.help-helpful').html($('.js-no-answers-area').html())">
                                        <?= _t("app", "No"); ?>
                                    </button>
                                </div>
                                <div class='js-no-answers-area' style='display:none;'>
                                    <div class="js-no-answers" data-url='<?= Url::toRoute(['help/vote',
                                        'id' => $currentArticle->id]); ?>'>

                                        <h3><?php echo _t("site.help", "Sorry about that. Why wasn't it helpful?"); ?></h3>
                                        <?php foreach (common\models\SiteHelpVote::getWhyNo() as $k => $answer): ?>
                                            <div class="radio">
                                                <input type="radio" name="why" id="why<?php echo $k; ?>" value="<?php echo $k; ?>">
                                                <label for="why<?php echo $k; ?>">
                                                    <?php echo $answer; ?>
                                                </label>
                                            </div>
                                        <?php endforeach; ?>
                                        <div class='text-success'>
                                            <?php echo _t('site.help', 'Please note that this information is used only to improve our FAQ content.'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (count($listTopics) > 1): ?>
                            <h2><?php echo _t("site.help", "Related topics"); ?></h2>
                        <?php endif; ?>
                    <?php elseif ($currentCategoryId): ?>

                        <h1><?php echo $currentCategory->title; ?></h1>
                        <?php echo $currentCategory->info; ?>
                        <?php if (count($listTopics) > 1): ?>
                            <h2><?php echo _t("site.help", "Frequently Asked Questions"); ?></h2>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php
                        $listTopics = common\models\SiteHelp::getPopular(5, $topCategory); ?>
                        <?= $defaultContent; ?>
                        <h2><?php echo _t("site.help", "Popular topics"); ?></h2>
                    <?php endif; ?>


                    <ul class="help-topic-list">

                        <?php

                        foreach ($listTopics as $article):
                            if ($article->id == $currentArticleId) continue;
                            if ((int)$article->is_active == 0) continue;
                            ?>
                            <li>
                                <span class="tsi tsi-doc"></span>
                                <a href="<?= Url::toRoute(['help/article', 'id' => $article->id, 'alias' => $article->getSlug()]); ?>" class='js-help-topic-<?= $article->id ?>'><?php
                                    echo $article->title; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>


                    <?php if (isset($currentArticle)): ?>

                        <hr/>
                    <?php endif; ?>
                </div>


            </div>
        </div>
    </div>
<?php
$this->registerJs("
$(document.body).on('click', '.js-no-answers input', function(){
    var ans = $(this).val();
    if(ans=='noanswer'){ window.location.href = '/site/contact'; }
    else{
        var ajaxLink = $('.js-no-answers').data('url');
        $('.help-helpful').html('<div class=\'loading\'>Loading...</div>');
        $.ajax({
            url: ajaxLink,
            data: {'answer': ans},
            type: 'POST',
            dataType: 'json', 
            success: function (result) { 
               $('.help-helpful').html(result.message);
            },
            error: function (result) { 
                new TS.Notify({
                    type: 'error',
                    text: result.responseText || 'Please try again',
                    target: '.messageBox',
                    automaticClose: false
                });
            },
        });
    }
    
});
$('.side-nav a[data-toggle=\"collapse\"]').click(function() {
  $('.side-nav .collapse.in').collapse('hide')
});
");