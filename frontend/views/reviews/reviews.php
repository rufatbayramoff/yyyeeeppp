<?php
/**
 * User: nabi
 */

use common\models\StoreOrderReview;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;
use frontend\widgets\ReviewStarsWidget;
use frontend\widgets\SwipeGalleryWidget;

/** @var $ps \common\models\Ps */
/** @var $publicServicePage \frontend\models\user\CompanyPublicPageEntity */


?>
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-9 wide-padding wide-padding--right">


<br />
                    <h2 class="ps-profile-user-review-title">
                        <?= _t('ps.profile', 'Sales Reviews') ?>
                    </h2>
                    <a name="reviews"></a>

                    <?php
                    /** @var StoreOrderReview[] $reviews */
                    $reviews = StoreOrderReview::find()
                        ->with('order.user')
                        ->isCanShow()
                        ->orderBy('id DESC')
                        ->limit(150)
                        ->all();


                    $listView = \yii\widgets\ListView::widget([
                        'dataProvider' => new \yii\data\ArrayDataProvider([
                            'allModels' => $reviews,
                            'pagination' => [
                                'pageSize' => 150,
                            ],
                        ]),
                        'itemOptions' => ['tag'=>null],
                        'itemView' => '@frontend/views/c/reviewItem.php'
                    ]);
                    ?>


                <?=$listView?>
            </div>

            <div class="col-sm-4 col-md-3 sidebar">

            </div>
        </div>
    </div>