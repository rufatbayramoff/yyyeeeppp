<?php


use common\components\ShareUrlHelper;
use common\models\Model3dReplica;
use common\models\StoreOrderReviewShare;
use common\services\Model3dService;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserUtils;
use frontend\models\user\CompanyPublicPageEntity;
use frontend\models\user\UserFacade;
use frontend\widgets\PsPrintServiceReviewStarsWidget;
use frontend\widgets\ReviewStarsWidget;
use yii\helpers\Html;
use yii\web\View;


/** @var $share StoreOrderReviewShare*/
/** @var $this View */
/** @var $publicServicePage \frontend\models\user\CompanyPublicPageEntity */

$ps = $share->review->order->currentAttemp->company;
$publicServicePage = CompanyPublicPageEntity::fill($ps);
$review = $share->review;

echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $ps->user, 'ps' => $ps, 'companyPublicPageEntity' => $publicServicePage]);

$ogTags = [
    'og:url'         => ShareUrlHelper::url($share, true),
    'og:title'       => H(_t('share', 'Review for {company}', ['company' => $ps->title])),
    'og:description' => H($share->text),
    'og:type'        => 'website'
];

if ($share->photo) {
    $ogTags['og:image'] = ShareUrlHelper::mainPhotoUrl($share, true);
}

$this->params['ogtags'] = $ogTags;

if ($orderItem = $share->review->order->getFirstReplicaItem()) {
    /** @var Model3dService $model3dService */
    $model3dService = Yii::createObject(['class' => Model3dService::class,'user'  => UserFacade::getCurrentUser()]);
    $allowOrder = $model3dService->isAvailableForCurrentUser($orderItem->storeUnit->model3d);
}
else {
    $allowOrder = false;
}



?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb m-t10">
                <li><a href="<?=$publicServicePage->psUrl;?>"><?=H($ps->title);?></a></li>
                <li><a href="<?=$publicServicePage->printServiceUrl;?>"><?=_t('site.user', 'Review');?></a></li>
            </ol>
        </div>
    </div>
    <div class="row ps-pub-profile">
        <div class="col-sm-8 wide-padding wide-padding--right">

            <h2 class="ps-profile-user-review-title">
                <?= _t('ps.profile', 'Review') ?>
                <div class="ps-profile-rating">
                    <?= PsPrintServiceReviewStarsWidget::widget(['ps' => $ps]); ?>
                </div>
            </h2>



            <?php
            $printer = '-';
            if ($review->order->currentAttemp) {
                $orderAttemp = $review->order->currentAttemp;
                $printer = $orderAttemp->machine ?$orderAttemp->machine->getTitleLabel() : $printer;
            }

            /** @var Model3dReplica|null $model3dReplica */
            $model3dReplica = $review->order->getFirstItem() && $review->order->getFirstItem()->hasModel() ? $review->order->getFirstItem()->model3dReplica : null;

            $material = $color = null;

            if ($model3dReplica && $model3dReplica->isOneTextureForKit()) {
                $material = $model3dReplica->model3dTexture->printerMaterial;
                $color = $model3dReplica->model3dTexture->printerColor;
            }
            ?>




            <div class="ps-profile-user-review">
                <div class="ps-profile-user-review__user-bar">
                    <div class="ps-profile-user-review__user-info">
                        <?php $userProfileLink = $link = UserUtils::getUserPublicProfileUrl($review->order->user); ?>
                        <a href="<?= $userProfileLink ?>" class="ps-profile-user-review__user-avatar">
                            <?= UserUtils::getAvatarForUser($review->order->user, 'store'); ?>
                        </a>
                        <a href="<?= $userProfileLink ?>" class="ps-profile-user-review__user-name">
                            <?= \H(UserFacade::getFormattedUserName($review->order->user)); ?>
                        </a>
                    </div>
                    <div class="ps-profile-user-review__user-date">
                        <?= Yii::$app->formatter->asDate($review->created_at) ?>
                    </div>
                    <div class="ps-profile-user-review__user-rate">
                        <?= ReviewStarsWidget::widget(['rating' => $review->getRating(), 'withSchema' => true]) ?>
                    </div>
                </div>

                <?php if ($share->text): ?>
                    <div class="ps-profile-user-review__text">
                        <?= nl2br(H($share->text)) ?>
                    </div>
                <?php endif; ?>


                <div class="ps-profile-user-review__data">
                    <div class="ps-profile-user-review__data-ps">
                        <table>
                            <tbody>
                            <tr>
                                <td class="ps-profile-user-review__data-label">
                                    <?= _t('ps.profile', 'Printed on') ?>:
                                </td>
                                <td><?= H($printer); ?></td>
                            </tr>
                            <tr>
                                <td class="ps-profile-user-review__data-label">
                                    <?= _t('ps.profile', 'Material') ?>:
                                </td>
                                <td>
                                    <?= $material ? $material->title : _t('site.model3d', 'Different materials'); ?>
                                    <?php if ($color) { ?>
                                        <div title="Biodegradable and flexible plastic" class="material-item">

                                            <div class="material-item__color" style="background-color: rgb(<?= $color->rgb; ?>)"></div>
                                            <div class="material-item__label"><?= $color->title ?></div>

                                        </div>
                                    <?php } else { ?>
                                        <?= _t('site.model3d', 'Different colors'); ?>
                                    <?php } ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <?php if($share->photo) : ?>

                        <div class="m-b20">
                            <?= Html::img(ShareUrlHelper::mainPhotoUrl($share), ['style' => 'max-width: 100%;border-radius: 5px'])?>
                        </div>

                    <?php endif; ?>


                    <?php if($allPhotos = $share->getAllPhotos()) : ?>

                        <div class="ps-profile-user-review__user-models swiper-container">

                            <div class="swiper-wrapper">
                                <?php foreach ($allPhotos as $image): ?>
                                    <?php
                                    if (\common\components\FileTypesHelper::isVideo($image)) {
                                        continue;
                                    }
                                    $path = $image->getLocalTmpFilePath();
                                    if (!file_exists($path)) {
                                        // For testing at local pc without files.
                                        continue;
                                    }

                                    ?>
                                    <a href="<?= ImageHtmlHelper::getThumbUrlForFile($image)?>"
                                       class="ps-profile-user-review__user-models-pic swiper-slide"
                                       data-lightbox="review_pics1">
                                        <img src="<?= ImageHtmlHelper::getThumbUrlForFile($image,
                                            120,
                                            120
                                        )?>" alt="<?= _t('ps.public', 'Review image #{id}', ['id' => $image->id]); ?>"/>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                            <div class="ps-profile-user-review__user-models-scrollbar swiper-scrollbar"></div>
                        </div>


                    <?php endif; ?>


                    <?php if($allowOrder): ?>

                        <a class="btn btn-primary" href="/my/print-model3d?model3d=M:<?=$review->order->getFirstReplicaItem()->storeUnit->model3d->id;?>"><?= _t('share', 'Buy Now')?></a>
                        <br/>
                        <br/>
                    <?php endif;?>

                </div>
            </div>
        </div>

        <div class="col-sm-4 ps-pub-profile__sidebar">
            <?= $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-inline.php', ['ps' => $ps]);?>
        </div>
    </div>
</div>

    <script>
        <?php $this->beginBlock('js1', false); ?>

        //Init slider for service reviews
        var publicFeaturedPS = new Swiper('.ps-profile-user-review__user-models', {
            scrollbar: '.ps-profile-user-review__user-models-scrollbar',
            scrollbarHide: true,
            slidesPerView: 'auto',
            grabCursor: true
        });

        <?php $this->endBlock(); ?>
    </script>

<?php $this->registerJs($this->blocks['js1']); ?>