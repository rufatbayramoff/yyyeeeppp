<div class="ie-alert">
    <div class="container">
        <div class="ie-alert__title">
            <span class="tsi tsi-warning-c"></span>
            <?= _t('front.upload', 'You are using an outdated browser. To continue working with our website, please upgrade to a modern, fully supported browser.'); ?>
        </div>

        <div class="ie-alert__browser-list">
            <a href="https://www.google.com/intl/en/chrome/browser/desktop/" rel="nofollow" target="_blank" class="ie-alert__browser-item is-chrome">
                Chrome
            </a>
            <a href="http://www.opera.com/" rel="nofollow" target="_blank" class="ie-alert__browser-item is-opera">
                Opera
            </a>
            <a href="https://www.mozilla.org/en-US/firefox/new/" rel="nofollow" target="_blank" class="ie-alert__browser-item is-firefox">
                Firefox
            </a>
            <a href="https://www.microsoft.com/en-us/windows/microsoft-edge" rel="nofollow" target="_blank" class="ie-alert__browser-item is-edge">
                Edge
            </a>
        </div>
    </div>
</div>