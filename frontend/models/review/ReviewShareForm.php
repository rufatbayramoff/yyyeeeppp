<?php
/**
 * Created by mitaichik
 */

namespace frontend\models\review;


use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\components\FileDirHelper;
use common\components\FileTypesHelper;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use common\models\StoreOrderReviewShare;
use common\models\User;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\user\UserFacade;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ReviewShareForm extends Model
{
    /**
     * @var string
     */
    public $text;

    /**
     * @var int
     */
    public $photoId;

    /**
     * @var UploadedFile
     */
    public $photo;

    /**
     * @var string
     */
    public $socialType;

    /**
     * @var StoreOrderReviewShare
     */
    private $share;

    /**
     * @var User
     */
    private $user;

    /**
     * ReviewShareForm constructor.
     * @param StoreOrderReviewShare $share
     * @param User|null $user
     */
    public function __construct(StoreOrderReviewShare $share, User $user = null)
    {
        parent::__construct([]);
        $this->share = $share;
        $this->user = $user;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['text', 'string', 'max' => 1000],
            ['text', 'required'],
            ['photoId', 'number'],
            ['photoId', 'existPhotoValidator'],
            ['photo', 'file', 'checkExtensionByMimeType' => false, 'extensions' => FileTypesHelper::IMAGES_EXTENSIONS],
            ['socialType', 'in', 'range' => StoreOrderReviewShare::SOCIAL_TYPES],
        ];
    }

    /**
     * Validate that foto selected or uploaded and check that review contains selected photo.
     *
     * @param string $attribute
     */
    public function existPhotoValidator(string $attribute) : void
    {
        if ($this->photoId) {
            $reviewFileIds = ArrayHelper::getColumn($this->share->review->reviewFilesSort, 'id');

            if (!in_array($this->photoId, $reviewFileIds)) {
                $this->addError($attribute, _t('share', 'This photo is no longer available.'));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, '')) {
            $this->photo = UploadedFile::getInstanceByName('photo');
            return true;
        }
        return false;
    }

    /**
     * Process form and save StoreOrderReviewShare.
     */
    public function process() : void
    {
        $share = $this->share;
        $share->social_type = $this->socialType;
        $share->photo_id = $this->photoId;
        $share->status = StoreOrderReviewShare::STATUS_APPROVED;

        if ($share->isUserCanEdit($this->user)) {

            $originalShareText = $share->text;

            $share->text = $this->text;

            if ($this->photo) {
                $fileFactory = Yii::createObject(FileFactory::class);
                $fileRepository =Yii::createObject(FileRepository::class);

                $file = $fileFactory->createFileFromUploadedFile($this->photo);
                $file->setOwner(StoreOrderReviewShare::class, 'photo_id');
                $file->is_public = true;
                $fileRepository->save($file);

                $share->photo_id = $file->id;
            }

            if ($share->text != $originalShareText || $this->photo) {
                $share->status = StoreOrderReviewShare::STATUS_NEW;
            }
        }

        AssertHelper::assertSave($share);

        // create watermarked image

        $photo = $share->getMainPhoto();

        if($photo) {
            $imagine = new Imagine();
            $outSize = new Box(ImageHtmlHelper::DEFAULT_WIDTH, ImageHtmlHelper::DEFAULT_HEIGHT);

            $watermark = $imagine->open(\Yii::getAlias('@frontend/web/static/ts-logo-watermark.png'));
            $image = $imagine->open($photo->getLocalTmpFilePath())->thumbnail($outSize);

            $wSize = $watermark->getSize();
            $size = $image->getSize();

            $bottomRight = new Point($size->getWidth() - $wSize->getWidth() - 20, $size->getHeight() - $wSize->getHeight() - 20);
            $image->paste($watermark, $bottomRight);

            $shareFolder = UserFacade::getUserFolderPath($share->review->ps->user_id) . DIRECTORY_SEPARATOR . 'share';
            FileDirHelper::createDir($shareFolder);
            $image->save($shareFolder.DIRECTORY_SEPARATOR.'share_'.$share->id.'.jpg');
        }
    }
}


