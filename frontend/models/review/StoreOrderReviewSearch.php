<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 27.08.18
 * Time: 9:51
 */

namespace frontend\models\review;

use common\models\base\StoreOrderAttemp;
use common\models\Company;
use common\models\CompanyService;
use common\models\repositories\ReviewRepository;
use common\models\StoreOrder;
use common\models\StoreOrderReview;
use yii\data\ActiveDataProvider;

/**
 * Class StoreOrderReviewSearch
 * @package frontend\models\review
 *
 * @property Company $forPs
 * @property integer $service_id
 */
class StoreOrderReviewSearch extends StoreOrderReview
{
    public $forPs;

    public $service_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_id'], 'integer']
        ];
    }

    /**
     * @return string
     */
    public function formName(): string
    {
        return '';
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $reviews = StoreOrderReview::find()
            ->with('order.user')
            ->from(['sor' => StoreOrderReview::tableName()])
            ->where([
                'sor.ps_id'  => $this->forPs->id,
                'sor.status' => StoreOrderReview::STATUS_MODERATED
            ])
            ->orderBy(['sor.id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $reviews,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->service_id) {
            $reviews
                ->innerJoin(['so' => StoreOrder::tableName()], 'so.id = sor.order_id')
                ->innerJoin(['soa' => StoreOrderAttemp::tableName()], 'so.current_attemp_id = soa.id')
                ->innerJoin(['cs' => CompanyService::tableName()], 'cs.id = soa.machine_id');

            $reviews->andWhere([
                'cs.id' => $this->service_id
            ]);
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getReviewPrinterListForPs(): array
    {
        return ReviewRepository::getReviewPrinterListForPs($this->forPs);
    }
}