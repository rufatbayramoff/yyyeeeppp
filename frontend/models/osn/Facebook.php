<?php
namespace frontend\models\osn;

use common\components\FileDirHelper;
use common\models\OsnVendor;
use yii\base\UserException;

/**
 * facebook
 * Array ( [id] => 1421968444796363
 * [email] => tsdev015@gmail.com
 * [first_name] => Tsdev
 * [gender] => male
 * [last_name] => Work
 * [link] => https://www.facebook.com/app_scoped_user_id/1421968444796363/
 * [locale] => en_US
 * [name] => Tsdev Work
 * [timezone] => 3
 * [updated_time] => 2015-06-19T10:05:36+0000
 * [verified] => 1 )
 */

/**
 * Facebook
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Facebook extends OsnVendor implements OsnInterface
{

    protected $_code = 'facebook';

    /**
     *
     * @param \yii\authclient\ClientInterface $client
     * @return array
     * @throws UserException
     */
    public function prepareUserData(\yii\authclient\ClientInterface $client)
    {
        $userAttributes = $client->getUserAttributes();

        if (!isset($userAttributes['email'])) {
            throw new UserException(
                _t(
                    'site.osn',
                    'We are unable to complete the registration using your Facebook account because you registered with Facebook using your phone number, but Treatstock requires an email address. Please register directly through the site.'
                )
            );
        }

        \Yii::info(var_export($userAttributes, true), 'osn');
        $result = [
            'profile' => []
        ];
        $result['email'] = $userAttributes['email'];
        $result['username'] = $this->formatUsername($userAttributes['name']);
        // osn
        $result['osn']['osn_code'] = $this->_code;
        $result['osn']['uid'] = $userAttributes['id'];
        $result['osn']['auth_response'] = json_encode($userAttributes);
        $result['osn']['status'] = 1;
        $result['osn']['username'] = $userAttributes['name'];
        $result['osn']['email'] = $result['email'];
        // https://graph.facebook.com/823877331050952/picture?type=large
        $link = 'https://www.facebook.com/app_scoped_user_id/';
        $result['osn']['link'] = isset($userAttributes['link']) ? $userAttributes['link'] : $link . $userAttributes['id'];

        // profile 
        $result['profile']['avatar_url'] = $this->formatAvatar($userAttributes['id']);

        $result['profile']['current_lang'] = app()->language; // $this->formatLanguage($userAttributes['locale']);
        $firstName = isset($userAttributes['first_name']) ? $userAttributes['first_name'] : '';
        $lastName = isset($userAttributes['last_name']) ? $userAttributes['last_name'] : '';
        $result['profile']['full_name'] = !empty($firstName) ? $firstName . ' ' . $lastName : '';
        $result['profile']['gender'] = isset($userAttributes['gender']) ? $userAttributes['gender'] : 'none';
        // $result['profile']['timezone_id'] = $userAttributes['timezone']; // 3
        \Yii::info(var_export($result, true), 'osn');
        return $result;
    }
    
    
    /**
     * url example:
     * https://lh6.googleusercontent.com/-plmdKPKVeyg/AAAAAAAAAAI/AAAAAAAAAJE/2kUdi0brNz8/photo.jpg?sz=50
     *
     * function saves avatar to local @staticweb and returns path for db
     *
     *
     * @param string $uid
     * @return string
     */
    private function formatAvatar($uid)
    {

        $avatarDir = '/user/' . $this->_code . '_' . $uid;
        $path = \Yii::getAlias('@static') . $avatarDir;
        FileDirHelper::createDir($path);
        try {
            $json = file_get_contents("https://graph.facebook.com/" . $uid . "/picture?width=140&height=140&redirect=false");
            $code = \yii\helpers\Json::decode($json);
            $url = $code['data']['url'];

            $urlParse = parse_url($url);
            $ext = pathinfo($urlParse['path'], PATHINFO_EXTENSION);
            if (!$ext) {
                $ext = 'jpg';
            }
            $fileName = "avatar." . $ext;

            file_put_contents($path . '/' . $fileName, fopen($url, 'r'));
            return $avatarDir . '/' . $fileName;
        } catch (\Exception $e) {
            logException($e, 'osn');
        }
        return '';
    }
}
