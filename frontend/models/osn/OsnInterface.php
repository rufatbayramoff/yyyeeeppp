<?php
namespace frontend\models\osn;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
interface OsnInterface
{

    /**
     * create user based on auth client
     *
     * @param \yii\authclient\ClientInterface $client            
     */
    public function prepareUserData(\yii\authclient\ClientInterface $client);
}
