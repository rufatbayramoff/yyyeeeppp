<?php

namespace frontend\models\osn;

use common\components\FileDirHelper;
use common\models\OsnVendor;
use Yii;

/**
 * Google
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Google extends OsnVendor implements OsnInterface
{

    protected $_code = 'google';

    /**
     *
     * @param \yii\authclient\ClientInterface $client
     */
    public function prepareUserData(\yii\authclient\ClientInterface $client)
    {
        $userAttributes = $client->getUserAttributes();
        \Yii::info(var_export($userAttributes, true), 'osn');

        $result             = [
            'profile' => [],
            'osn'     => []
        ];
        $result['email']    = $userAttributes['email'];
        $result['username'] = $this->formatUsername($userAttributes['name']);

        $result['osn']['osn_code'] = $this->_code;
        $result['osn']['uid']      = $userAttributes['id'];
        $result['osn']['link']     = $userAttributes['link'] ?? '';
        $result['osn']['username'] = $userAttributes['name'];

        $result['osn']['auth_response'] = json_encode($userAttributes);
        $result['osn']['status']        = 1;
        $result['osn']['email']         = $result['email'];

        // profile
        $result['profile']['avatar_url']   = $this->formatAvatar($userAttributes['id'], $userAttributes['picture']);
        $result['profile']['current_lang'] = app()->language;
        $result['profile']['full_name']    = '';
        if (!empty($userAttributes['name']['given_name'])) {
            $result['profile']['full_name'] = $userAttributes['given_name'] . ' ' . $userAttributes['family_name'];
        }
        return $result;
    }

    /**
     * url example:
     * https://lh6.googleusercontent.com/-plmdKPKVeyg/AAAAAAAAAAI/AAAAAAAAAJE/2kUdi0brNz8/photo.jpg?sz=50
     *
     * function saves avatar to local @staticweb and returns path for db
     *
     *
     * @param type $uid
     * @param type $url
     * @return string
     */
    private function formatAvatar($uid, $url)
    {
        $avatarDir = '/user/' . $this->_code . '_' . $uid;
        $path      = Yii::getAlias('@static') . $avatarDir;
        FileDirHelper::createDir($path);
        $urlParse = parse_url($url);
        $ext      = pathinfo($urlParse['path'], PATHINFO_EXTENSION);
        if (!$ext) {
            $ext = 'jpg';
        }
        $fileName = "avatar." . $ext;
        $url      = str_replace("sz=50", "sz=150", $url);
        $opts     = [
            "ssl" => [
                "verify_peer"      => false,
                "verify_peer_name" => false,
            ]
        ];
        if (strpos($url, 'https:') !== false) {
            file_put_contents($path . "/" . $fileName, fopen($url, 'r', false, stream_context_create($opts)));
        } else {
            file_put_contents($path . "/" . $fileName, fopen($url, 'r'));
        }
        return $avatarDir . "/" . $fileName;
    }
}
