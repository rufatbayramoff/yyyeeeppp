<?php
namespace frontend\models\osn;

/**
 * OsnGateway
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class OsnGateway
{

    /**
     * formats data to general format
     * [
     * 'username'=>'',
     * 'email'=>'',
     * 'profile'=>[
     * 'avatar_url'=>'',
     * 'full_name'=>'',
     * 'timezone_id'=>'',
     * 'gender'=>'',
     * 'current_lang'=>''
     * ]
     * ]
     *
     * @param \yii\authclient\ClientInterface $client            
     * @throws \yii\web\NotFoundHttpException
     */
    public static function prepareUserData($client)
    {
        $title = $client->getTitle();
        if (! file_exists(__DIR__ . '/' . $title . '.php')) {
            throw new \yii\web\NotFoundHttpException('OSN Vendor not found ' . $title);
        }
        $obj = \Yii::createObject('\\frontend\\models\\osn\\' . $title);
        return $obj->prepareUserData($client);
    }
}
