<?php
namespace frontend\models\order;

use common\components\reject\BaseRejectForm;
use common\models\SystemReject;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class CancelOrderForm extends BaseRejectForm
{
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reasonId' => \_t('app', 'Cancel reason'),
            'reasonDescription' => \_t('app', 'Cancel description')
        ];
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::USER_CANCEL_ORDER;
    }
}
