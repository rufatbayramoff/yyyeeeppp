<?php
/**
 * Created by mitaichik
 */

namespace frontend\models\order;


use common\components\exceptions\AssertHelper;
use common\components\TextHelper;
use yii\base\Model;
use yii\web\UploadedFile;


class ReviewForm extends Model
{
    /**
     * @var int
     */
    public $rating_speed;

    /**
     * @var int
     */
    public $rating_quality;

    /**
     * @var int
     */
    public $rating_communication;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var UploadedFile[]
     */
    public $images = [];

    /**
     * @var int[]
     */
    public $printFiles = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rating_speed', 'rating_quality', 'rating_communication'], 'required'],
            [['rating_speed', 'rating_quality', 'rating_communication'], 'number', 'min' => 1, 'max' => 5],
            ['comment', 'string', 'max' => 1000],
            ['images', 'file', 'extensions' => 'png, jpg, jpeg, gif', 'maxFiles' => 10],
            ['printFiles', 'safe'],
        ];
    }

    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * @param $post
     */
    public function loadFromPost($post)
    {
        AssertHelper::assert($this->load($post));
        $this->comment = TextHelper::filterUtf8($this->comment);
        $this->images = UploadedFile::getInstances($this, 'images');
    }
}