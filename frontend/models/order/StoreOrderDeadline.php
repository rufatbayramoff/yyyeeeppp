<?php
/**
 * User: nabi
 */

namespace frontend\models\order;


use common\components\DateHelper;
use common\models\DeliveryType;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use frontend\widgets\SiteHelpWidget;
use yii\helpers\Html;

/**
 * Deadlines for order seen by customer
 *
 * @package frontend\models\order
 * @property StoreOrderAttemp $attemp
 */
class StoreOrderDeadline
{
    /**
     * @var StoreOrder
     */
    public $order;

    /**
     * @var time in seconds from epoch
     */
    private $timeout;

    /**
     * return title for deadline line
     *
     * @var string
     */
    private $title = 'Deadline';

    private function __construct()
    {
    }

    /**
     * @param StoreOrder $order
     * @return StoreOrderAttemptDeadline
     * @internal param StoreOrderAttemp $attemp
     */
    public static function create(StoreOrder $order)
    {
        $deadline        = new self();
        $deadline->order = $order;
        if (!empty($order->currentAttemp)) {
            $deadline->attemp = $order->currentAttemp;

            $deadline->initAttemptTimeout();
        }
        $deadline->initTimeout();
        if (!$deadline->timeout) {
            return null;
        }
        return $deadline;
    }

    public function getTitle()
    {
        return $this->title;
    }


    private function initTimeout()
    {
        if ($this->order->canPay()) {
            $this->timeout = strtotime($this->order->primaryPaymentInvoice->date_expire);
            $this->title   = _t('site.order', 'Payment deadline');
        } else if ($this->order->currentAttemp === null) {
            # $this->timeout = strtotime($this->order->billed_at) + (24 * 60 * 60);
            #  $this->title = _t('site.order', 'Searching for new vendor');
        }

    }

    /**
     * init timeout for given order
     */
    private function initAttemptTimeout()
    {
        switch ($this->attemp->status) {
            case StoreOrderAttemp::STATUS_NEW:
            case StoreOrderAttemp::STATUS_ACCEPTED: // 2.3
            case StoreOrderAttemp::STATUS_PRINTING: // 2.4.
            case StoreOrderAttemp::STATUS_PRINTED: // 2.5.
                $this->timeout = null;
                break;
            case StoreOrderAttemp::STATUS_READY_SEND: // 2.6.
                if ($this->attemp->dates->scheduled_to_sent_at) {
                    $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->scheduled_to_sent_at);
                    $this->title   = _t('site.ps', 'Deadline for dispatch');
                } else {
                    $this->timeout = null;
                }

                break;

            case StoreOrderAttemp::STATUS_SENT: // shipped
                $this->timeout = strtotime($this->attemp->dates->shipped_at) + (30 * 24 * 60 * 60); // 30 days
                if ($this->attemp->order->delivery_type_id === DeliveryType::INTERNATIONAL_STANDART_ID) {
                    $this->timeout = strtotime($this->attemp->dates->shipped_at) + (45 * 24 * 60 * 60); // 45 days
                }
                $help        = SiteHelpWidget::widget(
                    [
                        'alias' => 'ps.shippedTimeout'
                    ]
                );
                $this->title = _t('site.ps', 'Time for Delivery') . ' ' . $help;

                $this->timeout = null; // turn off for now
                break;

            case StoreOrderAttemp::STATUS_DELIVERED:
                $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->delivered_at) + (7 * 24 * 60 * 60); // 7 days
                if ($this->attemp->order->isThingiverseOrder()) {
                    $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->delivered_at) + (21 * 24 * 60 * 60); // 21 days
                }
                $help        = SiteHelpWidget::widget(
                    [
                        'alias' => 'ps.deliveryTimeout'
                    ]
                );
                $this->title = _t('site.ps', 'Dispute Period') . ' ' . $help;
                break;

            default:
                break;

        }
    }

    /**
     * @param string $format
     * @return false|string
     */
    public function getDate($format = 'Y-m-d H:i:s e')
    {
        $date = date($format, $this->timeout);
        return $date;
    }

    public function getTimeout()
    {
        return $this->timeout;
    }
}