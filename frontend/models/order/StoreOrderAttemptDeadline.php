<?php
/**
 * User: nabi
 */

namespace frontend\models\order;


use common\components\DateHelper;
use common\models\DeliveryType;
use common\models\StoreOrderAttemp;
use common\services\Model3dService;
use DateTime;
use DateTimeZone;
use Exception;
use frontend\widgets\SiteHelpWidget;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Deadlines for order attemp
 * dates calculated based on store_order_attemp.status
 * and based on some dates from store_order_attemp_dates table
 *
 * @package frontend\models\order
 */
class StoreOrderAttemptDeadline
{


    /**
     * time in seconds from epoch
     * @var int
     */
    private $timeout;

    /**
     * return title for deadline line
     *
     * @var string
     */
    private $title = 'Deadline';

    /**
     * Total line include title and timeout
     * @var string
     */
    private $labelString;


    private function __construct(private StoreOrderAttemp $attemp, private $limitProductionDeadline = true)
    {
    }

    /**
     * For admin
     *
     * @param StoreOrderAttemp $attemp
     * @return StoreOrderAttemptDeadline
     */
    public static function create(StoreOrderAttemp $attemp, $limitProdDeadline = true)
    {
        $deadline = new self($attemp, $limitProdDeadline);
        $deadline->initTimeout();
        if (!$deadline->timeout && !$deadline->labelString) {
            return null;
        }
        return $deadline;
    }

    public static function createCompany(StoreOrderAttemp $attemp, $limitProdDeadline = true)
    {
        $deadline = new self($attemp, $limitProdDeadline);
        $deadline->initTimeoutCompany();
        if (!$deadline->timeout && !$deadline->labelString) {
            return null;
        }
        return $deadline;
    }

    public static function createClient(StoreOrderAttemp $attemp)
    {
        $deadline = new self($attemp);
        $deadline->initTimeoutClient();
        if (!$deadline->timeout && !$deadline->labelString) {
            return null;
        }
        return $deadline;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getLabelString()
    {
        return $this->labelString;
    }

    /**
     * init timeout for given attemp
     *
     */
    public function initTimeout(): void
    {
        if (!$this->attemp->order) {
            return;
        }
        switch ($this->attemp->status) {
            case StoreOrderAttemp::STATUS_NEW:
                if ($this->attemp?->order?->getPrimaryInvoice()?->paymentBankInvoice) {
                    $this->title   = _t('site.ps', 'Payment Due Date');
                    $this->timeout = DateHelper::strtotimeUtc($this->attemp->order->created_at) + 60 * 60 * 24 * 10;
                    break;
                }

                $changePrinterHours = param('order_change_printer', 24);

                $this->timeout = DateHelper::strtotimeUtc($this->attemp->created_at) + ($changePrinterHours * 60 * 60);
                if ($this->attemp->order->billed_at) {
                    $billTimeout = DateHelper::strtotimeUtc($this->attemp->order->billed_at) + ($changePrinterHours * 60 * 60);
                    if ($billTimeout > $this->timeout) {
                        $this->timeout = $billTimeout;
                    }
                }
                $this->title = _t('site.ps', 'Deadline for accepting');
                break;

            case StoreOrderAttemp::STATUS_ACCEPTED: // 2.3
            case StoreOrderAttemp::STATUS_PRINTING: // 2.4.
                $startPrint = DateHelper::strtotimeUtc($this->attemp->dates->accepted_date); // start_print_at
                $planPrint  = DateHelper::strtotimeUtc($this->attemp->dates->plan_printed_at);
                if (!$this->attemp->order->isForPreorder()) {
                    $planPrint += DateHelper::dayToSeconds(day: 1);
                } // plan + 24 hours
                $maxDaysInSeconds = $planPrint - $startPrint;
                if ($maxDaysInSeconds > 302400 && empty($this->attemp->order->preorder_id) && $this->limitProductionDeadline) {
                    $this->timeout = $startPrint + 302400;
                } else {
                    $this->timeout = $planPrint;
                }
                $this->title = _t('site.ps', 'Deadline for printing');
                break;

            case StoreOrderAttemp::STATUS_PRINTED: // 2.5.
                $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->finish_print_at) + DateHelper::dayToSeconds(day: 1) / 2;
                $this->title   = _t('site.ps', 'Waiting for moderation');
                break;

            case StoreOrderAttemp::STATUS_READY_SEND: // 2.6.
                if ($this->attemp->dates->scheduled_to_sent_at) {
                    $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->scheduled_to_sent_at);
                } else {
                    $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->ready_to_sent_at) + DateHelper::dayToSeconds(day: 1); // created_at used because it actually updated_at
                }

                $this->title = _t('site.ps', 'Deadline for shipping');
                break;

            case StoreOrderAttemp::STATUS_SENT: // shipped
                $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->shipped_at) + DateHelper::dayToSeconds(day: 30);
                if ($this->attemp->order->delivery_type_id === DeliveryType::INTERNATIONAL_STANDART_ID) {
                    $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->shipped_at) + DateHelper::dayToSeconds(day: 45);
                }
                $help        = SiteHelpWidget::widget(
                    [
                        'alias' => 'ps.deliveryTimeout'
                    ]
                );
                $this->title = _t('site.ps', 'Deadline for delivery') . ' ' . $help;
                break;

            case StoreOrderAttemp::STATUS_DELIVERED:
                $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->delivered_at) + DateHelper::dayToSeconds(day: 7);
                $help          = SiteHelpWidget::widget(
                    [
                        'alias' => 'ps.shippedTimeout'
                    ]
                );
                $this->title   = _t('site.ps', 'Dispute period') . ' ' . $help;
                break;

            default:
                break;

        }
    }

    public function initTimeoutCompany(): void
    {
        if (!$this->attemp->order) {
            return;
        }
        switch ($this->attemp->status) {
            case StoreOrderAttemp::STATUS_NEW:
                $this->initTimeout();
                break;

            case StoreOrderAttemp::STATUS_ACCEPTED: // 2.3
            case StoreOrderAttemp::STATUS_PRINTING: // 2.4.
            case StoreOrderAttemp::STATUS_PRINTED:
            case StoreOrderAttemp::STATUS_READY_SEND: // 2.6.
                $this->labelString = _t('site.ps', 'Dispatch by ') . $this->manufactureDate();
                break;

            case StoreOrderAttemp::STATUS_SENT: // shipped
                $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->shipped_at) + DateHelper::dayToSeconds(day: 30);
                if ($this->attemp->order->delivery_type_id === DeliveryType::INTERNATIONAL_STANDART_ID) {
                    $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->shipped_at) + DateHelper::dayToSeconds(day: 45);
                }
                $help        = SiteHelpWidget::widget(
                    [
                        'alias' => 'ps.deliveryTimeout'
                    ]
                );
                $this->title = _t('site.ps', 'Deadline for delivery') . ' ' . $help;
                break;

            case StoreOrderAttemp::STATUS_DELIVERED:
                $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->delivered_at) + DateHelper::dayToSeconds(day: 7);
                $help          = SiteHelpWidget::widget(
                    [
                        'alias' => 'ps.shippedTimeout'
                    ]
                );
                $this->title   = _t('site.ps', 'Dispute period') . ' ' . $help;
                break;

            default:
                break;

        }
    }

    public function initTimeoutClient(): void
    {
        if (!$this->attemp->order) {
            return;
        }
        $invoice = $this->attemp->order->getPrimaryInvoice();

        switch ($this->attemp->status) {
            case StoreOrderAttemp::STATUS_NEW:
                if ($invoice->isPayed()) {
                    break;
                }
                if ($invoice?->paymentBankInvoice) {
                    $this->title   = _t('site.ps', 'Payment Due Date');
                    $this->timeout = DateHelper::strtotimeUtc($this->attemp->order->created_at) + 60 * 60 * 24 * 10;
                    break;
                }

                $changePrinterHours = param('order_change_printer', 24);

                $this->timeout = DateHelper::strtotimeUtc($this->attemp->created_at) + ($changePrinterHours * 60 * 60);
                if ($this->attemp->order->billed_at) {
                    $billTimeout = DateHelper::strtotimeUtc($this->attemp->order->billed_at) + ($changePrinterHours * 60 * 60);
                    if ($billTimeout > $this->timeout) {
                        $this->timeout = $billTimeout;
                    }
                }
                $this->title = _t('site.ps', 'Deadline for Accepting');
                break;

            case StoreOrderAttemp::STATUS_ACCEPTED:
            case StoreOrderAttemp::STATUS_PRINTING:
            case StoreOrderAttemp::STATUS_PRINTED:
            case StoreOrderAttemp::STATUS_READY_SEND:
            case StoreOrderAttemp::STATUS_SENT:
                $dateArriving = $this->estimateDateArriving();
                if ($dateArriving) {
                    $this->labelString = _t('site.ps', 'Estimated date of arrival') . ': ' . $dateArriving;
                }
                break;

            case StoreOrderAttemp::STATUS_DELIVERED:
                $this->timeout = DateHelper::strtotimeUtc($this->attemp->dates->delivered_at) + DateHelper::dayToSeconds(day: 7);
                $help          = SiteHelpWidget::widget(
                    [
                        'alias' => 'ps.shippedTimeout'
                    ]
                );
                $this->title   = _t('site.ps', 'Dispute Period') . ' ' . $help;
                break;

            default:
                break;

        }
    }

    /**
     * @param string $format
     * @return string
     */
    public function getDate(string $format = 'Y-m-d H:i:s e')
    {
        $date = (new DateTime('@' . $this->timeout, new DateTimeZone('UTC')))->format($format);
        return $date;
    }

    public function getTimeout()
    {
        return $this->timeout;
    }

    public function getHours()
    {
        return abs(round((time() - $this->timeout) / 60 / 60));
    }

    /**
     * Dates only manufacture and with delivery
     * @return array
     * @throws InvalidConfigException
     */
    protected function getManufactureDates(): ?array
    {
        if (!$this->attemp->order || !$this->attemp->order->firstReplicaItem) {
            return null;
        }
        $acceptedDate = $this->attemp?->dates?->accepted_date;
        if (!$acceptedDate) {
            return null;
        }
        if (!in_array($this->attemp->status, [
            StoreOrderAttemp::STATUS_NEW,
            StoreOrderAttemp::STATUS_ACCEPTED,
            StoreOrderAttemp::STATUS_PRINTING,
            StoreOrderAttemp::STATUS_PRINTED,
            StoreOrderAttemp::STATUS_READY_SEND,
            StoreOrderAttemp::STATUS_SENT,
            StoreOrderAttemp::STATUS_DELIVERED
        ])) {
            return null;
        }
        $clientProfile  = $this->attemp->order->user->userProfile;
        $companyProfile = $this->attemp->ps->user->userProfile;
        if ($companyProfile->timezone_id && $clientProfile->timezone_id) {
            $acceptedDate = DateHelper::convert($this->attemp->dates->accepted_date, $companyProfile->timezone_id, $clientProfile->timezone_id);
        }
        $acceptedDate = (new \DateTime($acceptedDate))->format('Y-m-d 23:59:00');
        $service      = Yii::createObject(Model3dService::class);
        ['printingDay' => $printingDay, 'deliveryDay' => $deliveryDay] = $service->timeAround($this->attemp->order->firstReplicaItem, $this?->attemp?->machine?->location?->country, false);

        return [DateHelper::add(
            $acceptedDate,
            'PT' . DateHelper::dayToSeconds($printingDay) . 'S'
        ), DateHelper::add(
            $acceptedDate,
            'PT' . DateHelper::dayToSeconds($printingDay + $deliveryDay) . 'S'
        )];
    }

    public function manufactureDate(): string
    {
        if ([$manufactureDate] = $this->getManufactureDates()) {
            return Yii::$app->formatter->asDate($manufactureDate, 'MMMM d, EEEE');
        }
        return '';
    }

    /**
     * @return string|null
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function estimateDateArriving(): string
    {
        if ([$manufactureDate, $dateWithDelivery] = $this->getManufactureDates()) {
            return Yii::$app->formatter->asDate($dateWithDelivery, 'MMMM d, EEEE');
        }
        return '';
    }
}