<?php
namespace frontend\models\store;

use common\models\Model3dReplica;
use common\models\PsPrinter;

/**
 * GetPrintedForm - form logic in Get printed scenario
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class GetPrintedForm extends \common\components\BaseForm
{

    /**
     * total qty
     *
     * @var int
     */
    public $qty = 1;

    /**
     * @var Model3dReplica
     */
    public $model3dReplica;

    /**
     * selected printer
     *
     * @var PsPrinter
     */
    public $printer;

    /**
     * print service can be changed for this order
     *
     * @var bool
     */
    public $canChangePs = true;

    public function rules()
    {
        $rules = [
            [
                ['canChangePs'],
                'boolean'
            ]
        ];
        return $rules;
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function getAttributeLabel($attribute)
    {
        if ($attribute == 'qty') {
            return _t('site.field', 'quantity');
        }
        if ($attribute == 'canChangePs') {
            return _t('site.store', 'I agree that chosen print shop may be changed by Treatstock if it\'s necessary to complete the order.');
        }
        return parent::getAttributeLabel($attribute);
    }
}
