<?php

namespace frontend\models\store;

use common\components\exceptions\AssertHelper;
use common\components\exceptions\DeprecatedException;
use common\components\PaymentExchangeRateFacade;
use common\interfaces\Model3dBasePartInterface;
use common\models\Company;
use common\models\Ps;
use common\models\User;
use common\models\base\GeoCountry;
use common\models\StoreOrder;
use common\models\StoreOrderItem;
use common\models\StorePricerType;
use common\models\StoreUnit;
use common\modules\product\interfaces\ProductInterface;
use frontend\components\UserSessionFacade;
use lib\money\Currency;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Store facade
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class StoreFacade
{
    /**
     * get licenses for publishing model
     *
     * @return array
     */
    public static function getLicenses()
    {
        $class = new \common\models\base\Model3dLicense();
        return \yii\helpers\ArrayHelper::map(
            $class::find()->where('is_active=1')
                ->asArray()
                ->all(),
            'id',
            'title'
        );
    }

    /**
     * Get StoreUnit data provider
     *
     * @param StoreSearchForm $filter
     * @param int $limit
     * @return \yii\data\ActiveDataProvider
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public static function getStoreUnits(StoreSearchForm $filter, $limit = 40)
    {
        $sphinxFilter = null;

        if ($filter->search || $filter->tags) {
            $query = (new \yii\sphinx\Query())->from('store')->showMeta(true);
            if ($filter->search) {
                $text = trim(implode(' ', ArrayHelper::merge([$filter->search], $filter->tags)));
                $query->match($text);
            } elseif (!$filter->search && $filter->tags) {
                $query->match(new Expression(':match', ['match' => '@(tags) ' . \Yii::$app->sphinx->escapeMatchValue(implode(' ', $filter->tags))]));
            }
            $query->limit(1000);
            $query->addOptions(['max_matches' => 1000]);
            $sphinxResult = $query->all();
            if (!$sphinxResult) {
                // No text found
                $dataProvider = new  ActiveDataProvider();
                $dataProvider->setModels([]);
                $dataProvider->setPagination(
                    [
                        'defaultPageSize' => $limit,
                        'pageSizeLimit'   => 100
                    ]
                );
                $dataProvider->setTotalCount(0);
                return $dataProvider;
            }
            $sphinxFilter = ArrayHelper::getColumn($sphinxResult, 'id', false);
        }

        $query = StoreUnit::find()
            ->joinWith('model3d')
            ->with(
                [
                    'model3d.model3dImgs',
                    'model3d.coverFile',
                    'model3d.userCollectionModel3ds',
                    'model3d.model3dParts',
                    'model3d.model3dParts.file',
                    'model3d.user',
                    'model3d.user.userProfile'
                ]
            );

        $query->andWhere(['model3d.product_status' => ProductInterface::STATUS_PUBLISHED_PUBLIC]);
        $query->andWhere(['model3d.is_active' => 1]);
        $query->joinWith('model3d.user.company');
        $query
            ->andWhere(['<>', 'user.status', User::STATUS_DELETED])
            ->andWhere(['=', 'ps.moderator_status', Ps::MSTATUS_CHECKED])
            ->andWhere(['=', 'ps.is_deleted', Company::IS_NOT_DELETED_FLAG]);

        if ($sphinxFilter) {
            $query->andWhere(['in', 'model3d.id', $sphinxFilter]);
        }

        $query->joinWith('model3d.model3dImgs');
        $query->groupBy('store_unit.id');

        if ($filter->category) {
            $query->andWhere(
                [
                    'category_id' => (int)$filter->category
                ]
            );
        }

        if ($filter->priceMin) {
            $query->andWhere(
                [
                    '>=',
                    'price_per_produce',
                    (int)$filter->priceMin
                ]
            );
        }

        if ($filter->priceMax) {
            $query->andWhere(
                [
                    '<=',
                    'price_per_produce',
                    (int)$filter->priceMax
                ]
            );
        }

        if ($filter->sort) {
            $direction = $filter->sortDirection ?: $filter::getDefaultSortDirection($filter->sort);
            $direction = ($direction === StoreSearchForm::SORT_DIRECTION_ASC) ? 'ASC' : 'DESC';

            $sortAttributes = [
                StoreSearchForm::SORT_MODEL_RATING => 'store_unit.rating',
                StoreSearchForm::SORT_MODEL_DATE   => 'product_common.created_at',
                StoreSearchForm::SORT_MODEL_PRICE  => 'ISNULL(catalog_cost.cost_usd), catalog_cost.cost_usd',

            ];

            if ($filter->sort === StoreSearchForm::SORT_MODEL_PRICE) {
                $currentLocation = \frontend\components\UserSessionFacade::getLocation();
                $currentCountry = GeoCountry::tryFind(['iso_code' => $currentLocation->country]);
                $query->leftJoin('catalog_cost', 'catalog_cost.store_unit_id = store_unit.id and catalog_cost.country_id=' . $currentCountry->id);

            }

            $sortAttribute = $sortAttributes[$filter->sort];

            $query->orderBy([new Expression('count(model3d_img.id) DESC'),new Expression('model3d.description is null')]);
            $query->addOrderBy($sortAttribute . ' ' . $direction);
        }
        $dataProvider = new ActiveDataProvider();

        $dataProvider->setPagination(
            [
                'defaultPageSize' => $limit,
                'pageSizeLimit'   => 100
            ]
        );
        $dataProvider->query = $query;

        return $dataProvider;
    }

    /**
     * get category links based on item category ids
     *
     * @param mixed $item
     * @return array - array of links to categories
     */
    public static function getCategoryLinks($item)
    {
        $result = [];
        if (empty($item['cat_ids'])) {
            return [];
        }
        $ids = explode(",", $item['cat_ids']);
        $titles = explode(",", $item['cats_title']);

        $cats = array_combine($ids, $titles);
        foreach ($cats as $k => $v) {
            $v = trim($v);
            $link = \yii\helpers\Url::toRoute(
                [
                    'store/store/index',
                    'category' => $k
                ]
            );
            $result[] = \yii\helpers\Html::a($v, $link);
        }
        return $result;
    }

    /**
     * remove unit item from store
     *
     * @param \common\models\Model3d $model3dObj
     * @return boolean
     */
    public static function removeUnit(\common\models\Model3d $model3dObj)
    {
        $dbtr = app('db')->beginTransaction();
        try {
            $model3dObj->product_status = ProductInterface::STATUS_DRAFT;
            $res = $model3dObj->safeSave();
            $model3dObj->storeUnit->safeSave();
            AssertHelper::assertSave($model3dObj);

            $dbtr->commit();
        } catch (\Exception $e) {
            logException($e, 'store');
            $res = false;
        }
        return $res;
    }

    /**
     * Check, if print price is valid for international printing
     *
     * @param int $price
     * @param string $priceCurrency
     * @param string $countryCode
     * @return boolean
     */
    public static function validIntlPrice($price, $priceCurrency, $countryCode)
    {
        $priceUsd = PaymentExchangeRateFacade::convert($price, $priceCurrency, Currency::USD);
        $location = UserSessionFacade::getLocation();

        $userCountryCode = isset($location['country']) ? $location['country'] : 'US';

        $maxPrice = app('setting')->get('store.max_intl_price', false);
        if ($maxPrice == false) {
            $maxPrice = 2500;
            app('setting')->add(
                "store.max_intl_price",
                [
                    'value' => '2500'
                ]
            );
        }
        if ($userCountryCode != $countryCode && $priceUsd > $maxPrice) {
            return false;
        }
        return true;
    }

    /**
     * get snapped model3d files based on StoreOrder
     *
     * @param StoreOrder $storeOrder
     * @return Model3dBasePartInterface[]
     */
    public static function getSnapOrderModel3dParts(StoreOrder $storeOrder)
    {
        $result = [];
        foreach ($storeOrder->storeOrderItems as $orderItem) {
            $result = array_merge($result, self::getSnapModel3dParts($orderItem));
        }
        return $result;
    }

    /**
     * get snapped model3d files for given order item
     * //TODO: Replace with direct $orederItem->model3dReplica
     *
     * @param StoreOrderItem $orderItem
     * @return Model3dBasePartInterface[]
     */
    public static function getSnapModel3dParts(StoreOrderItem $orderItem)
    {
        return $orderItem->model3dReplica->getActiveModel3dParts();
    }

    /**
     * get status label
     * todo: move to helper
     *
     * @param StoreUnit $storeUnit
     * @return string
     */
    public static function getStatusLabel(StoreUnit $storeUnit)
    {
        $model3d = $storeUnit->model3d;
        $status = $model3d->product_status;
        if ($status === ProductInterface::STATUS_REJECTED) {
            $statusLabel = 'label-danger';
        } else {
            if (!$model3d->is_active) {
                $statusLabel = 'label-warning';
            } else {
                if ($status === ProductInterface::STATUS_PUBLISHED_PUBLIC) {
                    $statusLabel = 'label-success';
                } else {
                    if ($status === ProductInterface::STATUS_DRAFT) {
                        $statusLabel = 'label-primary';
                    } else {
                        $statusLabel = 'label-default';
                    }
                }
            }
        }
        return $statusLabel;
    }

    /**
     * @param $storePricers
     * @param StoreOrder|null $storeOrder
     *
     * @return array
     * @throws DeprecatedException
     */
    public static function getPriceTypesIntl($storePricers, StoreOrder $storeOrder = null)
    {
        throw new DeprecatedException('Depricated: StoreFacade:getPriceTypesIntl');
        $hasAffiliateFee = false;
        if ($storePricers) {
            foreach ($storePricers as $pricers) :
                if (in_array($pricers->pricerType->type, [StorePricerType::AFFILIATE_FEE])) {
                    $hasAffiliateFee = true;
                }
            endforeach;
        }
        $result = [
            'model'    => _t('site.store', 'Model Price'),
            'tax'      => _t('site.store', 'Tax'),
            'fee'      => _t('site.store', 'Service'),
            'shipping' => _t('site.store', 'Shipping'),
            'ps_print' => _t('site.store', 'Manufacturing'),
            'total'    => _t('site.store', 'Total')
        ];
        if (!empty($storeOrder->preorder_id)) {
            $result['fee'] = _t('site.store', 'Manufacturing');
        }
        if ($hasAffiliateFee) {
            $result['fee'] = _t('site.store', 'Service & Transaction Fee');
        }
        return $result;
    }
}