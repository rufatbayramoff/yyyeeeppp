<?php


namespace frontend\models\store;

use common\components\exceptions\AssertHelper;
use common\models\Model3dReplica;
use common\models\Preorder;
use common\models\ProductCommon;
use common\models\PsPrinter;
use common\models\query\PreorderQuery;
use common\models\query\StoreOrderAttempQuery;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\User;
use frontend\models\user\UserFacade;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;


class MyPurchasesForm extends \common\components\BaseForm
{
    public const STATUS_QUOTES = 'quotes';

    public const STATUS_NEW             = 'new';
    public const STATUS_PRODUCTION      = 'production';
    public const STATUS_DISPATCHED      = 'dispatched';
    public const STATUS_DELIVERED       = 'delivered';
    public const STATUS_COMPLETED       = 'completed';
    public const STATUS_CANCELED        = 'canceled';
    public const STATUS_INSTANT_PAYMENT = 'instant_payment';

    const ORDER_SEARCH_MAX_LENGTH = 100;

    /**
     *
     * @var string|int
     */
    public $orderSearch;

    /**
     * @var string
     */
    public $statusGroup;

    /** @var string */
    public $quoteStatus;

    /** @var User */
    public $user;

    public $perPage = 20;

    public function rules()
    {
        return [
            ['orderSearch', 'string', 'max' => self::ORDER_SEARCH_MAX_LENGTH],
            [['statusGroup', 'quoteStatus'], 'string'],
        ];
    }

    public function load($data, $formName = null)
    {
        if (array_key_exists('orderSearch', $data)) {
            $this->orderSearch = $data['orderSearch'];
        }
        if (array_key_exists('quoteStatus', $data)) {
            $this->quoteStatus = $data['quoteStatus'];
        }
    }


    public function getOrdersQuery(): ActiveQuery
    {
        AssertHelper::assertValidate($this);

        $currentUser = $this->user;
        $query       = StoreOrder::find()
            ->forUser($currentUser)
            ->withoutRemoved()
            ->groupBy('store_order.id');

        if ($orderSearch = $this->orderSearch) {
            if (is_numeric($orderSearch)) {
                $query->andWhere('store_order.id=:orderSearch OR store_order.preorder_id=:orderSearch', [':orderSearch' => $orderSearch]);
            } else {
                $query->joinWith('currentAttemp.ps');
                $query->joinWith('orderItem.model3dReplica');
                $query->andWhere('ps.title like :orderSearch OR model3d_replica.title like :orderSearch', [':orderSearch' => '%' . $orderSearch . '%']);
            }
            return $query;
        }

        if ($this->statusGroup === self::STATUS_NEW) {
            $query
                ->inProcess()
                ->hasNewStoreOrderAttempt();
        } elseif ($this->statusGroup === self::STATUS_PRODUCTION) {
            $query
                ->inProcess()
                ->storeOrderAttemptInStatus(
                    [
                        StoreOrderAttemp::STATUS_ACCEPTED,
                        StoreOrderAttemp::STATUS_PRINTED,
                        StoreOrderAttemp::STATUS_READY_SEND,
                        StoreOrderAttemp::STATUS_PRINTING,
                    ]
                );
        } elseif ($this->statusGroup === self::STATUS_DISPATCHED) {
            $query
                ->inProcess()
                ->storeOrderAttemptInStatus(StoreOrderAttemp::STATUS_SENT);
        } elseif ($this->statusGroup === self::STATUS_DELIVERED) {
            $query
                ->inProcess()
                ->storeOrderAttemptInStatus(StoreOrderAttemp::STATUS_DELIVERED);
        } elseif ($this->statusGroup === self::STATUS_COMPLETED) {
            $query->forState(StoreOrder::STATE_COMPLETED);
        } elseif ($this->statusGroup === self::STATUS_CANCELED) {
            $query->forState([
                StoreOrder::STATE_CLOSED,
                StoreOrder::STATE_CANCELED

            ]);
        }

        return $query;
    }

    public function getPreorderQuery(): ActiveQuery
    {
        $query = Preorder::find()
            ->forCustomer($this->user)
            ->orderBy('preorder.status_updated_at desc');

        if ($orderSearch = $this->orderSearch) {
            if (is_numeric($orderSearch)) {
                $query->andWhere('preorder.id=:orderSearch', [':orderSearch' => $orderSearch]);
            } else {
                $query->joinWith('company');
                $query->andWhere(['or',
                    ['like', 'ps.title', $orderSearch]
                    ]);
            }
            return $query;
        }

        if ($this->statusGroup === self::STATUS_CANCELED) {
                        $query->inCancelProcess();
        } elseif ($this->quoteStatus === Preorder::STATUS_NEW) {
            $query->waitingForOffer();
        } elseif ($this->quoteStatus === Preorder::STATUS_WAIT_CONFIRM) {
            $query->waitComfirm();
        } else {
            $query->inNewProcess();
        }
        return $query;
    }

    public function getPurchasesDataProvider()
    {
        if ($this->statusGroup === self::STATUS_QUOTES) {
            $query = $this->getPreorderQuery();
        } elseif ($this->statusGroup === self::STATUS_CANCELED) {
            $queryPreorders = $this->getPreorderQuery();
            $queryOrders    = $this->getOrdersQuery();

            $orders                                       = $queryOrders->all();
            $preorders                                    = $queryPreorders->all();
            $allList                                      = array_merge($orders, $preorders);
            $dataProvider                                 = new ArrayDataProvider([
                'allModels' => $allList,
                'sort'      => [
                    'defaultOrder' => ['created_at' => SORT_DESC]
                ]
            ]);
            $dataProvider->sort->attributes['created_at'] = [
                'asc'  => ['created_at' => SORT_ASC],
                'desc' => ['created_at' => SORT_DESC],
            ];
            return $dataProvider;
        } else {
            $query = $this->getOrdersQuery();
        }

        $dataProvider = new ActiveDataProvider(
            [
                'query'      => $query,
                'pagination' => [
                    'pageSize' => $this->perPage,
                ],
                'sort'       => ['defaultOrder' => ['id' => SORT_DESC]]
            ]
        );

        return $dataProvider;
    }
}