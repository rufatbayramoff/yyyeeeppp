<?php

namespace frontend\models\store;

use common\models\SiteTag;
use yii\db\Query;
use yii\helpers\Html;

/**
 * From for filter models
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class StoreSearchForm extends \common\components\BaseForm
{

    const MAX_SERACH_LENGTH = 60;

    /**
     * Default sort directions for sorting
     *
     * @var string[]
     */
    public static $defaultSortDirection = [
        self::SORT_MODEL_RATING => self::SORT_DIRECTION_DESC,
        self::SORT_MODEL_PRICE  => self::SORT_DIRECTION_ASC,
        self::SORT_MODEL_DATE   => self::SORT_DIRECTION_DESC
    ];

    /**
     * Have direction for sorting
     *
     * @var bool[]
     */
    public static $isHaveDirection = [
        self::SORT_MODEL_DATE   => true,
        self::SORT_MODEL_RATING => true,
        self::SORT_MODEL_PRICE  => true,
    ];

    /**
     * Min price that we can search
     */
    const MIN_PRICE = 0;

    /**
     * Max price that we can search
     */
    const MAX_PRICE = 200000;

    /**
     * Sort constants
     */
    const SORT_BEST_MATCH = 'best-match';
    // const SORT_MODEL_PRICE = 'model-price';
    const SORT_MODEL_DATE = 'model-date';
    // const SORT_MODEL_SIZE = 'model-size';
    // const SORT_MODEL_PLYGON_COUNT = 'model-polygon-count';
    const SORT_MODEL_RATING = 'model-rating';
    const SORT_MODEL_PRICE = 'model-price';


    /**
     * Sort direction constants
     */
    const SORT_DIRECTION_ASC = 'asc';

    const SORT_DIRECTION_DESC = 'desc';

    /**
     * Search string
     *
     * @var string
     */
    public $search;

    /**
     * Category for search
     *
     * @var int
     */
    public $category;

    /**
     * Tags for search
     *
     * @var string[]
     */
    public $tags = [];

    /**
     * Min price
     *
     * @var int
     */
    public $priceMin = self::MIN_PRICE;

    /**
     * Max price
     *
     * @var int
     */
    public $priceMax;

    /**
     * Sort type
     *
     * @var string
     */
    public $sort;

    /**
     * Sort direction
     *
     * @var string
     */
    public $sortDirection;

    /**
     * select store units only with cover
     *
     * @var boolean
     */
    public $withCover;

    /**
     * Init form
     */
    public function init()
    {
        parent::init();
        $this->priceMax = $this->priceMax ?: $this->resolveMaxModelsPrice();
    }

    /**
     *
     * @return bool
     */
    public function beforeValidate()
    {
        // fix select2 bug
        if (!$this->tags) {
            $this->tags = [];
        } elseif (!is_array($this->tags)) {
            $this->tags = [];
        }

        // default sort and direction
        if (!$this->sort) {
            $this->sort = self::SORT_MODEL_DATE;

            $this->sortDirection = $this->getDefaultSortDirection($this->sort);
        }

        return parent::beforeValidate();
    }

    /**
     * Rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            [
                'tags',
                'filter',
                'filter' => function ($tags) {
                    foreach ($tags as $k => $tag) {
                        if (empty($tag)) {
                            unset($tags[$k]);
                        }
                    }

                    return $tags;
                }
            ],
            [
                'search',
                'decodeSearchString'
            ],
            [
                'search',
                'string',
                'max' => self::MAX_SERACH_LENGTH
            ],
            [
                'tags',
                'safe'
            ],
            [
                'sort',
                'in',
                'range' => array_keys($this->getSortLabels())
            ],
            [
                'sortDirection',
                'in',
                'range' => [
                    self::SORT_DIRECTION_ASC,
                    self::SORT_DIRECTION_DESC
                ]
            ],
            [
                'category',
                'integer'
            ],
            [
                [
                    'priceMin',
                    'priceMax'
                ],
                'integer'
            ]
        ];
    }

    /**
     * Filter for decode search string
     */
    public function decodeSearchString()
    {
        $this->search = Html::decode($this->search);
    }

    /**
     * Attribute labels
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'category' => _t('front.store', 'Category'),
            'priceMin' => _t('front.store', 'Price from'),
            'priceMax' => _t('front.store', 'Price to'),
            'tags'     => _t('front.store', 'Tags'),
            'sort'     => _t('front.store', 'Sort'),
            'search'   => _t('front.store', 'Search')
        ];
    }

    /**
     * Return lables for sort constants
     *
     * @return string[]
     */
    public function getSortLabels()
    {
        return [
            self::SORT_MODEL_DATE   => _t('front.store.sort', 'Newest Models'),
            self::SORT_MODEL_RATING => _t('front.store.sort', 'Most Popular'),
        ]
            // self::SORT_MODEL_SIZE => _t('front.store.sort', 'Model size'),
            // self::SORT_MODEL_PLYGON_COUNT => _t('front.store.sort', 'Model polygon count'),
            ;
    }

    /**
     * Return default direction for sorting
     *
     * @param string $sort
     *            One const of self::SORT_BEST_
     * @return array
     */
    public static function getDefaultSortDirection($sort)
    {
        return self::$defaultSortDirection[$sort];
    }

    /**
     * Return is sort have direction
     *
     * @param string $sort
     *            One const of self::SORT_BEST_
     * @return bool
     */
    public static function getIsHaveSortDirection($sort)
    {
        return self::$isHaveDirection[$sort];
    }

    /**
     * Return max price for all models
     *
     * @return int
     */
    public function resolveMaxModelsPrice()
    {
        /*
         * if(!$this->maxModelPrice)
         * {
         * $this->maxModelPrice = (int) StoreUnit::find()
         * ->select("MAX(price_per_print)")
         * ->where(['is_active' => 1])
         * ->scalar();
         * }
         *
         * return min(self::MAX_PRICE, $this->maxModelPrice);
         */
        return self::MAX_PRICE;
    }

    /**
     * Retrun 3 most popular tags for this form
     *
     * @return string[]
     */
    public function resolvePopularTags()
    {
        $query = (new Query())->select('text')
            ->from(SiteTag::tableName())
            ->where(
                [
                    'object_type' => SiteTag::OBJECT_TYPE_MODEL_3D
                ]
            )
            ->orderBy('qty_used DESC')
            ->limit(3);

        if ($this->search) {
            $query->andWhere(
                [
                    'like',
                    'text',
                    $this->search
                ]
            );
        }

        return $query->column();
    }

    /**
     * This form used as GET form
     *
     * @return string
     */
    public function formName()
    {
        return '';
    }

}
