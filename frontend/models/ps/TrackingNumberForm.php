<?php
/**
 * Created by mitaichik
 */

namespace frontend\models\ps;


use common\components\BaseForm;

/**
 * Class TrackingNumberForm
 * @package frontend\models\ps
 */
class TrackingNumberForm extends BaseForm
{

    public $withoutTracking = false;
    /**
     * @var string
     */
    public $trackingNumber;

    /**
     * @var string
     */
    public $shipper;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['shipper'], 'required'],
            [['trackingNumber'],'required','when'=>function($model) {
                return !$model->withoutTracking;
            }],
            [['trackingNumber', 'shipper'], 'string', 'min' => 1, 'max' => 1000],
            [['withoutTracking'], 'boolean']
        ];
    }

}