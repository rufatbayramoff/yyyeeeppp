<?php

namespace frontend\models\ps;

use common\components\exceptions\AssertHelper;
use common\components\FileDirHelper;
use common\components\FileTypesHelper;
use common\components\validators\DenyCountryValidator;
use common\components\validators\PhoneValidator;
use common\models\Company;
use common\models\factories\FileFactory;
use common\models\factories\UploadedFileFactory;
use common\models\factories\UserLocationFactory;
use common\models\File;
use common\models\Ps;
use common\models\repositories\FileRepository;
use common\models\User;
use common\models\UserLocation;
use common\models\UserSms;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserSessionFacade;
use frontend\models\user\CropForm;
use frontend\models\user\UserFacade;
use lib\geo\GeoNames;
use lib\money\Currency;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\imagine\Image;
use yii\web\UploadedFile;
use function in_array;

/**
 * Add Print Service Form
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class AddPsForm extends Company
{
    public const SCENARIO_DELIVERY = 'delivery';

    /**
     *
     * @var UploadedFile|null
     */
    public $logoFile;

    /**
     * @var CropForm
     */
    public $circleCrop;

    /**
     * @var CropForm
     */
    public $squareCrop;

    /**
     * Pictures
     *
     * @var array
     */
    public $pictures = [];

    /**
     * Pictures options
     *
     * @var array
     */
    public $optsPictures = [];

    /**
     * Pictures
     *
     * @var array
     */
    public $designerPictures = [];

    /**
     * Pictures
     *
     * @var UploadedFile[]
     */
    public $newPictures = [];

    /**
     * Pictures
     *
     * @var UploadedFile[]
     */
    public $newDesignerPictures = [];

    /**
     * @var array
     */
    public $videoPreviews = [];

    /**
     * @var UploadedFile
     */
    public $newCoverFile;


    /**
     * @var string - json from google api
     */
    public $locationApi;

    /**
     * Flag allow incoming quotes
     *
     * @var bool
     */
    public $allowIncomingQuotes;

    /**
     *
     * @param bool $isForDesigner
     * @return AddPsForm
     */
    public static function createForCreate($isForDesigner = false)
    {
        $form = new AddPsForm();
        $location = UserSessionFacade::getLocation();
        $country = GeoNames::getCountryByISO($location->country);
        $form->phone_country_iso = $country->iso_code;
        $form->phone_status = Ps::PHONE_STATUS_NEW;
        $form->is_designer = (int)$isForDesigner;
        $form->currency = Currency::USD;
        $form->area_of_activity = Company::MANUFACTURING_BUSINESS;
        return $form;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            ['title', 'string', 'length' => [1, 250]],
            [['website', 'facebook', 'instagram', 'incoterms', 'twitter', 'ownership', 'total_employees', 'year_established'], 'string'],
            [['website', 'facebook'], 'url', 'defaultScheme' => 'http'],
            [['country_id', 'annual_turnover'], 'integer'],
            ['country_id', DenyCountryValidator::class],
            ['phone_country_iso', DenyCountryValidator::class],
            ['title', 'isUniquePsName'],
            ['logoFile', 'file', 'extensions' => FileTypesHelper::IMAGES_EXTENSIONS, 'checkExtensionByMimeType' => false],
            ['phone', 'string', 'on' => self::SCENARIO_DELIVERY],
            ['phone', PhoneValidator::class, 'phoneCountryIsoAttribute' => 'phone_country_iso', 'phoneCodeAttribute' => 'phone_code', 'correctPhone' => true],
            ['description', 'string', 'max' => 1500],
            [['pictures', 'videoPreviews', 'designerPictures', 'locationApi', 'optsPictures'], 'safe'],
            ['newCoverFile', 'file', 'extensions' => FileTypesHelper::IMAGES_EXTENSIONS, 'checkExtensionByMimeType' => false],
            ['is_designer', 'number']
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DELIVERY] = ['title'];
        return $scenarios;
    }

    /**
     * Unique name validator
     *
     * @param $attribute
     */
    public function isUniquePsName($attribute)
    {
        $query = Ps::find()->where([
            'phone_code' => $this->phone_code,
            'title'      => $this->title
        ])->active(); // @TODO - remove - ps.is_active ?

        if (!$this->isNewRecord) {
            $query->andWhere([
                '!=',
                'ps.id',
                $this->id
            ]);
        }

        if ($query->exists()) {
            $this->addError($attribute, _t('front.user', 'PS name already exists. It must be unique.'));
        }
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title'       => _t('front.user', 'Print Service name'),
            'description' => _t('front.user', 'Print Service description'),
            'logo'        => _t('front.user', 'Logo'),
            'phone'       => _t('front.user', 'Phone'),
            'phone_code'  => _t('front.user', 'Your mobile phone number')
        ];
    }


    /**
     * Get folder path for upload PS photo
     *
     * @param
     *            $userId
     * @return string
     */
    public function getFolderPath($userId)
    {
        $userFolder = UserFacade::getUserFolder($userId);
        $finalFolderPath = Yii::getAlias('@static/') . $userFolder;
        FileDirHelper::createDir($finalFolderPath);
        return $finalFolderPath;
    }

    /**
     * Update company.
     *
     * @throws \yii\base\UserException
     */
    public function savePs(): void
    {
        $editMode = !$this->isNewRecord;
        $user = UserFacade::getCurrentUser();
        $this->user_id = $user->id;
        $this->processLogo($user);
        $this->processCover();
        $this->processPictures();
        $this->processDesignerPictures();

        if ($this->moderator_status === Company::MSTATUS_REJECTED) {
            $this->moderator_status = Company::MSTATUS_CHECKING;

        }

        if (!$editMode) {
            $this->processUrl();
        }

        if ($this->allowIncomingQuotes !== null) {
            $this->user->allow_incoming_quotes = $this->allowIncomingQuotes;
            $this->user->safeSave();
        }

        $editMode ? $this->updated_at = dbexpr("NOW()") : $this->created_at = dbexpr("NOW()");

        if ($editMode && !in_array($this->moderator_status, [Company::MSTATUS_NEW, Company::MSTATUS_DRAFT, Company::MSTATUS_BANNED])) {
            $this->moderator_status = $this->moderator_status == Ps::MSTATUS_REJECTED
                ? Ps::MSTATUS_NEW
                : Ps::MSTATUS_UPDATED;
        }
        [$this->phone_status, $this->sms_gateway] = $this->resolvePhoneStatusAndSmsGateway();
        AssertHelper::assertSave($this);
    }


    /**
     * Process logo image.
     *
     * @param User $user
     */
    private function processLogo(User $user): void
    {
        if (!$this->logoFile) {
            return;
        }

        $finalFolderPath = $this->getFolderPath($user->id);
        array_map('unlink', glob($finalFolderPath . '/ps_logo*'));
        $time = time();

        // save circle image
        $this->logo_circle = "ps_logo_circle_{$time}.{$this->logoFile->extension}";
        $image = Image::getImagine()->open($this->logoFile->tempName);
        $this->circleCrop->adapt($image->getSize());
        $image
            ->crop($this->circleCrop->getCropStartPoint(), $this->circleCrop->getCropBox())
            ->save("{$finalFolderPath}/{$this->logo_circle}");

        // save square image
        $this->logo = "ps_logo_{$time}.{$this->logoFile->extension}";
        $image = Image::getImagine()->open($this->logoFile->tempName);
        $this->squareCrop->adapt($image->getSize());
        $image
            ->crop($this->squareCrop->getCropStartPoint(), $this->squareCrop->getCropBox())
            ->save("{$finalFolderPath}/{$this->logo}");

        $this->logoFile = null;
    }

    /**
     * Process cover image.
     *
     * @throws \yii\base\InvalidConfigException
     */
    private function processCover(): void
    {
        if (!$this->newCoverFile) {
            return;
        }

        $fileFactory = Yii::createObject(FileFactory::class);

        $file = $fileFactory->createFileFromUploadedFile($this->newCoverFile);
        $file->setPublicMode(true);

        $file->setOwner(Ps::class, 'cover_file_id');

        // this method also save file
        ImageHtmlHelper::stripExifInfo($file);
        $fileRepostiry = Yii::createObject(FileRepository::class);
        $fileRepostiry->save($file);

        $this->cover_file_id = $file->id;
    }

    /**
     *
     * @throws UserException
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    private function processPictures()
    {
        $oldFiles = $this->getPicturesFiles();

        $fileIds = [];
        foreach ($this->pictures as $pos => $picture) {
            $fileIds[$pos] = (int)$picture['id'];
        }

        $fileFactory = Yii::createObject(FileFactory::class);
        $fileRepository = Yii::createObject(FileRepository::class);
        foreach ($this->newPictures as $pos => $pictureUploadedFile) {
            $file = $fileFactory->createFileFromUploadedFile($pictureUploadedFile);
            $file->setPublicMode(true);
            $fileRepository->save($file);
            $fileIds[$pos] = $file->id;
            if (!empty($this->videoPreviews[$file->name])) {
                $previewData = $this->videoPreviews[$file->name];
                $videoFileLocation = $file->getLocalTmpFilePath();
                [, $previewData] = explode(';', $previewData);
                [, $previewData] = explode(',', $previewData);
                $previewData = base64_decode($previewData);
                file_put_contents($videoFileLocation . '.png', $previewData);
            }
        }

        ksort($fileIds);
        $this->picture_file_ids = Json::encode($fileIds);

        foreach ($oldFiles as $oldFile) {
            if (!in_array($oldFile->id, $fileIds, true)) {
                $fileRepository->delete($oldFile);
            }
        }

        $this->picturesRotate($fileIds);
    }

    /**
     * @param array $rotateIds
     *
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    protected function picturesRotate(array $rotateIds)
    {
        foreach ($rotateIds as $index => $id) {
            if (isset($this->optsPictures[$index]['rotate']) && $file = File::findOne($id)) {
                if (FileTypesHelper::isImage($file)) {
                    ImageHtmlHelper::rotateImage($file, (int)$this->optsPictures[$index]['rotate']);
                }
            }
        }
    }

    /**
     *
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \InvalidArgumentException
     * @throws \yii\base\InvalidParamException
     */
    private function processDesignerPictures()
    {
        $oldFiles = $this->getDesignerPicturesFiles();

        $fileIds = [];
        foreach ($this->designerPictures as $picture) {
            $fileIds[] = (int)$picture['id'];
        }

        $fileRepository = Yii::createObject(FileRepository::class);
        $fileFactory = Yii::createObject(FileFactory::class);
        foreach ($this->newDesignerPictures as $pictureUploadedFile) {
            $file = $fileFactory->createFileFromUploadedFile($pictureUploadedFile);
            $file->is_public = 1;
            $fileRepository->save($file);
            $fileIds[] = $file->id;
            if (!empty($this->videoPreviews[$file->name])) {
                $previewData = $this->videoPreviews[$file->name];
                $videoFileLocation = $file->getLocalTmpFilePath();
                [, $previewData] = explode(';', $previewData);
                [, $previewData] = explode(',', $previewData);
                $previewData = base64_decode($previewData);
                file_put_contents($videoFileLocation . '.png', $previewData);
            }
        }

        $this->designer_picture_file_ids = Json::encode($fileIds);

        foreach ($oldFiles as $oldFile) {
            if (!in_array($oldFile->id, $fileIds)) {
                $fileRepository->delete($oldFile);
            }
        }
    }

    /**
     * Generate PS url based on title
     * If url already busy, will be added some numbers to end of url
     */
    private function processUrl()
    {
        $url = Inflector::slug($this->title);

        $i = 0;
        while (Ps::find()->where(['url' => $url])->exists()) {
            $url .= $i;
        }

        $this->url = $url;
        $this->url_changes_count = 0;
    }

    /**
     * Return phone number
     *
     * @return string
     */
    private function getFullPhone()
    {
        return $this->phone_code . $this->phone;
    }

    /**
     * Resolve phne status for PS
     *
     * @return array [phoneStatus, smsGateway]
     * @throws Exception
     */
    private function resolvePhoneStatusAndSmsGateway()
    {

        if (!$this->phone) {
            return [null, null];
        }

        $isPhoneChanged = $this->isAttributeChanged('phone') || $this->isAttributeChanged('phone_country_iso');

        // if already confirmed
        if (!$isPhoneChanged && $this->phone_status == Ps::PHONE_STATUS_CHECKED) {
            return [Ps::PHONE_STATUS_CHECKED, $this->sms_gateway];
        }

        /** @var UserSms $confirmPhoneSms */
        $confirmPhoneSms = UserSms::find()->forPhoneNumber($this->getFullPhone())
            ->andWhere([
                'user_id' => $this->user_id,
                'type'    => UserSms::TYPE_CONFIRM
            ])
            ->latest();

        if (!$confirmPhoneSms || $confirmPhoneSms->status == UserSms::STATUS_NEW) {
            return [Ps::PHONE_STATUS_NEW, null];
        }

        if ($confirmPhoneSms->status == UserSms::STATUS_ACCEPTED) {
            return [Ps::PHONE_STATUS_CHECKING, null];
        }

        if ($confirmPhoneSms->status == UserSms::STATUS_CONFIRMED) {
            return [Ps::PHONE_STATUS_CHECKED, $confirmPhoneSms->gateway];
        }

        throw new Exception("Bad sms status on resolve ps phone status: {$confirmPhoneSms->status}");
    }

    /**
     * Load data from post
     *
     * @param $data
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function loadFromPost($dataIn)
    {
        AssertHelper::assert(parent::load($dataIn, $this->formName()));
        $data = $dataIn[$this->formName()];

        $this->logoFile = UploadedFile::getInstance($this, 'logoFile');
        $this->newCoverFile = UploadedFile::getInstance($this, 'coverFile');
        $this->newPictures = UploadedFileFactory::getInstances($this, 'pictures');
        $this->newDesignerPictures = UploadedFileFactory::getInstances($this, 'designerPictures');
        if (array_key_exists('allowIncomingQuotes', $data)) {
            $this->allowIncomingQuotes = $data['allowIncomingQuotes'];
        }

        if (!empty($data['locationApi'])) {
            $this->setCompanyLocation(is_array($data['locationApi']) ? $data['locationApi'] : Json::decode($data['locationApi']));
        }

        if ($this->logoFile) {
            AssertHelper::assert(isset($data['circleCrop']), _t('site.ps', 'Need circle crop data'), UserException::class);
            $this->circleCrop = $circleCrop = new CropForm($data['circleCrop']);
            AssertHelper::assertValidate($circleCrop);

            AssertHelper::assert(isset($data['squareCrop']), _t('site.ps', 'Need square crop data'), UserException::class);
            $this->squareCrop = $squareCrop = new CropForm($data['squareCrop']);
            AssertHelper::assertValidate($squareCrop);
        }
    }

    private function setCompanyLocation($obj)
    {
        $user = UserFacade::getCurrentUser();
        $location = UserLocationFactory::createFromGoogleApiData($user, $obj);
        $location->location_type = UserLocation::LOCATION_TYPE_SHIP_FROM;
        $location->safeSave();
        $this->location_id = $location->id;
    }
}
