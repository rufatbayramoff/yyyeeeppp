<?php
/**
 * Created by mitaichik
 */

namespace frontend\models\ps;


use yii\base\Model;

class PrepareDownloadFileResponse extends Model
{
    const STATUS_WAIT = 'wait';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $error;

    /**
     * @return PrepareDownloadFileResponse
     */
    public static function createSuccess() : PrepareDownloadFileResponse
    {
        $response = new PrepareDownloadFileResponse();
        $response->status = self::STATUS_SUCCESS;
        return $response;
    }

    /**
     * @return PrepareDownloadFileResponse
     */
    public static function createWait() : PrepareDownloadFileResponse
    {
        $response = new PrepareDownloadFileResponse();
        $response->status = self::STATUS_WAIT;
        return $response;
    }

    /**
     * @param string $error
     * @return PrepareDownloadFileResponse
     */
    public static function createError(string $error) : PrepareDownloadFileResponse
    {
        $response = new PrepareDownloadFileResponse();
        $response->status = self::STATUS_ERROR;
        $response->error = $error;
        return $response;
    }
}