<?php
namespace frontend\models\ps;

use common\components\reject\BaseRejectForm;
use common\models\SystemReject;

/**
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class DeclineProductOrderForm extends BaseRejectForm
{
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reasonId' => \_t('app', 'Decline reason'),
            'reasonDescription' => \_t('app', 'Comments')
        ];
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::PRODUCT_ORDER_DECLINE;
    }
}
