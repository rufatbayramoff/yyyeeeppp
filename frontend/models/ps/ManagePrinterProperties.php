<?php
namespace frontend\models\ps;

use Yii;
use \common\models\PrinterProperties;
use \common\models\PrinterToProperty;
use \yii\helpers\ArrayHelper;

/**
 * Class for working with Printer Properties:
 * Check, Create, Update
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class ManagePrinterProperties
{

    private $printerId;

    private $now;

    /**
     * Construct method set printerId param
     *
     * @param int|null $printerId            
     */
    public function __construct($printerId = null)
    {
        $this->printerId = $printerId;
        $this->now = new \yii\db\Expression('NOW()');
    }

    /**
     * Return all printer properties
     * If printerId is not null, we set value param for each property
     *
     * @return array
     */
    public function getProperties()
    {
        $properties = PrinterProperties::find()->where([
            'is_active' => 1
        ])
            ->asArray()
            ->all();
        
        $properties = ArrayHelper::getColumn($properties, function ($element) {
            return [
                'id' => $element['id'],
                'title' => $element['title']
            ];
        });
        $properties = ArrayHelper::index($properties, 'id');
        
        if ($this->printerId) {
            $printerProperties = $this->getPropertiesFilled();
            
            if ($properties)
                foreach ($properties as $oneProperty) {
                    $value = $this->findFilledProperty($printerProperties, $oneProperty['id'], 'value');
                    $properties = $this->setPropertyValue($properties, $oneProperty['id'], 'value', $value);
                    $is_editable = $this->findFilledProperty($printerProperties, $oneProperty['id'], 'is_editable');
                    $properties = $this->setPropertyValue($properties, $oneProperty['id'], 'is_editable', $is_editable);
                }
        }
        
        return $properties;
    }

    /**
     * Return only FILLED properties
     *
     * @return PrinterToProperty
     */
    public function getPropertiesFilled()
    {
        return PrinterToProperty::find()->where([
            'printer_id' => $this->printerId,
            'is_active' => 1
        ])->all();
    }

    /**
     * Find paramName for current property
     * Use $printerProperties array, propertyId and paramName
     *
     * @param
     *            PrinterToProperty
     * @param int $propertyId            
     * @param $paramName -
     *            'value' OR 'is_editable'
     * @return string
     */
    private function findFilledProperty($printerProperties, $propertyId, $paramName)
    {
        if ($printerProperties)
            foreach ($printerProperties as $oneProperty) {
                if ($oneProperty['property_id'] == $propertyId) {
                    return $oneProperty[$paramName];
                }
            }
        
        return false;
    }

    /**
     * Set value for current property
     *
     * @param array $properties            
     * @param int $propertyId            
     * @param $paramName -
     *            paramName 'value' OR 'is_editable'
     * @param string $value            
     * @return mixed
     */
    private function setPropertyValue($properties, $propertyId, $paramName, $value)
    {
        foreach ($properties as $onePropertyKey => $onePropertyValue) {
            if ($onePropertyValue['id'] == $propertyId) {
                $properties[$onePropertyKey][$paramName] = $value;
            }
        }
        
        return $properties;
    }

    /**
     * Manage Printer properties
     * Change property if Printer already has it
     * Add property if Printer does not have this property
     * Delete property if it is empty
     *
     * @param
     *            $properties
     * @param
     *            $editable
     * @throws \Exception
     */
    public function updateProperties($properties, $editable)
    {
        $printerProperties = $this->getPropertiesFilled();
        
        if (is_array($properties))
            foreach ($properties as $propertyId => $propertyValue) {
                $filledProperty = $this->findFilledProperty($printerProperties, $propertyId, 'value');
                
                if ($filledProperty) {
                    // Printer has this property
                    // Let's check if it changes
                    if ($filledProperty != $propertyValue) {
                        // Property doesn't match
                        // Let's update property
                        $this->updatePropertyQuery($propertyId, [
                            'value' => $propertyValue
                        ]);
                    }
                } else {
                    // Printer doesn't have this property
                    // Let's add property if it is not empty
                    if ($propertyValue != '') {
                        $params = [
                            'printer_id' => $this->printerId,
                            'property_id' => $propertyId,
                            'value' => $propertyValue,
                            'is_editable' => 0,
                            'is_active' => 1
                        ];
                        $this->createNewPrinterProperty($params);
                    }
                }
            }
        
        if (is_array($printerProperties))
            foreach ($printerProperties as $oneProperty) {
                $propertyId = $oneProperty->property_id;
                // Printer property is located in base
                // Let's delete empty property
                if (! isset($properties[$propertyId]) or $properties[$propertyId] == '') {
                    $oneProperty->delete();
                }
            }
        
        if (is_array($editable)) {
            PrinterToProperty::updateAll([
                'is_editable' => 0
            ], [
                'printer_id' => $this->printerId
            ]);
            
            foreach ($editable as $propertyId => $propertyValue) {
                $this->updatePropertyQuery($propertyId, [
                    'is_editable' => 1
                ]);
            }
        }
    }

    /**
     * Update property query
     *
     * @param int $propertyId            
     * @param array $properties            
     */
    private function updatePropertyQuery($propertyId, $properties)
    {
        foreach ($properties as $propertyName => $propertyValue) {
            $updateModel = PrinterToProperty::find()->where([
                'printer_id' => $this->printerId,
                'property_id' => $propertyId
            ])->one();
            if ($updateModel) {
                $updateModel->{$propertyName} = $propertyValue;
                $updateModel->updated_at = $this->now;
                $updateModel->update();
            }
        }
    }

    /**
     * Create new Printer property
     *
     * @param array $params            
     * @return PrinterToProperty
     */
    private function createNewPrinterProperty($params)
    {
        $data = PrinterToProperty::find()->where([
            'printer_id' => $params['printer_id'],
            'property_id' => $params['property_id']
        ])->one();
        
        if (is_null($data)) {
            $model = new PrinterToProperty();
            $model->printer_id = $params['printer_id'];
            $model->property_id = $params['property_id'];
            $model->value = $params['value'];
            $model->is_editable = $params['is_editable'];
            $model->is_active = $params['is_active'];
            $model->created_at = $this->now;
            $model->updated_at = $this->now;
            $model->safeSave();
            
            return $model;
        }
    }
}
