<?php

namespace frontend\models\ps;

use common\components\order\predicates\NeedCertificateForDownloadModelPredicate;
use common\components\order\TestOrderFactory;
use common\models\DeliveryType;
use common\models\Model3dLicense;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\Ps;
use common\models\CompanyService;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderAttemptModeration;
use common\models\user\UserIdentityProvider;
use common\modules\payment\services\RefundService;
use frontend\models\user\UserFacade;
use lib\delivery\delivery\DeliveryFacade;
use Yii;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\HttpException;

/**
 * StoreOrderAttemptProcess - class for process printing requests
 *
 * @property StoreOrderAttemp $orderAttemp
 */
class StoreOrderAttemptProcess
{
    /**
     * @var StoreOrderAttemp
     */
    protected $orderAttemp;

    /**
     * @param StoreOrderAttemp $orderAttemp
     * @return StoreOrderAttemptProcess
     */
    public static function create(StoreOrderAttemp $orderAttemp)
    {
        return new self($orderAttemp);
    }

    /**
     * PrintRequest constructor.
     *
     * @param StoreOrderAttemp $orderAttemp
     */
    private function __construct(StoreOrderAttemp $orderAttemp)
    {
        $this->orderAttemp = $orderAttemp;
    }

    public function tryCheckDownloadFiles()
    {
        if ($this->orderAttemp->machine->isCutting()) {
            return;
        }
        if (!$this->canDownloadModel3D()) {
            throw new HttpException(406, _t('site.ps', 'No access to 3D model file.'));
        }

        if (!$this->canDownloadModel3dPartByLimit()) {
            throw new HttpException(406, _t('site.ps', 'You need to finish printing your previous orders before you can download and print any more models.'));
        }
    }

    /**
     * Check if user can download Model 3D
     *
     * @param
     *            $orderId
     * @return bool
     */
    public function canDownloadModel3D()
    {
        // if prinservice is owner of model
        $model3d      = $this->orderAttemp->order->getFirstItem()->unit->model3d;
        $model3dAutor = $model3d->getAuthor();
        if ($model3dAutor && $this->orderAttemp->ps->user_id == $model3dAutor->id) {
            return true;
        }

        if ($this->isOwnOrderToTestedPrinter()) {
            return true;
        }

        if ($this->orderAttemp->order->orderItem->unit->id == TestOrderFactory::getTestOrderStoreUnitId()) {
            return true;
        }

        // If attempt for current user
        $userIdentityProvider = Yii::createObject(UserIdentityProvider::class);
        $currentUser          = $userIdentityProvider->getUser();
        if ($currentUser && $this->orderAttemp->ps->user_id === $currentUser->id) {
            if ($this->orderAttemp->isResolved()) {
                return false;
            }
            return true;
        }

        if ($this->orderAttemp->is_offer) {
            // Allow to download model from directed offers
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isOwnOrderToTestedPrinter()
    {
        if ($this->orderAttemp->order->user_id != $this->orderAttemp->ps->user_id) {
            return false;
        }

        if (!$this->orderAttemp->machine) {
            return false;
        }

        if ($this->orderAttemp->machine->isCnc()) {
            return false;
        }

        if ($this->orderAttemp->machine->isCutting()) {
            return false;
        }

        if (!$this->orderAttemp->machine->asPrinter()->isCertificated()) {
            return false;
        }

        return true;
    }

    /**
     * Show button if we can accept order
     *
     * @return bool
     */
    public function canAcceptOrder()
    {
        return $this->orderAttemp->isNew() && $this->orderAttemp->isPayed();
    }

    /**
     * Show button if we can decline order
     *
     * @return bool
     */
    public function canDeclineOrder()
    {
        if (!$this->orderAttemp->order) {
            return false;
        }
        if (!$this->orderAttemp->order->isPayed()) {
            return false;
        }
        if ($this->orderAttemp->order->getIsTestOrder()) {
            return false;
        }
        if ($this->orderAttemp->isNew()) {
            return true;
        }
        if ($this->orderAttemp->status === StoreOrderAttemp::STATUS_ACCEPTED && !$this->isOwnOrderToTestedPrinter()) {
            return true;
        }
        return false;
    }

    /**
     * Show button if we can print order
     *
     * @return bool
     */
    public function canPrintOrder()
    {
        return in_array($this->orderAttemp->status, [StoreOrderAttemp::STATUS_ACCEPTED, StoreOrderAttemp::STATUS_PRINTING]);
    }

    /**
     * Show button if we can set as printed current order
     *
     * @return bool
     */
    public function canSetAsPrinted()
    {
        if (in_array($this->orderAttemp->status, StoreOrderAttemp::$companyStatusesGroups[StoreOrderAttemp::STATUS_ACCEPTED])) {
            return true;
        }
        return false;
    }

    public function canSubmitResults()
    {
        return !$this->orderAttemp->storeOrderAttemptModeration->files || $this->orderAttemp->isRejected();
    }

    public function canContactCustomer()
    {
        if (($this->orderAttemp->isDispatched() || $this->orderAttemp->isDelivered()) &&
            (!$this->orderAttemp->order->user->equals($this->orderAttemp->ps->user) && !$this->orderAttemp->isCancelled() && !$this->orderAttemp->order->isAnonim())) {
            return true;
        }
        return false;
    }

    /**
     * Show button if we can set as sent current order
     *
     * @return bool
     */
    public function canSetAsSent()
    {
        if ($this->orderAttemp->status === StoreOrderAttemp::STATUS_READY_SEND) {
            return true;
        }
        return false;
    }

    /**
     * show Order sent with tracking number required
     *
     * @return boolean
     */
    public function canSetAsSentWithTrackingNumber()
    {
        if (!$this->orderAttemp->order) {
            return false;
        }

        if ($this->orderAttemp->order->isForPreorder()) {
            return $this->canSetAsSent();
        }

        if (!$this->orderAttemp->order->hasDelivery()) {
            return false;
        }

        $deliveryTypeDetails = $this->orderAttemp->order->getOrderDeliveryDetails();
        if (!$this->orderAttemp->order->getDeliveryType()->one()->getIsPostDelivery()) { // if pickup, no need in tracking number
            return false;
        }
        if ($deliveryTypeDetails && $deliveryTypeDetails['carrier'] == DeliveryType::CARRIER_MYSELF) {
            return $this->canSetAsSent();
        }
        return false;
    }

    /**
     * Can edit the tracking number
     *
     * @return bool
     */
    public function canChangeTrackingNumber(): bool
    {
        if (!$this->orderAttemp->order->hasDelivery() && !$this->orderAttemp->delivery) {
            return false;
        }

        if (
            $this->orderAttemp->order->order_state != StoreOrder::STATE_PROCESSING ||
            !in_array($this->orderAttemp->status, [StoreOrderAttemp::STATUS_SENT, StoreOrderAttemp::STATUS_DELIVERED])
        ) {
            return false;
        }

        $deliveryTypeDetails = $this->orderAttemp->order->getOrderDeliveryDetails();

        return ($deliveryTypeDetails && \in_array($deliveryTypeDetails['carrier'], [DeliveryType::CARRIER_MYSELF, DeliveryType::CARRIER_TS], true));
    }

    /**
     * Show button if we can set as delivered current order
     *
     * @return bool
     */
    public function canSetAsDelivered()
    {
        $deliveryTypeDetails = $this->orderAttemp->order->getOrderDeliveryDetails();
        if ($deliveryTypeDetails && $deliveryTypeDetails['carrier'] == DeliveryType::CARRIER_MYSELF) {
            // return false;
        }
        if ($this->orderAttemp->status == StoreOrderAttemp::STATUS_SENT) {
            return true;
        }
        return false;
    }

    /**
     * Show button if we can get postal label for this order
     *
     * @return bool
     */
    public function canGetPostalLabel()
    {
        return $this->orderAttemp->status == StoreOrderAttemp::STATUS_READY_SEND && DeliveryFacade::isCarrierPostDelivery($this->orderAttemp->order);
    }

    /**
     * Show delivered message
     *
     * @return bool
     */
    public function canShowDeliveredMessage()
    {
        if ($this->orderAttemp->status == StoreOrderAttemp::STATUS_DELIVERED || $this->orderAttemp->status == StoreOrderAttemp::STATUS_RECEIVED) {
            return true;
        }
        return false;
    }

    /**
     *
     * @return boolean
     */
    public function canShowShippedMessage()
    {
        return $this->orderAttemp->status != StoreOrderAttemp::STATUS_RECEIVED && !empty($this->orderAttemp->dates->shipped_at);
    }

    /**
     * @return bool
     */
    public function canShowReceivedMessage()
    {
        return $this->orderAttemp->status == StoreOrderAttemp::STATUS_RECEIVED;
    }

    /**
     * Show cancelled message
     *
     * @return bool
     */
    public function canShowCancelledMessage()
    {
        if ($this->orderAttemp->status == StoreOrderAttemp::STATUS_CANCELED) {
            return true;
        }
        return false;
    }

    /**
     * Can request more time
     *
     * @return bool
     */
    public function canRequestMoreTime()
    {
        return in_array($this->orderAttemp->status, StoreOrderAttemp::$companyStatusesGroups[StoreOrderAttemp::STATUS_NEW]) || in_array($this->orderAttemp->status,
                StoreOrderAttemp::$companyStatusesGroups[StoreOrderAttemp::STATUS_ACCEPTED]);
    }

    /**
     * Can user download model3dPart considering limit of max downloaded files (max_downloaded_in_work)
     *
     * @link https://redmine.tsdev.work/issues/962
     * @return bool
     * @throws \yii\base\UserException
     */
    public function canDownloadModel3dPartByLimit()
    {
        $orderWithDownloadedFiles = StoreOrderAttemp::find()
            ->select(StoreOrderAttemp::column('order_id'))
            ->distinct()
            ->notTest()
            ->inStatus(StoreOrderAttemp::$companyStatusesGroups[StoreOrderAttemp::STATUS_ACCEPTED])
            ->joinWith('machine.ps', false)
            ->andWhere([
                Ps::column('user_id')                => $this->orderAttemp->ps->user_id,
                CompanyService::column('is_deleted') => 0,
            ])
            ->asArray()
            ->column();

        if (in_array($this->orderAttemp->order->id, $orderWithDownloadedFiles)) {
            return true;
        }

        if (count($orderWithDownloadedFiles) < Yii::$app->setting->get('printer.max_downloaded_in_work', 5)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function canReceive()
    {
        return in_array($this->orderAttemp->status, [StoreOrderAttemp::STATUS_DELIVERED, StoreOrderAttemp::STATUS_SENT]);
    }


    public function canRefundOrderTransaction(): bool
    {
        $refundService = Yii::createObject(RefundService::class);
        return $this->orderAttemp->order && $refundService->canRefundOrderTransaction($this->orderAttemp->order);
    }
}
