<?php
/**
 * Created by mitaichik
 */

namespace frontend\models\ps;


use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class AddResultImageForm
 * @package frontend\models\ps
 */
class AddResultImageForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['file', 'file', 'extensions' => [
                'png', 'jpg', 'jpeg', 'gif',
                'mp4', 'm4a', 'webm', 'ogg', 'ogv', 'avi', 'mng', 'mov', 'mkv', 'qt', '3gp', '3g2'],
                'checkExtensionByMimeType' => false
            ]
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $this->file = $formName === '' ? UploadedFile::getInstanceByName('file') : UploadedFile::getInstance($this, 'file');
        return true;
    }
}