<?php

namespace frontend\models\ps;

use common\components\DateHelper;
use common\interfaces\Model3dBaseInterface;
use common\models\Company;
use common\models\DeliveryType;
use common\models\Ps;
use common\models\CompanyService;
use common\models\PsPrinter;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrderAttemp;
use common\models\User;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyOrderInformer;
use common\modules\informer\models\CompanyServiceInformer;
use common\services\Model3dService;
use common\services\PsPrinterService;
use frontend\models\order\StoreOrderAttemptDeadline;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * PSFacade - main class for PS
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class PsFacade
{
    /**
     * @param Ps $ps
     * @param array $params
     * @param bool $scheme
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public static function getPsLink(Ps $ps, $params = [], $scheme = false)
    {
        $itemLink = Url::toRoute(self::getPsRoute($ps, $params), $scheme);
        return $itemLink;
    }

    /**
     * @param Ps $ps
     * @param array $params
     * @return array
     */
    public static function getPsRoute(Ps $ps, $params = [])
    {
        $route = $ps->hasActiveMachines()
            ? '/c/public/print-services'
            : '/c/public/profile';

        $route = '/c/public/profile';
        return array_merge([$route, 'username' => $ps->url], $params);
    }

    /**
     * @param Ps $ps
     * @param array $params
     * @return array
     */
    public static function getPsReviewsRoute(Ps $ps, $params = [])
    {
        return array_merge(['/c/public/reviews', 'username' => $ps->url], $params);
    }

    public static function getPsLinkAbout(Ps $ps, $params = [], $scheme = false)
    {
        $itemLink = Url::toRoute(self::getPsRouteAbout($ps, $params), $scheme);
        return $itemLink;
    }

    public static function getPsRouteAbout(Ps $ps, $params = [])
    {

        return array_merge(['/c/public/profile', 'username' => $ps->url], $params);
        //return array_merge(['/user/u/printservice', 'psUrl' => $ps->url], $params);

        //return array_merge(['/user/u/printservice-new', 'psUrl' => $ps->url], $params);
    }

    public static function resetOrdersCount(User $user)
    {
        app('cache')->set('user' . $user->id . '.neworders', false, 10);
        PsPrinterService::updatePrintersCache();
    }

    /**
     * Main Properties method
     *
     * @param
     *            $printerId
     * @return ManagePrinterProperties
     */
    public static function properties($printerId = null)
    {
        return new ManagePrinterProperties($printerId);
    }

    /**
     * Get all PS belongs to user
     *
     * @param
     *            $userId
     * @return Ps
     */
    public static function getPsByUserId($userId)
    {
        /** @var Company $ps */
        $ps = Company::find()->where([
            'user_id' => $userId
        ])->one();
        return $ps;
    }

    /**
     * Get all Printers belongs to user
     * If we pass psPrinterId, return only selected printer
     *
     * @param
     *            $userId
     * @return PsPrinter[]|null
     */
    public static function getNotDeletedPsPrintersByUserId($userId)
    {
        /** @var Ps $ps */
        $ps = Ps::find()->where([
            'user_id' => $userId
        ])->one();

        if ($ps) {
            return $ps->notDeletedPrinters;
        }
        return null;
    }

    /**
     * Get PS Printers by Id
     *
     * @param
     *            $psId
     * @return mixed
     */
    public static function getNotDeletedPsPrintersByPsId($psId)
    {
        return PsPrinter::find()
            ->joinWith('company_service')
            ->notDeleted()
            ->andWhere([
                'ps_id' => $psId
            ])
            ->all();
    }

    /**
     * Get all delivery types
     *
     * @return \common\models\DeliveryType[]
     */
    public static function getDeliveryTypes()
    {
        $deliveryTypes = DeliveryType::find()->where([
            'is_active' => 1
        ])->orderBy('id ASC')->all();
        $deliveryTypes = self::wrapDeliveryTypeTranslates($deliveryTypes);
        return $deliveryTypes;
    }

    /**
     * @param string|null $code
     * @return string[]|string
     */
    public static function getDeliveryTypesIntl(string $code = null)
    {
        $translates = [
            'pickup'   => _t('site.delivery', 'Customer Pickup'),
            'standard' => _t('site.delivery', 'Domestic Shipping'),
            'intl'     => _t('site.delivery', 'International Shipping')
        ];
        return $code === null ? $translates : $translates[$code];
    }

    /**
     * @param $deliveryTypes
     * @return mixed
     */
    private static function wrapDeliveryTypeTranslates($deliveryTypes)
    {
        $translates = self::getDeliveryTypesIntl();
        foreach ($deliveryTypes as $k => $v) {
            if (array_key_exists($v->code, $translates)) {
                $v->title = $translates[$v->code];
            }
            $deliveryTypes[$k] = $v;
        }
        return $deliveryTypes;
    }


    /**
     * Get all locations for all printers for current user
     *
     * @param
     *            $userId
     * @return array
     */
    public static function getPrintersLocations($userId)
    {
        $markers = [];

        /** @var Ps $ps */
        $ps = Ps::find()->where([
            'user_id' => $userId
        ])->one();

        if ($ps) {
            /** @var PsPrinter[] $printers */
            $printers = $ps->getPsPrinters()->all();

            if (isset($printers[0])) {
                foreach ($printers as $onePrinter) {
                    $oneLocation = $onePrinter->getLocation()->one();

                    $data['id']    = $oneLocation['id'];
                    $data['title'] = $oneLocation['address'];
                    $data['lat']   = $oneLocation['lat'];
                    $data['lon']   = $oneLocation['lon'];
                    $markers[]     = $data;
                }
            }
        }

        return [
            'status'  => 'OK',
            'markers' => $markers
        ];
    }


    /**
     * Printer have unclosed orders
     *
     * @param PsPrinter $psPrinter
     * @return bool
     */
    public static function isPrinterHaveUnclosedOrders(PsPrinter $psPrinter)
    {
        return StoreOrderAttemp::find()
            ->joinWith([
                'order' => function (StoreOrderQuery $query) {
                    $query->notTest();
                }
            ], false)
            ->forPrinterId($psPrinter->id)
            ->notInStatus([
                StoreOrderAttemp::STATUS_CANCELED,
                StoreOrderAttemp::STATUS_RECEIVED,
                StoreOrderAttemp::STATUS_RETURNED
            ])
            ->exists();
    }

    /**
     * Get estimated printing time
     *
     * @param Model3dBaseInterface $model
     * @return float hours
     */
    public static function getEstimatedPrintingTime($model)
    {
        $timeMinutes = Model3dService::calculateModel3dPrintTime($model);
        $time        = $timeMinutes + app('setting')->get('printer.post_print_time');
        $time        = round($time / 60, 1);

        return $time;
    }

    public static function getAttempScheduledToSentAt(StoreOrderAttemp $attemp)
    {
        $scheduledToSentAt = DateHelper::strtotimeUtc($attemp->dates->scheduled_to_sent_at) - DateHelper::strtotimeUtc(DateHelper::now());
        $scheduledToSentAt = round($scheduledToSentAt / 60 / 60, 1);
        if ($scheduledToSentAt < 0) {
            $scheduledToSentAt = 0;
        }

        return abs($scheduledToSentAt);
    }

    // @TODO extract ALL build volume functions to another helper-class

    /**
     * parse build volume and return array[w,h,l]
     *
     * @param string $str
     * @return array
     */
    public static function parseBuildVolume($str)
    {
        $width  = 200;
        $height = 250;
        $length = 200;
        try {
            $str   = strtolower($str);
            $parts = explode("x", $str);
            if (count($parts) < 2) {
                $parts = explode("х", $str);
            }
            if (!empty($parts[0]) && !empty($parts[1]) && !empty($parts[2])) {


                $width  = floatval($parts[0]);
                $height = floatval($parts[1]);
                $length = floatval($parts[2]);

                if (!empty($parts[3])) { // inch
                    $ms = trim($parts[3]);
                    if ($ms == 'cm') {
                        $width  = $width * 10;
                        $height = $height * 10;
                        $length = $length * 10;
                    } else {
                        if ($ms == 'inches' || $ms == 'in.' || $ms == 'in') {
                            $width  = $width * \lib\MeasurementUtil::convertInchesToMm($width);
                            $height = $height * \lib\MeasurementUtil::convertInchesToMm($height);
                            $length = $length * \lib\MeasurementUtil::convertInchesToMm($length);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
        }
        return [
            $width,
            $height,
            $length
        ];
    }

    /**
     * get build area.
     * required to find largest build volume
     *
     * @param array $sizes
     * @return int
     */
    private static function getBuildArea($sizes)
    {
        return $sizes[0] * $sizes[1] * $sizes[2];
    }

    /**
     * get build volumes
     *
     * @return array
     */
    private static function getBuildVolumes()
    {
        $innerBuildVolumes = app('db')->createCommand("select b.id, a.value, a.printer_id from printer_to_property a
                join printer_properties b ON (b.id=a.property_id)
                join ps_printer  ON (ps_printer.printer_id=a.printer_id)
                join company_service ON company_service.ps_printer_id=ps_printer.id
                where b.id=:property_id  AND company_service.visibility=:visibility
                group by ps_printer.printer_id;")
            ->bindValues([
                ':visibility' => CompanyService::VISIBILITY_EVERYWHERE,
                ':property_id' => PsPrinter::PROPERTY_BUILD_VOLUME_ID
            ])
            ->queryAll();

        $buildVolumes = app('db')->createCommand("SELECT a.value, ps_printer.id, ps_printer.printer_id
            FROM ps_printer_to_property a
            JOIN printer_properties b ON (b.id=a.property_id)
            JOIN ps_printer ON (ps_printer.id=a.ps_printer_id)
            JOIN company_service ON company_service.ps_printer_id=ps_printer.id
            WHERE b.id=:property_id AND company_service.visibility=:visibility")
            ->bindValues([
                ':visibility' => CompanyService::VISIBILITY_EVERYWHERE,
                ':property_id' => PsPrinter::PROPERTY_BUILD_VOLUME_ID
            ])
            ->queryAll();

        $result = [];
        foreach ($buildVolumes as $b) {
            $sizes                              = self::parseBuildVolume($b['value']);
            $result[self::getBuildArea($sizes)] = $sizes;
        }
        foreach ($innerBuildVolumes as $b) {
            $sizes                              = self::parseBuildVolume($b['value']);
            $result[self::getBuildArea($sizes)] = $sizes;
        }
        return $result;
    }

    /**
     * get max build volume size in system
     *
     * @return \common\components\ps\locator\Size
     */
    public static function getMaxBuildVolume()
    {
        $cacheKey = 'ps.maxbuildvolume';
        $maxBuild = app('cache')->get($cacheKey, false);
        if ($maxBuild !== false) {
            return $maxBuild;
        }
        $buildVolumes = self::getBuildVolumes();
        if (empty($buildVolumes)) {
            return \common\components\ps\locator\Size::create(220, 220, 320);
        }
        $max     = max($buildVolumes);
        $maxSize = \common\components\ps\locator\Size::create($max[0], $max[1], $max[2]);

        app('cache')->set($cacheKey, $maxSize, 60 * 60 * 4); // 4 hours
        return $maxSize;
    }

    public static function getResizeMsg($modelIds)
    {
        foreach ($modelIds as $k => $v) {
        }
    }

    public static function getCompanyOrdersFolders()
    {
        return [
            StoreOrderAttemp::STATUS_QUOTE      => _t('site.ps', 'Quotes / Invoices'),
            StoreOrderAttemp::STATUS_NEW        => _t('site.ps', 'New Orders'),
            StoreOrderAttemp::STATUS_ACCEPTED   => _t('site.ps', 'In production'),
            StoreOrderAttemp::STATUS_PRINTED    => _t('site.ps', 'Submitted for moderation'),
            StoreOrderAttemp::STATUS_READY_SEND => _t('site.ps', 'Ready for dispatch'),
            StoreOrderAttemp::STATUS_SENT       => _t('site.ps', 'Dispatched'),
            StoreOrderAttemp::STATUS_DELIVERED  => _t('site.ps', 'Delivered'),
            StoreOrderAttemp::STATUS_RECEIVED   => _t('site.ps', 'Completed'),
            StoreOrderAttemp::STATUS_CANCELED   => _t('site.ps', 'History of cancellation'),
        ];
    }

    public static function getPrinterLayerResolution(CompanyService $psMachine)
    {
        if ($psMachine->type != CompanyService::TYPE_PRINTER) {
            return null;
        }
        $printer           = $psMachine->psPrinter;
        $printerProperties = $printer->getProperties(['code' => [PsPrinter::PROPERTY_LAYER_RESOLUTION_LOW, PsPrinter::PROPERTY_LAYER_RESOLUTION_HIGH]]);
        $res               = ArrayHelper::map(
            $printerProperties,
            'code',
            'value'
        );
        return sprintf("%s - %s", $res[PsPrinter::PROPERTY_LAYER_RESOLUTION_HIGH] ?? '-',
            $res[PsPrinter::PROPERTY_LAYER_RESOLUTION_LOW] ?? '-');

        #$result = sprintf("%s %s", "&#xB1;", (float)$attemp->machine->psPrinter->positional_accuracy?:'Not set' mm
    }
}
