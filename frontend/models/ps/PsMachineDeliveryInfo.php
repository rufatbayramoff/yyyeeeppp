<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.11.17
 * Time: 14:35
 */

namespace frontend\models\ps;

class PsMachineDeliveryInfo
{
    /**
     * treatstock, selfship
     * @var string[]
     */
    public $carriers;
    /**
     * @var string[]
     */
    public $deliveryTypes;

    /**
     * @var string
     */
    public $workTime;

    /**
     * @var  PsMachineDeliveryInfoLocation
     */
    public $location;

    public function getCarrierByDeliveryType($deliveryType)
    {
        if (array_key_exists($deliveryType, $this->carriers)) {
            return $this->carriers[$deliveryType];
        }
        return null;
    }
}

class PsMachineDeliveryInfoLocation
{

    /** @var  string */
    public $address;

    /** @var string */
    public $lat;

    /** @var string */
    public $lon;

    /** @var  string */
    public $countryIso;

}