<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace frontend\models\ps;

use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\PaymentExchangeRateFacade;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\GeoCountry;
use common\models\populators\PsMachinePopulator;
use common\models\Ps;
use common\models\PsCncMachine;
use common\models\PsMachineDelivery;
use common\models\PsPrinter;
use common\models\PsPrinterColor;
use common\models\PsPrinterHistory;
use common\models\PsPrinterMaterial;
use common\models\PsPrinterToProperty;
use common\models\UserAddress;
use common\models\UserLocation;
use common\services\PsPrinterService;
use DateTime;
use DateTimeZone;
use frontend\components\UserSessionFacade;
use frontend\models\site\DenyCountry;
use lib\delivery\exceptions\DeliveryAddressFailedException;
use lib\delivery\exceptions\HaveSuggestAddressException;
use lib\money\Currency;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;

/**
 * Class for create or update PsPrinter from user data
 *
 * @package frontend\models\ps
 */
class PsPrinterUpdater
{
    const ACTION_UPDATE_PRINTER              = 1;
    const ACTION_UPDATE_REDEFINED_PROPERTIES = 2;
    const ACTION_UPDATE_MATERIALS            = 3;
    const ACTION_UPDATE_DELIVERY_TYPES       = 4;
    const ACTION_UPDATE_LOCATION             = 5;

    /**
     *
     * @var PsPrinter
     */
    protected $psPrinter;


    /**
     * @var PsCncMachine
     */
    protected $psCncMachine;

    /**
     *
     * @var Ps
     */
    protected $ps;

    /**
     * Udater created for create new printer
     *
     * @var bool
     */
    protected $new;

    /**
     *
     * @var bool
     */
    protected $wasLocationUpdated = false;

    /**
     * List of executed actions
     *
     * @var int[]
     */
    protected $executedActions = [];

    protected PsMachinePopulator $deliveryUpdate;

    /**
     *
     * @param CompanyService $psMachine
     */
    protected function __construct(CompanyService $psMachine)
    {
        $psMachine->updated_at = dbexpr('NOW()');
        $this->companyService  = $psMachine;
        $this->ps              = $psMachine->ps;
        $this->psPrinter       = $psMachine->psPrinter;
        $this->psCncMachine    = $psMachine->psCncMachine;
        $this->new             = $psMachine->isNewRecord;
        $this->deliveryUpdate  = Yii::createObject(PsMachinePopulator::class);
        if ($psMachine->psPrinter) {
            $psMachine->psPrinter->setScenario(PsPrinter::SCENARIO_UPDATE);
        }
    }

    /**
     * Factory for create new printer
     *
     * @param CompanyService $psMachine
     * @return PsPrinterUpdater
     * @internal param $ps
     * @internal param $psPrinter
     */
    public static function createForCreate(CompanyService $psMachine)
    {
        AssertHelper::assert($psMachine, 'Cant create saved printer');
        $updater = new PsPrinterUpdater($psMachine);
        return $updater;
    }

    /**
     * Factory for update existed printer
     *
     * @param CompanyService $psMachine
     * @return PsPrinterUpdater
     */
    public static function createForUpdate(CompanyService $psMachine)
    {
        AssertHelper::assert(!$psMachine->isNewRecord, 'Cant update new printservice machine.');
        $updater = new PsPrinterUpdater($psMachine);
        return $updater;
    }

    /**
     *
     * @param array $data
     * @return $this
     */
    public function updatePrinter(array $data)
    {
        $this->psPrinter->printer_id = $data['printer_id'];
        $this->psPrinter->setAttributes($data);
        $this->psPrinter->is_ready_thingiverse       = empty($data['is_ready_thingiverse']) ? 0 : 1;
        $this->psPrinter->companyService->updated_at = DateHelper::now();

        if ($this->new) {
            $this->psPrinter->companyService->created_at       = DateHelper::now();
            $this->psPrinter->companyService->visibility       = CompanyService::VISIBILITY_EVERYWHERE;
            $this->psPrinter->companyService->moderator_status = CompanyService::MODERATOR_STATUS_PENDING;
        } else {
            AssertHelper::assert($this->psPrinter->printer_id == $this->psPrinter->getOldAttribute('printer_id'), 'Cant change printer for psPrinter');
        }

        AssertHelper::assertSave($this->psPrinter);
        AssertHelper::assertSave($this->psPrinter->companyService);

        $this->addExecutedAction(self::ACTION_UPDATE_PRINTER);
        PsPrinterService::updatePrintersCache();
        return $this;
    }

    /**
     *
     * @param array $data
     * @return $this
     */
    public function updateRedefinedProperties(array $data)
    {
        $this->checkIsNotNew();
        $psPrinter = $this->psPrinter;
        $user      = $this->ps->user;

        $redefinedPropertiesData = ArrayHelper::index($data, 'property_id');
        /** @var PsPrinterToProperty[] $redefinedProperties */
        $redefinedProperties = ArrayHelper::index($psPrinter->redifinedProperties, 'property_id');

        // delete

        $deletedIds = array_diff(array_keys($redefinedProperties), array_keys($redefinedPropertiesData));
        foreach ($deletedIds as $deletedId) {
            $psPrinter->unlink(PsPrinter::RELATION_REDEFINED_PROPERTIES, $redefinedProperties[$deletedId], true);
            PsPrinterHistory::saveHistory($psPrinter->id, $user->id, 'delete_redefined_property');
        }
        unset($deletedIds, $deletedId);

        // update or create

        foreach ($redefinedPropertiesData as $propertyId => $redefinedPropertyData) {
            // alradey exist
            if (isset($redefinedProperties[$propertyId])) {
                $redefinedProperty = $redefinedProperties[$propertyId];
                $redefinedProperty->setScenario(PsPrinterToProperty::SCENARIO_UPDATE);
                $redefinedProperty->setAttributes($redefinedPropertyData);
                AssertHelper::assertSave($redefinedProperty);
            } else {
                $redefinedProperty = new PsPrinterToProperty();
                $redefinedProperty->setAttributes($redefinedPropertyData);
                $psPrinter->addRedefinedProperty($redefinedProperty);
            }
        }

        $this->addExecutedAction(self::ACTION_UPDATE_REDEFINED_PROPERTIES);
        return $this;
    }

    /**
     *
     * @param array $data
     * @param string $measure
     * @return $this
     * @throws Exception
     * @todo Remove measure after refactoring
     */
    public function updateMaterials(array $data, $measure)
    {
        $this->checkIsNotNew();
        $psPrinter = $this->psPrinter;

        /** @var PsPrinterMaterial[] $materials */
        $materials     = ArrayHelper::index($psPrinter->materials, 'material_id');
        $materialsData = ArrayHelper::index($data, 'material_id');

        // delete deleted materials

        $deletedMaterialIds = array_diff(array_keys($materials), array_keys($materialsData));
        foreach ($deletedMaterialIds as $deletedMaterialId) {
            $psPrinter->deleteMaterial($materials[$deletedMaterialId]);
        }
        unset($deletedMaterialIds, $deletedMaterialId);

        // update or create materials
        $resetModeratorStatus = false;
        if ($psPrinter->min_order_price) {
            $min_order_price_usd = PaymentExchangeRateFacade::convert($psPrinter->min_order_price, $psPrinter->companyService->company->currency, Currency::USD);
        } else {
            $min_order_price_usd = null;
        }

        foreach ($materialsData as $materialId => $materialData) {
            if (isset($materials[$materialId])) {
                $material = $materials[$materialId];
                $material->setScenario(PsPrinterMaterial::SCENARIO_UPDATE);
                $material->setAttributes($materialData);
                AssertHelper::assertSave($material);
            } else {
                $material = new PsPrinterMaterial();
                $material->setAttributes($materialData);
                $psPrinter->addMaterial($material);
            }

            /** @var \common\models\PsPrinterColor[] $colors */
            $colors     = ArrayHelper::index($material->colors, 'color_id');
            $colorsData = ArrayHelper::index($materialData['colors'], 'color_id');

            if (!$colorsData) {
                throw new BusinessException(_t('site.ps', 'Material needs to contain color options.'));
            }

            // delete deleted colors

            $deletedColorsIds = array_diff(array_keys($colors), array_keys($colorsData));
            foreach ($deletedColorsIds as $deletedColorsId) {
                $material->unlink('colors', $colors[$deletedColorsId], true);
            }
            unset($deletedColorsIds, $deletedColorsId);

            // update or create colors

            foreach ($colorsData as $colorId => $colorData) {
                if (isset($colors[$colorId])) {
                    $color = $colors[$colorId];
                    $color->setScenario(PsPrinterColor::SCENARIO_UPDATE);
                    $color->setAttributes($colorData);
                    $color->price_measure = $measure;
                    AssertHelper::assertSave($color);
                } else {
                    $color = new PsPrinterColor();
                    $color->setAttributes($colorData);
                    $color->price_measure = $measure;
                    $material->addColor($color);
                    AssertHelper::assertSave($color);
                }

                if ($color->getPriceUsdPerGr()->getAmount() < (float)app('setting')->get('printer.moderation_minimum_print_cost_per_gram') &&
                    ((!$min_order_price_usd) || $min_order_price_usd < (float)app('setting')->get('printer.moderation_minimum_print_cost_per_order'))
                ) {
                    $resetModeratorStatus = true;
                }

            }
        }

        if ($resetModeratorStatus) {
            $psPrinter->companyService->moderator_status = CompanyService::MODERATOR_STATUS_PENDING;
            $psPrinter->companyService->updated_at       = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
            AssertHelper::assertSave($psPrinter->companyService);
            AssertHelper::assertSave($this->psPrinter);
        }

        $this->addExecutedAction(self::ACTION_UPDATE_MATERIALS);
        return $this;
    }

    /**
     *
     * @param array $data
     * @return $this
     */
    public function updateDeliveryTypes(array $data)
    {
        AssertHelper::assert(!$this->wasLocationUpdated, 'Must first update deliveryTypes, and after - location');

        $this->checkIsNotNew();
        $psMachine = $this->companyService;
        $this->saveHistoryDelivery($psMachine, $data['deliveryTypes']);

        $this->deliveryUpdate->populate($psMachine, $data);

        //renew delivery types
        $psMachine->__unset('deliveryTypess');
        $psMachine->populateRelation('deliveryTypess', $psMachine->getDeliveryTypess()->withoutStaticCache()->all());
        $this->addExecutedAction(self::ACTION_UPDATE_DELIVERY_TYPES);
        return $this;
    }

    /**
     * Update printer location.
     * It must be run after self::updateDeliveryTypes if deliveryTypes was changed
     *
     * @param array $data
     * @return $this
     * @throws HaveSuggestAddressException
     * @throws BusinessException
     */
    public function updateLocation(array $data)
    {
        $this->checkIsNotNew();
        $psMachine = $this->companyService;

        // verify post address

        $havePostDelivery = array_reduce(
            $psMachine->deliveryTypess,
            function ($result, PsMachineDelivery $deliveryType) {
                return $result || $deliveryType->deliveryType->getIsPostDelivery() && $deliveryType->carrier == DeliveryType::CARRIER_TS;
            },
            false
        );

        $address = new UserAddress();
        $address->setAttributes($data);

        if ($address->isRegionRequired() && empty($address->region)) {
            throw new BusinessException(_t('site.ps', 'Please enter region name'));
        }
        if ($havePostDelivery) {

            DenyCountry::checkCountry($address->country->iso_code);
            $this->validateEasyPostAddress($address);
        }
        unset($havePostDelivery, $address, $validateAddress);

        // update

        $location = self::findOrCreateLocation($data);

        DenyCountry::checkCountry($location->country->iso_code, true);

        if ((!$psMachine->location) || ($psMachine->location->id != $location->id)) {
            if (!$this->new) {
                $psMachine->safeSave();
                if ($psMachine->psPrinter) {
                    $psMachine->psPrinter->companyService->is_location_change = true;
                    $psMachine->psPrinter->companyService->updated_at         = DateHelper::now();
                    $psMachine->psPrinter->companyService->moderator_status   = CompanyService::MODERATOR_STATUS_PENDING;
                    $psMachine->psPrinter->safeSave();
                    $psMachine->psPrinter->companyService->safeSave();
                    $psMachine->psPrinter->addHistoryRecord(
                        'change_location',
                        [
                            'oldLocation' => $psMachine->location
                        ]
                    );
                }
            }
            $psMachine->link('location', $location);
        }

        $this->addExecutedAction(self::ACTION_UPDATE_LOCATION);
        return $this;
    }

    /**
     * Find same user location or create new user location
     *
     * @param
     *            $locationData
     * @return UserLocation
     * @throws \common\components\exceptions\InvalidModelException
     */
    protected function findOrCreateLocation($locationData)
    {
        $location = UserLocation::findOrCreateLocation($this->ps->user, $locationData);
        AssertHelper::assertSave($location);
        return $location;
    }

    /**
     *
     * @param array $post
     * @return $this
     */
    public function updateAll(array $post)
    {
        $printerInfo = $post['psPrinter'];
        $machineInfo = $post['psMachine'];
        $this->updatePrinter($printerInfo);
        $this->updateRedefinedProperties($printerInfo['redifinedProperties']);
        $this->updateMaterials($printerInfo['materials'], $printerInfo['measure']);
        $this->updateDeliveryTypes($machineInfo);
        $this->updateLocation($machineInfo['location']);
        return $this;
    }

    /**
     *
     * @throws Exception
     */
    protected function checkIsNotNew()
    {
        if ($this->psPrinter->isNewRecord) {
            throw new Exception('Cant update new printer');
        }
    }

    /**
     * Update printer modeartion status
     */
    public function unpdatePrinterModerationStatus()
    {
        if (!$this->executedActions) {
            return $this;
        }
        $newStatus = $currentStatus = $this->psPrinter->companyService->moderator_status;
        if ($currentStatus == CompanyService::MODERATOR_STATUS_REJECTED) {
            $newStatus = CompanyService::MODERATOR_STATUS_REJECTED_REVIEWING;
        }
        if ($this->isActionExecuted(self::ACTION_UPDATE_PRINTER) && $this->isActionExecuted(self::ACTION_UPDATE_REDEFINED_PROPERTIES)) {
            $newStatus = CompanyService::MODERATOR_STATUS_PENDING;
        }
        $this->psPrinter->companyService->moderator_status = $newStatus;
        $this->psPrinter->companyService->safeSave();
        return $this;
    }

    /**
     * @param int $action
     */
    protected function addExecutedAction($action)
    {
        $this->executedActions[] = $action;
    }

    /**
     * @param int $action
     * @return bool
     */
    protected function isActionExecuted($action)
    {
        return in_array($action, $this->executedActions);
    }

    /**
     * Update printers cache in deamons
     */
    public function invalidatePrintersCahce(): void
    {
        PsPrinterService::updatePrintersCache();
    }

    /**
     * @param $data
     * @return $this
     * @throws DeliveryAddressFailedException
     * @throws HaveSuggestAddressException
     */
    public function validateCanEasyPost($data)
    {
        $deliveries = array_filter($data, function ($delivery) {
            return $delivery['delivery_type_id'] != DeliveryType::PICKUP_ID &&
                $delivery['carrier'] === DeliveryType::CARRIER_TS;
        });
        $country    = $this->country();
        foreach ($deliveries as $delivery) {
            if ($delivery['delivery_type_id'] == DeliveryType::STANDART_ID) {
                $result = $this->psPrinter->companyService->canEasyPost('domestic');
            } else {
                $result = $this->psPrinter->companyService->canEasyPost('intl');
            }
            if (!$result) {
                throw new BusinessException("Easypost not work {$country->country->title}");
            }
        }
        if (!empty($deliveries) && $this->psPrinter->companyService && $this->psPrinter->companyService->location) {
            $this->validateEasyPostAddress($this->psPrinter->companyService->location->toUserAddress());
        }
        return $this;
    }

    protected function validateEasyPostAddress(UserAddress $address)
    {
        $validateAddressResult = Yii::$app->deliveryService->validateAddress($address);
        if (!$validateAddressResult->isSuccess()) {
            throw new DeliveryAddressFailedException($validateAddressResult->getErrorMessage());
        }

        if ($suggestAddress = $validateAddressResult->getSuggestAddress()) {
            throw new HaveSuggestAddressException($suggestAddress);
        }
    }

    protected function country()
    {
        if ($this->psPrinter->companyService->location) {
            $country = $this->psPrinter->companyService->location;
        } else {
            $country = GeoCountry::findOne(['iso_code' => UserSessionFacade::getLocation()->country]);
        }
        return $country;
    }

    protected function saveHistoryDelivery(CompanyService $psMachine, array $data)
    {
        $user              = $this->ps->user;
        $deliverTypes      = ArrayHelper::index($psMachine->deliveryTypess, 'delivery_type_id');
        $deliveryTypesData = ArrayHelper::index($data, 'delivery_type_id');
        $deleteIds         = array_diff_key($deliverTypes, $deliveryTypesData);
        foreach ($deleteIds as $deleteDelivery) {
            if ($psMachine->psPrinter) {
                PsPrinterHistory::saveRemoveDelivery($psMachine->psPrinter->id, $user->id, Json::encode($deleteDelivery->toArray()));
            }
        }
    }
}