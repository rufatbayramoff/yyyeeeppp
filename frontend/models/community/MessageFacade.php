<?php
namespace frontend\models\community;

use common\components\ActiveQuery;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\models\base\UserAdmin;
use common\models\factories\FileFactory;
use common\models\message\builders\TopicBuilder;
use common\models\message\checkers\FilesMessageLimit;
use common\models\message\checkers\MessageCreateChecker;
use common\models\message\checkers\NewAddresatsLimit;
use common\models\message\checkers\SessionMessagesLimit;
use common\models\message\forms\MessageForm;
use common\models\message\forms\TopicForm;
use common\models\MsgFile;
use common\models\MsgFolder;
use common\models\MsgMember;
use common\models\MsgMessage;
use common\models\MsgReport;
use common\models\MsgTopic;
use common\models\Preorder;
use common\models\query\MsgFileQuery;
use common\models\repositories\FileRepository;
use common\models\StoreOrder;
use common\models\User;
use common\models\UserProfile;
use common\modules\contentAutoBlocker\ContentAutoBlockerModule;
use frontend\models\user\UserFacade;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * Private messages facade class
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class MessageFacade
{

    /**
     * how many last messages select by default
     */
    const LIMIT_HISTORY = 500;
    public static $totalUnread = [];
    public static function getDefaultMessagesFolder()
    {
        $user = UserFacade::getCurrentUser();
        $folders = self::getFoldersForUser();

        // Make menu config
        foreach ($folders as $folder) {
            $unreadedCount = MessageFacade::getUnreadedTopicsCount($user, $folder);
            if ($unreadedCount) {
                return $folder;
            }
        }
        $folder = reset($folders);
        return $folder;
    }


    /**
     * Return message folders for user.
     * Now it basic system folders from, but it can be rewrited
     *
     * @return MsgFolder[]
     */
    public static function getFoldersForUser()
    {
        return MsgFolder::find()->disablePersonal()->orderBy('sort')->all();
    }

    /**
     * Return count of unreaded topics in folder.
     * This method caclulate unreaded topics for all folders, cached it, and return value for concrete folder.
     *
     * @param User $user
     * @param MsgFolder $folder
     * @return int
     */
    public static function getUnreadedTopicsCount(User $user, MsgFolder $folder)
    {
        static $cache = [];

        if (!isset($cache[$user->id])) {
            $data = \Yii::$app->db->createCommand(
                '
                SELECT
                    mm.folder_id, COUNT(*) AS count
                FROM msg_member mm
                    join msg_topic m on mm.topic_id = m.id
                    join user u on m.creator_id = u.id and u.status != :status
                WHERE mm.user_id = :userId AND mm.have_unreaded_messages = 1
                GROUP BY mm.folder_id
            ',
                [
                    'userId' => $user->id,
                    'status' => User::STATUS_UNCONFIRMED
                ]
            )->queryAll();

            $cache[$user->id] = ArrayHelper::map($data, 'folder_id', 'count');
        }

        return isset($cache[$user->id][$folder->id]) ? (int)$cache[$user->id][$folder->id] : 0;
    }

    /**
     * get total unreaded messages
     *
     * @param User $user
     * @return string
     */
    public static function getUnreadedTotal(User $user)
    {
        if(array_key_exists($user->id, self::$totalUnread)){
            return self::$totalUnread[$user->id];
        }

        $totalQuery = MsgMember::find()
            ->from(['mm' => MsgMember::tableName()])
                ->innerJoin(['t' => MsgTopic::tableName()], 'mm.topic_id = t.id')
                ->innerJoin(['u' => User::tableName()], 't.creator_id = u.id and u.status != :status', ['status' => User::STATUS_UNCONFIRMED])
            ->where([
                'user_id'                => $user->id,
                'have_unreaded_messages' => 1
            ]);
        $total = $totalQuery->count();

        self::$totalUnread[$user->id] = $total;
        app('cache')->set('user' . $user->id . '.unreaded', $total, 20); // 20 sec

        return $total;
    }

    /**
     * Return MsgTopic dataProvider witch latest dialogs
     *
     * @param User $user
     *            user
     * @param MsgFolder $folder
     *            specified folder
     * @param string|null $type
     * @return ActiveDataProvider
     */
    public static function getTopicsDataProvider(User $user, MsgFolder $folder,string $type = null)
    {
        $query = MsgTopic::find()
            ->from(['topic' => MsgTopic::tableName()])
                ->innerJoin(['cu' => User::tableName()], 'cu.id = topic.creator_id AND cu.status != :status', ['status' => User::STATUS_UNCONFIRMED])
                ->joinWith('members', false)
            ->andWhere([
                'msg_member.user_id'    => $user->id,
                'msg_member.is_deleted' => 0
            ])
            ->andFilterWhere([
                'msg_member.folder_id'  => $folder->id,
            ])
            ->orderBy([
                'topic.last_message_time' => SORT_DESC
            ]);
        if ($type === MsgMember::TYPE_UNREAD) {
            $query->andWhere(['<>', 'msg_member.have_unreaded_messages', 0]);
        }
        if ($type === MsgMember::TYPE_WITHOUT_ANSWER) {
            $query->innerJoin(['msg_message' => MsgMessage::tableName()], 'msg_message.topic_id = topic.id');
            $msgQuery = (new Query)->from(['mms' => 'msg_message']);
            $msgQuery->leftJoin(['cfcm' => 'content_filter_checked_messages'], 'cfcm.message_id = mms.id');
            $msgQuery->andWhere(['cfcm.isBanned' => 0]);
            $msgQuery->select(['id' => new Expression("max(mms.id)")]);
            $msgQuery->groupBy('mms.topic_id');
            $query->innerJoin(['msg' => $msgQuery], 'msg_message.id=msg.id and msg_message.user_id <>' . $user->id);
            $query->andWhere(['>','topic.id', 2]);
        }
        return new ActiveDataProvider(
            [
                'query'      => $query,
                'pagination' => [
                    'defaultPageSize' => static::LIMIT_HISTORY,
                    'pageSizeLimit'   => static::LIMIT_HISTORY
                ]
            ]
        );
    }

    /**
     * Return MsgMessage data provider for topic
     *
     * @param User $user
     * @param MsgTopic $topic
     * @param bool $withoutDeletedMessages
     * @param int|null $fromMessageId
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     * @todo data-provider is not need more
     */
    public static function getMessagesDataProvider(User $user, MsgTopic $topic, $withoutDeletedMessages = true, $fromMessageId = null)
    {
        $query = MsgMessage::find()
            ->with('user')
            ->with(['msgFiles' => function(MsgFileQuery $query) {
                $query
                    ->active()
                    ->notExpired();
            }])
            ->andWhere(['topic_id' => $topic->id]);

        if ($fromMessageId) {
            AssertHelper::assertNumeric($fromMessageId);
            $query->andWhere(['>', MsgMessage::column('id'), $fromMessageId]);
        }

        // if this user topic was deleted, dont show older message (message was sent before delete)
        if ($withoutDeletedMessages) {
            $member = $topic->getMemberForUser($user);
            if ($member->hide_message_before) {
                $query->andWhere(
                    [
                        '>',
                        'created_at',
                        $member->hide_message_before
                    ]
                );
            }
        }

        return new ActiveDataProvider(
            [
                'query'      => $query,
                'pagination' => [
                    'pageSize' => static::LIMIT_HISTORY
                ]
            ]
        );
    }

    /**
     * @param MsgMessage $message
     * @param $user
     * @param bool $checkImmediately
     * @return bool
     */
    public static function filterOneMessage($message, $user, $checkImmediately = false)
    {
        if (($message->topic_id === MsgTopic::BOT_MESSAGE_COMPANIES) || ($message->topic_id === MsgTopic::BOT_MESSAGE_ALL)) {
            return true;
        }
        foreach ($message->topic->msgMembers as $msgMember) {
            if ($msgMember->user_id === $user->id) {
                if ($msgMember->folder->alias === 'support') {
                    return true;
                }
            }
        }
        if ($checkImmediately || Yii::$app->getModule('contentAutoBlocker')->contentBlocker->isObjectChecked(ContentAutoBlockerModule::CHECK_MESSAGE, $message)) {
            if (Yii::$app->getModule('contentAutoBlocker')->contentBlocker->isObjectBanned(ContentAutoBlockerModule::CHECK_MESSAGE, $message, $checkImmediately)) {
                if ($message->user_id === $user->id) {
                    // $message->isBlocked = true;
                } else {
                    return false;
                }
            }
        } else {
            if ($message->user_id !== $user->id) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param MsgTopic $topic
     * @param $user
     * @param bool $checkImmediately
     * @return bool
     */
    public static function filterOneTopic(MsgTopic $topic, $user, $checkImmediately = false)
    {
        if (($topic->id === MsgTopic::BOT_MESSAGE_COMPANIES) || ($topic->id === MsgTopic::BOT_MESSAGE_ALL)) {
            return true;
        }
        foreach ($topic->msgMembers as $msgMember) {
            if ($msgMember->user_id === $user->id) {
                if ($msgMember->folder->alias === 'support') {
                    return true;
                }
            }
        }
        if ($checkImmediately || Yii::$app->getModule('contentAutoBlocker')->contentBlocker->isObjectChecked(ContentAutoBlockerModule::CHECK_MESSAGE_TOPIC, $topic)) {
            if (Yii::$app->getModule('contentAutoBlocker')->contentBlocker->isObjectBanned(ContentAutoBlockerModule::CHECK_MESSAGE_TOPIC, $topic, $checkImmediately)) {
                if ($topic->creator_id === $user->id) {
                    return true;
                } else {
                    $topic->title = 'Message from date ' . \Yii::$app->formatter->asDate(strtotime($topic->created_at));
                    return false;
                }
            }
        } else {
            if ($topic->creator_id !== $user->id) {
                return false;
            }
        }
        return true;
    }


    /**
     * @param User $user
     * @param MsgTopic $topic
     * @param bool $withoutDeletedMessages
     * @param null $fromMessageId
     * @return array
     */
    public static function getFilteredMessages(User $user, MsgTopic $topic, $withoutDeletedMessages = true, $fromMessageId = null)
    {
        $resultMessages = [];
        /** @var MsgMessage[] $messages */
        $messages = self::getMessagesDataProvider($user, $topic, $withoutDeletedMessages, $fromMessageId)->getModels();
        $i = 0;
        foreach ($messages as $message) {
            if (self::filterOneMessage($message, $user)) {
                $resultMessages[] = $message;
            } elseif ($i === 0 && !$fromMessageId) {
                //$message->text = _t('site.messages', 'Message is blocked');
                //$resultMessages[] = $message;
            }
            $i++;
        }
        return $resultMessages;
    }


    /**
     * @param User $user
     * @param MsgTopic $topic
     * @return MsgMessage|null
     */
    public static function getLastFilteredMessage(User $user, MsgTopic $topic)
    {

        $messages = MessageFacade::getFilteredMessages($user, $topic);
        if ($messages) {
            return end($messages);
        }

        return null;
    }

    /**
     * @param User $user
     * @param MsgFolder $folder
     * @return MsgTopic[]
     */
    public static function getFilteredTopics(User $user, MsgFolder $folder,$type = null)
    {
        $resultTopics = [];
        $topics = self::getTopicsDataProvider($user, $folder, $type)->getModels();
        foreach ($topics as $topic) {
            /** @var $topic MsgTopic */
            if (self::filterOneTopic($topic, $user)) {
                if(!empty($topic->messages))
                    $resultTopics[] = $topic;
            }
        }
        return $resultTopics;
    }

    public static function getFilteredTopicsProvider(User $user, MsgFolder $folder, $type = null)
    {
        $topics = self::getFilteredTopics($user, $folder, $type);
        $dataProvider = new ArrayDataProvider(
            [
                'allModels' => $topics,
            ]
        );
        return $dataProvider;
    }

    /**
     * Search by user messages
     *
     * @param User $user
     * @param string $search
     *
     * @return ActiveDataProvider
     */
    public static function getFilteredUserMessagesProvider(User $user, $search = ''): ActiveDataProvider
    {
        $topicsQuery = MsgTopic::find()
            ->select([
                't.*'
            ])
            ->from(['mb' => MsgMember::tableName()])
                ->join('JOIN', ['t' => MsgTopic::tableName()], 't.id = mb.topic_id')
                ->join('JOIN', ['mb2' => MsgMember::tableName()], 'mb2.topic_id = t.id')
                ->join('JOIN', ['u' => User::tableName()], 'u.id = mb2.user_id')
                ->join('JOIN', ['up' => UserProfile::tableName()], 'up.user_id = u.id')
                ->join('JOIN', ['m' => MsgMessage::tableName()], 'm.topic_id = mb2.topic_id')
            ->where([
                'mb.user_id' => $user->id,
                'mb.is_deleted' => 0
            ])
            ->orderBy(['t.last_message_time' => SORT_DESC])
            ->groupBy(['t.id']);

        if (!empty($search)) {
            $topicsQuery
                ->andWhere([
                    'or',
                    [
                        'and',
                        ['t.bind_to' => MsgTopic::BIND_OBJECT_ORDER],
                        ['t.bind_id' => $search]
                    ],
                    'MATCH (t.title) AGAINST (:search)',
                    'MATCH (up.firstname, up.lastname, up.full_name) AGAINST (:search)',
                    'MATCH (u.username) AGAINST (:search)',
                    'MATCH (m.text) AGAINST (:search)'
                ], [
                    'search' => $search
                ]);
        }

        return new ActiveDataProvider(
            [
                'query'      => $topicsQuery,
                'pagination' => [
                    'defaultPageSize' => static::LIMIT_HISTORY,
                    'pageSizeLimit'   => static::LIMIT_HISTORY
                ]
            ]
        );
    }

    /**
     * Move topic to archive for user
     *
     * @param MsgTopic $topic
     * @param User $user
     * @throws \yii\base\Exception
     */
    public static function moveTopicToArchive(MsgTopic $topic, User $user)
    {
        $member = $topic->getMemberForUser($user);
        if ($topic->isBotSupport()) {
            // Do nothing
            return;
        }
        AssertHelper::assert($member->folder_id != MsgFolder::FOLDER_ID_ARCHIVE, 'Topic already in archive', BusinessException::class);
        $member->original_folder = $member->folder_id;
        $member->folder_id = MsgFolder::FOLDER_ID_ARCHIVE;
        AssertHelper::assertSave($member);
    }

    /**
     * Mark this topic as readed for user
     *
     * @param MsgTopic $topic
     * @param User $user
     * @throws \yii\base\Exception
     */
    public static function markTopicAsReaded(MsgTopic $topic, User $user)
    {
        $member = $topic->getMemberForUser($user);
        $member->have_unreaded_messages = 0;
        AssertHelper::assertSave($member);
    }

    /**
     * Mark this topic as unreaded for user
     *
     * @param MsgTopic $topic
     * @param User $user
     * @throws \yii\base\Exception
     */
    public static function markTopicAsUnreaded(MsgTopic $topic, User $user)
    {
        $member = $topic->getMemberForUser($user);
        $member->have_unreaded_messages = 1;
        AssertHelper::assertSave($member);
    }

    /**
     * Delete topic for user
     *
     * @param MsgTopic $topic
     * @param User $user
     */
    public static function deleteTopic(MsgTopic $topic, User $user)
    {
        $member = $topic->getMemberForUser($user);
        $member->have_unreaded_messages = 0;
        $member->hide_message_before = new Expression('NOW()');
        $member->is_deleted = 1;
        AssertHelper::assertSave($member);
    }

    /**
     * @param MsgTopic $topic
     * @param MessageForm $messageForm
     * @param User $fromUser
     */
    public static function runAddMessageCheckers(MsgTopic $topic, MessageForm $messageForm, User $fromUser)
    {
        /** @var MessageCreateChecker[] $checkers */
        $checkers = [new SessionMessagesLimit(), new NewAddresatsLimit(), new FilesMessageLimit()];
        foreach ($checkers as $checker){
            $checker->checkCreateMessage($topic, $messageForm, $fromUser);
        }
    }

    /**
     * Add message to topic
     *
     * @param MsgTopic $topic
     * @param User $fromUser
     * @param MessageForm $messageForm
     * @param UserAdmin $supportUser
     * @param bool $sendNotify Send notify (email/sms) to members that they received new message.
     * @return MsgMessage
     * @throws HttpException
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public static function addMessage(MsgTopic $topic, User $fromUser, MessageForm $messageForm, UserAdmin $supportUser = null, bool $sendNotify = true)
    {
        if($fromUser->status===User::STATUS_UNCONFIRMED){
            if(!$fromUser->thingiverseUser)
                throw new BusinessException(_t('site.user', 'To send personal messages please confirm email.'), 400);
        }
        self::runAddMessageCheckers($topic, $messageForm, $fromUser);

        $member = $topic->getMemberForUser($fromUser);
        if ($member->getIsDeleted()) {
            $member->restoreToOriginalFolder();
        }

        $message = new MsgMessage();
        $message->user_id = $fromUser->id;
        $message->topic_id = $topic->id;
        $message->text = $messageForm->message;
        $message->created_at = new Expression('NOW()');

        if ($supportUser) {
            $message->support_user_id = $supportUser->id;
        }
        AssertHelper::assertSave($message);

        if ($messageForm->files) {
            $fileFactory = Yii::createObject(FileFactory::class);
            $fileRepository = Yii::createObject(FileRepository::class);
            foreach ($messageForm->files as $formFile) {
                $file = $fileFactory->createFileFromUploadedFile($formFile);
                $fileRepository->save($file);
                MsgFile::create($message, $file);
            }
        }

        $message->refresh();

        if ($sendNotify) {
            $emailer = new Emailer();
            foreach ($topic->getOtherUsers($fromUser) as $user) {
                if (self::filterOneTopic($topic, $user, true) && self::filterOneMessage($message, $user, true)) {

                    $bindObj = $topic->getBindedObject();

                    if ($bindObj instanceof StoreOrder) {
                        $emailer->sendNewOrderMessage($fromUser, $user, $message, $bindObj);
                    }
                    elseif ($bindObj instanceof Preorder) {
                        $emailer->sendNewPreorderMessage($fromUser, $user, $message, $bindObj);
                    }
                    else {
                        $emailer->sendNewMessage($fromUser, $user, $message);
                    }
                }
            }
        }

        // mark other user that they have new unreaded topic
        MsgMember::updateAll(
            [
                'is_deleted' => 0
            ],
            'topic_id = :topicId AND user_id <> :userId',
            [
                'topicId' => $topic->id,
                'userId'  => $fromUser->id
            ]
        );

        // update topic
        $topic->last_message_time = new Expression('NOW()');
        if ($supportUser) {
            $topic->last_support_user_id = $supportUser->id;
        }
        AssertHelper::assertSave($topic);

        return $message;
    }

    /**
     * Resolve topic by form.
     * For compare topics use userIds and bind object
     *
     * @todo after make topics like chat this method will be work not corrected
     * @param TopicForm $form
     * @return MsgTopic
     */
    public static function findTopicByForm(TopicForm $form)
    {
        $query = MsgTopic::find()->betweenUsers(
            $form->initialUser->id,
            $form->getMemberUser()->id
        );
        $query->andWhere([
            'bind_id' => $form->bindId,
            'bind_to' => $form->bindTo
        ]);
        $query->andWhere(['not in','member1user.topic_id',[MsgTopic::BOT_MESSAGE_ALL, MsgTopic::BOT_MESSAGE_COMPANIES]]);
        $query->andWhere(['not in','member2user.topic_id',[MsgTopic::BOT_MESSAGE_ALL, MsgTopic::BOT_MESSAGE_COMPANIES]]);
        $query->andWhere(['member1user.is_deleted' => 0, 'member2user.is_deleted' => 0]);
        $query->orderBy('id DESC');
        return $query->one();
    }

    /**
     * Simple topic factory
     *
     * @param User $initialUser
     * @param User $user
     * @param      $title
     * @param      $message
     * @return MsgTopic
     * @throws \yii\base\Exception
     */
    public static function createTopic(User $initialUser, User $user, $title, $message)
    {
        return TopicBuilder::builder()->setInitialUser($initialUser)
            ->to($user)
            ->setTitle($title)
            ->setMessage($message)
            ->create();
    }

    /**
     * Create topic who binded to object
     *
     * @param User $initialUser
     * @param User $user
     * @param StoreOrder $order
     * @param            $title
     * @param            $message
     * @return MsgTopic
     * @throws \yii\base\Exception
     */
    public static function createOrderTopic(User $initialUser, User $user, StoreOrder $order, $title, $message)
    {
        $topicForm = new TopicForm();
        $topicForm->initialUser = $initialUser;
        $topicForm->setMemberUser($user);
        $topicForm->setBindedObject($order);

        if ($existTopic = self::findTopicByForm($topicForm)) {
            MessageFacade::addMessage($existTopic, $initialUser, new MessageForm(['message' => $message]));
            return $existTopic;
        }

        return TopicBuilder::builder()->setInitialUser($initialUser)
            ->to($user)
            ->setTitle($title)
            ->setMessage($message)
            ->bindObject($order)
            ->create();
    }

    /**
     * @param StoreOrder $order
     * @param User $fromUser
     * @param string $message
     * @throws HttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public static function sendOrderNotify(StoreOrder $order, User $fromUser, $message)
    {
        // Find exists topic
        $lastTopic = MsgTopic::find()
            ->where(['bind_to' => MsgTopic::BIND_OBJECT_ORDER, 'bind_id' => $order->id])
            ->betweenUsers($order->user->id, $fromUser->id)
            ->orderBy('created_at desc')->one();

        if (!$lastTopic) {
            $lastTopic = TopicBuilder::builder()
                ->setInitialUser($fromUser)
                ->setTitle('Order #' . $order->id)
                ->to($order->user)
                ->bindObject($order)
                ->create();
        }

        $messageForm = new MessageForm(['message' => $message]);
        $message = MessageFacade::addMessage($lastTopic, $fromUser, $messageForm, null, true);
        // Possible duplication for notifications?
        // Anzhelika asked to send this notifications
        return $message;
    }

    /**
     * Create report message
     *
     * @param ReportMessageForm $form
     */
    public static function  reportMessage(ReportMessageForm $form)
    {
        $report = new MsgReport();
        $report->topic_id = $form->getTopic()->id;
        $report->reason_id = $form->reasonId;
        $report->create_user_id = $form->getUser()->id;
        $report->comment = $form->reasonDescription;
        $report->moderated = 0;
        $report->created_at = new Expression('NOW()');
        AssertHelper::assertSave($report);
    }
}
