<?php
/**
 * Created by mitaichik
 */

namespace frontend\models\community;


use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\models\ContentFilterCheckedTopics;
use common\models\MsgFile;
use common\models\MsgMessage;
use common\models\MsgReport;
use common\models\MsgTopic;
use Throwable;
use yii\base\Exception;

/**
 * Class TopicMerger
 *
 * Provide functional for merge two topics into one.
 *
 * @link https://redmine.tsdev.work/issues/5287
 * @package frontend\models\community
 */
class TopicMerger
{
    /**
     * Merge topics to $dstTopic and delete $srcTopic.
     *
     * @param MsgTopic $srcTopic
     * @param MsgTopic $dstTopic
     */
    public function mergeTopics(MsgTopic $srcTopic, MsgTopic $dstTopic) : void
    {
        if (!$this->checkPossibilityOfMerging($srcTopic, $dstTopic)) {
            throw new Exception("Cant merge topcs {$srcTopic->id} {$dstTopic->id}");
        }

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            MsgMessage::updateAll(['topic_id' => $dstTopic->id], ['topic_id' => $srcTopic->id]);
            MsgReport::updateAll(['topic_id' => $dstTopic->id], ['topic_id' => $srcTopic->id]);
            MsgFile::updateAll(['topic_id' => $dstTopic->id], ['topic_id' => $srcTopic->id]);
            ContentFilterCheckedTopics::updateAll(['topic_id' => $dstTopic->id], ['topic_id' => $srcTopic->id]);

            foreach ($dstTopic->members as $member) {
                $member->hide_message_before = null;
                $member->is_deleted = 0;
                $member->safeSave();
            }

            $dstTopic->refresh();
            $srcTopic->refresh();

            $this->removeTopic($srcTopic);

            $transaction->commit();
        }
        catch (Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * Check possibility of mergin two topics.
     *
     * @param MsgTopic $srcTopic
     * @param MsgTopic $dstTopic
     * @return bool
     */
    private function checkPossibilityOfMerging(MsgTopic $srcTopic, MsgTopic $dstTopic) : bool
    {
        AssertHelper::assertNotNew($srcTopic);
        AssertHelper::assertNotNew($dstTopic);

        $srcMembersIds = ArrayHelper::getColumn($srcTopic->members, 'user_id');
        $dstMembersIds = ArrayHelper::getColumn($dstTopic->members, 'user_id');
        return $srcMembersIds == $dstMembersIds;
    }

    /**
     * Remove topic.
     *
     * @param MsgTopic $topic
     * @throws \Exception
     * @throws Throwable
     */
    private function removeTopic(MsgTopic $topic) : void
    {
        foreach ($topic->members as $member) {
            AssertHelper::assertDelete($member);
        }
        AssertHelper::assertDelete($topic);
    }
}