<?php
/**
 * Created by mitaichik
 */

namespace frontend\models\community;


use common\components\reject\BaseRejectForm;
use common\models\MsgTopic;
use common\models\SystemReject;
use common\models\User;

class ReportMessageForm extends BaseRejectForm
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var MsgTopic
     */
    private $topic;


    public function __construct(User $user, MsgTopic $topic)
    {
        parent::__construct();
        $this->user = $user;
        $this->topic = $topic;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return MsgTopic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::REPORT_MESSAGE;
    }
}