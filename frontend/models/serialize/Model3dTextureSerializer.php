<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.01.17
 * Time: 9:51
 */

namespace frontend\models\serialize;

use common\models\factories\Model3dTextureFactory;
use common\models\Model3dTexture;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialGroupRepository;
use common\models\repositories\PrinterMaterialRepository;
use yii\db\BaseActiveRecord;

class Model3dTextureSerializer
{
    const ATTRIBUTES_DESCRIPTION = [
        'printer_color_id'          => [
            'relationName' => 'printerColor',
        ],
        'printer_material_id'       => [
            'relationName' => 'printerMaterial'
        ],
        'printer_material_group_id' => [
            'relationName' => 'printerMaterialGroup'
        ],
        'infill'                    => [
            'attributeName' => 'infill',
        ],
        'color'                     => [
            'relationName' => 'printerColor',
        ],
        'material'                  => [
            'relationName' => 'printerMaterial'
        ],
        'materialGroup'             => [
            'relationName' => 'printerMaterialGroup'
        ]
    ];


    /** @var null|PrinterColorRepository */
    protected $printerColorRepository = null;

    /** @var null|PrinterMaterialRepository */
    protected $printerMaterialRepository = null;

    /** @var null|PrinterMaterialGroupRepository */
    protected $printerMaterialGroupRepository = null;


    public function injectDependencies(
        PrinterColorRepository $printerColorRepository,
        PrinterMaterialRepository $printerMaterialRepository,
        PrinterMaterialGroupRepository $printerMaterialGroupRepository
    )
    {
        $this->printerColorRepository         = $printerColorRepository;
        $this->printerMaterialRepository      = $printerMaterialRepository;
        $this->printerMaterialGroupRepository = $printerMaterialGroupRepository;
    }

    protected function serializeAttributes($model, $attributesDescription)
    {
        $returnValue = [];
        foreach ($attributesDescription as $attribute => $attributeDescription) {
            if (strpos($attribute, '_')) {
                continue; // Skip underscore attributes
            }
            if (array_key_exists('attributeName', $attributeDescription)) {
                $attributeName               = $attributeDescription['attributeName'];
                $returnValue[$attributeName] = $model->$attributeName;
            } else {
                $relationName  = $attributeDescription['relationName'];
                if ($model->$relationName) {
                    $returnValue[$attribute] = $model->$relationName->code;
                } else {
                    $returnValue[$attribute] = null;
                }
            }
        }
        return $returnValue;
    }

    public function serialize($model3dTexture)
    {
        return $this->serializeAttributes($model3dTexture, self::ATTRIBUTES_DESCRIPTION);
    }

    /**
     * @param Model3dTexture $model
     * @param $modelDescription
     * @param $attributesDescription
     */
    protected function unSerializeAttributes($model, $modelDescription, $attributesDescription)
    {
        $model->printer_color_id = null;
        $model->populateRelation('printerColor', null);
        $model->printer_material_id = null;
        $model->populateRelation('printerMaterial', null);
        $model->printer_material_group_id = null;
        $model->populateRelation('printerMaterialGroup', null);
        $model->infill = null;

        foreach ($attributesDescription as $key => $attributeInfo) {
            $relationName = array_key_exists('relationName', $attributeInfo) ? $attributeInfo['relationName'] : $attributeInfo['attributeName'];
            if (!array_key_exists($key, $modelDescription)) {
                continue;
            }
            $idCode = $modelDescription[$key];
            $id     = (int)$idCode; // Check is id digit
            if ((string)$idCode === (string)$id) {
                // Use id
                if ($relationName === 'printerColor') {
                    $printerColor = $this->printerColorRepository->getById($id);
                    $model->setPrinterColor($printerColor);
                }
                if ($relationName === 'printerMaterial') {
                    $printerMaterial = $this->printerMaterialRepository->getById($id);
                    $model->setPrinterMaterial($printerMaterial);
                }
                if ($relationName === 'printerMaterialGroup') {
                    $printerMaterialGroup = $this->printerMaterialGroupRepository->getById($id);
                    $model->setPrinterMaterialGroup($printerMaterialGroup);
                }
                if ($relationName === 'infill') {
                    $model->infill = $id;
                }
            } else {
                $code = $idCode;
                // Use code
                if ($relationName === 'printerColor') {
                    $printerColor = $this->printerColorRepository->getByCode($code);
                    $model->setPrinterColor($printerColor);
                }
                if ($relationName === 'printerMaterial') {
                    $printerMaterial = $this->printerMaterialRepository->getByCode($code);
                    $model->setPrinterMaterial($printerMaterial);
                }
                if ($relationName === 'printerMaterialGroup') {
                    $printerMaterialGroup = $this->printerMaterialGroupRepository->getByCode($code);
                    $model->setPrinterMaterialGroup($printerMaterialGroup);
                }
            }
        }
    }

    /**
     * @param array $model3dTextureDescription
     * @return \common\models\Model3dTexture
     */
    public function unSerialize($model3dTextureDescription)
    {
        $model3dTexture = Model3dTextureFactory::createModel3dTexture();
        $this->unSerializeAttributes($model3dTexture, $model3dTextureDescription, self::ATTRIBUTES_DESCRIPTION);
        return $model3dTexture;
    }

}