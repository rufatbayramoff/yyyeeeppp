<?php
namespace frontend\models\site;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{

    public $name;

    public $email;

    public $subject;

    public $body;

    public $orderId;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    'name',
                    'email',
                    'subject',
                    'body'
                ],
                'required'
            ],
            [['name', 'subject', 'body', 'orderId'], 'string', 'max' => 2000],
            [
                'email',
                'email'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => _t('front.site', 'Verification Code')
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email
     *            the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        $body = $this->body;
        $body = $body . "\n\nName: " . $this->name . " \n\nEmail: " . $this->email;
        if ($this->orderId) {
            $body = $body."\n\nOrderId: ".$this->orderId;
        }
        $body = nl2br($body);
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom(param('adminEmail', 'support@treatstock.com'))
            ->setSubject("Contact Us - " . $this->subject)
            ->setHtmlBody($body)
            ->send();
    }
}
