<?php
namespace frontend\models\site;

use common\components\ArrayHelper;
use common\components\exceptions\BusinessException;
use common\models\GeoCountry;
use yii\base\Model;

/**
 * author: DeFacto
 * date: 20.11.15
 */
class DenyCountry extends Model
{

    /**
     *
     * @return array
     */
    public static function getDenyCountries()
    {
        return ArrayHelper::map(GeoCountry::find()->where(['paypal_payout' => 0, 'is_bank_transfer_payout' => 0])->asArray()->all(), 'iso_code', 'title');
    }

    /**
     *
     * @return string
     */
    public static function getWhiteList()
    {
        return [
            "US" => "United States"
        ];
    }

    /**
     *
     * @param
     *            $isoCode
     * @param bool $throw
     * @return bool
     * @throws BusinessException
     */
    public static function checkCountry($isoCode, $throw = true)
    {
        $toDeny = self::getDenyCountries();
        $isoCodes = array_keys($toDeny);
        if (empty($isoCode)) {
            return true;
        }
        if (in_array($isoCode, $isoCodes)) {
            if ($throw) {
                throw new BusinessException(_t("site", "Unfortunately, Treatstock payment system does not work in your country due to PayPal restrictions."));
            }
            return false;
        }

        return true;
    }
}