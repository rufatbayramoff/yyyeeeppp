<?php
/**
 * Created by mitaichik
 */

namespace frontend\models\site;


use frontend\components\UserSessionFacade;
use yii\base\Model;
use yii\web\UploadedFile;

class HireDesignerForm extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $description;

    /**
     * @var UploadedFile[]
     */
    public $files;

    /**
     * @var float
     */
    public $estimateCost;

    /**
     * @var string
     */
    public $completeDate;

    /**
     * @var boolean
     */
    public $agreePolicy;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'required', 'message' => _t('site.front', 'Email cannot remain blank')],
            ['email', 'email', 'message' => _t('site.front', 'Invalid email address')],

            ['description', 'required', 'message' => _t('site.front', 'Description cannot remain blank')],
            ['description', 'string'],

            ['estimateCost', 'required', 'message' => _t('site.front', 'Budget cannot remain blank')],
            ['estimateCost', 'number', 'message' => _t('site.front', 'Budget must be a number')],

            ['completeDate', 'required', 'message' => _t('site.front', 'Deadline cannot remain blank')],
            ['completeDate', 'string'],

            ['files', 'file', 'maxFiles' => 5],

            ['agreePolicy', 'boolean'],
            ['agreePolicy', 'compare', 'compareValue' => 1, 'message' => _t('site.front', 'You must accept Treatstock Rules.')],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => _t('site', 'Email'),
            'description' => _t('site', 'Description'),
            'files' => _t('site', 'Files'),
            'estimateCost' => _t('site', 'Budget'),
            'completeDate' => _t('site', 'Deadline'),
            'agreePolicy' => _t('site', 'Agree policy'),
        ];
    }

    /**
     * @param bool $asHtml
     * @return string
     */
    public function asString($asHtml = false)
    {
        $messageTextPairs = [];
        foreach ($this->getAttributes(['email', 'description', 'completeDate']) as $attrName => $attrValue) {
            $attrValue = htmlspecialchars($attrValue, ENT_QUOTES, 'utf-8');
            $messageTextPairs[] = $this->getAttributeLabel($attrName) . ': ' . $attrValue;
        }
        $messageTextPairs[] = $this->getAttributeLabel('estimateCost') . ': ' . $this->estimateCost.' '.UserSessionFacade::getCurrency();
        return implode($asHtml ? ";<br/>" : ";\n", $messageTextPairs);
    }
}