<?php

namespace frontend\models\site;

use common\components\Emailer;
use yii\base\Model;

/**
 */
class SendEmailForm extends Model
{

    public $email;

    public $shareLink;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'email'
                ],
                'required'
            ],
            [
                'email',
                'email'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => _t('front.site', 'Email to')
        ];
    }

    public function sendEmail()
    {
        $user = \frontend\models\user\UserFacade::getCurrentUser();
        $userEmail = $user->email;
        $name = H(\frontend\models\user\UserFacade::getFormattedUserName($user));

        $subject = _t('site.email', 'A product has been recommended to you on Treatstock.com', ['name' =>$name]);
        $messageText = _t(
            'site.email',
            'Hello,
{name} ({email}) would like to share this product with you: {model} 

Thank you,
Treatstock Team
        ', ['name' => $name, 'email' => $userEmail, 'model' => '<a href="'. $this->shareLink . '" target="_blank">'. $this->shareLink . '</a>']);


        $emailer = new Emailer();
        $emailer->sendMessage($this->email, $subject, $messageText);
    }
}
