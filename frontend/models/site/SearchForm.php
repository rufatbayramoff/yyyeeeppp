<?php
namespace frontend\models\site;

use yii\base\Model;

/**
 * Search form in header
 */
class SearchForm extends Model
{

    public $query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'query'
                ],
                'required'
            ],
            [
                'query',
                'string',
                'min' => 2,
                'max' => 55
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'query' => _t('front.site', 'Search')
        ];
    }
}