<?php

namespace frontend\models\payment;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaypalLib
{

    /**
     *
     * @var ApiContext
     */
    private static $apiContext;

    /**
     * reset payout inits
     */
    public static function reset()
    {
        self::$apiContext = null;
    }

    /**
     *
     * @param int $clientId            
     * @param string $clientSecret            
     * @return ApiContext
     */
    public static function initApiContext($clientId, $clientSecret)
    {
        if (self::$apiContext) {
            return self::$apiContext;
        }
        $apiContext = new ApiContext(new OAuthTokenCredential($clientId, $clientSecret));
        $apiContext->setConfig(array(
            'mode' => param('paypal_mode', 'sandbox'),
            'log.LogEnabled' => true,
            'log.FileName' => \Yii::getAlias('@app/runtime/logs/PayPal.log'),
            'log.LogLevel' => param('paypal_log_level', 'DEBUG'), // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
            'validation.level' => 'log',
            'cache.enabled' => true
        ));
        self::$apiContext = $apiContext;
        return $apiContext;
    }
}
