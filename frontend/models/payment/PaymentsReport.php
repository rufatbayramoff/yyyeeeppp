<?php
/**
 * User: nabi
 */

namespace frontend\models\payment;

use backend\modules\statistic\reports\BaseReportInterface;
use common\models\PaymentDetail;
use common\modules\payment\helpers\PaymentDetailHelper;

class PaymentsReport implements BaseReportInterface
{
    private $rows = [];
    private $columnNames = [];

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }

    public static function create()
    {
        return new self;
    }

    public function getFileName()
    {
        return 'earnings_report';
    }
    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return $this->columnNames;
    }

    public function setColumnNames(array $columns)
    {
        $this->columnNames = $columns;
    }

    public function setRows($rows)
    {
        /** @var PaymentDetail $row */
        foreach($rows as $row){
            $row->description = PaymentDetailHelper::getFullDescription($row);
            $row->type = $row->getTypeTitle();
        }
        $this->rows = $rows;


    }
    public function getItems()
    {
        return $this->rows;
    }
}