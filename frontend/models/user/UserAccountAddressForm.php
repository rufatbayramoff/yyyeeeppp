<?php

namespace frontend\models\user;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserAccountAddressForm extends \common\models\UserAddress
{

    public $country;

    public function rules()
    {
        return [
            [
                [
                    'created_at',
                    'updated_at'
                ],
                'safe'
            ],
            [
                [
                    'user_id'
                ],
                'required'
            ],
            [
                [
                    'user_id',
                    'country_id'
                ],
                'integer'
            ],
            [
                [
                    'zip_code'
                ],
                'string',
                'max' => 15
            ],
            [
                [
                    'contact_name',
                    'region',
                    'city',
                    'address',
                    'extended_address',
                    'type',
                    'phone',
                    'email',
                    'company'
                ],
                'string',
                'max' => 145
            ],
            [
                [
                    'comment'
                ],
                'string',
                'max' => 155
            ]
        ];
    }
}
