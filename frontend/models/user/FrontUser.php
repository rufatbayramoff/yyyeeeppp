<?php

namespace frontend\models\user;

use common\models\User;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * FrontUser - addon functions to work with website
 * menu, settings and etc.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class FrontUser extends \common\models\User implements IdentityInterface
{
    /**
     *
     * @param string $token
     * @param mixed $type
     * @return void|IdentityInterface
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne([
            'access_token' => $token,
            'status' => self::getActiveStatuses()
        ]);
    }

    public function rules()
    {
        return [
            [
                [ 
                    'email'
                ],
                'required'
            ],
            [
                [
                    'status'
                ],
                'integer'
            ],
            [
                [ 
                    'email'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'email'
                ],
                'unique'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'status' => self::getActiveStatuses()
        ]);
    }

    /**
     * Finds user by username
     *
     * @param string $username            
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne([
            'username' => $username,
            'status' => self::getActiveStatuses()
        ]);
    }

    /**
     * Finds user by email
     *
     * @param string $email            
     * @return FrontUser
     */
    public static function findByEmail($email)
    {
        return static::findOne([
            'email' => $email,
            'status' => self::getActiveStatuses()
        ]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token
     *            password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (! static::isPasswordResetTokenValid($token)) {
            return null;
        }
        
        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::getActiveStatuses()
        ]);
    }

    /**
     *
     * @return array
     */
    public static function getActiveStatuses()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_UNCONFIRMED,
            self::STATUS_DRAFT_COMPANY,
            self::STATUS_UNACTIVE,
            self::STATUS_DRAFT
        ];
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token
     *            password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $authKey && $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password
     *            password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password            
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function generateUserName()
    {
         // get part from email
        $username = substr($this->email, 0, strpos($this->email, '@'));
        if(\common\models\User::findOne(['username'=>$username])){
            $username = $username . rand(1000,9999);
        }
        $this->username = $username;
        $this->can_change_username = 1;
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * get editable fields for privateProfile
     *
     * @TODO - remove
     * @deprecated
     * @return array
     */
    public static function getEditableFields()
    {
        $editableFields = [
            'full_name',
            'info_about',
            'info_skills',
            'dob_date',
            'address',
            'website'
        ]
        // 'user.email'
        ;
        return $editableFields;
    }


    public function needConfirm()
    {
        return in_array($this->status, [User::STATUS_UNCONFIRMED, User::STATUS_DRAFT_COMPANY, User::STATUS_DRAFT]) && $this->email;
    }
}
