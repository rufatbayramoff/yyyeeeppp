<?php
namespace frontend\models\user;

use common\components\exceptions\BusinessException;
use yii\base\InvalidParamException;
use yii\base\Model;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{

    public $password;

    /**
     *
     * @var FrontUser
     */
    private $user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token            
     * @param array $config
     *            name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || ! is_string($token)) {
            throw new BusinessException('Password reset token cannot be blank.');
        }
        $this->user = FrontUser::findByPasswordResetToken($token);
        if (! $this->user) {
            throw new BusinessException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     *
     * @return type
     */
    public function attributeLabels()
    {
        return [
            'email' => _t('front.user', 'E-mail'),
            'password' => _t('front.user', 'Password')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'password',
                'required'
            ],
            [
                'password',
                'string',
                'min' => 6
            ]
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();
        
        return $user->save(false);
    }
}
