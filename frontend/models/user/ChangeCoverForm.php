<?php

namespace frontend\models\user;

use common\components\FileDirHelper;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class ChangeCoverForm extends Model
{
    const MAX_FILE_SIZE = 10000000;

    /**
     * file size limit
     *
     * @var int
     */
    private $maxSize = self::MAX_FILE_SIZE;

    /**
     * file extension
     *
     * @var array
     */
    private $extension = 'jpg,jpeg,png,gif';

    /**
     *
     * @var UploadedFile
     */
    public $file = '';

    /**
     * init upload configs from system_setting
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param $subFolder
     * @return bool|string
     */
    public function upload($subFolder)
    {
        if ($this->validate()) {
            $folder = \Yii::getAlias(\Yii::$app->params['uploadPath']);
            $filePath = '/' . $subFolder . '/cover_'.time().'.' . $this->file->extension;
            FileDirHelper::createDir($folder . '/' . $subFolder);
            array_map('unlink', glob($folder . '/' . $subFolder . '/cover*'));
            $this->file->saveAs($folder . $filePath);
            return $filePath;
        } else {
            return false;
        }
    }

    /**
     * rules and limits for uploaded file
     *
     * @return array
     */
    public function rules()
    {
        return [
            [
                'file',
                'file',
                'checkExtensionByMimeType' => false,
                'extensions' => $this->extension,
                'maxSize' => $this->maxSize
            ],
            [
                'file',
                'required'
            ]
        ];
    }
}
