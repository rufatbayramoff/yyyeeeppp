<?php

namespace frontend\models\user;

use common\models\User;
use Yii;

/**
 * Resend email request form
 */
class ResendEmailForm extends \common\components\BaseForm
{

    /**
     * email
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var FrontUser
     */
    private $user;

    /**
     *
     * @param User $user            
     * @internal param array $config
     */
    public function __construct(User $user, $config = [])
    {
        parent::__construct($config);
        $this->user = $user;
        $this->email = $user->email;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [
                'email',
                'filter',
                'filter' => 'trim'
            ],
            [
                'email',
                'required'
            ],
            [
                'email',
                'email'
            ]
        ];
    }

    /**
     * send new message
     */
    public function resend()
    {
        $user = $this->user;
        if (! $this->validate()) {
            throw new \Exception(_t('front', 'Validation error'));
        }
        $emailer = new \common\components\Emailer();
        $emailer->sendConfirmEmail($user);
    }
}
