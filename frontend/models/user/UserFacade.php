<?php

namespace frontend\models\user;

use common\components\exceptions\AssertHelper;
use common\components\FileDirHelper;
use common\models\Model3d;
use common\models\Ps;
use common\models\repositories\UserSessionRepository;
use common\models\User;
use common\models\user\ChangeEmailException;
use common\models\user\ChangeEmailRequest;
use common\models\UserCollection;
use common\models\UserRequest;
use common\models\UserSession;
use common\modules\affiliate\helpers\AffiliateUrlHelper;
use common\modules\affiliate\services\AffiliateService;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyInstantPaymentInformer;
use common\modules\informer\models\CompanyOrderInformer;
use common\modules\informer\models\CompanyServiceInformer;
use common\modules\informer\models\CustomerOrderInformer;
use common\modules\informer\models\CustomerQuoteInformer;
use common\modules\informer\models\EarningInformer;
use common\modules\informer\models\ProductInformer;
use common\modules\informer\models\ReviewInformer;
use common\services\StoreOrderService;
use frontend\components\Icon;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserSessionFacade;
use frontend\components\UserUtils;
use frontend\models\model3d\CollectionFacade;
use lib\money\Currency;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Html;

/**
 * UserFacade - collects functions to work with users [osn,profile]
 * - creating profile
 * - binding osn
 * - register & login with osn
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserFacade
{
    public static $denyPatterns = [
//        'admin',
        'support',
        'treatstock',
        'help',
        'manager'
    ];


    /**
     *
     * @var FrontUser
     */
    private static $user;

    /**
     * login user to system using OSN
     * update lastlogin_at
     * save login log
     *
     * @param \common\models\User|\yii\web\IdentityInterface $user
     * @param bool $rememberMe
     */
    public static function loginByOsn(\common\models\User $user, $rememberMe = false)
    {
        if (empty($user->auth_key)) {
            $user->auth_key = \Yii::$app->security->generateRandomString();
        }
        \Yii::$app->user->login($user, $rememberMe ? 3600 * 24 * 30 : 0);
        $user->lastlogin_at = time();
        $user->save(false);
        UserFacade::logLogin(
            [
                'user_id'    => $user->id,
                'result'     => 'ok',
                'login_type' => 'osn'
            ]
        );
    }

    /**
     * get user folder prefix
     *
     * @param int $userId
     * @return string
     */
    public static function getUserFolder($userId)
    {
        return 'user/' . md5($userId);
    }

    /**
     * returns current user, or false if a guest
     *
     * @return null|FrontUser
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getCurrentUser()
    {
        if (self::$user) {
            return self::$user;
        }
        // TODO remove this crutch
        if (Yii::$app instanceof \yii\console\Application) {
            self::$user = FrontUser::tryFindByPk(User::USER_CRON);
            return self::$user;
        }

        self::$user = app('user')->getIdentity();

        if (!self::$user || !self::$user->id) {
            return null;
        }

        // lets update last activity
        if (self::$user instanceof \common\models\base\User) {
            $userStats = self::$user->userStatistics;
            try {
                if ($userStats === null) {
                    $userStats                = new \common\models\UserStatistics();
                    $userStats->user_id       = self::$user->id;
                    $userStats->updated_at    = dbexpr('NOW()');
                    $userStats->lastonline_at = dbexpr('NOW()');
                    $userStats->safeSave();
                } else {
                    if (strtotime($userStats->lastonline_at) < time() - 60) { // 1 minute
                        $userStats->lastonline_at = dbexpr('NOW()');
                        $userStats->safeSave();
                    }
                }
            } catch (\Exception $e) {
            }
        }
        return self::$user;
    }

    /**
     * @return UserSession
     */
    public static function getUserSession()
    {
        if (Yii::$app instanceof \yii\console\Application) {
            return null;
        }
        return UserSessionRepository::getGlobalUserSession();
    }

    /**
     * get current user id
     *
     * @return int | boolean
     */
    public static function getCurrentUserId()
    {
        $curUser = self::getCurrentUser();
        $result  = null;
        if ($curUser) {
            $result = $curUser->id;
        }
        return $result;
    }

    /**
     * get full user folder path in system
     * will create folder if required for user if required
     *
     * @param int $userId
     * @return string
     */
    public static function getUserFolderPath($userId)
    {
        $uploadFolder   = \Yii::getAlias(param('uploadPath'));
        $prefix         = self::getUserFolder($userId);
        $userFolderPath = $uploadFolder . '/' . $prefix;
        FileDirHelper::createDir($userFolderPath);
        return $userFolderPath;
    }

    /**
     *
     * @param int $userId
     * @param bool $relative
     * @return string
     */
    public static function getUserFolderUrl($userId, $relative = false)
    {
        $userFolder = self::getUserFolder($userId);
        $staticUrl  = param('staticUrl');
        if ($relative) {
            return '/static/' . $userFolder;
        }
        return $staticUrl . '/' . $userFolder;
    }

    /**
     * create user profile
     *
     * @param int $userId
     * @param array $data
     * @return bool
     */
    public static function createProfile($userId, $data = [])
    {
        $userProfile                       = new \common\models\UserProfile();
        $userProfile->user_id              = $userId;
        $userProfile->current_lang         = app()->language;
        $userProfile->current_currency_iso = app()->session["currency"] ?: Currency::USD;
        $userProfile->current_metrics      = app()->session["metrics"] ?: 'in';
        $userProfile->timezone_id          = UserSessionFacade::getLocation()->timezone ?? 'UTC';
        foreach ($data as $k => $v) {
            $userProfile->$k = $v;
        }
        $result = $userProfile->safeSave();
        return $result;
    }

    /**
     * creates user and returns user object
     * password is required in userData
     *
     * @param array $userData
     * @return FrontUser
     * @throws \Exception
     * @throws \yii\base\UserException
     */
    public static function createUser($userData)
    {
        $user = new FrontUser();
        foreach ($userData as $k => $v) {
            $user->$k = $v;
        }
        if (empty($userData['password'])) {
            throw new \yii\base\UserException('Password not defined for user');
        }
        $dbtr = app('db')->beginTransaction();
        try {
            if (empty($user->username)) {
                $user->generateUserName();
            }

            $user->username     = self::fixDenyUsername($user->username);
            $user->lastlogin_at = time();
            $user->setPassword($userData['password']);
            $user->generateAuthKey();
            $user->safeSave();

            AssertHelper::assert(CollectionFacade::createCollection($user->id, _t('front.collection.names', 'Favorites'), UserCollection::SYSTEM_TYPE_LIKES));
            AssertHelper::assert(CollectionFacade::createCollection($user->id, _t('front.collection.names', 'Wish List'), UserCollection::SYSTEM_TYPE_LIKES));
            $profileData = !empty($userData['profile']) ? $userData['profile'] : [];
            UserFacade::createProfile($user->id, $profileData);
            $affiliateService = Yii::createObject(AffiliateService::class);
            $affiliateService->bindUserWithCurrentAffiliate($user);
            $dbtr->commit();
        } catch (\Exception $e) {
            $dbtr->rollBack();
            throw $e;
        }
        return $user;
    }

    /**
     * Check username for deny words and return correct username
     * @link https://redmine.tsdev.work/issues/4900
     * @param string $username
     * @return string
     */
    private static function fixDenyUsername(string $username): string
    {
        if (self::hasDanyUsernameWords($username)) {
            $username = "user" . $username;
        }
        return $username;
    }

    /**
     * @param string $username
     * @return bool
     */
    public static function hasDanyUsernameWords(string $username): bool
    {
        $username = strtolower($username);

        foreach (self::$denyPatterns as $denyPattern) {
            if (strpos($username, $denyPattern) === 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * binds user id to osn code.
     * table [[user_osn]] used
     *
     * @param int $userId
     * @param array $data
     * @return \common\models\UserOsn
     */
    public static function bindOsn($userId, $data = [])
    {
        $userOsn = new \common\models\UserOsn();
        $osnCode = $data['osn_code'];
        /**
         *
         * @var \common\models\UserOsn $userOsnRow
         */
        $userOsnRow = $userOsn->findOne(
            [
                'user_id'  => $userId,
                'osn_code' => $osnCode
            ]
        );

        if ($userOsnRow) {
            $userOsnRow->updated_at = dbexpr('NOW()');
            $userOsnRow->update(false);
            return $userOsn;
        }
        $userOsn->user_id = $userId;
        foreach ($data as $k => $v) {
            $userOsn->$k = $v;
        }
        $userOsn->created_at = dbexpr('NOW()');
        $userOsn->updated_at = dbexpr('NOW()');
        $userOsn->safeSave();
        return $userOsn;
    }

    /**
     * creates user by OSN login
     * password generated
     * - creates profile
     * - binds user_id to osn
     * - sends email to user to confirm account and email
     *
     * @param array $userData
     * @return \common\models\User
     */
    public static function createUserByOsn($userData)
    {
        // user
        $randPassword = substr(app()->getSecurity()->generateRandomString(), 0, 6);
        $hasUser      = User::findOne(
            [
                'username' => $userData['username']
            ]
        );
        if ($hasUser) {
            $userData['username'] = null;
        }
        $user                      = UserFacade::createUser(
            [
                'email'    => $userData['email'],
                'password' => $randPassword,
                'username' => $userData['username'],
                'status'   => \common\models\User::STATUS_ACTIVE
            ]
        );
        $user->can_change_username = 1;
        if ($user) {
            // osn
            if (!empty($userData['osn'])) {
                UserFacade::bindOsn($user->id, $userData['osn']);
            }
            \common\models\UserLog::log($user->id, 'register_new', 0, $userData['osn']['osn_code']);
            UserFacade::logLogin(
                [
                    'user_id'    => $user->id,
                    'result'     => 'ok',
                    'login_type' => 'osn'
                ]
            );
            // send email to confirm signup and email
            $userEmail = clone $user;
            $emailer   = new \common\components\Emailer();

            $emailer->sendSignupByOsnEmail($userEmail, $randPassword);
        }
        return $user;
    }

    /**
     * log user login
     *
     * @param array $userData
     *            - format ['user_id'=>, 'result'=>'ok|failed']
     */
    public static function logLogin($userData)
    {
        $request     = Yii::$app->request;
        $loginLogObj = new \common\models\UserLoginLog();
        $postData    = app('request')->post();
        if ($userData['result'] == 'ok') {
            $postData = ['result' => 'ok']; // do not log if correct
        }
        $logData = [
            'data' => [
                'login_type'      => !empty($userData['login_type']) ? $userData['login_type'] : 'form',
                'remote_addr'     => $request->getUserIP(),
                'http_user_agent' => substr($request->getUserAgent(), 0, 250),
                'created_at'      => new \yii\db\Expression('NOW()'),
                'http_referer'    => substr(\Yii::$app->request->referrer, 0, 250),
                'post_data'       => \yii\helpers\Json::encode($postData),
                'user_id'         => $userData['user_id'],
                'result'          => $userData['result']
            ]
        ];
        $loginLogObj->load($logData, 'data');
        $loginLogObj->safeSave();
    }

    /**
     * get user links for private profile
     *
     * @param User $user
     * @return array
     */
    public static function getUserLinks(User $user)
    {
        $menu = [
            [
                'url'   => '/profile/collection',
                'label' => Icon::get('collections') . _t('front.user', 'Collections')
            ],
            [
                'url'   => '@web/my/store',
                'label' => Icon::get('mystore') . _t('front.user', 'Store')
            ],
            [
                'url'   => '@web/mybusiness/company',
                'label' => Icon::get('printservice') . _t('front.user', 'Print service')
            ],
            [
                'url'   => '/workbench/messages',
                'label' => Icon::get('messages') . _t('front.user', 'Messages')
            ],
            [
                'url'   => '@web/my/social',
                'label' => Icon::get('social') . _t('front.user', 'Social')
            ],
            [
                'url'   => UserUtils::getUserPublicProfileRoute($user),
                'label' => Icon::get('profile') . _t('front.user', 'Public profile')
            ],
            [
                'url'   => '@web/my/settings',
                'label' => Icon::get('settings') . _t('front.user', 'Settings')
            ]
        ];
        return $menu;
    }

    public static function hasStore(\common\models\User $user)
    {
        return $user->company && !empty($user->userProfile->is_modelshop);
    }

    public static function hasUploads(User $user)
    {
        return !empty($user->model3ds);
    }

    public static function hasPs(\common\models\base\User $user)
    {
        return !empty($user->ps);
    }

    public static function hasPrinter(\common\models\base\User $user)
    {
        if (!self::hasPs($user)) {
            return false;
        }

        /**
         *
         * @var Ps $ps
         */
        $ps = Ps::tryFindByPk($user->ps[0]->id);
        return !empty($ps->notDeletedPrinters);
    }

    private static function hasCollections(User $user)
    {
        return !empty($user->userCollections);
    }

    public static function getTopNotify(User $user)
    {
        $totalInformers = 0;
        $checkInformers = [
            ProductInformer::class,
            CompanyOrderInformer::class,
            CompanyServiceInformer::class,
            CustomerOrderInformer::class,
            CustomerQuoteInformer::class,
            ReviewInformer::class,
            EarningInformer::class,
            CompanyInstantPaymentInformer::class
        ];
        foreach ($checkInformers as $checkInformer) {
            if (InformerModule::getInformer($user, $checkInformer)) {
                $totalInformers++;
            }
        }
        return $totalInformers;
    }

    public static function getTopBusinessMenuLinks(User $user)
    {
        $redDot = ImageHtmlHelper::getRedDot();

        $userTopMenu = [
            [
                'link'  => [
                    '/mybusiness/company'
                ],
                'title' => _t("site", "About"),
                'icon'  => 'tsi-briefcase'
            ],
            [
                'link'  => '/mybusiness/settings',
                'title' => _t("site", "Settings"),
                'icon'  => 'tsi-parameters-v'
            ],
            [
                'link'  => '/mybusiness/products',
                'title' => _t("site", "Store") . ' ' . (InformerModule::getInformer($user, ProductInformer::class) ? $redDot : ''),
                'icon'  => 'tsi-cube'
            ],
            [
                'link'  => '/mybusiness/services',
                'title' => _t("site", "Services") . ' ' . (InformerModule::getInformer($user, CompanyServiceInformer::class) ? $redDot : ''),
                'icon'  => 'tsi-wrench'
            ],
            [
                'link'  => '/mybusiness/widgets',
                'title' => _t("site", "Business Tools"),
                'icon'  => 'tsi-components'
            ],
        ];

        foreach ($userTopMenu as $k => $v) {
            if (!empty($v['hidden'])) {
                unset($userTopMenu[$k]);
            }
        }
        return $userTopMenu;
    }

    /**
     * @param User $user
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getTopWorkbenchLinks(User $user)
    {
        $ordersInformer         = InformerModule::getInformer($user, CompanyOrderInformer::class);
        $instantPaymentInformer = InformerModule::getInformer($user, CompanyInstantPaymentInformer::class);
        $ordersInformerCount    = ($ordersInformer ? $ordersInformer->getCount() : 0) + ($instantPaymentInformer ? $instantPaymentInformer->getCount() : 0);
        $salesReviewInformer    = InformerModule::getInformer($user, ReviewInformer::class);

        $purchaseInformer       = InformerModule::getInformer($user, CustomerOrderInformer::class);
        $customerQuotesInformer = InformerModule::getInformer($user, CustomerQuoteInformer::class);
        $purchaseInformerCount  = ($purchaseInformer ? $purchaseInformer->getCount() : 0) + ($customerQuotesInformer ? $customerQuotesInformer->getCount() : 0);
        $earningInformer        = InformerModule::getInformer($user, EarningInformer::class);

        $userTopMenu = [
            [
                'link'   => [
                    '/workbench/service-orders'
                ],
                'title'  => _t("site", "My Sales") . ($ordersInformerCount ? ImageHtmlHelper::getRedDot($ordersInformerCount) : ''),
                'icon'   => 'tsi-check-list',
                'hidden' => !self::hasPs($user)
                //'hidden' => empty($newOrders)
            ],
            [
                'link'   => [
                    '/workbench/reviews'
                ],
                'title'  => _t('site', 'Sales Reviews') . ' ' . ($salesReviewInformer ? $salesReviewInformer->getAsRedDot() : ''),
                'icon'   => 'tsi-star',
                'hidden' => !self::hasPs($user),
            ],
            [
                'link'  => [
                    '/workbench/orders'
                ],
                'title' => _t('site', "My Purchases") . ' ' . ($purchaseInformerCount ? ImageHtmlHelper::getRedDot($purchaseInformerCount) : ''),
                'icon'  => 'tsi-bag',
            ],
            [
                'link'  => [
                    '/workbench/payments'
                ],
                'title' => _t('site', "Earnings") . ' ' . ($earningInformer ? $earningInformer->getAsRedDot() : ''),
                'icon'  => 'tsi-money-stack',
            ],
            [
                'link'   => [
                    AffiliateUrlHelper::sourcesList($user->company)
                ],
                'title'  => _t('site', "Affiliate"),
                'icon'   => 'tsi-structure-2',
                'hidden' => self::hasPs($user),
            ]
        ];

        foreach ($userTopMenu as $k => $v) {
            if (!empty($v['hidden'])) {
                unset($userTopMenu[$k]);
            }
        }
        return $userTopMenu;
    }

    public static function getTopMenuLinks(User $user)
    {
        $newOrdersHtml = $unreadedHtml = "";
        $newOrders     = StoreOrderService::getNewOrdersCount($user);
        if ($newOrders) {
            $newOrdersHtml = sprintf(" <span class='badge'> %d</span>", $newOrders);
        }
        $userTopMenu = [
            [
                'link'   => [
                    '/mybusiness/company'
                ],
                'title'  => _t("site", "My Services"),
                'icon'   => 'tsi-blocks-1',
                'hidden' => !self::hasPs($user)
            ],
            [
                'link'   => [
                    '/workbench/service-orders'
                ],
                'title'  => _t("site", "My Sales") . $newOrdersHtml,
                'icon'   => 'tsi-check-list',
                'hidden' => !self::hasPs($user)
                //'hidden' => empty($newOrders)
            ],
            [
                'link'   => UserUtils::getUserPublicProfileRoute($user),
                'title'  => _t("site", "Public Profile"),
                'icon'   => 'tsi-group',
                'hidden' => !self::hasPs($user)
            ],
            [
                'link'  => [
                    '/workbench/orders'
                ],
                'title' => _t('site', "My Purchases"),
                'icon'  => 'tsi-bag',
                // 'hidden' => !self::hasOrders($user)
            ],
            [
                'link'   => [
                    '/my/store'
                ],
                'title'  => _t("site", "Models"),
                'icon'   => 'tsi-cube',
                'hidden' => !self::hasStore($user)
            ],/*
            [
                'link'  => [
                    '/user/profile'
                ],
                'title' => _t("site", "Profile"),
                'icon'  => 'tsi-user'
            ],*/
            [
                'link'   => [
                    '/my/store/index'
                ],
                'title'  => _t("site", "My Uploads"),
                'icon'   => 'tsi-archive-out',
                'hidden' => !(!self::hasStore($user) && self::hasUploads($user)) //!self::hasCollections($user)
            ],
            [
                'link'   => [
                    '/profile/collection'
                ],
                'title'  => _t("site", "Wish List"),
                'icon'   => 'tsi-stack',
                'hidden' => !self::hasCollections($user)
            ],
            /*
        [
            'link'  => [
                '/my/messages'
            ],
            'title' => _t("site", "Messages") . $unreadedHtml,
            'icon'  => 'tsi-message'
        ]*/
        ];

        foreach ($userTopMenu as $k => $v) {
            if (!empty($v['hidden'])) {
                unset($userTopMenu[$k]);
            }
        }
        // app('cache')->set('user'.$user->id. '.usertopmenu', $userTopMenu, 70); // 2min.
        return $userTopMenu;
    }

    /**
     * get top user menu
     *
     * @return array
     */
    public static function getGuestMenu()
    {
        $loginUrl    = \yii\helpers\Url::toRoute('/user/login');
        $signUpLink  = sprintf(
            '<span class="header-bar__logo"></span>' . '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse">' . '<span class="sr-only">Toggle navigation</span>' . '<span class="icon-bar"></span>' . '<span class="icon-bar"></span>' . '<span class="icon-bar"></span>' . '</button>' . '<span class="header-bar__clear"></span>' .

            '<li><a class="btn btn-danger" href="#" onclick="TS.Visitor.signUpForm();return false;">%s</a></li>' . '<li class="header-bar__menu-or">or</li>',
            $loginUrl,
            _t('front', 'Sign up')
        );
        $loginLink   = sprintf('<li><a class="btn btn-danger btn-ghost" href="#" onclick="TS.Visitor.loginForm();return false;">%s</a></li>', $loginUrl, _t('front', 'Sign in'));
        $menuItems   = [];
        $menuItems[] = $signUpLink;
        $menuItems[] = $loginLink;

        return $menuItems;
    }

    /**
     * user name to display on top
     *
     * @param User $user
     * @param boolean $full - give full name if has
     * @return string
     */
    public static function getFormattedUserName(User $user, $full = false)
    {
        if (!empty($user->userProfile) && !empty(trim($user->userProfile->firstname))) {
            $username = $user->userProfile->firstname;
        } elseif (!empty($user->userProfile) && !empty(trim($user->userProfile->full_name))) {
            $username = explode(" ", $user->userProfile->full_name)[0];
            if ($full) {
                $username = $user->userProfile->full_name;
            }
        } else {
            $username = $user->username;
            if (empty($username) || $user->can_change_username) {
                // get part from email
                $username = substr($user->email, 0, strpos($user->email, '@'));
            }
        }
        $username = $full ? $username : substr($username, 0, 12);
        return $username;
    }


    /**
     *
     * @param \yii\db\ActiveRecord $object
     * @param User|null $user
     * @param UserSession|null $userSession
     * @return boolean
     */
    public static function isObjectOwner(ActiveRecord $object, $user = null, $userSession = null)
    {
        if ($user === null) {
            $ownerId = self::getCurrentUserId();
        } else {
            $ownerId = $user->id;
        }
        if ($object->hasAttribute('user_session_id')) {
            if ($userSession === null) {
                $userSession = self::getUserSession();
            }
            if ($object->user_session_id === $userSession->id) {
                return true;
            }

            if ($object->user_id === null || $object->user_id === User::USER_ANONIM) {
                return true;
            }
        }
        return isset($object->user_id) && $ownerId === $object->user_id;
    }

    /**
     *
     * @param \yii\db\ActiveRecord $object
     * @param string $error
     * @throws \yii\base\UserException
     */
    public static function tryCheckObjectOwner(\yii\db\ActiveRecord $object, $error = "No access")
    {
        if (!self::isObjectOwner($object)) {
            throw new \yii\base\UserException($error);
        }
    }

    /**
     * get current measurement, users or from session
     *
     * @return string
     */
    public static function getCurrentMeasurement()
    {
        return \frontend\components\UserSessionFacade::getMeasurement();
    }

    /**
     * get current currency
     *
     * @return string
     */
    public static function getCurrentCurrency()
    {
        return \frontend\components\UserSessionFacade::getCurrency();
    }


    /**
     * mark user as model store, after publishing
     *
     * @param \common\models\base\User $user
     * @return bool
     */
    public static function markModelStore(\common\models\base\User $user)
    {
        if (!$user->userProfile) {
            return false;
        }
        // already shop
        if (!empty($user->userProfile->is_modelshop)) {
            return true;
        }
        $user->userProfile->is_modelshop = 1;
        return $user->userProfile->save();
    }

    /**
     * mark user as print service
     *
     * @param \common\models\base\User $user
     * @return boolean
     */
    public static function markPrintService(\common\models\base\User $user)
    {
        if (!$user->userProfile) {
            return false;
        }
        if (!empty($user->userProfile->is_printservice)) {
            return true;
        }
        $user->userProfile->is_printservice = 1;
        return $user->userProfile->save();
    }

    /**
     * get user statistics object
     *
     * @param \common\models\base\User $user
     * @return \common\models\base\UserStatistics
     */
    public static function getUserStatistics(\common\models\base\User $user)
    {
        $userStatObj = $user->userStatistics;
        if ($userStatObj == null) {
            return null;
        }
        return $userStatObj;
    }

    /**
     * get status by last online time
     *
     * @param \common\models\base\UserStatistics $userStats
     * @return string
     */
    public static function getUserOnlineStatus(\common\models\base\UserStatistics $userStats)
    {
        return self::getUserLastOnlineByDate($userStats->lastonline_at);
    }

    public static function getUserLastOnlineByDate($lastOnlineAt)
    {
        $now = new \DateTime();
        $UTC = new \DateTimeZone("UTC");
        $now->setTimezone($UTC);
        if ($lastOnlineAt instanceof Expression) {
            $minutes = 1;
            $date2   = null;
        } else {
            $date2   = new \DateTime($lastOnlineAt, $UTC);
            $date    = $date2->diff($now);
            $minutes = $date->days * 24 * 60;
            $minutes += $date->h * 60;
            $minutes += $date->i;
        }
        if ($minutes < 3) {
            $status = _t('site.user', 'Online');
        } else {
            if ($minutes < 10) {
                $status = _t('site.user', 'Away');
            } else {
                if ($minutes < 60) {
                    $status = _t(
                        'front.site',
                        'Last online {minutes} minutes ago',
                        [
                            'minutes' => $minutes
                        ]
                    );
                } else {
                    $status = _t('site.user', 'Last online ') . ' ' . app('formatter')->asDate($date2);
                }
            }
        }
        return $status;
    }

    /**
     * get user link to public profile
     *
     * @param User $user
     * @param
     *            $userName
     * @return string
     */
    public static function getUserLink($user, $userName)
    {
        $href = Html::a($userName, UserUtils::getUserPublicProfileRoute($user));
        return $href;
    }

    /**
     * Check user added tax information and submitted it.
     *
     * @param User $user
     * @return boolean
     */
    public static function hasTaxInfo(User $user)
    {
        if (empty($user->userTaxInfo)) {
            return false;
        }
        return true;
    }

    /**
     * get tax message information based on user data
     *
     * @param User $user
     * @return string
     */
    public static function getTaxMessage($user)
    {
        $msgTax = "";
        $link   = Html::a(
            _t('site.user', 'Open'),
            [
                'my/taxes'
            ]
        );
        if (UserFacade::hasTaxInfo($user) && $user->userTaxInfo->status != \common\models\UserTaxInfo::STATUS_NEW) {
            if ($user->userTaxInfo->status == \common\models\UserTaxInfo::STATUS_DECLINE) {
                $msgTax = _t("site.user", "Sorry, you cannot perform this operation. Your tax information has been declined.");
            } else {
                if ($user->userTaxInfo->status != \common\models\UserTaxInfo::STATUS_CONFIRMED) {
                    $msgTax = _t(
                        "site.user",
                        "Your tax information is being confirmed by the site's administrator. You will not be able to withdraw any money until your tax form has been reviewed."
                    );
                }
            }
            if (!empty($user->userTaxInfo->address)) {
                \frontend\models\site\DenyCountry::checkCountry($user->userTaxInfo->address->country->iso_code);
            }
            \frontend\models\site\DenyCountry::checkCountry($user->userTaxInfo->place_country);
        } else {
            if (!UserFacade::hasTaxInfo($user)) {
                $msgTax = _t(
                    "site.error",
                    "We will withdraw 30% of order amount for taxes purposes because you didn't fill Tax information. Please fill Tax Information. {link}",
                    [
                        'link' => $link
                    ]
                );
            } else {
                // $msgTax = _t("site.user", "Your Tax Information is not completely submitted. Please submit tax information for review. {link}", ['link' => $link]);
                $msgTax = _t(
                    "site.user",
                    "Please fill in tax form. You will not be able to withdraw money till your tax form will be reviewed. We will withdraw 30% of order amount for taxes purposes because you didn't fill Tax information. {link}",
                    [
                        'link' => $link
                    ]
                );
            }
        }
        return $msgTax;
    }

    /**
     * get default license, or get user license if can, or get from session
     *
     * @return int
     */
    public static function getDefaultLicenseId()
    {
        $licenseId = app('setting')->get('model3d.default_license_id', 1);
        if (is_guest()) {
        } else {
            /**
             *
             * @var \common\models\User $user
             */
            $user = app('user')->getIdentity();
            if ($user && $user->userProfile && $user->userProfile->default_license_id) {
                $licenseId = $user->userProfile->default_license_id;
            }
        }
        return (int)$licenseId;
    }

    /**
     * Create change email request, send email
     *
     * @param ChangeEmailForm $form
     */
    public static function createChangeEmailRequest(ChangeEmailForm $form)
    {
        $user    = $form->getUser();
        $request = ChangeEmailRequest::create($user, $form->email);

        $emailer = new \common\components\Emailer();
        $emailer->sendCreateChangeEmailRequest($user, $request);
        $emailer->sendConfirmChangeEmailRequest($user, $request);
    }

    /**
     * Apply change remail request
     *
     * @param User $user
     * @param ChangeEmailRequest $request
     * @throws ChangeEmailException
     */
    public static function applyChangeEmailRequest(User $user, ChangeEmailRequest $request)
    {
        AssertHelper::assert($user->id == $request->user_id, _t('site.user', 'Bad change email request'), ChangeEmailException::class);
        AssertHelper::assert($request->status == ChangeEmailRequest::STATUS_UNCONFIRMED, _t('site.user', 'Bad change email request'), ChangeEmailException::class);
        #AssertHelper::assert($user->getIsActive(), _t("site.user", "Can't change email on unconfirmed or unactive user"), ChangeEmailException::class);

        // check that other user with this email not exist
        // this was validate on create request, but it could changed
        if (User::find()->where(
            [
                'email' => $request->email
            ]
        )->exists()
        ) {
            throw new ChangeEmailException(_t('site.user', 'User with same email already exist'));
        }

        $request->confirm();
        $oldEmail    = $user->email;
        $user->email = $request->email;
        AssertHelper::assertSave($user);

        \common\models\UserLog::log($user->id, 'email', $oldEmail, $user->email);
    }

    /**
     * get model count published for current user
     *
     * @param \common\models\base\User $user
     * @return int
     */
    public static function getStoreModelsCount(\common\models\base\User $user)
    {
        $count = Model3d::find()
            ->user($user)
            ->published()
            ->count();
        return $count;
    }

    /**
     * @param \common\models\base\User $user
     * @throws \yii\base\UserException
     */
    public static function cancelRequestToDeleteUser(\common\models\base\User $user)
    {
        $userRequest = UserRequest::findOne(
            [
                'user_id'      => $user->id,
                'request_type' => UserRequest::REQUEST_TYPE_DELETE
            ]
        );
        if ($userRequest->status == UserRequest::STATUS_NEW) {
            $userRequest->delete();
            User::updateRecord($userRequest->user_id, ['status' => User::STATUS_ACTIVE]);
        } else {
            throw new \yii\base\UserException("Operation cannot be cancelled. If you think this is an error, please contact us");
        }
    }

    /**
     * @param \common\models\base\User $user
     * @return bool
     */
    public static function requestToDeleteUser(\common\models\base\User $user)
    {
        // self::validateDelete($user);
        $userRequest = UserRequest::findOne(
            [
                'user_id'      => $user->id,
                'request_type' => UserRequest::REQUEST_TYPE_DELETE
            ]
        );
        if ($userRequest) {
            $userRequest->code   = md5(time() . mt_rand(1, 1000));
            $userRequest->status = UserRequest::STATUS_NEW;
            $userRequest->safeSave();
        } else {
            UserRequest::addRecord(
                [
                    'user_id'      => $user->id,
                    'moderator_id' => null,
                    'created_at'   => dbexpr("NOW()"),
                    'updated_at'   => dbexpr("NOW()"),
                    'status'       => UserRequest::STATUS_NEW,
                    'request_type' => UserRequest::REQUEST_TYPE_DELETE,
                    'code'         => md5(time() . mt_rand(1, 1000))
                ]
            );
        }

        $user->status = User::STATUS_UNACTIVE;
        $user->safeSave();
        return true;
    }

    /**
     * @TODO - delete?
     *
     * @param \common\models\User $user
     * @return boolean
     */
    public static function requestToRestore(User $user)
    {
        $userRequest = UserRequest::findOne(
            [
                'user_id'      => $user->id,
                'request_type' => UserRequest::REQUEST_TYPE_RESTORE
            ]
        );
        if ($userRequest) {
            return true;
        }
        UserRequest::addRecord(
            [
                'user_id'      => $user->id,
                'moderator_id' => null,
                'created_at'   => dbexpr("NOW()"),
                'updated_at'   => dbexpr("NOW()"),
                'status'       => UserRequest::STATUS_NEW,
                'request_type' => UserRequest::REQUEST_TYPE_RESTORE
            ]
        );
        return true;
    }

    /**
     *
     * @param \common\models\base\UserRequest $userRequest
     * @return boolean
     * @throws \yii\base\UserException
     */
    public static function confirmRestore(\common\models\base\UserRequest $userRequest)
    {
        $dbtr = app('db')->beginTransaction();
        try {
            $user = $userRequest->user;
            // printers
            $ps = \common\models\Ps::findOne(
                [
                    'user_id' => $user->id
                ]
            );
            if ($ps) {
                \common\models\Ps::updateRecord(
                    $ps->id,
                    [
                        'is_active' => 1
                    ]
                );
                $printers = \common\models\PsPrinter::findAll(
                    [
                        'ps_id' => $ps->id
                    ]
                );
            }

            // models
            /** @var Model3d[] $models */
            $models = \common\models\Model3d::find()->with(
                [
                    'storeUnit'
                ]
            )
                ->where(
                    [
                        'user_id' => $user->id
                    ]
                )
                ->all();
            foreach ($models as $k => $model) {
                $model->is_active = 1;
                if ($model->storeUnit) {
                    \common\models\StoreUnit::updateRecord(
                        $model->storeUnit->id,
                        [
                            'status'    => \common\models\StoreUnit::STATUS_NEW,
                            'is_active' => 1
                        ]
                    );
                }
                $model->safeSave();
            }

            // profile
            User::updateRecord(
                $user->id,
                [
                    'status' => User::STATUS_ACTIVE
                ]
            );

            UserRequest::updateRecord(
                $userRequest->id,
                [
                    'status' => UserRequest::STATUS_REVERT
                ]
            );
            $dbtr->commit();
        } catch (\yii\base\UserException $e) {
            $dbtr->rollBack();
            throw $e;
        } catch (\Exception $ex) {
            $dbtr->rollBack();
            logException($ex, 'tsdebug');
            throw new \yii\base\UserException(
                _t(
                    "site.error",
                    "Server error [{time}]. Please contact us to find out problem",
                    [
                        'time' => time()
                    ]
                )
            );
        }
        return true;
    }

    /**
     * if secure page, remove any remote links to js,css and etc.
     * hide analytics and etc.
     *
     * @return boolean
     */
    public static function isSecuredPage()
    {
        $request    = app('request');
        $currentUrl = $request instanceof \yii\web\Request ? $request->url : $request->params[0];

        if (strpos($currentUrl, 'my/taxes') !== false) {
            return true;
        }
        return false;
    }

    /**
     * checks if current session requires analytics
     *
     * @return boolean
     */
    public static function needAnalytics()
    {
        /*
        $cookie = \Yii::$app->getRequest()
            ->getCookies()
            ->getValue('tsbackend_admin', false);

        if ($cookie || !empty($_COOKIE['tsbackend_admin'])) {
            return false;
        }
        if (self::isSecuredPage()) {
            return false;
        }
        if (YII_ENV == 'dev') {
            return false;
        }
        if (strpos($_SERVER['HTTP_HOST'], '.vcap.me') !== false) {
            return false;
        }
        if (strpos($_SERVER['HTTP_HOST'], '.tsdev.work') !== false) {
            return false;
        }
        if (strpos($_SERVER['HTTP_HOST'], 'test.treatstock.com') !== false) {
            return false;
        }
        if (strpos($_SERVER['HTTP_HOST'], 'beta.treatstock.com') !== false) {
            return false;
        }
        if (strpos($_SERVER['HTTP_HOST'], 'sandbox.treatstock.com') !== false) {
            return false;
        }
        $userIp  = \frontend\components\UserSessionFacade::getSessionIp();
        $ips     = app('setting')->get('site.analyticoff', "127.0.0.1\n192.168.66.1");
        $ipsList = explode("\n", $ips);
        foreach ($ipsList as $k => $ip) {
            $ip = trim($ip);
            if ($userIp == $ip) {
                return false;
            }
            $ipsList[$k] = $ip;
        }
        $gatrackId = app('setting')->get('site.ga_analytics', false);
        if (!$gatrackId) {
            return false;
        }
        */
        $location = UserSessionFacade::getLocation();
        if (in_array($location?->country, ['AT', 'NL', 'FR'])) {
            return false;
        }

        return true;
    }

    /**
     * can current user inline translate
     *
     * @return boolean
     */
    public static function canTranslate()
    {
        $translatorId  = app('setting')->get('site.translator');
        $translatorIds = explode(",", $translatorId);
        $translatorIds = array_map('trim', $translatorIds);

        $currentUserId = \frontend\models\user\UserFacade::getCurrentUserId();

        if ($currentUserId > 1 && param('server') == 'https://test.treatstock.com') { // for now just for all in test
            return true;
        }
        if ($currentUserId > 1 && strpos(param('server'), '.tsdev.work') !== false) { // for now just for all in test
            return true;
        }

        if ($currentUserId > 1 && in_array($currentUserId, $translatorIds)) {
            return true;
        }
        return false;
    }

    /**
     * Return anonim user
     *
     * @return User
     */
    public static function getAnonimUser()
    {
        return User::tryFind(User::USER_ANONIM);
    }

    /**
     * @param User $user
     * @param int $size
     *
     * @return string
     */
    public static function getSalesAvatarUrl(User $user, $size = 64): string
    {
        if ($user->company) {
            return $user->company->getCompanyLogo(false, $size) ?? '';
        }
        return UserUtils::getAvatarForUser($user, 'url');
    }

    /**
     * @param User $user
     *
     * @return string
     */
    public static function getSalesTitle(User $user): string
    {
        if ($user->company) {
            return $user->company->title;
        }

        return self::getFormattedUserName($user);
    }

    public static function getFormattedBusinessOrUserName(User $user)
    {
        if ($user->company) {
            return $user->company->getTitle();
        }
        return self::getFormattedUserName($user);
    }

    /**
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public static function isCurrentUserAdmin(): bool
    {
        $user = static::getCurrentUser();
        return $user && $user->id < 1000;
    }

}

