<?php

/**
 * Change user password form model
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
namespace frontend\models\user;

class ChangePasswordForm extends \yii\base\Model
{

    public $password;

    public $newPassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'password',
                    'newPassword'
                ],
                'required'
            ],
            [
                [
                    'password',
                    'newPassword'
                ],
                'string',
                'min' => 6
            ]
        ];
    }

    /**
     * change password
     *
     * @return type
     */
    public function attributeLabels()
    {
        return [
            'password' => _t('front.user', 'Current password'),
            'newPassword' => _t('front.user', 'New password')
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function changePassword()
    {        
        $userId = \Yii::$app->user->identity->id;
        /** @var FrontUser $user */
        $user = FrontUser::findOne(['id'=>$userId]);
       
        if(!$user->validatePassword($this->password)){
            return false;
        }
        $psw = $this->newPassword;
        $user->setPassword($psw);
        $user->removePasswordResetToken();
        $result = $user->save(false);
        if ($result) {
            $emailer = new \common\components\Emailer();
            $emailer->sendPasswordChanged($user, $psw);
        }
        return $result;
    }
}
