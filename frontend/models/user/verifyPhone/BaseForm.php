<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */
namespace frontend\models\user\verifyPhone;

use common\components\validators\CoutryIsoValidator;
use common\components\validators\DenyCountryValidator;
use common\components\validators\PhoneValidator;
use common\models\User;
use yii\base\Model;

/**
 * Class BaseForm
 * 
 * @package frontend\models\ps\verifyPhone
 */
abstract class BaseForm extends Model
{

    /**
     * Phone number
     * 
     * @var string
     */
    public $phone;

    /**
     * Phone country is code
     * 
     * @var string
     */
    public $phone_country_iso;

    /**
     *
     * @var User
     */
    protected $user;

    /**
     *
     * @param User $user            
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [
            [
                [
                    'phone',
                    'phone_country_iso'
                ],
                'required'
            ],
            [
                'phone_country_iso',
                CoutryIsoValidator::class
            ],
            [
                'phone_country_iso',
                DenyCountryValidator::class
            ],
            [
                'phone',
                PhoneValidator::class,
                'phoneCountryIsoAttribute' => 'phone_country_iso',
                'correctPhone' => true,
                'when' => function () {
                    return ! $this->hasErrors();
                }
            ]
        ];
    }

    /**
     *
     * @return string
     * @throws \lib\geo\GeoNamesException
     */
    protected function getPhoneCode()
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $numberObj = $phoneUtil->parse($this->phone, $this->phone_country_iso);
        return $numberObj->getCountryCode();
    }

    /**
     *
     * @return string
     */
    protected function getFullPhone()
    {
        return $this->getPhoneCode() . $this->phone;
    }

    /**
     * get code confirm message
     * 
     * @param string $code            
     * @return string
     */
    protected static function getCodeConfirmMessage($code)
    {
        return "Treatstock verification code: {$code}";
    }

    /**
     *
     * @return mixed
     */
    abstract public function process();
}