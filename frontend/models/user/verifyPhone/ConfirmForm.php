<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */
namespace frontend\models\user\verifyPhone;

use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\models\Ps;
use common\models\UserSms;
use frontend\models\ps\PsFacade;

/**
 * Class ConfirmForm
 * 
 * @package frontend\models\ps\verifyPhone
 */
class ConfirmForm extends BaseForm
{

    /**
     *
     * @var string
     */
    public $verifyCode;

    /**
     *
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['verifyCode', 'required'],
            [
                'verifyCode',
                'validateVerifyCode',
                'when' => function () {
                    return ! $this->hasErrors();
                }
            ]
        ]);
    }

    /**
     * Validate verify code
     */
    public function validateVerifyCode()
    {
        if (!$this->getConfirmSms($this->verifyCode)) {
            $this->addError('verifyCode', _t('site.ps', 'Wrong verification code.'));
        }
    }

    /**
     * Return confirm sms
     *
     * @param string $code
     * @return UserSms|null Null if not found
     */
    private function getConfirmSms($code)
    {
        /** @var UserSms $sms */
        $sms = UserSms::find()->forPhoneNumber($this->getFullPhone())
            ->andWhere([
                'user_id' => $this->user->id,
                'status' => UserSms::STATUS_ACCEPTED,
                'type' => UserSms::TYPE_CONFIRM,
                'message' => self::getCodeConfirmMessage($code)
            ])
            ->andWhere([
                '>', 'created_at', DateHelper::subNow('P1D')
            ])
            ->latest();
        return $sms;
    }

    /**
     * Confirm phone
     * 
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function process()
    {
        $confirmSms = $this->getConfirmSms($this->verifyCode);
        $confirmSms->status = UserSms::STATUS_CONFIRMED;
        AssertHelper::assertSave($confirmSms);
        
        // TODO move it to right place or move verify phone to user settings
        if (($ps = PsFacade::getPsByUserId($this->user->id)) && $ps->phone_status != Ps::PHONE_STATUS_CHECKED && $ps->phone == $this->phone && $ps->phone_country_iso == $this->phone_country_iso) {
            $ps->phone_status = Ps::PHONE_STATUS_CHECKED;
            $ps->sms_gateway = $confirmSms->gateway;
            AssertHelper::assertSave($ps);
        }
    }
}