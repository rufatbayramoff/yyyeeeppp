<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace frontend\models\user\verifyPhone;

use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\models\Ps;
use common\models\User;
use common\models\UserSms;

/**
 * Class SendVerifyPhoneForm
 *
 * @package frontend\models\ps\verifyPhone
 * @todo move to operation
 */
class SendForm extends BaseForm
{

    protected function checkSendPeriod(User $user)
    {
        $userSms = UserSms::find()->andWhere(['user_id' => $user->id])->andWhere(['>', 'created_at', DateHelper::subNowSec(60)])->one();
        if ($userSms) {
            throw new BusinessException(_t('site.ph', 'Too often sms send. Wait for 60 sec.'));
        }
    }

    protected function getNextSmsGateWay(User $user): string
    {
        $smsGateways = \Yii::$app->smsGatewaysBundle->getGatewaysNames();
        $userSms     = UserSms::find()->andWhere(['user_id' => $user->id])->orderBy('created_at desc')->one();
        if (!$userSms) {
            return reset($smsGateways);
        }
        $lastGateway = array_search($userSms->gateway, $smsGateways);
        $nextGateway = $lastGateway + 1;
        if ($nextGateway >= count($smsGateways)) {
            $nextGateway = 0;
        }
        return $smsGateways[$nextGateway];
    }

    /**
     * Send sms with verify code
     *
     * @throws BusinessException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \lib\geo\GeoNamesException
     */
    public function process()
    {
        $company                    = $this->user->company;
        $company->phone_status      = Ps::PHONE_STATUS_CHECKING;
        $company->phone_code        = $this->getPhoneCode() . '';
        $company->phone_country_iso = $this->phone_country_iso;
        $company->phone             = $this->phone;
        AssertHelper::assertSave($company);

        $this->checkSendPeriod($this->user);
        $code       = rand(1000, 9999);
        $message    = self::getCodeConfirmMessage($code);
        $smsGateway = $this->getNextSmsGateWay($this->user);
        try {
            UserSms::send($smsGateway, $this->user->id, $this->getFullPhone(), $message, UserSms::TYPE_CONFIRM);
        } catch (\Exception $e) {
            throw new BusinessException($e->getMessage());
        }

    }
}