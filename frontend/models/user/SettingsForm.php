<?php
namespace frontend\models\user;

/**
 * user settings form
 *
 * - metrics
 * - currency [usd,rub,etc]
 * - timezone
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class SettingsForm extends \common\models\UserProfile
{

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_UPDATE, function ($event) {
            $attributes = $event->changedAttributes;
            foreach ($attributes as $k => $v) {
                \common\models\UserLog::log($this->user_id, $k, $v, $this->$k);
            }
        });
    }

    public function rules()
    {
        return [
            [
                [
                    'current_currency_iso',
                    'current_metrics',
                    'current_lang',
                    'default_license_id',
                    'timezone_id'
                ],
                'required'
            ],
            [
                [
                    'timezone_id',
                    'gender'
                ],
                'safe'
            ]
        ];
    }

    /**
     * override labels
     *
     * @return type
     */
    public function attributeLabels()
    {
        return [
            'default_collection_id' => _t('front.user', 'Default Collection'),
            'default_license_id' => _t('front.user', 'Default License'),
            'timezone_id' => _t('front.user', 'Timezone'),
            'current_currency_iso' => _t('front.user', 'Currency'),
            'current_lang' => _t('front.user', 'Language'),
            'current_metrics' => _t('front.user', 'Measurement'),
            'gender' => _t('front.user', 'Gender')
        ];
    }

    /**
     * checks if language changed in profile, change site sesion language
     */
    public function checkLanguageChange()
    {
        if ($this->current_lang != \Yii::$app->session["lang"]) {
            \Yii::$app->session["lang"] = $this->current_lang;
            \Yii::$app->language = $this->current_lang;
            return true;
        }
        return false;
    }

    /**
     * get timezones
     * 
     * @return type
     */
    public function getTimezones()
    {
        // $abbr = \lib\time\Timezone::getAbbr();
        $list = \lib\time\Timezone::getList();
        foreach ($list as $k => $v) {
            $title = $v . ' (' . \lib\time\Timezone::getOffset($v) . ')';
            $result[$v] = $title;
        }
        return $result;
    }

    /**
     * get active langs in system_lang table
     * 
     * @return type
     */
    public function getLangs()
    {
        $langs = \common\models\SystemLang::find()->where('is_active=1')
            ->asArray()
            ->all();
        $langs = \yii\helpers\ArrayHelper::map($langs, 'iso_code', 'title_original');
        return $langs;
    }

    /**
     *
     * @return type
     */
    public function getCurrency()
    {
        $currency = \common\models\PaymentCurrency::find()->where('is_active=1')
            ->asArray()
            ->all();
        $currency = \yii\helpers\ArrayHelper::map($currency, 'currency_iso', 'title_original');
        return $currency;
    }
}   
