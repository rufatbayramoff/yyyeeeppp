<?php

namespace frontend\models\user;

use backend\modules\company\services\CompanyUserService;
use common\components\DateHelper;
use common\components\exceptions\BusinessException;
use common\models\User;
use common\modules\company\services\CompanyService;
use Yii;
use yii\helpers\Html;

/**
 * Login form
 */
class LoginForm extends \common\models\User
{

    public $email;

    public $password;

    public $rememberMe;

    /**
     * Redirect to after login
     *
     * @var string
     */
    public $redirectTo;


    protected $user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
            ['redirectTo', 'string'],
        ];
    }

    /**
     *
     * @return type
     */
    public function attributeLabels()
    {
        return [
            'email'      => _t('front.user', 'Email'),
            'password'   => _t('front.user', 'Password'),
            'rememberMe' => _t('front.user', 'Remember Me')
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute
     *            the attribute currently being validated
     * @param array $params
     *            the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || $user->password_hash == '-' || !$user->validatePassword($this->password)) {
                $this->addError(
                    $attribute,
                    _t('front.site', 'Incorrect email or password.')
                //_t('front.site', 'Incorrect email or password. <br>Forgot your password?') . ' ' . Html::a(_t('front.site', 'Reset here'), ['user/forgot-password'])
                );

                file_put_contents(
                    '/tmp/failedLogin.txt',
                    DateHelper::now() . ', failed login IP:' . $_SERVER['REMOTE_ADDR'] . ', email:' .
                    str_replace([' ', "\n"], '', $this->email) . "\n",
                    FILE_APPEND
                );
                sleep(3);
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     * @throws BusinessException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function login()
    {
        $userInf = FrontUser::find()->where(['email' => $this->email])->one();
        /** @var CompanyService $companyService */
        $companyService = \Yii::createObject(CompanyService::class);
        if ($userInf && $companyService->isCompanyCreatedUser($userInf)) {
            $this->addError('email', 'This user alreay exists, please confirm your email in <a href="'.$userInf->company->getPublicCompanyLink().'">company profile</a>');
            return false;
        }
        $logData = [
            'result'  => 'ok',
            'user_id' => 0
        ];
        if (!empty($userInf)) {
            $logData['user_id'] = $userInf->id;
        }
        $res = false;
        if ($this->validate()) {
            $user = $this->getUser();
            if ($user->status == \common\models\User::STATUS_DELETED) {
                throw new BusinessException("Your account is not active. If you think this is an error, please contact us.");
            }
            if (empty($user->auth_key)) {
                $user->auth_key = \Yii::$app->security->generateRandomString();
            }
            $res = Yii::$app->user->login($user, $this->rememberMe ? param('cookieExpire') : 0);
            // save last login
            $user->lastlogin_at = time();
            $user->safeSave();
            $logData['user_id'] = $user->id;
        } else {
            $logData['result'] = 'failed';
        }

        UserFacade::logLogin($logData);
        return $res;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->user === false) {
            $this->user = FrontUser::findByEmail($this->email);
        }

        return $this->user;
    }
}
