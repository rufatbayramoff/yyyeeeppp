<?php
namespace frontend\models\user;

class CollectionForm extends \common\models\UserCollection
{

    public function rules()
    {
        return [
            [
                [
                    'user_id',
                    'title'
                ],
                'required'
            ],
            [
                [
                    'user_id',
                    'is_active',
                    'is_visible'
                ],
                'integer'
            ],
            [
                [
                    'title'
                ],
                'string',
                'max' => 45
            ],
            [
                [
                    'description'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'title'
                ],
                'unique',
                'targetAttribute' => [
                    'title',
                    'user_id'
                ],
                'message' => _t('front.user', 'This Title has already been used.')
            ]
        ];
    }
}