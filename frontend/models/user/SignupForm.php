<?php

namespace frontend\models\user;

use common\models\User;
use common\models\UserEmailLogin;
use common\modules\captcha\services\CaptchaService;
use common\modules\company\services\CompanyService;
use yii\base\Model;
use yii\helpers\Html;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $email;

    public $password;

    /**
     * Redirect to after login
     *
     * @var string
     */
    public $redirectTo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'email',
                'filter',
                'filter' => 'trim'
            ],
            [
                'email',
                'required'
            ],
            [
                'email',
                'email'
            ],
            [
                'email',
                'unique',
                'targetClass' => User::class,
                'filter' => [
                    '<>',
                    'status',
                    User::STATUS_DRAFT
                ],
                'message'     => _t('front.site', 'This email address has already been taken.<br>If it is your account, you can {resetPassword}.', [
                    'resetPassword' => Html::a(_t('front.site', 'reset the password'), ['/user/forgot-password'])
                ])
            ],
            [
                'password',
                'required'
            ],
            [
                'password',
                'string',
                'min' => 6
            ],
            ['redirectTo', 'string'],
        ];
    }


    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email'      => _t('front.user', 'E-mail'),
            'password'   => _t('front.user', 'Password'),
            'rememberMe' => _t('front.user', 'Remember Me')
        ];
    }

    /**
     * Signs user up.
     *
     * @return FrontUser|null the saved model or null if saving fails
     */
    public function signup()
    {
        $userInf = FrontUser::find()->where(['email' => $this->email])->one();
        /** @var CompanyService $companyService */
        $companyService = \Yii::createObject(CompanyService::class);
        if ($userInf && $companyService->isCompanyCreatedUser($userInf)) {
            $this->addError('email', 'This user already exists, please confirm your email in <a href="'.$userInf->company->getPublicCompanyLink().'">company profile</a>');
            return null;
        }
        $captchaService  = \Yii::createObject(CaptchaService::class);
        $captchaService->checkGoogleRecaptcha2Response();
        if ($this->validate()) {
            $emailLogin  = UserEmailLogin::findOne(['email' => $this->email]);
            if ($userInf && $userInf->isStatusDraft() && $emailLogin) {
                $userInf->setPassword($this->password);
                $this->sendConfirmEmail($userInf);
                return null;
            }
            $user = UserFacade::createUser([
                'email'    => $this->email,
                'password' => $this->password
            ]);
            if ($user) {

                // send email to confirm signup and email
                $userEmail = clone $user;
                $emailer = new \common\components\Emailer();
                $emailer->sendSignupEmail($user, $this->password);
                UserFacade::logLogin([
                    'user_id'    => $user->id,
                    'result'     => 'ok',
                    'login_type' => 'osn'
                ]);
                return $user;
            }
        }
        return null;
    }

    /**
     * @param FrontUser $frontUser
     */
    public function sendConfirmEmail(FrontUser $frontUser): void
    {
        $confirm = new ChangeEmailForm($frontUser);

        if($confirm->changeEmail()){
            $this->addError('email',  _t('front.site', 'Confirm email to continue registration. Check you email.'));
        } else {
            $this->addError('email', _t('front.site', 'Sorry, we are unable to reset password for email provided.'));
        }
    }
}
