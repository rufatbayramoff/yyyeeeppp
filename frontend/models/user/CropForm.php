<?php
/**
 * Created by mitaichik
 */

namespace frontend\models\user;


use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Image\Point;
use yii\base\Model;

class CropForm extends Model
{
    /**
     * Start x of crop image
     * @var int
     */
    public $x;

    /**
     * Start y of crom image
     * @var int
     */
    public $y;

    /**
     * Width of crop image since x
     * @var int
     */
    public $width;

    /**
     * Height of crip image since y
     * @var int
     */
    public $height;

    /**
     * CropForm constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct();
        $this->setAttributes($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['x', 'y', 'width', 'height'], 'required'],
            [['x', 'y', 'width', 'height'], 'integer']
        ];
    }

    /**
     * Adapt crop data to image.
     * On client side crop plugin can recevie bad data, for example $x may be -1, or width may be large that real image size
     * It becouse plugin have some float point calcuation.
     * In this method we adapt probably bad crop data to real image
     * @param BoxInterface $imageSize
     */
    public function adapt(BoxInterface $imageSize)
    {
        $this->x = max(0, $this->x);
        $this->y = max(0, $this->y);

        $this->width = $this->width + $this->x <= $imageSize->getWidth()
            ? $this->width
            : $imageSize->getWidth() - $this->x;

        $this->height = $this->height + $this->y <= $imageSize->getHeight()
            ? $this->height
            : $imageSize->getHeight() - $this->y;
    }

    /**
     * @return Point
     */
    public function getCropStartPoint()
    {
        return new Point($this->x, $this->y);
    }

    /**
     * @return Box
     */
    public function getCropBox()
    {
        return new Box($this->width, $this->height);
    }

}