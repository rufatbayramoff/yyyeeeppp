<?php

namespace frontend\models\user;

use common\models\GeoCountry;
use lib\geo\models\Location;

/**
 * UserLocator - locator helps to locate user address using API
 * API result in json is parsed to object with loadDetails function
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserLocator extends \common\components\BaseForm
{

    public $address;

    public $addressApi;

    public $lat;

    public $lon;

    public $country;

    public $state;

    public $city;

    public $street;

    public $streetNumber;

    public $postalCode;

    public $formattedAddress;

    public function rules()
    {
        return [
            [
                [
                    'country',
                    'city'
                ],
                'required'
            ],
            [
                [
                    'lat',
                    'lon',
                    'country',
                    'city',
                    'state',
                    'street',
                    'streetNumber',
                    'postalCode',
                    'formattedAddress',
                    'addressApi'
                ],
                'safe'
            ]
        ];
    }

    public function toString()
    {
        return $this->city . ", " . $this->country;
    }

    public function load($data, $formName = null)
    {
        $result = parent::load($data, $formName);
        if ($result) {
            if ($this->addressApi) {
                $this->loadDetails($this->addressApi);
            } elseif ($this->country && !$this->city) {
                $this->loadCountryCapital();
            }
        }
        return $result;
    }

    /**
     * load address details by api json result
     *
     * @param string $addressJson
     */
    public function loadDetails($addressJson)
    {
        $obj                    = \yii\helpers\Json::decode($addressJson);
        $fromApi                = [
            'country'      => 'country',
            'state'        => 'administrative_area_level_1',
            'city'         => 'locality',
            'postalCode'   => 'postal_code',
            'street'       => 'route',
            'streetNumber' => 'street_number',
            'lat'          => 'lat',
            'lon'          => 'lon'
        ];
        $this->formattedAddress = $obj['formatted_address'];
        if (!empty($obj['administrative_area_level_1'])) {
            $obj['administrative_area_level_1'] = trim($obj['administrative_area_level_1'], "'- ");
            $obj['administrative_area_level_1'] = str_replace(['/', '"', "'"], '-', $obj['administrative_area_level_1']);
        }

        foreach ($fromApi as $key => $jsonKey) {
            if (isset($obj[$jsonKey])) {
                if (in_array($key, ['city', 'state', 'country'])) {
                    $this->$key = trim($obj[$jsonKey], "'- ");
                } else {
                    $this->$key = $obj[$jsonKey];
                }
            }
        }
    }

    public function loadCountryCapital()
    {
        $geoCountry = GeoCountry::tryFind(['iso_code' => $this->country]);
        $capital    = $geoCountry->capital;
        $this->city = $capital->title;
        $this->lat  = $capital->lat;
        $this->lon  = $capital->lon;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function getLon()
    {
        return $this->lon;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param Location $location
     * @return UserLocator
     */
    public static function createByLocation(Location $location)
    {
        $locator          = new UserLocator();
        $locator->lat     = $location->lat;
        $locator->lon     = $location->lon;
        $locator->country = $location->country;
        $locator->state   = $location->region;
        $locator->city    = $location->city;
        return $locator;
    }
}
