<?php
namespace frontend\models\user;

use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{

    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'email',
                'filter',
                'filter' => 'trim'
            ],
            [
                'email',
                'required'
            ],
            [
                'email',
                'email'
            ],
            [
                'email',
                'exist',
                'targetClass' => '\common\models\User',
                'filter'      => [
                    'and',
                    [
                        'status' => [
                            FrontUser::STATUS_ACTIVE,
                            FrontUser::STATUS_UNCONFIRMED,
                            FrontUser::STATUS_UNACTIVE
                        ]
                    ],
                    'id>1000'
                ],
                'message'     => _t('front.site', 'There is no user with this email.')
            ]
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user FrontUser */
        $user = FrontUser::findOne(
            [
                'status' => [
                    FrontUser::STATUS_ACTIVE,
                    FrontUser::STATUS_UNCONFIRMED,
                    FrontUser::STATUS_UNACTIVE
                ],
                'email'  => $this->email
            ]
        );

        if ($user) {
            if (!FrontUser::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }
            if ($user->save()) {
                $emailer = new \common\components\Emailer();
                $emailer->sendPasswordReset($user);
                return true;
            }
        }

        return false;
    }
}
