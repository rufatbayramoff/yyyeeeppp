<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */
namespace frontend\models\user;

use common\components\exceptions\AssertHelper;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use common\models\User;
use common\models\UserDocument;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadDocumentForm
 * 
 * @package frontend\models\user
 */
class UploadDocumentForm extends Model
{

    /**
     *
     * @var User
     */
    private $user;

    /**
     *
     * @var UploadedFile
     */
    public $documentFile;

    /**
     *
     * @param User $user            
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        parent::__construct([]);
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [
            [
                'documentFile',
                'required'
            ],
            [
                'documentFile',
                'file',
                'extensions' => 'png, jpg, jpeg, gif'
            ]
        ];
    }

    /**
     * Load data from post
     */
    public function loadFromPost()
    {
        $this->documentFile = UploadedFile::getInstance($this, 'documentFile');
    }

    /**
     * Process form
     *
     * @throws \Exception
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function process()
    {
        $fileFactory = Yii::createObject(FileFactory::class);
        $fileRepository = Yii::createObject(FileRepository::class);
        $file = $fileFactory->createFileFromUploadedFile($this->documentFile);
        $fileRepository->save($file);

        $doc = new UserDocument();
        $doc->user_id = $this->user->id;
        $doc->file_id = $file->id;

        AssertHelper::assertSave($doc);
    }

    /**
     *
     * @return string
     */
    public function formName()
    {
        return '';
    }
}