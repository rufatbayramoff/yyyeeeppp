<?php

namespace frontend\models\user;

use common\components\exceptions\AssertHelper;
use common\components\FileDirHelper;
use common\models\base\User;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Yii;
use yii\base\Model;
use yii\base\UserException;
use yii\web\UploadedFile;

/**
 * avatar form limits
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class ChangeAvatarForm extends Model
{
    /**
     * file size limit
     *
     * @var int
     */
    private $maxSize = 1500000;
    
    /**
     * file extension
     *
     * @var array
     */
    private $extension = 'jpg,jpeg,png,gif';

    /**
     * Crop information
     * @var CropForm
     */
    public $crop;

    /**
     *
     * @var UploadedFile
     */
    public $file;

    /**
     * rules and limits for uploaded file
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['file', 'required'],
            ['file', 'file', 'checkExtensionByMimeType' => false, 'extensions' => $this->extension, 'maxSize' => $this->maxSize],
        ];
    }

    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * @param array $data
     */
    public function loadFromPost(array $data)
    {
        AssertHelper::assert($this->load($data));
        $this->crop = new CropForm($data['crop']);
        $this->file = UploadedFile::getInstance($this, 'file');
        AssertHelper::assertValidate($this);
        AssertHelper::assertValidate($this->crop);
    }

    /**
     * @throws UserException
     */
    public function process()
    {
        $user = UserFacade::getCurrentUser();

        $avatarUrl = $this->saveFile($user);

        if(!$avatarUrl) {
            throw new UserException(_t('site.profile', "Can't save image"));
        }

        $oldValue = $user->userProfile->avatar_url;
        $user->userProfile->avatar_url = $avatarUrl;
        AssertHelper::assert($user->userProfile->save(false));
        \common\models\UserLog::log($user->id, 'avatar_url', $oldValue, $avatarUrl);

        Yii::$app->getSession()->setFlash('success', _t('front', 'Avatar updated'));
    }

    /**
     * @param User $user
     * @return string
     */
    public function saveFile(User $user)
    {
        $image = \yii\imagine\Image::getImagine()->open($this->file->tempName);
        $this->crop->adapt($image->getSize());
        $image->crop($this->crop->getCropStartPoint(), $this->crop->getCropBox());

        $subFolder = \frontend\models\user\UserFacade::getUserFolder($user->id);
        $folder = \Yii::getAlias(\Yii::$app->params['uploadPath']);
        $timestamp = time();
        $fileExtension = $this->file->extension??'jpg';
        $filePath = "/$subFolder/avatar_{$timestamp}.{$fileExtension}";
        FileDirHelper::createDir($folder . '/' . $subFolder);
        array_map('unlink', glob($folder . '/' . $subFolder . '/avatar*'));

        $image->save($folder . $filePath);

        return $filePath;
    }
}
