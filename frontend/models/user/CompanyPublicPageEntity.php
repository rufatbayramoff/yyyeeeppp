<?php
/**
 * User: nabi
 */

namespace frontend\models\user;


use backend\models\search\ProductSearch;
use common\models\Company;
use common\models\CompanyService;
use common\models\CuttingMachine;
use common\models\message\helpers\UrlHelper;
use common\models\Product;
use common\models\Ps;
use frontend\components\UserUtils;
use frontend\models\review\StoreOrderReviewSearch;
use Yii;
use yii\helpers\Url;

class CompanyPublicPageEntity
{
    public $psId;
    public $userLink;
    public $bgUrl;
    public $avatarUrl;
    public $psTitle;
    public $printersCount = 0;
    public $storeModelsCount = 0;
    public $productsCount = 0;
    public $sendMessageUrl;
    public $storeUrl;
    public $psUrl;
    public $reviewsCount = 0;
    // CNC
    public $cncCount = 0;
    public $cncUrl;
    // Cutting
    public $cuttingCount;
    public $cuttingUrl;

    public $printServiceUrl;
    public $companyServicesCount;

    public $ps;

    private function __construct(){
    }

    /**
     * @param Company $ps
     * @return CompanyPublicPageEntity
     */
    public static function fill(Company $ps)
    {
        $page = new CompanyPublicPageEntity();
        $page->ps = $ps;
        $user = $ps->user;
        $userProfile = $user->userProfile;
        $page->psId = $ps->id;
        $page->psUrl = '/c/' . $ps->url;
        $page->cncUrl = '/c/' . $ps->url . '/cnc';
        $page->printServiceUrl = '/c/' . $ps->url . '/3d-printing-service';
        $page->cuttingUrl = '/c/' . $ps->url . '/cutting';
        $page->storeUrl = '/c/' . $ps->url . '/3d-printable-models-store';
        $page->bgUrl = $ps->coverFile ? $ps->coverFile->getFileUrl() : UserUtils::getCover(null);
        //$page->avatarUrl = UserUtils::getAvatar($userProfile->avatar_url, $user->email);

        $page->avatarUrl = Ps::getCircleImageByPs($ps, 100, 100);
        $page->psTitle  = $ps->title;

        /** @var StoreOrderReviewSearch $storeOrderReviewSearch */
        $storeOrderReviewSearch        = Yii::createObject(StoreOrderReviewSearch::class);
        $storeOrderReviewSearch->forPs = $ps;

        $reviewsProvider = $storeOrderReviewSearch->search([]);

        $page->reviewsCount = $reviewsProvider->getTotalCount();

        $page->printersCount = $ps->getNotDeletedPrinters()->visible()->moderated()->count();
        $page->cncCount = $ps->getCncByAvailability(CompanyService::AVAILABILITY_STATUS_PUBLIC) ? 1 : 0;
        $page->cuttingCount = CuttingMachine::find()->company($ps)->availableForOffers()->count();

        $productSearch = new ProductSearch();

        $query = $productSearch->generateSearchQuery([
            'productSearch' => [
                'company_id' => $ps->id,
                'is_active' => 1
            ]
        ]);

        $query
            ->joinWith('productCommon.company')
            ->andWhere(['=', 'ps.moderator_status', Ps::MSTATUS_CHECKED])
            ->andWhere(['=', 'ps.is_deleted', Company::IS_NOT_DELETED_FLAG])
            ->andWhere(['!=', 'product_common.product_status', Product::STATUS_REJECTED]);

        $page->productsCount = $query->count();
       # debugSql( $ps->getNotDeletedPsMachines()->moderated()); exit;
        $page->companyServicesCount = $ps->getNotDeletedPsMachines()->byType([CompanyService::TYPE_SERVICE, CompanyService::TYPE_WINDOW])->moderated()->count();

        $page->storeModelsCount = \common\models\Model3d::find()->storePublished($user)->count();

        $page->sendMessageUrl  = Url::toRoute(
            array_merge(
                UrlHelper::personalMessageRoute($user),
                ['utm_source' => 'treatstock', 'utm_medium' => 'public-profile', 'utm_campaign' => 'hire_designer', 'utm_content' => 'send_msg']
            )
        );

        return $page;
    }

    public  function getTotalServiceCount(){
        return $this->printersCount + $this->companyServicesCount + $this->productsCount;
    }

    public function url()
    {
        return $this->ps->url;
    }
}