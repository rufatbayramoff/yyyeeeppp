<?php

namespace frontend\models\user;

use common\components\exceptions\AssertHelper;
use common\models\user\ChangeEmailRequest;
use Yii;

/**
 * ChangeEmailForm - change email with confirmation link
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class ChangeEmailForm extends \common\components\BaseForm
{

    /**
     * email
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var FrontUser
     */
    private $user;

    /**
     * email was already changed recently
     */
    const EMAIL_CHANGED_COOKIE = 'changeEmail_cookie1';

    /**
     * when user can change his email again
     * current 5 minutes
     */
    const CHANGE_TIMEOUT = 300;

    /**
     *
     * @param FrontUser $user            
     * @param array $config            
     */
    public function __construct(FrontUser $user, $config = [])
    {
        parent::__construct($config);
        $this->user = $user;
        $this->email = $user->email;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [
                'email',
                'filter',
                'filter' => 'trim'
            ],
            [
                'email',
                'required'
            ],
            [
                'email',
                'string',
                'max' => 45
            ],
            [
                'email',
                'email'
            ],
            
            // timeoute validator
            [
                'email',
                function () {
                    if (\Yii::$app->getRequest()
                        ->getCookies()
                        ->getValue(self::EMAIL_CHANGED_COOKIE) == '1') {
                        $this->addError('email', _t('front.user', 'E-mail was recently changed . Please try again after {timeout} minutes.', [
                            'timeout' => intval(self::CHANGE_TIMEOUT / 60)
                        ]));
                    }
                }
            ],
            
            // same email validator
            [
                'email',
                function () {
                    if ($this->user->email == $this->email) {
                        $this->addError('email', _t('front', 'Please specify new e-mail.'));
                    }
                }
            ],
            
            // unque email validator
            [
                'email',
                'unique',
                'targetClass' => '\common\models\User',
                'message' => _t('front.user', 'This email address has already been taken.')
            ],
            
            // already have request validator
            [
                'email',
                function () {
                    if ($this->user->getIsActive()) {
                        $isRequestExist = ChangeEmailRequest::find()->forUser($this->user)
                            ->unconfirmed()
                            ->byEmail($this->email)
                            ->exists();
                        
                        if ($isRequestExist) {
                            $this->addError('email', _t('front.user', 'Request to change to this email already exists and is awaiting confirmation.'));
                        }
                    }
                }
            ]
        ];
    }

    /**
     * change current user's email
     *
     * @return boolean
     */
    public function changeEmail()
    {
        $oldValue = $this->user->email;
        $this->user->email = $this->email;
        $this->user->auth_key = Yii::$app->getSecurity()->generateRandomString();

        AssertHelper::assert($this->user->save(false));
        \common\models\UserLog::log($this->user->id, 'email', $oldValue, $this->user->email);
        
        $emailer = new \common\components\Emailer();
        $emailer->sendConfirmEmail($this->user);
        
        $this->markChanged();
    }

    /**
     * add cookie - email was changed, deny to change by CHANGE_TIMEOUT
     */
    public function markChanged()
    {
        $cookie = new \yii\web\Cookie([
            'name' => self::EMAIL_CHANGED_COOKIE,
            'value' => '1',
            'expire' => time() + self::CHANGE_TIMEOUT,
            'path' => '/'
        ]);
        \Yii::$app->getResponse()
            ->getCookies()
            ->add($cookie);
    }

    /**
     * Getter fot user
     * 
     * @return FrontUser
     */
    public function getUser()
    {
        return $this->user;
    }
}