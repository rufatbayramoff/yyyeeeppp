<?php
namespace frontend\models\user;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserAccountForm extends \common\models\UserProfile
{
    
    public $username;
    
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['website', 'url_facebook'], 'url', 'defaultScheme' => 'http'];
        $rules[] = [['username'], 'string', 'max'=>50];
        $rules[] = [['is_hidden_public_page'], 'string', 'max'=>1];
        $rules[] = [['info_about', 'info_skills'], 'string', 'max'=>1500];
        return $rules;
    }
    /**
     *
     * @param string $username            
     */
    public function validateUsername($username)
    {
        $denyUsernames = (array) \Yii::$app->setting->get('user.disallowusernames');
        if (in_array($username, $denyUsernames)) {
            $this->addError('username', _t('front', 'Username incorrect. This username is denied.'));
        }
        if (! preg_match('/^[A-Za-z0-9_\-\.]*$/', $username)) {
            $this->addError('username', _t('front', 'Username must contain alphanumeric characters (letters A-Z, numbers 0-9). Cannot contain spaces or special characters except for _ - .'));
        }
        // check if id<name>
        if (preg_match("/^id[0-9]*$/", $username)) {
            $this->addError('username', _t('front', 'Username incorrect.  Please try another one.'));
        }
        // check for unique
        $userObj = \common\models\User::findOne(['username'=>$username]);
        $user = app('user')->getIdentity();
        if(!empty($userObj) && $user->id!=$userObj->id){
            $this->addError('username', _t('front', 'Username already exists. Please use another.'));
        }
    }
    
    public function validate($attributeNames = null, $clearErrors = true)
    {
        if(!empty($this->username)){
            $user = app('user')->getIdentity();
            if(!empty($user->can_change_username)){
                $this->validateUsername($this->username);
                $clearErrors = false;
            }
        }
        return parent::validate($attributeNames, $clearErrors);
    }
    
    public function save($runValidation = true, $attributeNames = null)
    {
        $user = app('user')->getIdentity();
        if(!empty($this->username) && !empty($user->can_change_username)){
            // change username only if diff
            if($user->username!=$this->username){
                \common\models\UserLog::log(
                    $this->user_id, 'username', $user->username, $this->username
                );
                $user->username = $this->username;
                $user->can_change_username = 0;
                $user->save(false);
            }
        }
        return parent::save($runValidation, $attributeNames);
    }
}
