<?php
namespace frontend\models\user;

use common\components\reject\BaseRejectForm;
use common\models\SystemReject;

/**
 * Class UserDeleteRequestForm
 * @package frontend\models\user
 */
class UserDeleteRequestForm extends BaseRejectForm
{
    public function attributeLabels()
    {
        return [
            'reasonId' => \_t('app', 'Reason'),
            'reasonDescription' => \_t('app', 'Reason description')
        ];
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::USER_DELETE_REQUEST;
    }
}
