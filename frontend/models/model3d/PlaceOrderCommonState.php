<?php
namespace frontend\models\model3d;

use common\models\AffiliateSource;
use common\models\DeliveryType;
use common\modules\cutting\models\PlaceCuttingOrderState;
use common\modules\product\models\ProductPlaceOrderState;
use frontend\models\delivery\DeliveryForm;

/**
 * Trait PlaceOrderCommonState
 * @package frontend\models\model3d
 * @mixin PlaceOrderState|ProductPlaceOrderState|PlaceCuttingOrderState
 */
trait PlaceOrderCommonState
{
    protected function saveLastDeliveryState($deliveryState): void
    {
        $sessionKey = self::SESSION_KEY . '_lastDelivery';
        \Yii::$app->asyncSession->set($sessionKey, $deliveryState);
    }


    public function getLastDeliveryState()
    {
        $sessionKey = self::SESSION_KEY . '_lastDelivery';
        $deliveryState = \Yii::$app->asyncSession->get($sessionKey, []);
        return $deliveryState;
    }

    public function setStateUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * Reset state, clear all status
     */
    public function resetState()
    {
        $state = $this->getState();
        unset($state['upload']);
        unset($state['orderId']);
        unset($state['print']);
        unset($state['promocode']);
        \Yii::$app->asyncSession->reset();
        $this->saveState($state);
    }

    public function setPromoCode($promocode)
    {
        $state = $this->getState();
        $state['promocode'] = $promocode;
        $this->saveState($state);
    }

    public function getPromoCode()
    {
        $state = $this->getState();
        return $state['promocode'] ?? '';
    }

    public function getDeliveryFormState(DeliveryForm $deliveryForm)
    {
        $state = $this->getState();
        if (array_key_exists('deliveryForm', $state)) {
            // Already was filled
            foreach ($state['deliveryForm'] as $name => $value) {
                $deliveryForm->$name = $value;
            }
        } else {
            // Use last
            $lastDeliveryState = $this->getLastDeliveryState();
            if ($lastDeliveryState && mb_strtolower($deliveryForm->country) == mb_strtolower($lastDeliveryState['country']) && mb_strtolower($deliveryForm->city) == mb_strtolower($lastDeliveryState['city'])) {
                foreach ($lastDeliveryState as $name => $value) {
                    if (!$deliveryForm->$name && $lastDeliveryState[$name]) {
                        $deliveryForm->$name = $value;
                    }
                }
                $deliveryForm->shipping = DeliveryType::STANDARD;
            }
        }
    }

    public function setDeliveryFormState(DeliveryForm $deliveryForm)
    {
        $state = $this->getState();
        foreach (get_object_vars($deliveryForm) as $key => $value) {
            if ($key === 'deliveryParams') {
                continue;
            }
            if (is_string($value)) {
                $state['deliveryForm'][$key] = $value;
            }
        }
        $this->saveLastDeliveryState($state['deliveryForm']);
        $this->saveState($state);
    }

    public function setAffiliateSource(?AffiliateSource $affiliateSource)
    {
        $state = $this->getState();
        $state['affiliate']['sourceUuid'] = (string)($affiliateSource->uuid??'');
        $this->saveState($state);
    }

    public function getAffiliateSource()
    {
        $state = $this->getState();
        $uuid = $state['affiliate']['sourceUuid'] ?? null;
        if (!$uuid) {
            return null;
        }
        return AffiliateSource::find()->where(['uuid' => $uuid])->one();
    }

    public function setActiveOfferItem($offerId)
    {
        $state = $this->getState();
        $state['offer']['activeOfferItemId'] = $offerId;
        $this->saveState($state);
    }

    public function getActiveOfferItem()
    {
        $state = $this->getState();
        return $state['offer']['activeOfferItemId'] ?? null;
    }

    public function setSortMode(string $sortMode)
    {
        $state = $this->getState();
        $state['offer']['sortMode'] = $sortMode;
        $this->saveState($state);
    }

    public function getSortMode()
    {
        $state = $this->getState();
        return  $state['offer']['sortMode'] ?? null;
    }
}

