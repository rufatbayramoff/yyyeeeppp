<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.09.17
 * Time: 11:14
 */

namespace frontend\models\model3d;

use common\components\exceptions\NoActiveModel3d;
use common\components\order\anonim\AnonimOrderHelper;
use common\interfaces\Model3dBaseInterface;
use common\models\AffiliateSource;
use common\models\factories\Model3dFactory;
use common\models\Model3d;
use common\models\repositories\Model3dRepository;
use common\models\StoreOrder;
use common\services\Model3dService;
use frontend\models\delivery\DeliveryForm;
use lib\MeasurementUtil;
use yii\base\Exception;

class PlaceOrderState implements PlaceOrderStateInterface
{
    use PlaceOrderStateTrait, PlaceOrderCommonState {
    }

    /** @var  Model3dService */
    public $model3dService;

    /** @var  Model3dFactory */
    public $model3dFactory;

    /** @var Model3dRepository */
    public $model3dRepository;

    public const SESSION_KEY = 'placeOrderState';

    public function injectDependencies(
        Model3dService $model3dService,
        Model3dFactory $model3dFactory,
        Model3dRepository $model3dRepository
    ) {
        $this->model3dService = $model3dService;
        $this->model3dFactory = $model3dFactory;
        $this->model3dRepository = $model3dRepository;
    }


    /**
     * @return Model3dBaseInterface
     * @throws \yii\web\GoneHttpException
     * @throws \HttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws NoActiveModel3d
     */
    public function getModel3d()
    {
        $state = $this->getState();
        $model3d = null;
        $model3dUid = $state['upload']['model3dUid'] ?? null;
        if ($model3dUid) {
            $model3d = $this->model3dRepository->getByUid($model3dUid);
            if (!$model3d->isActive()) {
                $this->model3dService->setActive($model3d);
            }
            $this->model3dService->tryCanBeOrderedByCurrentUser($model3d);
        } else {
            throw new NoActiveModel3d('No active model3dparts');
        }
        // Allow work with empty model
        $model3d->setScenario(Model3dBaseInterface::SCENARIO_EMPTY_MODEL);
        $model3d->isAnyTextureAllowed = $state['print']['model3dIsAnyTextureAllowed'] ?? $model3d->isAnyTextureAllowed;
        $state['upload']['model3dId'] = $model3d->id;
        $this->saveState($state);
        return $model3d;
    }

    public function getModel3dSourceDetails()
    {
        $state = $this->getState();
        return $state['sourceDetails']??[];
    }

    public function setModel3dSourceDetails($sourceDetails)
    {
        $state = $this->getState();
        $state['sourceDetails'] = $sourceDetails;
        $this->saveState($state);
    }

    /**
     * who started widget first
     *
     * @param $launcher
     */
    public function setLauncher($launcher)
    {
        $state = $this->getState();
        $state['launcher'] = $launcher;
        $this->saveState($state);
    }

    public function getLauncher()
    {
        $state = $this->getState();
        return $state['launcher'] ?? '';
    }

    public function clearModel3d()
    {
        $state = $this->getState();
        $state['upload']['model3dUid'] = null;
        $this->saveState($state);
    }

    public function setModel3d(Model3dBaseInterface $model3d)
    {
        $state = $this->getState();
        $state['upload']['model3dUid'] = $model3d->getUid();
        $this->saveState($state);
    }

    public function setPsIdOnly($psId): void
    {
        $state = $this->getState();
        $state['print']['psId'] = (int)$psId;
        $this->saveState($state);
    }

    public function getPsIdOnly()
    {
        $state = $this->getState();
        return $state['print']['psId'] ?? 0;
    }

    public function setPsPrinterIdOnly($psId): void
    {
        $state = $this->getState();
        $state['print']['psPrinterId'] = (int)$psId;
        $this->saveState($state);
    }

    public function getPsPrinterIdOnly()
    {
        $state = $this->getState();
        return $state['print']['psPrinterId'] ?? 0;
    }

    /**
     * @param bool $flag
     */
    public function setModel3dIsAnyTextureAllowed($flag)
    {
        $state = $this->getState();
        $state['print']['model3dIsAnyTextureAllowed'] = $flag;
        $this->saveState($state);
    }

    public function getScaleUnit()
    {
        $state = $this->getState();
        $state['upload']['scaleUnit'];
        return $state['upload']['scaleUnit'] ?? MeasurementUtil::MM;
    }

    public function setScaleUnit($scaleUnit)
    {
        $state = $this->getState();
        $state['upload']['scaleUnit'] = $scaleUnit;
        $this->saveState($state);
    }
}