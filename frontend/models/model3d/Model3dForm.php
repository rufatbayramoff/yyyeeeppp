<?php

namespace frontend\models\model3d;

use common\models\PrinterMaterialGroup;
use common\models\Model3d;
use common\models\Model3dPart;
use common\models\PrinterColor;
use yii\base\Model;

class Model3dForm extends Model
{
    const WIDGET_TYPE_PS_PRINTER = 'widget_type_ps_printer';
    const WIDGET_TYPE_VIEW_MODEL = 'widget_type_view_model';

    /**
     * View a model and buy in widget mode
     */
    public const WIDGET_TYPE_VIEW_MODEL_BUY_WIDGET = 'widget_type_view_model_buy_widget';

    const WIDGET_PS_PRINTER_COLORS_COUNT = 40;

    /**
     * @var Model3d
     */
    public $model3d = null;

    /**
     * @var int
     */
    public $selectedFotoramaFileId;

    /**
     * @var string
     */
    public $currentPrintersSort;

    /**
     * @param $fileId
     * @return bool
     */
    public function isImageFotoramaFileId($fileId)
    {
        $model3dImages = $this->model3d->model3dImgs;
        foreach ($model3dImages as $model3dImage) {
            if (($model3dImage->file_id) && ($model3dImage->file_id == $fileId)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     */
    public function setFirstModel3dPartAsFotoramaFileId()
    {
        $model3dParts = $this->model3d->getActiveModel3dParts();
        $firstFile = reset($model3dParts);
        $this->setCurrentSelectedFotoramaFileId($firstFile->file_id);
    }

    /**
     * @param $selectedFotoramaFileId
     */
    public function setCurrentSelectedFotoramaFileId($selectedFotoramaFileId)
    {
        $this->selectedFotoramaFileId = $selectedFotoramaFileId;
    }

    /**
     * @param $fileId
     * @param $materialGroupId
     * @param $colorId
     */
    public function setModelPartMaterialIdAndColorId($fileId, $materialGroupId, $colorId)
    {
        $printerMaterialGroup = PrinterMaterialGroup::tryFindByPk($materialGroupId);
        $printerColor = PrinterColor::tryFindByPk($colorId);
        /** @var Model3dPart[] $model3dParts */
        $model3dParts = $this->model3d->model3dParts;
        $model3dImages = $this->model3d->model3dImgs;
        if ($model3dTexture = $this->model3d->kitModel3dTexture) {
            $model3dTexture->isMarkForDelete = true;
            foreach ($model3dParts as $model3dPart) {
                if (!$model3dPart->model3dTexture) {
                    $model3dPart->setMaterialGroupAndColor($printerMaterialGroup, $printerColor);
                }
            }
        }
        foreach ($model3dImages as $model3dImage) {
            if ($model3dImage->file_id === $fileId) {
                // Setted color for all parts
                foreach ($model3dParts as $model3dPart) {
                    $model3dPart->setMaterialGroupAndColor($printerMaterialGroup, $printerColor);
                }
            }
        }

        foreach ($model3dParts as $model3dPart) {
            if ($model3dPart->file_id === $fileId) {
                $model3dPart->setMaterialGroupAndColor($printerMaterialGroup, $printerColor);
            }
        }
    }

    /**
     * @param $getData
     */
    public function loadGetParamsColors($getData)
    {
        $model3dPart = $this->model3d->getLargestPrintingModel3dPart();
        if ($model3dPart) {
            $texture = $model3dPart->getCalculatedTexture();
            if (array_key_exists('printerMaterialGroupId', $getData)) {
                $printerMaterialGroup = PrinterMaterialGroup::tryFindByPk($getData['printerMaterialGroupId']);
                $texture->setPrinterMaterialGroup($printerMaterialGroup);
            }
            if (array_key_exists('printerColorId', $getData)) {
                $printerColor = PrinterColor::tryFindByPk($getData['printerColorId']);
                $texture->setPrinterColor($printerColor);
            }
            $this->model3d->setKitTexture($texture);
        }
    }

    /**
     * @param $postData
     */
    public function loadWidgetColors($postData)
    {
        if (array_key_exists('modelMaterial', $postData) && $modelMaterial = $postData['modelMaterial']) {
            // Setup color for all model
            $materialGroupId = $modelMaterial['materialGroupId'];
            $colorId = $modelMaterial['colorId'];
            if ($materialGroupId !== null && $colorId !== null) {
                $printerMaterialGroup = PrinterMaterialGroup::tryFindByPk($materialGroupId);
                $printerColor = PrinterColor::tryFindByPk($colorId);
                $this->model3d->setKitModelMaterialGroupAndColor($printerMaterialGroup, $printerColor);
            }
            return;
        }

        $filesMaterial = $postData['filesMaterial'] ?? null;
        $filesMaterial = $filesMaterial ?? (app('request')->post()['Model3dEditForm']['filesMaterial'] ?? null);

        if ($filesMaterial) {
            // Set color for model part
            foreach ($filesMaterial as $fileId => $materialInfo) {
                if (is_array($materialInfo) && array_key_exists('materialGroupId', $materialInfo)) {
                    $this->setModelPartMaterialIdAndColorId($fileId, $materialInfo['materialGroupId'], $materialInfo['colorId']);
                }
            }
        }
    }
}