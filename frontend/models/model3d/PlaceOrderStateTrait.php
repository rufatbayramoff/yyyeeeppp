<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.04.18
 * Time: 15:15
 */

namespace frontend\models\model3d;

use common\components\DateHelper;
use common\components\order\anonim\AnonimOrderHelper;
use common\models\StoreOrder;
use common\modules\product\models\ProductPlaceOrderState;
use common\modules\xss\helpers\XssHelper;
use frontend\models\model3d\PlaceOrderState;

/**
 * Trait PlaceOrderStateTrait
 *
 * @package common\models\model3d
 * @mixin PlaceOrderState|ProductPlaceOrderState
 */
trait PlaceOrderStateTrait
{
    public $uid = '';

    protected function saveState($state)
    {
        $currentValue = \Yii::$app->asyncSession->get(self::SESSION_KEY, []);
        $state['updateDate'] = DateHelper::now();
        $uid = $this->uid;
        if (defined('TEST_XSS') && TEST_XSS) {
            $uid = XssHelper::cleanXssProtect($uid);
        }
        $currentValue[$uid] = $state;
        \Yii::$app->asyncSession->set(self::SESSION_KEY, $currentValue);
    }

    protected function getState()
    {
        $currentValue = \Yii::$app->asyncSession->get(self::SESSION_KEY, []);
        $uid = $this->uid;
        if (defined('TEST_XSS') && TEST_XSS) {
            $uid = XssHelper::cleanXssProtect($uid);
        }
        return $currentValue[$uid] ?? [];
    }

    protected function checkUidExists($uid)
    {
        $currentValue = \Yii::$app->getSession()->get(self::SESSION_KEY, []);
        if (defined('TEST_XSS') && TEST_XSS) {
            $uid = XssHelper::cleanXssProtect($uid);
        }
        return array_key_exists($uid, $currentValue);
    }

    public function getLastUpdatedState()
    {
        $lastUid = null;
        $lastUpdateDate = null;
        $currentValue = \Yii::$app->getSession()->get(self::SESSION_KEY, []);
        foreach ($currentValue as $uuid => $oneValue) {
            if(empty($oneValue['updateDate'])){
                continue;
            }
            if ((!empty($oneValue['updateDate']) && $oneValue['updateDate'] && ($lastUpdateDate === null)) ||
                ($lastUpdateDate < $oneValue['updateDate'])) {
                $lastUpdateDate = $oneValue['updateDate'];
                $lastUid = $uuid;
            }
        }
        if ($lastUid === null) {
            return null;
        }
        $lastValue = $currentValue[$lastUid];
        $lastValue['uid'] = $lastUid;
        return $lastValue;
    }

    public function getStateUids()
    {
        $currentValue = \Yii::$app->getSession()->get(self::SESSION_KEY, []);
        return array_keys($currentValue);
    }

    public function setOrderId($orderId)
    {
        $state = $this->getState();
        $state['orderId'] = $orderId;
        $this->saveState($state);
    }

    /**
     * @return null|StoreOrder
     * @throws \yii\web\NotFoundHttpException
     */
    public function getOrder()
    {
        $state = $this->getState();
        $orderId = $state['orderId'] ?? null;
        if ($orderId) {
            $order = StoreOrder::tryFindByPk($orderId);
            AnonimOrderHelper::checkOrderAccess($order);
            return $order;
        }
        return null;
    }
}