<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.04.18
 * Time: 11:40
 */

namespace frontend\models\model3d;

interface PlaceOrderStateInterface
{
    public function getLastUpdatedState();
}