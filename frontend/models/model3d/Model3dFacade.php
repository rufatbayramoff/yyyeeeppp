<?php

namespace frontend\models\model3d;

use common\components\ArchiveManager;
use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\IpComparator;
use common\interfaces\FileBaseInterface;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\modules\product\interfaces\ProductInterface;
use common\models\File;
use common\models\Model3d;
use common\models\model3d\RenderInfo;
use common\models\Model3dImg;
use common\models\Model3dPart;
use common\models\Model3dReplica;
use common\models\Model3dTexture;
use common\models\PsPrinter;
use common\models\repositories\FileRepository;
use common\models\StoreUnit;
use common\modules\xss\helpers\XssHelper;
use common\services\Model3dPartService;
use console\jobs\model3d\AntivirusJob;
use console\jobs\model3d\ParserJob;
use console\jobs\model3d\RenderJob;
use console\jobs\QueueGateway;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\BaseObject;
use yii\base\NotSupportedException;
use yii\db\Expression;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * Model3d Facade class to connect functions to work
 * with model3d files, images, rendered images, 360, video and etc.
 *
 * @TODO - remove all render functions to \lib\render\RenderFacade
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class Model3dFacade extends BaseObject
{

    const TYPE_MODEL3D = 'model3d';

    const TYPE_IMAGE = 'image';

    const TYPE_ARCHIVE = 'archive';

    const TYPE_RENDER = 'render';

    const EMPTY_RENDER_IMAGE = '/images/3dicon.png';

    const DEFAULT_IMG_PREVIEW_EXTENSION = 'png'; // TODO: move to application config

    /**
     * get current color prefix, if empty returns empty, else with _ColorTitle
     *
     * @param Model3dTexture $model3dTexture
     * @return string
     */
    public static function getColorPrefix($model3dTexture)
    {
        if ($model3dTexture) {
            return '_color_' . $model3dTexture->printerColor->getRgbHex();
        } else {
            return '_color_ff0000';
        }
    }

    /**
     * Gets file type by file extension,
     * based on model3d.allowextension & model3d.allowscreens
     *
     * @param string $fileName
     * @param boolean $extended
     * @return string|array - model3d | image | file , if extended - returns as array
     */
    public static function getModel3dFileType($fileName, $extended = false)
    {
        $allowExt    = Model3dBasePartInterface::ALLOWED_FORMATS;
        $allowImgExt = Model3dBaseImgInterface::ALLOWED_FORMATS;
        if (\defined('TEST_XSS') && TEST_XSS) {
            $fileName = XssHelper::cleanXssProtect($fileName);
        }
        $fileInfo = pathinfo($fileName);
        if (!isset($fileInfo['extension'])) {
            \Yii::info(VarDumper::dumpAsString($fileInfo), 'job');
        }
        $ext  = mb_strtolower($fileInfo['extension']);
        $type = false;
        if (in_array($ext, $allowExt)) {
            $type = self::TYPE_MODEL3D;
        }
        if (in_array($ext, $allowImgExt)) {
            $type = self::TYPE_IMAGE;
        }
        if (in_array($ext, ArchiveManager::ARCHIVE_FORMATS)) {
            $type = self::TYPE_ARCHIVE;
        }
        if ($extended && $type !== false) {
            $type = array_merge(
                $fileInfo,
                [
                    'type' => $type
                ]
            );
        }
        return $type;
    }


    /**
     * Get model3d cover image to show in catalog and main image in model3d item view
     * check if images are bined to this model, and select first image in sorted
     * if no images, get rendered images and set cover first render image
     *
     * @param Model3dBaseInterface|Model3d $model3d
     * @param integer $coverFileId
     * @return array
     * @throws \Exception
     * @internal param null|string $preferColor
     * @internal param bool $initColor
     */
    public static function getCover(Model3dBaseInterface $model3d, $coverFileId = null)
    {
        $coverType   = self::TYPE_IMAGE;
        $coverFileId = $coverFileId ?: $model3d->cover_file_id;
        $coverImage  = null;

        $defaultCoverImage = Yii::$app->params['staticUrl'] . self::EMPTY_RENDER_IMAGE;

        $model3dParts = $model3d->getActiveModel3dParts();
        $model3dImgs  = $model3d->getActiveModel3dImages();
        if ($coverFileId && $cover = ArrayHelper::findAR($model3dParts, ['file_id' => $coverFileId])) {
            /** @var Model3dPart $cover */
            if (!Model3dPartService::checkAndRunFileRender($cover)) {
                $cover      = reset($model3dParts);
                $coverImage = Model3dPartService::getModel3dPartRenderUrlIfExists($cover);
                if (!$coverImage) {
                    $coverImage = $defaultCoverImage;
                }
            } else {
                $coverImage = Model3dPartService::getModel3dPartRenderUrlIfExists($cover);
            }
            $coverType = self::TYPE_MODEL3D;
        } elseif ($coverFileId && $cover = ArrayHelper::findAR($model3dImgs, ['file_id' => $coverFileId])) {
            /** @var Model3dImg $cover */
            $coverType  = self::TYPE_IMAGE;
            $coverImage = ImageHtmlHelper::getThumbUrlForFile($cover->file, ImageHtmlHelper::DEFAULT_WIDTH, ImageHtmlHelper::DEFAULT_HEIGHT);
        } else {
            //Yii::error('Possible ivalid database: model id: ' . $model3d->id . ', model cover file id: ' . $coverFileId . '. No found cover image in model3dfile or model3dimgs.');
            if ($model3dImgs) {
                $coverType  = self::TYPE_IMAGE;
                $cover      = reset($model3dImgs);
                $coverImage = ImageHtmlHelper::getThumbUrlForFile($cover->file, ImageHtmlHelper::DEFAULT_WIDTH, ImageHtmlHelper::DEFAULT_HEIGHT);
            } else if ($model3dParts) {
                $cover      = reset($model3dParts);
                $coverImage = Model3dPartService::getModel3dPartRenderUrlIfExists($cover);
                /** @var Model3dPart $cover */
                if (!$coverImage) {
                    $coverImage = $defaultCoverImage;
                    Model3dPartService::checkAndRunFileRender($cover);
                }
                $coverType = self::TYPE_MODEL3D;
            }
        }

        return [
            'file_id' => $coverFileId,
            'image'   => $coverImage,
            'type'    => $coverType,
        ];
    }


    /**
     * set model3d cover
     *
     * @param \common\models\base\Model3d|Model3d $model3dObj
     * @param $file
     * @return bool
     * @throws \yii\base\Exception
     */
    public static function setCover(\common\models\base\Model3d $model3dObj, $file)
    {
        $model3dObj->setCoverFile($file);
        return $model3dObj->safeSave();
    }

    /**
     * @param $model3dPart
     * @return string
     */
    public static function getModel3dPartRenderedImagePath(Model3dBasePartInterface $model3dPart)
    {
        $path = Model3dPartService::getModel3dPartRenderPath($model3dPart);
        if (file_exists($path)) {
            return $path;
        }
        return null;
    }


    /**
     * get rendered images for stl files
     * return array format [[filename=>'', url=>'']]
     *
     * @param Model3dBasePartInterface[] $model3dParts
     * @param Model3dBaseInterface|Model3d|Model3dReplica $model3d
     * @param bool $forceRenderIfNotExists
     * @return RenderInfo[]
     */
    public static function getRenderedImages($model3dParts, Model3dBaseInterface $model3d = null, $forceRenderIfNotExists = false)
    {
        $renders = [];
        foreach ($model3dParts as $model3dPart) {
            $row                 = new RenderInfo();
            $row->model3dId      = $model3dPart->model3d->id ? $model3dPart->model3d->id : $model3dPart->model3d->original_model3d_id;
            $row->isReplicaModel = $model3dPart->model3d->id ? $model3dPart->model3d instanceof Model3dReplica : false;

            $filePath = self::getModel3dPartRenderedImagePath($model3dPart);
            if ($filePath) {
                $row->filename = $model3dPart->file->id . '.' . self::DEFAULT_IMG_PREVIEW_EXTENSION; // TODO: what for we need filename?
                $row->url      = Model3dPartService::getModel3dPartRenderUrl($model3dPart);
                if ($model3d && $model3d->getCadFlag()) {
                    $row->url = $row->url . '?cae=' . $model3d->cae;
                }
            } else {
                $rJob = RenderJob::create($model3dPart->getOriginalModel3dPartObj()->file);
                $mode = QueueGateway::ADD_STRATEGY_UNIQUE;
                if ($forceRenderIfNotExists) {
                    $mode = QueueGateway::ADD_STRATEGY_ANYWAY;
                }
                QueueGateway::addFileJob($rJob, $mode);
                $row           = new RenderInfo();
                $row->filename = 'rendering.png';
                $row->url      = Yii::$app->params['staticUrl'] . self::EMPTY_RENDER_IMAGE;
            }

            $row->model3dPartId              = $model3dPart->getOriginalModel3dPartObj()->id;
            $row->fileId                     = $model3dPart->getOriginalModel3dPartObj()->file->id;
            $texture                         = $model3dPart->getCalculatedTexture();
            $row->printerColorId             = $texture->printerColor->id;
            $row->printerColorRgb            = $texture->printerColor->isMulticolor() && $model3dPart->isMulticolorFormat() ? 'multicolor' : $texture->printerColor->getRgbHex();
            $row->printerMaterialId          = $texture->printerMaterial ? $texture->printerMaterial->id : null;
            $row->printerMaterialGroupId     = $texture->calculatePrinterMaterialGroup() ? $texture->calculatePrinterMaterialGroup()->id : null;
            $row->caption                    = Model3dPartService::getModel3dPartCaption($model3dPart, true);
            $row->md5sum                     = $model3dPart->getOriginalModel3dPartObj()->file->md5sum;
            $row->fileExt                    = $model3dPart->file->getFileExtension();
            $row->renderUrlBase              = Yii::$app->getModule('model3dRender')->renderer->getRenderImageUrlBase($model3dPart->file);
            $row->isParsed                   = (bool)$model3dPart->getSize();
            $renders[$model3dPart->file->id] = $row;
        }
        return $renders;
    }

    /**
     * get all model collections filtered by user id
     *
     * @param Model3d $model3dObj
     * @param int $userId
     * @return \common\models\base\UserCollection[]
     */
    public static function getModelCollections(Model3d $model3dObj, $userId)
    {
        $modelCollections = $model3dObj->userCollectionModel3ds;
        if (count($modelCollections) == 0) {
            return [];
        }
        $collectionId = [];
        foreach ($modelCollections as $k => $v) {
            $collectionId[] = $modelCollections[$k]->collection_id;
        }
        $modelCollection = $modelCollections[0]->collection->findAll(
            [
                'user_id' => $userId,
                'id'      => $collectionId
            ]
        );
        return $modelCollection;
    }

    /**
     * delete screen.
     * delete from model3d_img table
     * mark as deleted in file table. use cron job to clean up after.
     *
     * @param int $screenId
     * @return bool
     * @throws \Exception
     */
    public static function deleteScreen($screenId)
    {
        /**
         *
         * @var Model3dImg $screenObj
         */
        $screenObj = Model3dImg::findByPk($screenId);
        if ($screenObj === null) {
            return false;
        }
        $res = false;
        if (UserFacade::isObjectOwner($screenObj)) {
            $fileObj = $screenObj->file;
            /**
             *
             * @var \common\models\base\Model3d $model3d
             */
            $model3d = $screenObj->getModel3d()->one();
            if ($fileObj->id === $model3d->cover_file_id) {
                throw new \Exception("Cannot delete cover image");
            }
            $screenObj->deleted_at = dbexpr('NOW()');
            $res                   = $screenObj->safeSave();

            $fileObj->status     = FileBaseInterface::STATUS_INACTIVE;
            $fileObj->deleted_at = new Expression('NOW()');
            $res                 = $res && $fileObj->update();
        }
        return $res;
    }


    /**
     * delete user model file
     *
     * @param Model3dPart $model3dPart
     * @return bool
     * @throws NotSupportedException
     * @deprecated use \common\services\Model3dService::deleteModel3dPart unstead
     */
    public static function deleteModelFile(Model3dPart $model3dPart)
    {
        if (!UserFacade::isObjectOwner($model3dPart->model3d)) {
            throw new NotSupportedException(_t('front.site', 'You cannot delete this model'));
        }
        $model3dPart->user_status = 'inactive';
        $model3dPart->file->setInactiveStatus();
        $model3dPart->file->safeSave();
        if ($model3dPart->fileSrc) {
            $model3dPart->fileSrc->setInactiveStatus();
            $model3dPart->fileSrc->safeSave();

        }
        return $model3dPart->save();
    }

    /**
     * Restore model part
     *
     * @param Model3dPart $part
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public static function restoreModelFile(Model3dPart $part)
    {
        $part->user_status = Model3dBasePartInterface::STATUS_ACTIVE;
        $part->file->setActiveStatus();
        $fileRepository = Yii::createObject(FileRepository::class);
        $fileRepository->save($part->file);
        if ($part->fileSrc) {
            $part->fileSrc->setActiveStatus();
            $fileRepository->save($part->fileSrc);
        }
        AssertHelper::assertSave($part);
    }

    /**
     * add jobs to queue if required: render, converter, parser
     * returns array of jobs with token.
     * all jobs are saved to file_job table
     * This function is called after 3D model file is uploaded first time.
     *
     * Two places of call - Upload file (new model), Upoad file (Edit Model)
     *
     * @param int|File $fileIdName
     * @param array $args
     * @throws \yii\base\UserException
     * @throws \console\jobs\JobException
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public static function createQueueJobs($fileIdName, $args = [])
    {
        /**
         * @var \common\models\File $file
         */
        $file      = is_object($fileIdName) ? $fileIdName : File::tryFindByPk($fileIdName);
        $sessionId = isset($args['session_id']) ? $args['session_id'] : null;

        // parse job  - render size is used.
        if (ParserJob::isCanRun($file)) {
            $job = ParserJob::create($file);
            QueueGateway::addFileJob($job);
        }

        // render job
        $job = RenderJob::create($file, $sessionId);
        QueueGateway::addFileJob($job);

        // antivirus job
        $job = AntivirusJob::create($file);
        QueueGateway::addFileJob($job);
    }

    public static function repeatAllJobs(Model3d $model3d)
    {
        foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
            $file = $model3dPart->file;
            $job  = ParserJob::create($file);
            QueueGateway::addFileJob($job, QueueGateway::ADD_STRATEGY_ANYWAY);

            $job = RenderJob::create($file);
            QueueGateway::addFileJob($job, QueueGateway::ADD_STRATEGY_ANYWAY);

            // antivirus job
            $job = AntivirusJob::create($file);
            QueueGateway::addFileJob($job, QueueGateway::ADD_STRATEGY_ANYWAY);
        }
    }

    /**
     * @param Model3d $model3d
     * @return array
     * @throws \Exception
     */
    public static function createQueueParserJobs(Model3d $model3d)
    {
        $jobs = [];
        foreach ($model3d->model3dParts as $model3dPart) {
            if (ParserJob::isCanRun($model3dPart->file)) {
                // parse job  - render size is used.
                $parserJob                   = ParserJob::create($model3dPart->file);
                $jobs[$parserJob->getCode()] = QueueGateway::addFileJob($parserJob);
            }
        }
        return $jobs;
    }

    /**
     * @param Model3d $model3d
     * @return array
     * @throws \Exception
     */
    public static function createQueueRenderJobs(Model3d $model3d)
    {
        $jobs = [];
        foreach ($model3d->model3dParts as $model3dPart) {
            $rJob = RenderJob::create($model3dPart->file);
            QueueGateway::addFileJob($rJob);

            $aJob = AntivirusJob::create($model3dPart->file);
            QueueGateway::addFileJob($aJob);
        }
        return $jobs;
    }

    /**
     * start render for all default colors after user publish a model
     *
     * @param $model3d
     * @return bool
     * @throws \Exception
     */
    public static function startRender($model3d)
    {
        $model3dParts = Model3dFacade::getActive($model3d->model3dParts);
        foreach ($model3dParts as $model3dPart) {
            Model3dPartService::checkAndRunFileRender($model3dPart);
        }
        return true;
    }

    /**
     * get model3d item object for store, or user view
     *
     * @param int $id
     * @return Model3d
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getCatalogItem($id)
    {
        $model3d = Model3d::tryFindByPk($id);
        $isOwner = UserFacade::isObjectOwner($model3d);
        if (!$isOwner) {
            self::updateViewsStatistic($model3d);
        }
        return $model3d;
    }

    /**
     * update view stats for user owner
     *
     * @param \common\models\base\Model3d $model3dObj
     */
    private static function updateViewsStatistic(\common\models\base\Model3d $model3dObj)
    {
        if (IpComparator::compare(UserSessionFacade::getSessionIp(), param('officeIp'))) {
            return;
        }

        if (!$model3dObj->user_id) {
            return;
        }

        $modelId = $model3dObj->id;
        $key     = 'model3dview';
        $ids     = !isset(app('request')->cookies[$key]) ? [] : explode(",", app('request')->cookies[$key]);

        if (!in_array($modelId, $ids)) {
            \common\models\UserStatistics::to($model3dObj->user_id)->up('views')->safeSave();
            array_push($ids, $modelId);
            app('response')->cookies->add(
                new \yii\web\Cookie(
                    [
                        'name'  => $key,
                        'value' => implode(",", $ids)
                    ]
                )
            );
            if (isset($model3dObj->stat_views)) {

                Model3d::updateAllCounters(['stat_views' => 1], ['id' => $model3dObj->id]);
            }
        }
        return;
    }


    /**
     * get url in store to model3d with slug
     *
     * @param Model3dBaseInterface $model3d
     * @param array $params
     * @param bool $scheme
     * @return string
     */
    public static function getStoreUrl(Model3dBaseInterface $model3d, array $params = [], bool $scheme = false)
    {
        $slug     = Inflector::slug($model3d->title);
        $modelId  = $model3d->getSourceModel3d()->id;
        $itemLink = Url::toRoute(array_merge(['/3d-printable-models/' . $modelId . '-' . $slug], $params));
        if ($scheme) {
            $itemLink = param('server') . $itemLink;
        }
        return $itemLink;
    }

    public static function getPrintUrl(Model3dBaseInterface $model3d, array $params = [])
    {
        return '/my/print-model3d?model3d=' . $model3d->getUid();
    }


    /**
     * Filter collection and get only active files
     *
     * @param \common\models\base\Model3dPart[]|\common\models\Model3dImg[] $collection
     * @return \common\models\Model3dPart[]
     * @todo add interface to used models
     */
    public static function getActive($collection)
    {
        $returnValue = [];
        foreach ($collection as $k => $obj) {  // TODO: Заменить строковые значения на константы
            if ((isset($obj['user_status']) && $obj->user_status === 'inactive') || (isset($obj['moderator_status']) && $obj->moderator_status === 'banned')) {
                // Do nothing
            } else {
                $returnValue[$obj->file_id] = $obj;
            }
        }
        return $returnValue;
    }

    /**
     * Restore models from deleted
     *
     * @param StoreUnit $storeUnit
     */
    public static function restoreModel(StoreUnit $storeUnit)
    {
        $model                                = $storeUnit->model3d;
        $model->productCommon->product_status = ProductInterface::STATUS_DRAFT;
        $model->productCommon->updated_at     = DateHelper::now();
        $model->productCommon->is_active      = 1;
        AssertHelper::assertSave($model->productCommon);
    }


    /**
     * @param Model3d $model3d
     * @return string
     */
    public static function getWaitCalculationString(Model3d $model3d)
    {
        return self::isErrorOnParser($model3d)
            ? _t('site.model3d', 'A calculating error occurred for this model.')
            : _t('site.store', 'Please wait for model weight calculation to complete...');
    }

    /**
     * @param Model3dBaseInterface $model
     * @return bool
     */
    public static function isErrorOnParser(Model3dBaseInterface $model)
    {
        foreach ($model->getActiveModel3dParts() as $part) {
            if ($part->getWeight() === 0 && ParserJob::isFailed($part->file)) {
                return true;
            }
        }
        return false;
    }


    public static function isIgesOrStep($ext)
    {
        return in_array($ext, [
            Model3dBasePartInterface::STEP_FORMAT,
            Model3dBasePartInterface::IGES_FORMAT,
            Model3dBasePartInterface::STEP_FORMAT_2,
            Model3dBasePartInterface::IGES_FORMAT_2,
        ]);
    }


    /**
     * Is printer can print model.
     * Check only texture and size
     *
     * @param PsPrinter $printer
     * @param Model3dBaseInterface $model
     * @return bool
     */
    public static function isCanPrintModelByTextureAndSize(PsPrinter $printer, Model3dBaseInterface $model): bool
    {
        foreach ($model->getCalculatedModel3dParts() as $part) {

            if (!$printer->canPrintTextures([$part->getCalculatedTexture()])) {
                return false;
            }

            if (!$printer->canPrintSize($part->getSize())) {
                return false;
            }
        }
        return true;
    }

    public static function getUrlModel3dReplica(Model3dReplica $model3dReplica): string
    {
        return Url::toRoute(['/store/model3d-replica', 'id' => $model3dReplica->id]);
    }
}
