<?php
namespace frontend\models\model3d;

use common\components\exceptions\AssertHelper;
use common\models\Model3d;
use common\models\User;
use common\models\UserCollection;
use common\models\UserCollectionModel3d;
use common\models\UserStatistics;
use frontend\models\user\UserFacade;
use yii\base\Exception;

/**
 *
 * Models collection facade
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class CollectionFacade
{

    /**
     * current user id - to check collection owner and etc.
     *
     * @var int
     */
    public static $currentUserId;

    /**
     * limit number of images used in collection cover animation
     *
     * @var int
     */
    private static $coverImgLimit = 5;

    /**
     *
     * @param int $userId            
     */
    public static function init($userId)
    {
        self::setCurrentUserId($userId);
    }

    /**
     * get collection by id
     *
     * @param int $collectionId            
     * @return \common\models\UserCollection
     */
    public static function getById($collectionId)
    {
        return UserCollection::findByPk($collectionId);
    }

    /**
     * set current user id
     * 
     * @param int $userId            
     */
    public static function setCurrentUserId($userId)
    {
        self::$currentUserId = $userId;
    }

    /**
     * get current user id.
     * false if not guest
     *
     * @return int | boolean
     */
    public static function getCurrentUserId()
    {
        if (self::$currentUserId == null) {
            self::$currentUserId = UserFacade::getCurrentUserId();
        }
        return self::$currentUserId;
    }

    /**
     * check if owner
     *
     * @param int $userId            
     * @return boolean
     */
    public static function isOwner($userId)
    {
        $owner = self::getCurrentUserId();
        return $owner === $userId;
    }

    /**
     * if userId not specified, current user collections are returned
     *
     * @param int $userId            
     * @return \common\models\UserCollection[]
     */
    public static function getUserCollections($userId = 0, $withPrivate = true)
    {
        if ($userId === 0) {
            $userId = self::getCurrentUserId();
        }
        $query = UserCollection::find()->with('userCollectionModel3ds')->where([
            'user_id' => $userId,
            'is_active' => 1
        ]);
        if (! $withPrivate) {
            $query->andWhere([
                'is_visible' => 1
            ]);
        }
        $collections = $query->orderBy('updated_at desc')->all();
        return $collections;
    }

    /**
     * create collection for user
     *
     * @param int $userId
     * @param string $title
     * @param null $systemType
     * @return UserCollection
     * @throws \common\components\exceptions\BusinessException
     */
    public static function createCollection($userId, $title, $systemType = null)
    {
        $now = new \yii\db\Expression('NOW()');
        $checkCollection = UserCollection::findOne([
            'title' => $title,
            'user_id' => $userId
        ]);
        if ($checkCollection) {
            return $checkCollection;
        }
        $collection = new UserCollection();
        $collection->user_id = $userId;
        $collection->title = $title;
        $collection->description = '';
        $collection->created_at = $now;
        $collection->updated_at = $now;
        $collection->is_active = 1;
        $collection->system_type = $systemType;
        $collection->safeSave();
        return $collection;
    }

    /**
     * Ruturn current user system colelction
     * 
     * @param string $type
     *            type of system collection - one of constant UserCollection::SYSTEM_TYPE_
     * @return \common\models\UserCollection
     */
    public static function getUserSystemCollection($type)
    {
        static $cache = [];
        
        if (! array_key_exists($type, $cache)) {
            $user = UserFacade::getCurrentUser();
            $collection = UserCollection::find()->where([
                'user_id' => $user->id,
                'system_type' => $type
            ])->one();
            
            AssertHelper::assert($collection, "User {$type} collection not found");
            $cache[$type] = $collection;
        }
        
        return $cache[$type];
    }

    /**
     * binds 3d model to default user collection
     * and returns this collection id
     *
     * @param int $model3dId            
     * @return boolean
     */
    public static function bindToFilesCollection($model3dId)
    {
        $collection = self::getUserSystemCollection(UserCollection::SYSTEM_TYPE_FILES);
        if ($collection === false) {
            return false;
        }
        // bind this model to default collection
        $res = self::bindToCollection($model3dId, $collection->id);
        return $res ? $collection->id : $res;
    }

    /**
     * bind 3d model to collection
     *
     * @param int $model3dId            
     * @param int $collectionId            
     * @return boolean
     */
    public static function bindToCollection($model3dId, $collectionId)
    {
        // just to check that object exists
        \common\models\Model3d::tryFindByPk($model3dId, _t('front.site', 'Model not found'));
        
        // now add relation
        $relObj = new \common\models\UserCollectionModel3d();
        $relObj->collection_id = $collectionId;
        $relObj->model3d_id = $model3dId;
        $relObj->created_at = new \yii\db\Expression('NOW()');
        UserCollection::tryFindByPk($collectionId)->countUp();
        return $relObj->safeSave();
    }

    /**
     * Check that model in user collection
     * 
     * @param \common\models\base\Model3d $model3d            
     * @param User $user            
     * @return bool
     */
    public static function inCollection(\common\models\base\Model3d $model3d, User $user)
    {
        $userCollectionsIds = UserCollection::find()->select('id')
            ->where([
            'is_active' => 1,
            'user_id' => $user->id
        ])
            ->column();
        
        return UserCollectionModel3d::find()->where([
            'collection_id' => $userCollectionsIds,
            'model3d_id' => $model3d->id
        ])->exists();
    }

    /**
     * delete model3d relation to user collection
     *
     * @param \common\models\base\UserCollectionModel3d $relInf            
     * @return bool
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public static function unbindItemRelation(\common\models\base\UserCollectionModel3d $relInf)
    {
        $collection = $relInf->collection;
        $isOwner = self::isOwner($collection->user_id);
        
        if (! $isOwner) {
            return false;
        }
        UserCollection::tryFindByPk($relInf->collection_id)->countDown();
        
        $res = $relInf->delete();
        
        // Update model user statistic
        // TODO move to UserStatistic or events
        if ($collection->user_id != $relInf->model3d->user_id && !UserStatistics::isFavorite($relInf->model3d->user, $collection->user)) {
            // AssertHelper::assertSave(UserStatistics::to($relInf->model3d->user_id)->down('favorites'));
        }
        if($collection->system_type===UserCollection::SYSTEM_TYPE_LIKES){
            AssertHelper::assertSave(UserStatistics::to($relInf->model3d->user_id)->down('favorites'));
        }
        
        // if it was last collection - delete model
        // TODO move it to event-driven
        if ($res && self::isOwner($relInf->model3d->user_id)) {
            $userActiveCollectionIds = UserCollection::find()->select('id')
                ->where([
                'user_id' => $relInf->model3d->user_id,
                'is_active' => 1
            ])
                ->column();
            
            $existAnotherBinds = UserCollectionModel3d::find()->where([
                'collection_id' => $userActiveCollectionIds,
                'model3d_id' => $relInf->model3d->id
            ])->exists();
            
            if (! $existAnotherBinds) {
                /** @var Model3d $model3d */
                $model3d = Model3d::tryFindByPk($relInf->model3d_id);
                // turn off deleting when unbind from collection
                // $model3d->deleteModel();
            }
        }
        
        return $res;
    }

    /**
     * move item to new collection,
     * checks if owner of old collection is owner of current
     * and that current user is owner
     *
     * @param int $itemId            
     * @param int $toCollectionId            
     * @return boolean
     */
    public static function moveItem($itemId, $toCollectionId)
    {
        /** @var UserCollection $newCollection */
        $newCollection = UserCollection::tryFindByPk($toCollectionId);
        /** @var UserCollectionModel3d $itemInfo */
        $itemInfo = UserCollectionModel3d::tryFindByPk($itemId);
        
        $oldCollection = $itemInfo->collection;
        if (self::isOwner($newCollection->user_id) && $oldCollection->user_id == $newCollection->user_id) {
            $itemInfo->collection_id = $newCollection->id;
            $itemInfo->created_at = new \yii\db\Expression('NOW()');
            UserCollection::tryFindByPk($oldCollection->id)->countDown();
            UserCollection::tryFindByPk($toCollectionId)->countUp();
            return $itemInfo->save(false);
        } else {
            return false;
        }
    }

    /**
     * delete user collection, with owner check
     *
     * @param int $collectionId            
     * @return bool
     * @throws Exception
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public static function deleteCollection($collectionId)
    {
        /** @var UserCollection $collection */
        $collection = UserCollection::tryFindByPk($collectionId);
        
        if ($collection->getIsSystem()) {
            throw new Exception('Cant delete system collection');
        }
        
        $result = true;
        if (count($collection->userCollectionModel3ds) > 0) {
            return false;
        }
        if (self::isOwner($collection->user_id)) {
            $result = $collection->delete();
        }
        return $result;
    }

    /**
     * Returns collection cover.
     * Default realization - get all covers from items in collection,
     * and make slider preview for them in collections preview
     *
     * @param int $collectionId            
     * @param UserCollectionModel3d[] $collectionItems            
     * @return string[]
     */
    public static function getCollectionCover($collectionId, $collectionItems = null)
    {
        if (is_numeric($collectionId)) {
            /** @var UserCollection $collection */
            $collection = UserCollection::tryFindByPk($collectionId);
        } else {
            $collection = $collectionId;
        }
        if ($collectionItems === null) {
            $collectionItems = $collection->userCollectionModel3ds;
        }
        $imgs = [];
        foreach ($collectionItems as $k => $v) {
            if ($k > static::$coverImgLimit) {
                break;
            }
            $model3d = $v instanceof \common\models\base\Model3d ? $v : $v->model3d;
            if ($cover = Model3dFacade::getCover($model3d)) {
                $imgs[] = $cover['image'];
            }
        }
        return $imgs;
    }

    /**
     * list of user collection, used in combo list
     *
     * @param int $userId            
     * @return array
     */
    public static function getList($userId)
    {
        $colRes = \common\models\UserCollection::find()->where('is_active=1')
            ->andWhere('user_id=' . $userId)
            ->asArray()
            ->all();
        $colItems = \yii\helpers\ArrayHelper::map($colRes, 'id', 'title');
        return $colItems;
    }

    /**
     * Remove item from all collection all users
     * 
     * @param Model3d $model            
     */
    public static function removeModel(Model3d $model)
    {
        /** @var UserCollectionModel3d[] $rels */
        $rels = UserCollectionModel3d::find()->where([
            'model3d_id' => $model->id
        ])->all();
        
        /** @var User[] $users */
        $users = [];
        foreach ($rels as $rel) {
            AssertHelper::assert($rel->delete());
            /** @var UserCollection $collection */
            $collection = UserCollection::tryFindByPk($rel->collection_id);
            $collection->countDown();
            $users[$collection->user->id] = $collection->user;
        }
        
        foreach ($users as $user) {
            // TODO move to UserStatistic or events
            if ($model->user_id != $user->id && ! UserStatistics::isFavorite($model->user, $user)) {
                AssertHelper::assertSave(UserStatistics::to($model->user_id)->down('favorites'));
            }
        }
    }
}