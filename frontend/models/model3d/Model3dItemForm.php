<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.07.16
 * Time: 15:08
 */

namespace frontend\models\model3d;

use common\interfaces\Model3dBaseInterface;
use common\models\Model3d;
use common\models\Model3dReplica;
use common\modules\printersList\models\RequestInfo;
use yii\base\InvalidParamException;

class Model3dItemForm extends Model3dForm
{
    /**
     * @var Model3dBaseInterface|Model3d
     */
    public $model3d = null;

    /**
     * @var Model3d
     */
    public $originalModel3d = null;

    /**
     * @var float
     */
    public $scaleBy = 100.0;

    /**
     * @var string
     */
    public $scaleMeasure;

    /**
     * @var int
     */
    public $selectedFotoramaFileId;

    /**
     * @var string
     */
    public $currentPrintersSort = RequestInfo::SORT_STRATEGY_DEFAULT;

    /**
     * @var int
     */
    public $quantity = 1;

    /**
     * Parts qty.
     * Index - file_id of parts, value - quantity for parts
     * @var int[]
     */
    public $partsQty;

    /**
     * @var string filter material groups by usage
     */
    public $materialGroupUsageFilter = null;


    /**
     * @param Model3dBaseInterface $model
     * @return Model3dItemForm
     */
    public static function create(Model3dBaseInterface $model) : Model3dItemForm
    {
        $from = new Model3dItemForm();
        if ($model instanceof Model3dReplica) {
            $from->loadByReplica($model);
        }
        else if ($model instanceof Model3d){
            $from->model3d = $from->originalModel3d = $model;
        }
        else {
            throw new InvalidParamException();
        }

        $from->partsQty = [];
        foreach ($model->getActiveModel3dParts() as $part) {
            $from->partsQty[$part->file->id] = $part->qty * $from->quantity;
        }

        return $from;
    }


    /**
     * @param $fileId
     * @return bool
     */
    public function isImageFotoramaFileId($fileId)
    {
        $model3dImages = $this->model3d->model3dImgs;
        foreach ($model3dImages as $model3dImage) {
            if (($model3dImage->file_id) && ($model3dImage->file_id == $fileId)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     */
    public function setFirstModel3dPartAsFotoramaFileId()
    {
        $model3dParts = $this->model3d->getActiveModel3dParts();
        $firstFile = reset($model3dParts);
        if ($firstFile) {
            $this->setCurrentSelectedFotoramaFileId($firstFile->file_id);
        }
    }

    /**
     * @param $selectedFotoramaFileId
     */
    public function setCurrentSelectedFotoramaFileId($selectedFotoramaFileId)
    {
        $this->selectedFotoramaFileId = $selectedFotoramaFileId;
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $this->loadWidgetColors($data);
        if (array_key_exists('quantity', $data) && ($data['quantity'] > 0)) {
            $this->quantity = (int)$data['quantity'];
        }
        $this->partsQty = $data['partsQty'] ?? null;

        if (array_key_exists('currentPrintersSort', $data)) {
            $this->currentPrintersSort = $data['currentPrintersSort'];
        };
        return true;
    }


    /**
     * @param Model3dReplica $model3dReplica
     */
    public function loadByReplica(Model3dReplica $model3dReplica)
    {
        $this->scaleMeasure = $model3dReplica->model_units;
        $this->model3d = $model3dReplica;
        $this->originalModel3d = $model3dReplica->originalModel3d;
    }
}