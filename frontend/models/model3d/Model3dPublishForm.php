<?php

namespace frontend\models\model3d;

use common\components\BaseForm;
use common\components\exceptions\BusinessException;
use common\models\Model3d;
use common\models\StoreUnit;
use common\models\User;
use common\modules\product\interfaces\ProductInterface;
use frontend\models\user\UserFacade;
use yii\base\Exception;
use yii\helpers\Html;

/**
 * Publish form model
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class Model3dPublishForm extends BaseForm
{

    /**
     * array of categories
     * 
     * @var array
     */
    public $category;

    public $tags;

    public $unlimitedPrice;

    public $pricePerPrint;

    public $priceCurrency;

    public $model3dId;

    public $licenseId;

    public $allowAdaptation;

    public $allowAdaptationShare;

    public $allowCommercialUse;

    /**
     * @inheritdoc
     * 
     * @return array
     */
    public function rules()
    {
        return [
            [
                [
                    'licenseId',
                    'category',
                    'model3dId',
                    'priceCurrency'
                ],
                'required'
            ],
            [
                [
                    'tags',
                    'unlimitedPrice',
                    'pricePerPrint',
                    'allowAdaptation',
                    'allowAdaptationShare',
                    'allowCommercialUse'
                ],
                'safe'
            ],
            [
                [
                    'model3dId',
                    'licenseId'
                ],
                'integer'
            ],
            [
                [
                    'pricePerPrint'
                ],
                'number',
                'min' => 0,
                'max' => 5000
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model3d_id' => _t('app', 'Model3D Id'),
            'unlimitedPrice' => _t('app', 'Unlimited prints price'),
            'pricePerPrint' => _t('app', 'Price of Model per Print'),
            'category' => _t('front.publish', 'Category'),
            'tags' => _t('front.publish', 'Tags'),
            'allowAdaptation' => _t('front.publish', 'Allow adaptations of your work'),
            'allowAdaptationShare' => _t('front.publish', 'Allow adaptations of your work to be shared'),
            'allowCommercialUse' => _t('front.publish', 'Allow commercial uses of your work')
        ];
    }

    /**
     * 1.
     * add to store_unit
     * 2. bind unit to categories in store_unit2category
     * 3. mark for moderation
     * 4. start antivirus check
     * 5. start g-code slicer
     * 
     * @return bool
     * @throws Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function publish()
    {
        $this->licenseId = $this->detectLicenseId();
        $res = $this->validate();
        if (! $res) {
            $err = Html::errorSummary($this);
            throw new Exception($err);
        }
        if (!$this->category) {
            throw new Exception(_t("front.publish", "Please select category"));
        }
        $user = UserFacade::getCurrentUser();

        /**
         *
         * @var Model3d $modelObj
         */
        $modelObj = Model3d::findByPk($this->model3dId);
        if (! UserFacade::isObjectOwner($modelObj)) {
            throw new Exception(_t("front.publish", "You don't have access to publish this model!"));
        }
        $transaction = app('db')->beginTransaction();
        try {
            $tagRes = $modelObj->updateTags($this->tags);
            $modelObj->category_id = $this->category;
            $modelObj->safeSave();
            UserFacade::markModelStore($user);
            $transaction->commit();
        } catch (\yii\base\Exception $ex) {
            $transaction->rollback();
            throw new Exception($ex->getMessage());
        }
        return $res;
    }

    /**
     * get license id based on form properties
     * 
     * @return int
     */
    private function detectLicenseId()
    {
        $licenseId = \common\models\Model3dLicense::COMMERCIAL_LICENSE_ID;
        if (empty($this->pricePerPrint)) { // free, find out which used
            $licenseId = \common\models\Model3dLicense::FREE_LICENSE_ID;
            if ($this->licenseId == \common\models\Model3dLicense::COMMERCIAL_LICENSE_ID) {
                throw new BusinessException(_t("site.publish", "Price per print required for commercial license"));
            }
        }
        
        return $licenseId;
    }

    /**
     * Check if model can be published
     * 
     * @param Model3d $model3dObj            
     * @return string
     */
    public function validatePublish(Model3d $model3dObj)
    {
        $user = UserFacade::getCurrentUser('/');
        $msg = false;
        if ($model3dObj->product_status == ProductInterface::STATUS_PUBLISH_PENDING) {
            $msg = _t('front.publish', 'Your model is published and will be available in store after moderation. Thank you!');
        } elseif ($user->status == User::STATUS_UNCONFIRMED) {
            $msg = _t('front.publish', 'Please confirm your e-mail for Publish access.');
        } elseif (empty($model3dObj->description)) {
            $msg = _t('front.publish', 'Please enter model description for Publish');
            $link = Html::a(_t('front.site', "Edit"), \yii\helpers\Url::toRoute([
                'my/model/edit/' . $model3dObj->id
            ]), [
                'class' => 'btn btn-default pull-right'
            ]);
            $msg = "<div style='overflow: hidden'>" . "<div class='pull-left m-t10'>" . $msg . "</div>" . $link . "</div>";
        }
        $model3dParts = $model3dObj->getActiveModel3dParts();
        if (empty($model3dParts)) {
            $msg = _t('front.publish', 'No file was found. 3D model cannot be published.');
        }
        // check cover image
        if (count($model3dParts) > 1 && empty($model3dObj->model3dImgs)) {
            $msg = _t("front.publish", "This KIT doesn't have Cover image. Please upload cover image of assembled KIT.");
        }
        return $msg;
    }

    /**
     * create store unit object by model3d publish
     * 
     * @return StoreUnit
     */
    private function createStoreUnit()
    {
        /**
         *
         * @var StoreUnit $storeUnitObj
         */
        $storeUnitObj = StoreUnit::findOne([
            'model3d_id' => $this->model3dId
        ]);
        if ($storeUnitObj === null) {
            $storeUnitObj = new StoreUnit();
        }
        $storeUnitObj->model3d_id = $this->model3dId;
        $storeUnitObj->safeSave();
        return $storeUnitObj;
    }
}
