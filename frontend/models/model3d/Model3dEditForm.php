<?php

namespace frontend\models\model3d;

use common\components\DateHelper;
use common\models\factories\Model3dTextureFactory;
use common\models\GeoCountry;
use common\models\Model3d;
use common\models\repositories\CatalogCostRepository;
use common\models\repositories\Model3dRepository;
use common\models\SiteTag;
use common\modules\product\interfaces\ProductInterface;
use common\services\Model3dService;
use frontend\components\UserSessionFacade;
use frontend\models\site\DenyCountry;
use frontend\models\user\UserFacade;
use InvalidArgumentException;
use yii;
use yii\base\Exception;
use yii\db\IntegrityException;

/**
 * edit model3d form logic
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class Model3dEditForm extends Model3dForm
{
    /**
     * @var string[]
     */
    public $tags = null;

    /**
     * @var float
     */
    public $unlimitedPrice;

    /**
     * @var float
     */
    public $pricePerPrint;

    /**
     * @var string
     */
    public $priceCurrency;

    /**
     * @var int
     */
    public $licenseId;

    /**
     * @var array int[]
     */
    public $qty = [];

    /**
     * Is anything was changed
     *
     * @var bool
     */
    public $isChanged = false;

    /**
     * @var int
     */
    public $categoryId;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['priceCurrency'],
                'string',
                'max' => 25
            ],
            [
                'pricePerPrint',
                'pricePerPrintValidator'
            ],
            [
                ['tags', 'unlimitedPrice', 'pricePerPrint', 'qty', 'categoryId'],
                'safe'
            ],
            [
                ['title','description'],
                'required'
            ],
            [
                'model3d',                      // Use model3dValidator
                'model3dValidator'
            ]
        ];
    }

    /**
     * @param $attribute
     */
    public function pricePerPrintValidator($attribute)
    {
        if ($this->pricePerPrint == 0) {
            return;
        }
        $this->pricePerPrint = (float)$this->pricePerPrint;
        if ($this->pricePerPrint < 0.01) {
            $this->addError($attribute, _t('site.model3d', 'Price should be more or equal 0.01'));
        }
        if ($this->pricePerPrint > 9999) {
            $this->addError($attribute, _t('site.model3d', 'Price should be less than 9999'));
        }
    }

    /**
     * @return bool
     */
    public function model3dValidator()
    {

        if (!$this->model3d->validate()) {
            $this->addErrors($this->model3d->getErrors());
            return false;
        }
        if (count($this->model3d->getActiveModel3dImages()) === 0) {
            $this->addError('model3d', 'You should add at least one cover');
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            $this->model3d->attributeLabels(),
            'tags'           => _t('app', 'Model Tags'),
            'unlimitedPrice' => _t('app', 'Unlimited prints price'),
            'pricePerPrint'  => _t('app', 'Price of Model per Print'),
            'qty'            => _t('front.publish', 'Quantity of # in KIT')
        ];
    }

    /**
     * get model units
     *
     * @return array
     */
    public function getModelMeasureLabels()
    {
        return [
            'in' => _t('site.model3d', 'Inches'),
            'mm' => _t('site.model3d', 'Millimeters')
        ];
    }

    /**
     * Preload store unit details if store unit exists
     */
    public function loadStoreUnit()
    {
        $this->pricePerPrint = $this->model3d->price_per_produce;
        $this->priceCurrency = $this->model3d->price_currency;
    }

    /**
     * @param string $value
     * @throws Exception
     */
    public function setScenario($value)
    {
        if (!$this->model3d) {
            throw new Exception('Model3d is not set!');
        }
        $this->model3d->setScenario($value);
    }

    /**
     * @return bool
     */
    public function validate($att = null, $clear = true)
    {
        return parent::validate($att, $clear) && $this->model3d->validate() && $this->model3d->storeUnit->validate();
    }

    /**
     * @param null $attribute
     * @return array
     */
    public function getErrors($attribute = null)
    {
        return array_merge($this->model3d->getErrors($attribute), $this->model3d->storeUnit->getErrors($attribute), parent::getErrors($attribute));
    }

    /**
     * Set default values for data array
     *
     * @param $data
     * @return array
     */
    protected function prepareLoadData($data)
    {
        $data = array_merge(
            [
                'tags'           => [],
                'unlimitedPrice' => null,
                'pricePerPrint'  => null,
                'priceCurrency'  => null,
                'licenseId'      => null,
                'qty'            => null,
                'publish_btn'    => null,
            ],
            $data
        );
        return $data;
    }

    /**
     * Список аттрибутов передающихся  в model3d
     *
     * @return array
     */
    protected function getModel3dAvailableAttributes()
    {
        return [
            'title',
            'description',
            'category_id',
            'model_units'
        ];
    }

    /**
     * Подготовить данные для загрузки в model3d.
     * Заполняет по умолчанию значениями null, выбирает из всего массива $data только определенный список аттрибутов
     *
     * @param $data
     * @return array
     */
    protected function prepareModel3dLoadData($data)
    {
        $model3dAttributes        = $this->getModel3dAvailableAttributes();
        $model3dAttributesFlipped = [];
        foreach ($model3dAttributes as $k => $v) {
            $model3dAttributesFlipped[$v] = null;
        }
        $data = array_merge($model3dAttributesFlipped, $data);
        return array_intersect_key($data, $model3dAttributesFlipped);
    }

    protected function isTagsChanged($siteTags, $model3d)
    {
        $currentTags    = $model3d->tags;
        $currentTagsArr = yii\helpers\ArrayHelper::getColumn($currentTags, 'id');
        $newTagsArr     = yii\helpers\ArrayHelper::getColumn($siteTags, 'id');
        return array_diff($currentTagsArr, $newTagsArr) || array_diff($newTagsArr, $currentTagsArr);
    }

    /**
     * Данная функция должна содержать функционал приведения параметров POST к виду хранящихся в модели.
     * Например провести explode string параметра пришедшего с формы и т.д.
     *
     * @param array $data
     * @return bool|void
     */
    public function load($data, $formname = null)
    {
        $data                = $this->prepareLoadData($data);
        $this->pricePerPrint = $data['pricePerPrint'];
        if ($this->model3d->price_per_produce != $this->pricePerPrint) {
            $this->model3d->productCommon->single_price = $this->pricePerPrint;
            $this->isChanged                            = true;
        }
        $this->qty = $data['qty'];
        $this->updateFilesQty();

        $model3dData = $this->prepareModel3dLoadData($data);
        if ($model3dData['title'] != $this->model3d->title) {
            $this->isChanged                     = 1;
            $this->model3d->productCommon->title = $model3dData['title'];
        }
        if ($model3dData['description'] != $this->model3d->description) {
            $this->isChanged                           = 1;
            $this->model3d->productCommon->description = $model3dData['description'];
        }
        if (array_key_exists('categoryId', $data) && $data['categoryId'] != $this->model3d->productCommon->category_id) {
            $this->isChanged                           = 1;
            $this->model3d->productCommon->category_id = $data['categoryId'];
            $this->model3d->setCantBePublisehedUpdated();
        }

        $tags = $this->formTags($data['tags']);
        if ($this->isTagsChanged($tags, $this->model3d)) {
            $this->model3d->setSiteTags($tags);
            $this->isChanged = 1;
        }

        if (!array_key_exists('submitMode', $data) || ($data['submitMode'] == 'unpublish')) {
            $this->model3d->productCommon->product_status = ProductInterface::STATUS_DRAFT;
            $this->model3d->setScenario(Model3d::SCENARIO_UNPUBLISH);
        } elseif ($data['submitMode'] == 'publish') {
            if ($this->model3d->isCanBePublishUpdated() && in_array($this->model3d->product_status,
                    [ProductInterface::STATUS_PUBLISHED_PUBLIC, ProductInterface::STATUS_PUBLISHED_UPDATED])) {
                if ($this->isChanged) {
                    $this->model3d->productCommon->product_status = ProductInterface::STATUS_PUBLISHED_UPDATED;
                }
            } else {
                if ($this->isChanged || (!$this->model3d->isPublished())) {
                    $this->model3d->productCommon->product_status = ProductInterface::STATUS_PUBLISH_PENDING;
                }
            }
            $this->model3d->setScenario(Model3d::SCENARIO_PUBLISH);
        }
        return true;
    }

    /**
     * @param $tagsArray
     * @return array
     */
    protected function formTags($tagsArray)
    {
        $returnValue = [];
        if (!$tagsArray) {
            return [];
        }
        if (array_key_exists(0, $tagsArray) && is_array($tagsArray[0])) {
            // Jsform serialize fix
            $tagsArray = $tagsArray[0];
        }

        foreach ($tagsArray as $oneTag) {
            $oneTagText = trim(mb_strtolower($oneTag));
            if ($oneTagText) {
                $tag = SiteTag::findOne(['text' => $oneTagText]);
                if (!$tag) {
                    $tag       = new SiteTag();
                    $tag->text = $oneTagText;
                }
                $returnValue[] = $tag;
            }
        }
        return $returnValue;
    }


    /**
     * Магический метод, используется для доступа к аттрибутам формы, содержащихся в включенных в нее моделях
     *
     * @param string $name
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function __get($name)
    {
        if ($this->hasProperty($name)) {
            return parent::__get($name);
        } else {
            if (in_array($name, $this->getModel3dAvailableAttributes(), true)) {
                return $this->model3d->$name;
            } else {
                throw new InvalidArgumentException('Can`t get value. Attribute "' . $name . "' not exists in '" . Model3dEditForm::class . "'");
            }
        }
    }

    /**
     * Магический метод, используется для доступа к аттрибутам формы, содержащихся в включенных в нее моделях
     *
     * @param string $name
     * @param mixed $value
     * @throws yii\base\UnknownPropertyException
     */
    public function __set($name, $value)
    {
        if ($this->hasProperty($name)) {
            parent::__set($name, $value);
        } else {
            if (in_array($name, $this->getModel3dAvailableAttributes(), true)) {
                $this->model3d->$name = $value;
            } else {
                throw new InvalidArgumentException('Can`t set value. Attribute "' . $name . "' not exists in '" . Model3dEditForm::class . "'");
            }
        }
    }


    /**
     * Save updates to model3d
     *
     * @return bool
     * @throws \yii\base\Exception
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function save()
    {
        Model3dService::saveTags($this->model3d);
        $model3dRepository = Yii::createObject(Model3dRepository::class);
        $this->model3d->productCommon->updated_at = DateHelper::now();
        $model3dRepository->save($this->model3d);
        CatalogCostRepository::invalidateCost($this->model3d->storeUnit);
        return true;
    }

    /**
     * update files qty
     */
    private function updateFilesQty()
    {
        if ($this->qty) {
            foreach ($this->qty as $partId => $qty) {
                foreach ($this->model3d->model3dParts as $model3dPart) {
                    if ($model3dPart->id == $partId) {
                        $newQty = max([1, (int)$qty]);
                        if ($model3dPart->qty != $newQty) {
                            $model3dPart->qty = $newQty;
                            $this->isChanged  = 1;
                        }
                    }
                }
            }
        }
    }

    /**
     * @return bool
     * @throws yii\base\UserException
     * @throws yii\web\NotFoundHttpException
     */
    public function needTaxMessage()
    {
        $showMessage = true;
        if ($this->pricePerPrint <= 0) {
            $showMessage = false;
        }
        $user        = UserFacade::getCurrentUser();
        $hasTaxInfo  = \common\models\PaymentDetailTaxRate::hasUserTaxes($user);
        $userCountry = UserSessionFacade::getLocation()->getGeoCountry();
        if ($hasTaxInfo) {
            $userCountry = GeoCountry::findOne(['iso_code' => $user->userTaxInfo->place_country]);
        }
        if (DenyCountry::checkCountry($userCountry->iso_code, false)) { // if true - country is ok
            $showMessage = false;
        }
        return $showMessage;
    }
}
