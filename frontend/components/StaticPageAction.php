<?php
/**
 * Created by mitaichik
 */

namespace frontend\components;


use common\models\StaticPage;
use yii\base\Action;
use yii\web\NotFoundHttpException;

/**
 * Class StaticPageAction
 *
 * @package frontend\components
 */
class StaticPageAction extends Action
{
    /**
     * Page alias
     *
     * @var string
     */
    public $pageAlias;

    /**
     * @var string
     */
    public $viewPath = '/site/static_page';

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->pageAlias) {
            $this->pageAlias = $this->id;
        }
    }

    /**
     * Render static page use current language
     */
    public function run()
    {
        if (strpos($this->pageAlias, 'sitemap')!==false) {
            $this->controller->view->params['noindex'] = true;
        }
        $page = StaticPage::find()
            ->where(['alias' => $this->pageAlias])
            ->one();

        if (!$page) {
            throw new NotFoundHttpException();
        }
        return $this->controller->render($this->viewPath, ['page' => $page]);
    }
}