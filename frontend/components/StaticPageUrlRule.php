<?php
/**
 * User: nabi
 */

namespace frontend\components;

use Yii;
use yii\web\Request;
use yii\web\UrlManager;
use yii\web\UrlRuleInterface;

/**
 * Class StaticPageUrlRule
 *
 * url rule for static pages
 * use $pathStart to define what urls will be parsed as static pages.
 * it should be included in url-rules.php file
 *
 * @package frontend\components
 */
class StaticPageUrlRule implements UrlRuleInterface
{

    /**
     * to generate links using Url
     * $link = Url::toRoute(['/static-page', 'alias'=>'sitemap']);
     *
     * @var string
     */
    public $staticRoute = 'static-page'; // refers to StaticPageController

    /**
     * to check if given url is for static page url
     * if it starts with these prefix
     *
     * @var array
     */
    public $pathStart = ['site/'];

    /**
     * Parses the given request and returns the corresponding route and parameters.
     *
     * @param UrlManager $manager the URL manager
     * @param Request $request the request component
     * @return array|bool the parsing result. The route and the parameters are returned as an array.
     * If false, it means this rule cannot be used to parse this path info.
     */
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if (!$this->checkPathRoute($pathInfo)) {
            return false;
        }
        $params = [];
        $parameters = explode('/', $pathInfo);

        $route = $this->staticRoute;
        if(count($parameters) > 1){
            $params['type'] =  $parameters[0];
            $params['alias'] = $parameters[1];
        }else{
            $params['alias'] = $parameters[0];
        }
        Yii::trace("Request parsed with URL rule: " . $route, __METHOD__);
        return [$route, $params];
    }

    /**
     * Creates a URL according to the given route and parameters.
     *
     *  to create url to static page
     *  use $link = Url::toRoute(['/static-page', 'alias'=>'c/NewYork']);
     *
     * @param UrlManager $manager the URL manager
     * @param string $route the route. It should not have slashes at the beginning or the end.
     * @param array $params the parameters
     * @return string|bool the created URL, or false if this rule cannot be used for creating this URL.
     */
    public function createUrl($manager, $route, $params)
    {
        if ($route!==$this->staticRoute) {
            return false;
        }
        $url = '/' . ltrim($params['alias'], '/ ');
        return $url;
    }

    /**
     * check if given path route goes for static page url
     *
     * @param $pathInfo
     * @return bool
     */
    private function checkPathRoute($pathInfo)
    {
        foreach($this->pathStart as $path){
            if(strpos($pathInfo, $path) === 0){
                return true;
            }
        }
        return false;
    }
}