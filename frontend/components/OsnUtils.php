<?php
namespace frontend\components;

/**
 * General osn functions to get buttons, forms and etc.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com
 * @deprecated
 * @TODO delete
 */
class OsnUtils
{

    /**
     * get osn buttons for private profile
     *
     * @param bool $asArray            
     * @return string|array
     */
    public static function getSocialButtons($asArray = true)
    {
        $osn = [
            'facebook' => 'Facebook',
            'google' => 'Google',
            /*
            'twitter' => 'Twitter',
            'pinterest' => 'Pinterest'
            */
        ];
        $result = [];
        foreach ($osn as $k => $v) {
            $result[] = sprintf('<a href="%s" class="btn btn-sm btn-%s"><i class="soc-icons soc-icons-%s"></i> %s</a>', \yii\helpers\Url::toRoute('my/social'), $k, $k, $v);
        }
        return $asArray ? implode(" ", $result) : $result;
        // btn-sm btn-social
    }
}