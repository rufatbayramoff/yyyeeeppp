<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace frontend\components;

use common\components\exceptions\ArrayErrorsExceptionInterface;
use common\components\exceptions\DataExceptionInterface;
use common\components\exceptions\ErrorCodeExceptionInterface;
use common\components\exceptions\OutExceptionInterface;
use common\components\exceptions\ValidationExceptionInterface;
use common\modules\api\v2\controllers\BaseApiController;
use treatstock\api\v2\exceptions\UnSuccessException;
use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\web\HttpException;

/**
 * Class ErrorHandler
 *
 * @package frontend\components
 */
class ErrorHandler extends \yii\web\ErrorHandler
{

    protected function isApiRequest()
    {
        return Yii::$app->controller instanceof BaseApiController;
    }

    /**
     *
     * @param \Exception $exception
     * @return array
     */
    protected function convertExceptionToArray($exception)
    {
        if ($this->isApiRequest()) {
            if ($exception instanceof UnSuccessException) {
                $array['success'] = 0;
                $array['errors'] = $exception->errors;
                $array['message'] = $exception->getMessage();
            } else {
                $array = [
                    'name'    => ($exception instanceof Exception || $exception instanceof ErrorException) ? $exception->getName() : get_class($exception),
                    'message' => $exception->getMessage(),
                    'code'    => $exception->getCode(),
                ];
            }
        } else {
            $array = parent::convertExceptionToArray($exception);
        }

        if ($exception instanceof ArrayErrorsExceptionInterface) {
            $array['errors'] = $exception->errorsArray();
        }

        if ($exception instanceof ValidationExceptionInterface) {
            $array['validateErrors'] = $exception->errorsArray();
        }

        if ($exception instanceof ErrorCodeExceptionInterface) {
            $array['code'] = $exception->getErrorCode();
        }

        if (
            ($exception instanceof OutExceptionInterface)||
            ($exception instanceof \InvalidArgumentException)||
            ($exception instanceof \yii\base\InvalidArgumentException)
        ) {
            $array['message'] = $exception->getMessage();
        }

        if ($exception instanceof DataExceptionInterface) {
            $array['data'] = $exception->getData();
        }

        return $array;
    }

    /**
     * Logs the given exception
     * @param \Exception $exception the exception to be logged
     * @since 2.0.3 this method is now public.
     */
    public function logException($exception)
    {
        $category = get_class($exception);
        if ($exception instanceof HttpException) {
            $category = $category . '_http:' . $exception->statusCode;
        } elseif ($exception instanceof \ErrorException) {
            $category .= ':' . $exception->getSeverity();
        }

        Yii::error($exception, $category);
    }
}