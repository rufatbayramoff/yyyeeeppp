<?php

namespace frontend\components;

use Yii;
use yii\web\View;

class FrontendWebView extends View
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->params['isWidgetMode'] = Yii::$app->request->get('widget') === 'widget';
    }

    public function isForPs()
    {
        return $this->params['psId'] ?? false;
    }

    public function isThingiverseUtm()
    {
        return array_key_exists('utm_source', $this->params) && ($this->params['utm_source'] === 'thingiverse');
    }

    /**
     * @return bool
     */
    public function isWidgetMode(): bool
    {
        return $this->params['isWidgetMode'] ?? false;
    }

    /**
     * @param $mode
     *
     */
    public function setWidgetMode($mode): void
    {
        $this->params['isWidgetMode'] = $mode;
    }

    /**
     * Disables the search form in the header
     */
    public function headerSearchFormDisabled(): void
    {
        $this->params['headerSearchFormDisabled'] = true;
        $this->params['headerSearchFormDisabledClass'] = 'header_search_form_disabled';
    }

    public function noindex(): void
    {
        $this->params['noindex'] = true;
    }


    /**
     * @return bool
     */
    public function isHeaderSearchFormDisabled(): bool
    {
        return $this->params['headerSearchFormDisabled'] ?? false;
    }

    /**
     * @return string
     */
    public function getHeaderSearchFormDisabledClass(): string
    {
        return $this->params['headerSearchFormDisabledClass'] ?? '';
    }
}
