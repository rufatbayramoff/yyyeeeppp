<?php
namespace frontend\components;

use \yii\helpers\Html;

/**
 * StoreUnitUtils - helps to create small widgets in GUI to work with colors, links, materials, and etc.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class StoreUnitUtils
{

    /**
     * get colored cell
     * TODO: Оформимть как обычный нормальный виджет
     *
     * @param array|string $rgb            
     * @param string $title            
     * @return string
     */
    public static function displayColor($rgb, $title)
    {
        if (is_array($rgb)) {
            $rgb = implode(",", $rgb);
        }
        if (empty($rgb)) {
            $colorObj = \common\models\PrinterColor::findOne([
                'title' => $title
            ]);
            if ($colorObj) {
                $rgb = $colorObj->rgb;
            }
        }
        return Html::tag('span', '', [
            'class' => 'btn btn-xs ts-material-color',
            'style' => "background-color: rgb($rgb)",
            'title' => $title
        ]);
    }

    /**
     * display more
     * TODO: Оформимть как обычный нормальный виджет
     *
     * @param string $text
     * @param int $lineLength
     * @param bool $nl2br
     * @return string
     */
    public static function displayMore($text, $lineLength = 55, $nl2br = true)
    {
        $tt = \H($text); // 55 line
        $linesTotal = strlen($tt) / $lineLength;
        if ($linesTotal > 3) {
            $resultOrig = mb_substr($tt, 0, 3 * $lineLength);
            $resultOrig =  mb_substr($resultOrig, 0, strrpos($resultOrig, ' '));

            $result = $resultOrig . Html::a('...' . _t('front.site', 'Show more'), '#', [
                'class' => 'tsrm-toggle', 'style'=>'display:inline'
            ]);
            $result = $result . "<span class='tsrm-content'>";
            $result = $result . mb_substr($tt, mb_strlen($resultOrig));
            $result = $result . "</span>";
            $tt = $result;
        }
        return $nl2br?nl2br($tt):$tt;
    }

    /**
     * display more
     * TODO: Оформимть как обычный нормальный виджет
     *
     * @param string $html
     * @param int $lineLength
     * @return string
     */
    public static function displayMoreHtml($html, $lineLength = 55)
    {
        $tt = $html; // 55 line
        $linesTotal = strlen($tt) / $lineLength;
        if ($linesTotal > 3) {
            $resultOrig = mb_substr($tt, 0, 3 * $lineLength);
            $resultOrig =  mb_substr($resultOrig, 0, strrpos($resultOrig, ' '));

            $result = $resultOrig . Html::a('...' . _t('front.site', 'Show more'), '#', [
                    'class' => 'tsrm-toggle', 'style'=>'display:inline'
                ]);
            $result = $result . "<span class='tsrm-content'>";
            $result = $result . mb_substr($tt, mb_strlen($resultOrig));
            $result = $result . "</span>";
            $tt = $result;
        }
        return $tt;
    }
}
