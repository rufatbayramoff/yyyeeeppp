<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 31.05.2017
 * Time: 9:52
 */

namespace frontend\components;


use common\components\ArrayHelper;
use dosamigos\ckeditor\CKEditor;

class CKEditorDefault extends CKEditor
{
    public static function getDefaults($override = [])
    {
        return ArrayHelper::merge($override, [
            'options' => ['rows' => 6],
            'preset' => 'basic',
            'clientOptions' => [
                'allowedContent' => true, //(bool)app('request')->get('allowcontent', false),
                //'forcePasteAsPlainText' => true,
                'allowedContent' => 'table tr th td p u sup sub em strong ul ol li a h1 h2 h3 h4 h5', //(bool)app('request')->get('allowcontent', false),
                'contentsCss' => param('server').'/css/site.css',
                'removePlugins' => 'flash',
                'resize_enabled' => true,
                'toolbarGroups' => [
                    ['name' => 'undo'],
                    ['name' => 'list'],
                    ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                    ['name' => 'colors'],
                    ['name' => 'links', 'groups' => ['links', 'insert']],
                    ['name' => 'others', 'groups' => ['others', 'about']],
                ],
                'removeButtons' => 'Subscript,Superscript,Flash,Smiley,SpecialChar,PageBreak,Iframe',
            ],
        ]);
    }
}