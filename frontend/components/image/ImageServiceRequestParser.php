<?php
namespace frontend\components\image;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class ImageServiceRequestParser extends \yii\base\BaseObject
{

    /**
     * requested url
     * 
     * @var string
     */
    public $requestedUrl;

    /**
     * full requested server path
     * 
     * @var string
     */
    public $requestedServerPath;

    /**
     * requested path without filename
     * 
     * @var string
     */
    public $path;

    /**
     * requested file
     * 
     * @var string
     */
    public $file;

    /**
     * where to save thumbs and to search images
     * 
     * @var string
     */
    public $rootAlias = '@static';

    /**
     * reg exp to parse file name, example: 2010_White_100x100.png
     * 
     * @var string
     */
    public $regexp = "#(\w+)_(\d+)x(\d+)\.([a-zA-Z]{3,4})#";

    /**
     * set after init based on rootAlias
     * 
     * @var string
     */
    private $rootPath;

    /**
     * default image content type
     * 
     * @var string
     */
    public $contentType = 'image/png';

    /**
     * URL to static folder or subdomain
     *
     * @var string
     */
    public $staticUrl;

    /**
     *
     * @var string
     */
    public $thumbSaved = false;

    /**
     * default width
     * 
     * @var string
     */
    public $width = 350;

    /**
     * default height
     * 
     * @var string
     */
    public $height = 250;

    /**
     * init request details
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        $this->rootPath = \Yii::getAlias($this->rootAlias);
        $this->parseRequestUrl($this->requestedUrl);
        parent::init();
        if (!$this->staticUrl) {
            $this->staticUrl = param('staticUrl');
        }
    }

    /**
     */
    public function returnImage()
    {
        $lifetime = 60 * 60 * 24 * 30; // 30 days
        $path = $this->getThumbnailPath();
        $filetime = filemtime($path);
        $etag = md5($filetime . $path);
        $time = gmdate('r', $filetime);
        
        $expires = gmdate('D, d M Y H:i:s', $filetime + $lifetime) . ' GMT';
        
        $headers = [
            'Content-Type' => $this->contentType,
            'Content-Length' => filesize($path),
            'Cache-Control' => 'public',
            'Pragma' => 'public',
            'Expires' => $expires,
            'Etag' => $etag
        ];
        
        $headerTest1 = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $time;
        $headerTest2 = isset($_SERVER['HTTP_IF_NONE_MATCH']) && str_replace('"', '', stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])) == $etag;
        if ($headerTest1 || $headerTest2) { // image is cached by the browser, we dont need to send it again
            foreach ($headers as $header => $val) {
                header($header . ': ' . $val);
            }
            header('Last-Modified: ' . $time, true, 304);
            exit();
        }
        $headers['Last-Modified'] = $time;
        foreach ($headers as $header => $val) {
            header($header . ': ' . $val);
        }
        readfile($path);
        exit();
    }

    /**
     *
     * @param string $requestedPathOrig            
     */
    public function parseRequestUrl($requestedPathOrig)
    {
        $requestedPath = str_replace('/static', '', $requestedPathOrig);
        $this->requestedServerPath = $this->rootPath . $requestedPath;
        
        $parts = explode('/', $requestedPath);
        $fileRequestName = array_pop($parts); // 2010_White_100x100.png
        $path = implode('/', $parts);
        $this->path = $path;
        
        preg_match($this->regexp, $fileRequestName, $matches);
        if (empty($matches[2]) || empty($matches[3])) {
            $this->file = $fileRequestName;
        } else {
            $this->file = $matches[1] . '.' . $matches[4];
            $this->contentType = 'image/' . $matches[4];
            $this->setWidthAndHeight((int)$matches[2], (int)$matches[3]);
        }
    }

    /**
     *
     * @param string $width            
     * @param string $height            
     */
    public function setWidthAndHeight($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    /**
     *
     * @return string
     */
    public function getThumbnailFileName()
    {
        $fileInf = pathinfo($this->file);
        return $fileInf['filename'] . '_' . $this->width . 'x' . $this->height . '.' . $fileInf['extension'];
    }

    /**
     *
     * @return string
     */
    public function getThumbnailPath()
    {
        return $this->rootPath . $this->path . '/' . $this->getThumbnailFileName();
    }

    /**
     *
     * @return string
     */
    public function getOriginalImageFullPath()
    {
        return $this->rootPath . $this->path . '/' . $this->file;
    }

    /**
     *
     * @return string
     */
    public function getThumbnailUrl()
    {
        return $this->staticUrl . $this->path . '/' . $this->getThumbnailFileName();
    }

    /**
     * get server path to requested file
     * 
     * @return string
     */
    public function getImageUrl()
    {
        return $this->staticUrl . $this->path . '/' . $this->file;
    }
}
