<?php

namespace frontend\components\image;

use common\components\FileDirHelper;
use common\components\FileTypesHelper;
use common\interfaces\FileBaseInterface;
use common\models\File;
use Yii;
use yii\base\Exception;
use yii\helpers\Html;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * ImageHtmlHelper
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class ImageHtmlHelper
{
    const LARGE_WIDTH = 1200;

    const LARGE_HEIGHT = 1200;

    public const DEFAULT_WIDTH = 720;

    public const DEFAULT_HEIGHT = 540;

    public const IMG_CATALOG_WIDTH = 358;

    public const IMG_CATALOG_HEIGHT = 269;

    public const THUMB_SMALL = 85;

    public static $pattern = "{dir}/{image}_{width}{x}{height}.{extension}";

    public const IMAGE_EXTENSIONS = ['jpg', 'png'];

    public const PRINT_REVIEW_BLOCK_WIDTH = 320;

    public const PRINT_REVIEW_BLOCK_HEIGHT = 320;

    /**
     *
     * @return array
     */
    public static function getAllowThumbSizes()
    {
        $sizes = [
            [
                'width'  => self::THUMB_SMALL,
                'height' => self::THUMB_SMALL
            ],
            [
                'width'  => 128,
                'height' => 128
            ],
            [
                'width'  => 100,
                'height' => 100
            ],
            [
                'width'  => 200,
                'height' => 200
            ],
            [
                'width'  => 300,
                'height' => 300
            ],
            [
                'width'  => 800,
                'height' => 600
            ],
            [
                'width'  => self::DEFAULT_WIDTH,
                'height' => self::DEFAULT_HEIGHT
            ],
            [
                'width'  => self::IMG_CATALOG_WIDTH,
                'height' => self::IMG_CATALOG_HEIGHT
            ]
        ];
        return $sizes;
    }

    protected static function cutOldThumbSize($filename)
    {
        if ($underscorePosition = strrpos($filename, '_')) {
            // underscore exists
            $lastPart = mb_substr($filename, $underscorePosition + 1, 255);

            $delimiter = 'x';
            if (!strpos($lastPart, $delimiter)) {
                $delimiter = 'X';
            }
            if (strpos($lastPart, $delimiter)) {
                if (($sizeParts = explode($delimiter, $lastPart)) && count($sizeParts) === 2) {
                    [$sizeX, $sizeY] = $sizeParts;
                    $sizeX = (int)$sizeX;
                    $sizeY = (int)$sizeY;
                    $sizeX = $sizeX > 999 ? 999 : $sizeX;
                    $sizeY = $sizeY > 999 ? 999 : $sizeY;
                    if ($lastPart === ($sizeX . $delimiter . $sizeY)) {
                        return mb_substr($filename, 0, $underscorePosition);
                    }
                }
            }

        }
        return $filename;
    }

    /**
     * Usage: ImageHtmlHelper::getThumbUrl($imgSrc, 100, 100)
     * If file is not public, you should use getThumbUrlForFile function
     *
     * @param string $imageFileUrl
     * @param int $width
     * @param int $height
     * @param bool $center
     * @return string
     */
    public static function getThumbUrl($imageFileUrl, $width = self::DEFAULT_WIDTH, $height = self::DEFAULT_HEIGHT, $center = false)
    {
        if (!$imageFileUrl) {
            return '';
        }

        if (strpos($imageFileUrl, '.gif') !== false) {
            return $imageFileUrl;
        }
        $fullStaticUrl           = Yii::getAlias(Yii::$app->params['staticUrl']);
        $imageFileUrlWithoutHost = str_replace($fullStaticUrl, '/static', $imageFileUrl);
        $fParts                  = pathinfo($imageFileUrlWithoutHost);
        if (empty($fParts['extension'])) {
            $p = parse_url($imageFileUrl);
            if (!empty($p['query'])) {
                parse_str($p['query'], $p2);
                $p2['w']      = $width;
                $p2['h']      = $height;
                $p2           = http_build_query($p2);
                $p['query']   = $p2;
                $imageFileUrl = sprintf('%s://%s%s?%s', $p['scheme'], $p['host'], $p['path'], $p['query']);
            }
            return $imageFileUrl;
        }
        $fParts['filename'] = self::cutOldThumbSize($fParts['filename']);

        $parts  = [
            'image'     => $fParts['filename'],
            'extension' => $fParts['extension'],
            'dir'       => $fParts['dirname'],
            'width'     => $width,
            'height'    => $height,
            'x'         => $center ? 'X' : 'x'
        ];
        $result = self::$pattern;
        foreach ($parts as $k => $part) {
            $result = str_replace('{' . $k . '}', $part, $result);
        }
        $result = str_replace('/static', $fullStaticUrl, $result);
        return $result;
    }


    /**
     * @param FileBaseInterface $file
     *
     * @param int $width
     * @param int $height
     * @param bool $center
     * @return string
     */
    public static function getThumbUrlForFile(FileBaseInterface $file, $width = self::DEFAULT_WIDTH, $height = self::DEFAULT_HEIGHT, $center = false)
    {
        $imageFileUrl = $file->getFileUrl();
        if ($file->is_public) {
            if (!$width && !$height) {
                return $imageFileUrl;
            }
            return self::getThumbUrl($imageFileUrl, $width, $height, $center);
        } else {
            if ($width || $height) {
                $imageFileUrl .= '&w=' . $width . '&h=' . $height;
            }
        }
        return $imageFileUrl;
    }

    /**
     * @param FileBaseInterface $file
     * @param int $width
     * @param int $height
     * @return string
     * @throws Exception throws then file is not video
     */
    public static function getVideoPreviewUrl(FileBaseInterface $file, $width = self::IMG_CATALOG_WIDTH, $height = self::IMG_CATALOG_HEIGHT): string
    {
        if (!$file->testIsExists()) {
            return '';
        }

        if (!FileTypesHelper::isVideo($file)) {
            throw new Exception("File {$file->getLocalTmpFilePath()} is not video");
        }

        $filepath    = $file->getLocalTmpFilePath();
        $outFilepath = "{$filepath}_{$width}_{$height}.jpg";

        if (!file_exists($outFilepath)) {

            exec("ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=height,width {$filepath}", $out);
            preg_match('/width=(\d+).*height=(\d+)/s', implode(' ', $out), $matches);
            if (!$matches) {
                return '';
            }
            $box = new Box((int)$matches[1], (int)$matches[2]);

            $box = $box->getWidth() > $box->getHeight()
                ? $box = $box->widen($width)
                : $box->heighten($height);


            exec("ffmpeg -i {$filepath} -deinterlace -an -ss 1 -t 00:00:01 -s {$box->getWidth()}x{$box->getHeight()} -r 1 -y -vcodec mjpeg -f mjpeg {$outFilepath}");
        }

        return "{$file->getFileUrl()}_{$width}_{$height}.jpg";
    }


    /**
     * Create click able trumb for admin part
     *
     * @param File $file
     * @param array $linkOptions
     * @param int $width
     * @param int $height
     * @return string
     * @throws Exception
     * @throws \Exception
     */
    public static function getClickableVideo(
        File $file = null,
        array $linkOptions = [],
        int $width = ImageHtmlHelper::IMG_CATALOG_WIDTH,
        int $height = ImageHtmlHelper::IMG_CATALOG_HEIGHT
    ): string
    {
        if ($file == null || !FileTypesHelper::isVideo($file)) {
            return '';
        }

        $linkOptions['target']     = '_blank';
        $linkOptions['data-video'] = 'true';

        $linkContent = '<div class="isvideo-play tsi tsi-play"></div>'
            . HTML::img(ImageHtmlHelper::getVideoPreviewUrl($file, $width, $height), ['alt' => \H($file->name)]);

        return HTML::a($linkContent, $file->getFileUrl(), $linkOptions);
    }

    /**
     * Create click able trumb for admin part
     *
     * @param File $file
     * @return string
     */
    public static function getClickableThumb($file): string
    {
        return $file ? HTML::a(
            HTML::img(
                ImageHtmlHelper::getThumbUrlForFile(
                    $file,
                    ImageHtmlHelper::THUMB_SMALL,
                    ImageHtmlHelper::THUMB_SMALL
                ),
                ['width' => ImageHtmlHelper::THUMB_SMALL]
            ),
            $file->getFileUrl(), ['target' => '_blank']
        ) : '';
    }


    /**
     * @param File $file
     * @param int $width
     * @param int $height
     * @return string
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws \Imagine\Exception\RuntimeException
     */
    public static function getThumbPathForFile($file, $width = self::DEFAULT_WIDTH, $height = self::DEFAULT_HEIGHT)
    {
        $width        = (int)$width;
        $height       = (int)$height;
        $cachePathDir = Yii::getAlias('@runtime') . '/resizeImgCache';
        FileDirHelper::createDir($cachePathDir);
        $cachePath = $cachePathDir . '/' . $file->md5sum . '_' . $width . '_' . $height . '.' . (strtolower($file->extension) === 'png' ? 'png' : 'jpg');
        if (file_exists($cachePath)) {
            return $cachePath;
        }

        Image::getImagine()
            ->open($file->getLocalTmpFilePath())
            ->thumbnail(new Box($width, $height))
            ->save($cachePath);
        return $cachePath;
    }

    public static function stripExifInfo(FileBaseInterface $file)
    {
        $ext = strtolower($file->extension);
        if ($ext == 'jpg' || $ext == 'jpeg') {
            $tempRemoveExifPath = Yii::getAlias('@runtime') . '/removeExif_' . $file->uuid . '_' . date('Y_m_d_H_i_s') . '_' . random_int(0, 9999);

            try {
                $size = @getimagesize($file->getLocalTmpFilePath());
            } catch (\Exception $e) {
                $size = null;
            }
            if (is_array($size) && array_key_exists('mime', $size) && $size['mime'] === 'image/jpeg') {
                $img = @imagecreatefromjpeg($file->getLocalTmpFilePath());
                if ($img) {
                    imagejpeg($img, $tempRemoveExifPath, 100);
                    $file->publishFileToServerAndSave($tempRemoveExifPath);
                }
            }
        }
    }

    public static function getRedDot($num = 1)
    {
        if ($num == 0) {
            return '';
        }
        return sprintf("<span class='tab-new-badge' style=''> %d</span>", $num);
    }

    /**
     * @param File $file
     * @param int $rotate
     *
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public static function rotateImage(File $file, int $rotate): void
    {
        $location = $file->getLocalTmpFilePath();
        Image::frame($location, 0)->rotate($rotate)->save($location);
        $file->publishFileToServerAndSave($location);
    }
}
