<?php
namespace frontend\components\image;

use Imagine\Image\ManipulatorInterface;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\web\NotFoundHttpException;

/**
 * ImageService
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class ImageService
{

    const THUMB_MAX_WIDTH = 800;

    const THUMB_MIN_WIDTH = 32;

    const THUMB_MAX_HEIGHT = 600;

    const THUMB_MIN_HEIGHT = 32;

    /**
     *
     * @param string $requestedUrl
     * @return ImageServiceRequestParser
     * @throws \Imagine\Exception\RuntimeException
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws \yii\web\NotFoundHttpException
     */
    public function getImage($requestedUrl)
    {
        $requestDetails = $this->resize($requestedUrl);
        return $requestDetails;
    }

    /**
     *
     * @param string $requestedUrl
     * @return ImageServiceRequestParser
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws \Imagine\Exception\RuntimeException
     * @throws NotFoundHttpException
     */
    public function resize($requestedUrl)
    {
        $requestParser = $this->parseImageRequest($requestedUrl);
        $originalImgFullPath = $requestParser->getOriginalImageFullPath();
        $thumbnailImagePath = $requestParser->getThumbnailPath();
        if (!file_exists($originalImgFullPath)) {
            throw new NotFoundHttpException('Original image not found');
        }
        if (!file_exists($thumbnailImagePath)) {
            if ($this->isValidSize($requestParser)) {
                $thumbBox = new Box($requestParser->width, $requestParser->height);
                Image::getImagine()->open($originalImgFullPath)
                            ->thumbnail($thumbBox, ManipulatorInterface::THUMBNAIL_OUTBOUND)
                            ->save($thumbnailImagePath, [ 'quality' => 90 ]);
                $requestParser->thumbSaved = true;
            } else {
                $requestParser->width = ImageHtmlHelper::DEFAULT_WIDTH;
                $requestParser->height = ImageHtmlHelper::DEFAULT_HEIGHT;
            }
        }
        return $requestParser;
    }

    /**
     * @param $requestParser
     * @return bool
     */
    private function isValidSize($requestParser)
    {
        return $requestParser->width >= self::THUMB_MIN_WIDTH &&
        $requestParser->width <= self::THUMB_MAX_WIDTH &&
        $requestParser->height >= self::THUMB_MIN_HEIGHT &&
        $requestParser->height <= self::THUMB_MAX_HEIGHT;
    }

    /**
     *
     * @param string $requestedUrl
     * @return ImageServiceRequestParser
     */
    private function parseImageRequest($requestedUrl)
    {
        // /static/user/234234/model320/1610_100x120.jpg
        $result = new ImageServiceRequestParser(
            [
                'requestedUrl' => $requestedUrl
            ]
        );
        return $result;
    }
}
