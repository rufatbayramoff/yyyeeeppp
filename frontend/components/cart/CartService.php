<?php
/**
 * Created by mitaichik
 */

namespace frontend\components\cart;


use common\models\Cart;
use common\modules\product\models\ProductPlaceOrderState;
use common\modules\xss\helpers\XssHelper;
use frontend\models\model3d\PlaceOrderState;
use frontend\models\user\UserFacade;
use yii\base\Component;

/**
 * Class CartService
 *
 * @package frontend\components\cart
 */
class CartService extends Component
{
    /** @var PlaceOrderState */
    protected $placeOrderState;

    /** @var ProductPlaceOrderState */
    protected $productPlaceOrderState;

    public function injectDependencies(PlaceOrderState $placeOrderState, ProductPlaceOrderState $productPlaceOrderState)
    {
        $this->placeOrderState = $placeOrderState;
        $this->productPlaceOrderState = $productPlaceOrderState;
    }

    /**
     * @return bool
     */
    public function hasNotEmptyCarts()
    {
        $lastStateModel3d = $this->placeOrderState->getLastUpdatedState();
        $lastStateProduct = $this->productPlaceOrderState->getLastUpdatedState();
        return $lastStateModel3d || $lastStateProduct;
    }


    public function getCartLink()
    {
        $lastestState = null;
        $lastestStateUrl = null;
        $lastStateModel3d = $this->placeOrderState->getLastUpdatedState();
        if ($lastStateModel3d) {
            $lastestState = $lastStateModel3d;
            $lastestStateUrl = '/my/print-model3d' . ($lastStateModel3d['uid'] ? '?posUid=' . $lastStateModel3d['uid'] : '');
        }
        $lastStateProduct = $this->productPlaceOrderState->getLastUpdatedState();
        if ($lastStateProduct) {
            if ((!$lastestState) || ($lastestState && ($lastStateProduct['updateDate'] > $lastestState['updateDate']))) {
                $lastestState = $lastStateProduct;
                $lastestStateUrl = '/product/buy/'.$lastestState['productUuid'];
            }
        }
        return $lastestStateUrl;
    }

    /**
     * null or Cart object
     *
     * @param null $posUid
     * @return null|Cart
     */
    public function getSessionCart($posUid = null)
    {
        if (defined('TEST_XSS') && TEST_XSS) {
            $posUid = XssHelper::cleanXssProtect($posUid);
        }
        $posUid = $posUid ?: null;
        return Cart::findOne(['user_session_id' => UserFacade::getUserSession()->id, 'posUid' => $posUid]);
    }
}