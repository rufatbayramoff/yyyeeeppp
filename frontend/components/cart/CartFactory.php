<?php

namespace frontend\components\cart;

use common\components\order\PriceCalculator;
use common\models\Cart;
use common\models\CartItem;
use common\models\Model3dReplica;
use common\models\CompanyService;
use DateTime;
use DateTimeZone;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserFacade;
use frontend\widgets\Currency;
use lib\money\Money;


/**
 * CartFactory - adds to getCart function more loading information about product,
 * price and etc
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 * @author Dmitriy Strukov
 */
class CartFactory
{

    /**
     * @param null $posUid
     *
     * @return Cart
     * @throws \Exception
     */
    public static function createCart($posUid = null)
    {
        $userSession = UserFacade::getUserSession();
        $posUid = $posUid?:null; // Clean empty to null
        $cart = Cart::findOne(['user_session_id' => $userSession->id, 'posUid' => $posUid]);
        if (!$cart) {
            $cart = new Cart();
            $cart->user_session_id = $userSession->id;
            $cart->posUid = $posUid;
        }
        $cart->user_id = UserFacade::getCurrentUserId();
        $cart->update_date = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $cart->clear(); // because now cart can contains only one store unit
        return $cart;
    }

    /**
     * @param Model3dReplica $model3dReplica
     * @param CompanyService $machine
     * @param $qty
     * @param bool $canChangePs
     *
     * @return CartItem
     * @throws \yii\web\NotFoundHttpException
     */
    public static function createCartItem(Model3dReplica $model3dReplica, CompanyService $machine, $qty, $canChangePs = true)
    {
        $cartItem = new CartItem();
        $cartItem->setModel3dReplica($model3dReplica);
        $cartItem->setMachine($machine);
        $cartItem->price = 1;
        $cartItem->qty = 1;
        $cartItem->can_change_ps = (int)$canChangePs;
        $cartItem->setCurrencyIso(\lib\money\Currency::USD);
        return $cartItem;
    }
}
