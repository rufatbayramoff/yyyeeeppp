<?php
/**
 * Created by mitaichik
 */

namespace frontend\components\cart\exceptions;

/**
 * Throw when service cant find cart by hash
 * 
 * Class CartNotFoundException
 * @package frontend\components\cart\exceptions
 */
class CartNotFoundException extends CartException
{
    /**
     * CartNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('Cart not found');
    }
}