<?php
/**
 * Created by mitaichik
 */

namespace frontend\components\cart\exceptions;


use yii\base\Exception;

/**
 * Base cart exception
 * 
 * Class CartException
 * @package frontend\components\cart\exceptions
 */
abstract class CartException extends Exception
{

}