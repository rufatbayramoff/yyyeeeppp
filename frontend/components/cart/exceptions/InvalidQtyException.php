<?php
/**
 * Created by mitaichik
 */

namespace frontend\components\cart\exceptions;

/**
 * Throw when set invalid qty to cart
 *
 * Class InvalidQtyException
 * @package frontend\components\cart\exceptions
 */
class InvalidQtyException extends CartException
{
    /**
     * InvalidQtyException constructor.
     * @param int $qty
     */
    public function __construct($qty)
    {
        \Exception::__construct("Invalid qty in cart: {$qty}");
    }
}