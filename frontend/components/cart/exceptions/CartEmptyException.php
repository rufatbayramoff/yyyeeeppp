<?php
/**
 * Created by mitaichik
 */

namespace frontend\components\cart\exceptions;

/**
 * Class CartEmptyException
 *
 * This exception trow when operation with cart require not empty cart,
 * but cart is empty.
 *
 * @package frontend\components\cart\exceptions
 */
class CartEmptyException extends CartException
{
    /**
     * CartEmptyException constructor.
     */
    public function __construct()
    {
        parent::__construct('Cart is empty');
    }
}