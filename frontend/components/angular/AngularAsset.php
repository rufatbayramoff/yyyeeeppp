<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.10.16
 * Time: 11:18
 */

namespace frontend\components\angular;

use yii\web\AssetBundle;

/**
 * Asset for angular
 *
 * @package frontend\assets
 */
class AngularAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/web/js/ts/app';

    public $depends = [
        AngularLibAsset::class,
        AngularUiRouter::class
    ];

    public $js = [
        'app.js',
        'objects-storage.js',
        'object-to-form.js',
        'services/session-storage.js',
        'services/flushMessageManager.js',
        'services/formValidator.js'
    ];
}