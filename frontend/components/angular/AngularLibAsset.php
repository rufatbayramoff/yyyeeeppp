<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */
namespace frontend\components\angular;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Asset for angular
 * 
 * @package frontend\assets
 */
class AngularLibAsset extends AssetBundle
{

    public $sourcePath = '@bower';

    public $depends = [
        JqueryAsset::class,
    ];

    public $js = [

        'angular/angular.min.js',
        'angular-resource/angular-resource.min.js',
        'angular-i18n/angular-locale_en.js',
        'angular-ui-mask/dist/mask.min.js',
    ];
}