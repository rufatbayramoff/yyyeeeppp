<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.09.17
 * Time: 10:29
 */

namespace frontend\components\angular;

use yii\web\AssetBundle;

class AngularUiRouter extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/ts/angular-ui-router.min.js',
    ];
}