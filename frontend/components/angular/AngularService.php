<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace frontend\components\angular;

use common\models\PaymentCurrency;
use frontend\models\user\UserFacade;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * Service for work with angular frontend
 *
 * @package frontend\components\angular
 */
class AngularService
{
    const CONTROLLER_PARAMS_CONSTANT_NAME = 'controllerParams';

    public const ANGULAR_FRONT_JS = 'js/ts/app';
    public const ANGULAR_BACK_JS  = 'app';


    /**
     *
     * @var array
     */
    private $controllersParams = [];

    /**
     *
     * @var array
     */
    private $constants = [];

    /**
     *
     * @var bool
     */
    private $scriptRegistered = false;

    /**
     *
     * @var array
     */
    private $services = [];

    /**
     *
     * @var array
     */
    private $directives = [];

    /**
     *
     * @var array
     */
    private $controllers = [];

    /**
     *
     * @var array
     */
    private $resources = [];


    /**
     * @var array
     */
    protected $bundles = [];

    /**
     * @var array
     */
    protected $additionalModules = [];

    /**
     * Register controller param
     *
     * @param string $name
     * @param mixed $value
     * @param bool $checkNotSet
     * @return AngularService
     * @throws Exception
     */
    public function controllerParam($name, $value, $checkNotSet = true)
    {
        $this->checkIsNotRegistered();
        if ($checkNotSet && isset($this->controllersParams[$name])) {
            throw new Exception('Controller param already set');
        }

        $this->controllersParams[$name] = $value;
        return $this;
    }

    /**
     * Register controller params
     *
     * @param array $params
     * @param bool $checkNotSet
     * @return AngularService
     * @throws Exception
     */
    public function controllerParams(array $params, $checkNotSet = true)
    {
        foreach ($params as $name => $value) {
            $this->controllerParam($name, $value, $checkNotSet);
        }
        return $this;
    }

    /**
     * Register constants
     *
     * @param
     *            $name
     * @param
     *            $value
     * @param bool|true $checkNotSet
     * @return AngularService
     * @throws Exception
     */
    public function constant($name, $value, $checkNotSet = true)
    {
        $this->checkIsNotRegistered();

        if ($checkNotSet && isset($this->constants[$name])) {
            throw new Exception('Constant already set');
        }

        if ($name == self::CONTROLLER_PARAMS_CONSTANT_NAME) {
            throw new Exception('Cant register system constant name ' . self::CONTROLLER_PARAMS_CONSTANT_NAME);
        }

        $this->constants[$name] = $value;
        return $this;
    }

    /**
     * Register constants
     *
     * @param array $constants
     * @param bool|true $checkNotSet
     * @return AngularService
     * @throws Exception
     */
    public function constants(array $constants, $checkNotSet = true)
    {
        foreach ($constants as $name => $value) {
            $this->constant($name, $value, $checkNotSet);
        }
        return $this;
    }

    /**
     * Register service
     *
     * @param string|string[] $name
     * @return AngularService
     */
    public function service($name)
    {
        $this->checkIsNotRegistered();
        $this->services = ArrayHelper::merge($this->services, (array)$name);

        if (in_array('user', $this->services) && !isset($this->constants['$$userData'])) {
            $data = [
                'metricMeasure' => UserFacade::getCurrentMeasurement(),
                'currency'      => UserFacade::getCurrentCurrency(),
            ];
            $this->constant('$$userData', $data);
        }

        if (in_array('currency', $this->services) && !isset($this->constants['$$currencyData'])) {
            $this->constant('$$currencyData', ArrayHelper::toArray(PaymentCurrency::getAll(), [
                PaymentCurrency::class => [
                    'title',
                    'currency_iso',
                    'title_original'
                ]
            ]));
        }
        return $this;
    }

    /**
     * Register directive
     *
     * @param string|string[] $name
     * @return AngularService
     */
    public function directive($name)
    {
        $this->checkIsNotRegistered();
        $this->directives = ArrayHelper::merge($this->directives, (array)$name);
        return $this;
    }

    /**
     * Register resource
     *
     * @param string|string[] $name
     * @return AngularService
     */
    public function resource($name)
    {
        $this->checkIsNotRegistered();
        $this->resources = ArrayHelper::merge($this->resources, (array)$name);
        return $this;
    }

    /**
     * @param $name
     * @return $this
     * @throws Exception
     */
    public function controllerBackend($name)
    {
        $this->controller($name, self::ANGULAR_BACK_JS);
        return $this;
    }

    /**
     * Register controller
     *
     * @param string|string[] $name
     * @param string $modulePath
     * @return AngularService
     * @throws Exception
     */
    public function controller($name, $modulePath = '')
    {
        $this->checkIsNotRegistered();
        $controllerNames = [];
        if (!$modulePath) {
            $modulePath = self::ANGULAR_FRONT_JS;
        }
        foreach ((array)$name as $key => $oneName) {
            $controllerNames[$key] = $modulePath . '/' . $oneName;
        }
        $this->controllers = ArrayHelper::merge($this->controllers, $controllerNames);

        return $this;
    }

    /**
     * @param $additionalModules
     *
     * @return $this
     */
    public function additionalModules($additionalModules)
    {
        $this->additionalModules = array_merge($this->additionalModules, $additionalModules);
        return $this;
    }

    /**
     * @param $path
     */
    public function addPathToBundle($path)
    {
        $shortPath = substr($path, 0, strrpos($path, '/'));
        if ((strpos($shortPath, self::ANGULAR_FRONT_JS) === 0) || (strpos($shortPath, self::ANGULAR_BACK_JS) === 0)) {
            $bungleKey = 'angularMain';
            if (array_key_exists('angularMain', $this->bundles)) {
                $bundle = $this->bundles['angularMain'];
            } else {
                $bundle = new AssetBundle([
                    'basePath' => '@webroot',
                    'baseUrl'  => '@web',
                    'depends'  => [AngularLibAsset::class],
                ]);
            }
            $bundle->js[] = $path;
        } else {
            $bungleKey = 'angular_' . $shortPath;
            if (array_key_exists($bungleKey, $this->bundles)) {
                $bundle = $this->bundles[$bungleKey];
            } else {
                $bundle = new AssetBundle([
                    'sourcePath' => $shortPath,
                    'depends'    => [AngularLibAsset::class],
                ]);
            }
            $js = substr($path, strrpos($path, '/') + 1, 1024);
            $bundle->js[] = $js;
        }
        $this->bundles[$bungleKey] = $bundle;
    }

    /**
     * Register angular scripts
     *
     * @param View $view
     * @throws Exception
     */
    public function registerScripts(View $view)
    {
        if (!$this->getIsNeedRegisterScripts()) {
            return;
        }

        $this->checkIsNotRegistered();

        $this->redisterAdditinalModules($view);
        $this->registerConstantsScript($view);
        $this->registerControllerParamsScript($view);

        $this->registerServicesScript();
        $this->registerDirectivesScript();
        $this->registerControllersScript();
        $this->registerResourcesScript();

        foreach ($this->bundles as $key => $bundle) {
            $view->getAssetManager()->bundles[$key] = $bundle;
            $view->registerAssetBundle($key);
            if ($bundle->sourcePath) {
                $am = \Yii::$app->getAssetManager();
                $bundle->publish($am);
            }
        }
        $this->scriptRegistered = true;
    }

    /**
     *
     * @param View $view
     */
    private function registerConstantsScript(View $view)
    {
        foreach ($this->constants as $name => $value) {
            $this->registerConstantScript($view, $name, $value);
        }
    }

    /**
     *
     * @param View $view
     */
    private function registerControllerParamsScript(View $view)
    {
        if (!empty($this->controllersParams)) {
            $this->registerConstantScript($view, self::CONTROLLER_PARAMS_CONSTANT_NAME, $this->controllersParams);
        }
    }

    /**
     */
    private function registerServicesScript()
    {
        foreach (array_unique($this->services) as $service) {
            $this->addPathToBundle("js/ts/app/services/{$service}.js");
        }
    }

    /**
     */
    private function registerDirectivesScript()
    {
        foreach (array_unique($this->directives) as $directive) {
            $this->addPathToBundle("js/ts/app/directives/{$directive}.js");
        }
    }

    /**
     */
    private function registerControllersScript()
    {
        foreach (array_unique($this->controllers) as $controller) {
            $this->addPathToBundle($controller . '.js');
        }
    }

    /**
     */
    private function registerResourcesScript()
    {
        foreach (array_unique($this->resources) as $resource) {
            $this->addPathToBundle('js/ts/app/resources/' . $resource . '.js');
        }
    }

    protected function redisterAdditinalModules(View $view)
    {
        $view->registerJs('window.angularAdditionalModules = '.json_encode($this->additionalModules), View::POS_BEGIN);
    }


    /**
     *
     * @param View $view
     * @param string $name
     * @param mixed $value
     */
    private function registerConstantScript(View $view, $name, $value)
    {
        $view->registerJs('app.constant(' . Json::encode($name) . ', ' . Json::encode($value, JSON_UNESCAPED_UNICODE) . ');', View::POS_END);
    }

    /**
     * Check that scripst not registered yet
     *
     * @throws Exception
     */
    private function checkIsNotRegistered()
    {
        if ($this->scriptRegistered) {
            throw new Exception("Angular service already registered scripts");
        }
    }

    /**
     * If need register scripts
     *
     * @return bool
     */
    private function getIsNeedRegisterScripts()
    {
        return $this->controllersParams || $this->constants || $this->services || $this->directives || $this->controllers || $this->resources;
    }
}
