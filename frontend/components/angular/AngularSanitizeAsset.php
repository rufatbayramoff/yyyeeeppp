<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.09.17
 * Time: 10:29
 */

namespace frontend\components\angular;

use yii\web\AssetBundle;

class AngularSanitizeAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/js';

    public $js = [
        'angular-sanitize.min.js',
    ];

    public $depends = [
        AngularLibAsset::class,
    ];
}