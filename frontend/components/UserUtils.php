<?php

namespace frontend\components;

use cebe\gravatar\Gravatar;
use common\models\base\PsPrinter;
use common\models\Company;
use common\models\User;
use common\models\UserCollection;
use frontend\components\image\ImageHtmlHelper;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * User utils functions.
 * get default bg, and etc.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UserUtils
{

    const AVATAR_TYPE = 'retro';

    /**
     * @param User $user
     * @return array
     */
    public static function getUserPublicProfileRoute(User $user)
    {
        return ['/user/u/profile', 'username' => $user->username];
    }

    /**
     * @param User $user
     * @param bool $withServer
     * @return string
     */
    public static function getUserPublicProfileUrl(User $user, $withServer = true)
    {
        $server = $withServer?param('server'):'';
        return $server.Url::toRoute(['/user/u/profile', 'username' => $user->username]);
    }

    /**
     * @param User $user
     * @return string
     */
    public static function getUserCollectionsRoute(User $user)
    {
        return Url::toRoute(['/user/u/collections', 'username' => $user->username]);
    }

    /**
     * @param User $user
     * @param PsPrinter $printer
     * @return string
     */
    public static function getUserPrintersMapUrl(User $user, PsPrinter $printer)
    {
        return param('server').Url::toRoute(['/user/u/ps-map', 'username' => $user->username, 'printerId' => $printer->id]);
    }

    /**
     * @param UserCollection $collection
     * @return array
     */
    public static function getUserCollectionRoute(UserCollection $collection)
    {
        return ['/user/u/collection', 'username' => $collection->user->username, 'collectionId' => $collection->id];
    }

    /**
     * @param User $user
     * @param bool $widget
     * @param int $count
     * @param int|null $categoryId
     * @param string|null $sort
     * @return string
     */
    public static function getUserStoreUrl(User $user, $widget = false, $count = 120, $categoryId = null, $sort = null)
    {
        $route = ['/user/u/store', 'username' => $user->username];
        if($user->ps){
            $ps = $user->ps[0];
            $route = ['/c/public/designer', 'username' => $ps->url];
        }
        if ($widget) {
            $route['widget'] = 1;
        }

        if ($count != 120) {
            $route['count'] = $count;
        }

        if ($categoryId) {
            $route['categoryId'] = $categoryId;
        }

        if ($sort) {
            $route['sort'] = $sort;
        }

        return Url::toRoute($route);
    }

    public static function getUserProductUrl(User $user, $widget = false, $count = 120, $categoryId = null, $sort = null)
    {
        $route = ['/user/u/store', 'username' => $user->username];
        if($user->ps){
            $ps = $user->ps[0];
            $route = ['/c/public/products', 'username' => $ps->url];
        }
        if ($widget) {
            $route['widget'] = 1;
        }

        if ($count != 120) {
            $route['count'] = $count;
        }

        if ($categoryId) {
            $route['categoryId'] = $categoryId;
        }

        if ($sort) {
            $route['sort'] = $sort;
        }

        return Url::toRoute($route);
    }

    /**
     * get user avatar
     * if empty - gets avatar from gravatar based on email
     * if user doesn't have avatar on gravatar - uses default image - monsterid
     *
     * @param string $avatarUrl
     * @param string $email
     * @param string $mode
     * @param bool $withoutCache
     * @return string
     * @throws \Exception
     */
    public static function getAvatar($avatarUrl, $email, $mode = 'profile', $withoutCache = false)
    {
        $options = [];
        $size = 64;
        if ($mode == 'profile') {
            $size = 128;
            $options = [
                'align' => 'left',
                'alt' => '',
                'class' => "img-circle"
            ];
        } else if ($mode == 'chat') {
            $options = [
                'class' => 'direct-chat-img'
            ];
        } else if ($mode == 'print-request') {
            $options = [
                'class' => 'print-request-user-img'
            ];
        } else if ($mode == 'top-profile') {
            $options = [
                'class' => 'img-circle',
                'align' => 'left'
            ];
        } else if ($mode == 'author') {
            $size = 100;
        }
        if (empty($avatarUrl)) {
            if ($mode === 'url') {
                $avatar = Yii::createObject(
                    [
                        'class' => Gravatar::class,
                        'secure'       => true,
                        'email'        => $email,
                        'defaultImage' => self::AVATAR_TYPE,
                        'options'      => $options,
                        'size'         => $size
                    ]
                );
                return $avatar->getImageUrl();
            }
            $avatar = Gravatar::widget([
                'secure' => true,
                'email' => $email,
                'defaultImage' => self::AVATAR_TYPE,
                'options' => $options,
                'size' => $size
            ]);
        } else {
            $cachePrefix = '';
            if(strpos($avatarUrl,'https://')!==false || strpos($avatarUrl,'http://sandbox')!==false) {
            }else{
                $avatarPath  = Yii::getAlias('@static') . $avatarUrl;
                $avatarUrl = Yii::getAlias(Yii::$app->params['staticUrl']) . $avatarUrl;
                $avatarUrl = image\ImageHtmlHelper::getThumbUrl($avatarUrl, $size, $size);

                if($withoutCache){
                    $cachePrefix = '?mcache=' . @filemtime($avatarPath);
                }
            }
            $avatar = Html::img($avatarUrl . $cachePrefix, $options);
        }
        if($mode==='url'){
            if(empty($avatarUrl)){
                preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $avatar, $matches);
                $avatarUrl =  $matches[1];
            }
            return $avatarUrl;
        }
        return $avatar;
    }

    /**
     * get avatar url
     * 
     * @param User $user
     * @param int $size
     * @return string
     */
    public static function getAvatarUrl(User $user, $size = 64)
    {
        $avatarUrl = $user->userProfile ? $user->userProfile->avatar_url : '';
        if(empty($avatarUrl)){
            $grav = new Gravatar([
                'secure' => true,
                'email' => $user->email,
                'defaultImage' => self::AVATAR_TYPE, 
                'size' => $size
            ]);
            $avatarUrl = $grav->getImageUrl();
        }else{
            $avatarUrl = Yii::getAlias(Yii::$app->params['staticUrl']) . $avatarUrl;
            $avatarUrl = ImageHtmlHelper::getThumbUrl($avatarUrl, $size, $size);
        }
        return $avatarUrl;
    }

    public static function getCompanyAvatarUrl(User $user)
    {
        if ($user->company) {
            return $user->company->getCompanyLogo(false, 32);
        }
        return UserUtils::getAvatarUrl($user);
    }
    
    /**
     * Return avavtar for user
     * 
     * @param User $user
     * @param string $mode            
     * @return string
     */
    public static function getAvatarForUser(User $user, $mode = 'profile')
    {
        $avatarUrl = $user->userProfile ? $user->userProfile->avatar_url : '';
        $email = $user->email;
        if(empty($user->email)){
            $email = $user->id . 'user@treatstock.com';
        }
        $avatar = \frontend\components\UserUtils::getAvatar($avatarUrl, $email, $mode);
        //$avatar = str_replace('/static', \Yii::$app->params['staticUrl'], $avatar);
        return $avatar;
    }

    /**
     * get default user cover image
     *
     * @param string $bg
     * @return string
     */
    public static function getCover($bg)
    {
        if (! empty($bg)) {
            $bg = Yii::getAlias(Yii::getAlias(Yii::$app->params['staticUrl'])) . $bg;
            return $bg;
        }
        return '/static/images/profile-default-bg.jpg';
    }

    /**
     *
     * @param int $num
     * @return string
     */
    public static function getColor($num)
    {
        $hash = md5('ddddfff' . $num);
        return self::rgb2hex([
            hexdec(substr($hash, 0, 2)),
            hexdec(substr($hash, 2, 2)),
            hexdec(substr($hash, 6, 2) / 2)
        ]); // b
    }

    /**
     *
     * @param int[] $rgb
     * @return string
     */
    public static function rgb2hex($rgb)
    {
        $hex = "#";
        $hex .= str_pad(dechex($rgb[0]), 2, "1", STR_PAD_LEFT);
        $hex .= str_pad(dechex($rgb[1]), 2, "d", STR_PAD_LEFT);
        $hex .= str_pad(dechex($rgb[2]), 2, "f", STR_PAD_LEFT);
        return $hex;
    }
}
