<?php
/**
 * User: nabi
 */

namespace frontend\components;


use common\components\BaseController;
use common\models\User;
use common\models\user\UserIdentityProvider;

/**
 * Class AuthedController
 *
 * use it if all controller actions requires User
 *
 * @package frontend\components
 */
class AuthedController extends BaseController
{
    protected $enableSeo = false;

    /**
     * @var User
     */
    public $user;

    public function init()
    {
        parent::init();
        $identityProvider = \Yii::createObject(UserIdentityProvider::class);
        $this->user = $identityProvider->getUser();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }
}