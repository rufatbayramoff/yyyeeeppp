<?php

namespace frontend\components;

use common\components\UserSession;
use common\models\factories\LocationFactory;
use common\models\UserAddress;
use common\models\UserLocation;
use frontend\models\user\UserFacade;
use frontend\models\user\UserLocator;
use lib\geo\models\Location;
use lib\MeasurementUtil;
use lib\money\Currency;

/**
 * UserSessionFacade accumulates all logic to work with session objects.
 * So any object which is tied to user session, should be used from this
 * session facade.
 *
 * GENERAL (always required to be):
 * - Geo location
 * - Current printer (defaults Geo selected printer) - use can change printer by selecting another
 * - Current user
 * - Current rate
 * - Current currency
 * - Current measurement
 * - Current language
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserSessionFacade
{

    /**
     * object to work with user
     *
     * @var \frontend\models\user\FrontUser
     */
    private static $user;

    /**
     * current user location
     *
     * @var Location
     */
    private static $userLocation;

    /**
     * Returns timezone, if user is signed in, tries to get timezone from profile
     * if not, gets from geo location by ip returned by MaxMind db.
     *
     * @return string - i.e. America/Panama, Europe/Moscow
     */
    public static function getTimezone()
    {
        $location = self::getLocation();
        $timeZone = $location['timezone'];
        if (!is_guest()) {
            $user = UserFacade::getCurrentUser();
            if (!empty($user->userProfile) && !empty($user->userProfile->timezone_id)) {
                $timeZone = $user->userProfile->timezone_id;
            }
        }
        return $timeZone;
    }

    /**
     * Get user location
     *
     * @return \lib\geo\models\Location
     */
    public static function getLocation()
    {
        if (self::$userLocation) {
            return self::$userLocation;
        }

        if ($location = self::getLocationFromSession()) { // from session or from cookie
            if (!is_guest() && app('request')->getCookies()->getValue('ck_location')) {
                self::clearLocationFromCookie();
                // set to session
                self::setLocationToSession($location);
                self::updateUserProfile(UserLocator::createByLocation($location));
            }
            self::$userLocation = $location;
            return $location;
        }
        if (!is_guest()) {
            $address = UserAddress::getLatestShip(UserFacade::getCurrentUserId());
            if ($address) {
                self::$userLocation = LocationFactory::createFromUserAddress(reset($address));
                return self::$userLocation;
            }
        }

        $location = \Yii::$app->geo->getLocationByDomainOrIp($_SERVER['HTTP_HOST'], UserSessionFacade::getSessionIp());

        self::$userLocation = $location;

        if ($location->confidenceLevel !== Location::CONFIDENCE_LEVEL_NOT_DETECTED) {
            self::setLocationToSession($location);
        }

        return $location;
    }

    public static function getLocationAsString(Location $location = null)
    {
        $location = $location ?: self::getLocation();
        $result = [$location->city, $location->region, $location->country];
        $result = array_map(
            function ($value) {
                return str_replace([" ", ",", "/", "'", "\""], "-", $value);
            },
            $result
        );
        return implode("--", $result);
    }

    /**
     * Set specific user location to cookie, session, cache
     *
     * @param Location $location
     */
    public static function setLocationToSession(Location $location)
    {
        self::$userLocation = $location;
        app('session')->set('session_location', (array)$location);
    }

    /**
     * Set location to cookie
     *
     * @param Location $location
     */
    private static function setLocationToCookie(Location $location)
    {
        // set cookie
        $cookie = new \yii\web\Cookie([
            'name'   => 'ck_location',
            'value'  => serialize((array)$location),
            'path'   => '/',
            'expire' => time() + 86400 * 365
        ]);
        app('response')->getCookies()->add($cookie);
    }

    private static function clearLocationFromCookie()
    {
        $cookie = new \yii\web\Cookie([
            'name'  => 'ck_location',
            'value' => '',
            'path'  => '/'
        ]);
        app('response')->getCookies()->remove($cookie);
    }

    /**
     * set user location by locator
     * remember location in cookies
     *
     * @param \frontend\models\user\UserLocator $userLocator
     */
    public static function setLocationByLocator(\frontend\models\user\UserLocator $userLocator)
    {
        $location = new Location([
            'lat'             => $userLocator->lat,
            'lon'             => $userLocator->lon,
            'city'            => $userLocator->getCity(),
            'region'          => (string)$userLocator->getState(),
            'country'         => $userLocator->getCountry(),
            'timezone'        => self::getTimezone() ?: "UTC",
            'confidenceLevel' => Location::CONFIDENCE_LEVEL_SELECTED
        ]);

        UserLocation::factoryGeoCity($location);

        // update users profile location
        self::updateUserProfile($userLocator);
        self::setLocationToSession($location);
        //self::setLocationToCookie($location);
    }

    /**
     *
     * @param \frontend\models\user\UserLocator $userLocator
     * @return bool
     */
    private static function updateUserProfile(\frontend\models\user\UserLocator $userLocator)
    {
        $currentUserId = UserFacade::getCurrentUserId();
        if ($currentUserId) {
            $userAddress = \frontend\models\user\UserAccountAddressForm::findOne([
                'user_id' => $currentUserId,
                'type'    => 'profile'
            ]);

            $country = \common\models\GeoCountry::findOne(['iso_code' => $userLocator->getCountry()]);
            if (!$country) {
                return false;
            }

            if ($userAddress) {
                $userAddress->country_id = $country->id;
                $userAddress->city = $userLocator->getCity();
                $userAddress->region = (string)$userLocator->getState();
                $userAddress->address = '';
                $userAddress->extended_address = '';
                $userAddress->zip_code = '';
                $userAddress->safeSave();
            } else {
                $region = $userLocator->getState();
                if (empty($region)) {
                    $region = ' ';
                }
                \common\models\UserAddress::addRecord([
                    'user_id'    => $currentUserId,
                    'country_id' => $country->id,
                    'city'       => $userLocator->getCity(),
                    'region'     => $region,
                    'address'    => '-',
                    'type'       => 'profile',
                ]);
            }
        }
        return null;
    }

    /**
     *
     * @return Location
     */
    private static function getLocationFromSession()
    {
        if ($locationData = app('session')->get('session_location')) {
            return new Location($locationData);
        }

        if ($locationData = app('request')->getCookies()->getValue('ck_location')) {
            return new Location(unserialize($locationData));
        }

        return null;
    }

    /**
     * get session ip - used for tests
     *
     * @return string
     */
    public static function getSessionIp()
    {
        $ip = app('session')->get('session_ip', false);
        if (!$ip) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    /**
     * set session ip - used for tests
     *
     * @param string $ip
     */
    public static function setSessionIp($ip)
    {
        app('session')->set('session_ip', $ip);
    }

    /**
     * get current user
     *
     * @return \frontend\models\user\FrontUser|bool
     * @deprecated Use UserFacade::getCurrentUser() instead
     */
    public static function getUser()
    {
        self::$user = self::$user ? self::$user : UserFacade::getCurrentUser();
        return self::$user;
    }

    /**
     * get current currency
     *
     * @return string
     */
    public static function getCurrency()
    {
        if (is_guest()) {
            return app('session') ? app('session')->get('current_currency', Currency::USD) : Currency::USD;
        }

        $user = UserFacade::getCurrentUser();

        if (isset($user->userProfile)) {
            return $user->userProfile->current_currency_iso;
        }

        return Currency::USD;
    }

    /**
     * get currency symbol - USD - $, EUR - e and etc.
     *
     * @param bool|string $currency
     * @return string
     */
    public static function getCurrencySymbol($currency = false)
    {
        $userCurrency = $currency ? $currency : self::getCurrency();
        $locale = \Yii::$app->language;
        $fmt = new \NumberFormatter($locale . "@currency=$userCurrency", \NumberFormatter::CURRENCY);
        $symbol = $fmt->getSymbol(\NumberFormatter::CURRENCY_SYMBOL);
        return $symbol;
    }

    /**
     * @return bool
     */
    public static function isUsMeasurement()
    {
        return UserSessionFacade::getMeasurement() === MeasurementUtil::IN;
    }

    /**
     * get current measurement
     *
     * @return string
     */
    public static function getMeasurement()
    {
        $result = app('session')->get('current_measurement', 'in');
        if (!is_guest() && isset(UserFacade::getCurrentUser()->userProfile)) {
            $userProfile = UserFacade::getCurrentUser()->userProfile;
            $result = $userProfile->current_metrics;
        }
        return $result;
    }

    /**
     * set measurement - for user or session
     *
     * @param string $measurement
     */
    public static function setMeasurement($measurement)
    {
        app('session')->set('current_measurement', $measurement);

        if (!is_guest() && isset(UserFacade::getCurrentUser()->userProfile)) {
            $userProfile = UserFacade::getCurrentUser()->userProfile;
            $userProfile->timezone_id = $userProfile->timezone_id ?: 'UTC';
            $userProfile->current_metrics = $measurement;
            $userProfile->safeSave();
        }
    }

    /**
     * get current language
     *
     * @return string
     */
    public static function getLanguage()
    {
        return \Yii::$app->language;
    }
}
