<?php

namespace frontend\components;

class Icon
{

    public static $icons = [
        'collections' => '<span class="tsi tsi-heart"></span> ',
        'mystore' => '<span class="tsi tsi-shopping-cart"></span> ',
        'printservice' => '<span class="tsi tsi-printer3d"></span> ',
        'messages' => '<span class="tsi tsi-chat"></span> ',
        'social' => '<span class="tsi tsi-thumbs-up"></span> ',
        'profile' => '<span class="tsi tsi-user"></span> ',
        'settings' => '<span class="tsi tsi-cogwheel"></span> ',
        'delete' => '<span class="tsi tsi-bin"></span> '
    ];

    public static $images_path = [
        'place_item' => '/static/images/map/place_item.png',
        'place_level_1' => '/static/images/map/place_level_1.png',
        'place_level_2' => '/static/images/map/place_level_2.png',
        'place_level_3' => '/static/images/map/place_level_3.png'
    ];

    public static function get($icon)
    {
        if (! isset(self::$icons[$icon])) {
            return "<span class='tsi tsi-" . $icon . "'></span>";
        }
        return self::$icons[$icon];
    }

    public static function getPath($image)
    {
        return self::$images_path[$image];
    }
}
