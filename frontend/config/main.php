<?php

use frontend\components\FrontendWebView;
use yii\web\Response;

$params = array_merge(require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php'));
if (file_exists(__DIR__ . '/../../common/config/auth-clients-local.php')) {
    $authClients = require(__DIR__ . '/../../common/config/auth-clients-local.php');
} else {
    $authClients = require(__DIR__ . '/../../common/config/auth-clients.php');
}
$authClients['google']['returnUrl']   = $params['siteUrl'] . '/user/after-remote-login/google';
$authClients['facebook']['returnUrl'] = $params['siteUrl'] . '/user/after-remote-login/facebook';

$rules = require(__DIR__ . '/url-rules.php');

require(__DIR__ . '/di.php');

$components = [
    'apiUploader'          => [
        'class' => 'lib\upload\TsApiUploader',
        'url'   => $params['apiUrl']
    ],
    'user'                 => [
        'identityClass'   => 'frontend\models\user\FrontUser',
        'enableAutoLogin' => true,
        'loginUrl'        => '/user/login',
        'identityCookie'  => [
            'name' => '_frontendUser'
        ]
    ],
    'view'                 => [
        'class' => FrontendWebView::class,
    ],
    'authClientCollection' => [
        'class'   => 'yii\authclient\Collection',
        'clients' => $authClients
    ],
    'session'              => [
        // 'class' => 'yii\web\DbSession',
        'name' => 'PHPFRONTSESSID'
    ]
    // 'db' => 'db',
    // 'sessionTable' => 'user_session',
    ,
    'request'              => [
        'cookieValidationKey' => 'd540zs;jd803.zlqwozdf',
        'csrfParam'           => '_frontendCSRF',
        'parsers'             => [
            'application/json' => 'yii\web\JsonParser'
        ]
    ],
    'urlManager'           => [
        'class'           => \common\components\UrlManager::class,
        'enablePrettyUrl' => true,
        'showScriptName'  => false,
        'rules'           => $rules
    ],
    'i18n'                 => [
        'translations' => [
            '*' => [
                'class'                 => 'yii\i18n\DbMessageSource',
                'sourceMessageTable'    => '{{%system_lang_source}}',
                'messageTable'          => '{{%system_lang_message}}',
                'on missingTranslation' => [
                    'common\components\TranslationEventHandler',
                    'handleMissingTranslation'
                ]
            ]
        ]
    ],
    'errorHandler'         => [
        'class'       => \frontend\components\ErrorHandler::class,
        'errorAction' => 'site/error'
    ],
    'angular'              => [
        'class' => \frontend\components\angular\AngularService::class
    ],
    'cart'                 => [
        'class' => \frontend\components\cart\CartService::class
    ],
    'assetManager'         => [
        'appendTimestamp' => true
    ],
    'qr'                   => [
        'class' => '\Da\QrCode\Component\QrCodeComponent',
        // ... you can configure more properties of the component here
    ],
    'sentry'               => [
        'class'     => '\lib\sentry\Sentry',
        'enabled'   => true,
        'phpLogger' => [
            'dsn'       => 'https://ed7d6b5df3ed4f63a2034be01b9200d4@o4504125362798592.ingest.sentry.io/4504210450939904',
            'dsnTrash' => 'https://ed7d6b5df3ed4f63a2034be01b9200d4@o4504125362798592.ingest.sentry.io/4504210450939904',
            'logger'    => 'frontend-php',
            'levels'    => ['error'],
        ],
        'jsLogger'  => [
            'dsn'       => 'https://ed7d6b5df3ed4f63a2034be01b9200d4@o4504125362798592.ingest.sentry.io/4504210450939904',
            'logger'    => 'frontend-js',
        ],
    ]
];
return [
    'id'                  => 'app-frontend',
    'name'                => $params['name'],
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => [
        'log',
        'sentry',
        'affiliate',
        'comments',
        [
            'class'   => 'yii\filters\ContentNegotiator',
            'formats' => [
                'text/plain'       => Response::FORMAT_RAW,
                'text/html'        => Response::FORMAT_HTML,
                'application/json' => Response::FORMAT_JSON
            ]
        ]
    ],
    'on beforeRequest'    => function () {
        if (app('request')->getUrl() == '/ps148') {
            $ps = \common\models\Ps::findByPk(148);
            app('response')->redirect(\frontend\models\ps\PsFacade::getPsRoute($ps))->send();
            exit(0);
        }
        if (param('siteMode', '') === 'down') {
            if (substr(app('request')->getUrl(), 0, 5) === '/api/') {
                echo '{"success": false, "message": "Site is down"}';
                exit(0);
            }
            $letMeIn = Yii::$app->session['ts_skip'] || isset($_GET['ts_skip']);
            if (!$letMeIn) {
                Yii::$app->catchAll = [
                    'site/down',
                ];
            } else {
                Yii::$app->session['ts_skip'] = 1;
            }
        }
    },
    'controllerNamespace' => 'frontend\controllers',
    'components'          => $components,
    'modules'             => [
        'preorder'             => [
            'class' => \frontend\modules\preorder\PreorderModule::class
        ],
        'mybusiness'           => [
            'class'        => \frontend\modules\mybusiness\MybusinessModule::class,
            'defaultRoute' => 'company',
            'modules'      => [
                'products'         => [
                    'class'        => \frontend\modules\mybusiness\modules\product\ProductModule::class,
                    'defaultRoute' => 'products'
                ],
                'company-services' => [
                    'class'        => \frontend\modules\mybusiness\modules\services\ServicesModule::class,
                    'defaultRoute' => 'company-services'
                ],
                'cs-window'        => [
                    'class'        => 'frontend\modules\mybusiness\modules\CsWindow\CsWindowModule',
                    'defaultRoute' => 'manage'
                ],
                'cutting'          => [
                    'class' => \frontend\modules\mybusiness\modules\cutting\CuttingModule::class
                ],
            ]
        ],
        'captcha'              => [
            'class'            => \common\modules\captcha\CaptchaModule::class,
            'googlePublicKey'  => '6LddAjUUAAAAAK6syp59F28KU8COgLof9D727OlY',
            'googlePrivateKey' => '6LddAjUUAAAAADozCUilUH_NHMInarTceiLUSAY6',
        ],
        'product'              => [
            'class' => \frontend\modules\product\ProductModule::class,
        ],
        'comments'             => [
            'class' => 'common\modules\comments\Comments',
        ],
        'affiliate'            => [
            'class' => 'common\modules\affiliate\AffiliateModule',
        ],
        'payment'              => [
            'class' => 'common\modules\payment\PaymentModule',
        ],
        'api'                  => [
            'class'   => 'yii\base\Module',
            'modules' => [
                'v2' => [
                    'class'        => 'common\modules\api\v2\ApiModule',
                    'basePath'     => '@common/modules/api/v2',
                    'uploadFolder' => '/api/' . date('Ym')
                ]
            ]
        ],
        'thingprint'           => [
            'class' => \common\modules\thingPrint\ThingPrintModule::class
        ],
        'catalogps'            => [
            'class' => \common\modules\catalogPs\CatalogPsModule::class
        ],
        'machines'             => [
            'class' => \common\modules\equipments\EquipmentsModule::class
        ],
        'onshape'              => [
            'class' => \common\modules\onshape\OnshapeModule::class
        ],
        'profile'              => [
            'class'        => \frontend\modules\profile\ProfileModule::class,
            'defaultRoute' => 'profile'
        ],
        'workbench'            => [
            'class'        => \frontend\modules\workbench\WorkbenchModule::class,
            'defaultRoute' => 'workbench'
        ],
        'companies'            => [
            'class'        => \common\modules\company\CompanyModule::class,
            'defaultRoute' => 'companies'
        ],
        'informer'             => [
            'class' => \common\modules\informer\InformerModule::class
        ],
        'ts-internal-purchase' => [
            'class' => \frontend\modules\tsInternalPurchase\TsInternalPurchaseModule::class
        ],
        'test'                 => [
            'class' => \frontend\modules\test\TestModule::class
        ],
    ],
    'params'              => $params,
];
