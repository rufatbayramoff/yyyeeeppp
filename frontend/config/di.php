<?php
/**
 * Dependency container configuration
 */

/**
 * Configure pager
 */
\Yii::$container->set(yii\widgets\LinkPager::className(), [
    'prevPageLabel' => '<span class="tsi tsi-left"></span>',
    'nextPageLabel' => '<span class="tsi tsi-right"></span>'
]);

/**
 * Configure pager
 */
\Yii::$container->set(yii\widgets\Pjax::class, [
    'timeout' => 30000,
]);

