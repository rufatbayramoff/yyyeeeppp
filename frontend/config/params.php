<?php
return [
    /**
     * what upload type to use.
     * local|remote (later api|aws|cdn)
     */
    'uploadType' => 'local',
    'uploadPath' => '@static',

    'officeIp'          => '83.69.106.68',
    'cookieExpire'      => 60 * 60 * 24 * 30,

    'allow_loginas_any_user' => 1,

    'backendAccessKey' => 'test',
  //  'printers_limit_count' =>302
];
