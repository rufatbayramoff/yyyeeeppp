<?php

use yii\web\GroupUrlRule;
use yii\web\UrlNormalizer;

return [
    //'site/lang/<lang>' => 'site/lang',
    'site/currency/<code>'                 => 'site/currency',
    'site/image/<path>'                    => 'site/image',
    'site/<action>/<id>'                   => 'site/<action>',
    'user/<action>'                        => 'user/user/<action>',
    'user/after-remote-login/<authclient>' => 'user/user/after-remote-login',

    'c/<username>'                           => 'c/public/profile',
    'c/<username>/3d-printing-service'       => 'c/public/print-services',
    'c/<username>/3d-printable-models-store' => 'c/public/designer',
    'c/<username>/service/<id:\d+>-<slug>'   => 'c/public/service-view',
    'c/<username>/widget/rate'               => 'c/widget/rate',
    'c/<username>/widget/rate-wide'          => 'c/widget/rate-wide',
    'c/<username>/<action>'                  => 'c/public/<action>',
    'c/<username>/<action>/<id:\d+>'         => 'c/public/<action>',


    'u'                                                             => 'user/u',
    'u/<username>'                                                  => 'user/u/profile',
    'u/<username>/collection/<collectionId>'                        => 'user/u/collection',
    'u/<username>/<action:(profile|collection|ps-map|collections)>' => 'user/u/<action>',
    '3d-printable-models-store/<username>'                          => 'user/u/store',
    '3d-printing-services/professional-3d-printing-services'        => 'catalogps/ps-catalog/professional-services',
    '3d-printing-services/best-3d-printing-services'                => 'catalogps/ps-catalog/best-printing-services',
    [
        'class'       => GroupUrlRule::class,
        'prefix'      => 'reviews',
        'routePrefix' => 'catalogps/ps-catalog',
        'rules'       => [
            'category-<category:(3d-printing|cnc-manufacturing|cutting)>/material-<material>/technology-<technology>' => 'reviews',
            'category-<category:(3d-printing|cnc-manufacturing|cutting)>/technology-<technology>'                     => 'reviews',
            'category-<category:(3d-printing|cnc-manufacturing|cutting)>/material-<material>'                         => 'reviews',
            'category-<category:(3d-printing|cnc-manufacturing|cutting)>'                                             => 'reviews',
            'material-<material>/technology-<technology>'                                                             => 'reviews',
            'material-<material>'                                                                                     => 'reviews',
            'technology-<technology>'                                                                                 => 'reviews',
            ''                                                                                                        => 'reviews',
        ],
    ],
    '3d-printing-services/<id:\d+>-<slug>'                          => 'user/u/printservice',

    'ps/<psUrl>' => 'user/u/printservice-new',

    'user-profile'          => 'profile/profile/index',
    'user-profile/<action>' => 'profile/profile/<action>',

    'products/all'             => 'product/product/index',
    'products/<category>'      => 'product/product/category',
    'products'                 => 'product/home/index',

    '3d-printable-models/get-printers-list' => 'catalog/model3d/get-printers-list',
    '3d-printable-models/<id>-<slug>'       => 'catalog/model3d/item',
    '/3d-printable-models/<id>-<slug>'      => '/catalog/model3d/item',


    // @TODO turn on these url rewrite after implementing new category page
    [
        'pattern' => '3d-printable/<slug>-<category>',
        'route'   => 'product/product/index',
        'suffix'  => '',
    ],
    '3d-printable/<slug>-<category>'        => 'product/product/index',
    [
        'pattern' => '3d-printable-models',
        'route'   => 'product/product/index',
        'suffix'  => '/',
    ],
    '3d-printable-models'                   => 'product/product/index',


    '3dmodels/<id>'  => 'catalog/model3d/item',
    '/3dmodels/<id>' => '/catalog/model3d/item',


    'model3d/<action>'         => 'catalog/model3d/<action>',
    'catalog/printer/<action>' => 'catalog/printer/<action>',
    'catalog/printer/<id>'     => 'catalog/printer/index',
    'my/model/delete-part'     => 'my/model3d/delete-part',
    'my/model/delete-image'    => 'my/model3d/delete-image',
    'my/model/<id>'            => 'catalog/model3d/item', // 'my/model3d/index',
    'my/model/<action>/<id>'   => 'my/model3d/<action>',
    'my/store/<action>/<id>'   => 'my/store/<action>',

    // 'my/orders/<status>/<page>' => 'my/order/index',
    'my/orders/<status>'       => 'my/order/index',
    'my/order/<action>/<id>'   => 'my/order/<action>',
    'my/orders'                => 'my/order/index',

    '/workbench/order/<action>/<id>' => '/workbench/order/<action>',
    '/workbench/orders/<status>'     => '/workbench/orders/index',
    '/workbench/orders'              => '/workbench/orders/index',

    /*'my/collection/<action:(add|add-simple|update|delete|bind-default|unbind-items|delete-collections)>' => 'my/collection/<action>',
    'my/collection/<id>'                                                                                 => 'my/collection/index',
    'my/collection/<action>/<id>'                                                                        => 'my/collection/<action>',


    'my/messages/dialog/<userId>'                                                                        => 'my/messages/dialog',
*/

    '/profile/collection/<action:(add|add-simple|update|delete|bind-default|unbind-items|delete-collections)>' => '/profile/collection/<action>',
    '/profile/collection/<id>'                                                                                 => '/profile/collection/index',
    '/profile/collection/<action>/<id>'                                                                        => '/profile/collection/<action>',
    '/workbench/messages/dialog/<userId>'                                                                      => '/workbench/messages/dialog',

    'messages'          => '/workbench/messages/index',
    'messages/<action>' => '/workbench/messages/<action>',

    'my/messages/topic-messages'     => '/workbench/messages/topic-messages',
    'my/messages/add-message'        => '/workbench/messages/ad-message',

    // NEW Company
    'my/company'                     => 'my/printservice',
    'my/company/<ps_id:\d+>'         => 'my/printservice/show-ps',
    'my/company/embed'               => 'my/printservice/embed',
    'my/company/add-printer'         => 'my/printservice/add-printer',
    'my/company/create-printer'      => 'my/printservice/create-printer',
    'my/company/add-cnc'             => 'my/printservice/add-cnc',
    'my/company/save-cnc'            => 'my/printservice/save-cnc',
    'my/company/create-ps'           => 'my/printservice/create-ps',
    'my/company/edit-ps'             => 'my/printservice/edit-ps',
    'my/company/update-ps'           => 'my/printservice/update-ps',
    'my/company/certification'       => 'my/printservice/certification',
    'my/company/update-ps-url'       => 'my/printservice/update-ps-url',
    'my/company/select-service-type' => 'my/printservice/select-service-type',
    'my/company/new-ps'              => 'my/printservice/new-ps',
    'my/company/edit-cnc'            => 'my/printservice/edit-cnc',

    'my/company/<ps_id:\d+>/<action>/<ps_printer_id:\d+>' => 'my/printservice/<action>',
    'my/company/<ps_id:\d+>/<action>/<printer_id:\d+>'    => 'my/printservice/<action>',
    'my/company/<ps_id:\d+>/<action>'                     => 'my/printservice/<action>',

    'my/company/<ps_id:\d+>/edit-printer/<ps_printer_id:\d+>/<action>' => 'my/edit-ps-printer/<action>',
    'my/company/<ps_id:\d+>/edit-printer/<printer_id:\d+>/<action>'    => 'my/edit-ps-printer/<action>',
    'my/company/<ps_id:\d+>/edit-printer/<action>'                     => 'my/edit-ps-printer/<action>',
    'my/company/edit-printer/<action>'                                 => 'my/edit-ps-printer/<action>',
    'my/company/edit-printer/<action>/<id:\d+>'                        => 'my/edit-ps-printer/<action>',

    '/mybusiness/<ps_id:\d+>/<action>/<ps_printer_id:\d+>' => '/mybusiness/services/<action>',
    '/mybusiness/<ps_id:\d+>/<action>/<printer_id:\d+>'    => '/mybusiness/services/<action>',
    '/mybusiness/<ps_id:\d+>/<action>'                     => '/mybusiness/services/<action>',

    '/mybusiness/<ps_id:\d+>/edit-printer/<ps_printer_id:\d+>/<action>' => '/mybusiness/edit-printer/<action>',
    '/mybusiness/<ps_id:\d+>/edit-printer/<printer_id:\d+>/<action>'    => '/mybusiness/edit-printer/<action>',
    '/mybusiness/<ps_id:\d+>/edit-printer/<action>'                     => '/mybusiness/edit-printer/<action>',
    '/mybusiness/edit-printer/<action>'                                 => '/mybusiness/edit-printer/<action>',
    '/mybusiness/edit-printer/<action>/<id:\d+>'                        => '/mybusiness/edit-printer/<action>',
    '/mybusiness/company-services/company-service/<type>/add'           => 'mybusiness/company-services/company-service/add',

    'my/company-order/print-requests/<statusGroup>' => 'my/company-order/print-requests',

    // new RULES
    //'/workbench/service-order/<action>' => '/workbench/service-order/<action>',

    '/workbench/preorder/quotes'                               => '/workbench/preorder/quotes',
    '/workbench/service-orders/<statusGroup>'                  => '/workbench/service-orders/index',


    // new RULES finish

    // OLD rulles support
    'my/printservice/<ps_id:\d+>'                              => 'my/printservice/show-ps',
    'my/printservice/<ps_id:\d+>/<action>/<ps_printer_id:\d+>' => 'my/printservice/<action>',
    'my/printservice/<ps_id:\d+>/<action>/<printer_id:\d+>'    => 'my/printservice/<action>',
    'my/printservice/<ps_id:\d+>/<action>'                     => 'my/printservice/<action>',

    'my/printservice/<ps_id:\d+>/edit-printer/<ps_printer_id:\d+>/<action>' => 'my/edit-ps-printer/<action>',
    'my/printservice/<ps_id:\d+>/edit-printer/<printer_id:\d+>/<action>'    => 'my/edit-ps-printer/<action>',
    'my/printservice/<ps_id:\d+>/edit-printer/<action>'                     => 'my/edit-ps-printer/<action>',
    'my/printservice/edit-printer/<action>'                                 => 'my/edit-ps-printer/<action>',
    'my/printservice/edit-printer/<action>/<id:\d+>'                        => 'my/edit-ps-printer/<action>',

    'my/printservice-order/print-requests/<statusGroup>' => 'my/printservice-order/print-requests',
    // --- OLD rulles support ----

    'store/cart/add/<id>/<psPrinterId>/<material>/<color>' => 'store/cart/add',
    'store/cart/checkout'                                  => 'store/<controller>/<action>',
    // 'store/cart/checkout/<id>/<printerId>/<material>/<color>/<qty>' => 'store/cart/checkout',

    'store/<controller>/<action>/<id>' => 'store/<controller>/<action>',


    'upload/<action>'       => 'catalog/upload/<action>',
    'upload/'               => '/catalog/upload-model3d',
    'order-upload/add-file' => '/my/order/add-file',
    'order-upload/save'     => '/my/order/save',
    'order-upload/<widget>' => '/my/order/upload-order-files',
    'order-upload'          => '/my/order/upload-order-files',
    'file/download/<id>'    => 'file/download',

    'help/category/<id:\d+>-<slug>' => 'help/category',
    'help/category/<id>'            => 'help/category',
    'help/article/<id:\d+>-<alias>' => 'help/article',
    'help/article/<id>'             => 'help/article',


    'guide/<slug>'                  => 'guide/category',
    'guide/article/<id:\d+>-<slug>' => 'guide/article',
    'guide/tag/<tag>'               => 'guide/tag',


    'facebook-widget-ps' => 'my/ps-widget/facebook',

    'blog'         => 'blog/index',
    'blog/rss'     => '/blog/rss',
    'blog/<alias>' => '/blog/view',

    '3d-printing-services/?' => 'catalogps/ps-catalog/index',
    '3d-printing-services' => 'catalogps/ps-catalog/index',

    '3d-printable-models-store/?' => 'site/seo-page',

    'do-restore-session'  => 'session/do-restore-session',
    'set-session'         => 'session/set-session',
    'ping-global-session' => 'session/ping-global-session',

    'sitemap-xml/sitemap_index.xml' => 'sitemap/index-list',
    'sitemap-xml/sitemap.xml'       => '/sitemap/main-list',
    'sitemap-xml/sitemap<lang>.xml' => 'sitemap/main-list',
    'machines/item/<slug>'          => 'machines/default/item',
    'machines/3d-printers'          => 'machines/default/index',
    'machines/3d-printers/<filter>' => 'machines/default/index',
    'machines/wiki-item/<code>'     => 'machines/default/wiki-item',
    [
        'pattern'    => 'machines/3d-printers',
        'route'      => 'machines/default/index',
        'suffix'     => '/',
        'normalizer' => [
            'class' => 'yii\web\UrlNormalizer',
        ],
    ],

    'machines/<category>'          => 'machines/default/wiki',
    'machines/<category>/<filter>' => 'machines/default/wiki',
    /*
    [
        'pattern'    => 'machines/<category>',
        'route'      => 'machines/default/wiki',
        'suffix'     => '/',
        'normalizer' => [
            'class' => 'yii\web\UrlNormalizer',
        ],
    ],*/

    'my/print-model3d/printers'                 => 'my/print-model3d/printers',
    'my/print-model3d/delivery'                 => 'my/print-model3d/delivery',
    'my/print-model3d/checkout'                 => 'my/print-model3d/checkout',
    'my/print-model3d/pay-order'                => 'my/print-model3d/pay-order',
    'my/print-model3d/promo-code'               => 'my/print-model3d/promo-code',
    'my/print-model3d/<widget>'                 => 'my/print-model3d',
    'my/print-model3d/printers/<widget>'        => 'my/print-model3d/printers',
    'my/print-model3d/delivery/<widget>'        => 'my/print-model3d/delivery',
    'my/print-model3d/checkout/<widget>'        => 'my/print-model3d/checkout',
    'my/print-model3d/pay-order/<widget>'       => 'my/print-model3d/pay-order',
    'my/print-model3d-ajax/pay-order/<widget>'  => 'my/print-model3d-ajax/pay-order',
    'my/print-model3d-ajax/promo-code/<widget>' => 'my/print-model3d-ajax/promo-code',

    'materials'       => 'wiki/wiki-material/index',
    'material/<code>' => 'wiki/wiki-material/view-material',

    'apps'                 => 'promo/apps',
    'apps/<code>'          => 'promo/apps',
    [
        'pattern'    => '/l/<id>',
        'route'      => 'promo/page',
        'suffix'     => '',
        'normalizer' => [
            'class' => UrlNormalizer::class,
        ],
    ],
    'product/buy/<uuid>'   => 'product/product/buy',
    'product/buy-delivery' => 'product/product/buy-delivery',
    'product/buy-checkout' => 'product/product/buy-checkout',
    'product/pay-order'    => 'product/product/pay-order',
    'product/<slug>'       => 'product/product/view',

    '/product/ajax/buy-cart-save'      => 'product/product-ajax/buy-cart-save',
    '/product/ajax/save-delivery-info' => 'product/product-ajax/save-delivery-info',

    'company-service/<category>' => 'catalogps/catalog/category',
    'company-services'           => 'catalogps/catalog/index',

    '/my/order-laser-cutting/<name>-<page:\d+>_Images/<name2>-<page2:\d+>_<imgName>' => '/my/order-laser-cutting/download-image',
    '/my/order-laser-cutting/upload-file/<widget>'                                   => '/my/order-laser-cutting/upload-file',

    'companies'                                  => '/companies/companies/index',
    'reviews/<reviewId:\d+>/share/<shareId:\d+>' => 'reviews/view-share',

    // rest
    ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/uploads'],
    ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v2/printable-packs'],


    ['class' => '\common\modules\catalogPs\CatalogUrlRule'],
    [
        'class'     => '\frontend\components\StaticPageUrlRule',
        'pathStart' => ['sitemap', 'about', 'press', 'tech', 'services']
    ]
];
