-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 30, 2015 at 04:35 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
SET FOREIGN_KEY_CHECKS=0;
--
-- Database: `ts_dev2`
--

-- --------------------------------------------------------

--
-- Table structure for table `store_category`
--

CREATE TABLE IF NOT EXISTS `store_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`),
  KEY `fk_store_category_1_idx` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `store_category`
--

INSERT INTO `store_category` (`id`, `title`, `description`, `is_active`, `parent_id`) VALUES
(1, 'Art & Design', '', 1, NULL),
(2, 'Toys & Games', '', 1, NULL),
(3, 'Hobby & DIY', '', 1, NULL),
(4, 'Jewelry', '', 1, NULL),
(5, 'Mechanical', '', 1, NULL),
(6, 'Architecture', '', 1, NULL),
(7, 'Furniture', '', 1, NULL),
(8, 'Gadget', '', 1, NULL),
(9, 'Educational', '', 1, NULL),
(10, 'Fashion', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `store_unit`
--

CREATE TABLE IF NOT EXISTS `store_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model3d_id` int(11) NOT NULL,
  `price` decimal(6,2) NOT NULL DEFAULT '0.00',
  `price_per_print` decimal(6,2) DEFAULT '0.00',
  `price_currency` char(5) NOT NULL DEFAULT 'usd',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `moderated_at` timestamp NULL DEFAULT NULL,
  `status` set('new','active','deleted') NOT NULL DEFAULT 'new',
  `license_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `model3d_id_UNIQUE` (`model3d_id`),
  KEY `fk_store_unit_1_idx` (`model3d_id`),
  KEY `fk_store_unit_2_idx` (`license_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

 

-- --------------------------------------------------------

--
-- Table structure for table `store_units2category`
--

CREATE TABLE IF NOT EXISTS `store_units2category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_id_UNIQUE` (`unit_id`,`category_id`),
  KEY `fk_store_units2category_1_idx` (`unit_id`),
  KEY `fk_store_units2category_2_idx` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;
 

--
-- Constraints for dumped tables
--

--
-- Constraints for table `store_category`
--
ALTER TABLE `store_category`
  ADD CONSTRAINT `fk_store_category_1` FOREIGN KEY (`parent_id`) REFERENCES `store_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `store_unit`
--
ALTER TABLE `store_unit`
  ADD CONSTRAINT `fk_store_unit_1` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_store_unit_2` FOREIGN KEY (`license_id`) REFERENCES `model3d_license` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `store_units2category`
--
ALTER TABLE `store_units2category`
  ADD CONSTRAINT `fk_store_units2category_2` FOREIGN KEY (`category_id`) REFERENCES `store_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
