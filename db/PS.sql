-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 13 2015 г., 20:58
-- Версия сервера: 5.5.43-0+deb7u1
-- Версия PHP: 5.4.41-0+deb7u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `ts_new_base`
--

-- --------------------------------------------------------

--
-- Структура таблицы `printer`
--

CREATE TABLE IF NOT EXISTS `printer` (
  `id` int(11) NOT NULL,
  `firm` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `technology_id` int(11) DEFAULT NULL,
  `weight` smallint(6) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `website` varchar(100) NOT NULL,
  `build_volume` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `printer_color`
--

CREATE TABLE IF NOT EXISTS `printer_color` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `rgb` varchar(45) DEFAULT NULL,
  `cmyk` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `printer_material`
--

CREATE TABLE IF NOT EXISTS `printer_material` (
  `id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `printer_material_to_color`
--

CREATE TABLE IF NOT EXISTS `printer_material_to_color` (
  `id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `printer_properties`
--

CREATE TABLE IF NOT EXISTS `printer_properties` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `printer_technology`
--

CREATE TABLE IF NOT EXISTS `printer_technology` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `print_speed_min` smallint(6) DEFAULT NULL,
  `print_speed_max` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `printer_to_material`
--

CREATE TABLE IF NOT EXISTS `printer_to_material` (
  `id` int(11) NOT NULL,
  `printer_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `printer_to_property`
--

CREATE TABLE IF NOT EXISTS `printer_to_property` (
  `id` int(11) NOT NULL,
  `printer_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `value` varchar(100) NOT NULL,
  `is_editable` bit(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ps`
--

CREATE TABLE IF NOT EXISTS `ps` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `logo` varchar(100) NOT NULL,
  `phone_code` varchar(10) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `moderator_status` enum('new','checking','ok','banned','review') NOT NULL,
  `moderated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ps_printer`
--

CREATE TABLE IF NOT EXISTS `ps_printer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ps_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `printer_id` int(11) DEFAULT NULL,
  `price_per_hour` decimal(10,0) DEFAULT NULL,
  `currency_iso` char(5) DEFAULT NULL,
  `price_per_volume` decimal(10,0) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `user_status` set('active','inactive') DEFAULT NULL,
  `is_online` tinyint(1) DEFAULT '0',
  `moderator_status` set('new','checking','ok','banned','review') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ps_printer_color`
--

CREATE TABLE IF NOT EXISTS `ps_printer_color` (
  `id` int(11) NOT NULL,
  `ps_material_id` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ps_printer_delivery`
--

CREATE TABLE IF NOT EXISTS `ps_printer_delivery` (
  `id` int(11) NOT NULL,
  `ps_printer_id` int(11) DEFAULT NULL,
  `delivery_id` int(11) DEFAULT NULL,
  `delivery_details` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ps_printer_job`
--

CREATE TABLE IF NOT EXISTS `ps_printer_job` (
  `id` int(11) NOT NULL,
  `ps_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `finished_at` timestamp NULL DEFAULT NULL,
  `status` set('new','active','inactive','running','finished') DEFAULT NULL,
  `result` text,
  `log` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ps_printer_material`
--

CREATE TABLE IF NOT EXISTS `ps_printer_material` (
  `id` int(11) NOT NULL,
  `printer_id` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `material_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ps_printer_to_property`
--

CREATE TABLE IF NOT EXISTS `ps_printer_to_property` (
  `id` int(11) NOT NULL,
  `printer_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `value` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `printer`
--
ALTER TABLE `printer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_printer_1_idx` (`technology_id`);

--
-- Индексы таблицы `printer_color`
--
ALTER TABLE `printer_color`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `printer_material`
--
ALTER TABLE `printer_material`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `printer_material_to_color`
--
ALTER TABLE `printer_material_to_color`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mtc_material_id` (`material_id`),
  ADD KEY `mtc_color_id` (`color_id`);

--
-- Индексы таблицы `printer_properties`
--
ALTER TABLE `printer_properties`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `printer_technology`
--
ALTER TABLE `printer_technology`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `printer_to_material`
--
ALTER TABLE `printer_to_material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_printer_to_material_1_idx` (`printer_id`),
  ADD KEY `fk_printer_to_material_2_idx` (`material_id`);

--
-- Индексы таблицы `printer_to_property`
--
ALTER TABLE `printer_to_property`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ptp_printer_id` (`printer_id`),
  ADD KEY `ptp_property_id` (`property_id`);

--
-- Индексы таблицы `ps`
--
ALTER TABLE `ps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_id_5` (`user_id`) USING BTREE,
  ADD KEY `fk_location_id_2` (`location_id`);

--
-- Индексы таблицы `ps_printer`
--
ALTER TABLE `ps_printer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_printservice_printer_1_idx` (`user_id`),
  ADD KEY `fk_printservice_printer_@_idx` (`printer_id`),
  ADD KEY `fk_printservice_printer_3_idx` (`location_id`),
  ADD KEY `fk_printservice_printer_4_idx` (`ps_id`) USING BTREE;

--
-- Индексы таблицы `ps_printer_color`
--
ALTER TABLE `ps_printer_color`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_printservice_pr2_idx` (`color_id`),
  ADD KEY `fk_printservice_printer_color_1_idx` (`ps_material_id`);

--
-- Индексы таблицы `ps_printer_delivery`
--
ALTER TABLE `ps_printer_delivery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk1_pd_u1_idx` (`ps_printer_id`);

--
-- Индексы таблицы `ps_printer_job`
--
ALTER TABLE `ps_printer_job`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_printservice_printer_job_1_idx` (`ps_id`);

--
-- Индексы таблицы `ps_printer_material`
--
ALTER TABLE `ps_printer_material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_printservice_printer_material_1_idx` (`printer_id`),
  ADD KEY `fk_printservice_printer_material_2_idx` (`material_id`);

--
-- Индексы таблицы `ps_printer_to_property`
--
ALTER TABLE `ps_printer_to_property`
  ADD KEY `fk_printer_id_7` (`printer_id`),
  ADD KEY `fk_property_id_2` (`property_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `printer`
--
ALTER TABLE `printer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `printer_color`
--
ALTER TABLE `printer_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `printer_material`
--
ALTER TABLE `printer_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `printer_material_to_color`
--
ALTER TABLE `printer_material_to_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `printer_properties`
--
ALTER TABLE `printer_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `printer_technology`
--
ALTER TABLE `printer_technology`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `printer_to_material`
--
ALTER TABLE `printer_to_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `printer_to_property`
--
ALTER TABLE `printer_to_property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ps`
--
ALTER TABLE `ps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ps_printer`
--
ALTER TABLE `ps_printer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ps_printer_color`
--
ALTER TABLE `ps_printer_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ps_printer_delivery`
--
ALTER TABLE `ps_printer_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ps_printer_job`
--
ALTER TABLE `ps_printer_job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ps_printer_material`
--
ALTER TABLE `ps_printer_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `printer`
--
ALTER TABLE `printer`
  ADD CONSTRAINT `fk_printer_technology` FOREIGN KEY (`technology_id`) REFERENCES `printer_technology` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `printer_material_to_color`
--
ALTER TABLE `printer_material_to_color`
  ADD CONSTRAINT `fk_color_id` FOREIGN KEY (`color_id`) REFERENCES `printer_color` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_material_id` FOREIGN KEY (`material_id`) REFERENCES `printer_material` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `printer_to_material`
--
ALTER TABLE `printer_to_material`
  ADD CONSTRAINT `fk_printer_id_2` FOREIGN KEY (`printer_id`) REFERENCES `printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_printer_material` FOREIGN KEY (`material_id`) REFERENCES `printer_material` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `printer_to_property`
--
ALTER TABLE `printer_to_property`
  ADD CONSTRAINT `fk_printer_id_6` FOREIGN KEY (`printer_id`) REFERENCES `printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_property_id` FOREIGN KEY (`property_id`) REFERENCES `printer_properties` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `ps`
--
ALTER TABLE `ps`
  ADD CONSTRAINT `fk_location_id_2` FOREIGN KEY (`location_id`) REFERENCES `geo_location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ps_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `ps_printer`
--
ALTER TABLE `ps_printer`
  ADD CONSTRAINT `fk_location_id` FOREIGN KEY (`location_id`) REFERENCES `geo_location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_printer_id_1` FOREIGN KEY (`printer_id`) REFERENCES `printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ps_id` FOREIGN KEY (`ps_id`) REFERENCES `ps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `ps_printer_color`
--
ALTER TABLE `ps_printer_color`
  ADD CONSTRAINT `fk_ps_color` FOREIGN KEY (`color_id`) REFERENCES `printer_color` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ps_material` FOREIGN KEY (`ps_material_id`) REFERENCES `ps_printer_material` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `ps_printer_delivery`
--
ALTER TABLE `ps_printer_delivery`
  ADD CONSTRAINT `fk_ps_id_2` FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `ps_printer_job`
--
ALTER TABLE `ps_printer_job`
  ADD CONSTRAINT `fk_ps_id_3` FOREIGN KEY (`ps_id`) REFERENCES `ps_printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `ps_printer_material`
--
ALTER TABLE `ps_printer_material`
  ADD CONSTRAINT `fk_printer_id_5` FOREIGN KEY (`printer_id`) REFERENCES `ps_printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ps_printer_material_ibfk_1` FOREIGN KEY (`material_id`) REFERENCES `printer_material` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `ps_printer_to_property`
--
ALTER TABLE `ps_printer_to_property`
  ADD CONSTRAINT `fk_printer_id_7` FOREIGN KEY (`printer_id`) REFERENCES `ps_printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_property_id_2` FOREIGN KEY (`property_id`) REFERENCES `printer_properties` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
