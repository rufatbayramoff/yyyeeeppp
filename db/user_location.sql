
UPDATE `ps_printer` SET location_id = NULL;

ALTER TABLE `ps_printer_color` ADD UNIQUE `ps_material_color` (`ps_material_id`, `color_id`);

ALTER TABLE `ps_printer_delivery` ADD UNIQUE `ps_printer_delivery_type` (`ps_printer_id`, `delivery_type_id`);

ALTER TABLE `ps_printer` DROP FOREIGN KEY fk_location_id;

CREATE TABLE `user_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `address` varchar(45) DEFAULT NULL,
  `lat` decimal(8,5) NOT NULL,
  `lon` decimal(8,5) NOT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  `location_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lat_lon_UNIQUE` (`lat`,`lon`),
  KEY `fk_gl1_idx` (`country_id`),
  KEY `fk_gl2_idx` (`city_id`),
  KEY `fk_geo_location_1_idx` (`region_id`),
  CONSTRAINT `fk_geo_city` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_geo_country` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_loc` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ps_printer` ADD CONSTRAINT `fk_user_location` FOREIGN KEY (`location_id`) REFERENCES `user_location`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
