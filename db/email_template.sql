-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 08, 2015 at 01:07 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ts_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE IF NOT EXISTS `email_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `language_id` varchar(8) NOT NULL,
  `title` varchar(155) DEFAULT NULL,
  `description` varchar(245) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `template_html` mediumtext,
  `template_text` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`,`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`id`, `code`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`) VALUES
(1, 'register', 'en-US', 'Confirm email and registration', 'user gets this email after registration to confirm email', '2015-06-18 07:13:54', '<p>Hello %username%,</p>\r\n<p>\r\nFollow the link below to confirm your email and registration on TS:  <br />\r\n<a href="%link%">%link%</a>\r\n</p>\r\n<p>\r\nE-mail: %email% <br />\r\nUsername: %username% <br />\r\nPassword: %password% <br />\r\n</p>\r\n<p>\r\nThank you,\r\nTS Team</p>', 'Hello %username%,\r\n\r\nFollow the link below to confirm your email and registration on TS: \r\n%link%\r\n\r\nE-mail: %email%\r\nUsername: %username%\r\nPassword: %password%\r\n\r\n\r\nThank you,\r\nTS Team'),
(2, 'register', 'ru-RU', 'Подтвердите электронную почту и регистрацию', '', '2015-06-18 07:14:35', '<p>Уважаемый %username%, </p>\r\n<p>Кто-то, возможно вы, указали данный e-mail при регистрации в TS:</p>\r\n<p>\r\nE-mail: %email% <br />\r\nИмя пользователя: %username% <br />\r\nПароль: %password% <br />\r\n</p>\r\n\r\n<p>\r\nЕсли это вы, пожалуйста подтвердите e-mail, перейдя по ссылке: <br />\r\n<a href="%link%">%link%</a>\r\n</p>', 'Уважаемый %username%,\r\n\r\nКто-то, возможно вы, указали данный e-mail при регистрации в TS:\r\n\r\nE-mail: %email%\r\nИмя пользователя: %username%\r\nПароль: %password%\r\n\r\nЕсли это вы, пожалуйста подтвердите e-mail, перейдя по ссылке:\r\n%link%'),
(3, 'confirmEmail', 'en-US', 'Confirm email', 'message with link to confirm email', '2015-06-18 07:11:14', '<p>Hello %username%,</p>\r\n\r\n<p>Follow the link below to confirm your email, if you requested this operation on TS:</p>\r\n<a href="%link%">%link%</a>', 'Hello %username%,\r\n\r\nFollow the link below to confirm your email, if you requested this operation on TS:\r\n%link%'),
(4, 'confirmEmail', 'ru-RU', 'Подтвердите электронную почту', '', '2015-06-18 07:11:55', '<p>Уважаемый %username%,</p>\r\n<p>\r\nКто-то, возможно вы, запросили подтвердить данный e-mail  в TS.\r\nЕсли это вы пожалуйста подтвердите e-mail, перейдя по ссылке:\r\n</p>\r\n<a href="%link%">%link%</a>', 'Уважаемый %username%,\r\n\r\nКто-то, возможно вы, запросили подтвердить данный e-mail  в TS.\r\nЕсли это вы пожалуйста подтвердите e-mail, перейдя по ссылке\r\n%link%'),
(5, 'forgotPassword', 'en-US', 'Password reset', 'password reset', '2015-06-16 15:55:46', '<div class="password-reset">\r\n    <p>Hello %username%,</p>\r\n\r\n    <p>Follow the link below to reset your password:</p>\r\n\r\n    <p><a href="%link%">%link%</a></p>\r\n</div>\r\n', 'Hello %username%,\r\n\r\nFollow the link below to reset your password:\r\n\r\n%link%\r\n'),
(6, 'forgotPassword', 'ru-RU', 'Сброс пароля', '', '2015-06-16 15:55:53', '<div class="password-reset">\r\n<p>Привет %username%,</p>\r\n\r\n<p>Чтобы сбросить пароль и установить новый, нажмите ссылку:</p>\r\n\r\n    <p><a href="%link%">%link%</a></p>\r\n</div>', 'Привет %username%,\r\n\r\nЧтобы сбросить пароль и установить новый, нажмите ссылку:\r\n\r\n%link%'),
(7, 'registerOsn', 'en-US', 'Registration completed', 'user gets this email after OSN registration', NULL, '<p>Hello %name%,</p>\r\n<p>\r\nYou registered on our website using Social Network.\r\nYou can use the following credentials to login into your account.\r\n</p>\r\n<p>\r\nE-mail: %email% <br />\r\nUsername: %username% <br />\r\nPassword: %password% <br />\r\n</p>\r\n<p>\r\nThank you,\r\nTS Team</p>', 'Hello %name%,\r\n\r\nYou registered on our website using Social Network.\r\nYou can use the following credentials to login into your account.\r\n\r\nE-mail: %email%\r\nUsername: %username%\r\nPassword: %password%\r\n\r\n\r\nThank you,\r\nTS Team'),
(8, 'changePassword', 'en-US', 'Password was changed', 'this email sent after user changes his password in profile', NULL, '<p>Hello %username%,</p>\r\n<p>\r\nPassword was changed. Your new credentials:\r\n</p>\r\n<p> \r\nUsername: %username% <br />\r\nPassword: %password% <br />\r\n</p>\r\n<p>\r\nThank you,\r\nTS Team</p>', 'Hello %username%,\r\n\r\nPassword was changed. Your new credentials:\r\n\r\nUsername: %username%\r\nPassword: %password%\r\n\r\n\r\nThank you,\r\nTS Team');
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
