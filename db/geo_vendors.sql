

ALTER TABLE `geo_city` ADD UNIQUE `geoname_id` (`geoname_id`) USING BTREE;


CREATE TABLE IF NOT EXISTS `geo_location_vendor_results` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `response` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `geo_location_vendor_results`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `geo_location_vendor_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



CREATE TABLE IF NOT EXISTS `geo_vendor` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `geo_vendor` (`id`, `title`) VALUES
(1, 'yandex'),
(2, 'google'),
(3, 'bing');

ALTER TABLE `geo_vendor`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `geo_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;



CREATE TABLE IF NOT EXISTS `geo_vendor_settings` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `setting` enum('country_id') NOT NULL,
  `value` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `geo_vendor_settings` (`id`, `vendor_id`, `setting`, `value`) VALUES
(1, 1, 'country_id', '191'),
(2, 1, 'country_id', '255');

ALTER TABLE `geo_vendor_settings`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `geo_vendor_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



CREATE TABLE IF NOT EXISTS `geo_log` (
  `id` int(11) NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `geo_log`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `geo_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



ALTER TABLE geo_location_vendor_results ADD CONSTRAINT fk_vendor_id FOREIGN KEY (vendor_id) references geo_vendor(id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE geo_vendor_settings ADD CONSTRAINT fk_vendor_id_2 FOREIGN KEY (vendor_id) references geo_vendor(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
