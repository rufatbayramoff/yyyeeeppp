SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `model3d_license` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` text,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `model3d_img` 
ADD CONSTRAINT `fk_model3d_img_1`
  FOREIGN KEY (`model3d_id`)
  REFERENCES `model3d` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `model3d_img` 
ADD INDEX `fk_model3d_img_2_idx` (`user_id` ASC);
ALTER TABLE `model3d_img` 
ADD CONSTRAINT `fk_model3d_img_2`
  FOREIGN KEY (`user_id`)
  REFERENCES `user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `model3d_img` 
ADD COLUMN `file_id` INT(11) NULL AFTER `review_id`,
ADD INDEX `fk_model3d_img_3_idx` (`file_id` ASC);
ALTER TABLE `model3d_img` 
ADD CONSTRAINT `fk_model3d_img_3`
  FOREIGN KEY (`file_id`)
  REFERENCES `file` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `model3d_img` 
DROP FOREIGN KEY `fk_model3d_img_3`;
ALTER TABLE `model3d_img` 
CHANGE COLUMN `file_id` `file_id` INT(11) NOT NULL ;
ALTER TABLE `model3d_img` 
ADD CONSTRAINT `fk_model3d_img_3`
  FOREIGN KEY (`file_id`)
  REFERENCES `file` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `model3d` 
ADD CONSTRAINT `fk_model3d_2`
  FOREIGN KEY (`license_id`)
  REFERENCES `model3d_license` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `model3d_license` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `is_active` `is_active` TINYINT(1) NOT NULL DEFAULT 1 ; 

ALTER TABLE `model3d_license` 
ADD COLUMN `info` TEXT NOT NULL AFTER `is_active`;

ALTER TABLE `model3d_license` 
CHANGE COLUMN `info` `info` TEXT NOT NULL COMMENT 'License text' ;

ALTER TABLE `model3d_file` 
CHANGE COLUMN `format` `format` CHAR(5) NOT NULL DEFAULT 'stl' ;

ALTER TABLE `model3d_file` 
ADD COLUMN `is_image` TINYINT(1) NOT NULL DEFAULT 0 AFTER `file_id`;

ALTER TABLE `file` 
CHANGE COLUMN `path` `path` VARCHAR(555) NOT NULL ;