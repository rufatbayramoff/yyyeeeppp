-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 20 2015 г., 11:53
-- Версия сервера: 5.5.43-0+deb7u1
-- Версия PHP: 5.4.41-0+deb7u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `ts_new_base`
--

--
-- Дамп данных таблицы `printer`
--

INSERT INTO `printer` (`id`, `firm`, `title`, `description`, `created_at`, `updated_at`, `is_active`, `technology_id`, `weight`, `price`, `website`, `build_volume`) VALUES
(1, '1', 'Ultimaker 2', '', '2015-07-13 22:16:01', '2015-07-14 05:39:25', 0, 9, 1, 1.00, 'https://ultimaker.com/en/products/ultimaker-2-family/ultimaker-2', '1'),
(2, '1', 'Prusa i3', '', '2015-07-13 23:05:03', '2015-07-14 05:54:26', 0, 1, 1, 1.00, 'http://reprap.org/wiki/Prusa_i3', '1'),
(3, '1', 'Makerbot Replicator 2', '', '2015-07-14 05:54:17', '2015-07-14 05:54:47', 0, 1, 1, 1.00, 'https://eu.makerbot.com/shop/en/3d-printer/replicator-2/53/makerbot-replicator-2-desktop-3d-printer?c=78', '1'),
(4, '1', 'Ultimaker Original Plus', '', '2015-07-14 05:56:37', '2015-07-14 05:56:37', 0, 9, 1, 1.00, 'https://ultimaker.com/en/products/ultimaker-original', '1'),
(5, '1', 'Makerbot Replicator 2x', '', '2015-07-14 05:58:38', '2015-07-14 05:58:38', 0, 1, 1, 1.00, 'https://eu.makerbot.com/shop/en/3d-printer/replicator-2x/54/makerbot-replicator-2x?c=78', '1'),
(6, '1', 'Form 1+', '', '2015-07-14 05:59:24', '2015-07-14 05:59:24', 0, 8, 1, 1.00, 'http://formlabs.com/products/form-1-plus/', '1'),
(7, '1', 'Zortrax M200', '', '2015-07-14 06:05:56', '2015-07-14 06:05:56', 0, 10, 1, 1.00, 'https://zortrax.com/product-details/technical-details', '1'),
(8, '1', 'Makerbot Replicator 5th Gen', '', '2015-07-14 06:34:01', '2015-07-14 06:34:01', 0, 1, 1, 1.00, 'https://eu.makerbot.com/shop/en/3d-printer/replicator/55/makerbot-replicator?c=78', '1'),
(9, '1', 'Printrbot Simple Metal', '', '2015-07-14 06:34:48', '2015-07-14 06:34:48', 0, 1, 1, 1.00, 'http://printrbot.com/shop/assembled-simple-metal-with-heated-bed/', '1'),
(10, '1', 'Da Vinci 1.0', '', '2015-07-14 06:35:31', '2015-07-14 06:35:31', 0, 9, 1, 1.00, 'http://us.xyzprinting.com/us_en/Product/da-Vinci-1.0', '1'),
(11, '1', 'FlashForge Creator Pro', '', '2015-07-14 06:36:09', '2015-07-14 06:36:09', 0, 1, 1, 1.00, 'http://www.flashforge-usa.com/shop/3d-printers/flashforge-creator-pro-dual-extrusion-3d-printer.html', '1'),
(12, '1', 'Mendel Prusa', '', '2015-07-14 06:36:52', '2015-07-14 06:36:52', 0, 1, 1, 1.00, 'http://reprap.org/wiki/Prusa_Mendel_(iteration_2)', '1'),
(13, '1', 'Rostock MAX', '', '2015-07-14 06:37:29', '2015-07-14 06:37:29', 0, 1, 1, 1.00, 'http://reprap.org/wiki/Rostock_MAX', '1'),
(14, '1', 'UP Mini', '', '2015-07-14 06:41:37', '2015-07-14 06:41:37', 0, 1, 1, 1.00, 'http://3dtoday.ru/3d-printers/pp3dp/up-mini/', '1');

--
-- Дамп данных таблицы `printer_color`
--

INSERT INTO `printer_color` (`id`, `title`, `created_at`, `updated_at`, `is_active`, `rgb`, `cmyk`, `image`) VALUES
(1, 'Зеленый', '2015-07-13 17:45:20', '2015-07-13 18:52:59', 0, '50, 153, 68', '67%, 0%, 56%, 40%', ''),
(2, 'Синий', '2015-07-13 18:51:20', '2015-07-13 18:53:06', 1, '18, 32, 140', '87%, 77%, 0%, 45%', '');

--
-- Дамп данных таблицы `printer_material`
--

INSERT INTO `printer_material` (`id`, `title`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Full color gypsum', '2015-07-13 07:47:45', '2015-07-13 20:55:06', 1),
(2, 'Durable and flexible plastic', '2015-07-13 07:48:35', '2015-07-13 20:55:39', 1),
(3, 'Detailed plastic', '2015-07-13 07:49:59', '2015-07-13 20:56:18', 1),
(4, 'Detailed translucent plastic', '2015-07-13 20:56:40', '2015-07-13 20:56:40', 0),
(5, 'Bronze', '2015-07-13 20:57:17', '2015-07-13 20:57:17', 0),
(6, 'Brass', '2015-07-13 20:57:40', '2015-07-13 20:57:40', 0),
(7, 'Silver', '2015-07-13 20:58:15', '2015-07-13 20:58:15', 0),
(8, 'Gold', '2015-07-13 20:59:34', '2015-07-13 20:59:34', 0),
(9, 'Jewelry wax', '2015-07-13 20:59:52', '2015-07-13 20:59:52', 0),
(10, 'Stainless steel', '2015-07-13 21:00:14', '2015-07-13 21:00:14', 0);

--
-- Дамп данных таблицы `printer_technology`
--

INSERT INTO `printer_technology` (`id`, `title`, `created_at`, `updated_at`, `is_active`, `print_speed_min`, `print_speed_max`) VALUES
(1, 'FDM (Fused deposition modeling)', '2015-07-13 22:01:25', '2015-07-14 05:36:05', 0, 1, 1),
(2, 'DMLS (Direct metal laser sintering)', '2015-07-13 23:02:53', '2015-07-14 05:36:11', 0, 1, 1),
(3, 'EBM (Electron Beam Melting)', '2015-07-14 02:38:42', '2015-07-14 02:38:42', 0, 1, 1),
(4, 'SLS (Selective laser sintering)', '2015-07-14 02:38:59', '2015-07-14 02:38:59', 0, 1, 1),
(5, '3DP (3D printing, Powder bed and inkjet head 3d printing, Plaster-based 3D printing)', '2015-07-14 02:39:13', '2015-07-14 02:39:13', 0, 1, 1),
(6, 'PolyJet и PolyJetMatrix', '2015-07-14 02:39:28', '2015-07-14 02:39:28', 0, 1, 1),
(7, 'MJM (Multi Jet Modeling)', '2015-07-14 02:39:45', '2015-07-14 02:39:45', 0, 1, 1),
(8, 'SLA (Stereolithography)', '2015-07-14 02:40:00', '2015-07-14 02:40:00', 0, 1, 1),
(9, 'FFF (Fused Filament Fabrication)', '2015-07-14 05:35:45', '2015-07-14 05:38:29', 0, 1, 1),
(10, '(LPD) Layer Plastic Deposition', '2015-07-14 06:03:09', '2015-07-14 06:03:09', 0, 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
