
ALTER TABLE ps_printer ADD `min_order_price` DECIMAL(5,2) NULL AFTER `price_per_volume`;

DROP TABLE IF EXISTS ps_printer_delivery;


CREATE TABLE `delivery_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `is_payment` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `delivery_courier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `ps_printer_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ps_printer_id` int(11) NOT NULL,
  `delivery_type_id` int(11) NOT NULL,
  `delivery_details` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `price_local` decimal(5,2) DEFAULT NULL,
  `price_world` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk1_pd_u1_idx` (`ps_printer_id`),
  KEY `delivery_type_id_index` (`delivery_type_id`),
  CONSTRAINT `fk_delivery_type` FOREIGN KEY (`delivery_type_id`) REFERENCES `delivery_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ps_printer_id_3` FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `delivery_type` (`id`, `title`, `is_payment`, `is_active`, `created_at`, `updated_at`) VALUES
  (1, 'Pickup', 0, 1, '2015-08-18 08:34:08', '2015-08-18 08:34:08'),
  (2, 'Local delivery (town/city limits)', 1, 1, '2015-08-18 08:35:02', '2015-08-18 08:35:02'),
  (3, 'Courier service', 1, 1, '2015-08-18 08:36:38', '2015-08-18 08:36:38'),
  (4, 'Postal service', 1, 1, '2015-08-18 08:37:09', '2015-08-18 08:37:09');


INSERT INTO `delivery_courier` (`id`, `title`, `is_active`, `created_at`, `updated_at`) VALUES
  (1, 'UPS', 1, '2015-08-18 08:41:08', '2015-08-18 08:41:08'),
  (2, 'FedEx', 1, '2015-08-18 08:41:08', '2015-08-18 08:41:08');

