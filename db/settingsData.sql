
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
 
INSERT INTO `system_setting_group` (`id`, `title`, `created_at`, `updated_at`, `created_user_id`, `updated_user_id`, `description`) VALUES
(1, 'user', '2015-06-09 13:08:30', NULL, NULL, NULL, 'Settings for user on site'),
(2, 'file', '2015-06-09 13:08:36', NULL, NULL, NULL, 'File settings, limits and etc.'),
(3, 'system', '2015-06-09 15:15:00', NULL, NULL, NULL, 'System settings'),
(4, 'cache', '2015-06-09 15:19:08', NULL, NULL, NULL, ''),
(5, 'site', '2015-06-09 15:20:21', NULL, NULL, NULL, ''),
(6, 'geo', '2015-06-09 15:20:31', NULL, NULL, NULL, ''),
(7, 'osn', '2015-06-09 15:21:28', NULL, NULL, NULL, 'Social networks configs'),
(8, 'antivirus', '2015-06-09 15:21:58', NULL, NULL, NULL, 'antivirus settings'),
(9, 'printer', '2015-06-09 15:22:23', NULL, NULL, NULL, ''),
(10, 'store', '2015-06-09 15:27:42', NULL, NULL, NULL, 'Store settings'),
(11, 'email', '2015-06-10 08:17:46', NULL, NULL, NULL, 'email configs and settings'),
(13, 'settings', '2015-06-16 12:20:51', '2015-06-16 12:20:51', 1, 1, 'base settings'),
(15, 'model3d', '2015-06-25 14:43:14', '2015-06-25 14:43:14', 1, 1, '3D Model configs'),
(16, 'upload', '2015-07-21 12:15:05', '2015-07-21 12:15:05', 2, 2, 'all upload settings'),
(17, 'reject', '2015-08-14 08:20:32', '2015-08-14 08:20:32', 2, 2, 'reject list'),
(18, 'mainpage', '2015-08-21 13:26:32', '2015-08-21 13:26:32', 0, 0, '');
 

INSERT INTO `system_setting` (`id`, `group_id`, `key`, `value`, `created_at`, `updated_at`, `created_user_id`, `updated_user_id`, `description`, `json`, `is_active`) VALUES
(1, 1, 'disallowusernames', 'json', '2015-08-13 14:03:25', '2015-08-13 14:03:25', 1, 2, 'not allowed usernamesss', '[ "user",    "admin",    "author",    "manager",    "administrator",    "root",    "bot",    "auto",    "mysql",    "system",    "info",    "message",    "support",    "contact",    "treatstock",    "ts",    "moderator", "list" , "id", "select",  "update", "delete", "index"]', 1),
(2, 1, 'usercanregister', '1', '2015-06-09 14:40:15', '2015-06-09 14:40:15', NULL, 1, 'Allow new users to register', '', 1),
(3, 2, 'allowextension', 'json', '2015-06-22 08:40:34', '2015-06-22 08:40:34', NULL, 1, 'can be uploaded by user', '[ "png", "jpg", "jpeg", "gif", "stl", "obj", "txt", "pdf", "mp4", "swf"]', 1),
(4, 2, 'maxsize', '100000000', '2015-06-09 14:40:19', '2015-06-09 14:40:19', NULL, 1, '100 mb in bytes', '', 1),
(5, 1, 'avatar_limit', 'json', '2015-07-10 08:05:02', '2015-07-10 08:05:02', NULL, 1, '', '{    \r\n "extension": [        "png",        "gif",   "jpeg",     "jpg"    ],     "size": 1100000,     "width": 150,     "height": 150 \r\n}', 1),
(6, 1, 'fileslimit', '50', '2015-06-09 14:40:49', '2015-06-09 14:40:49', 1, NULL, 'лимит на кол-во файлов', '', 1),
(7, 4, 'settingstimelimit', '36000', '2015-06-09 15:19:34', '2015-06-09 15:19:34', 1, 1, 'cache limit in seconds - current value is 10 hours', '', 1),
(8, 11, 'messageconfig', 'json', '2015-06-10 09:13:01', '2015-06-10 09:13:01', 1, NULL, 'general message config options', '{\r\n  "title" : { "append" : "", "prepend": " - TS Dev" },\r\n  "message": { "footer" : "Sincerely, TS Dev" }\r\n}', 1),
(9, 11, 'supportEmail', 'support@tsdev.work', '2015-06-10 09:55:33', '2015-06-10 09:55:33', 1, NULL, 'user gets notifications from this email', '', 1),
(10, 2, 'uploadserver', 'json', '2015-06-19 10:37:20', '2015-06-19 10:37:20', 1, 1, '', '[\r\n{\r\n  "name" : "server1" , "url" : "http://server1/"\r\n},\r\n{\r\n  "name" : "server2", "url" : "http://server1/"\r\n}\r\n]', 1),
(121, 15, 'allowextension', 'json', '2015-06-25 14:43:55', '2015-06-25 14:43:55', 1, NULL, 'what extensions are allowed to upload as 3D models', '["stl", "ply", "obj"]', 1),
(122, 15, 'allowscreens', 'json', '2015-06-25 14:48:02', '2015-06-25 14:48:02', 1, NULL, 'allowed screen extensions for 3d model', '["jpg", "gif", "png", "jpeg"]', 1),
(123, 15, 'colors', 'json', '2015-06-29 13:03:41', '2015-06-29 13:03:41', 1, 1, 'colors in preview 3d model', '["#ff7777", "#7777ff","#FFFFFF", "#99dd99", "#ffff77", "#777777"]', 1),
(124, 15, 'default_license_id', '1', '2015-07-08 13:18:23', '2015-07-08 13:18:23', 1, NULL, 'default license id for uploaded 3d models', '', 1),
(125, 16, 'maxfilesize', '60', '2015-08-05 09:57:30', '2015-08-05 09:57:30', 2, 2, 'max file size in MB to upload', '', 1),
(126, 1, 'trustlevel', 'json', '2015-08-11 08:13:37', '2015-08-11 08:13:37', 2, 2, '', '[\r\n    {\r\n        "title": "Normal",\r\n        "color": "gray",\r\n        "code": "normal"\r\n    },\r\n    {\r\n        "title": "Trust",\r\n        "color": "green",\r\n        "code": "trust"\r\n    },\r\n    {\r\n        "title": "Wary",\r\n        "color": "orange",\r\n        "code": "wary"\r\n    },\r\n    {\r\n        "title": "Banned",\r\n        "color": "red",\r\n        "code": "banned"\r\n    }\r\n]', 1),
(127, 17, 'virus', 'Virus detected in some models. Cannot be published', '2015-08-14 08:21:06', '2015-08-14 08:21:06', 2, NULL, '', '', 1),
(128, 17, 'authority', 'Please prove your access rights to this model', '2015-08-14 08:21:32', '2015-08-14 08:21:32', 2, NULL, '', '', 1),
(129, 17, 'unformatted', 'Please prepare your model. Check title, description and keywords to satisfy requirements.', '2015-08-14 08:22:53', '2015-08-14 08:22:53', 2, NULL, '', '', 1),
(130, 17, 'nocover', 'Please update cover image to satisfy publishing rules.', '2015-08-14 08:23:35', '2015-08-14 08:23:35', 2, NULL, '', '', 1),
(131, 18, 'showcategories', '1,2,3', '2015-08-21 13:29:01', '2015-08-21 13:29:01', 2, NULL, 'IDs which categories to show on main page', '', 1);
 