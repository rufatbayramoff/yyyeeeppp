SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE IF NOT EXISTS `user_collection` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_collection_1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_collection_model3d` (
  `id` int(11) NOT NULL,
  `collection_id` int(11) DEFAULT NULL,
  `model3d_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_collection_models_1_idx` (`model3d_id`),
  KEY `fk_user_collection_models_2_idx` (`collection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `user_collection` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL ,
CHANGE COLUMN `title` `title` VARCHAR(45) NOT NULL ,
CHANGE COLUMN `created_at` `created_at` TIMESTAMP NULL DEFAULT NULL ,
ADD UNIQUE INDEX `title_UNIQUE` (`title` ASC, `user_id` ASC);

ALTER TABLE `user_collection` 
ADD CONSTRAINT `fk_user_collection_1`
  FOREIGN KEY (`user_id`)
  REFERENCES `user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `user_collection_model3d` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `collection_id` `collection_id` INT(11) NOT NULL ,
CHANGE COLUMN `model3d_id` `model3d_id` INT(11) NOT NULL ,
CHANGE COLUMN `created_at` `created_at` TIMESTAMP NULL DEFAULT NULL ;

ALTER TABLE `user_collection_model3d` 
ADD CONSTRAINT `fk_user_collection_model3d_1`
  FOREIGN KEY (`collection_id`)
  REFERENCES `user_collection` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_user_collection_model3d_2`
  FOREIGN KEY (`model3d_id`)
  REFERENCES `model3d` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
 
INSERT INTO `model3d_license` (`id`, `title`, `description`, `is_active`, `info`) VALUES (NULL, 'Default license', NULL, '1', '');



ALTER TABLE `user_profile` 
ADD COLUMN `default_license_id` INT(11) NOT NULL DEFAULT 1 AFTER `info_skills`;
