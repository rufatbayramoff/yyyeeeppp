ALTER TABLE `user` ADD UNIQUE INDEX `username_UNIQUE` (`username` ASC);

ALTER TABLE `user` ADD UNIQUE INDEX `email_UNIQUE` (`email` ASC);

ALTER TABLE `file` CHANGE COLUMN `name` `name` VARCHAR(245) NOT NULL ;

CREATE TABLE `model3d_file_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model3d_file_id` int(11) NOT NULL,
  `length` smallint(6) NOT NULL,
  `width` smallint(6) NOT NULL,
  `height` smallint(6) NOT NULL,
  `metric` char(5) DEFAULT 'mm',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='3D Model properties after processing';

ALTER TABLE `model3d_file_properties` 
ADD INDEX `fk_model3d_properties_1_idx` (`model3d_file_id` ASC);
ALTER TABLE `model3d_file_properties` 
ADD CONSTRAINT `fk_model3d_properties_1`
  FOREIGN KEY (`model3d_file_id`)
  REFERENCES `model3d_file` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
 

CREATE TABLE IF NOT EXISTS `file_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `finished_at` timestamp NULL DEFAULT NULL,
  `operation` varchar(100) DEFAULT NULL,
  `token` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` char(15) NOT NULL,
  `result` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_file_job_1_idx` (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
 
ALTER TABLE `file_job`
  ADD CONSTRAINT `fk_file_job_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `model3d_file` 
    ADD COLUMN `file_src_id` INT(11) NULL AFTER `file_id`,
    ADD INDEX `fk_model3d_file_2_idx` (`file_src_id` ASC);
    ALTER TABLE `model3d_file` 
    ADD CONSTRAINT `fk_model3d_file_2`
      FOREIGN KEY (`file_src_id`)
      REFERENCES `file` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION;

ALTER TABLE `model3d_file_properties` 
    ADD COLUMN `parser_result` VARCHAR(1000) NULL AFTER `created_at`,
    ADD COLUMN `vertices` INT(11) NOT NULL DEFAULT 0 AFTER `parser_result`,
    ADD COLUMN `faces` INT(11) NOT NULL DEFAULT 0 AFTER `vertices`;


ALTER TABLE `user_profile` CHANGE COLUMN `current_metrics` `current_metrics` ENUM('cm','in') NOT NULL DEFAULT 'cm' ;


ALTER TABLE `file_job` CHANGE COLUMN `token` `token` VARCHAR(50);


ALTER TABLE `model3d` 
CHANGE COLUMN `is_moderated` `is_moderated` TINYINT(1) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `is_printer_ready` `is_printer_ready` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'G code ready' ;
