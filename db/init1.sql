-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 02, 2015 at 06:36 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ts_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_access_1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `access_group`
--

CREATE TABLE IF NOT EXISTS `access_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE IF NOT EXISTS `email_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `language_id` varchar(8) NOT NULL,
  `title` varchar(155) DEFAULT NULL,
  `description` varchar(245) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `template_html` mediumtext,
  `template_text` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`,`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `path` varchar(45) NOT NULL,
  `extension` char(5) NOT NULL,
  `size` int(11) DEFAULT NULL COMMENT 'bytes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `server` set('localhost','static1','static2') NOT NULL DEFAULT 'localhost',
  `status` enum('new','active','inactive','danger') NOT NULL DEFAULT 'new',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_city`
--

CREATE TABLE IF NOT EXISTS `geo_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `timezone_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gcity_1_idx` (`country_id`),
  KEY `fk_geo_city_1_idx` (`region_id`),
  KEY `fk_geo_city_2_idx` (`timezone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_country`
--

CREATE TABLE IF NOT EXISTS `geo_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `iso_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iso_code_UNIQUE` (`iso_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_location`
--

CREATE TABLE IF NOT EXISTS `geo_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `lat` decimal(8,5) DEFAULT NULL,
  `lon` decimal(8,5) DEFAULT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lat_UNIQUE` (`lat`,`lon`),
  KEY `fk_gl1_idx` (`country_id`),
  KEY `fk_gl2_idx` (`city_id`),
  KEY `fk_geo_location_1_idx` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_region`
--

CREATE TABLE IF NOT EXISTS `geo_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`,`country_id`),
  KEY `fk_geo_region_1_idx` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_timezone`
--

CREATE TABLE IF NOT EXISTS `geo_timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(254) DEFAULT NULL,
  `utc_offset` int(11) DEFAULT NULL,
  `daylight` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `model3d`
--

CREATE TABLE IF NOT EXISTS `model3d` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` varchar(245) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `is_moderated` bit(1) NOT NULL DEFAULT b'0',
  `is_printer_ready` bit(1) NOT NULL DEFAULT b'0' COMMENT 'G code ready',
  `license_id` int(11) NOT NULL,
  `dimensions` varchar(45) DEFAULT NULL COMMENT 'width x height x length',
  `status` enum('new','active','moderation','published','inactive') NOT NULL DEFAULT 'new',
  `files_count` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_model3d_1_idx` (`user_id`),
  KEY `fk_model3d_2_idx` (`license_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `model3d_file`
--

CREATE TABLE IF NOT EXISTS `model3d_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model3d_id` int(11) NOT NULL,
  `format` char(5) NOT NULL DEFAULT 'stpl',
  `name` varchar(45) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `antivirus_checked_at` timestamp NULL DEFAULT NULL,
  `moderator_status` enum('new','checking','ok','banned','review') NOT NULL DEFAULT 'new',
  `moderated_at` timestamp NULL DEFAULT NULL,
  `user_status` enum('active','inactive','published') NOT NULL DEFAULT 'inactive',
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_model3d_files_1_idx` (`model3d_id`),
  KEY `fk_model3d_file_1_idx` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `model3d_img`
--

CREATE TABLE IF NOT EXISTS `model3d_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model3d_id` int(11) NOT NULL,
  `size` int(11) DEFAULT NULL COMMENT 'File size (KB)',
  `title` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `moderated_at` timestamp NULL DEFAULT NULL,
  `is_moderated` bit(1) NOT NULL DEFAULT b'0',
  `basename` varchar(45) NOT NULL COMMENT 'File name ',
  `extension` char(5) NOT NULL DEFAULT 'png',
  `user_id` int(11) NOT NULL COMMENT 'User ID who owns file',
  `type` set('owner','review','user') NOT NULL,
  `review_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_model3d_files_1_idx` (`model3d_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Images for 3D model' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `osn_vendor`
--

CREATE TABLE IF NOT EXISTS `osn_vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `app_id` varchar(255) NOT NULL,
  `app_key` varchar(255) DEFAULT NULL,
  `app_config` varchar(1500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_currency`
--

CREATE TABLE IF NOT EXISTS `payment_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `currency_iso` varchar(5) DEFAULT NULL,
  `title_original` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `site_block`
--

CREATE TABLE IF NOT EXISTS `site_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(55) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content` text,
  `is_active` tinyint(1) DEFAULT '1',
  `config` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `language` varchar(10) NOT NULL DEFAULT 'en-US',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`,`language`),
  KEY `act_indx` (`is_active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `system_lang`
--

CREATE TABLE IF NOT EXISTS `system_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `title_original` varchar(45) DEFAULT NULL,
  `iso_code` varchar(45) DEFAULT NULL,
  `is_active` smallint(6) NOT NULL DEFAULT '1',
  `url` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iso_code_UNIQUE` (`iso_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `system_lang_message`
--

CREATE TABLE IF NOT EXISTS `system_lang_message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text,
  PRIMARY KEY (`id`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `system_lang_source`
--

CREATE TABLE IF NOT EXISTS `system_lang_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=688 ;

-- --------------------------------------------------------

--
-- Table structure for table `system_setting`
--

CREATE TABLE IF NOT EXISTS `system_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `key` char(45) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `json` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`,`group_id`),
  KEY `fk_system_setting_1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=124 ;

-- --------------------------------------------------------

--
-- Table structure for table `system_setting_group`
--

CREATE TABLE IF NOT EXISTS `system_setting_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `lastlogin_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=624 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE IF NOT EXISTS `user_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`,`access_id`),
  KEY `fk_user_rights_1_idx` (`user_id`),
  KEY `index3` (`access_id`),
  KEY `index4` (`is_active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_admin`
--

CREATE TABLE IF NOT EXISTS `user_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `field` char(45) DEFAULT NULL,
  `old_value` varchar(255) DEFAULT NULL,
  `new_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_log_1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=113 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_login_log`
--

CREATE TABLE IF NOT EXISTS `user_login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `login_type` enum('form','osn') NOT NULL,
  `result` enum('failed','ok') NOT NULL,
  `remote_addr` varchar(255) DEFAULT NULL,
  `http_user_agent` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logout_at` timestamp NULL DEFAULT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `post_data` mediumtext,
  PRIMARY KEY (`id`),
  KEY `fk1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_osn`
--

CREATE TABLE IF NOT EXISTS `user_osn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `osn_code` char(25) NOT NULL,
  `uid` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(3) DEFAULT NULL,
  `auth_response` varchar(3000) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_osn_UNIQUE` (`user_id`,`osn_code`),
  KEY `fk_user_osn_1_idx` (`user_id`),
  KEY `fk_user_osn_2_idx` (`osn_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Users social networks' AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE IF NOT EXISTS `user_profile` (
  `user_id` int(11) NOT NULL,
  `dob_date` date DEFAULT NULL COMMENT 'Date of birth',
  `location_id` int(11) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT '',
  `phone_confirmed` bit(1) DEFAULT b'0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `current_lang` varchar(5) NOT NULL,
  `current_currency_iso` varchar(45) NOT NULL DEFAULT 'USD',
  `current_metrics` enum('sm','in') NOT NULL DEFAULT 'sm',
  `timezone_id` varchar(45) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `is_printservice` tinyint(1) DEFAULT '0',
  `is_modelshop` tinyint(1) DEFAULT '0',
  `trust_level` enum('untrusted','trusted','verified') NOT NULL DEFAULT 'untrusted' COMMENT 'Trust level',
  `avatar_url` varchar(255) DEFAULT NULL,
  `background_url` varchar(255) DEFAULT NULL,
  `gender` enum('male','female','none','multi') DEFAULT NULL,
  `printservice_title` varchar(255) DEFAULT NULL,
  `modelshop_title` varchar(255) DEFAULT NULL,
  `info_about` text,
  `info_skills` text,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `index4` (`user_id`),
  KEY `fk_user_profile_1_idx` (`user_id`),
  KEY `fk_user_profile_2_idx` (`timezone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `access`
--
ALTER TABLE `access`
  ADD CONSTRAINT `fk_access_1` FOREIGN KEY (`group_id`) REFERENCES `access_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `geo_city`
--
ALTER TABLE `geo_city`
  ADD CONSTRAINT `fk_gcity_1` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_geo_city_1` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_geo_city_2` FOREIGN KEY (`timezone_id`) REFERENCES `geo_timezone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `geo_location`
--
ALTER TABLE `geo_location`
  ADD CONSTRAINT `fk_geo_location_1` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gl1` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gl2` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `geo_region`
--
ALTER TABLE `geo_region`
  ADD CONSTRAINT `fk_geo_region_1` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `model3d`
--
ALTER TABLE `model3d`
  ADD CONSTRAINT `fk_model3d_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `model3d_file`
--
ALTER TABLE `model3d_file`
  ADD CONSTRAINT `fk_model3d_files_1` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_model3d_file_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `system_lang_message`
--
ALTER TABLE `system_lang_message`
  ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `system_lang_source` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `system_setting`
--
ALTER TABLE `system_setting`
  ADD CONSTRAINT `fk_system_setting_1` FOREIGN KEY (`group_id`) REFERENCES `system_setting_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_access`
--
ALTER TABLE `user_access`
  ADD CONSTRAINT `fk_user_access_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_access_ibfk_1` FOREIGN KEY (`access_id`) REFERENCES `access` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_log`
--
ALTER TABLE `user_log`
  ADD CONSTRAINT `fk_user_log_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_login_log`
--
ALTER TABLE `user_login_log`
  ADD CONSTRAINT `fk_user_login_log_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_osn`
--
ALTER TABLE `user_osn`
  ADD CONSTRAINT `fk_user_osn_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `fk_user_profile_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

 
ALTER TABLE  `user` ADD  `lastlogin_at` INT NOT NULL AFTER  `updated_at` ;
 

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

INSERT INTO `system_lang` (`iso_code`, `title`, `title_original`, `url`, `is_active`) VALUES ('en-US', 'English US', 'English US', 'en', 1);
INSERT INTO `payment_currency` (`title`, `is_active`, `currency_iso`, `title_original`) VALUES ('USD', '1', 'USD', 'USD');

