SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

ALTER TABLE `file` CHANGE COLUMN `status` `status` ENUM('new','active','inactive','danger','deleted') NOT NULL DEFAULT 'new' ;

ALTER TABLE `model3d` 
ADD COLUMN `cover_file_id` INT(11) NULL AFTER `files_count`,
ADD INDEX `fk_model3d_3_idx` (`cover_file_id` ASC);
ALTER TABLE `model3d` 
ADD CONSTRAINT `fk_model3d_3`
  FOREIGN KEY (`cover_file_id`)
  REFERENCES `file` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


CREATE TABLE IF NOT EXISTS `model3d_tag` (
  `id` int(11) NOT NULL,
  `model3d_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `site_tag`
--

CREATE TABLE IF NOT EXISTS `site_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(45) NOT NULL,
  `status` set('new','active','inactive','blocked') NOT NULL DEFAULT 'new',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `qty_used` mediumint(9) NOT NULL DEFAULT '0',
  `object_type` set('model3d','printer') NOT NULL DEFAULT 'model3d',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model3d_tag`
--
ALTER TABLE `model3d_tag`
  ADD CONSTRAINT `fk_model3d_tag_1` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_model3d_tag_2` FOREIGN KEY (`tag_id`) REFERENCES `site_tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `model3d_tag` 
ADD UNIQUE INDEX `model3d_id_UNIQUE` (`model3d_id` ASC, `tag_id` ASC);

ALTER TABLE `model3d_tag` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
