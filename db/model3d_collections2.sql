SET FOREIGN_KEY_CHECKS=0;

ALTER TABLE `user_collection` 
ADD COLUMN `is_active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `updated_at`;

ALTER TABLE `user_collection` 
ADD COLUMN `is_visible` TINYINT(1) NOT NULL DEFAULT 1 AFTER `is_active`;

ALTER TABLE `user_profile` 
ADD COLUMN `default_collection_id` INT NULL AFTER `default_license_id`,
ADD INDEX `fk_user_profile_2_idx1` (`default_collection_id` ASC);
ALTER TABLE `user_profile` 
ADD CONSTRAINT `fk_user_profile_2`
  FOREIGN KEY (`default_collection_id`)
  REFERENCES `user_collection` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `user_collection_model3d` 
ADD UNIQUE INDEX `collection_id_UNIQUE` (`collection_id` ASC, `model3d_id` ASC);


CREATE TABLE IF NOT EXISTS `user_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `object_type` set('model','img','ps','pub') NOT NULL,
  `object_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `object_type_UNIQUE` (`object_type`,`object_id`,`user_id`),
  KEY `user_id_UNIQUE` (`user_id`),
  KEY `fk_user_favorites_1_idx` (`object_type`,`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_like`
--
ALTER TABLE `user_like`
  ADD CONSTRAINT `fk_user_favorites_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
