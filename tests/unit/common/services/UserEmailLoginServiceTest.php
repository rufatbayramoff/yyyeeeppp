<?php
/**
 * User: nabi
 */

namespace common\services;


use Codeception\Util\Stub;
use common\components\BaseAR;
use common\components\Emailer;
use common\components\WebResponse;
use common\models\StoreOrder;
use common\models\ThingiverseOrder;
use common\models\ThingiverseUser;
use common\models\User;
use common\models\UserEmailLogin;
use common\repositories\BaseActiveRecordRepository;

class UserEmailLoginServiceTest extends \PHPUnit_Framework_TestCase
{



    /**
     * @test
     */
    public function checkGenerateEmailLogin()
    {
        $emailLoginService = \Yii::createObject(['class'=>UserEmailLoginService::class, 'arRepository' => $this->getArRepoStub(false)]);
        $user = new User();
        $user->id = 1;
        $user->username = 'Test';
        $email = 'test@vcap.me';
        $emailLogin = $emailLoginService->generateEmailLogin($user, $email);
        verify("new login by email is correct", $emailLoginService->isValidEmailLogin($emailLogin))->true();
    }

    /**
     * @test
     */
    public function checkRequestLoginByEmail()
    {
        $emailerStub = Stub::make(
            Emailer::class,
            [
                'sendLoginLinkByEmail' => Stub::once(
                    function () {
                        return true;
                    }
                ),
            ],
            $this
        );
        $emailLoginService = \Yii::createObject(['class' => UserEmailLoginService::class, 'arRepository' => $this->getArRepoStub()]);
        $emailLoginService->emailer = $emailerStub;
        $email = 'test@vcap.me';
        $url = 'http://ts.vcap.me/workbench/order/view/86';
        $user = new User();
        $user->id = 1;
        $user->username = 'Test';
        $emailLogin = $emailLoginService->requestLoginByEmail($user, $email, $url);

        verify('valid object', $emailLogin)->isInstanceOf(UserEmailLogin::class);
    }

    /**
     * @test
     */
    public function checkRequestLoginByEmailWithThingiverseOrder()
    {
        $emailerStub = Stub::make(
            Emailer::class,
            [
                'sendLoginLinkByEmailThingiverse' => Stub::once(
                    function () {
                        return true;
                    }
                ),
            ],
            $this
        );
        $emailLoginService = \Yii::createObject(['class' => UserEmailLoginService::class, 'arRepository' => $this->getArRepoStub()]);
        $emailLoginService->emailer = $emailerStub;

        $thingiverseOrder = $this->getThingiverseOrdreStub();
        $emailLogin = $emailLoginService->requestLoginByEmailWithThingiverseOrder($thingiverseOrder);
        verify('valid object', $emailLogin)->isInstanceOf(UserEmailLogin::class);
    }

    /**
     * @test
     */
    public function checkValidEmailLogin()
    {
        $emailLoginService = \Yii::createObject(['class' => UserEmailLoginService::class, 'arRepository' => $this->getArRepoStub(false)]);
        $emailLogin = $this->getStub();
        $emailLogin->expired_at = '2017-01-01 10:10:10';
        verify("login is expired", $emailLoginService->isValidEmailLogin($emailLogin))->false();

        $emailLogin->expired_at = '2021-12-10 10:10:10'; // update after 3 years :)
        verify("login is not expired", $emailLoginService->isValidEmailLogin($emailLogin))->true();

        $emailLoginService->markAsVisited($emailLogin);// should fail even if expired at in future
        verify("login is expired", $emailLoginService->isValidEmailLogin($emailLogin))->false();
    }

    /**
     * @test
     */
    public function checkMarkAsVisited()
    {
        $emailLoginService = \Yii::createObject(['class' => UserEmailLoginService::class, 'arRepository' => $this->getArRepoStub(false)]);
        $emailLogin = $this->getStub();
        $emailLoginService->markAsVisited($emailLogin);
        verify("login is expired", $emailLoginService->isValidEmailLogin($emailLogin))->false();
    }

    public function testLoginByEmailLink()
    {
        $emailLoginService = \Yii::createObject(['class' => UserEmailLoginService::class, 'arRepository' => $this->getArRepoStub()]);
        $emailLogin = $this->getStub();

        $response = $emailLoginService->signInByUserEmailLogin($emailLogin);
        verify("login with redirect is ok", $response)->isInstanceOf(WebResponse::class);
        verify("login is ok", $response->isRedirection)->true();
    }

    /**
     * @return UserEmailLogin
     */
    private function getStub(): UserEmailLogin
    {
        $emailLogin = Stub::make(
            UserEmailLogin::class,
            [
                'safeSave' => function () {
                    return true;
                }
            ],
            $this
        );

        $emailLogin->requested_url = 'http://ts.vcap.me';
        $emailLogin->visited_at = null;
        $emailLogin->user_id = 1;
        $emailLogin->hash = 'asdfasdf';
        $emailLogin->email = 'email@mail.ru';
        $emailLogin->expired_at = '2021-12-10 10:10:10';
        return $emailLogin;
    }

    /**
     * @return ThingiverseOrder
     */
    private function getThingiverseOrdreStub()
    {
        $user = Stub::make(
            User::class,
            [
                'id'                    => 1,
                'username'              => 'Test',
                'getFullNameOrUsername' => function () {
                    return 'Full name';
                }
            ]
        );
        $user->populateRelation('thingiverseUser', Stub::make(ThingiverseUser::class, ['email' => 'thinguser@mail.com']));
        $order = Stub::make(StoreOrder::class, ['id' => 10]);
        $order->populateRelation('user', $user);
        $thingiverseOrder = Stub::make(ThingiverseOrder::class);
        $thingiverseOrder->populateRelation('order', $order);
        return $thingiverseOrder;
    }

    private function getArRepoStub($needSave = true)
    {
        $arRepo = Stub::make(
            BaseActiveRecordRepository::class,
            [
                'save' => $needSave? Stub::atLeastOnce(function (BaseAR $ar) {
                    return true;
                }) :function (BaseAR $ar) {
                    return true;
                }
            ],
            $this
        );
        return $arRepo;
    }
}
