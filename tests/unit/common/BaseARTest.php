<?php
namespace common;


use test\unit\CodeceptionTestCase;

class BaseARTest extends CodeceptionTestCase
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $transaction;
    protected function _before()
    {
        $this->transaction = app('db')->beginTransaction();
    }

    protected function _after()
    {
        $this->transaction->rollBack();
    }

    
    public function testAddRecord()
    {
        $result = models\UserAddress::addRecord([
            'user_id' => 1,
            'created_at' => dbexpr('NOW()'),
            'contact_name' => 'John Smith',
            'country_id' => 12,
            'region' => 'New York',
            'city' => 'New York',
            'address' => 'Park Avenue',
            'extended_address' => 'Building 1A, Office 12B',
            'type' => models\UserAddress::TYPE_ALL
        ]);
        $this->assertEquals(true, $result->id > 0);
    }
}