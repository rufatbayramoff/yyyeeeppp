<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.12.17
 * Time: 14:59
 */

namespace tests\unit\common\models;

use common\models\factories\FileFactory;
use common\models\File;
use common\models\repositories\FileRepository;
use test\unit\CodeceptionTestCase;

class FileTest extends CodeceptionTestCase
{
    public const TEST_JPG1_PATH = __DIR__ . '/../../files/fileTest/imgpsh_fullsize.jpg';
    public const MD5_JPG1 = 'df7fcde68c40dbf9fa9d3f29765c226c';
    public const TEST_STL1_PATH = __DIR__ . '/../../files/fileTest/LightForJimny.stl';
    public const STL1_JPG = '4810f7b846f80a9a18b2d536fe0897a2';

    public function setUp()
    {
        $this->cleanTest();
    }

    /**
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function testCreateFileJpgCreate()
    {
        $fileRepository = \Yii::createObject(FileRepository::class);
        $fileFactory = \Yii::createObject(FileFactory::class);
        $file = $fileFactory->createFileFromPath(self::TEST_JPG1_PATH);
        $fileRepository->save($file);
        $privatePath = $file->getLocalTmpFilePath();
        self::assertStringStartsWith('/vagrant/repo/frontend/storage/files/', $privatePath, 'Invalid save dir.');
        self::assertEquals(md5_file($privatePath), self::MD5_JPG1);
        $file->setPublicMode(true);
        $fileRepository->save($file);
        $publicPath = $file->getLocalTmpFilePath();
        self::assertStringStartsWith('/vagrant/repo/frontend/web/static/files', $publicPath, 'Invalid save public dir.');
        self::assertEquals(md5_file($publicPath)    , self::MD5_JPG1);
    }

    public function tearDown()
    {
        $this->cleanTest();
    }

    protected function cleanTest()
    {
        $fileForDelete = File::find()
            ->md5([self::MD5_JPG1, self::STL1_JPG])
            ->all();

        foreach ($fileForDelete as $file) {
            $file->delete();
        }
    }
}