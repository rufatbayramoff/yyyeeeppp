<?php
/**
 * Date: 20.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\components\autofill;


use common\models\Model3d;
use common\modules\seo\models\SeoAutofillTemplate;
use common\modules\seo\placeholders\Model3dPlaceholder;
use lib\message\MessageRenderHelper;
use test\unit\CodeceptionTestCase;
use yii\db\ActiveRecord;

class Model3dAutofillTest  extends CodeceptionTestCase
{

    public function testGetQuery()
    {

        $modelAutofill = new Model3dAutofill(new Model3dPlaceholder(), new ActiveRecord(), new SeoAutofillTemplate());

        self::assertEquals(
            'SELECT `model3d`.* FROM `model3d` INNER JOIN `store_unit` ON store_unit.model3d_id=model3d.id LEFT JOIN `seo_page_autofill` ON seo_page_autofill.object_id=model3d.id AND seo_page_autofill.object_type="model3d" LEFT JOIN `product_common` ON product_common.uid=model3d.product_common_uid WHERE ((`seo_page_autofill`.`id` IS NULL) AND (`product_common`.`product_status`=\'published_public\')) AND (NOT (`model3d`.`published_at` IS NULL)) LIMIT 10000',
            $modelAutofill->getQuery()->createCommand()->getRawSql());
    }

    public function testParseTemplate()
    {
        $model3dPlaceholder = new Model3dPlaceholder();
        $model3dPlaceholder->modelTitle = '3dmodel';
        $model3dPlaceholder->modelUser = 'author';
        $model3dPlaceholder->modelCategory = 'Cat';
        $model3dPlaceholder->modelKeywords ='tag1, tag2';

        $filledData = $model3dPlaceholder->getFilledPlaceholders();
        $template = '%modelTitle% by %modelUser% in %modelCategory% with %modelKeywords%';
        $result = MessageRenderHelper::renderTemplate($template, $filledData);
        self::assertEquals('3dmodel by author in Cat with tag1, tag2', $result);
    }
}
