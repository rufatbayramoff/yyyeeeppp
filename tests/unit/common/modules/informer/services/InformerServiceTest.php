<?php
/**
 * User: nabi
 */

namespace common\modules\informer\services;


use common\models\User;
use common\modules\informer\models\CustomerOrderInformer;
use common\modules\informer\models\ProductInformer;
use common\modules\informer\models\CompanyServiceInformer;
use common\modules\informer\models\TaxInformer;

class InformerServiceTest extends \PHPUnit_Framework_TestCase
{

    public function testCreateInformer()
    {
        $informerService = \Yii::createObject(InformerService::class);
        $user = new User();
        $user->id = 1234;
        $informer = $informerService->createInformerForUser($user, TaxInformer::class);
        verify('informer created with key', $informer->getKey())->equals($informerService->generateKey($user, TaxInformer::class));
    }

    public function testGetInformerForUser()
    {
        $informerService = \Yii::createObject(InformerService::class);
        $user = new User();
        $user->id = 12341;
        $informer = $informerService->createInformerForUser($user, TaxInformer::class);

        // now find
        $informer = $informerService->getInformerForUser($user, TaxInformer::class);
        verify('informer found with key', $informer->getKey())->equals($informerService->generateKey($user, TaxInformer::class));

        // now fail find
        $informer2 = $informerService->getInformerForUser($user, CustomerOrderInformer::class);
        verify('verify not found with key', $informer2)->isEmpty();

    }

    public function testRemoveInformer()
    {
        $informerService = \Yii::createObject(InformerService::class);
        $user = new User();
        $user->id = 12342;
        $informer = $informerService->createInformerForUser($user, TaxInformer::class);

        $informerService->removeUserInformer($user, TaxInformer::class);

        $informer2 = $informerService->getInformerForUser($user, TaxInformer::class);
        verify('informer deleted', $informer2)->isEmpty();

        // try to delete not found informer
        $informerService->removeUserInformer($user, ProductInformer::class);
        $informer2 = $informerService->getInformerForUser($user, ProductInformer::class);
        verify('informer deleted', $informer2)->isEmpty();
    }

    public function testGetUserInformers()
    {
        $informerService = \Yii::createObject(InformerService::class);
        $user = new User();
        $user->id = 12343;
        $informerService->createInformerForUser($user, TaxInformer::class);
        $informerService->createInformerForUser($user, ProductInformer::class);
        $informerService->createInformerForUser($user, CompanyServiceInformer::class);

        $informers = $informerService->getUserInformers($user);

        verify('find all 3d informers', count($informers))->equals(3);
    }
}