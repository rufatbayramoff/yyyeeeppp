<?php
/**
 * Date: 06.04.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\models;

require_once __DIR__ . '/../DumpHelper.php';

use common\modules\thingPrint\components\DumpHelper;
use common\modules\thingPrint\factories\ThingFactory;
use test\unit\CodeceptionTestCase;

class ThingFactoryTest extends CodeceptionTestCase
{

    public function testDetails()
    {
        $json = DumpHelper::api_getThing();
        $thingFactory = new ThingFactory();
        $thing = $thingFactory->createThingByJson($json);
        self::assertNotEmpty($thing->getDetails());
    }
}