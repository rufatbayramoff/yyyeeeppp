<?php
/**
 * Date: 31.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\models;

require_once __DIR__ . '/../DumpHelper.php';

use common\models\User;
use common\modules\thingPrint\components\DumpHelper;
use test\unit\CodeceptionTestCase;

class ThingiverseUserTest extends CodeceptionTestCase
{

    public function testGetUser()
    {
        $json = DumpHelper::api_getMe();
        $thingiverseUser = new ThingUser($json);

        self::assertEquals($json['full_name'], $thingiverseUser->full_name);
        self::assertInstanceOf(ThingUser::class, $thingiverseUser);
    }

}
