<?php
/**
 * Date: 27.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\models;

require_once __DIR__ . '/../DumpHelper.php';

use common\modules\thingPrint\components\DumpHelper;
use test\unit\CodeceptionTestCase;

class ThingFileTest extends CodeceptionTestCase
{
    public function testThingFile()
    {
        $json = DumpHelper::api_getThingFiles15501();

        $thingOrder = new ThingFile($json[0]);
        self::assertEquals('http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_display_large.jpg', $thingOrder->getRender());

        self::assertEquals('2013-09-23 09:35:53', $thingOrder->date);
    }
}