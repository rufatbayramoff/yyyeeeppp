<?php
/**
 * Date: 31.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\factories;


require_once __DIR__ . '/../DumpHelper.php';

use common\models\User;
use common\modules\thingPrint\components\DumpHelper;
use common\modules\thingPrint\models\ThingUser;
use test\unit\CodeceptionTestCase;

class ThingiverseUserFactoryTest  extends CodeceptionTestCase
{

    public function testCreate()
    {
        $json = DumpHelper::api_getMe();
        $factory = new ThingiverseUserFactory();

        $thingUser = new ThingUser($json);
        $user = $factory->createUser($thingUser);
        self::assertInstanceOf(User::class, $user);
        self::assertEquals($json['full_name'], $user->userProfile->full_name);
    }
}
