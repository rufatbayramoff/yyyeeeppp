<?php
/**
 * Date: 16.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\components;

require_once __DIR__ . '/../DumpHelper.php';

use Codeception\Util\Stub;
use common\models\factories\FileFactory;
use common\models\File;
use common\models\repositories\FileRepository;
use common\models\repositories\UserSessionRepository;
use common\models\User;
use common\models\UserSession;
use common\modules\thingPrint\factories\ThingFactory;
use common\modules\thingPrint\models\Thing;
use common\modules\thingPrint\models\ThingFile;
use common\modules\thingPrint\models\ThingOrder;
use frontend\models\user\FrontUser;
use test\unit\CodeceptionTestCase;

class ThingiverseApiWrapperTest extends CodeceptionTestCase
{

    protected $tester;

    protected $transaction;

    public function testInit()
    {
        $api = Stub::make(
            ThingiverseApi::class,
            [
                'response_data' => null,
                'getThingFiles' => function () {
                    return ThingiverseApi::SUCCESS_CODE;
                },
                'getThing'      => function () {
                    return ThingiverseApi::SUCCESS_CODE;
                }
            ]
        );
        Stub::update(
            $api,
            [
                'response_data' => null,
                'getThingFiles' => function ($id) use ($api) {
                    Stub::update($api, ['response_data' => DumpHelper::api_getThingFiles()]);
                    return ThingiverseApi::SUCCESS_CODE;
                },
                'getThing'      => function ($id) use ($api) {
                    Stub::update($api, ['response_data' => DumpHelper::api_getThing()]);
                    return ThingiverseApi::SUCCESS_CODE;
                }
            ]
        );
        $wrapper = new ThingiverseApiWrapper($api);
        $thing = $wrapper->getThing(1);
        self::assertInstanceOf(Thing::class, $thing);
        self::assertInstanceOf(ThingFile::class, $wrapper->getThingFiles(1)[0]);
    }

    public function testGetOrder()
    {
        $api = Stub::make(
            ThingiverseApi::class,
            [
                'response_data' => null,
                'getOrder' => function ($id) {
                    return ThingiverseApi::SUCCESS_CODE;
                },
            ]
        );
        Stub::update(
            $api,
            [
                'response_data' => null,
                'getOrder' => function ($id) use ($api) {
                    Stub::update($api, ['response_data' => DumpHelper::api_getOrder($id)]);
                    return ThingiverseApi::SUCCESS_CODE;
                },
            ]
        );
        $wrapper = new ThingiverseApiWrapper($api);
        $thingOrder = $wrapper->getOrder(1);
        self::assertInstanceOf(ThingOrder::class, $thingOrder);
        self::assertEquals($thingOrder->id, 1);
    }

    /**
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function testApiCover()
    {
        $thingFactory = new ThingFactory();
        $thing = $thingFactory->createThingByJson(DumpHelper::api_getThing());
        $thingApi = new ThingiverseApi('9ee60ccf1cf910167fb6d59d08e9b181');
        $user = FrontUser::findByPk(User::USER_THINGIVERSE);
        $userSession = UserSession::tryFindByPk(1);
        $api = new ThingiverseApiWrapper($thingApi);

        $fileRepository = \Yii::createObject(FileRepository::class);
        $fileFactory = \Yii::createObject(FileFactory::class);
        $fileFactory->user = $user;
        $fileFactory->userSession = $userSession;
        $coverFile = $fileFactory->createFileFromPath($api->downloadThingCover($thing));
        $fileRepository->save($coverFile);
        self::assertTrue($coverFile->id > 0);
    }

}