<?php
/**
 * User: nabi
 */

namespace common\modules\payment;

use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\models\User;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayProvider;
use common\modules\payment\services\PaymentCheckoutService;
use lib\money\Money;

/**
 * Class PaymentCheckoutServiceTest
 *
 *
 * @link https://developers.braintreepayments.com/reference/general/testing/php
 * @package common\modules\payment
 */
class PaymentCheckoutServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function payAndGetTransaction()
    {
        // in
        $gateway = PaymentGateway::GATEWAY_TS;
        $amount = Money::usd(100);
        $customer = new User();
        $order = new StoreOrder();

        // unit of work
        $pgp = new PaymentGatewayProvider();
        $paymentTransaction = new PaymentTransaction();
        $checkoutService = new PaymentCheckoutService();
        $checkoutService->setGateway($pgp->getGatewayByCode($gateway));

        // 1. create payment transaction, 2. bind object(order,payout,invoice,...) + transaction, 3. create double entry
        $checkoutService->setPaymentTransaction($paymentTransaction);
        // out [payment_transaction, payment, payment_details]
        $payment = $checkoutService->pay($amount);
        //$paymentService->bindPaymentToOrder($payment, $order);
    }
}
