<?php
/**
 * User: nabi
 */

namespace common\modules\payment\services;


use AspectMock\Test;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionRefund;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use lib\money\Money;

class RefundServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkCanRefund()
    {
        $pt = $this->getPaymentTransaction();
        $refundService = \Yii::createObject(RefundService::class);

        $isRefundable = $refundService->canRefund($pt, Money::usd(0));
        verify('0 refund for payment false', $isRefundable)->false();

        $isRefundable = $refundService->canRefund($pt, Money::usd(100));
        verify('full refund for payment ok', $isRefundable)->true();
        $isRefundable = $refundService->canRefund($pt, Money::usd(50));
        verify('partial refund for payment ok', $isRefundable)->true();

        $isRefundable = $refundService->canRefund($pt, Money::usd(150));
        verify('over refund for payment check', $isRefundable)->false();

        $pt->type = PaymentTransaction::TYPE_REFUND;
        $isRefundable = $refundService->canRefund($pt, Money::usd(50));
        verify('refund to refund check', $isRefundable)->false();

        $pt->status = PaymentGatewayTransaction::STATUS_SUBMITED_SETTLE;
        $isRefundable = $refundService->canRefund($pt, Money::usd(50));
        verify('partial refund for payment with wrong status', $isRefundable)->false();
    }

    /**
     * @test
     */
    public function refundByForm()
    {
        $pt = $this->getPaymentTransaction();

        $refundModel = new PaymentTransactionRefund();

        $refundModel->status = PaymentTransactionRefund::STATUS_IN_PROGRESS;
        $refundModel->transaction_id = $pt->id;
        $refundModel->currency = $pt->currency;
        $refundModel->amount = $pt->amount;

        verify('full refund - not found', $refundModel->validate())->false();
    }

    /**
     * @test
     */
    public function refundByFormFail()
    {
        $pt = $this->getPaymentTransaction();

        $refundModel = new PaymentTransactionRefund();

        $refundModel->status = PaymentTransactionRefund::STATUS_IN_PROGRESS;
        $refundModel->transaction_id = $pt->id;
        $refundModel->currency = $pt->currency;
        $refundModel->amount = 200;

        expect('over full refund not ok', $refundModel->validate())->false();
    }

    protected function getPaymentTransaction(): PaymentTransaction
    {
        $pt = new PaymentTransaction();

        $pt->amount = 100;
        $pt->vendor = PaymentGateway::GATEWAY_TS;
        $pt->id = 1;
        $pt->transaction_id = 'not valid';
        $pt->currency = 'USD';
        $pt->type = PaymentTransaction::TYPE_PAYMENT;
        $pt->status = PaymentGatewayTransaction::STATUS_SETTLED;

        return $pt;
    }
}
