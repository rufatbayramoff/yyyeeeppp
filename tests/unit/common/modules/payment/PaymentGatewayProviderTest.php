<?php
/**
 * User: nabi
 */

namespace common\modules\payment;

use common\modules\payment\components\PaymentCriteria;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayProvider;
use lib\money\Money;


/**
 *
 * @package common\modules\payment\factories
 */
class PaymentGatewayProviderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var PaymentGatewayProvider
     */
    private $provider;

    public function setUp()
    {
        $this->provider = new PaymentGatewayProvider();
    }

    /**
     * @test
     */
    public function getPaymentGateways()
    {
        $paymentGatewayProvider = $this->provider;
        $gateways = $paymentGatewayProvider->getGatewaysNames();
        self::assertTrue(is_array($gateways));
    }

    /**
     * @test
     */
    public function getPaymentGatewayByCode()
    {
        $paymentGatewayProvider = $this->provider;

        $gateway = $paymentGatewayProvider->getGatewayByCode(PaymentGateway::GATEWAY_TS);
        verify('gateway is an object', $gateway)->notNull();
        verify('gateway is an object', $gateway)->isInstanceOf(PaymentGateway::class);
        verify('gateway object is correct', $gateway->code == PaymentGateway::GATEWAY_TS);

        $gateway = $paymentGatewayProvider->getGatewayByCode('no gateway');
        verify('check null gateway', $gateway)->null();
    }

    /**
     * @test
     */
    public function getPaymentGatewayByCriteria()
    {
        $criteria = PaymentCriteria::create()
            ->setCountry('USA')
            ->setIsThingiverse(false);

        verify('criteria object', $criteria)->notNull();

        $gateways = $this->provider->getGatewaysNamesByCriteria($criteria);
        verify('get gateways by criteria', $gateways)->notNull();

        $criteria->setPaymentAmount(Money::create(100, 'USD'));
        $gateways = $this->provider->getGatewaysNamesByCriteria($criteria);
        verify('no invoice payment', $gateways)->notContains(PaymentGateway::GATEWAY_INVOICE);

        $criteria->setPaymentAmount(Money::create(400, 'USD'));
        $criteria->setTreatstockBalance(Money::create(100, 'USD'));
        $gateways = $this->provider->getGatewaysNamesByCriteria($criteria);
        verify('has invoice payment', $gateways)->contains(PaymentGateway::GATEWAY_INVOICE);
        verify('no balance payment', $gateways)->notContains(PaymentGateway::GATEWAY_TS);
    }
}
