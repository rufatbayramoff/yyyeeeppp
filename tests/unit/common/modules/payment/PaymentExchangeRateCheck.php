<?php
/**
 * User: nabi
 */

namespace common\modules\payment;


use common\components\PaymentExchangeRateFacade;

class PaymentExchangeRateCheck extends \PHPUnit_Framework_TestCase
{

    public function testExchangeRate()
    {
        $amount = PaymentExchangeRateFacade::convert(100, 'EUR', 'USD');
        verify('amount', $amount > 100)->true();
    }
}