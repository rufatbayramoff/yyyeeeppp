<?php
/**
 * User: nabi
 */

namespace common\modules\payment;


use common\components\PaymentExchangeRateConverter;
use common\components\PaymentExchangeRateFacade;
use common\models\PaymentExchangeRate;
use common\modules\payment\components\PaymentCriteria;
use lib\money\Money;

class PaymentCriteriaTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function checkCriteria()
    {
        $criteria = PaymentCriteria::create()
            ->setTreatstockBalance(Money::create(100, 'USD'))
            ->setCountry('USA')
            ->setIsThingiverse(false)
            ->setPaymentAmount(Money::create(400, 'USD'));

        verify('country check', $criteria->getCountry())->equals('USA');
        verify('is thingiverse check', $criteria->isThingiverse())->equals(false);
        verify('balance check', $criteria->getTreatstockBalance())->equals(Money::create(100, 'USD'));
    }

    /**
     * @test
     */
    public function checkCriteriaPayByInvoice()
    {
        $criteria = PaymentCriteria::create()
            ->setPaymentAmount(Money::create(420, 'USD'));

        verify('can pay by invoice', $criteria->canPayByInvoice())->true();

        $criteria->setPaymentAmount(Money::create(100, 'USD'));
        verify('cannot pay by invoice', $criteria->canPayByInvoice())->false();
    }


    /**
     * @test
     */
    public function checkCriteriaPayByBalance()
    {
        $criteria = PaymentCriteria::create()
            ->setPaymentAmount(Money::create(399, 'EUR'));

        verify('cannot pay by balance', $criteria->canPayByTreatstockBalance())->false();

        $criteria->setPaymentAmount(Money::create(100, 'EUR'));
        verify('cannot pay by balance', $criteria->canPayByTreatstockBalance())->false();

        $criteria->setTreatstockBalance(Money::create(100, 'USD'));
        verify('cannot pay by balance - different currency', $criteria->canPayByTreatstockBalance())->false();

        $criteria->setTreatstockBalance(Money::create(100, 'EUR'));
        verify('can pay by balance - same currency', $criteria->canPayByTreatstockBalance())->true();
    }

}
