<?php
/**
 * Date: 05.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\promocode\components;


use common\models\Promocode;
use common\modules\promocode\exceptions\NotValidPromocode;
use test\unit\CodeceptionTestCase;

class PromocodeUsageValidatorTest extends CodeceptionTestCase
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $transaction;

    protected function _before()
    {
        $this->transaction = app('db')->beginTransaction();
    }

    protected function _after()
    {
        $this->transaction->rollBack();
    }

    public function testTryValidate()
    {
        $validator = new PromocodeUsageValidator();
        $promo = new Promocode();
        $promo->setAttributes([
            'usage_type' => Promocode::USAGE_TYPE_ONE,
            'discount_amount' => 20,
            'discount_currency' => 'USD',
            'discount_for' => 'print'
        ]);
        self::expectException(NotValidPromocode::class);
        $validator->tryValidate($promo);

        $promo->setAttributes([
            'usage_type' => Promocode::USAGE_TYPE_ONE,
            'discount_amount' => 20,
            'discount_currency' => 'USD',
            'discount_for' => 'print',
            'valid_from' => date('Y-m-d H:i:s', strtotime('-1 day')),
            'valid_to' => date('Y-m-d H:i:s', strtotime('+1 day')),
            'is_active' => 1,
            'is_valid' => 1,
        ]);
        self::assertEquals(true, $validator->tryValidate($promo));
    }

    public function testValidateFlagsInactive()
    {
        $validator = new PromocodeUsageValidator();
        $promo = new Promocode();
        $promo->setAttributes([
            'usage_type' => Promocode::USAGE_TYPE_ONE,
            'discount_amount' => 20,
            'discount_currency' => 'USD',
            'discount_for' => 'print',
            'valid_from' => date('Y-m-d H:i:s', strtotime('-1 day')),
            'valid_to' => date('Y-m-d H:i:s', strtotime('+20 day')),
            'is_active' => 0,
            'is_valid' => 1,
        ]);
        self::expectException(NotValidPromocode::class);
        $validator->tryValidate($promo);
    }

    public function testValidateFlagsInvalid()
    {
        $validator = new PromocodeUsageValidator();
        $promo = new Promocode();
        $promo->setAttributes([
            'usage_type' => Promocode::USAGE_TYPE_ONE,
            'discount_amount' => 20,
            'discount_currency' => 'USD',
            'discount_for' => 'print',
            'valid_from' => date('Y-m-d H:i:s', strtotime('-1 day')),
            'valid_to' => date('Y-m-d H:i:s', strtotime('+20 day')),
            'is_active' => 1,
            'is_valid' => 0,
        ]);
        self::expectException(NotValidPromocode::class);
        $validator->tryValidate($promo);
    }

    public function testValidateTypeGuest()
    {
        $validator = new PromocodeUsageValidator();
        $validator->setCurrentUser(null);
        $promo = new Promocode();
        $promo->setAttributes([
            'usage_type' => Promocode::USAGE_TYPE_ONE_PER_USER,
            'discount_amount' => 20,
            'discount_currency' => 'USD',
            'discount_for' => 'print',
            'valid_from' => date('Y-m-d H:i:s', strtotime('-1 day')),
            'valid_to' => date('Y-m-d H:i:s', strtotime('+20 day')),
            'is_active' => 1,
            'is_valid' => 1,
        ]);
        self::expectException(NotValidPromocode::class);
        $validator->tryValidate($promo);
    }

    public function testValidateType()
    {
        $validator = new PromocodeUsageValidator();
        $promo = new Promocode();
        $promo->setAttributes([
            'usage_type' => Promocode::USAGE_TYPE_MANY,
            'discount_amount' => 20,
            'discount_currency' => 'USD',
            'discount_for' => 'print',
            'valid_from' => date('Y-m-d H:i:s', strtotime('-1 days')),
            'valid_to' => date('Y-m-d H:i:s', strtotime('+20 days')),
            'is_active' => 1,
            'is_valid' => 1,
        ]);

        self::assertEquals(true, $validator->tryValidate($promo));
    }

    public function testValidateDate()
    {
        $validator = new PromocodeUsageValidator();
        $promo = new Promocode();
        $promo->setAttributes([
            'usage_type' => Promocode::USAGE_TYPE_ONE,
            'discount_amount' => 20,
            'discount_currency' => 'USD',
            'discount_for' => 'print',
            'valid_from' => date('Y-m-d H:i:s', strtotime('-1 days')),
            'valid_to' => date('Y-m-d H:i:s', strtotime('+20 days')),
            'is_active' => 1,
            'is_valid' => 1,
        ]);
        self::assertEquals(true, $validator->tryValidate($promo));
    }

    public function testValidateDateFuture()
    {
        $validator = new PromocodeUsageValidator();
        $promo = new Promocode();
        $promo->setAttributes([
            'usage_type' => Promocode::USAGE_TYPE_ONE,
            'discount_amount' => 20,
            'discount_currency' => 'USD',
            'discount_for' => 'print',
            'valid_from' => date('Y-m-d H:i:s', strtotime('+1 day')),
            'valid_to' => date('Y-m-d H:i:s', strtotime('+30 day')),
            'is_active' => 1,
            'is_valid' => 1,
        ]);
        self::expectException(NotValidPromocode::class);
        $validator->tryValidate($promo);
    }

    public function testValidateDateOld()
    {

        $validator = new PromocodeUsageValidator();
        $promo = new Promocode();
        $promo->setAttributes([
            'usage_type' => Promocode::USAGE_TYPE_ONE,
            'discount_amount' => 20,
            'discount_currency' => 'USD',
            'discount_for' => 'print',
            'valid_from' => date('Y-m-d H:i:s', strtotime('-3 day')),
            'valid_to' => date('Y-m-d H:i:s', strtotime('-1 day')),
            'is_active' => 1,
            'is_valid' => 1,
        ]);
        self::expectException(NotValidPromocode::class);
        $validator->tryValidate($promo);
    }
}