<?php
/**
 * User: nabi
 */

namespace common\modules\promocode\components;

use common\models\PaymentInvoice;
use common\models\Promocode;
use yii\base\UserException;

class PromocodeApplyServiceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function checkPromoValidateByPrices()
    {
        $pas = new PromocodeApplyService();
        $promo = new Promocode();
        $promo->discount_type = Promocode::DISCOUNT_TYPE_PERCENT;
        $promo->discount_amount  = 10;
        $promo->discount_for = Promocode::DISCOUNT_FOR_PRINT;

        $invoice = $this->getPaymentInvoicePrintAccounting();

        verify('valid prices', $invoice->storeOrderAmount->getAmountPrintForPs()->getAmount())->equals(12.15);

        verify('valid promo ok', $pas->validatePromoByPrices($promo, $invoice))->true();

        verify('valid promo not ok print price', $pas->validatePromoByPrices($promo, $this->getPaymentInvoicePreorderAccounting()))->false();

        $promo->discount_for = Promocode::DISCOUNT_FOR_MODEL;
        verify('valid promo not ok model price', $pas->validatePromoByPrices($promo, $invoice))->false();
    }

    /**
     * @test
     */
    public function checkPromoValidateOrderPrice()
    {
        $pas = new PromocodeApplyService();
        $promo = new Promocode();
        $promo->discount_type = Promocode::DISCOUNT_TYPE_PERCENT;
        $promo->discount_amount  = 10;
        $promo->discount_for = Promocode::DISCOUNT_FOR_PRINT;
        $promo->order_total_from = 5;

        verify('valid prices', $pas->validateByTotalPrice($promo, $this->getPaymentInvoicePrintAccounting()))->true();

        $promo->discount_for = Promocode::DISCOUNT_FOR_ALL;
        $promo->order_total_from = 4;

        verify('valid prices', $pas->validateByTotalPrice($promo, $this->getPaymentInvoicePrintAccounting()))->true();

        $promo->discount_for = Promocode::DISCOUNT_FOR_ALL;
        $promo->order_total_from = 0;

        verify('valid prices', $pas->validateByTotalPrice($promo, $this->getPaymentInvoicePrintAccounting()))->true();
    }

    /**
     * @test
     * @expectedException \yii\base\UserException
     */
    public function checkPromoValidateOrderPriceToFail()
    {
        $pas = new PromocodeApplyService();
        $promo = new Promocode();
        $promo->discount_type = Promocode::DISCOUNT_TYPE_PERCENT;
        $promo->discount_amount  = 10;
        $promo->discount_for = Promocode::DISCOUNT_FOR_PRINT;
        $promo->order_total_from = 25;

        $pas->validateByTotalPrice($promo, $this->getPaymentInvoicePreorderAccounting());
    }

    /**
     * @return PaymentInvoice
     */
    protected function getPaymentInvoicePrintAccounting(): PaymentInvoice
    {
        $invoice = new PaymentInvoice();

        $invoice->total_amount = 18.23;
        $invoice->currency = 'USD';

        $invoice->accounting = [
            'model' => [
                'type' => 'model',
                'price' => 0,
                'currency' => 'USD'
            ],
            'ps_fee' => [
                'type' => 'ps_fee',
                'price' => 3.04,
                'currency' => 'USD'
            ],
            'ps_print' => [
                'type' => 'ps_print',
                'price' => 15.19,
                'currency' => 'USD'
            ],
            'shipping' => [
                'type' => 'shipping',
                'price' => 4.8,
                'currency' => 'USD'
            ],
        ];

        return $invoice;
    }

    /**
     * @return PaymentInvoice
     */
    protected function getPaymentInvoicePreorderAccounting(): PaymentInvoice
    {
        $invoice = new PaymentInvoice();

        $invoice->total_amount = 350;
        $invoice->currency = 'USD';

        $invoice->accounting = [
            'invoice_items' => [
                1 => [
                    'fee' => [
                        'type' => 'fee',
                        'price' => 18.13,
                        'currency' => 'USD'
                    ],
                    'manufacturer' => [
                        'type' => 'manufacturer',
                        'price' => 231.86,
                        'currency' => 'USD'
                    ],
                ],
                2 => [
                    'fee' => [
                        'type' => 'fee',
                        'price' => 7.25,
                        'currency' => 'USD'
                    ],
                    'manufacturer' => [
                        'type' => 'manufacturer',
                        'price' => 92.74,
                        'currency' => 'USD'
                    ],
                ]
            ]
        ];

        return $invoice;
    }

}