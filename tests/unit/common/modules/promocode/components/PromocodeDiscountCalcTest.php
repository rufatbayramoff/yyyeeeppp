<?php
namespace common\modules\promocode\components;

use common\components\order\OrderPriceDetails;
use common\components\PaymentExchangeRateFacade;
use common\models\PaymentInvoice;
use common\models\Promocode;
use lib\money\Money;
use test\unit\CodeceptionTestCase;

/**
 * Date: 02.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class PromocodeDiscountCalcTest extends CodeceptionTestCase
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $transaction;

    protected function _before()
    {
        $this->transaction = app('db')->beginTransaction();
    }

    protected function _after()
    {
        $this->transaction->rollBack();
    }

    public function testGetDiscountPercent()
    {
        $money = Money::create(200, 'USD');
        $promo = new Promocode();
        $promo->setAttributes([
            'discount_type' => Promocode::DISCOUNT_TYPE_PERCENT,
            'discount_amount' => 15,
            'discount_currency' => 'USD',
            'discount_for' => 'print'
        ]);
        $discount = PromocodeDiscountCalc::getDiscount($promo, $money);
        self::assertEquals(30, $discount->getAmount());
        self::assertEquals('USD', $discount->getCurrency());
    }

    public function testGetDiscountFixed()
    {
        $money = Money::create(100, 'USD');
        $promo = new Promocode();
        $promo->setAttributes([
            'discount_type' => Promocode::DISCOUNT_TYPE_FIXED,
            'discount_amount' => 20,
            'discount_currency' => 'USD',
            'discount_for' => 'print'
        ]);
        $discount = PromocodeDiscountCalc::getDiscount($promo, $money);
        self::assertEquals(20, $discount->getAmount());
        self::assertEquals('USD', $discount->getCurrency());
    }

    public function testGetDiscountPercentCurrency()
    {
        $money = Money::create(150, 'EUR');
        $promo = new Promocode();
        $promo->setAttributes([
            'discount_type' => Promocode::DISCOUNT_TYPE_PERCENT,
            'discount_amount' => 5,
            'discount_currency' => 'USD',
            'discount_for' => 'print'
        ]);
        $discount = PromocodeDiscountCalc::getDiscount($promo, $money);
        self::assertEquals(7.5, $discount->getAmount());
        self::assertEquals('EUR', $discount->getCurrency());
    }

    public function testGetDiscountFixedCurrency()
    {
        $money = Money::create(120, 'USD');
        $discountAmount = 15;

        $resultDiscount  = PaymentExchangeRateFacade::convert($discountAmount, 'EUR', 'USD');

        $promo = new Promocode();
        $promo->setAttributes([
            'discount_type' => Promocode::DISCOUNT_TYPE_FIXED,
            'discount_amount' => $discountAmount,
            'discount_currency' => 'EUR',
            'discount_for' => 'print'
        ]);
        $discount = PromocodeDiscountCalc::getDiscount($promo, $money);
        self::assertEquals($resultDiscount, $discount->getAmount());
        self::assertEquals('USD', $discount->getCurrency());
    }

    public function testGetDiscountPercentNoOverlimit()
    {
        $money = Money::create(100, 'EUR');

        $promo = $this->getPromoStub([
            'discount_type' => Promocode::DISCOUNT_TYPE_PERCENT,
            'discount_amount' => 120,
            'discount_currency' => 'USD',
            'discount_for' => 'print'
        ]);
        $discount = PromocodeDiscountCalc::getDiscount($promo, $money);
        self::assertEquals(100, $discount->getAmount());
        self::assertEquals('EUR', $discount->getCurrency());
    }

    public function testGetDiscountFixedNoOverlimit()
    {
        $money = Money::create(10, 'USD');
        $promo = $this->getPromoStub([
            'discount_type' => Promocode::DISCOUNT_TYPE_FIXED,
            'discount_amount' => 15,
            'discount_currency' => 'USD',
            'discount_for' => 'print'
        ]);
        $discount = PromocodeDiscountCalc::getDiscount($promo, $money);
        self::assertEquals(10, $discount->getAmount());
    }

    public function testGetDiscountSettings()
    {
        $testPromos = $this->getPromosFixture();
        $amount = PaymentExchangeRateFacade::convert(7, 'EUR', 'USD');
        $settingsAssert = [
            ['discount_for'=> 'print', 'discount_type'=>'fixed', 'discount_amount'=>15],
            ['discount_for'=> 'print', 'discount_type' => 'fixed', 'discount_amount' => $amount],
            ['discount_for'=> 'model', 'discount_type'=>'percent', 'discount_amount'=>10],
            ['discount_for'=> 'all', 'discount_type' => 'fixed', 'discount_amount' => $amount]
        ];

        $settings = PromocodeDiscountCalc::getDiscountSettings($testPromos, 'USD');
        self::assertEquals($settingsAssert, $settings);
    }

    protected function getPromosFixture()
    {
        $promos = [
            $this->getPromoStub([
                'discount_type' => Promocode::DISCOUNT_TYPE_FIXED,
                'discount_amount' => 15,
                'discount_currency' => 'USD',
                'discount_for' => 'print'
            ]),
            $this->getPromoStub([
                'discount_type' => Promocode::DISCOUNT_TYPE_FIXED,
                'discount_amount' => 7,
                'discount_currency' => 'EUR',
                'discount_for' => 'print'
            ]),
            $this->getPromoStub([
                'discount_type' => Promocode::DISCOUNT_TYPE_PERCENT,
                'discount_amount' => 10,
                'discount_currency' => 'USD',
                'discount_for' => 'model'
            ]),
            $this->getPromoStub([
                'discount_type' => Promocode::DISCOUNT_TYPE_FIXED,
                'discount_amount' => 7,
                'discount_currency' => 'EUR',
                'discount_for' => 'all'
            ])
        ];
        return $promos;
    }

    protected function getPromoStub($array)
    {
        $promo = new Promocode();
        $promo->setAttributes($array);
        return $promo;
    }
}