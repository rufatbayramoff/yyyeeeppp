<?php
/**
 * User: nabi
 */

namespace unit\common\modules\onshape;


class OnshapeFixtures
{

    public static function getElements()
    {
        $str = '[
    {
        "elementType": "PARTSTUDIO",
        "thumbnailInfo": null,
        "type": "Part Studio",
        "microversionId": "0da2b66537c728435f556ac3",
        "thumbnails": null,
        "filename": null,
        "id": "72c70edb08d20c33cbbee850",
        "dataType": "onshape/partstudio",
        "foreignDataId": null,
        "lengthUnits": "inch",
        "angleUnits": "degree",
        "massUnits": "pound",
        "name": "Part Studio 1"
    },
    {
        "elementType": "APPLICATION",
        "thumbnailInfo": null,
        "type": "Application",
        "microversionId": "1e7438136ea03c7198c76ea4",
        "thumbnails": null,
        "filename": null,
        "id": "b3e5733d7b9f2638ce88f206",
        "dataType": "onshape-app/com.treatstock.ts-test1",
        "foreignDataId": null,
        "lengthUnits": null,
        "angleUnits": null,
        "massUnits": null,
        "name": "TS Test1"
    },
    {
        "elementType": "APPLICATION",
        "thumbnailInfo": null,
        "type": "Application",
        "microversionId": "a437930f5b08338bf21ecc55",
        "thumbnails": null,
        "filename": null,
        "id": "19db849108c8c7d7cfb1c57b",
        "dataType": "onshape-app/com.onshape.api-explorer",
        "foreignDataId": null,
        "lengthUnits": null,
        "angleUnits": null,
        "massUnits": null,
        "name": "API Explorer"
    },
    {
        "elementType": "ASSEMBLY",
        "thumbnailInfo": null,
        "type": "Assembly",
        "microversionId": "e6e4b0be78a42614c0e155d0",
        "thumbnails": null,
        "filename": null,
        "id": "e416ce9283ecb5d280a935dd",
        "dataType": "onshape/assembly",
        "foreignDataId": null,
        "lengthUnits": "inch",
        "angleUnits": "degree",
        "massUnits": "pound",
        "name": "Assembly 1"
    }
]';
        return $str;
    }
    public static function getMeta()
    {
        $str = '[
    {
        "description": null,
        "state": "IN_PROGRESS",
        "name": "Part 1 - Brick",
        "partQuery": "query=makeQuery(id+\"FXCVDkug34adGxU_0.opExtrude\",\"SWEPT_BODY\",EntityType.BODY,{\"disambiguationData\":[originalSetDisambiguation([sketchEntityQuery(id+\"FveQW2xEDsL2pLU_0.wireOp\",EntityType.EDGE,\"37KSZrUa-WYhR-Pkyw-01so-sNDUhvEPpru1.bottom\"),sketchEntityQuery(id+\"FveQW2xEDsL2pLU_0.wireOp\",EntityType.EDGE,\"37KSZrUa-WYhR-Pkyw-01so-sNDUhvEPpru1.top\"),sketchEntityQuery(id+\"FveQW2xEDsL2pLU_0.wireOp\",EntityType.EDGE,\"37KSZrUa-WYhR-Pkyw-01so-sNDUhvEPpru1.left\"),sketchEntityQuery(id+\"FveQW2xEDsL2pLU_0.wireOp\",EntityType.EDGE,\"37KSZrUa-WYhR-Pkyw-01so-sNDUhvEPpru1.right\")])]});",
        "partId": "JHD",
        "microversionId": "5459f4b0762f7896a1c8623a",
        "revision": null,
        "elementId": "72c70edb08d20c33cbbee850",
        "partNumber": null,
        "bodyType": "solid",
        "isMesh": false,
        "isFlattenedBody": false,
        "configurationId": "",
        "appearance": {
            "color": {
                "red": 154,
                "blue": 175,
                "green": 165
            },
            "opacity": 255
        },
        "isHidden": false,
        "ordinal": 0,
        "propertySourceTypes": {
            "57f3fb8efa3416c06701d60d": 3
        }
    },
    {
        "description": null,
        "state": "IN_PROGRESS",
        "name": "Part 2 - Tube",
        "partQuery": "query=makeQuery(id+\"FL3Qhm7UUfhwwd0_1.opExtrude\",\"SWEPT_BODY\",EntityType.BODY,{\"disambiguationData\":[originalSetDisambiguation([sketchEntityQuery(id+\"FckcvwIa0GmIwVL_1.wireOp\",EntityType.EDGE,\"VxW9j0Ky-5EWs-Duoa-SC4a-du1WVhlsAt7q\")])]});",
        "partId": "JLD",
        "microversionId": "5459f4b0762f7896a1c8623a",
        "revision": null,
        "elementId": "72c70edb08d20c33cbbee850",
        "partNumber": null,
        "bodyType": "solid",
        "isMesh": false,
        "isFlattenedBody": false,
        "configurationId": "",
        "appearance": {
            "color": {
                "red": 144,
                "blue": 241,
                "green": 207
            },
            "opacity": 255
        },
        "isHidden": false,
        "ordinal": 1,
        "propertySourceTypes": {
            "57f3fb8efa3416c06701d60d": 3
        }
    },
    {
        "description": null,
        "state": "IN_PROGRESS",
        "name": "Curve 1",
        "partQuery": "query=dummyQuery(id+\"FB9wMzKRdSCHANt_2.opHelix\",EntityType.BODY);",
        "partId": "JMD",
        "microversionId": "5459f4b0762f7896a1c8623a",
        "revision": null,
        "elementId": "72c70edb08d20c33cbbee850",
        "partNumber": null,
        "bodyType": "wire",
        "isMesh": false,
        "isFlattenedBody": false,
        "configurationId": "",
        "appearance": {
            "color": {
                "red": 154,
                "blue": 175,
                "green": 165
            },
            "opacity": 255
        },
        "isHidden": false,
        "ordinal": 2,
        "propertySourceTypes": {
            "57f3fb8efa3416c06701d60d": 0
        }
    }
]';
        return $str;
    }
}