<?php
/**
 * User: nabi
 */

namespace common\modules\onshape\components;


use Codeception\Util\Stub;
use yii\authclient\SessionStateStorage;

class OnshapeOAuth2Test extends \PHPUnit_Framework_TestCase
{

    public function testGetReturnUrl()
    {
        $auth2 = \Yii::createObject( ['class'=>OnshapeOAuth2::class]);
        verify('test', $auth2->getReturnUrl())->equals(param('server') . '/onshape/app/auth');
    }

}
