<?php
/**
 * User: nabi
 */

namespace lib\payment;


use common\modules\payment\fee\FeeHelper;
use lib\money\Money;

class FeeHelperTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function checkPrintFeeCalculation()
    {
        $feeHelper = new FeeHelper();
        $feeHelper->psFeePercent = 0.15;
        $feeHelper->psMinFee = 1.99;
        $printFee = $feeHelper->getTsCommonFee(Money::usd(155.55)); // 15%
        verify('10% percent from print price', $printFee)->equals(23.33); // with roundUp

        $printFee = $feeHelper->getTsCommonFee(Money::usd(2));
        verify('min fee from print price', $printFee)->equals(1.99);
    }

    /**
     * @test
     */
    public function getOurFeeFromPrintPrice()
    {
        $feeHelper = new FeeHelper();
        $feeHelper->psFeePercent = 0.15;
        $feeHelper->psMinFee = 1.99;

        $printFee = $feeHelper->getPrintFeeFromPrice(Money::usd(172.5)); // 15% - we should get 22.5
        verify('get fee from print price', $printFee)->equals(22.5);

        $printFee = $feeHelper->getPrintFeeFromPrice(Money::usd(5));
        verify('get min.fee from print price', $printFee)->equals(1.99);
    }

    /**
     * @test
     */
    public function checkEmptyFeeFromPs()
    {
        $feeHelper = new FeeHelper();
        $feeHelper->psFeePercent = 0;
        $feeHelper->psMinFee = 1.99;

        $printFee = $feeHelper->getPrintFeeFromPrice(Money::usd(172.5)); // 15% - we should get 22.5
        verify('get fee from print price', $printFee)->equals(0);

        $printFee = $feeHelper->getPrintFeeFromPrice(Money::usd(2));
        verify('get min.fee from print price', $printFee)->equals(0);

        $feeHelper->psFeePercent = 0;
        $feeHelper->psMinFee = 0;
        $printFee = $feeHelper->getPrintFeeFromPrice(Money::usd(2));
        verify('get min.fee from print price', $printFee)->equals(0);
    }
}
