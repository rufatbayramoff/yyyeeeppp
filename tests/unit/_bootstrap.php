<?php
// Here you can initialize variables that will be available to your tests
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');

// fcgi doesn't have STDIN and STDOUT defined by default
defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));
defined('STDOUT') or define('STDOUT', fopen('php://stdout', 'w'));

$dir = __DIR__ . '/../../';
require($dir . '/vendor/autoload.php');
require($dir . '/vendor/yiisoft/yii2/Yii.php');
require($dir . '/common/config/bootstrap.php');
require($dir . '/console/config/bootstrap.php');
require_once($dir . '/vendor/codeception/codeception/autoload.php');
require(__DIR__ . '/CodeceptionTestCase.php');

$config = yii\helpers\ArrayHelper::merge(
    require($dir . '/common/config/main.php'),
    require($dir . '/common/config/main-local.php'),
    require($dir . '/console/config/main.php'),
    require($dir . '/console/config/main-local.php')
);

$config['components']['session'] = [
    'class' => \yii\web\Session::class
];
$config['components']['user'] = \common\components\ArrayHelper::merge([
    'identityClass' => 'common\models\User',
    'enableAutoLogin' => false,
    'enableSession' => false,
    'loginUrl' => null,
], $config['components']['user']);

$config['components']['mailer'] = [
    'class'            => 'yii\swiftmailer\Mailer',
    'viewPath'         => '@common/mail',
    'useFileTransport' => false,
    'transport'        => [
        'class' => 'Swift_SmtpTransport',
        'host'  => '127.0.0.1',
        'port'  => '1025',  // mailcatcher
    ],
];
$application = new yii\console\Application($config);
