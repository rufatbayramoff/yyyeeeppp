<?php
use common\models\model3d\ScalePartPropertiesOperation;
use common\models\Model3dPartProperties;

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.05.17
 * Time: 10:58
 */

class ScalePartPropertiesOperationTest extends \test\unit\CodeceptionTestCase
{
    public function testScale2()
    {
        /** @var ScalePartPropertiesOperation $scalePartPropertiesOperation */
        $scalePartPropertiesOperation = new ScalePartPropertiesOperation(2);
        $model3dPropertiesStub = new Model3dPartProperties();
        $model3dPropertiesStub->volume = 152;
        $model3dPropertiesStub->area = 131;
        $scalePartPropertiesOperation->setModel3dPartProperties($model3dPropertiesStub);
        /** @var Model3dPartProperties $resultScaledProperties */
        $scalePartPropertiesOperation->execute();
        $resultScaledProperties = $scalePartPropertiesOperation->getResult();
        self::assertEquals($resultScaledProperties->volume, 1216, 'Incorrect volume scale 2.');
        self::assertEquals($resultScaledProperties->area, 524, 'Incorrect area scale 2.');
    }

    public function testScale0_5()
    {
        /** @var ScalePartPropertiesOperation $scalePartPropertiesOperation */
        $scalePartPropertiesOperation = new ScalePartPropertiesOperation(0.5);
        $model3dPropertiesStub = new Model3dPartProperties();
        $model3dPropertiesStub->volume = 152;
        $model3dPropertiesStub->area = 131;
        $scalePartPropertiesOperation->setModel3dPartProperties($model3dPropertiesStub);
        /** @var Model3dPartProperties $resultScaledProperties */
        $scalePartPropertiesOperation->execute();
        $resultScaledProperties = $scalePartPropertiesOperation->getResult();
        self::assertEquals($resultScaledProperties->volume, 19, 'Incorrect volume scale 0.5.');
        self::assertEquals($resultScaledProperties->area, 32.75, 'Incorrect area scale 0.5.');
    }
}