<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.10.16
 * Time: 15:16
 */

namespace test\unit;

use Codeception\Configuration;
use Codeception\Scenario;

class CodeceptionTestCase extends \Codeception\TestCase\Test
{
    protected function setUp()
    {
        if ($this->getMetadata()->isBlocked()) {
            if ($this->getMetadata()->getSkip() !== null) {
                $this->markTestSkipped($this->getMetadata()->getSkip());
            }
            if ($this->getMetadata()->getIncomplete() !== null) {
                $this->markTestIncomplete($this->getMetadata()->getIncomplete());
            }
            return;
        }
        $scenario = new Scenario($this);
        $actorClass = $this->getMetadata()->getCurrent('actor');
        if ($actorClass) {
            $I = new $actorClass($scenario);
            $property = lcfirst(Configuration::config()['actor']);
            $this->$property = $I;
        }
        try {
            $this->getMetadata()->getService('di')->injectDependencies($this); // injecting dependencies
        } catch (\Exception $e) {

        }
        $this->_before();
    }

    public function getCurrentDir()
    {
        return __DIR__;
    }

    public function readSourceData($sourceName)
    {
        $filePath = $this->getCurrentDir() . '/sources/' . $sourceName . '.json';
        $content = file_get_contents($filePath);
        return json_decode($content, true);
    }

    public function readTemplate($templateName)
    {
        $filePath = $this->getCurrentDir() . '/resultTemplates/' . $templateName . '.json';
        $content = file_get_contents($filePath);
        return json_decode($content, true);
    }

    public function assertJsonEqual($need, $real)
    {
        self::assertEquals($need, $real, 'Json is not equals.');
    }

    public function getBlockCronFlagPath()
    {
        return \Yii::getAlias('@console/runtime/blockCron.flag');
    }

    public function touchBlockCronFlagPath()
    {
        file_put_contents($this->getBlockCronFlagPath(), time());
    }

    public function unlinkBlockCronFlagPath()
    {
        unlink($this->getBlockCronFlagPath());
    }

    /**
     * to test private methods
     *
     * @param $obj
     * @param $methodName
     * @param array $args
     * @return mixed
     */
    protected static function callPrivateMethod($obj, $methodName, array $args)
    {
        $class = new ReflectionClass($obj);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($obj, $args);
    }
}