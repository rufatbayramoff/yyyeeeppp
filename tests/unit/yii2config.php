<?php

$dir = __DIR__ . '/../../';

require_once($dir . '/vendor/autoload.php');
require_once($dir . '/vendor/yiisoft/yii2/Yii.php');
require_once($dir . '/common/config/bootstrap.php');
#require($dir . '/console/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require($dir . '/common/config/main.php'),
    require($dir . '/common/config/main-local.php'),
    require($dir . '/console/config/main.php'),
    require($dir . '/console/config/main-local.php')
);
$config['components']['mailer'] = [
    'class' => 'yii\swiftmailer\Mailer',
];
// to remove error - User::identityClass must be set.
$config['components']['user'] = [
    'identityClass' => 'common\models\User',
    'loginUrl' => null,
];
$config['components']['request'] = [
    'class' => \yii\web\Request::class,
    'cookieValidationKey' => 'testRequest'
];
return $config;