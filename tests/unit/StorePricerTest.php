<?php


use common\models\PaymentExchangeRate;

class StorePricerTest extends \test\unit\CodeceptionTestCase
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $transaction;
    
    /**
     *
     * @var common\models\PaymentExchangeRate 
     */
    protected function _before()
    {        
        $this->transaction = app('db')->beginTransaction();
        $this->rate = PaymentExchangeRate::find()->latest();
    }

    protected function _after()
    {
        if($this->transaction){
            $this->transaction->rollBack();
        }
    }

    public function testPriceConverter()
    {
        $rate = $this->rate;
        $base = $rate->base;
        $testPrice = 133.5123;
        $convertedPrice = $rate->convert($testPrice, $base, 'RUB');
        
        // ASSERT 1
        $testRubPrice = $rate->getCurrencyRate('RUB')*$testPrice;
        $this->assertEquals(
            true, $testRubPrice == $convertedPrice, 
            sprintf('price converted %s result %s', $testRubPrice, $convertedPrice)
        );
        
        // ASSERT 2
        $price2 = $rate->convert($convertedPrice, 'RUB', $base);
        $priceBack = $rate->convert($price2, $base, 'RUB');
        $this->assertEquals(
            true, $priceBack == $convertedPrice, 
            sprintf('price converted back %s, %s', $convertedPrice, $price2)
        );
        // ASSERT 3
        
        // convert from two diff currencies to base
        $mixConvertRubTest = $rate->convert($testPrice, 'EUR', 'RUB');
        // check in two steps
        $mixConvertUsd = $rate->convert($testPrice, 'EUR', $base);
        $mixConvertRub = $rate->convert($mixConvertUsd, $base, 'RUB');
        $this->assertEquals(
            true, $mixConvertRub == $mixConvertRubTest, 
            sprintf('price mix converted need %s  was %s', $mixConvertRub, $mixConvertRubTest)
        );
        
        // ASSERT 4 - 
        $convertedPrice2 = $rate->convert($testPrice, $base, $base);
        $this->assertEquals(
            true, $testPrice == $convertedPrice2, 
            sprintf('price the same %s  was %s', $convertedPrice2, $testPrice)
        );
    }

}
