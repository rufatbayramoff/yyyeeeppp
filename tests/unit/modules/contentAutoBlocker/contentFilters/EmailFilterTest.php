<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.10.16
 * Time: 9:19
 */
namespace tests\unit\modules\contentAutoBlocker\contentFilters;

use common\modules\contentAutoBlocker\components\contentFilters\EmailFilter;

class EmailFilterTest extends BaseFilterTest
{
    public function testEmailFilter()
    {
        $this->filter = new EmailFilter();
        $this->phraseCheck('SomeFilterText', false);
        $this->phraseCheck('Simple email: ivan@mail.ru', true);
        $this->phraseCheck('Long email: ivanIvanov+treatstock@gmail.com', true);
    }
}