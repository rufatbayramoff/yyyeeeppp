<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 15:20
 */

namespace tests\unit\modules\contentAutoBlocker\contentFilters;

use common\modules\contentAutoBlocker\components\contentFilters\SkypeFilter;

class SkypeFilterTest extends BaseFilterTest
{
    public function testSkypeFilter()
    {
        $this->filter = new SkypeFilter();
        $this->phraseCheck('SomeFilterText', false);
        $this->phraseCheck('My skype: analitic1983', true);
        $this->phraseCheck('My Skype: analitic1983', true);
        $this->phraseCheck('My skype analitic1983', true);
        $this->phraseCheck('My noskype analitic1983', false);
        return false;
    }
}