<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.10.16
 * Time: 9:19
 */
namespace tests\unit\modules\contentAutoBlocker\contentFilters;

use common\modules\contentAutoBlocker\components\contentFilters\PhoneFilter;

class PhoneFilterTest extends BaseFilterTest
{
    public function testPhoneFilter()
    {
        $this->filter = new PhoneFilter();
        $this->phraseCheck('SomeFilterText', false);

        $this->phraseCheck('Ru phone: +79177021010', true);
        $this->phraseCheck('Ru phone: 89177021010', true);
        $this->phraseCheck('Ru phone: 8-917-702-1010', true);
        $this->phraseCheck('Ru phone: 8917-7021010', true);
        $this->phraseCheck('Us phone: 8917-7021010', true);
        // http://stdcxx.apache.org/doc/stdlibug/26-1.html Phone Examples from apache
        http://stdcxx.apache.org/doc/stdlibug/26-1.html
        $this->phraseCheck('Us local: 754-3010', true);
        $this->phraseCheck('Us Domestic: (541) 754-3010', true);
        $this->phraseCheck('Us International: +1-541-754-3010', true);
        $this->phraseCheck('Dialed in the US: 1-541-754-3010', true);
        $this->phraseCheck('Dialed from Germany: 001-541-754-3010', true);
        $this->phraseCheck('Dialed from France: 191 541 754 3010', true);
        // Now consider a German phone number. Although a German phone number consists of an area code and an extension like a US number, the format is different. Here is the same German phone number in a variety of formats:
        $this->phraseCheck('Local: 636-48018', true);
        $this->phraseCheck('Domestic: (089) / 636-48018', true);
        $this->phraseCheck('International: +49-89-636-48018', true);
        $this->phraseCheck('Dialed from France: 19-49-89-636-48018', true);

        // Not phone test
        $this->phraseCheck('No phone: 194989', false);
        $this->phraseCheck('No phone: 1924x989', false);

        $this->phraseCheck('Is min len track: 1234567890123', false);
        $this->phraseCheck('Track easypost: 9400110897700063338296', false);
        $this->phraseCheck('Track USPS: 9475 7036 9930 0198 6514 20', false);
        $this->phraseCheck('Track easypost: LN800118925US', false);
        $this->phraseCheck('Track Australia Post: R235387015084735', false);
        $this->phraseCheck('Track Hermes Post: 01334150839074', false);
        //$this->phraseCheck('Track DHL: 129936112391', false);
        $this->phraseCheck('Track Colissimo: 8J00049762471', false);
    }
}