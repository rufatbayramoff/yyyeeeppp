<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.10.16
 * Time: 9:19
 */

namespace tests\unit\modules\api;


use common\models\ApiExternalSystem;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use lib\money\Money;
use test\unit\CodeceptionTestCase;
use treatstock\api\v2\models\requests\CreatePrintablePackRequest;
use treatstock\api\v2\models\requests\GetPrintablePackStatusRequest;
use treatstock\api\v2\models\responses\CreatePrintablePackResponse;
use treatstock\api\v2\TreatstockApiService;

class ApiTest extends CodeceptionTestCase
{
    /**
     * @var CreatePrintablePackResponse
     */
    protected $createResponse;

    /** @var TreatstockApiService */
    protected $apiService;

    public function setUp()
    {
        $privateKey = '15ce76a20be443acfeae731cbc27dc';
        $this->apiService = new TreatstockApiService($privateKey, 'http://ts.h.tsdev.work');
        $this->addMoneyToTestUser($privateKey);
        $this->createPrintablePack();
    }

    public function addMoneyToTestUser($privateKey)
    {
        $apiExtUser = ApiExternalSystem::tryFind(['private_key' => $privateKey]);
        /** @var PaymentService $paymentService */
        $paymentService = \Yii::createObject(PaymentService::class);
        $paymentAccountService = \Yii::createObject(PaymentAccountService::class);
        $correctionAccount = $paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_CORRECTION);
        $testAccountMain = $paymentAccountService->getUserPaymentAccount($apiExtUser->bindedUser);
        $testAccountAuthorize = $paymentAccountService->getUserPaymentAccount($apiExtUser->bindedUser, PaymentAccount::ACCOUNT_TYPE_AUTHORIZE);

        $paymentDetailExists = PaymentDetail::tryFind(['payment_account_id' => $testAccountAuthorize->id]);
        $payment = $paymentDetailExists->payment;

        $paymentService->transferMoney(
            $payment,
            $correctionAccount,
            $testAccountMain,
            Money::usd(10000),
            PaymentDetail::TYPE_CORRECTION,
            'Add money for unit test: ApiTest.php'
        );
    }

    /**
     * @throws \treatstock\api\v2\exceptions\InvalidAnswerException
     */
    public function createPrintablePack()
    {
        // Try create printable pack
        $createRequest = new CreatePrintablePackRequest();
        $createRequest->filePaths[] = __DIR__ . '/test.stl';
        $createRequest->locationCountryIso = 'US'; // Optional params: to get printable pack price info

        // echo"\nSend create printable pack request...";
        $createResponse = $this->apiService->createPrintablePack($createRequest);
        // echo"\nCreate printable pack response:\n";
        // echo\treatstock\api\v2\helpers\FormattedJson::encode($createResponse);

        $this->assertAttributeGreaterThan(1, 'id', $createResponse, 'Id shoud be more then 1');
        $this->createResponse = $createResponse;
    }

    public function testGetPrintablePackStatus()
    {
        // Try get status printable pack
        // echo"\nGet printable pack status...";
        $testStatusRequest = new GetPrintablePackStatusRequest();
        $testStatusRequest->printablePackId = $this->createResponse->id;
        $testStatusResponse = $this->apiService->getPrintablePackStatus($testStatusRequest);
        // echo"\nGet printable pack status response:\n";
        // echo\treatstock\api\v2\helpers\FormattedJson::encode($testStatusResponse);

        $this->assertAttributeGreaterThan(1, 'id', $testStatusResponse, 'Id shoud be more then 1');
    }

    public function testChangePrintablePack()
    {
        // Change printable pack Qty
        $changePackRequest = new \treatstock\api\v2\models\requests\ChangePrintablePackRequest();
        $firstPart = reset($this->createResponse->parts);
        $changePackRequest->printablePackId = $this->createResponse->id;
        $changePackRequest->qty =
            [$firstPart->uid => 5];
        // echo"\nChange printable pack qty request.\n";
        $changePrintablePackResponse = $this->apiService->changePrintablePack($changePackRequest); // If request failed, it will have exception
        // echo"\nChanged.\n";
        sleep(15);
        $changePackRequest = new \treatstock\api\v2\models\requests\ChangePrintablePackRequest();
        $changePackRequest->printablePackId = $this->createResponse->id;
        $changePackRequest->scaleUnit = \treatstock\api\v2\models\requests\ChangePrintablePackRequest::SCALE_UNIT_IN;
        // echo"\nChange printable pack scale request.\n";
        $changePrintablePackResponse = $this->apiService->changePrintablePack($changePackRequest); // If request failed, it will have exception
        // echo"\nChanged.\n";

        // If you want, you can check it by get current status
        // echo"\nGet printable pack status...";
        sleep(15);
        $testStatusRequest = new \treatstock\api\v2\models\requests\GetPrintablePackStatusRequest();
        $testStatusRequest->printablePackId = $this->createResponse->id;
        $testStatusResponse = $this->apiService->getPrintablePackStatus($testStatusRequest);
        // echo"\nGet printable pack status response:\n";
        // echo\treatstock\api\v2\helpers\FormattedJson::encode($testStatusResponse);

        // Checking result response
        sleep(15);
        $this->assertEquals($this->createResponse->id, $testStatusResponse->id, 'Is not same printable pack');
        $firstPart = reset($testStatusResponse->parts);
        $this->assertEquals($firstPart->qty, 5, 'Invalid result qty');
        $this->assertEquals($firstPart->originalSize->length, 32.567, 'Invalid original size');
        $this->assertEquals($firstPart->size->length, 827.202, 'Invalid result scale size');
    }

    public function testPlaceOrder()
    {
        sleep(5);
//        // Try get printable pack prices
//        // echo"\nGet printable pack prices...";
//        $pricesRequest = new \treatstock\api\v2\models\requests\GetPrintablePackPricesRequest();
//        $pricesRequest->printablePackId = $this->createResponse->id;
//        $pricesResponse = $this->apiService->getPrintablePackPrices($pricesRequest);
//        //echo "\nGet printable pack prices response:\n";
//        // echo\treatstock\api\v2\helpers\FormattedJson::encode($pricesResponse);
//
//        // Checking result response
//        $this->assertNotEmpty($pricesResponse->pricesInfo, 'No prices');
//        $firstPrice = reset($pricesResponse->pricesInfo);
//        $this->assertGreaterThanOrEqual(1, $firstPrice->price, 'Invalid price');
//        $this->assertGreaterThanOrEqual(1, $firstPrice->providerId, 'Invalid provided id');
//        $this->assertNotEmpty($firstPrice->url, 'Empty url not allowed');
//
//        $placeOrderRequest = new \treatstock\api\v2\models\requests\PlaceOrderRequest();
//        $placeOrderRequest->printablePackId = $this->createResponse->id;
//        $placeOrderRequest->modelTextureInfo = new \treatstock\api\v2\models\ModelTextureInfo();
//        $placeOrderRequest->modelTextureInfo->modelTexture = new \treatstock\api\v2\models\Texture();
//        $placeOrderRequest->modelTextureInfo->modelTexture->color = $firstPrice->color;
//        $placeOrderRequest->modelTextureInfo->modelTexture->materialGroup = $firstPrice->materialGroup;
//        $placeOrderRequest->comment = 'Please print it as fast as possible.';
//        $placeOrderRequest->location = new \treatstock\api\v2\models\ClientLocation();
//        $placeOrderRequest->location->company = 'Big company';
//        $placeOrderRequest->location->email = 'test@company.com';
//        $placeOrderRequest->shippingAddress = new \treatstock\api\v2\models\ShippingAddress();
//        $placeOrderRequest->shippingAddress->country = 'US';
//        $placeOrderRequest->shippingAddress->state = 'DC';
//        $placeOrderRequest->shippingAddress->street = '727 C ST SE';
//        $placeOrderRequest->shippingAddress->city = 'WASHINGTON';
//        $placeOrderRequest->shippingAddress->firstName = 'Bill';
//        $placeOrderRequest->shippingAddress->lastName = 'Jobs';
//        $placeOrderRequest->shippingAddress->zip = '20003';
//        $placeOrderRequest->providerId = $firstPrice->providerId;
//        $placeOrderResponse = $this->apiService->placeOrder($placeOrderRequest);
//
//        $this->assertGreaterThanOrEqual(1, $placeOrderResponse->orderId, 'Invalid orderId');
//        $this->assertGreaterThanOrEqual(1, $placeOrderResponse->total, 'Invalid Total price');
//        $this->assertNotEmpty($placeOrderResponse->url, 'Invalid order url');
//        //echo \treatstock\api\v2\helpers\FormattedJson::encode($placeOrderResponse);
//        $this->payOrderSubTest($placeOrderResponse);
    }

    public function payOrderSubTest($placeOrderResponse)
    {
        $payOrderRequest = new \treatstock\api\v2\models\requests\PayOrderRequest();
        $payOrderRequest->orderId = $placeOrderResponse->orderId;
        $payOrderRequest->total = $placeOrderResponse->total;
        $payOrderResponse = $this->apiService->payOrder($payOrderRequest);
        //echo "\nPay order response:\n";
        $this->assertGreaterThanOrEqual(1, $payOrderResponse->orderId, 'Invalid pay orderId');
        //echo \treatstock\api\v2\helpers\FormattedJson::encode($payOrderResponse);

    }

    public function testGetColors()
    {
        // echo"\nGet material group colors...";
        $materialGroupColors = $this->apiService->getMaterialGroupColors();
        // echo"\nCreate printable pack responce:\n";
        // echo\treatstock\api\v2\helpers\FormattedJson::encode($materialGroupColors);

        // Checking result response
        $firstGroup = reset($materialGroupColors->materialGroupColors);
        $this->assertNotEmpty($firstGroup->code);
        $this->assertNotEmpty($firstGroup->colors);
    }
}