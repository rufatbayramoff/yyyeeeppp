<?php
return [
    'full_name' => $faker->firstName . ' ' .$faker->lastName,
    'address' => $faker->city . ', '. $faker->country,
    'current_lang' => 'en-US',
    'trust_level' => 'untrusted',
    'current_currency_iso' => 'USD',
    'current_metrics' => 'in',
    'updated_at' => time(),
    'info_about' => $faker->text(350),
    'info_skills' => $faker->text(250),
];
