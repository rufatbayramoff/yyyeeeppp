<?php
return [
    'username' => $faker->userName,
    'password_hash' => Yii::$app->getSecurity()->generateRandomString(),
    'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
    'email' => $faker->email,
    'status' => array_rand([1,10]),
    'created_at' => time()
];