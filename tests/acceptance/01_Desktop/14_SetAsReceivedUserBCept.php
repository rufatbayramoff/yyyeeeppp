<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Set as Received for order #100');
$I->expect("Local user already created in test 02 SignUp"); 

$I->amOnSubdomain('ts'); // TODO backend-test

$I->setBraintreeThreeDSecureMode(false);

$I->comment("because of active operations available only for registered and confirmed user, lets sign up"); 
$I->amOnPage("/");
$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');
$I->fillField('#loginform-email','test2@test.work');
$I->fillField('#loginform-password','testtest');
$I->click('Sign in', '.modal-body');
$I->wait(3);
$I->see('test2');

$I->wait(1);

$I->click('.header-bar__username');
$I->wait(1);
$I->click('My Purchases','ul.dropdown-menu-right');
$I->wait(3);

$I->click('Delivered', 'div.side-nav-mobile');

$I->waitForElement('div.order-block', 5);

$I->see('Payment status: Paid');
$I->see('Order status: Delivered');

$I->click('Order details');
$I->waitForElement('div.order__btns', 5);

$I->click('Set as Received');
$I->wait(3);
$I->click('button[ng-click="yesClick()"]');

$I->waitForText('Review', 5);

$I->fillField('textarea[ng-model="form.comment"]','Good');
$I->wait(1);
$I->click('div[class="rating-container tsi"]', 'div[class="star-rating rating-sm rating-active"]');
$I->wait(1);
$I->click('//html/body/div[8]/div/div/div[2]/form/div[2]/div/div/div');
$I->wait(1);
// $I->click('input[star-input="form.rating_communication"]');
$I->click('//html/body/div[8]/div/div/div[2]/form/div[3]/div/div/div');
$I->wait(1);
$I->click('Submit');
$I->wait(1);
// $I->see('Share review');
// $I->wait(1);
// $I->click('button.close');

$I->see('Payment status: Paid');
$I->see('Order status: Received');

$I->wait(1);

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
$I->see('Sign in');
