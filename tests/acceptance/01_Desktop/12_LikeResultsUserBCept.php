<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Like results for order #100');
$I->expect("Local user already created in test 02 SignUp"); 

$I->amOnSubdomain('ts'); // TODO backend-test

$I->setBraintreeThreeDSecureMode(false);

$I->comment("because of active operations available only for registered and confirmed user, lets sign up"); 
$I->amOnPage("/");
$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');
$I->fillField('#loginform-email','test2@test.work');
$I->fillField('#loginform-password','testtest');
$I->click('Sign in', '.modal-body');
$I->wait(3);
$I->see('test2');

$I->wait(1);

$I->click('.header-bar__username');
$I->wait(1);
$I->click('My Purchases','ul.dropdown-menu-right');
$I->wait(3);

$I->click('a[href="/workbench/order/view/100"]');

$I->waitForElement('div.order-block');

$I->click('Like results');
$I->wait(3);
$I->see('Liked results');

$I->see('Payment status: Paid');
$I->see('Order status: Ready for dispatch');

$I->wait(1);

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
$I->see('Sign in');
