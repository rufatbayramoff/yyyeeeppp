<?php 

$I = new AcceptanceTester($scenario);
$I->am('Anonymous user'); 
$I->wantTo('Create a new local account 2 for cross-testing');
$I->lookForwardTo('have user 2 created');

$I->amOnSubdomain('ts'); // TODO backend-test

$I->resetEmails();

$I->amOnPage("/");

$I->dontSee('By using this Service, you agree');
$I->click('Sign in');
$I->wait(1);
$I->see('By using this Service, you agree');
$I->wait(1);
$I->click('Sign up for free');
$I->wait(1);
$I->see('By using this Service, you agree');
$I->wait(1);
$I->fillField('SignupForm[email]','test2@test.work');
$I->fillField('SignupForm[password]','testtest');
$I->wait(1);
$I->click('Sign up', '.modal-body');
$I->waitForElement('.alert-warning', 3);
$I->wait(1);
$I->see('Thank you for signing up!');

$I->seeInLastEmail('Hi test2');
$relativeUrl = $I->grabMatchedFromLastEmail('@http://.*?/(.*)@');
$I->expect("Approve url is: $relativeUrl  "); 

$I->amOnPage($relativeUrl);
$I->see('E-mail confirmed');

