<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Accept and printed order #100 by user Test2');
$I->expect("Order #100 checkout in test 09_UploadAndOrderModelUserBCept"); 

$I->amOnSubdomain('ts'); // TODO backend-test

$I->amOnPage("/");

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','asen.kurin@gmail.com');
$I->fillField('#loginform-password','qwerty');
$I->click('Sign in', '.modal-body');
$I->wait(5);
$I->see('asen.kurin');

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('My Sales');
$I->wait(3);
$I->see('Order #');

$I->fillField('#ordersearch','100');
$I->wait(2);
$I->pressKey('#ordersearch', \Facebook\WebDriver\WebDriverKeys::ENTER);

$I->wait(10);
$I->click('div.service-order-row__num');
$I->wait(8);

// $I->scrollTo(['css'=>'.send-print-request-printing-time'],0,-20);
// $I->wait(1);
// $I->see('8 hours');
// $I->see('24 hours');

// $I->wait(2);

/*
$I->click('span.request-more-time-span');
$I->waitForElement('form[name = "requestMoreTimeForm"]', 10);
$I->wait(2);


$I->click('span[class = "tsi tsi-calendar"]');
$I->wait(2);
$date = date('Y-m-d');
$newDate = date("m/d/Y", strtotime($date."+1 day"));
$I->click('td[data-day="'.$newDate.'"]');


$I->wait(30);
$I->seeInLastEmailTo('test2@test.work', 'Hello test2');

$I->wait(3);

$I->fillField('textarea[placeholder = "Comments"]', 'Test of changing the printing time');
$I->wait(3);
$I->click('button[loader-click="requestMoreTime()"]');
$I->wait(5);
$I->see('2', 'div.send-print-request-printing-time');
$I->wait(1);
$I->see('hours', 'div.send-print-request-printing-time');

$I->wait(3);
*/

$I->click('Accept order');
$I->wait(5);
$I->see('PLA');
$I->wait(3);
$I->see('Order #');

$I->executeJS('$("#dropYourPhoto").show();');
$I->executeJS('$(".dz-hidden-input[accept]").css("visibility", "visible").css("width","1").css("height","1");');
$I->attachFile('.dz-hidden-input[accept]', 'Order100.jpg');

$I->wait(10);

// $I->see('Incomplete orders: 2');
// $I->wait(1);
// $I->see('Pending Earnings: $297.50');

// $I->click('Set as printed');
$I->click('Submit results');
$I->wait(8);

if ($I->tryToClick('button[class = "btn btn-primary confirm"]'))
    {
        $I->wait(2);
    } 
else 
    {
        // do nothing
    }

// $I->see('$297.50', '.one-print-request__table');
// $I->wait(1);
// $I->see('Pending moderation'); 
