<?php

$I = new AcceptanceTester($scenario);
$I->am('Moderator'); 
$I->wantTo('Check order #101 status');
$I->expect("Order #101 - delivered"); 

$I->amOnSubdomain('backend'); // TODO backend-test
$I->amOnPage("/site/login");
$I->fillField('UserAdminLoginForm[username]','admin');
$I->fillField('UserAdminLoginForm[password]','qwewerert');
$I->click('login-button');
$I->wait(1);
$I->see('TS Admin');

# wait for antivirus finish their job
$I->wait(5);

$I->amOnPage("/store/store-order/view?id=100");
// $I->amOnPage("/store/store-order/view?id=2");
$I->wait(5);
$I->see('printed');

$I->click('Accept', 'div[class = "box box-solid print-results"]');
$I->wait(5);
$I->see('ready_send');
