<?php

$I = new AcceptanceTester($scenario);

$I->am('Registered user'); 
$I->wantTo('Certification print service for user Ford');
$I->expect("Print service already created in test 08_CreatePSWithRegCept"); 

$I->amOnSubdomain('ts'); // TODO backend-test

$I->amOnPage("/");

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','asen.kurin@gmail.com');
$I->fillField('#loginform-password','qwerty');
$I->click('Sign in', '.modal-body');
$I->wait(5);
$I->see('asen.kurin');

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Services','ul.dropdown-menu-right');
$I->wait(3);

$I->click('Get certified');
$I->see('Certification - Professional');

$I->click('Upgrade');

$I->click('div[class = "braintree-option braintree-option__card"]');
$I->wait(3);

$I->switchToIFrame("braintree-hosted-field-number");
$I->pressKey('input#credit-card-number','4111111111111111');
$I->wait(1);
$I->switchToIFrame(); 
$I->wait(1);
$I->switchToIFrame("braintree-hosted-field-expirationDate");
$I->pressKey('input#expiration','0322');
$I->wait(1);
$I->switchToIFrame(); 
$I->wait(1);
$I->switchToIFrame("braintree-hosted-field-cvv");
$I->pressKey('input#cvv','123');
$I->wait(1);
$I->switchToIFrame(); 
$I->wait(1);
// $I->click('Pay');
$I->click('input#paymentSubmit');
$I->wait(15);
$I->switchToIFrame("Cardinal-CCA-IFrame");
$I->wait(1);
$I->fillField('challengeDataEntry', '1234');
$I->wait(1);
// $I->click('Submit');
$I->click('input[class = "button primary"]');
$I->wait(15);

$I->see("Checkout complete");

$I->click('Continue');

// $I->click('div.my-ps-printers__block--certification-btn');
// $I->wait(3);
// $I->see('Certification Checklist');

$I->click('a[ng-click="showUploadDocumentModal()"]');

$I->executeJS('$("#dropYourPhoto").show();');
$I->executeJS('$(".dz-hidden-input[accept]").css("visibility", "visible").css("width","100").css("height","100").css("z-index","9999");');
$I->wait(8);
$I->attachFile('.dz-hidden-input[accept]', 'Order100.jpg');
$I->wait(5);
$I->attachFile('.dz-hidden-input[accept]', 'Order100.jpg');
$I->wait(5);
$I->click('button[class="btn btn-primary inspectletIgnore"]');
$I->wait(5);
$I->see('Document uploaded');

// $I->click('div[class = "ps-test__required ps-test__required--fail"]');
// $I->wait(5);
// $I->see('Company');
// $I->wait(1);
$I->fillField('input[ng-model="company.phone"]','7575255686');
$I->wait(1);
$I->click('button[loader-click = "sendVerifyPhoneCode()"]');
$I->wait(1);
$I->see('verify');

// $I->amOnUrl('http://backend.vcap.me/');
$I->amOnSubdomain('backend');
$I->amOnPage("/site/login");
$I->fillField('UserAdminLoginForm[username]','admin');
$I->fillField('UserAdminLoginForm[password]','qwewerert');
$I->click('login-button');
$I->wait(1);
$I->see('TS Admin');

# wait for antivirus finish their job
$I->wait(5);

$I->amOnPage("/crud/user-sms");
$I->wait(2);
$I->fillField('UserSmsSearch[user_id]', '1000');
$I->wait(1);
$I->pressKey('tr#w0-filters input[name="UserSmsSearch[user_id]"]', \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->wait(3);
$I->click('span[class = "glyphicon glyphicon-eye-open"]');
$I->wait(3);

$heading2 = $I->grabTextFrom('~Treatstock verification code: (\d+)~');
echo $heading2;

$I->wait(1);

$I->click('img[class = "user-image"]');
$I->wait(5);
$I->click('Sign out');

$I->wait(3);

// $I->amOnUrl('http://ts.vcap.me/');
$I->amOnSubdomain('ts');
$I->amOnPage("/");

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Services', 'ul.dropdown-menu-right');
$I->wait(3);
$I->click('Checking');
$I->wait(3);
// $I->click('div[class = "ps-test__required ps-test__required--fail"]');
$I->see('Phone number');
$I->wait(1);
$I->fillField('input[ng-model="company.phone"]','7575255686');
$I->wait(3);
$I->fillField('input[ng-model="company.$$phoneVerifyCode"]', $heading2);

$I->wait(1);
$I->scrollTo(['css'=>'button[loader-click = "confirmVerifyPhoneCode()"]'],0,-50);
$I->wait(2);

$I->click('button[loader-click = "confirmVerifyPhoneCode()"]');
$I->wait(3);
$I->see('details');

$I->wait(1);
$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
$I->see('Sign in');
 
 