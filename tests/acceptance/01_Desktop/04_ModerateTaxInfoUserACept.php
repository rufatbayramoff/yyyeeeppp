<?php 

$I = new AcceptanceTester($scenario);
$I->am('Moderator'); 
$I->wantTo('Moderate tax info - Accept');
$I->expect("Tax info approved"); 

$I->amOnSubdomain('backend'); // TODO backend-test
$I->amOnPage("/site/login");
$I->fillField('UserAdminLoginForm[username]','admin');
$I->fillField('UserAdminLoginForm[password]','qwewerert');
$I->click('login-button');
$I->wait(1);
$I->see('TS Admin');
$I->wait(1);

$I->amOnPage("/user/user-tax-info");
$I->click('Moderation');
// $I->wait(3);
$I->waitForText('Approve', 5);
$I->click('Approve');
// $I->wait(3);
// $I->see('Tax information approved');
$I->waitForText('Tax information approved', 5); 