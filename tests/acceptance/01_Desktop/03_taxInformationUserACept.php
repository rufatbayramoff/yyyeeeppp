
<?php 

$I = new AcceptanceTester($scenario);

$I->am('Registered user'); 
$I->wantTo('Fill in tax info');
$I->expect("Local user already created in test 02 SignUpUserA"); 

$I->amOnSubdomain('ts'); // TODO backend-test

$I->comment("because of active operations available only for registered and confirmed user, lets sign up"); 
$I->amOnPage("/");
$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');
$I->fillField('#loginform-email','asen.kurin@gmail.com');
$I->fillField('#loginform-password','qwerty');
$I->click('Sign in', '.modal-body');
// $I->wait(1);
// $I->see('asen.kurin');
$I->waitForText('asen.kurin', 3); 

// $I->amOnPage("/mybusiness/settings");
// $I->wait(3);
// $I->see('Public Profile Information');
// $I->waitForText('Public Profile Information', 5); 
// $I->waitForText('Main Information', 5);  
// $I->wait(1);
// $I->fillField('input[ng-model="ps.title"]', 'QA Tester');
// $I->wait(1);
// $I->fillField('textarea[ng-model="ps.description"]', 'Test QA');
// $I->wait(1);
// $I->fillField('input[ng-model="ps.website"]', 'www.test.qa');
// $I->wait(1);
// $I->click('Save');

// $I->wait(3);

$I->amOnPage("/mybusiness/taxes/step1");
// $I->wait(3);
// $I->see('Tax Information Interview');
$I->waitForText('Tax Information Interview', 5);

$I->click('#txlb0');
$I->wait(1);
$I->click('Next');

// $I->wait(3);
// $I->see('Business Name');
$I->waitForText('Business Name', 5);

$I->selectOption('UserTaxInfo[classification]', 'Individual/Sole proprietor');
// $I->wait(1);
$I->fillField('UserTaxInfo[name]', 'Acceptance Tester');
// $I->wait(1);
$I->fillField('UserTaxInfo[business_name]', 'Tester');
// $I->wait(1);
$I->fillField('UserAddress[address]', '100 Renaissance Center');
// $I->wait(1);
$I->fillField('UserAddress[city]', 'Detroit');
// $I->wait(1);
$I->selectOption('UserAddress[country_id]', 'United States');
$I->wait(1);
$I->fillField('UserAddress[region]', 'Michigan');
// $I->wait(1);
$I->fillField('UserAddress[zip_code]', '48243-1312');
// $I->wait(1);
$I->fillField('UserTaxInfo[identification]', '111111111');
// $I->wait(1);
$I->scrollTo(['css'=>'.btn-pager--next'],0,-50);
$I->wait(1);
$I->click('Next');

// $I->wait(3);
// $I->see('Please check');
$I->waitForText('Please check', 5);

// $I->wait(1);
$I->scrollTo(['css'=>'.btn-pager--next'],0,-50);
$I->wait(1);
$I->click('Next');

// $I->wait(3);
$I->waitForElement('#txlb0', 3);
$I->click('#txlb0');
// $I->wait(3);
$I->waitForElement('#usertaxinfo-signature', 3);
$I->fillField('UserTaxInfo[signature]', 'Tester');
// $I->wait(3);
$I->waitForElement('.btn-pager--next', 3);
$I->scrollTo(['css'=>'.btn-pager--next'],0,-50);
$I->wait(1);
$I->click('Finish');

// $I->see('Connect social networks');

$I->wait(1);

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
$I->see('Sign in');