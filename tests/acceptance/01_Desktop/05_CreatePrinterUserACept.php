<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Create print service');
$I->expect("Local user already created in test 02 SignUp"); 

$I->amOnSubdomain('ts'); // TODO backend-test

$I->amOnPage("/");

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','asen.kurin@gmail.com');
$I->fillField('#loginform-password','qwerty');
$I->click('Sign in', '.modal-body');
$I->wait(5);
$I->see('asen.kurin');

/*
$I->click('.header-bar__username');
$I->wait(1);
$I->click('Create a Business','ul.dropdown-menu-right');
$I->wait(1);
$I->waitForText('Main Information', 5);  
$I->fillField('input[ng-model="ps.title"]', 'QA Tester');
$I->fillField('textarea[ng-model="ps.description"]', 'Test QA');
$I->click('Save');
*/

$I->click('.header-bar__username');
$I->wait(1);
$I->click('Services','ul.dropdown-menu-right');
$I->wait(3);

// $I->see('Provide a service');
$I->see('Equipment');
$I->wait(1);
// $I->click('Provide a service');
$I->click('button#dropdownMenu1');
$I->wait(3);
// $I->click('3D Printing', 'div.dropdown-primary');
$I->click('a[href = "/mybusiness/services/add-printer"]');

$I->waitForElement('input[ng-model="psPrinter.title"]', 5);

$I->see('Name of your printer');

$I->selectOption('[ng-model="psPrinter.printer_id"]', 'LeapFrog Creatr XL');

$I->fillField('textarea','Some printer description');
$I->scrollTo(['css'=>'button[ng-click="steps.nextStep()"]'],0,-50);
$I->wait(1);
$I->click('button[ng-click="steps.nextStep()"]');
$I->wait(3);
$I->see('Minimum order charge');

$I->click('Add New Color');
$I->wait(3);
$I->selectOption('.added-color-select', 'Silver');
$I->wait(3);
$I->click('Add New Color');
$I->wait(3);
$I->selectOption('.added-color-select', 'DarkSlateGray');
$I->wait(3);
$I->click('Add New Color');
$I->wait(3);
$I->selectOption('.added-color-select', 'LimeGreen');
$I->wait(3);
$I->click('Add New Color');
$I->wait(3);
$I->selectOption('.added-color-select', 'White');
$I->wait(3);

$I->fillField('//div[@class="tab-content"]/descendant::input[1]','10');
$I->wait(1);
$I->fillField('//div[@class="tab-content"]/descendant::input[2]','11');
$I->wait(1);
$I->fillField('//div[@class="tab-content"]/descendant::input[3]','12');
$I->wait(1);
$I->fillField('//div[@class="tab-content"]/descendant::input[4]','13');
$I->wait(1);
$I->fillField('//div[@class="tab-content"]/descendant::input[5]','14');
$I->wait(1);
$I->scrollTo(['css'=>'button[ng-click="steps.nextStep()"]'],0,-50);
$I->wait(1);
$I->click('button[ng-click="steps.nextStep()"]');
$I->wait(3);
$I->see('building, street');

// S Front St, 8
$I->fillField('input[placeholder="Enter a location"]','520 S Front St Georgetown, DE, United States');
$I->wait(5);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(5);
// $I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
// $I->wait(5);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->wait(5);
$I->fillField('input[placeholder="Enter a location"]','United States, Georgetown, DE, S Front St, 520');
$I->wait(5);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(5);
// $I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
// $I->wait(5);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ENTER); // 520 S Front St, Georgetown, DE, United States

$I->wait(5);
$I->scrollTo(['css'=>'div[class = "right-delivery-block"]'],0,-200);
$I->wait(3);

$I->click('input[ng-model="$$allowedDeliveryTypes[deliveryType.id]"]', '#delivery-type-pickup');
$I->wait(2);
$I->fillField('input[ng-if="deliveryType.is_work_time"]','9 - 18');

$I->wait(2);
$I->click('Save printer');
$I->wait(3);
// $I->see('Materials and Prices');
$I->see('in moderation');