<?php 

$I = new AcceptanceTester($scenario);
$I->am('Moderator'); 
$I->wantTo('Moderate printservice - Accept');
$I->expect("Printervice published"); 

$I->amOnSubdomain('backend'); // TODO backend-test
$I->amOnPage("/site/login");
$I->fillField('UserAdminLoginForm[username]','admin');
$I->fillField('UserAdminLoginForm[password]','qwewerert');
$I->click('login-button');
$I->wait(1);
$I->see('TS Admin');

# wait for antivirus finish their job
$I->wait(5);

$I->amOnPage("/ps/ps-printer");

// $I->fillField('PsPrinterSearch[id]', '1');
// $I->click('.summary');
$I->wait(3);
$I->click('Moderate');
$I->wait(3);
$I->switchToLastWindow();
$I->wait(3);
$I->see('Materials and colors');

$I->click('Approve');
$I->wait(5);
$I->see('checked', 'tr[data-key = "1"]');

$I->switchToParentWindow();

/*
$I->wait(2);
$I->fillField('PsPrinterSearch[id]', '2');
$I->click('.summary');
$I->wait(12);
$I->click('Moderate');
$I->wait(2);

$I->switchToLastWindow();
$I->wait(3);
$I->see('Materials and colors');

$I->click('Approve');
$I->wait(5);
$I->see('checked', 'tr[data-key = "2"]');

$I->wait(2);
$I->switchToParentWindow();
$I->wait(1);
 * */