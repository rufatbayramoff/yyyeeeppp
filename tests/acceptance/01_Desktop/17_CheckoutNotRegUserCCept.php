<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Upload 3D model as STL file for not reg user');
$I->expect("Local user already created in test 02 SignUp"); 

$I->amOnSubdomain('ts'); // TODO backend-test

$I->setBraintreeThreeDSecureMode(false);

$I->see('Treatstock powers online');

$I->wait(1);

$I->click('Order 3D Print');
$I->wait(3);

$I->executeJS('$("#ts-uploader").show();');
$I->executeJS('$(".dz-hidden-input[accept]").css("visibility", "visible").css("width","1").css("height","1");');
$I->attachFile('.dz-hidden-input[accept]', 'clamp.stl');

$I->click('button[ng-click="nextStep()"]');

$I->waitForElement('div.material-item__color');
$I->wait(1);
$I->click('.ts-user-location');
$I->wait(3);
$I->fillField('UserLocator[address]','Georgetown DE, USA');
$I->wait(3);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(2);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ENTER);

$I->wait(5);

$I->click('div.model-printers-available__print-btn');

$I->wait(3);

$I->waitForElement('.delivery-details__title', 10);
$I->see("Recipient's details");
$I->wait(1);
$I->fillField('deliveryform-contact_name', 'Irina');
$I->wait(1);
$I->fillField('deliveryform-last_name', 'Svitova');
$I->wait(1);
$I->fillField('deliveryform-phone', '757525-5683');
$I->wait(1);
$I->fillField('deliveryform-email', 'ivsvitova@gmail.com');
$I->wait(1);

$I->click('Next');

$I->wait(15);

$I->see('Address');

$I->wait(1);
$I->click('div[class = "braintree-option braintree-option__card"]');
$I->wait(3);

$I->switchToIFrame("braintree-hosted-field-number");
$I->pressKey('input#credit-card-number','4111111111111111');
$I->wait(1);
$I->switchToIFrame(); 
$I->wait(1);
$I->switchToIFrame("braintree-hosted-field-expirationDate");
$I->pressKey('input#expiration','0322');
$I->wait(1);
$I->switchToIFrame(); 
$I->wait(1);
$I->switchToIFrame("braintree-hosted-field-cvv");
$I->pressKey('input#cvv','123');
$I->wait(1);
$I->switchToIFrame(); 
$I->wait(1);
// $I->click('Pay');
$I->click('input#paymentSubmit');
$I->wait(15);
$I->switchToIFrame("Cardinal-CCA-IFrame");
$I->wait(3);
$I->fillField('challengeDataEntry', '1234');
$I->wait(1);
// $I->click('Submit');
$I->click('input[class = "button primary"]');
$I->wait(15);

$I->waitForText('ivsvitova', 3);

$I->see("successfully");

$I->seeInLastEmail('ivsvitova');
$relativeUrl = $I->grabMatchedFromLastEmail('@http://.*?/(.*)@');
$I->expect("Approve url is: $relativeUrl  "); 

$I->amOnPage($relativeUrl);
$I->see('E-mail confirmed');

