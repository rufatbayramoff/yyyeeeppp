
<?php 

$I = new AcceptanceTester($scenario);

$I->am('Registered user'); 
$I->wantTo('Fill in tax info');
$I->expect("Local user already created in test 02 SignUpUserA"); 

$I->amOnSubdomain('ts'); // TODO backend-test

$I->comment("because of active operations available only for registered and confirmed user, lets sign up"); 
$I->amOnPage("/");
$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');
$I->fillField('#loginform-email','asen.kurin@gmail.com');
$I->fillField('#loginform-password','qwerty');
$I->click('Sign in', '.modal-body');
$I->waitForText('asen.kurin', 3); 

$I->click('.header-bar__username');
$I->wait(1);
$I->click('Create a Business','ul.dropdown-menu-right');

$I->waitForText('Main Information', 5);  
$I->fillField('input[ng-model="ps.title"]', 'QA Tester');
$I->wait(3);
$I->fillField('textarea[ng-model="ps.description"]', 'Test QA');
$I->wait(3);
$I->click('Save');

$I->waitForText('Service Category', 5);  

// $I->click('CNC machining', 'div.my-ps-printers__card');
$I->click('//html/body/div[1]/div[5]/div/div[1]/div/div/div/div[1]/div/div/div/div/button[3]');
$I->wait(1);
$I->click('Save');

/*
$I->waitForText('Phone number', 5);
$I->fillField('input[ng-model="company.phone"]','7575255686');
$I->wait(1);
$I->click('Save');
*/

$I->wait(1);

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
$I->see('Sign in');

$I->amOnSubdomain('backend'); // TODO backend-test
$I->amOnPage("/site/login");
$I->fillField('UserAdminLoginForm[username]','admin');
$I->fillField('UserAdminLoginForm[password]','qwewerert');
$I->click('login-button');
$I->wait(1);
$I->see('TS Admin');

# wait for antivirus finish their job
$I->wait(5);

$I->amOnPage("/ps/ps");

$I->fillField('PsSearch[id]', '2');
$I->click('.summary');
$I->wait(2);
$I->click('Moderate');

$I->wait(3);
$I->switchToLastWindow();
$I->wait(3);

$I->click('Approve');
$I->wait(5);
$I->see('checked', 'tr[data-key = "2"]');

$I->wait(2);
$I->switchToParentWindow();
$I->wait(1);
