<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Checking the accrual of money for order #100');
$I->expect("Local user already created in test 01_SignUpUserACept"); 

$I->amOnSubdomain('ts'); // TODO backend-test

$I->setBraintreeThreeDSecureMode(false);

$I->comment("because of active operations available only for registered and confirmed user, lets sign up"); 
$I->amOnPage("/");
$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');
$I->fillField('#loginform-email','asen.kurin@gmail.com');
$I->fillField('#loginform-password','qwerty');
$I->click('Sign in', '.modal-body');
$I->wait(5);
$I->see('asen.kurin');

$I->wait(1);

$I->click('.header-bar__avatar');
$I->moveMouseOver('.header-bar__avatar');
$I->wait(1);

$I->click('Earnings');
$I->wait(3);
$I->see('$437.50');

$I->wait(1);

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
$I->see('Sign in');
