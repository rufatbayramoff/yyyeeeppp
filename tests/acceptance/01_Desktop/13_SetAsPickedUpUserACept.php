<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Set as picked up order #100');
$I->expect("Local user already created in test 01_SignUpUserACept"); 

$I->amOnSubdomain('ts'); // TODO backend-test

$I->setBraintreeThreeDSecureMode(false);

$I->comment("because of active operations available only for registered and confirmed user, lets sign up"); 
$I->amOnPage("/");
$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');
$I->fillField('#loginform-email','asen.kurin@gmail.com');
$I->fillField('#loginform-password','qwerty');
$I->click('Sign in', '.modal-body');
$I->wait(5);
$I->see('asen.kurin');

$I->wait(1);

$I->click('.header-bar__avatar');
$I->moveMouseOver('.header-bar__avatar');
$I->wait(1);

$I->click('My Sales');
$I->wait(3);

$I->click('div.service-order-row__num');
$I->wait(3);
$I->click('Set as picked up');

$I->wait(3);
$I->see('Dispute Period');

$I->wait(1);

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
$I->see('Sign in');
