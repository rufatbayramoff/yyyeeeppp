userEmail = alisko.load('userA_Email');
BusinessName = alisko.load('userA_BusinessName');

randomstring = Math.random().toString(36).substring(7);
AcceptanceTester = "Acceptance Tester" + randomstring ;

BusinessName = "QA" + randomstring ;
BusinessDescription = "QaText"  + randomstring;

await signInAsUser(userEmail);

// await page.goto("http://ts.h9.tsdev.work/mybusiness/taxes/step1");
await page.click( 'css=[alt*="Gravatar image"]' );
await page.click( 'text=Business Tools' );
await page.click( 'text=Taxes' );

await page.waitForSelector( 'input:below(  :text("1. For U.S. tax purposes, are you a U.S. person? *") )' );
await page.waitForSelector( 'input:below(  :text("Yes") )' );
await page.click( 'label:below(  :text("1. For U.S. tax purposes, are you a U.S. person? *") )' );

await page.waitForSelector( 'text=Tax Information Interview' );
await page.click( 'label:below(  :text("1. For U.S. tax purposes, are you a U.S. person? *") )' );
await page.click( 'text=Next' );

await page.waitForSelector( 'text=Tax Information Interview' );
await page.selectOption( 'select:below(  :text("Federal Tax Classification") )', {label :'Individual/Sole proprietor'} );
await page.click( 'input:below(  :text("Full name") )' );
await page.fill( 'input:below(  :text("Full name") )', AcceptanceTester);
await page.click( 'input:below(  :text("Business Name") )' );
await page.fill( 'input:below(  :text("Business Name") )', BusinessName);
await page.click( 'input:below(  :text("Street address") )' );
await page.fill( 'input:below(  :text("Street address") )', '100 Renaissance Center' );
await page.click( 'input:below(  :text("City or town") )' );
await page.fill( 'input:below(  :text("City or town") )', 'Detroit' );
await page.click( 'input:below(  :text("State/Region") )' );
await page.fill( 'input:below(  :text("State/Region") )', 'Michigan' );
await page.click( 'input:below(  :text("Postal code") )' );
await page.fill( 'input:below(  :text("Postal code") )', '48243-1312' );
await page.click( 'input:below(  :text("Identification number") )' );
await page.fill( 'input:below(  :text("Identification number") )', '111111111' );
await page.click( 'text=Next' );

await page.waitForSelector( 'text=Please check that the information you have entered is correct' );
await page.click( 'text=Next' );

await page.waitForSelector( 'text=I consent to provide my electronic signature' );
await page.click( 'css=input[placeholder*="Type your name"]' );
await page.fill( 'css=input[placeholder*="Type your name"]', 'Tester' );
await page.click( 'text=Finish' );

await page.waitForSelector( 'text=Tax information submitted. Thank you.' );


