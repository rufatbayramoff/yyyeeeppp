userA_PrinterDescription = alisko.load('userA_PrinterDescription');

await getAdminPage();

// await page.click( 'a:below(  :text("Products"):visible )' );
await page.click( 'text=Company (Print Services)' );

// await page.click( 'text=Company Services' );
await page.click( 'text=Printers Moderation' );
await page.click( 'div:right-of( :text("admin"):visible )' );

// await page.goto(BACKEND_DOMAIN+"/ps/ps-printer");

await page.waitForSelector( ':text("Printers Moderation"):right-of( :text("admin") )' );

await page.click( ':text("Moderate"):below(  :text("View") )' );
await context.waitForEvent("page").then( (p)=>{page=p; page.bringToFront() } );
await page.click( 'div:right-of( :text("admin"):visible )' );
await page.click( 'text=Approve' );
await page.waitForSelector( 'div:right-of( :text("admin"):visible )' );
// await page.waitForSelector( ':text("checked"):right-of( :text("active"):visible )' );
await page.waitForSelector( ':text("approved"):right-of( :text("everywhere") )' );


alisko.save('userA_PrinterModerated', "1");

