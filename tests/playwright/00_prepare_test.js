// prepare for tests
console.log(BASE_DOMAIN + 'test/test/init-auto-test');
await page.goto(BASE_DOMAIN + 'test/test/init-auto-test');
await page.waitForTimeout(1000);
await page.waitForSelector( 'text=Done' );
if (await page.$( 'text=Exception' )) {
    throw new Error('Has exception on init');
}

alisko.save('prepared', "1");
