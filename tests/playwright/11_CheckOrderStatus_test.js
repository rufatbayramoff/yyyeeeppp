orderId = alisko.load('userB_orderId');
userB_orderId_printed = alisko.load('userB_orderId_printed');
// console.log(orderId);

await getAdminPage();

await page.click( 'text=Store' );
await page.waitForSelector( 'text=Orders' );

await page.click( 'text=Orders' );

// await page.click( 'text=#100' );
await page.click( 'text=#' + orderId );
await context.waitForEvent("page").then( (p)=>{page=p; page.bringToFront() } );

await page.waitForSelector( ':text("printed"):right-of( :text("Current attempt status") )' );

await page.click( 'text=Accept' );

await page.waitForSelector( ':text("ready_send"):right-of( :text("Current attempt status") )' );
alisko.save('userB_orderId_checked', "1");


