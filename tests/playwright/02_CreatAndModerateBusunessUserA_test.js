userEmail = alisko.load('userA_Email');

randomstring = Math.random().toString(36).substring(7);
BusinessName = "QA" + randomstring ;
BusinessDescription = "QaText"  + randomstring;

await signInAsUser(userEmail);

await page.click( 'css=[alt*="Gravatar image"]' );
await page.click( ':text("Create a Business"):below(  :text("My Business") )' );
await page.waitForSelector( 'text=Main Information' );
await page.click( 'input:right-of( :text("Business name") )' );
await page.fill( 'input:right-of( :text("Business name") )', BusinessName);
await page.click( 'textarea:right-of( :text("Business description") )' );
//await page.click( ':text("My Business"):below(  :text("Products") )' );
await page.fill( 'textarea:right-of( :text("Business description") )', BusinessDescription);
await page.click( 'text=Save' );
await page.waitForSelector( 'text=Service Category' );
//await page.waitForTimeout(2000);
//await page.click( 'ul:near(  :text("About"):visible )' );
// await page.click( 'text=Save' );

await page.waitForSelector( 'text=Manufacturing Services' );
//await page.click( ':text("Manufacturing Services"):near(  :text("Services") )' );
//await page.click( 'text=Manufacturing Services' );
// await page.click( ':text("Manufacturing Services"):near( :text("Injection Molding"):visible )' );

// await page.click( ':text("Manufacturing Services"):below(  :text("3D Printing"):visible )' );
// await page.click( ':text("3D Printing"):below(  :text("Vacuum Forming"):visible )' );

await page.click( ':text("3D Printing"):right-of( :text("Service Category") )' );


//await page.click( ':text("Manufacturing Services"):right-of( :text("Injection Molding"):visible )' );
//await page.click( ':text("Manufacturing Services"):below(  :text("Cutting"):visible )' );
//await page.click( ':text("Manufacturing Services"):below(  :text("3D Design"):visible )' );

await page.click( 'text=Save' );
await page.waitForSelector( 'text=Provide a service' );
await page.click( 'css=[alt*="Gravatar image"]' );
await page.click( 'text=Sign Out' );

await page.goto(BACKEND_DOMAIN);

await page.fill( 'css=input[placeholder*="Username"]', 'admin' );
await page.fill( 'css=input[placeholder*="Password"]', '123456' );
await page.click( ':text("Sign in"):below(  :text("Sign in to start your session") )' );

await page.click( 'a:below(  :text("Products"):visible )' );
await page.click( 'a:below(  :text("Company (Print Services)") )' );

await page.waitForSelector( ':text("Companies"):right-of( :text("admin") )' );

await page.waitForTimeout(500);

await page.click( ':text("Moderate"):below(  :text("View") )' );
await page.waitForTimeout(500);
await context.waitForEvent("page").then( (p)=>{page=p; page.bringToFront() } );

await page.waitForSelector( 'text=Moderator Comments' );

await page.click( 'text=Approve' );

await page.click( ':text("Moderate"):below(  :text("View") )' );
await context.waitForEvent("page").then( (p)=>{page=p; page.bringToFront() } );
await page.waitForSelector( ':text("checked"):right-of( :text("Moderator Status") )' );

// save information for user A
alisko.save('userA_BusinessName', BusinessName);
alisko.save('userA_BusinessDescription', BusinessDescription);
console.log('User created:', BusinessName, BusinessDescription);



