userEmail = alisko.load('userA_Email');
orderId = alisko.load('userB_orderId');
userB_orderId_liked = alisko.load('userB_orderId_liked');

await signInAsUser(userEmail);

await page.click( 'css=[alt*="Gravatar image"]' );
await page.click( 'text=My Sales' );

await page.waitForSelector( 'text=Quotes / Invoices' );
await page.click( ':text("Ready for dispatch"):near(  :text("Dispatched") )' );

// await page.click( 'text=Order #100' );
await page.click( 'text=Order #' + orderId );

// await page.click( 'text=Set as picked up' );
// await page.waitForSelector( 'text=Dispute Period' );

// await page.click( ':text("Dispatch"):below(  :text("Dispatch by October 7, Thursday") )' );
await page.click( 'text=Generate postal label' );
await page.waitForSelector( 'text=Estimated parcel size (in)' );
await page.click( 'text=Print postal label' );
await page.waitForSelector( 'text=Generate postal label' );
await page.click( 'text=Close' );
await page.click( ':text("Dispatch"):below(  :text("Dispatch by") )' );


alisko.save('userA_PickedUp', "1");

