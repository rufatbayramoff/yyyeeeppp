userEmail = alisko.load('userB_Email');
userName = alisko.load('userB_Name');
orderId = alisko.load('userB_orderId');
userB_orderId_checked = alisko.load('userB_orderId_checked');

console.log(userEmail);

await signInAsUser(userEmail);

// await page.waitForSelector( 'text='+userName );

await page.click( 'css=[alt*="Gravatar image"]' );
await page.click( 'text=My Purchases' );

// await page.click( 'text=Order #100' );
await page.click( 'text=Order #' + orderId );
await page.waitForSelector( 'text=Back' );

await page.click( 'text=Like results' );

await page.waitForSelector( 'text=Liked results' );
await page.waitForSelector( 'text=Payment status:' );
await page.waitForSelector( 'text=Ready for dispatch' );
alisko.save('userB_orderId_liked', "1");
