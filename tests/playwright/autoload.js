// autoload file 
//
// Treatstock.com

BASE_DOMAIN = 'http://ts.h.tsdev.work/';
BACKEND_DOMAIN = 'http://backend.h.tsdev.work/';
MAIL_DOMAIN = 'http://mail.h.tsdev.work';
DEFAULT_TIMEOUT = 15000;
DISABLE_EXTRA_HEADERS = true;

// modules
path = require('path');
fs = require('fs');

// path to files
FILES_PATH =  alisko.getRootFolder() + path.sep + "_files" + path.sep;


// domain name if non-default
try {
  const data1 = fs.readFileSync(alisko.getRootFolder() + path.sep + "settings.json", 'utf8')
  let data2 = JSON.parse(data1);
  BASE_DOMAIN = data2['BASE_DOMAIN'];
  BACKEND_DOMAIN = data2['BACKEND_DOMAIN'];
  MAIL_DOMAIN = data2['MAIL_DOMAIN'];
} catch (err) {
  console.log('ERROR:',err)
}

async function beforeTest() {
    console.log('prepare...');
    await page.setDefaultTimeout( DEFAULT_TIMEOUT );
}

function beforeCommand(fileName, lineNum) {
    if (!DISABLE_EXTRA_HEADERS && lineNum>=0) {
        page.setExtraHTTPHeaders( { RunningTest: fileName + ':' + lineNum }) 
    }
    if (DISABLE_EXTRA_HEADERS===false) {
        page.setExtraHTTPHeaders( {  }) 
    }
}

// залогиниться по емайлу
async function signInAsUser(email) {
    await page.goto(BASE_DOMAIN);
    await page.click( 'text=Sign in' );
    // await page.fill( 'input:below(  :text("Email"):visible )', email );
    // await page.click( 'input:below(  :text("Password"):visible )' );
    // await page.fill( 'input:below(  :text("Password"):visible )', 'qwerty' );
    await page.waitForSelector( 'text=Forgot password?' );
    await page.fill( 'input:below(  :text("Email") )', userEmail );
    await page.fill( 'input:below(  :text("Password"):visible )', 'qwerty' );
    await page.click( ':text("Sign in"):below(  :text("By using this Service, you agree to our"):visible )' );
    await page.waitForTimeout(1500);
    await page.waitForSelector( ':text("3D printing"):near(  :text("Online tools for") )' );
}

// залогиниться как админ в бэкенде
async function getAdminPage() {
    // await page.goto("http://backend.h9.tsdev.work/");
    await page.goto(BACKEND_DOMAIN);
    await page.fill( 'css=input[placeholder*="Username"]', 'admin' );
    await page.fill( 'css=input[placeholder*="Password"]', '123456' );
    await page.click( ':text("Sign in"):below(  :text("Sign in to start your session") )' );
    await page.waitForTimeout(500);
    await page.waitForSelector( 'text=Affiliate' );
}

// страница оплаты целиком

async function getPay() {
    await page.setDefaultTimeout( 120000 );
    // await page.waitForSelector( 'text=Choose a way to pay' );
    await page.setDefaultTimeout( 50000 ); // timeout in ms
    await page.waitForTimeout(10000);
    // await page.click( ':text("Card"):below( :text("Choose a way to pay") )' );
    await page.waitForTimeout(5000);
    frame1 = await ( await page.$('css=[title*="Secure payment input frame"]') ).contentFrame();
    await frame1.fill( 'css=input[placeholder*="1234 1234 1234 1234"]', '4111111111111111' );
    frame1 = await ( await page.$('css=[title*="Secure payment input frame"]') ).contentFrame();
    await frame1.click( 'css=input[placeholder*="MM / YY"]' );
    frame1 = await ( await page.$('css=[title*="Secure payment input frame"]') ).contentFrame();
    await frame1.fill( 'css=input[placeholder*="MM / YY"]', '1223' );
    frame1 = await ( await page.$('css=[title*="Secure payment input frame"]') ).contentFrame();
    await frame1.click( 'css=input[placeholder*="CVC"]' );
    frame1 = await ( await page.$('css=[title*="Secure payment input frame"]') ).contentFrame();
    await frame1.fill( 'css=input[placeholder*="CVC"]', '123' );
    await page.waitForTimeout(5000);
    await page.click( 'css=input[id="stripe-payment-submit"]' );
    await page.waitForTimeout(8000);
    await page.setDefaultTimeout( DEFAULT_TIMEOUT ); // timeout in ms
}

// Старая оплата, сохраню на случай возращения к ней
/*
async function getPay() {
    await page.setDefaultTimeout( 120000 );
    // await page.waitForSelector( 'text=Choose a way to pay' );
    await page.setDefaultTimeout( 50000 ); // timeout in ms
    await page.waitForTimeout(10000);
    await page.click( ':text("Card"):below( :text("Choose a way to pay") )' );
    await page.waitForTimeout(3500);
    frame1 = await ( await page.$('css=[title*="Secure Credit Card Frame - Credit Card Number"]') ).contentFrame();
    await frame1.fill( 'css=input[placeholder*="•••• •••• •••• ••••"]', '4111111111111111' );
    await page.waitForTimeout(1500);
    frame1 = await ( await page.$('css=[title*="Secure Credit Card Frame - Expiration Date"]') ).contentFrame();
    await frame1.fill( 'css=input[placeholder*="MM/YY"]', '1223' );
    await page.waitForTimeout(1500);
    frame1 = await ( await page.$('css=[title*="Secure Credit Card Frame - CVV"]') ).contentFrame();
    await frame1.fill( 'css=input[placeholder*="•••"]', '123' );
    await page.waitForTimeout(1500);
    await page.click( 'css=input[type="submit"][value*="Pay"]' );
    await page.waitForTimeout(10000);
    frame1 = await ( await page.$('text=<head></head>') ).contentFrame();
    await frame1.fill( 'css=input[placeholder*="Enter Code Here"]', '1234' );
    await page.waitForTimeout(2500);
    await frame1.click( 'css=input[type="submit"][value*="SUBMIT"]' );
    await page.waitForTimeout(5000);
    await page.setDefaultTimeout( DEFAULT_TIMEOUT ); // timeout in ms
}
*/

