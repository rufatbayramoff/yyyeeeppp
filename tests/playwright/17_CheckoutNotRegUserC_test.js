BusinessName = alisko.load('userA_BusinessName');
userA_printer_ready = alisko.load('userA_printer_ready');
userB_orderId = alisko.load('userB_orderId'); // баг чтобы исключить параллельность оплат!

DISABLE_EXTRA_HEADERS = true;

randomstring = Math.random().toString(36).substring(7);
FirstName = "QA" + randomstring ;
LastName = "QA" + randomstring ;
newUserName = "user" + randomstring ;
newEmail = newUserName  + "@gmail.com";

console.log(newEmail);

await page.goto(BASE_DOMAIN);

await page.waitForTimeout(100);

await page.click( 'text=Get instant quote' );
//await page.click( 'text=Order 3D Print' );

page.once( "filechooser", (f)=>f.setFiles( FILES_PATH + "clamp.stl" ) );
await page.click( 'text=Browse files' );
//await page.click( ':text("Upload 3D models"):right-of( :text("1") )' );
await page.waitForSelector( 'text=Size:' );

await page.click( 'text=Next' );
await page.waitForSelector( 'text=Select a file to change its material or color' );
await page.click( 'text=Los Angeles, US' );
await page.waitForSelector( 'text=Shipping Address' );
// await page.click( 'css=input[placeholder*="Enter a location"]' );
await page.waitForTimeout(400);
await page.fill( 'css=input[placeholder*="Enter a location"]', 'Georgetown DE, USA' );
await page.click( 'css=input[placeholder*="Enter a location"]' );
await page.click( ':text(","):near(  :text("USA") )' );

await page.waitForSelector( 'text=Georgetown, US' );
await page.waitForTimeout(800);

// await page.click( 'text=Buy' );
// await page.click( ':text("Buy"):right-of( :text("'+PS_NAME+'") )' );
// await page.click( ':text("Buy"):right-of( :text("'+BusinessName+'") )' );
// await page.waitForSelector( 'input:below(  :text("Email"):visible )' );
await page.click( 'text=Buy' );

await page.waitForTimeout(4000);
// await page.waitForSelector( 'text=Recipient' );
// await page.waitForSelector( 'text=Recipient' );

/*
await page.click( 'input:below(  :text("First name") )' );
await page.fill( 'input:below(  :text("First name") )', FirstName);
await page.click( 'input:below(  :text("Last name") )' );
await page.fill( 'input:below(  :text("Last name") )', LastName);
await page.click( 'css=input[placeholder*="Your phone number"]' );
await page.fill( 'css=input[placeholder*="Your phone number"]', '757525-5683' );
await page.click( 'input:below(  :text("Email"):visible )' );
await page.fill( 'input:below(  :text("Email"):visible )', newEmail );

await page.click( 'text=Next' );
*/

await page.waitForSelector( 'text=Shipping information' );
await page.fill( 'input:below(  :text("First Name") )', 'Test' );
await page.fill( 'input:below(  :text("Last Name") )', 'Testerov' );
await page.fill( 'input:below(  :text("Street Address") )', '244 S Front St' );
await page.fill( 'input:below(  :text("Zip") )', '19947' );
await page.fill( 'input:below(  :text("Email"):visible )', newEmail );

await page.click( 'text=Next' );

await page.click( ':text("Confirm"):right-of( :text("Cancel") )' );

await page.waitForTimeout(2000);

// страница оплаты целиком
await getPay();

await page.waitForSelector( 'text=Your order was successfully' );

s = await page.innerText( 'span:right-of(  :text("Order id") )' );
console.log('grabbed:',s);
alisko.save('userC_orderId', s);

// mailcatcher.connect("http://mail.h.tsdev.work");
mailcatcher.connect(MAIL_DOMAIN);
console.log(newEmail);
z = await mailcatcher.grab({to:newEmail, subject:'Confirm your Treatstock account', regex:'href="(.*?confirm.*?)"'})
console.log(z[1]);
activateline = z[1];

await page.goto(activateline);
await page.waitForSelector( 'text=E-mail confirmed' );

// save new user C
alisko.save('userC_Email', newEmail);
alisko.save('userC_Name', newUserName);
console.log('User created:',newEmail,newUserName, s);
