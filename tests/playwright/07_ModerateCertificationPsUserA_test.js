userA_PrinterModerated = alisko.load('userA_PrinterModerated');
userEmail = alisko.load('userA_Email');
userName = alisko.load('userA_Name');
BusinessName = alisko.load('userA_BusinessName');
userId = alisko.load('userA_Id');


DISABLE_EXTRA_HEADERS = true;
console.log('User A ID:', userId);
console.log('Sign in as:', userEmail);

await page.goto(BASE_DOMAIN);
await signInAsUser(userEmail);

await page.waitForTimeout(500);
await page.click( 'css=[alt*="Gravatar image"]' );
await page.waitForTimeout(500);

await page.click( ':text("Services"):near(  :text("Business Tools") )' );
//await page.click( 'text=Services' );
await page.waitForTimeout(1500);
await page.reload(); // странно но факт

// await page.click( 'text=Click here to verify.' );
await page.waitForSelector( 'text=Get certified' );

// await page.waitForSelector( 'text=Verify via SMS' );
await page.waitForTimeout(1500);
await page.click( 'text=Get certified' );
await page.waitForSelector( 'text=Desktop filament printer' );
// await page.waitForSelector( 'text=Certification - Professional' );

await page.click( ':text("Upgrade"):below(  :text("$10.00") )' );
// await page.click( ':text("Upgrade"):below(  :text("$100.00") )' );

// страница оплаты целиком
await getPay();

await page.waitForTimeout(3500);

await page.click( 'text=Continue' );
await page.click( 'text=Upload a form of ID' );

await page.click( 'text=Upload an ID' );
page.once( "filechooser", (f)=>f.setFiles( alisko.getRootFolder() + "/_files/Order100.jpg" ) );
await page.click( 'text=Please upload a form of ID such as a business registration certificate, business bank statement, recent business utility bill, or a valid driver' );

await page.click( ':text("Save"):right-of( :text("Cancel") )' );

await page.waitForSelector( 'text=Confirm cell phone number' );
await page.waitForTimeout(5000);
await page.fill( 'css=input[placeholder*="Your phone number"]', '7575255686' );
await page.waitForTimeout(1000);
await page.click( 'text=Verify via SMS' );

await page.waitForTimeout(1000);

await getAdminPage();

//await page.click( 'text=CRUD tools' );
//await page.click( 'text=user-sms' );
//await page.waitForSelector( 'text=user-sms' );
await page.goto(BACKEND_DOMAIN + "crud/user-sms");
await page.waitForSelector( 'text=user-sms' );


await page.click( 'input:below(  :text("User ID") )' );
// await page.fill( 'input:below(  :text("User ID") )', '1010' );
await page.fill( 'input:below(  :text("User ID") )', userId);
await page.click( 'input:below(  :text("User ID") )' );
await page.press( 'input:below(  :text("User ID") )', 'Enter' );

// await page.click( 'text=User ID' );

s = await page.innerText( 'text=Treatstock verification code:' );
s = s.match( /Treatstock verification code: (.*)/ )[1];
console.log('grabbed:',s);

// await page.goto("http://ts.h9.tsdev.work/");
await page.goto(BASE_DOMAIN);

await page.click( 'css=[alt*="Gravatar image"]' );
//await page.click( 'text=Services' );
await page.click( ':text("Services"):near(  :text("Business Tools") )' );

await page.click( 'text=Checking' );

await page.fill( 'input:near(  :text("Verify") )', s);
await page.waitForTimeout(400);
await page.click( 'text=Verify' );

await page.waitForTimeout(1000);

await page.waitForSelector( 'text=Verified' );
alisko.save('userA_printer_ready', "1");
