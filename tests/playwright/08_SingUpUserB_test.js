prepared = alisko.load('prepared'); // tests prepared?

await page.goto(BASE_DOMAIN);

randomstring = Math.random().toString(36).substring(7);
newUserName = "userB" + randomstring ;
newEmail = newUserName  + "@gmail.com";

await page.waitForSelector( 'text=Online tools for' );
await page.click( 'text=Sign in' );
await page.click( 'text=Sign up for free' );
await page.fill( 'input:below(  :text("E-mail") )', newEmail );
await page.fill( 'input:below(  :text("Password"):visible )', 'qwerty' );
await page.click( ':text("Sign up"):below(  :text("By using this Service, you agree to our") )' );
await page.waitForSelector( 'text=Thank you for signing up!' );

mailcatcher.connect(MAIL_DOMAIN);
z = await mailcatcher.grab({to:newEmail, subject:'Confirm your Treatstock account', regex:'href="(.*?confirm.*?)"'})
console.log(z[1]);
activateline = z[1];

await page.goto(activateline);
await page.waitForSelector( 'text=E-mail confirmed' );

// save new user B
alisko.save('userB_Email', newEmail);
alisko.save('userB_Name', newUserName);
console.log('User B created:',newEmail,newUserName);

