userEmail = alisko.load('userB_Email');
userName = alisko.load('userB_Name');
orderId = alisko.load('userB_orderId');
userA_PickedUp = alisko.load('userA_PickedUp');

// console.log(userEmail);

await signInAsUser(userEmail);

await page.click( 'css=[alt*="Gravatar image"]' );
await page.click( 'text=My Purchases' );

await page.waitForSelector( 'text=Quotes / Invoices' );
// await page.click( 'text=Delivered' );
await page.click( 'text=Order #' + orderId );

await page.waitForSelector( 'text=Payment status:' );
// await page.waitForSelector( 'text=Delivered' );
await page.waitForSelector( 'text=Paid' );

await page.click( 'text=Set as Received' );
await page.waitForSelector( 'text=Confirm' );
await page.click( 'text=Yes' );

// await page.click( 'div.rating-container:above( :text("rate the speed") )' );
await page.click( 'div.rating-container:above( :text("rate the speed") )', {position: {x:120,y:10}} );
await page.click( ':text("Review"):near(  :text("×") )' );
await page.click( 'div.rating-container:above( :text("rate the quality") )', {position: {x:140,y:10}} );
await page.click( ':text("Review"):near(  :text("×") )' );
await page.click( 'div.rating-container:above( :text("rate the communication") )', {position: {x:140,y:10}} );

await page.click( 'text=Submit' );
await page.waitForSelector( 'text=★★★★☆ (4.7/5)' );
await page.click( ':text("×"):near(  :text("Share review") )' );
await page.waitForSelector( 'text=Paid' );
await page.waitForSelector( 'text=Received' );

alisko.save('userB_Recieved', "1");

