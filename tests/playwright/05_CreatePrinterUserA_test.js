userEmail = alisko.load('userA_Email');
BusinessName = alisko.load('userA_BusinessName');

randomstring = Math.random().toString(36).substring(7);
PrinterDescription = "QA" + randomstring;


await signInAsUser(userEmail);

await page.click( 'css=[alt*="Gravatar image"]' );
await page.click( ':text("Services"):near(  :text("Business Tools") )' );
//await page.click( 'text=Services' );
await page.waitForSelector( 'text=We recommend verifying your phone number to improve communication.' );
await page.click( 'text=Provide a service' );
// await page.click( ':text("3D Printing"):near(  :text("3D Scanning") )' );

await page.click( 'span:right-of( :text("Add") )' );
await page.waitForSelector( 'text=2D Cutting and Engraving' );
await page.click( ':text("3D Printing"):below(  :text("2D Cutting and Engraving"):visible )' );
//await page.click( ':text("3D Printing"):near(  :text("2D Cutting and Engraving") )' );

await page.click( 'text=LeapFrog Creatr XL' );
await page.click( 'textarea:below(  :text("Description") )' );
await page.fill( 'textarea:below(  :text("Description") )', PrinterDescription);
await page.click( 'text=Next Step →' );

await page.waitForSelector( 'text=Minimum order charge:' );
await page.waitForSelector( 'text=Add New Color' );

await page.click( 'text=Add New Color' );
await page.click( 'select:below(  :text("White") )' );
await page.click( 'text=Please add your material colors in accordance with a color sample rather than its title.' );
await page.selectOption( 'select:below(  :text("White") )', {label :'Silver'} );

await page.click( 'text=Add New Color' );
// await page.selectOption( 'select:below(  :text("Silver") )', {label :'DarkSlateGray'} );
await page.selectOption( 'select:below(  :text("Silver") )', {label :'Gray'} );

await page.click( 'text=Add New Color' );
// await page.selectOption( 'select:below(  :text("Dark Gray") )', {label :'Lime Green'} );

// await page.click( 'text=Next' );
// await page.waitForSelector( 'text=Materials' );

// await page.click( 'text=Add material' );
// await page.selectOption( 'select:below(  :text("Green Glow in Dark") )', {label :'Lime Green'} );
await page.selectOption( 'select:below(  :text("Gray") )', {label :'Lime Green'} );

await page.click( 'text=Add New Color' );
// await page.selectOption( 'select:below(  :text("LimeGreen") )', {label :'Blue'} );
await page.selectOption( 'select:below(  :text("Lime Green") )', {label :'Blue'} );

await page.click( 'css=input[placeholder*="Input Price"]' );
await page.fill( 'css=input[placeholder*="Input Price"]', '10' );
await page.click( 'input:below(  :text("$") )' );
await page.fill( 'input:below(  :text("$") )', '11' );
// await page.click( 'input:right-of( :text("DarkSlateGray") )' );
// await page.fill( 'input:right-of( :text("DarkSlateGray") )', '12' );
// await page.click( 'input:right-of( :text("LimeGreen") )' );
// await page.fill( 'input:right-of( :text("LimeGreen") )', '13' );
// await page.click( 'input:right-of( :text("Blue") )' );
// await page.fill( 'input:right-of( :text("Blue") )', '14' );

await page.fill( 'input:right-of( :text("Gray") )', '12' );
await page.fill( 'input:right-of( :text("Lime Green") )', '13' );
await page.fill( 'input:right-of( :text("Blue") )', '14' );

await page.click( 'text=Accept' );
await page.click( 'text=Next Step →' );

await page.waitForSelector( 'text=Search on map' );
await page.click( 'css=input[placeholder*="Enter a location"]' );
await page.fill( 'css=input[placeholder*="Enter a location"]', '244 S Front St Georgetown, DE, United States' );
await page.click( ':text("United States"):right-of( :text(","):visible )' );
await page.waitForTimeout(8000);
// await page.click( 'input:near(  :text("Customer Pickup") )' );
// await page.click( 'css=input[placeholder*="Working hours"]' );
// await page.fill( 'css=input[placeholder*="Working hours"]', '9-18' );

await page.click( 'input:near(  :text("International Shipping") )' );


await page.click( 'text=Save printer' );
await page.waitForTimeout(500);
await page.click( ':text("Ok"):right-of( :text("Cancel"):visible )' );

await page.waitForSelector( 'text=Please complete a test order for your machine to get it published on Treatstock.' );

// save information for user A
alisko.save('userA_PrinterDescription', PrinterDescription);
console.log('Printer description User A:', PrinterDescription);

