<?php

namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Facebook\WebDriver\Remote\WebDriverCommand;
use Facebook\WebDriver\Remote\DriverCommand;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\WebDriverBrowserType;
use Facebook\WebDriver\WebDriverPlatform;
use Facebook\WebDriver\Firefox\FirefoxProfile;
use Facebook\WebDriver\Firefox\FirefoxDriver;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use PDO;

class AcceptanceHelper extends \Codeception\Module\WebDriver
{
    var $langBrowser = "en";
    var $caps, $profile;
    var $debugmode;

    protected $previousTestSuccessFlag = false;
    protected $isFirstTest = true;
    protected $wasSkipped = false;

    public function _initialize()
    {
        // parent::_initialize();

        // Init main url
        if (array_search('-d', $_SERVER['argv'])) {
            $this->debugmode = true;
        }
        $purl = parse_url($this->config['url']);
        $host = explode(".", $purl['host']);
        if (file_exists('/vagrant/conf/hostid.php')) {
            $hostId = include '/vagrant/conf/hostid.php';
        } else {
            $hostId = "";
        }
        if (!preg_match('/' . $hostId . '$/', $host[1], $match)) {
            $host[1] = $host[1] . $hostId;
        }
        $newUrl = $purl['scheme'] . '://' . implode('.', $host);
        $this->config['url'] = $newUrl;

        $this->wd_host = sprintf('http://%s:%s/wd/hub', $this->config['host'], $this->config['port']);
        $this->capabilities = $this->config['capabilities'];
        if (isset($_SERVER['BROWSER'])) {
            switch (strtolower($_SERVER['BROWSER'])) {
                case "chrome":
                    $this->capabilities[WebDriverCapabilityType::BROWSER_NAME] = strtolower($_SERVER['BROWSER']);
                    $this->config['browser'] = strtolower($_SERVER['BROWSER']);
                    break;
                case "firefox":
                    $this->capabilities[WebDriverCapabilityType::BROWSER_NAME] = strtolower($_SERVER['BROWSER']);
                    $this->config['browser'] = strtolower($_SERVER['BROWSER']);
                    break;
                default:
                    $this->capabilities[WebDriverCapabilityType::BROWSER_NAME] = $this->config['browser'];
                    break;
            }
        }
        $this->capabilities[WebDriverCapabilityType::PROXY] = $this->getProxy();
        $this->connectionTimeoutInMs = $this->config['connection_timeout'] * 1000;
        $this->requestTimeoutInMs = $this->config['request_timeout'] * 1000;

        $this->webDriver = null;

        $caps = new DesiredCapabilities([
            WebDriverCapabilityType::BROWSER_NAME => $this->config['browser'],
            WebDriverCapabilityType::PLATFORM     => WebDriverPlatform::ANY,
        ]);
        if ($this->config['browser'] == 'chrome') {
            $profile = new ChromeOptions();
            $profile->addArguments(["--lang=en", "--disable-extensions", "--allow-running-insecure-content", "test-type", "--disable-web-security"]);
            $profile->setExperimentalOption('prefs', [
                'intl.accept_languages' => 'en'
                // 'download.prompt_for_download' => '0',
                // 'download.default_directory' => $_SERVER["DOWNLOADFOLDER"] 
                // 'download.directory_upgrade' => '1'
                // 'profile.default_content_settings.popups' => '0'
                // 'profile.default_content_setting_values.automatic_downloads'=>'1'
            ]);
            $caps->setCapability(ChromeOptions::CAPABILITY, $profile);
        }
        if ($this->config['browser'] == 'firefox') {
            // disable the "Reader View" help tooltip, which can hide elements in the window.document
            $profile = new FirefoxProfile();
            $profile->setPreference("intl.accept_languages", $this->langBrowser);
            $profile->setPreference('reader.parse-on-load.enabled', false);
            $profile->setPreference('browser.download.panel.shown', false);
            $profile->setPreference('browser.download.manager.retention', 1);
            $profile->setPreference('browser.download.animateNotifications', false);
            $profile->setPreference('browser.download.panel.removeFinishedDownloads', true);
            $profile->setPreference('browser.download.manager.flashCount', 3000);
            $profile->setPreference('browser.download.panel.shown', true);
            $profile->setPreference('browser.helperApps.neverAsk.saveToDisk',
                'application/download, application/octet-stream, application/sla, application/stl, application/vnd.ms-pki.stl');
            //$profile->setPreference('browser.download.folderList', 2);
            //$profile->setPreference('browser.download.dir', "c:\\temp\\");
            //$profile->setPreference('browser.download.manager.alertOnEXEOpen', false);
            $profile->setPreference('browser.download.manager.showWhenStarting', false);
            $profile->setPreference('browser.download.manager.focusWhenStarting', false);
            //$profile->setPreference('browser.download.useToolkitUI', true);          
            //$profile->setPreference('browser.download.useDownloadDir', false);
            //$profile->setPreference('browser.helperApps.alwaysAsk.force', false);
            $profile->setPreference('browser.download.manager.closeWhenDone', true);
            $profile->setPreference('browser.download.manager.showAlertOnComplete', false);
            $profile->setPreference('browser.download.manager.useWindow', false);
            //$profile->setPreference('services.sync.prefs.sync.browser.download.manager.showWhenStarting"', false); // Disable by firefox 57
            $profile->setPreference('security.mixed_content.block_active_content', false);
            $profile->setPreference('security.mixed_content.block_display_content', true);
            //$profile->setPreference('pdfjs.disabled', false);

            $caps->setCapability(FirefoxDriver::PROFILE, $profile);
        }

        try {
            $this->webDriver = RemoteWebDriver::create(
                $this->wd_host,
                $caps, // $this->capabilities,
                $this->connectionTimeoutInMs,
                $this->requestTimeoutInMs,
                $this->httpProxy,
                $this->httpProxyPort
            );
            // $this->_initializeSession();
            if ($this->config['browser'] == 'firefox') {
                $this->profile = $profile;
            }
            $this->caps = $caps;
        } catch (WebDriverCurlException $e) {
            throw new ConnectionException($e->getMessage() . "\n \nPlease make sure that Selenium Server or PhantomJS is running.");
        }
        //        $this->webDriver->manage()->timeouts()->implicitlyWait($this->config['wait']); # Disabled by firefox 57
        $this->initialWindowSize();
    }

    public function restoreDefaultWindowSize()
    {
        $this->initialWindowSize();
    }

    public function setLanguage($language = "en")
    {
        $this->langBrowser = $language;
        if ($this->config['browser'] == 'firefox') {
            $this->profile->setPreference("intl.accept_languages", $this->langBrowser);
            $this->caps->setCapability(FirefoxDriver::PROFILE, $this->profile);
        }
        if ($this->config['browser'] == 'chrome') {
            $profile = new ChromeOptions();
            $profile->addArguments(["--lang=" . $language, "--disable-extensions", "--allow-running-insecure-content", "test-type", "--disable-web-security"]);
            $profile->setExperimentalOption('prefs', [
                'intl.accept_languages' => $language
                // 'download.prompt_for_download' => '0',
                // 'download.default_directory' => $_SERVER["DOWNLOADFOLDER"] 
                // 'download.directory_upgrade' => '1'
                // 'profile.default_content_settings.popups' => '0'
                // 'profile.default_content_setting_values.automatic_downloads'=>'1'
            ]);
            $this->caps->setCapability(ChromeOptions::CAPABILITY, $profile);
        }
        //$this->_capabilities(function($currentCapabilities) {
        //    return $this->caps;
        //});
        $this->capabilities = $this->caps;
        $this->_closeSession($this->webDriver);
        $this->_initializeSession();
    }

    public function amOnSubdomain($subdomain)
    {
        $purl = parse_url($this->config['url']);
        $host = explode(".", $purl['host']);
        if (file_exists('/vagrant/conf/hostid.php')) {
            $hostId = include '/vagrant/conf/hostid.php';
        } else {
            $hostId = "";
        }
        if (!preg_match('/' . $hostId . '$/', $host[1], $match)) {
            $host[1] = $host[1] . $hostId;
        }
        array_shift($host);
        $newUrl = $purl['scheme'] . '://' . $subdomain . '.' . implode('.', $host);
        $this->_reconfigure(['url' => $newUrl]);
    }

    public function _before(\Codeception\TestInterface $test)
    {
        $checkpointName = $this->getCheckpointName($test);
        if ($this->testWasDone($checkpointName)) {
            if ($this->debugmode) {
                echo "   Was done, SKIP\n";
                $test->getMetadata()->setSkip('Was done');
                $this->wasSkipped = true;
            }
        } else {
            if ($this->previousTestSuccessFlag) {
                if ($this->debugmode) {
                    echo '   Make a dump of database: ' . $checkpointName . "...\n";
                }
                $this->makeCheckpoint($checkpointName, true);
            } else {
                if (!$this->isFirstTest || $this->wasSkipped) {
                    if ($this->debugmode) {
                        echo '   Restore a dump of database: ' . $checkpointName . "...\n";
                    }
                    if ($this->isCheckpointExists($checkpointName)) {
                        $this->restoreCheckpoint($checkpointName);
                    } else {
                        if (!$this->isFirstTest) {
                            die('   Can`t restore checkpoint.');
                        }
                    }
                }
            }
            if ($this->debugmode) {
                echo "   Waiting before start to ensure cookies deleted.\n";
            }
            sleep(1);
        }
        $this->previousTestSuccessFlag = false;
        $this->isFirstTest = false;
        parent::_before($test);
    }

    public function _after(\Codeception\TestInterface $test)
    {
        if ($test->getMetadata()->getSkip()) {
            parent::_after($test);
            return;
        }
        if ($this->debugmode) {
            echo "   Stopping timeouts, cleaning cookies...\n";
        }
        $this->webDriver->executeScript("var id = window.setTimeout(function() {}, 0); while (id--) { window.clearTimeout(id); }; "
            . "id = window.setInterval(function() {}, 0);  while (id--) { window.clearInterval(id); }; return true;"); //

        $this->deleteAllCookies();
        $checkpointName = $this->getCheckpointName($test);
        $this->previousTestSuccessFlag = true;
        sleep(1);
        if (!$test->getMetadata()->isBlocked()) {
            if ($this->debugmode) {
                echo "   mark done";
            }
            $this->markDone($checkpointName);
        } else {
            if ($this->debugmode) {
                echo "   done.\n";
            }
        }
        parent::_after($test);
    }

    function deleteAllCookies()
    {
        $this->webDriver->manage()->deleteAllCookies();
        //$this->webDriver->getSessionStorage()->clear(); //?
        //$this->webDriver->getLocalStorage()->clear(); //?
        $this->webDriver->executeScript("window.sessionStorage.clear(); return true;");
        $this->webDriver->executeScript("window.localStorage.clear(); return true;");
        if ($this->debugmode) {
            echo "   cookies deleted.\n";
        }
    }

    public function seeFullUrlEquals($expected)
    {
        $actual = $this->webDriver->getCurrentURL();
        $this->assertEquals($expected, $actual);
    }

    public function dontSeeFullUrlEquals($expected)
    {
        $actual = $this->webDriver->getCurrentURL();
        $this->assertNotEquals($expected, $actual);
    }

    protected function getDbh(): PDO
    {
        $dsn = $this->getModule('Db')->_getConfig('dsn');
        $user = $this->getModule('Db')->_getConfig('user');
        $pass = $this->getModule('Db')->_getConfig('password');
        $pdo = new PDO($dsn, $user, $pass);
        return $pdo;
    }

    function putValue($name, $value)
    {
        $dbh = $this->getDbh();
        // clean old values if exist
        $sth = $dbh->prepare('DELETE FROM off_codeception_temp_values WHERE code = :code');
        $sth->bindParam(':code', $name);
        $sth->execute();
        // insert new one
        $sth = $dbh->prepare('INSERT INTO off_codeception_temp_values (code,val) values(:code,:val)');
        $sth->bindParam(':code', $name);
        $sth->bindParam(':val', $value);
        $sth->execute();
    }

    function getValue($name)
    {
        $dbh = $this->getDbh();
        $sth = $dbh->prepare("SELECT val FROM off_codeception_temp_values WHERE code = :code");
        $sth->bindParam(':code', $name);
        $sth->execute();
        $result = $sth->fetch();
        if (isset($result["val"])) {
            return $result["val"];
        }
        return ""; // or none
    }

    protected function getCheckpointName(\Codeception\TestInterface $test)
    {
        $filename = $test->getFileName();
        $filename = substr($filename, strpos($filename, "acceptance"));
        $filename = substr($filename, strpos($filename, "/") + 1);
        $filename = str_replace("/", "--", $filename);
        $filename = str_replace(".php", "", $filename);
        $filename = "_auto_" . $filename;
        return $filename;
    }

    // save full copy of database to tests/_debug/$name.sql
    //  if this file does not exists, or if (force is true or omitted)
    public function makeCheckpoint($name, $forceToDo = false)
    {
        if (file_exists("tests/_debug/" . $name . ".sql") && !$forceToDo) {
            if ($this->debugmode) {
                echo "   Checkpoing tests/_debug/" . $name . ".sql already exist.\n";
            }
        } else {
            if ($this->debugmode) {
                echo "   Making checkpoing tests/_debug/" . $name . ".sql...";
            }
            $params = $this->config['dsn'];
            $command = "echo SET foreign_key_checks=0\; > tests/_debug/" . $name . ".sql; /usr/bin/mysqldump --add-drop-table " . $params . " >> tests/_debug/" . $name . ".sql 2> /tmp/checkpoint.log; echo SET foreign_key_checks=1\; >> tests/_debug/" . $name . ".sql;";
            exec($command, $output, $result);
            if ($result === 0) {
                if ($this->debugmode) {
                    echo " done.\n";
                }
            } else {
                if ($this->debugmode) {
                    echo " fail!\n";
                }
            }
        }
    }

    public function testWasDone($name)
    {
        if (file_exists('tests/_doneMark/' . $name . '.done')) {
            return true;
        }
        return false;
    }

    public function markDone($name)
    {
        file_put_contents('tests/_doneMark/' . $name . '.done', '1');
    }

    public function isCheckpointExists($name)
    {
        if (file_exists("tests/_debug/" . $name . ".sql")) {
            return true;
        }
        return false;
    }

    // restore database from tests/_debug/$name.sql
    public function restoreCheckpoint($name)
    {
        if (file_exists("tests/_debug/" . $name . ".sql")) {
            if ($this->debugmode) {
                echo "   Restoring checkpoing tests/_debug/" . $name . ".sql...";
            }
            $params = $this->config['dsn'];
            $command = "/usr/bin/mysql --init-command=\"SET SESSION FOREIGN_KEY_CHECKS=0;\" " . $params . " < tests/_debug/" . $name . ".sql 2> /tmp/checkpointin.log";
            exec($command, $output, $result);
            if ($result === 0) {
                if ($this->debugmode) {
                    echo " restore database done...";
                }
                exec('/usr/bin/php yii filepage-maps/printers-tree-update', $output, $result);
                if ($result === 0) {
                    if ($this->debugmode) {
                        echo " reset cache printers done.\n";
                    }
                } else {
                    if ($this->debugmode) {
                        echo " reset cache printers fail!\n";
                    }
                }
            } else {
                if ($this->debugmode) {
                    echo " restore database fail!\n";
                }
            }
        } else {
            if ($this->debugmode) {
                echo "   Can'd found the file tests/_debug/" . $name . ".sql...";
            }
        }
    }
}
