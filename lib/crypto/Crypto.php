<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.06.17
 * Time: 14:11
 */

namespace lib\crypto;


/**
 * Wrapper for crypto functions (libs)
 */
class Crypto
{
    private string $cipher;

    public function __construct()
    {
        $this->cipher = 'AES-256-CBC';
    }


    public function encrypt($message, $key)
    {
        $iv = substr(hash('sha256', $key), 0, 16);
        $encryptedText = openssl_encrypt($message, $this->cipher, $key, $options = 0, $iv);
        $base64encoded = base64_encode($encryptedText);
        return urlencode($base64encoded);
    }

    public function decrypt($message, $key)
    {
        $iv = substr(hash('sha256', $key), 0, 16);
        $base64encoded = base64_decode(urldecode($message));
        return openssl_decrypt($base64encoded, $this->cipher, $key, $options = 0, $iv);
    }
}