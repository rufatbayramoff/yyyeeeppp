<?php
/**
 * class to place watermarks to 3d models using REST API
 *
 * @author Nabi Defacto <n.ibatulin@treatstock.com>
 */

namespace lib\d3;


/**
 *
 * Class Model3dWatermark
 *
 * @package lib\d3
 */
class Model3dWatermark extends NodeJsApi
{

    public function __construct($server, $passPhrase = '')
    {
        parent::__construct($server, $passPhrase);
        $this->timeout = 120; // Watermark timeout
        $this->connectTimeout = 5; // Watermark timeout
        $this->retryRequest = false;
    }

    /**
     * curl -d passphrase=qwery -d filepathin=data/robo.stl -d filepathout=data/marked.stl -d text=ehlo url
     *
     * @param $fileIn
     * @param $fileOut
     * @param $textToEncode
     * @return string
     */
    public function encode($fileIn, $fileOut, $textToEncode)
    {
        $this->requestUrl = '/jsapi/private/watermark/encode';
        $this->requestPost = sprintf(
            'passphrase=%s&filepathin=%s&filepathout=%s&text=%s',
            $this->passPhrase,
            $fileIn,
            $fileOut,
            $textToEncode
        );
        return $this->run();
    }

    /**
     * curl -d passphrase=qwery -d filepath=data/marked.stl http://10.102.0.81:5858/jsapi/private/watermark/decode
     *
     * @param $filePath
     * @return string in json format
     */
    public function decode($filePath)
    {
        $this->requestUrl = '/jsapi/private/watermark/decode';
        $this->requestPost = sprintf(
            'passphrase=%s&filepath=%s',
            $this->passPhrase,
            $filePath
        );
        return $this->run();
    }

    /**
     * @param $filePath
     * @return string in json format
     */
    public function decodeAll($filePath)
    {
        $this->requestUrl = '/jsapi/private/watermark/decode';
        $this->requestPost = sprintf(
            'findall=1&passphrase=%s&filepath=%s',
            $this->passPhrase,
            $filePath
        );
        return $this->run();
    }
}