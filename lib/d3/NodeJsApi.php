<?php
/**
 * User: nabi
 */

namespace lib\d3;


use yii\db\Exception;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\HttpException;

class NodeJsApi
{
    /**
     * Node js answer timeout
     */
    public $timeout = 30;
    public $connectTimeout = 30;

    /**
     * Second attemtp timeout
     */
    public $retryRequest = true;
    public $retrySleep = 3;

    public $passPhrase;

    /**
     * @var string
     */
    public $server;

    /**
     * @var
     */
    protected $requestUrl;

    /**
     * string to post
     *
     * @var string
     */
    protected $requestPost;

    /**
     * @var string
     */
    public $isDebugOutputOn = false;

    /**
     * Model3dWatermark constructor.
     *
     * @param string $server
     * @param $passPhrase
     */
    public function __construct($server, $passPhrase = '')
    {
        $this->server     = $server;
        $this->passPhrase = $passPhrase;
    }

    public function measure($filePath)
    {
        $this->requestUrl  = '/jsapi/private/measure';
        $this->requestPost = sprintf('filepath=%s', $filePath);
        return $this->parseJsonResult($this->run());
    }

    /**
     * json format string
     *
     */
    public function parseJsonResult($output)
    {
        $props = Json::decode($output);
        return $props;
    }


    /**
     * make real request
     *
     * @return mixed
     * @throws HttpException
     */
    protected function runRequest()
    {
        $ch  = curl_init();
        $url = $this->server . $this->requestUrl;
        $this->debugOut('Request url: '.$url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connectTimeout);
        if (!empty($this->requestPost)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestPost);
            curl_setopt($ch, CURLOPT_POST, 1);
            $this->debugOut('Request post: ' . json_encode($this->requestPost));
        }
        $headers   = [];
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result   = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->debugOut('Response code: ' . $httpCode);
        $this->debugOut('Response len: ' . strlen($result));
        $this->debugOut('Response text: ' . substr($result, 0, 250) . (strlen($result) > 250 ? '...' : ''));
        if (curl_errno($ch)) {
            $error = 'Error: ' . curl_error($ch);
            $this->debugOut($error);
            \Yii::error($error, 'watermark');
            throw new HttpException(500, $error);
        }
        \Yii::info(VarDumper::dumpAsString($result), 'watermark');
        curl_close($ch);
        return $result;
    }

    public function debugOut($string)
    {
        if ($this->isDebugOutputOn) {
            echo date('Y-m-d H:i:s') . ' NodeJS - ' . $string . "\n";
        }
    }

    /**
     * @return mixed
     * @throws HttpException
     */
    protected function run()
    {
        $result = '';
        try {
            $result = $this->runRequest();
        } catch (HttpException $exception) {
            // HTTP Error, try again
            if ($this->retryRequest) {
                sleep($this->retrySleep);
                $result = $this->runRequest();
            }
        }
        return $result;
    }


    /**
     * set passphrase
     *
     * @param $passPhrase
     */
    public function setPassPhrase($passPhrase)
    {
        $this->passPhrase = $passPhrase;
    }

}