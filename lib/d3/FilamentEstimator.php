<?php namespace lib\d3;

/**
 * Based on https://github.com/algspd/Filament-Estimator-for-GCode
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class FilamentEstimator
{

    public static $filamentDiameter = 3; // mm\n
    public static $filamentDensity = 1.05; // g/cc
    public $filename = '';
    
    /**
     * 
     * @param string $gCodeFile path to gcode file
     * @param float  $diameter  - mm\n
     * @param float  $density   - g/cc
     * @return array - result format [length=>,volume=>,weight=>]
     */
    public static function getFilamentDetails($gCodeFile, $diameter = 3, $density = 1.05)
    { 
        $length = 0;
        $last = 0;
        $lineNum = 0;
        $filamentArea = ($diameter / 20) * ($diameter / 20) * pi(); // cm^2
        $file = fopen($gCodeFile, "r");
        while ($line = fgets($file)) {
            rtrim($line);
            if (1 == preg_match("/G1.*E *(\d+\.\d*)/i", $line, $match)) {
                $last = $match[1];
            } else if (1 == preg_match("/G92.*E0/", $line, $match)) {
                $length += $last / 10;
                $last = 0;
            }
            $lineNum++;
        }
        $length += $last / 10;
        $volume = $filamentArea * $length;
        $grams = ($density * $volume);
        return ['length' => $length, 'volume' => $volume, 'weight' => $grams];
    }
    
    /**
     * get filament info from g-code file
     * 
     * @param  string $gCodeFile full file path to gcode file
     * @return array [filamentRequired=>,filamentVolume=>]
     */
    public static function getFilamentInfo($gCodeFile)
    {
        $file = popen("tac $gCodeFile", 'r');
        $linesRead = 0;
        $result = [];
        while ($line = fgets($file)) {
            $linesRead++;
            if($linesRead>200) {
                break;
            }
            if(strpos($line, 'filament used')) {
                preg_match("#filament used = (\d+.\d+)mm \((\d+.\d+)cm3\)#", $line, $matches);
                if(!empty($matches[1])) {
                    $result['filamentRequired'] = $matches[1];
                    $result['filamentVolume'] = $matches[2];
                }
            }
        }
        return $result;
    }
    /**
     * get material price based on material name and price cost to 1cm3
     * 
     * @param  type $material
     * @param  real $cost
     * @return type
     */
    public static function getFilamentPrice($material, $cost)
    {
        $materialTypes = [
            "ABS", "HDPE", "PLA", "PVA"
        ];
        $diameter = 1.75; // mm // Size Of Filament
        $density = 0.01;
        $length  = 100; // mm
        $cost = 34.99; // $ per length
        $material = $materialTypes[0];
        if($material == 'ABS') {
            $density = 0.00105;            
        }
        if($material == "HDPE") {            
            $density=0.00097;
        }                

        if($material == "PLA") {
            $density = 0.00125;            
        }
        if($material == "PVA") {
            $density = 0.00119;
        }
        $radius = $diameter/2;
        $area = $radius * $radius * pi();
        $weight = $area * $length * $density;
        
        $price = $cost / 1000 * $weight;
        return $price;
    }
    
    /**
     * 
     * @param array $data
     * @return string
     */
    public static function debug($data)
    {
        $result = sprintf("\nTotal filament: %.3f mm\n", $data['length']);
        $result = $result . sprintf("Volume: %.3f cc\n", $data['volume']);
        $result = $result . sprintf("Weight: %.3f g\n", $data['weight']);
        return $result;
    }
}
