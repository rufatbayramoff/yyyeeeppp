<?php namespace lib\d3;

use \common\components\ps\locator\Size;
/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Model3dTools
{

    public static function convert($filePath, $to = 'stl')
    {
        
    }

    /**
     * 
     * @param string $filePath
     * @return array
     */
    public static function info($filePath)
    {
        $result = \lib\d3\SlicerWrapper::init(false)->info($filePath);        
        $parsedResult = \lib\d3\SlicerWrapper::parseSlicerInfo($result);    
        return $parsedResult;
    }

    /**
     * get model3d file sizes (w,x,h)
     * 
     * @param string $filePath
     * @return Size
     */
    public static function sizes($filePath)
    {
        $parsedResult = self::info($filePath);
        return Size::create($parsedResult['width'], $parsedResult['height'], $parsedResult['length']);
    }
    
    /**
     * zoom file
     *  supports (.ply) Stereolithography (.stl) Wavefront geometry file (.obj)
     * 
     * @param string $filePath
     * @param int $zoomBy
     * @return string
     */
    public static function zoom($filePath, $zoomBy)
    {
        //ctmconv magnolia.stl magnolia.stl --scale 2
        $path = \Yii::getAlias('@console/runtime/logs');
        $cmd  = "ctmconv $filePath $filePath --scale $zoomBy";
        exec("$cmd 2> $path/tools.txt &", $output);
        \Yii::info($cmd, 'filejob'); 
        \Yii::info(\yii\helpers\VarDumper::dumpAsString($output), 'filejob');
        return $output;
    }

    public static function rotate($filePath, $rotateConfig)
    {
        
    }
}
