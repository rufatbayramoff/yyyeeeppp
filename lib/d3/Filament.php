<?php namespace lib\d3;

/**
 * Filament - simple class
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Filament
{

    /**
     *
     * @var type 
     */
    public $diameter;

    /**
     *
     * @var type 
     */
    public $density;

    /**
     *
     * @var type 
     */
    public $title;

    public function __construct($title, $diameter = 0, $density = 0)
    {
        $this->title = $title;
        $this->diameter = $diameter;
        $this->density = $density;
    }

    public function getDiameter()
    {
        return $this->diameter;
    }

    public function getDensity()
    {
        return $this->density;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setDiameter($diameter)
    {
        $this->diameter = $diameter;
    }

    public function setDensity($density)
    {
        $this->density = $density;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }
}
