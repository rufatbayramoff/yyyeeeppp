<?php
namespace lib\d3;

use yii\base\UserException;

/**
 * Slic3r wrapper
 * Usage example:  SlicerWrapper::init()
 * ->nozzleDiameter(0.3)
 * ->file($filePath)->run()
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class SlicerWrapper
{
    public $cmd = 'slic3r';
    public $args = [];
    private $debug = false;
    /**
     *
     * @var SlicerWrapper
     */
    private static $slicer;

    /**
     *
     * @param boolean $debug
     * @return SlicerWrapper
     */
    public static function init($debug = false)
    {
        if (self::$slicer) {
            self::$slicer->reset();
            self::$slicer->debug = $debug;
            return self::$slicer;
        }
        self::$slicer = new SlicerWrapper();
        self::$slicer->debug = $debug;
        return self::$slicer;
    }

    /**
     * reset args params
     */
    public function reset()
    {
        $this->args = [];
    }

    /**
     * repair STL file
     *
     * @param bool $file
     * @return array|SlicerWrapper
     */
    public function repair($file = false)
    {
        $this->args[] = '--repair';
        if ($file) {
            return $this->file($file)->run();
        }
        return $this;
    }

    /**
     * get file info
     *
     * @param bool $file
     * @return array|SlicerWrapper
     */
    public function info($file = false)
    {
        $this->args[] = '--info';
        if ($file) {
            return $this->file($file)->run(false);
        }
        return $this;
    }

    /**
     *
     * @param $d
     * @return SlicerWrapper
     */
    public function nozzleDiameter($d)
    {
        $this->args[] = '--nozzle-diameter ' . $d;
        return $this;
    }

    /**
     * Diameter
     *
     * @param  $d
     * @return SlicerWrapper
     */
    public function filamentDiameter($d)
    {
        $this->args[] = '--filament-diameter ' . $d;
        return $this;
    }

    /**
     * Infill density (range: 0-1, default: 0.2)
     *
     * @param  $d
     * @return SlicerWrapper
     */
    public function fillDensity($d)
    {
        $this->args[] = '--fill-density ' . $d;
        return $this;
    }

    /**
     * scale given model
     *
     * @param int $scale
     * @return array|SlicerWrapper
     */
    public function scale($scale, $file)
    {
        $this->args[] = '--scale ' . $scale;
        if ($file) {
            return $this->file($file)->run(false);
        }
        return $this;
    }

    /**
     *
     * @param $d
     * @return SlicerWrapper
     */
    public function solidInfillSpeed($d)
    {
        $this->args[] = "--solid-infill-speed " . $d;
        return $this;
    }

    /**
     *
     * @param $d
     * @return SlicerWrapper
     */
    public function layerHeight($d)
    {
        $this->args[] = '--layer-height ' . $d;
        return $this;
    }

    /**
     *
     * @param  $d
     * @return SlicerWrapper
     */
    public function perimeters($d)
    {
        $this->args[] = '--perimeters ' . $d;
        return $this;
    }

    /**
     * where to ouptput g-code file
     *
     * @param  $d
     * @return SlicerWrapper
     */
    public function output($d)
    {
        $this->args[] = '--output ' . $d;
        return $this;
    }

    /**
     * which file to slice
     *
     * @param  string $file - full path location
     * @return SlicerWrapper
     */
    public function file($file)
    {
        $this->args[] = $file;
        return $this;
    }

    /**
     * start slic3r program
     *
     * @return array
     */
    public function run($withSupport = true)
    {
        if ($withSupport) {
            $this->args[] = ' --support-material --support-material-spacing 2.5 --support-material-threshold 50';
        }
        $args = implode(' ', $this->args);
        if ($this->debug) {
            echo('Running ' . $this->cmd . ' ' . $args . "\n\r");
        }
        $path = \Yii::getAlias('@console/runtime/logs');
        exec($this->cmd . ' ' . $args . " 2> $path/slicerlog.txt &", $output);
        $this->args = [];
        return $output;
    }

    /**
     * parse slicer info
     *
     * @param array $result
     * @return array
     * @throws \yii\base\UserException
     */
    public static function parseSlicerInfo($result)
    {
        $matcher = [
            'size'        => 'size:              x=(\d+.\d+) y=(\d+.\d+) z=(\d+.\d+)',
            'faces'       => 'number of facets:  (\d+)',
            'vertices'    => 'number of shells:  (\d+)',
            'volume'      => 'volume:            (\d+.\d+)',
            'need_repair' => 'needed repair:     (.+)'
        ];
        $info = [];
        foreach ($result as $line) {
            $line = trim($line);
            foreach ($matcher as $key => $pattern) {
                $patternNeedle = explode(' ', $pattern)[0];
                if (strpos($line, $patternNeedle) === false) {
                    continue;
                }
                preg_match('#' . $pattern . '#', $line, $matches);
                if (isset($matches[1])) {
                    $info[$key] = $matches[1];
                    if ($key === 'size') {
                        $info[$key] = [$matches[1], $matches[2], $matches[3]];
                    }
                }
            }
        }
        if (empty($info['size']) || empty($info['volume'])) {
            throw new UserException(_t('site.app', 'Wrong STL file'));
        }
        $info['width'] = (float)$info['size'][0];
        $info['height'] = (float)$info['size'][1];
        $info['length'] = (float)$info['size'][2];
        if (empty($info['width']) || empty($info['height']) || empty($info['length'])) {
            throw new UserException(_t('site.app', 'Wrong STL file. Empty sizes.'));
        }
        return $info;
    }
}