<?php
/**
 * Date: 28.11.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\pdf;


class HtmlToPdf
{

    public static function convert($htmlFile, $options = [])
    {
        $pathinfo = pathinfo($htmlFile);
        $pdfFile = $pathinfo['filename'] . '.pdf';
        $dir = $pathinfo['dirname'];
        chdir($dir);
        if (!empty($options)) {
            $options = implode(' ', $options);
            $cmd = sprintf('xvfb-run --server-args="-screen 0, 1200x800x24" wkhtmltopdf --encoding utf-8 %s %s %s', $options, $pathinfo['basename'], $pdfFile);

        } else {
            $cmd = sprintf('xvfb-run --server-args="-screen 0, 1200x800x24" wkhtmltopdf --encoding utf-8 %s %s', $pathinfo['basename'], $pdfFile);
        }
        self::runCmd($cmd);
        return $dir . '/' . $pdfFile;

    }

    /**
     * run given command and check for results
     *
     * @param string $cmd
     * @return int
     * @throws \Exception
     */
    private static function runCmd($cmd)
    {
        exec($cmd, $result, $result2);
        if ($result2 == 1) {
            return $result;
        }
        if ($result2 > 0) {
            \Yii::info($cmd . ' result: ' . var_export($result2, true), 'filejob');
            $cmdName = explode(' ', $cmd)[0];
            if ($result2 === 127) {
                throw new \Exception('Command not found ' . $cmdName);
            }
            if ($result2 === 126) {
                throw new \Exception('Command invoked cannot execute ' . $cmdName);
            }
            throw new \Exception('Command error[' . $result2 . "]");
        }
        return $result;
    }

}