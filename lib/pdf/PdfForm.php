<?php namespace lib\pdf;

use common\components\FileDirHelper;

/**
 * PdfForm - form helps to fill PDF forms
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PdfForm
{
    /**
     * full path to pdf file
     * @var string 
     */
    private $pdfFile;

    /**
     * where folder located
     * @var string
     */
    private $pdfFolder;

    /**
     * filename without extension
     * @var string 
     */
    private $pdfName;
    private $userId;

    /**
     *
     * @param string $pdfFile
     * @param $userId
     */
    public function __construct($pdfFile, $userId)
    {
        $pathInf = pathinfo($pdfFile);
        $this->pdfFile = $pathInf['basename'];
        $this->pdfFolder = $pathInf['dirname'];
        $this->pdfName = $pathInf['filename'];
        $this->userId = $userId;
    }

    /**
     * get full path to pdf file
     * 
     * @return string
     */
    public function getPdfFile()
    {
        return $this->pdfFolder . '/' . $this->pdfFile;
    }

    /**
     * fill PDF form with data and return file to PDF
     * 
     * @param  array $userData
     * @return string
     */
    public function fillForm($userData)
    {
        $xdfdFile = $this->buildXfdf($userData);
        $pdfFile = $this->getPdfFile();
        FileDirHelper::createDir($this->pdfFolder . '/output/');
        $filledPdf = $this->pdfFolder . '/output/' . $this->pdfName . '_' . $this->userId . '.pdf';
        $cmd = sprintf("pdftk %s fill_form %s output %s", $pdfFile, $xdfdFile, $filledPdf);
        $this->runCmd($cmd);
        return $filledPdf;
    }

    /**
     * get fields file.
     * 
     * @return string
     */
    private function getFieldsFile()
    {
        FileDirHelper::createDir($this->pdfFolder . '/cache/');
        $fieldsFile = $this->pdfFolder . '/cache/' . $this->pdfName . '_fields.txt';
        $pdfFile = $this->getPdfFile();
        if (file_exists($fieldsFile)) {
            // check if fields file is newer then pdf file
            if (filemtime($pdfFile) < filemtime($fieldsFile)) {
                return $fieldsFile;
            }
        }
        $cmd = sprintf("pdftk %s dump_data_fields output %s", $pdfFile, $fieldsFile);
        $this->runCmd($cmd);
        return $fieldsFile;
    }

    /**
     * run given command and check for results
     * 
     * @param  string $cmd
     * @return int
     * @throws \Exception
     */
    private function runCmd($cmd)
    {
        exec($cmd, $result, $result2);
        if ($result2 > 0) {
            \Yii::info($cmd . ' result: ' . var_export($result2, true), 'filejob');
            if ($result2 == 127) {
                $cmdName = explode(" ", $cmd)[0];
                throw new \Exception("Command not found " . $cmdName);
            }
            if ($result2 == 126) {
                throw new \Exception("Command invoked cannot execute " . $cmdName);
            }
            throw new \Exception("Command error[" . $result2 . "]");
        }
        return $result;
    }

    /**
     * map data fields
     * 
     * @param  array $userData
     * @param  array $fields
     * @return array
     */
    private function mapDataFields($userData, $fields)
    {
        $userDataKey = [];

        foreach ($userData as $k => $v) {
            $userDataKey[] = ['key' => $k, 'value' => $v];
        }
        foreach ($fields as $k => $v) {
            if (isset($userDataKey[$k])) {
                $v['value'] = $userDataKey[$k]['value'];
                $fields[$k] = $v;
            }
        }
        return $fields;
    }

    /**
     * build XFDF file and return created file path
     *
     * @param $userData
     * @return string
     * @throws \yii\base\ErrorException
     */
    private function buildXfdf($userData)
    {
        $data = file_get_contents($this->getFieldsFile());
        $fields = $this->parseFields($data);
        $realData = $this->mapDataFields($userData, $fields);
        $xml = $this->generateXml($realData);

        FileDirHelper::createDir($this->pdfFolder . '/cache/');
        $fileName = $this->pdfFolder . '/cache/' . $this->pdfName . '_' . $this->userId . '.xfdf';
        file_put_contents($fileName, $xml);
        return $fileName;
    }

    /**
     * get fields from data,
     * returns assoc array ['propertyName'=>'value']
     * 
     * @param  string $data
     * @return array
     */
    private function parseFields($data)
    {
        $fields = explode("---", $data);
        $result = [];
        foreach ($fields as $field) {
            if (empty($field)) {
                continue;
            }
            preg_match_all("#(\w+): (.+)?#", $field, $matches);
            $row = array_combine($matches[1], $matches[2]);
            $result[] = $row;
        }
        return $result;
    }

    /**
     * generate XML file with data to submit form
     * 
     * @param  array $data
     * @return string
     */
    private function generateXml($data)
    {
        $header = '<' . '?xml version="1.0" encoding="UTF-8"?' . '><xfdf xml:space="preserve">';
        $body = [];
        $body[] = $header;
        $body[] = '<fields>';
        foreach ($data as $row) {
            $name = $row['FieldName'];
            $type = $row['FieldType'];
            $value = isset($row['value']) ? $row['value'] : '';
            $body[] = "\t" . '<field name="' . $name . '">';
            if ($type == 'Button') {
                if (!empty($value)) {
                    $body[] = "\t\t" . "<value>" . $value . "</value>";
                } else {
                    $body[] = "\t\t" . "<value>No</value>";
                }
            } else {
                $body[] = "\t\t" . "<value>" . $value . "</value>";
            }
            $body[] = "\t" . '</field>';
        }
        $body[] = '</fields>';
        $body[] = '</xfdf>';
        $result = implode("\n", $body);
        return $result;
    }

    /**
     * generate PDF html files for console operation
     * 
     * @param  string  $folderPrefix
     * @param  boolean $force
     * @return string
     */
    public function generateHtml($folderPrefix = 'html', $force = false)
    {
        $pdfFile = $this->getPdfFile();
        $htmlFile = $this->pdfFolder . '/' . $folderPrefix . '/' . $this->pdfName . '.html';
        if ($force == false && file_exists($htmlFile)) {
            if (filemtime($pdfFile) < filemtime($htmlFile)) {
                echo "\n file exists $htmlFile \n";
                return $htmlFile;
            }
        }
        $cmd = sprintf("pdftohtml -c -q -noframes %s %s", $pdfFile, $htmlFile);
        return $this->runCmd($cmd);
    }
}
