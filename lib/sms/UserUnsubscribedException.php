<?php
/**
 * Created by mitaichik
 */

namespace lib\sms;

/**
 * Class UserUnsubscribedException
 *
 * This exception throw when user ubsubscribed from our sms throught sms gateway
 *
 * @package lib\sms
 */
class UserUnsubscribedException extends SmsException
{

}