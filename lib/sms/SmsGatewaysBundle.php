<?php
/**
 * Created by mitaichik
 */

namespace lib\sms;


use common\components\exceptions\AssertHelper;
use yii\base\Component;

/**
 * Class SmsGatewaysBundle
 * @package lib\sms
 */
class SmsGatewaysBundle extends Component
{
    /**
     * @var SmsGatewayInterface[]
     */
    private $gateways = [];

    /**
     * Return gateway by name
     * @param string $name
     * @return SmsGatewayInterface
     * @throws SmsException
     */
    public function getGatewayByName($name)
    {
        if(!isset($this->gateways[$name])){
            throw new SmsException("Gateway {$name} not found");
        }
        return $this->gateways[$name];
    }

    /**
     * @return string[]
     */
    public function getGatewaysNames()
    {
        return array_keys($this->gateways);
    }

    /**
     * @param array $gateways
     * @throws SmsException
     */
    public function setGateways(array $gateways)
    {
        foreach ($gateways as $gatewayName => $gatewayConfig){
            $gateway = \Yii::createObject($gatewayConfig);
            AssertHelper::assertInstance($gateway, SmsGatewayInterface::class, "Bad gateway config", SmsException::class);
            $this->gateways[$gatewayName] = $gateway;
        }
    }
}