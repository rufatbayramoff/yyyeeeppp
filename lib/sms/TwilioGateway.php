<?php
/**
 * Created by mitaichik
 */

namespace lib\sms;


use common\components\exceptions\AssertHelper;
use Services_Twilio;
use Services_Twilio_RestException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class TwilioGateway
 * @package lib\sms
 */
class TwilioGateway extends Component implements SmsGatewayInterface
{
    /**
     * Twilio account SID
     * @var string
     */
    public $accountId;

    /**
     * Twillio auth token
     * @var string
     */
    public $authToken;

    /**
     * Twilio outbox number
     * @var string
     */
    public $outboxNumber;

    /**
     * @var Client
     */
    private $service;

    /**
     * Init
     */
    public function init()
    {
        AssertHelper::assert($this->accountId, 'Twilio account not defined.', InvalidConfigException::class);
        AssertHelper::assert($this->authToken, 'Twilio auth token not defined.', InvalidConfigException::class);
        AssertHelper::assert($this->outboxNumber, 'Twilio outbox number not defined.', InvalidConfigException::class);
    }

    /**
     * Return service
     * @return Client
     * @throws \Twilio\Exceptions\ConfigurationException
     */
    private function getService()
    {
        if(!$this->service){
            $this->service = new Client($this->accountId, $this->authToken);
        }
        return $this->service;
    }

    /**
     * @param string $phoneNumber
     * @param string $message
     * @param int $ref
     * @return void
     * @throws UserUnsubscribedException
     * @throws \Twilio\Exceptions\ConfigurationException
     * @throws \Twilio\Exceptions\TwilioException
     */
    public function sendMessage($phoneNumber, $message, $ref)
    {
        try {
            $this->getService()->messages->create(
                to:$phoneNumber,
                options: [
                    "from" => $this->outboxNumber,
                    "body" => $message,
                ]);
        }
        catch (TwilioException $e) {
            if (str_contains($e->getMessage(), 'From/To pair violates a blacklist rule')) {
                throw new UserUnsubscribedException();
            }
            throw $e;
        }
    }

    public function call($to){
        $this->getService()->calls->create(
            to:$to,
            from:$this->outboxNumber,
            options: ['url' => "https://api.treatstock.com/twillio/call"]
        );
    }
}