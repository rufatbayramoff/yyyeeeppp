<?php namespace lib\sms;

/**
 * CMSMS
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class CMSMS implements SmsGatewayInterface
{
    /**
     * @param string $phoneNumber
     * @param string $message
     * @param int $ref
     * @return bool
     * @throws SmsException
     */
    public function sendMessage($phoneNumber, $message, $ref)
    {
        $phoneNumber = str_replace("+", "00", $phoneNumber);
        $xml = $this->buildMessageXml($phoneNumber, $message, $ref);

        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => 'https://sgw01.cm.nl/gateway.ashx',
            CURLOPT_HTTPHEADER => ['Content-Type: application/xml'],
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_RETURNTRANSFER => true
        ]);
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode==200 && !empty($response)) {
            throw new SmsException($response);
        }
        return empty($response);
    }
    
    /**
     * @param string $recipient
     * @param string $message
     * @param string $ref       - id from user_sms table
     * @return string
     */
    private function buildMessageXml($recipient, $message, $ref = '')
    {
        $xml = new \SimpleXMLElement('<MESSAGES/>');
        $message = htmlspecialchars($message);
        $authentication = $xml->addChild('AUTHENTICATION');
        $authentication->addChild('PRODUCTTOKEN', param('cmsms_token'));
        
        $msg = $xml->addChild('MSG');
        $msg->addChild('FROM', param('sms_from'));
        $msg->addChild('TO', $recipient);
        $msg->addChild('BODY', $message);
        $msg->addChild('MINIMUMNUMBEROFMESSAGEPARTS', 1);
        $msg->addChild('MAXIMUMNUMBEROFMESSAGEPARTS', 8);

        $msg->addChild('REFERENCE', $ref);
        \Yii::info("Sending SMS: <$recipient> <$message>", 'smsapi');
        return $xml->asXML();
    }
}
