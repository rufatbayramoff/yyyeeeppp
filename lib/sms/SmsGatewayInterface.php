<?php namespace lib\sms;

/**
 * Description of SmsGateway
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
interface SmsGatewayInterface
{
    /**
     * Send message
     * @param string $phoneNumber Phone number in E164 format
     * @param string $message Sms message
     * @param int $ref Reference id (message id on our system)
     * @return boolean
     */
    public function sendMessage($phoneNumber, $message, $ref);
}
