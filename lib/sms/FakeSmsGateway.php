<?php
namespace lib\sms;

/**
 * FakeSmsGateway - to use for tests
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class FakeSmsGateway implements SmsGatewayInterface
{
    /**
     * @param string $phoneNumber
     * @param string $message
     * @param string $ref
     * @return boolean
     */
    public function sendMessage($phoneNumber, $message, $ref)
    {
        \Yii::info(implode("\n", [$phoneNumber, $message, $ref]), 'sms');
        return true;
    }
}
