<?php namespace lib;
use common\components\exceptions\AssertHelper;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class MeasurementUtil
{
    const GRAM = 'gr';
    const OUNCE = 'oz';
    const POUND = 'lb';
    const MM = 'mm';
    const CM = 'cm';
    const IN = 'in';
    const UNKOWN = 'unkown'; // used by measurement detector, if cannot be sure
    const ML = 'ml';
    const OUNCE_TO_GRAM = 28.35;
    const POUND_TO_GRAM = 453.59;
    const MILLIMETERS_TO_INCHES = 25.4;
    const MM_TO_CM = 10;
    const CM_TO_INCHES = 2.54;
    const INCH_TO_MM = 0.039370079;

    /**
     * km in miles
     */
    const KM_IN_MILE = 1.609344;
    
    /**
     * miles in km
     */
    const MILE_IN_KM = 0.621371192;


    /**
     * Coefficients for converting weight
     * @var array
     */
    private static $convertWeightCoefficients = [
        self::GRAM => [
            self::POUND => 1 / self::POUND_TO_GRAM,
            self::OUNCE => 1 / self::OUNCE_TO_GRAM,
        ],
        self::POUND => [
            self::GRAM => self::POUND_TO_GRAM,
            self::OUNCE => self::POUND_TO_GRAM / self::OUNCE_TO_GRAM,
        ],
        self::OUNCE => [
            self::GRAM => self::OUNCE_TO_GRAM,
            self::POUND => self::OUNCE_TO_GRAM / self::POUND_TO_GRAM,
        ],
    ];

    private static $convertSizeCoefficients = [
        self::MM => [
            self::IN => 1 / self::MILLIMETERS_TO_INCHES,
            self::CM => 1 / 10,
        ],
        self::CM => [
            self::IN => 1 / self::CM_TO_INCHES,
            self::MM => 10,
        ],
        self::IN => [
            self::MM => self::MILLIMETERS_TO_INCHES,
            self::CM => self::CM_TO_INCHES,
        ],
    ];

    /**
     * 
     * @param string $measurement
     * @return float
     */
    public static function convertGramToOunce($measurement)
    {
        return $measurement / self::OUNCE_TO_GRAM;
    }

    /**
     * 
     * @param string $measurement
     * @return float
     */
    public static function convertOunceToGram($measurement)
    {
        return $measurement * self::OUNCE_TO_GRAM;
    }

    /**
     * 
     * @param string $measurement
     * @return float
     */
    public static function convertPoundToGram($measurement)
    {
        return $measurement / self::POUND_TO_GRAM;
    }

    /**
     * 
     * @param string $measurement
     * @return float
     */
    public static function convertGramToPound($measurement)
    {
        return $measurement * self::POUND_TO_GRAM;
    }
    /**
     *
     * @param $measurement
     * @return float
     */
    public static function convertMillimetersToInches($measurement)
    {
        return $measurement / self::MILLIMETERS_TO_INCHES;
    }
    
    public static function convertInchesToMm($measurement)
    {
        return $measurement * self::MILLIMETERS_TO_INCHES;
    }
    
     /**
     * 
     * @param float $miles
     * @return float
     */
    public static function milesToKm($miles)
    {
        return $miles * self::KM_IN_MILE;
    }
    
    /**
     * 
     * @param float $km
     * @return float
     */
    public static function kmToMiles($km)
    {
        return $km * self::MILE_IN_KM;
    }


    /**
     * Convert weight from $fromMeasure to $toMeasure
     * @param float $value
     * @param string $fromMeasure
     * @param string $toMeasure
     * @param bool $round Round result to 0.01
     * @return float
     */
    public static function convertWeight($value, $fromMeasure, $toMeasure, $round = false)
    {
        if($fromMeasure == $toMeasure){
            return $round ? round($value, 2) : $value;
        }
        AssertHelper::assert(isset(self::$convertWeightCoefficients[$fromMeasure][$toMeasure]), "Unknown convert measure {$fromMeasure} {$toMeasure}");
        $result = $value * self::$convertWeightCoefficients[$fromMeasure][$toMeasure];
        return $round ? round($result, 2) : $result;
    }

    public static function convertSize($value, $fromMeasure, $toMeasure, $round = false)
    {
        if($fromMeasure === $toMeasure){
            return $round ? round($value, 2) : $value;
        }
        AssertHelper::assert(isset(self::$convertSizeCoefficients[$fromMeasure][$toMeasure]), "Unknown convert measure {$fromMeasure} {$toMeasure}");
        $result = $value * self::$convertSizeCoefficients[$fromMeasure][$toMeasure];
        return $round ? round($result, 2) : $result;
    }
}
