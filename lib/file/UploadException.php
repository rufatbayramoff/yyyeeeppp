<?php

namespace lib\file;

/**
 * UploadException to be thrown if error with uploading
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UploadException extends \Exception
{
    
}
