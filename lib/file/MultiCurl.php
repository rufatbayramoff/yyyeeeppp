<?php
/**
 * Date: 27.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\file;

/**
 * Class MultiCurl
 * rewritten from source: https://github.com/petewarden/ParallelCurl
 *
 *  usage
 * $mCurl = new MultiCurl(10);
 * $mCurl->startRequest('http://example.com', 'onRequestDone', ['something']);
 *
 *
 * $mCurl->finishAllRequests();
 *
 * @package lib\file
 */
class MultiCurl
{
    public $maxRequests;
    public $options;

    public $outstandingRequests;
    public $multiCurlHandle;
    public $passContent = false;
    public function __construct($maxRequests = 10, $options = [])
    {
        $this->maxRequests = $maxRequests;
        $this->options = $options;
        $this->outstandingRequests = [];
        $this->multiCurlHandle = curl_multi_init();
    }

    /**
     *
     */
    public function __destruct()
    {
        $this->finishAllRequests();
    }

    /**
     * @param $max
     */
    public function setMaxRequests($max)
    {
        $this->maxRequests = $max;
    }

    /**
     * Sets the options to pass to curl, using the format of curl_setopt_array()
     *
     * @param $options
     */
    public function setOptions($options)
    {

        $this->options = $options;
    }

    /**
     * Start a fetch from the $url address, calling the $callback function passing the optional
     * $user_data value. The callback should accept 3 arguments, the url, curl handle and user
     * data, eg on_request_done($url, $ch, $user_data);
     *
     * @param $url
     * @param $callback
     * @param array $userData
     * @param null $postFields
     */
    public function startRequest($url, $callback, $userData = [], $postFields = null, $fp = null)
    {

        if ($this->maxRequests > 0) {
            $this->waitForOutstandingRequestsToDropBelow($this->maxRequests);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt_array($ch, $this->options);
        if($fp){
            $this->passContent = false;
            curl_setopt($ch, CURLOPT_FILE, $fp);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($postFields) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        }
        curl_multi_add_handle($this->multiCurlHandle, $ch);
        $key = (int)$ch;
        $this->outstandingRequests[$key] = [
            'url'       => $url,
            'callback'  => $callback,
            'user_data' => $userData,
        ];

        $this->checkForCompletedRequests();
    }

    /**
     * You *MUST* call this function at the end of your script. It waits for any running requests
     * to complete, and calls their callback functions
     */
    public function finishAllRequests()
    {
        $this->waitForOutstandingRequestsToDropBelow(1);
    }

    /**
     * Checks to see if any of the outstanding requests have finished
     */
    private function checkForCompletedRequests()
    {
        do {
            $mrc = curl_multi_exec($this->multiCurlHandle, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($this->multiCurlHandle) != -1) {
                do {
                    $mrc = curl_multi_exec($this->multiCurlHandle, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);
            } else {
                return;
            }
        }

        // Now grab the information about the completed requests
        while ($info = curl_multi_info_read($this->multiCurlHandle)) {
            $ch = $info['handle'];
            $ch_array_key = (int)$ch;
            if (!isset($this->outstandingRequests[$ch_array_key])) {
                \Yii::info('curl' . print_r($this->outstandingRequests, true));
                return;
            }
            $request = $this->outstandingRequests[$ch_array_key];
            $url = $request['url'];
            $content = $this->passContent ? curl_multi_getcontent($ch) : '';
            $callback = $request['callback'];
            $userData = $request['user_data'];
            call_user_func($callback, $content, $url, $ch, $userData);
            unset($this->outstandingRequests[$ch_array_key]);
            curl_multi_remove_handle($this->multiCurlHandle, $ch);
        }

    }

    /**
     * @param $max
     */
    private function waitForOutstandingRequestsToDropBelow($max)
    {
        while (1) {
            $this->checkForCompletedRequests();
            if (count($this->outstandingRequests) < $max) {
                break;
            }
            usleep(10000);
        }
    }
}