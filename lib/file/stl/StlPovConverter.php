<?php namespace lib\file\stl;

/**
 * StlPovConverter - extends stl pov converter and adds new tpl var modelColor
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class StlPovConverter extends \Libre3d\Render3d\Convert\StlPov
{

    protected function generatePov($tplVars)
    {
        $tplVars['modelColor'] = "Blue";
        return parent::generatePov($tplVars);
    }
}
