<?php namespace lib\file\stl;

/**
 * based on STL Volume Weight Calculator v1.1
 * original author Pushkar Paranjpe
 * 
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class STLStats
{

    private $volume;
    //ABS plastic : 1.04gm/cc
    private $density = 1.04; //Default material density.
    
   	private $bbox = array("length"=>0,"width"=>0,"height"=>0);
    
    private $trianglesCount;
    private $trianglesData;
    private $bBinary;
   
    private $points;
    private $stlSource;
    private $stlPath;
    private $triangles;
    private $flag = false;
    private $scanned = false;
    
    private $measure = 'inch';
    const UNIT_SM = 'sm';
    const UNIT_INCH = 'inch';
    
    private $minx = INF;
	private $maxx = -1E100000000;	 
	private $miny = INF;
	private $maxy = -1E100000000; 
	private $minz = INF;
	private $maxz = -1E100000000;	//replace by -INF if allowed by local PHP installation
    
    
    /**
     * set file to work with
     * 
     * @param type $stlFile
     * @return \lib\file\STLStats
     */
    public function setFile($stlFile)
    {
        $b = $this->isAscii($stlFile);
        if (!$b) {
            $this->bBinary = TRUE;
            $this->stlSource = fopen($stlFile, "rb");   
            $this->stlPath = $stlFile;
        }
        $this->triangles = array();
        return $this;
    }
    /**
     * set measure system
     * 
     * @param type $measure
     * @return \lib\file\stl\STLStats
     */
    public function setMeasureSystem($measure)
    {
        if(self::UNIT_SM==$measure){
            $this->measure = self::UNIT_SM;
        }else{
            $this->measure = self::UNIT_INCH;
        }
        return $this;
    }
    /**
     * get summary info about file
     * 
     * @return array
     */
    public function getInfo()
    {
        $vol = $this->getVolume();
        $weight = $this->getWeight();
        $polCount = $this->getTrianglesCount();
        $box = $this->getBBox();
        $data = [
            'volume' => round($vol, 4),
            'weight' => round($weight, 4),
            'polygons' => (int) $polCount,
            'width' => round($box['width'], 4),
            'height' => round($box['height'], 4),
            'length' => round($box['length'], 4),
        ];
        return $data;
    }
    /**
     * get bounding box length x width x height
     * 
     * @return type
     */
    public function getBBox()
    {
    	$measure = $this->measure;
    	$unitmap = array("sm"=>array($this, "mmtocm"),"inch"=>array($this, "mmtoinch"));
    	if(!$this->volume){  
            $this->volume = $this->calculateVolume();
        }
    	$this->bbox["length"] = $this->maxx - $this->minx;
		$this->bbox["width"] = $this->maxy - $this->miny;
		$this->bbox["height"] = $this->maxz - $this->minz;
    	return array_map($unitmap[$measure],$this->bbox);
    }
    
    /**
     * 
     * @param type $measure
     * @return int
     */
    public function getVolume()
    {
        if (!$this->volume) { 
            $this->volume =  $this->calculateVolume(); 
        }
        $volume = 0;
        if ($this->measure == self::UNIT_SM) {
            $volume = ($this->volume / 1000);
        } else {
            $volume = $this->inch3($this->volume / 1000);
        }

        return $volume;
    }

    /**
     * get weight
     * 
     * @return type
     */
    public function getWeight()
    {
        $volume = $this->getVolume($this->measure);
        $weight = $this->calculateWeight($volume);
        return $weight;
    }
    /**
     * Returns the set Density (gm/cc) of the material.
     * 
     * @return double
     */
    public function getDensity()
    {
        return $this->density;
    }
    
    /**
     * Sets the Density (gm/cc) of the material.
     * 
     * @param double $den
     * @return $this
     */
    public function setDensity($den)
    {
        $this->density = $den;
        return $this;
    }
    /**
     * Returns the number of trianges specified in the binary STL definition of the 3D object.
     * 
     * @return int
     */
    public function getTrianglesCount()
    {
        $tcount = $this->trianglesCount;
        return $tcount;
    }


    /*
     * Invokes the binary file reader to read the header,
     * serially reads all the normal vector and triangular co-ordinates,
     * calls the math function to calculate signed tetrahedral volumes for each trangle,
     * sums up these volumes to give the final volume of the 3D object represented in the .stl binary file.
     * 
     * @return int
     */
    private function calculateVolume()
    {
        $totalVolume = 0;
        if ($this->bBinary) {
            $totbytes = filesize($this->stlPath);
            try {
                $this->readHeader();
                $this->trianglesCount = $this->readTrianglesCount();
                try {
                    while (ftell($this->stlSource) < $totbytes) {
                        $totalVolume += $this->readTriangle();
                    }
                } catch (Exception $e) {
                    return $e;
                }
            } catch (Exception $e) {
                return $e;
            }
            fclose($this->stlSource);
        } else {
            $k = 0;
            while (sizeof($this->trianglesData[4]) > 0) {
                $totalVolume += $this->readTriangleAscii();
                $k += 1;
            }
            $this->trianglesCount = $k;
        }

        return abs($totalVolume);
    } 
    
    /**
     * 
     * Wrapper around PHP's unpack() function which decodes binary numerical data to float, int, etc types.
     * $sig specifies the type of data (i.e. integer, float, etc)
     * $l specifies number of bytes to read.
     * 
     * @param type $sig
     * @param type $l
     * @return type
     */
    private function myUnpack($sig, $l)
    {
        $s = fread($this->stlSource, $l);
        $stuff = unpack($sig, $s);
        return $stuff;
    }
    
    /**
     * Appends to an array either a single var or the contents of another array.
     * 
     * @param type $myarr
     * @param type $mystuff
     * @return type
     */
    private function myAppend($myarr, $mystuff)
    {

        if (is_array($mystuff)) {
            $myarr = array_merge($myarr, $mystuff);
        } else {
            $ctr = sizeof($myarr);
            $myarr[$ctr] = $mystuff;
        }
        return $myarr;
    }

    /**
     * Reads the binary header field in the STL file and offsets the file reader pointer to
     * enable reading the triangle-normal data.
     */
    private function readHeader()
    {
        fseek($this->stlSource, ftell($this->stlSource) + 80);
    }
    /**
     * Reads the binary field in the STL file which specifies the total number of triangles
     * and returns that integer.
     * 
     * @return int
     */
    private function readTrianglesCount()
    {
        $length = $this->myUnpack("I", 4);
        return $length[1];
    }
    
    /**
     * Reads a triangle data from the binary STL and returns its signed volume.
     * A binary STL is a representation of a 3D object as a collection of triangles and their normal vectors.
     * Its specifiction can be found here:
     * http://en.wikipedia.org/wiki/STL_(file_format)%23Binary_STL
     * This function reads the bytes of the binary STL file, decodes the data to give float XYZ co-ordinates of the trinaglular
     * vertices and the normal vector for a triangle.
     * 
     * @return type
     */
    private function readTriangle()
    {
        $n = $this->myUnpack("f3", 12);
        $p1 = $this->myUnpack("f3", 12);
        $p2 = $this->myUnpack("f3", 12);
        $p3 = $this->myUnpack("f3", 12);
        $b = $this->myUnpack("v", 2);
        
        $this->seekBounds($p1,$p2,$p3);
        $l = sizeof($this->points);
        $this->myAppend($this->triangles, array($l, $l + 1, $l + 2));
        return $this->signedVolumeOfTriangle($p1, $p2, $p3);
    }

    /**
     * Reads a triangle data from the ascii STL and returns its signed volume.
     * 
     * @return type
     */
    private function readTriangleAscii()
    {
        $p1[1] = floatval(array_pop($this->trianglesData[4]));
        $p1[2] = floatval(array_pop($this->trianglesData[5]));
        $p1[3] = floatval(array_pop($this->trianglesData[6]));

        $p2[1] = floatval(array_pop($this->trianglesData[7]));
        $p2[2] = floatval(array_pop($this->trianglesData[8]));
        $p2[3] = floatval(array_pop($this->trianglesData[9]));

        $p3[1] = floatval(array_pop($this->trianglesData[10]));
        $p3[2] = floatval(array_pop($this->trianglesData[11]));
        $p3[3] = floatval(array_pop($this->trianglesData[12]));

        $this->seekBounds($p1,$p2,$p3);
        $l = sizeof($this->points);
        $this->myAppend($this->triangles, array($l, $l + 1, $l + 2));
        return $this->signedVolumeOfTriangle($p1, $p2, $p3);
    }
    
    /**
     * 
     * @param type $p1
     * @param type $p2
     * @param type $p3
     */
    private function seekBounds($p1, $p2, $p3){
	    $lminx = min($p1[1],$p2[1],$p3[1]);
	    $lmaxx = max($p1[1],$p2[1],$p3[1]);
	    
	    if($lminx < $this->minx){
	    	$this->minx = $lminx;
	    }
	    if($lmaxx > $this->maxx){
	    	$this->maxx = $lmaxx;
	    }

	    //y
	    $lminy = min($p1[2],$p2[2],$p3[2]);
	    $lmaxy = max($p1[2],$p2[2],$p3[2]);

	    if($lminy < $this->miny){
	    	$this->miny = $lminy;
	    }
	    if($lmaxy > $this->maxy){
	    	$this->maxy = $lmaxy;
	    }
	    
	    //z
	    $lminz = min($p1[3],$p2[3],$p3[3]);
	    $lmaxz = max($p1[3],$p2[3],$p3[3]);
	    
	    if($lminz < $this->minz){
	    	$this->minz = $lminz;
	    }
	    if($lmaxz > $this->maxz){
	    	$this->maxz = $lmaxz;
	    }
    }
    
    /**
     * 
     * Checks if the given file is an ASCII file.
     * Populates the triangles_data array if TRUE.
     * 
     * @param type $infilename
     * @return boolean
     */
    private function isAscii($infilename)
    {
        $b = FALSE;
        $facePattern = '/facet\\s+normal\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+'
            . 'outer\\s+loop\\s+'
            . 'vertex\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+'
            . 'vertex\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+'
            . 'vertex\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+([-+]?\\b(?:[0-9]*\\.)?[0-9]+(?:[eE][-+]?[0-9]+)?\\b)\\s+'
            . 'endloop\\s+' . 'endfacet/';
        $fdata = file_get_contents($infilename);
        preg_match_all($facePattern, $fdata, $matches);
        if (sizeof($matches[0]) > 0) {
            $b = TRUE;
            $this->trianglesData = $matches;
        }
        return $b;
    }

    /**
     * 
     * Returns the signed volume of a triangle as determined by its 3D, XYZ co-ordinates.
     * The var $pn contains an array(x,y,z).
     * 
     * @param type $p1
     * @param type $p2
     * @param type $p3
     * @return type
     */
    private function signedVolumeOfTriangle($p1, $p2, $p3)
    {
        $v321 = $p3[1] * $p2[2] * $p1[3];
        $v231 = $p2[1] * $p3[2] * $p1[3];
        $v312 = $p3[1] * $p1[2] * $p2[3];
        $v132 = $p1[1] * $p3[2] * $p2[3];
        $v213 = $p2[1] * $p1[2] * $p3[3];
        $v123 = $p1[1] * $p2[2] * $p3[3];
        return (1.0 / 6.0) * (-$v321 + $v231 + $v312 - $v132 - $v213 + $v123);
    }
    
    /**
     * Converts the volume specified in cubic cm to cubic inches.
     * 
     * @param type $v
     * @return type
     */
    private function inch3($v)
    {
        return $v * 0.0610237441;
    }
    
    /**
     * Calculates the weight of the supplied volume specified in cubic cm and returns it in gms.
     * 
     * @param type $volume
     * @return double
     */
    private function calculateWeight($volume)
    {
        return $volume * $this->density;
    }
    
    /**
     * Converts the volume specified in cubic cm to cubic inches.
     * 
     * @param type $v
     * @return type
     */
    private function cm3toinch3($v){
	    return $v*0.0610237441;
    }
    
    /**
     * Converts the value specified in mm to cm
     * 
     * @param type $v
     * @return type
     */
    private function mmtocm($v){
	    return $v*0.1;
    }
    
    /**
     * Converts the value specified in mm to inches
     * @param type $v
     * @return type
     */
    private function mmtoinch($v){
	    return $v*0.0393701;
    }
}