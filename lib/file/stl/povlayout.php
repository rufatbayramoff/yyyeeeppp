
#include "math.inc"
#include "finish.inc"
#include "transforms.inc"
#include "colors.inc"
#include "textures.inc"
#include "functions.inc"
#include "<?php echo $includeFile; ?>" 

background {color rgb 1}
 
global_settings {
  assumed_gamma 2 
} 

light_source { 
    <-310, -310, 340> color rgb <1,1,1,0.2> shadowless
}
light_source { 
    <350, 350, 340> color rgb <1,1,1,0.2>  
} 

camera {
	//orthographic
	sky <0,0,1>
    //location <(240),(250),(260)>
	location <(<?php echo $x*3; ?>),(<?php echo $y*3; ?>),(<?php echo $z*$zMult*2; ?>)>
	look_at <0,0,(<?php echo $z*0.45; ?>)>
	angle 35
    right -1.33*x
	rotate <0,0,clock*360>
}



object {
  <?php echo $modelname; ?>
  Center_Trans(<?php echo $modelname; ?>, x+y)
    Align_Trans(<?php echo $modelname; ?>, -z, <0,0,0>)
    
    texture{ 
        pigment{ <?php echo isset($options['modelColor']) ? $options['modelColor'] : param('render_color', 'White'); ?> }
        /* finish {  phong 0.71 phong_size 40 } */
        finish { phong 0.71 phong_size 40 ambient .3 diffuse .6 }
    }
}
 
box {
    <-110, -110, -10>,   // Far upper right corner
    < 110, 110, 0 >  // Near lower left corner
    
    texture {  
      Silver3         // Pre-defined from stones.inc // /usr/share/povray-3.7/include 
      finish {
          ambient 0.8
          diffuse 0.7
          reflection .1 
          roughness .001
       }
    }   
    
}   