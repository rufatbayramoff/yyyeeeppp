<?php namespace lib\file\stl;

use Libre3d\Render3d\Render3d;
/**
 * Render stl file to PNG using openscad and povray
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Render
{
    private $fileName;
    private $workingDir;
    /**
     * 
     */
    public function __construct()
    {
        $this->workingDir = dirname(__DIR__) . '/ts/frontend/runtime/';
        $this->fileName = '/home/user/Documents/3D/3d1_0.stl';
    }
    /**
     * test render with default file and working dir
     */
    public function testRender()
    {                
        $render3d = new Render3d();
        $render3d->options(['buffer'=>  Render3d::BUFFER_OFF]);
        // this is the working directory, where it will put any files used during the render process, as well as the final
        // rendered image.
        $render3d->workingDir();

        // Set paths to the executables on this system
        $render3d->executable('openscad', '/usr/bin/openscad');
        $render3d->executable('povray', '/usr/bin/povray');

        try {
            // This will copy in your starting file into the working DIR if you give the full path to the starting file.
            // This will also set the fileType for you.
            $render3d->filename($this->fileName);
            // Render!  This will do all the necessary conversions as long as the render engine (in this
            // case, the default engine, PovRAY) "knows" how to convert the file into a file it can use for rendering.
            // Note that this is a multi-step process that can be further broken down if you need it to.
            $renderedImagePath = $render3d->render('povray');

            echo "Render successful!  Rendered image will be at $renderedImagePath";
        } catch (Exception $e) {
            echo "Render failed :( Exception: ".$e->getMessage();
        }
    }
}
