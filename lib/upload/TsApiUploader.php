<?php
namespace lib\upload;
/**
 * TsApiUploader
 * uploader which uses own api to upload to static server
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class TsApiUploader extends \yii\base\BaseObject
{
    
    public $url = 'http://127.0.0.1:8003/api';
    private $curl;
    
    public function init()
    {
        $this->curl = curl_init();
    }
    
    /**
     * multi file upload
     * 
     * @param  type $configs
     * @return type
     */
    public function uploadMulti($configs)
    {
        $toPost = [];
        $toPost['total'] = count($configs);
        foreach($configs as $k=>$config){
            $config['file'] =  curl_file_create($config['tempName']);            
            unset($config['tempName']);
            foreach($config as $k2=>$v){
                $toPost['files_'.$k.'_'.$k2] = $v;
            }
        }
        $json = self::apiRequest('multi-upload', $toPost);  
        return $json;
    }
    /**
     * 
     * @param array $config
     * @return type
     */
    public function upload($config)
    {
        $config['file'] =  curl_file_create($config['tempName']);
        unset($config['tempName']);
        $json = self::apiRequest('upload', $config);
        return $json;
    }
    
    public function fileExists($filePath)
    {
        $json = self::apiRequest('hasfile', ['filePath'=>$filePath]);  
        return $json;
    }
    
    /**
     * 
     * @param type $dir
     * @return type
     */
    public function scandir($dir)
    {
        $json = self::apiRequest('scandir', ['dir'=>$dir]);
        return $json;
    }
    
    /**
     * make API request to remote server using CURL
     * 
     * @param  string $action - API action
     * @param  array  $data   - data to send
     * @return array
     */
    private function apiRequest($action, $data)
    {
        $curl = $this->curl;
        try{
            curl_setopt($curl, CURLOPT_URL, $this->url . '/' . $action); 
            //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($curl, CURLOPT_POST, true); 
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);  
            //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);  
            $r = curl_exec($curl); 
            $json = \yii\helpers\Json::decode($r); 
            //$info = curl_getinfo($curl);
        } catch (\Exception $ex) {
            $json = [
                'success'=> false, 
                'message'=> _t(
                    'lib.upload', 
                    'Request {action} failed. Server error. Please try again', 
                    ['action' => $action]
                )
            ];
        }
        if(empty($json)) {
            $json = [
                'success'=>false, 
                'message'=> _t(
                    'lib.upload', 
                    'Request {action} failed. Please try again', 
                    ['action' => $action]
                )
            ];
        }
        return $json;
    }
        
    public function __destruct()
    {
        curl_close($this->curl);
    }
}
