<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message;


use common\components\exceptions\AssertHelper;
use common\models\User;

class Constants
{
    const GROUP_ORDER_NOTIFICATION = 'order';
    const GROUP_SERVICE_NOTIFICATION = 'service';
    const GROUP_NEWS_AND_PROMOTION = 'news';
    const GROUP_RULES_AND_SECURITY = 'rules';
    const GROUP_PERSONAL_MESSAGES = 'personalMessages';

    const SENDER_TYPE_EMAIL = 'email';
    const SENDER_TYPE_SMS = 'sms';

    const TIME_POLICY_NEVER = 'never';
    const TIME_POLICY_DAILY = 'daily';
    const TIME_POLICY_WORK = 'work';
    const TIME_POLICY_IMMEDIATELY = 'immediately';

    const CONTENT_TYPE_TEXT = 'text';
    const CONTENT_TYPE_HTML = 'html';

    /**
     * List of groups of messages
     * @return array
     */
    public static function getMessagesGroups()
    {
        return [
            self::GROUP_ORDER_NOTIFICATION,
            self::GROUP_SERVICE_NOTIFICATION,
            self::GROUP_NEWS_AND_PROMOTION,
            self::GROUP_RULES_AND_SECURITY,
            self::GROUP_PERSONAL_MESSAGES
        ];
    }

    /**
     * @return string[]
     */
    public static function getGroupLabels()
    {
        return [
            self::GROUP_ORDER_NOTIFICATION => _t('site.notify', 'Order\'s notification'),
            self::GROUP_SERVICE_NOTIFICATION => _t('site.notify', 'Service\'s notification'),
            self::GROUP_NEWS_AND_PROMOTION => _t('site.notify', 'News and promotions'),
            self::GROUP_RULES_AND_SECURITY => _t('site.notify', 'Rules and security'),
            self::GROUP_PERSONAL_MESSAGES => _t('site.notify', 'Personal messages'),
        ];
    }

    /**
     * @param User $user
     * @return array
     * @throws \yii\base\Exception
     */
    public static function getActiveSendersLabels(User $user)
    {
        AssertHelper::assert($user, "User is not loginned");

        $result = [];

        if($user->getContactByType(UserContactInterface::CONTACT_TYPE_EMAIL, false))
        {
            $result[self::SENDER_TYPE_EMAIL] = _t('site.notify', 'E-mail');
        }

        if($user->getContactByType(UserContactInterface::CONTACT_TYPE_PHONE))
        {
            $result[self::SENDER_TYPE_SMS] = _t('site.notify', 'Text message');
        }
        
        return $result;
    }


    /**
     * @return array
     */
    public static function getTimePolicyLabels()
    {
        return [
            self::TIME_POLICY_IMMEDIATELY => _t('site.notify', 'Immediately'),
            self::TIME_POLICY_WORK => _t('site.notify', 'Working hours'),
            self::TIME_POLICY_DAILY => _t('site.notify', 'Daily'),
            self::TIME_POLICY_NEVER => _t('site.notify', 'Never'),
        ];
    }
}