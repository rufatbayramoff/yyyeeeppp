<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message;

use common\components\exceptions\AssertHelper;
use common\models\NotifyMessage;
use yii\base\InvalidConfigException;
use yii\helpers\Json;


/**
 * Class Worker
 *
 * @package lib\message
 */
class Worker
{
    /**
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws exceptions\TemplateNotFoundException
     */
    public function work()
    {
        $messages = $this->getProcessMessages();

        foreach ($messages as $message) {
            try {
                /** @var senders\SenderInterface $sender */
                $sender = \Yii::$app->message->getSenderByType($message->sender);
                if (!$sender) {
                    throw new InvalidConfigException('Can`t generate sender object with type: ' . $message->sender . '. For message id: ' . $message->id);
                }
                $language = $sender->getContactType() === UserContactInterface::CONTACT_TYPE_PHONE ? 'en-US' : $message->user->userProfile->current_lang;
                $template = TemplateRepository::getTemplateByCode($message->user, $message->message_type, $language);
                $params = $message->params ? $message->params : [];

                list($messageHeader, $messageText, $messageHtml, $messageSms) = MessageRenderHelper::render($template, $params);
                $contact = $message->user->getContactByType($sender->getContactType(), false);
                if ($contact) {
                    $sender->send(
                        $contact,
                        $messageHeader,
                        ($sender->getContactType() === UserContactInterface::CONTACT_TYPE_PHONE ? $messageSms : $messageText),
                        $messageHtml,
                        $message->user
                    );
                }
            } catch (\Exception $e) {
                \Yii::error($e);
            }
            $message->is_sended = 1;
            AssertHelper::assertSave($message);
        }
    }


    /**
     * Return messages for send in it iteration
     *
     * @return NotifyMessage[]
     */
    private function getProcessMessages()
    {
        return NotifyMessage::find()
            ->where(['is_sended' => 0])
            ->andWhere(['<=', 'sent_datetime', date('Y-m-d H:i:s')])
            ->all();
    }
}