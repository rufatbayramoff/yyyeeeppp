#Сервис

Сервис для отправки сообщений пользователям выполнен в качестве службы приложения и доступен следующим образом:

`Yii::$app->message`

#Отправка сообщений пользователям

Служба имплементирует MessageServiceInterface, в котором определены 2 метода для отправки сообщения пользователю: 

`send($user, $templateCode, $params = [])`

Этот метод отправляет пользователю сообщение, учитывая настройки его нотификации, временные зоны, верификацию его _Контактов_ (например телефона) и прочее.
Следует учитывать, что он может и не отправить сообщение вовсе (если пользователь так настроил свои параметры).

В действительности, сам метод ничего не отправляет, а всего лишь вычилсяет когда и с помощью каких _Сендеров_ (объектов, нопсредственно отправляющих сообщения) 
необходимо отправлять сообщение, и записывает эту информацию в хранилище. Саму отправку осуществляет _Воркер_, запускаемый по крону.

`sendForce($user, $templateCode, $params = [], $senderType)`

Данный метод отправляет сообщение пользователю в форсированном режиме, а именно: 

- сообщение отсылается сразу-же и в любом случае, невзирая на настройки пользователя
- не провеяется верификация контакта
- в хранилище ничего не записывается

Так же в этот метод необходимо передавать тип _сендера_ ($senderType). Если контакт для данного _сендера_ не найден - будет вызвано исключение.

#Шаблоны и группы шаблонов

Любое сообщение, отправляемое пользователю, должно быть сформированно с помощью шаблона (в настоящий момент они храняться в таблице EmailTemplate)

Шаблон имеет ряд свойств:

- code - уникальный код шаблона
- group - группа шаблона
- language - язык шаблона
- title - заголовок шаблона
- template_html - контент шаблона для html версии
- template_text - контент шаблона для текстовой версии

Следует отметить, что для одного шаблона имеется несколько записей в хранилище, так как шаблон может быть на разных языках.

##Группа шаблона

Группа шаблона показывает к какой логической части принадлежит тот или иной шаблон.
В настоящий момент их 4: Order, Service, Notify, Rules - Их констнанты описан в классе \lib\message\Constants. 
Именн для этих групп пользователь и задает условия нотификации в своих настройках. В зависимости от того, к какой группе принадлежит шаблон, 
он будет отправляться в то или иное время указанными _сендерами_.

Так же есть группа System - для системных шаблонов, таких как восстановление пароля и прочее. Сообщения этой группы, как правило, отправляются 
с помощью метода `sendForce()`

##Язык шаблона

Шаблоны могут иметь несколько вариантов для разных языков. Репозиторий сначала ищет шаблон для локали пользователя. Если он не будет найден - 
используется локаль по умолчанию `\lib\message\TemplateRepository::DEFAULT_LANGAGE`. Если же и для нее не будет найден шаблон - 
будет эксепшн `\lib\message\exceptions\TemplateNotFoundException`.

##Заголовок шаблона

Заголовок шаблона служит не совсем заголовком - некоторые _сендеры_ его используют для отправки, например, он служит темой письма для `EmailSender` 

##Контент сообщений

Существует два типа контента шаблонов: html и text. Сендеры могут использовать любой из этих типов, возиожно и оба сразу.

##Ограничение по параметры, передаваемые в шаблон

Кву в теле шаблонов, так и в заголовке, имеется возможность передать параметры, обрамленные `%`, например, `%username%`. 

При создании сообщений с помощью методов `send` и `sendForce` проверяется факт передачи всех необходимых параметров. 
Так как мы изначально не знаем какими сендерами будет отправлен, параметры необходимо передавать сразу для всех возможных вариантов контента,
а именно `title` `template_html` `template_text`. В случае, если переданны не все параметры, будет эксепшн `\lib\message\exceptions\BadParamsException`

Еще одно ограничение на параметры - они все должны быть скалярного типа (так как используется их JSON сериализация и приведение к строке).
В случае если это не так - так же будет эксепшн `\lib\message\exceptions\BadParamsException`

#Создание _сендеров_

В настоящий момент реализованно 2 типа сендеров: `EmailSender` и `SmsSender`, а так же их фейковые варианты для отладки. 

Тем не менее, может возникнуть необходимость реализовать дополнительные _сендеры_, например, для WhatsApp, Viber, Telegram, Facebook и пр.

Что бы написать новый сендер необходимо реализовать интерфейс `\lib\message\senders\SenderInterface`, который описывает 2 метода:

`getContactType()` - метод должен вернуть тип _контакта_, который необходим ему для отправки (одна из констант `\lib\message\UserContactInterface`). 
Разные _сендеры_ могут использовать один и тот же тип контакат, например, как СМС так и WhatsApp сендеры буду требовать `\lib\message\UserContactInterface::CONTACT_TYPE_PHONE`

`send($contact, $header, $messageText, $messageHtml, $userId)` - непосредственно отправляет сообщение пользователю. Метод получает контакт, который он запрашивал, 
и отрендеренные шаблоны всех вариантов (так как менеджер не может знать какой тип нужен сендеру). `$userId` - этот параметр не желательно использовать так как он лишь 
для совместимости с текущй реализацией отправки смс сообщений.

Чтоб добавить сендер в службу необходимо дополнить конфиг службы.

#Контакты

Для получения контактов используется интерфейс `\lib\message\UserContactInterface`

Его метод `getContactByType($contactType, $onlyVerified)` должен возвращать контакт соответствующего типа, или null если контакт не найден. 
В настоящий момент этот интерфейс имплементирует сам пользователь.