<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message;

/**
 * Interface UserConfigInterface
 * @package lib\message
 */
interface UserConfigInterface
{
    /**
     * Check that we need send message
     * @param string $group Message group
     * @return boolean
     */
    public function isNeedSend($group);

    /**
     * Resolve send time based on user config
     * @param string $group Message group
     * @param string $sender Sender type
     * @return \DateTime
     * @throw exceptions\UnsendedMessageException If we don't need send message
     */
    public function resolveSendDateTime($group, $sender);

    /**
     * Retrun list of type of senders for message
     * @param string $group Message group
     * @return string[]
     */
    public function resolveSenderTypes($group);
}