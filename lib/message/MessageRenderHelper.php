<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message;


use common\models\EmailTemplate;
use lib\message\exceptions\BadParamsException;
use yii\base\Exception;

/**
 * Class MessageRender
 * @package lib\message
 */
class MessageRenderHelper
{
    /**
     * @param EmailTemplate $template
     * @param array         $params
     * @return array [$messageTitle, $messageText, $messageHtml, $messageSms]
     * @throws Exception
     */
    public static function render(EmailTemplate $template, array $params)
    {
        $replaces = array_combine(array_map(function($k){ return '%'.$k.'%';}, array_keys($params)), $params);
        return [
            strtr($template->title, $replaces),
            strtr($template->template_text, $replaces),
            nl2br(strtr($template->template_html, $replaces)),
            strtr($template->template_sms, $replaces),
        ];
    }

    public static function renderTemplate($templateText, array $params)
    {
        $replaces = array_combine(array_map(function($k){ return '%'.$k.'%';}, array_keys($params)), $params);
        return strtr($templateText, $replaces);
    }
    /**
     * Check params that all is scalar and that we have all need params for rendering template
     * @param EmailTemplate $template
     * @param array         $params
     * @throws BadParamsException
     */
    public static function checkParams(EmailTemplate $template, array $params)
    {
        foreach($params as $paramName => $paramValue)
        {
            if(empty($paramValue)){ $paramValue = ""; }
            if(!is_scalar($paramValue))
            {
                throw new BadParamsException("Param {$paramName} is no scalar");
            }
        }

        $text = $template->title.$template->template_text.$template->template_html;

        preg_match_all('/%(.+?)%/', $text, $matches);
        $requiredParams = array_unique($matches[1]);

        if($diff = array_diff($requiredParams, array_keys($params)))
        {
            throw new BadParamsException("Missing params for template {$template->code} : ".implode(', ', $diff));
        }
    }
}