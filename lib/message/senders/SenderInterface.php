<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message\senders;
use common\models\User;

/**
 * Interface SenderInterface
 * @package lib\message\senders
 */
interface SenderInterface
{
    /**
     * @param string $contact Contact like phone number or email
     * @param string $header Header of message (for excmple, subject on email)
     * @param string $messageText Message text
     * @param        $messageHtml
     * @param User $user User
     */
    public function send($contact, $header, $messageText, $messageHtml, User $user = null);

    /**
     * Return contact type like phone or email
     * @return string
     */
    public function getContactType();

    /**
     * Return primary content type
     * One of Constant::CONTENT_TYPE...
     * @return string
     */
    public function getPrimaryContentType();
}