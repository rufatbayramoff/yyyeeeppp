<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message\senders;


use common\components\ArrayHelper;
use common\models\Ps;
use common\models\User;
use common\models\UserSms;
use lib\message\Constants;
use lib\message\UserContactInterface;

/**
 * Class SmsSender
 * @package lib\message\senders
 */
class SmsSender implements SenderInterface
{
    /**
     * @param string $contact     Contact like phone number or email
     * @param string $header      Header of message (for excmple, subject on email)
     * @param string $messageText Message text
     * @param int    $messageHtml
     * @param User    $user
     */
    public function send($contact, $header, $messageText, $messageHtml, User $user = null)
    {
        /** @var Ps $ps */
        $ps = ArrayHelper::first($user->ps);
        if($messageText)
            UserSms::send($ps->sms_gateway, $user->id, $contact, $messageText, UserSms::TYPE_NOTIFY);
        
    }

    /**
     * Return contact type like phone or email
     * @return string
     */
    public function getContactType()
    {
        return UserContactInterface::CONTACT_TYPE_PHONE;
    }

    /**
     * Return primary content type
     * One of Constant::CONTENT_TYPE...
     * @return string
     */
    public function getPrimaryContentType()
    {
        return Constants::CONTENT_TYPE_TEXT;
    }
}