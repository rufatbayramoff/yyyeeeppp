<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message\senders;
use common\components\Emailer;
use common\models\User;
use lib\message\Constants;
use lib\message\UserContactInterface;


/**
 * Interface EmailSenderInterface
 * @package lib\message\senders
 */
class EmailSender implements SenderInterface
{
    /**
     * @param string $contact     Contact like phone number or email
     * @param string $header      Header of message (for excmple, subject on email)
     * @param string $messageText Message text
     * @param int    $messageHtml
     * @param User       $user
     */
    public function send($contact, $header, $messageText, $messageHtml, User $user = null)
    {
        $mailer = new Emailer();
        $mailer->sendMessage($contact, $header, $messageText, $messageHtml);
    }

    /**
     * Return contact type like phone or email
     * @return string
     */
    public function getContactType()
    {
        return UserContactInterface::CONTACT_TYPE_EMAIL;
    }

    /**
     * Return primary content type
     * One of Constant::CONTENT_TYPE...
     * @return string
     */
    public function getPrimaryContentType()
    {
        return Constants::CONTENT_TYPE_HTML;
    }
}