<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message;


use common\models\User;

/**
 * Interface MessageServiceInterface
 * @package lib\message
 */
interface MessageServiceInterface
{
    /**
     * Send message to user
     * Check that we need send message and create postpone message
     * @param User   $user User whom will send message
     * @param string $templateCode Type of message
     * @param array  $params Rendering message params
     */
    public function send(User $user, $templateCode, array $params = []) : void;

    /**
     * Send system message now
     * @param User   $user
     * @param string $templateCode
     * @param array  $params
     * @param string $senderType
     */
    public function sendForce(User $user, $templateCode, array $params, $senderType) : void;

    /**
     * @param string $email
     * @param string $templateCode
     * @param array $params
     * @param string $lang
     */
    public function sendForceEmail(string $email, string $templateCode, array $params, string $lang) : void;
}