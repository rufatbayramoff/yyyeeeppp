<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message\exceptions;


use yii\base\Exception;

class BadParamsException extends Exception
{

}