<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message;


use common\models\EmailTemplate;
use common\models\User;
use lib\message\exceptions\TemplateNotFoundException;

/**
 * Class TemplateRepository
 * @package lib\message
 */
class TemplateRepository
{
    /**
     * Default language
     */
    const DEFAULT_LANGUAGE = 'en-US';

    /**
     * @param User $user
     * @param string $templateCode Messgae type
     * @param null $lang
     * @return EmailTemplate
     * @throws TemplateNotFoundException
     */
    public static function getTemplateByCode(User $user = null, $templateCode, $lang = null)
    {
        /** @var EmailTemplate $template */
        $lang = $lang ?? $user->userProfile->current_lang;
        $template = EmailTemplate::findOne(['code' => $templateCode, 'language_id' => $lang])
            ?: EmailTemplate::findOne(['code' => $templateCode, 'language_id' => self::DEFAULT_LANGUAGE]);

        if(!$template)
        {
            throw new TemplateNotFoundException($templateCode);
        }

        return $template;
    }

}