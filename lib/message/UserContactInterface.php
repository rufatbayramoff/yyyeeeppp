<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message;

/**
 * Interface UserContactInterface
 * @package lib\message
 */
interface UserContactInterface
{
    const CONTACT_TYPE_EMAIL = 'email';
    const CONTACT_TYPE_PHONE = 'phone';

    /**
     * Retrun user contact by type
     * @param string  $contactType
     * @param boolean $onlyVerified
     * @return null|string
     */
    public function getContactByType($contactType, $onlyVerified = true);
}