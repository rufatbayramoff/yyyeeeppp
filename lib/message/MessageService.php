<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message;


use common\components\exceptions\AssertHelper;
use common\models\NotifyMessage;
use common\models\User;
use common\models\UserStatistics;
use lib\message\exceptions\ResolveContactException;
use lib\message\exceptions\UnsendedMessageException;
use lib\message\senders\SenderInterface;
use yii\base\Component;
use yii\helpers\Json;

/**
 * Class MessageService
 *
 * @package lib\message
 */
class MessageService extends Component implements MessageServiceInterface
{
    /**
     * Senders
     *
     * @var senders\SenderInterface[]
     */
    public $senders = [];

    /**
     * Check params. If true and some params not persist, will throw exception.
     *
     * @var bool
     */
    public $checkParams = false;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        foreach ($this->senders as &$sender) {
            $sender = \Yii::createObject($sender);
        }
        unset($sender);
    }

    /**
     * Return sender by sender type
     *
     * @param string $senderType
     * @return SenderInterface|null
     */
    public function getSenderByType($senderType)
    {
        return isset($this->senders[$senderType]) ? $this->senders[$senderType] : null;
    }

    /**
     * Send message to user
     * Check that we need send message and create postpone message
     *
     * @param User $user User whom will send message
     * @param string $templateCode Type of message
     * @param array $params Rendering message params
     */
    public function send(User $user, $templateCode, array $params = []): void
    {
        if ($user->getIsSystem()) {
            return;
        }
        $template = TemplateRepository::getTemplateByCode($user, $templateCode);
        if (!$template->is_active) {
            return;
        }
        $userConfig = new UserConfig($user);
        if (!$userConfig->isNeedSend($template->group)) {
            return;
        }
        if ($this->checkParams) {
            MessageRenderHelper::checkParams($template, $params);
        }
        foreach ($userConfig->resolveSenderTypes($template->group) as $senderType) {
            if ($senderType === Constants::SENDER_TYPE_SMS && UserStatistics::isOnline($user)) {
                if (!$userConfig->isNoCheckOnline()) {
                    continue;
                }
            }
            $sender = $this->senders[$senderType];
            if (!$this->getSenderByType($senderType)) {
                continue;
            }
            if (!$user->getContactByType($sender->getContactType(), false)) {
                continue;
            }
            if (!$template->haveContentForType($sender->getPrimaryContentType())) {
                continue;
            }
            if (\Yii::$app->setting->get('settings.sendEmailForce', 0)) {
                // This option used for debugging
                $this->sendForce($user, $templateCode, $params, $senderType);
            } else {

                $message = new NotifyMessage();
                $message->user_id = $user->id;
                $message->message_type = $templateCode;
                $message->params = $params;
                $message->sender = $senderType;
                $message->sent_datetime = $userConfig->resolveSendDateTime($template->group, $senderType)->format('Y-m-d H:i:s');
                $message->is_sended = 0;

                if (!NotifyMessage::find()->where($message->getAttributes(null, ['id']))->exists()) {
                    AssertHelper::assertSave($message);
                }
            }
        }
    }

    /**
     * Send system message now
     *
     * @param User $user
     * @param string $templateCode
     * @param array $params
     * @param string $senderType
     * @throws ResolveContactException
     * @throws UnsendedMessageException
     * @throws \yii\base\Exception
     * @throws exceptions\BadParamsException
     * @throws exceptions\TemplateNotFoundException
     */
    public function sendForce(User $user, $templateCode, array $params, $senderType): void
    {
        $sender = $this->getSenderByType($senderType);

        if (!$sender) {
            throw new UnsendedMessageException("Can't resolve sender {$senderType}");
        }

        $template = TemplateRepository::getTemplateByCode($user, $templateCode);
        if ($this->checkParams) {
            MessageRenderHelper::checkParams($template, $params);
        }

        list($messageHeader, $messageText, $messageHtml) = MessageRenderHelper::render($template, $params);

        $contactType = $sender->getContactType();
        $contact = $user->getContactByType($contactType, false);

        if (!$contact) {
            throw new ResolveContactException("Can't resolve contact {$contactType} for user {$user->id}");
        }

        $sender->send($contact, $messageHeader, $messageText, $messageHtml, $user);
    }


    /**
     * @param string $email
     * @param string $templateCode
     * @param array $params
     * @param string $lang
     * @throws UnsendedMessageException
     * @throws \yii\base\Exception
     * @throws exceptions\BadParamsException
     * @throws exceptions\TemplateNotFoundException
     */
    public function sendForceEmail(string $email, string $templateCode, array $params, string $lang): void
    {
        $sender = $this->getSenderByType(Constants::SENDER_TYPE_EMAIL);

        if (!$sender) {
            throw new UnsendedMessageException("Can't resolve email sender");
        }

        $template = TemplateRepository::getTemplateByCode(null, $templateCode, $lang);
        if ($this->checkParams) {
            MessageRenderHelper::checkParams($template, $params);
        }

        list($messageHeader, $messageText, $messageHtml) = MessageRenderHelper::render($template, $params);

        $sender->send($email, $messageHeader, $messageText, $messageHtml, null);
    }
}