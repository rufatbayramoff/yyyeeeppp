<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\message;
use common\models\User;
use common\models\UserProfile;
use lib\message\exceptions\UnsendedMessageException;
use yii\base\Exception;
use yii\helpers\Json;

/**
 * Class UserConfig
 * @package lib\message
 */
class UserConfig implements UserConfigInterface
{
    /**
     * Default time
     */
    const DEFAULT_TIME_POLICY = Constants::TIME_POLICY_IMMEDIATELY;

    /**
     * Default timezone of user
     */
    const DEFAULT_TIME_ZONE = 'America/Los_Angeles';

    /**
     * @var User
     */
    private $user;

    /**
     * @var string[][]
     */
    private $data = [];

    /**
     * @param User $user
     */
    function __construct(User $user)
    {
        $this->user = $user;
        $this->data = self::addDefaults($user, $user->notify_config ? Json::decode($user->notify_config) : []);
    }

    /**
     * Check that we need send message
     * @param string $group Message group
     * @return boolean
     */
    public function isNeedSend($group)
    {
        return (bool)$this->resolveSenderTypes($group);
    }

    /**
     * Resolve send time based on user config
     * @param string $group  Message group
     * @param string $senderType Sender type
     * @return \DateTime
     * @throws Exception
     * @throws UnsendedMessageException
     * @throw exceptions\UnsendedMessageException If we don't need send message
     * @todo refacrtor it to separate TimePolicy classes
     */
    public function resolveSendDateTime($group, $senderType)
    {
        $timePolicy = isset($this->data[$group][$senderType]) ? $this->data[$group][$senderType] : self::DEFAULT_TIME_POLICY;

        if($timePolicy == Constants::TIME_POLICY_NEVER)
        {
            throw new UnsendedMessageException();
        }

        $now = new \DateTime();
        $now->setTimezone(new \DateTimeZone($this->user->userProfile->timezone_id ?: self::DEFAULT_TIME_ZONE));
        $hour = (int)$now->format('H');

        $serverTimeZone = new \DateTimeZone('UTC');

        switch($timePolicy)
        {
            case Constants::TIME_POLICY_IMMEDIATELY:
            {
                return $now->setTimezone($serverTimeZone);
            }

            case Constants::TIME_POLICY_DAILY:
            {
                if($hour < 9)
                {
                    return $now
                        ->setTime(9, 0, 0)
                        ->setTimezone($serverTimeZone);
                }
                else
                {
                    return $now
                        ->add(new \DateInterval('P1D'))
                        ->setTime(9, 0, 0)
                        ->setTimezone($serverTimeZone);
                }
            }

            case Constants::TIME_POLICY_WORK:
            {

                if($hour < 9)
                {
                    return $now
                        ->setTime(9, 0, 0)
                        ->setTimezone($serverTimeZone);
                }
                elseif($hour > 19)
                {
                    return $now
                        ->add(new \DateInterval('P1D'))
                        ->setTime(9, 0, 0)
                        ->setTimezone($serverTimeZone);
                }
                else
                {
                    return $now
                        ->setTimezone($serverTimeZone);
                }
            }

            default :
            {
                throw new Exception("Unknown time {$timePolicy}");
            }
        }
    }

    /**
     * Retrun list of type of senders for message
     * @param string $messageGroup Message group
     * @return string[]
     */
    public function resolveSenderTypes($messageGroup)
    {
        $result = [];
        foreach($this->data as $group => $senders)
        {
            foreach($senders as $sender => $time)
            {
                if($group == $messageGroup && $time != Constants::TIME_POLICY_NEVER)
                {
                    $result[] = $sender;
                }
            }
        }
        return $result;
    }

    /**
     * Convert config to array
     * @return array
     */
    public function toArray()
    {
        return self::addDefaults($this->user, $this->data);
    }

    public function getUserSettings()
    {
        $userSettings = $this->user->userProfile->settings;
        if (!\is_array($userSettings)) {
            $userSettings = Json::decode($userSettings);
        }
        return $userSettings;
    }

    public function isNoCheckOnline(): bool
    {
        $settings = $this->getUserSettings();
        if (!empty($settings[UserProfile::SETTINGS_NOCHECKONLINE])) {
            return (boolean) $settings[UserProfile::SETTINGS_NOCHECKONLINE];
        }
        return false;
    }

    public function allowDialogAutoCreate():bool
    {
        $settings = $this->getUserSettings();
        if (isset($settings[UserProfile::SETTINGS_DIALOG_AUTO_CREATE])) {
            return (boolean) $settings[UserProfile::SETTINGS_DIALOG_AUTO_CREATE];
        }
        return true;
    }

    public function allowCompanyDialogAutoCreate():bool
    {
        $settings = $this->getUserSettings();
        if (isset($settings[UserProfile::SETTINGS_COMPANY_DIALOG_AUTO_CREATE])) {
            return (boolean) $settings[UserProfile::SETTINGS_COMPANY_DIALOG_AUTO_CREATE];
        }
        return true;
    }

    /**
     * Fill defaults time policy value for not set template groups (for example, if it new user, or new group)
     * @param User $user
     * @param      $data
     * @return array
     */
    private static function addDefaults(User $user, $data)
    {
        $result = [];
        $activeSendersKeys = array_keys(Constants::getActiveSendersLabels($user));

        foreach(array_keys(Constants::getGroupLabels()) as $group)
        {
            foreach($activeSendersKeys as $sender)
            {
                $result[$group][$sender] = isset($data[$group][$sender])
                    ? $data[$group][$sender]
                    : self::DEFAULT_TIME_POLICY;
            }
        }
        return $result;
    }
}