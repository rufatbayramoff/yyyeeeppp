<?php
namespace lib\geo;
use yii\base\Exception;

/**
 * Simple GeoException
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class GeoException extends Exception
{
    public function __construct($message)
    {
        return parent::__construct($message);
    }
}
