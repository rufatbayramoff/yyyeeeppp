<?php

namespace lib\geo;

use common\components\BaseActiveQuery;
use common\components\exceptions\AssertHelper;
use common\models\AffiliateSource;
use common\models\GeoRegion;
use common\models\GeoCity;
use common\models\GeoCountry;
use yii\db\Query;

/**
 * Class work with GeoNames database
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class GeoNames
{
    /**
     * Get Country object by ISO code
     *
     * @param string $country_iso
     * @return GeoCountry
     * @throws GeoNamesException
     */
    public static function getCountryByISO($country_iso = '')
    {
        $data = GeoCountry::find()
            ->where(['iso_code' => $country_iso])
            ->one();
        if (is_null($data)) {
            throw new GeoNamesException('Cannot find country by ISO=' . $country_iso);
        }
        return $data;
    }

    /**
     * Get Country object by ID
     *
     * @param int $id
     * @return GeoCountry
     * @throws GeoNamesException
     */
    public static function getCountryById($id = 0)
    {
        $data = GeoCountry::findByPk($id);
        if (is_null($data)) {
            throw new GeoNamesException('Cannot find country by ID=' . $id);
        }
        return $data;
    }

    /**
     * Get City object by Id
     *
     * @param int $id
     * @return GeoCity
     * @throws GeoNamesException
     */
    public static function getCityById($id = 0)
    {
        $data = GeoCity::findByPk($id);
        if (is_null($data)) {
            throw new GeoNamesException('Cannot find city by ID=' . $id);
        }
        return $data;
    }

    /**
     * Get Region object by Geoname Id
     *
     * @param string $geoname_id
     * @return GeoRegion
     * @throws GeoNamesException
     */
    public static function getRegionByGeoNameId($geoname_id = '')
    {
        $data = GeoRegion::find()
            ->where(['geoname_id' => $geoname_id])
            ->one();
        if (is_null($data)) {
            throw new GeoNamesException('Cannot find region by geoname_id=' . $geoname_id);
        }
        return $data;
    }

    /**
     * Get Region object by Id
     *
     * @param string $id
     * @return GeoRegion
     * @throws GeoNamesException
     */
    public static function getRegionById($id = '')
    {
        $data = GeoRegion::findByPk($id);
        if (is_null($data)) {
            throw new GeoNamesException('Cannot find region by ID=' . $id);
        }
        return $data;
    }

    /**
     * Get City object by Geoname Id
     *
     * @param string $geoname_id
     * @return GeoCity
     * @throws GeoNamesException
     */
    public static function getCityByGeoNameId($geoname_id = '')
    {
        $data = GeoCity::find()
            ->where(['geoname_id' => $geoname_id])
            ->one();
        if (is_null($data)) {
            throw new GeoNamesException('Cannot find city by geoname_id=' . $geoname_id);
        }
        return $data;
    }

    /**
     * Get all cities by coords
     *
     * @param $name
     * @param float $lat
     * @param float $lon
     * @return GeoCity
     */
    public static function getCitiesByCoordsAndName($name, $lat, $lon)
    {
        $lat = floatval($lat);
        $lon = floatval($lon);

        return GeoCity::find()
            ->where(['between', 'lat', $lat - 0.1, $lat + 0.1])
            ->andWhere(['between', 'lon', $lon - 0.1, $lon + 0.1])
            ->andWhere(['title' => $name])
            ->one();
    }

    /**
     * Get Country Id and Region Id by City Id
     *
     * @param int $city_id
     * @return GeoCity
     * @throws GeoNamesException
     */
    public static function getCountryRegionByCityId($city_id = 0)
    {
        $data = GeoCity::findByPk($city_id);
        if (is_null($data)) {
            throw new GeoNamesException('Cannot find country and region by city_id=' . $city_id);
        }

        return $data;
    }

    /**
     * Get region or create new region in base
     *
     * @param $countryId
     * @param $regionName
     * @return \common\models\GeoRegion
     */
    public static function getRegionOrCreate($countryId, $regionName)
    {
        BaseActiveQuery::$staticCacheGlobalEnable = false;
        $result                                   = GeoRegion::find()->where(['country_id' => $countryId, 'title' => $regionName])->one();
        BaseActiveQuery::$staticCacheGlobalEnable = true;
        if ($result) {
            return $result;
        }
        $model             = new GeoRegion();
        $model->country_id = $countryId;
        $model->title      = $regionName;
        $model->geoname_id = 1;
        $model->safeSave();

        return $model;
    }

    /**
     * Get city or create new city in base
     *
     * @param $data
     * @param $regionId
     * @return \common\models\GeoCity
     */
    public static function getCityOrCreate($data, $regionId)
    {
        $cityName = $data['city'] ?? $data['city_name'];

        $result = GeoCity::find()->where([
            'country_id' => $data['country_id'],
            'region_id'  => $regionId,
            'title'      => $cityName
        ])->one();

        if ($result) {
            return $result;
        } else {
            // Exclude any cache
            $geoCityArray = (new Query())->select('*')->from(GeoCity::tableName())->where(
                [
                    'country_id' => $data['country_id'],
                    'region_id'  => $regionId,
                    'title'      => $cityName
                ]
            )->one();
            if ($geoCityArray) {
                $geoCity = new GeoCity();
                GeoCity::populateRecord($geoCity, $geoCityArray);
                return $geoCity;
            }
        }

        $model              = new GeoCity();
        $model->title       = $cityName;
        $model->country_id  = $data['country_id'];
        $model->region_id   = $regionId;
        $model->timezone_id = null;
        $model->lat         = $data['lat'];
        $model->lon         = $data['lon'];
        $model->fcode       = '1';
        $model->geoname_id  = 1;
        $model->safeSave();

        return $model;
    }

    /**
     * Get all countries
     *
     * @return mixed
     */
    public static function getAllCountries()
    {
        return GeoCountry::find()->orderBy('title ASC')->all();
    }
}
