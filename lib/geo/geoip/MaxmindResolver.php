<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\geo\geoip;


use lib\geo\GeoException;
use lib\geo\models\Location;

/**
 * Class MaxmindResolver
 * @package lib\geo\geoip
 */
class MaxmindResolver implements IpResolverInterface
{
    /**
     * Return location by ip
     * @param string $ip
     * @return \lib\geo\models\Location|null
     */
    public function getLocationByIp($ip)
    {
        try
        {
            $newDb = \Yii::getAlias('@vendor') . '/geoip2/geoip2/maxmind-db/GeoLite2-City.mmdb';
            if(!file_exists($newDb)) {
                throw new GeoException('Max mind database not exist');
            }

            $reader = new \GeoIp2\Database\Reader($newDb);
            $record = $reader->city($ip);

            $res = [
                'timezone' => 'America/Los_Angeles'
            ];

            if(isset($record->subdivisions[0]->names['en'])) {
                $res['region'] = $record->subdivisions[0]->names['en'];
            }

            if (!empty($record->location)) {
                $res['lat'] = $record->location->latitude;
                $res['lon'] = $record->location->longitude;
            }
            if (!empty($record->location->timeZone)) {
                $res['timezone'] = $record->location->timeZone;
            }
            $res['country'] = $record->country->isoCode;
            if ($record->city) {
                $res['city'] = $record->city->name;
            }

            return new Location($res);

        }
        catch (\Exception $e)
        {
            \Yii::error($e, 'geo');
            return null;
        }
    }
}