<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\geo\geoip;

/**
 * Interface IpResolverInterface
 * @package lib\geo\geoip
 */
interface IpResolverInterface
{
    /**
     * Return location by ip
     * @param string $ip
     * @return \lib\geo\models\Location|null
     */
    public function getLocationByIp($ip);
}