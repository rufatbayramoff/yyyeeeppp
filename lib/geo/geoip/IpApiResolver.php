<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\geo\geoip;


use lib\geo\models\Location;

/**
 * Class IpApiResolver
 * @package lib\geo\geoip
 */
class IpApiResolver implements IpResolverInterface
{
    /**
     * Use cache
     * @var bool
     */
    public $useCache = true;

    /**
     * Cache duration in seconds
     * @var int
     */
    public $cacheDuration = 86400;

    /**
     * Timeout in seconds
     * @var int
     */
    public $timeout = 5;

    /**
     * Deactivate duration in seconds
     * @var int
     */
    public $deactivateDuration = 300;

    /**
     * Proxy http server for make request to api
     * @var string
     */
    public $proxy;

    /**
     * Return location by ip
     * @param string $ip
     * @return \lib\geo\models\Location|null
     */
    public function getLocationByIp($ip)
    {
        try
        {
            if ($res = $this->query($ip)) {
                return new Location(
                    [
                    'lat' => $res['lat'],
                    'lon' => $res['lon'],
                    'city' => $res['city'],
                    'region' => $res['regionName'],
                    'country' => $res['countryCode'],
                    'timezone' => $res['timezone']
                    ]
                );
            }

            return null;
        }
        catch (\Exception $e)
        {
            \Yii::error($e, "geo");
            return null;
        }
    }

    /**
     * @param $ip
     * @return array|mixed|null|object|string
     */
    private function query($ip)
    {
        $cache = \Yii::$app->cache;
        $cacheKey = md5('ipapi2' . $ip);
        if ($this->useCache && $cache->exists($cacheKey)) {
            return $cache->get($cacheKey);
        }

        if(!$this->getIsAcrive()) {
            return null;
        }

        /**
         * @var string $url Url for request to api
         * @link http://ip-api.com/docs/api:returned_values#field_generator
         */
        $url = "http://ip-api.com/php/{$ip}?fields=16895";

        $ch = curl_init();
        if($this->proxy) {
            curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $rawResponse = curl_exec($ch);

        // if request error - deactivate
        if(curl_errno($ch)) {
            $this->deactivate();
            return null;
        }
        curl_close($ch);

        $response = $this->parseResponse($rawResponse);

        if($this->useCache) {
            $cache->set($cacheKey, $response, $this->cacheDuration);
        }

        return $response;
    }


    /**
     * Parse responce from service api
     * @param $rawResponse
     * @return mixed|null
     */
    private function parseResponse($rawResponse)
    {
        $response = unserialize($rawResponse);

        // if bad response
        if(!is_array($response) || $response['status'] != 'success') {
            return null;
        }

        return $response;
    }

    /**
     * Deactivate this resolver
     */
    private function deactivate()
    {
        \Yii::$app->cache->set(__CLASS__.'_deactivated', true, $this->deactivateDuration);
    }

    /**
     * Check that this resolver is active
     * @return bool
     */
    private function getIsAcrive()
    {
        return !\Yii::$app->cache->exists(__CLASS__.'_deactivated');
    }

}