<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\geo;

use common\components\exceptions\AssertHelper;
use common\components\IpComparator;
use lib\geo\geoip\IpResolverInterface;
use lib\geo\models\Location;
use yii\base\Component;

class GeoService extends Component
{
    /**
     * Stub for location
     *
     * @var Location
     */
    public $locatonStub = [
        'class'    => Location::class,
        'lat'      => 34.052235,
        'lon'      => -118.243683,
        'city'     => 'Los Angeles',
        'region'   => 'California',
        'country'  => 'US',
        'timezone' => 'America/Los_Angeles',
        'confidenceLevel' => Location::CONFIDENCE_LEVEL_NOT_DETECTED
    ];

    /**
     * Stub ip
     * It ip use if resolved ip is local
     *
     * @var string
     */
    public $ipStub = '134.201.250.155';

    /**
     * Local ips who will replaced by ip stub
     *
     * @var string[]
     */
    public $localIps = [
        '10.*.*.*',
        '192.168.*.*'
    ];

    /**
     * @var geoip\IpResolverInterface[]
     */
    public $ipResolvers = [
        geoip\MaxmindResolver::class,
        geoip\IpApiResolver::class
    ];

    /**
     *
     */
    public function init()
    {
        $this->locatonStub = \Yii::createObject($this->locatonStub);

        foreach ($this->ipResolvers as $index => $resolverConfig) {
            $resolver = \Yii::createObject($resolverConfig);
            AssertHelper::assert($resolver instanceof IpResolverInterface, 'Ip resolver must implement IpResolverInterface');
            $this->ipResolvers[$index] = $resolver;
        }
        unset($index, $resolverConfig, $resolver);
    }

    public function getLocationStringByIp($ip)
    {
        $locationString = 'Not detected';

        if ((strpos($ip, '10.')===0)||(strpos($ip, '192.')===0)) {
            $locationString = 'Office local';
        } elseif (strpos($ip, '79.65.234.12')===0) {
            $locationString = 'Office global';
        } elseif ($ip) {
            foreach ($this->ipResolvers as $ipResolver) {
                if (($location = $ipResolver->getLocationByIp($ip)) && $location->isCityPersist) {
                    $locationString = $location->country . ' ' . $location->region . ' ' . $location->city;
                    break;
                }
            }
        }
        return $locationString;
    }

    /**
     * Return location info
     *
     * @param string $ip
     * @param bool $useStub
     * @return Location|null
     */
    public function getLocationByDomainOrIp($domain, $ip, $useStub = true)
    {
        $confidenceLevel = Location::CONFIDENCE_LEVEL_DETECTED;

        if ($ip === '192.168.66.1') {
            return clone $this->locatonStub;
        }
        if ((strpos(param('server'), '.vcap.me') !== false) || (strpos(param('server'), 'tsdev.work') !== false)) {
            return clone $this->locatonStub;
        }
        if (IpComparator::compare($ip, $this->localIps)) {
            $confidenceLevel = Location::CONFIDENCE_LEVEL_NOT_DETECTED;
            $domainIp = \Yii::$app->getModule('intlDomains')->domainManager->getDefaultIpLocationForDomain($domain);
            if ($domainIp) {
                $ip = $domainIp;
            } else {
                $ip = $this->ipStub;
            }
        }

        foreach ($this->ipResolvers as $ipResolver) {
            if (($location = $ipResolver->getLocationByIp($ip)) && $location->isCityPersist) {
                $location->confidenceLevel = $confidenceLevel;
                return $location;
            }
        }

        return $useStub ? clone $this->locatonStub : null;
    }
}