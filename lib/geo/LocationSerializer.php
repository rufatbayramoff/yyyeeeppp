<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.07.18
 * Time: 16:52
 */

namespace lib\geo;

use common\components\serizaliators\AbstractProperties;
use lib\geo\models\Location;

class LocationSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {

        return [
            \lib\geo\models\Location::class => [
                'lat',
                'lon',
                'city',
                'region',
                'country',
                'timezone',
            ],
        ];
    }
}