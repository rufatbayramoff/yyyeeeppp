<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\geo\models;

use common\components\ps\locator\Coord;
use common\models\GeoCity;
use common\models\GeoCountry;
use common\models\GeoRegion;
use common\models\UserLocation;
use common\modules\catalogPs\models\CatalogSearchForm;
use common\traits\PropertyArrayAccessTrait;
use GeoIp2\Model\Country;
use Yii;
use yii\base\BaseObject;

/**
 * TODO: Remove several location models. Set one GeoLocation model.
 *
 * Class Location
 *
 * @package lib\geo\models
 *
 * @property-read bool $isCityPersist
 * @property-read GeoCountry $geoCountry
 * @property GeoCountry $geoCountryObjectCache
 */
class Location extends BaseObject implements \ArrayAccess
{
    use PropertyArrayAccessTrait;

    CONST CONFIDENCE_LEVEL_NOT_DETECTED = 0;
    CONST CONFIDENCE_LEVEL_DETECTED = 1;
    CONST CONFIDENCE_LEVEL_SELECTED = 2;


    /**
     * Lat
     * @var float
     */
    public $lat;

    /**
     * Lon
     * @var float
     */
    public $lon;

    /**
     * Name of city
     * @var string
     */
    public $city;

    /**
     * Name of region
     * @var string
     */
    public $region;

    /**
     * Country ISO code
     * @var string
     */
    public $country;

    /**
     * @var GeoCountry
     */
    public $geoCountryObject;

    /**
     * @var GeoRegion
     */
    public $geoRegionObject;

    /**
     * @var GeoCity
     */
    public $geoCityObject;

    /**
     * Timezone
     * @var string|null
     */
    public $timezone;


    /**
     * @var string
     */
    public $confidenceLevel = self::CONFIDENCE_LEVEL_DETECTED;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->lat = $this->lat !== null ? round($this->lat, 5) : null;
        $this->lon = $this->lon !== null ? round($this->lon, 5) : null;
    }

    /**
     * Is sity persist
     * @return bool
     */
    public function getIsCityPersist()
    {
        return (bool)$this->city;
    }

    /**
     * @return Coord|null
     */
    public function getCoord()
    {
        return $this->lat && $this->lon ? Coord::create($this->lat, $this->lon) : null;
    }

    public function getGeoCountry()
    {
        $this->geoCountryObject = GeoCountry::findOne(['iso_code' => $this->country]);
        return $this->geoCountryObject;
    }

    /**
     * @return GeoCountry
     */
    public function getGeoCountryObjectCache()
    {
        if (!$this->geoCountryObject) {
            $this->getGeoCountry();
        }

        return $this->geoCountryObject;
    }

    /**
     * @return GeoCity
     */
    public function getGeoCity()
    {
        return UserLocation::factoryGeoCity($this);
    }

    public function __toString()
    {
        if($this->city==CatalogSearchForm::LOCATION_EMPTY){
            return (string)_t('site.catalog', 'All Locations');
        }
        return join(', ', array_filter([
            $this->city,
            $this->region,
            $this->country ? $this->getGeoCountry()->title : ''
        ]));
    }
}