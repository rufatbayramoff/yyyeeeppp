<?php
/**
 * User: nabi
 */

namespace lib\geo\google;


use yii\base\BaseObject;

class GeocodeService extends BaseObject
{
    public $locale = 'en';

    public $key;

    public function init()
    {
        $this->key = param('');
    }

    /**
     * https://maps.googleapis.com/maps/api/geocode/json?place_id=ChIJD7fiBh9u5kcRYJSMaMOCCwQ&language=de&key=AIzaSyD1VGxBmPEZw7IKVo9aysbTiDhmwP8d2fQ
     *
     * @param $placeId
     */
    public function getPlace($placeId)
    {

    }

    public function findPlace($string)
    {

    }
}

/**
 * answer example
 *
 * {
"results" : [
{
"address_components" : [
{
"long_name" : "Paris",
"short_name" : "Paris",
"types" : [ "locality", "political" ]
},
{
"long_name" : "Paris",
"short_name" : "Paris",
"types" : [ "administrative_area_level_2", "political" ]
},
{
"long_name" : "Île-de-France",
"short_name" : "Île-de-France",
"types" : [ "administrative_area_level_1", "political" ]
},
{
"long_name" : "Frankreich",
"short_name" : "FR",
"types" : [ "country", "political" ]
}
],
"formatted_address" : "Paris, Frankreich",
"geometry" : {
"bounds" : {
"northeast" : {
"lat" : 48.9021449,
"lng" : 2.4699208
},
"southwest" : {
"lat" : 48.815573,
"lng" : 2.224199
}
},
"location" : {
"lat" : 48.856614,
"lng" : 2.3522219
},
"location_type" : "APPROXIMATE",
"viewport" : {
"northeast" : {
"lat" : 48.9021449,
"lng" : 2.4699208
},
"southwest" : {
"lat" : 48.815573,
"lng" : 2.224199
}
}
},
"place_id" : "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
"types" : [ "locality", "political" ]
}
],
"status" : "OK"
}
 */