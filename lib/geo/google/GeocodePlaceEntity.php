<?php
/**
 * User: nabi
 */

namespace lib\geo\google;


class GeocodePlaceEntity
{

    /**
     * @var string
     */
    public $formattedAddress;
    /**
     * @var string
     */
    public $placeId;


    /**
     * @var AddressComponent[]
     */
    public $addressComponents;


    public $geometry;

}