<?php
namespace lib\geo;

/**
 * Simple GeoNamesException
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class GeoNamesException extends GeoException
{
    public function __construct($message)
    {
        return parent::__construct($message);
    }
}
