<?php
namespace lib\geo;
use common\components\ps\locator\GeoRadius;

/**
 * LocationUtils contains functions to work with location coordinates,
 * to find distance, to get square by center point and etc.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class LocationUtils
{
    const EARTH_R = 6372;
    
    /**
     * get distance between two points in KM
     * 
     * @param float $lat1
     * @param float $lon1
     * @param float $lat2
     * @param float $lon2
     * @return float
     */
    public static function getDistance($lat1, $lon1, $lat2, $lon2)
    {
        try { 
            $r = self::EARTH_R;  
            $dlat = deg2rad($lat2 - $lat1);
            $dlon = deg2rad($lon2 - $lon1);
            $lat1r = deg2rad($lat1);
            $lat2r = deg2rad($lat2); 
            $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1r) * cos($lat2r) * sin($dlon / 2) * sin($dlon / 2);
            $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
            $result = $c * $r;
            return $result; 
        } catch(GeoException $e) {
        } 
        return 0;
    }

    /**
     * get area around point with given radius in km
     * @param GeoRadius $radius
     * @return array
     */
    public static function getPointArea(GeoRadius $radius)
    {
        $lat = $radius->getCenter()->getLat();
        $lon  = $radius->getCenter()->getLon();
        
        $x1 = $lon - rad2deg($radius->getRadius() / self::EARTH_R / cos(deg2rad($lat)));
        $x2 = $lon + rad2deg($radius->getRadius() / self::EARTH_R / cos(deg2rad($lat)));
        
        $y1 = $lat + rad2deg($radius->getRadius() / self::EARTH_R);
        $y2 = $lat - rad2deg($radius->getRadius() / self::EARTH_R);
        
        $coords = [
            'point' => ['lat'=>$lat, 'lon'=>$lon],
            'from' => ['lat'=>$y1, 'lon' => $x1],
            'to' => ['lat' => $y2, 'lon' => $x2]
        ];
        return $coords;
    }

    public static function findUsaState($abbr, $flip = false)
    {
        /* From https://www.usps.com/send/official-abbreviations.htm */
        $states = array(
            'AL'=>'Alabama',
            'AK'=>'Alaska',
            'AZ'=>'Arizona',
            'AR'=>'Arkansas',
            'CA'=>'California',
            'CO'=>'Colorado',
            'CT'=>'Connecticut',
            'DE'=>'Delaware',
            'DC'=>'District of Columbia',
            'FL'=>'Florida',
            'GA'=>'Georgia',
            'HI'=>'Hawaii',
            'ID'=>'Idaho',
            'IL'=>'Illinois',
            'IN'=>'Indiana',
            'IA'=>'Iowa',
            'KS'=>'Kansas',
            'KY'=>'Kentucky',
            'LA'=>'Louisiana',
            'ME'=>'Maine',
            'MD'=>'Maryland',
            'MA'=>'Massachusetts',
            'MI'=>'Michigan',
            'MN'=>'Minnesota',
            'MS'=>'Mississippi',
            'MO'=>'Missouri',
            'MT'=>'Montana',
            'NE'=>'Nebraska',
            'NV'=>'Nevada',
            'NH'=>'New Hampshire',
            'NJ'=>'New Jersey',
            'NM'=>'New Mexico',
            'NY'=>'New York',
            'NC'=>'North Carolina',
            'ND'=>'North Dakota',
            'OH'=>'Ohio',
            'OK'=>'Oklahoma',
            'OR'=>'Oregon',
            'PA'=>'Pennsylvania',
            'RI'=>'Rhode Island',
            'SC'=>'South Carolina',
            'SD'=>'South Dakota',
            'TN'=>'Tennessee',
            'TX'=>'Texas',
            'UT'=>'Utah',
            'VT'=>'Vermont',
            'VA'=>'Virginia',
            'WA'=>'Washington',
            'WV'=>'West Virginia',
            'WI'=>'Wisconsin',
            'WY'=>'Wyoming',
            'AE'=>'ARMED FORCES AFRICA \ CANADA \ EUROPE \ MIDDLE EAST',
            'AA'=>'ARMED FORCES AMERICA (EXCEPT CANADA)',
            'AP'=>'ARMED FORCES PACIFIC'
        );
        if($flip){
            $states = array_flip($states);
        }
        if(array_key_exists($abbr, $states)){
            return $states[$abbr];
        }
        return $abbr;
    }
}
