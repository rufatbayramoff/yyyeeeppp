<?php namespace lib\payment;

use common\components\Emailer;
use common\components\PaymentExchangeRateConverter;
use common\models\PaymentbtTransaction;
use \common\models\StoreOrder;
use \common\models\Payment;
use \common\models\PaymentTransaction;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderPosition;
use \common\models\User;
use \common\models\UserTaxInfo;
use common\modules\payment\components\PaymentDoubleEntry;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\payment\components\TAccount;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\modules\payment\gateways\vendors\BankInvoiceGateway;
use common\modules\payment\services\PaymentService;
use common\modules\payment\services\RefundService;
use lib\money\Currency;
use lib\money\Money;
use Yii;
use common\modules\payment\taxes\Tax;
use \common\models\PaymentDetail;
use common\models\StorePricer;
use yii\base\UserException;
use yii\helpers\VarDumper;


/**
 * PaymentManagerFacade - helps to work with PaymentManager object
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaymentManagerFacade
{

    /**
     * Payment to EasyPost for order
     *
     * @param  StoreOrder $order
     * @param  $usdAmount
     * @return int payment id
     *
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @deprecated
     */
    public static function addEasyPostPayment(StoreOrder $order, $usdAmount)
    {
        if (empty($order->payment)) {
            throw new \yii\base\UserException(_t("site.payment", "No payment found for order {orderId}", ['orderId' => $order->id]));
        }
        $converter = Yii::createObject(PaymentExchangeRateConverter::class);
        $xrate = $converter->getLastPaymentExchangeRate();

        $description = "Payment to EasyPost for the order number #{$order->id}";

        return PaymentManager::init($xrate)
            ->withPayment($order->payment)
            ->addDetails([
                "user_id"     => User::USER_TS,
                "amount"      => -$usdAmount,
                'created_at'  => dbexpr("NOW()"),
                "type"        => PaymentDetail::TYPE_PAYMENT,
                'description' => $description
            ])
            ->addDetails([
                "user_id"     => User::USER_EASYPOST,
                "amount"      => $usdAmount,
                'created_at'  => dbexpr("NOW()"),
                "type"        => PaymentDetail::TYPE_PAYMENT,
                'description' => $description
            ])
            ->submit();
    }

    /******
     * User pays for model, we get money from user braintree, and put them to current user
     * (1 point in document)
     *
     * @param int $userId
     * @param int $fromUserId (\common\models\User::USER_BRAINTREE, USER_STRIPE
     * @param int $orderId
     * @param int $amount
     * @param string $currency
     * @return boolean
     * @throws \yii\db\Exception
     * @throws \yii\base\UserException
     * @throws \lib\payment\exception\PaymentManagerException
     * @deprecated
     *
     * *****
     */
    public static function reserveOrderPayment($userId, $fromUserId, $orderId, $amount, $currency)
    {
        $converter = Yii::createObject(PaymentExchangeRateConverter::class);
        $xrate = $converter->getLastPaymentExchangeRate();
        $result = PaymentManager::init($xrate)
            ->addPayment([
                'created_at'  => dbexpr("NOW()"),
                'description' => 'Order #' . $orderId,
                'status'      => Payment::STATUS_PENDING
            ])->addDetails([
                'original_amount'   => -$amount, // minus from braintree
                'original_currency' => $currency,
                'user_id'           => $fromUserId,
                'description'       => 'Order #' . $orderId,
                'type'              => PaymentDetail::TYPE_ORDER
            ])->addDetails([
                'original_amount'   => $amount, // + to user account
                'original_currency' => $currency,
                'user_id'           => $userId,
                'description'       => 'Order #' . $orderId,
                'type'              => PaymentDetail::TYPE_ORDER
            ])->submit();
        return $result;
    }

    /**
     * @param $toUserId
     * @param $fromUserId
     * @param $orderId
     * @param $amount
     * @param $currency
     * @return int
     * @deprecated
     */
    public static function reserveBalanceOrderPayment($toUserId, $fromUserId, $orderId, $amount, $currency)
    {
        $converter = Yii::createObject(PaymentExchangeRateConverter::class);
        $xrate = $converter->getLastPaymentExchangeRate();
        $result = PaymentManager::init($xrate, false)// !StoreOrder::findByPk($orderId)->getIsTestOrder())
        ->addPayment([
            'created_at'  => dbexpr("NOW()"),
            'description' => 'Order #' . $orderId,
            'status'      => Payment::STATUS_PENDING
        ])->addDetails([
            'original_amount'   => -$amount, // minus from braintree
            'original_currency' => $currency,
            'user_id'           => $fromUserId,
            'description'       => 'Order #' . $orderId,
            'type'              => PaymentDetail::TYPE_PAYMENT
        ])->addDetails([
            'original_amount'   => $amount, // + to user account
            'original_currency' => $currency,
            'user_id'           => $toUserId,
            'description'       => 'Order #' . $orderId,
            'type'              => PaymentDetail::TYPE_ORDER
        ])->submit();
        return $result;
    }


    /**
     *
     * @param \lib\payment\PaymentManager $paymentManager
     * @param StoreOrder $order
     * @param User $machineOwner
     * @param StorePricer $shippingPrice
     * @return PaymentManager
     */
    private static function addOrderMachineDelivery(PaymentManager $paymentManager, $order, User $machineOwner, StorePricer $shippingPrice)
    {
        $orderDetails = $order->getOrderDeliveryDetails();
        if ($orderDetails['carrier'] == \common\models\DeliveryType::CARRIER_MYSELF) {
            $paymentManager->addDetails([ // minus from TS              
                "user_id"           => User::USER_TS,
                "original_amount"   => -$shippingPrice->price,
                "original_currency" => $shippingPrice->currency,
                "rate_id"           => $shippingPrice->rate_id,
                'description'       => "Shipping fee for order #" . $order->id,
                "type"              => PaymentDetail::TYPE_PAYMENT
            ])->addDetails([ // plus to PS
                "user_id"           => $machineOwner->id,
                'description'       => "Shipping fee for order #" . $order->id,
                "original_amount"   => $shippingPrice->price,
                "original_currency" => $shippingPrice->currency,
                "rate_id"           => $shippingPrice->rate_id,
                "type"              => PaymentDetail::TYPE_PAYMENT
            ]);
        }
        return $paymentManager;
    }

    /**
     * add free order delivery details to payment
     *
     * @param \lib\payment\PaymentManager $paymentManager
     * @param StoreOrder $order
     * @param User $machineOwner
     * @return PaymentManager
     */
    private static function addOrderFreeDelivery(PaymentManager $paymentManager, StoreOrder $order, User $machineOwner)
    {
        /**
         * @var PaymentDetail $deliveryPrice
         */
        $deliveryPrice = PaymentDetail::findOne([
            'user_id'    => User::USER_EASYPOST,
            'payment_id' => $order->payment_id
        ]);
        Yii::info("free delivery by PS", 'order');
        if ($deliveryPrice) {
            Yii::info("PS delivery price minus " . $deliveryPrice->original_amount, 'order');
            $paymentManager->addDetails([ // minus from machine
                "user_id"           => $machineOwner->id,
                "original_amount"   => -$deliveryPrice->original_amount,
                "original_currency" => $deliveryPrice->original_currency,
                "rate_id"           => $deliveryPrice->rate_id,
                'description'       => "Free delivery",
                "type"              => PaymentDetail::TYPE_PAYMENT
            ])->addDetails([ // plus to TS
                "user_id"           => User::USER_TS,
                "amount"            => $deliveryPrice->amount,
                "original_amount"   => $deliveryPrice->original_amount,
                "original_currency" => $deliveryPrice->original_currency,
                "rate_id"           => $deliveryPrice->rate_id,
                "type"              => PaymentDetail::TYPE_PAYMENT
            ]);
        }
        return $paymentManager;
    }


    /**
     * @param PaymentManager $paymentManager
     * @param StoreOrder $order
     * @param User $machineOwner
     * @param StorePricer $packagePrice
     * @return PaymentManager
     */
    private static function addPacking(PaymentManager $paymentManager, StoreOrder $order, User $machineOwner, StorePricer $packagePrice): PaymentManager
    {
        $description = "Parcel package for order #{$order->id}";

        $paymentManager->addDetails([ // minus from TS
            "user_id"           => User::USER_TS,
            "original_amount"   => -$packagePrice->price,
            "original_currency" => $packagePrice->currency,
            "rate_id"           => $packagePrice->rate_id,
            'description'       => $description,
            "type"              => PaymentDetail::TYPE_PAYMENT
        ])->addDetails([ // plus to PS
            "user_id"           => $machineOwner->id,
            'description'       => $description,
            "original_amount"   => $packagePrice->price,
            "original_currency" => $packagePrice->currency,
            "rate_id"           => $packagePrice->rate_id,
            "type"              => PaymentDetail::TYPE_PAYMENT
        ]);

        return $paymentManager;
    }

    private static function addProduct(PaymentManager $paymentManager, StoreOrder $order, User $machineOwner, StorePricer $productPrice): PaymentManager
    {
        $description = "Product price for order #{$order->id}";

        $paymentManager->addDetails([ // minus from TS
            "user_id"           => User::USER_TS,
            "original_amount"   => -$productPrice->price,
            "original_currency" => $productPrice->currency,
            "rate_id"           => $productPrice->rate_id,
            'description'       => $description,
            "type"              => PaymentDetail::TYPE_PAYMENT
        ])->addDetails([ // plus to PS
            "user_id"           => $machineOwner->id,
            'description'       => $description,
            "original_amount"   => $productPrice->price,
            "original_currency" => $productPrice->currency,
            "rate_id"           => $productPrice->rate_id,
            "type"              => PaymentDetail::TYPE_PAYMENT
        ]);

        return $paymentManager;
    }

    /**
     * @param PaymentManager $paymentManager
     * @param StoreOrder $order
     * @param User $machineOwner
     * @param StorePricer $addonPrice
     * @return PaymentManager
     * @deprecated
     */
    private static function addAddon(
        PaymentManager $paymentManager,
        StoreOrder $order,
        User $machineOwner,
        StorePricer $addonPrice
    ): PaymentManager {
        foreach ($order->storeOrderPositions as $addonService) {
            if ($addonService->status != StoreOrderPosition::STATUS_PAID) {
                continue;
            }
            $description = _t('site.earning', "Additional service for order #{orderId}", ['orderId' => $order->id]);
            $paymentToPs = $addonService->amount - $addonService->fee_include;
            $paymentManager->addDetails([ // minus from TS
                "user_id"           => User::USER_TS,
                "original_amount"   => -$paymentToPs,
                "original_currency" => $addonService->currency_iso,
                "rate_id"           => $addonPrice->rate_id,
                'description'       => $description,
                "type"              => PaymentDetail::TYPE_PAYMENT
            ]);
            $paymentManager->addDetails([ // plus to PS
                "user_id"           => $machineOwner->id,
                'description'       => $description,
                "original_amount"   => $paymentToPs,
                "original_currency" => $addonService->currency_iso,
                "rate_id"           => $addonPrice->rate_id,
                "type"              => PaymentDetail::TYPE_PAYMENT
            ]);
        }
        return $paymentManager;
    }


    private static function isSettleRequested($storeOrder)
    {
        $paymentTransaction = self::findOrderPaymentTransaction($storeOrder->id);
        $settleRequestStatus = [PaymentGatewayTransaction::STATUS_SETTLING, PaymentGatewayTransaction::STATUS_SUBMITED_SETTLE];
        return in_array($storeOrder->payment_status, $settleRequestStatus) || in_array($paymentTransaction->status, $settleRequestStatus);
    }
}
