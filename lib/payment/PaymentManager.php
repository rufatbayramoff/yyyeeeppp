<?php namespace lib\payment;

use common\models\Model3d;
use common\models\Payment;
use common\models\PaymentDetail;
use common\models\PaymentExchangeRate;
use common\models\StoreOrder;
use common\models\StorePricer;
use common\models\User;
use common\modules\payment\components\PaymentInfoHelper;
use lib\money\Currency;
use lib\payment\exception\PaymentManagerException;
use lib\payment\exception\PaymentManagerSystemException;
use lib\payment\exception\PaymentManagerUserException;
use yii\base\UserException;
use yii\db\Exception;
use \yii\helpers\ArrayHelper;
/**
 * PaymentManager
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaymentManager
{
    /**
     *
     * @var PaymentExchangeRate
     * @deprecated
     */
    private $rate;    
    /**
     *
     * @var \yii\db\Transaction
     * @deprecated
     */
    private $dbTransaction;
    private $status = "new";
    private $parentId;
    
    /**
     * payment row to add
     * 
     * @var array
     * @deprecated
     */
    private $paymentRow;
    
    /**
     *
     * @var \common\models\base\Payment
     * @deprecated
     */
    private $paymentObj;
    
    /**
     * payment details to be added with current payment transaction
     * 
     * @var array
     * @deprecated
     */
    private $paymentDetails = [];
    
    /**
     * should we check that amount is not equals to zero?
     * @var boolean
     * @deprecated
     */
    private $checkZeroAmount = true;

    /**
     * init PaymentManager with given exchange rater
     *
     * @param  PaymentExchangeRate $rate
     * @param bool $checkZeroAmount
     * @return PaymentManager
     * @deprecated
     */
    public static function init(PaymentExchangeRate $rate, $checkZeroAmount = true)
    {        
        $paymentManager = new PaymentManager();
        $paymentManager->rate = $rate;
        $paymentManager->checkZeroAmount = $checkZeroAmount;
        return $paymentManager;
    }

    /**
     * add print award
     *
     * @param StoreOrder $order
     * @param User $machineOwner
     * @param StorePricer $printPrice
     * @return PaymentManager
     * @deprecated
     */
    public function addPrintAward(StoreOrder $order, User $machineOwner, StorePricer $printPrice, $msg = '')
    {
        $this->addDetails([ // minus from TS
            "user_id" => User::USER_TS,
            "original_amount" => -$printPrice->price,
            "original_currency" => $printPrice->currency,
            "rate_id" => $printPrice->rate_id
        ])->addDetails([ // plus to machine
            "user_id" => $machineOwner->id,
            "original_amount" => $printPrice->price,
            "original_currency" => $printPrice->currency,
            "rate_id" => $printPrice->rate_id,
            'description' => !empty($msg) ? $msg : PaymentInfoHelper::getAwardPrintDescription($order->id),
            "type" => PaymentDetail::TYPE_AWARD_PRINT
        ]);
        \Yii::info(PaymentInfoHelper::getAwardPrintDescription($order->id), 'order');
        return $this;
    }

    /**
     * @param StoreOrder $order
     * @param User $machineOwner
     * @param StorePricer $printPrice
     * @param string $msg
     * @return $this
     * @deprecated
     */
    public function minusPsFeeFromPrintAward(StoreOrder $order, User $machineOwner, StorePricer $printPrice, $msg = '')
    {
        $this->addDetails(
            [ // minus from machine
                "user_id"           => $machineOwner->id,
                "original_amount"   => -$printPrice->price,
                "original_currency" => $printPrice->currency,
                "rate_id"           => $printPrice->rate_id,
                'description'       => $msg,
                "type"              => PaymentDetail::TYPE_PAYMENT
            ]
        )->addDetails(
            [ // plus to ts
                "user_id"           => User::USER_TS,
                "original_amount"   => $printPrice->price,
                "original_currency" => $printPrice->currency,
                "rate_id"           => $printPrice->rate_id,
            ]
        );
        return $this;
    }
    
    /**
     * get model award
     *
     * @param Model3d $model3d
     * @param StorePricer $modelPrice
     * @return PaymentManager
     * @deprecated
     */
    public function addModelAward(Model3d $model3d, StorePricer $modelPrice)
    {
        if (!$model3d->getAuthor()) {
            return $this;
        }
        $this->addDetails([
            'user_id' => User::USER_TS,
            "original_amount" => -$modelPrice->price,
            "original_currency" => $modelPrice->currency,
            "rate_id" => $modelPrice->rate_id,
            "type" => PaymentDetail::TYPE_PAYMENT
        ])->addDetails([
            'user_id' => $model3d->getAuthor()->id,
            "original_amount" => $modelPrice->price,
            "original_currency" => $modelPrice->currency,
            "rate_id" => $modelPrice->rate_id,
            'description' => PaymentInfoHelper::getAwardModelDescription($model3d->id),
            "type" => PaymentDetail::TYPE_AWARD_MODEL
        ]);
        \Yii::info($modelPrice->price . PaymentInfoHelper::getAwardModelDescription($model3d->id), 'order');
        return $this;
    }
    
    /**
     * Add payment info
     * 
     * @param  array $data
     * @return \lib\payment\PaymentManager
     * @deprecated
     */
    public function addPayment($data)
    {
        $paymentRow =  ArrayHelper::merge(
            [
            "created_at" => dbexpr("NOW()"),
            "status" => $this->status,
            "parent_id" => $this->parentId
            ], $data
        );
        $this->paymentRow = $paymentRow;
        return $this;
    }
    
    /**
     * 
     * @param Payment $payment
     * @return PaymentManager
     * @deprecated
     */
    public function withPayment(Payment $payment)
    {
        $this->paymentObj = $payment;
        return $this;
    }

    /**
     * add payment details
     * 
     * @param  array $dataIn
     * @return \lib\payment\PaymentManager
     * @deprecated
     */
    public function addDetails($dataIn)
    {
        $data = $this->validateAmount($dataIn);
        $data['rate_id'] = isset($data['rate_id']) ? $data['rate_id'] : $this->rate->id;
        $data['created_at'] = dbexpr("NOW()");
        $data['description'] = !empty($data['description']) ? $data['description'] : "";
        $this->paymentDetails[] = $data;
        return $this;
    }
    
    /**
     * 
     * @return array
     * @deprecated
     */
    public function getDetails()
    {
        return $this->paymentDetails;
    }

    /**
     * submit collected data to database
     *
     * @param  string $status
     * @return int $paymentId - returns payment id
     * @throws UserException
     * @throws Exception
     * @throws PaymentManagerException
     * @deprecated
     */
    public function submit($status = '')
    {
        if (count($this->paymentDetails) === 0) {
            return null;
        }
        // add payment        
        $this->preValidatePayment();
        
        $this->dbTransaction = app("db")->beginTransaction();        
        try{
            // use new payment, or already added object
            $payment = $this->paymentObj ? $this->paymentObj : Payment::addRecord($this->paymentRow);
            foreach ($this->paymentDetails as $k=>$detail) {
                $detail['payment_id'] = $payment->id;
                // check if update
                $detailId  = false;
                if(!empty($detail['id'])) {
                    $detailId = $detail['id']; unset($detail['id']);
                }
                if($detailId) {                    
                    $this->paymentDetails[$k] = PaymentDetail::updateRecord($detailId, $detail);
                }else{
                    $this->paymentDetails[$k] = PaymentDetail::addRecord($detail);
                }
            }
            if(!empty($status)) {
                $payment->status = $status;
                $payment->safeSave();
            }
            $this->validatePayment($payment->id);
            $this->dbTransaction->commit();
            //$this->reset();
            return $payment->id;
        } catch(PaymentManagerUserException $ex){
            logException($ex, "payment_manager");            
            throw new UserException($ex->getMessage());
        } catch(PaymentManagerSystemException $ex){
            logException($ex, "payment_manager");            
            throw new UserException("Payment error. Contact us to help you.");
        } catch (\Exception $ex) {
            $this->dbTransaction->rollBack();
            $data = var_export($this->paymentDetails, true);
            \Yii::warning($data, "payment_manager");
            logException($ex, "payment_manager");            
            throw new UserException("Payment exception. Please try again.");
        }   
    }

    /**
     * validate before working with db
     * 
     * @return boolean
     * @throws PaymentManagerException
     * @deprecated
     */
    private function preValidatePayment()
    {
        $sum = 0;
        if(empty($this->paymentDetails)) {
            throw new PaymentManagerException("No payment details specified");
        }
        foreach ($this->paymentDetails as $k => $v) {
            $sum = $sum + floatval($v['amount']);
        }
        if ($sum != 0) {
             throw new PaymentManagerException("Payment pre-validate error, sum must be zero, got " . $sum);
        }
        return true;
    }
    /**
     * validate amount in payments, if original not set, assume that USD is used as default
     * 
     * @param  array $data
     * @return array
     * @throws PaymentManagerException
     * @deprecated
     */
    private function validateAmount($data)
    {
        if (!isset($data['original_amount'])) {
            $data['original_amount'] = $data['amount'];
            $data['original_currency'] = "USD";
        }
        if ($data['original_currency'] == "USD") {
            $amount = $data["original_amount"];
        } else {
            $rateObj = $this->rate;
            if(!empty($data['rate_id'])) {
                $rateObj = PaymentExchangeRate::tryFindByPk($data['rate_id'], "xRate not found " . $data['rate_id']);
            }
            $amount = $rateObj->convert($data['original_amount'], $data['original_currency'], Currency::USD);
        }
        if(empty($amount) && $this->checkZeroAmount) {
            throw new PaymentManagerException("Zero amount not allowed for payment");
        }
        $data['amount'] = $amount;
        return $data;
    }

    /**
     * validate payment details, total sum must be always = 0
     *
     * @param  int $paymentId
     * @return bool
     * @throws PaymentManagerException
     * @deprecated
     */
    private function validatePayment($paymentId)
    {
        $command = app("db")->createCommand("SELECT sum(amount) FROM payment_detail WHERE payment_id=$paymentId");
        $sum = (int)$command->queryScalar();
        if($sum!=0) {
            throw new PaymentManagerException("Payment validate error! Payment details sum must be zero, got " . $sum);
        }
        return true;
    }
}
