<?php
/**
 * Date: 30.06.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\api;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use function GuzzleHttp\Psr7\stream_for;

class ApiClient
{

    /**
     * http client for rest requests
     * @var Client $httpClient
     */
    private $httpClient;

    /**
     * user
     * @var string
     */
    protected $authUser;

    /**
     * password
     * @var string
     */
    protected $authPassword;

    /**
     * @var ApiFiles
     */
    public $files;

    /**
     * API URL
     * @var string
     */
    private $url;


    /**
     * ApiClient constructor.
     *
     * @param $username
     * @param $password
     * @param string $url
     */
    public function __construct($username, $password, $url = 'https://test.treatstock.com/api/v1/')
    {
        $this->url = $url;
        $this->setHttpClient(new Client());
        $this->files = new ApiFiles($this);
        $this->authUser = $username;
        $this->authPassword = $password;
    }

    /**
     * @param $client
     */
    public function setHttpClient($client)
    {
        $this->httpClient = $client;
    }

    /**
     * @param $endpoint
     * @param $json
     * @return mixed
     */
    public function post($endpoint, $json)
    {
        $response = $this->httpClient->request(
            'POST',
            $this->url . $endpoint,
            [
                'json'    => $json,
                'auth'    => $this->getAuth(),
                'headers' => [
                    'Accept' => 'application/json'
                ]
            ]
        );
        return $this->handleResponse($response);
    }

    /**
     * @param $endpoint
     * @param $json
     * @return mixed
     */
    public function delete($endpoint, $json)
    {
        $response = $this->httpClient->request(
            'DELETE',
            $this->url . $endpoint,
            [
                'json'    => $json,
                'auth'    => $this->getAuth(),
                'headers' => [
                    'Accept' => 'application/json'
                ]
            ]
        );
        return $this->handleResponse($response);
    }

    /**
     * @param $endpoint
     * @param $query
     * @return mixed
     */
    public function get($endpoint, $query)
    {
        $response = $this->httpClient->request(
            'GET',
            $this->url . $endpoint,
            [
                'query'   => $query,
                'auth'    => $this->getAuth(),
                'headers' => [
                    'Accept' => 'application/json'
                ]
            ]
        );
        return $this->handleResponse($response);
    }

    /**
     * @return array
     */
    public function getAuth()
    {
        return [$this->authUser, $this->authPassword];
    }

    /**
     * @param Response $response
     * @return mixed
     */
    private function handleResponse(Response $response)
    {
        $stream = stream_for($response->getBody());
        $data = json_decode($stream->getContents(), true);
        return $data;
    }
}