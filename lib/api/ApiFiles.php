<?php
/**
 * Date: 30.06.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\api;


class ApiFiles
{
    private $apiClient;

    public function __construct($apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function create($options)
    {

        return $this->apiClient->post('files', $options);
    }

    public function getFiles($options)
    {
        return $this->apiClient->get('files', $options);
    }
}