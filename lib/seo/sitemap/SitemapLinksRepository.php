<?php
/**
 * Date: 23.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap;

/**
 * Class SitemapLinksProvider
 *
 * @package lib\seo\sitemap
 */
class SitemapLinksRepository
{
    /**
     * array of links for sitemap
     * @var array
     */
    public $links = [];

    /**
     * @param SitemapLink $link
     */
    public function add(SitemapLink $link)
    {
        $this->links[$link->getUrl()] = $link;
    }

    /**
     * get array of links for sitemap
     *
     * @return SitemapLink[]
     */
    public function getAll()
    {
        return $this->links;
    }
}