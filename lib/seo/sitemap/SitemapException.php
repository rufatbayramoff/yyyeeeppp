<?php
/**
 * Date: 24.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap;

use yii\base\Exception;

class SitemapException extends Exception
{

}