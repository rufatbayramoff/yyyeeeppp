<?php


/**
 * Date: 23.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
namespace lib\seo\sitemap;

use common\components\FileDirHelper;
use yii\base\BaseObject;
use lib\seo\sitemap\provider\CatalogLinkProvider;
use lib\seo\sitemap\provider\HelpLinkProvider;
use lib\seo\sitemap\provider\UserLinkProvider;
use lib\seo\sitemap\provider\BlogLinkProvider;
use lib\seo\sitemap\provider\Model3dLinkProvider;
use lib\seo\sitemap\provider\StaticPageLinkProvider;

/**
 * Class SitemapConfig
 *
 * @package lib\seo\sitemap
 */
class SitemapConfig extends BaseObject
{
    /**
     * output dir for sitemap.xml file
     *
     * @var string
     */
    public $outputDir;

    /**
     * what host should be used before links
     *
     * @var string
     */
    public $host;

    /**
     * list of link providers for sitemap
     *
     * @var array
     */
    public $providersList = [
        StaticPageLinkProvider::class,
        BlogLinkProvider::class,
        CatalogLinkProvider::class,
        Model3dLinkProvider::class,
        HelpLinkProvider::class,
        UserLinkProvider::class,
    ];


    /**
     * init vars
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        if ($this->outputDir === null) {
            $this->outputDir = \Yii::getAlias('@frontend/web/');
        }
        if ($this->host === null) {
            $this->host = 'https://www.treatstock.com';
        }
    }

    /**
     * @return string
     */
    public function getOutputDir()
    {
        $dir =  $this->outputDir;
        FileDirHelper::createDir($dir);
        return $dir;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return array
     */
    public function getProvidersList()
    {
        return $this->providersList;
    }
}