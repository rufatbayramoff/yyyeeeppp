<?php
/**
 * Date: 23.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap;


use lib\sitemap\Sitemap;
use yii\base\BaseObject;

/**
 * Class SitemapLink
 *
 * @package lib\seo\sitemap
 */
class SitemapLink extends BaseObject
{
    /**
     * url without host prefix
     *
     * @var string
     */
    public $url;

    /**
     * validate url frequency
     *
     * @var string
     */
    public $validFrequency = Sitemap::WEEKLY;

    /**
     * link priority
     *
     * @var float
     */
    public $priority = 0.3;

    /**
     * @var SitemapImageItem[]
     */
    public $images;

    /**
     *
     * @throws \lib\seo\sitemap\SitemapException
     */
    public function init()
    {
        if (empty($this->getUrl())) {
            throw new SitemapException('URL is empty');
        }
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $url = $this->url;
        $url = str_replace('/?', '?', $url);
        $url = rtrim($url, '/');
        return mb_strtolower($url);
    }

    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return string
     */
    public function getValidFrequency()
    {
        return $this->validFrequency;
    }

    /**
     *
     * @return float
     */
    public function getPriority()
    {
        return $this->priority;
    }
}