<?php
/**
 * Date: 24.10.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap;


class SitemapImageItem
{
    /**
     * image location
     *
     * @var string
     */
    public $loc;

    /**
     * The caption of the image.
     *
     * @var string
     */
    public $caption;

    /**
     * The title of the image.
     *
     * @var string
     */
    public $title;
}