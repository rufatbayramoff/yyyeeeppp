<?php
/**
 * Date: 23.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap\provider;


use common\models\BlogPost;
use lib\seo\sitemap\SitemapLink;
use lib\sitemap\Sitemap;
use yii\helpers\Url;

/**
 * Class BlogLinkProvider
 *
 * @package lib\seo\sitemap\provider
 */
class BlogLinkProvider extends AbstractProvider
{
    /**
     * @var string
     */
    public $validFrequency = Sitemap::DAILY;

    /**
     * @var string
     */
    public $priority = '0.5';

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function populate()
    {
        $blogPosts = BlogPost::find()->active()->orderBy('date DESC')->asArray()->all();

        foreach ($blogPosts as $k => $post) {
            $url = Url::toRoute(['/blog/view', 'alias' => $post['alias']]);
            $link = new SitemapLink(
                [
                    'url'            => $url,
                    'validFrequency' => $this->validFrequency,
                    'priority'       => $this->priority
                ]
            );
            $this->repo->add($link);
        }
    }
}