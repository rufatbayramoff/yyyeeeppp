<?php
/**
 * Date: 23.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap\provider;

use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use frontend\components\UserSessionFacade;
use lib\seo\sitemap\SitemapLink;
use lib\sitemap\Sitemap;
use yii\helpers\Url;

class StaticPageLinkProvider extends AbstractProvider
{
    /**
     * @var string
     */
    public $validFrequency = Sitemap::WEEKLY;

    /**
     * @var string
     */
    public $priority = '0.3';

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function populate()
    {
        $urls = [
            Url::toRoute(['site/about']),
            Url::toRoute(['site/return-policy']),
            Url::toRoute(['site/policy']),
            Url::toRoute(['site/help']),
            Url::toRoute(['site/terms']),
            Url::toRoute(['site/contact']),
            Url::toRoute(['/blog']),
            Url::toRoute(['store/store/index/']),
            ['url'=>'https://www.treatstock.com/'.CatalogPrintingUrlHelper::landingPage(), 'priority'=>'0.7', 'freq'=>Sitemap::MONTHLY],
        ];

        foreach ($urls as $url) {
            $freq = $this->validFrequency;
            $priority = $this->priority;
            if(is_array($url)){
                $priority = $url['priority'];
                $freq = $url['freq'];
                $url = $url['url'];
            }
            $link = new SitemapLink(
                [
                    'url'            => $url,
                    'validFrequency' => $freq,
                    'priority'       => $priority
                ]
            );
            $this->repo->add($link);
        }
    }
}