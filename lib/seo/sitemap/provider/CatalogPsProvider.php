<?php
/**
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap\provider;


use common\models\BlogPost;
use lib\seo\sitemap\SitemapLink;
use lib\sitemap\Sitemap;
use yii\helpers\Url;

/**
 *
 * @package lib\seo\sitemap\provider
 */
class CatalogPsProvider extends AbstractProvider
{
    /**
     * @var string
     */
    public $validFrequency = Sitemap::DAILY;

    /**
     * @var string
     */
    public $priority = '0.5';

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function populate()
    {

        $link = new SitemapLink(
            [
                'url'            => '3d-printing-services',
                'validFrequency' => $this->validFrequency,
                'priority'       => $this->priority
            ]
        );
        $this->repo->add($link);
    }
}