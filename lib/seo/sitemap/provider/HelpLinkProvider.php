<?php
/**
 * Date: 24.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap\provider;

use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use lib\seo\sitemap\SitemapLink;
use lib\sitemap\Sitemap;
use yii\helpers\Url;

/**
 *
 * @package lib\seo\sitemap\provider
 */
class HelpLinkProvider extends AbstractProvider
{

    /**
     * @var string
     */
    public $validFrequency = Sitemap::WEEKLY;

    /**
     * @var string
     */
    public $priority = '0.5';

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function populate()
    {

        $categoryRows = SiteHelpCategory::find()->all();

        foreach ($categoryRows as $k => $row) {
            $url = Url::toRoute(['help/category', 'id'=>$row->id, 'slug'=>$row->getSlug()]);
            $link = new SitemapLink(
                [
                    'url'            => $url,
                    'validFrequency' => $this->validFrequency,
                    'priority'       => $this->priority
                ]
            );
            $this->repo->add($link);
        }
        $helpRows = SiteHelp::find()->where(['is_active'=>1])->all();
        foreach ($helpRows as $k => $row) {
            $url = Url::toRoute(['help/article', 'id'=>$row['id'], 'alias'=>$row->getSlug()]);
            $link = new SitemapLink(
                [
                    'url'            => $url,
                    'validFrequency' => $this->validFrequency,
                    'priority'       => $this->priority
                ]
            );
            $this->repo->add($link);
        }
    }
}