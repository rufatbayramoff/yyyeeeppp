<?php
/**
 * Date: 23.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap\provider;

use lib\seo\sitemap\SitemapLink;
use lib\seo\sitemap\SitemapLinksRepository;
use lib\sitemap\Sitemap;

/**
 * Class AbstractProvider
 *
 * @package lib\seo\sitemap\provider
 */
abstract class AbstractProvider
{
    /**
     * @var SitemapLinksRepository
     */
    public $repo;

    /**
     * @var string
     */
    public $validFrequency = Sitemap::WEEKLY;

    /**
     * @var string
     */
    public $priority = '0.5';

    public $host;
    /**
     * AbstractProvider constructor.
     *
     * @param SitemapLinksRepository $repo
     */
    public function __construct(SitemapLinksRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @return SitemapLink[]
     */
    public function getLinks()
    {
        return $this->repo->getAll();
    }

    public function getProviderCode()
    {
        return str_replace("lib\\seo\\sitemap\\provider\\", "", get_called_class());
    }
    /**
     * need to override by children to populate SitemapLinksRepository
     */
    public abstract function populate();
}