<?php
/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap\provider;

use common\models\SeoPage;
use lib\seo\sitemap\SitemapLink;
use lib\sitemap\Sitemap;

/**
 *
 * @package lib\seo\sitemap\provider
 */
class SeoPageSitemapProvider extends AbstractProvider
{

    /**
     * @var string
     */
    public $validFrequency = Sitemap::WEEKLY;

    /**
     * @var string
     */
    public $priority = '1.0';

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function populate()
    {
        $sitemapSeoPages = SeoPage::find()->where(['is_active'=>true, 'is_sitemap'=>true])->all();
        foreach ($sitemapSeoPages as $k => $row) {
            /* @var $row SeoPage */
            $host = !empty($this->host) ? $this->host : param('server');
            $url = $host . '/'.$row->url;
            $link = new SitemapLink(
                [
                    'url'            => $url,
                    'validFrequency' => $this->validFrequency,
                    'priority'       => $this->priority
                ]
            );
            $this->repo->add($link);
        }
    }
}