<?php
/**
 * Date: 24.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap\provider;


use common\models\Company;
use common\modules\product\interfaces\ProductInterface;
use common\models\Ps;
use common\models\StoreUnit;
use common\models\User;
use frontend\components\UserUtils;
use frontend\models\ps\PsFacade;
use lib\seo\sitemap\SitemapLink;
use lib\sitemap\Sitemap;

/**
 * Class BlogLinkProvider
 *
 * @package lib\seo\sitemap\provider
 */
class UserLinkProvider extends AbstractProvider
{
    /**
     * @var string
     */
    public $validFrequency = Sitemap::WEEKLY;

    /**
     * @var string
     */
    public $priority = '0.3';

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function populate()
    {
        $users = User::find()->notSystem()->onlyConfirmed()->active()->notDeveloperAccounts()->notHiddenProfile()->all();

        /** @var array $users */
        foreach ($users as $k => $user) {
            // check if ps or author model3d
            $this->populateByPs($user);
            $this->populateByModel3dAuthor($user);
        }
    }

    /**
     * @param $user
     * @throws \yii\base\InvalidParamException
     */
    private function populateByPs($user)
    {
        $ps = Ps::findOne(['user_id' => $user['id']]);
        if (!$ps || $ps->getIsRejected() || !$ps->isActive()) {
            return;
        }
        $psPrintersCount = $ps->getNotDeletedPrinters()->moderated()->visible()->count();
        if ($psPrintersCount > 0) {
            $url = PsFacade::getPsLink($ps);
            $link = new SitemapLink(
                [
                    'url'            => $url,
                    'validFrequency' => $this->validFrequency,
                    'priority'       => $this->priority
                ]
            );
            $this->repo->add($link);
        }
    }

    /**
     * @param User $user
     * @throws \yii\base\InvalidParamException
     */
    private function populateByModel3dAuthor($user)
    {
        if (!$user->company || !$user->company->isActive()) {
            return;
        }
        $storeUnitsCount = StoreUnit::find()->select(['model3d.id'])->where(
            [
                'product_common.is_active'       => 1,
                'product_common.product_status'  => ProductInterface::STATUS_PUBLISHED_PUBLIC,
                'product_common.user_id' => $user['id']
            ]
        )
            ->innerJoin('model3d', 'model3d.id=store_unit.model3d_id')
            ->innerJoin('product_common',"product_common.uid=model3d.product_common_uid")
            ->count();

        if ($storeUnitsCount > 0) {
            $url = UserUtils::getUserStoreUrl($user);
            $link = new SitemapLink(
                [
                    'url'            => $url,
                    'validFrequency' => $this->validFrequency,
                    'priority'       => $this->priority
                ]
            );
            $this->repo->add($link);
        }
    }
}