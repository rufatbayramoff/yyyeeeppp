<?php
/**
 * Date: 23.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap\provider;

use common\modules\product\interfaces\ProductInterface;
use common\models\Model3dImg;
use common\models\StoreUnit;
use frontend\models\model3d\Model3dFacade;
use lib\seo\sitemap\SitemapImageItem;
use lib\seo\sitemap\SitemapLink;
use lib\sitemap\Sitemap;
use yii\helpers\Inflector;
use yii\helpers\Url;

/**
 * Class Model3dLinkProvider
 *
 * @package lib\seo\sitemap\provider
 */
class Model3dLinkProvider extends AbstractProvider
{

    /**
     * @var string
     */
    public $validFrequency = Sitemap::WEEKLY;

    /**
     * @var string
     */
    public $priority = '0.6';

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function populate()
    {
        $rows = StoreUnit::find()
            ->published()
            ->all();

        foreach ($rows as $k => $unit) {
            $slug = Inflector::slug($unit->model3d->getTitleLabel());
            $host = !empty($this->host) ? $this->host : param('server');
            $url = $host.'/3d-printable-models/'.$unit['model3d_id'].'-'.$slug;
            $link = new SitemapLink(
                [
                    'url'            => $url,
                    'validFrequency' => $this->validFrequency,
                    'priority'       => $this->priority,
                ]
            );
            $this->repo->add($link);
        }
    }

    private function getImages($model3dId)
    {
        $images = Model3dImg::findAll(['model3d_id'=>$model3dId]);
        $result = [];
        foreach($images as $image){
            $img = new SitemapImageItem();
            $img->title = '';
            $img->loc = '';
            $img->caption = '';
            $result[] = $img;
        }
        return $result;
    }
}