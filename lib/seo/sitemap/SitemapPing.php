<?php
/**
 * Date: 24.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap;


class SitemapPing
{
    public function pingGoogle($link)
    {
        $result = file_get_contents("http://www.google.com/webmasters/sitemaps/ping?sitemap=$link");
        return $result;
    }
}