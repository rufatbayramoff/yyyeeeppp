<?php

/**
 * Date: 23.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
namespace lib\seo\sitemap;

use common\models\SystemLang;
use lib\seo\sitemap\provider\AbstractProvider;
use lib\sitemap\Index;

class SitemapBuilder
{


    /**
     * @var AbstractProvider[]
     */
    public $providers = [];

    /**
     * populate data in providers
     *
     * @param SitemapConfig $config
     */
    public function initProviders(SitemapConfig $config)
    {
        foreach ($config->getProvidersList() as $class) {
            $provider = new $class(new SitemapLinksRepository());
            $provider->host = $config->host;
            /** @var $provider AbstractProvider */
            $provider->populate();
            $this->providers[$class] = $provider;
        }
    }

    /**
     * @return AbstractProvider[]
     */
    public function getProviders()
    {
        if (empty($this->providers)) {
            $this->initProviders();
        }
        return $this->providers;
    }

    /**
     * @param SitemapConfig $config
     * @return array
     * @throws \InvalidArgumentException
     */
    public function build(SitemapConfig $config)
    {
        /** @var SystemLang[] $langs */
        $langs = SystemLang::find()->where('is_active=1')->all();

        foreach ($langs as $lang) {
            $langDomain = \Yii::$app->getModule('intlDomains')->domainManager->getDomainForLang($lang->iso_code);
            $config->host = param('httpScheme').$langDomain;
            \Yii::$app->urlManager->setBaseUrl( param('httpScheme').$langDomain);

            $this->initProviders($config); // TODO: make links without domains

            $index = new Index($config->getOutputDir() . '/sitemap_index_'.$lang->iso_code.'.xml');
            $sitemap = new SitemapWithImages($config->getOutputDir() . '/sitemap_'.$lang->iso_code.'.xml', $lang);

            $result = [];
            foreach ($this->getProviders() as $provider) {
                $allLinks = $provider->getLinks();
                $result[$provider->getProviderCode()] = count($allLinks);
                foreach ($allLinks as $link) {
                    $sitemap->addItem($link->getUrl(), time(), $link->getValidFrequency(), $link->getPriority(), $link->getImages());
                }
            }
            $sitemap->write();

            $sitemapFileUrls = $sitemap->getSitemapUrls($config->getHost() . '/sitemap-xml/');
            foreach ($sitemapFileUrls as $sitemapUrl) {
                $index->addSitemap($sitemapUrl);
            }
            $index->write();
        }

        return $result;
    }
}