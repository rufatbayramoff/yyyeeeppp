<?php
/**
 * Date: 24.10.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\seo\sitemap;

use common\components\UrlHelper;
use common\models\SystemLang;
use lib\sitemap\Sitemap;
use Yii;

class SitemapWithImages extends Sitemap
{
    /**
     * @var SystemLang[]
     */
    protected $langs;

    /**
     * @var SystemLang
     */
    protected $currentlang;

    /**
     * @param string $filePath path of the file to write to
     * @throws \InvalidArgumentException
     */
    public function __construct($filePath, $currentLang)
    {
        $this->langs = SystemLang::find()->where('is_active=1')->all();
        $this->currentlang = $currentLang;

        $dir = dirname($filePath);
        if (!is_dir($dir)) {
            throw new \InvalidArgumentException(
                "Please specify valid file path. Directory not exists. You have specified: {$dir}."
            );
        }

        $this->filePath = $filePath;
        parent::__construct($filePath);
    }

    /**
     * Finishes writing
     */
    public function write()
    {
        $this->finishFile();
    }


    public function writeAlternateLangs($location)
    {
        $currentDomain = Yii::$app->getModule('intlDomains')->domainManager->getDomainForLang($this->currentlang->iso_code);
        foreach ($this->langs as $lang) {
            $langDomain = Yii::$app->getModule('intlDomains')->domainManager->getDomainForLang($lang->iso_code);
            if ($currentDomain!=$langDomain) {
                $newlocation = UrlHelper::replaceDomain($location, $langDomain);
                $attributes = [
                    'rel'      => 'alternate',
                    'hreflang' => $lang->iso_code,
                    'href'     => $newlocation
                ];
                $this->writer->startElement('xhtml:link');
                foreach ($attributes as $attributeName => $attributeValue) {
                    $this->writer->writeAttribute($attributeName, $attributeValue);
                }
                $this->writer->endElement();
            }
        }
    }

    /**
     * Adds a new item to sitemap
     *
     * @param string $location location item URL
     * @param integer $lastModified last modification timestamp
     * @param float $changeFrequency change frequency. Use one of self:: constants here
     * @param string $priority item's priority (0.0-1.0). Default null is equal to 0.5
     *
     * @param array $images
     * @throws \InvalidArgumentException
     */
    public function addItem($location, $lastModified = null, $changeFrequency = null, $priority = null, array $images = null)
    {
        if ($this->urlsCount === 0) {
            $this->createNewFile();
        } elseif ($this->urlsCount % $this->maxUrls === 0) {
            $this->finishFile();
            $this->createNewFile();
        }

        if ($this->urlsCount % $this->bufferSize === 0) {
            $this->flush();
        }
        $this->writer->startElement('url');

        if (false === filter_var($location, FILTER_VALIDATE_URL)) {
            //throw new \InvalidArgumentException("The location must be a valid URL. You have specified: {$location}.");
        }

        $this->writeAlternateLangs($location);

        $this->writer->writeElement('loc', $location);
        if ($lastModified !== null) {
            $this->writer->writeElement('lastmod', date('c', $lastModified));
        }

        if ($changeFrequency !== null) {
            if (!in_array($changeFrequency, $this->validFrequencies, true)) {
                throw new \InvalidArgumentException(
                    'Please specify valid changeFrequency. Valid values are: '
                    . implode(', ', $this->validFrequencies)
                    . "You have specified: {$changeFrequency}."
                );
            }
            $this->writer->writeElement('changefreq', $changeFrequency);
        }

        if ($priority !== null) {
            if (!is_numeric($priority) || $priority < 0 || $priority > 1) {
                throw new \InvalidArgumentException(
                    "Please specify valid priority. Valid values range from 0.0 to 1.0. You have specified: {$priority}."
                );
            }
            $this->writer->writeElement('priority', number_format($priority, 1, '.', ','));
        }

        if(!empty($images)){
            $this->writer->startElement('image:image');
            foreach($images as $k=>$image){
                /** @var $image SitemapImageItem */
                $this->writer->writeElement('image:loc', $image->loc);
                if(!empty($image->caption)){
                    $this->writer->writeElement('image:caption', $image->caption);
                }
                if(!empty($image->title)){
                    $this->writer->writeElement('image:title', $image->title);
                }

            }
            $this->writer->endElement();
        }
        $this->writer->endElement();
        $this->urlsCount++;
    }

    /**
     * @return string path of currently opened file
     */
    protected function getCurrentFilePath()
    {
        if ($this->fileCount < 2) {
            return $this->filePath;
        }

        $parts = pathinfo($this->filePath);
        return $parts['dirname'] . DIRECTORY_SEPARATOR . $parts['filename'] . '_' . $this->fileCount . '.' . $parts['extension'];
    }

}