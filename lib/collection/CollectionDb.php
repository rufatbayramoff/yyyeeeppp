<?php
namespace lib\collection;
/**
 * Class with functions to work with arrays
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class CollectionDb
{
    /**
     * Gets column values from array
     * 
     * @param  array  $data   - data from db
     * @param  string $column - column in array
     * @return array
     */
    public static function getColumn($data, $column)
    {
        $result = [];
        foreach($data as $row){
            $result[] = $row[$column];
        }
        return $result;
    }
    
    /**
     * get list [key=>val]
     * @param mixed  $data
     * @param string $key
     * @param string $value
     * @return mixed
     */
    public static function getList($data, $key, $value)
    {
        $result = [];
        if(!is_array($data)) {
            return $result;
        }
        foreach((array)$data as $row){
            $result[$row[$key]] = $row[$value];
        }
        return $result;
    }
    /**
     * returns array with key of column id
     * 
     * @param  mixed $data
     * @param  mixed $columnId
     * @return mixed
     */
    public static function getMap($data, $columnId)
    {
        $result = [];
        foreach($data as $row){
            $result[$row[$columnId]] = $row;
        }
        return $result;
    }
    
    public static function getRows($data, $rowSearch)
    {
        $result = [];
        foreach($data as $key=>$row){
            $matched = true;
            foreach($rowSearch as $k=>$v){                
                $matched = $matched & $row[$k]==$v;
            }
            if($matched) { 
                $result[] = $row;
            }
        }
        return $result;
    } 
    
    /**
     * find row in data by search criteria
     * example
     *   CollectionDb::getRow($data, ['status'=>1, 'id'=>5]);
     * 
     * @param  array $data
     * @param  array $rowSearch
     * @param  bool  $withIndex - if with index returns [index,value]
     * @return array
     */
    public static function getRow($data, $rowSearch, $withIndex = false)
    {
        foreach($data as $key=>$row){
            $matched = true;
            foreach($rowSearch as $k=>$v){                
                $matched = $matched & $row[$k]==$v;
            }
            if($matched) {
                if($withIndex) {
                    return [$key, $row];
                }
                return $row;
            }
        }
    } 
    /**
     * finds row by $rowSearch and returns value by row column
     * 
     * @see getRow
     * 
     * @param  array  $data
     * @param  array  $rowSearch
     * @param  string $rowCol
     * @return string
     */
    public static function getRowValue($data, $rowSearch, $rowCol)
    {
        $row = self::getRow($data, $rowSearch);
        if(!isset($row[$rowCol])) {
            throw new CollectionException($rowCol . " index not found in row");
        }
        return $row[$rowCol];
    }
    
    /**
     * change element position in array
     * 
     * @param  array $array
     * @param  int   $currentIndex
     * @param  int   $newIndex
     * @return array
     */
    public static function moveElement(&$array, $currentIndex, $newIndex) 
    {
        $out = array_splice($array, $currentIndex, 1);
        array_splice($array, $newIndex, 0, $out);
        return $array;
    }
}
