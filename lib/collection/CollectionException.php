<?php namespace lib\collection;

/**
 * Simple CollectionException
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class CollectionException extends \Exception
{
}
