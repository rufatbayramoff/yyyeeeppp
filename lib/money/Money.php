<?php
/**
 * Created by mitaichik
 */

namespace lib\money;
use common\components\PaymentExchangeRateFacade;
use yii\base\Arrayable;

/**
 * Class Money
 * @package lib\money
 */
class Money implements Arrayable
{
    private $currency;
    private $amount;

    /**
     * Money constructor.
     * @param $amount
     * @param $currency
     */
    private function __construct($amount, $currency)
    {
        $this->currency = $currency;
        $this->amount = $amount;
    }

    /**
     * @param $amount
     * @return Money
     */
    public function plus($amount): Money
    {
        return new Money((float)$amount + $this->amount, $this->currency);
    }
    /**
     * Money factory
     * @param float $amount
     * @param string $currency
     * @return Money
     */
    public static function create($amount, $currency)
    {
        return new Money((float)$amount, $currency);
    }


    public static function createByMoney(Money $moneySrc)
    {
        return new Money($moneySrc->amount, $moneySrc->currency);
    }

    /**
     * Create usd money
     * @param float $amount
     * @return Money
     */
    public static function usd($amount)
    {
        return self::create($amount, Currency::USD);
    }

    /**
     * Return zero money
     * @param string $currency
     * @return Money
     */
    public static function zero(string $currency = null) : Money
    {
        return $currency ? new Money(0, $currency) : self::usd(0);
    }

    /**
     * Return money currency
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Return money amount
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return bool
     */
    public function notZero() : bool
    {
        return $this->amount != 0;
    }

    /**
     * @return bool
     */
    public function isZero() : bool
    {
        return $this->amount == 0;
    }

    /**
     * @return Money
     */
    public function round()
    {
        $amount = round($this->amount, 2, PHP_ROUND_HALF_UP);
        return new Money($amount, $this->currency);
    }

    /**
     * @param Money $money
     * @return bool
     */
    public function equal(Money $money = null) : bool
    {
        if ($money === null) {
            return false;
        }

        return $this->currency == $money->currency && $this->amount == $money->amount;
    }

    public function fields()
    {

    }


    public function extraFields()
    {

    }


    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        return [
            'amount' => $this->amount,
            'currency' => $this->currency,
        ];
    }

    public function asUsd()
    {
        return $this->convertTo('USD');
    }
    /**
     * @param $currency
     * @return Money
     */
    public function convertTo($currency)
    {
        if ($currency == $this->currency) {
            return $this;
        }
        $amount = PaymentExchangeRateFacade::convert($this->amount, $this->currency, $currency);
        return Money::create($amount, $currency);
    }

    public function __toString()
    {
        return $this->amount . ' ' . $this->currency;
    }
}