<?php
/**
 * Created by mitaichik
 */

namespace lib\money;

use common\components\exceptions\AssertHelper;
use yii\base\InvalidParamException;

/**
 * Class MoneyMath
 *
 * @package lib\money
 */
class MoneyMath
{
    /**
     * @param Money[] ...$moneys
     * @return Money
     */
    public static function sum(?Money ...$moneys): ?Money
    {
        AssertHelper::assert($moneys, 'Arguments cant be empty');

        $currency = null;
        $amount   = 0;

        foreach ($moneys as $money) {
            if ($money == null) {
                continue;
            }
            if ($currency === null) {
                $currency = $money->getCurrency();
            }
            AssertHelper::assert($money->getCurrency() === $currency, 'Can\'t sum moneys wih different currencies.');
            $amount += $money->getAmount();
        }

        if ($currency === null) {
            return null;
        }

        return Money::create($amount, $currency);
    }

    /**
     * @param Money|null $moneyA
     * @param Money|null $moneyB
     * @return Money
     */
    public static function sumIfNotNull($moneyA, $moneyB): ?Money
    {
        if ($moneyB === null) {
            return $moneyA;
        }
        AssertHelper::assert($moneyA->getCurrency() === $moneyB->getCurrency(), 'Can\'t sum moneys wih different currencies: ' . $moneyA->getCurrency() . ' !=' . $moneyB->getCurrency());
        return Money::create($moneyA->getAmount() + $moneyB->getAmount(), $moneyA->getCurrency());
    }


    /**
     * @param Money $m1
     * @param Money $m2
     * @return Money
     * @throws InvalidParamException
     */
    public static function minus(Money $m1, Money $m2): Money
    {
        AssertHelper::assert($m1->getCurrency() === $m2->getCurrency(), 'Error minus money in different currencies: ' . $m1->getCurrency() . ' !=' . $m2->getCurrency(), InvalidParamException::class);

        $amount = $m1->getAmount() - $m2->getAmount();
        return Money::create($amount, $m1->getCurrency());
    }

    /**
     * Money < condition
     *
     * @param Money $m1
     * @param Money $m2
     * @return bool
     */
    public static function less(Money $m1, Money $m2): bool
    {
        AssertHelper::assert($m1->getCurrency() === $m2->getCurrency(), 'Error less money in different currencies: ' . $m1->getCurrency() . ' !=' . $m2->getCurrency(), InvalidParamException::class);

        return round($m1->getAmount(), 2) < round($m2->getAmount(), 2);
    }

    /**
     * Money equal
     *
     * @param Money $m1
     * @param Money $m2
     * @return bool
     */
    public static function equal(Money $m1, Money $m2): bool
    {
        AssertHelper::assert($m1->getCurrency() === $m2->getCurrency(), 'Error less money in different currencies: ' . $m1->getCurrency() . ' !=' . $m2->getCurrency(), InvalidParamException::class);
        return (abs($m1->getAmount() - $m2->getAmount()) < 0.00001);
    }

    /**
     * @param Money $money
     * @param float $arg
     * @return Money
     */
    public static function mul(Money $money, float $arg): Money
    {
        return Money::create($money->getAmount() * $arg, $money->getCurrency());
    }

    /**
     * @param Money $money
     * @param int $pieces
     * @return Money[]
     */
    public static function div(Money $money, int $pieces): array
    {
        if ($pieces < 1) {
            throw new InvalidParamException("Pices must be more than 0");
        }

        if ($pieces == 1) {
            return [$money];
        }

        $pieceAmount = self::round($money->getAmount() / $pieces);

        $result = [];

        for ($i = 1; $i < $pieces; $i++) {
            $result[] = Money::create($pieceAmount, $money->getCurrency());
        }

        $result[] = Money::create(self::round($money->getAmount() - $pieceAmount * ($pieces - 1)), $money->getCurrency());

        return $result;
    }

    /**
     * @param float $amount
     * @return float
     */
    public static function round(float $amount)
    {
        return round($amount, 2);
    }

    /**
     * @param Money $money
     * @return Money
     */
    public static function abs(Money $money)
    {
        return Money::create(abs($money->getAmount()), $money->getCurrency());
    }
}