<?php
/**
 * Created by mitaichik
 */

namespace lib\money;


class Currency
{
    const USD = 'USD';
    const EUR = 'EUR';
    const BNS = 'BNS';

    const ALL_CURRENCIES = [
        self::USD => self::USD,
        self::EUR => self::EUR,
        self::BNS => self::BNS,
    ];

    const ALLOW_SELECT_CURRENCIES = [
        self::USD => self::USD,
        self::EUR => self::EUR,
    ];
}