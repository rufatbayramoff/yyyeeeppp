<?php
/**
 * User: DeFacto
 * Date: 23.11.2016
 */

namespace lib\translate;


interface TranslateApiInterface
{
    public function translate(array $data, $lang);
}