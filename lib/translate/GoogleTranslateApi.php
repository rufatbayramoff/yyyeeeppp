<?php
/**
 * User: DeFacto
 * Date: 23.11.2016
 */

namespace lib\translate;


use yii\base\Exception;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * Class GoogleTranslateApi
 * usage:
 * $translateApi = new GoogleTranslateApi();
 * $result = $translateApi->addText(["asdf","asdf"])->translate("de");
 *
 * @package backend\components
 */
class GoogleTranslateApi implements TranslateApiInterface
{
    public $target = '';
    public $source = 'en';
    const API_URL = 'https://www.googleapis.com/language/translate/v2';

    /**
     * GoogleTranslateApi constructor.
     *
     * @param string $apiKey
     */
    public function __construct( $apiKey = '')
    {
        $this->apiKey = $apiKey ?: param('google_api_key', 'AIzaSyD1VGxBmPEZw7IKVo9aysbTiDhmwP8d2fQ');
    }


    /**
     * @param array $data
     * @param $lang
     * @return array
     * @throws \yii\base\Exception
     */
    public function translate(array $data, $lang)
    {
        $this->target = $lang;
        $link = self::API_URL;
        $params = [
            'key' => $this->apiKey,
            'source' => $this->source,
            'target' => $lang,
            'format' => 'text'
        ];
        $query = http_build_query($params);
        $q = [];
        foreach($data as $q1){
            $q[] = "q=". urlencode($q1);
        }
        $query = $query .  '&' . implode("&", $q);
        $fullLink = $link . '?' . $query;
        \Yii::info(VarDumper::dumpAsString($fullLink), 'tsdebug');
        $result = $this->getResult($fullLink);
        return $result;
    }

    /**
     * @param $link
     * @return array
     * @throws \yii\base\Exception
     */
    private function getResult($link)
    {
        if(strlen($link) > 8192){
            throw new Exception('API Link more then 8192 characters. Use smaller limit');
        }
        $jsonResult = file_get_contents($link);
        $result = Json::decode($jsonResult);
        \Yii::info(VarDumper::dumpAsString($result), 'tsdebug');
        return $result;
    }

    private function getResultStub(){

        $jsonResultStub = '{
 "data": {
  "translations": [
   {
    "translatedText": "Translated from google 1"
   },
   {
    "translatedText": "Translated from google 2"
   }
  ]
 }
}';
        return $jsonResultStub;
    }
}