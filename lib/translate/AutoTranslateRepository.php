<?php
/**
 * User: DeFacto
 * Date: 23.11.2016
 */

namespace lib\translate;


use common\models\SystemLang;
use common\models\SystemLangMessage;
use common\models\SystemLangSource;

class AutoTranslateRepository
{

    public static $limit = 10;
    /**
     * @param $lang
     * @param bool $mapById
     * @return array
     */
    public static function findAllToTranslate($lang, $mapById = false)
    {
        $query = SystemLangSource::find()->isChecked()->select([
            SystemLangSource::column('id'),
            SystemLangSource::column('message')
        ]);
        $query->andFilterWhere(['!=', 'category', 'app']);
        $query->andFilterWhere(['!=', 'category', 'yii']);
        $query->andFilterWhere(['!=', 'message', dbexpr('""')]);
        $query->andFilterWhere(['NOT LIKE', 'message', dbexpr('"@@%"')]);
        $query->andWhere([SystemLangSource::column('not_found_in_source') => 0]);

        $query->joinWith(['systemLangMessages']);
        $query->andWhere([SystemLangMessage::column('translation') => null]);
        $query->andWhere([SystemLangMessage::column('language') => $lang]);
        $query->andWhere([SystemLangMessage::column('is_auto_translate') => 0]);
        $items = $query->limit(self::$limit)->all();
        if(empty($items)){
            return ['total'=>0, 'skip'=>0];
        }
        return $mapById ? self::mapMessagesById($items) : $items;
    }

    /**
     * @param $items
     * @return array
     */
    public static function mapMessagesById($items){
        $result = [];
        $skip = 0;
        foreach($items as $k=>$v){
            $withBrace = strpos($v->message, '{')!==false && strpos($v->message, '}')!==false;
            $withTags = strpos($v->message, '<')!==false && strpos($v->message, '>')!==false;
            if($withBrace || $withTags){
                SystemLangMessage::updateRow(['id' => $v->id], ['is_auto_translate' => 1]); // to skip in next select
                $skip++;
                continue;
            }
            $result[$v->id] = $v->message;
        }
        return ['total'=>$result, 'skip'=>$skip];
    }
    /**
     * @return array - iso codes
     */
    public static function findAllActiveLanguages()
    {
        return SystemLang::find()->select('iso_code')->where(['is_active'=>1])->andWhere(['!=', 'iso_code', 'en-US'])->column();
    }
}