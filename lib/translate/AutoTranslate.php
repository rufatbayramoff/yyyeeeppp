<?php
/**
 * User: DeFacto
 * Date: 23.11.2016
 */

namespace lib\translate;


use common\models\SystemLangMessage;
use common\models\SystemLangSource;
use yii\base\Exception;

class AutoTranslate
{

    /**
     * @var TranslateApiInterface
     */
    public $translateApi;

    /**
     * AutoTranslate constructor.
     *
     * @param TranslateApiInterface $api
     */
    public function __construct(TranslateApiInterface $api)
    {
        $this->translateApi = $api;
    }

    /**
     * @param array $data - data in {id=>text, ...}
     * format from table !system_lang_source!
     * @param $lang
     * @return mixed
     * @throws Exception
     */
    public function translate(array $data, $lang)
    {
        $result = $this->translateApi->translate($data, $lang);
        if(!isset($result['data']['translations'])){
            throw new Exception("No translations found for lang " . $lang);
        }
        $count = $this->updateTranslates($data, $result['data']['translations'], $lang);
        return $count;
    }

    /**
     * @param array $data
     * @param array $result
     * @param $lang
     * @return int
     */
    private function updateTranslates(array $data, array $result, $lang)
    {
        $key = 0;
        foreach($data as $id=>$text){
            $translateText = $result[$key]['translatedText'];
            SystemLangMessage::updateRow(
                [
                    'id' => $id,
                    'language' => $lang
                ],
                [
                    'is_auto_translate' => 1,
                    'translation' => $translateText
                ]
            );
            $key++;
        }
        return $key;
    }
}