<?php
namespace lib\delivery\exceptions;

/**
 * DeliveryException
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class DeliveryException extends \Exception
{

}
