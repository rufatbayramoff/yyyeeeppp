<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\exceptions;


use common\components\exceptions\DataExceptionInterface;
use common\components\exceptions\ErrorCodeExceptionInterface;
use common\components\exceptions\OutExceptionInterface;
use common\components\serizaliators\porperties\UserAddressAsUserLocatonProperties;
use common\models\base\UserAddress;
use yii\web\HttpException;

/**
 * Class HaveSuggestAddressException
 * @package lib\delivery\exceptions
 */
class HaveSuggestAddressException extends HttpException implements ErrorCodeExceptionInterface, OutExceptionInterface, DataExceptionInterface
{
    /**
     * @var UserAddress
     */
    private $userAddress;

    /**
     * HaveSuggestAddressException constructor.
     * @param UserAddress $userAddress
     */
    public function __construct(UserAddress $userAddress)
    {
        parent::__construct(422);
        $this->userAddress = $userAddress;
    }

    /**
     * @return UserAddress
     */
    public function getUserAddress()
    {
        return $this->userAddress;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return 'HaveSuggestAddress';
    }

    /**
     * @return array
     */
    public function getData()
    {
        return UserAddressAsUserLocatonProperties::serialize($this->userAddress);
    }
}