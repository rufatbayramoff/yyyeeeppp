<?php namespace lib\delivery\exceptions;

/**
 * UserDeliveryException
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserDeliveryException extends \yii\base\UserException
{
}
