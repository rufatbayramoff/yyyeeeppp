<?php

namespace lib\delivery\exceptions;

use common\components\exceptions\ErrorCodeExceptionInterface;
use yii\web\HttpException;

/**
 * Class DeliveryAddressFailedException
 *
 * @package lib\delivery\exceptions
 */
class DeliveryAddressFailedException extends HttpException implements ErrorCodeExceptionInterface
{
    public function __construct(string $message)
    {
        parent::__construct(422, $message);
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return 'DeliveryAddressFailed';
    }
}
