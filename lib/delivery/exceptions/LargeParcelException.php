<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\exceptions;

/**
 * Class LargeParcelException
 * @package lib\delivery\exceptions
 */
class LargeParcelException extends DeliveryException
{
    /**
     * @inheritdoc
     */
    public function __construct(string $message = null)
    {
        $message = $message ?? _t('store.delivery', 'The quantity of ordered parts cannot fit into one package. Please reduce the number of parts or create a new order.');
        parent::__construct($message);
    }
}