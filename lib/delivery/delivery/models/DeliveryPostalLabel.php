<?php namespace lib\delivery\delivery\models;

use common\components\FileDirHelper;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use common\models\StoreOrderAttemp;
use yii\base\Model;

/**
 * Work with EasyPost Postal Label: get path, download, save, etc...
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class DeliveryPostalLabel extends Model
{

    private $uniqueFileName;
    private $userId;
    private $url;
    private $carrier;
    private $trackingCode;
    private $attemp;

    /**
     * @param int $userId
     * @param StoreOrderAttemp $attemp
     * @param $url
     * @param $carrier
     * @param $trackingCode
     */
    public function __construct($userId, StoreOrderAttemp $attemp, $url, $carrier, $trackingCode)
    {
        parent::__construct();
        $this->userId = $userId;
        $this->attemp = $attemp;
        $this->url = $url;
        $this->trackingCode = $trackingCode;
        $this->carrier = $carrier;
    }

    /**
     * Download postal label from EasyPost and save image to local server
     *
     * @param $userId
     * @param StoreOrderAttemp $attemp
     * @param $url
     * @param $carrier
     * @param $trackingCode
     * @return string
     */
    public static function downloadPostalLabelFile($userId, StoreOrderAttemp $attemp, $url, $carrier, $trackingCode)
    {
        $model = new DeliveryPostalLabel($userId, $attemp, $url, $carrier, $trackingCode);
        $filePath = $model->getUniqueFileName();
        $result = $model->savePostalLabelImage($filePath);

        if ($result) {
            return $filePath;
        }
        return null;
    }

    /**
     * Get unique file name for postal label
     *
     * @return string
     */
    public function getUniqueFileName()
    {
        $ds = DIRECTORY_SEPARATOR;

        $staticPath = \Yii::getAlias('@static/');
        $userFolder = \frontend\models\user\UserFacade::getUserFolder($this->userId);
        $folderPath = $staticPath . $userFolder . $ds . 'postal_labels';

        FileDirHelper::createDir($folderPath);

        $uniqueFileName = md5(\Yii::$app->security->generateRandomString()) . '.png';
        $this->uniqueFileName = $uniqueFileName;

        return $folderPath . $ds . $uniqueFileName;
    }

    /**
     * Save postal label to user folder
     *
     * @param  $filePath
     * @return bool
     */
    public function savePostalLabelImage($filePath)
    {
        $data = null;

        for($i = 0; $i < 10; $i++) {
            $ch = curl_init($this->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($ch);
            curl_close($ch);
            if ($data) {
                break;
            }
            usleep(500);
        }

        file_put_contents($filePath, $data);
        return $this->saveFileToBase($filePath);

    }

    /**
     * Save file to table `file` and update `store_order_delivery`
     *
     * @param  $filePathOrig
     * @return bool
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function saveFileToBase($filePathOrig)
    {
        $fileFactory = \Yii::createObject(FileFactory::class);
        $fileRepository = \Yii::createObject(FileRepository::class);

        $fileObj = $fileFactory->createFileFromPath($filePathOrig);
        $fileRepository->save($fileObj);

        $delivery = $this->attemp->delivery;
        $delivery->file_id = $fileObj->id;
        $delivery->setTrackingNumber($this->trackingCode, $this->carrier);
        $delivery->link('file', $fileObj);
        return true;
    }

    /**
     * Check if we already have postal label
     * Return file path if exists
     * @param StoreOrderAttemp $attemp
     * @return bool|string
     */
    public static function checkPostalLabelExists(StoreOrderAttemp $attemp)
    {
        $delivery = $attemp->delivery;
        return $delivery && $delivery->file ? $delivery->file->getLocalTmpFilePath() : null;
    }
}
