<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\delivery\models;


use common\interfaces\machine\DeliveryParamsInterface;
use frontend\models\delivery\DeliveryForm;
use frontend\models\ps\PsFacade;
use lib\delivery\carrier\models\CarrierRate;
use lib\money\Money;
use lib\money\MoneyMath;
use yii\base\BaseObject;

/**
 * Class DeliveryRate
 *
 * @package lib\delivery\delivery\models
 */
class DeliveryRate extends BaseObject
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $code;

    /**
     * @var bool
     */
    public $isEstimate;

    /**
     * @var Money | null
     */
    public $delveiryCost;

    /**
     * @var Money | null
     */
    public $packingCost;

    /**
     * @var DeliveryParamsInterface
     */
    public $deliveryParams;

    /**
     * @var DeliveryForm
     */
    public $deliveryForm;

    public $express = false;

    public const EXPRESS_RATE = 30;

    /**
     * @param string $title
     * @param string $code
     * @param Money $deliveryCost
     * @param bool $isEstimate
     * @return DeliveryRate
     */
    public static function create(string $title = null, string $code, Money $deliveryCost = null, bool $isEstimate = false): DeliveryRate
    {
        $rate = new static();
        $rate->title = $title ?: PsFacade::getDeliveryTypesIntl($code);
        $rate->code = $code;
        $rate->delveiryCost = $deliveryCost;
        $rate->isEstimate = $isEstimate;
        return $rate;
    }

    /**
     * @param string $title
     * @param string $code
     * @param CarrierRate $carrierRate
     * @param bool $isEstimate
     * @return DeliveryRate
     */
    public static function createFromCarrierRate(string $title = null, string $code, CarrierRate $carrierRate, bool $isEstimate = false): DeliveryRate
    {
        return self::create($title, $code, Money::create($carrierRate->rate, $carrierRate->currency), $isEstimate);
    }

    /**
     * @return Money
     */
    public function getCost(): ?Money
    {
        if (!$this->delveiryCost) {
            return null;
        }

        if ($this->delveiryCost->getAmount()==0) {
            return Money::zero();
        }

        return $this->delveiryCost;
    }

    /**
     * @return Money|null
     */
    public function getDeliveryMoney(): ?Money
    {
        return $this->delveiryCost;
    }

    /**
     * @return bool
     */
    public function isExpress(): bool
    {
        return $this->express;
    }
}