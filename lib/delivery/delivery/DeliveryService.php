<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\delivery;


use common\components\exceptions\AssertHelper;
use common\components\PaymentExchangeRateConverter;
use common\interfaces\machine\DeliveryParamsInterface;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\PsMachineDelivery;
use common\models\StoreOrderAttemp;
use common\models\UserAddress;
use common\modules\payment\fee\FeeHelper;
use EasyPost\CustomsInfo;
use EasyPost\Error;
use frontend\models\delivery\DeliveryForm;
use lib\delivery\carrier\CarrierService;
use lib\delivery\carrier\models\CarrierRate;
use lib\delivery\carrier\models\RatesCriteria;
use lib\delivery\carrier\models\ValidateAddressResult;
use lib\delivery\carrier\vendors\easypost\CustomsTranslator;
use lib\delivery\delivery\carriers\CarrierInterface;
use lib\delivery\delivery\carriers\MyselfCarrier;
use lib\delivery\delivery\carriers\PickupCarrier;
use lib\delivery\delivery\carriers\TreatstockCarrier;
use lib\delivery\delivery\models\DeliveryRate;
use lib\delivery\exceptions\DeliveryException;
use lib\delivery\exceptions\UserDeliveryException;
use lib\delivery\parcel\Parcel;
use lib\geo\models\Location;
use lib\MeasurementUtil;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use Yii;
use yii\base\Component;

/**
 * Class DeliveryService
 *
 * @package lib\delivery\delivery
 */
class DeliveryService extends Component
{
    /**
     * @var CarrierService
     */
    private $carrierService;

    /**
     * @var PaymentExchangeRateConverter
     */
    private $moneyConvertor;

    /**
     * @var FeeHelper
     */
    private $feeHelper;

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->carrierService = Yii::$app->carrierService;
        $this->moneyConvertor = Yii::createObject(PaymentExchangeRateConverter::class);
        $this->feeHelper      = Yii::createObject(FeeHelper::class);
    }

    /**
     * Return rate or null if we can't delivery to user
     *
     * @param DeliveryParamsInterface $params
     * @param DeliveryForm $deliveryForm
     * @param Money $manufacturePrice
     * @param Parcel $parcel
     * @param $toCurrency
     * @return DeliveryRate|null
     */
    public function getOrderRate(DeliveryParamsInterface $params, DeliveryForm $deliveryForm, Money $manufacturePrice, Parcel $parcel, $toCurrency = Currency::USD)
    {
        $isPickup          = $deliveryForm->deliveryType == DeliveryType::PICKUP;
        $psPrinterDelivery = $this->resolveDeliveryTypeForDelivery($params, $isPickup, $deliveryForm->country);

        if (!$psPrinterDelivery) {
            return null;
        }

        $carrier = $this->createCarrier($psPrinterDelivery);

        $isEstimate      = true;
        $carrierRate     = $carrier->calculateEstimateRate($params->getCurrentLocation()->country->iso_code, $deliveryForm->country, $parcel);
        $criteriaCustoms = [];

        $isExpress = false;
        if ($this->canExpress($deliveryForm, $params)) {
            //$criteriaCustoms = $this->easyPostInfo($deliveryForm, $printPrice, $params, $parcel);
            $carrierRate = $carrier->expressRate($params->getUserAddress(), $deliveryForm->getUserAddress(), $parcel, $criteriaCustoms);
            $isEstimate  = false;
            $isExpress   = (bool)$carrierRate;
        }

        if (!$carrierRate) {
            if ($this->isNeedCustoms($params->getUserAddress(), $deliveryForm->getUserAddress())) {
                if (!$deliveryForm->phone) {
                    throw new UserDeliveryException('Sorry, phone required for this delivery.');
                }
                $criteriaCustoms = $this->easyPostInfo($deliveryForm, $manufacturePrice, $params, $parcel);
            }

            $carrierRate = $carrier->calculateRate($params->getUserAddress(), $deliveryForm->getUserAddress(), $parcel, $criteriaCustoms);
            $isEstimate  = false;
        }

        if (!$carrierRate) {
            return null;
        }

        if ($this->needIncreaseCost($psPrinterDelivery, $deliveryForm, $params)) {
            $rateCost = Money::create($carrierRate->priceWithMarkup()->getAmount(), $toCurrency);
        } else {
            $rateCost = Money::create($carrierRate->getPrice()->getAmount(), $toCurrency);
        }

        $rate          = DeliveryRate::create(null, $psPrinterDelivery->deliveryType->code, $rateCost, $isEstimate);
        $rate->express = $isExpress;
        $this->checkAndModifyForFreeDelivery($params, $psPrinterDelivery, $rate, $manufacturePrice);

        return $rate;
    }

    /**
     * @param $deliveryForm
     * @param $params
     */
    protected function canExpress(DeliveryForm $deliveryForm, DeliveryParamsInterface $params): bool
    {
        $location = $params->getCurrentLocation();
        if (!$location) {
            return false;
        }
        $country = $location->country;
        if (!$country) {
            return false;
        }
        return $deliveryForm->canExpress() && $country->isUsa();
    }

    protected function easyPostInfo(DeliveryForm $deliveryForm, Money $printPrice, DeliveryParamsInterface $params, Parcel $parcel): CustomsInfo
    {
        $weightGr        = MeasurementUtil::convertOunceToGram($parcel->weight);
        $criteriaCustoms = CustomsTranslator::formEasyPostCustomsInfo(
            'item_' . $deliveryForm->email . '_' . $weightGr . 'gr',
            'Treatstock order',
            $weightGr,
            $printPrice,
            $params->getCurrentLocation()->country->iso_code,
            $deliveryForm->deliveryParams->getSenderCompany()->getTitle()
        );
        return $criteriaCustoms;
    }

    /**
     * @param DeliveryParamsInterface $params
     * @param Money $printPrice
     * @param Location $toLocation
     * @param Parcel $parcel
     * @return models\DeliveryRate[]
     */
    public function getAvaliableRates(DeliveryParamsInterface $params, Money $printPrice, Location $toLocation, Parcel $parcel)
    {
        $rates = [];

        // for pickup

        if ($psPrinterDelivery = $params->getPsDeliveryTypeByCode(DeliveryType::PICKUP)) {
            $carrierRate = $this->createCarrier($psPrinterDelivery)->calculateEstimateRate($params->getCurrentLocation()->country->iso_code, $toLocation->country, $parcel);
            $rates[]     = DeliveryRate::createFromCarrierRate(null, DeliveryType::PICKUP, $carrierRate, true);
        }

        // for delivery

        if ($psPrinterDelivery = $this->resolveDeliveryTypeForDelivery($params, false, $toLocation->country)) {

            $rate = DeliveryRate::create(_t('site.store', 'Delivery'), $psPrinterDelivery->deliveryType->code, null, true);

            $carrier = $this->createCarrier($psPrinterDelivery);
            if ($carrierRate = $carrier->calculateEstimateRate($params->getCurrentLocation()->country->iso_code, $toLocation->country, $parcel)) {
                if ($psPrinterDelivery->isTsDelivery()) {
                    $rate->delveiryCost = $carrierRate->priceWithMarkup();
                } else {
                    $rate->delveiryCost = Money::create($carrierRate->rate, $carrierRate->currency);
                }
            }
            $this->checkAndModifyForFreeDelivery($params, $psPrinterDelivery, $rate, $printPrice);
            $rates[] = $rate;
        }
        return $rates;
    }


    /**
     * @param DeliveryParamsInterface $params
     * @param Money $printPrice
     * @param Location $toLocation
     * @param Parcel $parcel
     * @param $toCurrency
     * @return null|DeliveryRate
     */
    public function getEstimateDeliveryRate(DeliveryParamsInterface $params, Money $printPrice, Location $toLocation, Parcel $parcel, $toCurrency)
    {
        $psPrinterDelivery = $this->resolveDeliveryTypeForDelivery($params, false, $toLocation->country);

        if (!$psPrinterDelivery) {
            return null;
        }

        $carrier     = $this->createCarrier($psPrinterDelivery);
        $carrierRate = $carrier->calculateEstimateRate($params->getCurrentLocation()->country->iso_code, $toLocation->country, $parcel);

        if (!$carrierRate) {
            return null;
        }

        $rateCost = Money::create($this->moneyConvertor->convert($carrierRate->rate, $carrierRate->currency, $toCurrency), $toCurrency);
        $rate     = DeliveryRate::create(null, $psPrinterDelivery->deliveryType->code, $rateCost, true);
        $this->checkAndModifyForFreeDelivery($params, $psPrinterDelivery, $rate, $printPrice);
        return $rate;
    }


    /**
     * @param DeliveryParamsInterface $params
     * @param $isPickup
     * @param $toCountry
     * @return PsMachineDelivery|null
     */
    public function resolveDeliveryTypeForDelivery(DeliveryParamsInterface $params, $isPickup, $toCountry)
    {
        if ($isPickup) {
            return $params->getPsDeliveryTypeByCode(DeliveryType::PICKUP);
        }

        $isInternational = $toCountry != $params->getCurrentLocation()->country->iso_code;
        $deliveryType    = $isInternational ? DeliveryType::INTERNATIONAL : DeliveryType::STANDARD;
        return $params->getPsDeliveryTypeByCode($deliveryType);
    }

    /**
     * @param DeliveryParamsInterface $params
     * @param PsMachineDelivery $psPrinterDelivery
     * @param DeliveryRate $rate
     * @param Money $printPrice
     */
    private function checkAndModifyForFreeDelivery(DeliveryParamsInterface $params, PsMachineDelivery $psPrinterDelivery, DeliveryRate $rate, Money $printPrice)
    {
        if ($psPrinterDelivery->free_delivery && !$rate->isExpress()) {
            $freeDelivery         = Money::create($psPrinterDelivery->free_delivery, $printPrice->getCurrency());
            $deliveryFee          = $this->feeHelper->getTsCommonFee($freeDelivery);
            $deliveryPriceWithFee = MoneyMath::sum($freeDelivery, $deliveryFee);

            if (MoneyMath::less($deliveryPriceWithFee, $printPrice)) {
                $rate->delveiryCost = Money::zero();
                $rate->packingCost  = null;
                $rate->title        = _t('site.store', 'Free Delivery');
            }
        }

        if ($psPrinterDelivery->packing_price) {
            $packingCost       = Money::create($psPrinterDelivery->packing_price, $params->company->currency);
            $rate->packingCost = $packingCost;
        }
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param Parcel $parcel
     * @return CarrierRate
     * @throws UserDeliveryException
     */
    public function getCarrierRateForOrderAttemp(StoreOrderAttemp $attemp, Parcel $parcel)
    {
        if ($attemp->order->deliveryType->code == DeliveryType::PICKUP) {
            throw new UserDeliveryException("Cant calculate shipping for puckup");
        }

        $fromAddress = $attemp->machine->asDeliveryParams()->getUserAddress();
        $toAddress   = $attemp->order->shipAddress;

        $fromAddress->contact_name = substr($attemp->ps->title, 0, 44);
        if (empty($attemp->ps->phone)) {
            throw new UserDeliveryException("Fill your phone number in settings");
        }
        $fromAddress->phone = $attemp->ps->phone;
        $fromAddress->email = $attemp->ps->user->email;

        $ratesCriteria = RatesCriteria::create()
            ->from($fromAddress)
            ->to($toAddress)
            ->parcel($parcel)
            ->express($attemp->order->isExpressDelivery());

        if ($this->isNeedCustoms($fromAddress, $toAddress)) {
            if (!$toAddress->phone) {
                throw new UserDeliveryException('Sorry, phone required for this delivery');
            }
            $criteriaCustoms = CustomsTranslator::translateFromAttempt($attemp);
            $ratesCriteria->customs($criteriaCustoms);
        }

        $rate = \Yii::$app->carrierService->getLowestRate($ratesCriteria);
        return $rate;
    }

    /**
     * In fact, its not used now. Delivery To Us Army AE AP, not working good, no orders exists in fact.
     *
     * @param UserAddress $fromAddress
     * @param UserAddress $toAddress
     * @return bool
     */
    public function isNeedCustoms(UserAddress $fromAddress, UserAddress $toAddress)
    {
        if ($toAddress->country_id === UserAddress::COUNTRY_USA_ID
            && ($fromAddress->region != $toAddress->region)
            && ($fromAddress->region === 'AE' || $toAddress->region === 'AE' || $fromAddress->region === 'AP' || $toAddress->region === 'AP')) {
            return true;
        }
        return $fromAddress->country_id != $toAddress->country_id;
    }

    /**
     * @param CarrierRate $rate
     * @return \lib\delivery\carrier\models\BuyResult
     */
    public function buyCarrierShippment(CarrierRate $rate)
    {
        return $this->carrierService->buy($rate);
    }

    /**
     * Verify address
     *
     * @param UserAddress $address
     * @return ValidateAddressResult
     */
    public function validateAddress(UserAddress $address)
    {
        if (!$address->country->is_easypost_domestic && !$address->country->is_easypost_intl) {
            return ValidateAddressResult::createSuccess(null);
        }

        try {
            $result = $this->carrierService->validateAddress($address);
        } catch (Error $exception) {
            return ValidateAddressResult::createFail(_t('site.delivery', 'Unable to verify address.'));
        }

        if (!$result->isSuccess()) {
            $message = $result->getErrorMessage();
            if (!$message) {
                $message = _t('site.delivery', "Address confirmation failed. Please check your address and try again.");
            } elseif ($message == 'Address not found') {
                $message = _t('site.ps', 'Address cannot be found. Please indicate correct address.');
            }

            $message = str_replace('Unable to verify address.',
                _t('site.ps', 'This address is not supported by Treatstock Delivery Service. Please select another address. Reason: '),
                $message);
            return ValidateAddressResult::createFail($message);
        }

        return $result;
    }

    /**
     * Check that printer can delivery to country
     *
     * @param DeliveryParamsInterface $params
     * @param $countryIso
     * @return bool
     */
    private function canPrinterPickupFromCountry(DeliveryParamsInterface $params, $countryIso)
    {
        AssertHelper::assert($countryIso);

        if ($params->onlyPickup()) {
            if ($countryIso == $params->getCurrentLocation()->country->iso_code) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check that printer can delivery to country
     *
     * @param DeliveryParamsInterface $params
     * @param $countryIso
     * @return bool
     */
    public function canPrinterDeliveryToCountry(DeliveryParamsInterface $params, $countryIso)
    {
        AssertHelper::assert($countryIso);

        if ($params->onlyPickup()) {
            return false;
        }

        $isInternational = $countryIso != $params->getCurrentLocation()->country->iso_code;

        if (!$isInternational && !$params->hasDomesticDelivery()) {
            return false;
        }

        if ($isInternational && $params->onlyDomestic()) {
            return false;
        }

        return true;
    }

    /**
     * Check that printer have pickup in country or can delivery to country
     *
     * @param DeliveryParamsInterface $params
     * @param $countryIso
     * @return bool
     */
    public function canPrinterPickupOrDeliveryToCountry(DeliveryParamsInterface $params, $countryIso)
    {
        return $this->canPrinterPickupFromCountry($params, $countryIso)
            || $this->canPrinterDeliveryToCountry($params, $countryIso);
    }

    /**
     * @param CompanyService $companyService
     * @param Money $producePrice
     * @param Location $location
     * @param Parcel $parcel
     * @return array
     */
    public function resolveDeliveryRateAndLabels(CompanyService $companyService, Money $producePrice, Location $location, Parcel $parcel): array
    {
        $estimateRates = $this->getAvaliableRates($companyService->asDeliveryParams(), $producePrice, $location, $parcel);

        $totalPrice   = null;
        $packingPrice = null;
        $labels       = [];

        foreach ($estimateRates as $rate) {
            if ($rate->code === DeliveryType::STANDARD || $rate->code === DeliveryType::INTERNATIONAL) {
                if (!$totalPrice) {
                    $totalPrice   = $rate->getCost();
                    $packingPrice = $rate->packingCost;
                }
            }

            $labels[] = $rate->code;
        }

        return [$totalPrice, $labels, $packingPrice];
    }

    /**
     * Factory method for carrier
     *
     * @param PsMachineDelivery $psPrinterDelivery
     * @return CarrierInterface
     * @throws DeliveryException
     */
    protected function createCarrier(PsMachineDelivery $psPrinterDelivery)
    {
        if ($psPrinterDelivery->delivery_type_id == DeliveryType::PICKUP_ID) {
            return new PickupCarrier($psPrinterDelivery);
        }

        switch ($psPrinterDelivery->carrier) {
            case DeliveryType::CARRIER_TS:
                return new TreatstockCarrier($psPrinterDelivery);

            case DeliveryType::CARRIER_MYSELF:
                return new MyselfCarrier($psPrinterDelivery);
        }

        throw new DeliveryException("Unknown carrier {$psPrinterDelivery->carrier}");
    }

    protected function needIncreaseCost(PsMachineDelivery $psPrinterDelivery, DeliveryForm $deliveryForm, DeliveryParamsInterface $params): bool
    {
        return !$this->canExpress($deliveryForm, $params) && $psPrinterDelivery->isTsDelivery();
    }
}