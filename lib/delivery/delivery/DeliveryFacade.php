<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */
namespace lib\delivery\delivery;

use common\components\exceptions\AssertHelper;
use common\components\PaymentExchangeRateFacade;
use common\interfaces\machine\DeliveryParamsInterface;
use common\models\base\User;
use common\models\DeliveryType;

use common\models\StoreOrder;
use common\models\UserAddress;
use lib\delivery\delivery\models\DeliveryRate;
use lib\money\Currency;
use lib\money\Money;

/**
 * Class DeliveryFacade
 * @package lib\delivery\delivery
 */
class DeliveryFacade
{
    /**
     * @param StoreOrder $order
     * @return bool
     */
    public static function getIsPostDelivery(StoreOrder $order)
    {
        if(empty($order->delivery_type_id)){
            return false;
        }
        AssertHelper::assert($order->delivery_type_id, 'Delivery type is not set');
        return $order->deliveryType->code != DeliveryType::PICKUP;
    }
    
    /**
     * 
     * @param StoreOrder $order
     * @return bool
     */
    public static function isPickupDelivery(StoreOrder $order)
    {
        if(empty($order->delivery_type_id)){
            return false;
        }
        AssertHelper::assert($order->delivery_type_id, 'Delivery type is not set');
        return $order->deliveryType->code == DeliveryType::PICKUP;
    }
    
    /**
     * check if for given order post delivery is with treatstock carrier,
     * not "myself"
     * 
     * @param StoreOrder $order
     * @return boolean
     */
    public static function isCarrierPostDelivery(StoreOrder $order)
    {
        $deliveryTypeDetails = $order->getOrderDeliveryDetails();
        if($deliveryTypeDetails && $deliveryTypeDetails['carrier']==\common\models\DeliveryType::CARRIER_MYSELF){
            return false;
        }
        return self::getIsPostDelivery($order);
    }

    /**
     *
     * @return DeliveryRate
     */
    public static function getPickupRate()
    {
        return DeliveryRate::create(null, DeliveryType::PICKUP, Money::zero());
    }

    /**
     * @param DeliveryParamsInterface $params
     * @param User $user
     * @return array|\common\models\UserAddress[]
     */
    public static function getUserAddresses(DeliveryParamsInterface $params, User $user)
    {
        $adresses = UserAddress::getLatestShip($user->id);

        if ($params->onlyDomestic()){
            $adresses = array_filter($adresses, function (UserAddress $address) use ($params){
                return $address->country_id == $params->getCurrentLocation()->country_id;
            });
        }
        return array_values($adresses);
    }

    /**
     * @param DeliveryParamsInterface $params
     * @return array
     */
    public static function getAllowedDeliveryCoutriesCombo(DeliveryParamsInterface $params)
    {
        $data = \common\models\GeoCountry::getCombo('iso_code');
        if($params->onlyDomestic()){
            foreach($data as $iso=>$title){
                if($iso == $params->getCurrentLocation()->country->iso_code){
                    $data = [$iso => $params->getCurrentLocation()->country->title];
                    break;
                }
            }
        }
        return $data;
    }

    /**
     *
     * @param DeliveryParamsInterface $params
     * @param int $printPriceUsd
     * @return bool
     */
    public static function isFreeDeliveryCost(DeliveryParamsInterface $params, $printPriceUsd)
    {
        foreach ($params->getAvailableDeliverieTypes() as $psDelivery) {

            if (!$psDelivery->free_delivery) {
                continue;
            }

            $freeDeliveryUSD = PaymentExchangeRateFacade::convert($psDelivery->free_delivery, $params->company->currency, Currency::USD);
            if ($printPriceUsd >= $freeDeliveryUSD) {
                return true;
            }
        }
        return false;
    }

    /**
     * get list of available delivery types to display to end-user
     * in model page
     *
     * @param DeliveryParamsInterface $params
     * @param int $printPriceUsd - print price to calc Free delivery
     * @return array
     */
    public static function getAvailableDeliveriesLabels(DeliveryParamsInterface $params, $printPriceUsd)
    {
        $deliverTypes = [];

        if ($params->hasDeliveryPickup()) {
            $deliverTypes[] = _t('site.store', 'Pickup');
        }

        if (!$params->onlyPickup()) {
            $deliverTypes[] = self::isFreeDeliveryCost($params, $printPriceUsd)
                ? _t('site.store', 'Free Delivery')
                : _t('site.store', 'Domestic Delivery');
        }

        if ($params->hasInternationalDelivery()) {
            $deliverTypes[] = _t('site.store', 'International Delivery');
        }

        return $deliverTypes;
    }


    /**
     *
     * @param DeliveryParamsInterface $params
     * @param int $printPriceUsd
     * @return bool
     */
    public static function isFreeDeliveryCost2(DeliveryParamsInterface $params, $printPriceUsd)
    {
        foreach ($params->getAvailableDeliverieTypes() as $psDelivery) {

            if (!$psDelivery->free_delivery) {
                continue;
            }
            $freeDeliveryUSD = $psDelivery->free_delivery;
            if ($printPriceUsd >= $freeDeliveryUSD) {
                return true;
            }
        }
        return false;
    }

    /**
     * get list of available delivery types to display to end-user
     * in model page
     *
     * @param DeliveryParamsInterface $params
     * @param int $printPriceUsd - print price to calc Free delivery
     * @return array
     */
    public static function getAvailableDeliveriesLabels2(DeliveryParamsInterface $params, $printPriceUsd)
    {
        $deliverTypes = [];

        if ($params->hasDeliveryPickup()) {
            $deliverTypes[] = _t('site.store', 'Pickup');
        }

        if (!$params->onlyPickup()) {
            $deliverTypes[] = self::isFreeDeliveryCost2($params, $printPriceUsd)
                ? _t('site.store', 'Free Delivery')
                : _t('site.store', 'Domestic Delivery');
        }

        if ($params->hasInternationalDelivery()) {
            $deliverTypes[] = _t('site.store', 'International Delivery');
        }

        return $deliverTypes;
    }
}
