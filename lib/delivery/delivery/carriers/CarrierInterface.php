<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\delivery\carriers;


use common\models\UserAddress;
use EasyPost\CustomsItem;
use lib\delivery\carrier\models\CarrierRate;
use lib\delivery\parcel\Parcel;

/**
 * Interface CarrierInterface
 * @package lib\delivery\delivery\carriers
 */
interface CarrierInterface
{
    /**
     * Calculate rate
     * @param UserAddress $from
     * @param UserAddress $to
     * @param \lib\delivery\parcel\Parcel $parcel
     * @param CustomsItem[] $criteriaCustoms
     * @return CarrierRate|null
     */
    public function calculateRate(UserAddress $from, UserAddress $to, Parcel $parcel, $criteriaCustoms = []);

    public function expressRate(UserAddress $from, UserAddress $to, Parcel $parcel, $criteriaCustoms = []):?CarrierRate;

    /**
     * Calculate estimate rate
     * @param string $fromCountry
     * @param string $toCountry
     * @param Parcel $parcel
     * @return CarrierRate|null
     */
    public function calculateEstimateRate(string $fromCountry = null, string $toCountry = null, Parcel $parcel);

}