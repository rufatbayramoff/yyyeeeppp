<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\delivery\carriers;


use common\models\GeoCountry;
use common\models\PsMachineDelivery;
use common\models\UserAddress;
use EasyPost\CustomsItem;
use lib\delivery\carrier\models\CarrierRate;
use lib\delivery\carrier\models\RatesCriteria;
use lib\delivery\delivery\models\DeliveryRate;
use lib\delivery\parcel\Parcel;
use lib\delivery\parcel\ParcelFactory;
use lib\money\Currency;
use Yii;

/**
 * Class TreatstockCarrier
 * @package lib\delivery\delivery\carriers
 */
class TreatstockCarrier implements CarrierInterface
{
    /** @var PsMachineDelivery */
    protected $psPrinterDelivery;
    protected $carrierService;

    public function __construct(PsMachineDelivery $psPrinterDelivery)
    {
        $this->psPrinterDelivery = $psPrinterDelivery;
        $this->carrierService = Yii::$app->carrierService;
    }

    /**
     * Calculate rate
     * @param UserAddress $from
     * @param UserAddress $to
     * @param Parcel $parcel
     * @param CustomsItem[] $customs
     * @return CarrierRate|null
     */
    public function calculateRate(UserAddress $from, UserAddress $to, Parcel $parcel, $customs = [])
    {
        return $this->carrierService->getLowestRate($this->ratesCriteria($from, $to, $parcel, $customs));
    }

    /**
     * @param UserAddress $from
     * @param UserAddress $to
     * @param Parcel $parcel
     * @param array $customs
     * @return CarrierRate|null
     */
    public function expressRate(UserAddress $from, UserAddress $to, Parcel $parcel, $customs = []): ?CarrierRate
    {
        return CarrierRate::create(DeliveryRate::EXPRESS_RATE, $this->psPrinterDelivery->psMachine->getCurrency());
    }

    /**
     * Calculate estimate rate
     * @param string $fromCountry
     * @param string $toCountry
     * @param Parcel $parcel
     * @return CarrierRate|null
     */
    public function calculateEstimateRate(string $fromCountry = null, string $toCountry = null, Parcel $parcel)
    {
        if($this->isCanadaToCanada($fromCountry, $toCountry)) {
            return CarrierRate::create(15.0, $this->psPrinterDelivery->psMachine->getCurrency());
        }
        if ($fromCountry===GeoCountry::ISO_US && $toCountry===GeoCountry::ISO_PUERTO_RICO) {

        } elseif ($fromCountry != $toCountry) {
            return null;
        } elseif ($fromCountry != 'US') {
            return null;
        }

        $parcelInch = ParcelFactory::convertParcelInInch($parcel);
        if($parcelInch->weight <= 13) {
            return CarrierRate::create(3.6, $this->psPrinterDelivery->psMachine->getCurrency());
        }

        if($parcelInch->weight < 16) {
            return CarrierRate::create(6.0, $this->psPrinterDelivery->psMachine->getCurrency());
        }

        if($parcelInch->weight < 36) {
            return CarrierRate::create(10.0, $this->psPrinterDelivery->psMachine->getCurrency());
        }

        if($parcelInch->weight < 100) {
            return CarrierRate::create(15.0, $this->psPrinterDelivery->psMachine->getCurrency());
        }
        return null;
    }

    protected function ratesCriteria(UserAddress $from, UserAddress $to, Parcel $parcel, $customs = []): RatesCriteria
    {
        return RatesCriteria::create()
            ->from($from)
            ->to($to)
            ->parcel($parcel)
            ->customs($customs);
    }

    private function isCanadaToCanada(?string $fromCountry, ?string $toCountry): bool
    {
        return $fromCountry === GeoCountry::ISO_CA && $toCountry === GeoCountry::ISO_CA;
    }


}