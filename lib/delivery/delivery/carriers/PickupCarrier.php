<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\delivery\carriers;


use common\models\PsMachineDelivery;
use common\models\UserAddress;
use EasyPost\CustomsItem;
use lib\delivery\carrier\models\CarrierRate;
use lib\delivery\parcel\Parcel;
use lib\money\Currency;

class PickupCarrier implements CarrierInterface
{
    /** @var PsMachineDelivery */
    protected $psPrinterDelivery;

    public function __construct(PsMachineDelivery $psPrinterDelivery)
    {
        $this->psPrinterDelivery = $psPrinterDelivery;
    }

    /**
     * Calculate rate
     * @param UserAddress $from
     * @param UserAddress $to
     * @param \lib\delivery\parcel\Parcel $parcel
     * @param CustomsItem[] $criteriaCustoms
     * @return CarrierRate|null
     */
    public function calculateRate(UserAddress $from, UserAddress $to, Parcel $parcel, $criteriaCustoms = [])
    {
        return CarrierRate::create(0.0, $this->psPrinterDelivery->psMachine->getCurrency());
    }

    /**
     * Calculate estimate rate
     * @param string $fromCountry
     * @param string $toCountry
     * @param Parcel $parcel
     * @return CarrierRate
     */
    public function calculateEstimateRate(string $fromCountry = null, string $toCountry = null, Parcel $parcel)
    {
        return CarrierRate::create(0.0, $this->psPrinterDelivery->psMachine->getCurrency());
    }

    public function expressRate(UserAddress $from, UserAddress $to, Parcel $parcel, $criteriaCustoms = []):?CarrierRate
    {
        return null;
    }
}