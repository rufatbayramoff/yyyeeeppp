<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\delivery\carriers;


use common\models\PsMachineDelivery;
use common\models\repositories\PsMachineDeliveryCountryRepository;
use common\models\UserAddress;
use EasyPost\CustomsItem;
use lib\delivery\carrier\models\CarrierRate;
use lib\delivery\parcel\Parcel;
use lib\money\Currency;
use Yii;

/**
 * Class MyselfCarrier
 * @package lib\delivery\delivery\carriers
 */
class MyselfCarrier implements CarrierInterface
{

    public function __construct(private PsMachineDelivery $psPrinterDelivery){}

    /**
     * Calculate rate
     * @param UserAddress $from
     * @param UserAddress $to
     * @param Parcel $parcel
     * @param CustomsItem[] $criteriaCustoms
     * @return CarrierRate|null
     */
    public function calculateRate(UserAddress $from, UserAddress $to, Parcel $parcel, $criteriaCustoms = [])
    {
        return CarrierRate::create((float)$this->psPrinterDelivery->carrier_price, $this->psPrinterDelivery->psMachine->getCurrency());
    }

    /**
     * Calculate estimate rate
     * @param string $fromCountry
     * @param string $toCountry
     * @param Parcel $parcel
     * @return CarrierRate
     */
    public function calculateEstimateRate(string $fromCountry = null, string $toCountry = null, Parcel $parcel)
    {
        if($this->psPrinterDelivery->canDeliveryToCountry() && $toCountry) {
            $deliveryCountry = $this->repository()->findDeliveryByIso($toCountry,$this->psPrinterDelivery->id);
            if($deliveryCountry && $deliveryCountry->price){
                return CarrierRate::create((float)$deliveryCountry->price, $this->psPrinterDelivery->psMachine->getCurrency());
            }
        }
        return CarrierRate::create((float)$this->psPrinterDelivery->carrier_price, $this->psPrinterDelivery->psMachine->getCurrency());
    }

    public function expressRate(UserAddress $from, UserAddress $to, Parcel $parcel, $criteriaCustoms = []):?CarrierRate
    {
        return null;
    }

    /**
     * @return PsMachineDeliveryCountryRepository
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    protected function repository(): PsMachineDeliveryCountryRepository
    {
        return Yii::$container->get(PsMachineDeliveryCountryRepository::class);
    }

}