<?php

namespace lib\delivery\parcel\boxpacker;


use common\interfaces\Model3dBasePartInterface;
use common\models\CuttingPackPart;
use common\models\Model3dReplicaPart;
use DVDoug\BoxPacker\Item;

class CuttingPartItemAdapter implements Item
{
    private $modelWeight;
    private $modelWidth;
    private $modelLength;
    private $modelDepth;
    private $title;

    /**
     * ModelPartItemAdapter constructor.
     * @param CuttingPackPart $cuttingPart
     */
    public function __construct(CuttingPackPart $cuttingPart)
    {
        $this->title       = $cuttingPart->getTitle();
        $this->modelWeight = $cuttingPart->getWeight();
        $this->modelWidth  = (float)$cuttingPart->width;
        $this->modelLength = (float)$cuttingPart->height;
        $this->modelDepth  = (float)$cuttingPart->calcThickness();
    }

    /**
     * Item SKU etc
     * @return string
     */
    public function getDescription()
    {
        return $this->title;
    }

    /**
     * Item width in mm
     * @return int
     */
    public function getWidth()
    {
        return $this->modelWidth;
    }

    /**
     * Item length in mm
     * @return int
     */
    public function getLength()
    {
        return $this->modelLength;
    }

    /**
     * Item depth in mm
     * @return int
     */
    public function getDepth()
    {
        return $this->modelDepth;
    }

    /**
     * Item weight in g
     * @return int
     */
    public function getWeight()
    {
        return $this->modelWeight;
    }

    /**
     * Item volume in mm^3
     * @return int
     */
    public function getVolume()
    {
        return $this->modelWidth * $this->modelDepth * $this->modelLength;
    }

    /**
     * Does this item need to be kept flat?
     * XXX not yet used, all items are kept flat
     * @return bool
     */
    public function getKeepFlat()
    {
        return false;
    }
}