<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\parcel\boxpacker;


use common\interfaces\Model3dBasePartInterface;
use common\models\Model3dReplicaPart;
use DVDoug\BoxPacker\Item;

class ModelPartItemAdapter implements Item
{
    private $modelWeight;
    private $modelWidth;
    private $modelLength;
    private $modelDepth;
    private $title;

    /**
     * ModelPartItemAdapter constructor.
     * @param Model3dBasePartInterface|Model3dReplicaPart $modelPart
     */
    public function __construct(Model3dBasePartInterface $modelPart)
    {
        $this->title = $modelPart->title;
        $this->modelWeight = $modelPart->getWeight() / $modelPart->qty;
        $this->modelWidth = (float) ($modelPart->model3dPartProperties->width ?? 0);
        $this->modelLength = (float) ($modelPart->model3dPartProperties->length ?? 0);
        $this->modelDepth = (float) ($modelPart->model3dPartProperties->height ?? 0);
    }

    /**
     * Item SKU etc
     * @return string
     */
    public function getDescription()
    {
        return $this->title;
    }

    /**
     * Item width in mm
     * @return int
     */
    public function getWidth()
    {
        return $this->modelWidth;
    }

    /**
     * Item length in mm
     * @return int
     */
    public function getLength()
    {
        return $this->modelLength;
    }

    /**
     * Item depth in mm
     * @return int
     */
    public function getDepth()
    {
        return $this->modelDepth;
    }

    /**
     * Item weight in g
     * @return int
     */
    public function getWeight()
    {
        return $this->modelWeight;
    }

    /**
     * Item volume in mm^3
     * @return int
     */
    public function getVolume()
    {
        return $this->modelWidth * $this->modelDepth * $this->modelLength;
    }

    /**
     * Does this item need to be kept flat?
     * XXX not yet used, all items are kept flat
     * @return bool
     */
    public function getKeepFlat()
    {
        return false;
    }
}