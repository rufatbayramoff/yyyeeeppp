<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\parcel\boxpacker;


use common\components\exceptions\AssertHelper;

class Box implements \DVDoug\BoxPacker\Box
{
    private $name;
    private $width;
    private $height;
    private $length;
    private $emptyWeight;

    /**
     * TestBox constructor.
     * @param string $name
     * @param float $width
     * @param float $height
     * @param float $length
     * @param float $emptyWeight
     */
    public function __construct($name, $width, $height, $length, $emptyWeight)
    {
        AssertHelper::assert($name);
        AssertHelper::assertNumeric($width);
        AssertHelper::assertNumeric($height);
        AssertHelper::assertNumeric($length);
        AssertHelper::assertNumeric($emptyWeight);

        $this->name = $name;
        $this->width = $width;
        $this->height = $height;
        $this->length = $length;
        $this->emptyWeight = $emptyWeight;
    }

    /**
     * Reference for box type (e.g. SKU or description)
     * @return string
     */
    public function getReference()
    {
        return $this->name;
    }

    /**
     * Outer width in mm
     * @return int
     */
    public function getOuterWidth()
    {
        return $this->width;
    }

    /**
     * Outer length in mm
     * @return int
     */
    public function getOuterLength()
    {
        return $this->length;
    }

    /**
     * Outer depth in mm
     * @return int
     */
    public function getOuterDepth()
    {
        return $this->height;
    }

    /**
     * Empty weight in g
     * @return int
     */
    public function getEmptyWeight()
    {
        return $this->emptyWeight;
    }

    /**
     * Inner width in mm
     * @return int
     */
    public function getInnerWidth()
    {
        return $this->width;
    }

    /**
     * Inner length in mm
     * @return int
     */
    public function getInnerLength()
    {
        return $this->length;
    }

    /**
     * Inner depth in mm
     * @return int
     */
    public function getInnerDepth()
    {
        return $this->height;
    }

    /**
     * Total inner volume of packing in mm^3
     * @return int
     */
    public function getInnerVolume()
    {
        return $this->length * $this->height * $this->length;
    }

    /**
     * Max weight the packaging can hold in g
     * @return int
     */
    public function getMaxWeight()
    {
        return PHP_INT_MAX;
    }
}