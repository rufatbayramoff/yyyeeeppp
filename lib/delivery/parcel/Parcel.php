<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\parcel;


use common\components\exceptions\AssertHelper;
use lib\MeasurementUtil;
use yii\base\Model;

/**
 * Class Parcel
 *
 * @package lib\delivery\parcel
 */
class Parcel extends Model
{

    /**
     * Parcel constructor.
     *
     * @param float $width Parcel width in measure (in, mm)
     * @param float $height Parcel height in measure (in, mm)
     * @param float $length Parcel length in measure (in, mm)
     * @param float $weight Parcel weight in measure (ounce, gr)
     * @param float $measure
     */
    public function __construct($width, $height, $length, $weight, $measure = 'mm')
    {
        parent::__construct();

        AssertHelper::assertNumeric($width, "Invalid width: {$width}");
        AssertHelper::assertNumeric($height, "Invalid height: {$height}");
        AssertHelper::assertNumeric($length, "Invalid length: {$length}");
        AssertHelper::assertNumeric($weight, "Invalid weight: {$weight}");

        $this->width = $width;
        $this->height = $height;
        $this->length = $length;
        $this->weight = $weight;
        $this->measure = $measure;
    }

    /**
     * Parcel width in inch
     *
     * @var float
     */
    public $width;

    /**
     * Parcel height in inch
     *
     * @var float
     */
    public $height;

    /**
     * Parcel length in inch
     *
     * @var float
     */
    public $length;

    /**
     * Parcel weight in ounce
     *
     * @var float
     */
    public $weight;


    /**
     * Measure
     *
     * @var float|string
     */
    public $measure;

    public function isZerro()
    {
        return !$this->width && !$this->height && !$this->length;
    }

    public function getVolumeMM()
    {
        if ($this->measure === 'in') {
            return MeasurementUtil::convertInchesToMm($this->width) * MeasurementUtil::convertInchesToMm($this->height) * MeasurementUtil::convertInchesToMm($this->length);
        } else {
            return $this->width * $this->height * $this->length;
        }
    }

    public function getVolumeInch()
    {
        if ($this->measure === 'mm') {
            return MeasurementUtil::convertMillimetersToInches($this->width) * MeasurementUtil::convertMillimetersToInches($this->height) * MeasurementUtil::convertMillimetersToInches($this->length);
        } else {
            return $this->width * $this->height * $this->length;
        }
    }

    protected function getWidthInch()
    {
        if ($this->measure === 'mm') {
            return MeasurementUtil::convertMillimetersToInches($this->width);
        }
        return $this->width;
    }

    protected function getHeightInch()
    {
        if ($this->measure === 'mm') {
            return MeasurementUtil::convertMillimetersToInches($this->height);
        }
        return $this->height;
    }

    protected function getLengthInch()
    {
        if ($this->measure === 'mm') {
            return MeasurementUtil::convertMillimetersToInches($this->length);
        }
        return $this->length;
    }

    /**
     * @return array
     */
    public function getInchFormat(): array
    {
        return [$this->getWidthInch(), $this->getHeightInch(), $this->getLengthInch()];
    }


    /**
     * @return array
     */
    public function getMMFormat(): array
    {
        return [MeasurementUtil::convertInchesToMm($this->getWidthInch()), MeasurementUtil::convertInchesToMm($this->getHeightInch()), MeasurementUtil::convertInchesToMm($this->getLengthInch())];
    }
}