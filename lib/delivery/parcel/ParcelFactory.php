<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\parcel;

use common\interfaces\Model3dBaseInterface;
use common\models\Cart;
use common\models\CuttingPack;
use common\models\StoreOrder;
use DVDoug\BoxPacker\PackedBox;
use DVDoug\BoxPacker\Packer;
use frontend\models\store\StoreFacade;
use lib\delivery\parcel\boxpacker\Box;
use lib\delivery\parcel\boxpacker\CuttingPartItemAdapter;
use lib\delivery\parcel\boxpacker\ModelPartItemAdapter;
use lib\MeasurementUtil;

/**
 * Class ParcelFactory
 *
 * @package lib\delivery\parcel
 */
class ParcelFactory
{
    const BUBBLE_MATERIAL_WEIGHT_OUNCE = 0.5;
    const MAX_COUNT_THINGS_IN_PARCEL_DIRECTLY_CALC = 50;

    /**
     * Create parcel from cart
     *
     * @param Cart $cart
     * @return Parcel
     */
    public static function createFromCart(Cart $cart)
    {
        if (self::isParcelByCartVolumeCalc($cart)) {
            return self::createParcelFromCartVolume($cart);
        }

        $packer = self::createPacker();

        foreach ($cart->cartItems as $cartItem) {
            $modelParts = $cartItem->model3dReplica->getActiveModel3dParts();
            foreach ($modelParts as $part) {
                $realQty = $cartItem->qty * $part->qty;
                if ($realQty) {
                    $packer->addItem(new ModelPartItemAdapter($part), $realQty);
                }
            }
        }

        return self::createParcelFromPacker($packer);
    }

    /**
     * Create parcel from order
     *
     * @param StoreOrder $storeOrder
     * @return Parcel
     */
    public static function createFromOrder(StoreOrder $storeOrder)
    {
        if ($storeOrder->hasCuttingPack()) {
            return self::createFromCuttingPack($storeOrder->getCuttingPack());
        }

        if (self::isParcelByStoreOrderVolumeCalc($storeOrder)) {
            return self::createParcelFromStoreOrderVolume($storeOrder);
        }

        $packer = self::createPacker();
        foreach ($storeOrder->storeOrderItems as $orderItem) {
            $modelParts = StoreFacade::getSnapModel3dParts($orderItem);
            foreach ($modelParts as $part) {
                $realQty = $orderItem->qty * $part->qty;
                if ($realQty) {
                    $packer->addItem(new ModelPartItemAdapter($part), $realQty);
                }
            }
        }

        return self::createParcelFromPacker($packer);
    }

    /**
     * Create parcel from model or replica
     *
     * @param Model3dBaseInterface $model
     * @param int $qty
     * @return Parcel
     */
    public static function createFromModel(Model3dBaseInterface $model, $qty = 1)
    {
        if (self::isParcelByVolumeCalc($model, $qty)) {
            return self::createParcelFromVolume($model->getBoxVolume() * $qty, $model->getWeight());
        }

        $packer = self::createPacker();
        $modelParts = $model->getActiveModel3dParts();
        foreach ($modelParts as $part) {
            $realQty = $qty * $part->qty;
            if ($realQty) {
                $packer->addItem(new ModelPartItemAdapter($part), $realQty);
            }
        }
        return self::createParcelFromPacker($packer);
    }

    /**
     * Create packer with allowed boxes.
     * Packer work with mm and gramms
     *
     * @return Packer
     */
    protected static function createPacker()
    {
        $packer = new Packer();
        foreach (self::getAllowedParcelsInMM() as $title => $parcel) {
            $packer->addBox(new Box($title, $parcel->width, $parcel->height, $parcel->length, $parcel->weight));
        }
        return $packer;
    }

    /**
     * @param Packer $packer
     * @return \lib\delivery\parcel\Parcel
     */
    protected static function createParcelFromPacker(Packer $packer)
    {
        /** @var PackedBox $packedBox */
        $packedBox = $packer->pack()->current();

        $boxName = $packedBox->getBox()->getReference();
        $boxName = $boxName == 'Inifinity Box' ? 'Large Flat Rate Box' : $boxName;

        $parcel = clone self::getAllowedParcelsInMM()[$boxName];
        $parcel->weight = $packedBox->getWeight() + self::calculateBubleWeightInGR($parcel);

        return $parcel;
    }

    protected static function createParcelFromCartVolume(Cart $cart)
    {
        $cartVolume = 0;
        $cartWeight = 0;
        foreach ($cart->cartItems as $cartItem) {
            $cartVolume += $cartItem->model3dReplica->getBoxVolume() * $cartItem->qty;
            $cartWeight += $cartItem->model3dReplica->getWeight() * $cartItem->qty;
        }
        return self::createParcelFromVolume($cartVolume, $cartWeight);
    }

    protected static function createParcelFromStoreOrderVolume(StoreOrder $storeOrder)
    {
        $orderVolume = 0;
        $orderWight = 0;
        foreach ($storeOrder->storeOrderItems as $orderItem) {
            $orderVolume += $orderItem->model3dReplica->getBoxVolume() * $orderItem->qty;
            $orderWight += $orderItem->model3dReplica->getWeight() * $orderItem->qty;
        }
        return self::createParcelFromVolume($orderVolume, $orderWight);
    }

    /**
     * @param Parcel $parcel
     * @return Parcel
     */
    public static function convertParcelInInch($parcel)
    {
        if ($parcel->measure === 'in') {
            return $parcel;
        }
        $inchParcel = clone $parcel;
        $inchParcel->width = max(round(MeasurementUtil::convertMillimetersToInches($parcel->width), 2), 1);
        $inchParcel->height = max(round(MeasurementUtil::convertMillimetersToInches($parcel->height), 2), 1);
        $inchParcel->length = max(round(MeasurementUtil::convertMillimetersToInches($parcel->length), 2), 1);
        $inchParcel->weight = max(round(MeasurementUtil::convertGramToOunce($parcel->weight), 2), 1);
        $inchParcel->measure = 'in';
        return $inchParcel;
    }

    /**
     * @param Parcel $parcel
     * @return Parcel
     */
    public static function convertParcelFromInch($parcel)
    {
        if ($parcel->measure === 'mm') {
            return $parcel;
        }
        $mmParcel = clone $parcel;
        $mmParcel->width = max(round(MeasurementUtil::convertInchesToMm($parcel->width), 2), 1);
        $mmParcel->height = max(round(MeasurementUtil::convertInchesToMm($parcel->height), 2), 1);
        $mmParcel->length = max(round(MeasurementUtil::convertInchesToMm($parcel->length), 2), 1);
        $mmParcel->weight = max(round(MeasurementUtil::convertOunceToGram($parcel->weight), 2), 1);
        $mmParcel->measure = 'mm';
        return $mmParcel;
    }


    protected static function createParcelFromVolume($volume, $weight)
    {
        $allowedParcels = self::getAllowedParcelsInMM();
        foreach ($allowedParcels as $parcelName => $allowedParcel) {
            if ($parcelName === 'Inifinity Box') {
                continue;
            }
            if ($allowedParcel->getVolumeMM() > $volume) {
                $allowedParcel = clone $allowedParcel;
                $allowedParcel->weight = $allowedParcel->weight + self::calculateBubleWeightInGR($allowedParcel) + $weight;
                return $allowedParcel;
            }
        }
        $returnParcel = clone $allowedParcels['Large Flat Rate Box'];
        $returnParcel->weight = $returnParcel->weight + self::calculateBubleWeightInGR($returnParcel) + $weight;
        return $returnParcel;
    }

    /**
     * @return Parcel[]
     */
    protected static function getAllowedParcelsInMM()
    {
        static $parcels;
        if (!$parcels) {
            $parcels = [
                'Small Flat Rate Box'                 => new Parcel(
                    5.375 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    8.625 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    1.625 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    3 * MeasurementUtil::OUNCE_TO_GRAM
                ),
                'Medium Flat Rate Box (top loading)'  => new Parcel(
                    11 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    8.5 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    5.5 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    3.4 * MeasurementUtil::OUNCE_TO_GRAM
                ),
                'Medium Flat Rate Box (side loading)' => new Parcel(
                    11.875 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    3.375 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    13.625 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    3.4 * MeasurementUtil::OUNCE_TO_GRAM
                ),
                'Large Flat Rate Box'                 => new Parcel(
                    12 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    12 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    5.5 * MeasurementUtil::MILLIMETERS_TO_INCHES,
                    8 * MeasurementUtil::OUNCE_TO_GRAM
                ),
                'Inifinity Box'                       => new Parcel(
                    PHP_INT_MAX,
                    PHP_INT_MAX,
                    PHP_INT_MAX,
                    11 * MeasurementUtil::OUNCE_TO_GRAM
                ),
            ];
        }
        return $parcels;
    }

    /**
     * @param Parcel $parcel
     * @return float
     */
    protected static function calculateBubleWeightInGR(Parcel $parcel)
    {
        $areaMM = 2 * ($parcel->width * $parcel->height + $parcel->width * $parcel->length + $parcel->height * $parcel->length);
        return round($areaMM * 0.000406876, 2);
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param int $qty
     * @return int
     */
    public static function getModel3dPartsCount(Model3dBaseInterface $model3d, $qty = 1)
    {
        $modelPartsQty = 0;
        foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
            $modelPartsQty += $model3dPart->qty;
        }
        return $modelPartsQty;

    }

    protected static function isParcelByCartVolumeCalc(Cart $cart)
    {
        $cartPartsCount = 0;
        foreach ($cart->cartItems as $cartItem) {
            $cartPartsCount += self::getModel3dPartsCount($cartItem->model3dReplica, $cartItem->qty);
        }
        if ($cartPartsCount > self::MAX_COUNT_THINGS_IN_PARCEL_DIRECTLY_CALC) {
            return true;
        }
        return false;
    }

    protected static function isParcelByStoreOrderVolumeCalc(StoreOrder $storeOrder)
    {
        $orderPartsCount = 0;
        foreach ($storeOrder->storeOrderItems as $orderItem) {
            $orderPartsCount += self::getModel3dPartsCount($orderItem->model3dReplica, $orderItem->qty);
        }
        if ($orderPartsCount > self::MAX_COUNT_THINGS_IN_PARCEL_DIRECTLY_CALC) {
            return true;
        }
        return false;
    }

    protected static function isParcelByVolumeCalc(Model3dBaseInterface $model3d, $qty)
    {
        if (self::getModel3dPartsCount($model3d, $qty) > self::MAX_COUNT_THINGS_IN_PARCEL_DIRECTLY_CALC) {
            return true;
        }
        return false;
    }

    public static function createFromCuttingPack(CuttingPack $cuttingPack)
    {
        $packer = self::createPacker();
        foreach ($cuttingPack->getActiveCuttingParts() as $cuttingPart) {
            $realQty = $cuttingPart->qty;
            if ($realQty) {
                $packer->addItem(new CuttingPartItemAdapter($cuttingPart), $realQty);
            }
        }
        return self::createParcelFromPacker($packer);
    }

    /**
     * @param $parcelJson
     * @return Parcel
     */
    public static function createFromJson($parcelJson)
    {
        $parcel = new Parcel(
            round($parcelJson['width'], 2),
            round($parcelJson['height'], 2),
            round($parcelJson['length'], 2),
            round($parcelJson['weight'], 2),
            $parcelJson['measure'] ?? 'mm'
        );
        return $parcel;
    }
}