<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\carrier\models;


use lib\money\Currency;
use lib\money\Money;
use yii\base\Arrayable;

class CarrierRate implements Arrayable
{
    /**
     * Rate price
     * @var float
     */
    public $rate;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var \EasyPost\Shipment
     */
    public $rawShipment;

    /**
     * @var \EasyPost\Rate
     */
    public $rawRate;

    public const MAX_INCREASE_COST        = 5;
    public const MAX_INCREASE_COEFFICIENT = 0.15;

    /**
     * @inheritdoc
     */
    public function fields()
    {
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
    }

    public function isExpress():bool
    {
        if(!$this->rawRate) {
            return false;
        }

        return $this->rawRate->service === 'Express';

    }

    /**
     * @inheritdoc
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        return [
            'rate' => $this->rate,
            'currency' => $this->currency,
        ];
    }

    public function isTwoTimesExpensive(Money $shipping): bool
    {
        $shippingPay = $shipping->getAmount() * 2;
        return
            $shippingPay <= $this->rate &&
            $shipping->getCurrency() === $this->currency;
    }

    public function getPrice(): Money
    {
        return Money::create($this->rate, $this->currency);
    }

    /**
     * @return Money
     */
    public function priceWithMarkup(): Money
    {
        $rate = $this->rate;
        $currency = $this->currency;
        $rate += min($rate * self::MAX_INCREASE_COEFFICIENT, self::MAX_INCREASE_COST);
        return Money::create($rate, $currency);
    }

    public static function create(float $cost, string $currency): CarrierRate
    {
        $rate = new self();
        $rate->rate = $cost;
        $rate->currency = $currency;
        return $rate;
    }
}