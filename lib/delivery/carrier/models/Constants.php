<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\carrier\models;


class Constants
{
    /**
     * Unknown delivery status
     */
    const DELIVERY_STATE_UNKNOWN = 'unknown';

    /**
     * Parcel delivered to customer
     */
    const DELIVERY_STATE_DELIVERED = 'delivered';
}