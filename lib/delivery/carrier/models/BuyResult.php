<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\carrier\models;
use lib\delivery\parcel\Parcel;


/**
 * Class BuyResult
 * @package lib\delivery\models
 */
class BuyResult
{
    /**
     * @var string
     */
    public $carrier;
    /**
     * @var string
     */
    private $trackingCode;

    /**
     * @var string
     */
    private $labelUrl;

    /**
     * @var Parcel
     */
    private $parcel;

    private $publicUrl;

    /**
     * BuyResult constructor.
     *
     * @param $carrier
     * @param $trackingCode
     * @param $labelUrl
     * @param Parcel $parcel
     */
    public function __construct(
        $carrier,
        $trackingCode,
        $labelUrl,
        $publicUrl,
        Parcel $parcel)
    {
        $this->carrier = $carrier;
        $this->trackingCode = $trackingCode;
        $this->labelUrl = $labelUrl;
        $this->publicUrl = $publicUrl;
        $this->parcel = $parcel;
    }

    /**
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }
    /**
     * @return string
     */
    public function getTrackingCode()
    {
        return $this->trackingCode;
    }

    /**
     * @return string
     */
    public function getLabelUrl()
    {
        return $this->labelUrl;
    }

    /**
     * @return Parcel
     */
    public function getParcel()
    {
        return $this->parcel;
    }

    /**
     * @return mixed
     */
    public function getPublicUrl()
    {
        return $this->publicUrl;
    }
}