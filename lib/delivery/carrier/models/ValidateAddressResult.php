<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\carrier\models;


use common\models\UserAddress;

/**
 * Class ValidateAddressResult
 * @package lib\delivery
 */
class ValidateAddressResult
{
    /**
     * @var bool
     */
    private $isSuccess;

    /**
     * @var string
     */
    private $errorMessage;

    /**
     * @var UserAddress
     */
    private $hintAddress;

    /**
     * ValidateAddressResult constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param UserAddress $hintAddress
     * @return static
     */
    public static function createSuccess(UserAddress $hintAddress = null)
    {
        $result = new static();
        $result->isSuccess = true;
        $result->hintAddress = $hintAddress;
        return $result;
    }

    /**
     * @param string $errorMessage
     * @return static
     */
    public static function createFail($errorMessage)
    {
        $result = new static();
        $result->isSuccess = false;
        $result->errorMessage = $errorMessage;
        return $result;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->isSuccess;
    }

    /**
     * Return hint address.
     * If validate full valid - return null, else - return suggest address
     * @return UserAddress|null
     */
    public function getSuggestAddress()
    {
        return $this->hintAddress;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
}