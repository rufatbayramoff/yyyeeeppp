<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\carrier\models;


use common\models\StoreOrderAttemp;
use common\models\UserAddress;
use EasyPost\CustomsItem;
use lib\delivery\parcel\Parcel;

/**
 * Class RatesCriteria
 * @package lib\delivery
 */
class RatesCriteria
{
    private $from;
    private $to;
    private $parcel;
    private $customs;
    /**
     * @var boolean|null
     */
    private $isExpress;

    /**
     * RatesCriteria constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return static
     */
    public static function create()
    {
        return new static();
    }

    /**
     * @param UserAddress $address
     * @return $this
     */
    public function from(UserAddress $address)
    {
        $this->from = $address;
        return $this;
    }

    /**
     * @param UserAddress $address
     * @return $this
     */
    public function to(UserAddress $address)
    {
        $this->to = $address;
        return $this;
    }

    /**
     * @param Parcel $parcel
     * @return $this
     */
    public function parcel(Parcel $parcel)
    {
        $this->parcel = $parcel;
        return $this;
    }

    public function express(bool $isExpress)
    {
        $this->isExpress = $isExpress;
        return $this;
    }

    /**
     * @param CustomsItem[] $customs
     */
    public function customs($customs)
    {
        $this->customs = $customs;
        return $this;
    }

    /**
     * @return UserAddress
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return UserAddress
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return Parcel
     */
    public function getParcel()
    {
        return $this->parcel;
    }

    /**
     * @return StoreOrderAttemp
     */
    public function getCustoms()
    {
        return $this->customs;
    }

    /**
     * @return boolean|null
     */
    public function getExpress(): ?bool
    {
        return $this->isExpress;
    }
}