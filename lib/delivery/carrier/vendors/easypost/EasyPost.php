<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\carrier\vendors\easypost;

use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\models\StoreOrderAttemp;
use common\models\User;
use common\models\UserAddress;
use common\modules\xss\helpers\XssHelper;
use dosamigos\transliterator\TransliteratorHelper;
use lib\delivery\carrier\models\BuyResult;
use lib\delivery\carrier\models\Constants;
use lib\delivery\carrier\models\CarrierRate;
use lib\delivery\exceptions\LargeParcelException;
use lib\delivery\exceptions\UserDeliveryException;
use lib\delivery\parcel\Parcel;
use lib\delivery\carrier\models\RatesCriteria;
use lib\delivery\carrier\models\ValidateAddressResult;
use lib\delivery\carrier\vendors\VendorInterface;
use lib\delivery\parcel\ParcelFactory;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\VarDumper;

/**
 * Class EasyPost
 *
 * @package lib\delivery\vendors
 */
class EasyPost extends Component implements VendorInterface
{
    /**
     * @var string
     */
    const NATIONAL_CARRIER = 'USPS';
    const UPS              = 'UPS';
    const CANADA_POST      = 'CanadaPost';

    const MAX_EASYPOST_DELIVERY_VOLUME = 20000;


    const SHOW_MESSAGE_FROM_CARRIERS = [
        self::NATIONAL_CARRIER,
        self::UPS,
        self::CANADA_POST
    ];


    /**
     * Api key
     *
     * @var string
     */
    public $apikey;

    /**
     * Public api key
     *
     * @var string
     */
    public $publicApikey;

    /**
     *
     */
    public function init()
    {
        parent::init();
        AssertHelper::assert($this->apikey, 'Api key must set', InvalidConfigException::class);
        AssertHelper::assert($this->publicApikey, 'Api key must set', InvalidConfigException::class);
        \EasyPost\EasyPost::setApiKey($this->apikey);
    }

    /**
     * @param string $trackingNumber
     * @param string $trackingShipper
     * @return string One of constant DeliveryService::DELIVERY_STATE
     */
    public function getParcelStatus($trackingNumber, $trackingShipper = '')
    {
        $parcelInfo = $this->getParcelInfo($trackingNumber, $trackingShipper);
        return $this->translateRawStatus($parcelInfo->status, $parcelInfo->status_detail);
    }

    /**
     * @param $trackingNumber
     * @param string $trackingShipper
     * @return \EasyPost\Tracker
     */
    private function getParcelInfo($trackingNumber, $trackingShipper = '')
    {
        $trackerInf = ['tracking_code' => $trackingNumber];
        if (!empty($trackingShipper)) {
            $trackerInf['carrier'] = $trackingShipper;
        }
        $tracker = \EasyPost\Tracker::create($trackerInf);
        return $tracker;
    }

    /**
     * @param $rawStatus
     * @return string
     */
    private function translateRawStatus($rawStatus, $rawStatusDetail)
    {
        if ($rawStatus === 'delivered') {
            return Constants::DELIVERY_STATE_DELIVERED;
        }
        if ($rawStatus === 'available_for_pickup' && $rawStatusDetail === 'arrived_at_destination') {
            return Constants::DELIVERY_STATE_DELIVERED;
        }

        return Constants::DELIVERY_STATE_UNKNOWN;
    }

    /**
     * @param CarrierRate[] $rates
     */
    protected function filterRates($rates)
    {
        $filteredRates = [];
        foreach ($rates as $rate) {
            $carrier = $rate->rawRate->carrier ?? '';
            $service = $rate->rawRate->service ?? '';
            if (
                ($carrier === self::CANADA_POST) &&
                (($service === 'InternationalParcelSurface') || ($service === 'SmallPacketInternationalSurface'))
            ) {
                continue;
            }
            $filteredRates[] = $rate;
        }
        return $filteredRates;
    }

    /**
     * Return all rates
     *
     * @param RatesCriteria $criteria
     * @return CarrierRate[]
     * @throws UserDeliveryException
     */
    public function getRates(RatesCriteria $criteria)
    {
        try {
            $fromAddress = self::resolveRawAddress($criteria->getFrom());
            $toAddress   = self::resolveRawAddress($criteria->getTo());
            $parcel      = self::resolveRawParcel($criteria->getParcel());
            $customs     = $criteria->getCustoms();

            if (empty($fromAddress)) {
                throw new UserDeliveryException(_t('site.delivery', "The shipper's address is incorrect"));
            }
            if (empty($toAddress)) {
                throw new UserDeliveryException(_t('site.delivery', 'Recipient address is not defined'));
            }
            if (empty($parcel)) {
                throw new UserDeliveryException(_t('site.delivery', 'Parcel is not defined'));
            }

            $data = [
                "from_address" => $fromAddress,
                "to_address"   => $toAddress,
                "parcel"       => $parcel
            ];

            if ($customs) {
                $data['customs_info'] = $customs;
            }

            \EasyPost\EasyPost::setApiKey($this->apikey);

            $shipment = \EasyPost\Shipment::create($data);

            if ($shipment->rates) {

                $rates = [];

                foreach ($shipment->rates as $rateId => $rate) {
                    $newRate              = new CarrierRate();
                    $newRate->rate        = floatval($rate->rate);
                    $newRate->currency    = $rate->currency;
                    $newRate->rawRate     = $rate;
                    $newRate->rawShipment = $shipment;
                    $rates[]              = $newRate;
                }

                return $this->filterRates($rates);
            }

            $messages = ArrayHelper::map($shipment->messages, 'carrier', 'message');

            foreach (self::SHOW_MESSAGE_FROM_CARRIERS as $carrier) {
                if (isset($messages[$carrier])) {
                    throw new LargeParcelException($messages[$carrier]);
                }
            }

            throw new LargeParcelException();
        } catch (UserDeliveryException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new UserDeliveryException($e->getMessage(), 0, $e);
        }
    }

    /**
     * Buy shipping
     *
     * @param CarrierRate $rate
     * @return BuyResult
     */
    public function buy(CarrierRate $rate)
    {
        $shipment = $rate->rawShipment;
        /** @var \EasyPost\Shipment $result */
        $result = $shipment->buy($rate->rawRate);
        $parcel = new Parcel((float)$shipment->parcel->width, (float)$shipment->parcel->height, (float)$shipment->parcel->length, (float)$shipment->parcel->weight);
        \Yii::info(VarDumper::dumpAsString($rate->rawRate), 'easypost');
        return new BuyResult(
            $rate->rawRate['carrier'],
            $result->tracking_code,
            $result->postage_label->label_url,
            $result->tracker->public_url,
            $parcel);
    }

    /**
     * Verify address
     *
     * @param UserAddress $address
     * @return ValidateAddressResult
     */
    public function validateAddress(UserAddress $address)
    {
        $isPublickKey = $address->country_id == UserAddress::COUNTRY_USA_ID;

        try {
            if ($isPublickKey) {
                \EasyPost\EasyPost::setApiKey($this->publicApikey);
            }

            if (self::checkRussianLetters($address->address)) {
                $address->address = TransliteratorHelper::process($address->address, '', 'ru');
            }
            if (self::checkRussianLetters($address->city)) {
                $address->city = TransliteratorHelper::process($address->city, '', 'ru');
            }

            $addressParams = self::resolveRawAddressData($address);
            $addressObj    = \EasyPost\Address::create_and_verify($addressParams);
            // Todo move to translate address
            $validAddress = new UserAddress([
                'user_id'          => User::USER_EASYPOST,
                'contact_name'     => $addressObj->name,
                'company'          => $addressObj->company,
                'address'          => $addressObj->street1,
                'extended_address' => $addressObj->street2,
                'city'             => $addressObj->city,
                'region'           => $addressObj->state,
                'zip_code'         => $this->clearZipCode($addressObj->zip),
                'country_id'       => $address->country_id,
                'phone'            => $addressObj->phone,
                'lat'              => $addressObj->lat,
                'lon'              => $addressObj->lon
            ]);

            $result = $this->equalAddresses($address, $validAddress)
                ? ValidateAddressResult::createSuccess(null)
                : ValidateAddressResult::createSuccess($validAddress);
        } catch (\Exception $e) {
            \Yii::error($e, 'easypost');
            \Yii::error($isPublickKey ? 'Public key' : 'Private key', 'easypost');
            $errors       = isset($e->jsonBody, $e->jsonBody['error'], $e->jsonBody['error']['errors'])
                ? ArrayHelper::getColumn($e->jsonBody['error']['errors'], 'message')
                : [];
            $errorMessage = $e->getMessage();
            if ($errors) {
                $errorMessage .= ' ' . implode(', ', $errors) . '.';
            }
            $result = ValidateAddressResult::createFail($errorMessage);
        } finally {
            \EasyPost\EasyPost::setApiKey($this->apikey);
        }
        return $result;
    }

    /**
     * @param UserAddress $address1
     * @param UserAddress $address2
     * @return bool
     */
    private function equalAddresses(UserAddress $address1, UserAddress $address2)
    {
        // @TODO - replace with prepareAddressData and compare arrays, not object values
        $checkProps = ['region', 'city', 'address', 'zip_code'];
        foreach ($checkProps as $k => $v) {
            if (is_string($address1->$v) && is_string($address2->$v)) {
                $address1CleanXss = $address1->$v;
                if (defined('TEST_XSS') && TEST_XSS) {
                    $address1CleanXss = XssHelper::cleanXssProtect($address1CleanXss);
                }
                if (mb_strtoupper($address1CleanXss) !== mb_strtoupper($address2->$v)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param $zipCode
     */
    private function clearZipCode($zipCode)
    {
        return explode('-', $zipCode)[0];
    }

    /**
     * Checks if string has russian letters
     *
     * @param  $string
     * @return bool
     */
    private static function checkRussianLetters($string)
    {
        if (preg_match('#[а-яА-Яё]#i', $string)) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param \lib\delivery\parcel\Parcel $parcel
     * @return \EasyPost\Parcel
     */
    private static function resolveRawParcel(Parcel $parcel)
    {
        $parcelInch = ParcelFactory::convertParcelInInch($parcel); // Easypost working in inches
        $data       = array_filter($parcelInch->attributes);
        return \EasyPost\Parcel::create($data);
    }

    /**
     *
     * @param UserAddress $address
     * @return \EasyPost\Address
     */
    private static function resolveRawAddress(UserAddress $address)
    {
        $data = self::resolveRawAddressData($address);
        return \EasyPost\Address::create($data);
    }

    /**
     * @param UserAddress $address
     * @return array
     */
    private static function resolveRawAddressData(UserAddress $address)
    {
        $data          = [
            'name'    => $address->contact_name,
            'company' => $address->company,
            'phone'   => $address->phone,
            'street1' => $address->address,
            'street2' => $address->extended_address,
            'city'    => $address->city,
            'state'   => $address->region,
            'zip'     => $address->zip_code,
            'country' => $address->country->iso_code,
        ];
        $data          = str_replace(['|', '.'], '', $data);
        $data['email'] = $address->email;
        if (defined('TEST_XSS') && TEST_XSS) {
            $data = XssHelper::cleanArray($data);
        }
        return array_filter($data);
    }
}