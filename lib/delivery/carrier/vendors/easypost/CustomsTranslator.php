<?php

namespace lib\delivery\carrier\vendors\easypost;

use common\interfaces\Model3dBaseInterface;
use common\models\base\StoreOrderItem;
use common\models\CuttingPack;
use common\models\StoreOrderAttemp;
use lib\MeasurementUtil;
use lib\money\Money;
use yii\base\UserException;

/**
 * Specify customs params
 */
class CustomsTranslator
{
    /**
     * @var StoreOrderItem
     */
    private $orderItem;

    /**
     * @var StoreOrderAttemp
     */
    private $attemp;

    /**
     * @param StoreOrderAttemp $attemp
     * @param StoreOrderItem $orderItem
     */
    public function __construct(StoreOrderAttemp $attemp, StoreOrderItem $orderItem)
    {
        $this->orderItem = $orderItem;
        $this->attemp    = $attemp;
    }

    public static function formEasyPostCustomsInfo($uid, $description, $weightGr, Money $price, string $originCountryIso, $customsSigner)
    {
        if (strlen($description) > 40) {
            $description = substr($description, 0, 40) . '...';
        };
        $customsItemParams = [
            'id'               => $uid,
            'description'      => $description,
            'hs_tariff_number' => null,
            'origin_country'   => $originCountryIso,
            'quantity'         => 1,
            'value'            => $price->getAmount(),
            'weight'           => max([round(MeasurementUtil::convertGramToOunce($weightGr), 2), 0.5]),
        ];
        $customsItems[]    = \EasyPost\CustomsItem::create($customsItemParams);

        $customsInfoParams = [
            'customs_certify'      => true,
            'customs_signer'       => $customsSigner,
            'contents_type'        => 'merchandise',
            'contents_explanation' => '', // only necessary for contents_type=other
            'eel_pfc'              => 'NOEEI 30.37(a)',
            //'non_delivery_option'  => 'return',// 'return', // abandon - has error: This product requires a valid value for Non-Delivery Handling.  From Canada To France
            'restriction_type'     => 'none',
            'restriction_comments' => '',
            'customs_items'        => $customsItems
        ];
        $customsInfo       = \EasyPost\CustomsInfo::create($customsInfoParams);
        return $customsInfo;
    }

    /**
     * Translate from attempt
     *
     * @param StoreOrderAttemp $attemp
     * @return mixed
     * @throws UserException
     */
    public static function translateFromAttempt(StoreOrderAttemp $attemp)
    {
        /**
         * @var \common\models\base\StoreOrderItem[] $storeOrderItems
         */
        $order   = $attemp->order;
        $company = $attemp->ps;

        if ($model3d = $order->getFirstReplicaItem()) {
            return self::formEasyPostCustomsInfo('item_' . $model3d->getUid(),
                'Model: ' . $model3d->getTitle(),
                $model3d->getWeight(),
                $order->getPrimaryInvoice()->getAmountTotal(),
                $attemp->machine->location->country->iso_code,
                $company->user->getFullNameOrUsername());
        }
        if ($cuttingPack = $order->getCuttingPack()) {
            return self::formEasyPostCustomsInfo(
                'item_C:' . $cuttingPack->uuid,
                'Cutting: ' . $cuttingPack->getTitle(),
                $cuttingPack->getWeigthGr(),
                $order->getPrimaryInvoice()->getAmountTotal(),
                $attemp->machine->location->country->iso_code,
                $company->user->getFullNameOrUsername()
            );
        }
    }
}
