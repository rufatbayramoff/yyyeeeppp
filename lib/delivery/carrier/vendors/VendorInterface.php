<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\carrier\vendors;


use common\models\UserAddress;
use lib\delivery\carrier\models\BuyResult;
use lib\delivery\carrier\models\CarrierRate;
use lib\delivery\carrier\models\RatesCriteria;
use lib\delivery\carrier\models\ValidateAddressResult;

interface VendorInterface
{
    /**
     * Return parcel state based on trackingNumber
     *
     * @param string $trackingNumber
     * @param string $trackingShipper
     * @return string One of constant DeliveryService::DELIVERY_STATE
     */
    public function getParcelStatus($trackingNumber, $trackingShipper = '');

    /**
     * Return all rates
     * @param RatesCriteria $criteria
     * @return CarrierRate[]
     */
    public function getRates(RatesCriteria $criteria);

    /**
     * Buy shipping
     * @param CarrierRate $rate
     * @return BuyResult
     */
    public function buy(CarrierRate $rate);

    /**
     * Verify address
     * @param UserAddress $address
     * @return ValidateAddressResult
     */
    public function validateAddress(UserAddress $address);
}