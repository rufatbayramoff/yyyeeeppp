<?php
/**
 * Created by mitaichik
 */

namespace lib\delivery\carrier;


use common\components\PaymentExchangeRateConverter;
use common\models\UserAddress;
use lib\delivery\carrier\models\CarrierRate;
use lib\delivery\carrier\models\RatesCriteria;
use lib\delivery\carrier\models\ValidateAddressResult;
use lib\delivery\carrier\vendors\VendorInterface;
use lib\money\Currency;
use yii\base\Component;

/**
 * Class DeliveryService
 * @package lib\delivery
 */
class CarrierService extends Component
{
    /**
     * @var VendorInterface
     */
    public $vendor;
    /**
     * @var PaymentExchangeRateConverter
     */
    protected $moneyConvertor;

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->vendor = \Yii::createObject($this->vendor);
        $this->moneyConvertor = \Yii::createObject(PaymentExchangeRateConverter::class);
    }

    /**
     * Return parcel state based on trackingNumber
     *
     * @param string $trackingNumber
     * @param string $trackingShipper
     * @return string One of constant DeliveryService::DELIVERY_STATE
     */
    public function getParcelStatus($trackingNumber, $trackingShipper = '')
    {
        return $this->vendor->getParcelStatus($trackingNumber, $trackingShipper);
    }

    /**
     * Return all rates
     * @param RatesCriteria $criteria
     * @return CarrierRate[]
     */
    public function getRates(RatesCriteria $criteria)
    {
        return $this->vendor->getRates($criteria);
    }

    /**
     * Return lowest rates
     * @param RatesCriteria $criteria
     * @return CarrierRate|null
     */
    public function getLowestRate(RatesCriteria $criteria)
    {
        $rates = $this->getRates($criteria);

        if (!$rates){
            return null;
        }

        if($criteria->getExpress()) {
            foreach ($rates as $rate) {
                if($rate->isExpress()) {
                    return $this->convert($rate);
                }
            }
            return null;
        }

        $reduced = array_reduce($rates, function(CarrierRate $result = null, CarrierRate $current)
        {
            if ($result == null){
                return $current;
            }
            return $current->rate > $result->rate ? $result : $current;
        });
        return $this->convert($reduced);
    }

    /**
     * Buy shipping
     * @param CarrierRate $rate
     * @return \lib\delivery\carrier\models\BuyResult
     */
    public function buy(CarrierRate $rate)
    {
        return $this->vendor->buy($rate);
    }

    /**
     * Verify address
     * @param UserAddress $address
     * @return ValidateAddressResult
     */
    public function validateAddress(UserAddress $address)
    {
        return $this->vendor->validateAddress($address);
    }

    /**
     * @param CarrierRate $rate
     * @return CarrierRate
     * @throws \Exception
     */
    protected function convert(CarrierRate $rate): CarrierRate
    {
        $rate->rate = $this->moneyConvertor->convert($rate->rate, $rate->currency, Currency::USD, true);
        $rate->currency = Currency::USD;
        return $rate;
    }
}