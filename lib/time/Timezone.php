<?php namespace lib\time;

/**
 * Timezone helper class. 
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Timezone
{

    /**
     *  returns timezone offset, example "+3:00", "-4:30"
     *  
     *  if offset not found, exception will be thrown
     * 
     * @param  string $timeZone - "Europe/Moscow"
     * @return string
     */
    public static function getOffset($timeZone)
    {
        try {
            $dt = new \DateTimeZone($timeZone);
            $now = new \DateTime("now", $dt);
            $mins = $now->getOffset() / 60;
            $sgn = ($mins < 0 ? -1 : 1);
            $mins = abs($mins);
            $hrs = floor($mins / 60);
            $mins -= $hrs * 60;
            $offset = sprintf('%+d:%02d', $hrs * $sgn, $mins);
        } catch (\Exception $ex) {
            $msg = $ex->getMessage(); 
            throw new TimeException($msg);
        }
        return $offset;
    }
    
    public static function getList()
    {
        $li = \DateTimeZone::listIdentifiers();
        return $li;
    }

    public static function getAbbr()
    {
        $abbr = \DateTimeZone::listAbbreviations();
        return $abbr;
    }
}
