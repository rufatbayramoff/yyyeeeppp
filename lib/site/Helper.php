<?php
namespace lib\site;
/**
 * Helper for site
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Helper
{
    /**
     * get site block information based on locale
     * 
     * @param  mixed  $code
     * @param  string $lang
     * @return string
     */
    public static function getBlock($code, $lang = '')
    {
        if(empty($lang)) {
            $lang = \Yii::$app->language;
        }
        /**
 * @var \common\models\SiteBlock $block 
*/
        $block = \common\models\SiteBlock::findOne(
            ['code'=>$code, 'is_active'=>1, 'language'=>$lang]
        );
        if($block===null) {
            return '[' . $code .']';
        }
        return $block->content;
    }
}
