<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\sentry;


use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class RavenAsset
 * @package lib\sentry
 */
class RavenAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@bower/raven-js';

    /**
     * @var array
     */
    public $jsOptions = [
        'position' => View::POS_END,
    ];

    /**
     * @var array
     */
    public $js = [
        YII_DEBUG ? 'dist/raven.js' : 'dist/raven.min.js'
    ];
}