<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace lib\sentry;


use common\components\ArrayHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\InvalidModelException;
use common\components\exceptions\NoActiveModel3d;
use common\components\exceptions\NoCalculatedModel3d;
use common\components\exceptions\OnlyPostRequestException;
use common\components\exceptions\ParcelCalculateException;
use common\components\UserSession;
use common\modules\payment\exception\PaymentException;
use common\modules\promocode\exceptions\NotValidPromocode;
use GeoIp2\Exception\AddressNotFoundException;
use lib\delivery\exceptions\HaveSuggestAddressException;
use lib\delivery\exceptions\UserDeliveryException;
use lib\message\exceptions\BadParamsException;
use Psr\Log\InvalidArgumentException;
use Sentry\Event;
use Sentry\State\Scope;
use yii\log\Target;
use yii\web\ForbiddenHttpException;
use yii\web\GoneHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;

/**
 * Log target for sentry
 * @package lib\sentry
 */
class LogTarget extends Target
{
    /**
     * @var string
     */
    public $dsn;

    /**
     * @var string
     */
    public $dsnTrash;


    /**
     * @var string
     */
    public $logger;

    /**
     * Additional options for sending to sentry
     * @var array
     */
    public $options = [];

    /**
     * Patrs of bot headers
     * @var string[]
     */
    public $botsHeaders = [
        'mj12bot',
        'googlebot',
        'applebot',
    ];

    /**
     * @var \yii\console\Request|\yii\web\Request
     */
    private $request;

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->logVars = [];
        $this->request = \Yii::$app->request;
        foreach ($this->botsHeaders as &$botsHeader) {
            $botsHeader = strtolower($botsHeader);
        }
    }

    /**
     * Exports log [[messages]] to a specific destination.
     * Child classes must implement this method.
     */
    public function export()
    {
        foreach ($this->messages as $message) {
            [$msg, $level, $category] = $message;
            $dsn = $this->dsn;

            if ($category == 'tsdebug') {
                continue;
            }
            if ($this->isTrashException($msg)) {
                $dsn = $this->dsnTrash;
            }

            if ($msg instanceof \Exception && $this->isAllowableException($msg)) {
                continue;
            }

            $options = [
                'level'    => \yii\log\Logger::getLevelName($level),
                'logger'   => $this->logger,
                'extra'    => [
                    'category' => $category
                ],
                'userData' => [],
            ];
            $request = \Yii::$app->getRequest();
            if ($request instanceof Request && $request->getUserIP()) {
                $options['userData']['ip_address'] = $request->getUserIP();
            }

            try {
                /** @var UserSession $user */
                $user = \Yii::$app->has('user', true) ? \Yii::$app->get('user', false) : null;
                if ($user && ($identity = $user->getIdentity(false))) {
                    $options['userData']['id'] = $identity->getId();
                }
            } catch (\Throwable $e) {
            }

            $options = ArrayHelper::merge($options, $this->options);

            \Sentry\init(['dsn' => $dsn]);
            \Sentry\withScope(function (Scope $scope) use ($msg, $level, $options) {
                $scope->setUser($options['userData']);
                if ($msg instanceof \Throwable) {
                    \Sentry\captureException($msg);
                } elseif (is_string($msg)) {
                    $event = Event::createEvent();
                    $event->setMessage($msg);
                    $event->setLevel($options['level']);
                    $event->setLogger($options['logger']);
                    \Sentry\captureEvent($event);
                }
            });

        }
    }

    protected function isExceptionInList($trashList, $exception)
    {
        foreach ($trashList as $exceptionClass) {
            if ($exception instanceof $exceptionClass) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check that exception is bot error
     * @param \Exception $exception
     * @return bool
     */
    private function isAllowableException(\Exception $exception)
    {
        $trashList = [
            \EasyPost\Error::class,
            BusinessException::class,
            GoneHttpException::class,
            AddressNotFoundException::class,
            InvalidModelException::class,
            NotValidPromocode::class,
            ParcelCalculateException::class,
            \common\modules\cnc\api\ApiException::class,
            \backend\components\NoAccessException::class,
            NoActiveModel3d::class,
            NoCalculatedModel3d::class,
            MethodNotAllowedHttpException::class,
            PaymentException::class,
            UserDeliveryException::class,
            OnlyPostRequestException::class,
            HaveSuggestAddressException::class,
        ];
        if ($this->isExceptionInList($trashList, $exception)) {
            return true;
        }

        if ($this->request instanceof \yii\web\Request) {
            if ($exception instanceof NotFoundHttpException && $this->getIsBot()) {
                return true;
            }
        }
        return false;
    }

    protected function isTrashException(\Exception $exception)
    {
        $trashList = [
            \yii\web\BadRequestHttpException::class,
            \yii\web\NotFoundHttpException::class,
            \yii\base\InvalidArgumentException::class,
            InvalidArgumentException::class,
            AddressNotFoundException::class,
            BadParamsException::class,
            ForbiddenHttpException::class,

        ];
        return $this->isExceptionInList($trashList, $exception);
    }

    /**
     * @return bool
     */
    private function getIsBot()
    {
        $userAgnetHeader = strtolower($this->request->headers->get('User-Agent'));
        foreach ($this->botsHeaders as $botHeader) {
            if (strpos($userAgnetHeader, $botHeader) !== false) {
                return true;
            }
        }
        return false;
    }
}