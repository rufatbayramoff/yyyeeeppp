<?php
/**
 * Created by mitaichik
 */

namespace lib\sentry;


use common\components\ArrayHelper;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\web\View;

class Sentry implements BootstrapInterface
{
    /**
     * Is sentry handlers enabled
     * @var bool
     */
    public $enabled = true;

    /**
     * Config for php logger target
     * @var array|null
     */
    public $phpLogger;

    /**
     * Config for php logger target
     * @var array|null
     */
    public $trashLogger;

    /**
     * Config for js logger
     * @var array|null
     */
    public $jsLogger;

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$this->enabled){
            return;
        }

        if ($this->phpLogger){
            $this->checkConfiguration($this->phpLogger);
            $phpLogger = \Yii::$container->get(LogTarget::class, [], $this->phpLogger);
            $app->log->targets[] = $phpLogger;
        }

        if (Yii::$app instanceof \yii\web\Application && $this->jsLogger){
            $this->checkConfiguration($this->jsLogger);
            $app->on(Application::EVENT_BEFORE_REQUEST, function () use ($app) {
                if (!Yii::$app->request->getIsAjax()){
                    $this->registerRavenScript($app);
                }
            });
        }
    }

    /**
     * Check raven configuration
     * @param array $configuration
     * @throws InvalidConfigException
     */
    protected function checkConfiguration(array $configuration)
    {
        if(!isset($configuration['dsn'])) {
            throw new InvalidConfigException('Don`t set dsn for sentry');
        }

        if(!isset($configuration['logger'])) {
            throw new InvalidConfigException('Don`t set logger for sentry');
        }
    }

    /**
     * Register sentry raven init script
     * @param Application $app
     */
    protected function registerRavenScript(Application $app)
    {
        if (YII_DEBUG) {
            return ;
        }
        $app->getView()->registerAssetBundle(RavenAsset::class);
        $options = ['logger' => $this->jsLogger['logger']];
        $options = ArrayHelper::merge($options, $this->resolveCustomOptions());
        $app->getView()->registerJs("Raven.config('{$this->jsLogger['dsn']}').install(".Json::encode($options).");", View::POS_END);
    }

    /**
     * @return array
     */
    protected function resolveCustomOptions()
    {
        if(Yii::$app->has('user') && $userId = Yii::$app->user->getId()) {
            return ['user' => [
                'id' => $userId,
                'name' => @\Yii::$app->user->identity->username
            ]];
        }
        return [];
    }
}