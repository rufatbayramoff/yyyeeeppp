<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 20.10.15
 * Time: 10:12
 */

namespace lib\render;


use yii\base\Exception;

/**
 * Exception on render model
 * @package lib\render
 */
class RenderException extends Exception
{

}