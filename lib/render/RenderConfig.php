<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 19.10.15
 * Time: 17:37
 */

namespace lib\render;
use common\models\Model3d;
use common\modules\model3dRender\Model3dRenderModule;
use Yii;
use yii\base\Model;


/**
 * Config for rendering
 * @package lib\render
 */
class RenderConfig extends Model
{
    /**
     * Width if image
     * @var int
     */
    public $width = 720;

    /**
     * Height of image
     * @var int
     */
    public $height = 540;

    /**
     * Color of model
     * @var string
     */
    public $modelColor;
    
    /**
     * specify render rotate config if required
     * @var RenderConfigRotate
     */
    public $rotateConfig;

    /**
     * specify zoom config 
     * @var RenderConfigZoom
     */
    public $zoomConfig;

    /**
     * @var string
     */
    public $renderType;

    /**
     *   = 1 - using CAM,
     *   = 0 - using CAD
     * default is CAD
     * @var int
     */
    public $plateback = 0;

    /**
     * Init
     */
    public function init()
    {
        parent::init();
        $this->renderType = Yii::$app->getModule('model3dRender')->defaultRenderImageExtension;
        $this->modelColor = Yii::$app->getModule('model3dRender')->defaultRenderImageColor;
    }
}