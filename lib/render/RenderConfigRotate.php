<?php
namespace lib\render;

use common\interfaces\Model3dBasePartInterface;
use Yii;

/**
 * RenderRotateConfig
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class RenderConfigRotate
{

    /**
     * rotate vars
     *
     * @var int
     */
    public $rotateX = 0;
    public $rotateY = 0;
    public $rotateZ = 0;

    public $zoom = 1;
    /**
     * rotate step in degrees
     */
    const ROTATE_STEP = 90;

    /**
     * rotate directions - where to rotate
     */
    const UP = 'up';
    const DOWN = 'down';
    const LEFT = 'left';
    const RIGHT = 'right';

    /**
     *
     * @param string $direction - UP, DOWN, LEFT,  RIGHT
     * @return RenderConfigRotate
     */
    public function rotateModelFile($direction)
    {
        switch ($direction) {
            case self::UP:
                $this->rotateX = $this->rotateX - self::ROTATE_STEP;
                break;
            case self::DOWN:
                $this->rotateX = $this->rotateX + self::ROTATE_STEP;
                break;
            case self::LEFT:
                $this->rotateY = $this->rotateY - self::ROTATE_STEP;
                break;
            case self::RIGHT:
                $this->rotateY = $this->rotateY + self::ROTATE_STEP;
                break;
        }

        return $this;
    }

    /**
     *
     * @param string $zoom
     */
    public function zoomByPercent($zoom)
    {
        $this->zoom = (float)$zoom;
    }

    /**
     * get rotate config for soap render
     *
     * @return array
     */
    public function getRotateConfig()
    {
        return [
            'x'    => $this->rotateX,
            'y'    => $this->rotateY,
            'z'    => $this->rotateZ,
            'zoom' => $this->zoom
        ];
    }
}
