<?php
/**
 * Date: 09.09.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\render;

use common\models\factories\FileFactory;
use common\models\Model3dPart;
use common\models\Model3dPartProperties;
use common\models\repositories\FileRepository;
use Yii;

/**
 * Class RenderLocalRotate to rotate given model
 *
 * @package lib\render
 */
class RenderLocalRotate extends RenderLocal
{

    /**
     * @param Model3dPart $model3dPart
     * @param RenderConfigRotate $config
     *
     * @return bool
     * @throws \Exception
     */
    public function run(Model3dPart $model3dPart, RenderConfigRotate $config)
    {
        $fileInPath = $model3dPart->file->getLocalTmpFilePath();
        /** @var FileFactory $fileFactory */
        $fileFactory = Yii::createObject(FileFactory::class);
        $fileRepository = Yii::createObject(FileRepository::class);
        $newFile = $fileFactory->createFileFromCopy($model3dPart->file);
        $fileRepository->save($newFile);

        $fileOutPath = $newFile->getLocalTmpFilePath();
        $scaleOutput = $this->rotate($fileInPath, $fileOutPath, $config->rotateX, $config->rotateY, $config->rotateZ);

        $newFile->publishFileToServerAndSave($fileOutPath);

        $props = $this->parseJsonResult($scaleOutput);
        $props = array_map('floatval', $props);
        $model3dProperty = Model3dPartProperties::addRecord(
            [
                'length'     => (float)$props['length'],
                'width'      => (float)$props['width'],
                'height'     => (float)$props['height'],
                'volume'     => (float)$props['volume'],
                'area'       => (float)$props['area'],
                'faces'      => (float)$props['polygons'],
                'vertices'   => 1,
                'created_at' => dbexpr('NOW()'),
            ]
        );
        $model3dPart->setModel3dPartProperties($model3dProperty);
        if (empty($model3dPart->file_src_id)) {
            $model3dPart->file_src_id = $model3dPart->file_id;
        }
        $model3dPart->file = $newFile;
        $model3dPart->safeSave();
        return true;
    }
}