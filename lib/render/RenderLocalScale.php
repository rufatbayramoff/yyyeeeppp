<?php
/**
 * Date: 09.09.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace lib\render;


use common\models\factories\FileFactory;
use common\models\Model3dPart;
use common\models\Model3dPartProperties;
use common\models\repositories\FileRepository;
use Yii;

class RenderLocalScale extends RenderLocal
{


    /**
     * @param Model3dPart $model3dPart
     * @param RenderConfigRotate $config
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \lib\render\RenderException
     * @throws \yii\base\InvalidParamException
     */
    public function run(Model3dPart $model3dPart, RenderConfigRotate $config)
    {
        $fileInPath = $model3dPart->file->getLocalTmpFilePath();
        /** @var FileFactory $fileFactory */
        $fileFactory = Yii::createObject(FileFactory::class);
        $fileRepository = Yii::createObject(FileRepository::class);

        $newFile = $fileFactory->createFileFromCopy($model3dPart->file);
        $fileRepository->save($newFile);

        $fileOutPath = $newFile->getLocalTmpFilePath();

        $scaleOutput = $this->scale($fileInPath, $fileOutPath, $config->zoom);

        $newFile->publishFileToServerAndSave($fileOutPath);

        $props = $this->parseJsonResult($scaleOutput);
        $model3dProperty = Model3dPartProperties::addRecord(
            [
                'length'     => abs((float)$props['length']),
                'width'      => abs((float)$props['width']),
                'height'     => abs((float)$props['height']),
                'volume'     => abs((float)$props['volume']),
                'area'       => abs((float)$props['area']),
                'faces'      => abs((float)$props['polygons']),
                'vertices'   => 1,
                'created_at' => dbexpr('NOW()'),
            ]
        );
        $model3dPart->model3d_part_properties_id = $model3dProperty->id;
        if (empty($model3dPart->file_src_id)) {
            $model3dPart->file_src_id = $model3dPart->file_id;
        }
        $model3dPart->file = $newFile;
        $model3dPart->file_id = $newFile->id;
        \Yii::info(sprintf("old file %d new file %d", $model3dPart->file_src_id, $newFile->id), 'tsdebug');
        $model3dPart->safeSave();
        return true;
    }
}