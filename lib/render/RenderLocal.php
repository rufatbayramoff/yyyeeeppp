<?php
namespace lib\render;

use yii\helpers\Json;

/**
 * Date: 09.09.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 *
 * USAGE 2:
 *  measure sizes: (returns json)
 *      tools/renderer2/dist/Debug/GNU-Linux/renderer2 measure path/to/filename.stl
 *  rotate model: (result file has hidden marks, angles in degrees)
 *      tools/renderer2/dist/Debug/GNU-Linux/renderer2 rotate path/to/input/filename.stl path/to/result/filename.stl anglex angley anglez zoom
 */

class RenderLocal extends \yii\base\BaseObject
{
    /**
     * @var path to renderer shell cmd
     */
    public $renderPath;

    /**
     *
     */
    public function init()
    {
        $this->renderPath = param('local_render_cmd', '/vagrant/repo/tools/renderer2/dist/Debug/GNU-Linux/renderer2');
        parent::init();
    }

    /**
     * rotate given file with x,y,z
     *
     * @param $fileIn
     * @param $fileOut
     * @param $angelX
     * @param $angelY
     * @param $angelZ
     * @return mixed
     * @throws \lib\render\RenderException
     */
    public function rotate($fileIn, $fileOut, $angelX, $angelY, $angelZ)
    {
        $output = $this->runCmd(sprintf('%s rotate %s %s %d %d %d 1', $this->renderPath, $fileIn, $fileOut, $angelX, $angelY, $angelZ));
        return $output;
    }

    /**
     * scale given file with scale factor (from 0 and up), 0.1 - minimize 10 times
     *
     * @param $fileIn
     * @param $fileOut
     * @param $scale
     * @return mixed
     * @throws \lib\render\RenderException
     */
    public function scale($fileIn, $fileOut, $scale)
    {
        $output = $this->runCmd(sprintf('%s rotate %s %s 0 0 0 %s', $this->renderPath, $fileIn, $fileOut, $scale));
        return $output;
    }

    /**
     * @param $fileIn
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \lib\render\RenderException
     */
    public function measure($fileIn)
    {
        $output = $this->runCmd(sprintf('%s measure %s', $this->renderPath, $fileIn));
        return $this->parseJsonResult($output);
    }

    /**
     * json format string
     *
     * @param $output
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws RenderException
     */
    public function parseJsonResult($output)
    {
        $output = end($output); // to remove debug messages
        $props = Json::decode($output);
        if ($props['success'] === false) {
            throw new RenderException($props['errortext']);
        }
        if(isset($props['lenght'])){ // replace typo
            $props['length'] = $props['lenght'];
        }

        return $props;
    }

    /**
     * @param $cmd
     * @return int
     * @throws \lib\render\RenderException
     */
    public function runCmd($cmd)
    {
        $output = $status = 0;
        exec($cmd, $output, $status);
        $status = (int)$status;
        \Yii::info($cmd . ' result: ' . var_export($status, true), 'filejob');
        if ($status > 0) {
            $cmdName = explode(' ', $cmd)[0];
            if ($status === 127) {
                throw new RenderException('Command not found ' . $cmdName);
            }
            if ($status === 126) {
                throw new RenderException('Command invoked cannot execute ' . $cmdName);
            }
            throw new RenderException(sprintf('Command error[%s]', $status));
        }

        return $output;
    }
}