<?php
/**
 * @var \omnilight\scheduling\Schedule $schedule
 */

$blockCronFlagPath = \Yii::getAlias('@console/runtime/blockCron.flag');
if (file_exists($blockCronFlagPath)) {
    $blockCronDate = file_get_contents($blockCronFlagPath);
    if (($blockCronDate < (time() - 60 * 60 * 24 * 2)) || ($blockCronDate > (time() + 60 * 60 * 24 * 2))) {
        // Protect from invalid file
        unlink($blockCronFlagPath);
    }
    return ;
}


$filePath = \Yii::getAlias('@console/runtime/cronoutput.log');
$rateLog = \Yii::getAlias('@console/runtime/openexchangerate.log');
$fileSypexLog = \Yii::getAlias('@console/runtime/cron_sypexlog.log');
$fileOrders = \Yii::getAlias('@console/runtime/logs/orders.log');
$filePath10m = \Yii::getAlias('@console/runtime/cron_10m.log');
$filePath5m = \Yii::getAlias('@console/runtime/cron_5m.log');
$fileDaily = \Yii::getAlias('@console/runtime/cron_daily.log');
//$schedule->command('render/run')->appendOutputTo($fileMinuteLog)->everyMinute();

/**
 * auto-run migration and generate base models
 */
// $schedule->command('migrate --interactive=0')->appendOutputTo($filePath)->everyTenMinutes();
// $schedule->command('base-models/generate')->appendOutputTo($filePath)->everyFiveMinutes();

$schedule->command('taxes/check-tax-form')->appendOutputTo($fileDaily)->daily();

/**
 * run sphinx indexer
 */
$schedule->command('indexer/run')->appendOutputTo($filePath5m)->everyMinute();


/**
 * cancel unpaid orders, orders which not paid after unpaid_order_expire hours (48 hours)
 */
$schedule->command('orders/cancel-unpaid')->appendOutputTo($fileOrders)->hourly();

/**
 * informer remove old notifications
 */
$schedule->command('informer/remove-old-notifications')->appendOutputTo($fileDaily)->daily();

/**
 * update rates
 */
$schedule->command('payment/update-rates')->appendOutputTo($rateLog)->hourly();
$schedule->command('payment/braintree-update')->appendOutputTo($rateLog)->everyMinute();

$schedule->command('seo/autofill')->appendOutputTo($rateLog)->hourly();

// weekly
/**
 * update sypex geo database
 */
//$schedule->command('sypex-geo/update')->appendOutputTo($fileSypexLog)->weeklyOn(5, '11:40');
//$schedule->command('sypex-geo/update-maxmind')->appendOutputTo($fileSypexLog)->weeklyOn(6, '00:00');

/**
 * check for expired orders
 */
$schedule->command('orders/check-expired')->appendOutputTo($fileOrders)->hourly();
$schedule->command('orders/set-as-received-by-timeout')->appendOutputTo($fileOrders)->hourly();
$schedule->command('api-files/check-expired')->appendOutputTo($fileOrders)->hourly();
$schedule->command('api-printable-package/check-expired')->appendOutputTo($fileOrders)->hourly();

$schedule->command('orders/set-change-printer')->appendOutputTo($fileOrders)->hourly();
$schedule->command('orders/send-messages')->appendOutputTo($fileOrders)->everyTenMinutes();
$schedule->command('orders/set-as-delivered')->appendOutputTo($fileOrders)->daily();
$schedule->command('orders/notify-need-settled')->appendOutputTo($fileOrders)->hourly();

$schedule->command('orders/notify-sent-without-tracking')->appendOutputTo($fileOrders)->dailyAt('11:00');
$schedule->command('orders/notify-sent-client-received-order')->appendOutputTo($fileOrders)->everyFiveMinutes();


$schedule->command('messages/send-messages')->appendOutputTo($filePath10m)->everyMinute();
//$schedule->command('orders/cancel-printing-overdue-orders')->appendOutputTo($fileOrders)->hourly();

$schedule->command('messages/check-delivery-log')->appendOutputTo($filePath)->daily();
$schedule->command('messages/check-delivery-imap')->appendOutputTo($filePath)->daily();

/**
 * generating sitemaps
 */
$schedule->command('sitemap/run')->appendOutputTo($filePath)->weeklyOn(3, '01:31');
$schedule->command('sitemap/run')->appendOutputTo($filePath)->weeklyOn(5, '01:31');
$schedule->command('sitemap/ping')->appendOutputTo($filePath)->weeklyOn(4, '01:40');
$schedule->command('sitemap/ping')->appendOutputTo($filePath)->weeklyOn(6, '01:40');


/**
 * update orders count
 */
$schedule->command('ps/calculate-orders-count')->appendOutputTo($filePath)->everyMinute();


/**
 * update orders count
 */
$schedule->command('ps/refresh-rating')->appendOutputTo($filePath)->hourly();

/**
* cert exprired
*/
$schedule->command('ps/expire-certification')->appendOutputTo($filePath)->daily();

/**
 * send email to certify printer
 */
$schedule->command('ps/notify-certify-printer')->appendOutputTo($filePath)->hourly();

/**
 * Auto bun content
 */
$schedule->command('content-blocker/check-new-content')->appendOutputTo($filePath)->everyMinute();

/**
 * Mark failed jobs
 */
$schedule->command('rabbit/mark-failed-jobs')->appendOutputTo($filePath)->everyMinute();

/**
 * send emails about models to publish them.
 */
$schedule->command('email-notify/send-notify')->appendOutputTo($filePath)->hourly();


/**
 * remove old model3d viewed state
 */
$schedule->command('gc/clean-model3d-viewed-state')->appendOutputTo($filePath)->daily();


/**
 * run clean runtime folder
 */
$schedule->command('gc/clean-runtime')->appendOutputTo($filePath10m)->hourly();

/**
 * clean files
 * also delete others models
 */
$schedule->command('gc/clean-files')->appendOutputTo($fileDaily)->twiceDaily();

/**
 *  clean user sessions
 */
$schedule->command('gc/clean-user-session')->appendOutputTo($filePath)->daily();

/**
 *  clean old api files
 */
$schedule->command('file/clean-old-api-files 0 1 100000')->appendOutputTo($filePath)->daily();

/*
 * Clean archived files
 */
$schedule->command('file/clean-archive-unpacked-files')->appendOutputTo($filePath)->hourly();

/**
 * Archive files
 */
$schedule->command('file/archive-files')->appendOutputTo($filePath)->hourly();

/**
 * Clean old request log
 */
$schedule->command('gc/clean-http-request-log')->appendOutputTo($filePath)->daily();

/**
 * Phone validate number
 */
$schedule->command('phone/validate-numbers')->appendOutputTo($filePath)->daily();

/**
 * Close expired quotes
 */
$schedule->command('preorder/close-wait-confirm')->appendOutputTo($filePath)->daily();

/**
 * Instant payment
 */
$schedule->command('instant-payment/auto-settle')->appendOutputTo($filePath)->daily();
$schedule->command('instant-payment/auto-settle')->appendOutputTo($filePath)->daily();



/**
 * Reindex product categories
 */
$schedule->command('product/set-visible-categories')->appendOutputTo($filePath5m)->everyFiveMinutes();
$schedule->command('product/count')->appendOutputTo($filePath)->daily();

$schedule->command('response-time/run')->appendOutputTo($filePath5m)->monthly();
$schedule->command('rating/count')->appendOutputTo($filePath5m)->monthly();
$schedule->command('rating/top')->appendOutputTo($filePath5m)->monthly();

