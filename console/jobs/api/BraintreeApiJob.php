<?php namespace console\jobs\api;

/**
 * BraintreeApiJob
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class BraintreeApiJob extends \console\jobs\ApiQueueJob
{
    protected $code = 'braintree';
    
    public function doJob()
    {
        $apiRequest = $this->args['api'];
        $result = $this->handleApiRequest($apiRequest, $this->args);
        return ['exec_result'=> $result];
    }
    
    public function handleApiRequest($apiRequest, $args)
    {
        
    }
}
