<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.03.18
 * Time: 16:50
 */

namespace console\jobs\model3d;

use common\components\DateHelper;
use common\interfaces\Model3dBasePartInterface;
use common\models\FileJob;
use console\jobs\FileQueueJob;
use console\jobs\RabbitJob;

class ConverterJob extends FileQueueJob implements RabbitJob
{
    protected $code = FileJob::JOB_CONVERTER;

    /**
     * @param Model3dBasePartInterface $model3dPart
     * @return FileQueueJob
     */
    public static function createByPart(Model3dBasePartInterface $model3dPart)
    {
        $fileJob = parent::create($model3dPart->file);
        $fileJob->operation = FileJob::JOB_CONVERTER;
        $fileJob->args = ['model3dPartUid' => $model3dPart->getUid()];
        return $fileJob;
    }

    /**
     * Execute job.
     * Put job code here
     *
     * @return mixed
     */
    public function doJob()
    {
        // TODO: Implement doJob() method.
    }
}