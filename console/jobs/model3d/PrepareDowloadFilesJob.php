<?php
/**
 * Created by mitaichik
 */

namespace console\jobs\model3d;


use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\components\FileDirHelper;
use common\components\FileTypesHelper;
use common\interfaces\Model3dBasePartInterface;
use common\models\File;
use common\models\FileJob;
use common\models\model3d\Model3dWatermarkService;
use common\models\StoreOrderAttemp;
use console\jobs\FileQueueJob;
use console\jobs\RabbitJob;
use lib\render\RenderLocalScale;
use Yii;
use ZipArchive;

/**
 * Class PrepareDowloadFilesJob
 *
 * @package console\jobs\model3d
 */
class PrepareDowloadFilesJob extends FileQueueJob implements RabbitJob
{
    private static $SAVE_ARCHIVE_DIR_ALIAS = '@frontend/runtime/tmpZipUpload';
    private static $SCALE_DIR_ALIAS = '@frontend/runtime/scale';

    /**
     * @var string
     */
    protected $code = FileJob::JOB_PREPARE_DOWNLOAD_FILES;

    /**
     * @var int
     */
    public $attemptId;

    /**
     * Is job only for one file, not for all attemp.
     *
     * @var bool
     */
    public $forFile;

    /**
     * @var RenderLocalScale
     */
    private $localRenderer;

    /**
     * @var Model3dWatermarkService
     */
    private $watermarker;

    /**
     * @param RenderLocalScale $renderLocalScale
     */
    public function injectDependencies(RenderLocalScale $renderLocalScale)
    {
        $this->localRenderer = $renderLocalScale;
        $this->watermarker   = Yii::$app->watermarker;
    }

    /**
     * Execute job.
     * Put job code here
     *
     * @return mixed
     */
    public function doJob(): string
    {
        $attemp = StoreOrderAttemp::tryFind($this->attemptId);

        if ($attemp->order->hasCuttingPack()) {
            $files          = $this->prepareCuttingFiles($attemp);
            $originalFiles  = [];
            $convertedFiles = $this->prepareCuttingConvertedFiles($attemp);
        }
        if ($attemp->order->hasModel()) {
            $parts          = $this->resolveArchivedModelParts($attemp);
            $originalParts  = $this->resolveOriginalParts($parts);
            $files          = $this->prepareModelsFiles($parts, $attemp);
            $originalFiles  = $this->prepareOriginalFiles($originalParts);
            $convertedFiles = [];
        }

        [$zipPath, $name] = $this->resovleArchiveName($attemp, $this->forFile ? $this->getFile()->id : null);
        $this->makeZip($zipPath, $files, $originalFiles, $convertedFiles);
        $this->debugOut('Attempt Id: ' . $this->attemptId);
        return $zipPath;
    }

    /**
     * @param string $zipPath
     * @param string[] $files
     * @param File[] $originalFiles
     */
    private function makeZip(string $zipPath, array $files, array $originalFiles, array $convertedFiles)
    {
        $zip = new ZipArchive();
        $this->debugOut('Zip archive: ' . $zipPath);
        $zip->open($zipPath, ZIPARCHIVE::CREATE);

        foreach ($files as $fileName => $filePath) {
            $this->debugOut('Add: "' . $filePath . '" as "' . $fileName . '"');
            $zip->addFile($filePath, $fileName);
        }

        foreach ($originalFiles as $file) {
            $this->debugOut('Add "' . $file->getLocalTmpFilePath() . '" as "original/' . $file->getFileName() . '"');
            $zip->addFile($file->getLocalTmpFilePath(), 'original/' . $file->getFileName());
        }

        foreach ($convertedFiles as $fileName => $filePath) {
            $this->debugOut('Add "' . $filePath . '" as "selectedByClient/' . $fileName . '"');
            $zip->addFile($filePath, 'selectedByClient/' . $fileName);
        }

        $zip->close();
    }

    protected function prepareCuttingFiles(StoreOrderAttemp $attemp): array
    {
        $files = [];
        foreach ($attemp->order->getCuttingPack()->activeCuttingPackFiles as $cuttingPackFile) {
            $files[$cuttingPackFile->file->getFileName()] = $cuttingPackFile->file->getLocalTmpFilePath();
        }
        return $files;
    }

    protected function prepareCuttingConvertedFiles(StoreOrderAttemp $attemp): array
    {
        $files  = [];
        $tmpDir = \Yii::getAlias('@runtime') . '/cuttingSelections';
        FileDirHelper::createDir($tmpDir);
        foreach ($attemp->order->getCuttingPack()->activeCuttingPackFiles as $cuttingPackFile) {
            foreach ($cuttingPackFile->cuttingPackPages as $packPage) {
                if ($packPage->activeCuttingPackParts && $packPage->selections) {
                    $tmpPath = $tmpDir . '/' . date('Y-m-d_H-i-s') . '_' . mt_rand(0, 9999) . '.svg';
                    file_put_contents($tmpPath, $packPage->selections);
                    $fileName         = str_replace('.', '_', $packPage->getTitle()) . '.svg';
                    $files[$fileName] = $tmpPath;
                }
            }
        }
        return $files;
    }

    /**
     * Return list of zipped filespath
     *
     * @param Model3dBasePartInterface[] $parts
     * @param StoreOrderAttemp $attemp
     * @return string[]
     */
    private function prepareModelsFiles(array $parts, StoreOrderAttemp $attemp): array
    {
        $files = [];

        $replica = $attemp->order->orderItem->model3dReplica;

        $scaledDir = Yii::getAlias(self::$SCALE_DIR_ALIAS);
        FileDirHelper::createDir($scaledDir);

        $texture = null;
        if ($replica->isOneTextureForKit()) {
            $texture = $replica->getKitTexture();
        }
        foreach ($parts as $part) {

            $filePath = $part->file->getConvertedStlPath();

            // scale

            if ($replica->isScaled()) {
                $scaledFilePath = "{$scaledDir}/{$part->file->id}.{$part->file->getFileExtension()}";
                $this->debugOut('Scale "' . $filePath . '"" to "' . $scaledFilePath . '"');
                $this->localRenderer->scale($filePath, $scaledFilePath, $replica->getScale());
                $filePath = $scaledFilePath;
            }

            // sign
            if (FileTypesHelper::isFileInExt($part->file, [Model3dBasePartInterface::STL_FORMAT])) {
                $signedFilePath = null;
                try {
                    $watermarkText  = Yii::$app->watermarker->generateWatermark($attemp->order, $attemp->machine_id);
                    //$signedFilePath = Yii::$app->watermarker->encode($filePath, $watermarkText, true);

                } catch (\Exception $e) {
                    \Yii::error($e, 'watermark');
                    \Yii::error($e);
                    $this->debugOut($e->getMessage());
                }

                if ($signedFilePath && file_exists($signedFilePath)) {
                    $text = sprintf('AttempId: %d, Model3dPartUid: %s - SIGNED Path %s', $attemp->id, $part->getUid(), $signedFilePath);
                    $this->debugOut($text);
                    Yii::info($text, 'watermark');
                    $filePath = $signedFilePath;
                } else {
                    $text = sprintf('AttempId: %d, Model3dPartUid: %s - NOT SIGNED', $attemp->id, $part->getUid());
                    $this->debugOut($text);
                    Yii::info($text, 'watermark');
                }
            }
            if ($texture) {
                $material = $texture->printerMaterial ? $texture->printerMaterial->filament_title : '';
                $color    = $texture->printerColor ? $texture->printerColor->render_color : '';
            } else {
                $material = $part->model3dTexture && $part->model3dTexture->printerMaterial ? $part->model3dTexture->printerMaterial->filament_title : '';
                $color    = $part->model3dTexture && $part->model3dTexture->printerColor ? $part->model3dTexture->printerColor->render_color : '';
            }
            $qty      = $part->qty;
            $fileName = $part->file_id . '_' . ucfirst($part->file->getFileNameWithoutExtension()) . '_' .
                $material . '_' .
                $color . '_qty' .
                $qty . '.stl';

            $files[$fileName] = $filePath;
        }

        return $files;
    }

    /**
     * @param Model3dBasePartInterface[] $model3Parts
     * @return File[]
     */
    protected function prepareOriginalFiles($model3Parts): array
    {
        $files = [];
        foreach ($model3Parts as $model3Part) {
            $files[] = $model3Part->file;
        }
        return $files;
    }

    /**
     * Return list of parts for zip
     *
     * @param StoreOrderAttemp $attemp
     * @return Model3dBasePartInterface[]
     */
    private function resolveArchivedModelParts(StoreOrderAttemp $attemp): array
    {
        $parts = $attemp->order->orderItem->model3dReplica->getActiveModel3dParts();

        return $this->forFile
            ? [
                ArrayHelper::first($parts, function (Model3dBasePartInterface $part) {
                    return $part->file->id == $this->getFile()->id;
                })
            ]
            : $parts;
    }

    /**
     * Return model3d converted from parts
     *
     * @param $model3dParts Model3dBasePartInterface[]
     * @return Model3dBasePartInterface[]
     */
    protected function resolveOriginalParts($model3dParts): array
    {
        $sourceParts = [];
        foreach ($model3dParts as $model3dPart) {
            if ($model3dPart->convertedFromPart) {
                $sourceParts[$model3dPart->convertedFromPart->file->id] = $model3dPart->convertedFromPart;
            } elseif (FileTypesHelper::isFileInExt($model3dPart->file, [Model3dBasePartInterface::PLY_FORMAT, Model3dBasePartInterface::OBJ_FORMAT])) {
                $sourceParts[$model3dPart->file->id] = $model3dPart;
            }
        }
        return $sourceParts;
    }

    /**
     * Return filepath of archive
     *
     * @param StoreOrderAttemp $attemp
     * @param null $fileId
     * @return string
     */
    public static function resovleArchiveName(StoreOrderAttemp $attemp, $fileId = null): array
    {
        $orderItem = $attemp->order->orderItem;
        if ($orderItem->model3dReplica) {
            $archiveFileName = $orderItem->model3dReplica->title;
        }
        if ($orderItem->cuttingPack) {
            $archiveFileName = $orderItem->cuttingPack->getTitle();
        }

        $archiveFileName = str_replace([PHP_EOL, "\n", "\r", "."], ' ', $archiveFileName);
        $archiveFileName = ucfirst(trim($archiveFileName));

        $saveDir = Yii::getAlias(self::$SAVE_ARCHIVE_DIR_ALIAS);
        FileDirHelper::createDir($saveDir);

        $filename = $fileId
            ? "{$attemp->id}_{$fileId}.zip"
            : "{$attemp->id}.zip";

        return ["{$saveDir}/{$filename}", $archiveFileName];
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param null $fileId
     * @return static
     */
    public static function createFromAttmp(StoreOrderAttemp $attemp, $fileId = null): PrepareDowloadFilesJob
    {
        if ($attemp->order->hasCuttingPack()) {
            $file = $fileId
                ? File::tryFind($fileId)
                : ArrayHelper::first($attemp->order->orderItem->cuttingPack->activeCuttingPackFiles)->file;
        }
        if ($attemp->order->hasModel()) {
            $file = $fileId
                ? File::tryFind($fileId)
                : ArrayHelper::first($attemp->order->orderItem->model3dReplica->getActiveModel3dParts())->file;
        }

        $task            = PrepareDowloadFilesJob::create($file);
        $task->attemptId = $attemp->id;
        $task->forFile   = (bool)$fileId;
        return $task;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param $fileId
     * @return FileJob|null
     */
    public static function find(StoreOrderAttemp $attemp, $fileId = null): ?FileJob
    {
        if ($attemp->order->hasModel()) {
            $attempFilesIds = ArrayHelper::getColumn($attemp->order->orderItem->model3dReplica->getActiveModel3dParts(), 'file_id');
        }
        if ($attemp->order->hasCuttingPack()) {
            $attempFilesIds = [];
            foreach ($attemp->order->orderItem->cuttingPack->activeCuttingPackFiles as $cuttingPackFile) {
                $attempFilesIds[] = $cuttingPackFile->file->id;
            }
        }

        if ($fileId) {
            AssertHelper::assert(in_array($fileId, $attempFilesIds), "File {$fileId} not in active parts");
        }

        /** @var FileJob[] $fileJobs */
        $fileJobs = FileJob::find()
            ->where([
                FileJob::column('operation') => FileJob::JOB_PREPARE_DOWNLOAD_FILES,
                FileJob::column('file_id')   => $attempFilesIds,
            ])
            ->orderBy('id DESC')
            ->all();


        $fileJobs = array_filter($fileJobs, function (FileJob $job) use ($attemp) {
            return $job->getArgsData()['attemptId'] == $attemp->id;
        });


        foreach ($fileJobs as $fileJob) {

            $args = $fileJob->getArgsData();

            if (!$fileId && $args['forFile'] == false) {
                return $fileJob;
            }

            if ($fileId && $args['forFile'] == true && $fileJob->file_id == $fileId) {
                return $fileJob;
            }
        }

        return null;
    }

    public function debugOut($string)
    {
        echo date('Y-m-d H:i:s') . ' - ' . $string . "\n";
    }
}