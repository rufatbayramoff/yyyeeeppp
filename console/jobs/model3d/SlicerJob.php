<?php
namespace console\jobs\model3d;

use Yii;

/**
 * Slicer job using slic3r
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class SlicerJob extends \console\jobs\FileQueueJob
{ 
    
    protected $code = \common\models\FileJob::JOB_SLICER;
    
    /**
     * worker runs this method
     */
    public function doJob()
    {
        return $this->startSlicer();
    }

    /**
     * start slicer with specified params
     *
     * @return array|bool
     */
    private function startSlicer()
    {
        /** @var \common\models\File $file */
        $file = $this->getFile();
        $nozzleDiameter = param('gcode_nozzle_diameter');
        $filamentDiameter = param('gcode_filament_diameter'); // mm
        $fillDensity = param('gcode_fill_density'); //0.5; // 50% 
        $solidInfillSpeed = param('gcode_solid_infill_speed'); 
        $layerHeight = param('gcode_layer_height'); 
        $perimeters = param('gcode_perimeters'); 
        $filePath = $file->getLocalTmpFilePath();
        // g code already exists
        $gFile = str_replace(pathinfo($filePath, PATHINFO_EXTENSION), "gcode", $filePath);
        if(file_exists($gFile)){
            \Yii::info('Gcode found, but slicer try to start again: ' . $gFile, 'slicer3d');
            return false;
        }
        // first get info about file, and chech max build volume      
        $sizes = $this->parseSizes($filePath);
        if(empty($sizes) || !isset($sizes['volume'])){
            \Yii::info("file: $filePath" . var_export($sizes, true), 'slicer3d');
            return false;
        }
        $vol = intval($sizes['volume']);
        $maxVol = intval(param('gcode_max_volume'));
        if($vol > $maxVol){
            $result = [
                'result' => true,                
                'error' => sprintf('Volume [%d] over gcode_max_volume[%d]', $vol, $maxVol),
                'filamentRequired' => 0,
                'filamentVolume' => 0,
                'done_time' => 0,
            ];
            
            \Yii::info("file: $filePath" . var_export($result, true), 'slicer3d');
            return $result;
        }
        $result = \lib\d3\SlicerWrapper::init(true)
            ->nozzleDiameter($nozzleDiameter)
            ->filamentDiameter($filamentDiameter)
            ->fillDensity($fillDensity) 
            ->solidInfillSpeed($solidInfillSpeed)
            ->layerHeight($layerHeight)
            ->perimeters($perimeters)
            ->file($filePath)->run();
        
        $parsedResult = $this->parseResult($result);
        $parsedResult['result'] = $result;
        return $parsedResult;
    }

    /**
     * get sizes using slic3r
     *
     * @param $filePath
     * @return array
     * @internal param type $result
     */
    private function parseSizes($filePath)
    {
        $sizes = \lib\d3\SlicerWrapper::init(true)->info($filePath);
        $matcher = [
            'volume' => 'volume:            (\d+.\d+)'
        ];
        return $this->parseLines($sizes, $matcher);
    }
    /**
     * parse result. returns array with ['scanned'=>, 'infected'=>] data
     * 
     * @param array $result
     * @return array
     */
    private function parseResult($result)
    {
        $matcher = [ 
            'done_minute' => 'Done. Process took (\d+) minutes and \d+.\d+ seconds',
            'done_time' => 'Done. Process took \d+ minutes and (\d+.\d+) seconds',
            'filamentRequired' => 'Filament required: (\d+.\d+)mm \(\d+.\d+cm3\)',
            'filamentVolume' => 'Filament required: \d+.\d+mm \((\d+.\d+)cm3\)'
        ];
        return $this->parseLines($result, $matcher);
    }

    /**
     * parse each line with specified array of matchers
     *
     * @param array|\lib\d3\SlicerWrapper $result - array of lines with text
     * @param array $matcher - array of matcher and regexp
     * @return array
     */
    private function parseLines($result, $matcher)
    {
        $info = [];
        foreach($result as $line){
            $line = trim($line);
            foreach($matcher as $key=>$pattern){
                $patternNeedle = explode(" ", $pattern)[0];
                if(strpos($line, $patternNeedle)===false){
                    continue;
                }
                preg_match('#'.$pattern.'#', $line, $matches);
                if(isset($matches[1])){
                    $info[$key] = $matches[1];
                }
            }
        }
        return $info;
    }
}