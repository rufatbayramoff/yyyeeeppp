<?php
namespace console\jobs\model3d;

use common\components\exceptions\AssertHelper;
use common\models\FileJob;
use console\jobs\FileRuntimeDirectoryTrait;
use Yii;

/**
 * 
 * Antivirus using clamscan - http://www.clamav.net/index.html
 * 
 * to install : sudo apt-get install clamav
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class AntivirusJob extends \console\jobs\FileQueueJob
{
    // use FileRuntimeDirectoryTrait;
    
    protected $code = \common\models\FileJob::JOB_ANTIVIRUS;

    /**
     * worker runs this method
     */
    public function doJob()
    {
        return $this->scanFile();
    }
    
    /**
     * scan specified file by file_id in file db table
     *
     * @return array
     */
    private function scanFile()
    {
        //$this->createRuntimeDirectory();
        //$cmd = "clamscan -r -i ".$this->getRuntimeFile();
        //$result = $status = '';
        //exec($cmd, $result, $status);
        $result = <<<STR

----------- SCAN SUMMARY -----------
Known viruses: 6302989
Engine version: 0.99.2
Scanned directories: 0
Scanned files: 1
Infected files: 0
Data scanned: 3.53 MB
Data read: 1.89 MB (ratio 1.87:1)
Time: 10.742 sec (0 m 10 s)
THIS IS STUB

STR;
        $result = explode("\n", $result);
        //\Yii::info($cmd . ' result: ' . var_export($result, true), 'filejob');
        //$this->removeRuntimeDirectory();
        // parse answer
        $info = $this->parseResult($result);
        $info['file'] = '/STUB_CHECKING.STL';//$this->getRuntimeFile();
        $info['exec_result'] = $result;
        return $info;
    }
    
    /**
     * parse result. returns array with ['scanned'=>, 'infected'=>] data
     * 
     * @param array $result
     * @return array
     */
    private function parseResult($result)
    {
        $matcher = [
            'scanned' => 'Scanned files: (\d+)',
            'infected' => 'Infected files: (\d+)',
            'time' => 'Time: (\d+.\d+) sec \(\d+ m \d+ s\)'
        ];
        $info = [];
        foreach($result as $line){
            $line = trim($line);         
            foreach($matcher as $key=>$pattern){
                $patternNeedle = explode(" ", $pattern)[0];
                if(strpos($line, $patternNeedle)===false){
                    continue;
                }
                preg_match('#'.$pattern.'#', $line, $matches);
                if(isset($matches[1])){ 
                    $info[$key] = $matches[1];
                }
            }
        }
        return $info;
    }

    /**
     * @param FileJob $fileJob
     * @return array|bool
     */
    public static function getInfo(FileJob $fileJob)
    {
        AssertHelper::assert($fileJob->operation == FileJob::JOB_ANTIVIRUS, "FileJob {$fileJob->id} is not antivirus job");

        $resultData = $fileJob->getResultData();
        if(empty($resultData)){
            return false;
        }

        return [
            'status' => $fileJob->status,
            'scanned' => isset($resultData['scanned']) ? $resultData['scanned'] : 0,
            'infected' => isset($resultData['infected']) ? $resultData['infected'] : 0,
        ];
    }
}