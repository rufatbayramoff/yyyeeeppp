<?php
namespace console\jobs\model3d;

use common\interfaces\FileBaseInterface;
use common\models\File;
use common\models\FileJob;
use console\jobs\FileQueueJob;
use console\jobs\RabbitJob;

/**
 * Render job for queue. Render png file from 3d model
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class RenderJob extends FileQueueJob implements RabbitJob
{
    protected $code = FileJob::JOB_RENDER;

    /**
     * Source file id
     * @var int
     */
    public $file_src_id;


    public function doJob()
    {

    }

    /**
     * @param FileBaseInterface $file
     * @param string $sessionId
     * @param int $fileSrcId
     * @return static
     */
    public static function create(FileBaseInterface $file, $sessionId = null, $fileSrcId = null)
    {
        /** @var static $job */
        $job = parent::create($file);
        $job->file_src_id = $fileSrcId;
        $job->session_id = $sessionId;
        return $job;
    }

    /**
     * @param \common\models\FileJob $job
     * @return bool
     */
    public function isSameJob(\common\models\FileJob $job)
    {
        $args = $job->getArgsData();
        return $args['file_id']===$this->args['file_id'];
    }
}