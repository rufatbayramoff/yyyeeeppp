<?php

namespace console\jobs\model3d;

use common\interfaces\Model3dBasePartInterface;
use common\models\factories\Model3dPartPropertiesFactory;
use common\models\File;
use common\models\FileJob;
use common\models\Model3dPart;
use console\jobs\FileRuntimeDirectoryTrait;
use console\jobs\QueueGateway;
use console\jobs\RabbitJob;
use frontend\models\model3d\Model3dFacade;
use lib\d3\NodeJsApi;
use lib\render\RenderLocal;
use Psr\Log\InvalidArgumentException;
use Yii;

/**
 * parser job for queue. get information about 3d model
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class ParserJob extends \console\jobs\FileQueueJob implements RabbitJob
{
    use FileRuntimeDirectoryTrait;

    protected $code = FileJob::JOB_PARSER;

    /**
     * parsed 3d file and gets general information about file
     *
     * @throws \Exception
     */
    public function doJob()
    {
        $file = $this->getFile();
        if (strtolower($file->extension) === 'stl' || Model3dFacade::isIgesOrStep($file->extension)) {
            $result = $this->parseWithNodeJs();
        } else {
            $result = $this->parseWithLocalRender();
        }
        return $result;
    }


    protected function writeInfoIntoModel3dBasePart(Model3dBasePartInterface $model3dBasePart, $info)
    {
        if (isset($info['volume'])) {
            $info['volume'] = abs($info['volume']);
        }
        if (!$info) {
            throw new InvalidArgumentException('Can`t write result to model3dPartProperties: ' . json_encode($info));
        }
        if ($model3dBasePart->model3dPartProperties) {
            $model3dBasePart->model3dPartProperties->setData($info);
            $model3dBasePart->model3dPartProperties->safeSave();
        } else {
            $model3dPartProp = Model3dPartPropertiesFactory::createModel3dPartProperties($info);
            $model3dPartProp->safeSave();
            $model3dBasePart->setModel3dPartProperties($model3dPartProp);
            if ($model3dBasePart->hasAttribute('model3d_part_properties_original_id')) {
                $model3dBasePart->model3d_part_properties_original_id = $model3dPartProp->id;
            }
            $model3dBasePart->safeSave();
        }
    }

    protected function writeInfoIntoModel3dPart($info)
    {
        $model3dPart = Model3dPart::tryFind(['file_id' => $this->args['file_id']]);
        $this->writeInfoIntoModel3dBasePart($model3dPart, $info);

        foreach ($model3dPart->model3dReplicaParts as $model3dReplicaPart) {
            if (!$model3dReplicaPart->isCaclulatedProperties()) {
                $this->writeInfoIntoModel3dBasePart($model3dReplicaPart, $info);
            }
        }
        foreach ($model3dPart->copiedModel3dParts as $model3dPart) {
            if (!$model3dPart->isCaclulatedProperties()) {
                $this->writeInfoIntoModel3dBasePart($model3dPart, $info);
            }
        }
    }

    private function parseWithNodeJs()
    {
        $nodeJsApi = new NodeJsApi(param('watermarkServer', 'http://192.168.66.10:5858'));
        $this->createRuntimeDirectory();
        try {
            $infoMeasure = $nodeJsApi->measure($this->getFile()->getConvertedStlPath());
            if (!empty($infoMeasure['supportsVolume'])) {
                $infoMeasure['supports_volume'] = $infoMeasure['supportsVolume'];
                unset($infoMeasure['supportsVolume']);
            }
        } catch (\Exception $e) {
            Yii::error($e);
            throw $e;
        }
        $this->removeRuntimeDirectory();
        $this->writeInfoIntoModel3dPart($infoMeasure);
        return $infoMeasure;

    }

    /**
     * @return array|mixed
     * @throws \Exception
     */
    private function parseWithLocalRender()
    {
        $this->createRuntimeDirectory();
        try {
            $renderLocal = new RenderLocal();
            $info = $renderLocal->measure($this->getRuntimeFile());
            $info['vertices'] = 1;
            $info['faces'] = $info['polygons'];
            $info['volume'] = abs((float)$info['volume']);
            $info['area'] = abs((float)$info['area']);
        } catch (\Exception $e) {
            Yii::error($e);
            throw $e;
        }
        $this->removeRuntimeDirectory();
        $this->writeInfoIntoModel3dPart($info);
        return $info;
    }

    public static function isFailed(File $file): bool
    {
        /** @var FileJob $lastJob */
        $lastJob = FileJob::find()
            ->where(['file_id' => $file->id, 'operation' => FileJob::JOB_PARSER])
            ->orderBy('created_at DESC, id DESC')
            ->one();
        if ($lastJob && ($lastJob->status === QueueGateway::JOB_STATUS_FAILED)) {
            return true;
        }
        return false;
    }

    /**
     * Is can auto add new parser job
     *
     * @param File $file
     * @return bool
     */
    public static function isCanRun(File $file): bool
    {
        /** @var FileJob $lastJob */
        $lastJob = FileJob::find()
            ->where(['file_id' => $file->id, 'operation' => FileJob::JOB_PARSER])
            ->orderBy('created_at DESC, id DESC')
            ->one();
        if ($lastJob) {
            return false;
        };
        return true;
    }
}