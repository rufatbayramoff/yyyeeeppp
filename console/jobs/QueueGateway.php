<?php namespace console\jobs;

use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\models\FileJob;
use console\jobs\model3d\AntivirusJob;
use console\jobs\model3d\ConverterJob;
use console\jobs\model3d\ParserJob;
use console\jobs\model3d\PrepareDowloadFilesJob;
use console\jobs\model3d\RenderJob;
use DateInterval;
use DateTime;
use ReflectionClass;
use Yii;
use yii\base\Exception;
use yii\base\InvalidCallException;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Gateway to add jobs to queue, get job status and etc.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class QueueGateway
{

    /**
     * After that time job will be marked as failed
     */
    const JOB_RUNNING_TIMEOUT = 60 * 10 * 2;

    const MAX_FAILD_ATTEMPTS_COUNT = 3;

    /**
     * If exist same job and it not failed - job not create
     */
    const ADD_STRATEGY_UNIQUE = 'unique';

    /**
     * Job will added anywat
     */
    const ADD_STRATEGY_ANYWAY = 'anyway';


    const QUEUE_HIGHT          = 'hight';
    const QUEUE_MIDDLE         = 'middle';
    const QUEUE_LOW            = 'low';
    const JOB_STATUS_WAITING   = 'waiting';
    const JOB_STATUS_RUNNING   = 'running';
    const JOB_STATUS_FAILED    = 'failed';
    const JOB_STATUS_COMPLETED = 'complete';

    public static $queueForJob = [
        \console\jobs\model3d\AntivirusJob::class => self::QUEUE_HIGHT,
        \console\jobs\model3d\ParserJob::class    => self::QUEUE_MIDDLE,
        \console\jobs\model3d\RenderJob::class    => self::QUEUE_MIDDLE,
        \console\jobs\model3d\SlicerJob::class    => self::QUEUE_MIDDLE,
        \console\jobs\api\BraintreeApiJob::class  => self::QUEUE_MIDDLE,
        ConverterJob::class                       => self::QUEUE_MIDDLE
    ];

    public static $classToOperiation = [
        \console\jobs\model3d\PrepareDowloadFilesJob::class => \common\models\FileJob::JOB_PREPARE_DOWNLOAD_FILES,
        \console\jobs\model3d\AntivirusJob::class           => \common\models\FileJob::JOB_ANTIVIRUS,
        \console\jobs\model3d\ParserJob::class              => \common\models\FileJob::JOB_PARSER,
        \console\jobs\model3d\RenderJob::class              => \common\models\FileJob::JOB_RENDER,
        \console\jobs\model3d\SlicerJob::class              => \common\models\FileJob::JOB_SLICER,
        ConverterJob::class                                 => FileJob::JOB_CONVERTER,
        \console\jobs\api\BraintreeApiJob::class            => 'braintree',
    ];

    /**
     * add file job to queue and to database, return token
     *
     * @param \console\jobs\FileQueueJob $jobObj
     * @param string $addStrategy
     * @return FileJob
     * @throws Exception
     */
    public static function addFileJob(FileQueueJob $jobObj, $addStrategy = self::ADD_STRATEGY_UNIQUE)
    {
        $fileId    = $jobObj->getFile()->id;
        $operation = $jobObj->getCode();

        if (($jobObj instanceof ParserJob) || ($jobObj instanceof RenderJob)) {
            if (Yii::$app->getModule('convert')->converter->isConvertableFile($jobObj->getFile())) {
                return;
            }
        }

        if (app('db')->getTransaction() && app('db')->getTransaction()->getIsActive()) {
            throw new InvalidCallException('Can`t add new background job in transaction!');
        }

        AssertHelper::assert($operation, 'Operation cant be empty');

        switch ($addStrategy) {
            case self::ADD_STRATEGY_UNIQUE:
            {
                /** @var FileJob[] $existedJobs */
                $existedJobs = FileJob::find()->where(['file_id' => $fileId, 'operation' => $operation])->orderBy('created_at desc')->all();

                $failedCount = 0;
                $existedJob  = null;
                foreach ($existedJobs as $existedJob) {
                    if ($existedJob->status == self::JOB_STATUS_FAILED) {
                        $failedCount++;
                    }
                    if ($existedJob->status !== self::JOB_STATUS_FAILED && $jobObj->isSameJob($existedJob)) {
                        return $existedJob;
                    }
                }
                if ($failedCount > self::MAX_FAILD_ATTEMPTS_COUNT && $existedJob && ($existedJob->created_at > DateHelper::subNowSec(60 * 60 * 24))) {
                    // Max 3 attempt in a day
                    return $existedJob;
                }
                break;
            }

            case self::ADD_STRATEGY_ANYWAY:
            {
                break;
            }

            default:
            {
                throw new Exception("Undefined add job strategy {$addStrategy}");
            }
        }


        $args    = ArrayHelper::merge($jobObj->args, self::getConfigurableAttributes($jobObj));
        $fileJob = new \common\models\FileJob();
        $fileJob->setAttributes(
            [
                'file_id'    => $fileId,
                'created_at' => new Expression('NOW()'),
                'operation'  => $operation,
                'status'     => self::JOB_STATUS_WAITING,
                'token'      => '',
                'args'       => \yii\helpers\Json::encode($args)
            ]
        );
        AssertHelper::assertSave($fileJob);
        $fileJob->refresh();

        if ($jobObj instanceof RabbitJob) {
            Yii::$app->rabbitMq->sendMessage('treatstock.' . $jobObj->getCode(), [$jobObj::className(), $fileJob->id]);
            $token = null;
        } else {
            $args['file_job_id'] = $fileJob->id;
            $token               = '';
            $fileJob->token      = $token;

            if ($jobObj instanceof AntivirusJob) {
                $fileJob->result      = json_encode($jobObj->doJob());
                $fileJob->status      = self::JOB_STATUS_COMPLETED;
                $fileJob->finished_at = DateHelper::now();
                $fileJob->updated_at  = DateHelper::now();
            }
            AssertHelper::assertSave($fileJob);
        }
        return $fileJob;
    }

    /**
     * get file job by token
     *
     * @param string $token
     * @return \common\models\FileJob
     */
    public static function getFileJob($token)
    {
        /** @var \common\models\FileJob $fileJob */
        $fileJob = \common\models\FileJob::tryFind(['token' => $token]);
        return $fileJob;
    }

    /**
     *
     * @param string $token
     * @return null|\common\models\Job
     */
    public static function getDbJob($token)
    {
        $job = \common\models\Job::tryFind(['token' => $token]);
        return $job;
    }

    /**
     * Return priority for job
     *
     * @param QueueJob $job
     * @return int
     * @throws JobException
     */
    private static function resolveQueueId(QueueJob $job)
    {
        $jobClass = $job::className();
        if (!isset(self::$queueForJob[$jobClass])) {
            throw new JobException("Cant resolve queue for {$jobClass}");
        }
        return self::$queueForJob[$jobClass];
    }

    /**
     * @param QueueJob $job
     * @return array
     */
    public static function getConfigurableAttributesNames(QueueJob $job)
    {
        $class = new ReflectionClass($job);
        $names = [];
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic() && $property->class == get_class($job)) {
                $names[] = $property->getName();
            }
        }
        return $names;
    }

    /**
     * @param QueueJob $job
     * @param array $arguments
     */
    public static function configureJob(QueueJob $job, array $arguments)
    {
        $attributes = self::getConfigurableAttributesNames($job);

        foreach ($attributes as $attribute) {
            if (array_key_exists($attribute, $arguments)) {
                $job->$attribute = $arguments[$attribute];
            }
        }
    }

    /**
     * @param QueueJob $job
     * @return array
     */
    public static function getConfigurableAttributes(QueueJob $job)
    {
        $attributes = self::getConfigurableAttributesNames($job);
        $result     = [];
        foreach ($attributes as $attribute) {
            $result[$attribute] = $job->$attribute;
        }
        return $result;
    }

    /**
     * Repeat job
     *
     * @param FileJob $job
     */
    public static function repeat(FileJob $job)
    {
        $jobClass = array_search($job->operation, self::$classToOperiation);
        AssertHelper::assert($job);

        /** @var FileQueueJob $newJob */
        $newJob = new $jobClass;

        // make job configure
        $argsData = $job->getArgsData();
        self::configureJob($newJob, $argsData);

        // calculate job args
        $args                = [];
        $configureAttributes = self::getConfigurableAttributesNames($newJob);
        foreach ($argsData as $argName => $argValue) {
            if (!in_array($argName, $configureAttributes)) {
                $args[$argName] = $argValue;
            }
        }
        $args['file_id'] = $job->file_id;
        $newJob->args    = $args;

        self::addFileJob($newJob, QueueGateway::ADD_STRATEGY_ANYWAY);
    }

    public static function markFailedJobs()
    {
        $endDate = (new DateTime('now', new \DateTimeZone('UTC')))->sub(new DateInterval('PT' . QueueGateway::JOB_RUNNING_TIMEOUT . 'S'))->format('Y-m-d H:i:s');
        /** @var FileJob[] $failJobs */
        $failJobs = FileJob::find()->where(['and', ['status' => [QueueGateway::JOB_STATUS_WAITING, QueueGateway::JOB_STATUS_RUNNING]], ['<', 'created_at', $endDate]])->all();
        foreach ($failJobs as $failJob) {
            echo "\nId: " . $failJob->id . ' Date: ' . $failJob->created_at;
            $failJob->output = 'Faild by timeout. Original status: ' . $failJob->status;
            $failJob->status = QueueGateway::JOB_STATUS_FAILED;
            $failJob->safeSave();
        }
        echo "\n";
    }
}
