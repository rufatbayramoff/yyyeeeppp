<?php 
namespace console\jobs;

use Yii;
/**
 * class to be extended to create queue jobs
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
abstract class QueueJob
{
    /**
     * arguments for job
     * 
     * @var array
     */
    public $args = [];

    /**
     * job code name
     * 
     * @var string 
     */
    protected $code;
    
    /**
     * log jobs
     * 
     * @var boolean - to debug job added with args, no need in prod.
     */
    protected $log = false;
    
    /**
     * @var mixed
     */
    public $job;
    
    /**
     * @var string
     */
    public $queue;

    /**
     * before perform
     * @param array $data
     */
    public function logRun($data = [])
    {
        if($this->log){
            $args  = \yii\helpers\Json::encode($this->args);
            $data = !empty($data) ? serialize($data) : '';            
            $date = date("m_d");
            $path = \Yii::getAlias('@console/runtime/logs/job_'.$this->code.'_'.$date. '.log');
            $args = "\n" . $args . "\n";
            file_put_contents($path, $args . ' - ' . $data, FILE_APPEND);
        }
    }
    
    /**
     * 
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * method to implement
     */
    public abstract function perform();

    /**
     * get job class name
     * 
     * @return string
     */
    public static function className()
    {
        return get_called_class();
    }
    
    /**
     * after perform
     */
    public function tearDown()
    {
        
    }
}
