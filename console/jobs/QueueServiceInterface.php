<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace console\jobs;


interface QueueServiceInterface
{

    public function getStatusByToken($token);

    public function getStatusByJob($job);

    public function addJob($job);

}