<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace console\jobs;
use common\components\exceptions\AssertHelper;
use common\components\FileDirHelper;
use common\models\File;
use common\models\FileJob;
use yii\helpers\FileHelper;

/**
 * Trait provide functions for create runtime direcory for FileJob tasks and copy source file
 * to this direcory, and delete this directory
 *
 * It need becouse file can moved to another directory
 *
 * @package console\jobs
 */
trait FileRuntimeDirectoryTrait
{
    /**
     * @var string
     */
    private $runtimeDirectory;

    /**
     * @var string
     */
    private $runtimeFile;


    /**
     * Create runtime directory and copy file
     */
    protected function createRuntimeDirectory()
    {
        $file = $this->getFile();
        $filePath = $file->getLocalTmpFilePath();
        $filePathInfo = pathinfo($filePath);

        $this->runtimeDirectory = \Yii::getAlias("@frontend/runtime/jobs/".$this->getFileJob()->id);
        FileDirHelper::createDir($this->runtimeDirectory);
        $this->runtimeFile = $this->runtimeDirectory.'/'.$filePathInfo['basename'];
        copy($filePath, $this->runtimeFile);
    }

    /**
     * Return filename of copy file
     */
    protected function getRuntimeFile()
    {
        AssertHelper::assert($this->runtimeFile, 'First, you must create runtime directory');
        return $this->runtimeFile;

    }

    /**
     * Return runtime directory
     */
    protected function getRuntimeDirectory()
    {
        AssertHelper::assert($this->runtimeDirectory, 'First, you must create runtime directory');
        return $this->runtimeDirectory;
    }

    /**
     * Remove runtime directory
     */
    protected function removeRuntimeDirectory()
    {
        AssertHelper::assert($this->runtimeDirectory, 'First, you must create runtime directory');
        FileHelper::removeDirectory($this->runtimeDirectory);
    }

    /**
     * Must return file
     * @return File
     */
    abstract public function getFile();

    /**
     * Must return file
     * @return FileJob
     */
    abstract public function getFileJob();
}