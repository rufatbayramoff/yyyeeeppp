<?php

namespace console\jobs;

use common\interfaces\FileBaseInterface;
use common\models\File;
use common\models\FileJob;

/**
 * File jobs for queue.
 * For renderers, converters, parsers, antivirus checks and etc.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
abstract class FileQueueJob extends \console\jobs\QueueJob
{
    /**
     * File cache
     *
     * @var \common\models\File
     */
    private $file;

    /**
     * FileJob cache
     *
     * @var \common\models\FileJob
     */
    private $fileJob;

    /**
     * @param FileBaseInterface $file
     * @param null $sessionId
     * @param null $fileSrcId
     * @return static
     */
    public static function create(FileBaseInterface $file, $sessionId = null, $fileSrcId = null)
    {
        $job = new static;
        $job->file = $file;
        $job->args['file_id'] = $file->id;
        return $job;
    }

    /**
     * @return File
     * @throws \yii\web\NotFoundHttpException
     */
    public function getFile()
    {
        if (!$this->file) {
            $this->file = File::tryFindByPk($this->args['file_id'], 'File not found');
        }
        return $this->file;
    }

    /**
     * @return FileJob
     * @throws \yii\web\NotFoundHttpException
     */
    public function getFileJob()
    {
        if (!$this->fileJob) {
            $this->fileJob = FileJob::tryFindByPk($this->args['file_job_id'], 'FileJob not found');
        }
        return $this->fileJob;
    }

    public function setFileJob(FileJob $fileJob)
    {
        $this->fileJob = $fileJob;
    }

    /**
     * Add job doubles in order to prevent multiple jobs with the same args
     * for the same file.
     *
     * @param FileJob $job
     * @return bool
     */
    public function isSameJob(FileJob $job)
    {
        return true;
    }

    /**
     * Perform job.
     * It only wrapper for method self::doTask
     */
    final public function perform()
    {
        $result = $this->doJob();

        if ($result !== null) {
            $this->getFileJob()->setResult($result);
        }
    }

    /**
     * Execute job.
     * Put job code here
     *
     * @return mixed
     */
    abstract public function doJob();
}