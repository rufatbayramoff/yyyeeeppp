<?php namespace console\jobs;

/**
 *  ApiQueueJob
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
abstract class ApiQueueJob extends QueueJob
{
    /**
     *
     * @var \common\models\Job 
     */
    public $dbjob;
    public function __construct($args = [])
    {
        $this->args = $args;
    }
    
    /**
     * 
     * @return \common\models\Job
     */
    public function getDbJob()
    {
        if(!$this->dbjob){
            $this->dbjob = \common\models\Job::tryFindByPk($this->args['job_id'], 'Job not found');
        }
        return $this->dbjob;
    }
    
    /**
     * 
     */
    final public function perform()
    {
        try{
            $result = $this->doJob();
            if($result !== null){
                $this->getDbJob()->setResult($result);
            }
        }catch(\Exception $e){
            \Yii::warning(var_export($this->args), "job");
            logException($e, "job");
        }
    }    
    
    /**
     * Execute job.
     * Put job code here
     * @return mixed
     */
    abstract public function doJob();
}
