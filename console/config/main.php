<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$rules  = require(__DIR__ . '/../../frontend/config/url-rules.php');
return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => [
        'log',
        'sentry'
    ],
    'controllerMap'       => [
        'fixture' => [
            'class' => 'yii\faker\FixtureController'
        ],
    ],
    'controllerNamespace' => 'console\controllers',
    'components'          => [
        'log'        => [
            'targets' => [
                [
                    'class'  => common\components\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'baseUrl'         => 'https://www.yep3d.com/',
            'rules'           => $rules
        ],
        'sentry'     => [
            'class'     => '\lib\sentry\Sentry',
            'enabled'   => true,
            'phpLogger' => [
                'dsn'       => 'https://ed7d6b5df3ed4f63a2034be01b9200d4@o4504125362798592.ingest.sentry.io/4504210450939904',
                'dsnTrash' => 'https://ed7d6b5df3ed4f63a2034be01b9200d4@o4504125362798592.ingest.sentry.io/4504210450939904',
                'logger'    => 'console-php',
                'levels'    => ['error'],
            ],
        ],
        'schedule'   => 'omnilight\scheduling\Schedule',
    ],
    'modules'             => [
        'gii' => [
            'class'      => 'yii\gii\Module',
            'generators' => [
                'model-base' => [
                    'class'          => '\console\gii\generators\model\base\Generator',
                    'ns'             => '\common\models\base',
                    'modelsNs'       => '\common\models',
                    'baseClass'      => 'common\components\BaseAR',
                    'enableI18N'     => true,
                    'queryBaseClass' => '\common\components\BaseActiveQuery',
                    'queryesNs'      => '\common\models\query',
                ],
                'model-work' => [
                    'class'        => '\console\gii\generators\model\work\Generator',
                    'ns'           => 'common\models',
                    'baseModelsNs' => '\common\models\base'
                ],
                'crud'       => [
                    'class' => \console\gii\generators\crud\Generator::class,
                ],
            ],
        ]
    ],
    'params'              => $params,
];
