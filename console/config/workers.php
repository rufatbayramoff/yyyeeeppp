<?php
/**
 * Additional config for rescue workers
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

return[
    'components' => [
        'db' => [
            'on afterOpen' => function()
            {
                Yii::$app->db->createCommand('SET wait_timeout = 259200; SET interactive_timeout = 259200;')->execute();
            }
        ],
    ],
];