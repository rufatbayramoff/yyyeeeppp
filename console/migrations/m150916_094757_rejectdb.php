<?php

use yii\db\Schema;
use yii\db\Migration;

class m150916_094757_rejectdb extends Migration
{
     
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("CREATE TABLE `system_reject` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `group` varchar(45) NOT NULL,
            `title` varchar(145) NOT NULL,
            `description` varchar(245) DEFAULT NULL,
            `is_active` tinyint(1) NOT NULL DEFAULT '1',
            PRIMARY KEY (`id`),
            UNIQUE KEY `group_UNIQUE` (`group`,`title`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (1,'ps_printer','Информация не должна содержать оскорблений и ненормативной лексики','',1);");
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (2,'ps_printer','Информация не должна содержать рекламу иных проектов, не связанных с услугой печати на 3D принтере','',1);");
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (3,'ps_printer','Информация не должна содержать пропаганду алкоголя, табакокурения и наркотиков','',1);");

        $this->execute("ALTER TABLE `ps_printer`
            ADD COLUMN `created_at` TIMESTAMP NULL DEFAULT NULL AFTER `moderator_status`,
            ADD COLUMN `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER `created_at`;");

        $this->execute("ALTER TABLE `ps`
            CHANGE COLUMN `moderator_status` `moderator_status` ENUM('new', 'updated', 'checking', 'checked', 'banned', 'rejected') NOT NULL;");
        $this->execute("ALTER TABLE `ps_printer`
            CHANGE COLUMN `moderator_status` `moderator_status` ENUM('new', 'updated', 'checking', 'checked', 'banned', 'rejected') NOT NULL;");
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->truncateTable('system_reject');
        $this->dropTable('system_reject');

        $this->dropColumn('ps_printer', 'created_at');
        $this->dropColumn('ps_printer', 'updated_at');
        
        $this->execute("ALTER TABLE `ps`
            CHANGE COLUMN `moderator_status` `moderator_status` ENUM('new', 'checking', 'ok', 'banned', 'review') NOT NULL;");
        $this->execute("ALTER TABLE `ps_printer`
            CHANGE COLUMN `moderator_status` `moderator_status` SET('new', 'checking', 'ok', 'banned', 'review') NULL DEFAULT NULL;");
    }
     
}
