<?php

use yii\db\Schema;
use yii\db\Migration;

class m151128_124924_system_collection extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_collection` ADD `system_type` ENUM('files','likes')  NULL  DEFAULT NULL  AFTER `cover`;");
        $this->execute("ALTER TABLE `user_profile` DROP FOREIGN KEY `fk_user_profile_2`;");
        $this->execute("ALTER TABLE `user_profile` DROP `default_collection_id`;");
    }

    public function down()
    {
        echo "m151128_124924_system_collection cannot be reverted.\n";

        return false;
    }
}
