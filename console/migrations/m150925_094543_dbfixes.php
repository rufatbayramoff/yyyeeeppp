<?php

use yii\db\Schema;
use yii\db\Migration;

class m150925_094543_dbfixes extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `user_profile` DROP COLUMN `trust_level`");
        $this->execute("ALTER TABLE `user_profile`  CHANGE COLUMN `address` `address` VARCHAR(145) NULL DEFAULT NULL ;");
        $this->execute("ALTER TABLE  `ps_printer_color` ADD INDEX (  `price` ) ;");
    }

    public function safeDown()
    {
        echo "m150925_094543_dbfixes cannot be reverted.\n";
        $this->addColumn('user_profile', 'trust_level', "CHAR(25) NULL");
    }
}
