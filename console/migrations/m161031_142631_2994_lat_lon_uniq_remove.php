<?php

use yii\db\Schema;
use yii\db\Migration;

class m161031_142631_2994_lat_lon_uniq_remove extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('lat_UNIQUE', 'geo_location');
    }

    public function safeDown()
    {
    }
}
