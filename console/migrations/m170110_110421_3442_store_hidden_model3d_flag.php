<?php

use yii\db\Migration;
use yii\db\Query;

class m170110_110421_3442_store_hidden_model3d_flag extends Migration
{
    public function up()
    {
        $this->addColumn('model3d', 'is_hidden_in_store', 'BOOLEAN NOT NULL DEFAULT \'0\'');
        $this->update('model3d', ['is_hidden_in_store' => 1], 'source = \'api\'');
    }

    public function down()
    {
        $this->dropColumn('model3d', 'is_hidden_in_store');
    }
}
