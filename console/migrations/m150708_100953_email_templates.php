<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_100953_email_templates extends Migration
{ 
    public function up()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $path = realpath(dirname(__DIR__) . '/../db/');
        $sql = file_get_contents($path . '/email_template.sql');
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->truncateTable('{{%email_template}}');
        return 0;
    } 
}
