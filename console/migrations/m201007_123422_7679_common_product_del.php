<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\components\UuidHelper;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m201007_123422_7679_common_product_del
 */
class m201007_123422_7679_common_product_del extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_product_bind_product', 'product_common');
        $this->dropForeignKey('fk_product_bind_model3d', 'product_common');
        $this->dropColumn('product_common', 'product_uuid');
        $this->dropColumn('product_common', 'model3d_uuid');


        $this->dropForeignKey('fk_model3d_product_category_id', 'model3d');
        $this->dropColumn('model3d', 'product_category_id');
        $this->dropForeignKey('fk_model3d_company_id', 'model3d');
        $this->dropColumn('model3d', 'company_id');
        $this->dropForeignKey('fk_model3d_1', 'model3d');
        $this->dropColumn('model3d', 'user_id');
        $this->dropColumn('model3d', 'product_status');
        $this->dropColumn('model3d', 'created_at');
        $this->dropColumn('model3d', 'updated_at');
        $this->dropColumn('model3d', 'price_per_produce');
        $this->dropColumn('model3d', 'is_active');
        $this->dropColumn('model3d', 'title');
        $this->dropColumn('model3d', 'description');

        $this->dropForeignKey('fk_product_category_id', 'product');
        $this->dropColumn('product', 'category_id');
        $this->dropForeignKey('fk_product_company_id', 'product');
        $this->dropColumn('product', 'company_id');
        $this->dropForeignKey('fk_product_cover_user_id', 'product');
        $this->dropColumn('product', 'user_id');
        $this->dropColumn('product', 'product_status');
        $this->dropColumn('product', 'created_at');
        $this->dropColumn('product', 'updated_at');
        $this->dropColumn('product', 'single_price');
        $this->dropColumn('product', 'is_active');
        $this->dropColumn('product', 'title');
        $this->dropColumn('product', 'description');

        $this->dropColumn('product_common', 'id');
        $this->execute('ALTER TABLE `product_common` DROP INDEX `product_common_uid_indx`, ADD PRIMARY KEY (`uid`) USING BTREE');

        $this->getDb()->createCommand('OPTIMIZE TABLE `model3d`')->execute();
        $this->getDb()->createCommand('OPTIMIZE TABLE `product`')->execute();
        $this->getDb()->createCommand('OPTIMIZE TABLE `product_common`')->execute();

        $this->createIndex('file_md5_indx', 'file', 'md5sum');
        $this->createIndex('product_common_product_statux_indx', 'product_common', 'product_status');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}
