<?php

use yii\db\Migration;

class m161006_113737_2835_static_page_table extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `static_page`(
          `id` INT NOT NULL AUTO_INCREMENT,
          `alias` VARCHAR(45) NOT NULL,
          PRIMARY KEY (`id`),
          UNIQUE INDEX `alias_UNIQUE` (`alias` ASC));
        ");


        $this->execute("
        
        CREATE TABLE `static_page_intl` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `static_page_id` INT NOT NULL,
  `lang_iso` VARCHAR(10) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `content` LONGTEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_static_page_intl_2_idx` (`static_page_id`, `lang_iso`),
  CONSTRAINT `fk_static_page_intl_1`
    FOREIGN KEY (`static_page_id`)
    REFERENCES `static_page` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_static_page_intl_2`
    FOREIGN KEY (`lang_iso`)
    REFERENCES `system_lang` (`iso_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
        ");

    }

    public function down()
    {
        $this->dropTable("static_page_intl");
        $this->dropTable("static_page");
        return true;
    }
}
