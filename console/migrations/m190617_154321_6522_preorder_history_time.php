<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.06.19
 * Time: 14:31
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190617_154321_6522_preorder_history_time
 */
class m190617_154321_6522_preorder_history_time extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('time_UNIQUE', 'preorder_history');
    }

    public function safeDown()
    {
        return true;
    }
}