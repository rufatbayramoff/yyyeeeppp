<?php

use yii\db\Migration;

class m160504_142419_printertestsort extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer_test`  
            ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `comment`;");
    }

    public function down()
    {
        $this->dropColumn('ps_printer_test', 'created_at');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
