<?php

use yii\db\Migration;

/**
 * Class m210413_114528_8311_rename_business_type_ps_table
 */
class m210413_114528_8311_rename_business_type_ps_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('ps','business_type','area_of_activity');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('ps','area_of_activity','business_type');
    }
}
