<?php

use common\interfaces\Model3dBaseImgInterface;
use yii\db\Migration;

class m160926_140821_2956_material_groups extends Migration
{
    public function up()
    {
        $this->addColumn('printer_material_group', 'photo_file_id', 'integer');
        $this->addColumn('printer_material_group', 'density', 'decimal(5,2) not null default 0');
        $this->addColumn('printer_material_group', 'short_description', 'string');
        $this->addColumn('printer_material_group', 'long_description', 'text');
        $this->addForeignKey('printer_material_group_photo_file_id', 'printer_material_group', 'photo_file_id', 'file', 'id');
    }

    public function down()
    {
        $this->dropColumn('printer_material_group', 'photo_file_id');
        $this->dropColumn('printer_material_group', 'short_description');
        $this->dropColumn('printer_material_group', 'long_description');
    }
}
