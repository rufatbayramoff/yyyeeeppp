<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200803_132621_7311_cutting_price
 */
class m200803_132621_7311_cutting_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('cutting_machine_processing', 'cutting_price', 'DECIMAL(14,6)');
        $this->alterColumn('cutting_machine_processing', 'engraving_price', 'DECIMAL(14,6)');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}