<?php

use yii\db\Migration;

/**
 * Class m180131_115646_5270_insert_template
 */
class m180131_115646_5270_insert_template extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $body = 'Hi %psName%,

Order #%orderId% results have been approved. Please set the status of the order as shipped and provide the carrier name and tracking number. Please enter N/A if you ship without a tracking number.

<a href="https://www.treatstock.com/my/printservice-order/print-requests/ready_send" target="blank" style="display: inline-block;margin:10px 0;border-radius: 0px;font-family: sans-serif;padding: 5px 20px;min-width: 40px;font-size: 16px;font-weight: normal;line-height: 20px;border: none;text-decoration:none !important;color: #fff;background-color:#2d8ee0;">Ship order</a>

Best,
Treatstock
';
        $this->insert('email_template', [
            'code' => 'psPrintedReviewed.selfship',
            'group' => 'order',
            'language_id' => 'en-US',
            'title' => 'Order #%orderId% results have been approved',
            'description' =>  '%orderId%, %psName% - sent when moderator accepts print results.',
            'template_html' => $body,
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('email_template', [
            'code' => 'psPrintedReviewed.selfship',
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180131_115646_5270_insert_template cannot be reverted.\n";

        return false;
    }
    */
}
