<?php

use yii\db\Migration;
use yii\db\Query;

class m161223_115521_3342_model3d_null_user_id extends Migration
{

    public function up()
    {
        $this->alterColumn('model3d', 'user_id', 'integer(11) null');
        $this->alterColumn('model3d_replica', 'user_id', 'integer(11) null');
    }

    public function down()
    {
    }
}
