<?php

use yii\db\Migration;

class m160720_084900_2421_sms_gatewat_in_ps extends Migration
{
    public function safeUp()
    {
        $this->addColumn('ps', 'sms_gateway', 'varchar(20)');
        $this->update('ps', ['sms_gateway' => 'cmsms'], ['phone_status' => ['checking', 'checked']]);
    }

    public function safeDown()
    {
        $this->dropColumn('ps', 'sms_gateway');
        return true;
    }
}
