<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200924_162821_7679_model_categories
 */
class m200924_162821_7679_model_categories extends Migration
{
    protected function getCategoriesMap(): array
    {
        $stringMaps = [
            'Art'          => 'home-accessorises',
            'Fashion'      => 'jewelry-jewelry-imitation-jewelry',
            'Home Goods'   => 'home-accessorises',
            'Pet Goods'    => 'pet-supplies',
            'Games'        => 'game-miniatures',
            'Technology'   => 'consumer-electronics-appliances-consumer-electronics-other',
            'Construction' => 'construction',
            'Health'       => 'pesronal-protection',
            'Miniatures'   => 'miniatures',
            'Mockup'       => 'mockups',
            'Cosplay'      => 'cosplay',
            'Sport'        => 'sporting-goods-other-sports-other',
        ];
        $idsMap     = [];
        foreach ($stringMaps as $model3dCategoryTitle => $productCategoryCode) {
            $model3dCategory = (new Query())->select('*')->from('model3d_category')->where(['title' => $model3dCategoryTitle])->one();
            $productCategory = (new Query())->select('*')->from('product_category')->where(['code' => $productCategoryCode])->one();
            if($model3dCategory && $productCategory){
                $idsMap[$model3dCategory['id']] = $productCategory['id'];
            }
        }
        return $idsMap;
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `model3d` ADD `product_category_id` INT(11) AFTER `category_id`, ADD INDEX (`product_category_id`);');
        $this->execute('ALTER TABLE `model3d` ADD CONSTRAINT `fk_model3d_product_category_id` FOREIGN KEY (`product_category_id`) REFERENCES `product_category`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;');
        $this->execute('ALTER TABLE `model3d_replica` ADD `product_category_id` INT(11) AFTER `category_id`, ADD INDEX (`product_category_id`);');
        $this->execute('ALTER TABLE `model3d_replica` ADD CONSTRAINT `fk_model3d_replica_product_category_id` FOREIGN KEY (`product_category_id`) REFERENCES `product_category`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;');

        $idsMap = $this->getCategoriesMap();
        foreach ($idsMap as $model3dCategoryId => $productCategoryId) {
            $this->update('model3d', ['product_category_id' => $productCategoryId], ['category_id' => $model3dCategoryId]);
            $this->update('model3d_replica', ['product_category_id' => $productCategoryId], ['category_id' => $model3dCategoryId]);
        }

        $this->dropForeignKey('fk_model3d_2', 'model3d');
        $this->dropForeignKey('fk_model3d_replica_2', 'model3d_replica');

        $this->dropColumn('model3d', 'category_id');
        $this->dropColumn('model3d_replica', 'category_id');
        $this->dropForeignKey('fk_custom_code' ,'model3d_category');

        $this->dropTable('custom_codes_country');
        $this->dropTable('custom_code');

        $this->dropTable('model3d_category_intl');
        $this->dropTable('model3d_category');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}
