<?php

use yii\db\Migration;

class m170731_120821_4225_cnc_materail_same extends Migration
{
    public function safeUp()
    {
        $this->renameTable('cnc_material', 'cnc_material_same');

    }

    public function safeDown()
    {
        $this->renameTable('cnc_material_same', 'cnc_material');
    }

}
