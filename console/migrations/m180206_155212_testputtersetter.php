<?php

use yii\db\Migration;

/**
 * Class m180206_155212_testputtersetter
 */
class m180206_155212_testputtersetter extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute(
            '
CREATE TABLE `off_codeception_temp_values` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(100) NOT NULL,
  `val` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'
        );

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('off_codeception_temp_values');
    }

}
