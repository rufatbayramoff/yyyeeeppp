<?php

use yii\db\Migration;

/**
 * Class m180419_080857_5360_email_templates
 */
class m180419_080857_5360_email_templates extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('email_template', [
            'code' => 'company.service.moderation.reject',
            'group' => 'service',
            'language_id' => 'en-US',
            'title' => 'Publishing of your %serviceName% on treatstock.com',

            'description' => ' params: serviceName, reason, comment, companyName, rejectReason, companyServiceLink, username',
            'template_html' => 'Hello %companyName%,

The information you have entered about your service has not been approved by the moderator.  %rejectReason% Please <a href="%companyServiceLink%">edit your service</a>.

Best regards,
Treatstock',
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);

        $this->insert('email_template', [
            'code' => 'company.service.moderation.approved',
            'group' => 'service',
            'language_id' => 'en-US',
            'title' => 'Publishing of your %serviceName% on treatstock.com',

            'description' => ' params: serviceName, username, companyName, companyServiceLink',
            'template_html' => 'Hello %companyName%,

Your service approved! Please <a href="%companyServiceLink%">visit your service</a>.

Best regards,
Treatstock',
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('email_template', [
            'code' => 'company.service.moderation.reject',
        ]);
        $this->delete('email_template', [
            'code' => 'company.service.moderation.approved',
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180419_080857_5360_email_templates cannot be reverted.\n";

        return false;
    }
    */
}
