<?php

use common\components\ArrayHelper;
use common\models\Model3dReplica;
use yii\db\Migration;
use yii\db\Query;

class m160815_113121_1644_alter_ps_printer_status extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
ALTER TABLE `ps_printer_file_status` 
DROP FOREIGN KEY `fk_3diax_model3d_2`;
ALTER TABLE `ps_printer_file_status` 
CHANGE COLUMN `model3d_file_id` `model3d_replica_part_id` INT(11) NOT NULL;
ALTER TABLE `ps_printer_file_status` 
ADD CONSTRAINT `fk_3diax_model3d_2`
  FOREIGN KEY (`model3d_replica_part_id`)
  REFERENCES `model3d_replica_part` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SQL
        );
    }

    public function down()
    {
        echo "m160815_113121_1644_alter_ps_printer_status cannot be reverted.\n";
        return false;
    }
}
