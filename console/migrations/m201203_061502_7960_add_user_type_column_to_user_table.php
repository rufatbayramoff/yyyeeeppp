<?php

use yii\db\Migration;

/**
 * Class m201203_061502_7960_add_user_type_column_to_user_table
 */
class m201203_061502_7960_add_user_type_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'is_company_creating_in_progress', $this->boolean()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'is_company_creating_in_progress');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201203_061502_add_user_type_colum_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
