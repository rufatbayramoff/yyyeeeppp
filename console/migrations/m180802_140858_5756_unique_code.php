<?php

use yii\db\Migration;

/**
 * Class m180802_140858_5756_unique_code
 */
class m180802_140858_5756_unique_code extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `product_category` DROP INDEX `title_UNIQUE`, ADD UNIQUE INDEX `code_UNIQUE` (`code` ASC);');

        $this->execute('ALTER TABLE `product_category` 
CHANGE COLUMN `code` `code` VARCHAR(145) NOT NULL ,
CHANGE COLUMN `title` `title` VARCHAR(145) NOT NULL ;
');
        $this->execute('ALTER TABLE `product_category` ADD COLUMN `is_visible` TINYINT(1) NULL DEFAULT 1 AFTER `updated_at`;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('ALTER TABLE `product_category` DROP INDEX `code_UNIQUE` , ADD UNIQUE INDEX `title_UNIQUE` (`title` ASC);');
        $this->dropColumn('product_category', 'is_visible');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180802_140858_5756_unique_code cannot be reverted.\n";

        return false;
    }
    */
}
