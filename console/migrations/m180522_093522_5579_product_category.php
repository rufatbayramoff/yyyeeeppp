<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180522_093522_5579_product_category
 */
class m180522_093522_5579_product_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
  CREATE TABLE `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code`  VARCHAR (45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `total_count` int(11) NOT NULL DEFAULT '0',
  `description` text,
  `full_description` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`),
  KEY `fk_product_category_1_idx` (`parent_id`),
  CONSTRAINT `fk_product_category_1` FOREIGN KEY (`parent_id`) REFERENCES `product_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
        $this->execute("
  CREATE TABLE `product_category_intl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `title` varchar(245) NOT NULL,
  `description` text,
  `full_description` mediumtext,
  `lang_iso` char(5) NOT NULL DEFAULT 'en-US',
  `is_active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_category_intl_unique_index` (`model_id`,`lang_iso`),
  KEY `product_category_intl_search_index` (`model_id`,`lang_iso`,`is_active`),
  CONSTRAINT `product_category_intl_fk_model_id` FOREIGN KEY (`model_id`) REFERENCES `product_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return true;
    }
}
