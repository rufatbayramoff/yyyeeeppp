<?php

use yii\db\Schema;
use yii\db\Migration;

class m150909_084813_delivery extends Migration
{
     
    public function safeUp()
    {
         
        $this->addColumn('user_address', 'phone', 'VARCHAR(45) NULL');
        $this->addColumn('user_address', 'email', 'VARCHAR(45) NULL');
        $this->addColumn('user_address', 'company', 'VARCHAR(45) NULL');
        
        $this->execute("CREATE TABLE `delivery_parcel` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `order_id` INT(11) NOT NULL,
            `weight` DECIMAL(5,2) NOT NULL,
            `width` INT(11) NOT NULL,
            `height` INT(11) NOT NULL,
            `length` INT(11) NOT NULL,
            `created_at` TIMESTAMP NULL,
            `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            INDEX `fk_delivery_parcel_1_idx` (`order_id` ASC),
            CONSTRAINT `fk_delivery_parcel_1`
              FOREIGN KEY (`order_id`)
              REFERENCES `store_order` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION);
        ");
        $this->execute("ALTER TABLE `user_location`  ADD COLUMN `address_id` INT(11) NULL AFTER `location_type`; ");
        $this->execute("ALTER TABLE `user_location` 
            ADD INDEX `fk_user_location_1_idx` (`address_id` ASC);
            ALTER TABLE `user_location` 
            ADD CONSTRAINT `fk_user_location_1`
              FOREIGN KEY (`address_id`)
              REFERENCES `user_address` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
    }

    public function safeDown()
    {        
        $this->dropColumn('user_address', 'company');
        $this->dropColumn('user_address', 'email');
        $this->dropColumn('user_address', 'phone');
        
        $this->dropForeignKey('fk_user_location_1', 'user_location');
        $this->dropColumn('user_location', 'address_id');
        $this->dropTable('delivery_parcel');
    } 
}
