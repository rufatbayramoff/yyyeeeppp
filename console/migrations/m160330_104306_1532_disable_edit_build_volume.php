<?php

use yii\db\Migration;

class m160330_104306_1532_disable_edit_build_volume extends Migration
{
    public function up()
    {
        $this->execute("
          UPDATE printer_to_property
          LEFT JOIN printer ON printer.id = printer_to_property.printer_id
          SET printer_to_property.is_editable = 0 WHERE printer_to_property.property_id = 1 AND printer.have_unfixed_size = 0
        ");

        $this->execute("
            DELETE ps_printer_to_property
            FROM ps_printer_to_property
            LEFT JOIN ps_printer ON ps_printer.id = ps_printer_to_property.ps_printer_id
            LEFT JOIN printer ON printer.id = ps_printer.printer_id
            WHERE property_id = 1 AND printer.have_unfixed_size = 0
        ");

    }

    public function down()
    {
        return true;
    }

}
