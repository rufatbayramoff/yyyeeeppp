<?php

use yii\db\Migration;

class m170814_063519_4591_payment_addons extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `store_order_position` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `order_id` int(11) NOT NULL,
              `title` varchar(245) NOT NULL,
              `file_ids` json DEFAULT NULL,
              `created_at` datetime NOT NULL,
              `updated_at` datetime DEFAULT NULL,
              `status` char(25) NOT NULL DEFAULT \'new\',
              `amount` decimal(8,2) NOT NULL,
              `currency_iso` char(3) NOT NULL DEFAULT \'USD\',
              `fee` decimal(6,2) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_store_order_position_2_idx` (`order_id`),
              KEY `fk_store_order_position_1_idx` (`user_id`),
              CONSTRAINT `fk_store_order_position_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_store_order_position_2` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
        ');

        foreach($this->getEmails() as $code => $emailTpl){
            $this->insert('email_template', [
                'code' => $code,
                'group' => 'order',
                'language_id' => 'en-US',
                'title' => $emailTpl['title'],
                'description' => $emailTpl['description'] . ' params: ' . implode(',',$emailTpl['params']),
                'template_html' => nl2br($emailTpl['body']),
                'updated_at' => dbexpr('NOW()'),
                'is_active' => true
            ]);
        }
        $this->insert('store_pricer_type', [
            'type' => 'addon',
            'description' => 'Additional Services',
            'operation' => '+'
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('store_order_position');

        foreach($this->getEmails() as $code => $emailTpl){
            $this->delete('email_template', [
                'code' => $code,
            ]);
        }
        $this->delete('store_pricer_type', ['type' => 'addon']);
    }
    private function getEmails(){
        $emails = [
            'ps.order.positionNew' => [
                'title'  => 'Additional service for order %orderId% has been submitted',
                'description' => 'additional service added - email to ps',
                'params' => ['positionTitle', 'orderId', 'positionAmount'],
                'body'   => 'Hi %name%,
Your request for the additional service %positionTitle% for %positionAmount% has been submitted to the customer for approval. 
We will notify you when the customer responds.

Best Regards, 
Treatstock'
            ],
            'client.order.positionNew' => [
                'title'  => 'Additional service request for order %orderId%',
                'description' => 'additional service added - email to client',
                'params' => ['positionTitle', 'orderId', 'positionAmount'],
                'body'   => 'Hello %name%,
The print shop has reviewed order %orderId% and suggested an additional service %positionTitle% for %positionAmount%. 
To review the additional service and accept or decline, <a href="https://www.treatstock.com/workbench/orders">click here</a>. 

Best Regards, 
Treatstock'
            ],
            'ps.order.positionPaid' => [
                'title'  => 'Additional service request for order %orderId% has been accepted',
                'description' => 'additional service paid  - email to ps',
                'params' => ['positionTitle', 'orderId', 'positionAmount'],
                'body'   => 'Hello %name%,
The customer for order %orderId% has reviewed your request for the additional service %positionTitle% and paid %positionAmount%. 
To view the order, <a href="https://www.treatstock.com/my/printservice-order/print-requests/new">click here</a>.

Best Regards, 
Treatstock'
            ],
            'ps.order.positionDeclined' => [
                'title'  => 'Additional service request for order %orderId% has been declined',
                'description' => 'additional service declined - email to ps',
                'params' => ['positionTitle', 'orderId', 'positionAmount'],
                'body'   => 'Hi %name%,
The customer for order %orderId% has reviewed your request for the additional service %positionTitle% and declined to pay %positionAmount%. 
To view the order, <a href="https://www.treatstock.com/my/printservice-order/print-requests/new">click here</a>.

Best Regards, 
Treatstock'
            ],

        ];
        return $emails;
    }

}
