<?php

use yii\db\Schema;
use yii\db\Migration;

class m151012_104707_status_to_progress extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('ALTER TABLE `ps_printer_3diax_tokens`
                  CHANGE COLUMN `status` `progress` VARCHAR(20) NULL DEFAULT NULL;');
        $this->execute('ALTER TABLE `ps_printer_file_status` DROP INDEX `idx_order_model3d`;');
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE `ps_printer_3diax_tokens`
                  CHANGE COLUMN `progress` `status` VARCHAR(20) NULL DEFAULT NULL;');
    }
}
