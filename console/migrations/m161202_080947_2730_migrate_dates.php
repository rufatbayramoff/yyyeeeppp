<?php

use yii\db\Migration;

class m161202_080947_2730_migrate_dates extends Migration
{
    public function up()
    {
        $this->execute("
                  
            ALTER TABLE `ps_printer_order_dates`
            CHANGE COLUMN `accepted_date` `accepted_date` TIMESTAMP NULL DEFAULT NULL ,
            CHANGE COLUMN `plan_printed_at` `plan_printed_at` TIMESTAMP NULL DEFAULT NULL ,
            ADD COLUMN `shipped_at` TIMESTAMP NULL DEFAULT NULL AFTER `delivered_at`,
            ADD COLUMN `received_at` TIMESTAMP NULL DEFAULT NULL AFTER `shipped_at`;
            
             UPDATE ps_printer_order_dates SET plan_printed_at = NULL WHERE CAST(plan_printed_at AS CHAR(20)) = '0000-00-00 00:00:00';
             UPDATE ps_printer_order_dates SET accepted_date = NULL WHERE CAST(accepted_date AS CHAR(20)) = '0000-00-00 00:00:00';
            
            INSERT INTO ps_printer_order_dates (order_id, ps_printer_id ) (
            SELECT a.order_id, a.printer_id as ps_printer_id FROM store_order_attemp a
            LEFT JOIN ps_printer_order_dates d ON a.order_id = d.order_id
            WHERE ISNULL(d.id) 
            );
            
            UPDATE ps_printer_order_dates as d, store_order as o
            SET d.shipped_at = o.shipped_at, d.received_at = o.received_at
            WHERE d.order_id = o.id;
            
            ALTER TABLE `store_order` 
            DROP COLUMN `received_at`,
            DROP COLUMN `shipped_at`;


            ALTER TABLE `store_order_attemp` 
            ADD COLUMN `dates_id` INT UNSIGNED NULL AFTER `printer_id`;


            ALTER TABLE `ps_printer_order_dates` 
            ADD COLUMN `attemp_id` INT UNSIGNED NULL FIRST;

            ALTER TABLE `ps_printer_order_dates` 
            ADD INDEX `fk_ps_printer_order_dates_1_idx` (`attemp_id` ASC);
            ALTER TABLE `ps_printer_order_dates` 
            ADD CONSTRAINT `fk_ps_printer_order_dates_1`
              FOREIGN KEY (`attemp_id`)
              REFERENCES `store_order_attemp` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
            
            UPDATE ps_printer_order_dates as d, store_order_attemp a
            SET d.attemp_id = a.id
            WHERE d.order_id = a.order_id;

          UPDATE store_order_attemp SET dates_id = id;

ALTER TABLE `ps_printer_order_dates` 
DROP FOREIGN KEY `fk_ps_printer_order_dates_1`,
DROP FOREIGN KEY `order_dates_printer_fk`,
DROP FOREIGN KEY `order_dates_order_fk`;
ALTER TABLE `ps_printer_order_dates` 
DROP COLUMN `order_id`,
DROP COLUMN `ps_printer_id`,
DROP COLUMN `id`,
CHANGE COLUMN `attemp_id` `attemp_id` INT(10) UNSIGNED NOT NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`attemp_id`),
DROP INDEX `printer_dates_order_id` ,
DROP INDEX `printer_dates_printer_id` ,
DROP INDEX `printer_order_dates_unique` ;


ALTER TABLE `store_order_attemp` 
ADD INDEX `fk_store_order_attemp_3_idx` (`dates_id` ASC);
ALTER TABLE `store_order_attemp` 
ADD CONSTRAINT `fk_store_order_attemp_3`
  FOREIGN KEY (`dates_id`)
  REFERENCES `ps_printer_order_dates` (`attemp_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


ALTER TABLE `ps_printer_order_dates` 
RENAME TO  `store_order_attemp_dates`;
        ");


































    }

    public function down()
    {
        echo "m161202_080947_2730_migrate_dates cannot be reverted.\n";

        return false;
    }

}
