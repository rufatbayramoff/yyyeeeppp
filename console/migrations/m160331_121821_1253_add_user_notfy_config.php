<?php

use yii\db\Migration;

class m160331_121821_1253_add_user_notfy_config extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user` ADD `notify_config` TEXT  NULL  AFTER `document_file_id`;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `user` DROP `notify_config`;");
        return true;
    }
}
