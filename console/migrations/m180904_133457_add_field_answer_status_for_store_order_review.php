<?php

use yii\db\Migration;

/**
 * Class m180904_133457_add_field_answer_status_for_store_order_review
 */
class m180904_133457_add_field_answer_status_for_store_order_review extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_review', 'verified_answer_status', "enum ('moderated', 'rejected') default 'moderated' not null after answer");
        $this->addColumn('store_order_review', 'answer_reject_comment', 'text null after verified_answer_status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('store_order_review', 'verified_answer_status');
        $this->dropColumn('store_order_review', 'answer_reject_comment');
    }
}
