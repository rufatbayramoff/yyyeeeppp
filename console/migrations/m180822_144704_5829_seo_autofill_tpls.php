<?php

use yii\db\Migration;

/**
 * Class m180822_144704_5829_seo_autofill_tpls
 */
class m180822_144704_5829_seo_autofill_tpls extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // insert rows for products seo templates
        $rows = [
            [
                'type' => 'product',
                'template_name' => 'product',
                'title' => '%productTitle% by %companyTitle% in %parentCategoryTitle% %categoryTitle%',
                'meta_description' => '%description%',
                'meta_keywords' => '',
                'header' => '%title%',
                'lang_iso' => 'en-US',
                'is_default' => true,
                'is_apply_created' => false,
                'header_text' => '',
                'footer_text' => ''
            ],
            [
                'type' => 'product_category',
                'template_name' => 'product Category',
                'title' => '%title% %parentCategoryTitle% products - Treatstock',
                'meta_description' => '%description%',
                'meta_keywords' => '',
                'header' => '%title%',
                'lang_iso' => 'en-US',
                'is_default' => true,
                'is_apply_created' => false,
                'header_text' => '',
                'footer_text' => ''
            ],

            [
                'type' => 'company_service',
                'template_name' => 'Company Services',
                'title' => '%title% by %companyTitle% from %serviceCategoryTitle% - Treatstock',
                'meta_description' => '%descriptionPlain%',
                'meta_keywords' => '%companyTags%',
                'header' => '%title%',
                'lang_iso' => 'en-US',
                'is_default' => true,
                'is_apply_created' => false,
                'header_text' => '',
                'footer_text' => ''
            ],
            [
                'type' => 'company_service_category',
                'template_name' => 'Company services categories',
                'title' => '%title% Services - Treatstock',
                'meta_description' => '%descriptionPlain%',
                'meta_keywords' => '',
                'header' => '%title%',
                'lang_iso' => 'en-US',
                'is_default' => true,
                'is_apply_created' => false,
                'header_text' => '',
                'footer_text' => ''
            ],
        ];
        foreach ($rows as $row) {
            $this->insert('seo_page_autofill_template', $row);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('seo_page_autofill_template', ['type' => ['product', 'product_category', 'company_service', 'company_service_category']]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180822_144704_5829_seo_autofill_tpls cannot be reverted.\n";

        return false;
    }
    */
}
