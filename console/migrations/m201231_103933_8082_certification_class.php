<?php

use common\models\AffiliateSource;
use common\modules\company\components\CompanyServiceInterface;
use yii\db\Migration;
use yii\db\Query;

class m201231_103933_8082_certification_class extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
CREATE TABLE `company_service_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cst_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');

        $this->execute("
          CREATE TABLE `company_service_type_intl` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `model_id` int(11) NOT NULL,
          `title` varchar(255) NOT NULL,
          `lang_iso` char(5) NOT NULL DEFAULT 'en-US',
          `is_active` bit(1) DEFAULT b'1',
          PRIMARY KEY (`id`),
          UNIQUE KEY `cst_intl_unique_index` (`model_id`,`lang_iso`),
          KEY `cst_intl_search_index` (`model_id`,`lang_iso`,`is_active`),
          CONSTRAINT `cst_intl_fk_model_id` FOREIGN KEY (`model_id`) REFERENCES `company_service_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
        $this->batchInsert('company_service_type', ['id', 'name', 'title'], [
            [1, 'cnc', 'CNC machining'],
            [2, 'cutting', '2D Cutting and Engraving'],
            [3, 'designer', 'Designer'],
            [4, 'printer', '3D Printing'],
            [5, 'service', 'Other'],
            [6, 'window', 'Window manufacturing']
        ]);


        $this->addForeignKey('fk_company_service_type', 'company_service', 'type', 'company_service_type', 'name');

        $this->execute('
CREATE TABLE `ts_certification_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `service_type` char(15) NOT NULL,
  `price` decimal(14,2) NOT NULL,
  `currency` char(3) NOT NULL,
  `max_orders_count` int(11) NOT NULL,
  `max_order_price` decimal(14,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `currency` (`currency`),
  KEY `service_type` (`service_type`),
  CONSTRAINT `ts_certification_class_currency` FOREIGN KEY (`currency`) REFERENCES `payment_currency` (`currency_iso`),
  CONSTRAINT `ts_certification_service_type` FOREIGN KEY (`service_type`) REFERENCES `company_service_type` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        $this->insert('ts_certification_class', [
            'id'           => 1,
            'title'        => 'Default',
            'description'  => 'Default',
            'service_type' => 'service',
            'price'        => 100,
            'currency'     => 'USD',
            'max_orders_count' => 0,
            'max_order_price' => 0
        ]);
        $this->insert('ts_certification_class', [
            'id'           => 10,
            'title'        => 'Professional',
            'description'  => 'Professional Certificate confirms your printer is capable of printing complex models, with many small details, to a high quality.',
            'service_type' => 'printer',
            'price'        => 100,
            'currency'     => 'USD',
            'max_orders_count' => 10,
            'max_order_price' => 0
        ]);

        $this->execute('ALTER TABLE `printer` ADD `ts_certification_class_id` INT(11) AFTER `positional_accuracy`, ADD INDEX (`ts_certification_class_id`);');
        $this->update('printer', ['ts_certification_class_id' => 10]);
        $this->execute("ALTER TABLE `ps_printer_test` CHANGE `status` `status` ENUM('new','checked','failed','repeat') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'new';");
        $this->execute("ALTER TABLE `printer` CHANGE `ts_certification_class_id` `ts_certification_class_id` INT(11) NOT NULL DEFAULT '10';");
        $this->execute('ALTER TABLE `printer` ADD CONSTRAINT `fk_printer_certification_class` FOREIGN KEY (`ts_certification_class_id`) REFERENCES `ts_certification_class`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE ;');

        $this->execute('ALTER TABLE `ts_internal_purchase_certification` ADD `ts_certification_class_id` INT(11) NOT NULL AFTER `expire_date`, ADD INDEX (`ts_certification_class_id`);');
        $this->execute('ALTER TABLE `ts_internal_purchase_certification` ADD CONSTRAINT `ts_internal_purchase_class_id` FOREIGN KEY (`ts_certification_class_id`) REFERENCES `ts_certification_class`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;');

        $this->execute('ALTER TABLE `ps_printer_test` ADD `ts_internal_purchase_uid` VARCHAR(6) AFTER `comment`, ADD INDEX (`ts_internal_purchase_uid`);');
        $this->execute("UPDATE `ps_printer_test` SET `created_at`='2016-01-01 00:00:00' WHERE `created_at`<'2016-01-01 00:00:00'");
        $this->execute('ALTER TABLE `ps_printer_test` ADD CONSTRAINT `fk_ps_printer_test_internal_purchase` FOREIGN KEY (`ts_internal_purchase_uid`) REFERENCES `ts_internal_purchase`(`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT;');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
