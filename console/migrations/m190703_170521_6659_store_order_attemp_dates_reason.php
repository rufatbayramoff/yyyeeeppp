<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190703_170521_6659_store_order_attemp_dates_reason
 */
class m190703_170521_6659_store_order_attemp_dates_reason extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('store_order_attemp_dates', 'request_reason', 'varchar(1024)');
    }

    public function safeDown()
    {
        return false;
    }
}