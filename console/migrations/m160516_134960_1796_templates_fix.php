<?php

use yii\db\Migration;

class m160516_134960_1796_templates_fix extends Migration
{
    public function up()
    {


        $this->delete('email_template', ['code' => 'clientPsAcceptButCancel']);
        $this->execute("INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(42, 'clientPsAcceptButCancel', 'order', 'en-US', 'Your order #%orderId% on treatstock.com is rejected', NULL, NULL, 'Hello %clientName%!\nThe Print Service %psName% has rejected the order #%orderId% you have made earlier.\nThe order has now been cancelled.\nYou can choose another Print Service here: <a href=\"%chooseAnotherPsLink%\">%chooseAnotherPsLink%</a>\n\nBest regards,\nTreatstock', 'Hello %clientName%!\nThe Print Service %psName% has rejected the order #%orderId% you have made earlier.\nThe order has now been cancelled.\nYou can choose another Print Service here: <a href=\"%chooseAnotherPsLink%\">%chooseAnotherPsLink%</a>\n\nBest regards,\nTreatstock');
");

        $this->delete('email_template', ['code' => 'psDeclineOrder']);
        $this->execute("INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(12, 'psDeclineOrder', 'order', 'en-US', 'Your order #%orderId% has been cancelled', 'this email sent after PS decline order', '2016-01-28 08:04:21', 'Hello %clientName%!\nUnfortunately your order #%orderId% has been cancelled by %psName% Print Service due to the following reason: %reason%.\nPlease allow an additional 7 days for payment to return to your account.\nYou can get more information about your refund here: <a href=\"%returnPolicyLink%\">%returnPolicyLink%</a>\nAdditional comments: %comment%.\n\nBest regards,\nTreatstock', 'Hello %clientName%!\nUnfortunately your order #%orderId% has been cancelled by %psName% Print Service due to the following reason: %reason%.\nPlease allow an additional 7 days for payment to return to your account.\nYou can get more information about your refund here: %returnPolicyLink%\nAdditional comments: %comment%.\n\nBest regards,\nTreatstock');
");


    }

    public function down()
    {
        return true;
    }
}
