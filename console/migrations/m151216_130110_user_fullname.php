<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_130110_user_fullname extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_profile`  ADD COLUMN `full_name` VARCHAR(45) NULL DEFAULT '' AFTER `user_id`;");
    }

    public function down()
    {
        $this->dropColumn('user_profile', 'full_name');
    }
}
