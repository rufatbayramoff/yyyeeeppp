<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;


/**
 * Class m201203_133120_7921_cutting_qty
 */
class m201203_133120_7921_cutting_qty extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute('UPDATE `cutting_pack_part` SET `qty`=1');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}
