<?php

use yii\db\Migration;

class m160216_155103_g_to_gr_in_ps_colors extends Migration
{
    public function up()
    {
        $this->execute("UPDATE `ps_printer_color` SET `price_measure` = 'gr' WHERE `price_measure` = 'g'");
    }

    public function down()
    {
        return true;
    }
}
