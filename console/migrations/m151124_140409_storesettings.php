<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_140409_storesettings extends Migration
{
    public function up()
    {
        try{
            if(file_exists("common\models\base\SystemSetting")){
                app('setting')->add("store.min_price_usd", ['value' => '1']);
                app('setting')->add("store.max_intl_price", ['value' => '2500']);
                app('setting')->add("store.ts_fee", ['value' => '0.15']);
            }
        }catch(\Exception $e){    
            
        }
    }

    public function down()
    {
         
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
