<?php

use yii\db\Migration;

/**
 * Class m210128_121332_add_balance_type_to_ts_internal_purchase_table
 */
class m210128_121332_8129_add_balance_type_to_ts_internal_purchase_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE ts_internal_purchase MODIFY COLUMN `type` enum('certification','deposit') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
