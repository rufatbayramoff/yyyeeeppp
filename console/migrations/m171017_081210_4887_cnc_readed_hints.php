<?php

use yii\db\Migration;

class m171017_081210_4887_cnc_readed_hints extends Migration
{
    public function safeUp()
    {
        $this->addColumn("ps", "is_cnc_hints_readed", "TINYINT(1) NOT NULL DEFAULT 0");
    }

    public function safeDown()
    {
        $this->dropColumn("ps", "is_cnc_hints_readed");
        return true;
    }


}
