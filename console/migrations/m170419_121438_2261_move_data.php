<?php

use yii\db\Migration;

class m170419_121438_2261_move_data extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO store_order_attempt_moderation (attempt_id, `status`)
            (
                SELECT 
                    store_order_attemp.id as attempt_id,
                    'new' as `status`
                FROM store_order_attemp 
                WHERE store_order_attemp.status <> 'new'
            );  
        ");

        $this->execute(" 
            INSERT IGNORE INTO store_order_attempt_moderation (attempt_id, `status`) 
            (
                SELECT 
                    ps_printer_file_status.order_attemp_id as attempt_id,
                    'new' as `status`
                FROM ps_printer_file_status
            );
        ");

        $this->execute("
            UPDATE store_order_attemp, store_order_attempt_moderation
            SET store_order_attempt_moderation.status = 'accepted' 
            WHERE
             store_order_attemp.status IN ('ready_send', 'sent', 'delivered', 'received')
             AND store_order_attemp.id = store_order_attempt_moderation.attempt_id;
        ");

        $this->execute("
            UPDATE store_order_attemp, store_order_attempt_moderation
            SET store_order_attempt_moderation.status = 'rejected' 
            WHERE 
                store_order_attemp.status IN ('cancelled')
                AND store_order_attemp.id = store_order_attempt_moderation.attempt_id;
        ");

        $this->execute("
          INSERT INTO store_order_attempt_moderation_file (attempt_id, file_id)
            (
                SELECT 
                    ps_printer_file_status.order_attemp_id as attempt_id,
                    ps_printer_file_status.file_id as file_id
                FROM ps_printer_file_status 
                WHERE ps_printer_file_status.file_id IS NOT NULL
            );  
        ");

        $this->execute("
            UPDATE file, store_order_attempt_moderation_file 
            SET file.ownerClass = 'common\\models\\StoreOrderAttemptModerationFile'
            WHERE file.id = store_order_attempt_moderation_file.file_id;
        
        ");
    }

    public function safeDown()
    {
        $this->execute("DELETE * FROM store_order_attempt_moderation_file");
        $this->execute("DELETE * FROM store_order_attempt_moderation");
    }
}
