<?php

use yii\db\Migration;

/**
 * Class m180726_145136_dev_default_product_category
 */
class m180726_145136_dev_default_product_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("INSERT IGNORE INTO `product_category` 
        (`code`, `title`, `is_active`, `parent_id`, `total_count`, `description`, `full_description`, `need_premoderation`, `position`, `updated_at`) 
        VALUES ('home-goods', 'Home Goods', '1', '1', '0', '', '', '0', '1', '2018-07-26 10:10:10');");


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180726_145136_dev_default_product_category cannot be reverted.\n";

        return false;
    }
    */
}
