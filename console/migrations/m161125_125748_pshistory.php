<?php

use yii\db\Migration;

class m161125_125748_pshistory extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `ps_printer_history` CHANGE COLUMN `comment` `comment` TEXT NULL DEFAULT NULL ;');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
