<?php

use yii\db\Migration;

/**
 * Class m190111_103807_6070_store_order_attempt_moderation_file_main_file
 */
class m190111_103807_6070_store_order_attempt_moderation_file_main_file extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_attempt_moderation_file', 'is_main_review_file', $this->tinyInteger()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('store_order_attempt_moderation_file', 'is_main_review_file');
    }
}
