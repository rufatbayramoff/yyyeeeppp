<?php

use yii\db\Migration;

class m160609_151006_order_change_ps extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `store_order` ADD COLUMN `can_change_ps` TINYINT(1) NOT NULL DEFAULT 0 AFTER `messages`;');

        $this->execute("CREATE TABLE IF NOT EXISTS `store_order_offer` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `order_id` int(11) NOT NULL,
              `printer_id` int(11) NOT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `status` char(15) NOT NULL DEFAULT 'new',
              PRIMARY KEY (`id`),
              UNIQUE KEY `fk_store_order_offer_uq` (`order_id`,`printer_id`),
              KEY `fk_store_order_offer_1_idx` (`order_id`),
              KEY `fk_store_order_offer_2_idx` (`printer_id`),
              CONSTRAINT `fk_store_order_offer_1` FOREIGN KEY (`printer_id`) REFERENCES `ps_printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_store_order_offer_2` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB"
        );

        // copy offer template
        $this->execute("INSERT IGNORE INTO `email_template` (`code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
            SELECT 'psNewOrderOffer', `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text` 
            FROM email_template WHERe code='psNewOrder'");

        $this->execute("INSERT IGNORE INTO `email_template` (`code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
            SELECT 'clientPrintStartOfferedOrder', `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text` 
            FROM email_template WHERe code='clientPrintStartOrder'");

    }

    public function down()
    {
        $this->dropColumn('store_order', 'can_change_ps');
        $this->truncateTable('store_order_offer');
        $this->dropTable('store_order_offer');
    }

}
