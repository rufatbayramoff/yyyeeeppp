<?php

use yii\db\Migration;

/**
 * Class m210219_133455_update_client_new_order_template
 */
class m210219_133455_8273_update_client_new_order_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update(
            'email_template',
            [
                'template_html' => new \yii\db\Expression("REPLACE(template_html,'https://treatstock.com/workbench/order/view/%orderId%','%link%')")
            ],
            [
                'code' => 'clientNewOrder'
            ]
        );

        $this->update(
            'email_template',
            [
                'description' => 'Params %orderId%, %link%, %clientName%, %htmlOrderList% - order body'
            ],
            [
                'code' => 'clientNewOrder'
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210219_133455_update_client_new_order_template cannot be reverted.\n";

        return false;
    }
}
