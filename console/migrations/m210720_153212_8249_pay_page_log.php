<?php

use yii\db\Migration;

/**
 * Class m210720_153212_8249_pay_page_log
 */
class m210720_153212_8249_pay_page_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `payment_pay_page_log_process` DROP PRIMARY KEY;");
        $this->execute("ALTER TABLE `payment_pay_page_log_process` ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);");
        $this->execute("ALTER TABLE `payment_pay_page_log_process` ADD INDEX(`uuid`);");
        $this->execute("ALTER TABLE `payment_pay_page_log_process` ADD `date` DATETIME NULL AFTER `uuid`;");
    }
}