<?php

use yii\db\Migration;

class m170526_080457_4303_order_comments extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `store_order` ADD COLUMN `comment` TEXT NULL DEFAULT NULL AFTER `confirm_hash`;');
    }

    public function down()
    {
        $this->dropColumn('store_order', 'comment');
    }
}
