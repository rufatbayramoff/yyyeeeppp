<?php

use yii\db\Migration;

class m170726_144421_4225_cnc_material_is_active extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cnc_material', 'code', 'varchar(255) not null default "" after id');
        $this->addColumn('cnc_material', 'is_active', 'int(1) not null default 1');
    }

    public function safeDown()
    {
        $this->dropColumn('cnc_material', 'code');
        $this->dropColumn('cnc_material', 'is_active');
    }

}
