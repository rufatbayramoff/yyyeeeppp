<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200403_172221_7222_force_invalid_address
 */
class m200403_172221_7222_force_invalid_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_address', 'is_forced_invalid', "tinyint(1) NOT NULL DEFAULT '0' after zip_code");
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropColumn('user_address', 'is_forced_invalid');
    }
}