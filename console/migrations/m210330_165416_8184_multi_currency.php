<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m210330_165416_8184_multi_currency
 */
class m210330_165416_8184_multi_currency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `ps` ADD INDEX(`currency`)');
        $this->execute('ALTER TABLE `ps` ADD CONSTRAINT `fk_ps_currency` FOREIGN KEY (`currency`) REFERENCES `payment_currency`(`currency_iso`) ON DELETE RESTRICT ON UPDATE RESTRICT');

        $this->execute('ALTER TABLE `payment_invoice` ADD INDEX(`currency`)');
        $this->execute('ALTER TABLE `payment_invoice` ADD CONSTRAINT `fk_payment_invoice_currency` FOREIGN KEY (`currency`) REFERENCES `payment_currency`(`currency_iso`) ON DELETE RESTRICT ON UPDATE RESTRICT');
        $this->execute("ALTER TABLE `payment_account` ADD `currency` CHAR(3) NOT NULL DEFAULT 'USD' AFTER `type`, ADD INDEX (`currency`)");
        $this->execute('ALTER TABLE `payment_account` ADD CONSTRAINT `fk_payment_account_currency` FOREIGN KEY (`currency`) REFERENCES `payment_currency`(`currency_iso`) ON DELETE RESTRICT ON UPDATE RESTRICT');

        $this->execute('ALTER TABLE `payment_account` DROP INDEX `uniq_account`, ADD UNIQUE `uniq_account` (`user_id`, `type`, `currency`) USING BTREE');

        $this->batchInsert('payment_account', ['id', 'user_id', 'type', 'currency'], [
            [101, 1, 'main', 'EUR'], // ACCOUNT_SUPPORT_EUR
            [103, 1, 'reserved', 'EUR'],
            [104, 1, 'award', 'EUR'],
            [105, 1, 'invoice', 'EUR'],
            [106, 1, 'authorize', 'EUR'],
            [210, 110, 'main', 'EUR'], // ACCOUNT_TREATSTOCK_EUR
            [220, 120, 'main', 'EUR'], // ACCOUNT_EASYPOST_EUR
            [260, 160, 'main', 'EUR'], // ACCOUNT_BRAINTREE_EUR
            [265, 165, 'main', 'EUR'], // ACCOUNT_STRIPE_EUR
            [261, 161, 'main', 'EUR'], // ACCOUNT_BANK_TRANSFER_EUR
            [298, 998, 'main', 'EUR'], // ACCOUNT_ANONIM_EUR

            [215, 110, 'discount', 'EUR'], // ACCOUNT_TREATSTOCK_DISCOUNT_EUR
            [216, 110, 'easypost', 'EUR'], // ACCOUNT_TREATSTOCK_EASYPOST_EUR
            [217, 110, 'correction', 'EUR'], // ACCOUNT_TREATSTOCK_CORRECTION_EUR

            [211, 111, 'main', 'EUR'],  // ACCOUNT_CURRENCY_EUR
        ]);
        $this->execute('ALTER TABLE `payment_detail` DROP FOREIGN KEY `fk_payment_detail_4`; ALTER TABLE `payment_detail` ADD CONSTRAINT `fk_payment_detail_4` FOREIGN KEY (`payment_account_id`) REFERENCES `payment_account`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->update('payment_account', ['id' => '117'], ['type' => 'correction', 'user_id' => '110', 'currency' => 'USD']);
        $this->execute('ALTER TABLE `payment_detail` DROP FOREIGN KEY `fk_payment_detail_4`; ALTER TABLE `payment_detail` ADD CONSTRAINT `fk_payment_detail_4` FOREIGN KEY (`payment_account_id`) REFERENCES `payment_account`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;');

        $this->update('user', ['auth_key' => '-', 'password_hash' => '-'], 'id=111');
        $this->update('payment_currency', ['is_active' => 1], "currency_iso='EUR'");

        $this->dropColumn('ps_printer', 'currency_iso');
        $this->dropColumn('ps_printer_color', 'price_currency_iso');
        $this->dropColumn('ps_printer_color', 'price_usd');
        $this->dropColumn('ps_printer_color', 'price_usd_updated');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
