<?php

use yii\db\Migration;

/**
 * Class m210330_093016_7097_quote_order_attempt
 */
class m210330_093016_7097_quote_order_attempt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('store_order_attemp', ['status'=>'quote'], "preorder_id is not null and order_id is null and status='new'");
        $this->execute('ALTER TABLE `store_order_attemp` DROP FOREIGN KEY `fk_store_order_attemp_preorder_id`; ALTER TABLE `store_order_attemp` ADD CONSTRAINT `fk_store_order_attemp_preorder_id` FOREIGN KEY (`preorder_id`) REFERENCES `preorder`(`id`) ON DELETE CASCADE ON UPDATE CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
