<?php

use yii\db\Schema;
use yii\db\Migration;

class m150630_062808_geo_vendor_result_mode extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('{{%geo_location_vendor_results}}', 'using_map', "ENUM('address','html5','drag','geocode','') NOT NULL");
    }

    public function safeDown()
    {
        $this->alterColumn('{{%geo_location_vendor_results}}', 'using_map', "ENUM('address','html5','drag','') NOT NULL");
    }
}
