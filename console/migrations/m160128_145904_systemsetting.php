<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_145904_systemsetting extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE  `system_setting`  CHANGE COLUMN `description` `description` VARCHAR(1500) NULL DEFAULT NULL ;");
    }

    public function down()
    {
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
