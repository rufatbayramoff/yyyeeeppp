<?php

use yii\db\Migration;

class m170628_163621_4426_alter_user_session_sha256 extends Migration
{
    public function up()
    {
        $this->dropIndex('user_session_uuid', 'user_session'); // It`s not unique now. Login user, logout don`t destroy session, only clear it.
        $this->alterColumn('user_session', 'uuid', 'char(65) not null');
        $this->addColumn('user_session', 'created_at', 'timestamp not NULL DEFAULT CURRENT_TIMESTAMP after id ');
    }

    public function down()
    {
        $this->dropColumn('user_session', 'created_at');
        $this->createIndex('user_session_uuid', 'user_session', 'uuid');
    }

}
