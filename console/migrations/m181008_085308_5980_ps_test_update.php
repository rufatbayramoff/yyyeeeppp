<?php

use yii\db\Migration;

/**
 * Class m181008_085308_5980_ps_test_update
 */
class m181008_085308_5980_ps_test_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `ps_printer_test` 
ADD COLUMN `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `decision_at`;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('ps_printer_test', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181008_085308_5980_ps_test_update cannot be reverted.\n";

        return false;
    }
    */
}
