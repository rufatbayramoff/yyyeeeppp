<?php

use yii\db\Migration;
use yii\db\Query;

class m210210_171647_8242_redis_ar extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('file_download_hash');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
