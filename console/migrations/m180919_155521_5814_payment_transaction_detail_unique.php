<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180919_155521_5814_payment_transaction_detail_unique
 */
class m180919_155521_5814_payment_transaction_detail_unique extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `payment_transaction` DROP INDEX `payment_transaction_first_detail_indx`, ADD UNIQUE `payment_transaction_first_detail_indx` (`first_payment_detail_id`) USING BTREE;");
        $this->execute("ALTER TABLE `payment_detail`DROP FOREIGN KEY `fk_payment_detail_4`;
ALTER TABLE `payment_detail` DROP COLUMN `transaction_id`, DROP INDEX `fk_payment_detail_4_idx` ;");
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
