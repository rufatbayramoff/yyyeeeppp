<?php

use yii\db\Migration;

class m160706_111830_2003_material_add_recommend_price extends Migration
{
    public function up()
    {
        $this->addColumn('printer_material', 'recomend_price_per_gramm_min', 'double(15,6) null');
        $this->addColumn('printer_material', 'recomend_price_per_gramm_max', 'double(15,6) null');
    }

    public function down()
    {
        $this->dropColumn('printer_material', 'recomend_price_per_gramm_min');
        $this->dropColumn('printer_material', 'recomend_price_per_gramm_max');
        return true;
    }
}
