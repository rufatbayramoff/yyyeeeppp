<?php

use yii\db\Migration;

class m170724_075120_4544_autofill_tpls extends Migration
{
    public function safeUp()
    {

        $rows = [
            [
                'type' => 'pscatalog',
                'template_name' => 'location',
                'title' => '%found% print services found in %location%',
                'meta_description' => '3D printing services are available for %location%. Select suitable technology, material and application to make a more precise filtering.',
                'meta_keywords' => '%location%, 3d printing',
                'header' => '%found% print services found in %location%',
                'lang_iso' => 'en-US',
                'is_default' => false,
                'is_apply_created' => false,
                'header_text' => 'We found for you %found% 3D printing services that are available for %location%. Select suitable technology, material and application to make a more precise filtering. Upload 3D model to get instant quote.',
                'footer_text' => ''
            ],
            [
                'type' => 'pscatalog',
                'template_name' => 'location-technology',
                'title' => '%found% print services found in %location%',
                'meta_description' => '3D printing services are available for %location%. Select suitable technology, material and application to make a more precise filtering.',
                'meta_keywords' => '%technology%, %location%, 3d printing',
                'header' => '%found% print services found in %location%',
                'lang_iso' => 'en-US',
                'is_default' => false,
                'is_apply_created' => false,
                'header_text' => 'We found for you %found% 3D printing services that are available for %location%. Select suitable technology, material and application to make a more precise filtering. Upload 3D model to get instant quote.',
                'footer_text' => ''
            ],
            [
                'type' => 'pscatalog',
                'template_name' => 'location-material',
                'title' => '%found% print services found in %location%',
                'meta_description' => '3D printing services are available for %location%. Select suitable technology, material and application to make a more precise filtering.',
                'meta_keywords' => '%material%, %location%, 3d printing',
                'header' => '%found% print services found in %location%',
                'lang_iso' => 'en-US',
                'is_default' => false,
                'is_apply_created' => false,
                'header_text' => 'We found for you %found% 3D printing services that are available for %location%. Select suitable technology, material and application to make a more precise filtering. Upload 3D model to get instant quote.',
                'footer_text' => ''
            ],

            [
                'type' => 'pscatalog',
                'template_name' => 'location-usage',
                'title' => '%found% print services found in %location%',
                'meta_description' => '3D printing services are available for %location%. %usage.',
                'meta_keywords' => '%productApplication%, %location%, 3d printing',
                'header' => '%found% print services found in %location%',
                'lang_iso' => 'en-US',
                'is_default' => false,
                'is_apply_created' => false,
                'header_text' => 'We found for you %found% 3D printing services that are available for %location%. Select suitable technology, material and application to make a more precise filtering. Upload 3D model to get instant quote.',
                'footer_text' => ''
            ],

            [
                'type' => 'pscatalog',
                'template_name' => 'location-technology-usage',
                'title' => '%found% print services found in %location%',
                'meta_description' => '%technology% 3D printing services are available for %location%. ',
                'meta_keywords' => '%productApplication%, %location%, 3d printing, %technology%',
                'header' => '%found% print services found in %location%',
                'lang_iso' => 'en-US',
                'is_default' => false,
                'is_apply_created' => false,
                'header_text' => 'We found for you %found% 3D printing services that are available for %location%. Select suitable technology, material and application to make a more precise filtering. Upload 3D model to get instant quote.',
                'footer_text' => ''
            ],

            [
                'type' => 'pscatalog',
                'template_name' => 'location-technology-material',
                'title' => '%found% print services found in %location%',
                'meta_description' => '%technology% 3D printing services are available for %location%. ',
                'meta_keywords' => '%technology%, %location%, 3d printing, %material%',
                'header' => '%found% print services found in %location%',
                'lang_iso' => 'en-US',
                'is_default' => false,
                'is_apply_created' => false,
                'header_text' => 'We found for you %found% 3D printing services that are available for %location%. Select suitable technology, material and application to make a more precise filtering. Upload 3D model to get instant quote.',
                'footer_text' => ''
            ],


            [
                'type' => 'pscatalog',
                'template_name' => 'location-material-usage',
                'title' => '%found% print services found in %location%',
                'meta_description' => '%productApplication% 3D printing services are available for %location%. %material%. ',
                'meta_keywords' => '%productApplication%, %location%, 3d printing, %material%',
                'header' => '%found% print services found in %location%',
                'lang_iso' => 'en-US',
                'is_default' => false,
                'is_apply_created' => false,
                'header_text' => 'We found for you %found% 3D printing services that are available for %location%. Select suitable technology, material and application to make a more precise filtering. Upload 3D model to get instant quote.',
                'footer_text' => ''
            ],

            [
                'type' => 'pscatalog',
                'template_name' => 'location-technology-material-usage',
                'title' => '%found% print services found in %location%',
                'meta_description' => '%productApplication% 3D printing services are available for %location%. %material%. ',
                'meta_keywords' => '%productApplication%, %location%, %technology% 3d printing, %material%',
                'header' => '%found% %technology% print services found in %location%',
                'lang_iso' => 'en-US',
                'is_default' => false,
                'is_apply_created' => false,
                'header_text' => 'We found for you %found% 3D printing services that are available for %location%. Select suitable technology, material and application to make a more precise filtering. Upload 3D model to get instant quote.',
                'footer_text' => ''
            ],
        ];

        foreach($rows as $row){
            $this->insert('seo_page_autofill_template', $row);
        }

        # id, group_id, key, value, created_at, updated_at, created_user_id, updated_user_id, description, json, is_active
        $this->insert('system_setting',[
            'group_id' => 13,
            'key' => 'catalogFooterLinks',
            'value' => 'json',
            'created_at' =>  '2017-07-24 10:19:23',
            'updated_at' => '2017-07-24 10:19:23',
            'created_user_id' => 1,
            'updated_user_id' => 1,
            'description' => 'catalog footer links',
            'json' => '{"materials":["PLA","ABS","Nylon","Rubber (TPU)","Resin"],"technologies":["FDM","DLP","SLA"]}',
            'is_active' => 1
        ]);

        $this->execute('CREATE TABLE `geo_top_city` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `city_id` int(11) NOT NULL,
              `country_id` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `city_id_UNIQUE` (`city_id`),
              KEY `fk_geo_top_city_1_idx` (`city_id`),
              KEY `fk_geo_top_city_2_idx` (`country_id`),
              CONSTRAINT `fk_geo_top_city_1` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_geo_top_city_2` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
        ');
        try{
            $this->execute('ALTER TABLE `printer_technology` ADD COLUMN  `code` VARCHAR(5) NULL AFTER `print_speed_max`;');
        }catch(\Exception $e){}
    }

    public function safeDown()
    {
        $this->truncateTable('geo_top_city');
        $this->dropTable('geo_top_city');
        $this->delete('seo_page_autofill_template', ['type'=>'pscatalog']);
        $this->delete('system_setting', ['key'=>'catalogFooterLinks']);
    }

}
