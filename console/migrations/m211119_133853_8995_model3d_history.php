<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m211119_133853_8995_model3d_history
 */
class m211119_133853_8995_model3d_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute('ALTER TABLE `model3d_history` CHANGE `comment` `comment` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
