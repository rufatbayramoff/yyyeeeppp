<?php

use yii\db\Migration;

/**
 * Class m180802_141404_create_table_home_page_category_cards
 */
class m180802_141404_create_table_home_page_category_cards extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('home_page_category_card', [
            'id'                  => $this->primaryKey(),
            'created_at'          => $this->dateTime()->notNull(),
            'is_active'           => $this->tinyInteger(1)->defaultValue(1)->notNull(),
            'product_category_id' => $this->integer()->null(),
            'title'               => $this->string(255)->null(),
            'url'                 => $this->string(255)->null(),
            'cover_id'            => $this->integer()->notNull(),
            'position'            => $this->integer()->notNull()->defaultValue(1)
        ]);

        $this->createIndex('index_hpcc_product_category_id', 'home_page_category_card', 'product_category_id');
        $this->addForeignKey('fk_hpcc_product_category_id', 'home_page_category_card', 'product_category_id', 'product_category', 'id', 'CASCADE', 'CASCADE');

        $this->execute("
          CREATE TABLE `home_page_category_card_intl` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `model_id` int(11) NOT NULL,
          `title` varchar(255) NOT NULL,
          `lang_iso` char(5) NOT NULL DEFAULT 'en-US',
          `is_active` bit(1) DEFAULT b'1',
          PRIMARY KEY (`id`),
          UNIQUE KEY `hpcc_intl_unique_index` (`model_id`,`lang_iso`),
          KEY `hpcc_intl_search_index` (`model_id`,`lang_iso`,`is_active`),
          CONSTRAINT `hpcc_intl_fk_model_id` FOREIGN KEY (`model_id`) REFERENCES `home_page_category_card` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('home_page_category_card_intl');
        $this->dropTable('home_page_category_card');
    }
}
