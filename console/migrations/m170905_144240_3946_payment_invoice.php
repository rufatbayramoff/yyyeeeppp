<?php

use yii\db\Migration;

class m170905_144240_3946_payment_invoice extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `payment_invoice` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `invoice_id` varchar(45) NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `date_due` date NOT NULL,
              `currency` char(3) NOT NULL DEFAULT \'USD\',
              `total_amount` decimal(8,2) NOT NULL DEFAULT \'0.00\',
              `status` char(15) NOT NULL DEFAULT \'new\',
              `user_id` int(11) DEFAULT NULL,
              `ship_to` varchar(245) DEFAULT NULL,
              `bill_to` varchar(245) DEFAULT NULL,
              `bank_details` varchar(245) DEFAULT NULL,
              `order_id` int(11) DEFAULT NULL,
              `payment_details` varchar(245) DEFAULT NULL,
              `transaction_id` int(11) DEFAULT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `invoice_id_UNIQUE` (`invoice_id`),
              KEY `fk_payment_invoice_1_idx` (`user_id`),
              KEY `fk_payment_invoice_2_idx` (`order_id`),
              KEY `fk_payment_invoice_3_idx` (`transaction_id`),
              CONSTRAINT `fk_payment_invoice_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_payment_invoice_2` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_payment_invoice_3` FOREIGN KEY (`transaction_id`) REFERENCES `payment_transaction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        );');

        $this->execute('CREATE TABLE `payment_invoice_item` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `invoice_id` int(11) DEFAULT NULL,
              `title` varchar(45) DEFAULT NULL,
              `description` varchar(245) DEFAULT NULL,
              `measure` char(15) DEFAULT \'QUANTITY\',
              `qty` int(3) NOT NULL DEFAULT \'1\',
              `unit_price` decimal(8,2) DEFAULT NULL,
              `tax` decimal(8,2) DEFAULT NULL,
              `discount` decimal(8,2) DEFAULT NULL,
              `currency` char(3) NOT NULL DEFAULT \'USD\',
              `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `item_type` varchar(15) NOT NULL DEFAULT \'position\',
              PRIMARY KEY (`id`),
              KEY `fk_invoice` (`invoice_id`),
              CONSTRAINT `fk_invoice` FOREIGN KEY (`invoice_id`) REFERENCES `payment_invoice` (`id`)
            );
        ');
    }

    public function safeDown()
    {
        $this->truncateTable('payment_invoice_item');
        $this->truncateTable('payment_invoice');
        $this->dropTable('payment_invoice_item');
        $this->dropTable('payment_invoice');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170905_144240_3946_payment_invoice cannot be reverted.\n";

        return false;
    }
    */
}
