<?php

use yii\db\Migration;

class m171114_093921_5014_compress_files extends Migration
{
    public function safeUp()
    {
        $this->execute(
            '
CREATE TABLE `file_compressed` (
  `file_id` INT(11) NOT NULL,
  `date` TIMESTAMP NOT NULL,
  `unpacked` TINYINT(1) UNSIGNED NOT NULL,
  UNIQUE KEY `file_id` (`file_id`),
  CONSTRAINT `fk_file_compressed_file_id` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'
        );
    }

    public function safeDown()
    {
        $this->dropTable('file_compressed');
    }
}
