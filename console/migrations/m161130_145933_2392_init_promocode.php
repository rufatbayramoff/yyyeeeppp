<?php

use yii\db\Migration;

class m161130_145933_2392_init_promocode extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `promocode` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `code` varchar(15) NOT NULL,
              `usage_type` varchar(15) NOT NULL,
              `discount_type` varchar(15) NOT NULL,
              `discount_amount` int(11) NOT NULL,
              `discount_currency` char(5) NOT NULL,
              `discount_for` varchar(45) NOT NULL,
              `valid_from` date NOT NULL,
              `valid_to` date NOT NULL,
              `description` varchar(45) DEFAULT NULL,
              `is_active` bit(1) NOT NULL,
              `is_valid` bit(1) NOT NULL DEFAULT b\'1\',
              PRIMARY KEY (`id`),
              UNIQUE KEY `code_UNIQUE` (`code`),
              KEY `index3` (`is_valid`),
              KEY `index4` (`is_active`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        $this->execute('CREATE TABLE `promocode_history` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `promocode_id` int(11) NOT NULL,
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `action_id` varchar(45) DEFAULT NULL,
              `comment` text,
              `user_id` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_promocode_history_1_idx` (`user_id`),
              KEY `fk_promocode_history_2_idx` (`promocode_id`),
              CONSTRAINT `fk_promocode_history_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_promocode_history_2` FOREIGN KEY (`promocode_id`) REFERENCES `promocode` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute('CREATE TABLE IF NOT EXISTS `promocode_usage` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `user_id` int(11) NOT NULL,
          `promocode_id` int(11) NOT NULL,
          `used_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          `order_id` int(11) NOT NULL,
          `amount` decimal(10,2) NOT NULL DEFAULT \'0.00\',
          `amount_currency` char(5) NOT NULL DEFAULT \'USD\',
          PRIMARY KEY (`id`),
          KEY `fk_promocode_usage_1_idx` (`user_id`),
          KEY `fk_promocode_usage_2_idx` (`promocode_id`),
          KEY `fk_promocode_usage_3_idx` (`order_id`),
          CONSTRAINT `fk_promocode_usage_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
          CONSTRAINT `fk_promocode_usage_2` FOREIGN KEY (`promocode_id`) REFERENCES `promocode` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
          CONSTRAINT `fk_promocode_usage_3` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute('CREATE TABLE `store_order_promocode` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `order_id` int(11) NOT NULL,
              `promocode_id` int(11) NOT NULL,
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `amount` decimal(10,4) DEFAULT NULL,
              `amount_currency` char(5) DEFAULT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `index4` (`order_id`,`promocode_id`),
              KEY `fk_store_order_promocode_1_idx` (`order_id`),
              KEY `fk_store_order_promocode_2_idx` (`promocode_id`),
              CONSTRAINT `fk_store_order_promocode_1` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_store_order_promocode_2` FOREIGN KEY (`promocode_id`) REFERENCES `promocode` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
        );

        /*
        $this->insert('promocode', [
            'code'              => '3DPRINTME1',
            'usage_type'        => 'one_user',
            'discount_type'     => 'percent',
            'discount_amount'   => '15',
            'discount_currency' => 'USD',
            'discount_for'      => 'print',
            'valid_from'        => '2017-01-01',
            'valid_to'          => '2017-05-01',
            'description'       => '3d print promo code',
            'is_active'         => '1',
            'is_valid'          => '1'
        ]); */
    }

    public function down()
    {
        $this->truncateTable('promocode_usage');
        $this->truncateTable('promocode_history');
        $this->dropTable('store_order_promocode');
        $this->dropTable('promocode_usage');
        $this->dropTable('promocode_history');
        $this->dropTable('promocode');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
