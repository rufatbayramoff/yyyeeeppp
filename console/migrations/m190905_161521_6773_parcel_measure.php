<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m190905_161521_6773_parcel_measure
 */
class m190905_161521_6773_parcel_measure extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_delivery', 'calculated_parcel_measure', 'varchar(3)');
        $this->update('store_order_delivery', ['calculated_parcel_measure' => 'in']);
    }

    public function safeDown()
    {
        $this->dropColumn('store_order_delivery', 'calculated_parcel_measure');
    }
}