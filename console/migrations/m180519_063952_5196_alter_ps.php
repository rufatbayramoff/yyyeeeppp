<?php

use yii\db\Migration;

/**
 * Class m180519_063952_5196_alter_ps
 */
class m180519_063952_5196_alter_ps extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `ps`  ADD COLUMN `is_backend` TINYINT NOT NULL DEFAULT 0 AFTER `annual_turnover`;');
        $this->execute(
            "ALTER TABLE `ps` 
          CHANGE COLUMN `moderator_status` `moderator_status` ENUM('new', 'draft', 'updated', 'checking', 'checked', 'banned', 'rejected') NOT NULL ;
        "
        );
        $this->execute(
            'CREATE TABLE `company_history` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `company_id` int(11) NOT NULL,
          `user_id` int(11) DEFAULT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `action_id` varchar(45) NOT NULL,
          `comment` text,
          `data` text,
          PRIMARY KEY (`id`),
          KEY `fk_ps_id_history` (`company_id`),
          KEY `fk_ps_id_user_id` (`user_id`),
          CONSTRAINT `fk_ps_history_1` FOREIGN KEY (`company_id`) REFERENCES `ps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
          CONSTRAINT `fk_ps_history_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        '
        );

        $this->execute(
            'ALTER TABLE `ps` 
            ADD COLUMN `logo_file_id` INT NULL AFTER `is_backend`,
            ADD INDEX `fk_ps_2_idx` (`logo_file_id` ASC);
            ALTER TABLE `ps` 
            ADD CONSTRAINT `fk_ps_2`
              FOREIGN KEY (`logo_file_id`)
              REFERENCES `file` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        '
        );

        $this->execute(
            'CREATE TABLE `company_verify` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `company_id` int(11) DEFAULT NULL,
              `verify_method` varchar(45) NOT NULL DEFAULT \'email\',
              `created_at` datetime NOT NULL,
              `verified_at` datetime DEFAULT NULL,
              `code` varchar(45) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `code_UNIQUE` (`code`,`company_id`),
              KEY `fk_company_verify_1_idx` (`company_id`),
              CONSTRAINT `fk_company_verify_1` FOREIGN KEY (`company_id`) REFERENCES `ps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
        );
        /*
        $this->execute('CREATE TABLE `company_address` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `user_id` int(11) NOT NULL,
          `country_id` int(11) NOT NULL,
          `region_id` int(11) DEFAULT NULL,
          `city_id` int(11) NOT NULL,
          
          `region_string` varchar(245) DEFAULT NULL,
          `city_string` varchar(245) DEFAULT NULL,
          
          `address` varchar(245) DEFAULT NULL,
          `address2` varchar(245) DEFAULT NULL,
          `lat` decimal(8,5) NOT NULL,
          `lon` decimal(8,5) NOT NULL,
          `zip_code` varchar(45) DEFAULT NULL,
          `location_type` varchar(45) DEFAULT NULL
          PRIMARY KEY (`id`),
          KEY `fk_gl1_idx` (`country_id`),
          KEY `fk_gl2_idx` (`city_id`),
          KEY `fk_geo_location_1_idx` (`region_id`),
          CONSTRAINT `fk_geo_city` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
          CONSTRAINT `fk_geo_country` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
          CONSTRAINT `fk_user_loc` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        '); */
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('ps', 'is_backend');
        $this->truncateTable('company_history');
        $this->dropTable('company_history');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180519_063952_5196_alter_ps cannot be reverted.\n";

        return false;
    }
    */
}
