<?php

use yii\db\Migration;

class m170517_091029_4240_fix_unsigned_id_columns extends Migration
{
    public function up()
    {

     //   throw new Exception();


        $this->execute("
            ALTER TABLE `user_history` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
        ");

        $this->execute("
            ALTER TABLE `user_email_change_request` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
        ");

        $this->execute("
            ALTER TABLE `notify_message` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
        ");

        $this->execute("
            ALTER TABLE `store_order_attempt_moderation_file` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL;
        ");


        $this->attemps();

        $this->messages();
    }



    public function attemps()
    {
        $this->execute("ALTER TABLE `store_order` DROP FOREIGN KEY `fk_store_order_5`;");
        $this->execute("ALTER TABLE `store_order` DROP FOREIGN KEY `fk_store_order_6`;");
        $this->execute("ALTER TABLE `store_order_attemp` DROP FOREIGN KEY `fk_store_order_attemp_3`;");
        $this->execute("ALTER TABLE `store_order_attemp_delivery` DROP FOREIGN KEY `fk_store_order_attemp_delivery_2`;");
        $this->execute("ALTER TABLE `store_order_attempt_moderation` DROP FOREIGN KEY `fk_store_order_attempt_moderation_1`;");
        $this->execute("ALTER TABLE `store_order_attempt_moderation_file` DROP FOREIGN KEY `fk_store_order_attempt_moderation_file_2`;");

        $this->execute("
            ALTER TABLE `store_order` 
            CHANGE COLUMN `current_attemp_id` `current_attemp_id` INT(11) NULL DEFAULT NULL ,
            CHANGE COLUMN `previous_attemp_id` `previous_attemp_id` INT(11) NULL DEFAULT NULL ;
        ");



        $this->execute("
            ALTER TABLE `store_order_attemp` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT,
            CHANGE COLUMN `dates_id` `dates_id` INT(11) NULL DEFAULT NULL ;
        ");


        $this->execute("
            ALTER TABLE `store_order_attemp_dates` 
            CHANGE COLUMN `attemp_id` `attemp_id` INT(11) NOT NULL ;
        ");


        $this->execute("
            ALTER TABLE `store_order_attemp_delivery` 
            CHANGE COLUMN `order_attemp_id` `order_attemp_id` INT(11) NOT NULL ;
        ");


        $this->execute("
            ALTER TABLE `store_order_attempt_moderation` 
            CHANGE COLUMN `attempt_id` `attempt_id` INT(11) NOT NULL ;
        ");


        $this->execute("
            ALTER TABLE `store_order_attempt_moderation_file` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT,
            CHANGE COLUMN `attempt_id` `attempt_id` INT(11) NOT NULL ;
        ");


        $this->execute("
            ALTER TABLE `store_order`
            ADD CONSTRAINT `fk_store_order_5` FOREIGN KEY ( `current_attemp_id` )
            REFERENCES `store_order_attemp`( `id` )
            ON DELETE No Action
            ON UPDATE No Action;
        ");


        $this->execute("
            ALTER TABLE `store_order`
            ADD CONSTRAINT `fk_store_order_6` FOREIGN KEY ( `previous_attemp_id` )
            REFERENCES `store_order_attemp`( `id` )
            ON DELETE No Action
            ON UPDATE No Action;
        ");


        $this->execute("
            ALTER TABLE `store_order_attemp`
            ADD CONSTRAINT `fk_store_order_attemp_3` FOREIGN KEY ( `dates_id` )
            REFERENCES `store_order_attemp_dates`( `attemp_id` )
            ON DELETE No Action
            ON UPDATE No Action;
        ");


        $this->execute("
            ALTER TABLE `store_order_attemp_delivery`
            ADD CONSTRAINT `fk_store_order_attemp_delivery_2` FOREIGN KEY ( `order_attemp_id` )
            REFERENCES `store_order_attemp`( `id` )
            ON DELETE No Action
            ON UPDATE No Action;
        ");

        $this->execute("
            ALTER TABLE `store_order_attempt_moderation`
            ADD CONSTRAINT `fk_store_order_attempt_moderation_1` FOREIGN KEY ( `attempt_id` )
            REFERENCES `store_order_attemp`( `id` )
            ON DELETE No Action
            ON UPDATE No Action;
        ");


        $this->execute("ALTER TABLE `store_order_attempt_moderation_file`
            ADD CONSTRAINT `fk_store_order_attempt_moderation_file_2` FOREIGN KEY ( `attempt_id` )
            REFERENCES `store_order_attempt_moderation`( `attempt_id` )
            ON DELETE No Action
            ON UPDATE No Action;
	  ");
    }



    private function messages()
    {

        $this->execute("ALTER TABLE `msg_file` DROP FOREIGN KEY `fk_msg_file_message`;");
        $this->execute("ALTER TABLE `msg_file` DROP FOREIGN KEY `fk_msg_file_topic`;");
        try {
            $this->execute("ALTER TABLE `msg_folder_intl` DROP FOREIGN KEY `fk_msg_folder_intl_1`;");
        } catch (Exception $e) {

        }
        $this->execute("ALTER TABLE `msg_member` DROP FOREIGN KEY `msg_member_ibfk_2`;");
        $this->execute("ALTER TABLE `msg_member` DROP FOREIGN KEY `msg_member_ibfk_3`;");
        $this->execute("ALTER TABLE `msg_member` DROP FOREIGN KEY `msg_member_ibfk_4`;");
        $this->execute("ALTER TABLE `msg_message` DROP FOREIGN KEY `msg_message_ibfk_2`;");
        $this->execute("ALTER TABLE `msg_report` DROP FOREIGN KEY `lnk_msg_report_msg_message`;");
        $this->execute("ALTER TABLE `content_filter_checked_messages` DROP FOREIGN KEY `fk_content_checked_message_id`;");
        $this->execute("ALTER TABLE `content_filter_checked_topics` DROP FOREIGN KEY `fk_content_checked_topic_id`;");

        $this->execute("
            ALTER TABLE `msg_file` 
            CHANGE COLUMN `message_id` `message_id` INT(11) NOT NULL ,
            CHANGE COLUMN `topic_id` `topic_id` INT(11) NOT NULL ;
        ");

        $this->execute("
            ALTER TABLE `msg_folder` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
        ");

        $this->execute("
            ALTER TABLE `msg_folder_intl` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
            CHANGE COLUMN `model_id` `model_id` INT(11) NOT NULL ;
        ");

        $this->execute("
            ALTER TABLE `msg_member` 
            CHANGE COLUMN `topic_id` `topic_id` INT(11) NOT NULL ,
            CHANGE COLUMN `folder_id` `folder_id` INT(11) NOT NULL ,
            CHANGE COLUMN `original_folder` `original_folder` INT(11) NOT NULL ;
        ");

        $this->execute("
            ALTER TABLE `msg_message` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
            CHANGE COLUMN `topic_id` `topic_id` INT(11) NOT NULL ;
        ");

        $this->execute("
            ALTER TABLE `msg_report` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
            CHANGE COLUMN `message_id` `message_id` INT(11) NOT NULL ;
        ");

        $this->execute("
            ALTER TABLE `msg_topic` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
        ");

        $this->execute("
            ALTER TABLE `content_filter_checked_messages` 
            CHANGE COLUMN `message_id` `message_id` INT(11) NOT NULL ;
        ");

        $this->execute("
            ALTER TABLE `content_filter_checked_topics` 
            CHANGE COLUMN `topic_id` `topic_id` INT(11) NOT NULL ;
        ");



        $this->execute("
            ALTER TABLE `msg_file`
            ADD CONSTRAINT `fk_msg_file_message` FOREIGN KEY ( `message_id` )
            REFERENCES `msg_message`( `id` )
            ON DELETE No Action
            ON UPDATE No Action;
	    ");

        $this->execute("
            ALTER TABLE `msg_file`
            ADD CONSTRAINT `fk_msg_file_topic` FOREIGN KEY ( `topic_id` )
            REFERENCES `msg_topic`( `id` )
            ON DELETE No Action
            ON UPDATE No Action;
	    ");


        $this->execute("ALTER TABLE `msg_member`
            ADD CONSTRAINT `msg_member_ibfk_2` FOREIGN KEY ( `topic_id` )
            REFERENCES `msg_topic`( `id` )
            ON DELETE Restrict
            ON UPDATE Restrict;
	   ");


        $this->execute("
            ALTER TABLE `msg_member`
            ADD CONSTRAINT `msg_member_ibfk_3` FOREIGN KEY ( `folder_id` )
            REFERENCES `msg_folder`( `id` )
            ON DELETE Restrict
            ON UPDATE Restrict;
	    ");

        $this->execute("
            ALTER TABLE `msg_member`
            ADD CONSTRAINT `msg_member_ibfk_4` FOREIGN KEY ( `original_folder` )
            REFERENCES `msg_folder`( `id` )
            ON DELETE Restrict
            ON UPDATE Restrict;
        ");

        $this->execute("
            ALTER TABLE `msg_message`
            ADD CONSTRAINT `msg_message_ibfk_2` FOREIGN KEY ( `topic_id` )
            REFERENCES `msg_topic`( `id` )
            ON DELETE Restrict
            ON UPDATE Restrict;
        ");
        $this->execute("
            ALTER TABLE `msg_report`
            ADD CONSTRAINT `lnk_msg_report_msg_message` FOREIGN KEY ( `message_id` )
            REFERENCES `msg_message`( `id` )
            ON DELETE No Action
            ON UPDATE No Action;
        ");

        $this->execute("
            ALTER TABLE `content_filter_checked_messages`
            ADD CONSTRAINT `fk_content_checked_message_id` FOREIGN KEY ( `message_id` )
            REFERENCES `msg_message`( `id` )
            ON DELETE Restrict
            ON UPDATE Restrict;
        ");

        $this->execute("
            ALTER TABLE `content_filter_checked_topics`
            ADD CONSTRAINT `fk_content_checked_topic_id` FOREIGN KEY ( `topic_id` )
            REFERENCES `msg_topic`( `id` )
            ON DELETE Restrict
            ON UPDATE Restrict;
	    ");


        $this->execute("
            ALTER TABLE `msg_folder_intl`
            ADD CONSTRAINT `fk_msg_folder_intl_1` FOREIGN KEY ( `model_id` )
            REFERENCES `msg_folder`( `id` )
            ON DELETE No Action
            ON UPDATE No Action;
        ");



    }



















    public function down()
    {
        return true;
    }


}
