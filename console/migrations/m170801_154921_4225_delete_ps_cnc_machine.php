<?php

use yii\db\Migration;

class m170801_154921_4225_delete_ps_cnc_machine extends Migration
{
    public function safeUp()
    {
        $this->addColumn('ps_machine', 'is_deleted', 'TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT \'0\'');
        $this->addColumn('ps_machine', 'created_at', 'TIMESTAMP NULL DEFAULT NULL');
        $this->addColumn('ps_machine', 'updated_at', 'TIMESTAMP NULL DEFAULT NULL AFTER `created_at`');
        $this->addColumn('ps_machine', 'is_location_change', 'TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT \'0\'');
        $sql = <<<SQLSELECT
SELECT
  ps_printer.id AS id,
  ps_machine.id AS ps_machine_id,
  ps_printer.is_deleted AS is_deleted,
  ps_printer.created_at AS created_at,
  ps_printer.updated_at AS updated_at,
  ps_printer.is_location_change AS is_location_change
  FROM ps_printer LEFT JOIN ps_machine ON ps_machine.ps_printer_id=ps_printer.id
SQLSELECT;
        $printers = $this->db->createCommand($sql)->queryAll();
        foreach ($printers as $printer) {
            $this->update(
                'ps_machine',
                [
                    'is_deleted' => $printer['is_deleted'],
                    'created_at' => $printer['created_at'],
                    'updated_at' => $printer['updated_at'],
                    'is_location_change' => $printer['is_location_change']
                ],
                ['id' => $printer['ps_machine_id']]
            );
        }
        $this->dropColumn('ps_printer', 'is_deleted');
        $this->dropColumn('ps_printer', 'created_at');
        $this->dropColumn('ps_printer', 'updated_at');
        $this->dropColumn('ps_printer', 'is_location_change');
    }

    public function safeDown()
    {
        return false;
    }

}
