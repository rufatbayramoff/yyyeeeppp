<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180919_105521_5814_payment_transaction_detail_not_null
 *
 * Any payment transaction should have link to payment detail
 */
class m180919_105521_5814_payment_transaction_detail_not_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        #$this->execute("ALTER TABLE `payment_transaction` CHANGE `first_payment_detail_id` `first_payment_detail_id` INT(11) NOT NULL;");
        $this->execute("ALTER TABLE `payment_transaction` 
DROP FOREIGN KEY `payment_transaction_first_detail_fk`;
ALTER TABLE `payment_transaction` 
CHANGE COLUMN `first_payment_detail_id` `first_payment_detail_id` INT(11) NOT NULL ;
ALTER TABLE `payment_transaction` 
ADD CONSTRAINT `payment_transaction_first_detail_fk`
  FOREIGN KEY (`first_payment_detail_id`)
  REFERENCES `payment_detail` (`id`);
");
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
