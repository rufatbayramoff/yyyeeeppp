<?php

use yii\db\Migration;

class m170626_085040_3826_qty_fix extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `model3d_part` CHANGE COLUMN `qty` `qty` INT UNSIGNED NOT NULL DEFAULT 1;");
        $this->execute("ALTER TABLE `model3d_replica_part` CHANGE COLUMN `qty` `qty` INT UNSIGNED NOT NULL DEFAULT 1;");
    }

    public function down()
    {
        return true;
    }

}
