<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_082120_moderation1 extends Migration
{
    public function up()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $sql = "CREATE TABLE `community_message` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `from_user_id` int(11) NOT NULL,
            `to_user_id` int(11) NOT NULL,
            `type` char(15) NOT NULL DEFAULT 'message',
            `created_at` timestamp NULL DEFAULT NULL,
            `title` varchar(255) DEFAULT NULL,
            `text` mediumtext NOT NULL,
            `received_at` timestamp NULL DEFAULT NULL,
            `status` char(15) DEFAULT 'new',
            PRIMARY KEY (`id`),
            KEY `fk_messages_1_idx` (`from_user_id`),
            KEY `fk_messages_2_idx` (`to_user_id`),
            CONSTRAINT `fk_messages_1` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_messages_2` FOREIGN KEY (`to_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $this->execute($sql);
        
        $sql2 = "ALTER TABLE `store_unit`  CHANGE COLUMN `status` `status` CHAR(15) NOT NULL DEFAULT 'new' ;";
        $this->execute($sql2);
        
        $sql3 = "CREATE TABLE `model3d_history` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `model3d_id` int(11) NOT NULL,
            `model3d_file_id` int(11) DEFAULT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `user_id` int(11) DEFAULT NULL,
            `action_id` varchar(45) NOT NULL,
            `comment` tinytext,
            PRIMARY KEY (`id`),
            KEY `fk_model3d_history_1_idx` (`model3d_id`),
            KEY `fk_model3d_history_2_idx` (`model3d_file_id`),
            KEY `fk_model3d_history_3_idx` (`user_id`),
            CONSTRAINT `fk_model3d_history_1` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_model3d_history_2` FOREIGN KEY (`model3d_file_id`) REFERENCES `model3d_file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_model3d_history_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $this->execute($sql3);
        return 0;
    }

    public function down()
    {
        echo "m150812_082120_moderation1 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
