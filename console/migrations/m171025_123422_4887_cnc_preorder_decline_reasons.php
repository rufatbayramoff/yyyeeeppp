<?php

use yii\db\Migration;

class m171025_123422_4887_cnc_preorder_decline_reasons extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE cnc_preorder 
            ADD COLUMN `decline_reason` VARCHAR(255) NULL,
            ADD COLUMN `decline_comment` VARCHAR(255) NULL,
            ADD COLUMN `decline_initiator` TINYINT(3) NULL;
        ");
    }

    public function safeDown()
    {
        $this->dropColumn("cnc_preorder", "decline_reason");
        $this->dropColumn("cnc_preorder", "decline_comment");
        $this->dropColumn("cnc_preorder", "decline_initiator");

        return true;
    }
}
