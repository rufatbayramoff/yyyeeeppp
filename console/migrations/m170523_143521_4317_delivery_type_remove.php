<?php

use yii\db\Migration;

class m170523_143521_4317_delivery_type_remove extends Migration
{
    public function up()
    {
        $this->delete('filepage_cache');
        $this->delete('filepage_cache_color');
        $this->dropForeignKey('fk_filepage_cache_allow_delivery_type_id', 'filepage_cache');
        $this->dropForeignKey('fk_filepage_cache_colors_allow_delivery_type_id', 'filepage_cache_color');
        $this->dropColumn('filepage_cache', 'delivery_type_id');
        $this->dropColumn('filepage_cache_color', 'delivery_type_id');
    }

    public function down()
    {
        return false;
    }
}
