<?php

use yii\db\Migration;
use yii\db\Query;

class m160807_164414_1644_model3d_part_properties extends Migration
{
    public function up()
    {
        $this->renameTable('model3d_file_properties', 'model3d_part_properties');
        $this->execute(
            <<<SQL
        ALTER TABLE `model3d_part` 
ADD COLUMN `model3d_part_properties_id` INT(11) NULL AFTER `qty`,
ADD UNIQUE INDEX `model3d_part_properties_id_UNIQUE` (`model3d_part_properties_id` ASC);
ALTER TABLE `model3d_part` 
ADD CONSTRAINT `fk_model3d_part_properties_id`
  FOREIGN KEY (`model3d_part_properties_id`)
  REFERENCES `model3d_part_properties` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SQL
        );
        $model3dPartProperties = (new Query())
            ->select('*')
            ->from('model3d_part_properties')
            ->all();
        foreach ($model3dPartProperties as $model3dPartProperty) {
            $this->update('model3d_part', ['model3d_part_properties_id' => $model3dPartProperty['id']], ['id' => $model3dPartProperty['model3d_file_id']]);
        }
        $this->dropForeignKey('fk_model3d_properties_1', 'model3d_part_properties');
        $this->dropColumn('model3d_part_properties', 'model3d_file_id');
    }

    public function down()
    {
        echo "m160811_164414_1644_model3d_part_properties cannot be reverted.\n";
        return false;
    }
}
