<?php

use yii\db\Migration;

class m171010_110121_0000_fix_printer_material_key extends Migration
{
    public function safeUp()
    {
        $this->createIndex('filament_title_unique', 'printer_material', 'filament_title', true);
        $this->createIndex('render_color_unique', 'printer_color', 'render_color', true);
        $this->createIndex('code_unique', 'printer_material_group', 'code', true);

    }

    public function safeDown()
    {
        $this->dropIndex('filament_title_unique', 'printer_material');
        $this->dropIndex('render_color_unique', 'printer_color');
        $this->dropIndex('code_unique', 'printer_material_group');
    }
}
