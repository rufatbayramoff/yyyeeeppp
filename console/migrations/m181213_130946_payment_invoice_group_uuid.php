<?php

use yii\db\Migration;

/**
 * Class m181213_130946_payment_invoice_group_uuid
 */
class m181213_130946_payment_invoice_group_uuid extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_invoice', 'invoice_group_uuid', $this->string(32));
        $this->createIndex('idx_pi_invoice_group_uuid', 'payment_invoice', 'invoice_group_uuid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_pi_invoice_group_uuid', 'payment_invoice');
        $this->dropColumn('payment_invoice', 'invoice_group_uuid');
    }
}
