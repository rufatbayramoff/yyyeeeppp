<?php

use yii\db\Migration;

/**
 * Class m180731_190721_5559_preorder_description
 */
class m180731_190721_5559_preorder_description extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('preorder', 'description', 'text null');
        $this->alterColumn('preorder', 'offer_description', 'text null');
        $this->alterColumn('preorder', 'budget', 'decimal(15,4)');
        // Allow orders up to 10 000 000 000
        $this->alterColumn('store_order', 'price_total', 'decimal(15,4) not null');
        $this->alterColumn('store_order_item', 'price', 'decimal(15,4) not null');
        $this->alterColumn('store_order_item', 'price_fee', 'decimal(15,4) null');
        $this->alterColumn('store_pricer', 'price', 'decimal(15,4) not null');
        $this->alterColumn('store_pricer_history', 'old_price', 'decimal(15,4) not null');
        $this->alterColumn('store_pricer_history', 'new_price', 'decimal(15,4) not null');
        $this->alterColumn('store_order_promocode', 'amount', 'decimal(15,4) null');
        $this->alterColumn('store_order_position', 'fee_include', 'decimal(15,4) null');
        $this->alterColumn('store_order_position', 'fee', 'decimal(15,4) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
