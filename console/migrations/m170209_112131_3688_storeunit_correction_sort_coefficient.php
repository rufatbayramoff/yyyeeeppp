<?php

use yii\db\Migration;
use yii\db\Query;

class m170209_112131_3688_storeunit_correction_sort_coefficient extends Migration
{
    public function up()
    {
        $this->addColumn('store_unit', 'correction_sort_catalog_coefficient', 'integer not null default 0');
    }

    public function down()
    {
        $this->dropColumn('store_unit', 'correction_sort_catalog_coefficient');
    }
}
