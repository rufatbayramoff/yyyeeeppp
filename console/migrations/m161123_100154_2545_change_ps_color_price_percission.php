<?php

use yii\db\Migration;

class m161123_100154_2545_change_ps_color_price_percission extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `ps_printer_color` 
            CHANGE COLUMN `price` `price` DECIMAL(7,2) NOT NULL ,
            CHANGE COLUMN `price_usd` `price_usd` DECIMAL(7,2) NOT NULL DEFAULT '0.00' ;
        ");
    }

    public function down()
    {
        $this->execute("
            ALTER TABLE `ps_printer_color` 
            CHANGE COLUMN `price` `price` DECIMAL(5,2) NOT NULL ,
            CHANGE COLUMN `price_usd` `price_usd` DECIMAL(5,2) NOT NULL DEFAULT '0.00' ;
        ");
        return true;
    }
}
