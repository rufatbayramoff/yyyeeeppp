<?php

use yii\db\Migration;

class m160511_064755_1385_ps_pribter_description_length extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer` CHANGE `description` `description` TEXT  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `ps_printer` CHANGE `description` `description` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT '';");
        return true;
    }
}
