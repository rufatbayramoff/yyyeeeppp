<?php

use common\components\DateHelper;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m181211_114721_5814_payment_detail_progress_orders
 */
class m181211_114721_5814_payment_detail_progress_orders extends Migration
{
    /**
     * @return string
     * @throws Exception
     */
    protected function generatePaymentDetailOperationUuid()
    {
        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_detail_operation')->where('uuid="' . $operationUuid . '"')->one();
        } while ($checkUuid);
        return $operationUuid;
    }

    protected function getReservedAccountId($fromDetail)
    {
        $paymentAccount = (new Query())->select(['*'])->from('payment_account')->where(['id' => $fromDetail['payment_account_id']])->one();
        $toReservedAccount = (new Query())->select(['*'])->from('payment_account')->where(['user_id' => $paymentAccount['user_id'], 'type' => 'reserved'])->one();
        if ($toReservedAccount) {
            return $toReservedAccount['id'];
        }
        $this->insert('payment_account', ['user_id' => $paymentAccount['user_id'], 'type' => 'reserved']);
        $lastInsertId = Yii::$app->db->lastInsertID;
        return $lastInsertId;
    }

    protected function getAuthorizedAccountId($fromDetail)
    {
        $paymentAccount = (new Query())->select(['*'])->from('payment_account')->where(['id' => $fromDetail['payment_account_id']])->one();
        $toReservedAccount = (new Query())->select(['*'])->from('payment_account')->where(['user_id' => $paymentAccount['user_id'], 'type' => 'reserved'])->one();
        if ($toReservedAccount) {
            return $toReservedAccount['id'];
        }
        $this->insert('payment_account', ['user_id' => $paymentAccount['user_id'], 'type' => 'authorize']);
        $lastInsertId = Yii::$app->db->lastInsertID;
        return $lastInsertId;
    }

    protected function getTransaction($operation)
    {
        $paymentDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $operation['uuid']])->all();

        $fromDetail = $paymentDetails[0];
        $toDetail = $paymentDetails[1];


        $transaction = (new \yii\db\Query())->select(['*'])->from('payment_transaction')->where(['first_payment_detail_id' => $fromDetail['id']])->one();
        if (!$transaction) {
            $transaction = (new \yii\db\Query())->select(['*'])->from('payment_transaction')->where(['first_payment_detail_id' => $toDetail['id']])->one();
        }
        return $transaction;
    }

    /**
     * @param $paymentInvoice
     * @param $payment
     * @param $firstOperation
     * @param $fromDetail
     * @param $toDetail
     * @throws Exception
     */
    public function addPaymentDetail($paymentInvoice, $payment, $transaction, $fromDetail, $toDetail)
    {
        if ($fromDetail['amount'] > 0) {
            die('Invalid direction. Order: '.$paymentInvoice['store_order_id']);
        }
        echo "\nTransfer to reserved. Order: " . $paymentInvoice['store_order_id'] . "\n";

        $uuid = $this->generatePaymentDetailOperationUuid();
        $this->insert('payment_detail_operation', [
            'uuid'       => $uuid,
            'payment_id' => $payment['id'],
            'created_at' => \common\components\DateHelper::now()
        ]);

        if ($transaction['status'] === 'autorized') {
            $toAccountId = $this->getAuthorizedAccountId($fromDetail);
        } else {
            $toAccountId = $this->getReservedAccountId($fromDetail);
        }


        $amount = $toDetail['amount'];
        $currency = $toDetail['original_currency'];
        $paymentDetailFrom = [
            'payment_detail_operation_uuid' => $uuid,
            'payment_account_id'            => $toDetail['payment_account_id'],
            'updated_at'                    => DateHelper::now(),
            'amount'                        => -$amount,
            'original_currency'             => $currency,
            'rate_id'                       => 1,
            'description'                   => 'Fix progress orders. Transfer to reserved.',
            'type'                          => 'order'
        ];
        $paymentDetailTo = [
            'payment_detail_operation_uuid' => $uuid,
            'payment_account_id'            => $toAccountId,
            'updated_at'                    => DateHelper::now(),
            'amount'                        => $amount,
            'original_currency'             => $currency,
            'rate_id'                       => 1,
            'description'                   => 'Fix progress orders. Transfer to reserved.',
            'type'                          => 'order'
        ];
        $this->insert('payment_detail', $paymentDetailFrom);
        $this->insert('payment_detail', $paymentDetailTo);
    }

    public function processOrderOneOperations($storeOrder, $paymentInvoice, $payment, $paymentOperations)
    {
        if (count($paymentOperations) != 1) {
            return false;
        }
        $lastOperation = $paymentOperations[0];
        $paymentDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $lastOperation['uuid']])->orderBy('amount')->all();

        $fromDetial = $paymentDetails[1];
        $fromDetial['amount'] = -$fromDetial['amount'];
        $toDetail = $paymentDetails[1];

        $transaction = $this->getTransaction($paymentOperations[0]);

        $this->addPaymentDetail($paymentInvoice, $payment, $transaction, $fromDetial, $toDetail);
    }

    public function processOrderDoubleOperations($storeOrder, $paymentInvoice, $payment, $paymentOperations)
    {
        if (count($paymentOperations) != 2) {
            return false;
        }
        $lastOperation = $paymentOperations[1];
        $paymentDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $lastOperation['uuid']])->orderBy('amount')->all();

        $fromDetial = $paymentDetails[0];
        $toDetail = $paymentDetails[1];
        $description = $fromDetial['description'];
        $transaction = $this->getTransaction($paymentOperations[0]);

        if ((strpos($description, 'Order has been accepted') === 0) &&
            ($toDetail['payment_account_id'] == 110) && ($toDetail['type'] == 'order')) {
// Commented By order 27863
//            $paymentDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $firstOperation['uuid']])->all();
//            $fromDetial = $paymentDetails[0];
//            $toDetail = $paymentDetails[1];
            $this->addPaymentDetail($paymentInvoice, $payment, $transaction, $fromDetial, $toDetail);
        } elseif ((strpos($description, 'Thingiverse users fix zerro') === 0) &&
            ($toDetail['payment_account_id'] == 110)) {

            // Added for order 27912
            $firstOperation = $paymentOperations[0];
            $paymentDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $firstOperation['uuid']])->orderBy('amount')->all();
            $fromDetial = $paymentDetails[1];
            $fromDetial['amount'] = -$fromDetial['amount'];
            $toDetail = $paymentDetails[1];
            $toDetail['payment_account_id'] = 110;

            $this->addPaymentDetail($paymentInvoice, $payment, $transaction, $fromDetial, $toDetail);
        }
    }

    public function processOrderTripleOperations($storeOrder, $paymentInvoice, $payment, $paymentOperations)
    {
        if (count($paymentOperations) != 3) {
            return false;
        }
        $lastOperation = $paymentOperations[2];
        $paymentDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $lastOperation['uuid']])->all();

        $sendToReserv = false;

        if (count($paymentDetails) === 4) {
            // Есть отмена операции двойного начисления при Order has been printed
            $fromDetial = $paymentDetails[0];
            $toDetail = $paymentDetails[1];
            $description = $fromDetial['description'];
            $descriptionTo = $paymentDetails[3]['description'];
            if ((strpos($description, 'Order has been printed') === 0) && (strpos($descriptionTo, 'Rollback duplicate payment') === 0) &&
                ($toDetail['payment_account_id'] == 110) && ($toDetail['type'] == 'order')) {
                $sendToReserv = true;
            }
        }

        if (!$sendToReserv) {
            // Example: 26932
            $fromDetial = $paymentDetails[0];
            $toDetail = $paymentDetails[1];
            if (strpos($fromDetial['description'], 'Thingiverse users fix zerro balance')===0) {
                $easypostDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $paymentOperations[1]['uuid']])->all();
                $easypostDetailFrom = $easypostDetails[0];
                if (strpos($easypostDetailFrom['description'], 'Payment to EasyPost for the order number')===0) {
                    $sendToReserv = true;
                    $firstDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $paymentOperations[0]['uuid']])->orderBy('amount')->all();
                    $fromDetial  = $firstDetails[1];
                    $fromDetial['amount'] = - $fromDetial['amount'];
                    $toDetail  = $firstDetails[1];
                    $toDetail['payment_account_id'] = 110;
                }
            }
        }

        if ($sendToReserv) {
            $transaction = $this->getTransaction($paymentOperations[0]);
            $this->addPaymentDetail($paymentInvoice, $payment, $transaction, $fromDetial, $toDetail);
        }
    }

    public function processOrderFourOperations($storeOrder, $paymentInvoice, $payment, $paymentOperations)
    {
        if (count($paymentOperations) != 4) {
            return false;
        }
        $lastOperation = $paymentOperations[2];
        $paymentDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $lastOperation['uuid']])->all();

        $sendToReserv = false;

        if (count($paymentDetails) === 4) {
            // Есть отмена операции двойного начисления при Order has been printed
            $fromDetial = $paymentDetails[0];
            $toDetail = $paymentDetails[1];
            $description = $fromDetial['description'];
            $descriptionTo = $paymentDetails[3]['description'];
            if ((strpos($description, 'Order has been printed') === 0) && (strpos($descriptionTo, 'Rollback duplicate payment') === 0) &&
                ($toDetail['payment_account_id'] == 110) && ($toDetail['type'] == 'order')) {
                $easyPostOperation = $paymentOperations[3];
                $easyPostDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $easyPostOperation['uuid']])->all();
                $easypostDescription = $easyPostDetails[0]['description'];
                if (strpos($easypostDescription, 'Payment to EasyPost for the order number') === 0) {
                    $sendToReserv = true;
                }
            }
        }

        if ($sendToReserv) {
            $transaction = $this->getTransaction($paymentOperations[0]);
            $this->addPaymentDetail($paymentInvoice, $payment, $transaction, $fromDetial, $toDetail);
        }
    }

    public function manualFixes($storeOrder, $paymentInvoice, $payment, $paymentOperations)
    {
        if ($paymentOperations) {
            $transaction = $this->getTransaction($paymentOperations[0]);
        }
        if ($storeOrder['id'] == 27037) {
            if (count($paymentOperations) == 5) {
                echo "\nTransfer to reserved. Order: " . $storeOrder['id'] . "\n";
                $firstOperation = $paymentOperations[1];
                $paymentDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $firstOperation['uuid']])->all();
                $fromDetial = $paymentDetails[0];
                $toDetail = $paymentDetails[1];
                $this->addPaymentDetail($paymentInvoice, $payment, $transaction, $fromDetial, $toDetail);
            }
        }
        if ($storeOrder['id'] == 27697) {
            if (count($paymentOperations) == 3) {
                echo "\nTransfer to reserved. Order: " . $storeOrder['id'] . "\n";
                $firstOperation = $paymentOperations[0];
                $paymentDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $firstOperation['uuid']])->all();
                $fromDetial = $paymentDetails[0];
                $toDetail = $paymentDetails[1];
                $this->addPaymentDetail($paymentInvoice, $payment, $transaction, $fromDetial, $toDetail);
            }
        }
        if ($storeOrder['id'] == 8465) {
            if (count($paymentOperations) == 3) {
                $firstOperation = $paymentOperations[0];
                $paymentDetails = (new \yii\db\Query())->select(['*'])->from('payment_detail')->where(['payment_detail_operation_uuid' => $firstOperation['uuid']])->orderBy('amount')->all();
                $fromDetial = $paymentDetails[0];
                $toDetail = $paymentDetails[1];
                $this->addPaymentDetail($paymentInvoice, $payment, $transaction, $fromDetial, $toDetail);
            }
        }
    }


    public function processOrder($storeOrder)
    {
        $paymentInvoice = (new Query())->select(['*'])->from('payment_invoice')->where(['store_order_id' => $storeOrder['id']])->one();
        $payment = (new Query())->select(['*'])->from('payment')->where(['payment_invoice_uuid' => $paymentInvoice['uuid']])->one();
        $paymentOperation = (new Query())->select(['payment_detail_operation.*'])->from('payment_detail_operation')->leftJoin('payment_detail',
            'payment_detail.payment_detail_operation_uuid=payment_detail_operation.uuid')->where(['payment_id' => $payment['id']])->orderBy('created_at asc, payment_detail.id asc')->all();

        $paymentOperation = \yii\helpers\ArrayHelper::index($paymentOperation, 'uuid');
        $paymentOperation = array_values($paymentOperation);

        $this->processOrderOneOperations($storeOrder, $paymentInvoice, $payment, $paymentOperation);
        $this->processOrderDoubleOperations($storeOrder, $paymentInvoice, $payment, $paymentOperation);
        $this->processOrderTripleOperations($storeOrder, $paymentInvoice, $payment, $paymentOperation);
        $this->processOrderFourOperations($storeOrder, $paymentInvoice, $payment, $paymentOperation);
        $this->manualFixes($storeOrder, $paymentInvoice, $payment, $paymentOperation);
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $progressOrders = (new \yii\db\Query())->select(['*'])->from('store_order')->where('payment_status in (\'settled\', \'authorized\') and order_state=\'processing\'')
            ->all();
        foreach ($progressOrders as $progressOrder) {
            echo "\nOrder in progress: ".$progressOrder['id'];
            $this->processOrder($progressOrder);
        }
        echo "\nTotal in progress: ".count($progressOrders)."\n";
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
