<?php

use yii\db\Migration;

class m171204_163921_0000_unique_model3dpart_file_id extends Migration
{
    public function safeUp()
    {
        $this->createIndex('model3d_part_file_id_unique', 'model3d_part', 'file_id', true);
        $this->dropIndex(	'fk_model3d_file_1_idx', 'model3d_part');
    }

    public function safeDown()
    {
        return false;
    }
}
