<?php

use yii\db\Migration;

class m171115_120802_4592_msg_tipic_bind_preorder extends Migration
{
    public function safeUp()
    {

        $this->execute("
            ALTER TABLE `msg_topic` 
            CHANGE COLUMN `bind_to` `bind_to` ENUM('order', 'preorder', 'cnc_preorder') NULL DEFAULT NULL ;
        ");

        $this->execute("UPDATE msg_topic SET bind_to = 'cnc_preorder' WHERE bind_to = 'preorder'");
    }

    public function safeDown()
    {
        echo "m171115_120802_4592_msg_tipic_bind_preorder cannot be reverted.\n";

        return false;
    }

}
