<?php

use yii\db\Migration;

/**
 * Class m181017_133453_quote_window_calculator_email
 */
class m181017_133453_quote_window_calculator_email extends Migration
{
    public function safeUp()
    {
        $this->insert(
            'email_template',
            [
                'code'          => 'quoteWindowCalculator',
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => "New RFQ received from your online quoting widget",
                'description'   => 'Params: companyName, link',
                'updated_at'    => dbexpr('NOW()'),
                'template_html' => "Hi %companyName%,

You’ve received a new RFQ from your online quoting widget. To review the request and follow up with the customer, click <a href=\"%link%\">here</a>.

Best,
Treatstock",
                'template_text' => ""
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'quoteWindowCalculator', 'language_id' => 'en-US']);
    }
}
