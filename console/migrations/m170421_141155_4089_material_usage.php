<?php

use yii\db\Migration;

class m170421_141155_4089_material_usage extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `printer_material_group_usage` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `title` varchar(45) NOT NULL,
              `description` varchar(145) DEFAULT NULL,
              `is_active` bit(1) NOT NULL DEFAULT b\'1\',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');


        $this->execute('CREATE TABLE `printer_material_group_usage_intl` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `lang_iso` char(5) NOT NULL,
              `title` varchar(45) NOT NULL,
              `description` varchar(145) DEFAULT NULL,
              `is_active` bit(1) NOT NULL DEFAULT b\'1\',
              `usage_id` int(11) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_printer_material_group_usage_intl_1_idx` (`usage_id`),
              CONSTRAINT `fk_printer_material_group_usage_intl_1` 
                FOREIGN KEY (`usage_id`) REFERENCES `printer_material_group_usage` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute('CREATE TABLE `printer_material_group_to_usage` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `group_id` int(11) NOT NULL,
              `usage_id` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `index2` (`usage_id`,`group_id`),
              KEY `fk_printer_material_group_to_usage_2_idx` (`group_id`),
              CONSTRAINT `fk_printer_material_group_to_usage_1`
                FOREIGN KEY (`usage_id`) REFERENCES `printer_material_group_usage` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_printer_material_group_to_usage_2` 
                FOREIGN KEY (`group_id`) REFERENCES `printer_material_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

    }

    public function down()
    {
        $this->truncateTable('printer_material_group_to_usage');
        $this->truncateTable('printer_material_usage_intl');
        $this->truncateTable('printer_material_usage');

        $this->dropTable('printer_material_group_to_usage');
        $this->dropTable('printer_material_usage_intl');
        $this->dropTable('printer_material_usage');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
