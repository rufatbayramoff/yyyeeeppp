<?php

use yii\db\Migration;

class m171123_131205_4592_fix extends Migration
{
    public function safeUp()
    {
        $this->execute("
        
        ALTER TABLE `preorder` 
CHANGE COLUMN `offer_description` `offer_description` TEXT NOT NULL ;

        
        ");
    }

    public function safeDown()
    {
        echo "m171123_131205_4592_fix cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171123_131205_4592_fix cannot be reverted.\n";

        return false;
    }
    */
}
