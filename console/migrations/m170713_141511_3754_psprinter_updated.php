<?php

use yii\db\Migration;

class m170713_141511_3754_psprinter_updated extends Migration
{

    public function safeUp()
    {
        $this->execute('ALTER TABLE `ps_printer` 
            CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ;
        ');

        $this->execute('ALTER TABLE `printer_technology` 
            ADD COLUMN `code` VARCHAR(15) NULL AFTER `print_speed_max`,
            ADD UNIQUE INDEX `code_UNIQUE` (`code` ASC);
        ');

        $this->execute('ALTER TABLE `printer_technology_intl` 
          ADD COLUMN `description` TEXT NULL AFTER `is_active`;');
        $this->execute('ALTER TABLE `printer_technology` 
          ADD COLUMN `description` TEXT NULL AFTER `is_active`;');
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE `ps_printer` 
            CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL DEFAULT NULL;');
        $this->dropColumn('printer_technology', 'code');

        $this->dropColumn('printer_technology', 'description');
        $this->dropColumn('printer_technology_intl', 'description');
    }
}
