<?php

use yii\db\Migration;

class m160818_102831_1644_alter_cart_item_can_change_ps extends Migration
{
    public function up()
    {
        $this->addColumn('cart_item', 'can_change_ps', 'bool NOT NULL default true');
    }

    public function down()
    {
        $this->dropColumn('cart_item', 'can_change_ps');
    }
}
