<?php

use yii\db\Migration;

class m170220_135741_2900_seo_autofill extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE IF NOT EXISTS `seo_page_autofill` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `object_type` varchar(45) NOT NULL,
          `object_id` int(11) NOT NULL,
          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          `seo_page_id` int(11) NOT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `index2` (`object_type`,`object_id`),
          KEY `fk_seo_page_autofill_1_idx` (`seo_page_id`),
          CONSTRAINT `fk_seo_page_autofill_1` FOREIGN KEY (`seo_page_id`) REFERENCES `seo_page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');


        $config = [
            [
                'type' => 'model3d',
                'title' => '3D Printable %modelTitle% in %modelCategory% by %modelUser%',
                'meta_description' => 'Order %modelTitle% 3D printable model. %modelDescription% ',
                'meta_keywords' => '%modelKeywords%',
                'header' => '%modelTitle% ',
            ],
            [
                'type' => 'ps',
                'title' => '%psTitle% 3D Printing with %psMaterials% in %psLocation%',
                'meta_description' => '%psDescription%',
                'meta_keywords' => '%psPrinters%, %psMaterials%, %psLocation%',
                'header' => '%psTitle% ',
            ],
            [
                'type' => 'category',
                'title' => '%categoryTitle% - 3D Printable Models',
                'meta_description' => '%categoryDescription%',
                'meta_keywords' => '%categoryTitle%, 3d printables, 3d models',
                'header' => '%categoryTitle%',
            ]
        ];
        $this->insert(
            'system_setting',
            [
                'group_id'        => '5',
                'key'             => 'seosettings',
                'value'           => 'json',
                'json'            => json_encode($config, JSON_PRETTY_PRINT),
                'created_at'      => date('Y-m-d H:i:s'),
                'updated_at'      => date('Y-m-d H:i:s'),
                'created_user_id' => null,
                'updated_user_id' => null,
                'description'     => 'SEO Autofill settings',
                'is_active'       => 1,
            ]
        );
    }

    public function down()
    {
        #$this->dropTable('seo_page_autofill');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
