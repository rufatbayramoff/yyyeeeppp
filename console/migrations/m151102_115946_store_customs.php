<?php

use yii\db\Schema;
use yii\db\Migration;

class m151102_115946_store_customs extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `store_unit`
            ADD COLUMN `custom_code_id` INT(11) NULL DEFAULT NULL AFTER `model3d_id`,
            ADD INDEX `custom_code_idx` (`custom_code_id`);");

        $this->execute("ALTER TABLE `store_unit`
            ADD CONSTRAINT `fk_custom_code_2`
              FOREIGN KEY (`custom_code_id`)
              REFERENCES `custom_code` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;");
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE store_unit DROP FOREIGN KEY `fk_custom_code_2`;');
        $this->execute('ALTER TABLE store_unit DROP KEY `custom_code_idx`;');
        $this->execute('ALTER TABLE store_unit DROP COLUMN `custom_code_id`;');
    }
}
