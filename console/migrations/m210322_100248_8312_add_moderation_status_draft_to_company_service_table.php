<?php

use yii\db\Migration;

/**
 * Class m210322_100248_add_moderation_status_draft_to_company_service_table
 */
class m210322_100248_8312_add_moderation_status_draft_to_company_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE company_service MODIFY COLUMN `moderator_status` enum('new','approved','approvedReviewing','rejected','rejectedReviewing','unavailableReviewing','draft') CHARACTER SET utf8 COLLATE utf8_general_ci;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210322_100248_add_moderation_status_draft_to_company_service_table cannot be reverted.\n";

        return false;
    }
    */
}
