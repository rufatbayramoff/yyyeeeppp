<?php

use yii\db\Migration;

class m160516_143101_whishlist extends Migration
{
    public function up()
    {
        $this->execute("update user_collection set title='Wish List' where system_type='likes' and title='Wish gifts';");
    }

    public function down()
    {
         
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
