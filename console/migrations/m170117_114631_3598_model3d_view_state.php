<?php

use yii\db\Migration;
use yii\db\Query;

class m170117_114631_3598_model3d_view_state extends Migration
{
    public function up()
    {
        $this->renameTable('model3d_viewed_by', 'model3d_viewed_state');
        $this->addColumn('model3d_viewed_state', 'model3dTextureInfo', 'json null');
    }

    public function down()
    {
        echo 'no roll back';
        return false;
    }
}
