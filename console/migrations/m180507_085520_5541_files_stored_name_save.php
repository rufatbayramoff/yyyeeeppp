<?php

use common\interfaces\FileBaseInterface;
use common\models\File;
use common\models\FileAdmin;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180507_085520_5541_files_stored_name
 */
class m180507_085520_5541_files_stored_name_save extends Migration
{
    public function fixStoredName()
    {
        $files = File::find()->all();
        foreach ($files as $file) {
            $file->produceStoredName();
            $storedName = $file->getStoredName();
            $this->update('file', ['stored_name' => $storedName], 'id=' . $file->id);
        }
        $filesAdmin = FileAdmin::find()->all();
        foreach ($filesAdmin as $file) {
            $file->produceStoredName();
            $storedName = $file->getStoredName();
            $this->update('file_admin', ['stored_name' => $storedName], "uuid='" . $file->uuid . "'");
        }
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $file = (new Query())->select('*')->from('file')->one();
        if ($file) { // If files exists
            $this->fixStoredName();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
