<?php

use yii\db\Migration;

class m161028_123121_2830_api_external extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `affiliate_resource` 
                    DROP COLUMN `api_key`,
                    ADD COLUMN `api_external_system_id` INT(11) NULL AFTER `created_at`,
                    ADD INDEX `fk_affiliate_resource_1_idx` (`api_external_system_id` ASC);
                    ALTER TABLE `affiliate_resource` 
                    ADD CONSTRAINT `fk_affiliate_resource_1`
                      FOREIGN KEY (`api_external_system_id`)
                      REFERENCES `api_external_system` (`id`)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION;
        ');
    }

    public function down()
    {
       $this->execute('ALTER TABLE `affiliate_resource` 
            DROP FOREIGN KEY `fk_affiliate_resource_1`;
            ALTER TABLE `affiliate_resource` 
            DROP COLUMN `api_external_system_id`,
            DROP INDEX `fk_affiliate_resource_1_idx` ;
        ');

        $this->addColumn('affiliate_resource', 'api_key', 'varchar(255)');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
