<?php

use yii\db\Migration;

class m160714_094638_2421_sms_gateway_name extends Migration
{
    public function up()
    {
        $this->addColumn('user_sms', 'gateway', 'varchar(30)');
        $this->update('user_sms', ['gateway' => 'cmsms']);
    }

    public function down()
    {
        $this->dropColumn('user_sms', 'gateway');
        return true;
    }
}
