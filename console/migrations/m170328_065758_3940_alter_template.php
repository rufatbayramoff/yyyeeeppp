<?php

use yii\db\Migration;

class m170328_065758_3940_alter_template extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `seo_page_autofill_template` 
            CHANGE COLUMN `type` `type` VARCHAR(45) NOT NULL ,
            CHANGE COLUMN `template_name` `template_name` VARCHAR(245) NOT NULL ,
            CHANGE COLUMN `title` `title` VARCHAR(245) NOT NULL ,
            CHANGE COLUMN `meta_description` `meta_description` VARCHAR(245) NOT NULL ,
            CHANGE COLUMN `meta_keywords` `meta_keywords` VARCHAR(245) NOT NULL ,
            CHANGE COLUMN `header` `header` VARCHAR(245) NOT NULL ;
        ');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
