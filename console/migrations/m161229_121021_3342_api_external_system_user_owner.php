<?php

use yii\db\Migration;
use yii\db\Query;

class m161229_121021_3342_api_external_system_user_owner extends Migration
{

    public function up()
    {
        $this->addColumn('api_external_system', 'model3d_owner', "enum('upload_user','api_external_system') DEFAULT 'api_external_system'");
        $this->addForeignKey('api_external_system_fk', 'api_printable_pack', 'api_external_system_id',  'api_external_system', 'id');
    }

    public function down()
    {
        $this->dropColumn('api_external_system', 'model3d_owner');
        $this->dropForeignKey('api_external_system_fk', 'api_printable_pack');
    }
}
