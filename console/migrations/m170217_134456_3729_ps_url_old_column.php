<?php

use yii\db\Migration;

class m170217_134456_3729_ps_url_old_column extends Migration
{
    public function up()
    {
        $this->addColumn('ps', 'url_old', 'VARCHAR(255) NULL');
        $this->createIndex('ps_url_old_unique', 'ps', 'url', true);
    }

    public function down()
    {
        $this->dropColumn('ps', 'url_old');
        return true;
    }
}
