<?php

use yii\db\Migration;
use yii\db\Query;

class m210205_111147_8216_internal_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('system_setting', "`key`='internalOrdersCustomerUserId'");
        $this->insert('system_setting',
            [
                'group_id'        => 10,
                'key'             => 'internalOrdersCustomerUserId',
                'value'           => 'json',
                'created_at'      => '2021-02-05 08:04:39',
                'updated_at'      => '2021-02-05 08:07:29',
                'created_user_id' => 1,
                'updated_user_id' => 1,
                'description'     => 'internal users Id list',
                'json'            => '[60751, 1009]',
                'is_active'       => 1
            ]
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
