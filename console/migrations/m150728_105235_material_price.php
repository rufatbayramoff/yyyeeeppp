<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_105235_material_price extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%ps_printer_color}}', 'price_currency_iso', 'string(10) NULL AFTER `color_id` ');
        $this->addColumn('{{%ps_printer_color}}', 'price', 'DECIMAL(5,2) NULL AFTER `price_currency_iso` ');
        $this->addColumn('{{%ps_printer_color}}', 'price_measure', "ENUM('sm','in') NULL AFTER `price` ");
    }

    public function safeDown()
    {
        $this->dropColumn('{{%ps_printer_color}}', 'price_currency_iso');
        $this->dropColumn('{{%ps_printer_color}}', 'price');
        $this->dropColumn('{{%ps_printer_color}}', 'price_measure');
    }
}
