<?php

use yii\db\Migration;

class m170111_124611_3197_email_template extends Migration
{
    public function up()
    {
        $this->execute("
        INSERT INTO `email_template`(
    `id`,
    `code`,
    `group`,
    `language_id`,
    `title`,
    `description`,
    `updated_at`,
    `template_html`,
    `template_text`,
    `is_active`
) VALUES (
    NULL,
    'ps.order.adminChangeOffer',
    'service',
    'en-US',
    'Order #%orderId% has been transferred',
    NULL,
    NULL,
    'Order #%orderId% has been transferred to another print service.',
    'Order #%orderId% has been transferred to another print service.',
    1
 );

        ");

    }

    public function down()
    {
        $this->delete('email_template', ['code' => 'ps.order.adminChangeOffer']);
        return true;
    }
}
