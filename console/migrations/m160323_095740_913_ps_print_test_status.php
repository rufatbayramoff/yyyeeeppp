<?php

use yii\db\Migration;

class m160323_095740_913_ps_print_test_status extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer` ADD `test_status` ENUM('no','common','professional')  NOT NULL  DEFAULT 'no'  AFTER `is_location_change`;");

        $this->execute("
            CREATE TABLE `ps_printer_test` (
              `ps_printer_id` int(11) NOT NULL,
              `type` enum('common','professional') NOT NULL DEFAULT 'common',
              `status` enum('new','checked','failed') NOT NULL DEFAULT 'new',
              `image_file_ids` text,
              `comment` text,
              PRIMARY KEY (`ps_printer_id`),
              CONSTRAINT `ps_printer` FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer` (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

    }

    public function down()
    {
        $this->execute("ALTER TABLE `ps_printer` DROP `test_status`;");
        $this->execute("DROP TABLE `ps_printer_test`;");
        return true;
    }
}
