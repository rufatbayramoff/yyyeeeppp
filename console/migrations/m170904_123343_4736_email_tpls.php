<?php

use yii\db\Migration;

class m170904_123343_4736_email_tpls extends Migration
{
    public function safeUp()
    {
        $this->insert('email_template', [
            'code' => 'clientCanAcceptOrder',
            'group' => 'order',
            'language_id' => 'en-US',
            'title' => 'Review your order #%orderId%',
            'description' => 'email to client to review and accept his order. %orderId%, %clientName%, %orderLink%',
            'template_html' => 'Hello %clientName%,

Your order has been printed, you can review it here: <a href="%orderLink%">%orderLink%</a>

Best regards,
Treatstock Team.
',
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);
    }

    public function safeDown()
    {
        $this->delete('email_template', [
            'code' => 'clientCanAcceptOrder',
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170904_123343_4736_email_tpls cannot be reverted.\n";

        return false;
    }
    */
}
