<?php

use common\components\ArrayHelper;
use common\models\Model3dReplica;
use yii\db\Migration;
use yii\db\Query;

class m160812_172920_1644_alter_store_order_item extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
        ALTER TABLE `store_order_item` 
ADD COLUMN `model3d_replica_id` INT(11) NULL AFTER `updated_at`,
ADD INDEX `fk_store_order_item_3_idx` (`model3d_replica_id` ASC);
ALTER TABLE `store_order_item` 
ADD CONSTRAINT `fk_store_order_item_3`
  FOREIGN KEY (`model3d_replica_id`)
  REFERENCES `model3d_replica` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SQL
        );
    }

    public
    function down()
    {
        echo "m160812_172920_1644_alter_store_order_item cannot be reverted.\n";
        return false;
    }
}
