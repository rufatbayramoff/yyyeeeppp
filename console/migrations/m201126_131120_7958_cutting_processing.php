<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;


/**
 * Class m201126_131120_7958_cutting_processing
 */
class m201126_131120_7958_cutting_processing extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute('UPDATE `cutting_machine_processing` SET `cutting_price`=`cutting_price`*0.1, `engraving_price`=`engraving_price`*0.1');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}
