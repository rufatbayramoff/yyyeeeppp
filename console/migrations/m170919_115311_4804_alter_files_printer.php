<?php

use yii\db\Migration;

class m170919_115311_4804_alter_files_printer extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `printer` ADD COLUMN `files_json` JSON NULL AFTER `category_id`;');
    }

    public function safeDown()
    {
        $this->dropColumn('printer', 'files_json');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170919_115311_4804_alter_files_printer cannot be reverted.\n";

        return false;
    }
    */
}
