<?php

use yii\db\Migration;
use yii\db\Query;

class m170718_105321_4225_ps_model_creator_rename extends Migration
{
    public function up()
    {
        $this->renameTable('ps_model_creator', 'ps_machine');
        $this->renameTable('ps_model_creator_delivery', 'ps_machine_delivery');
        $this->renameTable('ps_cnc_service', 'ps_cnc_machine');
        $this->renameColumn('ps_printer', 'ps_model_creator_id', 'ps_machine_id');
        $this->renameColumn('ps_machine', 'ps_cnc_service_id', 'ps_cnc_machine_id');
        $this->renameColumn('ps_machine_delivery', 'ps_model_creator_id', 'ps_machine_id');
        $this->dropForeignKey('cnc_material_group_id', 'cnc_material');
        $this->addForeignKey('fk_cnc_material_group_id', 'cnc_material', 'cnc_material_group_id', 'cnc_material_group', 'id');
        $this->dropForeignKey('fk_ps_model_creator_id', 'ps_printer');
        $this->dropColumn('ps_printer', 'ps_machine_id');
        $this->execute('ALTER TABLE `ps_machine` ADD UNIQUE(`ps_printer_id`);');
        $this->execute('ALTER TABLE `ps_machine` ADD UNIQUE(`ps_cnc_machine_id`);');
    }

    public function down()
    {
        return true;
    }
}
