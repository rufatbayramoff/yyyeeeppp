<?php

use yii\db\Migration;

class m160711_161121_2381_upload_file_title_250_chars extends Migration
{
    public function up()
    {
        $this->alterColumn('model3d_img', 'title', 'varchar(250) DEFAULT NULL');
        $this->alterColumn('model3d_img', 'basename', 'varchar(250) DEFAULT NULL');
        $this->alterColumn('model3d_file', 'name', 'varchar(250) DEFAULT NULL');
        $this->alterColumn('model3d_file', 'title', 'varchar(250) DEFAULT NULL');
    }

    public function down()
    {
    }

}
