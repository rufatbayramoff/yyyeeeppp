<?php

use yii\db\Migration;

class m161025_074935_2797_ts_profile_and_test_model extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        $this->execute("INSERT INTO `user_profile`(`user_id`,`full_name`,`dob_date`,`address_id`,`website`,`phone`,`phone_confirmed`,`updated_at`,`current_lang`,
            `current_currency_iso`,`current_metrics`,`timezone_id`,`firstname`,`lastname`,`is_printservice`,`is_modelshop`,`avatar_url`,`background_url`,`gender`,
            `printservice_title`,`modelshop_title`,`info_about`,`info_skills`,`default_license_id`)
            VALUES (1,NULL,NULL,NULL,NULL,NULL,'',NULL,'en-US','USD','in','America/Los_Angeles',NULL,NULL,0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
        ");


        $this->execute("INSERT INTO `user_address`(`id`, `created_at`, `updated_at`, `user_id`, `contact_name`, `country_id`, `region`, `city`, `address`, `extended_address`, `zip_code`, `comment`,`type`,`phone`,`email`,`company`) 
          VALUES (NULL ,'2016-10-25 15:03:12','2016-10-25 15:03:26',1,'Treatstock',233,'Delaware','Newark','40 E Main St',NULL,'19711',NULL,'pickup','Treatstock phone','support@treatstock.com',NULL);
        ");

        $this->update('user', ['notify_config' => '{"order":{"email":"never"},"service":{"email":"never"},"news":{"email":"never"},"rules":{"email":"never"},"personalMessages":{"email":"never"}}'], ['id' => 1]);

        $this->execute("INSERT INTO `system_setting`(`id`,`group_id`,`key`,`value`,`created_at`,`updated_at`,`created_user_id`,`updated_user_id`,`description`,`json`,`is_active`)
          VALUES (NULL,9,'testorder.storeunit','-1','2016-10-25 14:28:21',NULL,1,NULL,NULL,NULL,1);
        ");

        $this->execute("INSERT INTO `system_setting`(`id`,`group_id`,`key`,`value`,`created_at`,`updated_at`,`created_user_id`,`updated_user_id`,`description`,`json`,`is_active`) 
          VALUES (NULL,9,'testorder.userid','1','2016-10-25 14:28:56',NULL,1,NULL,NULL,NULL,1);
        ");

    }

    public function safeDown()
    {
        $this->delete('user_profile', ['user_id' => 1]);
        $this->update('user', ['notify_config' => null], ['id' => 1]);
        $this->delete('system_setting', ['group_id' => 9, 'key' => 'testorder.storeunit']);
        $this->delete('system_setting', ['group_id' => 9, 'key' => 'testorder.userid']);
        $this->delete('user_address', ['user_id' => 1]);
    }

}
