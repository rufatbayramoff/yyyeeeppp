<?php

use yii\db\Migration;

class m170414_124259_3309_user_links extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `user_profile` 
            ADD COLUMN `url_facebook` VARCHAR(245) NULL AFTER `default_license_id`,
            ADD COLUMN `url_instagram` VARCHAR(245) NULL AFTER `url_facebook`,
            ADD COLUMN `url_twitter` VARCHAR(245) NULL AFTER `url_instagram`;
            ');
    }

    public function down()
    {
        $this->dropColumn('user_profile', 'url_facebook');
        $this->dropColumn('user_profile', 'url_instagram');
        $this->dropColumn('user_profile', 'url_twitter');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
