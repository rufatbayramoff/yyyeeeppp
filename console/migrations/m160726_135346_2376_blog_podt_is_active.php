<?php

use yii\db\Migration;

class m160726_135346_2376_blog_podt_is_active extends Migration
{
    public function up()
    {
        $this->execute("
          ALTER TABLE `blog_post`
          ADD COLUMN `is_active`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `image_file_id`,
          ADD INDEX `active` (`is_active`);
        ");

    }

    public function down()
    {
        $this->dropColumn('blog_post', 'is_active');
        return true;
    }
}
