<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_115750_printercolorusd extends Migration
{ 
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("ALTER TABLE `ps_printer_color` 
        CHANGE COLUMN `price_currency_iso` `price_currency_iso` VARCHAR(10) NOT NULL ,
        CHANGE COLUMN `price` `price` DECIMAL(5,2) NOT NULL ,
        CHANGE COLUMN `price_measure` `price_measure` ENUM('cm','in') NOT NULL ,
        CHANGE COLUMN `is_active` `is_active` TINYINT(1) NOT NULL DEFAULT 1 ,
        ADD COLUMN `price_usd` DECIMAL(5,4) NULL AFTER `is_active`,
        ADD COLUMN `price_usd_updated` TIMESTAMP NULL AFTER `price_usd`;
        ");
        
        $this->execute("ALTER TABLE `ps_printer_color` 
            ADD INDEX `priceusd` (`price_usd_updated` ASC),
            ADD INDEX `priceusdupdate` (`price_usd_updated` ASC);
        ");
        
        $this->execute("ALTER TABLE `ps_printer_color` 
            CHANGE COLUMN `price_usd` `price_usd` DECIMAL(5,4) NOT NULL DEFAULT 0 ;
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('ps_printer_color', 'price_usd');
        $this->dropColumn('ps_printer_color', 'price_usd_updated');
    } 
}
