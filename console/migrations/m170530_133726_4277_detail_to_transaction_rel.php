<?php

use yii\db\Migration;

class m170530_133726_4277_detail_to_transaction_rel extends Migration
{
    public function up()
    {
        $toFix = $this->db->createCommand('select a.id, a.payment_id, a.created_at, b.* from payment_detail a
left join payment_transaction b ON(a.created_at=b.created_at AND b.user_id=a.user_id and b.vendor=\'paypal\')
where a.user_id>1000 and a.amount<0 and a.type=\'payout\' and b.id is null')->queryAll();

        if (count($toFix) > 0) {
            throw new \yii\db\Exception('Relations problems. Fix payment_transaction.created_at and payment_detail.created_at to match. Records: ' . count($toFix));
        }


        $this->execute('ALTER TABLE `payment_detail` ADD COLUMN `transaction_id` INT NULL AFTER `type`;');
        $this->execute('ALTER TABLE `payment_detail` 
            ADD INDEX `fk_payment_detail_4_idx` (`transaction_id` ASC);
            ALTER TABLE `payment_detail` 
            ADD CONSTRAINT `fk_payment_detail_4`
              FOREIGN KEY (`transaction_id`)
              REFERENCES `payment_transaction` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');

        $sql = 'select a.id, a.payment_id,b.id as transaction_id from payment_detail a
                left join payment_transaction b ON(a.created_at=b.created_at AND b.user_id=a.user_id and b.vendor=\'paypal\')
                where a.user_id>1000 and a.amount<0 and a.type=\'payout\'';
        $paymentDetails = $this->db->createCommand($sql)->queryAll();
        foreach ($paymentDetails as $paymentDetail) {
            $this->update(
                'payment_detail',
                ['transaction_id' => $paymentDetail['transaction_id']],
                ['id' => $paymentDetail['id']]
            );
        }
    }

    public function down()
    {
        $this->execute('ALTER TABLE `payment_detail` 
            DROP FOREIGN KEY `fk_payment_detail_4`;
            ALTER TABLE`payment_detail` 
            DROP COLUMN `transaction_id`,
            DROP INDEX `fk_payment_detail_4_idx` ;
        ');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
