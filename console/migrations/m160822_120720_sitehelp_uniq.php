<?php

use yii\db\Migration;

class m160822_120720_sitehelp_uniq extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `site_help` DROP INDEX `index2`');
    }

    public function down()
    {
        $this->execute('ALTER TABLE `site_help` ADD UNIQUE INDEX `index2` (`alias` ASC);');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
