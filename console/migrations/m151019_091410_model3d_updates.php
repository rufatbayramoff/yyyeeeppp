<?php

use yii\db\Schema;
use yii\db\Migration;

class m151019_091410_model3d_updates extends Migration
{
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("ALTER TABLE `model3d` 
            ADD COLUMN `default_color` CHAR(25) NOT NULL DEFAULT 'LimeGreen' AFTER `cover_file_id`,
            ADD COLUMN `default_material` CHAR(45) NOT NULL DEFAULT 'PLA' AFTER `default_color`;
        ");
        
        $this->execute("ALTER TABLE `user_location` CHANGE COLUMN `address` `address` VARCHAR(245) NULL DEFAULT NULL ;");
        $this->execute("ALTER TABLE `store_unit` ADD COLUMN `license_config` VARCHAR(245) NULL");
    }

    public function safeDown()
    {
        $this->dropColumn('model3d', 'default_color');
        $this->dropColumn('model3d', 'default_material');
    }
}
