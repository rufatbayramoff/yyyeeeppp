<?php

use yii\db\Migration;

class m161028_090458_3060_set_as_delivered extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `email_template`(
    `id`,
    `code`,
    `group`,
    `language_id`,
    `title`,
    `description`,
    `updated_at`,
    `template_html`,
    `template_text`
) VALUES (
    NULL,
    'client.order.delivered',
    'order',
    'en-US',
    'Your order #%orderId% has been delivered',
    NULL,
    NULL,
    'Hi %clientName%,
Our system shows that your order #%orderId% has been delivered. Please visit your <a href=\"%ordersLink%\">orders page</a> and set the order as received. If you would like, you can leave a review and upload a photo of the print to inform future customers about the speed, quality, and communication of the print service.

Best regards,
Treatstock ',
    'Hi %clientName%,
Our system shows that your order #%orderId% has been delivered. Please visit your orders page%ordersLink% and set the order as received.'
 );
");
    }


    public function down()
    {
        $this->delete('email_template', ['code' => 'client.order.delivered']);
        return true;
    }
}?>

Title:

Text:




