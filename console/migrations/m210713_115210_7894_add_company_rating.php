<?php

use yii\db\Migration;

/**
 * Class m210713_115210_add_company_rating
 */
class m210713_115210_7894_add_company_rating extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ps_catalog}}', 'rating', $this->integer()->defaultValue(0));
        $this->addColumn('{{%ps_catalog}}', 'rating_log', $this->text());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ps_catalog}}', 'rating');
        $this->dropColumn('{{%ps_catalog}}', 'rating_log');

    }
}
