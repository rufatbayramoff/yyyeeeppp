<?php

use yii\db\Migration;

class m160225_105718_drop_payment_detail_process extends Migration
{
    public function up()
    {
        $this->dropTable('payment_detail_process');
    }

    public function down()
    {
        $this->execute("
        CREATE TABLE `payment_detail_process` (
          `id` int(11) NOT NULL,
          `processor` char(15) NOT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          `status` char(15) NOT NULL,
          `amount` decimal(9,4) NOT NULL,
          `currency` char(3) NOT NULL,
          `transaction_id` char(32) DEFAULT NULL,
          `payment_detail_id` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }
}
