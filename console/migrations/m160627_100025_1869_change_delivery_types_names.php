<?php

use yii\db\Migration;

class m160627_100025_1869_change_delivery_types_names extends Migration
{
    public function safeUp()
    {
        $this->update('delivery_type', ['title' => 'Domestic Shipping'], ['id' => 2]);
        $this->update('delivery_type', ['title' => 'International Shipping'], ['id' => 5]);
    }

    public function safeDown()
    {
        $this->update('delivery_type', ['title' => 'Domestic shipping'], ['id' => 2]);
        $this->update('delivery_type', ['title' => 'International shipping'], ['id' => 5]);
        return true;
    }
}
