<?php

use yii\db\Migration;

class m160812_161223_1644_alter_model3d_replica_store_unit extends Migration
{
    public function up()
    {
        $this->addColumn('model3d_replica', 'store_unit_id', 'integer(11) NOT NULL AFTER `title`');
        $this->addForeignKey('model3d_replica_store_unit_id', 'model3d_replica', 'store_unit_id', 'store_unit', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('model3d_replica_store_unit_id', 'model3d_replica');
        $this->dropColumn('model3d_replica', 'store_unit_id');
    }
}
