<?php

use yii\db\Migration;

class m160602_103944_usernamenull extends Migration
{
    public function up()
    {
        //$this->execute("ALTER TABLE `user` CHANGE COLUMN `username` `username` VARCHAR(255) CHARACTER SET 'utf8' NULL ;");
        $this->alterColumn('user', 'username', "VARCHAR(255) NULL ");
    }

    public function down()
    {
        $this->alterColumn('user', 'username', "VARCHAR(255) NOT NULL ");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
