<?php

use yii\db\Schema;
use yii\db\Migration;

class m150629_193122_geo_vendor_results extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->renameColumn('{{%geo_location_vendor_results}}', 'response', 'response_address');
        $this->alterColumn('{{%geo_location_vendor_results}}', 'response_address', 'text NOT NULL');
        $this->addColumn('{{%geo_location_vendor_results}}', 'response_location', 'TEXT NOT NULL AFTER `response_address` ');
        $this->addColumn('{{%geo_location_vendor_results}}', 'user_input', 'string(250) NOT NULL AFTER `vendor_id` ');
        $this->addColumn('{{%geo_location_vendor_results}}', 'using_map', "ENUM('address','html5','drag','') NOT NULL AFTER `response_location` ");
    }

    public function safeDown()
    {
        $this->renameColumn('{{%geo_location_vendor_results}}', 'response_address', 'response');
        $this->alterColumn('{{%geo_location_vendor_results}}', 'response', 'string(250) NOT NULL');
        $this->dropColumn('{{%geo_location_vendor_results}}', 'response_location');
        $this->dropColumn('{{%geo_location_vendor_results}}', 'user_input');
        $this->dropColumn('{{%geo_location_vendor_results}}', 'using_map');
    }
}
