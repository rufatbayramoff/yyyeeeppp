<?php

use common\components\ArrayHelper;
use common\models\Model3dReplica;
use yii\db\Migration;
use yii\db\Query;

class m160812_172921_1644_alter_model3d_part_id_fields extends Migration
{
    public function up()
    {
        // Add to store oreder item model3d_replica
        $this->delete('model3d_replica_part');
        $this->delete('model3d_replica');
        $storeOrders = (new Query())
            ->select('*')
            ->from('store_order')
            ->all();
        $storeOrders = ArrayHelper::index($storeOrders, 'id');
        $storeOrderItems = (new Query())
            ->select('*')
            ->from('store_order_item')
            ->all();
        $storeOrderItems = ArrayHelper::index($storeOrderItems, 'id');
        $storeOrderModel3dParts = (new Query())
            ->select('*')
            ->from('store_order_model3d_file')
            ->all();
        $storeOrderModel3dParts = ArrayHelper::index($storeOrderModel3dParts, 'id');
        $model3ds = (new Query())
            ->select('*')
            ->from('model3d')
            ->all();
        $model3ds = ArrayHelper::index($model3ds, 'id');
        $model3dParts = (new Query())
            ->select('*')
            ->from('model3d_part')
            ->all();
        $model3dParts = ArrayHelper::index($model3dParts, 'id');
        $storeUnits = (new Query())
            ->select('*')
            ->from('store_unit')
            ->all();
        $storeUnits = ArrayHelper::index($storeUnits, 'id');
        $printerMaterials = (new Query())
            ->select('*')
            ->from('printer_material')
            ->all();
        $printerMaterials = ArrayHelper::map($printerMaterials, 'filament_title', 'id');
        $printerColors = (new Query())
            ->select('*')
            ->from('printer_color')
            ->all();
        $printerColors = ArrayHelper::map($printerColors, 'title', 'id');
        $model3dPartProperties = (new Query())
            ->select('*')
            ->from('model3d_part_properties')
            ->all();
        $model3dPartProperties = ArrayHelper::index($model3dPartProperties, 'id');

        foreach ($storeOrderItems as $storeOrderItem) {
            $storeOrder = $storeOrders[$storeOrderItem['order_id']];
            $storeOrderItemId = $storeOrderItem['id'];
            $storeUnit = $storeUnits[$storeOrderItem['unit_id']];

            $texture = [
                'printer_material_id'=>$printerMaterials[$storeOrderItem['material']],
                'printer_color_id'=>$printerColors[$storeOrderItem['color']],
            ];
            $this->insert('model3d_texture', $texture);
            $texture['id'] = $this->db->getLastInsertID();

            $model3dReplica = $model3ds[$storeUnit['model3d_id']];
            $model3dReplica['original_model3d_id'] = $model3dReplica['id'];
            $model3dReplica['model3d_texture_id'] = $texture['id'];
            $model3dReplica['store_unit_id'] = $storeOrderItem['unit_id'];
            $model3dReplica['user_id'] = $storeOrder['user_id'];
            unset($model3dReplica['id']);
            $this->insert('model3d_replica', $model3dReplica);
            $model3dReplica['id'] = $this->db->getLastInsertID();

            $storeOrderParts = array_filter(
                $storeOrderModel3dParts,
                function ($storeOrderPart) use ($storeOrderItemId) {
                    if ($storeOrderPart['order_item_id'] == $storeOrderItemId) {
                        return true;
                    }
                    return false;
                }
            );
            foreach ($storeOrderParts as $storeOrderPart) {
                $orderPart = $model3dParts[$storeOrderPart['model3d_file_id']];

                if ($orderPart['model3d_part_properties_id']) {
                    $partProperties = $model3dPartProperties[$orderPart['model3d_part_properties_id']];
                    unset($partProperties['id']);
                    $this->insert('model3d_part_properties', $partProperties);
                    $partPropertiesId = $this->db->getLastInsertID();
                } else {
                    $partPropertiesId = null;
                }

                $model3dReplicaPart = $orderPart;
                $model3dReplicaPart['original_model3d_part_id'] = $model3dReplicaPart['id'];
                $model3dReplicaPart['model3d_replica_id'] = $model3dReplica['id'];
                $model3dReplicaPart['model3d_part_properties_id'] = $partPropertiesId;
                unset($model3dReplicaPart['id'], $model3dReplicaPart['model3d_id']);
                $this->insert('model3d_replica_part', $model3dReplicaPart);
            }
            $this->update('store_order_item', ['model3d_replica_id'=>$model3dReplica['id']], ['id'=>$storeOrderItemId]);
        }

        $this->delete('store_order_model3d_file');
    }

    public
    function down()
    {
        echo "m160812_172921_1644_alter_model3d_part_id_fields cannot be reverted.\n";
        return false;
    }
}
