<?php

use yii\db\Migration;

class m170710_121358_4290_default_seo_tpl extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `seo_page_autofill_template` 
            ADD COLUMN `is_apply_created` BIT(1) NULL DEFAULT b\'0\' AFTER `is_default`;');

        $this->execute('ALTER TABLE `seo_page`  ADD COLUMN `need_review` BIT(1) NULL DEFAULT b\'0\' AFTER `is_locked`;');
    }

    public function safeDown()
    {
        $this->dropColumn('seo_page_autofill_template', 'is_apply_created');
        $this->dropColumn('seo_page', 'need_review');

    }

}
