<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_141916_payment_detail_user extends Migration
{
    public function up()
    {
        $this->dropColumn('payment_detail', 'status');
        
        // add strip user for payment_detail
        $this->insert("user", [
            "id" => 165,
            "username" => "stripe",
            "auth_key" => "",
            'password_hash' => '',
            'email' => 'stripe@treatstock.com',
            'status' => 0,
            'created_at' => 0,
            'updated_at' => 0,
            'lastlogin_at' => 0,
            'trustlevel' => 'high'
        ]);
    }

    public function down()
    {
        $this->addColumn('payment_detail', 'status', 'CHAR (25)');        
        $this->delete('user', 'id IN(65)');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
