<?php

use yii\db\Migration;

class m170615_133858_postal_label_url extends Migration
{
    public function up()
    {
        $this->addColumn("store_order_attemp_delivery", "label_url", "TINYTEXT DEFAULT NULL");
    }

    public function down()
    {
        $this->dropColumn("store_order_attemp_delivery", "label_url");
        return true;
    }
}
