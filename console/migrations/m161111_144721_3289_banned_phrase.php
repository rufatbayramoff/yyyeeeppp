<?php

use yii\db\Query;
use yii\db\Schema;
use yii\db\Migration;

class m161111_144721_3289_banned_phrase extends Migration
{
    public function safeUp()
    {

        $this->execute(
<<<SQL
CREATE TABLE IF NOT EXISTS `content_filter_banned_phrase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase` varchar(1024) NOT NULL,
  `lang_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lang_id` (`lang_id`),
  CONSTRAINT `fk_content_filter_banned_phrase` FOREIGN KEY (`lang_id`) REFERENCES `system_lang` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
    );
        $enLangId = (new Query())
            ->select('id')
            ->from('system_lang')
            ->where(['iso_code' => 'en-US'])
            ->one()['id'];
        /*
        $list = app('setting')->get('site.bannedWords');
        foreach ($list as $listElement) {
            $this->insert(
                'content_filter_banned_phrase',
                [
                    'phrase' => $listElement,
                    'lang_id' => $enLangId
                ]
            );
        } */
        $this->delete('system_setting', '`key`="bannedWords"');
    }

    public function safeDown()
    {
        $this->dropTable('content_filter_banned_phrase');
    }
}

