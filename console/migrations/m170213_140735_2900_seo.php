<?php

use yii\db\Migration;

class m170213_140735_2900_seo extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `seo_page` CHANGE COLUMN `title` `title` CHAR(120) NULL DEFAULT NULL ;');
    }

    public function down()
    {
        $this->dropColumn('seo_page', 'title');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
