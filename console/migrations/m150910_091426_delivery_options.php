<?php

use yii\db\Schema;
use yii\db\Migration;

class m150910_091426_delivery_options extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->execute('TRUNCATE TABLE ps_printer_delivery;');
        $this->execute('TRUNCATE TABLE delivery_type;');
        $this->execute('ALTER TABLE `ps_printer_delivery` DROP COLUMN `price_world`, DROP COLUMN `price_local`;');
        $this->execute('ALTER TABLE `delivery_type` ADD COLUMN `code` VARCHAR(45) NOT NULL AFTER `title`;');
        $this->execute('ALTER TABLE `delivery_type` CHANGE COLUMN `is_payment` `is_work_time` TINYINT(1) NULL;');
        $this->execute('ALTER TABLE `delivery_type` ADD COLUMN `is_free_delivery` TINYINT(1) NULL DEFAULT NULL AFTER `code`;');

        $this->execute("INSERT INTO `delivery_type` (`id`,`title`,`code`,`is_free_delivery`,`is_work_time`,`is_active`,`created_at`,`updated_at`)
                VALUES (1,'Customer Pickup','pickup',NULL,1,1,'2015-08-18 08:34:08','2015-08-18 08:34:08');");
        $this->execute("INSERT INTO `delivery_type` (`id`,`title`,`code`,`is_free_delivery`,`is_work_time`,`is_active`,`created_at`,`updated_at`)
                VALUES (2,'Standard shipping (2-8 business days)','standard',NULL,0,1,'2015-08-18 08:35:02','2015-08-18 08:35:02');");
        $this->execute("INSERT INTO `delivery_type` (`id`,`title`,`code`,`is_free_delivery`,`is_work_time`,`is_active`,`created_at`,`updated_at`)
                VALUES (3,'Expedited shipping (1-3 business days)','expedited',NULL,0,1,'2015-08-18 08:36:38','2015-08-18 08:36:38');");
        $this->execute("INSERT INTO `delivery_type` (`id`,`title`,`code`,`is_free_delivery`,`is_work_time`,`is_active`,`created_at`,`updated_at`)
                VALUES (4,'One day shipping (24 hours)','one_day',NULL,0,1,'2015-08-18 08:37:09','2015-08-18 08:37:09');");
        $this->execute("INSERT INTO `delivery_type` (`id`,`title`,`code`,`is_free_delivery`,`is_work_time`,`is_active`,`created_at`,`updated_at`)
                VALUES (5,'International shipping (3-6 weeks)','intl',NULL,0,1,'2015-09-10 05:25:17','2015-09-10 05:25:17');");
        $this->execute("INSERT INTO `delivery_type` (`id`,`title`,`code`,`is_free_delivery`,`is_work_time`,`is_active`,`created_at`,`updated_at`)
                VALUES (6,'International expedited shipping (3-7 business days)','intl_expedited',NULL,0,1,'2015-09-10 05:25:17','2015-09-10 05:25:17');");
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->execute('TRUNCATE TABLE delivery_type;');
        $this->execute('ALTER TABLE `ps_printer_delivery` ADD COLUMN `price_local` DECIMAL(5,2) NULL AFTER `comment`;');
        $this->execute('ALTER TABLE `ps_printer_delivery` ADD COLUMN `price_world` DECIMAL(5,2) NULL AFTER `price_local`;');
        $this->execute('ALTER TABLE `delivery_type` DROP COLUMN `code`;');
        $this->execute('ALTER TABLE `delivery_type` CHANGE COLUMN `is_work_time` `is_payment` TINYINT(1) NULL;');
        $this->execute('ALTER TABLE `delivery_type` DROP COLUMN `is_free_delivery`;');
    }
}
