<?php

use yii\db\Migration;
use yii\db\Query;

class m160915_164543_2905_fix_incorrect_path extends Migration
{
    public function up()
    {
        $isPublicStl = (new Query())
            ->select('*')
            ->from('file')
            ->where('path like "/static/files/%" and server="localhost" and path_version="v2" and is_public=0')->all();
        foreach ($isPublicStl as $onePublicStlFile) {
            $name = $onePublicStlFile['id'].'.'.$onePublicStlFile['extension'];
            $md5 = md5($name);
            $currentPath = $onePublicStlFile['path'];
            $newPath = '/files/' . substr($md5, 0, 2) . '/' . substr($md5, 2, 2);
            $fullCurrentPath = Yii::getAlias('@localhost') . $currentPath . '/' . $name;
            $fullNewPath = Yii::getAlias('@storage') . $newPath . '/' . $name;
            if (file_exists($fullCurrentPath)) {
                $newDirName = dirname($fullNewPath);
                mkdir($newDirName, 0777, true);
                $this->update('file', ['path' => $newPath, 'server' => 'storage'], ['id'=>$onePublicStlFile['id']]);
                rename($fullCurrentPath, $fullNewPath);
            }
        }
    }

    public function down()
    {

    }
}
