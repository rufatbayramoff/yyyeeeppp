<?php

use yii\db\Migration;

class m170110_083347_3393_fix_old_aff extends Migration
{
    public function up()
    {
        $i = 0;
        if(class_exists('\common\models\base\User')){
            $users = \common\models\User::find()
                ->leftJoin('affiliate_user_source', 'affiliate_user_source.user_id=user.id')
                ->where('affiliate_user_source.id is NULL')->andWhere('user.id>1000')->all();


            foreach($users as $k=>$user)
            {
                /**
                 * @var $user User
                 */
                \common\models\AffiliateUserSource::addRecord([
                    'user_id' => $user->id,
                    'created_at' => date('Y-m-d H:i:s', $user->created_at),
                    'resource_id' => 1,
                    'type' => 'register',
                    'status' => 'active',
                    'first_url' => 'https://www.treatstock.com',
                    'user_agent' => 'Mozilla',
                    'ip' => '0.0.0.0'
                ]);
                $i++;
            }
        }
        echo "$i updated\n";
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
