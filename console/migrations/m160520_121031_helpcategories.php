<?php

use yii\db\Migration;

class m160520_121031_helpcategories extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `site_help_category` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `parent_id` INT(11) NULL DEFAULT NULL,
            `title` VARCHAR(500) NOT NULL,
            `info` TEXT NOT NULL,
            `slug` CHAR(150) NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            INDEX `FK_site_help_category_site_help_category` (`parent_id`),
            CONSTRAINT `FK_site_help_category_site_help_category` FOREIGN KEY (`parent_id`) REFERENCES `site_help_category` (`id`)
        )
        ENGINE=InnoDB;");
    }

    public function down()
    {
        $this->dropTable("site_help_category");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
