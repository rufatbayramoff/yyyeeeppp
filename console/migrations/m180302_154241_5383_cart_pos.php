<?php

use yii\db\Migration;

/**
 * Class m180302_154241_5383_cart_pos
 */
class m180302_154241_5383_cart_pos extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('cart', 'posUid', 'varchar(60) null');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('cart', 'posUid');
        return true;
    }
}
