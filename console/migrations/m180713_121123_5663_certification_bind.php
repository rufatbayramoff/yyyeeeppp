<?php

use yii\db\Migration;

/**
 * Class m180713_121123_5663_certification_bind
 */
class m180713_121123_5663_certification_bind extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            'CREATE TABLE `company_certification_bind` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_uuid` varchar(32) DEFAULT NULL,
  `company_service_id` int(11) DEFAULT NULL,
  `certification_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index5` (`product_uuid`,`certification_id`),
  KEY `fk_company_certification_bind_1` (`certification_id`),
  KEY `fk_company_certification_bind_2_idx` (`company_service_id`),
  KEY `index4` (`product_uuid`),
  CONSTRAINT `fk_company_certification_bind_1` FOREIGN KEY (`certification_id`) REFERENCES `company_certification` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_certification_bind_2` FOREIGN KEY (`company_service_id`) REFERENCES `company_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_certification_bind_3` FOREIGN KEY (`product_uuid`) REFERENCES `product` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('DROP TABLE IF EXISTS company_certification_bind;');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180713_121123_5663_certification_bind cannot be reverted.\n";

        return false;
    }
    */
}
