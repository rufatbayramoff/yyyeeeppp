<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_132449_usertaxinfo extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_tax_info` 
            CHANGE COLUMN `identification` `identification` BLOB NULL DEFAULT NULL;");
        
        $this->execute("ALTER TABLE `user_tax_info` 
                ADD COLUMN `encrypted` TINYINT(1) NULL DEFAULT 0 AFTER `sign_type`;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `user_tax_info` 
            CHANGE COLUMN `identification` `identification` VARCHAR(45) NULL DEFAULT NULL ;
        ");
        
        $this->dropColumn('user_tax_info', 'encrypted');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
