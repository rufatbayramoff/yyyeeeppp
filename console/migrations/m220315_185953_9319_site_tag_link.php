<?php

use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m220315_185953_9319_site_tag_link
 */
class m220315_185953_9319_site_tag_link extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute("CREATE TABLE `site_tag_link` (
  `id` int NOT NULL AUTO_INCREMENT,
  `site_tag_one` int NOT NULL,
  `site_tag_two` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_tag_one` (`site_tag_one`),
  KEY `site_tag_two` (`site_tag_two`),
  CONSTRAINT `fk_site_tag_one` FOREIGN KEY (`site_tag_one`) REFERENCES `site_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_site_tag_two` FOREIGN KEY (`site_tag_two`) REFERENCES `site_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
");
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
