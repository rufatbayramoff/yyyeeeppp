<?php

use yii\db\Migration;

/**
 * Class m180124_135642_5259_sort_of_reject_reasons
 */
class m180124_135642_5259_sort_of_reject_reasons extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('system_reject', 'priority', 'INT(11) NOT NULL DEFAULT 0');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('system_reject', 'priority');
        return true;
    }
}
