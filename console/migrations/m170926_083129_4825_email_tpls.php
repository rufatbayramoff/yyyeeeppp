<?php

use yii\db\Migration;

class m170926_083129_4825_email_tpls extends Migration
{
    public function safeUp()
    {
        $this->insert('email_template', [
            'code' => 'ps.order.openDispute',
            'group' => 'service',
            'language_id' => 'en-US',
            'title' => 'A dispute has been opened for order #%orderId%',
            'description' => '',
            'template_html' => nl2br('Hi %psName%,
            
A dispute has been opened by the customer for order #%orderId%. 
Please contact the customer for more information and to try and resolve the dispute. 
You may contact Treatstock support if you need any assistance.
            
Best Regards, 
Treatstock'),
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);

        $this->insert('email_template', [
            'code' => 'ps.order.openDisputeThingiverse',
            'group' => 'service',
            'language_id' => 'en-US',
            'title' => 'A dispute has been opened for order #%orderId%',
            'description' => '',
            'template_html' => nl2br('Hi %psName%,
            
A dispute has been opened by the customer for order #%orderId%. 
Please contact Treatstock support for more information.
            
Best Regards, 
Treatstock'),
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);

        $this->insert('email_template', [
            'code' => 'ps.order.closedDispute',
            'group' => 'service',
            'language_id' => 'en-US',
            'title' => 'Dispute has been closed for order #%orderId%',
            'description' => '',
            'template_html' => nl2br('Hi %psName%,

The dispute has been resolved and is now closed for order #%orderId%. We thank you for your efforts!

Best Regards, 
Treatstock'),
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);
    }

    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'ps.order.openDispute']);
        $this->delete('email_template', ['code' => 'ps.order.openDisputeThingiverse']);
        $this->delete('email_template', ['code' => 'ps.order.closedDispute']);
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170926_083129_4825_email_tpls cannot be reverted.\n";

        return false;
    }
    */
}
