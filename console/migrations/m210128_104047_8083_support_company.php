<?php
use yii\db\Migration;
use yii\db\Query;

class m210128_104047_8083_support_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tsPs = (new Query())->select('*')->from('ps')->where(['user_id' => 1])->one();
        if (!$tsPs) {
            $this->insert('ps', [
                'user_id'           => 1,
                'title'             => 'Treatstock Inc.',
                'description'       => 'Treatstock provides distributed manufacturing services for the global market and technology infrastructure to optimize the way companies market, sell and operate online.',
                'phone_code'        => '7',
                'phone'             => '9377711502',
                'moderator_status'  => 'checked',
                'created_at'        => '2017-04-21 09:54:42',
                'updated_at'        => '2017-04-21 09:54:42',
                'phone_status'      => 'checked',
                'phone_country_iso' => 'RU',
                'url'               => 'https://www.treatstock.com',
                'country_id'        => '233',
                'currency'          => 'USD'
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
