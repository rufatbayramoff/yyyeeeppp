<?php

use yii\db\Migration;

/**
 * Class m180305_132121_5376_wiki_material_code
 */
class m180305_132121_5376_wiki_material_code extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('wiki_material', 'code', 'VARCHAR(50) NOT NULL');
        $this->insert('biz_industry', ['id' => 5, 'title' => 'Engineering', 'photo_url' => '/static/images/common/main-page/industry-engineering.jpg']);
        $this->insert('biz_industry', ['id' => 6, 'title' => 'Dentistry', 'photo_url' => '/static/images/common/main-page/industry-dentistry.jpg']);
        $this->insert('biz_industry', ['id' => 7, 'title' => 'Jewelry', 'photo_url' => '/static/images/common/main-page/industry-jewelry.jpg']);
        $this->insert('biz_industry', ['id' => 8, 'title' => 'Education', 'photo_url' => '/static/images/common/main-page/industry-education.jpg']);
        $this->insert('biz_industry', ['id' => 9, 'title' => 'Research', 'photo_url' => '/static/images/common/main-page/industry-research.jpg']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('biz_industry', 'id in (5,6,7,8,9)');
    }
}
