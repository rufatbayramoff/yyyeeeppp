<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_073903_model3d_stats extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `model3d`  ADD COLUMN `stat_views` INT(5) NOT NULL DEFAULT 1 AFTER `default_material`;");
    }

    public function down()
    {
        $this->dropColumn('model3d', 'stat_views');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
