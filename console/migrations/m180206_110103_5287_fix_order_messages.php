<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180206_110103_5287_fix_order_messages
 */
class m180206_110103_5287_fix_order_messages extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $topics = (new Query())
            ->select(['id', 'title'])
            ->from('msg_topic')
            ->where(['like', 'title', 'Order #'])
            ->andWhere(['bind_to' => null, 'creator_id' => 1])
            ->all();

        foreach ($topics as $topic) {
            $orderId = (int) str_replace('Order #', '', $topic['title']);
            $this->update('msg_topic', ['bind_to' => 'order', 'bind_id' => $orderId], ['id' => $topic['id']]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return true;
    }
}
