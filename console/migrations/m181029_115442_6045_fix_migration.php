<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m181029_115442_6045_fix_migration
 */
class m181029_115442_6045_fix_migration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       # $this->renameColumn('product_delivery', 'uuid', 'product_uuid');
        $this->addColumn('product_delivery', 'uuid', 'varchar(32) first');
        $productDeliveryRows = (new Query())->select('*')->from('product_delivery')->all();
        foreach ($productDeliveryRows as $productDeliveryRow) {
            $uuid = \common\components\UuidHelper::generateUuid();
            $this->update('product_delivery', ['uuid' => $uuid], ['product_uuid' => $productDeliveryRow['product_uuid']]);
        }
        $this->execute("ALTER TABLE `product_delivery` DROP `id`, ADD INDEX `product_uuid_indx` (`product_uuid`) USING BTREE;");
        $this->execute("ALTER TABLE `product_delivery` ADD PRIMARY KEY (`uuid`);");

        $this->renameColumn('product_delivery', 'express_delivery_country_id', 'country_id');
        $this->renameColumn('product_delivery', 'express_delivery_first_item', 'first_item');
        $this->renameColumn('product_delivery', 'express_delivery_following_item', 'following_item');

        $this->renameTable('product_delivery', 'product_express_delivery');
        $this->execute("
CREATE TABLE `product_wholesale_delivery` (
  `uuid` varchar(32) NOT NULL,
  `product_uuid` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`uuid`) USING BTREE,
  KEY `product_uuid_indx` (`product_uuid`) USING BTREE,
  CONSTRAINT `fk_product_wholesale_delivery_` FOREIGN KEY (`product_uuid`) REFERENCES `product` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181029_115442_6045_fix_migration cannot be reverted.\n";

        return false;
    }
    */
}
