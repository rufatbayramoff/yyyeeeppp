<?php

use yii\db\Migration;

/**
 * Class m210724_173912_8797_min_order_price
 */
class m210724_173912_8797_min_order_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('ps_printer', ['min_order_price'=>1], 'min_order_price is null or min_order_price<1');
    }
}