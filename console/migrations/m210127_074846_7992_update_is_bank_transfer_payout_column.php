<?php

use yii\db\Migration;

/**
 * Class m210127_074846_update_is_bank_transfer_payout_column
 */
class m210127_074846_7992_update_is_bank_transfer_payout_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $countries = $this->countries();
        $this->update('geo_country', ['is_bank_transfer_payout' => 1], ['not in', 'id', $countries]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update('geo_country', ['is_bank_transfer_payout' => 0]);
    }

    private function countries():array
    {
        return [
            16,
            23,
            32,
            35,
            39,
            41,
            46,
            57,
            66,
            106,
            107,
            118,
            120,
            136,
            142,
            164,
            170,
            181,
            196,
            207,
            212,
            213,
            215,
            217,
            222,
            245,
            249,
        ];
    }
}
