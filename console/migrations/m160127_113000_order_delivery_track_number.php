<?php

use yii\db\Schema;
use yii\db\Migration;

class m160127_113000_order_delivery_track_number extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `store_order_delivery` CHANGE `tracking_number` `tracking_number` VARCHAR(45)  CHARACTER SET utf8  NULL  DEFAULT NULL;");
    }

    public function down()
    {
        return true;
    }
}
