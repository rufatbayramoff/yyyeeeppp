<?php

use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m220405_105421_9363_quote_interception
 */
class m220405_105421_9363_quote_interception extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('user', [
            'id'                  => 35,
            'uid'                 => 'lW7J5w',
            'username'            => 'customer_service',
            'can_change_username' => '0',
            'auth_key'            => '-',
            'password_hash'       => '$2y$13$pLD.pKMWByJAjtRIXdUJ2OUe.6kRmy9wDX9FvAhbwLgzxO7DwAbVa',
            'email'               => 'support+customer_service@treatstock.com',
            'status'              => 1,
            'created_at'          => '2022-04-01 10:10:10',
            'updated_at'          => '2022-04-01 10:10:10',
            'lastlogin_at'        => '2022-04-01 10:10:10',
            'trustlevel'          => 'high',
        ]);

        $this->insert('user_profile', [
            'user_id'              => 35,
            'full_name'            => 'Customer service Treatstock',
            'address_id'           => 1,
            'updated_at'           => '2022-04-01 10:10:10',
            'current_lang'         => 'en-US',
            'current_currency_iso' => 'USD',
            'current_metrics'      => 'cm',
            'timezone_id'          => 'America/Los_Angeles',
            'avatar_url'           => '/user/c4ca4238a0b923820dcc509a6f75849b/avatar_1484318770.png',
            'background_url'       => '/user/c4ca4238a0b923820dcc509a6f75849b/cover_1478088128.png',
        ]);
        $this->insert('payment_account', [
            'id'       => 30,
            'type'     => 'main',
            'currency' => 'USD',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 31,
            'type'     => 'award',
            'currency' => 'USD',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 32,
            'type'     => 'manufactureAward',
            'currency' => 'USD',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 33,
            'type'     => 'reserved',
            'currency' => 'USD',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 34,
            'type'     => 'authorize',
            'currency' => 'USD',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 35,
            'type'     => 'tax',
            'currency' => 'USD',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 36,
            'type'     => 'invoice',
            'currency' => 'USD',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 37,
            'type'     => 'refund',
            'currency' => 'USD',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 38,
            'type'     => 'refundRequest',
            'currency' => 'USD',
            'user_id'  => 35
        ]);


        $this->insert('payment_account', [
            'id'       => 231,
            'type'     => 'main',
            'currency' => 'EUR',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 232,
            'type'     => 'award',
            'currency' => 'EUR',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 233,
            'type'     => 'manufactureAward',
            'currency' => 'EUR',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 234,
            'type'     => 'reserved',
            'currency' => 'EUR',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 235,
            'type'     => 'authorize',
            'currency' => 'EUR',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 236,
            'type'     => 'tax',
            'currency' => 'EUR',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 237,
            'type'     => 'invoice',
            'currency' => 'EUR',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 238,
            'type'     => 'refund',
            'currency' => 'EUR',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 239,
            'type'     => 'refundRequest',
            'currency' => 'EUR',
            'user_id'  => 35
        ]);


        $this->insert('payment_account', [
            'id'       => 331,
            'type'     => 'main',
            'currency' => 'BNS',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 332,
            'type'     => 'award',
            'currency' => 'BNS',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 333,
            'type'     => 'manufactureAward',
            'currency' => 'BNS',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 334,
            'type'     => 'reserved',
            'currency' => 'BNS',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 335,
            'type'     => 'authorize',
            'currency' => 'BNS',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 336,
            'type'     => 'tax',
            'currency' => 'BNS',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 337,
            'type'     => 'invoice',
            'currency' => 'BNS',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 338,
            'type'     => 'refund',
            'currency' => 'BNS',
            'user_id'  => 35
        ]);
        $this->insert('payment_account', [
            'id'       => 339,
            'type'     => 'refundRequest',
            'currency' => 'BNS',
            'user_id'  => 35
        ]);


        $this->insert('user_location', [
            'id'            => 1,
            'user_id'       => 35,
            'country_id'    => 233,
            'region_id'     => 3600,
            'city_id'       => 2623861,
            'address'       => '3967 Savannah Ct',
            'lat'           => '37.64992',
            'lon'           => '-122.46431',
            'zip_code'      => '94080',
            'location_type' => 'delivery'
        ]);


        $this->insert('ps', [
            'id'                => 34,
            'user_id'           => 35,
            'title'             => 'Customer service',
            'description'       => 'Treatstock customer service',
            'logo'              => 'ps_logo_1483192536.png',
            'phone_code'        => '39',
            'phone'             => '5064101271',
            'moderator_status'  => 'checked',
            'moderated_at'      => '2022-04-01 10:10:10',
            'created_at'        => '2022-04-01 10:10:10',
            'updated_at'        => '2022-04-01 10:10:10',
            'phone_status'      => 'checked',
            'phone_country_iso' => 'US',
            'sms_gateway'       => 'cmsms',
            'logo_circle'       => 'ps_logo.png',
            'url'               => 'customer-service',
            'website'           => 'https://www.treatstock.com',
            'country_id'        => 233,
            'currency'          => 'usd',
            'is_backend'        => 1,
            'location_id'       => 1,
        ]);
        $this->addColumn('preorder', 'is_interception', 'tinyint(1) not null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('ps', 'id=34');
        $this->delete('payment_account', 'id=30');
        $this->delete('payment_account', 'id=231');
        $this->delete('payment_account', 'id=331');
        $this->delete('user_session', 'user_id=35');
        $this->delete('user_statistics', 'user_id=35');
        $this->delete('user', 'id=35');
        $this->dropColumn('preorder', 'is_interception');
    }

}
