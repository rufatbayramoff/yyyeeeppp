<?php

use yii\db\Schema;
use yii\db\Migration;

class m150925_115328_store_order_delivery extends Migration
{
    public function safeUp()
    {
        $this->execute("CREATE TABLE `store_order_delivery` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `order_id` INT(11) NOT NULL,
                `delivery_type_id` INT(11) NULL,
                `tracking_number` VARCHAR(45) NOT NULL,
                `status` VARCHAR(45) NOT NULL,
                `created_at` TIMESTAMP NOT NULL,
                PRIMARY KEY (`id`),
                INDEX `store_order_id_index` (`order_id`),
                INDEX `store_delivery_type_index` (`delivery_type_id`),
                UNIQUE INDEX `store_order_unique` (`order_id`, `delivery_type_id`),
                CONSTRAINT `order_delivery_fk`
                    FOREIGN KEY (`order_id`)
                    REFERENCES `store_order` (`id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION,
                CONSTRAINT `delivery_type_order_fk`
                    FOREIGN KEY (`delivery_type_id`)
                    REFERENCES `delivery_type` (`id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB
                DEFAULT CHARACTER SET = utf8
                COLLATE = utf8_general_ci;"
        );
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE store_order_delivery DROP FOREIGN KEY `order_delivery_fk`;');
        $this->execute('ALTER TABLE store_order_delivery DROP FOREIGN KEY `delivery_type_order_fk`;');
        $this->execute('ALTER TABLE store_order_delivery DROP KEY `store_order_id_index`;');
        $this->execute('ALTER TABLE store_order_delivery DROP KEY `store_delivery_type_index`;');
        $this->execute('ALTER TABLE store_order_delivery DROP KEY `store_order_unique`;');
        $this->execute('DROP TABLE store_order_delivery;');
    }
}
