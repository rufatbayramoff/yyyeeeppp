<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_151309_ps_printer_is_location_change extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer` ADD `is_location_change` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `is_deleted`");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `ps_printer` DROP `is_location_change`;");
        return true;
    }

}
