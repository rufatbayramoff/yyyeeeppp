<?php

use yii\db\Migration;

class m171227_170541_4145_refund2 extends Migration
{
    public function safeUp()
    {
        $this->execute(
            'ALTER TABLE `payment_transaction_refund` 
ADD COLUMN `refund_type` VARCHAR(45) NULL AFTER `status`,
ADD COLUMN `from_user_id` INT(11) NULL AFTER `refund_type`;
'
        );
        $this->execute(
            'ALTER TABLE `payment_transaction_refund` 
ADD INDEX `fk_payment_transaction_refund_22_idx` (`from_user_id` ASC);
ALTER TABLE `payment_transaction_refund` 
ADD CONSTRAINT `fk_payment_transaction_refund_22`
  FOREIGN KEY (`from_user_id`)
  REFERENCES `user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
'
        );
    }

    public function safeDown()
    {
        $this->dropColumn('payment_transaction_refund', 'refund_type');
        $this->dropForeignKey('fk_payment_transaction_refund_22', 'payment_transaction_refund');
        $this->dropIndex('fk_payment_transaction_refund_22_idx', 'payment_transaction_refund');
        $this->dropColumn('payment_transaction_refund', 'from_user_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171227_170541_4145_refund2 cannot be reverted.\n";

        return false;
    }
    */
}
