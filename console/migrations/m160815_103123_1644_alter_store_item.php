<?php

use common\components\ArrayHelper;
use common\models\Model3dReplica;
use yii\db\Migration;
use yii\db\Query;

class m160815_103123_1644_alter_store_item extends Migration
{
    public function up()
    {
        $this->dropColumn('store_order_item', 'material');
        $this->dropColumn('store_order_item', 'color');
        $this->dropTable('store_order_model3d_file');
    }

    public
    function down()
    {
        echo "m160815_103123_1644_alter_store_item cannot be reverted.\n";
        return false;
    }
}
