<?php

use yii\db\Migration;
use yii\db\Query;

class m170712_174121_4225_cnc_service_schema extends Migration
{
    public function up()
    {
        $this->execute(
            "
CREATE TABLE `ps_cnc_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(255) NOT NULL,
  `description` JSON NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
        );
    }

    public function down()
    {
        $this->dropTable('cnc_service');
    }
}
