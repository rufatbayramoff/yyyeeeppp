<?php

use yii\db\Migration;

class m170126_090456_3155_prevoius_attept_id extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `store_order` 
            ADD COLUMN `previous_attemp_id` INT(10) UNSIGNED NULL AFTER `current_attemp_id`,
            ADD INDEX `fk_store_order_6_idx` (`previous_attemp_id` ASC);"
        );

        $this->execute("
            ALTER TABLE `store_order` 
            ADD CONSTRAINT `fk_store_order_6`
              FOREIGN KEY (`previous_attemp_id`)
              REFERENCES `store_order_attemp` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
    }

    public function down()
    {
        $this->execute("
            ALTER TABLE `store_order` 
            DROP FOREIGN KEY `fk_store_order_6`;
        ");

        $this->execute("
            ALTER TABLE `store_order` 
            DROP COLUMN `previous_attemp_id`,
            DROP INDEX `fk_store_order_6_idx` ;
        ");

        return true;
    }
}
