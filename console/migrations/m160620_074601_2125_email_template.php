<?php

use yii\db\Migration;

class m160620_074601_2125_email_template extends Migration
{
    public function up()
    {

        $this->execute("INSERT INTO `email_template`(
    `id`,
    `code`,
    `group`,
    `language_id`,
    `title`,
    `description`,
    `updated_at`,
    `template_html`,
    `template_text`
) VALUES (
    NULL,
    'ps.moderation.reject',
    'service',
    'en-US',
    'Publishing of your %psName% on treatstock.com',
    NULL,
    NULL,
    'Hello %psName%,

The information you have entered about your print service has not been approved by the moderator.  %rejectReason% Please <a href=\"%psLink%\">edit your Print Service</a>

Best regards,
Treatstock',
    'Hello %psName%,

The information you have entered about your print service has not been approved by the moderator.  %rejectReason% Please edit your Print Service %psLink%

Best regards,
Treatstock'
 );
");

    }

    public function down()
    {
        $this->delete('email_template', ['code' => 'ps.moderation.reject']);
        return true;
    }
}
