<?php

use yii\db\Migration;

class m170411_102021_4070_model3dview_state_qty_scale extends Migration
{
    public function up()
    {
        $this->addColumn('model3d_viewed_state', 'qty_info', 'json');
        $this->addColumn('model3d_viewed_state', 'scale_info', 'json');
    }

    public function down()
    {
        $this->dropColumn('model3d_viewed_state', 'qty_info');
        $this->dropColumn('model3d_viewed_state', 'scale_info');
    }
}
