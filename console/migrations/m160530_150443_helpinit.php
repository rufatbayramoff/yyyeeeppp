<?php

use yii\db\Migration;

class m160530_150443_helpinit extends Migration
{
    public function up()
    {
        $this->execute("INSERT IGNORE INTO `site_help` (`user_id`, `alias`, `title`, `content`, `is_active`, `lang_iso`) 
            VALUES (null, 'help.main', 'Help Center', 'Help Center', '1', 'en-US');");
    }

    public function down()
    {
        $this->execute("DELETE FROM site_help WHERE alias='help.main' AND lang_iso='en-US'");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
