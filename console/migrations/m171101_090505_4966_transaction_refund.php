<?php

use yii\db\Migration;

class m171101_090505_4966_transaction_refund extends Migration
{
    public function safeUp()
    {
        $this->addColumn('payment_transaction', 'refund_id', 'varchar(45)');
    }

    public function safeDown()
    {
         $this->dropColumn('payment_transaction', 'refund_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171101_090505_4966_transaction_refund cannot be reverted.\n";

        return false;
    }
    */
}
