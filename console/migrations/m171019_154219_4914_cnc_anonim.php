<?php

use yii\db\Migration;

class m171019_154219_4914_cnc_anonim extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `cnc_preorder` 
            ADD COLUMN `user_session_id` INT NULL AFTER `version`,
            ADD INDEX `fk_cnc_preorder_4_idx` (`user_session_id` ASC);
            ALTER TABLE `cnc_preorder` 
            ADD CONSTRAINT `fk_cnc_preorder_4`
              FOREIGN KEY (`user_session_id`)
              REFERENCES `user_session` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');

        $this->execute('ALTER TABLE `cnc_preorder` 
            DROP FOREIGN KEY `fk_cnc_preorder_1`;
            ALTER TABLE `cnc_preorder` 
            CHANGE COLUMN `customer_user_id` `user_id` INT(11) NOT NULL ;
            ALTER TABLE `cnc_preorder` 
            ADD CONSTRAINT `fk_cnc_preorder_1`
              FOREIGN KEY (`user_id`)
              REFERENCES `user` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_cnc_preorder_4', 'cnc_preorder');
        $this->dropColumn('cnc_preorder', 'user_session_id');

        $this->execute('ALTER TABLE `cnc_preorder` 
            DROP FOREIGN KEY `fk_cnc_preorder_1`;
            ALTER TABLE `cnc_preorder` 
            CHANGE COLUMN `user_id` `customer_user_id` INT(11) NOT NULL ;
            ALTER TABLE `cnc_preorder` 
            ADD CONSTRAINT `fk_cnc_preorder_1`
              FOREIGN KEY (`customer_user_id`)
              REFERENCES `user` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171019_154219_4914_cnc_anonim cannot be reverted.\n";

        return false;
    }
    */
}
