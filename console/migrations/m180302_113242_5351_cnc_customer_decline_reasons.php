<?php

use yii\db\Migration;

/**
 * Class m180302_113242_5351_cnc_customer_decline_reasons
 */
class m180302_113242_5351_cnc_customer_decline_reasons extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_cnc_preorder_customer', 'Changed my mind', NULL, '1' );
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_cnc_preorder_customer', 'Taking too long', NULL, '1' );
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_cnc_preorder_customer', 'Found another manufacturer', NULL, '1' );
        ");
    }

    public function safeDown()
    {
        $this->delete('system_reject', ['group' =>'decline_cnc_preorder_customer']);
        return true;
    }
}
