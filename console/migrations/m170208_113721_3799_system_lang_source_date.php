<?php

use yii\db\Migration;
use yii\db\Query;

class m170208_113721_3799_system_lang_source_date extends Migration
{
    public function up()
    {
        $this->addColumn('system_lang_source', 'date', 'datetime');
        $this->update('system_lang_source', ['date'=>'2017-02-08 00:00:01']);
    }

    public function down()
    {
        $this->dropColumn('system_lang_source', 'date');
    }
}
