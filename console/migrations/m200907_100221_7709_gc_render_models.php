<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m200907_100221_7709_gc_render_models
 */
class m200907_100221_7709_gc_render_models extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `gc_render_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webdavUrl` varchar(4048) NOT NULL,
  `path` varchar(4048) NOT NULL,
  `loaded_at` datetime DEFAULT NULL,
  `processed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropTable('gc_render_models');
    }
}
