<?php

use yii\db\Migration;

/**
 * Class m180824_084429_5829_alter_seo_template
 */
class m180824_084429_5829_alter_seo_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `seo_page_autofill_template` CHANGE COLUMN `meta_keywords` `meta_keywords` VARCHAR(245) NULL;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180824_084429_5829_alter_seo_template cannot be reverted.\n";

        return false;
    }
    */
}
