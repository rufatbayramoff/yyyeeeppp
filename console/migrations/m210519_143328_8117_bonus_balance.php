<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m210519_143328_8117_bonus_balance
 */
class m210519_143328_8117_bonus_balance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `payment_account` CHANGE `type` `type` ENUM('main','authorize','reserved','award','tax','invoice','manufactureAward','refundRequest','discount','easypost','refund','payout','correction','fee', 'convert_bns_eur', 'convert_bns_usd', 'convert_eur_usd') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'main'");
        $this->insert('payment_currency', ['title' => 'Bonus', 'is_active' => 1, 'currency_iso' => 'BNS', 'title_original' => 'Bonus', 'round' => 2]);
        $this->batchInsert('payment_account', ['id', 'user_id', 'type', 'currency'], [
            [102, 1, 'main', 'BNS'], // ACCOUNT_SUPPORT_BNS
            [212, 110, 'main', 'BNS'], // ACCOUNT_TREATSTOCK_BNS
            [221, 120, 'main', 'BNS'], // ACCOUNT_EASYPOST_EUR
            [299, 998, 'main', 'BNS'], // ACCOUNT_ANONIM_EUR

            [315, 110, 'discount', 'BNS'], // ACCOUNT_TREATSTOCK_DISCOUNT_EUR
            [316, 110, 'easypost', 'BNS'], // ACCOUNT_TREATSTOCK_EASYPOST_EUR
            [317, 110, 'correction', 'BNS'], // ACCOUNT_TREATSTOCK_CORRECTION_EUR
            [318, 110, 'convert_bns_eur', 'BNS'], // ACCOUNT_TREATSTOCK_CONVERT_BNS_EUR
            [319, 110, 'convert_bns_eur', 'EUR'], // ACCOUNT_TREATSTOCK_CONVERT_BNS_EUR
            [320, 110, 'convert_bns_usd', 'BNS'], // ACCOUNT_TREATSTOCK_CONVERT_BNS_USD
            [321, 110, 'convert_bns_usd', 'USD'], // ACCOUNT_TREATSTOCK_CONVERT_BNS_USD
            [322, 110, 'convert_eur_usd', 'EUR'], // ACCOUNT_TREATSTOCK_CONVERT_EUR_USD
            [323, 110, 'convert_eur_usd', 'USD'], // ACCOUNT_TREATSTOCK_CONVERT_EUR_USD
        ]);

        $this->execute('ALTER TABLE `payment_detail_operation` DROP FOREIGN KEY `payment_operation_detail_fk`; ALTER TABLE `payment_detail_operation` ADD CONSTRAINT `payment_operation_detail_fk` FOREIGN KEY (`payment_id`) REFERENCES `payment`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $lastPayment = (new Query())->select('*')->from('payment')->orderBy('id desc')->one();
        $lastId      = $lastPayment ? $lastPayment['id'] + 1 : false;
        if ($lastId) {
            $this->update('payment', ['id' => $lastId], 'id=1');
        }
        $this->insert('payment', ['id' => 1, 'created_at' => '2021-01-01', 'description' => 'Correction payment', 'status' => 'processing']);
        $this->execute("ALTER TABLE `payment_invoice_payment_method` CHANGE `vendor` `vendor` ENUM('braintree','ts','thingiverse','invoice','stripe','bonus') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;");


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
