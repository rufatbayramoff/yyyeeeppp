<?php

use yii\db\Migration;

class m170726_160832_4225_cnc_material_base_records extends Migration
{
    public function safeUp()
    {
        $this->delete('cnc_material_group');
        $this->insert('cnc_material_group', ['id' => 1, 'code' => 'cast-iron', 'title' => 'Metal \ Cast iron']);
        $this->insert('cnc_material_group', ['id' => 2, 'code' => 'structural-steel', 'title' => 'Metal \ Structural steel']);
        $this->insert('cnc_material_group', ['id' => 3, 'code' => 'tools-steel', 'title' => 'Metal \ Tool steel']);
        $this->insert('cnc_material_group', ['id' => 4, 'code' => 'aluminum', 'title' => 'Metal \ Aluminum']);
        $this->insert('cnc_material_group', ['id' => 5, 'code' => 'copper', 'title' => 'Metal \ Copper']);

        $this->insert('cnc_material_group', ['id' => 6, 'code' => 'wood', 'title' => 'Wood']);
        $this->insert('cnc_material_group', ['id' => 7, 'code' => 'plastic', 'title' => 'Plastic']);
        $this->insert('cnc_material_group', ['id' => 8, 'code' => 'stone', 'title' => 'Stone']);

        $this->insert('cnc_material', ['code' => 'cast-iron-others', 'cnc_material_group_id' => 1, 'title' => 'Others', 'density' => '8746']);
        $this->insert('cnc_material', ['code' => 'structural-steel-A36', 'cnc_material_group_id' => 2, 'title' => 'A36', 'density' => '7800']);
        $this->insert('cnc_material', ['code' => 'structural-steel-A53', 'cnc_material_group_id' => 2, 'title' => 'A53', 'density' => '7840']);
        $this->insert('cnc_material', ['code' => 'structural-steel-others', 'cnc_material_group_id' => 2, 'title' => 'Others', 'density' => '7800']);

        $this->insert('cnc_material', ['code' => 'tools-steel-A3', 'cnc_material_group_id' => 3, 'title' => 'A3', 'density' => '7860']);
        $this->insert('cnc_material', ['code' => 'tools-steel-A4', 'cnc_material_group_id' => 3, 'title' => 'A4', 'density' => '8000']);
        $this->insert('cnc_material', ['code' => 'tools-steel-others', 'cnc_material_group_id' => 3, 'title' => 'Others', 'density' => '8000']);
        $this->insert('cnc_material', ['code' => 'aluminum-others', 'cnc_material_group_id' => 4, 'title' => 'Others', 'density' => '2700']);
        $this->insert('cnc_material', ['code' => 'copper-others', 'cnc_material_group_id' => 5, 'title' => 'Others', 'density' => '2710']);
        $this->insert('cnc_material', ['code' => 'wood-others', 'cnc_material_group_id' => 6, 'title' => 'Others', 'density' => '530']);
        $this->insert('cnc_material', ['code' => 'plastic-others', 'cnc_material_group_id' => 7, 'title' => 'Others', 'density' => '1030']);
        $this->insert('cnc_material', ['code' => 'stone-others', 'cnc_material_group_id' => 8, 'title' => 'Others', 'density' => '2200']);

    }

    public function safeDown()
    {
        $this->delete('cnc_material');
        $this->delete('cnc_material_group');
    }

}
