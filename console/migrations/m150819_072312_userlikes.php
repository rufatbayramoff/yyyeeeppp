<?php

use yii\db\Schema;
use yii\db\Migration;

class m150819_072312_userlikes extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_like`  CHANGE COLUMN `object_type` `object_type` CHAR(15) NOT NULL ;");
        return 0;
    }

    public function down()
    {
        return 0;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
