<?php

use yii\db\Migration;

class m160718_104243_2432_remove_unused_tables extends Migration
{
    public function safeUp()
    {
        $this->dropTable('geo_vendor_settings');
        $this->dropTable('geo_location_vendor_results');
        $this->dropTable('geo_vendor');
        $this->dropTable('geo_log');

    }

    public function safeDown()
    {
        $this->execute("
        
DROP TABLE IF EXISTS `geo_location_vendor_results` CASCADE;

CREATE TABLE `geo_location_vendor_results` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`vendor_id` Int( 11 ) NOT NULL,
	`user_input` VarChar( 250 ) NOT NULL,
	`response_address` Text NOT NULL,
	`response_location` Text NOT NULL,
	`using_map` Enum( 'address', 'html5', 'drag', 'geocode', '' ) NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 1;

CREATE INDEX `fk_vendor_id` USING BTREE ON `geo_location_vendor_results`( `vendor_id` );


DROP TABLE IF EXISTS `geo_vendor_settings` CASCADE;

CREATE TABLE `geo_vendor_settings` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`vendor_id` Int( 11 ) NOT NULL,
	`setting` Enum( 'country_id' ) NOT NULL,
	`value` VarChar( 100 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 3;


INSERT INTO `geo_vendor_settings`(`id`,`vendor_id`,`setting`,`value`) VALUES ( '1', '1', 'country_id', '191' );
INSERT INTO `geo_vendor_settings`(`id`,`vendor_id`,`setting`,`value`) VALUES ( '2', '1', 'country_id', '255' );

CREATE INDEX `fk_vendor_id_2` USING BTREE ON `geo_vendor_settings`( `vendor_id` );


DROP TABLE IF EXISTS `geo_log` CASCADE;

CREATE TABLE `geo_log` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`message` VarChar( 255 ) COLLATE utf8_estonian_ci NOT NULL,
	`file` VarChar( 255 ) NOT NULL,
	`line` Int( 11 ) NOT NULL,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS `geo_vendor` CASCADE;

CREATE TABLE `geo_vendor` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`title` VarChar( 45 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 4;

INSERT INTO `geo_vendor`(`id`,`title`) VALUES ( '1', 'yandex' );
INSERT INTO `geo_vendor`(`id`,`title`) VALUES ( '2', 'google' );
INSERT INTO `geo_vendor`(`id`,`title`) VALUES ( '3', 'bing' );
");


        return true;
    }
}
