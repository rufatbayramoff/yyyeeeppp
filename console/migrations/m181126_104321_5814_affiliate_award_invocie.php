<?php

use yii\db\Migration;

/**
 * Class m181126_104321_5814_affiliate_award_invocie
 */
class m181126_104321_5814_affiliate_award_invocie extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('affiliate_award', 'invoice_uuid', 'varchar(6) NOT NULL after order_id');
        $this->execute('UPDATE `affiliate_award` SET invoice_uuid = (SELECT uuid from payment_invoice WHERE payment_invoice.store_order_id=affiliate_award.order_id ORDER BY created_at desc LIMIT 1)');
        $this->execute('CREATE UNIQUE INDEX idx_affiliate_award_invoice_uuid ON affiliate_award (invoice_uuid)');
        $this->execute('ALTER TABLE affiliate_award
                                    ADD CONSTRAINT fk_affiliate_award_invoice_uuid
                                    FOREIGN KEY (invoice_uuid) REFERENCES payment_invoice (uuid)');
        $this->execute('ALTER TABLE affiliate_award DROP FOREIGN KEY fk_affiliate_award_3;');
        $this->dropColumn('affiliate_award', 'order_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181126_104321_5814_affiliate_award_invocie cannot be reverted.\n";
        return false;
    }
}
