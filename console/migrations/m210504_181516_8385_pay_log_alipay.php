<?php

use yii\db\Migration;

/**
 * Class m210504_181516_8385_pay_log_alipay
 */
class m210504_181516_8385_pay_log_alipay extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `payment_pay_page_log_status` ADD `vendor` ENUM('braintree','stripe') NULL");
        $this->update('payment_pay_page_log_status', ['vendor'=>'braintree']);
        $this->execute("ALTER TABLE `payment_pay_page_log_status` CHANGE `vendor` `vendor` ENUM('braintree','stripe') NOT NULL;");
        $this->execute("ALTER TABLE `payment_pay_page_log_status` CHANGE `status` `status` ENUM('press_button','processed','done','failed') NOT NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
