<?php

use yii\db\Schema;
use yii\db\Migration;

class m160217_082530_order_model3d extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `store_order_model3d_file` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `order_item_id` INT(11) NOT NULL,
                `model3d_file_id` INT(11) NOT NULL,
                PRIMARY KEY (`id`),
                INDEX `FK1_order` (`order_item_id`),
                INDEX `FK2_model3dfile` (`model3d_file_id`),
                CONSTRAINT `FK1_order` FOREIGN KEY (`order_item_id`) REFERENCES `store_order_item` (`id`),
                CONSTRAINT `FK2_model3dfile` FOREIGN KEY (`model3d_file_id`) REFERENCES `model3d_file` (`id`)
            ) ENGINE=InnoDB;
        ");
    }

    public function down()
    {
        $this->truncateTable('store_order_model3d_file');
        $this->dropTable('store_order_model3d_file');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
