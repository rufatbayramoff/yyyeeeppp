<?php

use yii\db\Migration;

/**
 * Class m180523_122040_5196_email_templates
 */
class m180523_122040_5196_email_templates extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //
        $this->insert(
            'email_template',
            [
                'code'          => 'ps.preorder.new.needClaim',
                'group'         => 'service',
                'language_id'   => 'en-US',
                'title'         => 'You have a new quote #%quoteId% on Treatstock',
                'description'   => 'Email received for quote by unclaimed company params: quoteId, psName, newOrdersLink',
                'template_html' => '<p>Hello %psName%! </p>
<p>You have received a quote #%quoteId% on Treatstock. </p>
<p>To verify company account and see this quote please click <a href="%newOrdersLink%">here</a></p>

<p>Best regards,
Treatstock</p>
            ',
                'updated_at'    => dbexpr('NOW()'),
                'is_active'     => true
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'ps.preorder.new.needClaim']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180523_122040_5196_email_templates cannot be reverted.\n";

        return false;
    }
    */
}
