<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_123756_system_settings_params extends Migration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO `system_setting_group`
            (`title`,`created_at`,`updated_at`,`created_user_id`,`updated_user_id`,`description`)
            VALUES ('easy_post','2015-11-26 12:30:19','2015-11-26 12:30:19',2,2,'');");

        $group = app('db')->createCommand('SELECT * FROM system_setting_group WHERE title=:title')
            ->bindValue('title', 'easy_post')
            ->queryOne();

        
        $this->execute("INSERT INTO `system_setting`
            (`group_id`,`key`,`value`,`created_at`,`updated_at`,`created_user_id`,`updated_user_id`,`description`,`json`,`is_active`)
            VALUES (" .$group['id'] . ",'weight_inc','1.5','2015-11-26 12:32:07','2015-11-26 12:32:07',2,NULL,'Weight in ounce will increase by this value','',1);");

        $this->execute("INSERT INTO `system_setting`
            (`group_id`,`key`,`value`,`created_at`,`updated_at`,`created_user_id`,`updated_user_id`,`description`,`json`,`is_active`)
            VALUES (" .$group['id'] . ",'box_sizes_inc','1.5','2015-11-26 12:32:59','2015-11-26 12:32:59',2,NULL,'Box sizes in inches will increase by this value','',1);");
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM `system_setting` WHERE `key` = 'weight_inc';");
        $this->execute("DELETE FROM `system_setting` WHERE `key` = 'box_sizes_inc';");
        $this->execute("DELETE FROM `system_setting_group` WHERE `title` = 'easy_post'");
    }
}
