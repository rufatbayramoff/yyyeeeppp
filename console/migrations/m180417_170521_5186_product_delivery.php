<?php

use yii\db\Migration;

/**
 * Class m180417_170521_5186_product_delivery
 */
class m180417_170521_5186_product_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
CREATE TABLE `product_delivery` (
  `uuid` varchar(32) NOT NULL,
  `express_delivery_counrty_id` int(11) DEFAULT NULL,
  `express_delivery_first_item` decimal(6,2) DEFAULT NULL,
  `express_delivery_following_item` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `express_delivery_counrty_id` (`express_delivery_counrty_id`),
  CONSTRAINT `fk_product_delivery_country_id` FOREIGN KEY (`express_delivery_counrty_id`) REFERENCES `geo_country` (`id`),
  CONSTRAINT `fk_product_delivery_uuid` FOREIGN KEY (`uuid`) REFERENCES `product` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('product_delivery');
    }
}
