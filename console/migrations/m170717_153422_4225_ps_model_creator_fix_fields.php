<?php

use yii\db\Migration;
use yii\db\Query;

class m170717_153422_4225_ps_model_creator_fix_fields extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_ps_model_creator_id', 'ps_printer', 'ps_model_creator_id', 'ps_model_creator', 'id');
        $this->addForeignKey('fk_model_creator_delivery_ps_model_creator_id', 'ps_model_creator_delivery', 'ps_model_creator_id', 'ps_model_creator', 'id');
        $this->dropForeignKey('fk_ps_id', 'ps_printer');
        $this->dropColumn('ps_printer', 'ps_id');
        $this->dropForeignKey('fk_ps_printer_id_3', 'ps_model_creator_delivery');
        $this->dropIndex('ps_printer_delivery_type', 'ps_model_creator_delivery');
        $this->dropColumn('ps_model_creator_delivery', 'ps_printer_id');
    }

    public function down()
    {
        // Cant roll back
        return false;

    }
}
