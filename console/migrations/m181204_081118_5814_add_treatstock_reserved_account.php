<?php

use yii\db\Migration;

/**
 * Class m181204_081118_5814_add_treatstock_reserved_account
 */
class m181204_081118_5814_add_treatstock_reserved_account extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('payment_account', ['id'=>[2,3,4,5]]);
        $this->insert('payment_account', [
            'id' => 2,
            'user_id' => 1,
            'type' => 'reserved'
        ]);

        $this->insert('payment_account', [
            'id' => 3,
            'user_id' => 1,
            'type' => 'award'
        ]);

        $this->insert('payment_account', [
            'id' => 4,
            'user_id' => 1,
            'type' => 'invoice'
        ]);

        $this->insert('payment_account', [
            'id' => 5,
            'user_id' => 1,
            'type' => 'authorize'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
