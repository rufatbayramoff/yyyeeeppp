<?php

use yii\db\Schema;
use yii\db\Migration;

class m151009_130535_add_message_status_column extends Migration
{
    public function up()
    {
        $this->addColumn('community_message', 'message_status', 'smallint');

    }

    public function down()
    {
        echo "m151009_130535_add_message_status_column cannot be reverted.\n";

        return false;
    }
}
