<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m211013_094253_8995_sort_service_categories
 */
class m211013_094253_8995_sort_service_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $companyServiceCategories = (new Query())
            ->select('*')
            ->from('company_service_category')
            ->where('(`company_service_category`.`id` <> 1) AND (`company_service_category`.`is_active`=1)')
            ->orderBy('position, slug')
            ->all();
        $pos                      = 1;
        foreach ($companyServiceCategories as $serviceCategory) {
            $this->update('company_service_category', ['position' => $pos], ['id' => $serviceCategory['id']]);
            $pos++;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

}
