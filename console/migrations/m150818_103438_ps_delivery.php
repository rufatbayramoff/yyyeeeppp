<?php

use yii\db\Schema;
use yii\db\Migration;

class m150818_103438_ps_delivery extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $path = realpath(dirname(__DIR__) . '/../db/');
        $sqlString = file_get_contents($path . '/ps_delivery.sql');
        $sqls = explode(";", $sqlString);
        foreach($sqls as $sql){
            $sql = trim($sql);
            if(empty($sql)){
                continue;
            }
            $this->execute($sql);
        }
        return 0;
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $sqlString = "
          DROP TABLE delivery_courier;
          DROP TABLE delivery_type;
          ALTER TABLE ps_printer DROP COLUMN min_order_price;
          ALTER TABLE ps_printer_delivery DROP FOREIGN KEY `fk_ps_printer_id_3`;
          ALTER TABLE ps_printer_delivery DROP FOREIGN KEY `fk_delivery_type`;
          DROP TABLE ps_printer_delivery;";
        $sqls = explode(";", $sqlString);
        foreach($sqls as $sql){
            $sql = trim($sql);
            if(empty($sql)){
                continue;
            }
            $this->execute($sql);
        }
        return 0;
    }
}
