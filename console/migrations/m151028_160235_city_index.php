<?php

use yii\db\Schema;
use yii\db\Migration;

class m151028_160235_city_index extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `geo_city`
              DROP INDEX `geoname_id`,
              ADD UNIQUE INDEX `title_UNIQUE` USING BTREE (`title`, `country_id`, `region_id`, `lat`, `lon`);");
    }

    public function safeDown()
    {
        $this->execute("ALTER TABLE `geo_city`
              DROP INDEX `title_UNIQUE`,
              ADD UNIQUE INDEX `geoname_id` USING BTREE (`geoname_id`);");
    }
}
