<?php

use yii\db\Migration;

class m160808_154521_1644_model3d_replica extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `model3d_replica` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `description` TEXT,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  `published_at` TIMESTAMP NULL DEFAULT NULL,
  `is_published` TINYINT(1) NOT NULL DEFAULT '0',
  `is_printer_ready` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'G code ready',
  `dimensions` VARCHAR(45) DEFAULT NULL COMMENT 'width x height x length',
  `status` ENUM('new','active','moderation','published','inactive') NOT NULL DEFAULT 'new',
  `files_count` SMALLINT(6) NOT NULL DEFAULT '1',
  `cover_file_id` INT(11) DEFAULT NULL,
  `stat_views` INT(5) NOT NULL DEFAULT '1',
  `model_units` CHAR(4) NOT NULL DEFAULT 'mm',
  `category_id` INT(11) DEFAULT NULL,
  `source` CHAR(15) DEFAULT 'website',
  `original_model3d_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_model3d_replica_1_idx` (`user_id`),
  KEY `fk_model3d_replica_3_idx` (`cover_file_id`),
  KEY `fk_model3d_replica_2_idx` (`category_id`),
  KEY `fk_model3d_original_idx` (`original_model3d_id`),
  CONSTRAINT `fk_model3d_replica_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_model3d_replica_2` FOREIGN KEY (`category_id`) REFERENCES `store_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_model3d_replica_3` FOREIGN KEY (`cover_file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_model3d_original` FOREIGN KEY (`original_model3d_id`) REFERENCES `model3d` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
        $this->execute(
            <<<SQL
CREATE TABLE `model3d_replica_part` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model3d_replica_id` INT(11) NOT NULL,
  `format` CHAR(5) NOT NULL DEFAULT 'stl',
  `name` VARCHAR(250) DEFAULT NULL,
  `title` VARCHAR(250) DEFAULT NULL,
  `antivirus_checked_at` TIMESTAMP NULL DEFAULT NULL,
  `moderator_status` ENUM('new','checking','ok','banned','review') NOT NULL DEFAULT 'new',
  `moderated_at` TIMESTAMP NULL DEFAULT NULL,
  `user_status` ENUM('active','inactive','published') NOT NULL DEFAULT 'inactive',
  `file_id` INT(11) NOT NULL,
  `file_src_id` INT(11) DEFAULT NULL,
  `rotated_x` SMALLINT(6) NOT NULL DEFAULT '0',
  `rotated_y` SMALLINT(6) NOT NULL DEFAULT '0',
  `rotated_z` SMALLINT(6) NOT NULL DEFAULT '0',
  `qty` TINYINT(4) NOT NULL DEFAULT '1',
  `model3d_part_properties_id` INT(11) DEFAULT NULL,
  `original_model3d_part_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_model3d_replica_files_1_idx` (`model3d_replica_id`),
  KEY `fk_model3d_replica_file_1_idx` (`file_id`),
  KEY `fk_model3d_replica_file_2_idx` (`file_src_id`),
  KEY `fk_model3d_part_properties_id_idx` (`model3d_part_properties_id`),
  KEY `fk_model3d_part_original_idx` (`original_model3d_part_id`),
  CONSTRAINT `fk_model3d_replica_files_1` FOREIGN KEY (`model3d_replica_id`) REFERENCES `model3d_replica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_model3d_replica_file_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_model3d_replica_file_2` FOREIGN KEY (`file_src_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_model3d_part_properties` FOREIGN KEY (`model3d_part_properties_id`) REFERENCES `model3d_part_properties` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_model3d_part_original` FOREIGN KEY (`original_model3d_part_id`) REFERENCES `model3d_part` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function down()
    {
        echo "m160808_154521_1644_model3d_replica cannot be reverted.\n";

        return false;
    }
}
