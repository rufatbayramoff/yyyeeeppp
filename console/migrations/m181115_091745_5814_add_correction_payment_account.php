<?php

use yii\db\Migration;

/**
 * Class m181115_091745_5814_add_correction_payment_account
 */
class m181115_091745_5814_add_correction_payment_account extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('payment_account', 'type', "enum ('main', 'authorize', 'reserved', 'award', 'tax', 'invoice', 'manufactureAward', 'refundRequest', 'discount', 'easypost', 'refund', 'payout', 'correction') default 'main' not null");

        $this->insert('payment_account', [
            'user_id' => 110,
            'type' => 'correction'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('payment_account', [
            'user_id' => 110,
            'type' => 'correction'
        ]);

        $this->alterColumn('payment_account', 'type', "enum ('main', 'authorize', 'reserved', 'award', 'tax', 'invoice', 'manufactureAward', 'refundRequest', 'discount', 'easypost', 'refund', 'payout') default 'main' not null");
    }
}
