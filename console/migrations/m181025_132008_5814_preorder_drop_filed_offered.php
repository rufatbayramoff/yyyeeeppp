<?php

use yii\db\Migration;

/**
 * Class m181025_132008_5814_preorder_drop_filed_offered
 */
class m181025_132008_5814_preorder_drop_filed_offered extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('preorder', 'offered');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('preorder', 'offered', "tinyint(1) default '0' not null");
    }
}
