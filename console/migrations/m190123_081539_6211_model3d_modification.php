<?php

use yii\db\Migration;

/**
 * Class m190123_081539_6211_model3d_modification
 */
class m190123_081539_6211_model3d_modification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('model3d', 'is_command_clean_check', $this->tinyInteger()->defaultValue(0)->comment('console command flag field'));
        $this->addColumn('model3d', 'command_clean_check_last_date', $this->dateTime()->comment('console command flag field'));

        $this->execute('alter table cart_item drop foreign key cart_item_cart_id;

alter table cart_item
	add constraint cart_item_cart_id
		foreign key (cart_id) references cart (id)
			on delete cascade;');

        $this->execute('alter table thingiverse_thing_file drop foreign key fk_thingiverse_thing_file_thing_id;

alter table thingiverse_thing_file
	add constraint fk_thingiverse_thing_file_thing_id
		foreign key (thing_id) references thingiverse_thing (thing_id)
			on delete cascade;');

        $this->execute('alter table store_order_review_file drop foreign key fk_sorf_review_id;

alter table store_order_review_file
	add constraint fk_sorf_review_id
		foreign key (review_id) references store_order_review (id)
			on delete cascade;');

        $this->execute('alter table store_order_review_share drop foreign key fk_store_order_review_share_review;

alter table store_order_review_share
	add constraint fk_store_order_review_share_review
		foreign key (review_id) references store_order_review (id)
			on delete cascade;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('model3d', 'is_command_clean_check');
        $this->dropColumn('model3d', 'command_clean_check_last_date');

        $this->execute('alter table cart_item drop foreign key cart_item_cart_id;

alter table cart_item
	add constraint cart_item_cart_id
		foreign key (cart_id) references cart (id);');

        $this->execute('alter table thingiverse_thing_file drop foreign key fk_thingiverse_thing_file_thing_id;

alter table thingiverse_thing_file
	add constraint fk_thingiverse_thing_file_thing_id
		foreign key (thing_id) references thingiverse_thing (thing_id);');

        $this->execute('alter table store_order_review_file drop foreign key fk_sorf_review_id;

alter table store_order_review_file
	add constraint fk_sorf_review_id
		foreign key (review_id) references store_order_review (id);');

        $this->execute('alter table store_order_review_share drop foreign key fk_store_order_review_share_review;

alter table store_order_review_share
	add constraint fk_store_order_review_share_review
		foreign key (review_id) references store_order_review (id);');
    }


}
