<?php

use yii\db\Migration;

class m170510_120731_4256_model3dpart_fk extends Migration
{
    public function up()
    {
        $this->dropForeignKey('model3d_replica_part_id', 'model3d_replica_part');
        $this->getDb()->createCommand(
            'UPDATE model3d_replica_part SET original_model3d_part_id=1 WHERE model3d_replica_part.original_model3d_part_id NOT IN (SELECT id FROM model3d_part)'
        )->execute();
        $this->addForeignKey('fk_model3d_original_part_id', 'model3d_replica_part', 'original_model3d_part_id', 'model3d_part', 'id');
    }

    public function down()
    {
        return true;
    }
}
