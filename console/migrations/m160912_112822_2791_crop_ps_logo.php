<?php

use yii\db\Migration;

class m160912_112822_2791_crop_ps_logo extends Migration
{
    public function up()
    {
        $this->addColumn('ps', 'logo_circle', 'VARCHAR(255) NULL');
        $this->execute("UPDATE ps SET logo_circle = logo");
    }

    public function down()
    {
        $this->dropColumn('ps', 'logo_circle');
        return true;
    }
}
