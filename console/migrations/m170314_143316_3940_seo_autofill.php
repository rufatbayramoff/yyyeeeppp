<?php

use yii\db\Migration;

class m170314_143316_3940_seo_autofill extends Migration
{
    public function safeUp()
    {
        $this->execute('DROP TABLE IF EXISTS seo_page_autofill_template');
        $this->execute('CREATE TABLE `seo_page_autofill_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `template_name` varchar(245) DEFAULT NULL,
  `title` varchar(245) DEFAULT NULL,
  `meta_description` varchar(245) DEFAULT NULL,
  `meta_keywords` varchar(245) DEFAULT NULL,
  `header` varchar(245) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lang_iso` varchar(45) NOT NULL,
  `is_default` bit(1) NOT NULL DEFAULT b\'1\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key3` (`is_default`,`lang_iso`,`type`,`template_name`),
  KEY `fk_seo_page_autofill_template_1_idx` (`lang_iso`),
  CONSTRAINT `fk_seo_page_autofill_template_1` FOREIGN KEY (`lang_iso`) REFERENCES `system_lang` (`iso_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;'
        );

        $this->execute('ALTER TABLE `seo_page_autofill` ADD COLUMN `template_id` INT NULL AFTER `seo_page_id`;');

        $this->execute('ALTER TABLE `seo_page_autofill` 
            ADD CONSTRAINT `fk_seo_page_autofill_2`
              FOREIGN KEY (`template_id`)
              REFERENCES `seo_page_autofill_template` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');



        $this->execute('ALTER TABLE `seo_page` ADD COLUMN `is_locked` BIT(1) NULL DEFAULT b\'0\' AFTER `updated_at`;');

        $this->execute('ALTER TABLE `seo_page_autofill` ADD UNIQUE INDEX `index5` (`seo_page_id` ASC);');

        $this->execute('ALTER TABLE `seo_page_intl` 
            CHANGE COLUMN `meta_description` `meta_description` VARCHAR(180) NULL DEFAULT NULL ,
            CHANGE COLUMN `meta_keywords` `meta_keywords` VARCHAR(150) NULL DEFAULT NULL ;
        ');

        $this->execute('ALTER TABLE `seo_page_autofill` 
            DROP FOREIGN KEY `fk_seo_page_autofill_1`;
            ALTER TABLE `seo_page_autofill` 
            CHANGE COLUMN `seo_page_id` `seo_page_id` INT(11) NULL ,
            ADD COLUMN `seo_page_intl_id` INT NULL AFTER `seo_page_id`;
            ALTER TABLE `seo_page_autofill` 
            ADD CONSTRAINT `fk_seo_page_autofill_1`
              FOREIGN KEY (`seo_page_id`)
              REFERENCES `seo_page` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');

        $this->execute('ALTER TABLE `seo_page_autofill` 
            ADD INDEX `fk_seo_page_autofill_3_idx` (`seo_page_intl_id` ASC);
            ALTER TABLE `seo_page_autofill` 
            ADD CONSTRAINT `fk_seo_page_autofill_3`
              FOREIGN KEY (`seo_page_intl_id`)
              REFERENCES `seo_page_intl` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');

        $this->execute('ALTER TABLE `seo_page_autofill` ADD UNIQUE INDEX `index7` (`seo_page_intl_id` ASC);');

        $this->execute('ALTER TABLE `seo_page_autofill` 
                DROP INDEX `index2` ,
                ADD UNIQUE INDEX `index2` (`object_type` ASC, `object_id` ASC, `seo_page_id` ASC, `seo_page_intl_id` ASC);'
        );

        // insert data

        $this->batchInsert(
            'seo_page_autofill_template',
            [
                'type',
                'template_name',
                'title',
                'meta_description',
                'meta_keywords',
                'header',
                'lang_iso',
                'is_default'
            ],
            [
                [
                    'model3d',
                    'Models with description',
                    '%modelTitle% by %modelUser% in %modelCategory%',
                    '%modelDescription%',
                    '%modelKeywords%, 3d model, %modelCategory%',
                    '%modelTitle%',
                    'en-US',
                    '1',
                ],
                [
                    'ps',
                    'PS Default',
                    '%psTitle% 3D Printing with %psMaterials% in %psLocation%',
                    '%psDescription%',
                    '%psPrinters%, %psMaterials%, %psLocation%',
                    '%psTitle% ',
                    'en-US',
                    '1',
                ],
                [
                    'category',
                    'Category Default',
                    '%categoryTitle% - 3D Printable Models',
                    '%categoryDescription%',
                    '%categoryTitle%, 3d printables, 3d models',
                    '%categoryTitle%',
                    'en-US',
                    '1',
                ]
            ]
        );
    }

    public function down()
    {
        $this->dropTable('seo_page_autofill_template');
        $this->dropForeignKey('fk_seo_page_autofill_2', 'seo_page_autofill');
        $this->dropColumn('seo_page_autofill', 'template_id');
        $this->dropIndex('index5', 'seo_page_autofill');
        $this->dropIndex('index7', 'seo_page_autofill');
        $this->dropForeignKey('fk_seo_page_autofill_3', 'seo_page_autofill');
        $this->dropForeignKey('fk_seo_page_autofill_3', 'seo_page_autofill');
        $this->dropColumn('seo_page_autofill', 'seo_page_intl_id');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
