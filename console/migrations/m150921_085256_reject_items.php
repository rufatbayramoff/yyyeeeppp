<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_085256_reject_items extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->truncateTable('system_reject');
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (1,'ps_printer_reject','The Content shall not contain propaganda of alcohol, tobacco and drugs','',1);");
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (2,'ps_printer_reject','The Content shall not contain advertising of other projects not related to printing services','',1);");
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (3,'ps_printer_reject','The Content shall not contain any vulgar or otherwise offensive material','',1);");
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (4,'ps_printer_banned','Unacceptable Content','',1);");
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (5,'ps_reject','Invalid Image','',1);");
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (6,'ps_banned','Violation of the Federal Law','',1);");
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->truncateTable('system_reject');
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (1,'ps_printer','Информация не должна содержать оскорблений и ненормативной лексики','',1);");
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (2,'ps_printer','Информация не должна содержать рекламу иных проектов, не связанных с услугой печати на 3D принтере','',1);");
        $this->execute("INSERT INTO `system_reject` (`id`,`group`,`title`,`description`,`is_active`)
            VALUES (3,'ps_printer','Информация не должна содержать пропаганду алкоголя, табакокурения и наркотиков','',1);");
    }
} 