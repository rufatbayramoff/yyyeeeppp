<?php

use yii\db\Migration;

class m161129_105021_1945_create_viewed_model_list extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `model3d_viewed_by` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model3d_id` int(11) NOT NULL,
  `user_session_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model_viewed` (`model3d_id`,`user_session_id`),
  KEY `model3d_id` (`model3d_id`),
  KEY `user_session_id` (`user_session_id`) USING BTREE,
  CONSTRAINT `fk_model3d_viewed_by_model3d_id` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`),
  CONSTRAINT `fk_model3d_viewed_by_user_session_id` FOREIGN KEY (`user_session_id`) REFERENCES `user_session` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function down()
    {
        $this->dropTable('model3d_viewed_by');
    }
}
