<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_094554_customs extends Migration
{
    public function safeUp()
    {
        $this->execute("CREATE TABLE `custom_code` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `title` varchar(45) NOT NULL,
              `descr` varchar(100) DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL,
              PRIMARY KEY (`id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $this->execute("INSERT INTO custom_code SET title = 'My first custom code', created_at = NOW(), updated_at = NOW()");

        $this->execute("CREATE TABLE `custom_codes_country` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `code_id` int(11) NOT NULL,
              `title` varchar(45) DEFAULT NULL,
              `iso_code` char(2) NOT NULL,
              `custom_code` varchar(45) NOT NULL,
              `is_active` tinyint(1) DEFAULT '1',
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `custom_codes_idx` (`code_id`),
              CONSTRAINT `custom_codes_fk`
                FOREIGN KEY (`code_id`) REFERENCES `custom_code` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $this->execute("ALTER TABLE `store_category` 
              ADD COLUMN `custom_code_id` INT(11) NOT NULL AFTER `id`,
              ADD INDEX `custom_code_idx` (`custom_code_id`);");

        $this->execute("UPDATE store_category SET custom_code_id = 1");

        $this->execute("ALTER TABLE `store_category`
              ADD CONSTRAINT `fk_custom_code`
                FOREIGN KEY (`custom_code_id`) REFERENCES `custom_code` (`id`)
                ON DELETE NO ACTION ON UPDATE NO ACTION;");
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE store_category DROP FOREIGN KEY `fk_custom_code`;');
        $this->execute('ALTER TABLE custom_codes_country DROP FOREIGN KEY `custom_codes_fk`;');

        $this->execute('ALTER TABLE store_category
              DROP COLUMN `custom_code_id`,
              DROP INDEX `custom_code_idx`;');

        $this->execute('ALTER TABLE custom_codes_country DROP KEY `custom_codes_idx`;');
        $this->execute('DROP TABLE custom_code;');
        $this->execute('DROP TABLE custom_codes_country;');
    }
}
