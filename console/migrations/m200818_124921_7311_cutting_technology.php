<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200818_124921_7311_cutting_technology
 */
class m200818_124921_7311_cutting_technology extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Set default value laser
        $this->addColumn('cutting_machine', 'technology', "ENUM('laser','plasma','cnc_router','water_jet','knife','oxy-fuel','other') NOT NULL DEFAULT 'laser'");
        // No default value
        $this->alterColumn('cutting_machine', 'technology', "ENUM('laser','plasma','cnc_router','water_jet','knife','oxy-fuel','other') NOT NULL");
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {


    }
}