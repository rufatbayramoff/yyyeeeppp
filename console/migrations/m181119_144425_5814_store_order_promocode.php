<?php

use yii\db\Migration;

/**
 * Class m181119_144425_5814_store_order_promocode
 */
class m181119_144425_5814_store_order_promocode extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_promocode', 'invoice_uuid', 'varchar(6) NOT NULL after id');

        $promocodes = app()->db->createCommand('select * from store_order_promocode')->queryAll();

        foreach ($promocodes as $promocode) {
            $invoiceUuid = app()->db->createCommand('select primary_payment_invoice_uuid from store_order where id = :id', ['id' => $promocode['order_id']])->queryScalar();

            if ($invoiceUuid) {
                $this->update('store_order_promocode', [
                    'invoice_uuid' => $invoiceUuid
                ], [
                    'id' => $promocode['id']
                ]);
                echo "Promocode order id: {$promocode['order_id']} => invoice uuid: {$invoiceUuid}\n";
            } else {
                echo "order not invoice\n";
                die;
            }
        }

        $this->dropForeignKey('fk_store_order_promocode_1', 'store_order_promocode');

        $this->dropIndex('index4', 'store_order_promocode');
        $this->dropIndex('fk_store_order_promocode_1_idx', 'store_order_promocode');

        $this->dropColumn('store_order_promocode', 'order_id');

        $this->execute('CREATE UNIQUE INDEX store_order_promocode_invoice_uuid_uindex ON store_order_promocode (invoice_uuid)');

        $this->execute('ALTER TABLE store_order_promocode
                                    ADD CONSTRAINT store_order_promocode_payment_invoice_uuid_fk
                                    FOREIGN KEY (invoice_uuid) REFERENCES payment_invoice (uuid)');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181119_144425_5814_store_order_promocode cannot be reverted.\n";
        return false;
    }
}
