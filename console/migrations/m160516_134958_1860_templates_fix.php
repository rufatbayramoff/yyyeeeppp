<?php

use yii\db\Migration;

class m160516_134958_1860_templates_fix extends Migration
{
    public function up()
    {
        $this->execute("UPDATE `email_template` SET `title` = 'Your model %modelname% on treatstock.com was printed' WHERE `code` = 'modelAward';");
    }

    public function down()
    {
        return true;
    }
}
