<?php

use yii\db\Migration;

/**
 * Class m180322_101949_3020_review_without_photo
 */
class m180322_101949_3020_review_without_photo extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
        ALTER TABLE `store_order_review_share` 
        DROP FOREIGN KEY `fk_store_order_review_share_photo`;
        ALTER TABLE `store_order_review_share` 
        CHANGE COLUMN `photo_id` `photo_id` INT(11) NULL ;
        ALTER TABLE `store_order_review_share` 
        ADD CONSTRAINT `fk_store_order_review_share_photo`
          FOREIGN KEY (`photo_id`)
          REFERENCES `file` (`id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION;
        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180322_101949_3020_review_without_photo cannot be reverted.\n";

        return false;
    }
}
