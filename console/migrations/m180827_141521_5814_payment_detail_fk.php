<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180827_141521_5814_payment_detail_fk
 */
class m180827_141521_5814_payment_detail_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('payment_detail_operation_uuid', 'payment_detail', 'payment_detail_operation_uuid', 'payment_detail_operation', 'uuid');
        $this->execute("ALTER TABLE `payment_detail` 
DROP FOREIGN KEY `fk_payment_detail_1`;
ALTER TABLE `payment_detail` 
DROP COLUMN `payment_id`,
DROP INDEX `fk_payment_detail_1_idx` ;
");


    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
