<?php

use yii\db\Schema;
use yii\db\Migration;

class m160108_125305_taxcountry extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `tax_country`  
            ADD COLUMN `is_default` TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'is default rate 30%, if yes, show notify message' AFTER `updated_at`;");
        
        $this->execute("update tax_country set rate_ps=10, rate_model=10, is_default=0 where country='CN';");
        $this->execute("update tax_country set rate_ps=0, rate_model=0, is_default=0 where country='US';");
        $this->execute("update tax_country set rate_ps=0, rate_model=0, is_default=0 where country='CA';");
        
        $this->execute("update tax_country set rate_ps=0 where id>0;");
    }

    public function down()
    {
        $this->dropColumn('tax_country', 'is_default');
    } 
}
