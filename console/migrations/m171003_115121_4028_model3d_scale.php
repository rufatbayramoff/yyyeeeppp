<?php

use yii\db\Migration;

class m171003_115121_4028_model3d_scale extends Migration
{
    public function safeUp()
    {
        $this->addColumn('model3d', 'scale', 'decimal(15,11) not null DEFAULT "1"');
    }

    public function safeDown()
    {
        $this->dropColumn('model3d', 'scale');
    }
}
