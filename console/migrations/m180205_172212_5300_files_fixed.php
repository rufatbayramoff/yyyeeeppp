<?php

use yii\db\Migration;
use yii\db\Query;

class m180205_172212_5300_files_fixed extends Migration
{
    public function moveRecords($table, $fieldName)
    {
        $rows = (new Query())->select($fieldName)->from($table)->all();
        $rows = \yii\helpers\ArrayHelper::getColumn($rows, $fieldName);
        $filesInfo = (new Query())->select('*')->from('file')->where(['uuid' => $rows])->all();
        $fields = [
            'uuid',
            'name',
            'path',
            'extension',
            'size',
            'created_at',
            'updated_at',
            'deleted_at',
            'user_id',
            'server',
            'status',
            'md5sum',
            'last_access_at',
            'ownerClass',
            'ownerField',
            'is_public',
            'path_version',
            'expire'
        ];
        $fieldKeys = array_combine($fields, $fields);
        $insertValues = [];
        foreach ($filesInfo as $fileInfo) {
            $insertValue = array_intersect_key($fileInfo, $fieldKeys);
            $insertValues[] = $insertValue;
        }
        $this->db->createCommand()->batchInsert('file_admin', $fields, $insertValues)->execute();
        $this->db->createCommand()->delete('file', ['uuid' => $rows])->execute();
    }

    public function up()
    {
        $this->execute("
        CREATE TABLE `file_admin` (
          `uuid` varchar(32) NOT NULL,
          `name` varchar(245) NOT NULL,
          `path` varchar(555) NOT NULL,
          `extension` char(9) NOT NULL,
          `size` int(11) DEFAULT NULL COMMENT 'bytes',
          `created_at` timestamp NULL DEFAULT NULL,
          `updated_at` timestamp NULL DEFAULT NULL,
          `deleted_at` timestamp NULL DEFAULT NULL,
          `user_id` int(11) DEFAULT NULL,
          `server` char(15) NOT NULL DEFAULT 'localhost',
          `status` enum('new','active','inactive','danger','deleted') NOT NULL DEFAULT 'new',
          `md5sum` char(32) DEFAULT '',
          `last_access_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `ownerClass` varchar(255) DEFAULT NULL,
          `ownerField` varchar(255) DEFAULT NULL,
          `is_public` tinyint(1) NOT NULL DEFAULT '0',
          `path_version` enum('v1','v2','v3') NOT NULL DEFAULT 'v2',
          `expire` datetime DEFAULT NULL,
          UNIQUE KEY `file-uniq-uuid` (`uuid`),
          KEY `pathVersionIndex` (`path_version`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
                ");
        $this->dropForeignKey('connected_app_logo_file_fk', 'connected_app');
        $this->moveRecords('connected_app', 'logo_file_uuid');
        $this->addForeignKey('connected_app_logo_file_fk', 'connected_app', 'logo_file_uuid', 'file_admin', 'uuid');

        $this->dropForeignKey('fk_main_photo_file_uuid', 'wiki_machine');
        $this->moveRecords('wiki_machine', 'main_photo_file_uuid');
        $this->addForeignKey('fk_main_photo_file_uuid', 'wiki_machine', 'main_photo_file_uuid', 'file_admin', 'uuid');

        $this->dropForeignKey('fk_wiki_machine_photo_photo_id', 'wiki_machine_photo');
        $this->moveRecords('wiki_machine_photo', 'photo_file_uuid');
        $this->addForeignKey('fk_wiki_machine_photo_photo_id', 'wiki_machine_photo', 'photo_file_uuid', 'file_admin', 'uuid');

        $this->dropForeignKey('fk_wiki_machine_file_file_uuid', 'wiki_machine_file');
        $this->moveRecords('wiki_machine_file', 'file_uuid');
        $this->addForeignKey('fk_wiki_machine_file_file_uuid', 'wiki_machine_file', 'file_uuid', 'file_admin', 'uuid');

        $this->dropForeignKey('fk_wiki_material_files_file_id', 'wiki_material_file');
        $this->moveRecords('wiki_material_file', 'file_uuid');
        $this->addForeignKey('fk_wiki_material_files_file_id', 'wiki_material_file', 'file_uuid', 'file_admin', 'uuid');

        $this->dropForeignKey('fk_wiki_material_file_uuid', 'wiki_material_photo');
        $this->moveRecords('wiki_material_photo', 'file_uuid');
        $this->addForeignKey('fk_wiki_material_file_uuid', 'wiki_material_photo', 'file_uuid', 'file_admin', 'uuid');
    }

    public function down()
    {
        $this->dropTable('file_admin');
    }

}
