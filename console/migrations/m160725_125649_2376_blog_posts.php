<?php

use yii\db\Migration;

class m160725_125649_2376_blog_posts extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `comments_count` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `image_preview_file_id` int(11) NOT NULL,
  `image_file_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_alias` (`alias`),
  KEY `image_preview_file` (`image_preview_file_id`),
  KEY `preview_file` (`image_file_id`),
  CONSTRAINT `preview_file` FOREIGN KEY (`image_file_id`) REFERENCES `file` (`id`),
  CONSTRAINT `image_preview_file` FOREIGN KEY (`image_preview_file_id`) REFERENCES `file` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        
        ");
    }

    public function down()
    {
        $this->dropTable('blog_post');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
