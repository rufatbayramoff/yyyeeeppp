<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190125_161121_5912_quote_created_user
 */
class m190219_132721_6308_fix_invoice_accounting_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $query = 'SELECT * FROM payment_invoice WHERE accounting like "%affiliate_fee_in%" or accounting like "%affiliate_fee_out%"';
        $invoices = \Yii::$app->db->createCommand($query)->queryAll();
        foreach ($invoices as $invoice) {
            $dbUpdate = 0;
            $accounting = json_decode($invoice['accounting'], true);
            if (array_key_exists('affiliate_fee_in', $accounting)) {
                if (!array_key_exists('type', $accounting['affiliate_fee_in'])) {
                    $accounting['affiliate_fee_in']['type'] = 'affiliate_fee_in';
                    $dbUpdate = 1;
                }
            }
            if (array_key_exists('affiliate_fee_out', $accounting)) {
                if (!array_key_exists('type', $accounting['affiliate_fee_out'])) {
                    $accounting['affiliate_fee_out']['type'] = 'affiliate_fee_out';
                    $dbUpdate = 1;
                }
            }
            if ($dbUpdate) {
                $this->update('payment_invoice', ['accounting' => $accounting], 'uuid="' . $invoice['uuid'].'"');
            }
        }
    }

    public function safeDown()
    {
    }
}