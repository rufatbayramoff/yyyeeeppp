<?php

use yii\db\Migration;

/**
 * Class m180827_105135_5564_seo_tpl
 */
class m180827_105135_5564_seo_tpl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rows = [
            [
                'type'             => 'printer',
                'template_name'    => 'Machinge Guide - Printer',
                'title'            => '%title%  - Treatstock',
                'meta_description' => '%descriptionPlain%',
                'meta_keywords'    => '',
                'header'           => '%title%',
                'lang_iso'         => 'en-US',
                'is_default'       => true,
                'is_apply_created' => false,
                'header_text'      => '',
                'footer_text'      => ''
            ],
        ];
        foreach ($rows as $row) {
            $this->insert('seo_page_autofill_template', $row);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('seo_page_autofill_template', ['type' => ['printer']]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180827_105135_5564_seo_tpl cannot be reverted.\n";

        return false;
    }
    */
}
