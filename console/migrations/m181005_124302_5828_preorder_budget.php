<?php

use yii\db\Migration;

/**
 * Class m181005_124302_5828_preorder_budget
 */
class m181005_124302_5828_preorder_budget extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `preorder` CHANGE COLUMN `budget` `budget` DECIMAL(15,2) NULL DEFAULT NULL;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('ALTER TABLE `preorder` CHANGE COLUMN `budget` `budget` DECIMAL(15,4) NULL DEFAULT NULL;');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181005_124302_5828_preorder_budget cannot be reverted.\n";

        return false;
    }
    */
}
