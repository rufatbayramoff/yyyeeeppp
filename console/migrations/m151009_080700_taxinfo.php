<?php
use yii\db\Schema;
use yii\db\Migration;

class m151009_080700_taxinfo extends Migration
{
    public function safeUp()
    {
        $this->execute("CREATE TABLE `user_tax_info` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `is_usa` tinyint(1) NOT NULL,
            `address_id` int(11) DEFAULT NULL,
            `identification_type` char(15) DEFAULT NULL,
            `identification` varchar(45) DEFAULT NULL,
            `classification` varchar(145) DEFAULT NULL,
            `name` varchar(45) DEFAULT NULL,
            `business_name` varchar(145) DEFAULT NULL,
            `mailing_address_id` int(11) DEFAULT NULL,
            `status` char(15) DEFAULT 'new',
            `place_country` char(15) DEFAULT NULL,
            `signature` varchar(45) DEFAULT NULL,
            `sign_date` date DEFAULT NULL,
            `sign_type` enum('form','email') NOT NULL DEFAULT 'form',
            PRIMARY KEY (`id`),
            UNIQUE KEY `user_id_UNIQUE` (`user_id`),
            KEY `fk_user_tax_info_2_idx` (`address_id`),
            KEY `fk_user_tax_info_3_idx` (`mailing_address_id`),
            CONSTRAINT `fk_user_tax_info_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_user_tax_info_2` FOREIGN KEY (`address_id`) REFERENCES `user_address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_user_tax_info_3` FOREIGN KEY (`mailing_address_id`) REFERENCES `user_address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

        ");
        $this->execute("CREATE TABLE `user_tax_info_history` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_tax_info_id` int(11) NOT NULL,
            `user_id` int(11) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `action_id` varchar(45) NOT NULL,
            `comment` tinytext,
            PRIMARY KEY (`id`),
            KEY `fk_user_tax_info_history_1_idx` (`user_tax_info_id`),
            KEY `fk_user_tax_info_history_2_idx` (`user_id`),
            CONSTRAINT `fk_user_tax_info_history_1` FOREIGN KEY (`user_tax_info_id`) REFERENCES `user_tax_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_user_tax_info_history_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->truncateTable('user_tax_info_history');
        $this->truncateTable('user_tax_info');
        $this->dropTable('user_tax_info_history');
        $this->dropTable('user_tax_info');
    }
}
