<?php

use yii\db\Schema;
use yii\db\Migration;

class m160111_123817_issue341 extends Migration
{
    public function up()
    {
        $rowsStr = "REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) 
            VALUES (12,'AT',0,0,'2016-01-11 15:44:47',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (13,'AU',0,5,'2016-01-11 15:47:04',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (19,'BE',0,0,'2016-01-11 15:44:59',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (21,'BG',0,5,'2016-01-11 15:47:14',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (35,'CA',0,0,'2016-01-11 15:43:27',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (40,'CH',0,0,'2016-01-11 15:46:48',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (45,'CN',0,10,'2016-01-11 15:47:48',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (54,'DE',0,0,'2016-01-11 15:43:40',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (56,'DK',0,0,'2016-01-11 15:45:08',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (67,'FI',0,0,'2016-01-11 15:45:19',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (72,'FR',0,0,'2016-01-11 15:44:24',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (74,'GB',0,0,'2016-01-11 15:44:10',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (85,'GR',0,0,'2016-01-11 15:45:30',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (98,'IE',0,0,'2016-01-11 15:45:46',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (100,'IN',0,15,'2016-01-11 15:48:33',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (108,'JP',0,0,'2016-01-11 15:44:35',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (158,'NL',0,0,'2016-01-11 15:45:54',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (159,'NO',0,0,'2016-01-11 15:46:09',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (163,'NZ',0,5,'2016-01-11 15:47:23',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (171,'PL',0,10,'2016-01-11 15:47:58',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (182,'RU',0,0,'2016-01-11 15:46:18',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (188,'SE',0,0,'2016-01-11 15:46:39',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (191,'SI',0,5,'2016-01-11 15:47:33',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (214,'TR',0,10,'2016-01-11 15:48:09',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (219,'UA',0,10,'2016-01-11 15:48:18',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (222,'US',0,0,'2016-01-08 15:59:59',0);
REPLACE INTO `tax_country` (`id`,`country`,`rate_ps`,`rate_model`,`updated_at`,`is_default`) VALUES (236,'ZA',0,0,'2016-01-11 15:46:29',0);";
        $rows = explode(";", $rowsStr);
        foreach($rows as $k=>$v){
            $this->execute($v);
        }
        
    }

    public function down()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
