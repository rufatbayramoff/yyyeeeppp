<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%geo_country}}`.
 */
class m201111_110633_3432_add_is_europa_column_to_geo_country_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%geo_country}}', 'is_europa', $this->tinyInteger()->defaultValue(0)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%geo_country}}', 'is_europa');
    }
}
