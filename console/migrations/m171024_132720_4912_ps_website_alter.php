<?php

use yii\db\Migration;

class m171024_132720_4912_ps_website_alter extends Migration
{
    public function safeUp()
    {
        $this->addColumn('ps', 'website', $this->string(245));
        $this->addColumn('ps', 'facebook', $this->string(245));
        $this->addColumn('ps', 'instagram', $this->string(245));
        $this->addColumn('ps', 'twitter', $this->string(245));
    }

    public function safeDown()
    {
        $this->dropColumn('ps', 'website');
        $this->dropColumn('ps', 'facebook');
        $this->dropColumn('ps', 'instagram');
        $this->dropColumn('ps', 'twitter');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171024_132720_4912_ps_website_alter cannot be reverted.\n";

        return false;
    }
    */
}
