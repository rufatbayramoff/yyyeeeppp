<?php

use yii\db\Migration;
use yii\db\Query;

class m170718_175331_4225_ps_machine_location extends Migration
{
    public function up()
    {
        $this->addColumn('ps_machine', 'location_id', 'INT(11) NULL');
        $sql = 'SELECT ps_printer.id as id, ps_machine.id as ps_machine_id, ps_printer.location_id as location_id FROM ps_printer LEFT JOIN ps_machine ON ps_machine.ps_printer_id=ps_printer.id';
        $printers = $this->db->createCommand($sql)->queryAll();
        foreach ($printers as $printer) {
            $this->update('ps_machine', ['location_id' => $printer['location_id']], ['id' => $printer['ps_machine_id']]);
        }
        $this->addForeignKey('fk_ps_machine_location_id', 'ps_machine', 'location_id', 'user_location', 'id');
        $this->dropForeignKey('fk_user_location', 'ps_printer');
        $this->dropColumn('ps_printer', 'location_id');
    }

    public function down()
    {
        return false;
    }
}
