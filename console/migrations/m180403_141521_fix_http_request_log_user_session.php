<?php

use yii\db\Migration;

/**
 * Class m180403_141521_fix_http_request_log_user_session
 */
class m180403_141521_fix_http_request_log_user_session extends Migration
{
    public function safeUp()
    {
        $this->addColumn('http_request_log', 'user_session_id', 'INT(11) NULL');
        $this->addForeignKey('fk_http_request_log_user_session', 'http_request_log', 'user_session_id', 'user_session', 'id');
        $this->addColumn('http_request_log', 'responce_code', 'INT(11) NOT NULL DEFAULT "200" after answer');
        $this->addColumn('http_request_log', 'headers', 'JSON NOT NULL after answer');
    }

    public function safeDown()
    {
        return false;
    }
}
