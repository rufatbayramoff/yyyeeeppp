<?php

use yii\db\Migration;
use yii\db\Query;

class m161012_170211_2778_create_content_filter_stat extends Migration
{
    public function safeUp()
    {
        $this->execute(<<<SQL
CREATE TABLE `content_filter_checked_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) unsigned NOT NULL,
  `isBanned` BOOLEAN ,
  PRIMARY KEY (`id`),
  KEY `message_id` (`message_id`),
  CONSTRAINT `fk_content_checked_message_id` FOREIGN KEY (`message_id`) REFERENCES `msg_message` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
    );
        $this->execute(<<<SQL
CREATE TABLE `content_filter_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `alert_type` enum('message','own_page', 'messageTopic') NOT NULL DEFAULT 'message',
  `alert_info` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

    }

    public function safeDown()
    {
        $this->dropTable('content_filter_checked_messages');
        $this->dropTable('content_filter_statistics');
        return true;
    }

}
