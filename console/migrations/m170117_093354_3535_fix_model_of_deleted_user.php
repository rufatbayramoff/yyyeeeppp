<?php

use yii\db\Migration;

class m170117_093354_3535_fix_model_of_deleted_user extends Migration
{
    public function up()
    {
        $this->execute("
            UPDATE model3d, store_unit, `user`
            SET model3d.status = 'inactive', store_unit.status = 'deleted', store_unit.is_active = 0
            WHERE 
              `user`.status = 0
              AND model3d.id = store_unit.model3d_id
              AND model3d.user_id = `user`.id
        ");
    }

    public function down()
    {
        return true;
    }
}
