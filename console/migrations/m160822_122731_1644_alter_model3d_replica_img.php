<?php

use yii\db\Migration;

class m160822_122731_1644_alter_model3d_replica_img extends Migration
{
    public function up()
    {
        $this->addColumn('model3d_replica_img', 'original_model3d_img_id', 'integer not null');
        $this->addForeignKey('model3d_replica_img_original_img', 'model3d_replica_img', 'original_model3d_img_id', 'model3d_img', 'id');
        $this->dropForeignKey('fk_model3d_replica_img_1', 'model3d_replica_img');
        $this->addForeignKey('fk_model3d_replica_img_1', 'model3d_replica_img', 'model3d_id', 'model3d_replica', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('model3d_replica_img_original_img', 'model3d_replica_img');
        $this->dropColumn('model3d_replica_img', 'original_model3d_img_id');
    }
}
