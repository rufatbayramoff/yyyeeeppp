<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_094942_ps_pronter_descriotion extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer` CHANGE `description` `description` VARCHAR(255)  CHARACTER SET utf8  NULL  DEFAULT '';");
    }

    public function down()
    {
        return true;
    }

}
