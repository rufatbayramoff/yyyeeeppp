<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200212_172721_7133_expire_orders_time
 */
class m200212_172721_7133_expire_orders_time extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('UPDATE payment_invoice SET date_expire=DATE(DATE_ADD(created_at, INTERVAL + 1 DAY))');
        $this->execute('UPDATE payment_invoice SET date_expire=DATE(DATE_ADD(created_at, INTERVAL + 7 DAY)) WHERE uuid in (SELECT payment_invoice_uuid FROM payment_bank_invoice)');
    }

    public function safeDown()
    {

    }
}