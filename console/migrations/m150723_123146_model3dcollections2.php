<?php

use yii\db\Schema;
use yii\db\Migration;

class m150723_123146_model3dcollections2 extends Migration
{
    public function up()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $path = realpath(dirname(__DIR__) . '/../db/');
        $sql = file_get_contents($path . '/model3d_collections2.sql');
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function down()
    {
        echo "m150723_123146_model3dcollections2 cannot be reverted.\n";

        return false;
    } 
}
