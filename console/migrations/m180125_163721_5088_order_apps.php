<?php

use yii\db\Migration;

/**
 * Class m180125_163721_5088_order_apps
 */
class m180125_163721_5088_order_apps extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('promo_app', 'order', 'int(11) not null default "0" after code');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('promo_app', 'order');
    }
}
