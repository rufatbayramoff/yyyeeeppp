<?php

use yii\db\Migration;

class m170525_095421_4090_model3d_scale extends Migration
{
    public function up()
    {
        $this->addColumn('model3d_replica', 'scale', 'DECIMAL(15,11) NOT NULL DEFAULT 1');
        $this->alterColumn('model3d_part_properties', 'length', 'DECIMAL(11,3) NOT NULL');
        $this->alterColumn('model3d_part_properties', 'width', 'DECIMAL(11,3) NOT NULL');
        $this->alterColumn('model3d_part_properties', 'height', 'DECIMAL(11,3) NOT NULL');
        $this->alterColumn('model3d_part_properties', 'volume', 'double(25,5) unsigned NOT NULL DEFAULT 0');
        $this->alterColumn('model3d_part_properties', 'area', 'double(25,5) unsigned NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('model3d_replica', 'scale');
    }
}