<?php

use yii\db\Migration;

/**
 * Class m180807_075234_5589_services_alter
 */
class m180807_075234_5589_services_alter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            "ALTER TABLE `company_service` 
        ADD COLUMN `single_price` DECIMAL(7,2) NULL DEFAULT NULL AFTER `primary_image_id`,
        ADD COLUMN `supply_ability` INT(11) NULL DEFAULT NULL AFTER `single_price`,
        ADD COLUMN `supply_ability_time` ENUM('day', 'week', 'month', 'year') NULL DEFAULT NULL AFTER `supply_ability`,
        ADD COLUMN `unit_type` CHAR(15) NOT NULL DEFAULT 'piece' AFTER `supply_ability_time`,
        ADD COLUMN `custom_properties` JSON NULL DEFAULT NULL AFTER `unit_type`,
        ADD COLUMN `videos` JSON NULL DEFAULT NULL AFTER `custom_properties`;
      "
        );
        $this->execute(
            "ALTER TABLE `company_service` 
        ADD COLUMN `avg_production` INT(11) NULL DEFAULT NULL AFTER `unit_type`,
        ADD COLUMN `avg_production_time` ENUM('day', 'week', 'month', 'year') NULL DEFAULT NULL AFTER `avg_production`,
        ADD COLUMN `incoterms` VARCHAR(200) NULL DEFAULT NULL AFTER `avg_production_time`;
        "
        );

        $this->execute("ALTER TABLE `company_service_image` ADD COLUMN `pos` INT(11) NOT NULL DEFAULT '0' AFTER `file_id`;");

        $this->execute(
            'CREATE TABLE `company_service_tag` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `company_service_id` int(11) NOT NULL,
              `tag_id` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `company_service_id_UNIQUE` (`company_service_id`,`tag_id`),
              KEY `fk_company_service_tag_2` (`tag_id`),
              CONSTRAINT `fk_company_service_tag_1` FOREIGN KEY (`company_service_id`) REFERENCES `company_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_company_service_tag_2` FOREIGN KEY (`tag_id`) REFERENCES `site_tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            '
        );

        $this->execute(
            'ALTER TABLE `company_service` 
            ADD COLUMN `moq_prices` JSON NULL DEFAULT NULL AFTER `videos`;
        '
        );

        $this->execute(
            'CREATE TABLE `company_service_certification` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `title` varchar(45) NOT NULL,
                  `file_id` int(11) NOT NULL,
                  `certifier` varchar(45) DEFAULT NULL,
                  `application` varchar(45) DEFAULT NULL,
                  `created_at` datetime NOT NULL,
                  `issue_date` date NOT NULL,
                  `expire_date` date DEFAULT NULL,
                  `company_service_id` INT(11) DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  KEY `fk_csc_1_idx` (`company_service_id`),
                  KEY `fk_csc_2_idx` (`file_id`),
                  CONSTRAINT `fk_csc_1` FOREIGN KEY (`company_service_id`) REFERENCES `company_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  CONSTRAINT `fk_csc_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        '
        );

        $this->execute(
            "CREATE TABLE `company_service_block` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `title` varchar(45) NOT NULL,
              `content` text,
              `created_at` datetime NOT NULL,
              `company_service_id` INT(11) NOT NULL,
              `is_visible` tinyint(1) NOT NULL DEFAULT '1',
              `position` tinyint(3) NOT NULL DEFAULT '1',
              PRIMARY KEY (`id`),
              KEY `fk_company_service_block_1_idx` (`company_service_id`),
              CONSTRAINT `fk_company_service_block_1` FOREIGN KEY (`company_service_id`) 
                REFERENCES `company_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            );
        "
        );

        $this->execute(
            "CREATE TABLE `company_service_block_image` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `block_id` int(11) NOT NULL,
              `position` int(11) NOT NULL DEFAULT '0',
              `file_uuid` varchar(32) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `file_uuid` (`file_uuid`),
              KEY `index3` (`block_id`),
              CONSTRAINT `fk_company_service_block_image_1` FOREIGN KEY (`block_id`) REFERENCES `company_service_block` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
              CONSTRAINT `fk_company_service_block_file_uuid` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
            );"
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company_service', 'single_price');
        $this->dropColumn('company_service', 'supply_ability');
        $this->dropColumn('company_service', 'supply_ability_time');
        $this->dropColumn('company_service', 'unit_type');
        $this->dropColumn('company_service', 'custom_properties');
        $this->dropColumn('company_service', 'videos');
        $this->dropColumn('company_service', 'moq_prices');

        $this->dropColumn('company_service', 'avg_production');
        $this->dropColumn('company_service', 'avg_production_time');
        $this->dropColumn('company_service', 'incoterms');
        $this->dropColumn('company_service_image', 'pos');

        $this->dropTable('company_service_block_image');
        $this->dropTable('company_service_block');
        $this->dropTable('company_service_certification');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180807_075234_5589_services_alter cannot be reverted.\n";

        return false;
    }
    */
}
