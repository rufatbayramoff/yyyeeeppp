<?php

use yii\db\Migration;

/**
 * Class m180503_120758_5202_dislike
 *
 *
 */
class m180503_120758_5202_dislike extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `user_like` ADD COLUMN `is_dislike` TINYINT NOT NULL DEFAULT \'0\' AFTER `object_id`;');

        $this->insert('email_template', [
            'code' => 'ps.order.clientDislike',
            'group' => 'order',
            'language_id' => 'en-US',
            'title' => 'Information about Order #%orderId%',

            'description' => ' params: link, orderId, username, psName',
            'template_html' => 'Hi %psName%,

The customer has disliked the results of order #%orderId%. 
<a href="%link%" style="display: inline-block;margin:10px 0;border-radius: 0px;font-family: sans-serif;padding: 5px 20px;min-width: 40px;font-size: 16px;font-weight: normal;line-height: 20px;border: none;text-decoration:none !important;color: #fff;background-color:#2d8ee0;">View order</a>

Best,
Treatstock',
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user_like', 'is_dislike');
        $this->delete('email_template', [
            'code' => 'ps.order.clientDislike',
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180503_120758_5202_dislike cannot be reverted.\n";

        return false;
    }
    */
}
