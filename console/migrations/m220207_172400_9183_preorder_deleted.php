<?php

use common\components\ArrayHelper;
use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m220207_172400_9183_preorder_deleted
 */
class m220207_172400_9183_preorder_deleted extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute("ALTER TABLE `preorder` CHANGE `status` `status` ENUM('wait_confirm','new','accepted','draft','rejected_by_client','rejected_by_company', 'rejected_by_company_after_client_reject','canceled_by_client','deleted_by_client') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'new'");
        $this->update('preorder', ['status' => 'deleted_by_client'], ['status' => 'canceled_by_client']);
        $preordersId = (new Query())
            ->select('id')
            ->from('preorder')
            ->where(['status' => 'deleted_by_client'])
            ->all();
        $preordersId = ArrayHelper::getColumn($preordersId, 'id');
        $this->update('store_order_attemp', ['status' => 'canceled'], ['preorder_id' => $preordersId]);

        $preordersId = (new Query())
            ->select('id')
            ->from('preorder')
            ->where(['status' => 'rejected_by_client'])
            ->all();
        $preordersId = ArrayHelper::getColumn($preordersId, 'id');
        $this->update('store_order_attemp', ['status' => 'quote'], ['preorder_id' => $preordersId]);

        $customerDeclineReasons = [
            [
                'group'       => 'customer_decline_preorder',
                'title'       => 'Price is too high',
                'description' => '',
                'is_active'   => 1,
                'priority'    => 1
            ],
            [
                'group'       => 'customer_decline_preorder',
                'title'       => 'Changed my mind',
                'description' => '',
                'is_active'   => 1,
                'priority'    => 2
            ],
            [
                'group'       => 'customer_decline_preorder',
                'title'       => 'The company has contacted directly',
                'description' => '',
                'is_active'   => 1,
                'priority'    => 3
            ],
            [
                'group'       => 'customer_decline_preorder',
                'title'       => 'Other',
                'description' => '',
                'is_active'   => 1,
                'priority'    => 4
            ],
        ];

        foreach ($customerDeclineReasons as $declineReason) {
            $this->insert('system_reject', $declineReason);
        }
        $this->dropColumn('ps_machine_history', 'action_id');
        $this->dropColumn('ps_cnc_machine_history', 'action_id');


        $this->execute('ALTER TABLE `preorder_history` CHANGE `preorder_id` `model_id` INT NOT NULL');
        $this->execute('ALTER TABLE `preorder_history` CHANGE `time` `created_at` DATETIME NOT NULL');
        $this->execute('ALTER TABLE `preorder_history` ADD `user_id` INT(11) NOT NULL AFTER `created_at`, ADD INDEX (`user_id`)');
        $this->execute('ALTER TABLE `preorder_history` ADD `source` JSON NOT NULL AFTER `action`');
        $this->execute('ALTER TABLE `preorder_history` CHANGE `action` `action_id` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;');
        $this->execute('ALTER TABLE `preorder_history` CHANGE `data` `result` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');
        $this->execute('ALTER TABLE `preorder_history` CHANGE `result` `result` JSON NULL DEFAULT NULL;');
        $this->execute("UPDATE `preorder_history` set source='{}'");
        $this->execute("UPDATE `preorder_history` set result='{}' where result is null");
        $this->execute('ALTER TABLE `preorder_history` CHANGE `result` `result` JSON NOT NULL');
        $this->execute('ALTER TABLE `preorder_history` ADD `comment` TEXT AFTER `result`');
        $this->execute('ALTER TABLE `preorder_history` ADD CONSTRAINT `fk_preorder_history_user_id` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
