<?php

use yii\db\Migration;

/**
 * Class m210906_130839_product_main_featured_slider_table
 */
class m210906_130839_8638_product_main_feture_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_main_card', [
            'id'                 => $this->primaryKey(),
            'product_common_uid' => $this->string()->unique()->null(),
            'type'               => "enum('feature','machine','material')",
            'position'           => $this->integer(),
        ]);

        $this->createIndex(
            'idx-product_main_card_product_common_uid',
            'product_main_card',
            'product_common_uid'
        );

        $this->addForeignKey(
            'fk-product_main_card_product_common_uid',
            'product_main_card',
            'product_common_uid',
            'product_common',
            'uid'
        );
        $this->insert('product_main_card', ['id' => 1, 'product_common_uid' => null, 'type' => 'machine', 'position' => 1]);
        $this->insert('product_main_card', ['id' => 2, 'product_common_uid' => null, 'type' => 'machine', 'position' => 2]);

        $this->insert('product_main_card', ['id' => 3, 'product_common_uid' => null, 'type' => 'material', 'position' => 1]);
        $this->insert('product_main_card', ['id' => 4, 'product_common_uid' => null, 'type' => 'material', 'position' => 2]);

        $this->insert('product_main_card', ['id' => 5, 'product_common_uid' => null, 'type' => 'feature', 'position' => 1]);
        $this->insert('product_main_card', ['id' => 6, 'product_common_uid' => null, 'type' => 'feature', 'position' => 2]);

        $this->insert('product_main_card', ['id' => 7, 'product_common_uid' => null, 'type' => 'feature', 'position' => 3]);
        $this->insert('product_main_card', ['id' => 8, 'product_common_uid' => null, 'type' => 'feature', 'position' => 4]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_main_card');
    }

}
