<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_004459_geo_country extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('{{%geo_country}}', 'title', 'string(80) NOT NULL');
        $this->alterColumn('{{%geo_country}}', 'iso_code', 'CHAR(2) NOT NULL');
    }

    public function safeDown()
    {
        $this->alterColumn('{{%geo_country}}', 'title', 'string(45) NOT NULL');
        $this->alterColumn('{{%geo_country}}', 'iso_code', 'string(10) NOT NULL');
    }
}
