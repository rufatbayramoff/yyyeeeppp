<?php

use yii\db\Migration;

class m171121_164721_4964_wiki_material_data extends Migration
{
    public function safeUp()
    {
        $this->insert('biz_industry', ['id' => '1', 'title' => 'Medicine', 'photo_url' => '/static/images/common/main-page/industry-prosth.jpg']);
        $this->insert('biz_industry', ['id' => '2', 'title' => 'Robotics', 'photo_url' => '/static/images/common/main-page/industry-robo.jpg']);
        $this->insert('biz_industry', ['id' => '3', 'title' => 'Aerospace', 'photo_url' => '/static/images/common/main-page/industry-aero.jpg']);
        $this->insert('biz_industry', ['id' => '4', 'title' => 'VR', 'photo_url' => '/static/images/common/main-page/industry-vr.jpg']);

    }

    public function safeDown()
    {
        $this->delete('biz_industry');
    }
}
