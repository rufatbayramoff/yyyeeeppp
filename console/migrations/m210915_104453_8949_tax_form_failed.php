<?php

use yii\db\Migration;

/**
 * Class m210915_104453_8949_tax_form_failed
 */
class m210915_104453_8949_tax_form_failed extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE `user_tax_info` CHANGE `classification` `classification` ENUM('Individual','Simple trust','Disregarded entity','Corporation','C Corporation','S Corporation','LLC','Partnership','Trust/estate','Other','Private foundation','Government','Tax-exempt organization','Complex trust','Grantor trust','Estate','Central bank of issue') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

}
