<?php

use yii\db\Schema;
use yii\db\Migration;

class m151119_133404_request_time_reason extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `ps_printer_order_dates`
            ADD COLUMN `request_reason` VARCHAR(245) NULL DEFAULT NULL AFTER `order_id`;");
    }

    public function safeDown()
    {
        $this->dropColumn("ps_printer_order_dates", "request_reason");
    }
}
