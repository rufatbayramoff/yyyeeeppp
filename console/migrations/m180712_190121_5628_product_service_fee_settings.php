<?php

use yii\db\Migration;

/**
 * Class m180712_190121_5628_product_service_fee_settings
 */
class m180712_190121_5628_product_service_fee_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('system_setting', [
            'group_id'    => 10,
            'key'         => 'productServiceFeeParams',
            'value'       => 'json',
            'created_at'  => '2018-07-12 19:12:21',
            'updated_at'  => '2018-07-12 19:12:21',
            'created_user_id' => 1,
            'description' => 'this is fee params for product and services',
            'json'        => json_encode([
                'A'          => 1000,
                'B'          => 200,
                'C'          => 0.6,
                'minPercent' => 25,
//                'maxPercent' => '0.3'
            ]),
            'is_active'   => 1,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
