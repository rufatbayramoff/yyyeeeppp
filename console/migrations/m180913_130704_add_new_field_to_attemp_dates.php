<?php

use yii\db\Migration;

/**
 * Class m180913_130704_add_new_field_to_attemp_dates
 */
class m180913_130704_add_new_field_to_attemp_dates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_attemp_dates', 'scheduled_to_sent_at', 'timestamp null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('store_order_attemp_dates', 'scheduled_to_sent_at');
    }
}
