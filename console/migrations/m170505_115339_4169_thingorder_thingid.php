<?php

use yii\db\Migration;

class m170505_115339_4169_thingorder_thingid extends Migration
{

    public function safeUp()
    {
        $this->execute('ALTER TABLE `thingiverse_order` 
          ADD COLUMN `thing_id` INT NOT NULL DEFAULT 0 AFTER `thingiverse_order_id`;
      ');
    }

    public function safeDown()
    {
        $this->dropColumn('thingiverse_order', 'thing_id');
    }
}
