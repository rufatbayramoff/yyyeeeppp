<?php

use yii\db\Migration;

class m170821_130617_4226_tables extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `msg_topic` 
            CHANGE COLUMN `bind_to` `bind_to` ENUM('order', 'preorder') NULL DEFAULT NULL ;
        ");

        $this->execute("
            CREATE TABLE `cnc_preorder` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `customer_user_id` int(11) NOT NULL,
              `machine_id` int(11) NOT NULL,
              `replica_id` int(11) NOT NULL,
              `data` text,
              `status` enum('new','accepted','rejected') NOT NULL,
              `checker` enum('ps','customer') NOT NULL DEFAULT 'ps',
              `version` int(11) unsigned NOT NULL DEFAULT '1',
              PRIMARY KEY (`id`),
              KEY `fk_cnc_preorder_1_idx` (`customer_user_id`),
              KEY `fk_cnc_preorder_2_idx` (`machine_id`),
              KEY `fk_cnc_preorder_3_idx` (`replica_id`),
              CONSTRAINT `fk_cnc_preorder_1` FOREIGN KEY (`customer_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_cnc_preorder_2` FOREIGN KEY (`machine_id`) REFERENCES `ps_machine` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_cnc_preorder_3` FOREIGN KEY (`replica_id`) REFERENCES `model3d_replica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

        ");

        $this->execute("
            CREATE TABLE `cnc_preorder_file` (
              `preorder_id` int(11) NOT NULL,
              `file_id` int(11) NOT NULL,
              PRIMARY KEY (`preorder_id`,`file_id`),
              KEY `fk_cnc_preorder_file_2_idx` (`file_id`),
              CONSTRAINT `fk_cnc_preorder_file_1` FOREIGN KEY (`preorder_id`) REFERENCES `cnc_preorder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_cnc_preorder_file_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE `cnc_preorder_preset` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `preorder_id` int(11) NOT NULL,
              `part_replica_id` int(11) NOT NULL,
              `material` varchar(255) NOT NULL,
              `qty` int(10) unsigned NOT NULL,
              `postprocessing` text NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_cnc_preorder_preset_1_idx` (`preorder_id`),
              KEY `fk_cnc_preorder_preset_2_idx` (`part_replica_id`),
              CONSTRAINT `fk_cnc_preorder_preset_1` FOREIGN KEY (`preorder_id`) REFERENCES `cnc_preorder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_cnc_preorder_preset_2` FOREIGN KEY (`part_replica_id`) REFERENCES `model3d_replica_part` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE `cnc_preorder_work` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `preorder_id` int(11) NOT NULL,
              `part_replica_id` int(11) DEFAULT NULL,
              `title` varchar(255) NOT NULL,
              `cost` decimal(10,2) NOT NULL,
              `currency` varchar(45) NOT NULL,
              `per` enum('part','batch') NOT NULL,
              `source` enum('api','manual') NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_cnc_preorder_work_1_idx` (`preorder_id`),
              KEY `fk_cnc_preorder_work_2_idx` (`part_replica_id`),
              CONSTRAINT `fk_cnc_preorder_work_1` FOREIGN KEY (`preorder_id`) REFERENCES `cnc_preorder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_cnc_preorder_work_2` FOREIGN KEY (`part_replica_id`) REFERENCES `model3d_replica_part` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->dropTable("cnc_preorder");
        $this->dropTable("cnc_preorder_file");
        $this->dropTable("cnc_preorder_preset");
        $this->dropTable("cnc_preorder_work");

        return true;
    }

}
