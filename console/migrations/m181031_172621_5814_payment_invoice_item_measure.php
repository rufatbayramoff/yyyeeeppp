<?php

use yii\db\Migration;

/**
 * Class m181031_172621_5814_payment_invoice_item_measure
 */
class m181031_172621_5814_payment_invoice_item_measure extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('payment_invoice_item', 'measure','char(15) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
