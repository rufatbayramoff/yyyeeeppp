<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200427_143121_7311_cutting_machine
 */
class m200427_143121_7311_cutting_machine extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(<<<SQL
CREATE TABLE `cutting_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `default_thickness` decimal(5,2) NOT NULL,
  `default_width` decimal(11,0) NOT NULL,
  `default_length` decimal(11,0) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(<<<SQL
CREATE TABLE `cutting_material_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cutting_material_id` int(11) NOT NULL,
  `cutting_color_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cutting_material_id` (`cutting_material_id`),
  KEY `cutting_color_id` (`cutting_color_id`),
  CONSTRAINT `fk_cutting_material_color_id` FOREIGN KEY (`cutting_color_id`) REFERENCES `printer_color` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cutting_material_material_id` FOREIGN KEY (`cutting_material_id`) REFERENCES `cutting_material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
        $this->execute(<<<SQL
CREATE TABLE `cutting_workpiece_material` (
  `uuid` varchar(32) NOT NULL,
  `company_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `width` decimal(10,0) NOT NULL,
  `height` decimal(10,0) NOT NULL,
  `thickness` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `material_id` (`material_id`),
  KEY `company_id` (`company_id`),
  KEY `color_id` (`color_id`),
  CONSTRAINT `fk_cutting_workpiece_material_color_id` FOREIGN KEY (`color_id`) REFERENCES `printer_color` (`id`),
  CONSTRAINT `fk_cutting_workpiece_material_company_id` FOREIGN KEY (`company_id`) REFERENCES `ps` (`id`),
  CONSTRAINT `fk_cutting_workpiece_material_material_id` FOREIGN KEY (`material_id`) REFERENCES `cutting_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(<<<SQL
CREATE TABLE `cutting_machine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `max_width` decimal(10,2) NOT NULL,
  `max_length` decimal(10,2) NOT NULL,
  `min_order_price` decimal(10,2) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
        $this->addColumn('company_service', 'cutting_machine_id', 'int (11) after ps_cnc_machine_id');
        $this->createIndex('company_service_cutting_machine_idx', 'company_service', 'cutting_machine_id', true);
        $this->addForeignKey('fk_company_service_cutting_machine', 'company_service', 'cutting_machine_id', 'cutting_machine', 'id');
        $this->execute(<<<SQL
CREATE TABLE `cutting_machine_processing` (
  `uuid` varchar(32) NOT NULL,
  `cutting_machine_id` int(11) NOT NULL,
  `cutting_material_id` int(11) NOT NULL,
  `thickness` decimal(10,2) NOT NULL,
  `cutting_price` decimal(10,2) NOT NULL,
  `engraving_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `cutting_machine_id` (`cutting_machine_id`),
  KEY `cutting_material_id` (`cutting_material_id`),
  CONSTRAINT `fk_cutting_machine_material_id` FOREIGN KEY (`cutting_material_id`) REFERENCES `cutting_material` (`id`),
  CONSTRAINT `fk_cutting_machine_processing_machine_id` FOREIGN KEY (`cutting_machine_id`) REFERENCES `cutting_machine` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
        $laserCuttingCategory = (new Query())
            ->select('*')
            ->from('company_service_category')
            ->where(['code' => 'Laser-Cutting'])->one();
        if ($laserCuttingCategory) {
            $this->update('company_service_category', ['code' => 'laser-cutting', 'slug' => 'laser-cutting'], ['code' => 'Laser-Cutting']);
        } else {
            $this->insert('company_service_category', [
                'lft'        => 20,
                'rgt'        => 21,
                'depth'      => 2,
                'code'       => 'laser-cutting',
                'slug'       => 'laser-cutting',
                'title'      => 'Laser Cutting',
                'position'   => 1,
                'is_visible' => 1,
                'is_active'  => 1
            ]);
        }

        $this->execute(<<<SQL
INSERT INTO `cutting_material` (`id`, `code`, `title`, `default_thickness`, `default_width`, `default_length`, `is_active`) VALUES
(1, 'pvc', 'Polyvinyl chloride', '3.00', '2000', '3000', 1),
(2, 'plywood', 'Plywood', '3.00', '1200', '2500', 1),
(3, 'acrylic-glass', 'Acrylic glass', '3.00', '2000', '3000', 1);

INSERT INTO `cutting_material_color` (`id`, `cutting_material_id`, `cutting_color_id`) VALUES
(1, 2, 103),
(2, 2, 92),
(3, 2, 3),
(4, 1, 94);
SQL
        );
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}