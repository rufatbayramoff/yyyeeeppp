<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m181217_165321_5814_payment_warnings
 *
 * Set at least one transaction history record
 */
class m181217_165321_5814_payment_warnings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_warning', 'date', 'datetime');
        $this->addColumn('payment_warning', 'description', 'text');
        return true;
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
