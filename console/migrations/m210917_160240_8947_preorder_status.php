<?php

use yii\db\Migration;

/**
 */
class m210917_160240_8947_preorder_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('preorder', ['status'=>'new'], "`status`='wait_confirm' and `confirm_hash` is not null");
        $this->update('preorder', ['confirm_hash' => 'null'], "`status`!='new' and `confirm_hash` is not null");
        $this->execute("ALTER TABLE `preorder` CHANGE `status` `status` ENUM('wait_confirm','new','accepted','rejected','draft','deleted','rejected_by_client','rejected_by_company', 'canceled_by_client') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'new';)");

        $this->update('preorder',['status'=>'rejected_by_company'], 'decline_initiator=3');
        $this->update('preorder',['status'=>'canceled_by_client'], "`status`='rejected' or `status`='deleted'");
        $this->execute("ALTER TABLE `preorder` CHANGE `status` `status` ENUM('wait_confirm','new','accepted', 'draft', 'rejected_by_client','rejected_by_company', 'canceled_by_client') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'new';)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
