<?php

use yii\db\Migration;

class m170803_172521_4225_cnc_machine_history extends Migration
{
    public function safeUp()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `ps_cnc_machine_history` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_id` INT(11) NOT NULL COMMENT 'ps_cnc_machine_id',
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` INT(11) NOT NULL,
  `action_id` VARCHAR(45) NOT NULL,
  `source` JSON NOT NULL,
  `result` JSON NOT NULL,
  `comment` TINYTEXT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `fk_cnc_ps_cnc_machine_id` FOREIGN KEY (`model_id`) REFERENCES `ps_cnc_machine` (`id`),
  CONSTRAINT `fk_cnc_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function safeDown()
    {
        $this->dropTable('ps_cnc_machine_history');
    }

}
