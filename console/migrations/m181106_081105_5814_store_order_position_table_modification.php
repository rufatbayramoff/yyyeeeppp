<?php

use common\modules\payment\fee\FeePercentCondition;
use common\modules\payment\fee\FeePercentRuleForPrice;
use lib\money\Money;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m181106_081105_5814_store_order_position_table_modification
 */
class m181106_081105_5814_store_order_position_table_modification extends Migration
{
    protected $feeParams;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $feeParams = app()->db->createCommand("select json from system_setting where `key` = 'productServiceFeeParams'")->queryScalar();
        $this->feeParams = \yii\helpers\Json::decode($feeParams);

        $this->alterColumn('store_order_position', 'currency_iso', 'char(3) null');

        $positions = (new Query())
            ->select([
                'sop.*'
            ])
            ->from(['sop' => 'store_order_position'])
            ->where(['sop.status' => 'new'])
            ->all();

        foreach ($positions as $position) {
            echo " - Create invoice for position: {$position['id']}, order: {$position['order_id']}\n";
            $this->createInvoice($position);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function generateInvoiceUuid()
    {
        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_invoice')->where('uuid="' . $operationUuid . '"')->one();
        } while ($checkUuid);

        return $operationUuid;
    }

    protected function createInvoice($position)
    {
        $orderData = app()->db->createCommand('select * from store_order where id = :id', ['id' =>  $position['order_id']])->queryOne();
        $companyId = app()->db->createCommand('select id from ps where user_id = :user_id', ['user_id' => $position['user_id']])->queryScalar();

        $paymentInvoice = [
            'uuid'             => $this->generateInvoiceUuid(),
            'created_at'       => $position['created_at'],
            'date_expire'      => date('Y-m-d H:i:s', strtotime($position['created_at']) + (60 * 60 * 24 * 30)),
            'total_amount'     => $position['amount'],
            'currency'         => 'USD',
            'rate_id'          => '1',
            'status'           => 'new',
            'user_id'          => $orderData['user_id'],
            'user_session_id'  => $orderData['user_session_id'] ?? 1,
            'store_order_id'   => $position['order_id'],
            'details'          => 'Additional service invoice for order: #' . $position['order_id'],
            'accounting'       => null,
            'is_settle_submit' => 1,
            'company_id'       => $companyId ?: null
        ];

        $formInvoiceItemsAndAccounting = $this->formInvoiceItemsAndAccounting($paymentInvoice, $position);

        $paymentInvoice['accounting'] = $formInvoiceItemsAndAccounting['accounting'];

        $this->insert('payment_invoice', $paymentInvoice);
        $this->saveItems($formInvoiceItemsAndAccounting['items']);

        $this->update('store_order_position', [
            'primary_payment_invoice_uuid' => $paymentInvoice['uuid'],
        ], [
            'id' => $position['id']
        ]);
    }

    public function formInvoiceItemsAndAccounting($paymentInvoice, $position): array
    {
        $items = [];
        $accounting['invoice_items'] = [];

        $totalCost = $position['amount'];

        $totalCostMoney = Money::create($totalCost, 'USD');

        $feePercentCondition = new FeePercentCondition();
        $feePercentCondition->totalPrice =$totalCostMoney;

        /** @var FeePercentRuleForPrice $ruleForPrice */
        $ruleForPrice = \Yii::createObject([
            'class'               => FeePercentRuleForPrice::class,
            'feePercentCondition' => $feePercentCondition,
            'feeParams'           => $this->feeParams
        ]);

        $fee = $totalCostMoney->getAmount() * $ruleForPrice->detectFeePercent() / 100;

        $uuidItem = \common\components\UuidHelper::generateUuid(4);

        $items[] = [
            'uuid'                 => $uuidItem,
            'payment_invoice_uuid' => $paymentInvoice['uuid'],
            'pos'                  => 1,
            'title'                => $position['title'] ?: 'Item',
            'description'          => 'Additional service invoice for order: #: ' . $position['id'],
            'measure'              => '',
            'qty'                  => 1,
            'total_line'           => $totalCost,
        ];

        $accounting['invoice_items'][$uuidItem] = [
            'additionalServiceId'         => $position['id'],
            'paymentInvoiceItemUuid' => $uuidItem,
            'manufacturer'           => [
                'type'  => 'manufacturer',
                'price' => $totalCost - $fee
            ],
            'fee'                    => [
                'type'  => 'fee',
                'price' => $fee
            ]
        ];

        return [
            'items'      => $items,
            'accounting' => $accounting
        ];
    }

    protected function saveItems($items)
    {
        foreach ($items as $item) {
            $this->insert('payment_invoice_item', $item);
        }
    }
}
