<?php

use yii\db\Migration;

class m170209_123528_3484_thingverse_user_profile extends Migration
{
    public function up()
    {
        $this->insert('user_profile', [
            'user_id' => 112,
            'current_lang' => 'en-US',
            'current_currency_iso' => 'USD',
            'current_metrics' => 'in',
            'timezone_id' => 'America/Los_Angeles',
            'gender' => 'none',
            'default_license_id' => 1
        ]);

        $this->update('user', [
            'notify_config' => '{"order":{"email":"never"},"service":{"email":"never"},"news":{"email":"never"},"rules":{"email":"never"},"personalMessages":{"email":"never"}}',
        ], [
            'id' => 112
        ]);
    }

    public function down()
    {
        $this->delete('user_profile', ['user_id' => 112]);
        $this->update('user', ['notify_config' => null], ['id' => 112]);
        return true;
    }
}
