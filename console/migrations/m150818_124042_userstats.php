<?php

use yii\db\Schema;
use yii\db\Migration;

class m150818_124042_userstats extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `user_statistics` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `downloads` int(11) NOT NULL DEFAULT '0',
            `prints` int(11) NOT NULL DEFAULT '0',
            `favorites` int(11) NOT NULL DEFAULT '0',
            `views` int(11) NOT NULL DEFAULT '0',
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            KEY `fk_user_statistics_1_idx` (`user_id`),
            CONSTRAINT `fk_user_statistics_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
        $this->execute("ALTER TABLE `user_statistics`  ADD UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC);");
        $this->execute("ALTER TABLE `user_statistics`  ADD COLUMN `lastonline_at` TIMESTAMP NOT NULL AFTER `updated_at`");
        return 0;
    }

    public function down()
    {        
        $this->execute("DROP TABLE `user_statistics`;");
        return 0;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
