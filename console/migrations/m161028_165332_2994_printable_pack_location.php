<?php

use yii\db\Schema;
use yii\db\Migration;

class m161028_165332_2994_printable_pack_location extends Migration
{
    public function safeUp()
    {
        $this->addColumn('api_printable_pack', 'geo_location_id', 'int NULL AFTER api_external_system_id');
        $this->addForeignKey('fk_api_printable_pack', 'api_printable_pack', 'geo_location_id', 'geo_location', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_api_printable_pack', 'api_printable_pack');
        $this->dropColumn('api_printable_pack', 'geo_location_id');
    }
}
