<?php

use yii\db\Migration;

/**
 * Class m180907_075118_add_new_email_ps_order_reviewed
 */
class m180907_075118_add_new_email_ps_order_reviewed extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(
            'email_template',
            [
                'code'          => 'psOrderReviewed',
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => "You've received a new review from a customer",
                'description'   => 'Params: clientName, psName, orderId',
                'updated_at'    => dbexpr('NOW()'),
                'template_html' => "Hi %psName%,

A customer has left a review for your business. Please visit your <a href=\"https://www.treatstock.com/workbench/reviews\">sales reviews</a> page to view, reply and share the review on Facebook, Linkedin, Reddit, Messenger or copy paste the link on a web page.

Best,
Treatstock",
                'template_text' => ""
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'psOrderReviewed', 'language_id' => 'en-US']);
    }
}
