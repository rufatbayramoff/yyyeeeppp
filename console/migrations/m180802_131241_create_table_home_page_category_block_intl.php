<?php

use yii\db\Migration;

/**
 * Class m180802_131241_create_table_home_page_category_block_intl
 */
class m180802_131241_create_table_home_page_category_block_intl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          CREATE TABLE `home_page_category_block_intl` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `model_id` int(11) NOT NULL,
          `title` varchar(255) NOT NULL,
          `lang_iso` char(5) NOT NULL DEFAULT 'en-US',
          `is_active` bit(1) DEFAULT b'1',
          PRIMARY KEY (`id`),
          UNIQUE KEY `hpcb_intl_unique_index` (`model_id`,`lang_iso`),
          KEY `hpcb_intl_search_index` (`model_id`,`lang_iso`,`is_active`),
          CONSTRAINT `hpcb_intl_fk_model_id` FOREIGN KEY (`model_id`) REFERENCES `home_page_category_block` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('home_page_category_block_intl');
    }
}
