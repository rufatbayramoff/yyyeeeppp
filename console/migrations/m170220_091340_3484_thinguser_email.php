<?php

use yii\db\Migration;

class m170220_091340_3484_thinguser_email extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `thingiverse_user` ADD COLUMN `email` VARCHAR(145) NULL AFTER `updated_at`;');
    }

    public function down()
    {
        $this->dropColumn('thingiverse_user', 'email');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
