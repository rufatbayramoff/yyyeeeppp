<?php

use yii\db\Migration;

class m161207_091408_2730_attemp_status extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `store_order_attemp` 
            ADD COLUMN `status` VARCHAR(25) NOT NULL AFTER `dates_id`,
            ADD COLUMN `created_at` TIMESTAMP NOT NULL AFTER `status`,
            ADD COLUMN `is_offer` TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER `created_at`,
            ADD INDEX `status` (`status` ASC);
        ");

        $this->execute("
            UPDATE store_order_attemp a, store_order o
            SET a.created_at = o.created_at
            WHERE a.order_id = o.id;
        ");

        $this->execute("
            UPDATE store_order_attemp a, store_order o
            SET a.status = o.order_status
            WHERE a.order_id = o.id;
        ");

        $this->execute("
            UPDATE store_order_attemp a, store_order o
            SET a.status = 'cancelled', o.current_attemp_id = NULL
            WHERE a.status IN ('change_printer', 'offered') AND a.order_id = o.id;
        ");


        $this->execute("
            INSERT INTO store_order_attemp (order_id, printer_id, status, created_at, is_offer) (
                SELECT order_id, printer_id, 'new' as status, created_at, 1 is_offer FROM store_order_offer o
                WHERE o.status = 'new'
            );
        ");

        $this->execute("
            INSERT INTO store_order_attemp_dates (attemp_id) (
                SELECT id as attemp_id 
                FROM store_order_attemp
                WHERE ISNULL(dates_id)
            );
        ");

        $this->execute("
          UPDATE store_order_attemp SET dates_id = id WHERE ISNULL(dates_id);
        ");

        $this->execute("
            INSERT INTO store_order_attemp_delivery (order_attemp_id) (
                SELECT a.id as order_attemp_id 
                FROM store_order_attemp a
                LEFT JOIN store_order_attemp_delivery d on a.id = d.order_attemp_id
                WHERE ISNULL(d.order_attemp_id)
            );
        ");

        $this->execute("
          DROP TABLE `store_order_offer`;
        ");
    }

    public function down()
    {
        echo "m161207_091408_2730_attemp_status cannot be reverted.\n";

        return false;
    }
}
