<?php

use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m220611_225021_9432_customer_service
 */
class m220611_225021_9432_customer_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('ps', ['currency'=>'USD'], 'user_id=35');
        $this->update('payment_invoice', ['currency'=>'USD'], '`currency`="usd"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

}
