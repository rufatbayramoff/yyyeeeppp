<?php

use yii\db\Migration;

/**
 * Class m210301_102216_update_ps_preorder_new_email_template
 */
class m210301_102216_8273_update_ps_preorder_new_email_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update(
            'email_template',
            [
                'template_html' => new \yii\db\Expression("REPLACE(template_html,'https://www.treatstock.com/workbench/preorder/quotes','%link%')")
            ],
            [
                'code' => 'ps.preorder.new'
            ]
        );

        $this->update(
            'email_template',
            [
                'description' => 'Params %quoteId%, %link%, %psName%'
            ],
            [
                'code' => 'ps.preorder.new'
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210301_102216_update_ps_preorder_new_email_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210301_102216_update_ps_preorder_new_email_template cannot be reverted.\n";

        return false;
    }
    */
}
