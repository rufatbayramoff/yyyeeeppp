<?php

use yii\db\Migration;
use yii\db\Query;

class m160916_141721_2647_calculated_cost extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `catalog_cost` (
  `store_unit_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `cost_usd` decimal(5,2) DEFAULT NULL,
  `update_date` datetime NOT NULL,
  UNIQUE KEY `store_unit_country` (`store_unit_id`,`country_id`),
  KEY `store_unit_id` (`store_unit_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `country_foreing_key` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `store_unit_foreing_key` FOREIGN KEY (`store_unit_id`) REFERENCES `store_unit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

    }

    public function down()
    {
        $this->dropTable('catalog_cost');
    }
}