<?php

use yii\db\Schema;
use yii\db\Migration;

class m150824_093704_storeUpdates extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `store_category` DROP COLUMN `description`;"); 
        return 0;
    }

    public function safeDown()
    {
        $this->execute("ALTER TABLE `store_category` ADD COLUMN `description` VARCHAR(245) NULL AFTER `title`;");
        return 0;
    }
}
