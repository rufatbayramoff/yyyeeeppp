<?php

use yii\db\Migration;

class m161209_084257_2730_order_state extends Migration
{
    public function up()
    {
        $this->execute("
            UPDATE store_order SET order_state = 'processing' WHERE order_state = 'new';
        ");

        $this->execute("
            UPDATE store_order, store_order_attemp 
            SET order_state = 'completed' 
            WHERE order_state = 'closed' AND store_order_attemp.status = 'received' AND store_order.current_attemp_id = store_order_attemp.id
        ");

        $this->execute("
            ALTER TABLE `store_order` 
            CHANGE COLUMN `order_state` `order_state` CHAR(25) NOT NULL DEFAULT 'processing' ;
        ");

        $this->execute("
            ALTER TABLE `store_order` 
            DROP COLUMN `order_status`;
        ");
    }

    public function down()
    {
        echo "m161209_084257_2730_order_state cannot be reverted.\n";
        return false;
    }
}
