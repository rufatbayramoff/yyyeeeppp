<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180315_142732_5404_model3d_parts
 */
class m180315_142732_5404_model3d_parts extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_model3d_file_1', 'model3d_part');
        $this->dropIndex('model3d_part_file_id_unique', 'model3d_part');
        $this->addForeignKey('fk_model3d_file_1', 'model3d_part', 'file_id', 'file', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}
