<?php

use yii\db\Migration;

/**
 * Class m210504_160312_8618_pay_page_log
 */
class m210604_160312_8618_pay_page_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `payment_pay_page_log_status` CHANGE `status` `status` ENUM('press_button','processed','done','failed', 'authorize_failed') NOT NULL");
    }
}