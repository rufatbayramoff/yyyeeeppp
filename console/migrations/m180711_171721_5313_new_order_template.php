<?php

use yii\db\Migration;

/**
 * Class m180711_171721_5313_new_order_template
 */
class m180711_171721_5313_new_order_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('UPDATE email_template SET template_html = REPLACE(template_html, \'%newOrdersLink%\', \'%link%\') WHERE code in (\'psNewOrder\', \'psNewOrder3Hour\', \'psNewOrder12Hour\')');
        $this->execute('UPDATE email_template SET template_html = REPLACE(template_html, \'%username%\', \'%psName%\') WHERE code=\'psCancelOrder\'');

        // Work server
        $this->execute('UPDATE email_template SET template_html = REPLACE(template_html, \'by the customer.\', \'by the customer.'."\\n".'Reason: %reason%.\') WHERE code=\'psCancelOrder\'');

        //Test server
        $this->execute('UPDATE email_template SET template_html = REPLACE(template_html, \'has been cancelled!\', \'has been cancelled!'."\\n".'Reason: %reason%.\') WHERE code=\'psCancelOrder\'');
        $this->execute('UPDATE email_template SET template_text = REPLACE(template_text, \'has been cancelled!\', \'has been cancelled!'."\\n".'Reason: %reason%.\') WHERE code=\'psCancelOrder\'');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
