<?php

use yii\db\Schema;
use yii\db\Migration;

class m151218_085728_userprofile extends Migration
{
    public function up()
    { 
        $this->execute("ALTER TABLE `user_profile` DROP COLUMN `address`;");
        $this->execute("ALTER TABLE `user_profile` CHANGE COLUMN `location_id` `address_id` INT(11) NULL DEFAULT NULL ;");
        $this->execute("ALTER TABLE `user_profile` 
            ADD INDEX `fk_user_profile_2_idx1` (`address_id` ASC);
            ALTER TABLE `user_profile` 
            ADD CONSTRAINT `fk_user_profile_2`
              FOREIGN KEY (`address_id`)
              REFERENCES `user_address` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
            ");
    }

    public function down()
    {   
        $this->dropForeignKey('fk_user_profile_2', 'user_profile');
        $this->dropIndex('fk_user_profile_2_idx1', 'user_profile');
        $this->execute("ALTER TABLE `user_profile` CHANGE COLUMN `address_id` `location_id` INT(11) NULL DEFAULT NULL ;");
        $this->addColumn('user_profile', 'address', 'varchar(255)');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
