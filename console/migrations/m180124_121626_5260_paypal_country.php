<?php

use yii\db\Migration;

/**
 * Class m180124_121626_5260_paypal_country
 */
class m180124_121626_5260_paypal_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $isoCodes = array_keys([
            "BY" => "Belarus",
            "MM" => "Burma (Myanmar)",
            "CI" => "Ivory Coast", // Cote d'Ivoire
            "CD" => "Democratic Republic of the Congo",
            "IR" => "Iran",
            "IQ" => "Iraq",
            "KW" => "Kuwait",
            "LB" => "Lebanon",
            "LR" => "Liberia",
            "LY" => "Libya",
            "KP" => "North Korea",
            "QA" => "Qatar",
            "SA" => "Saudi Arabia",
            "SL" => "Sierra Leone",
            "SD" => "Sudan",
            "SY" => "Syria",
            "AE" => "United Arab Emirates",
            "YE" => "Yemen",
            "ZW" => "Zimbabwe",
            "AL" => "Albania",
            "BA" => "Bosnia and Herzegovina",
            "MK" => "Macedonia",
            "CS" => "Serbia and Montenegro",
            "XK" => "Kosovo",
            "AO"=>"",
            "BJ"=>"",
            "BF"=>"",
            "BI"=>"",
            "CM"=>"",
            "CV"=>"",
            "TD"=>"",
            "KM"=>"",
            "CI"=>"",
            "CD"=>"",
            "DJ"=>"",
            "ER"=>"",
            "CD"=>"",
            "DJ"=>"",
            "ET"=>"",
            "GA"=>"",
            "GM"=>"",
            "GN"=>"",
            "GW"=>"",
            "MG"=>"",
            "ML"=>"",
            "MR"=>"",
            "YT"=>"",
            "NA"=>"",
            "NE"=>"",
            "NG"=>"",
            "CG"=>"",
            "RW"=>"",
            "SH"=>"",
            "ST"=>"",
            "SL"=>"",
            "SO"=>"",
            "SZ"=>"",
            "TZ"=>"",
            "TG"=>"",
            "TN"=>"",
            "UG"=>"",
            "ZM"=>"",
            "ZW"=>"",
            "AI"=>"",
            "AW"=>"",
            "BO"=>"",
            "VG"=>"",
            "FK"=>"",
            "GY"=>"",
            "MS"=>"",
            "AN"=>"",
            "PY"=>"",
            "PM"=>"",
            "VC"=>"",
            "SR"=>"",
            "AM"=>"",
            "BT"=>"",
            "BN"=>"",
            "KH"=>"",
            "CK"=>"",
            "KI"=>"",
            "KG"=>"",
            "LA"=>"",
            "MV"=>"",
            "MH"=>"",
            "FM"=>"",
            "MN"=>"",
            "NR"=>"",
            "NP"=>"",
            "NU"=>"",
            "NF"=>"",
            "PG"=>"",
            "PN"=>"",
            "WS"=>"",
            "SB"=>"",
            "LK"=>"",
            "TJ"=>"",
            "TO"=>"",
            "TM"=>"",
            "TV"=>"",
            "VU"=>"",
            "WF"=>"",
            "YE"=>"",
            "AZ"=>"",
            "BY"=>"",
            "MK"=>"",
            "ME"=>"",
            "SJ"=>"",
            "UA"=>"",
            "VA"=>"",
        ]);

        $this->execute('ALTER TABLE `geo_country` ADD COLUMN `paypal_payout` BIT(1) NOT NULL DEFAULT b\'1\' AFTER `region_required`;');

        $this->update('geo_country', ['paypal_payout'=>0], ['iso_code'=>$isoCodes]);

        $this->execute('ALTER TABLE `ps` ADD COLUMN `country_id` INT(11) NULL AFTER `is_designer`;');

        $this->execute('ALTER TABLE `ps` 
            ADD INDEX `fk_ps_1_idx` (`country_id` ASC);
            ALTER TABLE `ps` 
            ADD CONSTRAINT `fk_ps_1`
              FOREIGN KEY (`country_id`)
              REFERENCES `geo_country` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('geo_country', 'paypal_payout');
        $this->dropForeignKey('fk_ps_1', 'ps');
        $this->dropColumn('ps', 'country_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180124_121626_5260_paypal_country cannot be reverted.\n";

        return false;
    }
    */
}
