<?php

use yii\db\Migration;

class m160406_161638_1426_update_free_license_models extends Migration
{
    /**
     *
     */
    public function up()
    {
        $this->execute("UPDATE store_unit SET price_per_print = 0, license_id = 1, license_config = NULL WHERE license_id = 2");
    }

    /**
     * @return bool
     */
    public function down()
    {
        return true;
    }

}
