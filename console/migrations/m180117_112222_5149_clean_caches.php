<?php

use yii\db\Migration;

class m180117_112222_5149_clean_caches extends Migration
{
    public function safeUp()
    {
        $this->dropTable('filepage_cache');
        $this->dropTable('printer_min_cost_hash');
        $this->dropTable('statistics_cache');
    }

    public function safeDown()
    {
        return true;
        echo "m180117_112222_5149_clean_caches cannot be reverted.\n";
        return false;
    }

}
