<?php

use yii\db\Migration;

class m171208_114721_5056_root_equipment extends Migration
{
    public function safeUp()
    {
        $this->insert('equipment_category', ['id' => 1, 'lft' => 1, 'rgt' => 4, 'depth' => 1, 'code' => 'root', 'slug' => '', 'title' => 'Root']);
        $this->insert('equipment_category', ['id' => 2, 'lft' => 2, 'rgt' => 3, 'depth' => 2, 'code' => '3d-printers', 'slug' => '3d-printers', 'title' => '3D Printers']);
    }

    public function safeDown()
    {
        $this->delete('equipment_category', '1=1');
    }
}
