<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m181001_152521_5814_fix_double_order_pays
 *
 * Set at least one transaction history record
 */
class m181001_152521_5814_fix_double_order_pays extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sqlCommand = "SELECT payment_detail.id, payment_detail.amount, dubl.payment_detail_operation_uuid FROM `payment_detail`
LEFT JOIN `payment_detail_operation` as op1 on op1.`uuid` = payment_detail.`payment_detail_operation_uuid`
LEFT JOIN `payment_detail_operation` as ops on ops.`payment_id` = op1.`payment_id`
LEFT JOIN `payment_detail` as dubl on dubl.`payment_detail_operation_uuid` = ops.uuid
WHERE payment_detail.id!=dubl.id and payment_detail.user_id=dubl.user_id and payment_detail.amount=dubl.amount and payment_detail.description like 'Order has been accepted #%' and dubl.description like 'Order has been printed #%'
    ";
        $duplicatePayments = $this->db->createCommand($sqlCommand)->queryAll();
        $fixedOperations = [];
        foreach ($duplicatePayments as $duplicatePayment) {
            if (array_key_exists($duplicatePayment['payment_detail_operation_uuid'], $fixedOperations)) {
                continue;
            }
            $fixedOperations[$duplicatePayment['payment_detail_operation_uuid']] = $duplicatePayment['payment_detail_operation_uuid'];
            $duplicatePaymentRows = (new Query())->select('*')->from('payment_detail')->where(['payment_detail_operation_uuid'=>$duplicatePayment['payment_detail_operation_uuid']])->all();
            if (count($duplicatePaymentRows)!=2) {
                die('Work only with dobule records');
            }
            foreach ($duplicatePaymentRows as $duplicatePaymentRow) {
                $duplicatePaymentRow['description'] = 'Rollback duplicate payment';
                $duplicatePaymentRow['amount'] = 0 - $duplicatePaymentRow['amount'];
                unset($duplicatePaymentRow['id']);
                $this->insert('payment_detail', $duplicatePaymentRow);
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
