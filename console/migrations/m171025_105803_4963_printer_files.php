<?php

use yii\db\Migration;

class m171025_105803_4963_printer_files extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('printer', 'image_file_id');
    }

    public function safeDown()
    {
        $this->addColumn('printer', 'image_file_id', $this->integer(11));
    }

}
