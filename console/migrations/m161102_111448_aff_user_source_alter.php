<?php

use yii\db\Migration;

class m161102_111448_aff_user_source_alter extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `affiliate_user_source` DROP INDEX `index4` ;');
        $this->execute('ALTER TABLE `affiliate_user_source` 
            CHANGE COLUMN `first_url` `first_url` VARCHAR(1245) NULL DEFAULT NULL ,
            CHANGE COLUMN `referrer` `referrer` VARCHAR(1245) NULL DEFAULT NULL ,
            CHANGE COLUMN `user_agent` `user_agent` VARCHAR(1245) NULL DEFAULT NULL ;
        ');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
