<?php

use yii\db\Migration;

class m160511_143958_1796_create_order_notify_log extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `store_order` ADD `messages` TEXT  NULL  AFTER `payment_id`;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `store_order` DROP `messages`;");

        return true;
    }
}
