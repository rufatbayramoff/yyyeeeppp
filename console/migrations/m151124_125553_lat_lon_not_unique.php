<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_125553_lat_lon_not_unique extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `user_location` DROP KEY `lat_lon_UNIQUE`');
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE `user_location` ADD UNIQUE KEY `lat_lon_UNIQUE` (`lat`,`lon`)');
    }
}
