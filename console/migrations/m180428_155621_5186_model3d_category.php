<?php

use yii\db\Migration;

/**
 * Class m180428_155621_5186_model3d_category
 */
class m180428_155621_5186_model3d_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('store_category', [
            'id'               => 1,
            'custom_code_id'   => 1,
            'title'            => 'Model3d',
            'is_active'        => 1,
            'parent_id'        => null,
            'img'              => '',
            'description'      => 'Model3d',
            'full_description' => 'Model3d category'
        ]);
        $this->update('store_category', ['parent_id' => 1], ['id' => [11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24]]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
