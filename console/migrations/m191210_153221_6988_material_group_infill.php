<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m191210_153221_6988_material_group_infill
 */
class m191210_153221_6988_material_group_infill extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('printer_material_group', 'fill_percent_max', 'int(11) null after fill_percent');
        $this->addColumn('model3d_texture', 'infill', 'int(11) null');
    }

    public function safeDown()
    {
        $this->dropColumn('printer_material_group', 'fill_percent_max');
        $this->dropColumn('model3d_texture', 'infill');
    }
}