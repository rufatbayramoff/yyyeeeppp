<?php

use yii\db\Schema;
use yii\db\Migration;

class m151016_143618_postal_label_image extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('ALTER TABLE `store_order_delivery` 
            ADD COLUMN `file_id` INT(11) NULL DEFAULT NULL AFTER `order_id`,
            ADD INDEX `postal_label_file_fk_idx` (`file_id`);
            ');
        $this->execute('ALTER TABLE `store_order_delivery`
            ADD CONSTRAINT `postal_label_file_fk`
            FOREIGN KEY (`file_id`)
              REFERENCES `file` (`id`)
              ON DELETE NO ACTION ON UPDATE NO ACTION;
            ');
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE store_order_delivery DROP FOREIGN KEY `postal_label_file_fk`;');
        $this->execute('ALTER TABLE store_order_delivery DROP KEY `postal_label_file_fk_idx`;');
        $this->execute('ALTER TABLE store_order_delivery DROP COLUMN `file_id`;');
    }
}
