<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180410_113221_5186_product_updated_at
 */
class m180410_113221_5186_product_updated_at extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'updated_at', 'datetime NOT NULL after product_status');
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'updated_at');
    }
}
