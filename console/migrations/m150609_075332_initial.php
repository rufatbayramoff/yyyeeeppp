<?php
use yii\db\Schema;
use yii\db\Migration;

class m150609_075332_initial extends Migration
{

    public function createTable($table, $columns, $options = null)
    {
        try{
            parent::createTable($table, $columns, $options);
        } catch (Exception $ex) {
            echo "[$table] error : " . $ex->getMessage() . PHP_EOL;
        }
    }
    public function up()
    {
         $path = realpath(dirname(__DIR__) . '/../db/');
         $sql = file_get_contents($path . '/init1.sql');
         $this->execute($sql);
         echo 'done';
         return 0;
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        try{            
            $this->dropForeignKey('fk_access_access_group_0', 'access');
            $this->dropForeignKey('fk_geo_city_geo_country_0', 'geo_city');
            $this->dropForeignKey('fk_geo_city_geo_region_1', 'geo_city');
            $this->dropForeignKey('fk_geo_city_geo_timezone_2', 'geo_city');
            $this->dropForeignKey('fk_geo_location_geo_region_0', 'geo_location');
            $this->dropForeignKey('fk_geo_location_geo_country_1', 'geo_location');
            $this->dropForeignKey('fk_geo_location_geo_city_2', 'geo_location');
            $this->dropForeignKey('fk_geo_region_geo_country_0', 'geo_region');
            $this->dropForeignKey('fk_model3d_user_0', 'model3d');
            $this->dropForeignKey('fk_model3d_file_model3d_0', 'model3d_file');
            $this->dropForeignKey('fk_model3d_file_file_1', 'model3d_file');
            $this->dropForeignKey('fk_system_lang_message_system_lang_source_0', 'system_lang_message');
            $this->dropForeignKey('fk_system_setting_system_setting_group_0', 'system_setting');
            $this->dropForeignKey('fk_user_access_user_0', 'user_access');
            $this->dropForeignKey('fk_user_access_access_1', 'user_access');
            $this->dropForeignKey('fk_user_login_log_user_0', 'user_login_log');
            $this->dropForeignKey('fk_user_profile_user_0', 'user_profile');
        } catch (Exception $ex) {

        }

        $this->dropTable('access');
        $this->dropTable('access_group');
        $this->dropTable('email_template');
        $this->dropTable('file');
        $this->dropTable('geo_city');
        $this->dropTable('geo_country');
        $this->dropTable('geo_location');
        $this->dropTable('geo_region');
        $this->dropTable('geo_timezone'); 
        $this->dropTable('model3d');
        $this->dropTable('model3d_file');
        $this->dropTable('model3d_img');
        $this->dropTable('system_lang');
        $this->dropTable('system_lang_message');
        $this->dropTable('system_lang_source');
        $this->dropTable('system_setting');
        $this->dropTable('system_setting_group');
        $this->dropTable('user');
        $this->dropTable('user_access');
        $this->dropTable('user_admin');
        $this->dropTable('user_log');
        $this->dropTable('user_login_log');
        $this->dropTable('user_profile');
    }
}
