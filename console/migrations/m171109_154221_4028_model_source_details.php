<?php

use yii\db\Migration;
use yii\db\Query;

class m171109_154221_4028_model_source_details extends Migration
{
    public function safeUp()
    {
        $this->addColumn('model3d', 'source_details', 'json not null after source');
    }

    public function safeDown()
    {
        $this->dropColumn('model3d', 'source_details');
    }
}
