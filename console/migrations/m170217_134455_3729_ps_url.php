<?php

use yii\db\Migration;
use yii\helpers\Inflector;

class m170217_134455_3729_ps_url extends Migration
{
    public function up()
    {
        $this->addColumn('ps', 'url', 'VARCHAR(255) NOT NULL');
        $this->addColumn('ps', 'url_changes_count', 'TINYINT(4) NOT NULL DEFAULT 0');


        $data = Yii::$app->db->createCommand("SELECT id, title FROM ps")->queryAll();

        foreach ($data as $ps) {
            $url = Inflector::slug($ps['title']);

            $i = null;
            while (Yii::$app->db->createCommand("SELECT COUNT(*) FROM ps WHERE url = :url", ['url' => $url])->queryScalar()){
                $i++;
                $url.= $i;
            }

            Yii::$app->db->createCommand()->update('ps', ['url' => $url], ['id' => $ps['id']])->execute();
        }

        $this->createIndex('ps_url_unique', 'ps', 'url', true);

    }

    public function down()
    {
        $this->dropColumn('ps', 'url');
        $this->dropColumn('ps', 'url_changes_count');

        return true;
    }
}
