<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m191212_174021_7024_preorder_notify
 */
class m191212_174021_7024_preorder_notify extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('preorder', 'status_updated_at', 'datetime null after status');
    }

    public function safeDown()
    {
        $this->dropColumn('preorder', 'status_updated_at', 'datetime null after status');
    }
}