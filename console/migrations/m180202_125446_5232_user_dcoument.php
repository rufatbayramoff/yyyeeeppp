<?php

use yii\db\Migration;

/**
 * Class m180202_125446_5232_user_dcoument
 */
class m180202_125446_5232_user_dcoument extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `user_document` (
              `user_id` INT NOT NULL,
              `file_id` INT NOT NULL,
              PRIMARY KEY (`user_id`, `file_id`),
              INDEX `fk_user_document_file_idx` (`file_id` ASC),
              CONSTRAINT `fk_user_document_user`
                FOREIGN KEY (`user_id`)
                REFERENCES `user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_user_document_file`
                FOREIGN KEY (`file_id`)
                REFERENCES `file` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION);
        ");

        $this->execute("INSERT INTO user_document (SELECT id, document_file_id FROM user WHERE document_file_id IS NOT NULL)");

        $this->dropForeignKey('document_file', 'user');
        $this->dropColumn('user', 'document_file_id');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
