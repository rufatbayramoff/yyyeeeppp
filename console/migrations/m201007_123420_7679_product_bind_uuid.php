<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\components\UuidHelper;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m201007_123420_7679_product_bind_uuid
 */
class m201007_123420_7679_product_bind_uuid extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('product_bind', 'uid', 'char(6) null after id');
        $this->createIndex('product_common_uid_indx', 'product_bind', 'uid', true);
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}
