<?php

use yii\db\Migration;

/**
 * Class m180710_073123_5681_product_incoterms
 */
class m180710_073123_5681_product_incoterms extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `product` CHANGE COLUMN `incoterms` `incoterms` VARCHAR(200) NULL DEFAULT NULL ;');
        $this->execute('ALTER TABLE `ps` CHANGE COLUMN `incoterms` `incoterms` VARCHAR(200) NULL DEFAULT NULL ;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180710_073123_5681_product_incoterms cannot be reverted.\n";

        return false;
    }
    */
}
