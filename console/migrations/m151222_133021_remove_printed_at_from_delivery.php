<?php

use yii\db\Schema;
use yii\db\Migration;

class m151222_133021_remove_printed_at_from_delivery extends Migration
{
    public function up()
    {
        $this->dropColumn('store_order_delivery', 'printed_at');
    }

    public function down()
    {
        echo "m151222_133021_remove_printed_at_from_delivery cannot be reverted.\n";
        return true;
    }

}
