<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m181003_134521_5814_payments_check_users_balances
 *
 * Set at least one transaction history record
 */
class m181003_134521_5814_payments_check_users_balances extends Migration
{

    protected function isThingiverseUser($user)
    {
        if (strpos($user['username'], '_tg') > 0) {
            return true;
        }
        $thingiverseUser = (new Query())
            ->select('thingiverse_user.*')
            ->from('thingiverse_user')
            ->where(['thingiverse_user.user_id' => $user['id']])
            ->one();
        if ($thingiverseUser) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $mapTxt = file_get_contents(Yii::getAlias('@runtime') . '/checkUsersBalances2018.json');
        $amountsMap = json_decode($mapTxt, true);
        $amountAll = (new Query())
            ->select('payment_detail.user_id as user_id, SUM(amount) as amount')
            ->from('payment_detail')
            ->where('user_id>999')
            ->groupBy('user_id')
            ->all();
        $amountsAllMap = ArrayHelper::map($amountAll, 'user_id', 'amount');
        $diff = array_diff_assoc($amountsMap, $amountsAllMap);
        $diffCount = 0;
        foreach ($diff as $dKey => $dVal) {
            $user = (new Query())
                ->select('id, username')
                ->from('user')
                ->where("id=" . $dKey)
                ->one();
            if (($amountsMap[$dKey] < 0) && ($amountsAllMap[$dKey] == 0) && $this->isThingiverseUser($user)) {
                // Skip thingiverse fix balances
            } else {
                echo 'Diff balance for user: ' . $user['username'] . ' Now: ' . $amountsMap[$dKey] . ' Will be:' . $amountsAllMap[$dKey] . "\n";
                $diffCount++;
            }
        }
        echo 'Diff count: ' . $diffCount . "\n";
        if ($diffCount === 0) {
            return true;
        }
        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
