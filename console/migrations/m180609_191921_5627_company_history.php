<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180609_191921_5627_company_history
 */
class m180609_191921_5627_company_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `user_login_log` DROP FOREIGN KEY `fk_user_login_log_1`; ALTER TABLE `user_login_log` ADD CONSTRAINT `fk_user_login_log_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }
}
