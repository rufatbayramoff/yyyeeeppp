<?php

use yii\db\Migration;

/**
 * Class m180813_084610_5779_alter_binds
 */
class m180813_084610_5779_alter_binds extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `company_block_bind` ADD UNIQUE INDEX `index6` (`block_id` ASC, `company_service_id` ASC, `product_uuid` ASC);');
        $this->execute('ALTER TABLE `company_block_bind` DROP INDEX `index5` ;');

        $this->execute('ALTER TABLE `company_block` ADD COLUMN `is_public_profile` TINYINT(1) NOT NULL DEFAULT 0 AFTER `videos`;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('ALTER TABLE `company_block_bind` ADD UNIQUE INDEX `index5` (`block_id` ASC, `product_uuid` ASC);');
        $this->execute('ALTER TABLE `company_block_bind` DROP INDEX `index6` ;');

        $this->dropColumn('company_block', 'is_public_profile');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180813_084610_5779_alter_binds cannot be reverted.\n";

        return false;
    }
    */
}
