<?php

use yii\db\Migration;
use yii\db\Query;

class m170630_144021_4426_cart_user_id_add extends Migration
{
    public function up()
    {
        $this->addColumn('cart', 'user_id', 'int(11) default null');
        $this->addForeignKey('cart_user_id', 'cart', 'user_id', 'user' , 'id');
    }

    public function down()
    {
        $this->dropForeignKey('cart_user_id', 'cart');
        $this->dropColumn('cart', 'user_id');
    }

}
