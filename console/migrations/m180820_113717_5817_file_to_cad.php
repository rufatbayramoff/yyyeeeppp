<?php

use yii\db\Migration;

/**
 * Class m180820_113717_5817_file_to_cad
 */
class m180820_113717_5817_file_to_cad extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('UPDATE product_file set type="cad" WHERE type="file"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180820_113717_5817_file_to_cad cannot be reverted.\n";

        return false;
    }
    */
}
