<?php

use yii\db\Migration;

class m151216_151123_fix_fileds_parcel extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `delivery_parcel` CHANGE `width` `width` DECIMAL(6,2)  NOT NULL;
            ALTER TABLE `delivery_parcel` CHANGE `height` `height` DECIMAL(6,2)  NOT NULL;
            ALTER TABLE `delivery_parcel` CHANGE `weight` `weight` DECIMAL(6,2)  NOT NULL;
            ALTER TABLE `delivery_parcel` CHANGE `length` `length` DECIMAL(6,2)  NOT NULL;
        ");
    }

    public function down()
    {
        echo "m151216_151123_fix_fileds_parcel cannot be reverted.\n";
        return false;
    }
}
