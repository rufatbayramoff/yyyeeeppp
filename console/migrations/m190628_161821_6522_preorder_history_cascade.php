<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.06.19
 * Time: 14:31
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190628_161821_6522_preorder_history_cascade
 */
class m190628_161821_6522_preorder_history_cascade extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `preorder_history` DROP FOREIGN KEY `fk_preorder_history_preorder`; ALTER TABLE `preorder_history` ADD CONSTRAINT `fk_preorder_history_preorder` FOREIGN KEY (`preorder_id`) REFERENCES `preorder`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->execute('ALTER TABLE `preorder_work` DROP FOREIGN KEY `fk_preorder_work_preorder`; ALTER TABLE `preorder_work` ADD CONSTRAINT `fk_preorder_work_preorder` FOREIGN KEY (`preorder_id`) REFERENCES `preorder`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->execute('ALTER TABLE `preorder_work_file` DROP FOREIGN KEY `fk_preorder_work_file_preorder_work`; ALTER TABLE `preorder_work_file` ADD CONSTRAINT `fk_preorder_work_file_preorder_work` FOREIGN KEY (`work_id`) REFERENCES `preorder_work`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
    }

    public function safeDown()
    {
        return true;
    }
}