<?php

use yii\db\Migration;

class m160418_123853_psdelivery extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `store_order_delivery` 
            ADD COLUMN `ps_delivery_details` VARCHAR(255) NULL DEFAULT NULL AFTER `send_message_at`;
        ");
        $this->execute("ALTER TABLE `store_order_delivery` 
            ADD COLUMN `tracking_shipper` VARCHAR(45) NULL DEFAULT NULL AFTER `tracking_number`;
        ");
    }

    public function down()
    {
        $this->dropColumn('store_order_delivery', 'ps_delivery_details');
        $this->dropColumn('store_order_delivery', 'tracking_shipper');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
