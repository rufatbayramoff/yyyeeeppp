<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190524_103821_6533_receipt_buyer_details
 */
class m190524_103821_6533_receipt_buyer_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_receipt_invoice_comment', 'additional_buyer_details' ,'varchar(1024)');
    }

    public function safeDown()
    {
        $this->dropColumn('payment_receipt_invoice_comment', 'additional_buyer_details');
    }
}