<?php

use yii\db\Migration;

class m161205_141048_2730_multi_attemp extends Migration
{
    public function up()
    {

        $this->execute("
        
        ALTER TABLE `store_order` 
            ADD COLUMN `current_attemp_id` INT UNSIGNED NULL AFTER `can_change_ps`,
            ADD INDEX `fk_store_order_5_idx` (`current_attemp_id` ASC);
            ALTER TABLE `store_order` 
            ADD CONSTRAINT `fk_store_order_5`
              FOREIGN KEY (`current_attemp_id`)
              REFERENCES `store_order_attemp` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;


        UPDATE store_order o, store_order_attemp a
        SET o.current_attemp_id = a.id 
        WHERE o.id = a.order_id;
                
        ");
    }

    public function down()
    {
        echo "m161205_141048_2730_multi_attemp cannot be reverted.\n";

        return false;
    }
}
