<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_092829_diax_token_file_status_id extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer_3diax_token` ADD `print_file_status_id` INT  NOT NULL  AFTER `check_status_at`;");
        $this->execute("
        UPDATE ps_printer_3diax_token, ps_printer_file_status
        SET ps_printer_3diax_token.print_file_status_id = ps_printer_file_status.id
        WHERE
            ps_printer_3diax_token.order_id = ps_printer_file_status.order_id
            AND ps_printer_3diax_token.model3d_file_id = ps_printer_file_status.model3d_file_id
        ");
        $this->execute("ALTER TABLE `ps_printer_3diax_token` ADD CONSTRAINT `file_status` FOREIGN KEY (`print_file_status_id`) REFERENCES `ps_printer_file_status` (`id`);");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `ps_printer_3diax_token` DROP FOREIGN KEY `file_status`;");
        $this->execute("ALTER TABLE `ps_printer_3diax_token` DROP `print_file_status_id`;");
        return true;
    }
}
