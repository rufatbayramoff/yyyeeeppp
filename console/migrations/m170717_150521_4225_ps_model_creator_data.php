<?php

use yii\db\Migration;
use yii\db\Query;

class m170717_150521_4225_ps_model_creator_data extends Migration
{
    public function up()
    {
        $query = (new Query())->select('id, ps_id')->from('ps_printer');
        $printers = $query->all();
        $printersMap = [];
        foreach ($printers as $printer) {
            $this->insert('ps_model_creator', ['ps_id' => $printer['ps_id'], 'type' => 'printer', 'ps_printer_id' => $printer['id']]);
            $insertId = Yii::$app->db->getLastInsertID();
            $this->update('ps_printer', ['ps_model_creator_id' => $insertId], ['id' => $printer['id']]);
            $printersMap[$printer['id']] = $insertId;
        }
        $query = (new Query())->select('id, ps_printer_id')->from('ps_model_creator_delivery');
        $deliveries = $query->all();
        foreach ($deliveries as $delivery) {
            $creatorId = $printersMap[$delivery['ps_printer_id']];
            $this->update('ps_model_creator_delivery', ['ps_model_creator_id' => $creatorId], ['id' => $delivery['id']]);
        }
    }

    public function down()
    {
    }
}
