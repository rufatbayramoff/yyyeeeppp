<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_133726_refactor_ps_location extends Migration
{

    public function safeUp()
    {
        $this->dropForeignKey('fk_location_id_2', 'ps');
        $this->dropColumn('ps', 'location_id');
        $this->addColumn('ps', 'phone_country_iso', 'char(2)');
        $this->execute("UPDATE ps, geo_country SET phone_country_iso = iso_code WHERE  geo_country.phone_code = ps.phone_code");
        return true;
    }

    public function safeDown()
    {
        $this->dropColumn('ps', 'phone_country_iso');
        $this->addColumn('ps', 'location_id', 'integer');
        return true;
    }

}
