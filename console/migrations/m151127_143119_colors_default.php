<?php

use yii\db\Schema;
use yii\db\Migration;

class m151127_143119_colors_default extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `ps_printer_color`
            CHANGE COLUMN `price_currency_iso` `price_currency_iso` VARCHAR(10) NOT NULL DEFAULT 'USD',
            CHANGE COLUMN `price_measure` `price_measure` CHAR(10) NOT NULL DEFAULT 'gr';");
    }

    public function safeDown()
    {

    }
}
