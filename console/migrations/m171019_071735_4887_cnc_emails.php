<?php

use yii\db\Migration;

class m171019_071735_4887_cnc_emails extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'ps.cnc.preorder.new', 'service', 'en-US', 'You have received a new CNC quote', NULL, '2017-10-16 07:03:47', '<p>Hello %psName%!</p> <p>You have received a new CNC quote #%orderId% on Treatstock and have 24 hours to accept it <a href=\"%newOrdersLink%\">here</a></p> <p>Best regards, Treatstock</p>', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'client.cnc.preorder.offer', 'service', 'en-US', 'Provider has created an offer on your quote #%orderId%', NULL, '2017-10-16 07:03:47', 'Hello %clientName%,<br /> 
            The provider has reviewed the order #%orderId% and suggested the following offer. <br /> 
            Review this offer <a href=\"%ordersLink%\">click here</a>. 
            <br /> <br /> 
            Best Regards, <br /> 
            Treatstock', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'ps.cnc.preorder.returnOffer', 'service', 'en-US', 'Customer\'s reply to the offer #%orderId%', NULL, '2017-10-16 07:03:47', 'Customer reviewed the offer and proposed a susgestion. For more details please transfer to the <a href=\"%newOrdersLink%\"> print requests</a>', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'ps.cnc.order.new', 'service', 'en-US', 'Quote #%orderId% is confirmed by the customer.', NULL, '2017-10-16 07:03:47', 'Customer accepted offer orderId. The payment received successfully. Now you can fill an order.', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'ps.cnc.preorder.declineByCustomer', 'service', 'en-US', '#%orderId% was cancelled by the customer', NULL, '2017-10-16 07:03:47', '#%orderId% was cancelled by the customer', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'client.cnc.preorder.declineByPs', 'service', 'en-US', 'Your #%orderId% was denied by provider', NULL, '2017-10-16 07:03:47', 'Your #%orderId% was denied by provider by reason: %reason%', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'client.cnc.preorder.declineByModerator', 'service', 'en-US', '#%orderId% was cancelled ', NULL, '2017-10-16 07:03:47', 'CNC Quote #%orderId% was cancelled by reason: %reason%.', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'ps.cnc.preorder.declineByModerator', 'service', 'en-US', '#%orderId% was cancelled ', NULL, '2017-10-16 07:03:47', '<p>CNC Quote #%orderId% was cancelled by reason: %reason%.</p>
            <p>
            Embed our widget on your website for free... therefore you get orders from e.i. visitors can create cnc orders: <a href=\"%toolsLink%\">%toolsLink%</a>.', NULL, '1' );
        ");
    }

    public function safeDown()
    {

        $this->delete('email_template', [
            'code' => [
                'ps.cnc.preorder.new',
                'client.cnc.preorder.offer',
                'ps.cnc.preorder.returnOffer',
                'ps.cnc.order.new',
                'ps.cnc.preorder.declineByCustomer',
                'client.cnc.preorder.declineByPs',
                'client.cnc.preorder.declineByModerator',
                'ps.cnc.preorder.declineByModerator',
            ]
        ]);



        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171019_071735_4887_cnc_emails cannot be reverted.\n";

        return false;
    }
    */
}
