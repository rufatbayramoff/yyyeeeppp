<?php

use common\interfaces\FileBaseInterface;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180507_085521_5541_printer_material_group
 */
class m180507_085521_5541_printer_material_group extends Migration
{
    public function addAdminFile($fileInfo)
    {
        $fields = [
            'uuid',
            'name',
            'path',
            'stored_name',
            'extension',
            'size',
            'created_at',
            'updated_at',
            'deleted_at',
            'user_id',
            'server',
            'status',
            'md5sum',
            'last_access_at',
            'ownerClass',
            'ownerField',
            'is_public',
            'path_version',
            'expire'
        ];
        $fieldKeys = array_combine($fields, $fields);
        $insertValue = array_intersect_key($fileInfo, $fieldKeys);
        $this->insert('file_admin', $insertValue);
    }



    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('printer_material_group', 'photo_file_uuid', 'varchar(32) NULL after photo_file_id');
        $this->addForeignKey('printer_material_group_fk', 'printer_material_group', 'photo_file_uuid', 'file_admin', 'uuid');

        $printerMaterialGroups = (new Query())->select('*')->from('printer_material_group')->where('photo_file_id is not null')->all();
        foreach ($printerMaterialGroups as $printerMaterialGroup) {
            $fileInfo = (new Query())->select('*')->from('file')->where(['id' => $printerMaterialGroup['photo_file_id']])->one();
            $this->addAdminFile($fileInfo);
            $this->update('printer_material_group', ['photo_file_uuid' => $fileInfo['uuid']], 'id=' . $printerMaterialGroup['id']);
        }

        $this->dropForeignKey('printer_material_group_photo_file_id', 'printer_material_group');
        $this->dropColumn('printer_material_group', 'photo_file_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
