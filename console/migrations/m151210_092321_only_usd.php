<?php

use yii\db\Schema;
use yii\db\Migration;

class m151210_092321_only_usd extends Migration
{
    public function up()
    {
        $this->execute("UPDATE payment_currency SET is_active=0 WHERE currency_iso!='USD'");
    }

    public function down()
    { 
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
