<?php

use yii\db\Migration;

class m160803_082414_2419_alter_stats_un extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_statistics` 
            CHANGE COLUMN `lastonline_at` `lastonline_at` TIMESTAMP NULL DEFAULT NULL ;
        ");
        $this->execute("ALTER TABLE `user_statistics` 
            CHANGE COLUMN `downloads` `downloads` INT(11) UNSIGNED NOT NULL DEFAULT '0' ,
            CHANGE COLUMN `prints` `prints` INT(11) UNSIGNED NOT NULL DEFAULT '0' ,
            CHANGE COLUMN `favorites` `favorites` INT(11) UNSIGNED NOT NULL DEFAULT '0' ,
            CHANGE COLUMN `views` `views` INT(11) UNSIGNED NOT NULL DEFAULT '0' ;
            ");
    }

    public function down()
    {
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
