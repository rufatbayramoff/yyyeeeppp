<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%model3d_part_cnc_params}}`.
 */
class m201221_120830_7943_add_error_column_to_model3D_part_cnc_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('model3d_part_cnc_params','error_type',$this->string());
        $this->addColumn('model3d_part_cnc_params','error_msg',$this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('model3d_part_cnc_params','error_type');
        $this->dropColumn('model3d_part_cnc_params','error_msg');
    }
}
