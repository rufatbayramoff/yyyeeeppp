<?php

use yii\db\Migration;

class m171023_114821_3946_transaction_id_fix extends Migration
{
    public function safeUp()
    {
        if (!Yii::$app->db->schema->getTableSchema('payment_invoice')->getColumn('transaction_id')) {
            $this->execute("
                ALTER TABLE `payment_invoice` ADD COLUMN `transaction_id` int(11) DEFAULT NULL;
                ALTER TABLE `payment_invoice` ADD KEY `fk_payment_invoice_3_idx` (`transaction_id`);
                ALTER TABLE `payment_invoice` ADD CONSTRAINT `fk_payment_invoice_3` FOREIGN KEY (`transaction_id`) REFERENCES `payment_transaction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
            ");
        }
    }

    public function safeDown()
    {
        return false;
    }
}
