<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\modules\translation\components\DbI18n;
use common\modules\translation\components\Statistics;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200831_160921_7681_sql_words_count
 */
class m200831_160921_7681_sql_words_count extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        Statistics::fixWordscountSqlFunction();
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}