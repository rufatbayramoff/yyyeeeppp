<?php

use yii\db\Migration;

/**
 * Class m210823_165022_8855_seo_autofill_tpls
 */
class m210823_165022_8855_seo_autofill_tpls extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `seo_page_autofill` DROP FOREIGN KEY `fk_seo_page_autofill_1`; ALTER TABLE `seo_page_autofill` ADD CONSTRAINT `fk_seo_page_autofill_1` FOREIGN KEY (`seo_page_id`) REFERENCES `seo_page`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT; ALTER TABLE `seo_page_autofill` DROP FOREIGN KEY `fk_seo_page_autofill_2`; ALTER TABLE `seo_page_autofill` ADD CONSTRAINT `fk_seo_page_autofill_2` FOREIGN KEY (`template_id`) REFERENCES `seo_page_autofill_template`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT; ALTER TABLE `seo_page_autofill` DROP FOREIGN KEY `fk_seo_page_autofill_3`; ALTER TABLE `seo_page_autofill` ADD CONSTRAINT `fk_seo_page_autofill_3` FOREIGN KEY (`seo_page_intl_id`) REFERENCES `seo_page_intl`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;');
        // DELETE seo_page from seo_page LEFT JOIN seo_page_autofill ON seo_page.id=seo_page_autofill.seo_page_id WHERE seo_page_autofill.template_id=109
        return ;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
