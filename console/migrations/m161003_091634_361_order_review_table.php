<?php

use yii\db\Migration;

class m161003_091634_361_order_review_table extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `store_order_review` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `order_id` INT NOT NULL,
          `store_unit_id` INT NOT NULL,
          `ps_id` INT NOT NULL,
          `rating_speed` TINYINT UNSIGNED NOT NULL,
          `rating_quality` TINYINT UNSIGNED NOT NULL,
          `rating_communication` TINYINT UNSIGNED NOT NULL,
          `comment` TEXT NULL,
          `file_ids` TEXT NULL,
          `status` ENUM('new', 'moderated', 'rejected') NOT NULL,
          `created_at` DATETIME NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `ps` (`ps_id` ASC),
          INDEX `unit` (`store_unit_id` ASC),
          INDEX `order` (`order_id` ASC),
          CONSTRAINT `fk_store_order`
            FOREIGN KEY (`order_id`)
            REFERENCES `store_order` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_ps`
            FOREIGN KEY (`ps_id`)
            REFERENCES `ps` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_store_unit`
            FOREIGN KEY (`store_unit_id`)
            REFERENCES `store_unit` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);
        ");

    }

    public function down()
    {
        $this->dropTable("store_order_review");
        return true;
    }

}
