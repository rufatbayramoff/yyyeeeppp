<?php

use yii\db\Migration;

/**
 * Class m180427_110857_5347_ps_cover_column
 */
class m180427_110857_5347_ps_cover_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `ps` 
            ADD COLUMN `cover_file_id` INT(11) NULL AFTER `currency`,
            ADD INDEX `fk_ps_cover_file_idx` (`cover_file_id` ASC);
            ALTER TABLE `ps` 
            ADD CONSTRAINT `fk_ps_cover_file`
              FOREIGN KEY (`cover_file_id`)
              REFERENCES `file` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('ps', 'cover_file_id');
        return true;
    }
}
