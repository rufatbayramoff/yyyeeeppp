<?php

use yii\db\Migration;

class m170327_104808_3956_thing_file_create extends Migration
{
    public function up()
    {
        $this->execute(
            'CREATE TABLE `thingiverse_thing_file` (
              `thingfile_id` int(11) NOT NULL,
              `thing_id` int(11) NOT NULL,
              `name` varchar(245) DEFAULT NULL,
              `size` int(11) DEFAULT NULL,
              `url` varchar(245) DEFAULT NULL,
              `download_url` varchar(245) DEFAULT NULL,
              `thumb` varchar(245) DEFAULT NULL,
              `render` varchar(245) DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `file_id` int(11) DEFAULT NULL,
              PRIMARY KEY (`thingfile_id`),
              UNIQUE KEY `thingfile_id_UNIQUE` (`thingfile_id`),
              UNIQUE KEY `file_id_UNIQUE` (`file_id`),
              KEY `fk_thingiverse_thing_file_1_idx` (`file_id`),
              CONSTRAINT `fk_thingiverse_thing_file_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
        );
    }

    public function down()
    {
        $this->dropTable('thingiverse_thing_file');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
