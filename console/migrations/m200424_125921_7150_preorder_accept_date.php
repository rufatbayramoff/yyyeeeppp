<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200403_172221_7222_force_invalid_address
 */
class m200424_125921_7150_preorder_accept_date extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('preorder', 'offer_accepted_at', "datetime after offer_description");
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropColumn('preorder', 'offer_accepted_at');
    }
}