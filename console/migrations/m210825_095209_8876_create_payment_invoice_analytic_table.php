<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payment_invoice_analytic}}`.
 */
class m210825_095209_8876_create_payment_invoice_analytic_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payment_invoice_analytic}}', [
            'id' => $this->primaryKey(),
            'sort' => 'ENUM("default", "low_price", "price", "rating", "distance")',
            'payment_invoice_uuid' => $this->string(6)->notNull()->unique(),
        ]);

        // creates index for column `payment_invoice_uuid`
        $this->createIndex(
            'idx-payment-invoice-analytic_payment_invoice_uuid',
            'payment_invoice_analytic',
            'payment_invoice_uuid'
        );

        // add foreign key for table `payment_invoice`
        $this->addForeignKey(
            'fk-payment-invoice-analytic_payment_invoice_uuid',
            'payment_invoice_analytic',
            'payment_invoice_uuid',
            'payment_invoice',
            'uuid',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payment_invoice_analytic}}');
    }
}
