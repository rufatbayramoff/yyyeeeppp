<?php

use yii\db\Migration;

class m171222_145226_5113_affiliate_fee extends Migration
{
    public function safeUp()
    {
        $this->execute(
            'ALTER TABLE `affiliate_award_rule` 
        ADD COLUMN `order_type_widget` BIT(1) NULL AFTER `order_type_any`,
        ADD COLUMN `fee_type` ENUM(\'fee_over\', \'fee_in\') NULL DEFAULT \'fee_in\' COMMENT \'Fee Type\' AFTER `reward_amount`;'
        );

        $this->insert('store_pricer_type', [
            'type' => 'affiliate_fee',
            'description' => 'Affiliate Fee',
            'operation' => ''
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('affiliate_award_rule', 'order_type_widget');
        $this->dropColumn('affiliate_award_rule', 'fee_type');

        $this->delete('store_pricer_type', ['type'=>'affiliate_fee']);
    }
}
