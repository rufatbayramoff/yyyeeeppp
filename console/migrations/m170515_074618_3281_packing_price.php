<?php

use yii\db\Migration;

class m170515_074618_3281_packing_price extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `ps_printer_delivery` 
            ADD COLUMN `packing_price` DECIMAL(7,2) UNSIGNED NULL DEFAULT NULL AFTER `free_delivery`;
        ");
    }

    public function down()
    {
        $this->dropColumn('ps_printer_delivery', 'packing_price');
        return true;
    }
}
