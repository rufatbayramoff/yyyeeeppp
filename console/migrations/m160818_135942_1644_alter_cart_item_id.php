<?php

use yii\db\Migration;

class m160818_135942_1644_alter_cart_item_id extends Migration
{
    public function up()
    {
        $this->addColumn('cart_item', 'id', 'pk FIRST');
    }

    public function down()
    {
        echo 'Can`t revert migration m160818_135942_1644_alter_cart_item_id';
        return false;
    }
}
