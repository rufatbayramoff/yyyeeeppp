<?php

use yii\db\Migration;
use yii\db\Query;

class m160629_115821_1644_fill_model3d_color_data extends Migration
{

    public function getMaterialId($materialCode)
    {
        $material = (new Query())
            ->select('id')
            ->from('printer_material')
            ->where(['filament_title' => $materialCode])
            ->one();
        if (!$material) {
            throw new UnexpectedValueException('Cant find printer material for code: ' . $materialCode);
        }
        return $material['id'];
    }

    public function getColorId($colorCode)
    {
        $color = (new Query())
            ->select('id')
            ->from('printer_color')
            ->where(['title' => $colorCode])
            ->one();
        if (!$color) {
            throw new UnexpectedValueException('Cant find printer color for code: ' . $colorCode);
        }
        return $color['id'];
    }

    public function safeUp()
    {
        $model3ds = (new Query())
            ->select('id, default_material, default_color')
            ->from('model3d')
            ->all();
        foreach ($model3ds as $model3d) {
            try {
                $materialId = $this->getMaterialId($model3d['default_material']);
                $colorId = $this->getColorId($model3d['default_color']);
            } catch (UnexpectedValueException $e) {
                throw new UnexpectedValueException($e->getMessage() . "\nModel3d id: " . $model3d['id'] . "\n");
            }

            $this->insert(
                'model3d_color',
                [
                    'model3d_id'          => $model3d['id'],
                    'printer_material_id' => $materialId,
                    'printer_color_id'    => $colorId,
                ]
            );
        };
    }

    public function safeDown()
    {
        return false;
    }
}
