<?php

use yii\db\Migration;

class m171124_163921_4964_wiki_material_industry extends Migration
{
    public function safeUp()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_industry` (
  `wiki_material_id` INT(11) NOT NULL,
  `biz_industry_id` INT(11) NOT NULL,
  KEY `wiki_material_id` (`wiki_material_id`),
  KEY `biz_industry_id` (`biz_industry_id`),
  CONSTRAINT `fk_wiki_material_industry_biz_industry` FOREIGN KEY (`biz_industry_id`) REFERENCES `biz_industry` (`id`),
  CONSTRAINT `fk_wiki_material_industry_id` FOREIGN KEY (`wiki_material_id`) REFERENCES `wiki_material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );


    }

    public function safeDown()
    {
        $this->dropTable('wiki_material_industry');
    }
}
