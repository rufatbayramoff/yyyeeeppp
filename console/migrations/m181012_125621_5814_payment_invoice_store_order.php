<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m181012_125621_5814_payment_invoice_store_order
 *
 * Payment invoice store order
 */
class m181012_125621_5814_payment_invoice_store_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
CREATE TABLE `payment_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `level` enum('debug','info','warning','error') NOT NULL DEFAULT 'info',
  `type` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `info` json NOT NULL,
  `search_markers` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment_log');
    }
}
