<?php

use yii\db\Schema;
use yii\db\Migration;

class m150730_133640_storeInit extends Migration
{
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $path = realpath(dirname(__DIR__) . '/../db/');
        $sqlString = file_get_contents($path . '/store_init.sql');
        $sqls = explode(";", $sqlString);
        foreach($sqls as $sql){
            $sql = trim($sql);
            if(empty($sql)){
                continue;
            }
            $this->execute($sql);
        }
        echo 'done';
        return 0;
    }

    public function safeDown()
    {
        echo "m150730_133640_storeInit cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
