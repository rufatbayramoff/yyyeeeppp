<?php

use yii\db\Migration;

class m170305_145621_3900_file_job extends Migration
{
    public function up()
    {
        $this->addColumn('file_job', 'output', 'text');
    }

    public function down()
    {
        $this->dropColumn('file_job', 'output');
    }
}
