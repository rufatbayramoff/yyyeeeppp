<?php

use yii\db\Migration;

/**
 * Class m180307_142732_5392_model3dpart_original
 */
class m180307_142732_5392_model3dpart_original extends Migration
{
    public function safeUp()
    {
        $this->addColumn('model3d_part', 'model3d_part_properties_original_id', 'int(11) after model3d_part_properties_id');
        $this->execute('UPDATE model3d_part SET model3d_part_properties_original_id=model3d_part_properties_id ');
        $this->addForeignKey('fk_model3d_part_properties_original_id', 'model3d_part', 'model3d_part_properties_original_id', 'model3d_part_properties', 'id');

        $this->addColumn('model3d_replica_part', 'model3d_part_properties_original_id', 'int(11) after model3d_part_properties_id');
        $this->execute('UPDATE model3d_replica_part SET model3d_part_properties_original_id=model3d_part_properties_id ');
        $this->addForeignKey('fk_model3d_replica_part_properties_original_id', 'model3d_replica_part', 'model3d_part_properties_original_id', 'model3d_part_properties', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_model3d_part_properties_original_id', 'model3d_part');
        $this->dropColumn('model3d_part', 'model3d_part_properties_original_id');

        $this->dropForeignKey('fk_model3d_replica_part_properties_original_id', 'model3d_replica_part');
        $this->dropColumn('model3d_replica_part', 'model3d_part_properties_original_id');
    }
}
