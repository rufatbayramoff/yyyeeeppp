<?php

use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m211203_134253_9146_empty_services.php
 */
class m211203_134253_9146_empty_services extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute("ALTER TABLE `company_service` CHANGE `moderator_status` `moderator_status` ENUM('new','approved','approvedReviewing','rejected','rejectedReviewing','unavailableReviewing','draft','pending')");
        $this->update('company_service', ['moderator_status' => 'pending'], ['moderator_status' => ['new', 'draft']]);
        $psPrinters = (new Query())
            ->select('*')
            ->from('ps_printer')
            ->all();
        foreach ($psPrinters as $psPrinter) {
            $newStatus = [
                'visibility'       => 'nowhere',
                'moderator_status' => 'pending'
            ];
            if ($psPrinter['user_status'] == 'active') {
                $newStatus['visibility'] = 'everywhere';
            }
            $moderatorStatusMap            = [
                'new'      => 'pending',
                'updated'  => 'approvedReviewing',
                'checking' => 'pending',
                'checked'  => 'approved',
                'banned'   => 'rejected',
                'rejected' => 'rejected'
            ];
            $newStatus['moderator_status'] = $moderatorStatusMap[$psPrinter['moderator_status']];
            $this->update('company_service', $newStatus, ['ps_printer_id' => $psPrinter['id']]);
        }
        $this->dropColumn('ps_printer', 'user_status');
        $this->dropColumn('ps_printer', 'moderator_status');
        $this->execute("ALTER TABLE `company_service` CHANGE `moderator_status` `moderator_status` ENUM('new','approved','approvedReviewing','rejected','rejectedReviewing','unavailableReviewing','draft','pending') NOT NULL;");
        $this->execute("ALTER TABLE `company_service` CHANGE `visibility` `visibility` ENUM('everywhere','widgetOnly','nowhere') NOT NULL DEFAULT 'everywhere'");
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
