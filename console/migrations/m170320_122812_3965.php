<?php

use yii\db\Migration;
use yii\helpers\Json;

class m170320_122812_3965 extends Migration
{
    public function up()
    {
        $this->addColumn('store_order_history', 'data', 'text DEFAULT NULL');

        $transaction = Yii::$app->db->beginTransaction();

        try{

            $this->execute("UPDATE store_order_history SET action_id = 'order_updated' WHERE `comment` = 'go to change offer'");
            $this->execute("UPDATE store_order_history SET action_id = 'cancel_order' WHERE `action_id` = 'order_status' AND `comment` LIKE 'Reason%'");
            $this->execute("UPDATE store_order_history SET action_id = 'cancel_order' WHERE `action_id` = 'order_status' AND `comment` LIKE '{\"reason\":%'");

            $updateItem = function (&$item, $data) {

                $this->update('store_order_history', ['data' => Json::encode($data)], ['id' => $item['id']]);

            };

            $resolveAttemptId = function ($item) {
                return (int) Yii::$app->db->createCommand("SELECT id FROM store_order_attemp WHERE order_id = $item[order_id] ORDER BY id DESC LIMIT 1 ")->queryScalar();
            };

            /** @var [][] $items */
            $items = Yii::$app->db->createCommand("SELECT * FROM store_order_history")->queryAll();




            foreach ($items as &$item) {


                switch ($item['action_id']) {

                    case 'item_added':
                        preg_match_all('/^\d+/', $item['comment'], $matches);
                        $data = ['orderItemId' => (int)$matches[0][0]];
                        $updateItem($item, $data);
                        break;

                    case 'order_updated':
                        break;


                    case 'order_status':
                        preg_match_all('/old\[(.+)\] new\[(.+)\]/', $item['comment'], $matches);

                        $data = [
                            'attemptId' => $resolveAttemptId($item),
                            'oldStatus' => $matches[1][0],
                            'newStatus' => $matches[2][0],
                        ];

                        $updateItem($item, $data);

                        break;


                    case 'decline_reason':

                        $d = Json::decode($item['comment']);

                        $data = [

                            'attemptId' => $resolveAttemptId($item),
                            'reason' => $d['reason'],
                            'comment' => $d['comments']
                        ];

                        $updateItem($item, $data);
                        break;


                    case 'payment_status':

                        preg_match_all('/old\[(.*)\] new\[(.+)\]/', $item['comment'], $matches);

                        $data = [
                            'oldStatus' => $matches[1][0],
                            'newStatus' => $matches[2][0],
                        ];

                        $updateItem($item, $data);

                        break;


                    case 'user_cancel_order':
                    case 'cancel_order':

                        if (strpos($item['comment'], '{"reason"') === 0) {

                            $d = Json::decode($item['comment']);
                            $data = [
                                'reason' => $d['reason'],
                                'comment' => $d['comments']
                            ];
                        }
                        elseif (strpos($item['comment'], 'reason[') === 0) {
                            preg_match_all('/reason\[(.*)\] descr\[(.*)\]/', $item['comment'], $matches);


                            $data = [
                                'reason' => $matches[1][0],
                                'comment' => $matches[2][0],
                            ];

                        }
                        else {
                            $data = [
                                'comment' => $item['comment']
                            ];
                        }

                        $updateItem($item, $data);
                        break;


                    case 'rsa_tax':

                        preg_match_all('/: (.+), total (.+)/', $item['comment'], $matches);

                        $data = [
                            'fee' => (float)$matches[1][0],
                            'rsaFee' => (float)$matches[2][0],
                        ];

                        $updateItem($item, $data);
                        break;


                    case 'accept_attemp':


                        $d = Json::decode($item['comment']);

                        $data = [
                            'attemptId' => $d['attempId'],
                        ];

                        $updateItem($item, $data);
                        break;


                    default :
                        throw new Exception("Bad acion id $item[action_id]");
                }
            }

            $transaction->commit();
        }
        catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        catch (Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public function down()
    {
        $this->dropColumn('store_order_history', 'data');
        return true;
    }
}
