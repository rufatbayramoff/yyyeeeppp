<?php

use yii\db\Migration;

class m161021_111102_2830_affiliate_api_key extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `affiliate_resource` ADD COLUMN `api_key` VARCHAR(45) NULL AFTER `description`');
    }

    public function down()
    {
        $this->dropColumn('affiliate_resource', 'api_key');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
