<?php

use yii\db\Migration;

/**
 * Class m180215_102957_5309_new_messages_email_templates
 */
class m180215_102957_5309_new_messages_email_templates extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
        
            INSERT INTO `email_template`(
                `id`,
                `code`,
                `group`,
                `language_id`,
                `title`,
                `description`,
                `updated_at`,
                `template_html`,
                `template_text`,
                `is_active`
            ) VALUES (
                NULL ,
                'preorder.userNewMessage',
                'personalMessages',
                'en-US',
                'You have a new message for quote #%preorderId% on Treatstock',
                'You have a new message for quote #%preorderId% on Treatstock',
                NOW(),
                '
                    Hi %username%,

                    You\'ve received a new message on Treatstock from %messageUsername%. 
                    <a href=\"%messageLink%\" style=\"display: inline-block;margin:10px 0;border-radius: 0px;font-family: sans-serif;padding: 5px 20px;min-width: 40px;font-size: 16px;font-weight: normal;line-height: 20px;border: none;text-decoration:none !important;color: #fff;background-color:#2d8ee0;\">View message</a>
                    
                    Best,
                    Treatstock
                ',
                '',
                1
            );
        ");


        $this->execute("
        
            INSERT INTO `email_template`(
                `id`,
                `code`,
                `group`,
                `language_id`,
                `title`,
                `description`,
                `updated_at`,
                `template_html`,
                `template_text`,
                `is_active`
            ) VALUES (
                NULL ,
                'cnc.userNewMessage',
                'personalMessages',
                'en-US',
                'You have a new message for CNC quote #%preorderId% on Treatstock ',
                'You have a new message for CNC quote #%preorderId% on Treatstock ',
                NOW(),
                '
                    Hi %username%,

                    You\'ve received a new message on Treatstock from %messageUsername%. 
                    <a href=\"%messageLink%\" style=\"display: inline-block;margin:10px 0;border-radius: 0px;font-family: sans-serif;padding: 5px 20px;min-width: 40px;font-size: 16px;font-weight: normal;line-height: 20px;border: none;text-decoration:none !important;color: #fff;background-color:#2d8ee0;\">View message</a>
                    
                    Best,
                    Treatstock
                ',
                '',
                1
            );
        ");

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'preorder.userNewMessage']);
        $this->delete('email_template', ['code' => 'cnc.userNewMessage']);

        return true;
    }
}
