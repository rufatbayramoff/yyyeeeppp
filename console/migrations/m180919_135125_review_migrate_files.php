<?php

use yii\db\Migration;

/**
 * Class m180919_135125_review_migrate_files
 */
class m180919_135125_review_migrate_files extends Migration
{
    /**
     * @return bool|void
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $storeOrderReview = app()->db->createCommand("SELECT * from store_order_review")->queryAll();

        $this->createTable('store_order_review_file', [
            'review_id'=>  $this->integer()->notNull(),
            'file_uuid'=>  $this->string(32)->notNull(),
            'is_main'=> $this->tinyInteger(1)->defaultValue(0)->notNull(),
        ]);

        $this->createIndex('index_sorf_review_id', 'store_order_review_file', 'review_id');
        $this->createIndex('index_sorf_review_id_file', 'store_order_review_file', ['review_id', 'file_uuid'], true);
        $this->createIndex('index_sorf_file_id_main', 'store_order_review_file', ['review_id', 'is_main']);

        $this->addForeignKey('fk_sorf_review_id', 'store_order_review_file', 'review_id', 'store_order_review', 'id');
        $this->addForeignKey('fk_sorf_file_uuid', 'store_order_review_file', 'file_uuid', 'file', 'uuid');

        foreach ($storeOrderReview as $item) {
            $filesId = json_decode($item['file_ids'], true);

            $dataFile = [];

            if (empty($filesId)) {
                continue;
            }

            foreach ($filesId as $id) {

                $uuid = app()->db->createCommand("select uuid from file where id = :id", ['id' => $id])->queryScalar();

                if ($uuid) {
                    $dataFile[] = [$item['id'], $uuid];
                }
            }

            app()->db->createCommand()->batchInsert('store_order_review_file', ['review_id', 'file_uuid'], $dataFile)->execute();
        }

        $this->dropColumn('store_order_review', 'file_ids');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('store_order_review', 'file_ids', $this->text());

        $storeOrderReviewFile = app()->db->createCommand("select
                                   sorf.review_id as review_id,
                                  concat('[', group_concat(f.id), ']') as file_ids
                                from store_order_review_file sorf
                                join file f on sorf.file_uuid = f.uuid
                                group by sorf.review_id")->queryAll();

        foreach ($storeOrderReviewFile as $item) {
            app()->db->createCommand()->update('store_order_review', ['file_ids' => $item['file_ids']], ['id' => $item['review_id']])->execute();
        }

        $this->dropTable('store_order_review_file');
    }

}
