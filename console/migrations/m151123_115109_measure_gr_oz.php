<?php

use yii\db\Schema;
use yii\db\Migration;

class m151123_115109_measure_gr_oz extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `ps_printer_color`
            CHANGE COLUMN `price_measure` `price_measure` ENUM('gr', 'oz') NOT NULL;");

        $this->execute("UPDATE `ps_printer_color` SET price_measure = 'gr';");
    }

    public function safeDown()
    {
        
    }
}
