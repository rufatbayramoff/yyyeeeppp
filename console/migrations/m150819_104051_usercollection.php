<?php

use yii\db\Schema;
use yii\db\Migration;

class m150819_104051_usercollection extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_collection` 
            ADD COLUMN `items_count` INT(11) NOT NULL DEFAULT 0 AFTER `is_visible`,
            ADD COLUMN `cover` VARCHAR(145) NULL AFTER `items_count`");
        return 0;
    }

    public function down()
    {
        $this->execute("ALTER TABLE `user_collection`  DROP COLUMN `cover`, DROP COLUMN `items_count`");
        return 0;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
