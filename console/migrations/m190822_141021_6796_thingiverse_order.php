<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m190822_141021_6796_thingiverse_order
 */
class m190822_141021_6796_thingiverse_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('thingiverse_order', 'api_json', 'JSON null');
        $this->alterColumn('thingiverse_report', 'thing_id', 'varchar(25) null');
    }

    public function safeDown()
    {
    }
}