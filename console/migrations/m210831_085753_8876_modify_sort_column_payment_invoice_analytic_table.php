<?php

use yii\db\Migration;

/**
 * Class m210831_085753_modify_sort_column_payment_invoice_analytic_table
 */
class m210831_085753_8876_modify_sort_column_payment_invoice_analytic_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE payment_invoice_analytic MODIFY COLUMN sort enum('default','low_price','price','rating','distance','default_2021') CHARACTER SET utf8 COLLATE utf8_general_ci NULL";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $sql = "ALTER TABLE payment_invoice_analytic MODIFY COLUMN sort enum('default','low_price','price','rating','distance') CHARACTER SET utf8 COLLATE utf8_general_ci NULL";
        $this->execute($sql);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210831_085753_modify_sort_column_payment_invoice_analytic_table cannot be reverted.\n";

        return false;
    }
    */
}
