<?php

use yii\db\Migration;

class m170112_110508_3630_seo_intl extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `seo_page_intl` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `seo_page_id` INT(11) NULL DEFAULT NULL,
            `lang_iso` CHAR(5) NULL DEFAULT NULL,
            `title` VARCHAR(145) NULL DEFAULT NULL,
            `header` VARCHAR(145) NULL DEFAULT NULL,
            `meta_description` VARCHAR(145) NULL DEFAULT NULL,
            `meta_keywords` VARCHAR(145) NULL DEFAULT NULL,
            `header_text` MEDIUMTEXT NULL,
            `footer_text` MEDIUMTEXT NULL,
            `is_active` BIT(1) NOT NULL DEFAULT b\'1\',
            `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            UNIQUE INDEX `seo_page_id_lang_iso` (`seo_page_id`, `lang_iso`),
            CONSTRAINT `FK_seo_page_intl_seo_page` FOREIGN KEY (`seo_page_id`) REFERENCES `seo_page` (`id`)
        )');
    }

    public function down()
    {
        $this->dropTable('seo_page_intl');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
