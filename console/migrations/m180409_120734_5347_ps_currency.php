<?php

use yii\db\Migration;

/**
 * Class m180409_120734_5347_ps_currency
 */
class m180409_120734_5347_ps_currency extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `ps` 
            ADD COLUMN `currency` VARCHAR(4) NOT NULL DEFAULT 'USD' AFTER `country_id`;
        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('ps', 'currency');
        return true;
    }
}
