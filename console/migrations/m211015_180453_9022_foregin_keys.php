<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m211015_180453_9022_foregin_keys
 */
class m211015_180453_9022_foregin_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute("ALTER TABLE `user_osn` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT '2000-01-01 00:00:00';");
        $this->execute("ALTER TABLE `user_statistics` CHANGE `lastonline_at` `lastonline_at` TIMESTAMP NOT NULL DEFAULT '2000-01-01 00:00:00';");
        $this->update('user_session', ['user_id'=>1], ['id'=>1]);
        $this->dropForeignKey('printer_material_photo_file_id', 'printer_material');
        $this->dropColumn('printer_material', 'photo_file_id');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
