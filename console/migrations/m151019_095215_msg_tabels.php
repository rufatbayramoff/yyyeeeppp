<?php

use yii\db\Schema;
use yii\db\Migration;

class m151019_095215_msg_tabels extends Migration
{
    public function up()
    {
        $this->execute("
        DROP TABLE IF EXISTS `msg_folder`;

CREATE TABLE `msg_folder` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `is_active` tinyint(1) unsigned NOT NULL,
  `sort` tinyint(4) unsigned NOT NULL,
  `alias` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sort` (`sort`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `msg_folder` WRITE;

INSERT INTO `msg_folder` (`id`, `title`, `is_active`, `sort`, `alias`)
VALUES
	(1,'Personal Messages',1,1,'personal'),
	(2,'Support Service',1,2,'support'),
	(3,'Print Jobs',1,3,'print-jobs'),
	(4,'Your orders',1,4,'orders'),
	(5,'Archive',1,5,'archive'),
	(6,'Printed Jobs',1,6,'printed-jobs');

UNLOCK TABLES;


DROP TABLE IF EXISTS `msg_topic`;

CREATE TABLE `msg_topic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `bind_to` enum('order') DEFAULT NULL,
  `bind_id` int(11) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `last_message_time` datetime DEFAULT NULL,
  `last_support_user_id` int(11) DEFAULT NULL,
  `hold_support_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`),
  KEY `last_support_user_id` (`last_support_user_id`),
  KEY `hold_support_user_id` (`hold_support_user_id`),
  CONSTRAINT `msg_topic_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`),
  CONSTRAINT `msg_topic_ibfk_2` FOREIGN KEY (`last_support_user_id`) REFERENCES `user_admin` (`id`),
  CONSTRAINT `msg_topic_ibfk_3` FOREIGN KEY (`hold_support_user_id`) REFERENCES `user_admin` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





DROP TABLE IF EXISTS `msg_member`;

CREATE TABLE `msg_member` (
  `user_id` int(11) NOT NULL,
  `topic_id` int(11) unsigned NOT NULL,
  `folder_id` int(11) unsigned NOT NULL,
  `have_unreaded_messages` tinyint(1) unsigned NOT NULL,
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hide_message_before` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`topic_id`),
  KEY `topic_id` (`topic_id`),
  KEY `folder_id` (`folder_id`),
  CONSTRAINT `msg_member_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `msg_member_ibfk_2` FOREIGN KEY (`topic_id`) REFERENCES `msg_topic` (`id`),
  CONSTRAINT `msg_member_ibfk_3` FOREIGN KEY (`folder_id`) REFERENCES `msg_folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `msg_message`;

CREATE TABLE `msg_message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `text` text NOT NULL,
  `topic_id` int(10) unsigned NOT NULL,
  `support_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `topic_id` (`topic_id`),
  KEY `support_user_id` (`support_user_id`),
  CONSTRAINT `msg_message_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `msg_message_ibfk_2` FOREIGN KEY (`topic_id`) REFERENCES `msg_topic` (`id`),
  CONSTRAINT `msg_message_ibfk_3` FOREIGN KEY (`support_user_id`) REFERENCES `user_admin` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        ");

    }

    public function down()
    {
        echo "m151019_095215_msg_tabels cannot be reverted.\n";

        return false;
    }
}
