<?php

use yii\db\Migration;

class m171215_111707_5102_email_login extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `user_email_login` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `created_at` datetime NOT NULL,
              `visited_at` datetime DEFAULT NULL,
              `expired_at` datetime NOT NULL,
              `hash` char(16) NOT NULL,
              `email` varchar(145) NOT NULL,
              `requested_url` varchar(245) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `index3` (`hash`,`email`),
              KEY `fk_new_table_1_idx` (`user_id`),
              CONSTRAINT `fk_new_table_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
        );
        $this->insert('email_template', [
            'code' => 'user.loginByEmailLink',
            'group' => 'system',
            'language_id' => 'en-US',
            'title' => 'Sign in with e-mail - Treatstock.',
            'description' => 'Sign in with e-mail. params %user%, %linkWithAccess%, %urlWithAccess%", %requestedUrl%',
            'template_html' => nl2br('Hi %user%,
If you had requested to sign in using your email to visit %requestedUrl%, please click <a href="%urlWithAccess%">here</a>.
%passwordSetted%

Best Regards, 
Treatstock'),
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);

        $this->insert('email_template', [
            'code' => 'user.emailThingiverseOrderLink',
            'group' => 'system',
            'language_id' => 'en-US',
            'title' => 'Order placed on Thingiverse - Treatstock.',
            'description' => 'Order placed on Thingiverse. params %user%, %linkWithAccess%, %urlWithAccess%", %requestedUrl%',
            'template_html' => nl2br('Hello,

This email is to confirm that your order has been placed on Treatstock. 

If you would like to view your order details, communicate with the manufacturer or track the process of your order, please click: %linkWithAccess%.

Best Regards, 
Treatstock'),
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);

        $this->insert('email_template', [
            'code' => 'user.emailThingiverseOrderLinkPs',
            'group' => 'system',
            'language_id' => 'en-US',
            'title' => 'Thingiverse Order - Treatstock.',
            'description' => 'PS clicks contact customer for thingiverse order. params %user%, %linkWithAccess%, %urlWithAccess%", %requestedUrl%',
            'template_html' => nl2br('Hello,

You have received a message from the manufacturer for Thingiverse Order.
To view the message and reply to the manufacturer, please click the following link: %linkWithAccess%.

Best Regards, 
Treatstock'),
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);


    }

    public function safeDown()
    {
        $this->dropTable('user_email_login');
        $this->delete('email_template', ['code' => 'user.loginByEmailLink']);
        $this->delete('email_template', ['code' => 'user.emailThingiverseOrderLink']);
    }
}
