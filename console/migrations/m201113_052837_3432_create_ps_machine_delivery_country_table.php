<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ps_machine_delivery_country}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%ps_machine_delivery}}`
 * - `{{%company_service}}`
 * - `{{%geo_country}}`
 */
class m201113_052837_3432_create_ps_machine_delivery_country_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ps_machine_delivery_country}}', [
            'id' => $this->primaryKey(),
            'ps_machine_delivery_id' => $this->integer()->notNull(),
            'geo_country_id' => $this->integer()->notNull(),
            'price' => $this->decimal(7,2),
        ]);

        // creates index for column `ps_machine_delivery_id`
        $this->createIndex(
            '{{%idx-ps_machine_delivery_country-ps_machine_delivery_id}}',
            '{{%ps_machine_delivery_country}}',
            'ps_machine_delivery_id'
        );

        // add foreign key for table `{{%ps_machine_delivery}}`
        $this->addForeignKey(
            '{{%fk-ps_machine_delivery_country-ps_machine_delivery_id}}',
            '{{%ps_machine_delivery_country}}',
            'ps_machine_delivery_id',
            '{{%ps_machine_delivery}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );


        // creates index for column `geo_country_id`
        $this->createIndex(
            '{{%idx-ps_machine_delivery_country-geo_country_id}}',
            '{{%ps_machine_delivery_country}}',
            'geo_country_id'
        );

        // add foreign key for table `{{%geo_country}}`
        $this->addForeignKey(
            '{{%fk-ps_machine_delivery_country-geo_country_id}}',
            '{{%ps_machine_delivery_country}}',
            'geo_country_id',
            '{{%geo_country}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%ps_machine_delivery}}`
        $this->dropForeignKey(
            '{{%fk-ps_machine_delivery_country-ps_machine_delivery_id}}',
            '{{%ps_machine_delivery_country}}'
        );

        // drops index for column `ps_machine_delivery_id`
        $this->dropIndex(
            '{{%idx-ps_machine_delivery_country-ps_machine_delivery_id}}',
            '{{%ps_machine_delivery_country}}'
        );

        // drops foreign key for table `{{%geo_country}}`
        $this->dropForeignKey(
            '{{%fk-ps_machine_delivery_country-geo_country_id}}',
            '{{%ps_machine_delivery_country}}'
        );

        // drops index for column `geo_country_id`
        $this->dropIndex(
            '{{%idx-ps_machine_delivery_country-geo_country_id}}',
            '{{%ps_machine_delivery_country}}'
        );

        $this->dropTable('{{%ps_machine_delivery_country}}');
    }
}
