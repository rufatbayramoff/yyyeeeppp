<?php

use yii\db\Migration;

class m161201_131409_2730_attemp_table extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `store_order_attemp` (
                `id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
                `order_id` Int( 11 ) NOT NULL,
                `printer_id` Int( 11 ) NOT NULL,
                PRIMARY KEY ( `id` ) )
            ENGINE = InnoDB
            AUTO_INCREMENT = 1;
            CREATE INDEX `printer_id` USING BTREE ON `store_order_attemp`( `printer_id` );
            CREATE INDEX `order_id` USING BTREE ON `store_order_attemp`( `order_id` );
              
            INSERT INTO store_order_attemp (SELECT id, id as order_id, printer_id FROM store_order);
            
            ALTER TABLE `store_order` DROP FOREIGN KEY `fk_store_order3`;
            ALTER TABLE `store_order` DROP COLUMN `printer_id`,DROP INDEX `fk_store_order3` ;
        ");


    }

    public function down()
    {
        echo "m161201_131409_2730_attemp_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
