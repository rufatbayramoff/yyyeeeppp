<?php

use yii\db\Schema;
use yii\db\Migration;

class m150622_203702_geo_city extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('{{%geo_city}}', 'title', 'string(80) NOT NULL');
        $this->addColumn('{{%geo_city}}', 'lat', 'DECIMAL(8,5) NOT NULL AFTER `timezone_id` ');
        $this->addColumn('{{%geo_city}}', 'lon', 'DECIMAL(8,5) NOT NULL AFTER `lat` ');
        $this->addColumn('{{%geo_city}}', 'lat_trim', 'DECIMAL(4,1) NOT NULL AFTER `lon` ');
        $this->addColumn('{{%geo_city}}', 'lon_trim', 'DECIMAL(4,1) NOT NULL AFTER `lat_trim` ');
        $this->addColumn('{{%geo_city}}', 'fcode', 'string(10) NOT NULL AFTER `lon_trim` ');
        $this->addColumn('{{%geo_city}}', 'geoname_id', 'integer(11) NOT NULL AFTER `fcode` ');
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%geo_city}}', 'title', 'string(45) NOT NULL');
        $this->dropColumn('{{%geo_city}}', 'lat');
        $this->dropColumn('{{%geo_city}}', 'lon');
        $this->dropColumn('{{%geo_city}}', 'lat_trim');
        $this->dropColumn('{{%geo_city}}', 'lon_trim');
        $this->dropColumn('{{%geo_city}}', 'fcode');
        $this->dropColumn('{{%geo_city}}', 'geoname_id');
    }
}
