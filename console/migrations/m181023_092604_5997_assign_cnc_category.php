<?php

use yii\db\Migration;

/**
 * Class m181023_092604_5997_assign_cnc_category
 */
class m181023_092604_5997_assign_cnc_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $idCategory = app()->db->createCommand("select id from company_service_category where code= 'cnc-manufacturing'")->queryScalar();

        if ($idCategory) {
            $this->execute("UPDATE company_service SET category_id=$idCategory WHERE type='cnc' AND id>0");
        }
        //         $cncCategory = CompanyServiceCategory::findOne(['code'=>ServicesController::CATEGORY_CNC]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181023_092604_5997_assign_cnc_category cannot be reverted.\n";

        return false;
    }
    */
}
