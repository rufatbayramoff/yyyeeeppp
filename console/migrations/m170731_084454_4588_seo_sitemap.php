<?php

use yii\db\Migration;

class m170731_084454_4588_seo_sitemap extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `seo_page` ADD COLUMN `is_sitemap` BIT(1) NOT NULL DEFAULT b\'0\' AFTER `need_review`;');

        $defaultTpls = [
            'location',
            'location-technology',
            'location-material',
            'location-usage',
            'location-technology-usage',
            'location-technology-material',
            'location-material-usage',
            'location-technology-material-usage'
        ];
        $this->update('seo_page_autofill_template', ['is_default' => 1], ['type' => 'pscatalog', 'template_name' => $defaultTpls]);
    }

    public function safeDown()
    {
        $this->dropColumn('seo_page', 'is_sitemap');
    }
}
