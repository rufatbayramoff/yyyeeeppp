<?php

use yii\db\Schema;
use yii\db\Migration;

class m160105_132157_delete_address_from_user_location extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `user_location` DROP FOREIGN KEY `fk_user_location_1`;
            ALTER TABLE `user_location` DROP `address_id`;
        ");
    }

    public function down()
    {
        echo "m160105_132157_delete_address_from_user_location cannot be reverted.\n";
        return true;
    }

}
