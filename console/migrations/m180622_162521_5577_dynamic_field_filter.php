<?php

use yii\db\Migration;

/**
 * Class m180622_162521_5577_dynamic_field_filter
 */
class m180622_162521_5577_dynamic_field_filter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dynamic_field', 'filter_class', 'varchar(255) null after description');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dynamic_field', 'filter_class');
    }
}
