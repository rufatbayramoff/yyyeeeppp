<?php

use yii\db\Migration;
use yii\db\Query;

class m161219_161832_3342_files_alter extends Migration
{

    public function up()
    {
        $this->addColumn('file', 'uuid', 'char(32) null after id');
        $this->createIndex('file-uniq-uuid', 'file', 'uuid', true);
        $allFiles = (new Query())
            ->select('*')
            ->from('file')->all();
        foreach ($allFiles as $file) {
            $uuid = md5(mt_rand(-99990999, 99990999) . microtime(true) . 'sde7');
            $this->update(
                'file',
                [
                    'uuid' => $uuid,
                ],
                'id=' . $file['id']
            );
        }
        $this->alterColumn('file', 'uuid', 'string(32) not null');
        $this->addColumn('file', 'expire', 'datetime null');

    }

    public function down()
    {
        $this->dropColumn('file', 'uuid');
        $this->dropColumn('file', 'expire');
    }
}
