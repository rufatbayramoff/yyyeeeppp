<?php

use yii\db\Migration;

/**
 * Class m180824_091337_add_description_for_client_new_order
 */
class m180824_091337_add_description_for_client_new_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('email_template', [
            'description' => 'Params %orderId%, %clientName%, %htmlOrderList% - order body'
        ], [
            'code' => 'clientNewOrder'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
