<?php

use yii\db\Migration;

class m170310_111321_3569_settings_max_orders extends Migration
{
    public function up()
    {
        $this->insert(
            'system_setting',
            [
                'group_id'        => 13,
                'key'             => 'max_orders_per_printer_count',
                'value'           => 3,
                'created_at'      => date('Y-m-d H:i:s'),
                'updated_at'      => date('Y-m-d H:i:s'),
                'created_user_id' => null,
                'updated_user_id' => null,
                'description'     => 'This is max active orders count to print service. If count is more, printer will not be shown in printers list.'
            ]
        );
    }

    public function down()
    {
        $this->delete('system_setting', ['group']);
    }
}
