<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_110659_payment_dbfixes extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `payment_transaction` CHANGE COLUMN `status` `status` CHAR(25) NOT NULL ;");
        /*$this->execute("ALTER TABLE `ps`  DROP FOREIGN KEY `fk_location_id_2`");
        $this->execute("ALTER TABLE  `ps`  DROP COLUMN `location_id`, DROP INDEX `fk_location_id_2`");*/
    }

    public function down()
    {
        $this->execute("ALTER TABLE `payment_transaction` CHANGE COLUMN `status` `status` CHAR(15) NOT NULL ;");
        /*$this->execute("ALTER TABLE `ps`  ADD COLUMN `location_id` INT(11) NULL AFTER `sms_notify`;");
        $this->execute("ALTER TABLE `ps`  ADD INDEX `fk_location_id_2` (`location_id` ASC); ALTER TABLE `ps` 
            ADD CONSTRAINT `fk_location_id_2` FOREIGN KEY (`location_id`)
              REFERENCES `geo_location` (`id`) ON DELETE NO ACTION  ON UPDATE NO ACTION;
        ");*/

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
