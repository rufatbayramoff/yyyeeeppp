<?php

use yii\db\Migration;

class m170829_135114_4724_alter_city extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `geo_city` ADD COLUMN `google_place_id` VARCHAR(45) NULL AFTER `geoname_id`;');
    }

    public function safeDown()
    {
        $this->dropColumn('google_place_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170829_135114_4724_alter_city cannot be reverted.\n";

        return false;
    }
    */
}
