<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_083952_filedb extends Migration
{
   
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("ALTER TABLE `file` ADD COLUMN `md5sum` CHAR(32) NOT NULL DEFAULT '' AFTER `status`;");
        $this->execute("ALTER TABLE `file` ADD COLUMN `last_access_at` TIMESTAMP NULL AFTER `md5sum`;");
        $this->execute("ALTER TABLE `file` CHANGE COLUMN `server` `server` CHAR(15) NOT NULL DEFAULT 'localhost';");
    }

    public function safeDown()
    {
        $this->dropColumn('file', 'md5sum');
        $this->dropColumn('file', 'last_access_at');
    }
    
}
