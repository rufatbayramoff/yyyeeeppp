<?php

use yii\db\Schema;
use yii\db\Migration;

class m150821_132325_storeCats extends Migration
{
    public function safeUp()
    {
        $this->execute("CREATE TABLE `store_category_intl` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `category_id` int(11) NOT NULL DEFAULT '0',
                `title` varchar(245) NOT NULL,
                `description` varchar(245) DEFAULT NULL,
                `lang_iso` char(5) NOT NULL DEFAULT 'en-US',
                PRIMARY KEY (`id`),
                UNIQUE KEY `lang_iso_UNIQUE` (`lang_iso`,`category_id`),
                KEY `fk_store_category_intl_1_idx` (`category_id`),
                CONSTRAINT `fk_store_category_intl_1` FOREIGN KEY (`category_id`) REFERENCES `store_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
              ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
        ");

        $this->execute("ALTER TABLE `store_category` ADD COLUMN `img` VARCHAR(245) NULL AFTER `parent_id`;");
        return 0;
    }

    public function safeDown()
    {
        $this->dropTable('store_category_intl');
        $this->dropColumn('store_category', 'img');
        return 0;
    } 
}
