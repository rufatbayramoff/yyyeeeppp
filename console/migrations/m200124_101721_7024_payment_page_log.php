<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m200124_101721_7024_payment_page_log
 */
class m200124_101721_7024_payment_page_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(<<<SQL
CREATE TABLE `payment_pay_page_log` (
  `uuid` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `ip` char(15) NOT NULL,
  `user_agent` varchar(2048) NOT NULL,
  `invoice_uuid` char(6) NOT NULL,
  `status` enum('press_button','processed','done') NOT NULL,
  `process_err` json NOT NULL,
  `process_payload` json NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `invoice_uuid` (`invoice_uuid`),
  CONSTRAINT `pay_log_invoice_uuid` FOREIGN KEY (`invoice_uuid`) REFERENCES `payment_invoice` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
);
    }

    public function safeDown()
    {
        $this->dropTable('payment_pay_page_log');
    }
}