<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_093339_remoterender extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `model3d_file_filament` 
            CHANGE COLUMN `length` `length` DOUBLE(10,4) NOT NULL DEFAULT 0 ,
            ADD COLUMN `area` DOUBLE(10,4) NOT NULL DEFAULT 0 AFTER `volume`;
        ");
        
        $this->execute("ALTER TABLE `printer_color` 
            CHANGE COLUMN `rgb` `rgb` VARCHAR(45) NOT NULL ,
            CHANGE COLUMN `render_color` `render_color` VARCHAR(45) NOT NULL ,
            ADD COLUMN `ambient` INT(5) NULL DEFAULT 40 AFTER `render_color`;
        ");
    }

    public function down()
    {
        $this->dropColumn('model3d_file_filament', 'area');
        $this->dropColumn('printer_color', 'ambient');
        
        $this->execute("ALTER TABLE `printer_color` 
            CHANGE COLUMN `rgb` `rgb` VARCHAR(45) NULL DEFAULT NULL ,
            CHANGE COLUMN `render_color` `render_color` VARCHAR(45) NULL DEFAULT NULL ;
        ");
    }
 
}
