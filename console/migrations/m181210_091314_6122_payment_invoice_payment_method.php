<?php

use yii\db\Migration;

/**
 * Class m181210_091314_6122_payment_invoice_payment_method
 */
class m181210_091314_6122_payment_invoice_payment_method extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payment_invoice_payment_method', [
            'payment_invoice_uuid' => $this->string(6)->notNull(),
            'vendor' => "enum('braintree', 'ts', 'thingiverse', 'invoice') not null"
        ]);

        $this->createIndex('idx_pipm_payment_invoice_uuid_vendor', 'payment_invoice_payment_method', ['payment_invoice_uuid', 'vendor'], true);
        $this->createIndex('idx_pipm_payment_invoice_uuid', 'payment_invoice_payment_method', ['payment_invoice_uuid']);

        $this->addForeignKey('fk_pipm_payment_invoice', 'payment_invoice_payment_method', 'payment_invoice_uuid', 'payment_invoice', 'uuid');

        $paidPaymentInvoices = app()->db->createCommand("
        select
            pi.uuid,
            pt.vendor
        from payment_invoice pi
            join payment p on pi.uuid = p.payment_invoice_uuid
            join payment_detail_operation pdo on p.id = pdo.payment_id
            join payment_detail pd on pdo.uuid = pd.payment_detail_operation_uuid
            join payment_transaction pt on pd.id = pt.first_payment_detail_id
        where pi.status = 'paid'")->queryAll();

        foreach ($paidPaymentInvoices as $invoice) {
            $isVendoe = (new \yii\db\Query())->select(['*'])->from('payment_invoice_payment_method')->where([
                'payment_invoice_uuid' => $invoice['uuid'],
                'vendor' => $invoice['vendor'],
            ])->scalar();

            if (!$isVendoe) {
                $this->insert('payment_invoice_payment_method', [
                    'payment_invoice_uuid' => $invoice['uuid'],
                    'vendor' => $invoice['vendor']
                ]);
            }
        }

        $otherPaymentInvoices = app()->db->createCommand("
        select
            pi.uuid
        from payment_invoice pi
        where pi.status != 'paid'")->queryAll();

        echo "\n----------------\n";

        foreach ($otherPaymentInvoices as $invoice) {

            foreach (['braintree', 'ts', 'thingiverse', 'invoice'] as $type) {
                $isVendoe = (new \yii\db\Query())->select(['*'])->from('payment_invoice_payment_method')->where([
                    'payment_invoice_uuid' => $invoice['uuid'],
                    'vendor' => $type,
                ])->scalar();

                if (!$isVendoe) {
                    $this->insert('payment_invoice_payment_method', [
                        'payment_invoice_uuid' => $invoice['uuid'],
                        'vendor' => $type
                    ]);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment_invoice_payment_method');
    }
}
