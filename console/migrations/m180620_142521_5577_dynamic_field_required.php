<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180620_142521_5577_dynamic_field_required
 */
class m180620_142521_5577_dynamic_field_required extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('dynamic_field','is_required', 'tinyint(1) not null after type');
        $this->alterColumn('dynamic_field_category', 'is_required', 'tinyint(1) null');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('dynamic_field','is_required');
    }
}
