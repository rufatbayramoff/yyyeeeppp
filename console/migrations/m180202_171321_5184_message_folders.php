<?php

use yii\db\Migration;

class m180202_171321_5184_message_folders extends Migration
{
    public function safeUp()
    {
        $this->update('msg_folder', ['title'=>'Service orders', 'alias'=>'service-orders'], 'id=3');
        $this->update('msg_folder', ['title'=>'Completed orders', 'alias'=>'completed-orders'], 'id=6');
    }

    public function safeDown()
    {
    }
}
