<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180522_101422_5579_product_category_link
 */
class m180522_101422_5579_product_category_link extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $categories = (new Query())->select('*')->from('store_category')->where('parent_id!=1 or (parent_id  is null) and id!=1')->orderBy('id asc')->all();
        $deleteIds = [];
        foreach ($categories as $category) {
            unset($category['custom_code_id']);
            unset($category['img']);
            $category['code'] = strtolower($category['title']);
            $category['code'] = str_replace('&', '', $category['code']);
            $category['code'] = str_replace(' ', '-', $category['code']);
            $this->insert('product_category', $category);
            $categoryIntls = (new Query())->select('*')->from('store_category_intl')->where('model_id=' . $category['id'])->all();
            foreach ($categoryIntls as $categoryIntl) {
                $this->insert('product_category_intl', $categoryIntl);
            }
            $deleteIds[] = $category['id'];
        }

        $this->execute("ALTER TABLE `product` DROP FOREIGN KEY `fk_product_category_id`; ALTER TABLE `product` ADD CONSTRAINT `fk_product_category_id` FOREIGN KEY (`category_id`) REFERENCES `product_category`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        rsort($deleteIds);
        foreach ($deleteIds as $deleteId) {
            $this->delete('store_category', 'id=' . $deleteId);
        }
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return true;
    }
}
