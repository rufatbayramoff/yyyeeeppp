<?php

use yii\db\Migration;

class m160610_113621_2084_alter_system_lang_source extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `system_lang_source` ADD `not_found_in_source` TINYINT NOT NULL DEFAULT '0' AFTER `message`");
    }

    public function down()
    {
        $this->dropColumn('system_lang_source', 'not_found_in_source');
    }
}
