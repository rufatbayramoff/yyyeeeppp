<?php

use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m220128_172553_9161_wiki_matrial_photo
 */
class m220128_172553_9161_wiki_matrial_photo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute("ALTER TABLE `wiki_material_photo_intl` DROP FOREIGN KEY `fk_wiki_material_photo_intl_lang_iso`; ALTER TABLE `wiki_material_photo_intl` ADD CONSTRAINT `fk_wiki_material_photo_intl_lang_iso` FOREIGN KEY (`lang_iso`) REFERENCES `system_lang`(`iso_code`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `wiki_material_photo_intl` DROP FOREIGN KEY `fk_wiki_material_photo_intl_model_id`; ALTER TABLE `wiki_material_photo_intl` ADD CONSTRAINT `fk_wiki_material_photo_intl_model_id` FOREIGN KEY (`model_id`) REFERENCES `wiki_material_photo`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
