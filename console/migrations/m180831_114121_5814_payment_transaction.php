<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180831_114121_5814_payment_transaction
 *
 * Set at least one transaction history record
 */
class m180831_114121_5814_payment_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $paymentTransactions = $this->db->createCommand('select payment_transaction.*  from payment_transaction LEFT JOIN payment_transaction_history ON payment_transaction_history.transaction_id = payment_transaction.id WHERE payment_transaction_history.id is null')->queryAll();
        echo "\nTransactions without history count: " . count($paymentTransactions)."\n";

        foreach ($paymentTransactions as $paymentTransaction) {
            $paymentTransactionHistoryElement = [
                'transaction_id' => $paymentTransaction['id'],
                'created_at' => $paymentTransaction['created_at'],
                'action_id'  => 'create',
                'comment' => [],
                'user_id'  => $paymentTransaction['user_id']
            ];

            $this->insert('payment_transaction_history', $paymentTransactionHistoryElement);
        }

        $this->insert('user', [
            'id'=>'200',
            'uid' => '9VUIG1',
            'username' => 'pay-braintree-authorized',
            'email' => 'pay-braintree-autorized@treatstock.com',
            'auth_key' => '-',
            'password_hash' => '-',
            'created_at' => \common\components\DateHelper::now(),
            'updated_at' => \common\components\DateHelper::now(),
            'lastlogin_at' => '00-00-00',
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
