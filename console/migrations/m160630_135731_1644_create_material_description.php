<?php

use yii\db\Migration;

class m160630_135731_1644_create_material_description extends Migration
{

    public function safeUp()
    {
        $this->addColumn('printer_material', 'photo_file_id', 'integer NULL');
        $this->addColumn('printer_material', 'short_description', 'string(250) NULL');
        $this->addColumn('printer_material', 'long_description', 'string(5000) NULL');
        $this->createIndex('printer_material_photo_file_id','printer_material','photo_file_id');
        $this->addForeignKey('printer_material_photo_file_id', 'printer_material', 'photo_file_id', 'file', 'id');

    }

    public function safeDown()
    {
        $this->dropForeignKey('printer_material_photo_file_id', 'printer_material');
        $this->dropColumn('printer_material', 'short_description');
        $this->dropColumn('printer_material', 'long_description');
        $this->dropColumn('printer_material', 'photo_file_id');
    }
}
