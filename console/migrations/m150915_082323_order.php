<?php

use yii\db\Schema;
use yii\db\Migration;

class m150915_082323_order extends Migration
{
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("ALTER TABLE `store_order` 
            ADD COLUMN `delivery_type_id` INT(11) NULL AFTER `printer_id`;");
        
        $this->execute("ALTER TABLE `store_order` 
            ADD INDEX `fk_store_order_3_idx` (`delivery_type_id` ASC);
            ALTER TABLE `store_order` 
            ADD CONSTRAINT `fk_store_order_3`
              FOREIGN KEY (`delivery_type_id`)
              REFERENCES `delivery_type` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
            ");
        
        $this->execute("ALTER TABLE `store_pricer_type`  ADD COLUMN `title` VARCHAR(45) NULL AFTER `operation`;");
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_store_order_3', 'store_order');
        $this->dropColumn('store_order', 'delivery_type_id');
        $this->dropColumn('store_order', 'store_pricer_type');
    }
    
}
