<?php

use yii\db\Migration;

/**
 * Class m180424_073311_5489_share_improve
 */
class m180424_073311_5489_share_improve extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `store_order_review_share`
            CHANGE COLUMN `social_type` `social_type` ENUM('link', 'google', 'facebook', 'facebook_im', 'reddit', 'linkedin') NOT NULL ;
        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `store_order_review_share`
            CHANGE COLUMN `social_type` `social_type` ENUM('link', 'google', 'facebook', 'facebook_im', 'reddit') NOT NULL ;
        ");
        return true;
    }

}
