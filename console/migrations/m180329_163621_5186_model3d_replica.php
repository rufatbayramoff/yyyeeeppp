<?php

use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m180329_163621_5186_model3d_replica
 */
class m180329_163621_5186_model3d_replica extends Migration
{
    public function formNewModel3dStatus($model3d, $storeUnit)
    {
        $source = $model3d['source'];
        $model3dStatus = $model3d['status'];
        $unitStatus = $storeUnit['status'];
        $isHidden = $storeUnit['is_hidden_in_store'];

        if ($isHidden) {
            if ($source === 'api') {
                return 'private';
            } else {
                return 'published_directly';
            }
        }

        if ($model3dStatus === 'inactive') {
            return 'draft';
        }

        if ($unitStatus === 'published') {
            return 'published_public';
        }

        if ($unitStatus === 'unpublished') {
            return 'draft';
        }

        if ($unitStatus === 'rejected') {
            return 'rejected';
        }

        if ($model3d['is_published']) {
            return 'published_updated';
        }

        return 'publish_pending';
    }

    public $batchUpdates = [];

    /**
     * @param $table string     sql table name, example: users
     * @param $set string       sql set value command, example: name="rqw"
     * @param $id int           id line to update
     */
    public function batchUpdate($table, $set, $id)
    {
        $this->batchUpdates[$table][$set][$id] = 1;
    }

    public function batchUpdateRun()
    {
        $this->maxSqlOutputLength = 1000;
        foreach ($this->batchUpdates as $table => $set) {
            foreach ($set as $setValue => $idList) {
                $idsChunks =array_chunk($idList, 1000, true);
                foreach ($idsChunks as $idsChunk) {
                    $ids = implode(',', array_keys($idsChunk));
                    $sql = 'UPDATE ' . $table . ' SET ' . $setValue . ' WHERE id in(' . $ids . ')';
                    $this->execute($sql);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $storeUnits = (new Query())->select('*')->from('store_unit')->all();
        $storeUnits = ArrayHelper::index($storeUnits, 'model3d_id');
        $storeUnitCategories = (new Query())->select('*')->from('store_units2category')->all();
        $storeUnitCategories = ArrayHelper::index($storeUnitCategories, 'unit_id');
        $models = (new Query())->select('*')->from('model3d')->all();
        $models = ArrayHelper::index($models, 'id');

        foreach ($models as $model3d) {
            if (array_key_exists($model3d['id'], $storeUnits)) {
                $storeUnit = $storeUnits[$model3d['id']];
                $newModel3dStatus = $this->formNewModel3dStatus($model3d, $storeUnit);
                $this->batchUpdate('model3d', "product_status='" . $newModel3dStatus . "'", $model3d['id']);
                if (array_key_exists($storeUnit['id'], $storeUnitCategories)) {
                    $unitCategoryId = $storeUnitCategories[$storeUnit['id']]['category_id'];
                    $this->batchUpdate('model3d', 'category_id='.$unitCategoryId, $model3d['id']);
                }
            }
            if ($model3d['status'] === 'inactive') {
                $this->batchUpdate('model3d', 'is_active=0', $model3d['id']);
            }
        }

        unset($models);
        $modelsReplica = (new Query())->select('*')->from('model3d_replica')->all();
        $modelsReplica = ArrayHelper::index($modelsReplica, 'id');

        foreach ($modelsReplica as $model3d) {
            if ($model3d['status'] === 'inactive') {
                $this->batchUpdate('model3d_replica', 'is_active=0', $model3d['id']);
            }
        }
        $this->batchUpdateRun();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

    }
}
