<?php

use yii\db\Migration;

class m161124_144321_2945_create_store_unit_shopping_candidate extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL
CREATE TABLE `store_unit_shopping_candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_unit_id` int(11) NOT NULL,
  `type` enum('api','utm','print_here') NOT NULL,
  `user_session_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `view_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_session_id` (`user_session_id`),
  KEY `create_date` (`create_date`),
  KEY `store_unit_id` (`store_unit_id`),
  CONSTRAINT `fk_shopping_candidate_store_unit_id` FOREIGN KEY (`store_unit_id`) REFERENCES `store_unit` (`id`),
  CONSTRAINT `fk_shopping_candidate_user_session` FOREIGN KEY (`user_session_id`) REFERENCES `user_session` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
    );
    }

    public function down()
    {
        $this->dropTable('store_unit_shopping_candidate');
    }
}
