<?php

use yii\db\Migration;

class m161114_114151_3233_model3d_cae extends Migration
{
    /**
     *
     */
    public function up()
    {
        $this->execute("ALTER TABLE `model3d`  ADD COLUMN `cae` CHAR(10) NOT NULL DEFAULT 'CAD' AFTER `model3d_texture_id`;");
        $this->execute('ALTER TABLE `api_external_system` ADD COLUMN `json_config` VARCHAR(255) NULL AFTER `binded_user_id`;');
    }

    public function down()
    {
        $this->dropColumn('model3d', 'cae');
        $this->dropColumn('api_external_system', 'json_config');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
