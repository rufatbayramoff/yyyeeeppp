<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_101928_diax_token_progress extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer_3diax_token` CHANGE `progress` `progress` SMALLINT(5)  NULL  DEFAULT NULL;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `ps_printer_3diax_token` CHANGE `progress` `progress` VARCHAR(20)  NULL  DEFAULT NULL;");
        return true;
    }
}
