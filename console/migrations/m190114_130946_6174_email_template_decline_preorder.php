<?php

use yii\db\Migration;

/**
 * Class m190114_130946_6174_email_template_decline_preorder
 */
class m190114_130946_6174_email_template_decline_preorder extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $templatesPs = (new \yii\db\Query())->select(['*'])->from('email_template')->where(['code' => 'customer.preorder.psDeclinePreorder'])->all();

        foreach ($templatesPs as $templatePs) {
            $template_html = str_replace('#%quoteId%', '<a target="_blank" href="%quoteViewCustomerUrl%">#%quoteId%</a>', $templatePs['template_html']);

            $this->update('email_template', [
                'description' => 'psName, userName, quoteId, reason, comment, quoteViewCustomerUrl',
                'template_html' => $template_html
            ], [
                'id' => $templatePs['id']
            ]);
        }

        $templatesCustomer = (new \yii\db\Query())->select(['*'])->from('email_template')->where(['code' => 'ps.preorder.customerDeclinePreorder'])->all();

        foreach ($templatesCustomer as $item) {

            $template_html = str_replace('#%quoteId%', '<a target="_blank" href="%quoteViewPsUrl%">#%quoteId%</a>', $item['template_html']);

            $this->update('email_template', [
                'description' => 'psName, quoteId, quoteViewPsUrl',
                'template_html' => $template_html
            ], [
                'id' => $item['id']
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
