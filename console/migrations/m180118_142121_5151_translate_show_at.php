<?php

use yii\db\Migration;

class m180118_142121_5151_translate_show_at extends Migration
{
    public function safeUp()
    {
        $this->execute(
            '  
CREATE TABLE `system_lang_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_lang_source_id` int(11) NOT NULL,
  `url` varchar(4000) NOT NULL,
  `referer` varchar(4000) NULL,
  `created_at` datetime NOT NULL,
  `lang_iso` CHAR(7) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `isPost` tinyint(1) NOT NULL,
  `isJs` tinyint(1) NOT NULL,
  `getParams` JSON NULL,  
  `postParams` JSON NULL,
  PRIMARY KEY (`id`),
  KEY `system_lang_source_id` (`system_lang_source_id`),
  KEY `user_id_indx` (`user_id`),
  CONSTRAINT `system_lang_fk` FOREIGN KEY (`system_lang_source_id`) REFERENCES `system_lang_source` (`id`),
  CONSTRAINT `system_lang_page_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `system_lang_lang_iso_fk` FOREIGN KEY (`lang_iso`) REFERENCES `system_lang` (`iso_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'
        );

    }

    public function safeDown()
    {
        $this->dropTable('system_lang_page');
    }
}
