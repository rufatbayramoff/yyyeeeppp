<?php

use yii\db\Schema;
use yii\db\Migration;

class m151201_143849_order_printed extends Migration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO `email_template`
            (`code`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`)
            VALUES ('psOrderPrinted','en-US','Model printed','this email sent after PS marked order as printed',NULL,'<p>Hello %username%,</p>\r\n<p>\r\nThe model of the order № %order_id% printed by %ps_title%\r\n</p>\r\n<p>\r\nThank you,\r\nTS Team</p>','Hello %username%,\r\n\r\nThe model of the order № %order_id% printed by %ps_title%\r\n\r\nThank you,\r\nTS Team');");

        $this->execute("INSERT INTO `email_template`
            (`code`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`)
            VALUES ('psOrderPrintedPickUp','en-US','Model printed','this email sent after PS marked order as printed and delivery type set as Pickup','2015-12-01 15:26:47','<p>Hello %username%,</p>\r\n<p>\r\nThe model of the order № %order_id% printed by %ps_title%\r\n</p>\r\n<p>\r\nYou can obtain the printed model here: %printer_address%. The working hours: %working_hours%\r\n</p>\r\n<p>\r\nThank you,\r\nTS Team</p>','Hello %username%,\r\n\r\nThe model of the order № %order_id% printed by %ps_title%\r\nYou can obtain the printed model here: %printer_address%. The working hours: %working_hours%\r\n\r\nThank you,\r\nTS Team');");
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM `email_template` WHERE `code` = 'psOrderPrinted';");
        $this->execute("DELETE FROM `email_template` WHERE `code` = 'psOrderPrintedPickUp';");
    }
}
