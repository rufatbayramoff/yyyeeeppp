<?php

use yii\db\Migration;
use yii\helpers\Json;

class m170830_153432_4070_model3d_cnc_params extends Migration
{
    public function safeUp()
    {
        $this->addColumn('model3d_part', 'model3d_part_cnc_params_id', 'INT(11)');
        $this->addColumn('model3d_replica_part', 'model3d_part_cnc_params_id', 'INT(11)');
        $this->dropForeignKey('fk_cnc_params', 'model3d_part_cnc_params');
        $this->dropColumn('model3d_part_cnc_params', 'model3d_part_id');
        $this->execute('ALTER TABLE `model3d_part_cnc_params` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`)');


        $this->createIndex('model3d_part_cnc_params_idx', 'model3d_part', 'model3d_part_cnc_params_id', true);
        $this->createIndex('model3d_replica_part_cnc_params_id', 'model3d_part', 'model3d_part_cnc_params_id', true);


        $this->addForeignKey('model3d_part_cnc_params_id_fk', 'model3d_part', 'model3d_part_cnc_params_id', 'model3d_part_cnc_params', 'id');
        $this->addForeignKey('model3d_replica_part_cnc_params_id_fk', 'model3d_replica_part', 'model3d_part_cnc_params_id', 'model3d_part_cnc_params', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}
