<?php

use yii\db\Migration;

class m170504_100846_4136_test_order_alert extends Migration
{
    public function up()
    {
        $this->addColumn('ps', 'is_test_order_offer_showed', 'TINYINT(1) NOT NULL DEFAULT 0');
        if(class_exists('\common\models\base\User')) {
            $testUserId = (int)app('setting')->get('printer.testorder.userid', -1);
            $this->execute(
                "
                UPDATE ps 
                LEFT JOIN ps_printer ON ps.id = ps_printer.ps_id
                LEFT JOIN store_order_attemp ON ps_printer.id = store_order_attemp.printer_id
                LEFT JOIN store_order ON store_order_attemp.order_id = store_order.id
                SET is_test_order_offer_showed = 1
                WHERE store_order.user_id = {$testUserId};
            "
            );
        }
    }

    public function down()
    {
        $this->dropColumn('ps', 'is_test_order_offer_showed');
        return true;
    }
}
