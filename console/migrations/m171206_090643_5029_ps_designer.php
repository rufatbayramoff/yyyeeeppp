<?php

use yii\db\Migration;

class m171206_090643_5029_ps_designer extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `ps` 
            ADD COLUMN `is_designer` TINYINT(1) NOT NULL DEFAULT 0;
        ");

/*        $this->execute("
            ALTER TABLE `ps` 
            CHANGE COLUMN `phone_code` `phone_code` VARCHAR(10) NULL ,
            CHANGE COLUMN `phone` `phone` VARCHAR(45) NULL ,
            CHANGE COLUMN `phone_status` `phone_status` ENUM('new', 'checking', 'checked', 'failed') NULL DEFAULT NULL ;
        ");*/

 /*       $this->execute("

            INSERT INTO ps(
                user_id,
                title,
                description,
                is_active,
                moderator_status,
                created_at,
                picture_file_ids,
                url,
                is_designer)
            
            SELECT
              `user`.id,
              user.username,
              user_profile.info_about,
              1,
              'checked',
              NOW(),
              '[]',
              LOWER(user.username),
              1
            FROM
              store_unit,
              model3d,
              `user`,
              user_profile
            WHERE
            
              store_unit.status = 'published'
              AND store_unit.is_active = 1
              AND store_unit.is_hidden_in_store = 0
              AND model3d.is_published = 1
              AND model3d.is_hidden_in_store = 0
            
              AND model3d.id = store_unit.model3d_id
              AND `user`.id = model3d.user_id
              AND user_profile.user_id = `user`.id
              AND ((SELECT id FROM ps WHERE ps.user_id = user.id) IS NULL)
            
            GROUP BY `user`.id
        ");*/

/*        $this->execute("
        UPDATE ps SET is_designer = 1 WHERE ps.id IN (SELECT DISTINCT 
              ps.id
            FROM
              store_unit,
              model3d,
              `user`
            WHERE
              store_unit.status = 'published'
              AND store_unit.is_active = 1
              AND store_unit.is_hidden_in_store = 0
              AND model3d.is_published = 1
              AND model3d.is_hidden_in_store = 0
              AND model3d.id = store_unit.model3d_id
              AND `user`.id = model3d.user_id
              AND `user`.id = ps.user_id)
        ");*/
    }

    public function safeDown()
    {
        $this->dropColumn('ps', 'is_designer');

/*        $this->execute("
            ALTER TABLE `ps`
            CHANGE COLUMN `phone_code` `phone_code` VARCHAR(10) NOT NULL ,
            CHANGE COLUMN `phone` `phone` VARCHAR(45) NOT NULL ,
            CHANGE COLUMN `phone_status` `phone_status` ENUM('new', 'checking', 'checked', 'failed') NOT NULL DEFAULT 'new' ;
        ");*/

     //   $this->delete('ps', ['is_designer' => 1]);

        return true;
    }
}
