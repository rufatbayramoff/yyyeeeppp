<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m181002_160021_5814_manual_balance_fixes
 *
 * Set at least one transaction history record
 */
class m181002_160021_5814_manual_balance_fixes extends Migration
{
    /**
     * @return string
     * @throws Exception
     */
    protected function generatePaymentDetailOperationUuid()
    {
        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_detail_operation')->where('uuid="' . $operationUuid . '"')->one();
        } while ($checkUuid);
        return $operationUuid;
    }

    public function makePaymentOperation($paymentId, $userFromId, $userToId, $summ)
    {
        // Check  payment Exists. May be test database;
        $payment = (new Query())->select('*')->from('payment')->where('id=' . $paymentId)->one();
        if (!$payment) {
            return;
        }
        $paymentDetailOperationUuid = $this->generatePaymentDetailOperationUuid();
        $paymentDetailOperation = [
            'uuid'       => $paymentDetailOperationUuid,
            'payment_id' => $paymentId,
            'created_at' => \common\components\DateHelper::now()
        ];
        $this->insert('payment_detail_operation', $paymentDetailOperation);
        $paymentDetail = [
            'user_id'                       => $userFromId,
            'payment_detail_operation_uuid' => $paymentDetailOperationUuid,
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$summ,
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Manual fix transfer',
            'type'                          => 'order'
        ];

        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => $userToId,
            'payment_detail_operation_uuid' => $paymentDetailOperationUuid,
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $summ,
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Manual fix transfer',
            'type'                          => 'order'
        ];
        $this->insert('payment_detail', $paymentDetail);
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->makePaymentOperation(4267, 4436, 110, 186.97); // Fix: Not exists from user to treatstock transfer
        $this->makePaymentOperation(4267, 4436, 110, 297.79); // Fix: Not exists from user to treatstock transfer
        $this->makePaymentOperation(4909, 110, 11321, 14.66);   // Fix: Payment directly form user without braintree, emulate braintree
        $this->makePaymentOperation(5245, 110, 11560, 7.51);    // Fix: double payments, Order has been accepted #6313
        $this->makePaymentOperation(4891, 12867, 110, 154.13);  // Fix: payment form user to treatstock
        $this->makePaymentOperation(5632, 160, 13056, 3.51);   // Fix: Additional services payment
        $this->makePaymentOperation(5275, 13540, 110, 90.01);  // Payment from braintree to user, without treatstock
        $this->makePaymentOperation(5481, 110, 13927, 25.51);  // Fix: order fuckup sum, attache  correct to some of payment
        $this->makePaymentOperation(8807, 110, 19928, 11.5);  // Fix: orders summ
        $this->makePaymentOperation(13823, 110, 20969, 5.75); // Fix: orders summ
        $this->makePaymentOperation(10568, 110, 25303, 34.57);// Manual fix thingiverse
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
