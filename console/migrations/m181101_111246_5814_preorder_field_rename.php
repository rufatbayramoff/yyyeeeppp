<?php

use yii\db\Migration;

/**
 * Class m181101_111246_5814_preorder_field_rename
 */
class m181101_111246_5814_preorder_field_rename extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('preorder', 'current_payment_invoice_uuid', 'primary_payment_invoice_uuid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('preorder', 'primary_payment_invoice_uuid', 'current_payment_invoice_uuid');
    }
}
