<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200611_102921_7481_affiliate_referrer
 */
class m200611_102921_7481_affiliate_referrer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
CREATE TABLE `affiliate_source_referrer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_source_uuid` char(7) NOT NULL,
  `referrer` varchar(1024) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliate_source_uuid` (`affiliate_source_uuid`),
  CONSTRAINT `affiliate_source_referrer_uuid` FOREIGN KEY (`affiliate_source_uuid`) REFERENCES `affiliate_source` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropTable('affiliate_source_referrer');
    }
}