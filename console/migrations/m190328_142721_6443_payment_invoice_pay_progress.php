<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190328_142721_6443_payment_invoice_pay_progress
 */
class m190328_142721_6443_payment_invoice_pay_progress extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_invoice', 'start_pay_progress_date', 'datetime');
    }

    public function safeDown()
    {
        $this->dropColumn('payment_invoice', 'start_pay_progress_date');
    }
}