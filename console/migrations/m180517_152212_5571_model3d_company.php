<?php

use yii\db\Migration;

/**
 * Class m180517_152212_5571_model3d_company
 */
class m180517_152212_5571_model3d_company extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('model3d', 'company_id', 'int(11) null after user_id');
        $this->createIndex('model3d_company_id', 'model3d', 'company_id');
        $this->addForeignKey('fk_model3d_company_id', 'model3d', 'company_id', 'ps', 'id', 'RESTRICT', 'RESTRICT');

        $this->addColumn('product', 'company_id', 'int(11) null after user_id');
        $this->createIndex('product_company_id', 'product', 'company_id');
        $this->addForeignKey('fk_product_company_id', 'product', 'company_id', 'ps', 'id', 'RESTRICT', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return true;
    }
}
