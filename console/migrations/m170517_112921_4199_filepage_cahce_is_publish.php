<?php

use yii\db\Migration;

class m170517_112921_4199_filepage_cahce_is_publish extends Migration
{
    public function up()
    {
        $this->renameColumn('filepage_cache', 'is_published', 'only_certificated');
        $this->renameColumn('filepage_cache_color', 'is_published', 'only_certificated');
    }

    public function down()
    {
        $this->renameColumn('filepage_cache', 'only_certificated', 'is_published');
        $this->renameColumn('filepage_cache_color', 'only_certificated', 'is_published');
    }
}
