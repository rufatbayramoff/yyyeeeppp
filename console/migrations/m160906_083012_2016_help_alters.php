<?php

use yii\db\Migration;

class m160906_083012_2016_help_alters extends Migration
{
    public function up()
    {
        $this->execute(' ALTER TABLE `site_help` ADD COLUMN `is_popular` BIT NOT NULL DEFAULT 0  AFTER `priority`;');

        $this->execute('CREATE TABLE `site_help_related` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `help_id` int(11) NOT NULL,
          `related_help_id` int(11) NOT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `index4` (`help_id`,`related_help_id`),
          KEY `fk_site_help_related_1_idx` (`help_id`),
          KEY `fk_site_help_related_2_idx` (`related_help_id`),
          CONSTRAINT `fk_site_help_related_1` FOREIGN KEY (`help_id`) REFERENCES `site_help` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
          CONSTRAINT `fk_site_help_related_2` FOREIGN KEY (`related_help_id`) REFERENCES `site_help` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute('ALTER TABLE `site_help` ADD COLUMN `views` INT(11) NOT NULL DEFAULT 0 AFTER `is_popular`;');
    }

    public function down()
    {
        $this->dropColumn('site_help', 'is_popular');
        $this->truncateTable('site_help_related');
        $this->dropTable('site_help_related');

        $this->dropColumn('site_help', 'views');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
