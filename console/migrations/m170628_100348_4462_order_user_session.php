<?php

use yii\db\Migration;

class m170628_100348_4462_order_user_session extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `store_order` 
            ADD COLUMN `user_session_id` INT NULL AFTER `comment`,
            ADD INDEX `fk_store_order_7_idx` (`user_session_id` ASC);
            ALTER TABLE `store_order` 
            ADD CONSTRAINT `fk_store_order_7`
              FOREIGN KEY (`user_session_id`)
              REFERENCES `user_session` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
    }

    public function down()
    {
        $this->execute("
            ALTER TABLE `store_order` 
            DROP FOREIGN KEY `fk_store_order_7`;
            ALTER TABLE `store_order` 
            DROP COLUMN `user_session_id`,
            DROP INDEX `fk_store_order_7_idx` ;
        ");
        return true;
    }
}
