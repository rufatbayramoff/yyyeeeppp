<?php

use yii\db\Migration;
use yii\db\Query;

class m170130_101022_3642_alter_country_capitals extends Migration
{
    public function up()
    {
        $this->addColumn('geo_country', 'capital_id', 'integer');
        $this->addForeignKey('geo_country_capital', 'geo_country', 'capital_id', 'geo_city', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('geo_country_capital', 'geo_country');
        $this->dropColumn('geo_country', 'capital_id');
    }
}
