<?php

use yii\db\Migration;

/**
 * Class m180824_112117_add_new_field_for_email_template
 */
class m180824_112117_add_new_field_for_email_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('email_template', 'template_sms', 'mediumtext NULL after template_text');
        $this->execute('UPDATE email_template SET template_sms = template_text');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('email_template', 'template_sms');
    }
}
