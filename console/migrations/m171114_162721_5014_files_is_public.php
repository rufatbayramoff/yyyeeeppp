<?php

use yii\db\Migration;

class m171114_162721_5014_files_is_public extends Migration
{
    public function safeUp()
    {
        $this->update('file', ['is_public'=>0],'is_public is null');
        $this->alterColumn('file', 'is_public', 'tinyint(1) not null default 0');

        $this->update('file', ['last_access_at'=>'2016-07-04 01:01:01'],'last_access_at is null');
        $this->alterColumn('file', 'last_access_at', 'timestamp not null DEFAULT CURRENT_TIMESTAMP');

    }

    public function safeDown()
    {
    }
}
