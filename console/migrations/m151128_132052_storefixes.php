<?php

use yii\db\Schema;
use yii\db\Migration;

class m151128_132052_storefixes extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `store_unit`  CHANGE COLUMN `price_per_print` `price_per_print` DECIMAL(6,2) NOT NULL DEFAULT '0.00' ; ");
    }

    public function down()
    {
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
