<?php

use yii\db\Migration;
use yii\db\Query;

class m171109_134021_4028_affiliate_widget extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'uid', 'char(6) not null after id');
        $users = (new Query())
            ->select('id')
            ->from('user')
            ->all();
        foreach ($users as $userInfo) {
            $userId = $userInfo['id'];
            $this->update('user', ['uid' => generateUid()], 'id=' . $userId);
        }
        $this->createIndex('user_uid', 'user', 'uid', true);

    }

    public function safeDown()
    {
        $this->dropColumn('user', 'uid');
    }
}
