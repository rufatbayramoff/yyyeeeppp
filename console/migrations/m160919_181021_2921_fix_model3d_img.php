<?php

use common\components\ArrayHelper;
use yii\db\Migration;
use yii\db\Query;

class m160919_181021_2921_fix_model3d_img extends Migration
{
    public function up()
    {
        $model3ds = (new Query())
            ->select('*')
            ->from('model3d')->all();

        $model3dImgs = (new Query())
            ->select('*')
            ->from('model3d_img')->all();

        $model3dParts = (new Query())
            ->select('*')
            ->from('model3d_part')->all();

        foreach ($model3ds as $model3d) {

            $coverFileId = $model3d['cover_file_id'];
            $model3dId = $model3d['id'];

            if ($imgExists = ArrayHelper::findAR($model3dImgs, ['file_id' => $coverFileId, 'model3d_id' => $model3dId])) {
                continue;
            }

            if ($partExists = ArrayHelper::findAR($model3dParts, ['file_id' => $coverFileId, 'model3d_id' => $model3dId])) {
                continue;
            }

            $part = ArrayHelper::findAR($model3dImgs, ['model3d_id' => $model3dId]);
            if (!$part) {
                $part = ArrayHelper::findAR($model3dParts, ['model3d_id' => $model3dId]);
                if (!$part) {
                    // Invalid model3d. model3dpart not extis
                    continue;
                }
            }
            $this->update(
                'model3d',
                [
                    'cover_file_id' => $part['file_id']
                ],
                'id=' . $model3d['id']
            );
        }
    }

    public function down()
    {

    }
}
