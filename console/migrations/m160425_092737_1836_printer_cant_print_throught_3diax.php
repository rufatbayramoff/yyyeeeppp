<?php

use yii\db\Migration;

class m160425_092737_1836_printer_cant_print_throught_3diax extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `printer` ADD `cant_print_throught_3diax` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `have_unfixed_size`;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `printer` DROP `cant_print_throught_3diax`;");
        return true;
    }
}
