<?php

use yii\db\Migration;

class m171116_144522_4592_email_templates extends Migration
{
    public function safeUp()
    {

        $this->execute("
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'ps.preorder.new', 'service', 'en-US', ' You have a new quote #%quoteId% on Treatstock', NULL, '2017-10-16 07:03:47', '<p>Hello %psName%! </p>
            <p>You have received a quote #%quoteId% on Treatstock. Please have a look <a href=\"%newOrdersLink%\">here</a></p>
            <p>Best regards,
            Treatstock</p>
            ', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'customer.preorder.psSendOffer', 'service', 'en-US', 'Quote #%quoteId% has been modified by Print Service', NULL, '2017-10-16 07:03:47', '<p>Hello %userName%! </p>
            <p>Quote #%quoteId% has been modified by Print Service. New offer has been sent. Please have a look <a href=\"%newOrdersLink%\">here</a></p>
            <p>Best regards,
            Treatstock</p>', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'ps.preorder.customerDeclinePreorder', 'service', 'en-US', 'Quote #%quoteId% has been canceled by the customer', NULL, '2017-10-16 07:03:47', '<p>Hello %psName%,</p>
            <p>
            Your quote #%quoteId% has been canceled by the customer.
            </p>
            <p>
            Thank you,
            Treatstock Team</p>
            ', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES (NULL, 'customer.preorder.psDeclinePreorder', 'service', 'en-US', 'Quote #%quoteId% has been denied by the Print Service', NULL, '2017-10-16 07:03:47', '<p>Hello %userName%,</p>
            <p>
            Your quote #%quoteId% has been denied by the Print Service.
            </p>
            <p>
            Thank you,
            Treatstock Team</p>
            ', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'customer.preorder.moderatorDeclinePreorder', 'service', 'en-US', 'Quote #%quoteId% has been canceled by moderator', NULL, '2017-10-16 07:03:47', '<p>Hello %userName%,</p> <p> Your quote #%quoteId% has been canceled by moderator. </p> <p> Thank you, Treatstock Team</p>', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES ( NULL, 'ps.preorder.moderatorDeclinePreorder', 'service', 'en-US', 'Quote #%quoteId% has been canceled by moderator', NULL, '2017-10-16 07:03:47', '<p>Hello %psName%,</p> <p> Your quote #%quoteId% has been canceled by moderator. </p> <p> Thank you, Treatstock Team</p>', NULL, '1' );
            INSERT INTO `email_template`(`id`,`code`,`group`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`,`is_active`) VALUES (NULL, 'ps.preorder.customerAcceptOffer', 'service', 'en-US', 'The customer accept your offer regarding your quote #%quoteId%', NULL, '2017-10-16 07:03:47', '<p>Hello %psName%,</p>
            <p>
            The customer accept your offer, to see the details, please follow the link <a href=\"%newOrdersLink%\">%newOrdersLink%</a>
            </p>
            <p>
            Thank you,
            Treatstock Team</p>
            ', NULL, '1' );
        ");



    }

    public function safeDown()
    {
        echo "m171116_144522_4592_email_templates cannot be reverted.\n";

        return false;
    }
}
