<?php

use yii\db\Migration;

class m160404_160703_1253_user_history extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `user_history` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `user_changed_id` int(11) DEFAULT NULL,
              `user_id` int(11) DEFAULT NULL,
              `created_at` datetime DEFAULT NULL,
              `action_id` varchar(255) DEFAULT NULL,
              `comment` text,
              `data` text,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

    }

    public function down()
    {
        $this->dropTable("user_history");
        return true;
    }
}
