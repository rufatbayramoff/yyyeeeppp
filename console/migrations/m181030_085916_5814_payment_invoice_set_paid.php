<?php

use common\models\StoreOrder;
use yii\db\Migration;

/**
 * Class m181030_085916_5814_payment_invoice_set_paid
 */
class m181030_085916_5814_payment_invoice_set_paid extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $paymentInvoices = (new \yii\db\Query())->select(['*'])
            ->from('payment_invoice')
            ->where([
                'and',
                ['is not', 'store_order_id', dbexpr('null')]
            ])
            ->all();

        foreach ($paymentInvoices as $invoice) {
            if ($this->isStoreOrderPaid($invoice['store_order_id'])) {
                echo "Order #{$invoice['store_order_id']} is paid\n";

                $this->update('payment_invoice', [
                    'status' => 'paid'
                ], [
                    'uuid' => $invoice['uuid']
                ]);
            } else {
                echo "Order #{$invoice['store_order_id']} not paid\n";
            }
            echo "-------------------------\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }

    protected function isStoreOrderPaid($storeOrderId): bool
    {
        $paymentStatus = (new \yii\db\Query())->select(['id', 'payment_status'])->from('store_order')->where(['id' => $storeOrderId])->one();

        if (!$paymentStatus) {
            return false;
        }

        $payedStatuses = [
            'authorized',
            'settling',
            'settled',
            'submitted',
            'submitted_for_settlement',
            'paid',
            'succeeded'
        ];

        return \in_array($paymentStatus['payment_status'], $payedStatuses, true);
    }
}
