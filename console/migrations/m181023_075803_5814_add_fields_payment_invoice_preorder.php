<?php

use yii\db\Migration;

/**
 * Class m181023_075803_5814_add_fields_payment_invoice_preorder
 */
class m181023_075803_5814_add_fields_payment_invoice_preorder extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_invoice', 'preorder_id', 'int NULL after order_position_id');
        $this->addColumn('preorder', 'current_payment_invoice_uuid', 'varchar(6) NULL after user_id');

        $this->createIndex('index_pi_preorder_id', 'payment_invoice', 'preorder_id');
        $this->createIndex('index_p_current_payment_invoice_uuid', 'preorder', 'current_payment_invoice_uuid', true);

        $this->addForeignKey('fk_pi_preorder_id', 'payment_invoice', 'preorder_id', 'preorder', 'id');
        $this->addForeignKey('fk_p_current_payment_invoice_uuid', 'preorder', 'current_payment_invoice_uuid', 'payment_invoice', 'uuid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pi_preorder_id', 'payment_invoice');
        $this->dropForeignKey('fk_p_current_payment_invoice_uuid', 'preorder');

        $this->dropColumn('payment_invoice', 'preorder_id');
        $this->dropColumn('preorder', 'current_payment_invoice_uuid');
    }
}
