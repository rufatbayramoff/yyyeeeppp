<?php

use yii\db\Schema;
use yii\db\Migration;

class m151125_150947_model3d_props_uniq extends Migration
{
    public function up()
    { 
        $this->execute("ALTER TABLE `model3d_file_properties` 
        ADD UNIQUE INDEX `model3d_file_id_UNIQUE` (`model3d_file_id` ASC);
        ");
    }

    public function down()
    {
       
       $this->dropIndex('model3d_file_id_UNIQUE', 'model3d_file_properties');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
