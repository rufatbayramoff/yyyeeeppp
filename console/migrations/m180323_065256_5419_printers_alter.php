<?php

use yii\db\Migration;

/**
 * Class m180323_065256_5419_printers_alter
 */
class m180323_065256_5419_printers_alter extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `printer` CHANGE COLUMN `weight` `weight` VARCHAR(15) NULL DEFAULT NULL ;');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
