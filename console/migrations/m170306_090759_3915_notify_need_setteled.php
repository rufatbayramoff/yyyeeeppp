<?php

use yii\db\Migration;

class m170306_090759_3915_notify_need_setteled extends Migration
{
    public function up()
    {
        $this->addColumn('payment_transaction', 'is_notified_need_settled', 'boolean DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('payment_transaction', 'is_notified_need_settled');

        return true;
    }
}
