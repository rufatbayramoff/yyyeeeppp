<?php

use yii\db\Migration;

/**
 * Class m180822_142001_5829_product_alter
 */
class m180822_142001_5829_product_alter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `product` ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT AFTER `uuid`, ADD INDEX `key` (`id` ASC);');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180822_142001_5829_product_alter cannot be reverted.\n";

        return false;
    }
    */
}
