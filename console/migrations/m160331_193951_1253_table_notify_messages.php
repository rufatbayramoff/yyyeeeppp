<?php

use yii\db\Migration;

class m160331_193951_1253_table_notify_messages extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `notify_message` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `user_id` int(11) NOT NULL,
          `message_type` varchar(255) NOT NULL DEFAULT '',
          `params` text,
          `sender` varchar(30) NOT NULL DEFAULT '',
          `sent_datetime` datetime NOT NULL,
          `is_sended` tinyint(3) unsigned NOT NULL DEFAULT '0',
          PRIMARY KEY (`id`),
          KEY `user` (`user_id`),
          CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;");
    }

    public function down()
    {
        $this->dropTable('notify_message');
        return true;
    }
}
