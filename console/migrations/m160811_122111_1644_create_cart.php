<?php

use yii\db\Migration;

class m160811_122111_1644_create_cart extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `cart_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SQL
        );
        $this->execute(
            <<<SQL
CREATE TABLE `cart_item` (
  `cart_id` int(11) NOT NULL,
  `model3d_replica_id` int(11) NOT NULL,
  `ps_printer_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  KEY `cart_item_cart_id` (`cart_id`),
  KEY `cart_item_model3d_replica_id` (`model3d_replica_id`),
  KEY `cart_item_ps_printer_id` (`ps_printer_id`),
  KEY `cart_item_currency_id` (`currency_id`),
  CONSTRAINT `cart_item_payement_currency` FOREIGN KEY (`currency_id`) REFERENCES `payment_currency` (`id`),
  CONSTRAINT `cart_item_cart_id` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`),
  CONSTRAINT `cart_item_model3d_replica` FOREIGN KEY (`model3d_replica_id`) REFERENCES `model3d_replica` (`id`),
  CONSTRAINT `cart_item_ps_printer_id` FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SQL
        );
    }

    public function down()
    {
        echo "m160811_122111_1644_create_cart cannot be reverted.\n";

        return false;
    }
}
