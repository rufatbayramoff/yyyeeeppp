<?php

use yii\db\Migration;

class m170302_061941_3877_region_null extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `user_address` CHANGE COLUMN `region` `region` VARCHAR(145) NULL ;');
        $this->execute('ALTER TABLE `geo_country` ADD COLUMN `region_required` BIT(1) NOT NULL DEFAULT b\'0\' AFTER `capital_id`');

        $this->execute("update geo_country set region_required=1 where iso_code IN('US', 'CA', 'ES', 'IT', 'RU', 'CN');");
    }

    public function down()
    {
        $this->dropColumn('geo_country', 'region_required');
        $this->execute('ALTER TABLE `user_address` CHANGE COLUMN `region` `region` VARCHAR(145) NOT NULL;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
