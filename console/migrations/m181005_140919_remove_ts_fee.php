<?php

use yii\db\Migration;

/**
 * Class m181005_140919_remove_ts_fee
 */
class m181005_140919_remove_ts_fee extends Migration
{
    public function safeUp()
    {
        $group = app('db')->createCommand('SELECT * FROM system_setting_group WHERE title=:title')
            ->bindValue('title', 'store')
            ->queryOne();
        $groupId  = $group['id'];
        $this->execute("UPDATE `system_setting` set value=0 WHERE `key` = 'ts_fee' AND group_id=$groupId;");
    }

    public function safeDown()
    {
        $group = app('db')->createCommand('SELECT * FROM system_setting_group WHERE title=:title')
            ->bindValue('title', 'store')
            ->queryOne();
        $groupId  = $group['id'];
        $this->execute("INSERT INTO `system_setting`
                (`group_id`,`key`,`value`, `created_user_id`,`updated_user_id`,`description`,`json`,`is_active`) VALUES
                (".$groupId. ",'ts_fee', '0.15', 2, NULL, 'Our fee (15%)','',1);");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181005_140919_remove_ts_fee cannot be reverted.\n";

        return false;
    }
    */
}
