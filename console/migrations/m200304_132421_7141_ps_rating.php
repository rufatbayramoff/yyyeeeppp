<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200304_132421_7141_ps_rating
 */
class m200304_132421_7141_ps_rating extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `ps_catalog` ( `ps_id` INT(11) NOT NULL , `rating_avg` DECIMAL(2,1) NOT NULL DEFAULT \'0\' , `rating_med` DECIMAL(5,4) NOT NULL DEFAULT \'0\' , `rating_count` INT(11) NOT NULL DEFAULT \'0\' , `price_low` DECIMAL(11,2) NOT NULL ) ENGINE = InnoDB;');
        $this->execute('ALTER TABLE `ps_catalog` ADD PRIMARY KEY(`ps_id`);');
        $this->execute('ALTER TABLE `ps_catalog` ADD CONSTRAINT `fk_ps_catalog_id` FOREIGN KEY (`ps_id`) REFERENCES `ps`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropTable('ps_catalog');
    }
}