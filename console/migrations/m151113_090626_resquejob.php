<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_090626_resquejob extends Migration
{
   
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("CREATE TABLE `job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `finished_at` timestamp NULL DEFAULT NULL,
  `operation` char(25) NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  `status` char(15) NOT NULL,
  `args` varchar(250) NOT NULL,
  `result` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_oper_idx` (`operation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    }

    public function safeDown()
    {
        $this->truncateTable('job');
        $this->dropTable('job');
    } 
}
