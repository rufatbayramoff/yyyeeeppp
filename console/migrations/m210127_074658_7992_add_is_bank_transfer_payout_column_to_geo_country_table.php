<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%geo_country}}`.
 */
class m210127_074658_7992_add_is_bank_transfer_payout_column_to_geo_country_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%geo_country}}', 'is_bank_transfer_payout', $this->tinyInteger()->defaultValue(0)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%geo_country}}', 'is_bank_transfer_payout');
    }
}
