<?php

use yii\db\Schema;
use yii\db\Migration;

class m151225_080958_admin_access_updates extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_admin_access` ADD COLUMN `by_group` INT(11) NOT NULL DEFAULT 1 AFTER `updated_at`;");
        
        $this->execute("CREATE TABLE `user_admin_group` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `title` VARCHAR(45) NOT NULL,
            `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            UNIQUE INDEX `title_UNIQUE` (`title` ASC)) 
            ENGINE=InnoDB DEFAULT CHARSET=utf8;
          ");
        $this->insert('user_admin_group', ['title'=>'Admin']);        
        $this->insert('user_admin_group', ['title'=>'Moderator']);     
        
        $this->execute("ALTER TABLE `user_admin` 
            ADD COLUMN `group_id` INT(11) NULL AFTER `updated_at`,
            ADD INDEX `fk_user_admin_1_idx` (`group_id` ASC);
            ALTER TABLE `user_admin` 
            ADD CONSTRAINT `fk_user_admin_1`
              FOREIGN KEY (`group_id`)
              REFERENCES `user_admin_group` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
        
        $this->execute("CREATE TABLE `user_admin_group_access` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `group_id` int(11) NOT NULL,
            `access` char(45) NOT NULL,
            `is_active` tinyint(1) NOT NULL DEFAULT '0',
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            UNIQUE KEY `index2` (`group_id`,`access`),
            CONSTRAINT `fk_user_admin_group_access_1` FOREIGN KEY (`group_id`) REFERENCES `user_admin_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        
        $this->truncateTable('user_admin_group_access');
        $this->dropTable('user_admin_group_access');
        
        $this->dropColumn('user_admin_access', 'by_group');        
        $this->truncateTable('user_admin_group');
        $this->dropTable('user_admin_group');
        
        $this->dropForeignKey('fk_user_admin_1', 'user_admin');
        $this->dropIndex('fk_user_admin_1_idx', 'user_admin');
        $this->dropColumn('user_admin', 'group_id');
        
        
        
        
    }
}
