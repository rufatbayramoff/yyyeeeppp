<?php

use yii\db\Migration;

class m170414_163821_3999_user_adress_lat_lon extends Migration
{
    public function up()
    {
        $this->addColumn('user_address', 'lat', 'decimal(8,5)');
        $this->addColumn('user_address', 'lon', 'decimal(8,5)');
    }

    public function down()
    {
        $this->dropColumn('user_address', 'lat');
        $this->dropColumn('user_address', 'lon');
    }
}
