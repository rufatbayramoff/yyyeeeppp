<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_073148_ustaxes extends Migration
{
     
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("CREATE TABLE `tax_us` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `zip_code` char(15) NOT NULL,
        `state_abbrevation` char(3) NOT NULL,
        `county_name` char(3) NOT NULL,
        `city_name` char(3) NOT NULL,
        `state_sales_tax` decimal(7,6) NOT NULL,
        `county_sales_tax` decimal(7,6) NOT NULL,
        `city_sales_tax` decimal(7,6) NOT NULL,
        `state_use_tax` decimal(7,6) NOT NULL,
        `county_use_tax` decimal(7,6) NOT NULL,
        `city_use_tax` decimal(7,6) NOT NULL,
        `total_sales_tax` decimal(7,6) NOT NULL,
        `total_use_tax` decimal(7,6) NOT NULL,
        `is_tax_shipping_alone` char(1) NOT NULL DEFAULT 'N',
        `is_tax_ship_handling` char(1) NOT NULL DEFAULT 'N',
        `created_at` timestamp NULL DEFAULT NULL,
        `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `zip_code_UNIQUE` (`zip_code`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      ");
    }

    public function safeDown()
    {
        $this->truncateTable("tax_us");
        $this->dropTable("tax_us");
    }
     
}
