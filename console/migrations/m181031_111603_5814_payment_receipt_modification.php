<?php

use yii\db\Migration;

/**
 * Class m181031_111603_5814_payment_receipt_modification
 */
class m181031_111603_5814_payment_receipt_modification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payment_receipt_invoice_comment', [
            'receipt_id' => $this->integer()->notNull(),
            'payment_invoice_uuid' => $this->string(6)->notNull(),
            'comment' => $this->string(245)
        ]);

        $this->createIndex('inx_pric_receipt_id', 'payment_receipt_invoice_comment', 'receipt_id');
        $this->createIndex('inx_pric_payment_invoice_uuid', 'payment_receipt_invoice_comment', 'payment_invoice_uuid', true);

        $this->addForeignKey('fk_pric_receipt_id', 'payment_receipt_invoice_comment', 'receipt_id', 'payment_receipt', 'id');
        $this->addForeignKey('fk_pric_payment_invoice_uuid', 'payment_receipt_invoice_comment', 'payment_invoice_uuid', 'payment_invoice', 'uuid');

        $receipts = (new \yii\db\Query())->select(['*'])->from('payment_receipt')->where([
            'and',
            ['is not', 'comment', dbexpr('null')],
            ['<>', 'comment', '']
        ])->all();

        foreach ($receipts as $receipt) {
            $invoiceUuid = app()->db->createCommand('select
                                                            max(i.uuid) as uuid
                                                        from store_order so
                                                          join payment_invoice i on so.id = i.store_order_id
                                                        where so.id = :id
                                                        group by so.id
                                                        order by max(i.created_at) DESC', ['id' => $receipt['order_id']])->queryScalar();

            if ($invoiceUuid) {
                $this->insert('payment_receipt_invoice_comment', [
                    'receipt_id' => $receipt['id'],
                    'payment_invoice_uuid' => $invoiceUuid,
                    'comment' => $receipt['comment'],
                ]);
            }
        }

        $this->dropColumn('payment_receipt', 'comment');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pric_receipt_id', 'payment_receipt_invoice_comment');
        $this->dropForeignKey('fk_pric_payment_invoice_uuid', 'payment_receipt_invoice_comment');

        $this->dropTable('payment_receipt_invoice_comment');

        $this->addColumn('payment_receipt', 'comment', $this->string(245));
    }
}
