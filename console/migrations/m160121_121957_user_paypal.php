<?php

use yii\db\Schema;
use yii\db\Migration;

class m160121_121957_user_paypal extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `user_paypal` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `user_id` int(11) DEFAULT NULL,
            `fullname` varchar(125) DEFAULT NULL,
            `email` varchar(145) DEFAULT NULL,
            `address` varchar(145) DEFAULT NULL,
            `country_iso` char(2) DEFAULT NULL,
            `check_status` varchar(45) NOT NULL DEFAULT 'new',
            PRIMARY KEY (`id`),
            UNIQUE KEY `fk_user_paypal_1_idx` (`user_id`),
            UNIQUE KEY `userp_email_uq` (`email`),
            KEY `userp_index4_country` (`country_iso`),
            CONSTRAINT `fk_user_paypal_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        
        $this->execute("CREATE TABLE `user_paypal_history` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `user_paypal_id` int(11) NOT NULL,
            `user_id` int(11) NOT NULL,
            `action_id` varchar(45) NOT NULL,
            `comment` varchar(145) NOT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_user_paypal_history_1_idx` (`user_paypal_id`),
            KEY `fk_user_paypal_history_2_idx` (`user_id`),
            CONSTRAINT `fk_user_paypal_history_1` FOREIGN KEY (`user_paypal_id`) REFERENCES `user_paypal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_user_paypal_history_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function down()
    {
        $this->truncateTable('user_paypal_history');
        $this->dropTable('user_paypal_history');
        $this->truncateTable('user_paypal');
        $this->dropTable('user_paypal');
    }
}
