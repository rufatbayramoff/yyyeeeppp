<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180619_150021_5577_product_created_at
 */
class m180619_150021_5577_product_created_at extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('product', 'created_at', 'datetime null after product_status');
        $this->execute('UPDATE product SET created_at=updated_at');
        $this->alterColumn('product', 'created_at', 'datetime not null');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
