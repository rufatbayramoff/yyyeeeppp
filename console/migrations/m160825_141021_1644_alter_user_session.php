<?php

use yii\db\Migration;

class m160825_141021_1644_alter_user_session extends Migration
{
    public function up()
    {
        $this->dropColumn('user_session', 'id');
        $this->addColumn('user_session', 'id', 'pk first');
        $this->addColumn('user_session', 'uuid', 'char(40) not null');
        $this->createIndex('user_session_uuid', 'user_session', 'uuid', true);
        $this->addColumn('user_session', 'user_id', 'integer(11) null default null');
        $this->addForeignKey('user_session_user_id', 'user_session', 'user_id', 'user', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('user_session_user_id', 'user_session');
        $this->dropColumn('user_session', 'user_id');
        $this->dropColumn('user_session', 'uuid');
    }
}
