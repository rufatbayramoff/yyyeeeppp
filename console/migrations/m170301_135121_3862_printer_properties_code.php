<?php

use yii\db\Migration;

class m170301_135121_3862_printer_properties_code extends Migration
{
    public function up()
    {
        $this->addColumn('printer_properties', 'code', 'varchar(100) after id');
        $this->createIndex('printer_properties_code_unique', 'printer_properties', 'code', true);
        $this->db->createCommand('UPDATE printer_properties SET `code`=`title`')->execute();
    }

    public function down()
    {
        $this->dropColumn('printer_properties', 'code');
    }
}
