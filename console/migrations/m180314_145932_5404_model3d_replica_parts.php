<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180314_145932_5404_model3d_parts
 */
class m180314_145932_5404_model3d_replica_parts extends Migration
{
    public function safeUp()
    {
        $this->addColumn('model3d', 'original_model3d_id', 'int(11) null');
        $this->addForeignKey('fk_model3d_original_id', 'model3d', 'original_model3d_id', 'model3d', 'id');

        $this->addColumn('model3d', 'master_store_unit_id', 'int(11) null');
        $this->addForeignKey('fk_model3d_store_unit_id', 'model3d', 'master_store_unit_id', 'store_unit', 'id');

        $this->addColumn('model3d_part', 'original_model3d_part_id', 'int(11) null');
        $this->addForeignKey('fk_model3d_part_original_id', 'model3d_part', 'original_model3d_part_id', 'model3d_part', 'id');

        $this->addColumn('model3d_img', 'original_model3d_img_id', 'int(11) null');
        $this->addForeignKey('fk_model3d_img_original_id', 'model3d_img', 'original_model3d_img_id', 'model3d_img', 'id');

        $this->addColumn('file', 'user_session_id', 'int(11) not null after user_id');
        $this->update('file', ['user_session_id' => 1]);
        $this->addForeignKey('fk_file_user_session_id', 'file', 'user_session_id', 'user_session', 'id');

        $model3dParts = (new Query())
            ->select('model3d_part.*, model3d.user_id as user_id')
            ->from('model3d_part')
            ->leftJoin('model3d', 'model3d_part.model3d_id=model3d.id')
            ->where('model3d.user_id is not null')
            ->all();
        foreach ($model3dParts as $model3dPart) {
            $this->update('file', ['user_id' => $model3dPart['user_id']], 'id=' . $model3dPart['file_id'] . ' and user_id is null');
        }

        $this->addForeignKey('fk_file_user_id', 'file', 'user_id', 'user', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}
