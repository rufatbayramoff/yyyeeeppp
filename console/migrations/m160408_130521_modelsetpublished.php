<?php

use yii\db\Migration;

class m160408_130521_modelsetpublished extends Migration
{
    public function up()
    {
        $this->execute("UPDATE store_unit SET moderated_at=updated_at WHERE moderated_at IS NULL AND status NOT IN('new', 'draft')");
    }

    public function down()
    {
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
