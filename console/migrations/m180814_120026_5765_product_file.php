<?php

use yii\db\Migration;

/**
 * Class m180814_120026_5765_product_file
 */
class m180814_120026_5765_product_file extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `product_file` CHANGE COLUMN `type` `type` ENUM('cad', 'file', 'attachment') NULL DEFAULT 'file' ;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180814_120026_5765_product_file cannot be reverted.\n";

        return false;
    }
    */
}
