<?php

use yii\db\Migration;

/**
 * Class m180420_175021_5186_store_pricer_type
 */
class m180420_175021_5186_store_pricer_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('store_pricer_type', ['type'=>'ps_product', 'description'=>'Product total price', 'updated_at'=>'2018-04-20 07:19:02', 'operation'=>'+']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('store_pricer_type', ['type'=>'ps_product']);
    }
}


