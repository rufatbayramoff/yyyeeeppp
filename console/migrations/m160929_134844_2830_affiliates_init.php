<?php

use yii\db\Migration;

class m160929_134844_2830_affiliates_init extends Migration
{
    public function up()
    {
        $this->execute(
            'CREATE TABLE IF NOT EXISTS `affiliate` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `status` varchar(15) NOT NULL DEFAULT \'active\',
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `title` varchar(245) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_affiliate_1_idx` (`user_id`),
              CONSTRAINT `fk_affiliate_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
        );

        $this->execute(
            'CREATE TABLE IF NOT EXISTS  `affiliate_award_rule` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `title` varchar(145) NOT NULL,
                  `visitor_days_valid` int(5) DEFAULT NULL,
                  `max_orders_per_user` int(11) DEFAULT NULL,
                  `order_type_api` bit(1) DEFAULT NULL,
                  `order_type_upload` bit(1) DEFAULT NULL,
                  `order_type_any` bit(1) DEFAULT NULL,
                  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  `reward_type` enum(\'api\',\'fixed\',\'percent\') DEFAULT NULL,
                  `reward_amount` int(3) NOT NULL DEFAULT \'0\',
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            '
        );

        $this->execute(
                'CREATE TABLE IF NOT EXISTS  `affiliate_resource` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `affiliate_id` int(11) NOT NULL,
                  `url_referrer` varchar(245) DEFAULT NULL,
                  `params` varchar(245) DEFAULT NULL,
                  `status` char(15) NOT NULL DEFAULT \'active\',
                  `award_rule_id` int(11) DEFAULT NULL,
                  `priority` int(5) NOT NULL DEFAULT \'1\',
                  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  `description` varchar(245) DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  KEY `fk_affiliate_source_1_idx` (`affiliate_id`),
                  KEY `fk_affiliate_source_2_idx` (`award_rule_id`),
                  CONSTRAINT `fk_affiliate_source_1` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  CONSTRAINT `fk_affiliate_source_2` FOREIGN KEY (`award_rule_id`) REFERENCES `affiliate_award_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
        );

        $this->execute(
            'CREATE TABLE IF NOT EXISTS `affiliate_award` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `resource_id` int(11) NOT NULL,
              `price` decimal(6,2) NOT NULL DEFAULT \'0.00\',
              `price_currency` char(5) NOT NULL DEFAULT \'USD\',
              `type` varchar(25) DEFAULT NULL,
              `status` char(15) NOT NULL DEFAULT \'active\',
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `order_id` int(11) NOT NULL,
              `award_rule_id` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_affiliate_award_1_idx` (`resource_id`),
              KEY `fk_affiliate_award_2_idx` (`award_rule_id`),
              KEY `fk_affiliate_award_3_idx` (`order_id`),
              CONSTRAINT `fk_affiliate_award_1` FOREIGN KEY (`resource_id`) REFERENCES `affiliate_resource` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_affiliate_award_2` FOREIGN KEY (`award_rule_id`) REFERENCES `affiliate_award_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_affiliate_award_3` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
        );
        $this->execute(
            'CREATE TABLE IF NOT EXISTS `affiliate_history` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `affiliate_id` int(11) NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `action_id` char(15) NOT NULL,
              `comment` varchar(245) DEFAULT NULL,
              `user_id` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_affiliate_history_1_idx` (`affiliate_id`),
              KEY `fk_affiliate_history_2_idx` (`user_id`),
              CONSTRAINT `fk_affiliate_history_1` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_affiliate_history_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
        );
        $this->execute(
            'CREATE TABLE IF NOT EXISTS `affiliate_user_source` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `user_id` int(11) DEFAULT NULL,
              `resource_id` int(11) NOT NULL,
              `type` enum(\'register\',\'visit\') DEFAULT NULL,
              `status` char(15) DEFAULT \'active\',
              `first_url` varchar(245) DEFAULT NULL,
              `referrer` varchar(245) DEFAULT NULL,
              `user_agent` varchar(245) DEFAULT NULL,
              `ip` varchar(245) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_affiliate_user_source_1_idx` (`resource_id`),
              KEY `fk_affiliate_user_source_2_idx` (`user_id`),
              KEY `index4` (`user_agent`,`ip`,`resource_id`),
              CONSTRAINT `fk_affiliate_user_source_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_affiliate_user_source_1` FOREIGN KEY (`resource_id`) REFERENCES `affiliate_resource` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
        );

        // insert default affiliates Treatstock
        $this->insert('affiliate', ['id'=>1, 'user_id'=>110, 'title'=>'Treatstock', 'created_at' => dbexpr('NOW()')]);
        $this->insert(
            'affiliate_resource',
            [
                'id'           => 1,
                'affiliate_id' => 1,
                'url_referrer' => '-',
                'params'       => null,
                'status'       => 'active',
                'priority'     => 1,
                'description'  => 'Default source'
            ]
        );
        // default rules ?
        $this->insert(
            'affiliate_award_rule',
            [
                'title'             => 'No Reward Rule',
                'order_type_api'    => 0,
                'order_type_upload' => 0,
                'order_type_any'    => 0,
                'reward_type'       => 'fixed',
                'reward_amount'     => 0
            ]
        );
        $this->insert(
            'affiliate_award_rule',
            [
                'title'                => 'Basic 90 days Reward Rule',
                'visitor_days_valid'   => 90,
                'max_orders_per_user'  => 10,
                'order_type_api'       => 1,
                'order_type_upload'    => 1,
                'order_type_any'       => 1,
                'reward_type'          => 'fixed',
                'reward_amount'        => 1
            ]
        );
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->truncateTable('affiliate_history');
        $this->truncateTable('affiliate_user_source');
        $this->truncateTable('affiliate_award');
        $this->truncateTable('affiliate_award_rule');
        $this->truncateTable('affiliate');
        $this->truncateTable('affiliate_source');

        $this->dropTable('affiliate_history');
        $this->dropTable('affiliate_user_source');
        $this->dropTable('affiliate_source');
        $this->dropTable('affiliate_award');
        $this->dropTable('affiliate_award_rule');
        $this->dropTable('affiliate');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
