<?php

use yii\db\Migration;
use yii\db\Query;

class m170131_142021_3756_alter_translate_en extends Migration
{
    public function getTranslateTables()
    {
        return [
            'model3d_license'        => 'license_id',
            'msg_folder'             => 'msg_folder_id',
            'printer_color'          => 'printer_color_id',
            'printer_material_group' => 'group_id',
            'printer_material'       => 'printer_material_id',
            'printer_properties'     => 'printer_properties_id',
            'printer_technology'     => 'printer_technology_id',
            'seo_page'               => 'seo_page_id',
            'site_help_category'     => 'category_id',
            'site_help'              => 'help_id',
            'static_page'            => 'static_page_id',
            'store_category'         => 'category_id',
            'system_reject'          => 'system_reject_id'
        ];
    }


    public function processTable($tableName, $attributeIdName)
    {
        $enRows = (new Query())->select('*')->from($tableName . '_intl')->where('lang_iso="en-US"')->all();
        foreach ($enRows as $oneRow) {
            foreach ($oneRow as $attributeName => $value) {
                if ($attributeName == 'id' || $attributeName == 'lang_iso' || $attributeName == $attributeIdName) {
                    continue;
                }
                $this->update($tableName, [$attributeName => $value], 'id=' . $oneRow[$attributeIdName]);
            }
        }
        $this->delete($tableName . '_intl', ['lang_iso' => 'en-US']);
    }

    public function safeUp()
    {
        //Manual fixes:
        $this->delete('printer_material_group_intl', ['id' => '1']);

        $this->addColumn('static_page', 'title', 'string');
        $this->addColumn('static_page', 'content', 'longtext');

        // Auto
        $tables = $this->getTranslateTables();
        foreach ($tables as $tableName => $attributeIdName) {
            $this->processTable($tableName, $attributeIdName);
        }
    }

    public function down()
    {

    }
}
