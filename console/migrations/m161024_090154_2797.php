<?php

use yii\db\Migration;

class m161024_090154_2797 extends Migration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO `email_template`(
    `id`,
    `code`,
    `group`,
    `language_id`,
    `title`,
    `description`,
    `updated_at`,
    `template_html`,
    `template_text`
) VALUES (
    NULL,
    'ps.testorder.success',
    'service',
    'en-US',
    'Test order completed',
    NULL,
    NULL,
    'Hi %psName%! 
    
Congratulations! You successfully completed the test order and are now prepared for any future incoming orders.

If you are unsure about anything or would like to give some feedback, please contact support at support@treatstock.com.

Best regards,
Treatstock ',
    'Hi %psName%! 
Congratulations! You successfully completed the test order and are now prepared for any future incoming orders.'
 );
");


        $this->addColumn('ps_printer', 'is_test_order_resolved', 'tinyint NOT NULL DEFAULT 0');


        $this->execute("UPDATE ps_printer 
            LEFT JOIN store_order ON store_order.printer_id = ps_printer.id
            SET  is_test_order_resolved = 1
            WHERE store_order.order_status = 'received'
        ");


        $this->execute("
        
        INSERT INTO `site_help`(
    `id`,
    `created_at`,
    `updated_at`,
    `user_id`,
    `alias`,
    `title`,
    `content`,
    `is_active`,
    `clicks`,
    `is_ok`,
    `is_bad`,
    `category_id`,
    `priority`,
    `is_popular`,
    `views`
) VALUES (
    NULL ,
    NULL,
    '2016-11-01 12:20:45',
    NULL,
    'ps.order.test',
    'Test Order',
    'This is a test order so you can familiarize yourself with how the orders process will actually work with your printers. Click ''Accept test order'' to begin test order.',
    1,
    0,
    0,
    0,
    NULL,
    0,
    0,
    0
 );

        
        ");



    }

    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'ps.testorder.success']);
        $this->delete('site_help', ['alias' => 'ps.order.test']);
        $this->dropColumn('ps_printer', 'is_test_order_resolved');
        return true;
    }
}
