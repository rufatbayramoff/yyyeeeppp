<?php

use yii\db\Migration;

class m171110_130929_4592_order_store_unit extends Migration
{
    public function safeUp()
    {

        $this->execute("
            ALTER TABLE `store_order_item` 
            DROP FOREIGN KEY `fk_store_order_item_2`;
            ALTER TABLE `store_order_item` 
            CHANGE COLUMN `unit_id` `unit_id` INT(11) NULL ;
            ALTER TABLE `store_order_item` 
            ADD CONSTRAINT `fk_store_order_item_2`
              FOREIGN KEY (`unit_id`)
              REFERENCES `store_unit` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
    }

    public function safeDown()
    {
        echo "m171110_130929_4592_order_store_unit cannot be reverted.\n";

        return false;
    }
}
