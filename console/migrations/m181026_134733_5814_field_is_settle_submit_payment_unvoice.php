<?php

use yii\db\Migration;

/**
 * Class m181026_134733_5814_field_is_settle_submit_payment_unvoice
 */
class m181026_134733_5814_field_is_settle_submit_payment_unvoice extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_invoice', 'is_settle_submit', $this->tinyInteger()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('payment_invoice', 'is_settle_submit');
    }
}
