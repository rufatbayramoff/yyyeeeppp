<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180517_153712_5571_model3d_company_data
 */
class m180517_153712_5571_model3d_company_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $models = (new Query())->select('id, user_id')->where('user_id is not null')->from('model3d')->all();
        $models = \common\components\ArrayHelper::map($models, 'id', 'user_id');
        $companies = (new Query())->select('id, user_id')->from('ps')->all();
        $companies = \common\components\ArrayHelper::map($companies, 'user_id', 'id');

        foreach ($models as $modelId => $modelUserId) {
            if (array_key_exists($modelUserId, $companies)) {
                $companyId = $companies[$modelUserId];
                $this->update('model3d', ['company_id' => $companyId], ['id' => $modelId]);
            } else {
                echo "\nModel: ".$modelId. '. No company for user: ' .$modelUserId;
            }
        }

        $products = (new Query())->select('uuid, user_id')->where('user_id is not null')->from('product')->all();
        $products = \common\components\ArrayHelper::map($products, 'uuid', 'user_id');
        $companies = (new Query())->select('id, user_id')->from('ps')->all();
        $companies = \common\components\ArrayHelper::map($companies, 'user_id', 'id');

        foreach ($products as $productUuid => $productUserId) {
            if (array_key_exists($productUserId, $companies)) {
                $companyId = $companies[$productUserId];
                $this->update('product', ['company_id' => $companyId], ['uuid' => $productUuid]);
            } else {
                echo "\nProduct: ".$productUuid. '. No company for user: ' .$productUserId;
            }
        }
        $this->getDb()->close();
        $this->getDb()->open();
        $this->dropForeignKey('fk_product_company_id', 'product');
        $this->alterColumn('product', 'company_id', 'int(11) not null');
        $this->addForeignKey('fk_product_company_id', 'product', 'company_id', 'ps', 'id', 'RESTRICT', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        return true;
    }
}
