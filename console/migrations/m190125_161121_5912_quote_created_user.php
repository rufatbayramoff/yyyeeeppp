<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190125_161121_5912_quote_created_user
 */
class m190125_161121_5912_quote_created_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $body = '

%companyTitle% was created new quote for you.
You can view it by <a href="%link%" target="blank" style="display: inline-block;margin:10px 0;border-radius: 0px;font-family: sans-serif;padding: 5px 20px;min-width: 40px;font-size: 16px;font-weight: normal;line-height: 20px;border: none;text-decoration:none !important;color: #fff;background-color:#2d8ee0;">View Quote</a>

Best,
Treatstock
';
        $this->insert('email_template', [
            'code' => 'createdClientFroQuote',
            'group' => 'order',
            'language_id' => 'en-US',
            'title' => 'New account was created for you.',
            'description' =>  '%companyTitle% created new quote for you.',
            'template_html' => $body,
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);
    }

    public function safeDown()
    {
        $this->delete('email_template', ['code'=>'createdClientFroQuote']);
    }
}