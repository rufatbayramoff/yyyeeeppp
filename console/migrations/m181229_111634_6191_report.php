<?php

use yii\db\Migration;

/**
 * Class m181229_111634_6191_report
 */
class m181229_111634_6191_report extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `thingiverse_report` (
  `order_id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `thing_id` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `earnings` decimal(8,2) DEFAULT NULL,
  `platform_fee` decimal(8,2) DEFAULT NULL,
  `transaction_fee` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('thingiverse_report');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181229_111634_6191_report cannot be reverted.\n";

        return false;
    }
    */
}
