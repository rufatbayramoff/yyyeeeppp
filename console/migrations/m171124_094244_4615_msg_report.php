<?php

use yii\db\Migration;

class m171124_094244_4615_msg_report extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `msg_report` 
            ADD COLUMN `topic_id` INT(11) NULL AFTER `id`,
            ADD INDEX `fk_msg_report_topic_idx` (`topic_id` ASC);
            ALTER TABLE `msg_report` 
            ADD CONSTRAINT `fk_msg_report_topic`
              FOREIGN KEY (`topic_id`)
              REFERENCES `msg_topic` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");

        $this->execute("
            UPDATE msg_report, msg_message 
            SET msg_report.topic_id = msg_message.topic_id 
            WHERE msg_report.message_id = msg_message.id;
        ");

        $this->execute("
            ALTER TABLE `msg_report` 
            DROP FOREIGN KEY `fk_msg_report_topic`,
            DROP FOREIGN KEY `lnk_msg_report_msg_message`;
            ALTER TABLE `msg_report` 
            DROP COLUMN `message_id`,
            CHANGE COLUMN `topic_id` `topic_id` INT(11) NOT NULL ,
            DROP INDEX `lnk_msg_report_msg_message` ;
            ALTER TABLE `msg_report` 
            ADD CONSTRAINT `fk_msg_report_topic`
              FOREIGN KEY (`topic_id`)
              REFERENCES `msg_topic` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;        
        ");
    }

    public function safeDown()
    {
        echo "m171124_094244_4615_msg_report cannot be reverted.\n";
        return false;
    }
}
