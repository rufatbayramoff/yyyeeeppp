<?php

use yii\db\Migration;

/**
 * Class m180207_125340_5257_printed_pickup_email
 */
class m180207_125340_5257_printed_pickup_email extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $body = 'Hi %psName%,

Order #%orderId% results have been approved and we\'ve notified the customer that their order is ready for pickup. Please remember to set the order as picked up after the customer has collected their order. 

<a href="https://www.treatstock.com/my/printservice-order/print-requests/ready_send" target="blank" style="display: inline-block;margin:10px 0;border-radius: 0px;font-family: sans-serif;padding: 5px 20px;min-width: 40px;font-size: 16px;font-weight: normal;line-height: 20px;border: none;text-decoration:none !important;color: #fff;background-color:#2d8ee0;">View order</a>

Best,
Treatstock
';
        $this->insert('email_template', [
            'code' => 'psPrintedReviewed.pickup',
            'group' => 'order',
            'language_id' => 'en-US',
            'title' => 'Order #%orderId% results have been approved',
            'description' =>  '%orderId%, %psName% - sent when moderator accepts print results  and delivery is Pickup.',
            'template_html' => $body,
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('email_template', [
            'code' => 'psPrintedReviewed.pickup',
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180207_125340_5257_printed_pickup_email cannot be reverted.\n";

        return false;
    }
    */
}
