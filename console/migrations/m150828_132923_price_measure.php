<?php

use yii\db\Schema;
use yii\db\Migration;

class m150828_132923_price_measure extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->execute('TRUNCATE TABLE `ps_printer_color`;');
        $this->execute("ALTER TABLE `ps_printer_color`
            CHANGE COLUMN `price_measure` `price_measure` ENUM('cm', 'in') NULL DEFAULT NULL;");
        $this->execute('ALTER TABLE `ps_printer` DROP INDEX `user_printer_unique`;');
        $this->execute('DELETE FROM printer_to_property WHERE printer_id > 14;');
        $this->execute('DELETE FROM printer WHERE id > 14;');
    }

    public function safeDown()
    {
        $this->execute("ALTER TABLE `ps_printer_color`
            CHANGE COLUMN `price_measure` `price_measure` ENUM('sm', 'in') NULL DEFAULT NULL;");
        $this->execute('ALTER TABLE `ps_printer` ADD UNIQUE `user_printer_unique` (`user_id`, `printer_id`);');
    }
}
