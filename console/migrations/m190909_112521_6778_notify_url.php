<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m190909_112521_6778_notify_url
 */
class m190909_112521_6778_notify_url extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('UPDATE  email_template SET  template_html = REPLACE(template_html, \'%newOrdersLink%\', \'%link%\')');
        $this->execute('UPDATE  email_template SET  template_text = REPLACE(template_text, \'%newOrdersLink%\', \'%link%\')');
        $this->execute('UPDATE  email_template SET  template_sms = REPLACE(template_sms, \'%newOrdersLink%\', \'%link%\')');
    }

    public function safeDown()
    {

    }
}