<?php

use yii\db\Migration;

class m170428_124126_4158_thingiverse_public extends Migration
{
    public function up()
    {
        $this->execute("
          UPDATE file SET is_public=1 WHERE extension in('jpg', 'JPG', 'png', 'PNG', 'jpeg', 'gif', 'GIF') AND user_id=112
        ");
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
