<?php

use yii\db\Schema;
use yii\db\Migration;

class m160105_135119_site_help extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `site_help` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `user_id` int(11) DEFAULT NULL,
            `alias` varchar(45) NOT NULL,
            `title` varchar(155) NOT NULL,
            `content` mediumtext NOT NULL,
            `is_active` tinyint(1) NOT NULL DEFAULT '1',
            `clicks` int(11) NOT NULL DEFAULT '0',
            `is_ok` int(11) NOT NULL DEFAULT '0' COMMENT 'Is this help was helpful? Yes | No',
            `is_bad` int(11) NOT NULL DEFAULT '0',
            `lang_iso` char(7) NOT NULL DEFAULT 'en-US',
            PRIMARY KEY (`id`),
            UNIQUE KEY `index2` (`alias`),
            UNIQUE KEY `index4` (`alias`,`lang_iso`),
            KEY `fk_site_help_1_idx` (`user_id`),
            CONSTRAINT `fk_site_help_1` FOREIGN KEY (`user_id`) REFERENCES `user_admin` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        "); 
    }

    public function down()
    {
         $this->truncateTable('site_help');
         $this->dropTable('site_help');
    }
 
}
