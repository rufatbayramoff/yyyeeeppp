<?php

use yii\db\Migration;
use yii\db\Query;

class m170203_140621_3780_ps_facebook_page_id extends Migration
{
    public function up()
    {
        $this->addColumn('ps', 'facebook_page_id', 'string');
        $this->createIndex('ps_facebook_page_id', 'ps', 'facebook_page_id', true);
    }

    public function down()
    {
        $this->dropColumn('ps', 'facebook_page_id');
    }
}
