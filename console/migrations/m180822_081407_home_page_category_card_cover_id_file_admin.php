<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180822_081407_home_page_category_card_cover_id_file_admin
 */
class m180822_081407_home_page_category_card_cover_id_file_admin extends Migration
{
    public function addAdminFile($fileInfo)
    {
        $fields = [
            'uuid',
            'name',
            'path',
            'stored_name',
            'extension',
            'size',
            'created_at',
            'updated_at',
            'deleted_at',
            'user_id',
            'server',
            'status',
            'md5sum',
            'last_access_at',
            'ownerClass',
            'ownerField',
            'is_public',
            'path_version',
            'expire'
        ];
        $fieldKeys = array_combine($fields, $fields);
        $insertValue = array_intersect_key($fileInfo, $fieldKeys);
        $this->insert('file_admin', $insertValue);
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('home_page_category_card', 'cover_file_uuid', 'varchar(32) NULL after cover_id');
        $this->addForeignKey('fk_hpcc_cover_file_uuid', 'home_page_category_card', 'cover_file_uuid', 'file_admin', 'uuid');

        $siteHelps = (new Query())->select('*')->from('home_page_category_card')->where('cover_id is not null')->all();

        foreach ($siteHelps as $siteHelp) {
            $fileInfo = (new Query())->select('*')->from('file')->where(['id' => $siteHelp['cover_id']])->one();
            $this->addAdminFile($fileInfo);
            $this->update('home_page_category_card', ['cover_file_uuid' => $fileInfo['uuid']], 'id=' . $siteHelp['id']);
        }

        $this->dropColumn('home_page_category_card', 'cover_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
