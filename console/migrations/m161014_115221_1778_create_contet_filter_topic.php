<?php

use yii\db\Migration;
use yii\db\Query;

class m161014_115221_1778_create_contet_filter_topic extends Migration
{
    public function safeUp()
    {
        $this->execute(
            <<<SQL
 CREATE TABLE `content_filter_checked_topics` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `topic_id` INT(11) UNSIGNED NOT NULL,
  `isBanned` BOOLEAN ,
  PRIMARY KEY (`id`),
  KEY `topic_id` (`topic_id`),
  CONSTRAINT `fk_content_checked_topic_id` FOREIGN KEY (`topic_id`) REFERENCES `msg_topic` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function safeDown()
    {
        $this->dropTable('content_filter_checked_topics');
        return true;
    }

}
