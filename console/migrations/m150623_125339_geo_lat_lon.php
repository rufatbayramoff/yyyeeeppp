<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_125339_geo_lat_lon extends Migration
{
    public function up()
    {
        $this->createIndex('lat_lon', '{{%geo_city}}', ['lat', 'lon'], false);
        $this->dropColumn('{{%geo_city}}', 'lat_trim');
        $this->dropColumn('{{%geo_city}}', 'lon_trim');
    }

    public function down()
    {
        $this->dropIndex('lat_lon', '{{%geo_city}}');
        // DELETE for lat_trim and lon_trim can't be reverted
    }
}
