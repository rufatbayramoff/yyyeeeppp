<?php

use common\interfaces\Model3dBaseImgInterface;
use yii\db\Migration;

class m160926_151621_2956_model3d_texture_material_group extends Migration
{
    public function up()
    {
        $this->alterColumn('model3d_texture', 'printer_material_id', 'integer');
        $this->addColumn('model3d_texture', 'printer_material_group_id', 'integer');
        $this->addForeignKey('model3d_texture_material_group_id', 'model3d_texture', 'printer_material_group_id', 'printer_material_group', 'id');
    }

    public function down()
    {
        $this->dropColumn('model3d_texture', 'printer_material_group_id');
    }
}
