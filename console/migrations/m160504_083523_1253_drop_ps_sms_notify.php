<?php

use yii\db\Migration;

class m160504_083523_1253_drop_ps_sms_notify extends Migration
{
    public function up()
    {
        $this->dropColumn('ps', 'sms_notify');
    }

    public function down()
    {
        $this->execute("ALTER TABLE `ps`  ADD COLUMN `sms_notify` TINYINT(1) NOT NULL DEFAULT 0 AFTER `phone_status`;");
        return true;
    }
}
