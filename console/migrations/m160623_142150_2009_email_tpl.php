<?php

use yii\db\Migration;

class m160623_142150_2009_email_tpl extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->insert(
            'email_template',
            [
                'code'          => 'psPrintedReviewed', // ORDER_PRINT_REVIEW
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => 'Order #%orderId% print results reviewed',
                'description'   => 'PS gets this message after moderator accepted printed results',
                'updated_at'    => dbexpr('NOW()'),
                'template_html' => '<p>Order #%orderId% print results have been approved. You can now print the postal label and send this order to the customer. Thank you.</p><p>Please visit treatstock.com for details.</p>',
                'template_text' => 'Order #%orderId% print results have been approved. You can now print the postal label and send this order to the customer.  \n\nThank you.  \n\nPlease visit treatstock.com for details.'
            ]
        );

        $this->insert(
            'email_template',
            [
                'code'          => 'psPrintReject',
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => 'Order #%orderId% print rejected',
                'description'   => 'PS gets this message after moderator rejects print results',
                'updated_at'    => dbexpr('NOW()'),
                'template_html' => '<p>Order #%orderId% print results have been rejected. Please re-print required files. Thank you.</p><p>Visit: https://www.treatstock.com/my/printservice-order/print-requests/accepted</p>',
                'template_text' => "Order #%orderId% print results have been rejected: %reason%. Please re-print required files. Thank you. \n\n Visit: https://www.treatstock.com/my/printservice-order/print-requests/accepted"
            ]
        );


    }

    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'psPrintedReviewed', 'language_id' => 'en-US']);
        $this->delete('email_template', ['code' => 'psPrintReject', 'language_id' => 'en-US']);
    }
}
