<?php

use yii\db\Migration;

class m160513_095227_1860_new_email_templates extends Migration
{
    public function up()
    {

        $this->truncateTable("email_template");

        $this->execute("
INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(1,'register','system','en-US','Activate your Treatstock account ','user gets this email after registration to confirm email','2016-01-27 17:25:29','<p>Hi %username%,</p>\r\n<p>\r\n<p>You are almost finished with your Treatstock registration!</p>\r\nPlease confirm your account by clicking on the following link:  <br />\r\n<a href=\"%link%\">%link%</a>\r\n</p>\r\n<p>\r\nE-mail: %email% <br />\r\nUsername: %username% <br />\r\nPassword: %password% <br />\r\n</p>\r\n<p>\r\nBest regards,\r\nTreatstock Team</p>','Hi %username%,\r\n\r\nYou are almost finished with your Treatstock registration!\r\n\r\nPlease confirm your account by clicking on the following link:\r\n%link%\r\n\r\nE-mail: %email%\r\nUsername: %username%\r\nPassword: %password%\r\n\r\nBest regards,\r\nTreatstock Team');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(2,'register','system','ru-RU','Подтвердите электронную почту и регистрацию','','2015-06-18 07:14:35','<p>Уважаемый %username%, </p>\r\n<p>Кто-то, возможно вы, указали данный e-mail при регистрации в TS:</p>\r\n<p>\r\nE-mail: %email% <br />\r\nИмя пользователя: %username% <br />\r\nПароль: %password% <br />\r\n</p>\r\n\r\n<p>\r\nЕсли это вы, пожалуйста подтвердите e-mail, перейдя по ссылке: <br />\r\n<a href=\"%link%\">%link%</a>\r\n</p>','Уважаемый %username%,\r\n\r\nКто-то, возможно вы, указали данный e-mail при регистрации в TS:\r\n\r\nE-mail: %email%\r\nИмя пользователя: %username%\r\nПароль: %password%\r\n\r\nЕсли это вы, пожалуйста подтвердите e-mail, перейдя по ссылке:\r\n%link%');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(3,'confirmEmail','system','en-US','Confirm email','message with link to confirm email','2015-06-18 07:11:14','<p>Hello %username%,</p>\r\n\r\n<p>Follow the link below to confirm your email, if you requested this operation on TS:</p>\r\n<a href=\"%link%\">%link%</a>','Hello %username%,\r\n\r\nFollow the link below to confirm your email, if you requested this operation on TS:\r\n%link%');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(4,'confirmEmail','system','ru-RU','Подтвердите электронную почту','','2015-06-18 07:11:55','<p>Уважаемый %username%,</p>\r\n<p>\r\nКто-то, возможно вы, запросили подтвердить данный e-mail  в TS.\r\nЕсли это вы пожалуйста подтвердите e-mail, перейдя по ссылке:\r\n</p>\r\n<a href=\"%link%\">%link%</a>','Уважаемый %username%,\r\n\r\nКто-то, возможно вы, запросили подтвердить данный e-mail  в TS.\r\nЕсли это вы пожалуйста подтвердите e-mail, перейдя по ссылке\r\n%link%');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(5,'forgotPassword','system','en-US','Treatstock account password change request','password change request','2016-01-28 08:03:09','<div class=\"password-reset\">\r\n    <p>Hi %username%,</p>\r\n\r\n    <p>Please click on the link below to reset the password to your Treatstock account.</p>\r\n\r\n    <p><a href=\"%link%\">%link%</a></p>\r\n    <p>If clicking on the link doesn\'t work, just copy/paste the link into your browser. This will return you to the Treatstock website where we will guide you through resetting your password.</p>\r\n <p>Best regards,\r\nTreatstock Team</p>\r\n</div>\r\n','Hi %username%,\r\nPlease click on the link below to reset the password to your Treatstock account.\r\n%link%\r\n\r\nIf clicking on the link doesn\'t work, just copy/paste the link into your browser. This will return you to the Treatstock website where we will guide you through resetting your password.\r\n\r\nBest regards,\r\nTreatstock Team\r\n');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(6,'forgotPassword','system','ru-RU','Сброс пароля','','2015-06-16 15:55:53','<div class=\"password-reset\">\r\n<p>Привет %username%,</p>\r\n\r\n<p>Чтобы сбросить пароль и установить новый, нажмите ссылку:</p>\r\n\r\n    <p><a href=\"%link%\">%link%</a></p>\r\n</div>','Привет %username%,\r\n\r\nЧтобы сбросить пароль и установить новый, нажмите ссылку:\r\n\r\n%link%');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(7,'registerOsn','system','en-US','Registration complete','user gets this email after OSN registration','2016-01-28 08:03:21','<p>Hello %name%,</p>\r\n<p>\r\nYou registered on our website using a Social Network account. You can use the following credentials to login into your account. \r\n</p>\r\n<p>\r\nE-mail: %email% <br />\r\nUsername: %username% <br />\r\nPassword: %password% <br />\r\n</p>\r\n<p>\r\nBest regards,\r\nTreatstock Team</p>','Hello %name%,\r\n\r\nYou registered on our website using a Social Network account. You can use the following credentials to login into your account. \r\n\r\nE-mail: %email%\r\nUsername: %username%\r\nPassword: %password%\r\n\r\nBest regards,\r\nTreatstock Team');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(8,'changePassword','system','en-US','Password has been changed','this email sent after user changes his password in profile','2016-01-28 08:03:34','<p>Hi %username%,</p>\r\n<p>\r\nYour password has been changed. Your new credentials are: \r\n</p>\r\n<p> \r\nUsername: %username% <br />\r\nPassword: %password% <br />\r\n</p>\r\n<p>\r\nBest regards,\r\nTreatstock Team</p>','Hi %username%,\r\n\r\nYour password has been changed. Your new credentials are: \r\n\r\nUsername: %username%\r\nPassword: %password%\r\n\r\nBest regards,\r\nTreatstock Team');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(9,'deliveryWait','service','en-US','Waiting for delivery order status ','this email sent after some hours after clicked Printed button','2016-01-28 08:03:58','<p>Hello %username%,</p>\r\n<p>\r\nWe are inquiring  when printed order  #%orderId% is going to be sent.\r\n</p>\r\n<p>\r\nBest regards,\r\nTreatstock Team</p>','Hello %username%,\r\n\r\nWe are inquiring  when printed order  #%orderId% is going to be sent.\r\n\r\nBest regards,\r\nTreatstock Team');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(10,'createChangeEmailRequest','system','en-US','Request to update your e-mail','','2016-01-28 08:04:08','<p>Hello %username%</p>\r\n<p> Your account has requested a change of email from %oldEmail% to %newEmail%. </p>\r\n<p>If it was not you - contact support.</p>\r\n\r\n<p>Best regards,\r\nTreatstock Team</p>','Hello %username%! \r\nYour account has requested a change of email from %oldEmail% to %newEmail%. If it was not you - contact support.\r\n\r\nBest regards,\r\nTreatstock Team');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(11,'confirmChangeEmailRequest','system','en-US','Confirmation of e-mail update ','','2016-01-15 11:49:05','<p>Hello %username%,</p>\r\n\r\n<p>Follow the link below to confirm your email, if you have requested this operation on TS:</p>\r\n<a href=\"%link%\">%link%</a>','Hello %username%,\r\nFollow the link below to confirm your email, if you have requested this operation on TS:\r\n%link%');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(12,'psDeclineOrder','order','en-US','Your order has been declined by the Print Service','this email sent after PS decline order','2016-01-28 08:04:21','<p>Hello %username%,</p>\r\n            <p>\r\n                Print Service %ps_title% declined the order %order_id% by reason %reason% with comments %comments%\r\n            </p>\r\n            <p>\r\n                Best regards,\r\n                Treatstock Team</p>','Hello %username%,\r\n\r\nPrint Service %ps_title% declined the order %order_id% by reason %reason% with comments %comments%\r\n\r\nBest regards,\r\nTreatstock Team');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(13,'psCancelOrder','service','en-US','Order has been cancelled','this email sent after User cancel order','2016-01-28 08:04:32','<p>Hello %username%,</p>\r\n<p>\r\nYour order has been cancelled!\r\n</p>\r\n<p>\r\nThank you,\r\nTreatstock Team</p>','Hello %username%,\r\n\r\nYour order has been cancelled!\r\n\r\nThank you,\r\nTreatstock Team');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(14,'psOrderPrinted','order','en-US','Model has been printed','this email sent after PS marked order as printed','2016-01-28 08:04:42','<p>Hello %username%,</p>\r\n<p>\r\nThe model of the order № %order_id% printed by %ps_title%\r\n</p>\r\n<p>\r\nThank you,\r\nTreatstock Team</p>','Hello %username%,\r\n\r\nThe model of the order № %order_id% printed by %ps_title%\r\n\r\nThank you,\r\nTreatstock Team');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(15,'psOrderPrintedPickUp','order','en-US','Model is ready for pick-up/delivery','this email sent after PS marked order as printed and delivery type set as Pickup','2016-01-28 08:04:53','<p>Hello %username%,</p>\r\n<p>\r\nThe model of the order № %order_id% printed by %ps_title%\r\n</p>\r\n<p>\r\nYou can obtain the printed model here: %printer_address%. The working hours: %working_hours%\r\n</p>\r\n<p>\r\nThank you,\r\nTreatstock Team</p>','Hello %username%,\r\n\r\nThe model of the order № %order_id% printed by %ps_title%\r\nYou can obtain the printed model here: %printer_address%. The working hours: %working_hours%\r\n\r\nThank you,\r\nTreatstock Team');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(16,'cancelOrderPrintingTimeExpired','order','en-US','Order #%orderId% is cancelled.',NULL,NULL,'Dear %orderUsername%, the order #%orderId% with %psName% is cancelled because of printing time expired.','Dear %orderUsername%, the order #%orderId% with %psName% is cancelled because of printing time expired.');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(17,'psNewOrder','service','en-US','You have a new order #%order_id% on treatstock.com','New order to PS',NULL,'Hello! You have received a new order #%order_id% on treatstock.com \nYou have 24 hours to accept or decline the order : <a href=\"%newOrdersLink%\">%newOrdersLink%</a>\n\nBest regards,\nTreatstock','Hello! You have received a new order #%order_id% on treatstock.com.\nYou have 24 hours to accept or decline the order : %newOrdersLink%');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(18,'firstTaxMessage','system','en-US','Payment Card Reporting Requirements',NULL,NULL,'Hello from Treatstock,\n\nPlease provide your tax identity information within %days% days from the date of this e-mail. \nIf you have not provided the correct tax information within this time, Treatstock will deduct 30% from your earnings. \nIt is crucial to remember to submit all the necessary paperwork to avoid 30% deduction. \nAlso, in order to withdraw your earnings from Treatstock account, please login with your PayPal account.\n\nTo help protect the security of your tax identification information,\ndo not respond to this e-mail with your tax identity information or share it over the phone.\nEnter your tax identity information using this link https://www.treatstock.com/my/taxes\n\nLearn more about the regulations and Form 1099-K at the IRS website: http://www.irs.gov/uac/FAQs-on-New-Payment-Card-Reporting-Requirements\n\nBest regards,\nTreatstock team.','Hello from Treatstock,\n\nPlease provide your tax identity information within 30 days from the date of this e-mail. If you have not provided corrected tax identity information by that time, your Treatstock privileges will be suspended.\nTo help protect the security of your tax identification information, do not respond to this e-mail with your tax identity information or share it over the phone.\nEnter your tax identity information using this link https://www.treatstock.com/my/taxes\n\nLearn more about the regulations and Form 1099-K at the IRS website: http://www.irs.gov/uac/FAQs-on-New-Payment-Card-Reporting-Requirements\n\nBest regards,\nTreatstock team.');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(19,'supportMessage','system','en-US','%subject%',NULL,NULL,'%message%','%message%');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(20,'restoreUserRequest','system','en-US','Treatstock - Request to restore your account',NULL,NULL,'Hello, \nTo restore you account on Treatstock, please click: %link%','Hello, \nTo restore you account on Treatstock, please click: %link%');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(21,'declineTaxInfo','system','en-US','Tax information declined.','On moderator decline PS tax info',NULL,'Hello!\nYour tax information has been declined. The reason: %reason%.\nYou can change or update your tax form here: %taxLink%.\n\nBest regards,\nTreatstock','Hello!\nYour tax information has been declined. The reason: %reason%.\nYou can change or update your tax form here: %taxLink%.\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(23,'requestMoreTime','order','en-US','requestMoreTime','On PS request more time for printing model',NULL,'Hello %username%,\n\nUnfortunately, the print service has experienced a delay in printing your order #%orderId% due to … Your order will now be completed on %newPrintDate%. Feel free to contact <a href=\"%psContactLink%\">%psName%</a> through private message on Treatstock.\nBest regards,\nTreatstock','Hello %username%,\n\nUnfortunately, the print service has experienced a delay in printing your order #%orderId% due to … Your order will now be completed on %newPrintDate%. Feel free to contact %psName% through private message on Treatstock.\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(24,'needSetAsReceived','order','en-US','Please set order #%orderId% as received',NULL,NULL,'Hello %username%,\n\nAfter you receive the model(s) you have ordered, please press «set as printed» button in your profile here: <a href=\"%myOrdersLink%\">%myOrdersLink%</a>\nBest regards,\nTreatstock','Hello %username%,\n\nAfter you receive the model(s) you have ordered, please press «set as printed» button in your profile here: %myOrdersLink%\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(25,'modelAward','service','en-US','Your model Your model %modelname% on treatstock.com was printed on treatstock.com was printed',NULL,NULL,'Hello %username%!\nYour model %modelname% was successfully printed and delivered to the customer.\nYour fee will be transfered to your Treatstock account. You can then withdraw the payment to your PayPal account.\n\nBest regards,\nTreatstock','Hello %username%!\nYour model %modelname% was successfully printed and delivered to the customer.\nYour fee will be transfered to your Treatstock account. You can then withdraw the payment to your PayPal account.\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(26,'psNewOrder12Hour','service','en-US','You have a new order #%orderId% on treatstock.com',NULL,NULL,'Hello %psName%! \nYou have received a new order #%orderId%\nYou can accept it here: <a href=\"%newOrdersLink%\">%newOrdersLink%</a>. \nPlease keep in mind that in case you do not accept the order, it will be automatically cancelled in 12 hours.\n\nBest regards,\nTreatstock ',NULL);

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(27,'psNewOrder3Hour','service','en-US','You have a new order #%orderId% on treatstock.com',NULL,NULL,'Hello %psName%! \nYou have received a new order <order#> \nYou can accept or decline it here: <a href=\"%newOrdersLink%\">%newOrdersLink%</a>. Please keep in mind that there are just 3 hours left to accept or decline the order, after this period it will be automatically cancelled\n\nBest regards,\nTreatstock','Hello %psName%! \nYou have received a new order <order#> \nYou can accept or decline it here: %newOrdersLink%. Please keep in mind that there are just 3 hours left to accept or decline the order, after this period it will be automatically cancelled\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(28,'psCancelNewOrder','service','en-US','Cancellation of the order #%orderId% on treatstock.com',NULL,NULL,'Hello %psName%! \nYou haven’t accepted the order #%orderId%\nThe order has now been cancelled.\n\nBest regards,\nTreatstock','Hello %psName%! \nYou haven’t accepted the order #%orderId%\nThe order has now been cancelled.\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(29,'psAcceptOrder','service','en-US','Acceptance of the order #%orderId% on treatstock.com',NULL,NULL,'Hello %psName%!\nYou have accepted the order #%orderId%\nPlease keep in mind that you have 3 days to print it.\nIf you need more time for printing, you can prolong it in your profile in : <a href=\"%acceptOrdersLink%\">%acceptOrdersLink%</a>\n\nBest regards,\nTreatstock','Hello %psName%!\nYou have accepted the order #%orderId%\nPlease keep in mind that you have 3 days to print it.\nIf you need more time for printing, you can prolong it in your profile in : %acceptOrdersLink%\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(30,'ps2DayAfterAccept','service','en-US','The order #%orderId% on treatstock.com requires your attention',NULL,NULL,'Hello %psName%!\nYou have accepted the order #%orderId%\nA short reminder that you have 1 day left to print it.\nIf you need more time for printing, you can prolong it in your profile in <a href=\"%acceptedOrdersLink%\">%acceptedOrdersLink%</a>\n\nBest regards,\nTreatstock','Hello %psName%!\nYou have accepted the order #%orderId%\nA short reminder that you have 1 day left to print it.\nIf you need more time for printing, you can prolong it in your profile in %acceptedOrdersLink%\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(31,'psAcceptButCancel','service','en-US','The order #%orderId% on treatstock.com has been rejected',NULL,NULL,'Hello %psName%!\nYou have rejected the order #%orderId% you had accepted earlier.\nThe order has now been cancelled.\n\nBest regards,\nTreatstock\n\n',NULL);

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(32,'psClientCancelNewOrder','service','en-US','The order #%orderId% on treatstock.com has been rejected',NULL,NULL,'Hello %psName%!\nThe customer has rejected the order #%orderId% you had accepted.\nThe order has now been cancelled.\n\nBest regards,\nTreatstock','Hello %psName%!\nThe customer has rejected the order #%orderId% you had accepted.\nThe order has now been cancelled.\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(33,'psPrintedOrder','service','en-US','The order #%orderId% on treatstock.com is printed',NULL,NULL,'Hello %psName%!\nYou have printed the order #%orderId% on treatstock.com\nPlease keep in mind that you have 24 hours to send it to the customer.\nAlso do not forget to change the order status from «printed» to «sent» after you send it, otherwise the order will still be considered incomplete.\n\nBest regards,\nTreatstock','Hello %psName%!\nYou have printed the order #%orderId% on treatstock.com\nPlease keep in mind that you have 24 hours to send it to the customer.\nAlso do not forget to change the order status from «printed» to «sent» after you send it, otherwise the order will still be considered incomplete.\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(34,'ps19HoursAfterPrinted','service','en-US','The order #%orderId% on treatstock.com requires your attention',NULL,NULL,'Hello %psName%!\nYou have printed the order %orderId% on treatstock.com\nThis is a short reminder that you have 5 hours left to send it to the customer.\nPlease do not forget to change the order status from «printed» to «sent» after you send it, otherwise the order will still be considered incomplete.\n\nBest regards,\nTreatstock','Hello %psName%!\nYou have printed the order %orderId% on treatstock.com\nThis is a short reminder that you have 5 hours left to send it to the customer.\nPlease do not forget to change the order status from «printed» to «sent» after you send it, otherwise the order will still be considered incomplete.\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(35,'psSentOrder','service','en-US','Your order #%orderId% on treatstock.com is sent',NULL,NULL,NULL,'Hello %psName%!\nThank you for sending the order #%orderId%\nWe will let you know when the order has been delivered.\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(36,'psReceivedOrder','service','en-US','The order #%orderId% is delivered',NULL,NULL,'Hello %psName%!\nThe order #%orderId% printed by you has been delivered to the customer.\nThank you for your great work!\nThe payment for your printing service will be transferred to your Treatstock account. You can then withdraw the payment to your PayPal account.\n\nBest regards,\nTreatstock','Hello %psName%!\nThe order #%orderId% printed by you has been delivered to the customer.\nThank you for your great work!\nThe payment for your printing service will be transferred to your Treatstock account. You can then withdraw the payment to your PayPal account.\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(37,'clientNewOrder','order','en-US','Your order #%orderId% is now on treatstock.com',NULL,NULL,'Hello %clientName%! \nYou have made an order #%orderId% on treatstock.com\nWe will keep you informed about the progress of the order.\n\nBest regards,\nTreatstock','Hello %clientName%! \nYou have made an order #%orderId% on treatstock.com\nWe will keep you informed about the progress of the order.\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(38,'clientPrintStartOrder','order','en-US','Your order #%orderId% on treatstock.com is being printed',NULL,NULL,'Hello %clientName%!\nYour order #%orderId% is being printed.\nWe will keep you informed about the progress of the order.\n\nBest regards,\nTreatstock',NULL);

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(39,'clientPrintedOrder','order','en-US','Your order #%orderId% on treatstock.com is printed',NULL,NULL,'Hello %clientName%!\nYour order #%orderId% is currently being printed and will be posted to you shortly.\n\nBest regards,\nTreatstock',NULL);

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(40,'clientSentOrder','order','en-US','Your order on treatstock.com is sent',NULL,NULL,'Hello %clientName%!\nYour order #%orderId% has been sent and is currently in transit.\n\nBest regards,\nTreatstock','Hello %clientName%!\nYour order #%orderId% has been sent and is currently in transit.\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(41,'clientReceivedOrder','order','en-US','Delivery confirmation of your order #%orderId% on treatstock.com',NULL,NULL,'Hello %clientName%!\nThank you for choosing Treatstock!\nWe hope that you enjoyed our services.\nYou can find more 3D models for printing on treatstock.com\n\nBest regards,\nTreatstock','Hello %clientName%!\nThank you for choosing Treatstock!\nWe hope that you enjoyed our services.\nYou can find more 3D models for printing on treatstock.com\n\nBest regards,\nTreatstock');

INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(42,'clientPsAcceptButCancel','order','en-US','Your order #%orderId% on treatstock.com is rejected',NULL,NULL,'Hello %clientName%!\nThe Print Service %psName% has rejected the order #%orderId% you have made earlier.\nThe order has now been cancelled.\nYou can choose another Print Service here: <a href=\"%chooseAnotherPsLink%\">%chooseAnotherPsLink%</a>\n\nBest regards,\nTreatstock','Hello %clientName%!\nThe Print Service %psName% has rejected the order #%orderId% you have made earlier.\nThe order has now been cancelled.\nYou can choose another Print Service here: %chooseAnotherPsLink%\n\nBest regards,\nTreatstock');");

    }

    public function down()
    {
        echo "m160513_095227_1860_new_email_templates cannot be reverted.\n";

        return false;
    }
}
