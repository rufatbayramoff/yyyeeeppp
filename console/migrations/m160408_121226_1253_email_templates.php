<?php

use yii\db\Migration;

class m160408_121226_1253_email_templates extends Migration
{
    public function up()
    {
        $this->update('email_template', ['group' => 'service'], ['code' => 'deliveryWait']);
        $this->update('email_template', ['group' => 'order'], ['code' => 'psDeclineOrder']);
        $this->update('email_template', ['group' => 'service'], ['code' => 'psCancelOrder']);
        $this->update('email_template', ['group' => 'order'], ['code' => 'psOrderPrinted']);
        $this->update('email_template', ['group' => 'order'], ['code' => 'psOrderPrintedPickUp']);

        $this->execute("
        INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
        VALUES
            (16, 'cancelOrderPrintingTimeExpired', 'order', 'en-US', 'Order #%orderId% is cancelled.', NULL, NULL, 'Dear %orderUsername%, the order #%orderId% with %psName% is cancelled because of printing time expired.', 'Dear %orderUsername%, the order #%orderId% with %psName% is cancelled because of printing time expired.'),
            (17, 'psNewOrder', 'service', 'en-US', 'New Printing Order - Treatstock.com', NULL, NULL, 'Hello %username%, \nYour Print Service %ps_title%  has a new order pending in your Treatstock account.\nPlease go to https://www.treatstock.com to accept.\n\nBest regards,\nTreatstock', 'Hello %username%, \nYour Print Service %ps_title%  has a new order.'),
            (18, 'firstTaxMessage', 'system', 'en-US', 'Payment Card Reporting Requirements', NULL, NULL, 'Hello from Treatstock,\n\nPlease provide your tax identity information within %days% days from the date of this e-mail. \nIf you have not provided the correct tax information within this time, Treatstock will deduct 30% from your earnings. \nIt is crucial to remember to submit all the necessary paperwork to avoid 30% deduction. \nAlso, in order to withdraw your earnings from Treatstock account, please login with your PayPal account.\n\nTo help protect the security of your tax identification information,\ndo not respond to this e-mail with your tax identity information or share it over the phone.\nEnter your tax identity information using this link https://www.treatstock.com/my/taxes\n\nLearn more about the regulations and Form 1099-K at the IRS website: http://www.irs.gov/uac/FAQs-on-New-Payment-Card-Reporting-Requirements\n\nBest regards,\nTreatstock team.', 'Hello from Treatstock,\n\nPlease provide your tax identity information within 30 days from the date of this e-mail. If you have not provided corrected tax identity information by that time, your Treatstock privileges will be suspended.\nTo help protect the security of your tax identification information, do not respond to this e-mail with your tax identity information or share it over the phone.\nEnter your tax identity information using this link https://www.treatstock.com/my/taxes\n\nLearn more about the regulations and Form 1099-K at the IRS website: http://www.irs.gov/uac/FAQs-on-New-Payment-Card-Reporting-Requirements\n\nBest regards,\nTreatstock team.'),
            (19, 'supportMessage', 'system', 'en-US', '%subject%', NULL, NULL, '%message%', '%message%'),
            (20, 'restoreUserRequest', 'system', 'en-US', 'Treatstock - Request to restore your account', NULL, NULL, 'Hello, \nTo restore you account on Treatstock, please click: %link%', 'Hello, \nTo restore you account on Treatstock, please click: %link%');

        ");

    }

    public function down()
    {

        $this->delete('email_template', ['code' => 'cancelOrderPrintingTimeExpired']);
        $this->delete('email_template', ['code' => 'psNewOrder']);
        $this->delete('email_template', ['code' => 'firstTaxMessage']);
        $this->delete('email_template', ['code' => 'supportMessage']);
        $this->delete('email_template', ['code' => 'restoreUserRequest']);

        return true;
    }
}
