<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190320_101621_6418_user_profile_hide_public_page
 */
class m190320_101621_6418_user_profile_hide_public_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_profile', 'is_hidden_public_page', 'tinyint not null default 0');
    }

    public function safeDown()
    {
        $this->dropColumn('user_profile', 'is_hidden_public_page');
    }
}