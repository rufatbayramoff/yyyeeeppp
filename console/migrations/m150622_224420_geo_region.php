<?php

use yii\db\Schema;
use yii\db\Migration;

class m150622_224420_geo_region extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%geo_region}}', 'geoname_id', 'INTEGER(11) NOT NULL AFTER `title` ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%geo_region}}', 'geoname_id');
    }
}
