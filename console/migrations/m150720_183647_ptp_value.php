<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_183647_ptp_value extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('{{%printer_to_property}}', 'value', 'string(100) NULL');
    }

    public function safeDown()
    {
        $this->alterColumn('{{%printer_to_property}}', 'value', 'string(100) NOT NULL');
    }
}
