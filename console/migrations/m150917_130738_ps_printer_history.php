<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_130738_ps_printer_history extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("CREATE TABLE `ps_printer_history` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `ps_printer_id` int(11) NOT NULL,
          `user_id` int(11) DEFAULT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `action_id` varchar(45) NOT NULL,
          `comment` tinytext,
          PRIMARY KEY (`id`),
          KEY `fk_ps_printer_id_history` (`ps_printer_id`),
          KEY `fk_ps_printer_id_user_id` (`user_id`),
          CONSTRAINT `fk_ps_printer_history_1` FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
          CONSTRAINT `fk_ps_printer_history_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->truncateTable('ps_printer_history');
        $this->execute('ALTER TABLE `ps_printer_history` DROP FOREIGN KEY `fk_ps_printer_history_1`;');
        $this->execute('ALTER TABLE `ps_printer_history` DROP FOREIGN KEY `fk_ps_printer_history_2`;');
        $this->dropTable('ps_printer_history');
    }
}
