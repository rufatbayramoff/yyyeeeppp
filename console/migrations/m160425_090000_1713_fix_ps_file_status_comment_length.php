<?php

use yii\db\Migration;

class m160425_090000_1713_fix_ps_file_status_comment_length extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer_file_status` CHANGE `details` `details` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT NULL;");

    }

    public function down()
    {
        $this->execute("ALTER TABLE `ps_printer_file_status` CHANGE `details` `details` VARCHAR(100)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT NULL;");
        return true;
    }
}
