<?php

use yii\db\Schema;
use yii\db\Migration;

class m151125_112359_fix_min_order_price extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer` CHANGE `min_order_price` `min_order_price` DECIMAL(10,2)  NULL  DEFAULT NULL;");
    }

    public function down()
    {
    }
}
