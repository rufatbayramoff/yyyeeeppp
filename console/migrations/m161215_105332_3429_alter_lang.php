<?php

use yii\db\Migration;

class m161215_105332_3429_alter_lang extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `system_lang_source`
	ADD COLUMN `is_checked` TINYINT(1) NOT NULL DEFAULT \'0\' AFTER `not_found_in_source`;');

        $this->execute('UPDATE system_lang_source SET is_checked=1 WHERE id < 3000');
        $this->execute('UPDATE system_lang_source SET is_checked=0 WHERE id > 3000');

        $this->execute('ALTER TABLE `system_lang_message` ADD INDEX `index2` (`is_auto_translate`);');
    }

    public function down()
    {
        $this->dropColumn('system_lang_source', 'is_checked');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
