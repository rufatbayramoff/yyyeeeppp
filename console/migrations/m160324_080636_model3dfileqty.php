<?php

use yii\db\Migration;

class m160324_080636_model3dfileqty extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `model3d_file` 
                ADD COLUMN `qty` TINYINT NOT NULL DEFAULT 1 AFTER `rotated_z`;");
        $this->execute("ALTER TABLE `model3d` 
                        ADD COLUMN `model_units` CHAR(4) NOT NULL DEFAULT 'mm' AFTER `stat_views`;");      
        
        $this->execute("ALTER TABLE `model3d` 
                    ADD COLUMN `category_id` INT(11) NULL AFTER `model_units`;");
        
        $this->execute("ALTER TABLE `model3d` 
                ADD INDEX `fk_model3d_2_idx` (`category_id` ASC);
                ALTER TABLE `model3d` 
                ADD CONSTRAINT `fk_model3d_2`
                  FOREIGN KEY (`category_id`)
                  REFERENCES `store_category` (`id`)
                  ON DELETE NO ACTION
                  ON UPDATE NO ACTION;
        ");
    }

    public function down()
    {
        $this->dropColumn('model3d_file', 'qty');
        $this->dropColumn('model3d', 'model_units');
        
        $this->dropForeignKey('fk_model3d_2', 'model3d');
        $this->dropIndex('fk_model3d_2_idx', 'model3d');
        $this->dropColumn("model3d", "category_id");
    }
}
