<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200221_110121_7105_wiki_materials_widget
 */
class m200221_110121_7105_wiki_materials_widget extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('wiki_material' ,'widget_about', 'text');
        $this->addColumn('wiki_material' ,'widget_advantages', 'text');
        $this->addColumn('wiki_material' ,'widget_disadvantages', 'text');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropColumn('wiki_material' ,'widget_about');
        $this->dropColumn('wiki_material' ,'widget_advantages');
        $this->dropColumn('wiki_material' ,'widget_disadvantages');
    }
}