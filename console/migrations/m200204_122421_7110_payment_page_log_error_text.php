<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m200204_122421_7110_payment_page_log_error_text
 */
class m200204_122421_7110_payment_page_log_error_text extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_pay_page_log', 'process_message', 'varchar(1024)');
    }

    public function safeDown()
    {
        $this->dropColumn('payment_pay_page_log', 'process_message');
    }
}