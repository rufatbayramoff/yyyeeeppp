<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190605_145921_6578_user_address_last_name
 */
class m190605_145921_6578_user_address_last_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_address', 'first_name' ,'varchar(250) after user_id');
        $this->addColumn('user_address', 'last_name' ,'varchar(250) after first_name');
        $this->alterColumn('user_address', 'contact_name', 'varchar(145)');
        $this->renameColumn('user_address', 'contact_name', 'common_contact_name');
    }

    public function safeDown()
    {
        $this->dropColumn('user_address', 'first_name');
        $this->dropColumn('user_address', 'last_name');
        $this->renameColumn('user_address', 'common_contact_name', 'contact_name');
    }
}