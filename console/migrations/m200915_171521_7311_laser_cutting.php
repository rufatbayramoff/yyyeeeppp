<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\modules\translation\components\DbI18n;
use common\modules\translation\components\Statistics;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200915_171521_7311_laser_cutting
 */
class m200915_171521_7311_laser_cutting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE cutting_pack_file DROP FOREIGN KEY fk_cutting_pack_file_svg_file_uuid;');
        $this->dropColumn('cutting_pack_file', 'svg_file_uuid');
        $this->dropColumn('cutting_pack_file', 'selections');

        $this->execute('CREATE TABLE `cutting_pack_page` (
  `uuid` varchar(32) NOT NULL,
  `cutting_pack_file_uuid` varchar(32) NOT NULL,
  `file_uuid` varchar(32) NOT NULL,
  `selections` longblob,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `file_uuid` (`file_uuid`) USING BTREE,
  KEY `cutting_pack_file_uuid` (`cutting_pack_file_uuid`),
  CONSTRAINT `fk_cutting_pack_fiage_file_uuid` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`),
  CONSTRAINT `fk_cutting_pack_page_cut_file_uuid` FOREIGN KEY (`cutting_pack_file_uuid`) REFERENCES `cutting_pack_file` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');
        $this->delete('cutting_pack_part');
        $this->execute('ALTER TABLE `cutting_pack_part` CHANGE `cutting_pack_file_uuid` `cutting_pack_page_uuid` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;');
        $this->execute('ALTER TABLE `cutting_pack_part` DROP FOREIGN KEY `fk_cutting_pack_file_id`;');
        $this->execute('ALTER TABLE `cutting_pack_part` ADD CONSTRAINT `fk_cutting_pack_page_uuid` FOREIGN KEY (`cutting_pack_page_uuid`) REFERENCES `cutting_pack_page`(`uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT;');
        $this->execute('ALTER TABLE `cutting_pack_file` ADD `is_converted` TINYINT(1) NOT NULL DEFAULT \'0\' AFTER `file_uuid`;');

        $this->execute('CREATE TABLE `cutting_pack_page_image` (
  `uuid` varchar(32) NOT NULL,
  `cutting_pack_page_uuid` varchar(32) NOT NULL,
  `file_uuid` varchar(32) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `cutting_pack_page_uuid` (`cutting_pack_page_uuid`),
  KEY `file_uuid` (`file_uuid`),
  CONSTRAINT `fk_cutting_page_image_file` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`),
  CONSTRAINT `fk_cutting_page_image_pack` FOREIGN KEY (`cutting_pack_page_uuid`) REFERENCES `cutting_pack_page` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        return false;
    }
}
