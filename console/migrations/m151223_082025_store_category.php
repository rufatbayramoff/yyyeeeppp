<?php

use yii\db\Schema;
use yii\db\Migration;

class m151223_082025_store_category extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `store_category` ADD COLUMN `total_count` INT(11) NOT NULL DEFAULT '0' AFTER `img`;");
    }

    public function down()
    {
        $this->dropColumn('store_category', 'total_count');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
