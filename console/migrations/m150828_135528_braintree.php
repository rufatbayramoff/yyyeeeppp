<?php
use yii\db\Schema;
use yii\db\Migration;

class m150828_135528_braintree extends Migration
{

    public function safeUp()
    {
        $this->execute("CREATE TABLE `paymentbt_customer` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) DEFAULT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NULL DEFAULT NULL,
            `firstname` varchar(45) DEFAULT NULL,
            `lastname` varchar(45) DEFAULT NULL,
            `company` varchar(45) DEFAULT NULL,
            `email` varchar(45) DEFAULT NULL,
            `phone` varchar(45) DEFAULT NULL,
            `fax` varchar(45) DEFAULT NULL,
            `customer_id` char(15) DEFAULT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `user_id_UNIQUE` (`user_id`),
            UNIQUE KEY `customer_id_UNIQUE` (`customer_id`),
            CONSTRAINT `fk_paymentbt_customer_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $this->execute("CREATE TABLE `paymentbt_method` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `customer_id` int(11) NOT NULL,
            `payment_nonce` text NOT NULL,
            `token` varchar(55) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `token_UNIQUE` (`token`),
            KEY `fk_paymentbt_method_1_idx` (`customer_id`),
            CONSTRAINT `fk_paymentbt_method_1` FOREIGN KEY (`customer_id`) REFERENCES `paymentbt_customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $this->execute("CREATE TABLE `paymentbt_address` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `address_id` char(5) NOT NULL,
            `customer_id` int(11) DEFAULT NULL,
            `firstname` varchar(45) NOT NULL DEFAULT '',
            `lastname` varchar(45) NOT NULL DEFAULT '',
            `company` varchar(45) NOT NULL DEFAULT '',
            `street_address` varchar(45) NOT NULL DEFAULT '',
            `extended_address` varchar(45) NOT NULL DEFAULT '',
            `city` varchar(45) NOT NULL DEFAULT '',
            `region` varchar(45) NOT NULL DEFAULT '',
            `postal_code` varchar(45) NOT NULL DEFAULT '',
            `country` varchar(45) NOT NULL DEFAULT '',
            PRIMARY KEY (`id`),
            UNIQUE KEY `address_id_UNIQUE` (`address_id`),
            KEY `fk_paymentbt_address_1_idx` (`customer_id`),
            CONSTRAINT `fk_paymentbt_address_1` FOREIGN KEY (`customer_id`) REFERENCES `paymentbt_customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        $this->execute("CREATE TABLE `paymentbt_transaction` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `transaction_id` varchar(45) DEFAULT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
            `status` varchar(45) NOT NULL,
            `type` varchar(45) DEFAULT NULL,
            `currency` char(5) NOT NULL DEFAULT 'USD',
            `amount` varchar(45) NOT NULL,
            `merchant_acount_id` varchar(45) DEFAULT NULL,
            `store_unit_id` int(11) NOT NULL,
            `channel` varchar(45) DEFAULT NULL,
            `full_response` text,
            `success` tinyint(1) NOT NULL DEFAULT '0',
            `customer_id` int(11) DEFAULT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `transaction_id_UNIQUE` (`transaction_id`),
            KEY `fk_paymentbt_transaction_1_idx` (`customer_id`),
            CONSTRAINT `fk_paymentbt_transaction_1` FOREIGN KEY (`customer_id`) REFERENCES `paymentbt_customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->truncateTable('paymentbt_transaction');
        $this->truncateTable('paymentbt_address');
        $this->truncateTable('paymentbt_method');
        $this->truncateTable('paymentbt_customer');

        $this->dropTable('paymentbt_transaction');
        $this->dropTable('paymentbt_address');
        $this->dropTable('paymentbt_method');
        $this->dropTable('paymentbt_customer');
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
