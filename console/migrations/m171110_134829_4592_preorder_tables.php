<?php

use yii\db\Migration;

class m171110_134829_4592_preorder_tables extends Migration
{
    public function safeUp()
    {
        $this->execute("
          CREATE TABLE `preorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('new','accepted','rejected') NOT NULL DEFAULT 'new',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ps_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `message` text NOT NULL,
  `budget` int(10) unsigned DEFAULT NULL,
  `estimate_time` int(10) unsigned DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `decline_reason` varchar(255) DEFAULT NULL,
  `decline_comment` varchar(255) DEFAULT NULL,
  `decline_initiator` tinyint(4) DEFAULT NULL,
  `offer_description` varchar(255) NOT NULL,
  `offer_estimate_time` int(11) unsigned DEFAULT NULL,
  `offered` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_preorder_ps_idx` (`ps_id`) USING BTREE,
  KEY `fk_preorder_user_idx` (`user_id`) USING BTREE,
  CONSTRAINT `fk_preorder_ps` FOREIGN KEY (`ps_id`) REFERENCES `ps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_preorder_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

        ");



        $this->execute("
CREATE TABLE `preorder_file` (
  `preorder_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`preorder_id`,`file_id`),
  KEY `fk_preorder_file_file_idx` (`file_id`) USING BTREE,
  CONSTRAINT `fk_preorder_file_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_preorder_file_preorder` FOREIGN KEY (`preorder_id`) REFERENCES `preorder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        ");



        $this->execute("
CREATE TABLE `preorder_work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preorder_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_preorder_work_preorder_idx` (`preorder_id`) USING BTREE,
  CONSTRAINT `fk_preorder_work_preorder` FOREIGN KEY (`preorder_id`) REFERENCES `preorder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

        ");


        $this->execute("
CREATE TABLE `preorder_work_file` (
  `work_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`work_id`,`file_id`),
  KEY `fk_preorder_work_file_file_idx` (`file_id`) USING BTREE,
  CONSTRAINT `fk_preorder_work_file_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_preorder_work_file_preorder_work` FOREIGN KEY (`work_id`) REFERENCES `preorder_work` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        ");

    }

    public function safeDown()
    {
        echo "m171110_134830_4592_ordr_preorder_link cannot be reverted.\n";

        return false;
    }
}
