<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Handles the creation of table `{{%product_main_category}}`.
 */
class m210907_100950_8638_create_product_main_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_main_category}}', [
            'id'                  => $this->primaryKey(),
            'product_category_id' => $this->integer()->unique()->null(),
        ]);

        $this->addColumn('product_category', 'file_uuid', $this->string());

        $this->createIndex(
            'idx-product_category_file_uuid',
            'product_category',
            'file_uuid'
        );

        $this->addForeignKey(
            'fk-product_category_file_uuid',
            'product_category',
            'file_uuid',
            'file_admin',
            'uuid'
        );

        $this->createIndex(
            'idx-product_main_category_product_category_id',
            'product_main_category',
            'product_category_id'
        );

        $this->addForeignKey(
            'fk-product_main_category_product_category_id',
            'product_main_category',
            'product_category_id',
            'product_category',
            'id'
        );

        $this->execute("INSERT IGNORE INTO `product_category` 
        (`code`, `title`, `is_active`, `parent_id`, `total_count`, `description`, `full_description`, `need_premoderation`, `position`, `updated_at`) 
        VALUES ('machinery', 'Machinery', '1', '1', '0', '', '', '0', '1', '2021-09-27 10:10:10');");

        $this->batchInsert('product_main_category', [
            'id'
        ], [
            [1],
            [2],
            [3],
            [4],
            [5]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_category', 'file_uuid');
        $this->dropTable('{{%product_main_category}}');
    }
}
