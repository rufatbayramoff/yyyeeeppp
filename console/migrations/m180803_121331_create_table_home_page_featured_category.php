<?php

use yii\db\Migration;

/**
 * Class m180803_121331_create_table_home_page_featured_category
 */
class m180803_121331_create_table_home_page_featured_category extends Migration
{
    public function safeUp()
    {
        $this->createTable('home_page_featured', [
            'id'                  => $this->primaryKey(),
            'created_at'          => $this->dateTime()->notNull(),
            'is_active'           => $this->tinyInteger(1)->defaultValue(1)->notNull(),
            'product_category_id' => $this->integer()->null(),
            'position'            => $this->integer()->notNull()->defaultValue(1)
        ]);

        $this->createIndex('index_hpf_product_category_id', 'home_page_featured', 'product_category_id');
        $this->addForeignKey('fk_hpf_product_category_id', 'home_page_featured', 'product_category_id', 'product_category', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('home_page_featured_product', [
            'featured_category_id' => $this->integer()->notNull(),
            'product_uuid'         => $this->string(32)->notNull(),
        ]);
//
        $this->createIndex('index_hpfp_featured_category_id', 'home_page_featured_product', 'featured_category_id');
        $this->createIndex('index_hpfp_product_uuid', 'home_page_featured_product', 'product_uuid');
//
        $this->addForeignKey('fk_hpfp_product_category_id', 'home_page_featured_product', 'featured_category_id', 'home_page_featured', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_hpfp_product_uuid', 'home_page_featured_product', 'product_uuid', 'product', 'uuid', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('home_page_featured_product');
        $this->dropTable('home_page_featured');
    }
}
