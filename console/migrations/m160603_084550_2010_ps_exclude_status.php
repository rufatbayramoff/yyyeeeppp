<?php

use yii\db\Migration;

class m160603_084550_2010_ps_exclude_status extends Migration
{
    public function up()
    {
        $this->addColumn('ps', 'is_excluded_from_printing', \yii\db\Schema::TYPE_SMALLINT.' UNSIGNED DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('ps', 'is_excluded_from_printing');
        return true;
    }
}
