<?php

use common\interfaces\Model3dBaseImgInterface;
use yii\db\Migration;
use yii\db\Query;

class m160926_174921_2959_fill_density_material_group extends Migration
{
    public function up()
    {
        $printerMaterials = (new Query())
            ->select('*')
            ->from('printer_material')->all();

        foreach ($printerMaterials as $printerMaterial) {
            if ($printerMaterial['group_id']) {
                $this->update(
                    'printer_material_group',
                    [
                        'density' => $printerMaterial['density']
                    ],
                    'id=' . $printerMaterial['group_id']
                );
            }
        }

    }

    public function down()
    {
    }
}
