<?php

use yii\db\Migration;

class m160805_080003_category_cosplay extends Migration
{
    public function up()
    {
        $this->execute("INSERT IGNORE INTO `store_category` (`id`, `custom_code_id`, `title`, `is_active`) VALUES ('23', '1', 'Mockups', '1');");
        $this->execute("INSERT IGNORE INTO `store_category` (`id`, `custom_code_id`, `title`, `is_active`) VALUES ('24', '1', 'Cosplay', '1');");
    }

    public function down()
    {
        $this->execute('DELETE FROM store_category WHERE id IN (23,24)');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
