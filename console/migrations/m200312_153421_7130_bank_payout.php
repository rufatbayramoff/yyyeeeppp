<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200312_153421_7130_bank_payout
 */
class m200312_153421_7130_bank_payout extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `payment_transaction` ADD `details` JSON NULL DEFAULT NULL AFTER `currency`;');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}