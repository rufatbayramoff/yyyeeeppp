<?php

use yii\db\Migration;

/**
 * Class m211006_103953_8930_pla_materail
 */
class m211006_103953_8930_pla_materail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('printer_material', ['filament_title' => 'pla'], ['filament_title'=>'PLA']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

}
