<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_072445_phone_code extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%geo_country}}', 'phone_code', 'string(10) NOT NULL AFTER `iso_code` ');

        $this->execute('SET FOREIGN_KEY_CHECKS=0');

        $this->truncateTable('{{%geo_country}}');

        $path = realpath(dirname(__DIR__) . '/../db/');
        $sql = file_get_contents($path . '/country_phone_codes.sql');
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function safeDown()
    {
        $this->dropColumn('{{%geo_country}}', 'phone_code');
    }
}
