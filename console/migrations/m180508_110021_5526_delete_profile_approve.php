<?php

use yii\db\Migration;

/**
 * Class m180508_110021_5526_delete_profile_approve
 */
class m180508_110021_5526_delete_profile_approve extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
CREATE TABLE `user_profile_delete_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `approve_code` varchar(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_profile_id` (`user_id`),
  CONSTRAINT `fk_user_profile_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        $this->insert('email_template', [
            'code'          => 'system.user_profile.deleteApprove',
            'group'         => 'system',
            'language_id'   => 'en-US',
            'title'         => 'Confirm delete your Treatstock account',
            'description'   => 'The user receives this email after registration to...',
            'updated_at'    => '2018-01-30 08:24:30',
            'template_html' => 'Hi %username%,
If you sure in account deletion. Please confirm it by link: %approveUrl%. Or using delete approve code : %approveCode%. 
',
            'is_active'     => 1
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_profile_delete_request');
    }
}
