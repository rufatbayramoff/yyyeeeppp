<?php

use yii\db\Migration;

/**
 * Class m181108_141529_5814_add_bank_payment_account
 */
class m181108_141529_5814_add_bank_payment_account extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('user', [
            'id' => 161,
            'uid' => \common\components\UuidHelper::generateRandCode(6),
            'username' => 'banktransfer',
            'auth_key' => '-',
            'password_hash' => '-',
            'email' => 'banktransfer@treatstock.com',
            'status' => '0',
            'created_at' => '0',
            'updated_at' => '0',
            'lastlogin_at' => '0',
            'trustlevel' => 'high',
        ]);

        $this->insert('payment_account', [
            'id' => 161,
            'user_id' => 161,
            'type' => 'main'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('payment_account', [
            'id' => 161
        ]);

        $this->delete('user', [
            'id' => 161
        ]);
    }
}
