<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_133904_ps_printer_is_deleted extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer` ADD `is_deleted` TINYINT(1) UNSIGNED  NOT NULL  DEFAULT '0';");
    }

    public function down()
    {
        $this->execute('ALTER TABLE `ps_printer` DROP `is_deleted`;');
        return true;
    }
}
