<?php

use yii\db\Migration;

/**
 * Class m210909_171349_disable_customer_pickup
 */
class m210909_171349_8881_disable_customer_pickup extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('delivery_type', ['is_active' => 0], ['id' => 1]);
        $sql = <<<EOD
            select
                pp.id
            from
                ps_machine_delivery pmd
            inner join company_service cs on cs.id = pmd.ps_machine_id 	
            inner join ps_printer pp on pp.id = cs.ps_printer_id 
            where
                pp.moderator_status = 'checked' and
                pp.user_status = 'active' and
                pmd.ps_machine_id not in (
                select
                    pmd.ps_machine_id
                from
                    ps_machine_delivery pmd
                where
                    pmd.delivery_type_id > 1)
        EOD;
        $ids = Yii::$app->db->createCommand($sql)
            ->queryColumn();
        $this->update('ps_printer',['moderator_status' => 'new'],['id' => $ids]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update('delivery_type', ['is_active' => 1], ['id' => 1]);
    }
}
