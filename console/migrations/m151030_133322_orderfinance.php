<?php

use yii\db\Schema;
use yii\db\Migration;

class m151030_133322_orderfinance extends Migration
{
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        try{
            $this->insert('user', [
                'id' => 109,
                'username' => 'cron',
                'auth_key'=>'', 
                'password_hash'=>'', 
                'email'=>'treatstock@treatstock.com', 
                'status'=> 0, 
                'created_at' =>0,
                'updated_at' => 0,
                'lastlogin_at'=>0,
                'trustlevel' => 'high'
            ]);
            $this->insert('user', [
                    'id'=> 110, 
                    'username'=>'treatstock', 
                    'auth_key'=>'', 
                    'password_hash'=>'', 
                    'email'=>'ts@treatstock.com', 
                    'status'=> 0, 
                    'created_at' =>0,
                    'updated_at' => 0,
                    'lastlogin_at'=>0,
                    'trustlevel' => 'high'
                ]
            );
            $this->insert('user', [
                    'id'=> 111, 
                    'username'=>'tsbank', 
                    'auth_key'=>'', 
                    'password_hash'=>'', 
                    'email'=>'tsbank@treatstock.com', 
                    'status'=> 0, 
                    'created_at' =>0,
                    'updated_at' => 0,
                    'lastlogin_at'=>0,
                    'trustlevel' => 'high'
                ]
            );
            

            $this->insert('user', [
                    'id'=> 120, 
                    'username'=>'easypost', 
                    'auth_key'=>'', 
                    'password_hash'=>'', 
                    'email'=>'easypost@treatstock.com', 
                    'status'=> 0, 
                    'created_at' =>0,
                    'updated_at' => 0,
                    'lastlogin_at'=>0,
                    'trustlevel' => 'high'
                ]
            );
        }catch(\Exception $e){
            echo $e->getMessage();
            echo "\n\nERROR\nDelete users with id from 9 to 1000\n\n\n"; exit;
        }
        $this->execute("ALTER TABLE `store_order`    ADD COLUMN `order_state` CHAR(15) NOT NULL DEFAULT 'new' AFTER `payment_status`;");

        $this->insert('user', [
            'id'=> 130, 
            'username'=>'taxagent', 
            'auth_key'=>'', 
            'password_hash'=>'', 
            'email'=>'taxagent@treatstock.com', 
            'status'=> 0, 
            'created_at' =>0,
            'updated_at' => 0,
            'lastlogin_at'=>0,
            'trustlevel' => 'high'
        ]);
        
        $this->insert("user", [
            "id" => 150,
            "username" => "paypal",
            "auth_key" => "",
            'password_hash' => '',
            'email' => 'paypal@treatstock.com',
            'status' => 0,
            'created_at' => 0,
            'updated_at' => 0,
            'lastlogin_at' => 0,
            'trustlevel' => 'high'
        ]);
        $this->insert("user", [
            "id" => 160,
            "username" => "braintree",
            "auth_key" => "",
            'password_hash' => '',
            'email' => 'braintree@treatstock.com',
            'status' => 0,
            'created_at' => 0,
            'updated_at' => 0,
            'lastlogin_at' => 0,
            'trustlevel' => 'high'
        ]);
        $this->insert("user", [
            "id" => 170,
            "username" => "diax",
            "auth_key" => "",
            'password_hash' => '',
            'email' => 'diax@treatstock.com',
            'status' => 0,
            'created_at' => 0,
            'updated_at' => 0,
            'lastlogin_at' => 0,
            'trustlevel' => 'high'
        ]);
        
         $this->insert("user", [
            "id" => 190,
            "username" => "exchanger", // for exhange rates
            "auth_key" => "",
            'password_hash' => '',
            'email' => 'exchanger@treatstock.com',
            'status' => 0,
            'created_at' => 0,
            'updated_at' => 0,
            'lastlogin_at' => 0,
            'trustlevel' => 'high'
        ]);
        $this->insert('user', [
                'id'=>999, 
                'username'=>'test', 
                'auth_key'=>'', 
                'password_hash'=>'', 
                'email'=>'tester@treatstock.com', 
                'status'=>1, 
                'created_at' =>0,
                'updated_at' => 0,
                'lastlogin_at'=>0,
                'trustlevel' => 'high'
            ]
        );
    }

    public function safeDown()
    {
        $this->dropColumn('store_order', 'order_state');
        $this->delete('user', 'id IN(999, 70,60,50,40,30,20,10,9)');
    }
    
}
