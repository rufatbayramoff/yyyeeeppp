<?php

use yii\db\Migration;

class m160730_105721_1644_rename_model3dColor_texture extends Migration
{
    public function up()
    {
        $this->renameTable('model3d_color', 'model3d_texture');
    }

    public function down()
    {
        $this->renameTable('model3d_texture', 'model3d_color');
    }
}
