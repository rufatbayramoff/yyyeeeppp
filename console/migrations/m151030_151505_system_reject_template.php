<?php

use yii\db\Schema;
use yii\db\Migration;

class m151030_151505_system_reject_template extends Migration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('print_model_fail','Model printed with defects','',1);");
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('print_model_fail','Model printed partially (filament finished)','',1);");
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('print_model_fail','Model printed partially (connection lost)','',1);");
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('print_model_fail','Model not printed (connection lost)','',1);");
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('print_model_fail','Model not printed (other reason)','',1);");
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM `system_reject` WHERE `group` = 'print_model_fail'");
    }
}
