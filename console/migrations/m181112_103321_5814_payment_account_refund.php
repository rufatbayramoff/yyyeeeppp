<?php

use yii\db\Migration;

/**
 * Class m181112_103321_5814_payment_account_refund
 */
class m181112_103321_5814_payment_account_refund extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `payment_account` CHANGE `type` `type` ENUM('main','authorize','reserved','award','tax','invoice','manufactureAward','refundRequest','discount','easypost', 'refund') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'main';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
