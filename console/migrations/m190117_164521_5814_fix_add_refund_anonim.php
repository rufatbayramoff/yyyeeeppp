<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190117_164521_5814_fix_add_refund_anonim
 */
class m190117_164521_5814_fix_add_refund_anonim extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('payment_account', ['id'=>905, 'user_id'=>998, 'type'=>'refund']);
    }

    public function safeDown()
    {
        $this->delete('payment_account', ['id'=>905]);
    }
}