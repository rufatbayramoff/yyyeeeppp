<?php

use yii\db\Migration;

class m201214_073633_7094_add_uniq_order_id_column_to_store_order_delivery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx_unique_order_id','store_order_delivery','order_id',true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_unique_order_id','store_order_delivery');
    }
}
