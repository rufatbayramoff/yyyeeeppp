<?php

use yii\db\Migration;

/**
 * Class m180920_112325_review_file_add_new_field
 */
class m180920_112325_review_file_add_new_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_review_file', 'created_at', 'timestamp default CURRENT_TIMESTAMP not null');
        $this->addColumn('store_order_review_file', 'sort_index', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('store_order_review_file', 'created_at');
        $this->dropColumn('store_order_review_file', 'sort_index');
    }
}
