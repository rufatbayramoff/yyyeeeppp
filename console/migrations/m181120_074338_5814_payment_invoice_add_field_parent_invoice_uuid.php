<?php

use yii\db\Migration;

/**
 * Class m181120_074338_5814_payment_invoice_add_field_parent_invoice_uuid
 */
class m181120_074338_5814_payment_invoice_add_field_parent_invoice_uuid extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_invoice', 'parent_invoice_uuid', $this->string(6)->null());
        $this->createIndex('idx_pi_parent_invoice_uuid', 'payment_invoice', 'parent_invoice_uuid');
        $this->addForeignKey('fk_pi_parent_invoice_uuid', 'payment_invoice', 'parent_invoice_uuid', 'payment_invoice', 'uuid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_pi_parent_invoice_uuid', 'payment_invoice');
        $this->dropIndex('idx_pi_parent_invoice_uuid', 'payment_invoice');
        $this->dropColumn('payment_invoice', 'parent_invoice_uuid');
    }
}
