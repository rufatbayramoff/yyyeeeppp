<?php

use yii\db\Migration;

/**
 * Class m210311_091116_8342_message_shadow_ban
 */
class m210311_091116_8342_message_shadow_ban extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'shadow_message_ban_from', 'datetime NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
