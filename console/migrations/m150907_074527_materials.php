<?php

use yii\db\Schema;
use yii\db\Migration;
/**
 * @author Nabi Ibatulin
 */
class m150907_074527_materials extends Migration
{
    public function safeUp()
    {
        try{
            
            $this->execute("SET FOREIGN_KEY_CHECKS=0");
            
            $this->truncateTable('printer_color');
             
            // create group table
            $this->execute("CREATE TABLE `printer_material_group` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(45) NOT NULL,
                `is_active` tinyint(1) NOT NULL DEFAULT '1',
                PRIMARY KEY (`id`),
                UNIQUE KEY `title_UNIQUE` (`title`)
              ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
            ");

            $this->execute("CREATE TABLE `printer_material_group_intl` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(145) DEFAULT NULL,
                `lang_iso` char(5) NOT NULL,
                `group_id` int(11) NOT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `lang_iso_UNIQUE` (`lang_iso`,`group_id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ");
            // add group_id column to material
            $this->execute("ALTER TABLE `printer_material`  ADD COLUMN `group_id` INT(11) NULL AFTER `is_active`;");        
            $this->execute("ALTER TABLE`printer_material` 
            ADD INDEX `fk_printer_material_1_idx` (`group_id` ASC);
            ALTER TABLE `printer_material` 
            ADD CONSTRAINT `fk_printer_material_1`
              FOREIGN KEY (`group_id`)
              REFERENCES `printer_material_group` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;");

            // add filament title for PS to show
            $this->execute("ALTER TABLE `printer_material`  ADD COLUMN `filament_title` VARCHAR(145) NULL AFTER `group_id`");
            $this->execute("ALTER TABLE `printer_material`  ADD COLUMN `density` DECIMAL(5,2) NULL AFTER `filament_title`");

            $this->execute("ALTER TABLE `printer_material` 
            CHANGE COLUMN `is_active` `is_active` TINYINT(1) NOT NULL ,
            CHANGE COLUMN `filament_title` `filament_title` VARCHAR(145) NOT NULL ,
            CHANGE COLUMN `density` `density` DECIMAL(5,2) NOT NULL DEFAULT 1 ;
            ");

            $data = [
                ['1', 'Plastic'],
                ['2', 'Metal'],
                ['3', 'Composite']
            ];
            foreach($data as $vals){
                $keys = ['id', 'title'];
                $row = array_combine($keys, $vals);
                $this->insert('printer_material_group', $row);
            }
            $this->truncateTable('printer_material');
            $materials = [
                ['1', 'Full color gypsum', '1', '3', 'Gypsum', '2.31'],
                ['2', 'Durable and flexible plastic', '1', '1', 'PLA', '1.25'],
                ['3', 'Detailed plastic', '1', '1', 'ABS', '1.04'],
                ['4', 'Detailed translucent plastic', '1', '1', 'Nylon', '1.15'],
                ['5', 'Bronze', '1', '3', 'Bronze PLA', '2.40'],
                ['6', 'Brass', '1', '3', 'Brass PLA', '2.40'],
                ['7', 'Silver', '1', '2', 'Silver PLA', '2.30'],
                ['8', 'Gold', '1', '2', 'Gold PLA', '2.70'],
                ['9', 'Jewelry wax', '1', '3', 'Wax PLA', '2.00'],
                ['10', 'Stainless steel', '1', '2', 'STEEL PLA', '2.40'],
            ];
            foreach($materials as $material){
                $keys = ['id', 'title', 'is_active', 'group_id', 'filament_title', 'density'];
                $row = array_combine($keys, $material);
                $row['created_at'] = dbexpr('NOW()');
                $row['updated_at'] = dbexpr('NOW()');
                $this->insert('printer_material', $row);
            }
            
            $this->execute("ALTER TABLE `printer_material_to_color` 
                ADD COLUMN `is_default` TINYINT(1) NOT NULL DEFAULT 1 AFTER `is_active`;");
            
            // color used by povray renderer
            $this->execute("ALTER TABLE `printer_color` 
                ADD COLUMN `render_color` VARCHAR(45) NULL AFTER `image`;");
           
            
            $colors = [
               ['Green', '1', '0,255,0', 'Green'],
               ['Blue', '1', '0,0,255', 'Blue'],
               ['Red', '1', '255,0,0', 'Red'],
               ['Yellow', '1', '255, 255, 0', 'Yellow'],
               ['Silver', '1', '230,232,230', 'Silver'],
               ['Bronze', '1', '140,120,26', 'Bronze'],
               ['Cyan', '1', '0,255,255', 'Cyan'],
               ['Magenta', '1', '255,0,255', 'Magenta'],
               ['Aquamarine', '1', '112,219,128', 'Aquamarine'],
               ['BlueViolet', '1', '159,95,153', 'BlueViolet'],
               ['Brown', '1', '165,42,26', 'Brown'],
               ['CadetBlue', '1', '95,159,153', 'CadetBlue'],
               ['Coral', '1', '255,127,0', 'Coral'],
               ['CornflowerBlue', '1', '66,66,102', 'CornflowerBlue'],
               ['DarkGreen', '1', '47,79,26', 'DarkGreen'],
               ['DarkOliveGreen', '1', '79,79,26', 'DarkOliveGreen'],
               ['DarkOrchid', '1', '153,50,204', 'DarkOrchid'],
               ['DarkSlateBlue', '1', '31,35,128', 'DarkSlateBlue'],
               ['DarkSlateGray', '1', '47,79,77', 'DarkSlateGray'],
               ['DarkTurquoise', '1', '112,147,204', 'DarkTurquoise'],
               ['Firebrick', '1', '142,35,26', 'Firebrick'],
               ['ForestGreen', '1', '35,142,26', 'ForestGreen'],
               ['Gold', '1', '204,127,26', 'Gold'],
               ['Goldenrod', '1', '219,219,102', 'Goldenrod'],
               ['GreenYellow', '1', '147,219,102', 'GreenYellow'],
               ['IndianRed', '1', '79,47,26', 'IndianRed'],
               ['Khaki', '1', '159,159,77', 'Khaki'],
               ['LightBlue', '1', '191,216,204', 'LightBlue'],
               ['LightSteelBlue', '1', '143,143,179', 'LightSteelBlue'],
               ['LimeGreen', '1', '50,204,26', 'LimeGreen'],
               ['Maroon', '1', '142,35,102', 'Maroon'],
               ['MediumAquamarine', '1', '50,204,153', 'MediumAquamarine'],
               ['MediumBlue', '1', '50,50,204', 'MediumBlue'],
               ['MediumForestGreen', '1', '107,142,26', 'MediumForestGreen'],
               ['MediumGoldenrod', '1', '234,234,153', 'MediumGoldenrod'],
               ['MediumOrchid', '1', '147,112,204', 'MediumOrchid'],
               ['MediumSeaGreen', '1', '66,111,51', 'MediumSeaGreen'],
               ['MediumTurquoise', '1', '112,219,204', 'MediumTurquoise'],
               ['MediumVioletRed', '1', '219,112,128', 'MediumVioletRed'],
               ['MidnightBlue', '1', '47,47,77', 'MidnightBlue'],
               ['Navy', '1', '35,35,128', 'Navy'],
               ['NavyBlue', '1', '35,35,128', 'NavyBlue'],
               ['Orchid', '1', '219,112,204', 'Orchid'],
               ['PaleGreen', '1', '143,188,128', 'PaleGreen'],
               ['Pink', '1', '188,143,128', 'Pink'],
               ['Plum', '1', '234,173,230', 'Plum'],
               ['Salmon', '1', '111,66,51', 'Salmon'],
               ['SeaGreen', '1', '35,142,102', 'SeaGreen'],
               ['Sienna', '1', '142,107,26', 'Sienna'],
               ['SkyBlue', '1', '50,153,204', 'SkyBlue'],
               ['SteelBlue', '1', '35,107,128', 'SteelBlue'],
               ['Tan', '1', '219,147,102', 'Tan'],
               ['Thistle', '1', '216,191,204', 'Thistle'],
               ['Turquoise', '1', '173,234,230', 'Turquoise'],
               ['Violet', '1', '79,47,77', 'Violet'],
               ['VioletRed', '1', '204,50,153', 'VioletRed'],
               ['Wheat', '1', '216,216,179', 'Wheat'],
               ['YellowGreen', '1', '153,204,26', 'YellowGreen'],
               ['SummerSky', '1', '56,176,204', 'SummerSky'],
               ['RichBlue', '1', '89,89,153', 'RichBlue'],
               ['Brass', '1', '181,166,51', 'Brass'],
               ['Copper', '1', '184,115,51', 'Copper'],
               ['Bronze2', '1', '166,125,51', 'Bronze2'],
               ['BrightGold', '1', '217,217,26', 'BrightGold'],
               ['OldGold', '1', '207,181,51', 'OldGold'],
               ['Feldspar', '1', '209,145,102', 'Feldspar'],
               ['Quartz', '1', '217,217,230', 'Quartz'],
               ['NeonPink', '1', '255,110,179', 'NeonPink'],
               ['DarkPurple', '1', '135,31,102', 'DarkPurple'],
               ['NeonBlue', '1', '77,77,255', 'NeonBlue'],
               ['CoolCopper', '1', '217,135,26', 'CoolCopper'],
               ['MandarinOrange', '1', '227,120,51', 'MandarinOrange'],
               ['LightWood', '1', '232,194,153', 'LightWood'],
               ['MediumWood', '1', '166,128,77', 'MediumWood'],
               ['DarkWood', '1', '133,94,51', 'DarkWood'],
               ['SpicyPink', '1', '255,28,153', 'SpicyPink'],
               ['SemiSweetChoc', '1', '107,66,26', 'SemiSweetChoc'],
               ['BakersChoc', '1', '92,51,0', 'BakersChoc'],
               ['Flesh', '1', '245,204,153', 'Flesh'],
               ['NewTan', '1', '235,199,153', 'NewTan'],
               ['NewMidnightBlue', '1', '0,0,153', 'NewMidnightBlue'],
               ['VeryDarkBrown', '1', '89,41,26', 'VeryDarkBrown'],
               ['DarkBrown', '1', '92,64,51', 'DarkBrown'],
               ['DarkTan', '1', '150,105,77', 'DarkTan'],
               ['GreenCopper', '1', '82,125,102', 'GreenCopper'],
               ['DkGreenCopper', '1', '74,117,102', 'DkGreenCopper'],
               ['DustyRose', '1', '133,99,77', 'DustyRose'],
               ['HuntersGreen', '1', '33,94,77', 'HuntersGreen'],
               ['Scarlet', '1', '140,23,0', 'Scarlet'],
            ];
            
            foreach($colors as $k=>$color){
                $id = $k + 1;
                $keys = ['title', 'is_active', 'rgb', 'render_color'];
                $row = array_combine($keys, $color);
                
                $row['id'] = $id;
                $row['created_at'] = dbexpr('NOW()');
                $row['updated_at'] = dbexpr('NOW()');
                $this->insert('printer_color', $row);
            }
            $this->execute("ALTER TABLE `model3d`  CHANGE COLUMN `description` `description` TEXT NULL DEFAULT NULL ;");
        }catch(\Exception $e){
            return false;
        }
        return true;
    }

    public function safeDown()
    {
        try{
            $this->execute("SET FOREIGN_KEY_CHECKS=0"); 
            $this->dropForeignKey('fk_printer_material_1', 'printer_material');
            $this->dropIndex('fk_printer_material_1_idx', 'printer_material');
            $this->dropColumn('printer_material', 'group_id');
            $this->dropColumn('printer_material', 'filament_title');
            $this->dropColumn('printer_material', 'density');
            $this->dropColumn('printer_material_to_color', 'is_default');
            $this->dropColumn('printer_color', 'render_color');            
            $tables = ['printer_material_group_intl', 'printer_material_group'];
            foreach($tables as $k=>$v){
                $this->truncateTable($v);
                $this->dropTable($v);
            }
        }catch(\Exception $e){
            return false;
        }

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
