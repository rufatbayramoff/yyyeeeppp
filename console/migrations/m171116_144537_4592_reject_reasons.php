<?php

use yii\db\Migration;

class m171116_144537_4592_reject_reasons extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_preorder', 'My machines are currently unavailable', NULL, '1' );
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_preorder', 'The amount quoted is not accurate', NULL, '1' );
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_preorder', 'Specified materials currently out of stock', NULL, '1' );
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_preorder', 'Other, please explain', NULL, '1' );
        ");
    }

    public function safeDown()
    {
        echo "m171116_144537_4592_reject_reasons cannot be reverted.\n";
        return false;
    }

}
