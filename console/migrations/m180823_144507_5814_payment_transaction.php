<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180823_144507_5814_payment_transaction
 */
class m180823_144507_5814_payment_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable('payment_invoice', 'payment_bank_invoice');
        $this->execute('RENAME TABLE `payment_invoice_item` TO `payment_bank_invoice_item`');
        $this->execute('CREATE TABLE `payment_invoice` (
  `uuid` varchar(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `date_expire` datetime NOT NULL,
  `total_amount` decimal(14,2) NOT NULL,
  `currency` char(3) NOT NULL,
  `rate_id` int(11) NOT NULL,
  `status` char(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_position_id` int(11) DEFAULT NULL,
  `details` varchar(255) NOT NULL,
  `accounting` json NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `payment_invoice_rate_id` (`rate_id`),
  KEY `user_id` (`user_id`),
  KEY `order_id` (`order_id`),
  KEY `order_position_id` (`order_position_id`),
  CONSTRAINT `fk_payment_invoice_order_id` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`),
  CONSTRAINT `fk_payment_invoice_order_position_id` FOREIGN KEY (`order_position_id`) REFERENCES `store_order_position` (`id`),
  CONSTRAINT `fk_payment_invoice_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `payment_exchange_rate` (`id`),
  CONSTRAINT `fk_payment_invoice_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');

        $this->execute('CREATE TABLE `payment_invoice_item` (
  `uuid` varchar(12) not null,
  `payment_invoice_uuid` varchar(6) NOT NULL,
  `pos` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `measure` char(15) NOT NULL,
  `qty` decimal(15,4) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `payment_invoice_uuid` (`payment_invoice_uuid`),
  CONSTRAINT `payment_invoice_item_fk` FOREIGN KEY (`payment_invoice_uuid`) REFERENCES `payment_invoice` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');
        $this->addColumn('payment', 'payment_invoice_uuid', 'varchar(6) NULL after id');
        $this->createIndex('payment_invoice_uuid_idx', 'payment', 'payment_invoice_uuid');
        $this->addForeignKey('payment_invoice_uuid_fk', 'payment', 'payment_invoice_uuid', 'payment_invoice', 'uuid');

        $this->execute('CREATE TABLE `payment_detail_operation` (
  `uuid` varchar(6) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `payment_id` (`payment_id`),
  CONSTRAINT `payment_operation_detail_fk` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->addColumn('payment_detail', 'payment_detail_operation_uuid', 'varchar(6) NOT NULL after id');
        $this->createIndex('payment_detail_operation_uuid_idx', 'payment_detail', 'payment_detail_operation_uuid');

    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
