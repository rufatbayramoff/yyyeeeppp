<?php

use yii\db\Migration;

class m171121_143221_4954_preordre_history extends Migration
{
    public function safeUp()
    {
        $this->execute("
        
        
        
        
        CREATE TABLE `preorder_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preorder_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `action` varchar(45) NOT NULL,
  `data` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `time_UNIQUE` (`time`),
  KEY `fk_preorder_history_preorder_idx` (`preorder_id`),
  CONSTRAINT `fk_preorder_history_preorder` FOREIGN KEY (`preorder_id`) REFERENCES `preorder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

        
        
        ");
    }

    public function safeDown()
    {
        echo "m171121_143221_4954_preordre_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171121_143221_4954_preordre_history cannot be reverted.\n";

        return false;
    }
    */
}
