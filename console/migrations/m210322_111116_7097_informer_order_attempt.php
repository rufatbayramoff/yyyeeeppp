<?php

use yii\db\Migration;

/**
 * Class m210322_111116_7097_informer_order_attempt
 */
class m210322_111116_7097_informer_order_attempt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
CREATE TABLE `informer_store_order_attempt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `count` int(11) NOT NULL,
  `part_key` int(11) NOT NULL,
  `part_info` json NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `part_key` (`part_key`),
  KEY `key` (`key`),
  CONSTRAINT `fk_informer_store_order_attempt_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_informer_store_order_part_key` FOREIGN KEY (`part_key`) REFERENCES `store_order_attemp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute('
CREATE TABLE `informer_customer_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `count` int(11) NOT NULL,
  `part_key` int(11) NOT NULL,
  `part_info` json NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `part_key` (`part_key`),
  KEY `key` (`key`),
  CONSTRAINT `fk_informer_customer_order_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_informer_customer_order_part_key` FOREIGN KEY (`part_key`) REFERENCES `store_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;        
        ');

        $this->execute('
CREATE TABLE `informer_customer_preorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `count` int(11) NOT NULL,
  `part_key` int(11) NOT NULL,
  `part_info` json NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `part_key` (`part_key`),
  KEY `key` (`key`),
  CONSTRAINT `fk_informer_customer_preorder_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_informer_customer_preorder_part_key` FOREIGN KEY (`part_key`) REFERENCES `preorder` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('informer_store_order_attempt');
        $this->dropTable('informer_customer_order');
        $this->dropTable('informer_customer_preorder');
    }
}
