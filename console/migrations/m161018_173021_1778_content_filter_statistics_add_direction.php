<?php

use yii\db\Migration;
use yii\db\Query;

class m161018_173021_1778_content_filter_statistics_add_direction extends Migration
{
    public function up()
    {
        $this->addColumn('content_filter_statistics', 'direction', "enum('send', 'recive') NULL AFTER alert_type");
        $messages = (new Query())
            ->select('id')
            ->from('msg_message')
            ->all();
        foreach ($messages as $message) {
            $this->insert('content_filter_checked_messages', ['message_id' => $message['id'], 'isBanned' => 0]);
        }
        $topics = (new Query())
            ->select('id')
            ->from('msg_topic')
            ->all();
        foreach ($topics as $topic) {
            $this->insert('content_filter_checked_topics', ['topic_id' => $topic['id'], 'isBanned' => 0]);
        }
    }

    public function down()
    {
        $this->dropColumn('content_filter_statistics', 'direction');
    }
}
