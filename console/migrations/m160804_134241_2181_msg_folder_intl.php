<?php

use yii\db\Migration;

class m160804_134241_2181_msg_folder_intl extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `msg_folder_intl` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `msg_folder_id` int(11) unsigned NOT NULL,
              `lang_iso` char(5) NOT NULL,
              `title` varchar(245) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_msg_folder_intl_1_idx` (`msg_folder_id`),
              CONSTRAINT `fk_msg_folder_intl_1` FOREIGN KEY (`msg_folder_id`) REFERENCES `msg_folder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute("INSERT INTO `msg_folder_intl` (`id`, `msg_folder_id`, `lang_iso`, `title`) VALUES
                        (1, 1, 'ru', 'Персональные сообщения'),
                        (2, 2, 'ru', 'Служба поддержки'),
                        (3, 3, 'ru', 'Заказы на печать'),
                        (4, 4, 'ru', 'Ваши заказы'),
                        (5, 5, 'ru', 'Архив'),
                        (6, 6, 'ru', 'Готовые заказы');");
        $this->execute("INSERT INTO `msg_folder_intl` (`id`, `msg_folder_id`, `lang_iso`, `title`) VALUES
                        (7, 1, 'ru-RU', 'Персональные сообщения'),
                        (8, 2, 'ru-RU', 'Служба поддержки'),
                        (9, 3, 'ru-RU', 'Заказы на печать'),
                        (10, 4, 'ru-RU', 'Ваши заказы'),
                        (11, 5, 'ru-RU', 'Архив'),
                        (12, 6, 'ru-RU', 'Готовые заказы');");

        $this->execute('CREATE TABLE `printer_color_intl` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `printer_color_id` int(11) DEFAULT NULL,
          `lang_iso` char(5) DEFAULT NULL,
          `title` varchar(125) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `fk_printer_color_intl_1_idx` (`printer_color_id`),
          CONSTRAINT `fk_printer_color_intl_1` FOREIGN KEY (`printer_color_id`) REFERENCES `printer_color` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute("INSERT INTO `printer_color_intl` (`printer_color_id`, `lang_iso`, `title`) VALUES ('1', 'ru', 'Зеленый');");
        $this->execute("INSERT INTO `printer_color_intl` (`printer_color_id`, `lang_iso`, `title`) VALUES ('2', 'ru', 'Синий');");
        $this->execute("INSERT INTO `printer_color_intl` (`printer_color_id`, `lang_iso`, `title`) VALUES ('3', 'ru', 'Красный');");

        $this->execute('CREATE TABLE `printer_material_intl` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `printer_material_id` int(11) NOT NULL,
              `lang_iso` char(5) NOT NULL,
              `title` varchar(145) NOT NULL,
              `short_description` varchar(250) DEFAULT NULL,
              `long_description` varchar(5000) DEFAULT NULL,
              `filament_title` varchar(45) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_printer_material_intl_1_idx` (`printer_material_id`),
              CONSTRAINT `fk_printer_material_intl_1` FOREIGN KEY (`printer_material_id`) REFERENCES `printer_material` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        ');

        $this->execute('CREATE TABLE `printer_properties_intl` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `printer_properties_id` int(11) NOT NULL,
          `lang_iso` char(5) NOT NULL,
          `title` varchar(145) NOT NULL,
          `description` varchar(245) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `fk_printer_properties_intl_1_idx` (`printer_properties_id`),
          CONSTRAINT `fk_printer_properties_intl_1` FOREIGN KEY (`printer_properties_id`) REFERENCES `printer_properties` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute('CREATE TABLE `system_reject_intl` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `system_reject_id` int(11) NOT NULL,
              `lang_iso` char(5) NOT NULL,
              `title` varchar(145) DEFAULT NULL,
              `description` varchar(245) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_system_reject_intl_1_idx` (`system_reject_id`),
              CONSTRAINT `fk_system_reject_intl_1` FOREIGN KEY (`system_reject_id`) REFERENCES `system_reject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute('CREATE TABLE `printer_technology_intl` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `printer_technology_id` int(11) NOT NULL,
              `lang_iso` char(5) NOT NULL,
              `title` varchar(145) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_printer_technology_intl_1_idx` (`printer_technology_id`),
              CONSTRAINT `fk_printer_technology_intl_1` FOREIGN KEY (`printer_technology_id`) REFERENCES `printer_technology` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    public function safeDown()
    {
        $this->truncateTable('msg_folder_intl');
        $this->dropForeignKey('fk_msg_folder_intl_1', 'msg_folder_intl');
        $this->dropTable('msg_folder_intl');

        $this->truncateTable('printer_color_intl');
        $this->dropForeignKey('fk_printer_color_intl_1', 'printer_color_intl');
        $this->dropTable('printer_color_intl');

        $this->truncateTable('printer_material_intl');
        $this->dropTable('printer_material_intl');

        $this->truncateTable('printer_properties_intl');
        $this->dropTable('printer_properties_intl');

        $this->truncateTable('system_reject_intl');
        $this->dropTable('system_reject_intl');

        $this->truncateTable('printer_technology_intl');
        $this->dropTable('printer_technology_intl');
    }
}
