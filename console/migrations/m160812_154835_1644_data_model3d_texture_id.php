<?php

use yii\db\Migration;
use yii\db\Query;

class m160812_154835_1644_data_model3d_texture_id extends Migration
{
    public function up()
    {
        $model3dTextures = (new Query())
            ->select('*')
            ->from('model3d_texture')
            ->all();
        foreach ($model3dTextures as $model3dTexture) {
            if ($model3dTexture['model3d_file_id']) {
                $this->update('model3d_part', ['model3d_texture_id' => $model3dTexture['id']], ['id' => $model3dTexture['model3d_file_id']]);
            } else {
                $this->update('model3d', ['model3d_texture_id' => $model3dTexture['id']], ['id' => $model3dTexture['model3d_id']]);
            }
        }
        $this->execute(
            <<<SQL
ALTER TABLE `model3d_texture` 
DROP FOREIGN KEY `model3d_color_model3d_id`,
DROP FOREIGN KEY `model3d_color_model3d_file_id`;
ALTER TABLE `model3d_texture` 
DROP COLUMN `model3d_file_id`,
DROP COLUMN `model3d_id`,
DROP INDEX `model3d_file_id` ,
DROP INDEX `model3d_id` ;
SQL
        );
    }

    public function down()
    {
        echo "m160812_154835_1644_data_model3d_texture_id cannot be reverted.\n";
        return false;
    }
}
