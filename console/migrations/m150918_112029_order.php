<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_112029_order extends Migration
{
    
    public function safeUp()
    {
        $this->addColumn('store_order', 'payment_status', "CHAR(15) NOT NULL DEFAULT 'new'"); 
        $this->renameColumn('paymentbt_transaction', 'store_unit_id', 'order_id');
        $this->truncateTable('paymentbt_transaction');
        $this->addForeignKey('fkstoreid', 'paymentbt_transaction', 'order_id', 'store_order', 'id');
    }

    public function safeDown()
    {
        $this->dropColumn('store_order', 'payment_status');        
        $this->dropForeignKey('fkstoreid', 'paymentbt_transaction');
        $this->renameColumn('paymentbt_transaction', 'order_id', 'store_unit_id');        
    }    
}
