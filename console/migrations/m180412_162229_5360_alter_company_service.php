<?php

use yii\db\Migration;

/**
 * Class m180412_162229_5360_alter_company_service
 */
class m180412_162229_5360_alter_company_service extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('company_service', 'title', 'string');
        $this->addColumn('company_service', 'category_id', 'int');
        $this->addColumn('company_service', 'description', 'text');

        $this->execute(
            'CREATE TABLE `company_service_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_service_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`company_service_id`),
  KEY `index3` (`file_id`),
  CONSTRAINT `fk_company_service_image_1` FOREIGN KEY (`company_service_id`) REFERENCES `company_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_service_image_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;'
        );

        $this->execute('CREATE TABLE `company_service_category` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `lft` int(11) NOT NULL,
          `rgt` int(11) NOT NULL,
          `depth` int(11) NOT NULL,
          `code` varchar(120) NOT NULL,
          `slug` varchar(255) NOT NULL,
          `title` varchar(255) NOT NULL,
          `description` text,
          PRIMARY KEY (`id`),
          UNIQUE KEY `code` (`code`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        ');

         $this->execute('CREATE TABLE `company_service_category_intl` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `model_id` int(11) NOT NULL,
          `title` varchar(255) NOT NULL,
          `description` text,
          `lang_iso` char(7) NOT NULL,
          `is_active` bit(1) NOT NULL,
          PRIMARY KEY (`id`),
          KEY `model_id` (`model_id`),
          CONSTRAINT `fk_company_service_category_intl_1` FOREIGN KEY (`model_id`) REFERENCES `company_service_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

         $this->execute('ALTER TABLE `company_service` 
            ADD INDEX `fk_company_service_1_idx` (`category_id` ASC);
            ALTER TABLE `company_service` 
            ADD CONSTRAINT `fk_company_service_1`
              FOREIGN KEY (`category_id`)
              REFERENCES `company_service_category` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');

        $this->insert('company_service_category', ['id' => 1, 'lft' => 1, 'rgt' => 4, 'depth' => 1, 'code' => 'root', 'slug' => '', 'title' => 'Root']);
        $this->insert('company_service_category', ['id' => 2, 'lft' => 2, 'rgt' => 3, 'depth' => 2, 'code' => 'manufacturing-services', 'slug' => 'manufacturing-services', 'title' => 'Manufacturing Services']);


        $this->addColumn('company_service', 'primary_image_id', 'int');
        $this->execute('ALTER TABLE `company_service` 
            ADD INDEX `fk_company_service_2_idx` (`primary_image_id` ASC);
            ALTER TABLE `company_service` 
            ADD CONSTRAINT `fk_company_service_2`
              FOREIGN KEY (`primary_image_id`)
              REFERENCES `company_service_image` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');

        $this->execute('CREATE TABLE `company_service_history` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `company_service_id` int(11) NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `action_id` char(15) NOT NULL,
              `comment` varchar(245) DEFAULT NULL,
              `user_id` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_company_service_history_1_idx` (`company_service_id`),
              KEY `fk_company_service_history_2_idx` (`user_id`),
              CONSTRAINT `fk_company_service_history_1` FOREIGN KEY (`company_service_id`) REFERENCES `company_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_company_service_history_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('company_service_category_intl');
        $this->truncateTable('company_service_category');

        $this->dropColumn('company_service', 'title');
        $this->dropColumn('company_service', 'category_id');
        $this->dropColumn('company_service', 'description');
        $this->dropColumn('company_service', 'primary_image_id');
        $this->truncateTable('company_service_image');
        $this->dropTable('company_service_image');
        $this->truncateTable('company_service_history');
        $this->dropTable('company_service_history');

        $this->dropTable('company_service_category_intl');
        $this->dropTable('company_service_category');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180412_162229_5360_alter_company_service cannot be reverted.\n";

        return false;
    }
    */
}
