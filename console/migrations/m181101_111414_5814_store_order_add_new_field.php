<?php

use yii\db\Migration;

/**
 * Class m181101_111414_5814_store_order_add_new_field
 */
class m181101_111414_5814_store_order_add_new_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order', 'primary_payment_invoice_uuid', $this->string(6));

        app()->db->createCommand('
UPDATE store_order so 
set so.primary_payment_invoice_uuid = (select pi.uuid from payment_invoice pi where pi.store_order_id = so.id order by pi.created_at DESC)
')->execute();

        $this->createIndex('idx_so_primary_payment_invoice_uuid', 'store_order', 'primary_payment_invoice_uuid', true);
        $this->addForeignKey(
            'fk_so_primary_payment_invoice_uuid',
            'store_order',
            'primary_payment_invoice_uuid',
            'payment_invoice',
            'uuid'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_so_primary_payment_invoice_uuid', 'store_order');
        $this->dropColumn('store_order', 'primary_payment_invoice_uuid');
    }

}
