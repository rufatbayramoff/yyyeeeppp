<?php

use yii\db\Migration;

/**
 * Class m180307_124540_3020_share_table
 */
class m180307_124540_3020_share_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `store_order_review_share` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) DEFAULT NULL,
              `review_id` int(11) NOT NULL,
              `social_type` enum('link','google','facebook','facebook_im','reddit') NOT NULL,
              `photo_id` int(11) NOT NULL,
              `text` TEXT NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `status` enum('new','approved','rejected') NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_store_order_review_share_photo_idx` (`photo_id`),
              KEY `fk_store_order_review_share_review_idx` (`review_id`),
              KEY `fk_store_order_review_share_user_idx` (`user_id`),
              CONSTRAINT `fk_store_order_review_share_photo` FOREIGN KEY (`photo_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_store_order_review_share_review` FOREIGN KEY (`review_id`) REFERENCES `store_order_review` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_store_order_review_share_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('store_order_review_share');

        return true;
    }
}
