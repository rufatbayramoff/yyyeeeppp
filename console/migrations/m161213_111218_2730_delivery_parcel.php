<?php

use yii\db\Migration;

class m161213_111218_2730_delivery_parcel extends Migration
{

    public function up()
    {
        $this->execute("
            ALTER TABLE `store_order_attemp_delivery` 
            ADD COLUMN `parcel_width`  DECIMAL(7,2) UNSIGNED NULL AFTER `send_message_at`,
            ADD COLUMN `parcel_height` DECIMAL(7,2) UNSIGNED NULL AFTER `parcel_width`,
            ADD COLUMN `parcel_length` DECIMAL(7,2) UNSIGNED NULL AFTER `parcel_height`,
            ADD COLUMN `parcel_weight` DECIMAL(7,2) UNSIGNED NULL AFTER `parcel_length`;
        ");


        $this->execute("
            UPDATE 
              store_order_attemp_delivery ad, 
              store_order o, 
              delivery_parcel p 
            SET 
              ad.parcel_height = p.height, 
              ad.parcel_length = p.length,  
              ad.parcel_weight = p.weight, 
              ad.parcel_width = p.width
            WHERE ad.order_attemp_id = o.current_attemp_id AND p.order_id = o.id;
        ");

        $this->execute("DROP TABLE `delivery_parcel`;");
    }

    public function down()
    {
        echo "m161213_111218_2730_delivery_parcel cannot be reverted.\n";

        return false;
    }
}
