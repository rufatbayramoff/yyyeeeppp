<?php

use yii\db\Schema;
use yii\db\Migration;

class m150922_080234_ps_printer_normalize extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->execute('ALTER TABLE `ps_printer` DROP FOREIGN KEY `fk_user_id`;');
        $this->execute('ALTER TABLE `ps_printer` DROP COLUMN `user_id`, DROP INDEX `fk_printservice_printer_1_idx`;');
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->execute('ALTER TABLE `ps_printer` ADD COLUMN `user_id` INT(11) NOT NULL AFTER `id`,
                        ADD INDEX `fk_printservice_printer_1_idx` (`user_id` ASC);');
        $this->execute('ALTER TABLE `ps_printer` ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`)
                        REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;');
    }
}
