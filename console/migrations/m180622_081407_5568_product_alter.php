<?php

use yii\db\Migration;

/**
 * Class m180622_081407_5568_product_alter
 */
class m180622_081407_5568_product_alter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            'ALTER TABLE `product` 
ADD COLUMN `videos` JSON NULL AFTER `switch_quote_qty`;'
        );

        $this->execute(
            'CREATE TABLE `product_block` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `title` varchar(45) NOT NULL,
              `content` text,
              `created_at` datetime NOT NULL,
              `product_uuid` varchar(32)  NULL,
              PRIMARY KEY (`id`),
              KEY `fk_product_block_1_idx` (`product_uuid`),
              CONSTRAINT `fk_product_block_1` FOREIGN KEY (`product_uuid`) REFERENCES `product` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
            '
        );

        $this->execute(
            'CREATE TABLE `product_block_image` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `block_id` int(11) NOT NULL,
              `position` int(11) NOT NULL DEFAULT \'0\',
              `file_uuid` varchar(32) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `file_uuid` (`file_uuid`),
              KEY `index3` (`block_id`),
              CONSTRAINT `fk_product_block_image_1` FOREIGN KEY (`block_id`) REFERENCES `product_block` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
              CONSTRAINT `fk_product_block_image_file_uuid` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;');

        $this->execute(
            "CREATE TABLE `product_file` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `type`  enum('cad','file') DEFAULT 'file',
              `title` varchar(245) NOT NULL,
              `file_id` int(11) NOT NULL, 
              `created_at` datetime NOT NULL, 
              `product_uuid` varchar(32) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_product_file_1_idx` (`product_uuid`),
              KEY `fk_product_file_2_idx` (`file_id`),
              CONSTRAINT `fk_product_file_1` FOREIGN KEY (`product_uuid`) REFERENCES `product` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_product_file_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        "
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'videos');
        $this->dropTable('product_block_image');
        $this->dropTable('product_block');
        $this->dropTable('product_file');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180622_081407_5568_product_alter cannot be reverted.\n";

        return false;
    }
    */
}
