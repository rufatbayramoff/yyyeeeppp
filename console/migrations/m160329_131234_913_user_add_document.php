<?php

use yii\db\Migration;

class m160329_131234_913_user_add_document extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user` ADD `document_file_id` INT  NULL  DEFAULT NULL  AFTER `trustlevel`;");
        $this->execute("ALTER TABLE `user` ADD CONSTRAINT `document_file` FOREIGN KEY (`document_file_id`) REFERENCES `file` (`id`);");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `user` DROP FOREIGN KEY `document_file`;");
        $this->execute("ALTER TABLE `user` DROP `document_file_id`;");
        return true;
    }
}
