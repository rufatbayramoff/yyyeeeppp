<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\components\UuidHelper;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m201016_093322_7635_product_category_search_keywords
 */
class m201016_093322_7635_product_category_search_keywords extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
CREATE TABLE `product_category_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_id` int(11) NOT NULL,
  `keyword` varchar(1024) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  KEY `product_category_id` (`product_category_id`),
  KEY `keyword` (`keyword`),
  CONSTRAINT `product_category_keyword_category_id_fk` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropTable('product_category_keyword');
    }
}
