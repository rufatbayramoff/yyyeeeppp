<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_145032_fix_fileds_file_properties extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `model3d_file_properties` CHANGE `length` `length` DECIMAL(6,2)  NOT NULL;
            ALTER TABLE `model3d_file_properties` CHANGE `width` `width` DECIMAL(6,2)  NOT NULL;
            ALTER TABLE `model3d_file_properties` CHANGE `height` `height` DECIMAL(6,2)  NOT NULL;
        ");



    }

    public function down()
    {
        echo "m151216_145032_fix_fileds_file_properties cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
