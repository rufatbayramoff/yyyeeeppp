<?php

use yii\db\Migration;

/**
 * Class m181113_133721_5814_payment_transaction_back_relation
 */
class m181113_133721_5814_payment_transaction_back_relation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('payment_transaction_refund', ['transaction_refund_id' => null]);
        $this->execute('ALTER TABLE `payment_transaction_refund` DROP INDEX `fk_payment_transaction_refund_3_idx`, ADD UNIQUE `fk_payment_transaction_refund_3_idx` (`transaction_refund_id`) USING BTREE;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
