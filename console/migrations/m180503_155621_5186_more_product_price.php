<?php

use yii\db\Migration;

/**
 * Class m180503_155621_5186_more_product_price.php
 */
class m180503_155621_5186_more_product_price extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('product', 'single_price', '	decimal(7,2)');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
