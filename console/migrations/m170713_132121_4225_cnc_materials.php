<?php

use yii\db\Migration;
use yii\db\Query;

class m170713_132121_4225_cnc_materials extends Migration
{
    public function up()
    {
        $this->execute(
            "
CREATE TABLE `cnc_material` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `cnc_material_group_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `density` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cnc_material_group_id` (`cnc_material_group_id`),
  CONSTRAINT `cnc_material_group_id` FOREIGN KEY (`cnc_material_group_id`) REFERENCES `cnc_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cnc_material_group` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
"
        );
        $this->insert('cnc_material_group', ['code' => 'iron', 'title' => 'Iron']);
        $this->insert('cnc_material_group', ['code' => 'steel', 'title' => 'Steel']);
        $this->insert('cnc_material_group', ['code' => 'aluminium', 'title' => 'Aluminium']);
        $this->insert('cnc_material_group', ['code' => 'soft_metal', 'title' => 'Soft metal']);
        $this->insert('cnc_material_group', ['code' => 'plastic', 'title' => 'Plastic']);
        $this->insert('cnc_material_group', ['code' => 'wood', 'title' => 'Wood']);
        $this->insert('cnc_material_group', ['code' => 'other', 'title' => 'Other']);
    }

    public function down()
    {
        $this->dropTable('cnc_material');
        $this->dropTable('cnc_material_group');
    }
}
