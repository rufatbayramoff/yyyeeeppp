<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180828_164920_5814_fix_payment_transactions
 */
class m180828_164920_5814_fix_payment_transactions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $ids = $this->db->createCommand("SELECT `payment_transaction`.id FROM `payment_transaction` LEFT JOIN `store_order` on `store_order`.id=`payment_transaction`.order_id LEFT JOIN `payment_transaction` as `pt` on `store_order`.id=`pt`.order_id WHERE `payment_transaction`.`vendor`='thingiverse' and `payment_transaction`.`status` ='authorized' and `pt`.`vendor`='braintree' and `pt`.`status`='settled'")->queryAll();
        $this->update('payment_transaction', ['status' => 'settled'], ['id'=>$ids]);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
