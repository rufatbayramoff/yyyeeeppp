<?php

use yii\db\Migration;

/**
 * Class m180725_141206_5560_alter_company
 */
class m180725_141206_5560_alter_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `ps`
	ADD COLUMN `business_type` VARCHAR(125) NULL DEFAULT NULL AFTER `ownership`;
');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('ps', 'business_type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180725_141206_5560_alter_company cannot be reverted.\n";

        return false;
    }
    */
}
