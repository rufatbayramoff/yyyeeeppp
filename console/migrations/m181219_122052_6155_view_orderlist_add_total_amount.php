<?php

use yii\db\Migration;

/**
 * Class m181219_122052_6155_view_orderlist_add_total_amount
 */
class m181219_122052_6155_view_orderlist_add_total_amount extends Migration
{
    public function safeUp()
    {
        $this->execute('DROP VIEW IF EXISTS `view_orderlist`');
        $this->execute("create view view_orderlist as select o.id                                                       AS order_id,
       o.user_id                                                  AS buyer_id,
       o.billed_at                                                AS billed_at,
       af.referrer                                                AS referrer,
       m3.user_id                                                 AS designer_id,
       o.created_at                                               AS created_at,
       atdates.accepted_date                                      AS accepted_at,
       oh2.created_at                                             AS ready_sent_at,
       atdates.delivered_at                                       AS delivered_at,
       atdates.shipped_at                                         AS shipped_at,
       atdates.received_at                                        AS received_at,
       geoc.iso_code                                              AS to_country,
       shipto.city                                                AS to_city,
       psp.id                                                     AS printer_id,
       psp.title                                                  AS printer_title,
       geoc2.iso_code                                             AS ps_country,
       dt.title                                                   AS delivery_type,
       concat(atdl.tracking_number, ' - ', atdl.tracking_shipper) AS delivery_opts,
       pi.total_amount                                            AS total_amount,
       pi.currency                                                AS currency
from store_order o
       left join payment_invoice pi on pi.uuid = o.primary_payment_invoice_uuid
       left join store_order_attemp attemp on attemp.id = o.current_attemp_id
       left join store_order_attemp_dates atdates on atdates.attemp_id = attemp.id
       left join store_order_attemp_delivery atdl on atdl.order_attemp_id = attemp.id
       left join affiliate_user_source af on af.user_id = o.user_id and af.type = 'register'
       left join store_order_item soi on soi.order_id = o.id
       left join store_unit su on su.id = soi.unit_id
       left join model3d m3 on m3.id = su.model3d_id
       left join user_address shipto on shipto.id = o.ship_address_id
       left join geo_country geoc on geoc.id = shipto.country_id
       left join company_service psm on psm.id = attemp.machine_id
       left join ps_printer psp on psp.id = psm.ps_printer_id
       left join user_location l on l.id = psm.location_id
       left join geo_country geoc2 on geoc2.id = l.country_id
       left join store_order_history oh2 on oh2.order_id = o.id and oh2.action_id = 'order_status' and
                                            oh2.comment = 'old[printed] new[ready_send]'
       left join delivery_type dt on dt.id = o.delivery_type_id
       left join store_order_history oh3 on oh3.order_id = o.id and oh3.action_id = 'order_status' and
                                            oh3.comment = 'old[printed] new[ready_send]'
where o.user_id <> 1;");

    }

    public function safeDown()
    {
        $this->execute('DROP VIEW `view_orderlist`');
        $this->execute("create view view_orderlist as
select `o`.`id`                                                           AS `order_id`,
       `o`.`user_id`                                                      AS `buyer_id`,
       `o`.`billed_at`                                                    AS `billed_at`,
       `af`.`referrer`                                                    AS `referrer`,
       `m3`.`user_id`                                                     AS `designer_id`,
       `o`.`created_at`                                                   AS `created_at`,
       `atdates`.`accepted_date`                                          AS `accepted_at`,
       `oh2`.`created_at`                                                 AS `ready_sent_at`,
       `atdates`.`delivered_at`                                           AS `delivered_at`,
       `atdates`.`shipped_at`                                             AS `shipped_at`,
       `atdates`.`received_at`                                            AS `received_at`,
       `geoc`.`iso_code`                                                  AS `to_country`,
       `shipto`.`city`                                                    AS `to_city`,
       `psp`.`id`                                                         AS `printer_id`,
       `psp`.`title`                                                      AS `printer_title`,
       `geoc2`.`iso_code`                                                 AS `ps_country`,
       `dt`.`title`                                                       AS `delivery_type`,
       concat(`atdl`.`tracking_number`, ' - ', `atdl`.`tracking_shipper`) AS `delivery_opts`
from ((((((((((((((((`store_order` `o` left join `store_order_attemp` `attemp` on ((`attemp`.`id` = `o`.`current_attemp_id`))) left join `store_order_attemp_dates` `atdates` on ((`atdates`.`attemp_id` = `attemp`.`id`))) left join `store_order_attemp_delivery` `atdl` on ((`atdl`.`order_attemp_id` = `attemp`.`id`))) left join `affiliate_user_source` `af` on (((`af`.`user_id` = `o`.`user_id`) and (`af`.`type` = 'register')))) join `store_order_item` `soi` on ((`soi`.`order_id` = `o`.`id`))) join `store_unit` `su` on ((`su`.`id` = `soi`.`unit_id`))) join `model3d` `m3` on ((`m3`.`id` = `su`.`model3d_id`))) join `user_address` `shipto` on ((`shipto`.`id` = `o`.`ship_address_id`))) join `geo_country` `geoc` on ((`geoc`.`id` = `shipto`.`country_id`))) left join `company_service` `psm` on ((`psm`.`id` = `attemp`.`machine_id`))) left join `ps_printer` `psp` on ((`psp`.`id` = `psm`.`ps_printer_id`))) left join `user_location` `l` on ((`l`.`id` = `psm`.`location_id`))) left join `geo_country` `geoc2` on ((`geoc2`.`id` = `l`.`country_id`))) left join `store_order_history` `oh2` on ((
    (`oh2`.`order_id` = `o`.`id`) and (`oh2`.`action_id` = 'order_status') and
    (`oh2`.`comment` = 'old[printed] new[ready_send]')))) left join `delivery_type` `dt` on ((`dt`.`id` = `o`.`delivery_type_id`)))
       left join `store_order_history` `oh3`
                 on (((`oh3`.`order_id` = `o`.`id`) and (`oh3`.`action_id` = 'order_status') and
                      (`oh3`.`comment` = 'old[printed] new[ready_send]'))))
where (`o`.`user_id` <> 1);");
    }
}
