<?php

use yii\db\Migration;

class m160919_144037_2693_partproperties_alter extends Migration
{
    public function up()
    {
        $this->execute(
            'ALTER TABLE `model3d_part_properties` 
            CHANGE COLUMN `length` `length` DECIMAL(7,3) NOT NULL ,
            CHANGE COLUMN `width` `width` DECIMAL(7,3) NOT NULL ,
            CHANGE COLUMN `height` `height` DECIMAL(7,3) NOT NULL ;
        '
        );

        $this->execute('ALTER TABLE `model3d_part` ADD COLUMN `scaled` INT(5) NULL DEFAULT NULL AFTER `rotated_z`;');
        $this->execute('ALTER TABLE `model3d_replica_part` ADD COLUMN `scaled` INT(5) NULL DEFAULT NULL AFTER `rotated_z`;');
    }

    public function down()
    {
        $this->execute(
            'ALTER TABLE `model3d_part_properties` 
            CHANGE COLUMN `length` `length` DECIMAL(6,2) NOT NULL ,
            CHANGE COLUMN `width` `width` DECIMAL(6,2) NOT NULL ,
            CHANGE COLUMN `height` `height` DECIMAL(6,2) NOT NULL ;
        '
        );

        $this->dropColumn('model3d_part', 'scaled');
        $this->dropColumn('model3d_replica_part', 'scaled');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
