<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180524_162332_5584_help_admin_files
 */
class m180524_162332_5584_help_admin_files extends Migration
{
    public function addAdminFile($fileInfo)
    {
        $fields = [
            'uuid',
            'name',
            'path',
            'stored_name',
            'extension',
            'size',
            'created_at',
            'updated_at',
            'deleted_at',
            'user_id',
            'server',
            'status',
            'md5sum',
            'last_access_at',
            'ownerClass',
            'ownerField',
            'is_public',
            'path_version',
            'expire'
        ];
        $fieldKeys = array_combine($fields, $fields);
        $insertValue = array_intersect_key($fileInfo, $fieldKeys);
        $this->insert('file_admin', $insertValue);
    }


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('site_help', 'cover_file_uuid', 'varchar(32) NULL after cover_file_id');
        $this->addForeignKey('site_help_cover_file_fk', 'site_help', 'cover_file_uuid', 'file_admin', 'uuid');

        $siteHelps = (new Query())->select('*')->from('site_help')->where('cover_file_id is not null')->all();
        foreach ($siteHelps as $siteHelp) {
            $fileInfo = (new Query())->select('*')->from('file')->where(['id' => $siteHelp['cover_file_id']])->one();
            $this->addAdminFile($fileInfo);
            $this->update('site_help', ['cover_file_uuid' => $fileInfo['uuid']], 'id=' . $siteHelp['id']);
        }

        $this->dropForeignKey('fk_site_help_cover_file_idx', 'site_help');
        $this->dropColumn('site_help', 'cover_file_id');


        $this->addColumn('site_help_category', 'cover_file_uuid', 'varchar(32) NULL after cover_file_id');
        $this->addForeignKey('site_help_category_cover_file_fk', 'site_help_category', 'cover_file_uuid', 'file_admin', 'uuid');

        $siteCategoryHelps = (new Query())->select('*')->from('site_help_category')->where('cover_file_id is not null')->all();
        foreach ($siteCategoryHelps as $siteHelpCategory) {
            $fileInfo = (new Query())->select('*')->from('file')->where(['id' => $siteHelpCategory['cover_file_id']])->one();
            $this->addAdminFile($fileInfo);
            $this->update('site_help_category', ['cover_file_uuid' => $fileInfo['uuid']], 'id=' . $siteHelpCategory['id']);
        }

        $this->dropForeignKey('fk_site_help_category_cover_file_idx', 'site_help_category');
        $this->dropColumn('site_help_category', 'cover_file_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
