<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%wiki_material}}`.
 */
class m210505_110254_8549_add_show_widget_column_to_wiki_material_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%wiki_material}}', 'show_widget', $this->tinyInteger()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%wiki_material}}', 'show_widget');
    }
}
