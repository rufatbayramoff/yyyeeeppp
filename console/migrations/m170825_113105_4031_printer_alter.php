<?php

use yii\db\Migration;

class m170825_113105_4031_printer_alter extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `printer` 
            ADD COLUMN `image_file_id` INT(11) NULL AFTER `have_unfixed_size`,
            ADD COLUMN `images_json` JSON NULL AFTER `image_file_id`;');

        $this->execute('ALTER TABLE `printer` 
            ADD COLUMN `category_id` INT(11) NULL AFTER `images_json`;
            ');
        $this->execute('CREATE TABLE `category` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `title` varchar(145) NOT NULL,
          `parent_id` int(11) DEFAULT NULL,
          `description` mediumtext,
          `slug` varchar(45) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `fk_category_1_idx` (`parent_id`),
          CONSTRAINT `fk_category_1` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        );
         ');

        $this->execute('ALTER TABLE `printer` 
            ADD INDEX `fk_printer_1_idx1` (`category_id` ASC);
            ALTER TABLE `printer` 
            ADD CONSTRAINT `fk_printer_1`
              FOREIGN KEY (`category_id`)
              REFERENCES `category` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
            ');

        $this->execute('CREATE TABLE `category_intl` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `model_id` int(11) NOT NULL,
              `title` varchar(45) DEFAULT NULL,
              `description` mediumtext,
              `lang_iso` char(5) DEFAULT NULL,
              `is_active` tinyint(1) DEFAULT \'1\',
              PRIMARY KEY (`id`),
              UNIQUE KEY `index3` (`model_id`,`lang_iso`),
              KEY `fk_category_intl_1_idx` (`model_id`),
              CONSTRAINT `fk_category_intl_1` FOREIGN KEY (`model_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ;);
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('printer', 'image_file_id');
        $this->dropColumn('printer', 'images_json');
        $this->dropForeignKey('fk_printer_1', 'printer');
        $this->dropColumn('printer', 'category_id');
        $this->dropTable('category');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170825_113105_4031_printer_alter cannot be reverted.\n";

        return false;
    }
    */
}
