<?php

use yii\db\Migration;

class m161026_131328_3041_payment_order_status extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `store_order` 
CHANGE COLUMN `payment_status` `payment_status` CHAR(25) NOT NULL DEFAULT \'new\' ;');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
