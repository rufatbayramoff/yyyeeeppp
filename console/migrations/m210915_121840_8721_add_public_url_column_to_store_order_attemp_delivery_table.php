<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%store_order_attemp_delivery}}`.
 */
class m210915_121840_8721_add_public_url_column_to_store_order_attemp_delivery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%store_order_attemp_delivery}}', 'public_url', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%store_order_attemp_delivery}}', 'public_url');
    }
}
