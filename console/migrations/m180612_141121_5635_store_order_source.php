<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180612_141121_5635_store_order_source
 */
class m180612_141121_5635_store_order_source extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('store_order', 'source', 'varchar(25) null');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('store_order', 'source');
    }
}
