<?php

use yii\db\Migration;

class m151102_134331_tbl_search_query extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `site_search_query` (
          `query` varchar(255) NOT NULL DEFAULT '',
          `qty` int(10) unsigned NOT NULL,
          PRIMARY KEY (`query`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function down()
    {
        echo "m151102_134331_tbl_search_query cannot be reverted.\n";
        $this->truncateTable('site_search_query');
        $this->dropTable('site_search_query');        
    }

}
