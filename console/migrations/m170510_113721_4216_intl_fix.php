<?php

use yii\db\Migration;

class m170510_113721_4216_intl_fix extends Migration
{
    public function up()
    {
        $this->dropColumn('printer_material_intl', 'filament_title');
    }

    public function down()
    {
        return false;
    }
}
