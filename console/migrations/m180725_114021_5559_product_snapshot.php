<?php

use yii\db\Migration;

/**
 * Class m180725_114021_5559_product_snapshot
 */
class m180725_114021_5559_product_snapshot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            'CREATE TABLE `product_snapshot` (
  `uuid` varchar(32) NOT NULL,
  `product_uuid` varchar(32) NOT NULL,
  `description` json NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `product_uuid` (`product_uuid`),
  CONSTRAINT `fk_product_shapshot_product_uuid` FOREIGN KEY (`product_uuid`) REFERENCES `product` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'
        );

        $this->execute(
            'CREATE TABLE `product_snapshot_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_snapshot_uuid` varchar(32) NOT NULL,
  `file_uuid` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_snapshot_uuid` (`product_snapshot_uuid`),
  KEY `file_uuid` (`file_uuid`),
  CONSTRAINT `fk_product_snapshot_file_file_uuid` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`),
  CONSTRAINT `fk_product_snapshot_file_product_snapshot_uuid` FOREIGN KEY (`product_snapshot_uuid`) REFERENCES `product_snapshot` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            '
        );
        $this->addColumn('preorder', 'product_snapshot_uuid', 'varchar(32) null');
        $this->createIndex('preorder_product_snapshot_indx', 'preorder', 'product_snapshot_uuid');
        $this->addForeignKey('fk_preorder_product_snapshot', 'preorder', 'product_snapshot_uuid', 'product_snapshot', 'uuid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_preorder_product_snapshot', 'preorder');
        $this->dropIndex('preorder_product_snapshot_indx', 'preorder');
        $this->dropColumn('preorder', 'product_snapshot_uuid');
        $this->dropTable('product_snapshot_file');
        $this->dropTable('product_snapshot');
    }
}
