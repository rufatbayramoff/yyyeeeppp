<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%store_order_delivery}}`.
 */
class m201214_065603_7094_add_is_express_column_to_store_order_delivery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%store_order_delivery}}', 'is_express', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%store_order_delivery}}', 'is_express');
    }
}
