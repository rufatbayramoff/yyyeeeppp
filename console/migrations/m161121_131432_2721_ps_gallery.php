<?php

use yii\db\Migration;

class m161121_131432_2721_ps_gallery extends Migration
{
    public function up()
    {
        $this->addColumn('ps', 'picture_file_ids', 'TEXT default NULL');
    }

    public function down()
    {
        $this->dropColumn('ps', 'picture_file_ids');
        return true;
    }
}
