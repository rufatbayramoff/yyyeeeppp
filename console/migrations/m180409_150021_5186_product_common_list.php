<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180409_150021_5186_product_common_list
 */
class m180409_150021_5186_product_common_list extends Migration
{
    public function safeUp()
    {
        $this->addColumn('model3d', 'uuid', 'varchar(32) not NULL DEFAULT "" after id');
        $this->createIndex('model3d_indx', 'model3d', 'uuid');

        $this->execute("
CREATE TABLE `product_bind` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_uuid` varchar(32) NULL,
  `model3d_uuid` varchar(32) NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `model3d_uuid` (`model3d_uuid`),
  UNIQUE KEY `product_uuid_indx` (`product_uuid`) USING BTREE,
  CONSTRAINT `fk_product_bind_model3d` FOREIGN KEY (`model3d_uuid`) REFERENCES `model3d` (`uuid`),
  CONSTRAINT `fk_product_bind_product` FOREIGN KEY (`product_uuid`) REFERENCES `product` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

        $models = (new Query())->select('id')->from('model3d')->all();
        foreach ($models as $model3d) {
            $uuid = \common\components\UuidHelper::generateUuid();
            $this->update('model3d', ['uuid' => $uuid],'id=' . $model3d['id']);
            $this->insert('product_bind', ['model3d_uuid' => $uuid]);
        }

        $this->execute(
            "
ALTER TABLE `model3d` 
DROP INDEX `model3d_indx` ,
ADD UNIQUE INDEX `model3d_indx` (`uuid` ASC);
            "
        );
    }

    public function safeDown()
    {
        $this->dropColumn('model3d', 'uuid');
        $this->dropTable('product_bind');
    }
}
