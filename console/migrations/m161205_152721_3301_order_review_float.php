<?php

use yii\db\Migration;

class m161205_152721_3301_order_review_float extends Migration
{
    public function up()
    {
        $this->alterColumn('store_order_review', 'rating_speed', 'DECIMAL(5,2) NOT NULL DEFAULT "0"');
        $this->alterColumn('store_order_review', 'rating_quality', 'DECIMAL(5,2) NOT NULL DEFAULT "0"');
        $this->alterColumn('store_order_review', 'rating_communication', 'DECIMAL(5,2) NOT NULL DEFAULT "0"');
    }

    public function down()
    {
        return true;
    }
}
