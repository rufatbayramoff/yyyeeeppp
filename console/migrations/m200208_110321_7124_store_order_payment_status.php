<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m200208_110321_7124_store_order_payment_status
 */
class m200208_110321_7124_store_order_payment_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('store_order', 'payment_status');
        $this->update('payment_invoice', ['status'=>'new'], ['status'=>'pending']);
        $this->update('payment_invoice', ['status'=>'canceled'], ['status'=>'authorization_expired']);
        $this->update('payment_invoice', ['status'=>'canceled'], ['status'=>'settlement_declined']);
    }

    public function safeDown()
    {

    }
}