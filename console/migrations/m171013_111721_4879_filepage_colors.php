<?php

use yii\db\Migration;

class m171013_111721_4879_filepage_colors extends Migration
{
    public function safeUp()
    {
        $this->dropTable('filepage_cache_color');
        $this->truncateTable('filepage_cache');
    }

    public function safeDown()
    {
        return false;
    }
}
