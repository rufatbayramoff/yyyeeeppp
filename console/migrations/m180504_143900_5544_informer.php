<?php

use yii\db\Migration;

/**
 * Class m180504_143900_5544_informer
 */
class m180504_143900_5544_informer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `user_informer` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `key` char(45) NOT NULL,
          `user_id` int(11) NOT NULL,
          `created_at` datetime NOT NULL,
          `count` tinyint(4) NOT NULL DEFAULT \'1\',
          `config` json DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `key_UNIQUE` (`key`),
          KEY `fk_user_informer_1_idx` (`user_id`),
          CONSTRAINT `fk_user_informer_1` FOREIGN KEY (`user_id`) REFERENCES `user_informer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_informer');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180504_143900_5544_informer cannot be reverted.\n";

        return false;
    }
    */
}
