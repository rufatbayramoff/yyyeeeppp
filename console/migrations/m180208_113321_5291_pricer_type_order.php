<?php

use yii\db\Migration;

/**
 * Class m180208_113321_5291_pricer_type_order
 */
class m180208_113321_5291_pricer_type_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('store_pricer_type', 'checkout_order', 'INT(11) NOT NULL DEFAULT 0');
        $this->update('store_pricer_type', ['checkout_order' => '1'], "type='ps_print'");
        $this->update('store_pricer_type', ['checkout_order' => '2'], "type='fee'");
        $this->update('store_pricer_type', ['checkout_order' => '3'], "type='shipping'");
        $this->update('store_pricer_type', ['checkout_order' => '4'], "type='package'");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('store_pricer_type', 'checkout_order');
    }

}
