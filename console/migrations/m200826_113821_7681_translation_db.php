<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\modules\translation\components\DbI18n;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200811_174421_7472_message_bot
 */
class m200826_113821_7681_translation_db extends Migration
{
    protected $existsLangs;

    protected function fixIntlTable($tableName)
    {
        $table   = $this->db->schema->getTableSchema($tableName); //returns array of tbl schema's
        $foreignKeys = $table->foreignKeys;
        $columns = $table->getColumnNames();
        $langIsoFkExists = false;
        foreach ($foreignKeys as $fkName => $fkDescription) {
            if (strpos($fkName, 'lang_iso')) {
                $langIsoFkExists = true;
                break;
            }
        }
        if (!$langIsoFkExists) {
            if (!$this->existsLangs) {
                $this->existsLangs = (new Query())->select('*')->from('system_lang')->all();
                $this->existsLangs = \common\components\ArrayHelper::getColumn($this->existsLangs, 'iso_code');
            }
            $this->delete($tableName, ['not in', 'lang_iso' , $this->existsLangs]);

            $this->addForeignKey('fk_' . $tableName . '_lang_iso', $tableName, 'lang_iso', 'system_lang', 'iso_code', 'CASCADE', 'CASCADE');
        }

        foreach ($columns as $column) {
            if (in_array($column, DbI18n::getTechnialColumnsList())) {
                continue;
            };
            $columnInfo = $table->getColumn($column);
            $columnType = $columnInfo->dbType.' NULL';
            $this->alterColumn($tableName, $column, $columnType);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `translation_db` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(250) NOT NULL,
  `column_name` varchar(250) NOT NULL,
  `line_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `translated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `table` (`table_name`),
  KEY `column` (`column_name`),
  KEY `translated_by` (`translated_by`),
  CONSTRAINT `fk_translation_db_translated_by` FOREIGN KEY (`translated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

');

        $this->addColumn('system_lang_message', 'updated_at', 'DATETIME');
        $this->addColumn('system_lang_message', 'translated_by', 'INT(11)');
        $this->createIndex('system_lang_message_indx', 'system_lang_message', 'translated_by');
        $this->addForeignKey('fk_system_lang_message', 'system_lang_message', 'translated_by', 'user', 'id', 'SET NULL', 'CASCADE');


        $tables     = $this->db->schema->getTableNames();
        foreach ($tables as $tableName) {
            if (strpos($tableName, '_intl')) {
                $this->fixIntlTable($tableName);
            }
        }


    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}