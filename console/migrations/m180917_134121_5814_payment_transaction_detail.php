<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180917_134121_5814_payment_transaction_detail
 *
 * Trying set links to every payment_vendor_transaction to payment_detail
 */
class m180917_134121_5814_payment_transaction_detail extends Migration
{
    protected function generateInvoiceUuid()
    {

        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_invoice')->where('uuid="' . $operationUuid . '"')->one();
        } while ($checkUuid);
        return $operationUuid;
    }

    protected function generatePaymentDetailOperationUuid()
    {
        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_detail_operation')->where('uuid="' . $operationUuid . '"')->one();
        } while ($checkUuid);
        return $operationUuid;
    }

    protected function getInvoiceForOrder($order)
    {
        $invoice = (new Query())->select('*')->from('payment_invoice')->where(['order_id' => $order['id']])->one();
        if ($invoice) {
            return $invoice;
        }
        die('Not found invoice for order: ' . $order['id']);
    }

    protected function getOrCreatePaymentForInvoice($invoice)
    {
        $payment = (new Query())->select('*')->from('payment')->where(['payment_invoice_uuid' => $invoice['uuid']])->one();
        if ($payment) {
            return $payment;
        }
        $payment = [
            'payment_invoice_uuid' => $invoice['uuid'],
            'created_at'           => \common\components\DateHelper::now(),
            'description'          => 'Technical payment for order: ' . $invoice['order_id'],
            'status'               => 'cancelled',
        ];
        $this->insert('payment', $payment);
        $payment['id'] = $this->db->getLastInsertID();
        return $payment;
    }

    protected function createPaymentOperation($payment)
    {
        $paymentDetailOperation = [
            'uuid'       => $this->generatePaymentDetailOperationUuid(),
            'payment_id' => $payment['id'],
            'created_at' => \common\components\DateHelper::now()
        ];
        $this->insert('payment_detail_operation', $paymentDetailOperation);
        return $paymentDetailOperation;
    }

    protected function formDoubleEntryBrainTreeFailed($paymentTransaction)
    {
        if ($paymentTransaction['status'] !== 'failed') {
            echo "Vendor: Braintree\n";
            var_dump($paymentTransaction);
            exit;
        }
        $order = (new Query())->select('*')->from('store_order')->where(['id' => $paymentTransaction['order_id']])->one();
        $invoice = $this->getInvoiceForOrder($order);
        $payment = $this->getOrCreatePaymentForInvoice($invoice);
        $paymentDetailOperation = $this->createPaymentOperation($payment);
        // Fix double entry
        $paymentDetail = [
            'user_id'                       => 160,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed braintree payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 160,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed braintree payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail['id'] = $this->db->getLastInsertID();
        return $paymentDetail;
    }

    protected function fixPaymentFirstDetailId($paymentDetail, $paymentTransaction, $histories)
    {
        echo 'Payment detail: ' . $paymentDetail['id'] . ' Order id: ' . $paymentTransaction['order_id'] . ' Transaction id: ' . $paymentTransaction['id'] . "\n";
        $firstHistory = reset($histories);
        if($firstHistory){
            $this->update('payment_transaction_history', ['payment_detail_id' => $paymentDetail['id']], 'id=' . $firstHistory['id']);
        }
        $this->update('payment_transaction', ['first_payment_detail_id' => $paymentDetail['id']], 'id=' . $paymentTransaction['id']);
    }

    protected function linkPaymentBraintree($paymentTransaction, $histories): bool
    {
        $paymentDetail = (new Query())->select('*')->from('payment_detail')->where(['transaction_id' => $paymentTransaction['id'], 'user_id' => 160])->one();
        if ($paymentDetail) {
            // Direct bind exists
            echo 'Direct: ' . $paymentTransaction['order_id'] . "\n";
        } else {
            $paymentDetailQuery = (new Query())->select('payment_detail.*')->from('payment_detail')
                ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                ->where([
                    'payment_invoice.order_id' => $paymentTransaction['order_id'],
                    'payment_detail.user_id'   => 160, // Select payment detail for user braintree
                    'amount'                   => '-' . $paymentTransaction['amount']
                ]);
            $paymentDetail = $paymentDetailQuery->one();
            if ($paymentDetail) {
                echo 'Bind via order: ' . $paymentTransaction['order_id'] . "\n";
            } else {
                // Payment detail  may be for additional service
                $paymentDetail = (new Query())->select('payment_detail.*')->from('payment_detail')
                    ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                    ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                    ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                    ->where([
                        'payment_invoice.order_id' => $paymentTransaction['order_id'],
                        'payment_detail.user_id'   => 110, // Select payment detail for user braintree
                        'amount'                   => '-' . $paymentTransaction['amount']
                    ])->andWhere('payment_detail.description like "Additional services%"')->one();
                if ($paymentDetail) {
                    echo 'Bind via additional service order: ' . $paymentTransaction['order_id'] . "\n";
                } else {
                    // Payment detail  may be for additional service
                    $paymentDetail = (new Query())->select('payment_detail.*')->from('payment_detail')
                        ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                        ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                        ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                        ->where([
                            'payment_invoice.order_id' => $paymentTransaction['order_id'],
                            'payment_detail.user_id'   => 110, // Select payment detail for user braintree
                            'amount'                   => $paymentTransaction['amount'] // Inverted amount!!!
                        ])->andWhere('payment_detail.description like "Additional services%"')->one();
                    if ($paymentDetail) {
                        echo "Bind via inverted additional service order: " . $paymentTransaction['order_id'] . "\n";
                    } else {
                        if ($paymentTransaction['type'] === 'refund') {
                            $paymentLinkedTransaction = (new Query())->select('payment_transaction.*')->from('payment_transaction')
                                ->where([
                                    'payment_transaction.refund_id' => $paymentTransaction['id'],
                                ])->one();
                        }
                    }
                }
            }
        }
        if ($paymentDetail) {
            $this->fixPaymentFirstDetailId($paymentDetail, $paymentTransaction, $histories);
            return true;
        }
        if ($paymentTransaction['status'] === 'failed') {
            $paymentDetail = $this->formDoubleEntryBrainTreeFailed($paymentTransaction);
            echo "Created double entry: " . $paymentDetail['payment_detail_operation_uuid'] . "\n";
            $this->fixPaymentFirstDetailId($paymentDetail, $paymentTransaction, $histories);
            return true;
        }
        echo 'Not bind payment transaction: ' . $paymentTransaction['id'] . ' Order id: ' . $paymentTransaction['order_id'] . "\n";
        exit;

    }

    protected function linkPaymentPaypal($paymentTransaction, $histories): bool
    {
        echo "\nSkip Paypal";
        exit;
    }

    protected function formDoubleEntryTsFailed($paymentTransaction, $order)
    {
        if ($paymentTransaction['status'] != 'authorized') {
            echo "formDoubleEntryThingiverseFailed";
            var_dump($paymentTransaction);
            exit;
        }
        $invoice = $this->getInvoiceForOrder($order);
        $payment = $this->getOrCreatePaymentForInvoice($invoice);
        $paymentDetailOperation = $this->createPaymentOperation($payment);
        // Fix double entry
        $paymentDetail = [
            'user_id'                       => 1,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed ts payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 1,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed ts payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail['id'] = $this->db->getLastInsertID();
        return $paymentDetail;
    }

    protected function linkPaymentTs($paymentTransaction, $histories): bool
    {
        $paymentDetail = (new Query())->select('*')->from('payment_detail')->where(['transaction_id' => $paymentTransaction['id']])->one();
        if ($paymentDetail) {
            // Direct bind exists
            echo 'Direct Ts: ' . $paymentTransaction['order_id'] . "\n";
        } else {
            $paymentDetailQuery = (new Query())->select('payment_detail.*')->from('payment_detail')
                ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                ->where([
                    'payment_invoice.order_id' => $paymentTransaction['order_id'],
                    'payment_detail.user_id'   => $paymentTransaction['user_id'], // Select payment detail for user thingiverse
                    'amount'                   => -$paymentTransaction['amount']
                ])->orderBy('payment_detail.updated_at');
            $paymentDetail = $paymentDetailQuery->one();
            if ($paymentDetail) {
                echo 'Bind Ts via order: ' . $paymentTransaction['order_id'] . "\n";
            } else {
                $order = (new Query())->select('*')->from('store_order')->where(['id' => $paymentTransaction['order_id']])->one();
                if ($paymentTransaction['amount'] == 0 && $order['user_id'] == 1) {
                    // Is test order
                    $paymentDetailQuery = (new Query())->select('payment_detail.*')->from('payment_detail')
                        ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                        ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                        ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                        ->where([
                            'payment_invoice.order_id' => $paymentTransaction['order_id'],
                            'payment_detail.user_id'   => 1, // Select payment detail for user thingiverse
                            'amount'                   => 0
                        ])->orderBy('payment_detail.updated_at');
                    $paymentDetail = $paymentDetailQuery->one();
                    if (!$paymentDetail) {
                        $paymentDetail = $this->formDoubleEntryTsFailed($paymentTransaction, $order);
                    }
                }
            }
        }
        if ($paymentDetail) {
            $this->fixPaymentFirstDetailId($paymentDetail, $paymentTransaction, $histories);
            return true;
        }
        echo 'Not bind Ts payment transaction: ' . $paymentTransaction['id'] . ' Order id: ' . $paymentTransaction['order_id'] . "\n";
        exit;
    }

    protected function formDoubleEntryThingiverseFailed($paymentTransaction, $order)
    {
        if ($paymentTransaction['status'] != 'authorized') {
            echo "formDoubleEntryThingiverseFailed";
            var_dump($paymentTransaction);
            exit;
        }
        $invoice = $this->getInvoiceForOrder($order);
        $payment = $this->getOrCreatePaymentForInvoice($invoice);
        $paymentDetailOperation = $this->createPaymentOperation($payment);
        // Fix double entry
        $paymentDetail = [
            'user_id'                       => 112,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed thingiverse payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 112,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed thingiverse payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail['id'] = $this->db->getLastInsertID();
        return $paymentDetail;

    }

    protected function linkPaymentBankInvoice($paymentTransaction, $histories): bool
    {
        $paymentDetail = (new Query())->select('*')->from('payment_detail')->where(['transaction_id' => $paymentTransaction['id']])->one();
        if ($paymentDetail) {
            // Direct bind exists
            echo 'Direct BankInvoice: ' . $paymentTransaction['order_id'] . "\n";
        } else {
            $paymentDetailQuery = (new Query())->select('payment_detail.*')->from('payment_detail')
                ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                ->where([
                    'payment_invoice.order_id' => $paymentTransaction['order_id'],
                    'payment_detail.user_id'   => 110, // Select payment detail for user treatstock
                    'amount'                   => $paymentTransaction['amount']
                ])->orderBy('payment_detail.updated_at');
            $paymentDetail = $paymentDetailQuery->one();
            if ($paymentDetail) {
                echo 'Bind Thingiverse via order: ' . $paymentTransaction['order_id'] . "\n";
            }
        }
        if ($paymentDetail) {
            $this->fixPaymentFirstDetailId($paymentDetail, $paymentTransaction, $histories);
            return true;
        }
        echo 'Not bind Bank Transaction ' . $paymentTransaction['id'] . ' Order id: ' . $paymentTransaction['order_id'] . "\n";
        exit;
    }

    protected function linkPaymentThingiverse($paymentTransaction, $histories): bool
    {
        $paymentDetail = (new Query())->select('*')->from('payment_detail')->where(['transaction_id' => $paymentTransaction['id']])->one();
        if ($paymentDetail) {
            // Direct bind exists
            echo 'Direct Thingiverse: ' . $paymentTransaction['order_id'] . "\n";
        } else {
            $paymentDetailQuery = (new Query())->select('payment_detail.*')->from('payment_detail')
                ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                ->where([
                    'payment_invoice.order_id' => $paymentTransaction['order_id'],
                    'payment_detail.user_id'   => 112, // Select payment detail for user thingiverse
                    'amount'                   => $paymentTransaction['amount']
                ])->orderBy('payment_detail.updated_at');
            $paymentDetail = $paymentDetailQuery->one();
            if ($paymentDetail) {
                echo 'Bind Thingiverse via order: ' . $paymentTransaction['order_id'] . "\n";
            }
        }
        if ($paymentDetail) {
            $this->fixPaymentFirstDetailId($paymentDetail, $paymentTransaction, $histories);
            return true;
        }
        $order = (new Query())->select('*')->from('store_order')->where(['id' => $paymentTransaction['order_id']])->one();
        if ($paymentTransaction['status'] === 'authorized' && $order['order_state'] === 'closed') {
            $paymentDetail = $this->formDoubleEntryThingiverseFailed($paymentTransaction, $order);
            echo 'Created Thingiverse double entry: ' . $paymentDetail['payment_detail_operation_uuid'] . "\n";
            $this->fixPaymentFirstDetailId($paymentDetail, $paymentTransaction, $histories);
            return true;
        }
        echo 'Not bind Thingiverse payment transaction: ' . $paymentTransaction['id'] . ' Order id: ' . $paymentTransaction['order_id'] . "\n";
        exit;
    }

    protected function linkPaymentType($paymentTransaction, $histories): bool
    {
        if ($paymentTransaction['vendor'] === 'braintree') {
            return $this->linkPaymentBraintree($paymentTransaction, $histories);
        } elseif ($paymentTransaction['vendor'] === 'paypal') {
            return $this->linkPaymentPaypal($paymentTransaction, $histories);
        } elseif ($paymentTransaction['vendor'] === 'ts') {
            return $this->linkPaymentTs($paymentTransaction, $histories);
        } elseif ($paymentTransaction['vendor'] === 'thingiverse') {
            return $this->linkPaymentThingiverse($paymentTransaction, $histories);
        } else {
            return $this->linkPaymentBankInvoice($paymentTransaction, $histories);
        }
    }

    protected function formDoubleEntryMinusFailed($paymentTransaction)
    {
        if ($paymentTransaction['status'] != 'canceled') {
            echo "formDoubleEntryMinusFailed";
            var_dump($paymentTransaction);
            exit;
        }
        $payment = [
            'payment_invoice_uuid' => null,
            'created_at'           => \common\components\DateHelper::now(),
            'description'          => 'Technical failed payment for payout transaction: ' . $paymentTransaction['id'],
            'status'               => 'cancelled',
        ];
        $this->insert('payment', $payment);
        $payment['id'] = $this->db->getLastInsertID();

        $paymentDetailOperation = $this->createPaymentOperation($payment);
        // Fix double entry
        $paymentDetail = [
            'user_id'                       => 150,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed payout payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payout'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 150,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed payout payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payout'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail['id'] = $this->db->getLastInsertID();
        return $paymentDetail;
    }

    protected function formDoubleEntryMinusHold($paymentTransaction)
    {
        echo "formDoubleEntryMinusHold";
        var_dump($paymentTransaction);
        exit;
        $payment = [
            'payment_invoice_uuid' => null,
            'created_at'           => \common\components\DateHelper::now(),
            'description'          => 'Technical hold payment for payout transaction: ' . $paymentTransaction['id'],
            'status'               => 'cancelled',
        ];
        $this->insert('payment', $payment);
        $payment['id'] = $this->db->getLastInsertID();

        $paymentDetailOperation = $this->createPaymentOperation($payment);
        // Fix double entry
        $paymentDetail = [
            'user_id'                       => $paymentTransaction['user_id'],
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Hold payout payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payout'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 150,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Hold payout payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payout'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail['id'] = $this->db->getLastInsertID();
        return $paymentDetail;
    }

    protected function formDoubleEntryMinusSuccess($paymentTransaction)
    {
        echo "Vendor: Minus\n";
        var_dump($paymentTransaction);
        exit;
        $payment = [
            'payment_invoice_uuid' => null,
            'created_at'           => \common\components\DateHelper::now(),
            'description'          => 'Technical success payment for payout transaction: ' . $paymentTransaction['id'],
            'status'               => 'cancelled',
        ];
        $this->insert('payment', $payment);
        $payment['id'] = $this->db->getLastInsertID();

        $paymentDetailOperation = $this->createPaymentOperation($payment);
        // Fix double entry
        $paymentDetail = [
            'user_id'                       => $paymentTransaction['user_id'],
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Success payout payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payout'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 150,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Success payout payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payout'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail['id'] = $this->db->getLastInsertID();
        return $paymentDetail;
    }

    protected function linkMinusType($paymentTransaction, $histories): bool
    {
        $paymentDetail = (new Query())->select('*')->from('payment_detail')->where(['transaction_id' => $paymentTransaction['id']])->one();
        if ($paymentDetail) {
            // Direct bind exists
            echo "Direct Minus Type: " . $paymentTransaction['id'] . "\n";
        } else {
            $paymentDetails = (new Query())->select('*')->from('payment_detail')
                ->where(['type' => 'payout', 'user_id' => $paymentTransaction['user_id'], 'amount' => -$paymentTransaction['amount']])
                ->all(); // Direct via user and amount
            if (count($paymentDetails) == 1) {
                $paymentDetail = reset($paymentDetails);
            } elseif (count($paymentDetail) > 1) {
                echo "Sevral possible link values\n";
                exit;
            } else {
                $paymentDetails = (new Query())->select('*')->from('payment_detail')
                    ->where(['type' => 'payment', 'user_id' => $paymentTransaction['user_id'], 'amount' => -$paymentTransaction['amount']])
                    ->all(); // Incorrect payment type
                if (count($paymentDetails) == 1) {
                    $paymentDetail = reset($paymentDetails);
                    // Check send to paypal
                    $paymentDetailsTo = (new Query())->select('*')->from('payment_detail')
                        ->where([
                            'type'                          => 'payment',
                            'payment_detail_operation_uuid' => $paymentDetail['payment_detail_operation_uuid'],
                            'amount'                        => $paymentTransaction['amount']
                        ])
                        ->all();
                    if (count($paymentDetailsTo) != 1) {
                        die('Invalid payout to');
                    }
                    $paymentTo = reset($paymentDetailsTo);
                    if ($paymentTo['user_id'] != 150) {
                        die('Payout not to paypal');
                    }
                } elseif (count($paymentDetail) > 1) {
                    echo "Sevral possible link values\n";
                    exit;
                } else {
                    if ($paymentTransaction['status'] === 'FAILED' || $paymentTransaction['status'] === 'canceled') {
                        $paymentDetail = $this->formDoubleEntryMinusFailed($paymentTransaction);
                        echo 'Created Minus double entry: ' . $paymentDetail['payment_detail_operation_uuid'] . "\n";
                    } elseif ($paymentTransaction['status'] === 'UNCLAIMED' || $paymentTransaction['status'] === 'ONHOLD' || $paymentTransaction['status'] === 'pending') { // Ps does not confirm payment
                        $paymentDetail = $this->formDoubleEntryMinusHold($paymentTransaction);
                    } elseif ($paymentTransaction['status'] === 'SUCCESS') {
                        //$paymentDetail = $this->formDoubleEntryMinusSuccess($paymentTransaction);
                    }
                }
            }
        }
        if ($paymentDetail) {
            $this->fixPaymentFirstDetailId($paymentDetail, $paymentTransaction, $histories);
            return true;
        }
        echo 'Not bind Payout payment transaction: ' . $paymentTransaction['id'] . ' User id: ' . $paymentTransaction['user_id'] . "\n";
        exit;
    }

    protected function formDoubleEntryCustomRefund($paymentTransaction)
    {
        echo "formDoubleEntryCustomRefund";
        var_dump($paymentTransaction);
        exit;
        $payment = [
            'payment_invoice_uuid' => null,
            'created_at'           => \common\components\DateHelper::now(),
            'description'          => 'Technical success payment for refund transaction: ' . $paymentTransaction['id'],
            'status'               => 'new',
        ];
        $this->insert('payment', $payment);
        $payment['id'] = $this->db->getLastInsertID();

        $paymentDetailOperation = $this->createPaymentOperation($payment);
        // Fix double entry
        $paymentDetail = [
            'user_id'                       => 110,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Success refund payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 160,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Success refund payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail['id'] = $this->db->getLastInsertID();
        return $paymentDetail;
    }

    protected function linkRefundType($paymentTransaction, $histories): bool
    {
        $paymentDetail = (new Query())->select('*')->from('payment_detail')->where(['transaction_id' => $paymentTransaction['id']])->one();
        if ($paymentDetail) {
            // Direct bind exists
            echo "Direct refund Type: " . $paymentTransaction['id'] . "\n";
        } elseif ($paymentTransaction['vendor'] === 'braintree' && $paymentTransaction['type'] === 'refund') {
            $paymentLinkedTransaction = (new Query())->select('payment_transaction.*')->from('payment_transaction')
                ->where([
                    'payment_transaction.refund_id' => $paymentTransaction['transaction_id'],
                ])->one();
            if ($paymentLinkedTransaction) {
                echo 'Payment linked transaction: ' . $paymentLinkedTransaction['id'] . " Order id: ".$paymentLinkedTransaction['order_id']."\n";
                $paymentDetailQuery = (new Query())->select('payment_detail.*')->from('payment_detail')
                    ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                    ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                    ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                    ->where([
                        'payment_invoice.order_id'   => $paymentLinkedTransaction['order_id'],
                        'payment_detail.user_id'     => 110, // Select payment detail for user thingiverse
                        'amount'                     => $paymentTransaction['amount'],
                        'payment_detail.description' => 'Reversal payment for the unused shipping label'
                    ])
                    ->orderBy('payment_detail.updated_at');
                $paymentDetails = $paymentDetailQuery->all();
                if (count($paymentDetails) == 0) {
                    $paymentDetailQuery = (new Query())->select('payment_detail.*')->from('payment_detail')
                        ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                        ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                        ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                        ->where([
                            'payment_invoice.order_id'   => $paymentLinkedTransaction['order_id'],
                            'payment_detail.user_id'     => 160, // Select payment detail for user thingiverse
                            'amount'                     => $paymentTransaction['amount'],
                            'payment_detail.description' => 'Partial refund. Additional Service refund',
                            'transaction_id'             => $paymentLinkedTransaction['id'],
                            'type'                       => 'payment'
                        ])
                        ->orderBy('payment_detail.updated_at');
                    $paymentDetails = $paymentDetailQuery->all();
                    if (count($paymentDetails) === 0) {
                        if ($paymentTransaction['id'] == 13199) {
                            // Manual direct fix
                            $paymentDetail = \common\models\PaymentDetail::tryFindByPk(76212);
                        } else {
                             // Any in order with same sum
                            $paymentDetailQuery = (new Query())->select('payment_detail.*')->from('payment_detail')
                                ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                                ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                                ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                                ->where([
                                    'payment_invoice.order_id' => $paymentLinkedTransaction['order_id'],
                                    'amount'                   => $paymentTransaction['amount'],
                                ])
                                ->orderBy('payment_detail.updated_at');
                            $paymentDetail = $paymentDetailQuery->one();
                            if (!$paymentDetail) {
                                // Any in order to braintree
                                $paymentDetailQuery = (new Query())->select('payment_detail.*')->from('payment_detail')
                                    ->leftJoin('payment_detail_operation', '`payment_detail_operation`.`uuid`=`payment_detail`.`payment_detail_operation_uuid`')
                                    ->leftJoin('payment', '`payment_detail_operation`.`payment_id`=`payment`.id')
                                    ->leftJoin('payment_invoice', '`payment`.`payment_invoice_uuid`=`payment_invoice`.`uuid`')
                                    ->where([
                                        'payment_invoice.order_id' => $paymentLinkedTransaction['order_id'],
                                        'payment_detail.user_id' => '160'
                                    ])
                                    ->orderBy('payment_detail.updated_at');
                                $paymentDetail = $paymentDetailQuery->one();
                            }
                        }
                    } elseif (count($paymentDetails) > 1) {
                        die('Invalid count paymnet refund');
                    } else {
                        $paymentDetail = reset($paymentDetails);
                    }
                } elseif (count($paymentDetails) > 1) {
                    die('Invalid count paymnet refund');
                } else {
                    $paymentDetail = reset($paymentDetails);
                }
            }
        } else {
            $paymentDetail = $this->formDoubleEntryCustomRefund($paymentTransaction);
        }

        if ($paymentDetail) {
            $this->fixPaymentFirstDetailId($paymentDetail, $paymentTransaction, $histories);
            return true;
        }
        echo 'Not bind refund payment transaction: ' . $paymentTransaction['id'] . ' User id: ' . $paymentTransaction['user_id'] . "\n";
        exit;
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Payment by breaintree
        $paymentTransactions = (new Query())->select('*')->from('payment_transaction')
            ->where('first_payment_detail_id is null and created_at>"2015-12-30" and vendor!="stripe"')->orderBy('created_at, id')->limit(10000000)->all();
        echo "\nTransactions without detail count: " . count($paymentTransactions) . "\n";

        $processed = 0;
        $skiped = 0;

        foreach ($paymentTransactions as $paymentTransaction) {
            $paymentTransactionHistories = (new Query())->select('*')->from('payment_transaction_history')->where(['transaction_id' => $paymentTransaction['id']])->orderBy('created_at, id')->all();
            echo "\n\nTransaction: " . $paymentTransaction['id'] . "\n";
            // $firstHistory = reset($paymentTransactionHistories); // Now it`s first but, may be not first
            if ($paymentTransaction['type'] === 'payment') {
                $result = $this->linkPaymentType($paymentTransaction, $paymentTransactionHistories);
            } elseif ($paymentTransaction['type'] === 'minus') {
                $result = $this->linkMinusType($paymentTransaction, $paymentTransactionHistories);
            } elseif ($paymentTransaction['type'] === 'refund') {
                $result = $this->linkRefundType($paymentTransaction, $paymentTransactionHistories);
            }
            if ($result) {
                $processed++;
            } else {
                $skiped++;
            }
        }
        echo "\nProcessed: " . $processed;
        echo "\nSkipped: " . $skiped;
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
