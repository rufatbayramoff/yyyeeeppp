<?php

use yii\db\Migration;

/**
 * Class m180808_140122_5599_preorder_work_fk
 */
class m180808_140122_5599_preorder_work_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('preorder_work_product_uuid_idx', 'preorder_work', 'product_uuid');
        $this->createIndex('preorder_work_company_service_idx', 'preorder_work', 'company_service_id');
        $this->addForeignKey('fk_preorder_work_product_uuid', 'preorder_work', 'product_uuid', 'product', 'uuid');
        $this->addForeignKey('fk_preorder_company_service_id', 'preorder_work', 'company_service_id', 'company_service', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_preorder_work_product_uuid', 'preorder_work');
        $this->dropForeignKey('fk_preorder_company_service_id', 'preorder_work');
        $this->dropIndex('preorder_work_product_uuid_idx', 'preorder_work');
        $this->dropIndex('preorder_work_company_service_idx', 'preorder_work');
    }
}
