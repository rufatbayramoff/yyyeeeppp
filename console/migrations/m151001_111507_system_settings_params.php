<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_111507_system_settings_params extends Migration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO `system_setting`
            (`group_id`,`key`,`value`,`created_at`,`updated_at`,`created_user_id`,`updated_user_id`,`description`,`json`,`is_active`) VALUES
            (9,'post_print_time','120','2015-10-01 09:51:09','2015-10-01 09:51:09',2,NULL,'Time for post print in minutes','',1);");
        $this->execute("INSERT INTO `system_setting`
            (`group_id`,`key`,`value`,`created_at`,`updated_at`,`created_user_id`,`updated_user_id`,`description`,`json`,`is_active`) VALUES
            (9,'deadline_expired','2880','2015-10-01 11:05:06','2015-10-01 11:05:06',2,NULL,'Deadline + time, when order  mark as expired, in minutes','',1);");
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM system_setting WHERE group_id = 9 AND `key` = 'post_print_time';");
        $this->execute("DELETE FROM system_setting WHERE group_id = 9 AND `key` = 'deadline_expired';");
    }
}
