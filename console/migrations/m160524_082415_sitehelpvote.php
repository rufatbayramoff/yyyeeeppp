<?php

use yii\db\Migration;

class m160524_082415_sitehelpvote extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `site_help_vote` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `site_help_id` int(11) NOT NULL,
                `vote_hash` varchar(145) DEFAULT NULL,
                `vote_answer` varchar(245) DEFAULT NULL,
                `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                UNIQUE KEY `vote_hash_UNIQUE` (`vote_hash`),
                KEY `fk_site_help_vote_1_idx` (`site_help_id`),
                CONSTRAINT `fk_site_help_vote_1` 
                    FOREIGN KEY (`site_help_id`) REFERENCES `site_help` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function down()
    {
        $this->truncateTable('site_help_vote');
        $this->dropTable('site_help_vote');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
