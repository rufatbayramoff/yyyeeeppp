<?php

use yii\db\Migration;

class m170516_090243_ps_printer_moder_log extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `moder_log` CHANGE COLUMN `result` `result` TEXT NULL DEFAULT NULL;");
    }

    public function down()
    {
        return true;
    }
}
