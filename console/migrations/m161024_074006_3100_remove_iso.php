<?php

use yii\db\Migration;

class m161024_074006_3100_remove_iso extends Migration
{
    public function up()
    {
        $this->execute('UPDATE site_help SET is_active=0 WHERE lang_iso!="en-US"');
        $this->execute('ALTER TABLE `site_help` 
            DROP COLUMN `lang_iso`,
            DROP INDEX `index4` ,
            ADD UNIQUE INDEX `index4` (`alias` ASC);
        ');
    }

    public function down()
    {
        $this->execute('ALTER TABLE `site_help` ADD COLUMN `lang_iso` VARCHAR(15) NULL AFTER `views`;');
        $this->execute('ALTER TABLE `site_help` 
            DROP INDEX `index4` ,
            ADD UNIQUE INDEX `index4` (`alias` ASC, `lang_iso` ASC);
        ');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
