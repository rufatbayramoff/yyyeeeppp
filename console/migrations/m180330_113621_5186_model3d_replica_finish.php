<?php

use yii\db\Schema;
use yii\db\Migration;

class m180330_113621_5186_model3d_replica_finish extends Migration
{
    public function up()
    {
        $this->dropColumn('model3d', 'status');
        $this->dropColumn('model3d', 'is_published');
        $this->dropColumn('model3d', 'is_hidden_in_store');
        $this->dropColumn('model3d_replica', 'status');
        $this->dropColumn('store_unit', 'status');
        $this->dropColumn('store_unit', 'is_hidden_in_store');
        $this->dropTable('store_units2category');
    }

    public function down()
    {
    }
}
