<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190607_172321_6603_max_orders_count
 */
class m190607_172321_6603_max_orders_count extends Migration
{
    public function up()
    {
        $this->insert(
            'system_setting',
            [
                'group_id'        => 13,
                'key'             => 'max_orders_per_printer_count_jc_company', // Just created company
                'value'           => 1,
                'created_at'      => date('Y-m-d H:i:s'),
                'updated_at'      => date('Y-m-d H:i:s'),
                'created_user_id' => null,
                'updated_user_id' => null,
                'description'     => 'This is max active orders count to print service. Same as "max_orders_per_printer_count" for just created company.'
            ]
        );
    }

    public function down()
    {
        $this->delete('system_setting', ['group']);
    }
}