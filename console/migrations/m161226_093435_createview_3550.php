<?php

use yii\db\Migration;

class m161226_093435_createview_3550 extends Migration
{
    public function up()
    {
        $this->execute('CREATE VIEW view_orderlist AS SELECT 
                    `o`.`id` AS `order_id`,
                    `o`.`user_id` AS `buyer_id`, `o`.`billed_at`,
                    `af`.`referrer` AS `referrer`,
                    `m3`.`user_id` AS `designer_id`,
                    `o`.`price_total` AS `price_total`,
                    `o`.`created_at` AS `created_at`,
                    `atdates`.`accepted_date` AS `accepted_at`,
                    `oh2`.`created_at` AS `ready_sent_at`,
                    `atdates`.`delivered_at` AS `delivered_at`,
                    `atdates`.`shipped_at` AS `shipped_at`,
                    `atdates`.`received_at` AS `received_at`,
                    `geoc`.`iso_code` AS `to_country`,
                    `shipto`.`city` AS `to_city`,
                    `psp`.`id` as `printer_id`,
                    `psp`.`title` as `printer_title`,
                    
                    `geoc2`.`iso_code` AS `ps_country`,
                    `dt`.`title` AS `delivery_type`,
                    CONCAT(`atdl`.`tracking_number`,
                            \' - \',
                            `atdl`.`tracking_shipper`) AS `delivery_opts`,
                    `pricer`.`price` AS `delivery_price`
                FROM
                    (((((((((((((((`store_order` `o`
                    LEFT JOIN `store_pricer` `pricer` ON (((`o`.`id` = `pricer`.`order_id`)
                        AND (`pricer`.`pricer_type` = 16))))
                    LEFT JOIN `store_order_attemp` `attemp` ON ((`attemp`.`id` = `o`.`current_attemp_id`)))
                    LEFT JOIN `store_order_attemp_dates` `atdates` ON ((`atdates`.`attemp_id` = `attemp`.`id`)))
                    LEFT JOIN `store_order_attemp_delivery` `atdl` ON ((`atdl`.`order_attemp_id` = `attemp`.`id`)))
                    LEFT JOIN `affiliate_user_source` `af` ON (((`af`.`user_id` = `o`.`user_id`)
                        AND (`af`.`type` = \'register\'))))
                    JOIN `store_order_item` `soi` ON ((`soi`.`order_id` = `o`.`id`)))
                    JOIN `store_unit` `su` ON ((`su`.`id` = `soi`.`unit_id`)))
                    JOIN `model3d` `m3` ON ((`m3`.`id` = `su`.`model3d_id`)))
                    JOIN `user_address` `shipto` ON ((`shipto`.`id` = `o`.`ship_address_id`)))
                    JOIN `geo_country` `geoc` ON ((`geoc`.`id` = `shipto`.`country_id`)))
                    LEFT JOIN `ps_printer` `psp` ON ((`psp`.`id` = `attemp`.`printer_id`)))
                    LEFT JOIN `user_location` `l` ON ((`l`.`id` = `psp`.`location_id`)))
                    LEFT JOIN `geo_country` `geoc2` ON ((`geoc2`.`id` = `l`.`country_id`)))
                    LEFT JOIN `store_order_history` `oh2` ON (((`oh2`.`order_id` = `o`.`id`)
                        AND (`oh2`.`action_id` = \'order_status\')
                        AND (`oh2`.`comment` = \'old[printed] new[ready_send]\'))))
                    LEFT JOIN `delivery_type` `dt` ON ((`dt`.`id` = `o`.`delivery_type_id`)))
                     LEFT JOIN `store_order_history` `oh3` ON (((`oh3`.`order_id` = `o`.`id`)
                        AND (`oh3`.`action_id` = \'order_status\')
                        AND (`oh3`.`comment` = \'old[printed] new[ready_send]\')))
                WHERE
                    `o`.`user_id` <> 1
         ');
    }

    public function down()
    {
        $this->execute('DROP VIEW `view_orderlist`');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
