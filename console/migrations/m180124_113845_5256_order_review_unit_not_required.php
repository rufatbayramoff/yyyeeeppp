<?php

use yii\db\Migration;

/**
 * Class m180124_113845_5256_order_review_unit_not_required
 */
class m180124_113845_5256_order_review_unit_not_required extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `store_order_review` 
            DROP FOREIGN KEY `fk_store_unit`;
            ALTER TABLE `store_order_review` 
            CHANGE COLUMN `store_unit_id` `store_unit_id` INT(11) NULL ;
            ALTER TABLE `store_order_review` 
            ADD CONSTRAINT `fk_store_unit`
              FOREIGN KEY (`store_unit_id`)
              REFERENCES `store_unit` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `store_order_review` 
            DROP FOREIGN KEY `fk_store_unit`;
            ALTER TABLE `store_order_review` 
            CHANGE COLUMN `store_unit_id` `store_unit_id` INT(11) NOT NULL ;
            ALTER TABLE `store_order_review` 
            ADD CONSTRAINT `fk_store_unit`
              FOREIGN KEY (`store_unit_id`)
              REFERENCES `store_unit` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");

        return true;
    }
}
