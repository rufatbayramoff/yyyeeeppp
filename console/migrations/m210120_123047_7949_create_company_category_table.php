<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_category}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%ps}}`
 * - `{{%company_service_category}}`
 */
class m210120_123047_7949_create_company_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_category}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'company_service_category_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `company_id`
        $this->createIndex(
            '{{%idx-company_category-company_id}}',
            '{{%company_category}}',
            'company_id'
        );

        // add foreign key for table `{{%ps}}`
        $this->addForeignKey(
            '{{%fk-company_category-company_id}}',
            '{{%company_category}}',
            'company_id',
            '{{%ps}}',
            'id',
            'CASCADE'
        );

        // creates index for column `company_service_category_id`
        $this->createIndex(
            '{{%idx-company_category-company_service_category_id}}',
            '{{%company_category}}',
            'company_service_category_id'
        );

        // add foreign key for table `{{%company_service_category}}`
        $this->addForeignKey(
            '{{%fk-company_category-company_service_category_id}}',
            '{{%company_category}}',
            'company_service_category_id',
            '{{%company_service_category}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%ps}}`
        $this->dropForeignKey(
            '{{%fk-company_category-company_id}}',
            '{{%company_category}}'
        );

        // drops index for column `company_id`
        $this->dropIndex(
            '{{%idx-company_category-company_id}}',
            '{{%company_category}}'
        );

        // drops foreign key for table `{{%company_service_category}}`
        $this->dropForeignKey(
            '{{%fk-company_category-company_service_category_id}}',
            '{{%company_category}}'
        );

        // drops index for column `company_service_category_id`
        $this->dropIndex(
            '{{%idx-company_category-company_service_category_id}}',
            '{{%company_category}}'
        );

        $this->dropTable('{{%company_category}}');
    }
}
