<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_125012_ps_fields extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `ps`
            CHANGE COLUMN `description` `description` TEXT NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `ps`
            CHANGE COLUMN `phone` `phone` VARCHAR(45) NOT NULL;");
    }

    public function safeDown()
    {
        $this->execute("ALTER TABLE `ps`
            CHANGE COLUMN `description` `description` TEXT NOT NULL;");
        $this->execute("ALTER TABLE `ps`
            CHANGE COLUMN `phone` `phone` VARCHAR(20) NOT NULL;");
    }
}
