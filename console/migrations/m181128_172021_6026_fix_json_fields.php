<?php

use yii\db\Migration;

/**
 * Class m181128_172021_6026_fix_json_fields
 */
class m181128_172021_6026_fix_json_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `store_order_delivery` CHANGE `ps_delivery_details` `ps_delivery_details` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');
        $this->execute('ALTER TABLE `store_order_delivery` CHANGE `ps_delivery_details` `ps_delivery_details` JSON NULL DEFAULT NULL;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
