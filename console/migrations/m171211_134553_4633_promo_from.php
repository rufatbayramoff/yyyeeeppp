<?php

use yii\db\Migration;

class m171211_134553_4633_promo_from extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `promocode` 
            ADD COLUMN `order_total_from` DECIMAL(6,2) NOT NULL DEFAULT 0 AFTER `is_valid`;
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('promocode', 'order_total_from');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_134553_4633_promo_from cannot be reverted.\n";

        return false;
    }
    */
}
