<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m190821_155721_6796_thingiverse_report
 */
class m190821_155721_6796_thingiverse_report extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('thingiverse_report', 'app_transaction_code', 'varchar(25) null');
        $this->addColumn('thingiverse_report', 'printing_fee', 'varchar(25) null');
        $this->addColumn('thingiverse_report', 'shipping', 'varchar(25) null');
        $this->addColumn('thingiverse_report', 'refund', 'varchar(25) null');
        $this->addColumn('thingiverse_report', 'treatstock_transaction_fee', 'varchar(25) null');
    }

    public function safeDown()
    {
        $this->dropColumn('thingiverse_report', 'app_transaction_code');
        $this->dropColumn('thingiverse_report', 'printing_fee');
        $this->dropColumn('thingiverse_report', 'shipping');
        $this->dropColumn('thingiverse_report', 'refund');
        $this->dropColumn('thingiverse_report', 'treatstock_transaction_fee');
    }
}