<?php

use yii\db\Migration;

/**
 * Class m180801_141938_fulltext_index_to_user_and_topic_and_message
 */
class m180801_141938_fulltext_index_to_user_and_topic_and_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE FULLTEXT INDEX fulltext_title ON msg_topic (title)");
        $this->execute("CREATE FULLTEXT INDEX fulltext_username ON user (username)");
        $this->execute("CREATE FULLTEXT INDEX fulltext_text ON msg_message (text)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("DROP INDEX fulltext_title ON msg_topic;");
        $this->execute("DROP INDEX fulltext_username ON user;");
        $this->execute("DROP INDEX fulltext_text ON msg_message;");
    }
}
