<?php

use yii\db\Migration;

/**
 * Class m181107_141249_5814_payment_bank_invoice_migration
 */
class m181107_141249_5814_payment_bank_invoice_migration extends Migration
{

    public function generateUuid(): string
    {
        do {
            $uuid = strtoupper(\common\components\UuidHelper::generateUuid());
            $checkUuid = (new \yii\db\Query())->select(['*'])->from('payment_bank_invoice')->where(['uuid' => $uuid])->one();
        } while ($checkUuid);

        return $uuid;
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_invoice', 'payment_bank_invoice_item');
        $this->dropTable('payment_bank_invoice_item');

        $bankInvoices = (new \yii\db\Query())->select(['*'])->from('payment_bank_invoice')->all();

        $this->addColumn('payment_bank_invoice', 'uuid', 'VARCHAR(32) NULL AFTER id');

        $this->dropForeignKey('fk_payment_invoice_1', 'payment_bank_invoice');
        $this->dropForeignKey('fk_payment_invoice_2', 'payment_bank_invoice');
        $this->dropForeignKey('fk_payment_invoice_3', 'payment_bank_invoice');

        $this->dropIndex('invoice_id_UNIQUE', 'payment_bank_invoice');
        $this->dropIndex('fk_payment_invoice_1_idx', 'payment_bank_invoice');
        $this->dropIndex('fk_payment_invoice_2_idx', 'payment_bank_invoice');
        $this->dropIndex('fk_payment_invoice_3_idx', 'payment_bank_invoice');

        $this->dropColumn('payment_bank_invoice', 'currency');
        $this->dropColumn('payment_bank_invoice', 'total_amount');
        $this->dropColumn('payment_bank_invoice', 'user_id');
        $this->dropColumn('payment_bank_invoice', 'order_id');

        foreach ($bankInvoices as $bankInvoice) {
            $invoice_uuid = app()->db->createCommand('select primary_payment_invoice_uuid from store_order where id = :id', ['id' => $bankInvoice['order_id']])->queryScalar();
            $this->update('payment_bank_invoice', [
                'invoice_id' => $invoice_uuid,
                'uuid' => $this->generateUuid()
            ], ['id' => $bankInvoice['id']]);
        }

        $this->alterColumn('payment_bank_invoice', 'invoice_id', $this->string(6)->notNull());
        $this->renameColumn('payment_bank_invoice', 'invoice_id', 'payment_invoice_uuid');

        $this->createIndex('idx_payment_invoice_uuid', 'payment_bank_invoice', 'payment_invoice_uuid', true);
        $this->createIndex('idx_payment_transaction_id', 'payment_bank_invoice', 'transaction_id');

        $this->addForeignKey('fk_pbi_payment_invoice_uuid', 'payment_bank_invoice', 'payment_invoice_uuid', 'payment_invoice', 'uuid');
        $this->addForeignKey('fk_pbi_payment_transaction_id', 'payment_bank_invoice', 'transaction_id', 'payment_transaction', 'id');

        $this->dropColumn('payment_bank_invoice', 'id');
        $this->alterColumn('payment_bank_invoice', 'uuid', 'varchar(32) NOT NULL PRIMARY KEY');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181107_141249_5814_payment_bank_invoice_migration cannot be reverted.\n";

        return false;
    }
}
