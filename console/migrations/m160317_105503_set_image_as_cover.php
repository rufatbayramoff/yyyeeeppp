<?php

use yii\db\Migration;

class m160317_105503_set_image_as_cover extends Migration
{
    public function up()
    {

        $models3ds = Yii::$app->db->createCommand("
          SELECT model3d.id, model3d_img.file_id FROM model3d
          LEFT JOIN file ON model3d.cover_file_id = file.id
          LEFT JOIN model3d_img ON model3d.id = model3d_img.model3d_id
          WHERE file.extension IN ('stl', 'obj') AND model3d_img.id IS NOT NULL AND model3d_img.deleted_at IS NULL AND model3d_img.is_moderated
          ")->queryAll();

        foreach($models3ds as $models3d)
        {
            Yii::$app->db->createCommand()->update('model3d', ['cover_file_id' => $models3d['file_id']], ['id' => $models3d['id']])->execute();
        }
    }

    public function down()
    {
        return true;
    }
}
