<?php

use yii\db\Migration;

/**
 * Class m180524_074307_5577_create_dynamic_field
 */
class m180524_074307_5577_create_dynamic_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $this->execute(
                'CREATE TABLE `dynamic_field` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `code` varchar(255) DEFAULT NULL,
                  `binded_model` varchar(255) NOT NULL,
                  `title` varchar(255) NOT NULL,
                  `type` varchar(45) NOT NULL,
                  `type_params` json DEFAULT NULL,
                  `default_value` varchar(45) DEFAULT NULL,
                  `description` text,
                  `is_active` tinyint(1) NOT NULL DEFAULT 1,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `code_UNIQUE` (`code`)
                ) ;'
            );
            $this->execute('CREATE TABLE `dynamic_field_category` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `field_code` varchar(255) NOT NULL,
              `category_id` int(11) NOT NULL,
              `is_required` tinyint(1) NOT NULL DEFAULT \'0\',
              `default_value` varchar(45) DEFAULT NULL,
              `is_active` tinyint(1) NOT NULL DEFAULT \'1\',
              `has_filter` tinyint(1) NOT NULL DEFAULT \'0\',
              `type_params` json DEFAULT NULL,
              `position` int(11) NOT NULL DEFAULT \'0\',
              PRIMARY KEY (`id`),
              UNIQUE KEY `field_code_UNIQUE` (`field_code`,`category_id`),
              KEY `fk_dynamic_field_category_store_category1_idx` (`category_id`),
              CONSTRAINT `fk_dynamic_field_category_dynamic_field1` FOREIGN KEY (`field_code`) REFERENCES `dynamic_field` (`code`) ON DELETE NO ACTION ON UPDATE CASCADE,
              CONSTRAINT `fk_dynamic_field_category_store_category1` FOREIGN KEY (`category_id`) REFERENCES `product_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            );');

            $this->execute('ALTER TABLE `product` ADD COLUMN `dynamic_fields_values` JSON NULL AFTER `single_price`;');
            $transaction->commit();
        } catch (Exception $e) {
            echo 'Catch Exception ' . $e->getMessage() . ' ';
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $this->dropTable('dynamic_field_filter');
            $this->dropTable('dynamic_field_category');
            $this->dropTable('dynamic_field');

            $this->dropColumn('product', 'dynamic_fields_values');
            $transaction->commit();
        } catch (Exception $e) {
            echo 'Catch Exception ' . $e->getMessage() . ' ';
        }

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180524_074307_5577_create_dynamic_field cannot be reverted.\n";

        return false;
    }
    */
}
