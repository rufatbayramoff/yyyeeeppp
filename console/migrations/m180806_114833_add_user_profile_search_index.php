<?php

use yii\db\Migration;

/**
 * Class m180806_114833_add_user_profile_search_index
 */
class m180806_114833_add_user_profile_search_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE FULLTEXT INDEX fulltext_user_profile_search ON user_profile (full_name,firstname,lastname)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("DROP INDEX fulltext_user_profile_search ON user_profile;");
    }
}
