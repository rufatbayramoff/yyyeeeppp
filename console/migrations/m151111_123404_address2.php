<?php

use yii\db\Schema;
use yii\db\Migration;

class m151111_123404_address2 extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `user_location`
            ADD COLUMN `address2` VARCHAR(245) NULL DEFAULT NULL AFTER `address`;");
    }

    public function safeDown()
    {
        $this->dropColumn("user_location", "address2");
    }
}
