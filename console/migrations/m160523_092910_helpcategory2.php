<?php

use yii\db\Migration;

class m160523_092910_helpcategory2 extends Migration
{
    public function up()
    {
        try{
            $this->execute("ALTER TABLE `site_help_category` 
                                ADD COLUMN `priority` INT NULL DEFAULT NULL AFTER `slug`;");

            $this->execute("ALTER TABLE `site_help` 
                    ADD COLUMN `category_id` INT(11) NULL DEFAULT NULL AFTER `lang_iso`,
                    ADD INDEX `fk_site_help_2_idx` (`category_id` ASC);
                    ALTER TABLE `site_help` 
                    ADD CONSTRAINT `fk_site_help_2`
                      FOREIGN KEY (`category_id`)
                      REFERENCES `site_help_category` (`id`)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION;
            ");
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    public function down()
    {
        try{
            $this->dropColumn('site_help_category', 'priority');
            
            $this->execute("ALTER TABLE `site_help` 
                DROP FOREIGN KEY `fk_site_help_2`;
            ");
            
            $this->dropColumn('site_help', 'category_id');
            $this->execute("ALTER TABLE `site_help` 
                DROP INDEX `fk_site_help_2_idx`");
            
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
