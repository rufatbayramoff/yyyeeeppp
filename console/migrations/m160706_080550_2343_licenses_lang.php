<?php

use yii\db\Migration;

class m160706_080550_2343_licenses_lang extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `model3d_license_intl` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `license_id` INT NOT NULL,
          `title` VARCHAR(145) NULL,
          `description` TEXT NULL,
          `info` MEDIUMTEXT NULL,
          `lang_iso` CHAR(5) NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `fk_model3d_license_intl_1_idx` (`license_id` ASC),
          CONSTRAINT `fk_model3d_license_intl_1`
            FOREIGN KEY (`license_id`)
            REFERENCES `model3d_license` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);
        ');

        $this->execute('alter table model3d_license_intl convert to character set utf8 collate utf8_unicode_ci');

        $this->execute('ALTER TABLE `model3d_license_intl`  ADD UNIQUE INDEX `uq_intl_lang` (`license_id` ASC, `lang_iso` ASC)');
    }

    public function down()
    {
        $this->truncateTable('model3d_license_intl');
        $this->dropTable('model3d_license_intl');
    }

}
