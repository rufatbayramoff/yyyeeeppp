<?php

use yii\db\Schema;
use yii\db\Migration;

class m150727_175925_property_value extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('{{%printer_to_property}}', 'value', 'string(100) NULL');

        $sql = 'SET FOREIGN_KEY_CHECKS=0;
          ALTER TABLE `ps_printer` ADD UNIQUE `user_printer_unique` (`user_id`, `printer_id`);
          ALTER TABLE `ps_printer_to_property` ADD PRIMARY KEY (`id`);
          ALTER TABLE `ps_printer_to_property` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;';
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function safeDown()
    {
        $sql = 'SET FOREIGN_KEY_CHECKS=0;
          ALTER TABLE `ps_printer` DROP INDEX `user_printer_unique`;
          ALTER TABLE `ps_printer_to_property` CHANGE `id` `id` INT(11) NOT NULL;
          ALTER TABLE `ps_printer_to_property` DROP PRIMARY KEY;';
        $this->execute($sql);
        echo 'done';
        return 0;
    }
}
