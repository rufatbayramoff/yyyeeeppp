<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_115830_payment_tax_rate extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `payment_detail_tax_rate` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `payment_detail_id` int(11) NOT NULL,
                `parent_id` int(11) DEFAULT NULL,
                `child_id` int(11) DEFAULT NULL,
                `tax_rate` int(11) NOT NULL DEFAULT '0',
                `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `user_id` int(11) DEFAULT NULL COMMENT 'who updated tax rate. default null',
                PRIMARY KEY (`id`),
                UNIQUE KEY `payment_detail_id_UNIQUE` (`payment_detail_id`),
                KEY `fk_payment_detail_tax_rate_1_idx` (`parent_id`),
                KEY `fk_payment_detail_tax_rate_2_idx` (`child_id`),
                KEY `fk_payment_detail_tax_rate_3_idx` (`user_id`),
                CONSTRAINT `fk_payment_detail_tax_rate_4` FOREIGN KEY (`payment_detail_id`) REFERENCES `payment_detail` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                CONSTRAINT `fk_payment_detail_tax_rate_1` FOREIGN KEY (`parent_id`) REFERENCES `payment_detail` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                CONSTRAINT `fk_payment_detail_tax_rate_2` FOREIGN KEY (`child_id`) REFERENCES `payment_detail` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                CONSTRAINT `fk_payment_detail_tax_rate_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        ");
        
        $this->execute("ALTER TABLE `payment_detail` 
            ADD COLUMN `type` VARCHAR(45) NOT NULL DEFAULT 'payment' AFTER `description`;");
        
        // update to correct types now
        // PaymentDetail::TYPE_AWARD, PaymentDetail::TYPE_ORDER, PaymentDetail::TYPE_PAYOUT
        $this->execute("UPDATE payment_detail set type='award_model' where description like 'award for model%';");
        $this->execute("UPDATE payment_detail set type='award_print' where description like 'award for printing%';");
        $this->execute("UPDATE payment_detail set type='order' where description like 'Order%';");
        $this->execute("UPDATE payment_detail set type='payout' where description like 'payout';");
    }

    public function down()
    {
        $this->truncateTable("payment_detail_tax_rate");
        $this->dropTable("payment_detail_tax_rate");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
