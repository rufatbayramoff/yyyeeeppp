<?php

use yii\db\Migration;

class m160516_134961_1860_templates_fix extends Migration
{
    public function up()
    {


        $this->delete('email_template', ['code' => 'ps19HoursAfterPrinted']);
        $this->execute("INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(NULL, 'ps19HoursAfterPrinted', 'service', 'en-US', 'The order #%orderId% on treatstock.com requires your attention', NULL, NULL, 'Hello %psName%!\nYou have printed the order #%orderId% on treatstock.com\nThis is a short reminder that you have 5 hours left to send it to the customer.\nPlease do not forget to change the order status from «printed» to «sent» after you send it, otherwise the order will still be considered incomplete.\n\nBest regards,\nTreatstock', 'Hello %psName%!\nYou have printed the order #%orderId% on treatstock.com\nThis is a short reminder that you have 5 hours left to send it to the customer.\nPlease do not forget to change the order status from «printed» to «sent» after you send it, otherwise the order will still be considered incomplete.\n\nBest regards,\nTreatstock');
");




    }

    public function down()
    {
        return true;
    }
}
