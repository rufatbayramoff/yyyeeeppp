<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_123946_user_location_add_user_data extends Migration
{
    public function up()
    {
        $this->addColumn('user_location', 'user_data', Schema::TYPE_TEXT);

    }

    public function down()
    {
        $this->dropColumn('user_location', 'user_data');
        return true;
    }

}
