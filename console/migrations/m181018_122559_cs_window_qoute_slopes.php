<?php

use yii\db\Migration;

/**
 * Class m181018_122559_cs_window_qoute_slopes
 */
class m181018_122559_cs_window_qoute_slopes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('cs_window_quote_item', 'slope', 'slopes');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('cs_window_quote_item', 'slopes', 'slope');
    }
}
