<?php

use yii\db\Migration;

class m170127_073409_3155_email_templates extends Migration
{
    public function safeUp()
    {
        $this->execute("UPDATE email_template SET `code` = 'client.order.orderoffer.accept.delivery' WHERE `code` = 'clientPrintStartOfferedOrder'");
        $this->execute("
        INSERT INTO `email_template`(`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`, `is_active`)
        VALUES (NULL , 'client.order.orderoffer.accept.pickup', 'order', 'en-US', 'We have found a new Print Service for your order!', NULL, NULL,
    'Dear %clientName%, %oldPs% has declined your request to print order #%orderId%, but do not worry as we have found another print service named %newPs% that has accepted your order for print. You will not be charged any additional costs and you can contact the print service in Treatstock messages if you wish.

If your shipping option was set as pick up, you can view the new address for pick up in the order details.

new Address : %newAddress%',
    NULL,
    1
 );
");

    }

    public function safeDown()
    {
        $this->execute("UPDATE email_template SET `code` = 'clientPrintStartOfferedOrder' WHERE `code` = 'client.order.orderoffer.accept.delivery'");
        $this->delete("email_template", ['code' => 'client.order.orderoffer.accept.pickup']);
        return true;
    }
}
