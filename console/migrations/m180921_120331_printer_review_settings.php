<?php

use yii\db\Migration;

/**
 * Class m180921_120331_printer_review_settings
 */
class m180921_120331_printer_review_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $idGroup = app()->db->createCommand("select id from system_setting_group where title = 'printer'")->queryScalar();

        if ($idGroup) {
            $this->insert('system_setting', [
                'group_id' => $idGroup,
                'key' => 'printReviewCount',
                'value' => 10,
                'description' => 'Count limit settings for displaying reviews on the print page.'
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
