<?php

use yii\db\Migration;

class m171128_105354_4592_confirm_preorder extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `preorder` 
            CHANGE COLUMN `status` `status` ENUM('wait_confirm', 'new', 'accepted', 'rejected') NOT NULL DEFAULT 'new' ,
            ADD COLUMN `confirm_hash` VARCHAR(45) NULL AFTER `offered`;
        ");

        $this->execute("
            INSERT INTO `email_template`(
                `id`,
                `code`,
                `group`,
                `language_id`,
                `title`,
                `description`,
                `updated_at`,
                `template_html`,
                `template_text`,
                `is_active`
            ) VALUES (
                NULL,
                'customer.preorder.confirmPreorder',
                'service',
                'en-US',
                'Confirm quote #%quoteId%',
                NULL,
                '2017-10-16 07:03:47',
                '<p>Hello %userName%,</p>
                        <p>
                        Please, <a href=\"%confirmLink%\">cofirm</a> or <a href=\"%declineLink%\">decline</a> quote
                        </p>
                        <p>
                        Thank you,
                        Treatstock Team</p>
                        ',
                NULL,
                1
             );
        ");
    }

    public function safeDown()
    {
        echo "m171128_105354_4592_confirm_preorder cannot be reverted.\n";

        return false;
    }
}
