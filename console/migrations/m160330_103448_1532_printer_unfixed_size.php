<?php

use yii\db\Migration;

class m160330_103448_1532_printer_unfixed_size extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `printer` ADD `have_unfixed_size` TINYINT  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `build_volume`;");
        $this->execute("UPDATE printer SET have_unfixed_size = 1 WHERE firm LIKE('%RepRap%')");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `printer` DROP `have_unfixed_size`;");
        return true;
    }
}
