<?php

use yii\db\Migration;

class m161202_112924_2730_keys extends Migration
{
    public function up()
    {
        $this->execute("
                  ALTER TABLE `store_order_attemp` 
            ADD CONSTRAINT `fk_store_order_attemp_1`
              FOREIGN KEY (`order_id`)
              REFERENCES `store_order` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION,
            ADD CONSTRAINT `fk_store_order_attemp_2`
              FOREIGN KEY (`printer_id`)
              REFERENCES `ps_printer` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
    }

    public function down()
    {
        return true;
    }
}
