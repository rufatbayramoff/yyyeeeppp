<?php

use yii\db\Migration;
use yii\db\Query;

class m170530_101021_4218_thingiverse_file_is_active extends Migration
{
    public function up()
    {
        $this->addColumn('thingiverse_thing_file', 'is_active', 'TINYINT(1) NOT NULL DEFAULT 1');
    }

    public function down()
    {
        $this->dropColumn('thingiverse_thing_file', 'is_active');
    }
}