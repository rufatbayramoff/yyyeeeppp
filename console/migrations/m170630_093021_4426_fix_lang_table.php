<?php

use yii\db\Migration;
use yii\db\Query;

class m170630_093021_4426_fix_lang_table extends Migration
{
    const LANG_INFO = [
        [
            'title' => 'English',
            'title_original' => 'English',
            'iso_code' => 'en-US',
            'is_active' => 1,
            'url' => 'en'
        ],
        [
            'title' => 'Russian',
            'title_original' => 'Русский',
            'iso_code' => 'ru',
            'is_active' => 1,
            'url' => 'ru'
        ],
        [
            'title' => 'Japanese',
            'title_original' => '日本語',
            'iso_code' => 'ja',
            'is_active' => 0,
            'url' => 'ja'
        ],
        [
            'title' => 'Chinese',
            'title_original' => '中文',
            'iso_code' => 'zh-CN',
            'is_active' => 1,
            'url' => 'cn'
        ],
        [
            'title' => 'French',
            'title_original' => 'Français',
            'iso_code' => 'fr',
            'is_active' => 1,
            'url' => 'fr'
        ],
        [
            'title' => 'German',
            'title_original' => 'Deutch',
            'iso_code' => 'de',
            'is_active' => 0,
            'url' => 'de'
        ],
        [
            'title'          => 'English UK',
            'title_original' => 'English UK',
            'iso_code'       => 'en-GB',
            'is_active'      => 0,
            'url'            => 'en-GB'
        ],
    ];

    protected function fixLang($langInfo)
    {
        $row = (new Query())->select('*')->from('system_lang')->where("iso_code='".$langInfo['iso_code']."'")->one();
        $id = null;
        if ($row) {
            $id = $row['id'];
        } else {
            $row = (new Query())->select('*')->from('system_lang')->where("url='".$langInfo['url']."'")->one();
            if ($row) {
                $id = $row['id'];
            } else {
                $this->insert('system_lang', $langInfo);
            }
        }
        if ($id) {
            $this->update('system_lang', $langInfo, 'id=' . $id);
        }
    }

    public function up()
    {
        foreach (self::LANG_INFO as $langInfo) {
            $this->fixLang($langInfo);
        }
    }

    public function down()
    {

    }

}
