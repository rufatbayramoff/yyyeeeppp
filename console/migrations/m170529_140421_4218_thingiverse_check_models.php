<?php

use yii\db\Migration;
use yii\db\Query;

class m170529_140421_4218_thingiverse_check_models extends Migration
{
    protected function getModel3dIdByFile($id)
    {
        $sql = 'SELECT * FROM `model3d_part` WHERE file_id=' . $id;
        $model3dPart = $rows = $this->db->createCommand($sql)->queryScalar();
        if (!is_array($model3dPart) || !array_key_exists('model3d_id', $model3dPart)) {
            return null;
        }
        return $model3dPart['model3d_id'];
    }

    protected function createThingiverseThing($createdAt, $thingId, $model3dId)
    {
        $this->insert('thingiverse_thing', ['created_at' => $createdAt, 'thing_id' => $thingId, 'model3d_id' => $model3dId]);
    }

    public function up()
    {
        $sql = 'SELECT `thingiverse_thing_file`.* FROM `thingiverse_thing_file` LEFT JOIN `thingiverse_thing` ON `thingiverse_thing_file`.`thing_id` = `thingiverse_thing`.thing_id WHERE `thingiverse_thing`.thing_id IS NULL';
        $nullThingFiles = $rows = $this->db->createCommand($sql)->queryAll();
        foreach ($nullThingFiles as $nullThingFile) {
            $model3dId = $this->getModel3dIdByFile($nullThingFile['file_id']);
            if ($model3dId) {
                $this->createThingiverseThing($nullThingFile['created_at'], $nullThingFile['thing_id'], $model3dId);
            } else {
                $this->delete('thingiverse_thing_file', ['thing_id' => $nullThingFile['thing_id']]);
            }
        }
        $this->createIndex('thingiverse_thing_file_thing_id', 'thingiverse_thing_file', 'thing_id');
        $sql = 'ALTER TABLE `thingiverse_thing_file` ADD CONSTRAINT `fk_thingiverse_thing_file_thing_id` FOREIGN KEY (`thing_id`) REFERENCES `thingiverse_thing`(`thing_id`) ON DELETE RESTRICT ON UPDATE RESTRICT';
        $this->db->createCommand($sql)->execute();
    }

    public function down()
    {
        return false;
    }
}