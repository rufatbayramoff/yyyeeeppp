<?php

use yii\db\Migration;

class m170227_130912_3620_order_without_reg extends Migration
{

    public function safeUp()
    {
        $this->insert('email_template', [
            'code' => 'client.order.new.anonim',
            'group' => 'order',
            'language_id' => 'en-US',
            'title' => 'New anonim order',
            'template_html' => 'Hello %clientName%, you have new anonim order %orderId%, if it you - follow link <a href="%confirmOrderLink%">%confirmOrderLink%</a>',
        ]);

        $this->addColumn('store_order', 'confirm_hash', 'string NULL');
        $this->createIndex('store_order_confirm_hash', 'store_order', 'confirm_hash', true);

        $this->insert('user', [
                'id'=>998,
                'username'=>'anonim',
                'auth_key'=>'',
                'password_hash'=>'',
                'email'=>'anonim@treatstock.com',
                'status'=>1,
                'created_at' =>0,
                'updated_at' => 0,
                'lastlogin_at'=>0,
                'trustlevel' => 'high',
                'notify_config' => '{"order":{"email":"never"},"service":{"email":"never"},"news":{"email":"never"},"rules":{"email":"never"},"personalMessages":{"email":"never"}}'
            ]
        );


        $this->insert('user_profile', [
                'user_id'=>998,
                'full_name'=>'test',
                'current_lang'=>'en-US',
                'current_currency_iso'=>'',
                'current_metrics'=>'in',
                'timezone_id' => 'America/Los_Angeles',
                'is_printservice' => 0,
                'is_modelshop' => 0,
                'gender' => 'none',
                'default_license_id' => 1
            ]
        );
















    }

    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'client.order.new.anonim']);
        $this->dropColumn('store_order', 'confirm_hash');
    }

}
