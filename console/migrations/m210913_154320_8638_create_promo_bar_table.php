<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Handles the creation of table `{{%promo_bar}}`.
 */
class m210913_154320_8638_create_promo_bar_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_main_promo_bar}}', [
            'id'        => $this->primaryKey(),
            'file_uuid' => $this->string()->null(),
            'url'       => $this->string(),
        ]);
        $this->addForeignKey(
            '{{%fk-promo_bar-file_uuid}}',
            '{{%product_main_promo_bar}}',
            'file_uuid',
            '{{%file_admin}}',
            'uuid'
        );
        $this->insert('product_main_promo_bar', ['id'=>1, 'url'=>'https://www.treatstock.com/']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_main_promo_bar}}');
    }
}
