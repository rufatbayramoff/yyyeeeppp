<?php

use yii\db\Migration;

class m170718_105433_4534_setting_exludeuser extends Migration
{
    public function up()
    {
        $this->insert('system_setting', [
           'group_id' => 1,
            'key' => 'excludeReport',
            'value' => 'json',
            'created_user_id' => 1,
            'updated_user_id' => 1,
            'description' => 'exclude these user ids from reports',
            'json' => json_encode(["1"]),
            'is_active' => 1
        ]);
    }

    public function down()
    {
        $this->delete('system_setting', ['group_id'=>1, 'key'=>'excludeReport']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
