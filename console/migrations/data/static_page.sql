
INSERT INTO `static_page`(`id`,`alias`) VALUES ( '3', 'about' );
INSERT INTO `static_page`(`id`,`alias`) VALUES ( '5', 'policy' );
INSERT INTO `static_page`(`id`,`alias`) VALUES ( '6', 'return-policy' );
INSERT INTO `static_page`(`id`,`alias`) VALUES ( '4', 'terms' );



INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '1', '3', 'en-US', 'About', '<div class="site-about">
<div class="site-about__text">
<p>Treatstock is an online platform for consumers to search and purchase anything that can be made on a 3D printer. All members of our team are enthusiasts of what we do and we are all, to some extent, carried away by 3D printing. Our main objective is to provide the most extensive range of 3D models for both unique and everyday items that can also be personalized to your liking. Another important objective is to minimize delivery time for the 3D models which is achieved by using the closest print service to the customer.</p>

<p>We invite authors of 3D models, 3D printer owners, 3D print services, and anyone who wants to make a profit on 3D printing to register on the website!</p>

<div class="blockquote">
<div class="blockquote__body">
<div class="text-center">
<h2><strong>Treatstock Began with a Dream</strong></h2>
</div>

<p>Hello and welcome!</p>

<p>My name is Tim Arno and I am the founder of Treatstock. Let me tell you a short story about how the inspiration of creating a toy from a childhood photo has led to the creation of our 3D printing network.</p>

<p>One day I was looking through an old photo album and I recognized a toy airplane that was given to me by my father in one of the pictures. I remember that he brought it back from a business trip, and from that moment on I didn&#39;t want to part with it. This airplane became one of the symbols of my childhood. Unfortunately, it has long been lost but that picture brought back nostalgic memories.</p>

<p>No matter how hard I looked, I couldn&rsquo;t find the same one anywhere, so I decided to recreate it in a 3D model using the old pictures I had. The result exceeded all my expectations, the plane looked great! It took a proud place on my desk, where my children could see it. They loved the airplane so much that they asked if they could have it, to which I asked them if they would like to print their own unique models of the plane. At first, they did not believe that they could choose the color and the size of their toy. The kids were excited about the fact that they could design it and make it their &#39;own&#39;. They even decided to name them, so I put the names on the 3D models.</p>

<p>A few days later there they were, the red plane with the name &ldquo;Roy&rdquo; on the right wing, and the yellow one with the name &quot;Winkie&quot;. The children were beyond happy, and that happiness was passed onto me. I thought such a personal gift shouldn&#39;t just be limited to toys, but also for jewelry, accessories, household items and much more, all that which can become a special gift for yourself and your loved ones.</p>

<p>So, let&rsquo;s make the world around us special and unique!</p>
</div>

<p>Best Regards,<br />
Tim Arno</p>
</div>
</div>
</div>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '2', '3', 'ru', 'About', ' <div class="site-about">

                <div class="site-about__text">
                    <p>
                        Treatstock - онлайн платформа для поиска и заказа всего, что можно напечатать на 3D принтере. Все сотрудники нашей команды – страстные любители своего дела, и мы все в той или иной степени, помешаны на 3D печати. Одна из главных целей разработчиков предоставить самый обширный выбор как уникальных, так и массовых вещей, которые можно подстроить под свой вкус. Другая приоритетная цель сократить время доставки до минимума за счет того, что модель можно будет изготовить у ближайшего к заказчику сервиса 3D печати.
                    </p>

                    <p>
                        Мы приглашаем авторов 3D-моделей, владельцев 3D принтеров, службы 3D печати, а также тех, кто хочет получить прибыль от 3D-печати, зарегистрироваться на сайте!
                    </p>

                    <div class="blockquote">

                        <div class="blockquote__body">
                            <div class="text-center">
                                <h2 style="display: inline-block">
                                    <strong>Treatstock начался с мечты</strong>
                                </h2>
                            </div>

                            <p>
                                Здравствуйте и добро пожаловать!
                            </p>

                            <p>
                                Меня зовут Тим Арно и я являюсь основателем Treatstock. Позвольте мне рассказать вам короткую историю о том, как создание игрушки из детства по фотографии сподвигло на создание платформы 3D-печати.
                            </p>

                            <p>
                                Как-то я просматривал фотоальбом и на одном из снимков увидел свою игрушечный самолетик, подаренный мне отцом. Он привез его из командировки, и после этого я с ним не расставался. Самолет для меня стал одним из символом детства. К сожалению, он давно затерялся в прошлом, но увиденная мной фотография заново пробудила ностальгические воспоминания.
                            </p>

                            <p>
                                Найти такой же не получилось, и тогда я решил сделать 3D модель по сохранившимся фотографиям. Результат превзошел все ожидания, самолет выглядел великолепно! Он занял почетное место на моем столе, где мои дети могли видеть его. Им так понравился самолет, что они спросили, если они могут взять его, к чему я спросил их, если они хотели бы напечатать свои собственные уникальные модели этого самолета. Сначала, они не поверили, что они могут выбрать цвет и размер своих  игрушек. Дети были в восторге! Сама возможность того, что они могут заказать “свой” самолет. Они даже решили назвать их, поэтому я  впечатал имена на 3D-модели.
                            </p>

                            <p>
                                Спустя несколько дней, они были готовы, красный самолет по имени "Рой" на правом крыле, и желтый по имени "Винки". Дети были запредельно счастливы, и что чувство было передано мне. Мне пришло в голову, что такие персональные подарки могут включать в себе не только игрушки, но и украшения, аксессуары, гаджеты и многое другое -все что станет особенным подарком для вас и ваших близких.
                            </p>

                            <p>
                                Итак, давайте сделаем мир вокруг нас особенным и уникальным!
                            </p>
                        </div>

                        <p class="blockquote__author">
                            С наилучшими пожеланиями,<br> Tim Arno
                        </p>
                    </div>
                </div>

            </div>' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '3', '3', 'zh-CN', 'About', '   <div class="site-about">
            <div class="site-about__text">
                <p>
                    treatstock 是一个供客户购买3d打印物品，获取3d打印服务的网上平台。公司团队热衷于3d打印事业，以为客户提供最广泛也最独特的3d打印模型，为客户提供距离最近的3d打印服务，缩短3d模型运送时间为宗旨充满热情地工作着。
                </p>

                <p>
                    一切源于梦想
                </p>

                <div class="blockquote">

                    <div class="blockquote__body">
                        <div class="text-center">
                            <h2 style="display: inline-block">
                                <strong>你好！欢迎来到treatstock！</strong>
                            </h2>
                        </div>

                        <p>
                            我是treatstock的创始人Tim Arno。我和treatstock的故事起始于我想要重造一个儿时的玩具的想法。
                        </p>

                        <p>
                            有一天在相册里的一张老照片里，我一眼认出父亲曾送给我的玩具飞机。父亲出差回来把它送给我之后，我们便形影不离，可以说这个玩具飞机是我童年的象征之一。遗憾的是很久之前我把它弄丢了，但那张照片唤起了那段昔日的回忆。
                        </p>

                        <p>
                            但我怎么也找不到和它一模一样的玩具飞机，于是我决定根据那张老照片利用3d打印重造一个。制造出的玩具飞机看起来超棒，完全超出了我的期望 ！我把它放在书桌上，我的孩子们看到了也十分喜欢。他们来问我能不能把飞机送给他们时，我问他们愿不愿意制造一个专属自己的飞机。起初孩子们并不相信自己能够选择玩具的颜色和大小，为能够设计专属于自己的玩具而兴奋不已的他们甚至还给各自的飞机起了名字。
                        </p>

                        <p>
                            几天后，看到一台在右翼上印着“Roy” 的红色飞机和一台名叫“Winkie”的黄色飞机的孩子们高兴得不得了，他们的快乐也感染了我。我想这样的个性礼物不应该局限于玩具，首饰、配饰、家居用品等等都可以成为送给自己或自己爱的人的个性礼物。
                        </p>

                        <p>
                            所以，让我们一起让周围的世界独一无二！
                        </p>
                    </div>

                    <p class="blockquote__author">
                        祝好！
                        <br>
                        Tim Arno
                    </p>
                </div>
            </div>

        </div>' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '4', '3', 'ja', 'About', '  <div class="site-about">

            <div class="site-about__text">
                <p>
                    Treatstockは3Dプリンターで作れるあらゆる物を検索して購入できる、消費者のためのオンラインプラットフォームです。私たちのチームメンバーは全員この分野の愛好家であり、私達全員は、時々3Dプリントで調子に乗ってしまいます。私たちの主な目的は、ユニークな、または日常品の3Dモデルを最も広範な範囲でお好みに合わせてパーソナライズしながら提供することです。もう一つの重要な目標は、顧客に最も近いプリントサービスを使用することによって3Dモデルの配達時間を最短にすることです。
                </p>

                <p>
                    私たちは、3Dモデルの作成者、3Dプリンタの所有者、3Dプリントサービス業、および3Dプリントで利益を上げたい人をウェブサイト登録に招待します！
                </p>

                <div class="blockquote">

                    <div class="blockquote__body">
                        <div class="text-center">
                            <h2 style="display: inline-block">
                                <strong>Treatstockは夢から始まりました</strong>
                            </h2>
                        </div>

                        <p>
                            こんにちは、いらっしゃい！
                        </p>

                        <p>
                            私の名前はティム・アルノ、Treatstockの創始者です。どうやって子供の頃の写真からおもちゃを作成するインスピレーションが当社の3D印刷ネットワークの創出につながったかについて短い話をします。
                        </p>

                        <p>
                            ある日、私が古いアルバムを見ている時、写真の1つに父が私にくれた飛行機のおもちゃが写っているのを見つけました。そして、彼がそれを出張のお土産として持ってきてくれて、その時以来手放したくなかったことを事を思い出しました。この飛行機は

                            、私の子供の頃のシンボルの一つとなったのです。残念ながら、それを無くしてから長い間が経ちますが、その写真は懐かしい思い出を蘇らせました。
                        </p>

                        <p>
                            何処をどんなに探しても同じ飛行機をみつけられなかった私は、その古い写真を頼りに3Dモデルを使い、再作成しようと決心したのです。結果的に飛行機は素晴らしい出来上がりで、私の全ての期待を超えました！私の子供たちが見れる様に、それは私の机の上に誇りを持って飾られました。その飛行機を凄く気に入った子供達は、彼らも一つ貰えるかと訪ねて来たので、私は、自分たちのユニークな飛行機のモデルを作りたいかと答えました。最初に彼らは、おもちゃの色とサイズを選べるとは信じませんでした。子供たちはそれを設計し、それを「自分自身」の物に出来る事に興奮していました。しかも、それらに名前を付けると決めたので、私は3Dモデルの上に名前を入れて上げました。
                        </p>

                        <p>
                            数日後、そこには右翼に「ロイ」と書かれた赤い飛行機と「ウィンキー」と書かれた黄色い飛行機が有りました。子供たちは凄く幸せで、その幸せは私にも伝わりました。私は、そのような贈り物は単に玩具に限定されるべきでは無く、ジュエリー、アクセサリーや家庭用品など、あなた自身や愛する人のための特別な贈り物に成ることが出来ると思いました。
                        </p>

                        <p>
                            なので、私たちの周りの世界を特別でユニークにしましょう！
                        </p>
                    </div>

                    <p class="blockquote__author">
                        宜しくお願い致します。<br> ティム・アルノ
                    </p>
                </div>
            </div>

        </div>' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '5', '3', 'fr', 'About', '   <div class="site-about">

            <div class="site-about__text">
                <p>
                    Treatstock est une plateforme en ligne permettant à ses clients de rechercher et d’acheter tout ce qui peut être créé grâce à une imprimante 3D. L’ensemble des membres de notre équipe est passionné par notre projet et nous sommes tous, dans une certaine mesure, transcendés par l’impression en trois dimensions. Notre objectif principal est de fournir la gamme la plus large possible de modèles 3D, d’éléments uniques ou courants, qui pourront être personnalisés selon vos envies. Un autre de nos principaux objectifs est de réduire au maximum les délais de livraison en utilisant l’imprimante 3D la plus proche du lieu de livraison.
                </p>

                <p>
                    Nous vous invitons – vous, les créateurs d’objets 3D, les possesseurs d’imprimantes 3D, les services d’impression 3D, et tous ceux qui souhaitent être rémunérés dans le domaine de l’impression 3D – à vous inscrire sur notre site !
                </p>

                <div class="blockquote">

                    <div class="blockquote__body">
                        <div class="text-center">
                            <h2 style="display: inline-block">
                                <strong>Treatstock : tout a commencé par un rêve</strong>
                            </h2>
                        </div>

                        <p>
                            Bonjour, et bienvenue !
                        </p>

                        <p>
                            Je m’appelle Tim Arno, je suis le créateur et fondateur de Treatstock. Laissez-moi vous conter la petite histoire de quelqu’un qui, en cherchant à recréer un jouet depuis une vieille photo, a eu l’idée de mettre en place notre réseau d’imprimantes 3D.
                        </p>

                        <p>
                            Un jour, alors que je feuilletais un vieil album photos, je reconnus ce jouet, un avion que m’avait offert mon père. Il me l’avait ramené d’un voyage d’affaires et, à partir de cet instant, je n’ai plus voulu m’en séparer. Cet avion est un symbole fort de mon enfance, mais il a malheureusement disparu depuis longtemps. Revoir cette photo a fait remonter beaucoup de souvenirs à la surface.
                        </p>

                        <p>
                            J’ai eu beau chercher et chercher, impossible de retrouver le même ! J’ai donc décidé de le recréer en 3D à partir de ces vieilles photos. Le résultat fut au-delà de toutes mes espérances : l’avion était incroyable ! Je lui ai fait une place de choix sur mon bureau. C’est là que mes enfants l’ont vu et qu’ils ont voulu savoir s’ils pouvaient jouer avec… et je leur ai proposé de créer le leur. Au début, ils n’arrivaient pas à croire qu’ils pouvaient choisir la couleur et la taille de leur futur jouet. Ils étaient très excités à la perspective de pouvoir le dessiner et se l’approprier. Et comme ils ont même été jusqu’à baptiser leurs appareils, j’ai ajouté les noms dessus.
                        </p>

                        <p>
                            Quelques jours plus tard, l’avion rouge avec la mention « Roy » sur l’aile et le jaune portant l’inscription « Winkie » étaient arrivés. Les enfants étaient extatiques, et leur bonne humeur contagieuse. Je me rappelle avoir pensé qu’un cadeau aussi personnel ne devrait pas rester limité aux simples jouets, mais au contraire s’élargir aux bijoux, aux accessoires, à la décoration et bien plus encore, comme tout ce qui peut devenir un présent très personnel pour vous et vos proches.
                        </p>

                        <p>
                            Ensemble, créons un monde spécial et unique !
                        </p>
                    </div>

                    <p class="blockquote__author">
                        Bien à vous,<br> Tim Arno
                    </p>
                </div>
            </div>

        </div>' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '6', '4', 'en-US', 'Treatstock Terms and Conditions ', '

    <h2>I. INTRODUCTION:</h2>

    <p>Welcome to Treatstock! We are thrilled to have you here. Please read these Terms and Conditions carefully before accessing the Site, it is a binding agreement that you accept when you are browsing or registering to use Treatstock. </p>
    <p>The terms and conditions are legal obligations between you and Treatstock, Inc. and its affiliates (collectively, \'”Treatstock”, “we”, “us” or “our”). This agreement (together with Treatstock\'s Privacy Statement, the “Terms and Conditions”) regulates your rights and responsibilities when you use the services provided by <a href="https://www.treatstock.com">treatstock.com</a>, (the “Treatstock Site” or “Site”) and any Treatstock mobile applications (the “Applications”).</p>
    <p>By using our Services, you agree to and are bound by these Terms and Conditions, and are subject to all applicable laws and regulations. You also confirm that any 3D file (“3D Model”) uploaded to the Treatstock Site does not infringe on any federal law or regulation, including but not limited to those referring to firearms. Treatstock reserves the right to make changes to the Site, the Services and the Terms and Conditions at any time. Such amended Terms and Conditions shall be effective upon posting on the Site or through some other reasonable method. Treatstock will take reasonable efforts to post notices regarding any changes to these Terms and Conditions. Please check the Terms and Conditions published on this Site regularly to ensure that you are aware of all the terms governing the Site and the Services. Also, you are responsible for maintaining applicable standards maintained by ASTM, ANSI, ASME, or other comparable standards organizations. If you do not agree to all of the terms and conditions described above, do not access or use this Site. </p>

    <h2>II. DEFINITIONS:</h2>

    <p><strong>Site:</strong> the Site offered by Treatstock at <a href="https://www.treatstock.com">treatstock.com</a> refers to the Treatstock Site, API’s, software applications or any approved means or utility either currently in existence or in the future; the software and source code used by Treatstock to provide such services; user interface layouts, designs, images, text, knowledge base articles, program offers; site information provided in reports (such as popular keyword searches); and all other intellectual property protected under copyright, trademark, patent, publicity, or any other proprietary right.</p>
    <p><strong>Treatstock:</strong> includes Treatstock, Inc. and all licensed affiliates and partners.</p>
    <p><strong>Visitor:</strong> registered or unregistered User who is browsing the Site.</p>
    <p><strong>User:</strong> an individual or a legal entity making the use of the Services that are offered through the Site.</p>
    <p><strong>Account:</strong> User\'s registration record on Site.  </p>
    <p><strong>3D Model:</strong> 3D printable file supplied by the User. </p>
    <p><strong>3D Print:</strong> 3D model turned into a physical object by a 3D printer.</p>
    <p><strong>Author:</strong> the User who supplies a 3D Model for Treatstock.</p>
    <p><strong>Manufacturer:</strong> the User who operates a 3D printer and offers 3D printing services through the Site.</p>
    <p><strong>Order:</strong> An Order placed by the User through the Site for a 3D Print with the selected Manufacturer to turn the Author’s 3D Model into the 3D Print upon receiving the payment for the Author, Manufacturer, Service and Delivery Fees. </p>
    <p><strong>Customer:</strong> is a person who uses the Site to Order a 3D Model or 3D Print.</p>
    <p><strong>Product Page:</strong> is the product page or interface that displays the 3D Model Assets available for Order, typically on the Site <a href="https://www.treatstock.com">treatstock.com</a>.</p>
    <p><strong>Delivery Service:</strong> third party services used for delivery of the 3D Print from the Manufacturer to the Customer.  </p>
    <p><strong>User Content:</strong> 3D Models, images, videos, text, or any other content that the User submits to the Site. </p>
    <p><strong>Author Fee:</strong> the total fee due to the Author for use of the 3D Model in a particular Order.</p>
    <p><strong>Printing Fee:</strong> the total fee due to the Manufacturer for the printing services, including the start-up and material costs for printing job used to produce the 3D Print, offered and indicated by the Manufacturer as indicated on the Site. </p>
    <p><strong>Service Fee:</strong> the fee due to Treatstock in consideration for the Services provided by Treatstock and any applicable taxes. </p>
    <p><strong>Delivery Fee:</strong> the fee due to Treatstock or third party for delivery of the 3D Print from the Manufacturer to the Customer. </p>
    <p><strong>Services:</strong> The services offered by Treatstock through the Site as set forth in the clause “Service” in section 3 below of these Terms and Conditions. </p>
    <p><strong>Intellectual Property:</strong> means copyright, patent, trademark, trade secret, right of publicity, or any other proprietary right throughout the world.</p>

    <h2>III. TREATSTOCK SERVICES: </h2>
    <p>The Treatstock Services combine different 3D printing necessities provided through the Site. Treatstock is a marketplace for searching and ordering anything that can be made on a 3D printer.</p>
    <p>Users will be able to choose from a wide range of 3D Models that has been added by a designer and then print this Model on a 3D printer. Also, the user is able to select a print service and this print service will manufacture a Model into a real object and ship it to you. Additionally, the user can request to create a custom made 3D object by a designer. </p>
    <p>Treatstock also offers support for designers and print services if they have problems with the manufacture of 3D Models. There is a community support system and forums for Treatstock members for questions and sharing their experiences.</p>
    <p>In addition, the owner of the print service has access to educational materials to help with the manufacturing process. </p>

    <h2> IV. TERMS OF SERVICES:</h2>
    <p>In order to use the Services, you certify that you are of lawful age to make a legal agreement. If you are under the lawful age, you may use Services of <a href="https://www.treatstock.com">Treatstock.com</a> solely with parental or guardian acknowledgment.  </p>
    <p>In order to use and access the Services you are responsible for providing, maintaining and updating your full legal name, current address, phone number, a valid email address, and any other required information to complete the registration.  In our sole discretion, we may deny the use of the Services to any person or entity for an Account.  </p>
    <p>You are responsible for secure storing of your password. Treatstock cannot and will not be liable for any loss or damage from your failure to maintain the security of your account and password.  </p>
    <p>You authorize Treatstock to communicate with you via an email address provided during registration.   </p>
    <p>To cancel your Treatstock account, you can do one of two things. You can send us a message through the website to our support service, or you <a href="/site/contact">can email us</a> through your account.</p>
    <p>The Services must not be used for any illegal or unauthorized purposes that could violate any laws and regulations.  </p>
    <p>A failure to comply or violation of any term in the Terms of Service as determined in the sole discretion of Treatstock will result in an immediate termination of your Services.</p>
    <p>We reserve the right to refuse the Services to, or terminate the Services, of anyone at any time without notice for any reason. </p>
    <p>The Services offered by Treatstock through its Site consist solely of providing an online platform that facilitates Manufacturers to offer 3D Prints of the Author\'s 3D Models to Users with the possibility of using Delivery Services, including the payment transaction between Users, Authors, Manufacturers and Delivery Service Providers.  </p>
    <p>The Services enable Authors to upload the 3D Model and other Content to our Site and specify the Author\'s fee. The Services enable the Manufacturer to list the 3D printing services and specify Printing Fee for the services on the Site. This allows the User to place the Order with the Manufacturer to turn the uploaded 3D Model into the 3D Print. Once the Order has been processed, the User will select the delivery method. The Manufacturer will hold the 3D Print on behalf of the User until the 3D Print is shipped. Also, the User will be able to pick up the 3D Print if desired.   </p>
    <p>Treatstock is not a party to any agreements entered into between Manufacturers, Authors and Users, nor is Treatstock a manufacturer or a provider of any printing services or financial services. </p>
    <p>Treatstock has no obligation to control the conduct of Manufacturers, Authors and Users, the Content uploaded to the Site by Users and the 3D Print and services provided by the Manufacturer. Treatstock disclaims all liability in this regard. </p>
    <p>Treatstock provides protection for all purchases, and thus reserves the rights to terminate the Order and return money back to the Customer fully or partially by its sole decision using the procedure described in “purchase protection and return policy” clause in section XII. </p>

    <h2>V. USER:</h2>
    <p>The Services offered by Treatstock are available only to individuals who are 18 years or older, who can form legally binding contracts under applicable law and who have registered as the User on the Site by creating the Account. </p>
    <p>The User or any Visitor to the Site represents and warrants that all information submitted to the Site is accurate, truthful, lawful and does not infringe any rights of third parties. The User or any Visitor of the Site warrants that it will not use the Services to: </p>
    <ul>
        <li>contact another user of Treatstock for the purpose of soliciting your services outside of Treatstock</li>
        <li>send any unsolicited mass mailings ("Spam");</li>
        <li>launch or use any automated means or process, including but not limited to “spiders”, “robots”, “load testers” etc., for sending more communication than a natural person could reasonably produce or to create Users accounts (except that we grant the operators of public search engines revocable permission to use spiders to copy materials from the Sites or Services for the sole purpose of, and solely to, the extent necessary for creating publicly available searchable indices of the materials, but not caches or archives of such materials); </li>
        <li>collect or store personal information from other Users other than the information collected during the normal course of using the Service;  </li>
        <li>threaten or harass any person or entity; </li>
        <li>gain or attempt to gain unauthorized access to any computer system, server, network or hardware of Treatstock, other Users or any other third party; </li>
        <li>in any way that could overload, damage, disable, disrupt or harm the Service or interfere with any other User’s use of the Service; upload, transmit, or distribute any computer viruses, worms, or any software intended to damage or alter a computer system or data; </li>
        <li>adapt, modify or reverse engineer any of the systems or protocols of Treatstock; </li>
        <li>adapt, modify or reverse engineer other User’s 3D Model and any of its derivatives in any form or format; or </li>
        <li>reformat, resell, or redistribute the Service in any way without the explicit consent of Treatstock in writing. </li>
    </ul>

    <p>To use some Services, you must register for an account on the Site (an “Account”). You must provide current, accurate identification, contact, and other information that may be required as part of the Account registration process and/or continued use of the Service, and you must keep your Account information updated. You shall not:  </p>
    <ul>
        <li>select or use, as a user name, a name of another person with the intent to impersonate that person;  </li>
        <li>use, as a user name, a name subject to any rights of another person without appropriate authorization; or  </li>
        <li>use, as a user name, a name that is otherwise offensive, vulgar or obscene.</li>
    </ul>
    <p>You are responsible for maintaining the confidentiality of your Service password and Account, and are solely responsible for all activities that occur on your Account. You must notify us immediately of any change in your eligibility to use the Services, breach of security or unauthorized use of your Account. You should never publish, distribute or post login information for your Account. We will not be liable for any loss or damage from your failure to maintain the security of your Account and password.  </p>
    <p>You shall have the ability to delete your Account, either directly or through a request made to one of our employees or affiliates. </p>
    <p>In the event, Treatstock determines the User has breached these Terms and Conditions, Treatstock may, at its sole discretion, without any compensation deny the User or entity any further access to the Site, and/or any of its Services, and/or change the eligibility criteria at any time without notice.</p>
    <p>Any attempt by the User to damage the Site or to undermine the legitimate operations of the Services may be a violation of criminal and civil laws, and should any such attempt be made, Treatstock reserves the right to seek damages from any such User to the fullest extent permitted by law.  </p>
     
    <h2>VI. PUBLISHED USER GENERATED CONTENT</h2>
    <p>The term “Published User Generated Content” when used in this Terms and Conditions shall mean any content uploaded to the Site by you, including but not limited to all uploaded 3D designs, 3D Models, 3D files, pictures of any objects, pictures of any model, comments and reviews on the Site. All User Generated Content should not be or contain abusive, harassing, threatening, defamatory, obscene, fraudulent, deceptive, misleading, offensive, pornographic, illegal or unlawful information or be likely to infringe, or infringing, on any Intellectual Property rights. Intellectual Property rights mean copyright, patent, registered design, design right, trademark, trade secret or any other proprietary or industrial right. Treatstock reserves the right (but have no obligation) to review any User Content, investigate, and/or take appropriate action against you in our sole discretion if you violate these Terms and Conditions, or otherwise create liability for us or any other person. Such acts may include removing or modifying your User Content, terminating your Company Account, and/or reporting you to law enforcement authorities.  </p>
    <p>Treatstock has no obligation to review the User Generated Content and can in no way be held responsible for the content of the User Generated Content. Opinions expressed in the User Generated Content are not necessarily the opinion of Treatstock.  </p>
    <p>All User Generated Content including 3D Models, which are referenced below, by submitting User Generated Content through the Services or Site, you hereby do and shall grant us a worldwide, non-exclusive, royalty-free, fully paid, sub-licensable and transferable license to use, reproduce, distribute, prepare derivative works of, display, perform, and otherwise fully exploit the User Generated Content in connection with the Site, the Services and our (and our successors’ and assigns’) businesses, including without limitation for promoting and redistributing part or all of the Site or the Services (and derivative works thereof) in any media formats and through any media channels (including, without limitation, third party Sites and feeds).  </p>
    <p>You also hereby do and shall grant each User of the Site and/or the Services a non-exclusive license to access your User Generated Content (except 3D Models which is described below) through the Site and/or the Services, and to use, edit, modify, reproduce, distribute, prepare derivative works of, display and perform such User Generated Content.  Every time you publish your 3D Models on the Site, Treatstock Commercial License grants a limited use of copyrighted material by others which can be found in Appendix A.</p>
    <p>For clarity, the foregoing license grants to us and our Users does not affect your other ownership or license rights in your User Generated Content, including the right to grant additional licenses to your User Generated Content, unless otherwise agreed in writing. You represent and warrant that you have all rights to grant such licenses to us without infringement or violation of any third party rights, including without limitation, any privacy rights, publicity rights, copyrights, contract rights, or any other intellectual property or proprietary rights.  </p>
    <p>You also hereby do and shall grant each user of the Site, the Applications, or the Services a non-exclusive license to access your User Generated Content through the Site, the Applications or the Services, and share your User Generated Content on blogs and social media.  </p>
    <p>Treatstock is not obligated to backup any User Content and User Content may be deleted at any time. You are solely responsible for creating backup copies of your User Content if you desire. </p>
    <p>You agree not to use the Site or Services to collect, upload, transmit, display, or distribute any User Content:  </p>
    <ul>
        <li>that violates any third-party right, including any copyright, trademark, patent, trade secret, moral right, design rights, privacy rights, right of publicity, or any other intellectual property or proprietary right;  </li>
        <li>that is unlawful, harassing, abusive, tortuous, threatening, harmful, invasive of another\'s privacy, vulgar, defamatory, false, intentionally misleading, trade libelous, pornographic, sexually explicit, obscene, patently offensive, promotes racism, bigotry, hatred, or physical harm of any kind against any group or individual, promotes illegal activities or contributes to the creation of weapons, illegal materials or is otherwise objectionable;  </li>
        <li>that is harmful to minors in any way; </li>
        <li>that is in violation of any law, regulation, or obligations or restrictions imposed by any third party. </li>
    </ul>
    <p>Automatically Generated Content. Treatstock reserves the right to collect data about Site Users in order to improve User experience. In particular, this includes (i) model images rendered in different colors and from different angles, (ii) the calculation of model weight and sizes for various 3D printer.  </p>

    <h2>VII. PRICE </h2>
    <p>The price quoted on the Site consists of the Printing Fee, Author’s Fee, Delivery Fee, Service Fee, and additional taxes if applicable, and is shown in the currency selected by the Visitor. </p>
    <p>All prices are indicated in corresponding currencies. If the User does not choose to pay in the shown currency, the currency conversion will be handled by our payment service provider.  </p>
    <p>Because every Fee is denominated at a currency specified by the Author, the Manufacturer, Delivery service and Treatstock, Treatstock reserves the rights to set prices in the currency specified by the Visitor using the exchange-rate or cross-rate at Treatstock’s sole discretion.  </p>
    <p>The price quoted on the Site includes shipping costs, Sales Tax or VAT (if applicable) but excludes import duty or taxes if applicable. However, duty, tax and brokerage fees may be levied upon importation, depending upon the destination country of your order. Shipping charges indicated by us are only for the carriage of your order and do not include these additional charges. You may contact the customs authority of the destination country of your order to learn more about the applicable duties and taxes. The amount of the import tax is determined by the classification of the traded product, and this is done based on a code which will be indicated on the shipment document, which is known as an ‘Intrastate Code’. Combined import duty and tax are typically 5-20% of the total order value. In some cases, an additional brokerage fee may be levied by the carrier. </p>
    <p>Treatstock reserves the right to change the prices of the products and services shown in the gallery published on the Site, as well as the prices indicated for personalized orders, at any time. </p>

    <h2>VIII. CHOICE OF PRODUCT</h2>
    <p>By ordering a product, the User confirms that the product is not subject to any licensing restrictions, certifications, or any other regulations or rules that prohibits its production or use. The User shall be exclusively liable for any breach for any licensing restrictions, certifications, or other regulation or rules that prohibits its production or use. Treatstock disclaims any liability foregoing.   </p>
    <p>When selecting products, the Visitor acts at his/her own risk. Treatstock, the Author, and the Manufacturer are not responsible for the choice of the Customer, fitness for purpose or expectations, the applicability of the product for any purpose, or the use of products in any region.  </p>
    <p>In order to improve User experience, we are trying to show every 3D Model in different colors; however, the images of 3D Model from certain angles may be in one color. </p>
    <p>To protect the intellectual property rights of designers, Treatstock does not display the actual files in the 3D viewer. Instead, a series of images of the files are used to replicate what the Model will look like.</p>

    <h2>IX. ORDER  </h2>
    <p>The Site detects the IP Address of the Visitor to determine the geographic location of the Visitor. This information is used to suggest Printers that are close, or within proximity, to the Visitor\'s physical location. The Visitor may easily change his/her location by specifying the name of the city where he/she is located. </p>
    <p>While browsing the Site, the Visitor may choose the 3D Model or KIT (a set of separate parts to be assembled). The 3D Model will be shown on the image in a material and color chosen by the Visitor. The Site will then try to find a printer service that may print the 3D Model in that material and color. </p>
    <p>The Site shows the cost to 3D Print the chosen Model and the list of available print services that are nearby. This price will include the Author’s fee as well. The Visitor may select a print service and proceed to checkout. </p>
    <p>During the checkout process, the Site allows the Visitor to select a delivery method, including all the necessary information; at the final stage the Site shows the total cost including all Fees and applicable Taxes. </p>
    <p>The Visitor agrees to the Terms and Conditions, and pays for the Order using the chosen method.  </p>
    <p>Treatstock sends the Order for execution to the Manufacturer that the Visitor has selected, identifying the material, color, and the cost of printing based on our calculation. </p>
    <p>Treatstock reserves the right to reject the Order with or without explanations, and refund the money fully or partially to the Account from which it was paid, with or without compensation to the Author or the Manufacturer, by our sole discretion. </p>

    <h2>X. PAYMENTS AND BILLING </h2>
    <p>Treatstock accepts payment via most credit and debit cards, including VISA, MasterCard, American Express, Discover, JCB, and Diners Club. PayPal is also supported.</p>
    <p>When the User authorizes a payment using credit/debit card, the payment is pending for a maximum period of 8 days. When the Order is accepted by the Manufacturer, these funds are transferred to our account and only then will the Order be processed. This process is managed by our payment solution partners Braintree and Stripe.</p>
    <p>When the User authorizes a payment using Alipay, the funds are transferred to our account at the moment of authorization.</p>
    <p>In the event that the Manufacturer cannot process your order, we will fully return your money back to your card or account. </p>
    <p>In the event that we determine that the 3D Print is not of sufficient quality, we may fully or partially return the money back to your card or account based on guidelines of Purchase Protection Policy.</p>
    <p>If the Manufacturer has accepted an order, but for some reason wants to cancel it, the Manufacturer must contact customer support to do so. If the Manufacturer cannot fulfil the order for any reason, Treatstock may change the Manufacturer to complete the order. The customer must agree that chosen Manufacturer can be changed by Treatstock when placing the order. If the customer does not agree to change the Manufacturer in the event that the Manufacturer cannot process the order, Treatstock will return your money in full to the card or bank account that was used to place the order.</p>

    <h2>XI. DELIVERY</h2>
    <p>The Manufacturer may choose the Pickup delivery method if available. </p>
    <p>The Manufacturer may choose the independent delivery services. In this case, the Manufacturer sets the price for delivery, and Treatstock will include this price to the total cost for the Customer. </p>
    <p>The Manufacturer may choose that he is willing to work with independent delivery services such as UPS, USPS etc. using Treatstock integration.  </p>
    <p>In this case, Treatstock sets the price for delivery at its sole discretion, and includes this fee to the total cost for the Customer. It may not match the price of public usual delivery service, because at the time of ordering the exact parameters of the 3D print and possible services is not yet known and Treatstock takes on the associated risks. </p>
    <p>In this case, Treatstock takes the Delivery Fee, finds and pays for real service delivery suitable at the time of the finished printing 3D model, and provides the Manufacturer with the ability to print labels for delivery.</p>
    <p>Manufacturer shall pack 3D Print for delivery if applicable.</p>

    <h2>XII. PURCHASE PROTECTION AND RETURN POLICY</h2>
    <p>Treatstock allows the Customer to cancel their order and return money in full before the 3D Print has been started. After the 3D Print has been started, the Customer may not cancel their Order. </p>
    <p>Since the product is custom made, we will refund the money only in the case of fault or defect of the 3D Print, or in case of non-delivery of the 3D Print within a reasonable time. </p>
    <p>Treatstock reserves the rights to delay payment to the Author and the Manufacturer until the Customer confirms an acceptable quality of the 3D Print. </p>
    <p>If the Customer has not registered a complaint with Treatstock within 14 days after purchase, Treatstock assumes that the quality is acceptable. We do not accept claims after 14 days after delivery. </p>
    <p>After receiving the 3D Print and prior to using it, the Customer must ensure its quality, in case the quality does not satisfy set requirements. The User must immediately let us know and attach photos of the 3D Print and original packaging. Treatstock will not refund any payments for 3D Prints that have been used. The User shall be ready to return the 3D Print together with original packaging if we ask to do so. The Customer is responsible for returning the 3D Print, including any shipping costs. </p>
    <p>Upon the receipt of a complaint from the Customer, Treatstock may terminate the Order and refund money to the Customer at its sole discretion. Alternatively, Treatstock may request the Manufacturer to print and deliver the 3D Print once more at their own cost, or use another solution to resolve the problem. </p>
    <p>Treatstock reserves the rights to seek indemnification for damages from any party who violated this agreement.</p>

    <h2>XIII. QUALITY POLICY  </h2>
    <p>3D Models are not considered defective if:</p>
    <ul>
        <li>There are visible lines, to the naked eye, from layers after printing.  </li>
        <li>There are traces of surface treatment due to the support structures which were used during printing.   </li>
        <li>The presence of thin filaments on the surface of the printed object.  </li>
        <li>The internal structure can be visible through the glimpse of light.  </li>
        <li>Traces of lines where the printer goes from one layer to the next layer.</li>
        <li>Visible distinct lines on the bottom surface of the 3D object.</li>
    </ul>
    <p>3D Prints are considered to be defective if: </p>
    <ul>
        <li>Top surfaces are not closed properly or come out bumpy.  </li>
        <li>Irregular circles: Circles come out misshapen and lines are not properly touching.  </li>
        <li>Corners of the 3D Print lift and detach from the platform.  </li>
        <li>Parts of the 3D Print shift along the X or Y axis.  </li>
        <li>3D Prints gradually lean over.  </li>
        <li>Under extrusion.  </li>
        <li>Parts of, or entire walls of the print are not fused and touching.  </li>
        <li>The lowest parts of the print appear to shrink before reaching the proper dimensions.  </li>
        <li>First layer not sticking / Parts are coming loose.  </li>
        <li>Stringing: Unwanted strands of plastic span across the print.  </li>
        <li>Waves/shadows appear in the print.  </li>
        <li>Ugly overhangs: The lower surface of overhangs come out ugly.  </li>
        <li>Lines are overly visible or spaced apart on the first layer.  </li>
        <li>Very thin strands of plastic appear on the surface of the print.  </li>
    </ul>
    <p>Post-processing rules of 3D prints:  </p>
    <ul>
        <li>Despite the fact that 3D printing parts are produced from the filament using FDM-technology, the surface of the final product cannot be expected to be perfectly smooth. To improve it, the following methods are suggested:  </li>
        <li>Print with the highest resolution possible for the printer (For example, the resolution of the Ultimaker’s 2 is 20 microns - 0.02 mm). The final 3D Print will be the smoothest one, but such settings may result in different printing defects, and significantly lengthen the time of printing.  </li>
        <li>Post-processing. The surface can be smoothed out by sanding it with sandpaper or refining it with a needle-file, however, you should be cautious.  </li>
        <li>Treatment with different solvents. The surface can be smoothed out perfectly by using solvents, such as acetone for ABS-plastic or Dichloromethane for PLA-plastic (relatively toxic). The Model can be dipped into a container with a solvent or treated with the vapor.  </li>
        <li>However, it is important to keep in mind that solvents can dissolve the outer layers of the 3D Model more than is necessary, thus ruining it.  </li>
        <li>Keep in mind that when working with solvents one has to be extremely careful.  All work must be carried out in a well ventilated room.  Also, one should use safety equipment to protect eyes, respiratory organs and skin.  </li>
    </ul>
    <p>Rules for model Authors to have printable 3D Models: </p>

    <ol>
        <li>Thin elements 
            <br>
            When printing thin elements, the following should be considered: 

            <ul>
                <li>The size of the thinnest elements should not be smaller than the diameter of the nozzle of the printer. Otherwise, these elements would not be printable. </li>
                <li>The wall thickness (including the upper and lower walls) of 3D Print should exceed the height of a single layer by at least 2-3 times (ideally by 4-6 times). If the wall thickness is smaller, the strength of the walls significantly reduces and there may be visible gaps between the layers. </li>
                <li>When performing the post-processing procedure of small parts one should remember they may become less visible or disappear altogether. Treating such small parts must be done with great caution. </li>
                <li>It is difficult for some Slicers (Cura) to build support structures for small parts. This problem can be avoided by increasing the density of building support structures, or by using several slicers to generate support structures. </li>
                <li>The level of detail of the model deteriorates with its decreasing size when scaling. Some elements of the model are well reproduced when the size of the Model is large, while the same elements cannot be printed as well on a smaller version, that is why they should be deleted or modified. It is necessary to pay great attention when preparing for printing all models subjected to scaling. </li>
            </ul>
        </li>

        <li>What to do with support structures  
            <ul>
                <li>Support structures are necessary for printing elements of the model that are overhanging.  </li>
                <li>One can print without support structures with the angle of no more than 60-70 degrees. Keep in mind print quality may decrease slightly.  </li>
                <li>The number of support structures can be reduced by simply placing the model at a different angle. For example, a flat object lying on the horizontal plane of the table will be printed a lot faster than standing vertically.  </li>
                <li>In some cases, one can print without any support structures, simply by dividing the model into several parts.</li>
                <li>Keep in mind that support structures can ruin the appearance of the 3D model surface, so it is better to place the model at such an angle that support structures would be created in places that are not visible, or in such places that can be easily treated or removed.  </li>
                <li>Reducing the number of support structures will contribute to a better printing process. It will reduce the amount of filament required for printing and time, thus reduce its cost.  </li>
                <li>Positioning relative to the print table glass  </li>
                <li>When positioning the 3D Model relative to the working surface of the printer the following should be considered:
                    <ul>
                        <li>Changing the angle can significantly reduce (or increase) the amount of support structures required to build the 3D Model. For example, the overhanging element of the 3D Model can be avoided by placing the vertical Model horizontally.  </li>
                        <li>The strength of the 3D Model can be increased by placing the model so that the area with the greatest load are arranged perpendicular to the printed layers, and not parallel to them.  </li>
                        <li>The quality of the pattern with circle elements can be increased by placing the 3D Model so that it is printed horizontally. In some cases, the circles of the vertical seal can lead to their deformation (instead of circular holes — ellipses will be obtained).  </li>
                    </ul>
                </li>
            </ul>
        </li>

        <li>The size of the 3D Model 
            <br>
            When selecting the size of the Model the following should be considered:
                <ul>
                    <li> The maximum size of the Model for any 3D printer is usually determined by its build volume (for example, the build volume for the Ultimaker 2 is 230x225x205 mm). </li>
                    <li> It is possible to print a larger size Model if the Model is divided into several parts (each of which can be placed in the build volume), and print them separately and connect or glue them together.  </li>
                    <li>The printer does not print objects thinner than the diameter of the nozzle. In other words, the minimum size of the Model should not be less than the diameter of the nozzle. To ensure the strength and stability of the structure, the optimum thickness of the walls of any Model should be 2-3 times the diameter of the nozzle.</li>
                </ul>
        </li>
    </ol>

    <p>Packing standards: </p>
    <ul>
        <li>Use sturdy and undamaged boxes with all flaps intact.</li>
        <li>Use double-wall boxes for heavier items.</li>
        <li>Wrap items individually with cushioning material and center them in cartons away from other items and away from the sides, corners, top, and bottom of the box. Use adequate cushioning material.</li>
        <li>Place items that might be damaged by normal handling, such as soiling, marking, or application of adhesive labels, in a protective outer box.</li>
        <li>Use fillers to fill void spaces and prevent movement of goods inside the box during shipping.</li>
        <li>Use the H taping method for sealing your package. Close and tape the inner box using the H taping method. This will help prevent accidental opening.</li>
        <li>Restrict product movement inside the box using filler like crumpled newspaper, loose fill peanuts, or other cushioning material.</li>
        <li>Ship fragile products individually, wrapping them in a minimum 3" thickness of air-cellular cushioning material.</li>
        <li>Fill any void spaces with more cushioning material.</li>
        <li>Wrap all items separately</li>
        <li>Use strong tape designed for shipping</li>
        <li>Use a single address label that has clear, complete delivery and return information</li>
    </ul>

    <p>If you have any questions regarding a possible printing defect, please contact us using the <a href="/site/contact">contact form</a> with a good quality image of the 3D Print attached.<p>

    <h2>XIV. THIRD PARTY SERVICES</h2>
    <p>The Site may contain links to other sites and services, or include special I-Frames from other sites and services. We are not responsible for the content and availability of these sites and services. Each of these services has its own terms and conditions, please read them carefully. </p>
    <p>In some cases, to provide the functions of a Site we can transfer data to other sites and services, and this data may include personal information (or PII). We transfer only the necessary information and require such services to carefully handle this personal information, but using our service you agree with the possibility of such a transfer. </p>
    <p>Includes but not limited to the subject to change.</p>

    <h2>XV. COPYRIGHT INFRINGEMENT</h2>
    <p>If you believe that any material on the Site is in violation of any copyright or other Intellectual Property rules or laws, please send a notice of copyright infringement containing the following information to us:  </p>
    <p>Identification of the work or material being infringed; </p>
    <p>Identification of the material that is claimed to be infringing, including its location, with sufficient detail so that Treatstock is capable of finding it and verifying its existence;  </p>
    <p>Contact information for the notifying party (the "Notifying Party"), including name, address, telephone number and email address; </p>
    <p>A statement that the Notifying Party has a good faith belief that the material is not authorized by the intellectual property owner, its agent or law; </p>
    <p>A statement made under penalty of perjury that the information provided in the notice is accurate and that the Notifying Party is authorized to make the complaint on behalf of the copyright owner; </p>
    <p>A physical or electronic signature of a person authorized to act on behalf of the owner of the intellectual property that has been allegedly infringed. </p>
    <p>In all cases, if you do not receive a response from Treatstock within 10 days of submitting a complaint, please contact Treatstock again to confirm that we received your original complaint. As you may know, spam blockers sometimes reject important emails from unknown parties and delivery of physical mail may be delayed without notice due to any number of unforeseen circumstances. </p>
    <p>Please note that any person who knowingly and materially misrepresents that there is an infringement may be subject to liability for damages. </p>
    <p>In an effort to be transparent in removing or restricting access to user-uploaded content, Treatstock may make public any notices of infringement received (with personal contact information removed). This may include posting the notice to a public-facing Site, among other methods. </p>
    <p>After removing or disabling access to the material or Content pursuant to a valid infringement notice, Treatstock will immediately notify the user responsible for the allegedly infringing material that it has removed or disabled access to the material. </p>
    <p>Treatstock reserves the right, in its sole discretion, to immediately terminate the account of any member who is the subject of a valid infringement notification. </p>
    <p>If you believe you are the wrongful subject of an infringement notification, you may file a counter-notification with Treatstock by providing the following: </p>
    <p>The specific URLs of material that Treatstock has removed or to which Treatstock has disabled access. </p>
    <p>Your name, address, telephone number, and email address. </p>
    <p>A statement that you consent to indemnify Treatstock against any copyright infringement claims that arise from the material. </p>
    <p>The following statement: "I swear, under penalty of perjury, that I have a good faith belief that the material was removed or disabled as a result of a mistake or misidentification of the material to be removed or disabled." </p>
    <p>Your signature. </p>
    <p>Upon receipt of a valid counter-notification, Treatstock will forward it to the Notifying Party who submitted the original infringement complaint. The original Notifying Party (or the intellectual property owner he or she represents) will then have ten (10) days to notify us that he or she has filed legal action relating to the allegedly infringing material. If Treatstock does not receive any such notification within ten (10) days, we may restore the material to the Services. </p>

    <h2>XVI. COMMUNITY POLICY</h2>
    <p>Treatstock would like to offer an access to worldwide community pages for new and developing technologies such as 3D design and 3D printing. We’ve created these community pages to help the participants to bring their creations to life, inspire each other, share knowledge, discuss ideas and encourage a new interest in 3D designing and printing. The purpose of this policy is to ensure that all members of the Treatstock community have the ultimate experience.</p>
    <p>This policy describes the activity of members of the community, in any forum, reviews, mailing list, Site, correspondence, or public messages. In order to avoid communication issues there are a few ground rules to follow.</p>
    <ul>
        <li>Be considerate. Community pages may not be used to harass other members of the community. Offensive comments cannot be posted on community pages.</li>
        <li>Be patient. Unsafe, harmful, dangerous or Any threats of violence against others or promotion of violence or illegal activity cannot take place within community pages.   </li>
        <li>Be respectful. Be respectful towards other members. We all have our own opinion, but disagreement is no excuse for poor behavior. It\'s important to remember that a community where people feel uncomfortable or threatened is not a productive one.   </li>
        <li>Be nice. Respect other members’ privacy. You cannot share any private or personal information that should not be shared in public areas of the Site.   </li>
    </ul>

    <h2>XVII. INDEMNIFICATION </h2>
    <p>You hereby agree to indemnify and undertake to keep Treatstock and its affiliates, distributors, suppliers, contractors, and their employees unharmed from and against any liabilities, losses, costs, damages, and expenses (including without limitation legal expenses) arising from or relating to, any claims that you have breached in the provisions of these Terms and Conditions.  </p>

    <h2>XVIII. LIMITATION OF LIABILITY </h2>
    <p>YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT TREATSTOCK SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, COMPENSATORY, PUNITIVE OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF INCOME, PROFITS, CONTRACTS, DATA, OR OTHER INTANGIBLE LOSSES OF ANY KIND WHATSOEVER RESULTING FROM THE USE OF, OR INABILITY TO USE THE SERVICES.  </p>
    <p>IN NO EVENT SHALL TREATSTOCK OR OUR DIRECTORS, EMPLOYEES, PARTNERS, AFFILIATES OR SUPPLIERS, BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY LOSS OF PROFITS, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER, WHETHER OR NOT WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, AND ON ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH OUR SITE, OUR SERVICES OR THIS AGREEMENT, HOWEVER ARISING, INCLUDING NEGLIGENCE.    </p>
     
    <p>FURTHER, WE SHALL NOT BE LIABLE IN ANY WAY FOR THIRD PARTY GOODS AND SERVICES OFFERED THROUGH OUR SITE INCLUDING WITHOUT LIMITATION THE PROCESSING OF ORDERS.  </p>
    <p>SOME JURISDICTIONS PROHIBIT THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SO THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.  </p>

    <h2>XIX. APPLICABLE LAW</h2>
    <p>These Terms and Conditions shall be governed by the laws of the state of Delaware, USA, without regard to the conflicts of law principles thereof that would apply the law of any jurisdiction other than Delaware, USA. Any and all disputes arising from these Terms and Conditions, including disputes relating to the validity thereof, and any disputes related to the use of the Treatstock Services or Site, or relating to the products ordered and delivered, shall be brought in the federal and state courts located in the state of Delaware, USA. The United Nations Convention on the International Sale of Goods shall not apply to the Treatstock Services or Site, or relating to the products ordered and delivered.  </p>

    <h2>XX. MISCELLANEOUS</h2>
    <p>Force Majeure. We shall not be liable for any failure to perform our obligations hereunder where such failure results from any cause beyond our reasonable control, including, without limitation, mechanical, electronic or communications failure or degradation. </p>
    <p>Assignment. These Terms of Service are personal to you, and are not assignable, transferable, or sub-licensable by you except with our prior written consent. We may assign, transfer or delegate any of our rights and obligations hereunder without consent. </p>
    <p>Agency. No agency, partnership, joint venture, or employment relationship is created as a result of these Terms of Service and neither party has any authority of any kind to bind the other in any respect. </p>
    <p>Notices. Unless otherwise specified in these Term of Service, all notices under these Terms of Service will be in writing and will be deemed to have been duly given when received, if personally delivered or sent by certified or registered mail, return receipt requested; when receipt is electronically confirmed, if transmitted by facsimile or e-mail; or the day after it is sent, if sent for next day delivery by recognized overnight delivery service. Electronic notices should be sent using the <a href="/site/contact">contact form</a>.  </p>
    <p>No Waiver. Our failure to enforce any part of these Terms of Service shall not constitute a waiver of our right to later enforce that or any other part of these Terms of Service. Waiver of compliance in any particular instance does not mean that we will waive compliance in the future. In order for any waiver of compliance with these Terms of Service to be binding, we must provide you with written notice of such waiver through one of our authorized representatives. </p>
    <p>Headings. The section and paragraph headings in these Terms of Service are for convenience only and shall not affect their interpretation. </p>
    <p>Feedback. Since we always want to further improve our Services, we welcome all comments, suggestions, recommendations, and feedback (collectively, the “Feedback”). You hereby grant to us a world-wide, royalty free, irrevocable, perpetual license to use and otherwise incorporate any Feedback in connection with the Services. </p>
    <p>Use of "cookies". You agree that when you visit the Site \'cookies\' may be installed on the hard disk of your computer.  </p>
    <p>No Support or Maintenance. You acknowledge and agree that the Company will have no obligation to provide you with any support or maintenance in connection with the Site. </p>

    <h2>XXI. COPYRIGHT/TRADEMARK INFORMATION</h2>
    <p>Copyright © 2015, 2016 Treatstock, Inc. All rights reserved. All trademarks, logos and service marks ("Marks") displayed on the Sites are our property or the property of other third parties. You are not permitted to use these Marks without our prior written consent or the consent of such third party which may own the Marks. </p>

    <h2>XXII. CONTACT INFORMATION</h2>
    <p>If you have any questions regarding these Terms and Conditions or the Services, you can contact us using the <a href="/site/contact">contact form</a>.</p>

    <br>

    <h2>Appendix A</h2>

    <h2 class="m-t0">Commercial License</h2>

    <h3>1. Definitions</h3>

    <p><strong>Site</strong>: the Site offered by Treatstock at http://www.treatstock.com refers to the Treatstock Site, API’s, software applications or any approved means or utility either currently in existence or in the future; the software and source code used by Treatstock to provide such services; user interface layouts, designs, images, text, knowledge base articles, program offers; site information provided in reports (such as popular keyword searches); and all other intellectual property protected under copyright, trademark, patent, publicity, or any other proprietary right.</p>

    <p><strong>Treatstock</strong>: includes Treatstock, Inc. and all licensed affiliates and partners.</p>

    <p><strong>Visitor</strong>: registered or unregistered User who is browsing the Site.</p>

    <p><strong>User</strong>: an individual or a legal entity making use of the Services that are offered through the Site.</p>

    <p><strong>Account</strong>: User\'s registration record on the Site.</p>

    <p><strong>3D Model</strong>: 3D printable file supplied by the User.</p>

    <p><strong>3D Print</strong>: 3D model turned into physical object by a 3D printer.</p>

    <p><strong>KIT</strong>: a set of separate parts to be assembled.</p>

    <p><strong>Author</strong>: the User who supplies a 3D Model for Treatstock.</p>

    <p><strong>Manufacturer</strong>: the User who operates a 3D printer and offers 3D printing services through the Site.</p>

    <p><strong>Order</strong>: An order placed by the User through the Site for any 3D Print job with the selected Manufacturer to turn the Author’s 3D Model into the 3D Print upon receiving payment for the Author, Manufacturer, Service and Delivery Fees.</p>

    <p><strong>Customer</strong>: any User that uses the Site to Order a 3D Model or 3D Print.</p>

    <p><strong>Product Page</strong>: is the product page or interface that displays the 3D Model assets available for Order, typically on the Site www.treatstock.com</p>

    <p><strong>Delivery Service</strong>: third party services used for delivery of a 3D Print from a Manufacturer to a User.</p>

    <p><strong>User Content</strong>: 3D Models, images, videos, text, or any other content that the User submits to the Site.</p>

    <p><strong>Author Fee</strong>: the total fee due to the Author for use of their 3D Model in an order.</p>

    <p><strong>Printing Fee</strong>: the total fee due to the Manufacturer for the printing services, including the start-up and material costs of each individual printing job and the costs of the material used to produce the 3D, as offered by the Manufacturer on the Site.</p>

    <p><strong>Service Fee</strong>: the fee due to Treatstock in consideration for the Services provided by Treatstock and any applicable taxes.</p>

    <p><strong>Delivery Fee</strong>: Any fee due and owed to any party related to services regarding the delivery of a 3D print from a Manufacturer to a Customer.</p>

    <p><strong>Services</strong>: The services offered by Treatstock through this Site..</p>

    <p><strong>Intellectual Property</strong>: Any copyright, patent, trademark, trade secret, right of publicity, or any other proprietary right throughout the world.</p>

    <h3>2. Parties</h3>

    <p class="p-l20">2.1. This License Agreement is made effective between Author, Customer, and Manufacturer upon making a purchase of any 3D Model product from the Site.</p>

    <p class="p-l20">2.2. By purchasing any 3D Print of the 3D Model published under this License Agreement (or set of 3D Models such as KIT) from the Site, Customer agrees to these Terms of License Agreement.</p>

    <h3>3. Rights Granted</h3>

    <p class="p-l20">3.1. Author represents and warrants to have the right to grant, an irrevocable, nonexclusive, worldwide license to reproduce, prepare derivative works of, turn into physical objects, incorporate into other works, and otherwise use any 3D Model or KIT selected by a User in using the Site.</p>

    <p class="p-l20">3.2. Author grants the Manufacturer, selected by the User during the placement of an Order, an irrevocable, nonexclusive, worldwide license to copy, prepare derivative works of (including, but not limited to making “Slices”), and reproduce in physical form any 3D Models or sets of 3D Models (KIT), solely for the purposes of printing and processing a selected Order.</p>

    <p class="p-l20">3.3. Changes of 3D Models with the possibility to print.</p>

    <p class="p-l20">3.4. Manufacturer shall print the exact amount of 3D Prints of the selected models described in Order.</p>

    <p class="p-l20">3.5. Manufacturer shall destroy all defective parts, or unsuccessful attempts to print any Model.</p>

    <p class="p-l20">3.6. Manufacturer shall destroy all digital copies of Author’s 3D Model included in the Order, including all derivative work and trace logs, after finishing or if the Order is cancelled within one day.</p>

    <p class="p-l20">3.7. Author grants the Manufacturer who prints the 3D Model the right to take photos and video of the 3D Print and upload them to the Site in order to ensure quality, or to improve the User experience, or for advertising purposes.</p>

    <p class="p-l20">3.8. Author grants the Customer the right to use the 3D Print in any manner at their own discretion and at their own risk (including but not limited to commercial use, sales, rentals, photo or video compilations, etc.), except if the Customer may not use an Order to reproduce the original digital 3D model (for example, “3D scan” or “3D photo”) is prohibited.</p>

    <h3>4. Royalties and Fees</h3>

    <p class="p-l20">4.1. Author, after publishing the 3D Model in accordance with these Terms of License Agreement, specifies a price for a single item of the 3D Model or a KIT.</p>

    <p class="p-l20">4.2. In order to use the rights described in the Clause of this Agreement entitled; “Rights Granted”, the Customer must pay the Author, using the Treatstock Site, a fee in the form of royalties in the sum of the item price specified by the Author and multiplied by the number of ordered 3D Prints (in the case of the KIT - the KIT price multiplied by the quantity ordered KIT).</p>

    <p class="p-l20">4.3. The above payment process is completed through the regular Order Payment on the Site. The Author promises not to use any other methods of accepting payment from Treatstock customers for Treatstock orders, except for payment through the Site.</p>

    <p class="p-l20">4.4. In the case that the price specified by the Author is different from the currency that is used to process the order, Treatstock will use exchange rates at its sole discretion and set prices for Customers corresponding with an amount in the currency of the order.</p>

    <p class="p-l20">4.5. Defective attempts to print are NOT payable.</p>

    <h3>5. Termination</h3>

    <p class="p-l20">5.1. Author can stop new agreements by removing the 3D Model from publication on the Site</p>

    <p class="p-l20">5.2. Termination of new agreements means that no new order with this 3D Model can be made. All rights for already made orders remain after the termination of the agreement. For example, the Manufacturer must finish printing and processing orders containing models that have been removed from publication, in the case that the order was made before the model was removed from publication.</p>

    <p class="p-l20">5.3. The license rights granted under this Agreement will immediately and automatically be terminated without notice if you fail to comply with any term or condition of this Agreement. Upon notice of termination, you must destroy all copies of 3D Models.</p>

    <h3>6. Refunds</h3>

    <p class="p-l20">6.1. This agreement is for orders that have already been made. It is non-refundable by Author’s initiative. An Author cannot revoke the right to print the order.</p>

    <p class="p-l20">6.2. If the Manufacturer claims that they cannot print an order for any reason, or they prove that while trying to print an order that it is defective, Treatstock reserves the right to cancel the order. In that case, the Author’s royalty will not be paid.</p>

    <p class="p-l20">6.3. Refund by Customer’s initiative</p>

    <h3>7. Tax</h3>

    <p class="p-l20">7.1. Author understands and agrees that they are solely responsible for determining their applicable tax reporting requirements and any applicable taxes for the services they provide to Users through the Site.</p>

    <p class="p-l20">7.2. Treatstock reserves the obligation to act as withholding US tax agent for non-US Users and entities. Author may see withholding percentage in specific area of the personal account after filling tax information form.</p>
    <p class="p-l20">7.3 If you have not provided the correct tax information within this time, Treatstock will deduct 30% from your earnings. It is crucial to remember to submit all the necessary paperwork to avoid 30% deduction. Also, in order to withdraw your earnings from Treatstock account, please login with your PayPal account.</p>
    <p class="p-l20">7.4 If you are a non-US designer and do not provide a W-8BEN, we are required under US tax law to treat you as a US person and withhold 28% from income tax and send it to the IRS. This also applies to persons who have not furnished their taxpayer identification number (TIN) in the manner required such as missing or incorrect information. This is known as "backup withholding."</p>
    <div class="p-l20">
        <p>7.5 Treatstock acts as a third party settlement organization that is responsible for reporting the gross reportable transaction amounts paid to participating payees in their network. This information can be found <a href="https://www.irs.gov/uac/Third-Party-Network-Transactions-FAQs" target="_blank">here</a>.</p>
        <p>If you are a non-US taxpayer, your royalties from your US earned income will be subject to a 30% withholding tax and you will have to provide <a href="https://www.irs.gov/uac/Form-W-8BEN,-Certificate-of-Foreign-Status-of-Beneficial-Owner-for-United-States-Tax-Withholding" target="_blank">Form W-8BEN</a> until or unless you complete the necessary paperwork to claim full exemption. A separate form (<a href="https://www.irs.gov/uac/About-Form-W9" target="_blank">W-9</a>) is applicable for US Authors.</p>
        <p>You can claim a specific amount of exemption depending on the tax treaty your country has with the US.</p>
        <ul>
        <li>You can <a href="https://www.irs.gov/Businesses/International-Businesses/United-States-Income-Tax-Treaties---A-to-Z" target="_blank">check which countries have a tax treaty with the USA here.</a></li>
        <li>And check <a href="https://www.irs.gov/publications/p515/ar02.html" target="_blank">your country’s rate here</a> – see the 2nd and 4th table and look in the far right-hand column – the withholding rate for royalties come under ‘Copyrights’ (‘Income category 12’.)</li>
        </ul>
        <p>It is crucial to remember that if you don’t submit all the necessary paperwork, 30% will be deducted from your earnings.</p>
    </div>


    <h3>8. Notices</h3>

    <p class="p-l20">8.1. This is NOT an agreement of ownership.</p>

    <p class="p-l20">8.2. This license is non-transferable.</p>

    <p class="p-l20">8.3. You may not rent, lease, sublicense, distribute, lend or transfer Author’s digital 3D Models, or any copy or portion of 3D Models, without prior written consent from the Author. You are granted only the rights expressly stated in this Agreement, and you may not use 3D Models for any other use.</p>

    <p class="p-l20">8.4. The author can publish his or hers 3D Model for the total cost of 0$ but the copyright will always reside with the author.</p>

    <p class="p-l20">8.5. If you do not agree to the terms, you are not permitted to use 3D Models.</p>

    <p class="p-l20">8.6. Purchasing the rights to print 3D Models as prescribed in this agreement does not allow resale of digital 3D Models parts.</p>

    <h3>9. IP rights and Trademarks</h3>

    <p>You acknowledge and agree that 3D Models listed on the Site and its permitted copies, and the trademarks associated with the Site, are the intellectual property owned by their respective owners. 3D Models or any other User Content on Site are protected by Copyright Laws including without limitation, by United States Federal Copyright Law, respective State Intellectual Property Law, international treaty provisions, and applicable laws in the jurisdiction of use.</p>

    <h3>10. Warranty</h3>

    <p>Treatstock warrants that the printed product will substantially meet the features of the ordered 3D Model within the limitations of the current 3D printing technology. Treatstock does not guarantee that any 3D Model can be applied for any particular purpose. For any real-world application the user shall use the 3D Models at his/her own risk.</p>

    <h3>11. Disclaimer</h3>

    <p>THE SITE AND THE SERVICES ARE PROVIDED IN THE FOREGOING PARAGRAPH "AS IS" AND "AS AVAILABLE" BASIS WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. TREATSTOCK CANNOT WARRANT THE PERFORMANCE OR RESULTS YOU MAY OBTAIN BY USING THE SITE. TREATSTOCK DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR NON-INFRINGEMENT. SOME STATES AND JURISDICTIONS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES, SO THE FOREGOING LIMITATIONS MAY NOT APPLY TO YOU.</p>

    <p>EXCEPT FOR THE FOREGOING LIMITED WARRANTY, AND FOR ANY WARRANTY WHICH MAY NOT BE LIMITED OR EXCLUDED BY LAW APPLICABLE IN YOUR JURISDICTION, TREATSTOCK MAKES NO WARRANTIES, CONDITIONS, REPRESENTATIONS OR TERMS, EXPRESS OR IMPLIED, WHETHER BY STATUTE, COMMON LAW, CUSTOM, USAGE OR OTHERWISE AS TO OTHER MATTERS, INCLUDING WITHOUT LIMITATION, NON INFRINGEMENT OF THIRD PARTY RIGHTS, TITLE, INTEGRATION, SATISFACTORY QUALITY, MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE.</p>

    <h3>12. Limitation of Liability</h3>

    <p>YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT TREATSTOCK SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, COMPENSATORY, PUNITIVE OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF INCOME, PROFITS, CONTRACTS, DATA, OR OTHER INTANGIBLE LOSSES OF ANY KIND WHATSOEVER RESULTING FROM THE USE OF OR INABILITY TO USE THE SERVICES.</p>

    <p>IN NO EVENT SHALL TREATSTOCK OR OUR DIRECTORS, EMPLOYEES, PARTNERS, AFFILIATES OR SUPPLIERS, BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY LOSS OF PROFITS, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER, WHETHER OR NOT WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, AND ON ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH OUR SITE, OUR SERVICES OR THIS AGREEMENT, HOWEVER ARISING INCLUDING NEGLIGENCE.</p>

    <p>FURTHER, WE SHALL NOT BE LIABLE IN ANY WAY FOR THIRD PARTY GOODS AND SERVICES OFFERED THROUGH OUR SITE INCLUDING WITHOUT LIMITATION THE PROCESSING OF ORDERS.</p>

    <p>SOME JURISDICTIONS PROHIBIT THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SO THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.</p>

    <h3>13. Possible changes</h3>

    <p class="p-l20">13.1. Treatstock reserves the right to make changes to the Site, the Services, Agreement and the Terms and Conditions at any time. Such amended Agreements will apply to new purchases made on Site after the date of its application.</p>

    <p class="p-l20">13.2. Any amended Agreement shall be effective upon posting on the Site or through some other reasonable means of communication. Treatstock will take reasonable efforts to post notices at least one day before making any changes to this Agreement. Please check the Agreement published on this Site regularly to ensure that you are aware of all the terms governing the Site and the Services.</p>

    <p class="p-l20">13.3. Leaving models published after the change agreement is agreed to publish them on the new agreement. If you do not agree - remove your 3D models from publication timely.</p>

    <h3>14. Applicable Law</h3>

    <p>These Terms and Conditions shall be governed by the laws of the State of Delaware, USA, without regard to the conflicts of law principles thereof that would apply the law of any jurisdiction other than Delaware, USA. Any and all disputes arising from these Terms and Conditions, including disputes relating to the validity thereof, and any disputes related to the use of the Treatstock Services or Site or relating to the products ordered and delivered, shall be brought in the federal and state courts located in the State of Delaware, USA. The United Nations Convention on the International Sale of Goods shall not apply to the Treatstock Services or Site or relating to the products ordered and delivered.</p>

    <h3>15. Contact Information</h3>

    <p>If you have any questions regarding these Terms and Conditions or the Services, you can contact us by sending an email to
        <script>
            var parts = ["support", "treatstock", "com", "&#46;", "&#64;"];
            var mailAddress = parts[0] + parts[4] + parts[1] + parts[3] + parts[2];
            document.write(\'<a href="mailto:\' + mailAddress + \'">\' + mailAddress + \'</a>\');
        </script>.
    </p>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '7', '4', 'ru', 'Условия использования', '
    <h2>I. ВВЕДЕНИЕ:</h2>

    <p>Добро пожаловать на сайт Treatstock! Мы очень рады  Вас здесь видеть. Пожалуйста, прочтите эти  Условия пользования прежде, чем начинать пользоваться сайтом Основные Условия составляют обязательным юридическим соглашением между вами и , Treatstock касающееся использования вами Веб-сайта и Услуг.</p>
    <p>Условия пользования являются юридическими обязательства между вами и Treatstock, Inc. и ее партнерами (коллективно, "Treatstock", "мы", "нас" или "наш"). Это соглашение (вместе с Политикой конфиденциальности Treatstock,  “Условия пользования”) регулирует ваши права и обязанности при использовании услуг, предоставляемых <a href="https://www.treatstock.com">treatstock.com</a>, (далее "Treatstock Сайт" или "Сайт") и любыми мобильными приложениями Treatstock ("Приложения").</p>
    <p>Используя наши услуги, вы соглашаетесь и принимаете настоящие Условия, с соблюдением всех применимых законов и правил. Вы также подтверждаете, что любой 3D-файл ( "3D-модель") загруженый на Treatstock Сайт не нарушает ни федерального закона ни нормативного акта, в том числе, но не ограничиваясь теми  об огнестрельном оружие. Treatstock оставляет за собой право время от времени вносить изменения в Сайт, Услуги сайта и изменять данные Условия пользования в любое время. Такие изменения Условий будут вступать в силу с момента их размещения на Сайте или каким-либо другим умесным способом. Treatstock будет предпринимать разумные усилия для сообщений, касающихся каких-либо изменений настоящих Условий. Регулярно проверяйте "Условия использования" опубликованные на сайте, чтобы убедиться в том, что вам известны Условия, регулирующие Сайт и его Услуги. Кроме того, вы несете ответственность за поддержание применимых стандартов поддерживаемых ASTM, ANSI, ASME, или другими организациями по стандартизации.</p>
    <p>Если вы не согласны со всеми Условиями, описанными выше, прекратите доступ и использование  данного сайта. </p>

    <h2>II. ОПРЕДЕЛЕНИЯ:</h2>

    <p><strong>Сайт:</strong> Сайт предлагаемый Treatstock на <a href="https://www.treatstock.com">treatstock.com</a> относится к Treatstock Сайту, API, программному обеспечению приложения или какому-либо утвержденному средству или услугам существующих в сейчас или в будущем; программное обеспечение и исходный код, используемый Treatstock для предоставления таких услуг; макеты пользовательского интерфейса, дизайн, изображения, текст, базы данных, программ офферт; Информация на сайте представлена ​​в отчетах (например, популярных поисковых запросов по ключевым словам); и все другие права на интеллектуальную собственность под защитой авторских прав, товарных знаков, патентов, рекламы или любого другого права собственности.</p>
    <p><strong>Treatstock:</strong> включает в себя Treatstock, Inc. и всех лицензированных филиалов и партнеров.</p>
    <p><strong>Посетитель:</strong> зарегистрированный или незарегистрированный Пользователь, который просматривает сайт.</p>
    <p><strong>Пользователь:</strong> физическое или юридическое лицо, использующий Услуги, которые предлагаются через Сайт.</p>
    <p><strong>Аккаунт:</strong> зарегистрированный Пользователь на Сайте.  </p>
    <p><strong>3D модель:</strong> файл 3D версии для печати представляемый Пользователем.</p>
    <p><strong>3D Print:</strong> 3D Модель превращенная в физический объект с помощью 3D-принтера.</p>
    <p><strong>KIT:</strong> набор отдельных частей для сборки.</p>
    <p><strong>Автор:</strong> Пользователь, кто предлагает 3D-модель для Treatstock.</p>
    <p><strong>Производитель:</strong> Пользователь, кто обслуживает  3D-принтер и предлагает услуги 3D печати через сайт.</p>
    <p><strong>Заказ:</strong> заказ, размещенный Пользователем посредством Сайта для любой 3D печати с выбранным Производителем, чтобы напечатать авторскую 3D Модель после получения гонорара Автору, Производителю, сервисных сборов и сборов за доставку.</p>
    <p><strong>Клиент:</strong> любой Пользователь, который использует Сайт заказавший 3D Модель или 3D напечатанное изделие.</p>
    <p><strong>Страница с Информацией о Товаре:</strong> страница изделия или интерфейс, который отображает 3D Модели доступные для Заказа, через Сайт www.treatstock.com</p>
    <p><strong>Служба доставки:</strong> услуги третьих лиц, используемые для доставки 3D напечатанного изделия от Производителя к Пользователю.</p>
    <p><strong>Пользовательский Контент:</strong> 3D-модели, изображения, видео, текст или любое другое содержание, которое Пользователь размещает на Сайте.</p>
    <p><strong>Авторский Сбор:</strong> общий платеж Автору за использование 3D Модели в Заказе.</p>
    <p><strong>Сбор за Печать:</strong> общий платеж Производителю за услуги печати, в том числе начало печати и материальные затраты каждого отдельного заказа печати и затраты материала, используемого для производства 3D напечатанного изделия.</p>
    <p><strong>Сбор за услуги:</strong> платеж Treatstock  за услуги, предоставляемые Treatstock и любые применимые налоги.</p>
    <p><strong>Сбор за доставку:</strong> платеж третьим лицам, связанный с услугами поставки 3D напечатанного изделия от Производителя к Клиенту.</p>
    <p><strong>Услуги:</strong> Услуги, предоставляемые Treatstock через Сайт.</p>
    <p><strong>Интеллектуальная Собственность:</strong> авторские права, патенты, товарные знаки, коммерческая тайна, право на публичное использование, или любое другое право собственности по всему миру.</p>

    <h2>III. УСЛУГИ TREATSTOCK: </h2>
    <p>Услуги Treatstock сочетают в себе различные потребности 3Д печати, предоставляемые посредством Сайта. Treatstock это площадка для поиска и заказа всего, что может быть напечатано на 3Д принтере.</p>
    <p>Пользователи могут выбирать из широкого спектра 3Д моделей, добавленые дизайнером, а затем распечатать эту модель на 3Д принтере. Кроме того, пользователь может выбрать печатный сервис, который изготовит реальный объект из модели и отправит его вам. Кроме того, пользователь может подать заявку, на создание 3D объекта дизайнером по индивидуальному заказу.</p>
    <p>Treatstock также оказывает поддержку дизайнерам и печатным сервисам, в случае возникновения проблем с производством 3D-моделей. Сообщества и форумы для членов Treatstock созданы специально для вопросов и обмена опытом.</p>
    <p>Кроме того, владелец 3D принтера имеет доступ к учебным материалам, для оказания содействия с процессом производства.</p>

    <h2> IV. УСЛОВИЯ ОКАЗАНИЯ УСЛУГ:</h2>
    <p>Для того, чтобы воспользоваться Услугами сайта, вы подтверждаете, что вы достигли законного возраста и вы имеете юридическое право заключать данное соглашение. Если вы не достигли законного возраста, Вы можете воспользоваться услугами Treatstock.com исключительно с согласия родителей или опекунов.</p>
    <p>Чтобы воспользоваться Услугами сайта вы несете ответственность за предоставление, поддержание и обновление вашего полного имени согласно документам, текущий адрес, номер телефона, действующий адрес электронной почты, а также любую другую необходимую информацию для завершения регистрации. По нашему усмотрению, мы можем отказать в использовании сайта любому физическому или юридическому лицу.</p>
    <p>Вы несете ответственность за безопасное хранение вашего пароля. Treatstock не может и не будет нести ответственность за любые потери или ущерб от вашей неспособности поддерживать безопасность вашей учетной записи и пароля.</p>
    <p>Вы разрешаете Treatstock общаться с вами посредством электронной почты, указанной при регистрации.</p>
    <p>Чтобы аннулировать ваш аккаунт Treatstock, вы можете предпринять одно из двух. Вы можете отправить нам сообщение через Сайт в нашу службу поддержки, или <a href="/site/contact">связаться с нами</a> через свой аккаунт.</p>
    <p>Использование услуг не должно иметь незаконных или несанкционированных целей, которые могут нарушить какие-либо законы или нормативные акты.</p>
    <p>Несоблюдение или нарушение любого пункта данных Условий пользования, по усмотрению Treatstock приведет к немедленному прекращению ваших услуг.</p>
    <p>Мы оставляем за собой право отказать или прекратить оказание Услуг любому без предварительного уведомления в любое время, по любой причине.</p>
    <p>Услуги, предлагаемые Treatstock через Сайт состоят исключительно из предоставления онлайн-платформы, которая содейсвует печатному сервису в осуществлении 3D Печати авторских 3D моделей для пользователей с возможностью использования Служб доставки, в том числе проводить платежные операции между пользователями, авторами, производителями и службами доставки.</p>
    <p>Услуги позволяют Авторам загрузить 3D Модели и другой контент на Сайт и указать авторские сборы. Услуги позволяют печатному сервису перечислить список услуг по 3D печати и указать сумму сбора за печатные услуги на Сайте. Это позволит пользователю заказать у производителя печать  закачанной 3D Модели в напечатанный 3D объкт. После того, как заказ обработан, Пользователь может выбрать способ доставки. Кроме того, пользователь сможет сам забрать напечатанный 3D объект, если это необходимо.</p>
    <p>Treatstock не является участником ни одного из соглашений, заключаемых между производителями, авторами и пользователями, а так же Treatstock не является не производителем не исполнителем никаких услуг по 3D печати или финансовых услуг.</p>
    <p>Treatstock не обязан контролировать поведение производителей, авторов и пользователей, а так же загруженый контент пользователями на Сайт и перечень услуг по 3D печати, предоставляемых производителем. Treatstock не несет никакой ответственности в этом отношении.</p>
    <p>Treatstock обеспечивает защиту для всех покупок, и, таким образом, оставляет за собой право отменить Заказ и вернуть деньги обратно заказчику полностью или частично по своему собственному усмотрению, используя процедуру, описанную в пункте "защита покупок и политика возврата"  раздела XII.</p>

    <h2>V. ПОЛЬЗОВАТЕЛЬ:</h2>
    <p>Услуги, предлагаемые Treatstock доступны только для лиц 18-ти (восемнадцати) лет или старше, которые могут составлять юридически обязывающие контракты согласно применимому законодательству и которые зарегистрировались  как Пользователь на Сайте и создали учетную запись.</p>
    <p>Пользователь или любой посетитель Сайта гарантирует, что вся представленная информация, является точной, правдивой, законной и не нарушает прав третьих лиц. Пользователь или любой посетитель Сайта гарантирует, что он не будет использовать Услуги для:</p>
    <ul>
        <li>отправки незапрашиваемых писем ("спама");</li>
        <li>запуск или использования любыех автоматизированныех способов, включая, но не ограничиваясь "пауками", "роботами", "Load тестерами" и т.д., для отправки сообщений больше, чем возможно  физическим лицом или создавать учетные записи пользователей (за исключением того, что мы предоставляем операторы общественных поисковых систем отзывными разрешение на использование пауков для копирования материалов с сайтов или услуг для единственной цели, а исключительно, насколько это необходимо для создания общедоступных поисковых индексов материалов, но не кэшей или архивов таких материалов ); </li>
        <li>сбор или храниние личной информации о других, отличных от информации, собранной в ходе текущего использования Сервиса;</li>
        <li>угрозы или причинения беспокойств любому иному лицу или юридическому лицу;</li>
        <li>получение или попытки получить несанкционированный доступ к любой компьютерной системе, сервера, сети или оборудования Treatstock, других пользователей или любых иных третьех сторон;</li>
        <li>того, что может перегрузить, повреждить, отключить, нарушить или нанести ущерб Сайту или мешать использованию Услуг любым иным пользователем;</li>
        <li>загружать, передавать или распространять какие-либо компьютерные вирусы, черви, или любое программное обеспечение, предназначенное для повредения или измения компьютерной системы или данных; </li>
        <li>адаптации, модифицирования или перепроектирования любой из систем или протоколов Treatstock;</li>
        <li>адаптации, изменения или перепроектирования 3D-Модели другого пользователя и любого из его производных в любой форме или формате; или</li>
        <li>переформатирования, перепродажи или распространение Услуг каким-либо образом без явно выраженного согласия Treatstock в письменной форме.</li>
    </ul>

    <p>Для использования некоторых Услуг, необходимо зарегистрировать учетную запись на Сайте ( «Аккаунт»). Вы должны предоставить действительную, точную идентификацию, контакт, а также другую информацию, которая может потребоваться в рамках процесса регистрации счета и / или дальнейшего использования Услуг, и вы должны сохранить ваши данные учетной записи обновляются. Ты не должен:</p>
    <ul>
        <li>выбрать или использовать, как имя пользователя, имя другого лица с целью выдать себя за этого человека;</li>
        <li>использовать, как имя пользователя, имя при условии каких-либо прав другого лица без соответствующего разрешения; или</li>
        <li>использовать, как имя пользователя, имя, которое является оскорбительным, вульгарным или непристойным.</li>
    </ul>
    <p>Вы несете ответственность за сохранение конфиденциальности вашего пароля и учетной записи службы, и несут полную ответственность за все действия, которые происходят в вашем аккаунте. Вы должны немедленно уведомить нас о любых изменениях вашего права пользоваться Услугами, нарушения безопасности или несанкционированного использования вашей учетной записи. Вы никогда не должны публиковать, распространять или после Логин информацию для вашего аккаунта. Мы не будем нести ответственность за любые убытки или ущерб от вашей неспособности поддерживать безопасность вашего счета и пароль.</p>
    <p>Вы должны иметь возможность удалить свой аккаунт, либо непосредственно, либо через запрос, направленный к одному из наших сотрудников или филиалов.</p>
    <p>В случае, Treatstock определяет пользователь нарушил настоящие Условия, Treatstock может, по своему усмотрению, без какой-либо компенсации отказать Пользователю или юридическое лицо любой дальнейший доступ к сайту, и / или любой из его услуг, и / или изменения критерии отбора в любое время без предварительного уведомления.</p>
    <p>Любая попытка Пользователем повредить Сайт или подорвать законные операции Услуг, может являться нарушением уголовного и гражданского законодательства, а также должна быть сделана любая подобная попытка, Treatstock оставляет за собой право требовать возмещения убытков от любого такого Пользователя в полном объеме насколько это разрешено законом.</p>
     
    <h2>VI. ПОЛЬЗОВАТЕЛЬСКИЙ КОНТЕНТ</h2>
    <p>Термин "Пользовательский контент" в настоящих Условиях означает любое содержание, загруженное вами на сайт, в том числе, но не ограничиваясь всеми загруженными 3D-дизайнами, 3D моделями, 3D-файлами, изображениями объектов, изображениями моделей , комментариями и отзывами на сайте. Весь Пользовательский контент не должен содержать оскорбительной, угрожающей, клеветнической, непристойной, мошеннической, обманчивой, вводящей в заблуждение, оскорбительной, порнографической, незаконной информации или нарушающий авторские права на интеллектуальную собственность. Правами интеллектуальной собственности являются авторское право, патенты, зарегистрированный дизайн, право на промышленный образец, торговую марку, коммерческую тайну, или любой другой собственности или промышленного права. Treatstock оставляет за собой право, но не берет на себя никаких обязательств просматривать Пользовательский контент, разбираться, и/или принимать соответствующие меры в отношении вас по своему собственному усмотрению, в том случае, если вы нарушаете настоящие Условия, или иным образом влекете ответственность на Treatstock или другое лицо. Такие действия могут включать в себя удаление или изменение вашего Пользовательского контента, блокирования аккаунта и/или обращение в правоохранительные органы.</p>
    <p>Treatstock не принимает на себя никакой ответственности или обязательств просматривать контент, создаваемый пользователями, и никоим образом не может нести ответственность за содержание контента, созданного пользователями. Мнения, выраженные в Пользовательском контенте не обязательно являются мнением Treatstock.</p>
    <p>Весь Пользовательский контент, включая 3D Модели, Вы тем самым предоставляете нам всемирную, неисключительную, сублицензируемую, переуступаемую, полностью оплаченную, безвозмездную, бессрочную и безотзывную лицензию на использование, копирование, публичное воспроизведение, демонстрацию, и изменение пользовательского контента связанного с Сайтом, Услугами и нашими (преемниками и правопреемниками) бизнеса, в том числе без ограничений для продвижения и распространения части или всего Сайта и  Услуг (и производных продуктов) в любых медиа-форматах и через любые медиа-каналы (включая, среди прочего, сайты и веб-каналы третьих лиц).</p>
    <p>Настоящим вы также предоставляете каждому пользователю Сайта и/или Услуг неэксклюзивную лицензию на получение доступа к предоставленным вами Пользовательского контента (за исключением 3D-моделей, которые описаны ниже) через Сайт и/или Услуги, а также на использование, редактирование, изменение, воспроизведение, распространение, подготовку производных продуктов, отображение, применение такого Пользовательского контента. Каждый раз, когда вы публикуете свои 3D-Модели на сайте, Коммерческая лицензия Treatstock предоставляет ограниченное использование защищенного авторским правом материала другими, которую можно найти в Приложении А. </p>
    <p>Для ясности, вышеприведенная лицензия дает согласие на использование контента нам и нашим пользователям, но не влияет на другие права собственности или лицензионные права на созданный пользователем контент, в том числе право на предоставление дополнительных лицензий на ваш пользовательский контент, если иное не оговорено в письменной форме. Вы заявляете и гарантируете, что имеете все права на предоставление такой лицензии без посягательства или нарушения любых прав третьих лиц, включая, среди прочего, любые права на неприкосновенность частной жизни, права на гласность, авторские права, права по договору, любые другие права на интеллектуальную собственность и права собственности.</p>
    <p>Настоящим вы также предоставляете каждому пользователю Сайта, Приложения или Услуг неэксклюзивную лицензию на получение доступа к предоставленным вами Пользовательского контента через Сайт, Приложения или Услуги, а также возможности поделиться вашим Пользовательским контентом в блогах и социальных СМИ.</p>
    <p>Treatstock не обязан делать резервную копию вашего Пользовательского контента, и Пользовательский контент может быть удален в любое время. Вы несете ответственность за создание резервных копий вашего Пользовательского контента.</p>
    <p>Вы соглашаетесь не использовать Сайт или Услуги для того, чтобы собирать, загружать, передавать, демонстрировать или распространять какой-либо Пользовательский контент:</p>
    <ul>
        <li>который  нарушает права третьих лиц, включая авторские права, торговые марки, патенты, коммерческую тайну, моральное право, неприкосновенность частной жизни, права гласности, или любой другой интеллектуальной собственности или права собственности;</li>
        <li>который является незаконным, угрожающим, оскорбительным, вызывающим беспокойство, дискредитирующим, клеветническим, обманчивым, мошенническим, агрессивным по отношению к чьей-либо частной жизни, неправомерным, непристойным, вульгарным, порнографическим, обидным, грубым, а также является сексуально откровенным, непристойным, пропагандирующим нетерпимость, дискриминацию или насилие, физический вред любого вида против любой группы или личности, способствующего незаконной деятельности или созданию оружия, незаконных материалов; </li>
        <li>что является вредным для несовершеннолетних в любой форме;</li>
        <li>что является нарушением каких-либо законов, положений, обязательств или ограничений, наложенных какой-либо третьей стороной.</li>
    </ul>
    <p>Автоматически сгенерированный контент. Treatstock оставляет за собой право собирать данные о Пользователях Сайта с целью оптимизации пользовательского опыта. В частности, это включает в себя (I) изображения модели, исполненной в различных цветах и ​​с разных углов, (II) расчет веса и размеров модели для различных 3D-принтеров.</p>

    <h2>VII. ЦЕНА </h2>
    <p>Указанная цена на Сайте состоит из гонорара за печать, гонорар автору, комиссионный сбор за доставку, сервисный сбор, а также, если это применимо дополнительные налоги. Цена указана в валюте, выбранной посетителем.</p>
    <p>Все цены указаны в соответствующих валютах. Если Пользователь не выбирает оплату в выбранной валюте, исполнитель платежных услуг проведет конверсию валюты.</p>
    <p>Поскольку каждый гонорар указан в валюте, указанной автором, производителем, службой доставки и Treatstock, Treatstock оставляет за собой право устанавливать цены в денежной единице, указанной Посетителем с использованием курса обмена валют или кросс-курса по собственному усмотрению.</p>
    <p>Указанная цена на Сайте включает в себя транспортные расходы, налог с продаж или НДС (если применимо), но исключает таможенную пошлину на импорт или, при необходимости, налоги.  При этом пошлина, налог и брокерские сборы могут взиматься при ввозе, в зависимости от страны назначения вашего заказа. Транспортные расходы, указанны только для перевозки вашего заказа и не включают в себя другие дополнительные расходы. Вы можете обратиться в таможенный орган страны назначения вашего заказа, чтобы узнать больше о применимых пошлинах и налогах. Сумма налога на импорт определяется классификацией торгуемого продукта, основываясь на коде из транспортного документа который называется "внутригосударственный кодекс". Общая импортная пошлина и налог, как правило, составляет 5-20% от общей стоимости заказа. В некоторых случаях, дополнительная комиссионные могут быть взысканы перевозчиком.</p>
    <p>Treatstock оставляет за собой право изменять цены на товары и услуги, показанные в галерее, опубликованной на Сайте, а также цены, указанные для персонифицированных заказов, в любое время.</p>

    <h2>VIII. ВЫБОР ПРОДУКТА</h2>
    <p>При заказе продукта, Пользователь подтверждает, что продукт не подпадает под действие каких-либо ограничений по лицензированию, сертификации или любых других правил или правил, которые запрещают его производства или использования. Пользователь несет исключительную ответственность за любое нарушение для каких-либо лицензионных ограничений, сертификатов, или другими нормативными актами или правилами, которые запрещают его производства или использования. Treatstock декламирует любой отказ от ответственности.</p>
    <p>При выборе продуктов Посетитель действует на его / ее собственный страх и риск. Treatstock, Автор, и производитель не несет ответственности за выбор Заказчика, пригодности для конкретной цели или ожидания, применимости продукта для каких-либо целей или использования продукции в любом регионе.</p>
    <p>Для улучшения пользовательского опыта, мы пытаемся показать все 3D-модели в разных цветах; Тем не менее, образы 3D моделей с определенными углами может быть в одном цвете.</p>
    <p>В целях защиты авторских прав, мы не разрешаем на 360 градусов вращения модели.</p>

    <h2>IX. ЗАКАЗ  </h2>
    <p>Сайт определяет IP-адрес Посетителя, чтобы определить географическое местоположение Посетителя. Эта информация используется для того, чтобы предложить близко расположенные принтеры или принтеры в непосредственной близости к физическому местоположению посетителя. Посетитель может легко изменить его/ее местоположение, указав название города, где он/она находится.</p>
    <p>При просмотре сайта, Посетитель может выбрать 3D Модель или Комплект (набор отдельных частей, которые будут в собранном виде). 3D Модель будет отображена в материале и цвете, выбранном посетителем. Далее Сайт найдет сервис, который напечатает 3D Модель в нужном материале и цвете.</p>
    <p>Сайт отображает затраты на печать выбранной 3D модели и список доступных служб по печати, которые находятся поблизости. Так же эта цена будет включать в себя авторский гонорар. Посетитель может выбрать службу печати и перейти к оформлению заказа.</p>
    <p>Во время оформления заказа, Сайт позволяет Посетителю выбрать способ доставки, включая всю необходимую информацию; на заключительном этапе Сайт показывает общую стоимость включая все сборы и применимые налогов.</p>
    <p>Посетитель соглашается с Условиями, и платит за заказ, используя выбранный метод.</p>
    <p>Treatstock отправляет заказ на исполнение к Изготовителю, выбранный Пользователем, нужного материала, цвета, а стоимость печати на основе нашего расчета.</p>
    <p>Treatstock оставляет за собой право отклонить заказ с или без объяснения причин, а также возвратить деньги полностью или частично, на Аккаунт с которого был оплачен заказ, с или без компенсации автору или производителю, по собственному усмотрению.</p>

    <h2>X. ПЛАТЕЖИ И ОПЛАТА </h2>
    <p>Treatstock принимает оплату через большинство кредитных и дебетовых карт, в том числе VISA, MasterCard, American Express, Discover, JCB и Diners Club. PayPal также поддерживается.</p>
    <p>Когда Пользователь проводит оплату кредитной/дебетовой картой, оплата находится в обработке в течение максимум 8 дней. Когда заказ принимается Сервисом, эти средства перечисляются на наш счет, и только тогда заказ будет обработан. Оплата проводится нашими партнерами, платежным решением Braintree и Stripe.</p>
    <p>Когда Пользователь проводит оплату через Alipay, средства переводятся на наш расчетный счет в момент авторизации.</p>
    <p>В том случае, если производитель не может удовлетворить ваш заказ, мы полностью возместим деньги обратно на карту или счет.</p>
    <p>В том случае, если мы определим, что 3D-печать не достаточно высокого качества, мы можем полностью или частично вернуть деньги на карту или счет, основываясь на рекомендациях по политики защиты покупок.</p>

    <h2>XI. ДОСТАВКА</h2>
    <p>Изготовитель может выбрать пикап как способ доставки, если таковой существует.</p>
    <p>Изготовитель может выбрать независимые службы доставки. В этом случае Изготовитель устанавливает цену на доставку, и Treatstock включит эту цену к общей стоимости заказа.</p>
    <p>Изготовитель может выбрать независимые службы доставки, такие как UPS, USPS и т.д. используя интеграцию с Treatstock.</p>
    <p>В этом случае Treatstock устанавливает цену на доставку по собственному усмотрению, и включает этот сбор для клиента к общей стоимости. Этот сбор может не соответствовать цене обычной службы доставки, потому что в момент заказа точные параметры 3D напечатанного изделия и возможных услуг не известно и Treatstock берет на себя связанные с этим риски.</p>
    <p>В этом случае, Treatstock берет оплату за доставку на себя, находит и оплачивает за доставку в момент когда 3D напечатанная модель готова, и тогда Изготовитель сможет напечатать почтовые наклейки для доставки.</p>
    <p>Изготовитель должен упаковать 3D напечатанный объект для доставки, если это применимо.</p>

    <h2>XII. ЗАЩИТА ПОКУПК И ПОЛИТИКА ВОЗВРАТА ТОВАРА</h2>
    <p>Treatstock допускает Пользователю отменить заказ и вернуть деньги в полном размере если печать еще не начата. Если печать уже начата, Пользователь уже не может отменить заказ.</p>
    <p>Поскольку изделие изготавливается на заказ, мы возвратим деньги только в том случае если 3D напечатанное изделие имеет неисправности или дефекты, или в том случае если изделие не дошло по почте в течение разумного периода времени.</p>
    <p>Treatstock оставляет за собой право отложить выплату гонорара Автору модели и Изготовителю, пока Пользователь не подтвердит приемлемое качество 3D напечатанного изделия.</p>
    <p>Если Пользователь не оформил претензию в течение 30 дней после покупки, Treatstock предполагает, что качество является приемлемым. Мы не принимаем претензий по истечении 30 дней после доставки.</p>
    <p>После получения 3D-печати и до его использования, Клиент должен обеспечить его качество, в случае, если качество не удовлетворяет требованиям установленных. Пользователь должен немедленно сообщите нам об этом и приложить фотографии 3D печати и оригинальной упаковке. Treatstock не будет возмещать какие-либо платежи за 3D отпечатков, которые были использованы. Пользователь должен быть готов вернуть 3D печать вместе с оригинальной упаковке, если мы просим, чтобы сделать это. Клиент несет ответственность за возвращение печать 3D, включая любые транспортные расходы.</p>
    <p>После получения жалобы от клиента, Treatstock, по своему усмотрению, может приостановить заказ и возвратить деньги Клиенту. В качестве альтернативы, Treatstock может просить Изготовителя печати перепечатать 3D изделие за свой счет, или прибегнуть к другому решению, чтобы решить проблему.</p>
    <p>Treatstock оставляет за собой право добиваться компенсации за ущерб от любого лица, которое нарушило это соглашение.</p>

    <h2>XIII. ПОЛИТИКА КАЧЕСТВА</h2>
    <p>Браком не является:</p>
    <ul>
        <li>Видимые невооруженным глазом линии слоев у объекта после печат.</li>
        <li>Следы обработки поверхностей, которые печатались с помощью поддержек.</li>
        <li>Наличие на поверхности напечатанной модели тонких нитей.</li>
        <li>Через поверхность модели на просвет видна её внутренняя структура.</li>
        <li>Следы обработки мест, в которых принтер переходил на следующий слой.</li>
        <li>Отчетливые линии на нижней поверхности изделия.</li>
    </ul>
    <p>Браком является:</p>
    <ul>
        <li>Верхние поверхности не завершены должным образом или выходят бугорчатыми.</li>
        <li>Несимметричные круги: Круги выходят безобразными и линии не соприкасаются должным образом.</li>
        <li>Углы 3D распечатки поднимаются и не прилипают к платформе.</li>
        <li>Части 3D изделия сдвигаются вдоль Х или Y оси.</li>
        <li>Смещение слоев 3D распечатки постепенно наклоняются.</li>
        <li>Недостаточное экструдирование.</li>
        <li>Части, или целые стенки 3D изделия не сплавлены и не касаются друг друга.</li>
        <li>Самые нижние части 3D распечатки сжимаются до того как объект достигнет надлежащих размеров.</li>
        <li>Первый слой не прилипает к платформе/ и весь процесс быстро дает сбой.</li>
        <li>Внешние провисания: множество нитей между раздельными частями модели.</li>
        <li>Волнистость/тени появляются при печати.</li>
        <li>* Провисание пластика на детали: нижняя поверхность провисает из-за отсутствия поддержек.</li>
        <li>Слоистость нижнего слоя, четко выраженный нижний слой, толстые линии первого слоя</li>
        <li>Пушистость: очень тонкие нити из пластика появляются на поверхности распечатки.</li>
    </ul>
    <p>Правила постобработки 3D распечаток:</p>
    <ul>
        <li>Несмотря на то, что FDM-принтеры используют в качестве метода печати наплавление нитей, получаемую в результате поверхность нельзя назвать идеально гладкой. Для её улучшения используются следующие методы:</li>
        <li>Печать с максимально высоким разрешением принтера (у Ultimaker 2 таким разрешением является 20 микрон - 0,02 мм). Полученные в результате модели будут самыми гладкими, однако такая печать чревата появлением различных дефектов, и существенно удлиняет время печати.</li>
        <li>Механическая обработка. Поверхность можно сгладить, ошкурив её наждачной бумагой или обработав надфилем, однако следует проявлять осторожность.</li>
        <li>Обработка с помощью растворителей. Поверхность можно сделать идеально гладкой, используя растворители, к примеру, ацетон для ABS-пластика или дихлорметан для PLA-пластика (относительно токсичен). Модель либо погружают в емкость с растворителем, либо обрабатывают её его парами.</li>
        <li>При этом нужно учитывать, что растворители могут растворить внешние слои модели сильнее необходимого, испортив её.</li>
        <li>Следует помнить, что при работе с растворителями нужно проявлять осторожность, все работы с ними должны проводиться в проветриваемых помещениях с приточно-вытяжной вентиляцией, также должны быть использованы средства защиты для органов зрения, дыхания и кожи.</li>
    </ul>
    <p>Рекомендации, которым должен следовать автор модели для того чтобы его модель была распечатываемо:</p>

    <ol>
        <li>Тонкие элементы
            <br>
            При печати тонких элементов следует учитывать:

            <ul>
                <li>Размер самых тонких элементов не должен быть меньше диаметра сопла принтера. Иначе эти элементы просто не напечатаются.</li>
                <li>Ширина стенок (а также верхних и нижних поверхностей) детали должна превышать высоту слоя минимум в 2-3 раза (идеально, если в 4-6 раз). Если ширина меньше, то прочность стенок значительно снизится и в них могут стать видны промежутки между слоями.</li>
                <li>При проведении постобработки мелкие детали могут стать менее заметны либо исчезнуть вовсе. Обрабатывать такие места необходимо с большой осторожностью.</li>
                <li>Некоторым слайсерам (Cura) бывает проблематично построить поддержки для мелких элементов. Это можно исправить, увеличив плотность построения поддержек либо комбинировать поддержки, сгенерированные в нескольких слайсерах.</li>
                <li>Уровень детализации модели ухудшается вместе с уменьшением её размера при масштабировании. Некоторые элементы модели, которые хорошо воспроизводятся на изделиях большого размера, не пропечатаются на уменьшенной версии, поэтому их необходимо просто исключать или изменять. Необходимо аккуратно готовить к печати любые модели, подвергшиеся масштабированию.</li>
            </ul>
        </li>

        <li>Куда девать поддержки<br>
            Поддержки - это поддерживающие конструкции, необходимые для печати нависающих элементов модели.
            <ul>
                <li>Допускается печать без поддержек стенок, которые имеют угол наклона до 60-70 градусов. Качество печати при этом может немного ухудшиться.</li>
                <li>Количество поддержек можно уменьшить, просто расположив модель под другим углом. К примеру, плоский объект, лежащий на горизонтальной плоскости стола, напечатается гораздо быстрее, чем стоящий вертикально.</li>
                <li>В некоторых случаях можно обойтись вообще без поддержек, просто разделив модель на несколько частей.</li>
                <li>Нужно учитывать, что поддержки портят внешний вид поверхности, поэтому модель лучше располагать под таким углом, чтобы они строились в местах, которые либо не будут видны, либо в тех местах, которые потом легко можно будет подвергнуть обработке.</li>
                <li>Снижение количества поддержек положительно влияет на процесс печати - уменьшит количество необходимого для печати материала, сократит время печати и соответственно удешевит её стоимость.</li>
                <li>- позиционирование относительно подложки</li>
                <li>When positioning the 3D Model relative to the working surface of the printer the following should be considered:
                    <ul>
                        <li>Changing the angle can significantly reduce (or increase) the amount of support structures required to build the 3D Model. For example, the overhanging element of the 3D Model can be avoided by placing the vertical Model horizontally.  </li>
                        <li>The strength of the 3D Model can be increased by placing the model so that the area with the greatest load are arranged perpendicular to the printed layers, and not parallel to them.  </li>
                        <li>The quality of the pattern with circle elements can be increased by placing the 3D Model so that it is printed horizontally. In some cases, the circles of the vertical seal can lead to their deformation (instead of circular holes — ellipses will be obtained).  </li>
                    </ul>
                </li>
            </ul>
        </li>

        <li>Размер 3D-модели
            <br>
            При выборе размера модели следует учитывать:
            <ul>
                <li>Максимальный размер модели в 3D принтерах обычно определяется его рабочим объемом (к примеру, у Ultimaker 2 этот объем составляет 230x225x205 мм).</li>
                <li>Печать моделей большего размера возможна, если разделить модель на несколько составных частей (каждая из которых помещается в рабочем объеме), и напечатать их по отдельности, после чего их соединяют/склеивают вместе.</li>
                <li>Принтер не печатает объекты тоньше диаметра сопла. То есть минимальный размер модели не должен быть меньше диаметра сопла, оптимальная толщина стенок любой модели должна в 2-3 раза превышать диаметр сопла для обеспечения прочности и стабильности конструкции.</li>
            </ul>
        </li>

        <li>Позиционирование относительно подложки<br>
            При позиционировании модели относительно рабочей поверхности принтера следует учитывать:
            <ul>
                <li>Изменение угла наклона может существенно снизить (или увеличить) количество необходимых для построения модели поддержек. К примеру, от нависающего элемента модели, которому нужна поддержка, можно избавиться, расположив вертикальную модель горизонтально.</li>
                <li>Прочность модели можно увеличить, расположив модель так, чтобы места, на которые будет оказываться большая нагрузка были напечатаны перпендикулярно слоям печати, а не параллельно им.</li>
                <li>Качество элементов-окружностей модели можно увеличить, расположив модель так, чтобы они печатались горизонтально. В некоторых случаях, вертикальная печать окружностей может привести к их деформации (из круглых отверстий получаются эллипсы).</li>
            </ul>
        </li>
    </ol>

    <p>Стандарты упаковывания:</p>
    <ul>
        <li>Используйте прочные и неповрежденные коробки с ненарушенными клапанами.</li>
        <li>Используйте с двойными стенками коробки для более тяжелых объектов.</li>
        <li>Оберните изделие по отдельности амортизирующим материалом и расположите по центру в коробке подальше от других предметов, стенок, углов, верха и низа коробки. Используйте соответствующий прокладочный материал.</li>
        <li>Поместите предметы, которые могут быть повреждены при обычном обращении, на пример при загрязнении, маркировки или нанесении самоклеящихся этикеток, во внешнюю защитную коробку.</li>
        <li>Используйте наполнители для заполнения пустот и предотвращения перемещения товаров внутри коробки во время транспортировки.</li>
        <li>Используйте Н-образный метод при упаковки пакета. Закройте и ленты внутренняя коробка с использованием метода H записи на пленку. Это поможет предотвратить случайное открытие.</li>
        <li>Ограничить движение продукта внутри коробки, используя наполнитель как мятой газеты, рыхлых заполнения арахисом или другого прокладочного материала.</li>
        <li>Судовые хрупкие продукты по отдельности, заворачивая их в минимальном 3 "толщина воздушно-клеточного прокладочного материала.</li>
        <li>Заполните любые пустоты с более амортизирующим материалом.</li>
        <li>Заверните все предметы по отдельности</li>
        <li>Используйте крепкую ленту, предназначенный для транспортировки</li>
        <li>Используйте одну метку адреса, который имеет четкую, полную поставку и возвращать информацию</li>
    </ul>

    <p>Если у вас возникли какие-либо вопросы по поводу возможного дефекта, пожалуйста, свяжитесь с нами, используя <a href="/site/contact">контактную форму</a> приложите изображение 3D-распечатки хорошего качества.<p>

    <h2>XIV. СТОРОННИЕ УСЛУГИ</h2>
    <p>Сайт может содержать ссылки на отдельные независимые сайты и услуги, или включать в себя специальные I-Frames с других сайтов и услуг. Мы не несем ответственности за содержание и доступность этих сайтов и услуг. Каждая из этих услуг имеет свои собственные условия, пожалуйста, внимательно ознакомьтесь с ними.</p>
    <p>В некоторых случаях, для обеспечения функций сайта мы можем передавать данные на другие сайты и услуги, и эти данные могут включать в себя личную информацию (или PII). Мы передаем только необходимую информацию и требуем сторонние сайты тщательно обрабатывать личную информацию, используя наш сервис вы соглашаетесь с возможностью такого перевода.</p>
    <p>Включая, но не ограничиваясь, условия могут быть изменены.</p>

    <h2>XV. НАРУШЕНИЕ АВТОРСКОГО ПРАВА</h2>
    <p>Если вы считаете, что какой-либо материал на Сайте является нарушением авторских прав или иных прав интеллектуальной собственности или законов, пожалуйста, отправьте нам уведомление о нарушении авторских прав, содержащий следующую информацию:</p>
    <p>Описание защищенной авторским правом работы, права которой, по вашему мнению, были нарушены;</p>
    <p>Описание местоположения этого материала (достаточное, чтобы мы могли найти этот материал и проверить его существование;</p>
    <p>Контактная информация уведомляющей стороны ( "Уведомляющая Сторона") адрес, номер телефона и адрес электронной почты;</p>
    <p>Письменное заявление, что у вас есть предположение, что использование материала не разрешено владельцем авторского права, его агентом или законом;</p>
    <p>Ваше заявление, сделанное под страхом наказания за лжесвидетельство, что вышеупомянутая информация является точной и что вы являетесь владельцем авторских прав или уполномочены действовать от имени владельца авторского права;</p>
    <p>Физическая или электронная подпись лица, уполномоченного действовать от имени владельца авторских прав;, которое было предположительно нарушено.</p>
    <p>В обязательном порядке, если вы не получите ответ от Treatstock в течение 10 дней с момента подачи заявления, пожалуйста, свяжитесь с Treatstock еще раз, чтобы подтвердить, что мы получили вашу первоначальную претензию. Как вы знаете, спам-блокаторы иногда отправляют важные электронные письма от неизвестных пользователей и доставка физического письма может быть отложена без предварительного уведомления в связи с некоторым количеством непредвиденных обстоятельств.</p>
    <p>Обратите внимание, что любое лицо, которое сознательно и материально, сообщает ложную информацию о нарушение может быть привлечен к ответственности за причиненный ущерб.</p>
    <p>В стремлении быть прозрачной для удаления или ограничения доступа к контенту независимо, Treatstock могут предать гласности любые уведомления о нарушении, полученных (с личной контактной информацией удалены). Это может включать в себя разместив уведомление публично-облицовочного сайта, наряду с другими методами.</p>
    <p>После того, как удаление или отключение доступа к материалу или контента в соответствии с действительным нарушением уведомления, Treatstock немедленно уведомлять пользователя, ответственного за якобы нарушающего авторские права, которые он удалили или блокировали доступ к материалу.</p>
    <p>Treatstock оставляет за собой право, по своему усмотрению, немедленно прекратить аккаунт любого пользователя, который является предметом действительного уведомление о нарушении.</p>
    <p>Если вы считаете, что предположительно нарушающие закон материалы не являются таковыми, то вы можете подать встречное уведомление  Treatstock, предоставляя следующую информацию:</p>
    <p>Заявление, определяющее материалы, которые были удалены или к которым был прекращен доступ, и расположение материалов до их удаления или прекращения к ним доступа.</p>
    <p>Ваше имя, адрес, номер телефона и адрес электронной почты.</p>
    <p>Заявление, что вы соглашаетесь освободить Treatstock от любых претензий о нарушении авторских прав.</p>
    <p>Следующее утверждение: "Я клянусь под страхом наказания за лжесвидетельство о том, что по вашему добросовестному предположению материалы были ошибочно удалены или к ним был ошибочно прекращен доступ."</p>
    <p>Ваша подпись.</p>
    <p>При получении надлежащего опровержения, Treatstock направит его стороне, которая прислала исходное уведомление об удалении. Сторона, которая прислала исходное уведомление об удалении, должна будет уведомить Treatstock в течение 10 рабочих дней о подаче иска для получения судебного распоряжения о прекращении вашего участия в предположительно противозаконной деятельности. Если Treatstock не получает такого уведомления в течение десяти (10) дней, мы можем восстановить материалы.</p>

    <h2>XVI. ПОЛИТИКА СООБЩЕСТВА</h2>
    <p>Treatstock предлагает доступ к страницам сообщества  для новых и развивающихся технологий, таких как 3D-дизайн и 3D-печать. Мы создали эти страницы сообщества, чтобы помочь участникам воплотить в жизнь свои творения, вдохновлять друг друга, обмениваться знаниями, обсуждать идеи и поддерживать новый интерес к 3D-проектированию и печати. Целью этой политики является обеспечение членов сообщества Treatstock обрести  наилучший опыт.</p>
    <p>Эта политика описывает деятельность членов сообщества, в любом форуме, мнениях, рассылках, Сайте, личной переписки, или публичных сообщениях. Для того, чтобы избежать проблем при переписки существует несколько основных правил, которым нужно следовать.</p>
    <ul>
        <li>Быть внимательным. Страницы сообщества не могут быть использованы для преследования других членов сообщества. Оскорбительные комментарии не могут быть размещены на страницах сообщества.</li>
        <li>Быть терпимым. Небезопасные, вредные, опасные или любые угрозы насилия в отношении других лиц или пропаганды насилия или незаконной деятельности не могут иметь место в страницах сообщества.</li>
        <li>Быть почтительным. Уважительно относитесь к другим членам. У всех нас есть наше собственное мнение, но разногласия не оправдание за плохое поведение. Важно помнить, что сообщество, где люди чувствуют себя неуютно или под угрозой не является продуктивным.</li>
        <li>Будьте вежливы. Уважать частную жизнь других членов. Вы не можете поделиться какой-либо частной или личной информации, которая не должна быть общей в зонах общественного пользования Сайта.</li>
    </ul>

    <h2>XVII. КОМПЕНСАЦИЯ </h2>
    <p>Настоящим Вы соглашаетесь освободить от ответственности и гарантируете возместить Treatstock и наших аффилированных лиц и любых из наших соответствующих должностных лиц, директоров, сотрудников, представителей и агентов, ущерб, связанный с любыми претензиями, требованиями, потерями, расходами, обязательствами или возмещениями (включая обоснованные судебные издержки), возникающие в результате любых претензий, которые вы нарушили в положениях настоящих Условий.</p>

    <h2>XVIII. ОГРАНИЧЕНИЕ ОТВЕТСТВЕННОСТИ </h2>
    <p>ВЫ СОГЛАШАЕТЕСЬ С ТЕМ, ЧТО TREATSTOCK НЕ НЕСЕТ ОТВЕТСТВЕННОСТИ ЗА ЛЮБЫЕ УТРАТЫ ИЛИ ПОВРЕЖДЕНИЯ ЛЮБОГО РОДА (ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ЛЮБЫМИ ПРЯМЫМИ, КОСВЕННЫМИ, ЭКОНОМИЧЕСКИМИ, КАРАТЕЛЬНЫМИ, ОСОБЫМИ, СВЯЗАННЫМИ С ПРИМЕНЕНИЕМ НАКАЗАНИЯ, СЛУЧАЙНЫМИ ИЛИ ПОБОЧНЫМИ УТРАТАМИ ИЛИ ПОВРЕЖДЕНИЯМИ), ПРЯМО ИЛИ КОСВЕННО СВЯЗАННЫЕ С ИСПОЛЬЗОВАНИЕМ ИЛИ НЕВОЗМОЖНОСТЬЮ ИСПОЛЬЗОВАНИЯ УСЛУГ.</p>
    <p>НИ В КОЕМ СЛУЧАЕ TREATSTOCK ИЛИ НАШИ РУКОВОДИТЕЛИ, СОТРУДНИКИ, ПАРТНЕРЫ, ФИЛИАЛЫ ИЛИ ПОСТАВЩИКИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПЕРЕД ВАМИ ИЛИ ТРЕТЬИМИ ЛИЦАМИ ЗА ЛЮБОЙ КОСВЕННЫЙ, СЛУЧАЙНЫЙ, СПЕЦИАЛЬНЫЙ, ПОБОЧНЫЙ ИЛИ ШТРАФНОЙ УЩЕРБ (ВКЛЮЧАЯ УЩЕРБ ОТ ПОТЕРИ ПРИБЫЛИ, ИЛИ ДРУГИЕ НЕМАТЕРИАЛЬНЫЕ УБЫТКИ), НЕЗАВИСИМО ОТ ТОГО, БЫЛИ ЛИ МЫ ПРЕДУПРЕЖДЕНЫ О ВОЗМОЖНОСТИ УЩЕРБА, И ЛЮБОЙ ДРУГОЙ ПРАВОВОЙ ТЕОРИИ, В РЕЗУЛЬТАТЕ ИЛИ С ИСПОЛЬЗОВАНИЕМ  НАШЕГО САЙТА, ​​ИЛИ НАШИМИ УСЛУГАМИ ДАННОГО СОГЛАШЕНИЯ, ВКЛЮЧАЯ НЕБРЕЖНОСТЬ. </p>
     
    <p>КРОМЕ ТОГО, МЫ НЕ НЕСЕТ ОТВЕТСТВЕННОСТИ ЗА СТОРОННИХ ПРОИЗВОДИТЕЛЕЙ ТОВАРОВ И УСЛУГ ЧЕРЕЗ НАШ САЙТ ВКЛЮЧАЯ БЕЗ ОГРАНИЧЕНИЙ ОБРАБОТКИ ЗАКАЗОВ. </p>
    <p>НЕКОТОРЫЕ ЗАКОНОДАТЕЛЬСТВА ЗАПРЕЩАЮТ ИСКЛЮЧЕНИЕ ИЛИ ОГРАНИЧЕНИЕ ПОДРАЗУМЕВАЕМЫХ ГАРАНТИЙ ИЛИ ОТВЕТСТВЕННОСТИ ЗА ОПРЕДЕЛЕННЫЕ КАТЕГОРИИ УБЫТКОВ. ПОЭТОМУ, НЕКОТОРЫЕ ИЛИ ВСЕ ОГРАНИЧЕНИЯ МОГУТ НЕ ОТНОСИТЬСЯ К ВАМ В ЭТОМ СЛУЧАЕ.</p>

    <h2>XIX. ЮРИДИЧЕСКИЕ РАЗНОГЛАСИЯ</h2>
    <p>Настоящие Условия пользования регулируются законами штата Делавэр, США, без учета конфликта юридических положений, которые применяют закон какой-либо юрисдикции, кроме штата Делавэр, США. Любые и все споры, вытекающие из настоящих Условий, в том числе споры, касающиеся их действительности, и любые споры, связанные с использованием Услуг Treatstock или Сайта, или относящиеся к заказанным продуктам, рассмотрение возникших споров и разногласий относится к исключительной юрисдикции судов, расположенных в штате Делавэр, США. Конвенция Организации Объединенных Наций о международной купле-продаже товаров не распространяется на услуги Treatstock или сайта, или относящиеся к заказанным продуктам.</p>

    <h2>XX. РАЗНОЕ</h2>
    <p>Форс-мажор. Мы не несем ответственности за неисполнение или ненадлежащее исполнение наших обязательств по настоящему Договору, если такое имеет причины вне нашего контроля, включая, без ограничения, механические, электронные отказы или ухудшение связи.</p>
    <p>Назначение. Настоящие Условия использования составлены лично для вас, и не могут быть переданы вами иным лицам, кроме как с нашего предварительного письменного согласия. Мы можем уступать, передавать или делегировать любое из своих прав и обязательств по настоящему Договору без вашего согласия.</p>
    <p>Посредничество. Ни одно ведомство, товарищество, совместное предприятие, или трудовые отношения не создается в результате этих Условий использования и ни одна из сторон не имеет полномочий обязывать другую в любом отношении.</p>
    <p>Уведомления. Если иное не указано в настоящих Условия использования, все уведомления в соответствии с настоящими Условиями пользования будут приходить в письменной форме и будут считаться надлежащими, когда они получены, если они были лично доставлены или направлены заказным письмом с уведомлением о вручении; когда квитанция в электронном виде была подтверждена, если передача осуществлялась по факсу или по электронной почте; или на следующий день после отправки, если они отправлены с условием доставки на следующий день общепризнанными службами ночной доставки. Электронные уведомления должны быть отправлены, используя <a href="/site/contact">контактную форму</a>.  </p>
    <p>Отсутствие отказа от прав. Наша неспособность исполнения какой-либо части настоящих Условий пользования не означает отказа от нашего права на соблюдение той или иной части настоящих Условий пользования в дальнейшем. Отказ от соблюдения в каждом конкретном случае не означает, что мы будем делать это и в будущем. В случае любого отказа от соблюдения этих условий использования, мы или один из наших уполномоченных представителей должны предоставить вам письменное уведомление о таком отказе.</p>
    <p>Названия статей. Названия статей и пунктов в настоящих Условиях использования созданы только для удобства и ни на что не влияют.</p>
    <p>Обратная связь. Так как в дальнейшем мы хотим совершенствовать свои Услуг, мы приветствуем все комментарии, предложения, рекомендации и обратную связь (в дальнейшем, "Обратная связь"). Настоящим Вы предоставляете всемирную, безвозмездную, бессрочную, неотзываемую, лицензию на использование и воспроизведение любым иным образом обратную связь связанную с Услугами.</p>
    <p>Использование "cookies". Вы согласны с тем, что, когда вы посещаете "cookies" на сайте, могут быть установлены на жестком диске вашего компьютера.</p>
    <p>Отсутствие поддержки и технического обслуживания. Вы признаете и соглашаетесь с тем, что компания не будет обязана предоставлять вам любую поддержку или обслуживание по отношению к Сайту.</p>

    <h2>XXI. АВТОРСКОЕ ПРАВО/ИНФОРМАЦИЯ О ТОВАРНЫХ ЗНАКАХ </h2>
    <p>Copyright © 2015, 2016 Treatstock, Inc. Все права защищены. Все торговые марки, логотипы и знаки обслуживания ( "Марки"), отображаемые на Сайтах являются нашей собственностью или собственностью третьих лиц. Вам не разрешается использовать эти Знаки без нашего предварительного письменного согласия или согласия третьей стороны, которая может владеть Знаками.</p>

    <h2>XXII. КОНТАКТНАЯ ИНФОРМАЦИЯ</h2>
    <p>Если у вас есть какие-либо вопросы относительно настоящих Условий или Услуг, вы можете связаться с нами, используя <a href="/site/contact">контактную форму</a>.</p>

    <br>

    <h2>Приложение A</h2>

    <h2 class="m-t0">Коммерческая лицензия</h2>

    <h3>1. Определения</h3>

    <p><strong>Сайт</strong>: Сайт предлагаемый Treatstock на http://www.treatstock.com относится к Treatstock Сайту, API, программному обеспечению приложения или какому-либо утвержденному средству или услугам существующих в сейчас или в будущем; программное обеспечение и исходный код, используемый Treatstock для предоставления таких услуг; макеты пользовательского интерфейса, дизайн, изображения, текст, базы данных, программ офферт; Информация на сайте представлена ​​в отчетах (например, популярных поисковых запросов по ключевым словам); и все другие права на интеллектуальную собственность под защитой авторских прав, товарных знаков, патентов, рекламы или любого другого права собственности.</p>

    <p><strong>Treatstock</strong>: включает в себя Treatstock, Inc. и всех лицензированных филиалов и партнеров.</p>

    <p><strong>Посетитель</strong>: зарегистрированный или незарегистрированный Пользователь, который просматривает сайт.</p>

    <p><strong>Пользователь</strong>: физическое или юридическое лицо, использующий Услуги, которые предлагаются через Сайт.</p>

    <p><strong>Аккаунт</strong>: зарегистрированный Пользователь на Сайте.</p>

    <p><strong>3D модель</strong>: файл 3D версии для печати представляемый Пользователем.</p>

    <p><strong>3D Print</strong>: 3D Модель превращенная в физический объект с помощью 3D-принтера.</p>

    <p><strong>KIT</strong>: набор отдельных частей для сборки.</p>

    <p><strong>Автор</strong>: Пользователь, кто предлагает 3D-модель для Treatstock.</p>

    <p><strong>Производитель</strong>: Пользователь, кто обслуживает  3D-принтер и предлагает услуги 3D печати через сайт.</p>

    <p><strong>Заказ</strong>: заказ, размещенный Пользователем посредством Сайта для любой 3D печати с выбранным Производителем, чтобы напечатать авторскую 3D Модель после получения гонорара Автору, Производителю, сервисных сборов и сборов за доставку.</p>

    <p><strong>Клиент</strong>: любой Пользователь, который использует Сайт заказавший 3D Модель или 3D напечатанное изделие.</p>

    <p><strong>Страница с Информацией о Товаре</strong>: страница изделия или интерфейс, который отображает 3D Модели доступные для Заказа, через Сайт www.treatstock.com</p>

    <p><strong>Служба доставки</strong>: услуги третьих лиц, используемые для доставки 3D напечатанного изделия от Производителя к Пользователю.</p>

    <p><strong>Пользовательский Контент</strong>: 3D-модели, изображения, видео, текст или любое другое содержание, которое Пользователь размещает на Сайте.</p>

    <p><strong>Авторский Сбор</strong>: общий платеж Автору за использование 3D Модели в Заказе.</p>

    <p><strong>Сбор за Печать</strong>: общий платеж Производителю за услуги печати, в том числе начало печати и материальные затраты каждого отдельного заказа печати и затраты материала, используемого для производства 3D напечатанного изделия.</p>

    <p><strong>Сбор за услуги</strong>: платеж Treatstock  за услуги, предоставляемые Treatstock и любые применимые налоги.</p>

    <p><strong>Сбор за доставку</strong>: платеж третьим лицам, связанный с услугами поставки 3D напечатанного изделия от Производителя к Клиенту.</p>

    <p><strong>Услуги</strong>: Услуги, предоставляемые Treatstock через Сайт.</p>

    <p><strong>Интеллектуальная Собственность</strong>: авторские права, патенты, товарные знаки, коммерческая тайна, право на публичное использование, или любое другое право собственности по всему миру.</p>

    <h3>2. Стороны</h3>

    <p class="p-l20">2.1. Настоящее Лицензионное соглашение вступает в силу в между Автором, Заказчиком и Производителем при совершении покупки любой 3D Модели на Сайте.</p>

    <p class="p-l20">2.2. Приобретая любое 3D печатное изделие 3D Модели, опубликованной в соответствии с настоящим лицензионным соглашением (или набор 3D-моделей, таких как KIT) на Сайте, Клиент соглашается с Условиями лицензионного соглашения.</p>

    <h3>3. Предоставленные Права</h3>

    <p class="p-l20">3.1. Автор заявляет и гарантирует, что имеет право предоставлять, безвозмездное, бессрочное, безотзывное, действующую во всем мире лицензию на воспроизведение, создание производных работ, воспроизведение в физические объекты, инкорпорировать в другие работы, а также иным образом использовать 3D модель или KIT, выбранный пользователем при помощи сайт.</p>

    <p class="p-l20">3.2. Автор предоставляет Производителю, выбранный Пользователем во время размещения Заказа, безотзывное, действующую во всем мире лицензию на воспроизведение, создание производных работ (в том числе, но не ограничиваясь "Слайсерами"), и воспроизведение в физической форме любую 3D Модель или наборы 3D-моделей (KIT), исключительно для печати и обработки выбранного Заказа.</p>

    <p class="p-l20">3.3. Изменения 3D Моделей с возможностью распечатки.</p>

    <p class="p-l20">3.4. Производитель должен напечатать точное количество 3D печатных изделий выбранных моделей, описанных в Заказе.</p>

    <p class="p-l20">3.5. Изготовитель должен уничтожить все дефектные детали или неудачные попытки напечатать любую модель.</p>

    <p class="p-l20">3.6. Изготовитель должен уничтожить все цифровые копии авторских 3D Моделей включеных в заказ, в том числе все производные работы и журналов трассировки, после завершения Заказа или если Заказ отменен в течение одного дня.</p>

    <p class="p-l20">3.7. Автор предоставляет Производителю, который печатает 3D Модель право делать фотографии и видео, 3D печатного изделия и загружать их на сайт для того, чтобы подтвердить качество, для улучшения Пользовательского опыта, или в рекламных целях.</p>

    <p class="p-l20">3.8. Автор предоставляет Клиенту право на использование 3D печатных изделий любым способом по своему усмотрению и на свой страх и риск (включая, но не ограничиваясь коммерческим использованием, продажей, арендой, фото или видео сборниках и т.д.), за исключением того, когда  Клиент не использует Заказ, для воспроизведения оригинальной 3D Модели (например, "3D сканирование" или "3D-фото") что является запрещенным.</p>

    <h3>4. Гонорары и сборы</h3>

    <p class="p-l20">4.1. Автор, после публикации 3D-модели в соответствии с настоящими Условиями лицензионного соглашения, назначает цену за еденицу  3D Модели или KIT.</p>

    <p class="p-l20">4.2. Для того, чтобы воспользоваться лицензией, описанной в пункте настоящего Соглашения; Клиент должен оплатить Автору гонорар, используя Сайт, указанный Автором и умноженный на число заказанных 3D печатей (в случае с KIT - цена KIT, умноженая на количество заказанных KIT).</p>

    <p class="p-l20">4.3. Описанный выше процесс оплаты оформляется через Оплату Заказа на Сайте. Автор обещает не использовать любые другие методы приема платежей от клиентов Treatstock для заказов.</p>

    <p class="p-l20">4.4. В случае, если цена, указанная автором, отличается от валюты, используемой для обработки заказа, Treatstock будет использовать обменные курсы по своему усмотрению и устанавливать цены для клиентов, в соответствующей сумме валюте заказа.</p>

    <p class="p-l20">4.5. Дефектная печать не подлежит оплате.</p>

    <h3>5. Прекращение</h3>

    <p class="p-l20">5.1. Автор может остановить новые соглашения путем удаления 3D Модели с Сайта</p>

    <p class="p-l20">5.2. Прекращение новых соглашений означает, что нельзя сделать новый заказ  3D Модели. Все права на уже соверщенные заказы остаются после окончания срока действия соглашения. Например, Изготовитель должен закончить заказы печати и обработки модели, которые были удалены из публикации, в том случае, заказ был соверщен до того, как модель была удалена из публикации.</p>

    <p class="p-l20">5.3. Лицензионные права, предоставленные в соответствии с настоящим Соглашением немедленно и автоматически прекращается без уведомления, если вы не в состоянии выполнить какое-либо из условий настоящего Соглашения. В случае такого прекращения, вы должны уничтожить 3D Модели вместе со всеми их копиями.</p>

    <h3>6. Возврат денег</h3>

    <p class="p-l20">6.1. Соглашение для заказов, которые уже были сделаны. Не подлежит возврату по инициативе автора. Автор не может аннулировать право печатать на заказ.</p>

    <p class="p-l20">6.2. Если Производитель утверждает, что по какой-то причине не возможно напечатать заказ, или подтвержается, что при попытке напечатать заказ, он неисправен, Treatstock оставляет за собой право отменить заказ. В этом случае авторский гонорар не будет выплачен.</p>

    <p class="p-l20">6.3. Возврат по инициативе клиента.</p>

    <h3>7. Налог</h3>

    <p class="p-l20">7.1. Автор понимает и соглашается с тем, что он несет полную ответственность за определение применимых требований налоговой отчетности и любые применимые налоги за услуги, которые они предоставляют пользователям через сайт.</p>

    <p class="p-l20">7.2. Treatstock оставляет за собой обязательство действовать в качестве налогового агента для неамериканских пользователей и организаций. Автор может увидеть процент удержания налогов в определенном месте аккаунта после заполнения налоговой информации.</p>
    <p class="p-l20">7.3. Если вы не предоставили правильную налоговую информацию в течение определенного  времени, Treatstock будет вычитать 30% от вашего заработка. Очень важно помнить представить все необходимые документы, чтобы избежать 30% вычета. Кроме того, для того, чтобы вывести свои доходы со счета Treatstock, пожалуйста, зарегистрируйтесь через PayPal.</p>
    <p class="p-l20">7.4. Если вы неамериканский дизайнер и не представили W-8BEN, мы обязаны в соответствии с налоговым законодательством США относиться к вам как налогоплательщику США и удерживать 28% от налога на прибыль и направить его в IRS. Это также относится и к лицам, которые не представили свой идентификационный номер налогоплательщика (ИНН) в предусмотренном порядке, такие, как отсутствие или неправильную информацию. Это известно как "вспомогательное удержание."</p>
    <div class="p-l20">
        <p>7.5. Treatstock выступает в качестве сторонней расчетной организацией, которая отвечает за отчетность общей стоимости сделки, оплаченной бенефициару. Эту информацию можно найти <a href="https://www.irs.gov/uac/Third-Party-Network-Transactions-FAQs" target="_blank">здесь</a>.</p>
        <p>Если вы не являетесь американским налогоплательщиком, гонорары от вашего дохода из США будет облагаться налогом в 30%, и вы не обязательно предоставлять <a href="https://www.irs.gov/uac/Form-W-8BEN,-Certificate-of-Foreign-Status-of-Beneficial-Owner-for-United-States-Tax-Withholding" target="_blank">форму W-8BEN</a> до тех пор пока вы не представите необходимые документы, чтобы претендовать на налоговую льготу. Отдельная форма (<a href="https://www.irs.gov/uac/About-Form-W9" target="_blank">W-9</a>) применяется для американских авторов.</p>
        <p>Вы можете претендовать на определенное количество налоговых льгот в зависимости от налогового договора вашей страны с США.</p>
        <ul>
            <li>Вы можете <a href="https://www.irs.gov/Businesses/International-Businesses/United-States-Income-Tax-Treaties---A-to-Z" target="_blank"> проверить, какие страны имеют налоговое соглашение с США здесь.</a></li>
            <li>Проверить ставку <a href="https://www.irs.gov/publications/p515/ar02.html" target="_blank">для вашей страны здесь</a> – см 2-й и 4-й таблицу и посмотрете в самой правой колонке - налоговая ставка подпадающая под "Copyrights"  (\'Категории Доход 12\'). </li>
        </ul>
        <p>Очень важно помнить, что если вы не представляете все необходимые документы, 30% будут вычтены из вашего заработка.</p>
    </div>


    <h3>8. Уведомления</h3>

    <p class="p-l20">8.1. Это не соглашение о собственности.</p>

    <p class="p-l20">8.2. Эта лицензия не может быть передана.</p>

    <p class="p-l20">8.3. Вы не можете сдавать в аренду или лизинг, лицензировать, распространять, одалживать или передавать цифровые 3D-модели автора, а также копию или часть 3D Моделей, без предварительного письменного согласия со стороны автора. Вам предоставляется только права, прямо указанные в настоящем Соглашении, и вы не можете использовать 3D модели для любого другого использования.</p>

    <p class="p-l20">8.4. Автор может опубликовать его или ее 3D-модель общей стоимостью в ноль долларов, но автор всегда будет имееть авторское право на нее.</p>

    <p class="p-l20">8.5. Если вы не согласны с условиями, вы не имеете право использовать 3D Модели.</p>

    <p class="p-l20">8.6. Условия приобретения прав на печать 3D Модели, как это предусмотрено в этом соглашении не позволяет перепродажи цифровых частей 3D Модели.</p>

    <h3>9. Права интеллектуальной собственности и Товарные знаки</h3>

    <p>Вы признаете и соглашаетесь с тем, что 3D-Модели, опубликованные на Сайте и его разрешенные копии, а также товарные знаки, связанные с сайтом, являются интеллектуальной собственностью соответствующих владельцев. 3D-модели или любой другой контент пользователя на Сайте, защищены законами об авторском праве, включая без ограничений, Федеральным законом США об авторском праве, соответствующим Государственным Законом об Интеллектуальной Собственности, положениями международных договоров и действующим законодательством в юрисдикции использования.</p>

    <h3>10. Гарантия</h3>

    <p>Treatstock гарантирует, что печатная продукция будет по существу отвечать особенностям заказанной 3D Модели в рамках ограничений технологии 3D печати. Treatstock не гарантирует, что любая 3D модель может быть применена для конкретной цели. Для практического применения пользователю следует использовать 3D-модели на его/ее собственный страх и риск.</p>

    <h3>11. Отказ от ответственности</h3>

    <p>САЙТ И УСЛУГИ ПРЕДОСТАВЛЯЮТСЯ ВАМ НА УСЛОВИЯХ "КАК ЕСТЬ" И "КАК ДОСТУПНО" БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ. TREATSTOCK НЕ ГАРАНТИРУЮТ И НЕ МОГУТ ГАРАНТИРОВАТЬ ДОСТИЖЕНИЯ ОПРЕДЕЛЕННОЙ ПРОИЗВОДИТЕЛЬНОСТИ ИЛИ РЕЗУЛЬТАТОВ ПРИ ИСПОЛЬЗОВАНИ САЙТА. TREATSTOCK ОТКАЗЫВАЕТСЯ ОТ ВСЕХ ГАРАНТИЙ, ЯВНЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ЛЮБЫМИ ПОЖРАЗУМЕВАЕМЫМИ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ ИЛИ ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННОЙ ЦЕЛИ. НЕКОТОРЫЕ ЗАКОНОДАТЕЛЬСТВА ЗАПРЕЩАЮТ ИСКЛЮЧЕНИЕ ИЛИ ОГРАНИЧЕНИЕ ПОДРАЗУМЕВАЕМЫХ ГАРАНТИЙ ИЛИ ОТВЕТСТВЕННОСТИ ЗА ОПРЕДЕЛЕННЫЕ КАТЕГОРИИ УБЫТКОВ. ПОЭТОМУ, НЕКОТОРЫЕ ИЛИ ВСЕ ОГРАНИЧЕНИЯ МОГУТ НЕ ОТНОСИТЬСЯ К ВАМ В ЭТОМ СЛУЧАЕ.</p>

    <p>ЗА ИСКЛЮЧЕНИЕМ ГАРАНТИЙ ВЫШЕУПОМЯНУТАЯ LIMITED, И ДЛЯ КАКИХ-ЛИБО ГАРАНТИЙ, КОТОРЫЕ НЕ МОГУТ БЫТЬ ОГРАНИЧЕНА ИЛИ ИСКЛЮЧЕНЫ ЗАКОНОМ ДЕЙСТВУЮЩИМ В ВАШЕЙ ЮРИСДИКЦИИ, TREATSTOCK НЕ ДАЕТ НИКАКИХ ГАРАНТИЙ, УСЛОВИЙ ИЛИ ПОЛОЖЕНИЙ, ПРЯМО ИЛИ ПОДРАЗУМЕВАЕМЫХ, БУДЬ ТО ПО ПОЛОЖЕНИЙ, СВЯЗАННЫХ С ИСПОЛЬЗОВАНИЕМ ИЛИ ИНАЧЕ В ОТНОШЕНИИ прочем, ВКЛЮЧАЯ, БЕЗ ОГРАНИЧЕНИЙ, ОТСУТСТВИЯ НАРУШЕНИЯ ПРАВ ТРЕТЬИХ ЛИЦ, название, ИНТЕГРАЦИИ, УДОВЛЕТВОРИТЕЛЬНОГО КАЧЕСТВА, ПРИГОДНОСТИ ДЛЯ КАКОЙ-ЛИБО ЦЕЛИ.</p>

    <h3>12. Ограничение ответственности</h3>

    <p>ВЫ СОГЛАШАЕТЕСЬ С ТЕМ, ЧТО TREATSTOCK НЕ НЕСЕТ ОТВЕТСТВЕННОСТИ ЗА ЛЮБЫЕ УТРАТЫ ИЛИ ПОВРЕЖДЕНИЯ ЛЮБОГО РОДА (ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ЛЮБЫМИ ПРЯМЫМИ, КОСВЕННЫМИ, ЭКОНОМИЧЕСКИМИ, КАРАТЕЛЬНЫМИ, ОСОБЫМИ, СВЯЗАННЫМИ С ПРИМЕНЕНИЕМ НАКАЗАНИЯ, СЛУЧАЙНЫМИ ИЛИ ПОБОЧНЫМИ УТРАТАМИ ИЛИ ПОВРЕЖДЕНИЯМИ), ПРЯМО ИЛИ КОСВЕННО СВЯЗАННЫЕ С ИСПОЛЬЗОВАНИЕМ ИЛИ НЕВОЗМОЖНОСТЬЮ ИСПОЛЬЗОВАНИЯ УСЛУГ.</p>

    <p>НИ В КОЕМ СЛУЧАЕ TREATSTOCK ИЛИ НАШИ РУКОВОДИТЕЛИ, СОТРУДНИКИ, ПАРТНЕРЫ, ФИЛИАЛЫ ИЛИ ПОСТАВЩИКИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПЕРЕД ВАМИ ИЛИ ТРЕТЬИМИ ЛИЦАМИ ЗА ЛЮБОЙ КОСВЕННЫЙ, СЛУЧАЙНЫЙ, СПЕЦИАЛЬНЫЙ, ПОБОЧНЫЙ ИЛИ ШТРАФНОЙ УЩЕРБ (ВКЛЮЧАЯ УЩЕРБ ОТ ПОТЕРИ ПРИБЫЛИ, ИЛИ ДРУГИЕ НЕМАТЕРИАЛЬНЫЕ УБЫТКИ), НЕЗАВИСИМО ОТ ТОГО, БЫЛИ ЛИ МЫ ПРЕДУПРЕЖДЕНЫ О ВОЗМОЖНОСТИ УЩЕРБА, И ЛЮБОЙ ДРУГОЙ ПРАВОВОЙ ТЕОРИИ, В РЕЗУЛЬТАТЕ ИЛИ С ИСПОЛЬЗОВАНИЕМ  НАШЕГО САЙТА, ​​ИЛИ НАШИМИ УСЛУГАМИ ДАННОГО СОГЛАШЕНИЯ, ВКЛЮЧАЯ НЕБРЕЖНОСТЬ. </p>

    <p>КРОМЕ ТОГО, МЫ НЕ НЕСЕТ ОТВЕТСТВЕННОСТИ ЗА СТОРОННИХ ПРОИЗВОДИТЕЛЕЙ ТОВАРОВ И УСЛУГ ЧЕРЕЗ НАШ САЙТ ВКЛЮЧАЯ БЕЗ ОГРАНИЧЕНИЙ ОБРАБОТКИ ЗАКАЗОВ. </p>

    <p>НЕКОТОРЫЕ ЗАКОНОДАТЕЛЬСТВА ЗАПРЕЩАЮТ ИСКЛЮЧЕНИЕ ИЛИ ОГРАНИЧЕНИЕ ПОДРАЗУМЕВАЕМЫХ ГАРАНТИЙ ИЛИ ОТВЕТСТВЕННОСТИ ЗА ОПРЕДЕЛЕННЫЕ КАТЕГОРИИ УБЫТКОВ. ПОЭТОМУ, НЕКОТОРЫЕ ИЛИ ВСЕ ОГРАНИЧЕНИЯ МОГУТ НЕ ОТНОСИТЬСЯ К ВАМ В ЭТОМ СЛУЧАЕ.</p>

    <h3>13. Возможные изменения</h3>

    <p class="p-l20">13.1. Treatstock оставляет за собой право в любое время вносить изменения в настоящие Условия. Внесенные поправки соглашения будут применяться к новым покупкам, сделанные на Сайте после даты их изменения.</p>

    <p class="p-l20">13.2. Любые поправки Соглашения вступают в силу с момента их публикации на Сайте или с момента уведомления вас иным способом. Treatstock предпримет усилия для сообщения, по крайней мере, за один день до внесения каких-либо изменений в настоящее Соглашение. Пожалуйста, регулярно проверяйте Соглашение, опубликованное на сайте, чтобы быть в курсе всех условий регулирующих использование данного Сайта и Услуг.</p>

    <p class="p-l20">13.3. Опубликованные модели после изменений в соглашении будут считаться опубликованными по новому соглашению. Если вы не согласны - своевременно удалите свои 3D модели из публикации.</p>

    <h3>14. Юридические разногласия</h3>

    <p>Настоящие Условия пользования регулируются законами штата Делавэр, США, без учета конфликта юридических положений, которые применяют закон какой-либо юрисдикции, кроме штата Делавэр, США. Любые и все споры, вытекающие из настоящих Условий, в том числе споры, касающиеся их действительности, и любые споры, связанные с использованием Услуг Treatstock или Сайта, или относящиеся к заказанным продуктам, рассмотрение возникших споров и разногласий относится к исключительной юрисдикции судов, расположенных в штате Делавэр, США. Конвенция Организации Объединенных Наций о международной купле-продаже товаров не распространяется на услуги Treatstock или сайта, или относящиеся к заказанным продуктам.</p>

    <h3>15. Контактная информация</h3>

    <p>Если у вас есть какие-либо вопросы относительно настоящих Условий или Услуг, вы можете связаться с нами, отправив по электронной почте
        <script>
            var parts = ["support", "treatstock", "com", "&#46;", "&#64;"];
            var mailAddress = parts[0] + parts[4] + parts[1] + parts[3] + parts[2];
            document.write(\'<a href="mailto:\' + mailAddress + \'">\' + mailAddress + \'</a>\');
        </script>.
    </p>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '8', '4', 'zh-CN', 'Treatstock使用协议与条款', '

    <h2>I. 公司介绍</h2>

    <p>欢迎使用Treatstock！请在访问网站前仔细阅读以下条款，客户浏览或注册Treatstock都将被默认为接受此具有法律约束力的协议。</p>
    <p>此条款协议是客户与Treatstock公司以及其他相关条目（以下简称“Treatstock”“我们”“我们的”）等共同承担的法律义务。这份条款协议规定了客户在使用Treatstock.com（以下简称“Treatstock网站”或“网站”）或任何Treatstock业务（以下简称“业务”）时的拥有的权利和应尽的责任。</p>
    <p>客户使用我们的服务即表示已同意此份条约协议，任何操作需符合此份条款协议以及所有适用的法律条款和规定。同时客户确定所有上传于Treatstock网站的3D文件（3D模型）是符合所有国家和地区，包括战乱地区的法律和规定的。Treatstock拥有随时修改网站内容、服务内容以及条约协议内容的权利。</p>

    <h2>II. 名词定义:</h2>

    <p><strong>网站:</strong> 指Treatstock公司在<a href="https://www.treatstock.com">treatstock.com</a>上当前存在或将使用的所有网站内容，API、软件的应用程序或任何的方法和工具；指Treatstock为提供服务使用的软件和源代码；用户界面、设计、图片、文字、知识库文章、程序流程；基于数据的网站信息（例如热门关键字搜索）；以及其他所有受到知识产权保护的版权、商标权、专利权、形象权及其他所有权。</p>
    <p><strong>Treatstock:</strong> 指包括Treatstock公司及其授权的子公司和合作伙伴。</p>
    <p><strong>访客:</strong> 指注册或未注册的浏览网站的用户。</p>
    <p><strong>用户:</strong> 指使用网站服务的独立或合法的实体</p>
    <p><strong>账户:</strong> 指用户在网站时注册的记录。  </p>
    <p><strong>3D模型:</strong> 指由用户提供的可打印的3D文件。</p>
    <p><strong>3D打印成品:</strong> 指3D模型由3D打印机打印成为物体。</p>
    <p><strong>设计师:</strong> 指为Treatstock提供3D模型的用户。</p>
    <p><strong>制造商:</strong> 指操作3D打印机以及通过网站提供3D打印服务的用户。</p>
    <p><strong>订单:</strong> 指由用户通过网站订购的，已选定3D模型及其3D打印制造商的，并向设计师、制造商、服务商支付过费用及运费的订单。</p>
    <p><strong>客户:</strong> 指使用网站订购3D模型或3D打印服务的人。</p>
    <p><strong>产品页:</strong> 特指在<a href="https://www.treatstock.com">Treatstock.com</a>上展示的可订购的3D模型产品网页或界面.</p>
    <p><strong>运送服务:</strong> 指由制造商将3D打印品运送给客户的第三方服务。</p>
    <p><strong>用户内容:</strong> 指3D模型、图片、视频、文字或其他任何由用户提交于网站的内容。</p>
    <p><strong>模型费:</strong> 指在该订单中支付给提供3D模型的设计师费用。</p>
    <p><strong>打印费:</strong> 指在该订单中支付给提供打印服务的制造商，包括由于3D打印产生的机器、材料使用的费用。</p>
    <p><strong>服务费:</strong> 指在该订单中支付给提供服务的TREATSTOCK公司的费用以及相关税款。</p>
    <p><strong>运费:</strong> 指在该订单中支付给将3D打印品由制造商运送给客户的Treatstock公司或第三方的费用。</p>
    <p><strong>服务:</strong> 指由TREATSTOCK公司通过网站提供服务，即本条约协议中第三条第四款中“服务”的内容。</p>
    <p><strong>知识产权:</strong> 指版权权、专利权、商标权、商业机密权、形象权及其他所有权</p>

    <h2>III. Treatstock服务 </h2>
    <p>Treatstock网站涵盖了3D打印所需的不同环节，是一个用户可以搜索和订购到能够在3D打印机上制造的物品的市场。</p>
    <p>用户可以在设计师上传的众多3D模型中选择自己所需并打印成品。同时，用户可以选择打印服务，即3D模型打印成品及运送的服务过程。另外，用户可以向设计师沟通定制3D物品。</p>
    <p>同时Treatstock向在制造3D模型过程中遇到问题的设计师和打印服务商提供支持，针对Treatstock会员有专门用于答疑和经验分享的会员支持系统和论坛。打印服务商可以获得适用于制造过程的教学材料。</p>

    <h2> IV. 服务条款</h2>
    <p>Treatstock服务于在法定年龄能够履行法定协议的成人。当用户年龄小于法定年龄时，需要在家长或监护人知情的情况下使用Treatstock.com上的服务。</p>
    <p>为了获得和使用服务，用户需在注册时负责任地提供、维护并及时更新其全名、现住址、电话号码、有效的电子邮箱地址。公司有权拒绝任何个人或实体使用网站服务。</p>
    <p>用户有责任保护好自己的密码。Treatstock不承担任何因用户疏于保护账户及密码而带来的损失及破坏。</p>
    <p>用户在注册时使用的邮箱默认为授权于Treatstock使用于联系事宜。</p>
    <p>用户可以通过发送网内信至支持服务部门或发送邮件注销Treatstock账号。</p>
    <p>用户不能将服务使用于违法行为或违反法律法规的未授权行为中。</p>
    <p>用户若有任何未能遵守或违反条约协议的行为，Treatstock有权立即中止服务。</p>
    <p>Treatstock保留在不预先通知的情况下，在任何时间向任何人拒绝或中止服务的权利。</p>
    <p>Treatstock是服务于制造商向用户打印并运送3D模型的过程，以及用户、模型作者、制造商和运送服务商之间的支付交易的网络平台。</p>
    <p>为使用户顺利下单得到3D打印成品，网站授权设计师上传3D模型和其他内容并制定模型费，授权制造商列出3D打印服务条目以及制定打印费。用户将在网站接单后选择运送方式，允许自取。在3D打印成品被运送之前，制造商将代表用户持有3D打印成品。</p>
    <p>Treatstock不会介入制造商、作者与用户的协议中，也不是任何打印或交易服务的制造者和提供者。</p>
    <p>因此Treatstock对制造商、作者、用户以及用户上传的内容、制造商提供的服务免责。</p>
    <p>Treatstock保护购买交易，基于第十二条中的“交易保护及退换政策”，保留单方面中止订单，全部或部分退换用户支付金额的权利。</p>

    <h2>V. 关于用户</h2>
    <p>Treatstock网站服务于能够承担法律责任以及在网站上注册账号的18岁及18岁以上的用户。</p>
    <p>网站用户或任何访客向网站递交的内容是必须是正确、真实、不侵犯第三方权利的。用户和访客不允许：</p>
    <ul>
        <li>发送垃圾邮件</li>
        <li>使用“spiders”, “robots”, “load testers”等外挂程序，制造过多访问和创建过多账号（以下情况除外：公司授权公众搜索引擎使用Spiders进行可撤销的网站和服务复制，建立非缓存和非档案的可查询的材料目录。）。</li>
        <li>在使用服务的过程中以非正常目的收集或存储其他用户的个人信息。</li>
        <li>恐吓或攻击其他用户或实体。</li>
        <li>非法获得或试图获得任何Treatstock、其他用户或第三方的程序、服务、网络或硬件。</li>
        <li>任何可能过载、破坏、冻结、中断或威胁服务或者干涉其他用户的操作；上传、传播或散布任何可能损坏或更改计算机程序和数据的病毒、蠕虫或软件。</li>
        <li>改编、修改或违反系统设计或与Treatstock的协议。</li>
        <li>改变、修改或违法其他用户上传的3D模型或其他格式、形式的派生物。</li>
        <li>在未经Treatstock书面同意的情况下重置格式、重新分配或转售其服务。</li>
    </ul>

    <p>用户需注册账户以获得服务。在建立账户时用户需提供最近的、正确的身份证明、联系方式以及其他信息并及时更新以继续使用服务。用户不允许：</p>
    <ul>
        <li>出于影射他人目的选择或使用他人姓名作为用户名；</li>
        <li>未经授权使用具有所有权的姓名作为用户名；</li>
        <li>使用带有攻击性、低俗和淫秽的姓名作为用户名。</li>
    </ul>
    <p>用户有责任保护自己的账户及密码不被泄漏，并对自己账户的操作负全责。用户有责任在权限范围内服务内容发生变化、不安全操作或非授权行为发生时立刻告知公司。用户不得发表、散布自己的账户登录信息。由用户疏于保护自己的账户和密码造成的损失和破坏，Treatstock公司概不负责。</p>
    <p>用户有权直接或者向工作人员申请注销账户。</p>
    <p>对于违反以上条约协议的用户，Treatstock有权在不进行赔偿的情况下拒绝用户使用或继续使用网站服务或在不通知的情况下更改适用标准。</p>
    <p>用户任何破坏或试图破坏网站、服务程序的行为将被诉诸于法律，Treatstock保留最大程度上要求用户依法赔偿的权利。</p>
     
    <h2>VI. 用户原创内容</h2>
    <p>此条约协议中的“用户原创内容”条款表示用户上传于网站的所有内容，包括但不仅限于所有上传于网站的3D设计、3D模型、3D文件、物品图片、模型图片、网站评论及注解。所有用户原创内容不得含有或包括辱骂、攻击、威胁、淫秽、欺骗、欺诈、误导、色情、违法性质的信息，不得侵犯或试图侵犯任何知识产权。知识产权指版权、专利权、专利设计、设计权、商标、商业机密以及其他任何产业所有权利。Treatstock有权（而没有义务）浏览任何用户内容，或针对用户违反条约协议的行为进行调查或采取行动。违反条约协议的行为将导致用户内容被清除或修改，关闭账户或诉诸法律。</p>
    <p>Treatstock没有义务浏览所有用户原创内容，不为用户原创内容负责。用户原创内容表达的观点不代表Treatstock立场。</p>
    <p>用户通过网站或服务提交的用户原创内容，即视为授权于Treatstock网站、服务、产业（以及我们的继承者和债权人的产业）全球性、非独有、非专利、完全支付、可转让、可次级授权的权利，同意使用、复制、发布、制作衍生产品、展示、演示、开发其原创内容。包含且不限于在任何媒体形式和渠道（包含但不限于第三方网站）推广和再分配全部或部分所有网站及服务内容及其衍生品。</p>
    <p>另外提交用户原创内容的行为即表示用户放弃独有权，默认网站或服务用户可以使用、编辑、修改、复制、发布、衍生、展示、演示。用户在网站上发布的3D模型，Treatstock授权他人有限使用受版权保护的材料。详见附录A。</p>
    <p>在此声明，前述的授权许可于网站及网站用户的行为不会影响用户对原创内容的所有权和许可权，包含添加其他权限至自己提交的用户原创内容的权利，除非另有书面协议。用户代表并担保自己的授权不侵犯危害任何第三方权利，包含且不限于任何隐私权、形象权、版权、合同权或其他知识产权和所有权。</p>
    <p>提交用户原创内容的行为即授权网站、应用或服务共享内容以及分享至博客和社会媒体的权利。</p>
    <p>Treatstock无义务备份任何用户内容，用户内容有随时被删除的可能。用户全权负责内容备份事宜。</p>
    <p>用户同意不对网站或服务上的任何用户内容进行收集、上传、传播及展示：</p>
    <ul>
        <li>不损害第三方权利，包括任何版权、商标、专利权、商业机密、道德权、设计权、隐私权、形象权或任何其他知识产权和所有权。</li>
        <li>不对任何团体或个体进行违法、骚扰、辱骂、歪曲、恐吓、损害、侵害隐私的行为，不得使用粗俗、诽谤、伪造、故意误导、损害商业名誉、色情、淫秽、公然攻击、种族歧视、偏见、仇恨或身体伤害他人，不得进行违法活动或参与制造武器、非法材料。</li>
        <li>不以任何方式伤害未成年人。</li>
        <li>不违反任何法律法规或第三方施加的义务及限制条件</li>
    </ul>
    <p>自动生成的内容。Treatstock保留收集网站用户数据以改进用户体验的权利。一般来说这包含了不同颜色和不同角度渲染的模型图片，以及为不同3D打印机估算模型重量和尺寸的行为。</p>

    <h2>VII. 标价 </h2>
    <p>网站上的产品标价包括打印费、模型费、运费、服务费以及可能存在的税费，并且由访客选择货币展示。</p>
    <p>所有标价对应相应的货币，如果用户没有选择由默认货币支付，公司付款服务商将负责货币兑换。</p>
    <p>由于标价是由作者、制造商、运送服务和Treatstock共同制定的，Treatstock全权保留对访客选择的标价货币使用汇率或交叉汇率的权利。</p>
    <p>网站报价包括邮费、销售税或可能产生的增值税，但不包括可能产生的进口关税和其他税收。然而依据用户订单对应的不同目的国，进口产品可能被征收关税、其他税和经济佣金。网站上标明的邮费仅指运输用户订单的费用，而不包含这些额外费用。对是否会产生关税和其他税的问题，用户需自行联络目的国海关。进口税由进口产品的种类决定，并根据发运单据的内部统计码进行收费。一般来说进口关税和其他税会收取订单总额的5%-20%。一些情况下承运人会收取额外的经济佣金。</p>
    <p>Treatstock保留在任何时间更改产品和服务标价、个性定制订单价格的权利</p>

    <h2>VIII. 产品选择</h2>
    <p>用户一旦订购产品，即确认该商品将不被任何许可限制、资质证书、或其他任何条例或规则阻止其生产和使用。用户将对任何许可限制、资质证书或其他条例或规则阻值产品生产和使用的行为全权负责。Treatstock对其免责。</p>
    <p>用户在选择产品时承担所有风险。Treatstock、作者及制造商不对客户的选择、产品实际情况与期待值的符合程度、产品在任何意义上的适用性或在任何地区的使用情况负责。</p>
    <p>为改善用户体验，公司尝试使用不同颜色展示3D模型，然而从特定角度展示的3D模型图像可能是同一颜色。</p>
    <p>为保护版权，公司不允许360度旋转展示模型。</p>

    <h2>IX. 订单</h2>
    <p>网站获取访客IP地址以确认访客所在地理位置，以此推荐附近可以提供3D打印的制造商。访客可以通过更改所在城市名称切换所在位置。</p>
    <p>浏览网页时，访客可以选择3D模型或工具包（待组装的一系列独立零件）。用户可以选择通过不同材料和颜色展示3D模型图像。之后网站会尝试找到提供此材料和颜色3D打印服务的制造商。</p>
    <p>网站展示包括模型费在内的，用于打印已选择的3D模型以及一系列打印服务的花费。访客可以选择需要的打印服务并结算。</p>
    <p>结算过程中，访客将选择运送方式。最后网站将展示包括了所有费用和可能产生的税款的总价。</p>
    <p>默认访客同意条约协议并自主选择结算订单的方式。</p>
    <p>Treatstock将订单递送给访客选择的制造商，确认材料、颜色以及估算的打印成本。</p>
    <p>Treatstock保留有理由或无理由驳回订单，全部或部分退还已付款项，赔偿或不赔偿作者或制造商的全部权利。</p>

    <h2>X. 支付及结算 </h2>
    <p>Treatstock接受包括VISA、MasterCard、American Express、Discover、JCB、Diners Club、 PayPal在内的大部分信用卡和借记卡结算。</p>
    <p>用户授权通过信用卡或借记卡付款后，款项最多预留8天。在制造商接受订单之后，款项将转移至公司账户，之后订单才能开始被处理。此支付过程由付款方案合作伙伴Braintree 和Stripe负责。</p>
    <p>用户若使用支付宝支付，在支付成功后款项即转移至公司账户。</p>
    <p>如果制造商不能完成订单，公司将退还已付款项至银行卡或账户。</p>
    <p>如果我们认为3D打印成品质量不达标，我们将以购买保护政策为参考，全部或部分退换已付款项至银行卡或账户。</p>

    <h2>XI. 运送</h2>
    <p>可能的情况下，制造商可以选择送货上门。</p>
    <p>制造商可以选择独立的运送服务。因此制造商需设定邮费，Treatstock会将其算入客户结算的金额中。</p>
    <p>制造商可以选择由Treatstock整合，与物流公司例如UPS、USPS合作。</p>
    <p>在此情况下，Treatstock将全权设置邮费并将其算入客户结算金额中。这种方式下的邮费可能和其他普通邮费有所不同，因为客户订购的3D打印成品参数未知且各不相同，Treatstock将承担相应风险。</p>
    <p>因此Treatstock接受运费，在3D模型打印完成后以最合适的方式运送，并向制造商提供打印标签的技术。</p>
    <p>可能的情况下，制造商需在运送前包装好3D打印成品。</p>

    <h2>XII. 交易保护及退换政策</h2>
    <p>在订单开始3D打印之前，Treatstock允许客户取消订单并全款退还。一旦订单开始3D打印，客户将不能取消订单。</p>
    <p>若是定制产品，公司只有在3D打印故障或出错的情况下或在超出合理时间未运送3D打印成品的情况下进行退款。</p>
    <p>Treatstock有权延迟支付作者和制造商酬金直至客户确认满意3D打印成品。</p>
    <p>若在购买的30天内，客户未向Treatstock投诉，公司默认客户为满意3D打印成品。公司不接受在交货30天后的索赔。</p>
    <p>在收到3D打印成品和使用之前，客户需确认满意其质量符合设置。若客户认为产品质量不达标，需立即告知公司，并提交3D打印成品及原包装照片。若3D打印成品已被使用，Treatstock将不退还任何已付款项。若有需要，用户需做好将原包装与3D打印成品一同寄回公司的准备。客户将对退回的3D打印成品负责，并自行支付邮费。</p>
    <p>在收到客户投诉后，Treatstock全权判断是否中止订单并退还已付款项。Treatstock可能要求制造商重新制作并运送3D打印成品或使用其他解决方法。</p>
    <p>对于违反此条约的任何一方，Treatstock保留追究赔偿的权利。</p>

    <h2>XIII. 质量政策</h2>
    <p>在下列情况下3D模型将不被认定为有缺陷：</p>
    <ul>
        <li>在图层上有裸眼能见的线条。</li>
        <li>由于在打印过程中使用支撑结构，成品有表面处理过的痕迹。</li>
        <li>在成品表面有细丝存在。</li>
        <li>透过光线可以看见成品内部结构。</li>
        <li>有打印机在图层间移动留下的线痕。</li>
        <li>在成品底部表面有明显可见的线。</li>
    </ul>
    <p>在下列情况下3D打印成品将被认定为有缺陷：</p>
    <ul>
        <li>顶部表面合拢不充分或不平整</li>
        <li>不规则圆形：畸形圆，线错误相交。</li>
        <li>3D打印成品角落抬升，并脱离主体。</li>
        <li>部分3D打印成品随X轴或Y轴移动。</li>
        <li>3D打印成品倾斜。</li>
        <li>遭受挤压。</li>
        <li>部分或整个壁体没有熔合和接触</li>
        <li>在达到适当规模之前，成品底部出现萎缩。</li>
        <li>第一图层不粘着或部分松弛。</li>
        <li>串接：多余的塑料线环绕成品。</li>
        <li>成品出现波浪或阴影。</li>
        <li>俯角畸形：俯角的下表面突出、不美观。</li>
        <li>第一图层上线条过度明显或等距离间隔。</li>
        <li>在成品表面出现很细的塑料线。</li>
    </ul>
    <p>3D打印成品的后期加工规则：</p>
    <ul>
        <li>尽管3D打印步骤是以使用FDM科技的细丝制造的，最终产品的表面不可能完全平滑。以下为改进技术的方式和建议：</li>
        <li>使用最高分辨率的打印机（例如Ultimaker 2的分辨率高达20微米，即0.02毫米），其成品将有最平滑的外表。然而此类设施有其他打印缺陷，且打印耗时较长。</li>
        <li>后期加工。成品表面可由砂纸或针锉谨慎抛光，以达到光滑的目的。</li>
        <li>使用不同溶剂。成品表面在使用溶剂后能够变得光滑。例如用丙酮处理ABS塑料，或用二氯甲烷处理PLA塑料（相对有毒性）。成品可以浸泡在溶剂里或浸浴溶剂蒸汽。</li>
        <li>然而请谨记，如果成品浸泡与溶剂中超过所需时间，溶剂会损坏成品。</li>
        <li>另外请在使用溶剂时格外谨慎，保证所有步骤在通风的房间里操作，并使用保护眼睛、呼吸道和皮肤的防护措施。</li>
    </ul>
    <p>持有可打印的3D模型的模型设计师规则</p>

    <ol>
        <li>小零件
            <br>
            当打印小零件时，请做以下考虑：

            <ul>
                <li>最小的零件的尺寸不得小于打印机喷嘴的直径。否则此物件将不能被打印。</li>
                <li>3D打印成品的壁体厚度（包括上壁及底壁）必须超过单个图层的2至3倍（理想条件下为4-6倍）。若壁体太薄，壁体的强度会显著减弱，并且造成图层间出现可见的缺口。</li>
                <li>在进行后期处理时，需要注意过小的零件经过处理后也许会更加不可见甚至整体小时，需要极其小心操作。</li>
                <li>一些切片机（如Cura）很难为小物件设计支撑结构。这个问题可以通过增加支撑结构密度或使用多个切片机形成共同支撑结构来解决。</li>
                <li>在模型缩放的过程中，一些零件细节会有丢失。一些模型零件在模型尺寸较大时能够被很好地制造出来，而不能够在模型尺寸小时打印成型，因此需要及时删减和改进。在准备打印经过缩放的模型时需要极其小心。</li>
            </ul>
        </li>

        <li>支撑结构
            <ul>
                <li>对于用来悬挂的零件，支撑结构时必需的。</li>
                <li>没有支撑结构，打印的角度最多在60-70度，可能会降低打印质量。</li>
                <li>可以通过改变放置模型的角度减少支撑结构的数量。例如，将物体平放打印会大大快于垂直放置打印。</li>
                <li>一些情况下将模型分成多个部分打印可以避免打印支撑结构。</li>
                <li>请谨记支撑结构会改变3D模型的外表，因此最好将模型设计在可以隐藏支撑结构或可以轻易控制和移除的角度</li>
                <li>减少支撑结构的数量有利于打印过程。这将降低损耗、减少打印时间及成本。</li>
                <li>打印位置与打印玻璃位置相关。</li>
                <li>在决定支撑结构打印位置时，请做以下考虑
                    <ul>
                        <li>改变角度可以增多或减少3D模型所需的支撑结构数量。例如通过水平打印垂直的3d模型可以减少悬挂式支撑结构的数量。</li>
                        <li>3d模型的强度将因放置支撑结构而增加，因为承受最大重量的部分是垂直而非平行于打印图层。</li>
                        <li>通过水平打印可以增强具有圆形元素部分的质量。一些情况下垂直封闭的圆形会形成畸形（若为圆环则形成椭圆）。</li>
                    </ul>
                </li>
            </ul>
        </li>

        <li>3D模型的尺寸
            <br>
            在决定3D模型的尺寸时请做以下考虑：
                <ul>
                    <li>模型能够达到的最大尺寸取决于打印机的体积（例如，Ultimaker 2的体积为230x225x205 mm）</li>
                    <li>如果将模型分成多个部分打印（每一个都以最大体积），并将它们连接或粘合在一起可以制造出更大的模型。</li>
                    <li>打印机不能打印比喷嘴直径更小的零件。也就是说最小的零件也应该大于喷嘴直径。为保证强度和结构的稳定度，壁体的最适宜厚度应该是喷嘴厚度的2-3倍。</li>
                </ul>
        </li>
    </ol>

    <p>包装标准：</p>
    <ul>
        <li>使用坚硬且完整的纸箱包装。</li>
        <li>若为重物使用双层纸箱。</li>
        <li>将零件分别用大量缓冲材料包好，避开箱子的边、角、顶、底部分，放在靠近中部的位置。</li>
        <li>将可能容易被腐蚀或在标记和封箱过程中损坏的零件多加一层保护盒。</li>
        <li>使用填充料填充箱子与物品间的空隙，以防在运输过程中发生移动。</li>
        <li>封箱时使用H形密封法以防箱子意外打开。</li>
        <li>在箱内防治揉皱的报纸、疏松的填充颗粒或其他缓冲材料防止产品移动。</li>
        <li>单独运送易碎物品，至少使用3厘米厚的泡沫填充材料。</li>
        <li>其他任何空隙使用更多填充材料。</li>
        <li>独立包装所有零件。</li>
        <li>使用运输专用的强力胶带。</li>
        <li>地址标签需具有清晰、完整的投递及退还信息。</li>
    </ul>

    <p>如果你对可能存在的打印缺陷有任何问题，请拍摄清楚的图片并点击这里联系我们。<p>

    <h2>XIV. 第三方服务</h2>
    <p>网站包含通向其他网页和服务的链接，或包含其他网页或服务器俄网络帧内编码。公司不对这些网页和服务器的内容和有效性负责。每个服务器尤其自己的条约协议，请仔细阅读。</p>
    <p>一些情况下，为尽好网站职能，公司会将一些数据，可能包括个人信息或个人验证信息发送给其他网页或服务器。公司只会发送必要的信息并要求服务方保证个人信息安全。使用网页服务即表示用户同意此行为。</p>
    <p>包含且不限于随时改变。</p>

    <h2>XV. 版权保护</h2>
    <p>如果客户认为网站上得任何材料侵犯了任何版权或知识产权法规或法律，请发送含有以下信息的版权受到侵犯的声明至公司：</p>
    <p>被侵犯权利的作品或材料的证明文件；</p>
    <p>声称被侵犯权利的材料的证明文件，包含具体细节以便Treatstock搜索以及验证其存在。</p>
    <p>投诉方联系方式，包含姓名、住址、电话号码和电子邮箱地址。</p>
    <p>一份关于投诉方真实认为材料在未经知识产权所有人、代理商或法律允许的情况下被使用的声明。</p>
    <p>一份代表版权所有者投诉，并担保所有信息都是真实正确的声明。</p>
    <p>一份本人或电子签字的授权书。</p>
    <p>如果在10天内客户没有收到对投诉的反馈，请尝试再次联系公司确认已收到上次投诉。众所周知，垃圾邮件防火墙有时候会拒绝来自未知方的重要邮件。</p>
    <p>请注意任何人故意或实际上歪曲事实将被诉诸法律。</p>
    <p>首先直接移除或限制用户上传内容的访问，Treatstock将就侵权事宜，通过曝光网站等方式发布公共声明。</p>
    <p>在移除或限制对材料或内容的访问之后，Treatstock将立即通告用户该材料因非法侵权而被移除或限制访问。</p>
    <p>Treatstock保留立刻中止涉嫌侵犯版权的账号的完全权利。</p>
    <p>如果客户认为行为不造成侵权，可以将下列信息发送至Treatstock：</p>
    <p>该已被移除的或被Treatstock限制访问的材料的URL。</p>
    <p>客户姓名、地址、电话号码、电子邮箱地址。</p>
    <p>一份关于赞成赔偿Treatstock对版权侵犯的</p>
    <p>以下声明： 我保证，若做伪证将依法被处罚。我相信材料被移除或失效是因为错误和混淆。</p>
    <p>客户署名</p>
    <p>在收到有效的反向通知后，Treatstock将向提交侵权投诉的通知方转寄确认函。投诉将在十日内被受理。如果Treatstock在10天内没有收到任何通知，材料将恢复到服务器中。</p>

    <h2>XVI. 论坛政策</h2>
    <p>Treatstock将开设全球性的3D设计、3D打印论坛网页，分享最新的和发展中的科技，以此激励参与者将自己的创造带进生活，启发他人，分享知识，讨论想法以及激发参与者在3D设计和打印产生新的兴趣。Treatstock论坛将给所有会员带来全方位体验。</p>
    <p>论坛政策是指论坛会员在论坛、评论、邮寄名单、网站、私人通信或公众信息的行为。为避免出现沟通问题，以下是需要遵守的基本原则：</p>
    <ul>
        <li>请体谅他人。论坛网页不能用于骚扰其他论坛会员，侵犯性言论将不得发表于论坛网页。</li>
        <li>请耐心对人。危险的、有害的、包含任何暴力威胁的、上升为暴力行为的或违法行为不得发表于论坛网页。</li>
        <li>请尊重他人。尊重其他论坛会员。每个人都有自己的观点，不能因为观念相左而诉诸暴力。论坛应是一个让会员轻松高效获得信息的地方。</li>
        <li>请友善待人。尊重他人隐私，不得在网页公共区域分享任何个人隐私与信息。</li>
    </ul>

    <h2>XVII. 赔偿金 </h2>
    <p>用户同意赔偿任何因客户本人违反条约协议引起或牵连的Treatstock及其子公司、经销商、供货商、承包商以及雇员带来的债务、损失、诉讼、破坏和额外开支。</p>

    <h2>XVIII. 责任范围 </h2>
    <p>客户清楚知晓且同意Treatstock不对任何直接的、间接的、偶然的、特别的、随机的、处罚性的或可效仿的损失负责，不赔偿包括但不限于收入、红利损失，合约、数据或其他无形损失，或其他任何由形式造成在使用服务或服务中断时造成的损失。</p>
    <p>任何情况下，Treatstock或其董事、雇员、合作伙伴、子公司或供应商均不为客户或任何第三方的收益损失负责；不为任何特殊的、偶然的或随机的损失负责；不为无论什么原因产生损失，不论公司是否提醒过该种损失的可能性，均不负责；由网站、服务器或此条约引起或与网站相关的任何责任理论，疏漏之处在所难免。</p>
     
    <p>另外，我们将不对第三方在网站上提供的商品和服务负责，包括且不限于订单处理过程。</p>
    <p>一些区域禁止排斥随机的、偶然的损失于责任范围之外，以上责任范围不适用于所有用户，</p>

    <h2>XIX. 适用的法律</h2>
    <p>此条约协议依照美国特拉华州法律，不考虑与其法律原则冲突的其他区域。因此条约协议引发的任何及一切争议，包括对其有效性及其他有关使用Treatstock服务器或网站、产品订单及运送的争议，均将诉诸于联邦法院或位于美国特拉华州的州法院。联合国国际货物销售合同公约不适用于Treatstock服务器或网站及其产品订单和运送。</p>

    <h2>XX. 其他</h2>
    <p>不可抗力。公司不对任何因超出合理控制范围，未能履行公司义务而造成的损失负责，包括但不限于机械、电子、通信中断或退化。</p>
    <p>分配。此服务条约只针对用户个人，用户在无公司书面同意的情况下不得分配、转让、再授权于他</p>
    <p>人。公司有权可以分配、转让、委派任何公司权利和义务。</p>
    <p>通知函。若无其他特殊声明，所有有关此条约协议的通知函，若客户通过挂号信件形式发送回执，公司将回复纸质通知并视为已按时发送；若通过传真或电子邮件，即由系统自动确认；若通过隔夜快递业务的即日达业务，即在接收邮件第二天发出。电子通知将以表格形式发送。</p>
    <p>非弃权。若公司因任何原因未履行任何跳跃协议中的条款，不代表公司放弃继续旅行该条款或其他条款的权利。在个案中顺从性弃权不代表在未来所有情况下弃权。为保证任何与此条约协议有关的遵从性放弃具有约束力，公司将提供由公司授权代理人撰写的弃权通知。</p>
    <p>代理。此条约协议不产生代理、合作伙伴、合资企业、雇佣关系。</p>
    <p>标题。此条约协议里的章节和段落标题是为方便阅读，不影响其原本意义。</p>
    <p>反馈。出于进一步改善服务内容的期望，公司时刻欢迎所有评论、建议、意见和反馈。以此方式客户授予公司全球性、非专利、不可撤销、永久的许可，将服务和反馈结合起来。</p>
    <p>使用Cookies。客户同意在浏览网站时，cookies可能安装在电脑硬盘上的行为。不支持与维护。客户知晓并同意公司没有义务对客户连接至网页的行为行为进行支持和维护。</p>

    <h2>XXI. 版权/商标信息</h2>
    <p>Copyright © 2015, 2016 Treatstock, Inc.所有权利归Treatstock公司所有。所有出现在网站上的商标、标识和服务商标为公司财产或其他第三方财产。用户不得在取得公司或第三方书面许可前使用这些标识。</p>

    <h2>XXII. 联系信息</h2>
    <p>如果您对此条约协议或服务有疑问，请联系我们。</p>

    <br>

    <h2>附录A</h2>

    <h2 class="m-t0">商业执照</h2>

    <h3>1. 定义</h3>

    <p><strong>网站</strong>: 指Treatstock公司在Treatstock.com上当前存在或将使用的所有网站内容，API、软件的应用程序或任何的方法和工具；指Treatstock为提供服务使用的软件和源代码；用户界面、设计、图片、文字、知识库文章、程序流程；基于数据的网站信息（例如热门关键字搜索）；以及其他所有受到知识产权保护的版权、商标权、专利权、形象权及其他所有权。</p>

    <p><strong>Treatstock</strong>: 指包括Treatstock公司及其授权的子公司和合作伙伴。</p>

    <p><strong>访客</strong>: 指注册或未注册的浏览网站的用户。</p>

    <p><strong>用户</strong>: 指使用网站服务的独立或合法的实体</p>

    <p><strong>账户</strong>: 指用户在网站时注册的记录。</p>

    <p><strong>3D模型</strong>: 指由用户提供的可打印的3D文件。</p>

    <p><strong>3D打印成品</strong>: 指3D模型由3D打印机打印成为物体。</p>

    <p><strong>KIT</strong>: 待组装的一系列独立零件</p>

    <p><strong>设计师</strong>: 指为Treatstock提供3D模型的用户。</p>

    <p><strong>制造商</strong>: 指操作3D打印机以及通过网站提供3D打印服务的用户。</p>

    <p><strong>订单</strong>: 指由用户通过网站订购的，已选定3D模型及其3D打印制造商的，并向设计师、制造商、服务商支付过费用及运费的订单。</p>

    <p><strong>客户</strong>: 指使用网站订购3D模型或3D打印服务的人。</p>

    <p><strong>产品页</strong>: 特指在Treatstock.com上展示的可订购的3D模型产品网页或界面</p>

    <p><strong>运送服务</strong>: 指由制造商将3D打印品运送给客户的第三方服务。</p>

    <p><strong>用户内容</strong>: 指3D模型、图片、视频、文字或其他任何由用户提交于网站的内容。</p>

    <p><strong>模型费</strong>: 指在该订单中支付给提供3D模型的设计师费用。</p>

    <p><strong>打印费</strong>: 指在该订单中支付给提供打印服务的制造商，包括由于3D打印产生的机器、材料使用的费用。</p>

    <p><strong>服务费</strong>: 指在该订单中支付给提供服务的TREATSTOCK公司的费用以及相关税款。</p>

    <p><strong>运费</strong>: 指在该订单中支付给将3D打印品由制造商运送给客户的Treatstock公司或第三方的费用。</p>

    <p><strong>服务</strong>: ：指由TREATSTOCK公司通过网站提供服务，即本条约协议中第三条第四款中“服务”的内容。</p>

    <p><strong>知识产权</strong>: 指版权权、专利权、商标权、商业机密权、形象权及其他所有权。</p>

    <h3>2. 合约方</h3>

    <p class="p-l20">2.1. 此授权合约由在网站上构成任何3D模型产品交易的作者、客户和制造商共同签署。</p>

    <p class="p-l20">2.2. 在此授权合约下，客户成功购买网站发布的打印3D模型服务（或3D模型零件），均视为同意此协议。</p>

    <h3>3. 权利授予</h3>

    <p class="p-l20">3.1. 作者代表并有权授予客户在网站上使用的任何3D模型或零件不可取消的、非独家的、可在全球范围内复制的、可制作派生品的、可制成物体的、可合并使用于其他作品或其他用途的权利。</p>

    <p class="p-l20">3.2. 作者授权在订单过程中被客户选中的制造商仅在打印和加工订单时享有不可取消的、非独家的、可在全球范围内复制的、可制作派生品的（包括但不限于制作切片）、可复制3D模型或零件的权利。</p>

    <p class="p-l20">3.3. 3D模型因打印成品的可能性而改变。</p>

    <p class="p-l20">3.4. 制造商必须打印订单中显示的3D模型数量。</p>

    <p class="p-l20">3.5. 制造商必须销毁任何模型中有缺陷或打印失败的部分。</p>

    <p class="p-l20">3.6. 制造商必须在订单完成或取消的一天内销毁所有属于作者的3D模型数码拷贝，包括在订单中和所有衍生品和追踪档案。</p>

    <p class="p-l20">3.7. 作者授权打印3D模型的制造商拍摄3D打印成品照片、视频并上传于网络的权利，以便保证质量、改进用户体验或做广告宣传。</p>

    <p class="p-l20">3.8. 作者授权客户以任何方式、凭自我意志使用3D打印成品包括但不限于使用于广告、销售、出租、图片或视频合集等），然而禁止客户使用订单复制原3D模型（例如3D扫描或3D图片）。</p>

    <h3>4. 版税及费用</h3>

    <p class="p-l20">4.1. 在依据此条约协议内容发布3D模型之后，作者给每一个3D模型单品或零件标价。</p>

    <p class="p-l20">4.2. 为能够享受本条约协议“权利授予”条款中描述的权利，客户必须在Treatstock网站上按照作者标价支付所有订购的3D模型或零件费用。</p>

    <p class="p-l20">4.3. 以上支付过程在网站支付订单页进行。作者不得通过其他方式接受Treatstock客户对Treatstock订单的付款。</p>

    <p class="p-l20">4.4. 若作者标价使用的货币与订单默认货币不一致，Treatstock具有对汇率的独家决定权，并以相应货币价格告知客户。</p>

    <p class="p-l20">4.5. 打印残次品不可支付。</p>

    <h3>5. 解约</h3>

    <p class="p-l20">5.1. 作者可以通过移除发布在网站的3D模型以示解约。</p>

    <p class="p-l20">5.2. 解约后将不会产生新的3D模型订单。已成功的订单在解约后权利仍旧保留权利。例如，若订单在模型下架之前已生效，制造商必须完成打印和加工该模型。</p>

    <p class="p-l20">5.3. 若客户不遵守此条约协议，此条约授予客户的权利将在不通知的情况下即刻、自动中止。根据中止通知，客户必须销毁所有3D模型拷贝。</p>

    <h3>6. 退款</h3>

    <p class="p-l20">6.1. 订单协议已达成，作者不得单方面退款，作者不得撤回打印订单的权利。</p>

    <p class="p-l20">6.2. 如果制造商表示因任何理由无法打印订单，或证明打印出残次品，Treatstock保留取消订单的权利。在此情况下作者的版权费将不被支付。</p>

    <p class="p-l20">6.3. 客户允许退款。</p>

    <h3>7. 税收</h3>

    <p class="p-l20">7.1. 作者知晓并同意其在报税在网站提供服务有独家决定权。</p>

    <p class="p-l20">7.2. Treatstock保留为非美国用户或实体代扣美国税款的义务。作者在填写税单时在个人账户的特定区域可以看见扣缴比例。</p>
    <p class="p-l20">7.3. 请谨记如果作者不在规定时间内提供正确的税务信息，Treatstock将从佣金中扣除30%。另外为了能够把Treatstock的佣金提现，请使用Paypal账户。</p>
    <p class="p-l20">7.4. d. 若作者是非美国国籍且不用填写W-8BEN税单，公司根据美国税法将必须收取其28%的所得税并上交美国国税局。这也适用于没有上报其纳税人识别号的作者。即为“预扣税”。</p>
    <div class="p-l20">
        <p>7.5. e. Treatstock作为第三方结算机构，有责任上报其网站的总交易金额。详细信息如下。</p>
        <p>如果客户不是美国纳税人，需上缴因与美国交易所得的30%作为预扣税，并需要填写<a href="https://www.irs.gov/uac/Form-W-8BEN,-Certificate-of-Foreign-Status-of-Beneficial-Owner-for-United-States-Tax-Withholding" target="_blank">W-8BEN</a>税单，除非拥有或直至完成关于豁免版税的相关文件。美国作家将需填写<a href="https://www.irs.gov/uac/About-Form-W9" target="_blank">W-9</a>税单。</p>
        <ul>
            <li>点击这里查询哪些国家与美国有税务条约</li>
            <li>点击这里查询查询各国税率。详见第二和第四表格最右栏‘Copyrights’之下的版税预扣税(‘Income category 12’.)</li>
        </ul>
        <p>请谨记，如果不能及时提交所需文件，30%的税额将从工作所得中扣除。</p>
    </div>


    <h3>8. 通知</h3>

    <p class="p-l20">8.1. 这不是拥有权协议。</p>

    <p class="p-l20">8.2. 此许可不得转让。</p>

    <p class="p-l20">8.3. 客户不得在未取得作者书面同意的情况下，租借、转售、分发、借用或转让作者的3D数码模型、拷贝或零件。客户的授权范围在条约里已详细阐明，3D模型不得使用于其他用途。</p>

    <p class="p-l20">8.4. 作者可将3D模型价格定位0$，但仍持有版权。</p>

    <p class="p-l20">8.5. 若客户不同意条款，则不允许使用3D模型。</p>

    <p class="p-l20">8.6. 条约规定购买3D打印模型的客户不得转售3D模型零件。</p>

    <h3>9. 财产及商标</h3>

    <p>访客清楚并同意网站中展示的3D模型及其合法拷贝，和网站关联商标都是其各自所有者的知识产权。网站上得3D模型或任何用户原创内容都受到版权法保护，包括但不贤惠美国联邦版权法、各国知识产权法、国际条约规定以及司法使用的适用法律。</p>

    <h3>10. 保证</h3>

    <p>Treatstock保证所有订购的3D打印模型将使用最广泛的3D打印技术进行打印。Treatstock无法保证所有3D模型将被使用于特定用途。在任何实际应用上使用者承担所有风险。</p>

    <h3>11. 免责声明</h3>

    <p>在前文提及的网站和服务不明示或默示任何担保。Treatstock不能保证访客浏览网页的体验与结果。Treatstock不保证，不明示或默示包括但不限于默认担保网站内容适用于特定或合理用途。一些国家或司法不允许限制或暗示担保，因此以上限制不适用于所有用户</p>

    <p>除前文提到的有限保证及被客户所在区域法律限制或排除的保证之外，Treatstock将不做任何保证、条件、陈述或条款，不明示或默示对其他事项是否由使用法令、普通法、习惯约束，包含但不限于不担保第三方权力、所有权、综合、满意度、适销性及对任何特别用途不被侵权。</p>

    <h3>12. 责任范围</h3>

    <p>客户清楚知晓且同意Treatstock不对任何直接的、间接的、偶然的、特别的、随机的、处罚性的或可效仿的损失负责，不赔偿包括但不限于收入、红利损失，合约、数据或其他无形损失，或其他任何由形式造成在使用服务或服务中断时造成的损失。</p>

    <p>任何情况下，Treatstock或其董事、雇员、合作伙伴、子公司或供应商均不为客户或任何第三方的收益损失负责；不为任何特殊的、偶然的或随机的损失负责；不为无论什么原因产生损失，不论公司是否提醒过该种损失的可能性，均不负责；由网站、服务器或此条约引起或与网站相关的任何责任理论，疏漏之处在所难免。</p>

    <p>另外，我们将不对第三方在网站上提供的商品和服务负责，包括且不限于订单处理过程。</p>

    <p>一些区域禁止排斥随机的、偶然的损失于责任范围之外，以上责任范围不适用于所有用户，</p>

    <h3>13. 可能的变化</h3>

    <p class="p-l20">13.1. Treatstock保留随时改变网站网页、服务、条约协议的权利。修改后的协议在发布后立即生效。</p>

    <p class="p-l20">13.2. 任何修改后的协议一经发布于网站或其他渠道后立即生效。Treatstock将负责任地在改变协议前至少一天发布通知。如有需要请定期查阅协议。</p>

    <p class="p-l20">13.3. 在协议内容改变后，作者可以继续在网站上发布其模型。若作者不同意即可及时将模型下架。</p>

    <h3>14. 适用法律</h3>

    <p>此条约协议依照美国特拉华州法律，不考虑与其法律原则冲突的其他区域。因此条约协议引发的任何及一切争议，包括对其有效性及其他有关使用Treatstock服务器或网站、产品订单及运送的争议，均将诉诸于联邦法院或位于美国特拉华州的州法院。联合国国际货物销售合同公约不适用于Treatstock服务器或网站及其产品订单和运送。</p>

    <h3>15. 联系方式</h3>

    <p>有对此条约协议或服务的任何疑问，请发送邮件至
        <script>
            var parts = ["support", "treatstock", "com", "&#46;", "&#64;"];
            var mailAddress = parts[0] + parts[4] + parts[1] + parts[3] + parts[2];
            document.write(\'<a href="mailto:\' + mailAddress + \'">\' + mailAddress + \'</a>\');
        </script>.
    </p>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '9', '4', 'fr', 'Conditions d’utilisation', '

    <h2>I. INTRODUCTION:</h2>

    <p>Bienvenue à Treatstock! Nous sommes ravis de vous avoir ici. S\'il vous plaît, veuillez lire ces termes et conditions attentivement avant d\'accéder au site, car il concerne un accord contraignant que vous devrez accepter pour naviguer ou vous inscrire sur Treatstock.</p>
    <p>Les termes et conditions sont des obligations juridiques entre vous et Treatstock, Inc. et ses filiales (collectivement, «Treatstock», «nous», «notre» ou «nos»). Cet accord (avec la déclaration de confidentialité Treatstock dans les «Conditions») régit vos droits et responsabilités lorsque vous utilisez les services fournis par treatstock.com, (le «Site Treatstock» ou «Site») et toutes les applications mobiles Treatstock (les "Applications").</p>
    <p>En utilisant nos services, vous acceptez d\'être lié par ces Termes et Conditions et vous êtes soumis à toutes les lois et règlements applicables. Vous confirmez également que tout fichier 3D ( "Modèle 3D") téléchargé sur le site Treatstock ne porte pas atteinte à toute loi ou réglementation fédérale, y compris celles relatives aux armes à feu. Treatstock se réserve le droit d\'apporter des modifications sur le Site, les Services et les Termes et Conditions à tout moment. Ces Termes et Conditions modifiées entreront en vigueur dès leur publication sur le Site ou par une autre méthode raisonnable. Treatstock prendra des efforts raisonnables pour publier les avis concernant les modifications à ces Termes et Conditions. S\'il vous plaît vérifiez les Termes et Conditions publiés sur ce site régulièrement pour vous assurer que vous êtes au courant de tous les termes régissant le site et les services. En outre, vous êtes responsable du maintien de normes applicables maintenues par ASTM, ANSI, ASME, ou d\'autres organismes de normalisation comparables. Si vous n\'êtes pas en accord avec tous les termes et conditions décrites ci-dessus, veuillez ne pas utiliser le site.</p>

    <h2>II. DEFINITIONS:</h2>

    <p><strong>Site:</strong> le site proposé par Treatstock au <a href="https://www.treatstock.com">treatstock.com</a> se réfère au site Treatstock, API, les applications logicielles ou tout moyen approuvé ou utilitaire soit actuellement en existence ou à l\'avenir; dont le code logiciel et la source utilisée par Treatstock pour fournir de tels services; mises en page de l\'interface utilisateur, dessins, images, textes, articles de la base de connaissances, le programme offre; informations sur le site fournies dans les rapports (tels que les recherches par mots clés populaires); et toute autre propriété intellectuelle protégé par le droit d\'auteur, marques, brevets, publicité, ou tout autre droit de propriété.</p>
    <p><strong>Treatstock:</strong> comprend Treatstock, Inc. et tous les affiliés et partenaires agréés.</p>
    <p><strong>Visiteur:</strong> Utilisateur enregistré ou non qui navigue sur le Site.</p>
    <p><strong>Utilisateur:</strong> Personne physique ou moral disposant et/ou utilisant l\'offre de service commercialisé (sous sa forme physique ou digitale).</p>
    <p><strong>Compte:</strong> le dossier d\'inscription de l\'utilisateur sur le site.</p>
    <p><strong>Modèle 3D:</strong> fichier imprimable en 3D fourni par l\'utilisateur.</p>
    <p><strong>3D Print:</strong> modèle 3D transformé en un objet physique par une imprimante 3D.</p>
    <p><strong>Auteur:</strong> l\'utilisateur qui fournit un modèle 3D pour Treatstock.</p>
    <p><strong>Fabricant:</strong> l\'utilisateur qui exploite une imprimante 3D et offre des services d\'impression 3D à travers le Site.</p>
    <p><strong>Ordre:</strong> une commande passée par l\'utilisateur via le site pour une impression 3D avec le fabricant sélectionné pour tourner de l\'auteur Modèle 3D dans le Print 3D lors de la réception du paiement pour l\'auteur, fabricant, service et frais de livraison.</p>
    <p><strong>Client:</strong> une personne qui utilise le site pour commander un modèle 3D ou 3D Print.</p>
    <p><strong>Page Produit:</strong> est la page du produit ou de l\'interface qui affiche les actifs Modèle 3D disponible pour l\'ordre, généralement sur le site <a href="https://www.treatstock.com">treatstock.com</a>.</p>
    <p><strong>Service de livraison:</strong> services tiers utilisés pour la livraison de l\'impression 3D du fabricant au client.</p>
    <p><strong>Contenu de l\'utilisateur:</strong> Modèles 3D, images, vidéos, textes, ou tout autre contenu que l\'utilisateur soumet au Site.</p>
    <p><strong>Auteur Fee:</strong> le montant total dû à l\'auteur pour l\'utilisation du modèle 3D dans un ordre particulier.</p>
    <p><strong>Impression Fee:</strong> le montant total dû au fabricant pour les services d\'impression, y compris le démarrage et les coûts des matières pour le travail d\'impression utilisé pour produire l\'impression 3D, proposé et désigné par le fabricant comme indiqué sur le site.</p>
    <p><strong>Frais de service:</strong> la redevance due à Treatstock en contrepartie des services fournis par Treatstock et les taxes applicables.</p>
    <p><strong>Frais de livraison:</strong> la redevance due à Treatstock ou à un tiers pour la livraison de l\'impression 3D du fabricant au client.</p>
    <p><strong>Services:</strong> Les services offerts par Treatstock à travers le site tels que définis dans l’article « Service » de la section 3 ci-dessous de ces Termes et Conditions.</p>
    <p><strong>Propriété intellectuelle:</strong> signifie le droit d\'auteur, brevet, marque, secret commercial, droit de la publicité, ou tout autre droit de propriété à travers le monde.</p>

    <h2>III. SERVICES TREATSTOCK: </h2>
    <p>Les services Treatstock combinent différents besoins d\'impression 3D fournis par le site. Treatstock est un marché pour la recherche et la commande de tout ce qui peut être fait sur une imprimante 3D.</p>
    <p>Les utilisateurs seront en mesure de choisir parmi un large éventail de modèles 3D ajouté par un designer, puis d’imprimer ce modèle sur une imprimante 3D. En outre, l\'utilisateur est en mesure de choisir un service d\'impression et ce service d\'impression va fabriquer un modèle dans un objet réel et de lui expédier. En outre, l\'utilisateur peut demander de créer un objet 3D sur mesure par un designer.</p>
    <p>Treatstock offre également un soutien pour les concepteurs et les services d\'impression si elles ont des problèmes avec la fabrication de modèles 3D. Il existe un système de soutien communautaire et des forums pour les membres Treatstock pour les questions et pour partager leurs expériences.</p>
    <p>En outre, le propriétaire du service d\'impression a accès à du matériel éducatif pour l’aider avec le processus de fabrication.</p>

    <h2> IV. CONDITIONS DE SERVICES:</h2>
    <p>Pour utiliser les Services, vous devez certifier que vous avez l\'âge légal de faire un accord juridique. Si vous êtes sous l\'âge légal, vous pouvez utiliser les services de <a href="https://www.treatstock.com">Treatstock.com</a>  uniquement avec accusé des parents ou d\'un tuteur.</p>
    <p>Pour utiliser et accéder aux services que vous êtes responsable de fournir, d’entretenir et de mettre à jour votre nom légal complet, l\'adresse actuelle, numéro de téléphone, une adresse e-mail valide, et toute autre information nécessaire pour compléter l\'enregistrement. Dans notre seule discrétion, nous pouvons refuser l\'utilisation des services à toute personne ou entité pour un compte.</p>
    <p>Vous êtes responsable du stockage sécurisé de votre mot de passe. Treatstock ne peut pas et ne sera pas responsable de toute perte ou dommages résultant de votre incapacité à maintenir la sécurité de votre compte et mot de passe.</p>
    <p>Vous autorisez Treatstock à communiquer avec vous via une adresse e-mail fournie lors de l\'inscription.</p>
    <p>Pour annuler votre compte Treatstock, vous avez deux possibilités : Vous pouvez nous envoyer un message via le site Web à notre service de support, ou vous pouvez nous <a href="/site/contact">envoyer un courriel</a> via votre compte.</p>
    <p>Les services ne doivent pas être utilisé à des fins illégales ou non autorisées qui pourraient violer les lois et règlements.</p>
    <p>Un non-respect ou la violation de tout terme dans les conditions d\'utilisation tel que déterminé ici à la seule discrétion de Treatstock entraînera une résiliation immédiate de vos services.</p>
    <p>Nous nous réservons le droit de refuser les services, ou de résilier les Services, de quiconque, à tout moment et sans préavis pour une raison quelconque.</p>
    <p>Les services offerts par Treatstock à travers son site consistent uniquement à fournir une plate-forme en ligne aux fabricants pour faciliter la transmission des impressions 3D de modèles 3D de l\'auteur à l\'utilisateur avec la possibilité d\'utiliser les services de livraison, y compris la transaction de paiement entre les utilisateurs, les auteurs, les fabricants, les services de livraison et les fournisseurs.</p>
    <p>Les services permettent aux auteurs de télécharger le modèle 3D et tout autre contenu de notre site et de préciser ses honoraires. Les services permettent au fabricant d\'énumérer les services 3D d\'impression et de spécifier le montant des frais pour les services sur le Site. Cela permet à l\'utilisateur de gérer les conditions avec le fabricant avant l’impression 3D du modèle. Une fois que la commande a été traitée, l\'utilisateur sélectionne le mode de livraison. Le fabricant tiendra Print 3D au nom de l\'utilisateur jusqu\'à ce que l\'impression 3D soit livrée. En outre, l\'utilisateur sera en mesure de prendre le Print 3D s’il le souhaite.</p>
    <p>Treatstock n’est pas partie prenante des accords conclus entre les fabricants, les auteurs et les utilisateurs. Treatstock ne doit pas non plus être considéré comme un fabricant, un fournisseur de service d\'impression ou de service financier.</p>
    <p>Treatstock n\'a aucune obligation de contrôler la conduite des fabricants, des auteurs et des utilisateurs, ni le contenu téléchargé sur le site par les utilisateurs et l\'impression 3D et des services fournis par le fabricant. Treatstock décline toute responsabilité à cet égard.</p>
    <p>Treatstock offre une protection pour tous les achats, et se réserve donc le droit de résilier la commande et de rembourser la clientèle en tout ou en partie par sa seule décision selon la procédure décrite dans la clause «la protection de l\'achat et de retour politique" de la section XII.</p>

    <h2>V. L\'UTILISATEUR:</h2>
    <p>Les services offerts par Treatstock sont disponibles uniquement aux personnes qui ont 18 ans ou plus, qui peuvent signer des contrats juridiquement contraignants en vertu du droit applicable, et qui se sont enregistrés comme utilisateur sur le site en créant un compte.</p>
    <p>L\'utilisateur ou tout visiteur du site déclare et garantit que tous les renseignements présentés sur le site soit exacte, véridique, légal et ne viole pas les droits d’un tiers. L\'utilisateur ou tout visiteur du site garantit qu\'il n’utilisera pas les services pour:</p>
    <ul>
        <li>contact another user of Treatstock for the purpose of soliciting your services outside of Treatstock</li>
        <li>Envoyer des courriers non sollicités en masse («spam»);</li>
        <li>Lancer ou utiliser tout moyen ou procédé automatisé, y compris mais sans s\'y limiter "araignées", "robots", "testeurs de charge", etc., pour communiquer plus qu’une personne physique ne le pourrait ou de créer des comptes d\'utilisateurs (sauf pour les opérateurs de moteurs de recherche publics dont nous autorisons à utiliser des robots pour copier les matériaux des Sites ou services dans le seul but et uniquement dans la mesure nécessaire de créer des index de recherche de matériaux accessibles au public, mais pas des caches ou des archives de ces matériaux );</li>
        <li>Recueillir ou stocker des informations personnelles provenant d\'autres utilisateurs autres que les informations recueillies dans le cours normal de l\'utilisation du service;</li>
        <li>Menacer ou de harceler toute personne ou entité;</li>
        <li>Gagner ou tenter d\'obtenir un accès non autorisé à un système informatique, serveur, réseau ou matériel de Treatstock, d\'autres utilisateurs ou de toute autre tierce partie d\'une façon qui pourrait surcharger, endommager, désactiver, perturber ou nuire au service ou interférer avec l\'utilisation de tout autre utilisateur du Service</li>
        <li>Télécharger, transmettre ou distribuer des virus informatiques, des vers ou tout logiciel destiné à endommager ou de modifier un système informatique ou des données;</li>
        <li>Adapter, modifier ou désosser un des systèmes ou des protocoles de Treatstock;</li>
        <li>Adapter, modifier ou infirmer le modèle 3D d’un autre utilisateur, de ses dérivés dans une forme ou un format différent. </li>
        <li>Reformater, revendre ou redistribuer le service de quelque façon sans le consentement explicite de Treatstock par écrit. </li>
    </ul>

    <p>Pour utiliser certains services, vous devez vous devez créer un compte sur la page (un «compte»). Vous devez fournir un contact et votre identification précise et actuelle, et d\'autres informations qui peuvent être nécessaires dans le cadre du processus d\'enregistrement du compte et / ou d\'utilisation continue du service, et vous devez mettre à jour vos informations sur le compte. Vous ne devez pas:</p>
    <ul>
        <li>Choisir ou utiliser comme nom d\'utilisateur le nom d\'une autre personne avec l\'intention d\'usurper l\'identité de cette personne;</li>
        <li>Utiliser comme nom d\'utilisateur, un nom sous réserve des droits d\'une autre personne sans autorisation appropriée; ou</li>
        <li>Utilisercomme nom d\'utilisateur, un nom qui est offensant, vulgaire ou obscène.</li>
    </ul>
    <p>Vous êtes responsable du maintien de la confidentialité du mot de passe de service de votre compte et êtes seul responsable de toutes les activités qui se produisent sur votre compte. Vous devez nous informer immédiatement de tout changement de votre admissibilité à utiliser les services, violation de la sécurité ou de l\'utilisation non autorisée de votre compte. Vous ne devriez jamais publier, distribuer ou afficher des renseignements de connexion pour votre compte. Nous ne serons pas responsables de toute perte ou dommages résultant de votre incapacité à maintenir la sécurité de votre compte et mot de passe.</p>
    <p>Vous aurez la possibilité de supprimer votre compte, soit directement, soit par le biais d\'une demande faite à un de nos employés ou affiliés.</p>
    <p>Dans le cas où Treatstock détermine que l’utilisateur a enfreint les présentes Conditions Générales, Treatstock peut, à sa seule discrétion, sans aucune compensation interdire à l\'utilisateur ou à l\'entité tout accès au Site et / ou la prestation de ses services, et / ou le changement des critères d\'admissibilité à tout moment sans préavis.</p>
    <p>Toute tentative par l\'utilisateur d’endommager le site ou de saper les opérations légitimes des services peut constituer une violation des lois criminelles et civiles, et au cas où une telle tentative, Treatstock se réserve le droit de demander des dommages et intérêts à un utilisateur au maximum de ce qui est permis par la loi.</p>
     
    <h2>VI. CONTENU GÉNÉRÉ PAR L\'UTILISATEUR</h2>
    <p>Le terme «contenu généré par l’utilisateur» lorsqu\'il est utilisé dans les présentes Conditions générales de vente signifie tout contenu téléchargé sur le site par vous, y compris mais non limité à tous les modèles 3D téléchargées, Modèles 3D, des fichiers 3D, des images de tous les objets, les images de tout modèle, commentaires et avis sur le site. Tout contenu généré par l’utilisateur ne devrait pas contenir de propos abusifs, harcelants, menaçants, diffamatoires, obscènes, frauduleux, trompeurs, offensants, pornographiques, illégales ou illicites ou susceptible de porter atteinte aux droits de propriété intellectuelle. Les droits de propriété intellectuelle signifie le droit d\'auteur, brevet, dessin ou modèle enregistré, marque, secret commercial ou tout autre droit de propriété ou industrielle. Treatstock se réserve le droit (mais n\'a aucune obligation) d\'examiner tout contenu d’utilisateur, d’enquêter, et / ou de prendre des mesures appropriées contre vous à notre entière discrétion si vous violez ces Termes et Conditions, ou autrement de créer une responsabilité pour nous ou toute autre personne. De tels actes peuvent inclure la suppression ou la modification de votre Contenu utilisateur, la résiliation de votre compte de l\'entreprise, et / ou le signalement aux autorités compétentes.</p>
    <p>Treatstock n\'a aucune obligation de revoir le contenu généré par l\'utilisateur et ne peut en aucun cas être tenu responsable du contenu généré par l\'utilisateur. Les opinions exprimées dans le contenu généré par l\'utilisateur ne sont pas nécessairement l\'opinion de Treatstock.</p>
    <p>Tout contenu généré par l’utilisateur, y compris les modèles 3D, qui sont référencés ci-dessous et qui sont soumis au contenu généré par l’utilisateur à travers les Services ou le Site, nous est accordé par vous via une licence mondiale non exclusive, libre de redevances, entièrement libérée, sous-licenciable et transférable pour utiliser, reproduire, distribuer, préparer des travaux dérivés, afficher, exécuter, et autrement exploiter pleinement le contenu généré par l\'utilisateur en relation avec le site, les services et nos entreprises (y compris nos successeurs) et sans limitation pour la promotion et la redistribution de tout ou partie du site et/ou les services (et des œuvres dérivées de ceux-ci) dans tous les formats de médias et à travers tous les canaux de médias (y compris des sites tiers).</p>
    <p>Vous octroyez aussi par la présente à chaque Utilisateur du Site et / ou les services une licence non exclusive pour accéder à votre Contenu généré par l’utilisateur (sauf modèles 3D qui est décrit ci-dessous) par l\'intermédiaire du Site et / ou les services, et à utiliser, éditer, modifier, reproduire, distribuer, préparer des travaux dérivés, afficher et effectuer un tel contenu généré par l’utilisateur. Chaque fois que vous publiez vos modèles 3D sur le Site, Licence Treatstock Commercial accorde une utilisation limitée du matériel protégé par d\'autres qui se trouvent à l\'annexe A.</p>
    <p>Pour plus de clarté, les licences qui précède, accordées à nous et nos utilisateurs ne portent pas atteinte à vos autres droits de propriété ou de licence dans votre Contenu généré de l’Utilisateur, y compris le droit d\'accorder des licences supplémentaires à votre Contenu Généré par l’Utilisateur, sauf convention contraire par écrit. Vous déclarez et garantissez que vous avez tous les droits à nous accorder de telles licences sans contrefaçon ou violation des droits de tiers, y compris, sans limitation, tous les droits à la vie privée, les droits de publicité, droits d\'auteur, les droits contractuels, ou toute autre propriété intellectuelle ou les droits de propriété.</p>
    <p>Vous octroyez aussi par la présente à chaque utilisateur du site, les applications ou les services d\'une licence non exclusive pour accéder à votre Contenu utilisateur via le Site, les applications ou les services, et partager votre contenu généré sur les blogs et médias sociaux.</p>
    <p>Treatstock n\'a pas l\'obligation de sauvegarder tout Contenu utilisateur. Le contenu de l\'utilisateur peut être supprimé à tout moment. Vous êtes seul responsable de la création de copies de sauvegarde de votre Contenu utilisateur si vous le désirez.</p>
    <p>Vous acceptez de ne pas utiliser le Site ou les Services pour collecter, télécharger, transmettre, afficher ou distribuer tout Contenu Utilisateur:</p>
    <ul>
        <li>Qui viole un des droit de tiers, y compris les droits d\'auteur, marques, brevets, secrets commerciaux, droit moral, les droits de conception, les droits de la vie privée, droit de la publicité, ou de toute autre propriété intellectuelle ou droit de propriété;</li>
        <li>Qui utilise des propos illégaux, abusifs, tortueux, menaçants, nuisibles, intrusifs dans la vie privée d\'autrui, vulgaires, diffamatoires, faux, intentionnellement trompeuses, de nature diffamatoire, pornographique, sexuellement explicite, obscène, manifestement offensif, favorisant le racisme, le sectarisme, la haine, causant des dommages de toute nature contre un groupe ou un individu, la promotion d\'activités illégales ou contribue à la création d\'armes, de matériel illégal ou est autrement répréhensible;</li>
        <li>Ce qui est nuisible aux mineurs de quelque manière; </li>
        <li>Ce qui est en violation de toute loi, réglementation, ou des obligations ou des restrictions imposées par une tierce partie. </li>
    </ul>
    <p>Concernant la création automatique de contenu,Treatstock se réserve le droit de collecter des données sur les utilisateurs du site afin d\'améliorer l\'expérience de l\'utilisateur. En particulier, cela inclut des images (i) modèles rendus dans des couleurs différentes et sous des angles différents, (ii) le calcul du poids de modèle et de tailles pour différentes imprimantes 3D.</p>

    <h2>VII. PRIX </h2>
    <p>Le prix indiqué sur le site comprend les frais d\'impression, frais de l\'auteur, frais de livraison, frais de service et taxes supplémentaires le cas échéant, et est représenté dans la devise choisie par le visiteur.</p>
    <p>Tous les prix sont indiqués en devises correspondantes. Si l\'utilisateur choisit de ne pas payer dans la devise indiquée, la conversion de devises sera traitée par notre fournisseur de services de paiement.</p>
    <p>Chaque frais est libellée dans une monnaie déterminée par l\'auteur, le fabricant, le service de livraison et Treatstock. Treatstock se réserve le droit de fixer les prix dans la monnaie spécifiée par le Visiteur en utilisant le taux de change à la seule discrétion de Treatstock.</p>
    <p>Le prix indiqué sur le site comprend les frais d\'expédition, la taxe de vente ou de la TVA (le cas échéant), mais exclut les droits d\'importation ou taxes si applicable. Toutefois, droits, taxes et frais de courtage peuvent être perçus à l\'importation, selon le pays de votre commande de destination. Les frais d\'expédition indiqués par nous sont uniquement pour le transport de votre commande et ne comprennent pas ces frais supplémentaires. Vous pouvez communiquer avec l\'autorité douanière du pays de votre commande pour en apprendre plus sur les droits et taxes applicables. Le montant de la taxe à l\'importation est déterminé par le classement du produit commercialisé, et cela se fait sur la base d\'un code qui sera indiqué sur le document d\'expédition, qui est connu comme un «code Intrastate». Le droit à l\'importation et de la taxe combinée sont typiquement 5-20% de la valeur totale de la commande. Dans certains cas, des frais de courtage additionnel peuvent être perçu par le transporteur.</p>
    <p>Treatstock se réserve le droit de modifier les prix des produits et services présentés dans la galerie publiée sur le site, ainsi que les prix indiqués pour les commandes personnalisées, à tout moment.</p>

    <h2>VIII. CHOIX DU PRODUIT</h2>
    <p>En commandant un produit, l\'utilisateur confirme que le produit est soumis à aucune restriction de licence, de certifications, ou d\'autres règlements ou règles qui interdit sa production ou son utilisation. L\'Utilisateur est seul responsable de toute violation et de toute restriction de licence, certifications, ou tout autre règlement ou des règles qui interdit sa production ou de l\'utilisation. Treatstock décline toute responsabilité pour les cas précédents.</p>
    <p>Lors de la sélection des produits, le visiteur agit à ses propres risques. Treatstock, l\'auteur et le fabricant ne sont pas responsable du choix du client, de l’aptitude à l\'emploi ou les attentes, de l\'applicabilité du produit à des fins, ou de l\'utilisation de produits dans toutes les régions.</p>
    <p>Afin d\'améliorer l\'expérience de l\'utilisateur, nous essayons de montrer tous les modèles 3D dans des couleurs différentes; Cependant, les images de modèle 3D sous certains angles peuvent être en une seule couleur.</p>
    <p>Aux fins de la protection du droit d\'auteur, nous ne permettons pas la rotation à 360 degrés du modèle.</p>

    <h2>IX. COMMANDE  </h2>
    <p>Le site détecte l\'adresse IP du visiteur pour déterminer son emplacement géographique. Cette information est utilisée pour suggérer les imprimantes qui sont proches, ou dans la proximité, de l\'emplacement physique du visiteur. Le visiteur peut facilement changer son emplacement en spécifiant le nom de la ville où il / elle se trouve.</p>
    <p>En naviguant sur le site, le visiteur peut choisir le modèle ou KIT 3D (un ensemble de parties distinctes à assembler). Le modèle 3D sera montré sur l\'image dans un matériau et la couleur choisie par le visiteur. Le site va alors essayer de trouver un service d\'imprimante qui peut imprimer le modèle 3D dans ce matériau et la couleur.</p>
    <p>Le site montre le coût de l’imprimerie 3D, le modèle choisi et la liste des services d\'impression disponibles qui sont à proximité. Ce prix comprend aussi les honoraires de l\'auteur. Le visiteur peut choisir un service d\'impression et engager la procédure de payement.</p>
    <p>Au cours du processus de commande, le site permet au visiteur de choisir une méthode de livraison. A l’étape finale, le site montre le cout total, les frais et les taxes applicables et toutes les informations nécessaires.</p>
    <p>Le Visiteur accepte les Termes et Conditions, et paie pour la commande en utilisant la méthode choisie.</p>
    <p>Treatstock envoie la commande d\'exécution du fabricant que le visiteur a sélectionné, et identifie la matière, la couleur, et le coût de l\'impression sur la base de notre calcul.</p>
    <p>Treatstock se réserve le droit de rejeter la commande avec ou sans explications, et de rembourser l\'argent entièrement ou partiellement au compte à partir duquel il a été payé, avec ou sans compensation à l\'auteur ou le fabricant, par notre seule discrétion.</p>

    <h2>X. PAIEMENTS ET FACTURATION</h2>
    <p>Treatstock accepte les paiements via la plupart des cartes de crédit et de débit dont VISA, MasterCard, American Express, Discover, JCB et Diners Club. PayPal est également accepté.</p>
    <p>Lorsque l\'utilisateur autorise un paiement par carte de crédit / débit, le paiement est en attente pour une durée maximale de 8 jours. Lorsque la commande est acceptée par le fabricant, ces fonds sont transférés sur notre compte et la commande sera alors traité. Ce processus est géré par nos partenaires de solutions de paiement Braintree et Stripe.</p>
    <p>Lorsque l\'utilisateur autorise un paiement en utilisant Apipay, les fonds sont transférés sur notre compte au moment de l\'autorisation.</p>
    <p>Dans le cas où le fabricant ne peut pas traiter votre commande, nous vous faisons parvenir votre argent sur votre compte bancaire.</p>
    <p>Dans le cas où nous déterminons que l\'impression 3D n’est pas d\'une qualité suffisante, nous pouvons totalement ou vous rembourser sur la base des lignes directrices de la politique de protection des achats.</p>
    <p>If the Manufacturer has accepted an order, but for some reason wants to cancel it, the Manufacturer must contact customer support to do so. If the Manufacturer cannot fulfil the order for any reason, Treatstock may change the Manufacturer to complete the order. The customer must agree that chosen Manufacturer can be changed by Treatstock when placing the order. If the customer does not agree to change the Manufacturer in the event that the Manufacturer cannot process the order, Treatstock will return your money in full to the card or bank account that was used to place the order.</p>

    <h2>XI. LIVRAISON</h2>
    <p>Le fabricant peut choisir le mode de livraison et de ramassage si disponible.</p>
    <p>Le fabricant peut choisir des services de livraison indépendants. Dans ce cas, le fabricant fixe le prix pour la livraison, et Treatstock comprendra ce prix dans le coût total du client.</p>
    <p>Le fabricant peut choisir de travailler avec des services de livraison indépendants tels que UPS, USPS etc. en utilisant les services de Treatstock.</p>
    <p>Dans ce cas, Treatstock fixe le prix pour la livraison à sa seule discrétion, et comprend ces frais au coût total pour le client. Il peut ne pas correspondre au prix du service de livraison habituel public, car au moment de la commande les paramètres exacts de l\'impression 3D et des services possibles ne sont pas encore connus et Treatstock prend les risques associés.</p>
    <p>Dans ce cas, Treatstock prend en charge les frais de livraison, et paye pour la prestation de services réelle appropriée au moment du modèle d\'impression 3D fini, et fournit au fabricant avec la possibilité d\'imprimer des étiquettes pour la livraison.</p>
    <p>Le fabricant doit emballer l’imprimante 3D pour la livraison le cas échéant.</p>

    <h2>XII. ACHAT DE PROTECTION ET POLITIQUE DE RETOUR</h2>
    <p>Treatstock permet au client d\'annuler sa commande et de se faire rembourser dans son intégralité avant que l\'impression 3D n’ait été lancée. Après que l\'impression 3D ait été lancée, le client ne peut pas annuler sa commande.</p>
    <p>Étant donné que le produit est fait sur mesure, nous vous rembourserons l\'argent que dans le cas de faute ou de défaut de l\'impression 3D, ou en cas de non-livraison de l\'impression 3D dans un délai raisonnable.</p>
    <p>Treatstock se réserve le droit de retarder le paiement à l\'auteur et le fabricant jusqu\'à ce que le client confirme une qualité acceptable de l\'impression 3D.</p>
    <p>Si le client n\'a pas enregistré une plainte auprès de Treatstock dans les 30 jours suivant l\'achat, Treatstock suppose que la qualité est acceptable. Nous n\'acceptons plus de plainte après 30 jours après la livraison.</p>
    <p>Après avoir reçu l\'impression 3D et avant de l\'utiliser, le client doit en assurer la qualité, dans le cas où la qualité ne répond pas aux exigences fixées. L\'utilisateur doit immédiatement nous faire connaître et joindre des photos de l\'impression 3D et de son emballage d\'origine. Treatstock ne remboursera pas les paiements pour les impressions 3D qui ont été utilisés. L\'utilisateur doit être prêt à retourner l’impression 3D conjointement avec l\'emballage d\'origine si nous demandons de le faire. Le client est responsable de retour d\'impression 3D, y compris des frais d\'expédition.</p>
    <p>Lors de la réception d\'une plainte de la part du client, Treatstock peut mettre fin à la commande et de rembourser l\'argent au client à sa seule discrétion. Alternativement, Treatstock peut demander au fabricant d\'imprimer et de livrer l\'impression 3D une fois de plus à leurs propres frais, ou d\'utiliser une autre solution pour résoudre le problème.</p>
    <p>Treatstock se réserve le droit de demander une indemnisation pour les dommages de toute partie qui a violé cet accord.</p>

    <h2>XIII. POLITIQUE DE QUALITÉ`</h2>
    <p>Les modèles 3D ne sont pas considérés comme défectueux si:</p>
    <ul>
        <li>Il y a des lignes visibles à l\'œil nu après l\'impression.</li>
        <li>On trouve des traces de traitement de surface à cause des structures de support qui ont été utilisées lors de l\'impression.</li>
        <li>On constate la présence de filaments minces sur la surface de l\'objet imprimé.</li>
        <li>La structure interne est visible à travers la lueur de la lumière.</li>
        <li>Il y a Des traces de lignes où l\'imprimante passe d\'une couche à la couche suivante.</li>
        <li>Des lignes distinctes sont visibles sur la surface inférieure de l\'objet 3D.</li>
    </ul>
    <p>Les impressions 3D sont considérées comme défectueux si:</p>
    <ul>
        <li>Les surfaces extérieures ne sont pas fermées correctement ou semblent cahoteuses.</li>
        <li>Cercles irréguliers: les cercles sortent difformes et les lignes ne sont pas correctement tracées.</li>
        <li>Les Coins du l’impression 3D se détache de la plate-forme.</li>
        <li>Certaines parties de l\'impression 3D se déplacent le long de l\'axe X ou Y.</li>
        <li>L’impression 3D se penche progressivement.</li>
        <li>L’impression 3D possède une extrusion.</li>
        <li>Les parties ou les murs entiers de l\'impression ne sont pas fusionnés et ne se touche pas.</li>
        <li>Les parties les plus basses de l\'impression semblent avoir rétréci avant d\'atteindre les dimensions appropriées.</li>
        <li>La première couche ou les pièces de desserrage n’ont pas collé</li>
        <li>On constate des brins indésirables en plastique à travers l\'impression.</li>
        <li>Des vagues/ ombres apparaissent sur l’impression 3D.</li>
        <li>Surfaces laides: La surface inférieure des surplombs sort laides.</li>
        <li>Les lignes sont visibles ou trop espacées sur la première couche.</li>
        <li>Des très minces brins de plastique apparaissent sur la surface de l\'impression.</li>
    </ul>
    <p>Les règles d\'impressions 3D Post-traitement:</p>
    <ul>
        <li>En dépit du fait que les pièces d\'impression 3D sont produites à partir du fil en utilisant la technologie FDM, la surface du produit final ne peut pas être prévu pour être parfaitement lisse. Pour l\'améliorer, les méthodes suivantes sont proposées:</li>
        <li>Imprimer avec la plus haute résolution possible pour l\'imprimante (par exemple, la résolution du Ultimaker de 2 à 20 microns - 0,02 mm). La l’impression 3D finale sera la plus douce, mais ces paramètres peut entraîner différents défauts d\'impression, et d\'allonger considérablement la durée de l\'impression.</li>
        <li>Le Post-traitement : La surface peut être lissée par ponçage au papier de verre ou de raffinage avec une aiguille-dossier, cependant, vous devez être prudent.</li>
        <li>Le traitement avec des solvants différents. La surface peut être lissée parfaitement en utilisant des solvants tels que l\'acétone pour ABS-plastique ou Dichlorométhane pour PLA-plastique (relativement toxiques). Le modèle peut être plongé dans un récipient avec un solvant ou traitées à la vapeur.</li>
        <li>Cependant, il est important de garder à l\'esprit que les solvants peuvent dissoudre les couches externes du modèle 3D plus que nécessaire, ruinant ainsi le produit.</li>
        <li>Gardez à l\'esprit que lorsque vous travaillez avec des solvants, il faut être extrêmement prudent. Tout travail doit être effectué dans une pièce bien ventilée. En outre, on doit utiliser l\'équipement de sécurité pour protéger les yeux, les voies respiratoires et la peau.</li>
    </ul>
    <p>Règles pour les modèles Auteurs d\'avoir des modèles 3D imprimables: </p>

    <ol>
        <li>éléments minces
            <br>
            Lors de l\'impression des éléments minces, les éléments suivants devraient être pris en compte:

            <ul>
                <li>La taille des éléments les plus minces ne doit pas être plus petit que le diamètre de la buse de l\'imprimante. Dans le cas contraire, ces éléments ne seraient pas imprimables.</li>
                <li>L\'épaisseur de paroi (y compris les parois supérieure et inférieure) de l’impression 3D doit dépasser la hauteur d\'une seule couche d\'au moins 2-3 fois (idéalement 4-6 fois). Si l\'épaisseur de la paroi est plus petite, la résistance des parois réduit de manière significative et il peut y avoir des écarts entre les couches visibles.</li>
                <li>Lors de l\'exécution de la procédure de post-traitement de petites pièces il faut se rappeler qu\'elles peuvent devenir moins visibles ou disparaître. Il est nécessaire de traiter ces petites pièces avec une grande prudence.</li>
                <li>Il est difficile pour certaines machines à couper/trancher (Cura) de construire des structures de soutien pour les petites pièces. Ce problème peut être évité en augmentant la densité de construction de structures de support, ou en utilisant plusieurs machines à couper/ trancher pour générer des structures de support.</li>
                <li>Le niveau de détail du modèle se détériore avec sa taille décroissante lors du redimensionnement. Certains éléments du modèle sont bien reproduits lorsque la taille du modèle est grand, alors que les mêmes éléments ne peuvent pas être imprimées aussi bien sur une version plus petite, qui est la raison pour laquelle ils doivent être supprimés ou modifiés. Il est nécessaire de prêter une grande attention lors de la préparation pour l\'impression de tous les modèles soumis à l\'entartrage.</li>
            </ul>
        </li>

        <li>Que faire avec des structures de soutien :
            <ul>
                <li>Les structures de soutien sont nécessaires pour les éléments d\'impression du modèle qui sont en surplomb.</li>
                <li>Il est possible d’imprimer sans structure d’appui pour les angles ne dépassant pas 60-70 degrés. Gardez à l\'esprit la qualité d\'impression peut diminuer légèrement.</li>
                <li>Le nombre de structures de soutien peut être réduite en plaçant simplement le modèle à un angle différent. Par exemple, un objet plat couché sur le plan horizontal de la table sera imprimé beaucoup plus rapide que debout verticalement.</li>
                <li>Dans certains cas, il est possible d\'imprimer sans aucune structure de support, en divisant simplement le modèle en plusieurs parties.</li>
                <li>Gardez à l\'esprit que le soutien des structures peuvent ruiner l\'apparence de la surface du modèle 3D, il est donc préférable de placer le modèle à un angle tel que le soutien des structures seraient créés dans des endroits qui ne sont pas visibles, ou dans des endroits qui peuvent être facilement traitées ou enlevé.</li>
                <li>Réduire le nombre de structures de soutien contribuera à un meilleur processus d\'impression. Il permettra de réduire la quantité de filament nécessaire pour l\'impression et le temps, donc de réduire son coût.</li>
                <li>- Positionnement par rapport à la vitre de la table d\'impression</li>
                <li>Lors du positionnement du modèle 3D par rapport à la surface de travail de l\'imprimante les éléments suivants devraient être pris en compte:
                    <ul>
                        <li>La modification d’un angle peut réduire de manière significative (ou augmenter) la quantité de structures de soutien nécessaires à la construction du modèle 3D. Par exemple, l\'élément en surplomb du modèle 3D peut être évité en plaçant horizontalement le modèle vertical.</li>
                        <li>La force du modèle 3D peut être augmentée en plaçant de telle sorte que la zone avec la plus grande charge soit disposée perpendiculairement aux couches imprimées du modèle, et non parallèle à eux.</li>
                        <li>La qualité du motif d\'éléments circulaires peut être augmentée en plaçant le modèle 3D de sorte qu\'il soit imprimé à l\'horizontale. Dans certains cas, les cercles du joint vertical peuvent conduire à leur déformation (au lieu de trous circulaires, desellipses seront obtenus).</li>
                    </ul>
                </li>
            </ul>
        </li>

        <li>3. La taille du modèle 3D
            <br>
            Lors de la sélection de la taille du modèle devrait être considéré comme le suivant:
                <ul>
                    <li>La taille maximale du modèle pour une imprimante 3D est généralement déterminée par le volume de construction (par exemple, le volume de construction pour la Ultimaker 2 est 230x225x205 mm).</li>
                    <li>Il est possible d\'imprimer un modèle en plus grandes taille si le modèle est divisé en plusieurs parties (chacune d\'entre elles peuvent être placées dans le volume de construction), et les imprimer séparément et connecter ou les coller ensemble. L\'imprimante n\'imprime pas des objets plus minces que le diamètre de la buse. En d\'autres termes, la taille minimale du modèle ne doit pas être inférieur au diamètre de la buse.</li>
                    <li>Afin d\'assurer la solidité et la stabilité de la structure, l\'épaisseur optimale des parois de n\'importe quel type devrait être 2-3 fois le diamètre de la buse.</li>
                </ul>
        </li>
    </ol>

    <p>Les normes des emballages :</p>
    <ul>
        <li>Utilisez des boîtes robustes et en bon état avec tous les volets intacts.</li>
        <li>Utilisez des boîtes à double paroi pour les articles plus lourds.</li>
        <li>Enveloppez les articles individuellement avec un matériau de rembourrage et les centrer dans des cartons loin des autres articles et loin des côtés, coins, en haut et en bas de la boîte. Utiliser un matériau de rembourrage adéquat.</li>
        <li>Placez les articles qui pourraient être endommagés par une manipulation normale, tels que l\'encrassement, le marquage, ou l\'application d\'étiquettes adhésives, dans une boîte extérieure de protection.</li>
        <li>Utilisez des charges pour combler les espaces vides et empêcher tout mouvement des marchandises à l\'intérieur de la boîte lors de l\'expédition.</li>
        <li>Utilisez la méthode de pose de ruban de H pour sceller votre colis. Fermer et la bande de la boîte intérieure en utilisant la méthode en H. Cela aidera à prévenir l\'ouverture accidentelle.</li>
        <li>Restreindre le mouvement de produit à l\'intérieur de la boîte en utilisant une charge comme du papier journal froissé, des arachides de remplissage en vrac, ou tout autre matériau de rembourrage.</li>
        <li>Transporter les produits fragiles individuellement en les enveloppant dans un minimum de 3 cm d’épaisseur de matériau de rembourrage air-cellulaire.</li>
        <li>Remplissez les espaces vides avec plus de matériau de rembourrage.</li>
        <li>Enveloppez tous les articles séparément</li>
        <li>Utilisez du ruban adhésif solide conçu pour l\'expédition</li>
        <li>Utilisez une étiquette d\'adresse unique qui complétera les informations et facilitera la livraison.</li>
    </ul>

    <p>Si vous avez des questions concernant un défaut d\'impression possible, s\'il vous plaît nous contacter en utilisant <a href="/site/contact">le formulaire de contact</a> avec une image de bonne qualité de l\'impression 3D ci-joint.<p>

    <h2>XIV. SERVICES DE TIERS</h2>
    <p>Le Site peut contenir des liens vers d\'autres sites et services, ou inclure des I-Frames spéciaux provenant d\'autres sites et services. Nous ne sommes pas responsable du contenu et de la disponibilité de ces sites et services. Chacun de ces services a ses propres termes et conditions, veuillez les lire attentivement.</p>
    <p>Dans certains cas pour fournir les fonctions d\'un site, nous pouvons transférer des données vers d\'autres sites et services, et ces données peuvent inclure des informations personnelles (ou PII). Nous transférons uniquement les informations nécessaires et requis par de tels services pour gérer soigneusement ces renseignements personnels, mais en utilisant notre service vous êtes d’accord avec la possibilité d\'un tel transfert.</p>
    <p>Incluant mais sans s\'y limiter, le changement de sujet.</p>

    <h2>XV. VIOLATION DE COPYRIGHT</h2>
    <p>Si vous croyez qu’un matériel sur le site est en violation de tout droit d\'auteur ou d\'autres règles ou des lois sur la propriété intellectuelle, s\'il vous plaît envoyer un avis de violation du droit d\'auteur contenant les informations suivantes pour nous:</p>
    <p>Identification de l\'œuvre ou du matériau violé; </p>
    <p>L\'identification du matériel qui est prétendu être atteint, y compris son emplacement, avec suffisamment de détails pour que Treatstock soit capable de trouver et de vérifier son existence;</p>
    <p>Les coordonnées de la partie notifiante (la «partie notificatrice»), y compris le nom, l\'adresse, le numéro de téléphone et l’adresse e-mail;</p>
    <p>Une déclaration détaillant que la partie notifiante a une croyance de bonne foi que le matériel ne soit pas autorisée par le propriétaire de la propriété intellectuelle, son agent ou la loi;</p>
    <p>Une déclaration faite sous peine de parjure, que les renseignements fournis dans l\'avis sont exacts et que la partie notifiante est autorisée à porter plainte au nom du propriétaire du droit d\'auteur;</p>
    <p>Une signature physique ou électronique d\'une personne autorisée à agir au nom du propriétaire de la propriété intellectuelle qui a été prétendument violé.</p>
    <p>Dans tous les cas, si vous ne recevez pas une réponse de Treatstock dans les 10 jours de dépôt d\'une plainte, s\'il vous plaît contacter Treatstock pour confirmer que nous avons reçu votre plainte initiale. Comme vous le savez, les anti-spams rejettent parfois des e-mails importants de tiers inconnus et la livraison du courrier physique peuvent être retardés sans préavis en raison d\'un certain nombre de circonstances imprévues.</p>
    <p>S\'il vous plaît noter que toute personne qui, sciemment et matériellement dénature le produit peut être soumis à la responsabilité des dommages.</p>
    <p>Dans un souci de transparence pour éliminer ou restreindre l\'accès au contenu généré par les utilisateurs, Treatstock peut rendre publics les avis d\'infraction reçus (avec les informations personnelles du contact retirée). Cela peut inclure un avis affiché sur les pages publics du site, entre autres méthodes.</p>
    <p>Après le retrait ou la désactivation de l\'accès à l\'application de matériel ou de contenu après un avis d\'infraction valide, Treatstock informera immédiatement l\'utilisateur responsable du matériel incriminé qu\'il a supprimé ou désactivé l\'accès à la matière.</p>
    <p>Treatstock se réserve le droit, à sa seule discrétion, de résilier immédiatement le compte de tout membre qui fait l\'objet d\'une notification d\'infraction valide.</p>
    <p>Si vous croyez que vous êtes l\'objet illicite d\'une notification d\'infraction, vous pouvez déposer une contre-notification avec Treatstock en fournissant les éléments suivants:</p>
    <p>Les URL des éléments e Treatstock a supprimées ou dont Treatstock a désactivé l\'accès.</p>
    <p>Votre nom, adresse, numéro de téléphone et adresse e-mail.</p>
    <p>Une déclaration que vous consentez à indemniser Treatstock contre toute réclamation d\'infraction au droit d\'auteur qui découlent de la matière.</p>
    <p>La déclaration suivante: «Je jure, sous peine de parjure, que j\'ai une croyance de bonne foi que le matériel a été retiré ou désactivé à la suite d\'une erreur ou d\'une erreur d\'identification de la matière à être supprimé ou désactivé."</p>
    <p>Votre signature.</p>
    <p>Dès réception d\'une contre-notification valide, Treatstock transmettra à la partie notifiante qui a soumis la plainte pour infraction initiale. La partie notifiante original (ou le propriétaire de la propriété intellectuelle qu\'il représente) auront alors dix (10) jours pour nous aviser qu\'il ou elle a déposé une action en justice relative au matériel incriminé. Si Treatstock ne reçoit pas de notification dans les dix (10) jours, nous pouvons restaurer le matériel aux Services.</p>

    <h2>XVI. POLITIQUE COMMUNAUTAIRE</h2>
    <p>Treatstock souhaite offrir un accès aux pages de la communauté à travers le monde pour les technologies nouvelles et en développement tels que la conception 3D et l\'impression 3D. Nous avons créé ces pages communautaires pour aider les participants à porter leurs créations à la vie, inspirer les uns les autres, à partager des connaissances, à échanger des idées et d\'encourager un nouvel intérêt dans la conception 3D et l\'impression. Le but de cette politique est de veiller à ce que tous les membres de la communauté Treatstock vivent une expérience optimale.</p>
    <p>Cette politique décrit l\'activité des membres de la communauté, dans un forum, avis, liste de diffusion, site, correspondance privée, ou des messages publics. Afin d\'éviter les problèmes de communication, il y a quelques règles de base à suivre.</p>
    <ul>
        <li>Soyez prévenant. Les pages communautaires ne peuvent pas être utilisées pour harceler d\'autres membres de la communauté. Les commentaires offensifs ne peuvent pas être affichés sur les pages de la communauté.</li>
        <li>Soyez patient. Les menaces dangereuses, nocives, dangereuses ou toutes violences contre les autres, la promotion de la violence ou des activités illégales ne peuvent pas avoir lieu dans les pages de la communauté.</li>
        <li>Soyez respectueux envers les autres membres. Nous avons tous notre propre opinion, mais le désaccordn’ est pas une excuse pour un mauvais comportement. Il est important de se rappeler qu\'une communauté où les gens se sentent mal à l\'aise ou menacé n’est pas productif.</li>
        <li>Soyez gentil. Respecter la vie privée des autres membres. Vous ne devez pas partager toute information privée ou personnelle qui ne devrait pas être partagé dans les zones publiques du site.</li>
    </ul>

    <h2>XVII. INDEMNITÉ </h2>
    <p>Vous acceptez d\'indemniser et d\'entreprendre pour maintenir Treatstock et ses sociétés affiliées, distributeurs, fournisseurs, entrepreneurs et leurs employés indemnes contre toute responsabilité, perte, coûts, dommages et dépenses (y compris sans limitation des frais juridiques) découlant ou se rapportant à toute réclamation que vous avez enfreint dans les dispositions de ces Termes et Conditions.</p>

    <h2>XVIII. LIMITATION DE RESPONSABILITÉ</h2>
    <p>VOUS RECONNAISSEZ ET ACCEPTEZ QUE TREATSTOCK NE SERA PAS RESPONSABLE DES DOMMAGES DIRECTS, INDIRECTS, SPÉCIAUX, INDIRECTS, COMPENSATOIRES, PUNITIFS OU EXEMPLAIRES, Y COMPRIS MAIS SANS S\'Y LIMITER, LES DOMMAGES POUR PERTE DE REVENUS, DE PROFITS, CONTRATS, DE DONNEES OU AUTRES PERTES INTANGIBLES DE QUELQUE NATURE QUE CE SOIT RÉSULTANT DE L\'UTILISATION OU DE L\'INCAPACITE D\'UTILISER LES SERVICES.</p>
    <p>EN AUCUN CAS TREATSTOCK OU NOS DIRIGEANTS, EMPLOYÉS, PARTENAIRES, FILIALES OU FOURNISSEURS, NE SERONT TENUS RESPONSABLES ENVERS VOUS OU UN TIERS DE TOUTE PERTE DE PROFITS, DOMMAGES SPÉCIAUX, ACCESSOIRES OU INDIRECTS DE QUELQUE NATURE QUE CE SOIT, OU TOUT DOMMAGE OU NON NOUS ONT ETE INFORME DE LA POSSIBILITE DE TELS DOMMAGES, ET LA THÉORIE DE LA RESPONSABILITÉ, PROVENANT DE OU EN RELATION AVEC NOTRE SITE, NOS SERVICES OU CE CONTRAT, PROVENANT CEPENDANT, Y COMPRIS LA NÉGLIGENCE.</p>
     
    <p>DE PLUS, NOUS NE SERONS PAS RESPONSABLE DE QUELQUE MANIERE DES SERVICES OU DES BIENS D’UN TIERS PRESENT SUR NOTRE SITE, Y COMPRIS, SANS LIMITATION LE TRAITEMENT DES COMMANDES.</p>
    <p>CERTAINES JURIDICTIONS INTERDISENT L\'EXCLUSION OU LA LIMITATION DE RESPONSABILITÉ POUR LES DOMMAGES INDIRECTS OU INDIRECTS, LES LIMITATIONS CI-DESSUS PEUVENT NE PAS S’APPLIQUER POUR VOUS.</p>

    <h2>XIX. LOI APPLICABLE</h2>
    <p>Les présentes conditions sont régies par les lois de l\'État du Delaware, États-Unis, sans égard aux conflits de principes de droit de celui-ci qui appliquerait la d’une autre juridiction autre que le Delaware, États-Unis. Tout litige découlant de ces modalités et conditions, y compris les litiges relatifs à la validité de celui-ci, et tous les litiges liés à l\'utilisation des Services ou du site Treatstock, ou concernant les produits commandés et livrés, seront amenés dans le gouvernement fédéral et de l\'État tribunaux situés dans l\'état du Delaware, États-Unis. La Convention des Nations Unies sur la vente internationale de marchandises ne sont pas applicables aux Services ou du site Treatstock, ou concernant les produits commandés et livrés.</p>

    <h2>XX. DIVERS</h2>
    <p>Force majeure. Nous ne serons pas responsables de tout manquement à nos obligations en vertu des présentes lorsque ces résultats d\'échec résultent de toute autre cause hors de notre contrôle, y compris, sans limitation, l\'échec ou la dégradation électronique ou la communication mécanique.</p>
    <p>Affectation. Ces conditions d\'utilisation sont personnels et ne sont pas cessibles, transférables ou sous-licenciable par vous, sauf avec notre autorisation écrite au préalable. Nous pouvons céder, transférer ou déléguer l\'un de nos droits et obligations en vertu des présentes sans le consentement des utilisateurs.</p>
    <p>Agence. Aucune agence, partenariat, coentreprise ou relation de travail est créé à la suite de ces Conditions générales d\'utilisation et aucune des parties n\'a autorité lier l\'autre à tous égards.</p>
    <p>Avis. Sauf indication contraire dans ces durée du service, tous les avis en vertu des Conditions générales d\'utilisation seront par écrit et seront considérés après avoir été dûment reçus, si livré personnellement ou envoyé par courrier recommandé ou certifié, avec accusé de réception; lorsque la réception est confirmée par voie électronique, si elle est transmise par télécopieur ou par e-mail; ou le jour après son envoi, si elle est envoyée pour une livraison le lendemain par un service de livraison de nuit. Des avis électroniques doivent être envoyés à l\'aide du <a href="/site/contact">formulaire de contact</a>.  </p>
    <p>Aucune renonciation. Notre incapacité à appliquer une partie de ces Conditions d\'utilisation ne constitue pas une renonciation de notre droit à faire valoir plus tard toute autre partie de ces conditions d\'utilisation. Renonciation de conformité dans un cas particulier ne signifie pas que nous allons renoncer à son application dans l\'avenir. Pour toute renonciation de l’application de ces conditions d\'utilisation est obligatoire, nous devons vous fournir un avis écrit de cette renonciation par l\'un de nos représentants autorisés.</p>
    <p>Rubriques. La section et le paragraphe titrée « Conditions générales d\'utilisation » sont là par commodité seulement et ne doivent pas affecter leur interprétation.</p>
    <p>Retour d\'information. Puisque nous voulons toujours améliorer nos services, nous vous remercions pour tous les commentaires, suggestions, recommandations et commentaires (collectivement, le "Feedback"). Vous accordez à nous une libre, irrévocable, perpétuelle licence à l\'échelle mondiale à utiliser et autrement incorporer toute rétroaction dans le cadre des Services.</p>
    <p>Utilisez des "cookies". Vous accepterez que lorsque vous visitez le site, des «cookies» peuvent être installé sur le disque dur de votre ordinateur.</p>
    <p>Pas de support ou de maintenance. Vous reconnaissez et acceptez que la Société n\'a aucune obligation de vous fournir toute assistance ou de maintenance en relation avec le site.</p>

    <h2>XXI. COPYRIGHT / INFORMATION DES MARQUES</h2>
    <p>Copyright © 2015, 2016 Treatstock, Inc. Tous droits réservés. Toutes les marques, logos et marques de service ( "Marques") affichés sur les sites sont notre propriété ou la propriété de tiers. Vous n\'êtes pas autorisé à utiliser ces marques sans notre consentement écrit préalable ou le consentement de ce tiers qui peut posséder les marques.</p>

    <h2>XXII. COORDONNÉES</h2>
    <p>Si vous avez des questions concernant les présentes Conditions Générales ou les Services, vous pouvez nous contacter en utilisant le <a href="/site/contact">formulaire de contact</a>.</p>

    <br>

    <h2>Annexe A</h2>

    <h2 class="m-t0">Licence commerciale</h2>

    <h3>1. Définitions</h3>

    <p><strong>Site</strong>: le site proposé par Treatstock au http://www.treatstock.com se réfère au site Treatstock, API, les applications logicielles ou tout moyen approuvé ou utilitaire soit actuellement en existence ou à l\'avenir; le code logiciel et la source utilisée par Treatstock pour fournir de tels services; mises en page de l\'interface utilisateur, dessins, images, textes, articles de la base de connaissances, le programme offre; informations sur le site fournies dans les rapports (tels que les recherches par mots clés populaires); et toute autre propriété intellectuelle protégé par le droit d\'auteur, marques, brevets, publicité, ou tout autre droit de propriété.</p>

    <p><strong>Treatstock</strong>: comprend Treatstock, Inc. et tous les affiliés et partenaires agréés.</p>

    <p><strong>Visiteur</strong>: Utilisateur enregistré ou non qui navigue sur le Site.</p>

    <p><strong>Utilisateur</strong>: une personne physique ou morale qui utilise les services qui sont offerts par le Site.</p>

    <p><strong>Compte</strong>: le dossier d\'inscription de l\'utilisateur sur le Site.</p>

    <p><strong>Modèle 3D</strong>: fichier imprimable 3D fourni par l\'utilisateur.</p>

    <p><strong>Impression 3D</strong>:  modèle 3D transformé en objet physique par une imprimante 3D.</p>

    <p><strong>KIT</strong>: un ensemble de pièces séparées à assembler.</p>

    <p><strong>Auteur</strong>: l\'utilisateur qui fournit un modèle 3D pour Treatstock.</p>

    <p><strong>Fabricant</strong>: l\'utilisateur qui exploite une imprimante 3D et offre des services d\'impression 3D à travers le Site.</p>

    <p><strong>Commande</strong>: Une commande passée par l\'utilisateur via le Site pour tout travail d\'impression 3D avec le fabricant sélectionné pour tourner de l\'auteur Modèle 3D dans le Print 3D lors de la réception du paiement pour l\'auteur, fabricant, service et frais de livraison.</p>

    <p><strong>Client</strong>: tout Utilisateur qui utilise le site pour commander un modèle 3D ou une impression 3D</p>

    <p><strong>Page Produit</strong>: est la page de produit ou de l\'interface qui affiche les actifs Modèle 3D disponible pour les commandes, généralement sur le site www.treatstock.com</p>

    <p><strong>Service de livraison</strong>: services tiers utilisés pour la livraison d\'une impression 3D à partir d\'un fabricant à un utilisateur.</p>

    <p><strong>Contenu de l\'utilisateur</strong>: Modèles 3D, images, vidéos, textes, ou tout autre contenu que l\'utilisateur soumet au Site.</p>

    <p><strong>Auteur Fee</strong>:  le montant total dû à l\'auteur pour l\'utilisation de leur modèle 3D pour une commande</p>

    <p><strong>Impression Fee</strong>: le montant total dû au fabricant pour les services d\'impression, y compris le démarrage et les coûts du matériel de chaque tâche d\'impression individuelle et les coûts du matériau utilisé pour produire la 3D, comme proposé par le fabricant sur le Site.</p>

    <p><strong>Frais de service</strong>: la redevance due à Treatstock en contrepartie des services fournis par Treatstock et les taxes applicables.</p>

    <p><strong>Frais de livraison</strong>: Toute taxe due à toute partie liée aux services concernant la livraison d\'une impression 3D à partir d\'un fabricant à un client.</p>

    <p><strong>Services</strong>: Les services offerts par Treatstock à travers ce site.</p>

    <p><strong>Propriété intellectuelle</strong>: droit d\'auteur, brevet, marque, secret commercial, droit de la publicité, ou tout autre droit de propriété à travers le monde.</p>

    <h3>2. Les Parties</h3>

    <p class="p-l20">2.1. Cet accord de licence vaut entre Auteur, Client, et le fabricant pour achat de tout produit Modèle 3D du site.</p>

    <p class="p-l20">2.2. En achetant toute impression 3D ou modèle 3D publié en vertu du présent Accord de licence (ou un ensemble de modèles 3D tels que des KIT), à partir du site, le client accepte ces conditions de l\'entente de licence.</p>

    <h3>3. Droits Accordée</h3>

    <p class="p-l20">3.1. L’auteur déclare et garantit avoir le droit d\'accorder, une, non exclusive, licence mondiale irrévocable de reproduire, préparer des travaux dérivés, transformer en objets physiques, incorporer dans d\'autres œuvres, et d\'utiliser autrement tout modèle 3D ou KIT sélectionné par un utilisateur à l\'aide du site.</p>

    <p class="p-l20">3.2. L’auteur accorde au fabricant, sélectionné par l\'utilisateur lors de la mise en place d\'une commande, une, non exclusive, licence mondiale irrévocable de copier, préparer des travaux dérivés (y compris, mais sans s\'y limiter à faire des «tranches»), et de reproduire dans la forme physique tout en modèles 3D ou ensembles de modèles 3D (KIT), uniquement aux fins de l\'impression et le traitement d\'un ordre sélectionné.</p>

    <p class="p-l20">3.3. Les changements de modèles 3D avec la possibilité d’imprimer.</p>

    <p class="p-l20">3.4. Le fabricant doit imprimer le montant exact des impressions 3D des modèles décrits dans la commande.</p>

    <p class="p-l20">3.5. Le fabricant doit détruire toutes les pièces défectueuses, ou tentatives infructueuses d’impression des modèles.</p>

    <p class="p-l20">3.6. Le fabricant doit détruire toutes les copies numériques de modèle 3D de l\'auteur inclus dans la commande, y compris tous les travaux dérivés et les journaux de suivi, après avoir terminé ou si la commande est annulée en moins d’un jour.</p>

    <p class="p-l20">3.7. L’auteur accorde au fabricant qui imprime le modèle 3D le droit de prendre des photos et vidéo de l\'impression 3D et les télécharger sur le site afin d\'en assurer la qualité, ou pour améliorer l\'expérience de l\'utilisateur, ou à des fins publicitaires.</p>

    <p class="p-l20">3.8. L’auteur accorde au Client le droit d\'utiliser l\'impression 3D d\'une manière quelconque à leur propre discrétion et à leurs propres risques (y compris mais non limité à un usage commercial, ventes, locations, photo ou vidéo compilations, etc.), sauf si le client ne peut pas reproduire le modèle numérique 3D d\'origine mais sous une autre forme (par exemple, "scan 3D" ou "photo 3D"), cela est interdite.</p>

    <h3>4. Redevances et droits</h3>

    <p class="p-l20">4.1. Après la publication du modèle 3D en conformité avec les présentes Conditions d\'accord de licence, l’auteur spécifie un prix pour un seul élément du modèle 3D ou un KIT.</p>

    <p class="p-l20">4.2. Pour utiliser les droits décrits dans la clause du présent Accord intitulé; «Droits accordés», le client doit payer l\'auteur ainsi que l\'utilisation du Site Treatstock, la somme du prix de l\'article spécifié par l\'auteur et multiplié par le nombre d\'impressions 3D commandés (dans le cas de la KIT - le prix KIT multiplié par la quantité KIT commandé).</p>

    <p class="p-l20">4.3. Le processus de paiement ci-dessus est complété par la Commande Paiement régulier sur le Site. L\'auteur nous promet de ne pas utiliser d\'autres méthodes de paiements, d\'accepter seulement le mode de paiement par l\'intermédiaire du Site.</p>

    <p class="p-l20">4.4. Dans le cas où le prix spécifié par l\'auteur est différent de la monnaie qui est utilisée pour traiter la commande, Treatstock utilisera les taux de change à sa seule discrétion et fixera le prix pour les clients du montant de la commande dans la monnaie correspondante.</p>

    <p class="p-l20">4.5. Les tentatives défectueuses à imprimer ne sont pas payables.</p>

    <h3>5. Résiliation</h3>

    <p class="p-l20">5.1. L’auteur peut résilier de nouveaux accords en supprimant le modèle 3D à partir de sa publication sur le site.</p>

    <p class="p-l20">5.2. La résiliation de nouveaux accords signifie qu\'aucune nouvelle commande avec ce modèle 3D ne peut être faite. Tous droits de commandes déjà effectuées restent après l\'expiration de l\'accord. Par exemple, le fabricant doit terminer l\'impression et le traitement des commandes contenant des modèles qui ont été retirés de la publication, dans le cas où la commande a été rendu avant que le modèle ait été retiré de la publication.</p>

    <p class="p-l20">5.3. Les droits de licence accordés en vertu du présent accord seront immédiatement et automatiquement résiliés sans préavis si vous ne respectez pas les termes et conditions du présent Accord. Sur avis de résiliation, vous devez détruire toutes les copies de modèles 3D.</p>

    <h3>6. Les remboursements</h3>

    <p class="p-l20">6.1. Cet accord concerne les commandes qui ont déjà été faites. Elles sont non-remboursables par l\'initiative de l\'auteur. Un auteur ne peut pas révoquer le droit d\'imprimer la commande.</p>

    <p class="p-l20">6.2. Si le fabricant affirme qu\'il ne peut pas imprimer une commande pour une raison quelconque, ou prouver que, tout en essayant d\'imprimer une commande, le produit est défectueux, Treatstock se réserve le droit d\'annuler la commande. Dans ce cas, l’auteur ne sera pas payé.</p>

    <p class="p-l20">6.3. Remboursement à l\'initiative du client</p>

    <h3>7. Taxes</h3>

    <p class="p-l20">7.1. L’auteur comprend et accepte qu’il soit le seul responsable de la détermination de leurs déclarations d\'impôt applicables et des taxes applicables pour les services qu\'ils fournissent aux utilisateurs via le Site.</p>

    <p class="p-l20">7.2. Treatstock se réserve l\'obligation d\'agir comme agent de retenue fiscale américaine pour les utilisateurs et les entités non américaines. L’auteur peut voir le pourcentage de retenue dans la zone spécifique du compte personnel après le remplissage des informations fiscales.</p>
    <p class="p-l20">7.3 Si vous n\'avez pas fourni les informations fiscales correctes dans ce délai, Treatstock déduira 30% de vos revenus. Il est crucial de se rappeler de soumettre tous les documents nécessaires pour éviter la déduction de 30%. En outre, afin de retirer vos bénéfices de votre compte Tretstock, veuillez vous connecter sur la plateforme PayPal.</p>
    <p class="p-l20">7.4 Si vous êtes un concepteur non américain et ne fournissez pas un W-8BEN, nous sommes tenus en vertu du droit fiscal américain de vous traiter comme une personne des États-Unis et de retenir 28% de l\'impôt sur le revenu et de l\'envoyer à l\'IRS. Cela vaut également pour les personnes qui n’ont pas fournis leur numéro d\'identification du contribuable (TIN) de la manière requise (avec par exemple, des informations manquantes ou incorrectes). Ceci est connu comme "la source de sauvegarde."</p>
    <div class="p-l20">
        <p>7.5 Treatstock agit comme un organisme de règlement des tiers qui est responsable de la déclaration des montants de transaction bruts versés aux bénéficiaires participant à leur réseau. Cette information peut être trouvée <a href="https://www.irs.gov/uac/Third-Party-Network-Transactions-FAQs" target="_blank">ici</a>.</p>
        <p>Si vous êtes un contribuable non-américain, vos redevances de votre revenu américain gagné seront assujetties à une retenue d\'impôt de 30% et vous aurez à fournir le <a href="https://www.irs.gov/uac/Form-W-8BEN,-Certificate-of-Foreign-Status-of-Beneficial-Owner-for-United-States-Tax-Withholding" target="_blank">formulaire W-8BEN</a> à moins que vous remplissiez les documents nécessaires pour demander une exemption complète. Un formulaire distinct (<a href="https://www.irs.gov/uac/About-Form-W9" target="_blank">W-9</a>) est applicable pour les auteurs américains.</p>
        <p>Vous pouvez demander un montant spécifique d\'exonération en fonction de la convention fiscale de votre pays avec les États-Unis.</p>
        <ul>
        <li>Vous pouvez <a href="https://www.irs.gov/Businesses/International-Businesses/United-States-Income-Tax-Treaties---A-to-Z" target="_blank">vérifier quels pays ont une convention fiscale avec les États-Unis ici.</a></li>
        <li>Et vérifier le <a href="https://www.irs.gov/publications/p515/ar02.html" target="_blank">taux de votre pays ici</a> - voir le 2ème et 4ème table et regarder dans la colonne de droite - (. \'Catégorie de revenu 12\'), le taux de retenue à la source des redevances voir sous la rubrique «Droits d\'auteur»</li>
        </ul>
        <p>Il est essentiel de se rappeler que si vous ne soumettez pas tous les documents nécessaires, 30% seront déduits de vos revenus.</p>
    </div>


    <h3>8. Avis</h3>

    <p class="p-l20">8.1. Ceci n\'est pas un accord de propriété.</p>

    <p class="p-l20">8.2. Cette licence est non-transférable.</p>

    <p class="p-l20">8.3. Vous ne pouvez pas louer, céder, distribuer, prêter ou transférer des modèles 3D d’un auteur, ou toute copie ou partie des modèles 3D, sans le consentement écrit préalable de l\'auteur. On vous accorde que les droits expressément énoncés dans le présent Accord, et vous ne pouvez pas utiliser des modèles 3D pour toute autre utilisation.</p>

    <p class="p-l20">8.4. L\'auteur peut publier son/ses modèles 3D pour le coût total de 0 $, mais le droit d\'auteur résidera toujours avec l\'auteur.</p>

    <p class="p-l20">8.5. Si vous n\'êtes pas d\'accord sur les termes, vous n\'êtes pas autorisé à utiliser des modèles 3D.</p>

    <p class="p-l20">8.6. L\'achat des droits pour imprimer des modèles 3D comme prescrit dans le présent accord ne permet pas la revente des pièces numériques Modèles 3D.</p>

    <h3>9. Droits et marques de commerce IP</h3>

    <p>Vous reconnaissez et acceptez que les modèles 3D répertoriés sur le site et ses copies autorisées, et les marques associées avec le site, sont la propriété intellectuelle détenus par leurs propriétaires respectifs. Les modèles 3D ou tous les autres contenus de l\'utilisateur sur le site sont protégés par les lois du droit d\'auteur, y compris mais sans s\'y limiter, par les lois fédérales des États-Unis du droit d\'auteur, droit de la propriété intellectuelle respective État, les dispositions des traités internationaux et les lois applicables dans la juridiction d\'utilisation.</p>

    <h3>10. Garantie</h3>

    <p>Treatstock garantit que le produit imprimé réunira sensiblement les caractéristiques du modèle 3D commandé dans les limites de la technologie actuelle de l\'impression 3D. Treatstock ne garantit pas que tout modèle 3D peut être appliquée à un usage particulier. Pour toute application dans le monde réel, l\'utilisateur doit utiliser les modèles 3D à son / ses propres risques.</p>

    <h3>11. Disclaimer</h3>

    <p>LE SITE ET LES SERVICES SONT FOURNIS DANS LE PARAGRAPHE PRECEDENT «TEL QUEL» ET «TELS QUE DISPONIBLES» SANS GARANTIE D\'AUCUNE SORTE, EXPLICITE OU IMPLICITE. TREATSTOCK NE PEUT PAS GARANTIR LES PERFORMANCES OU LES RÉSULTATS OBTENUS PAR L\'UTILISATION DU SITE. TREATSTOCK EXCLUT TOUTE GARANTIE, EXPLICITE OU IMPLICITE, Y COMPRIS, SANS LIMITATION, TOUTE GARANTIE IMPLICITE DE QUALITÉ MARCHANDE OU D\'ADAPTATION À UN USAGE PARTICULIER OU DE NON-VIOLATION. CERTAINS ÉTATS ET JURIDICTIONS NE PERMETTENT PAS LA LIMITATION DE GARANTIE IMPLICITE, LES LIMITATIONS CI-DESSUS PEUVENT NE PAS VOUS CONCERNES.</p>

    <p>SAUF POUR LA GARANTIE LIMITÉE PRECEDENTE, ET POUR TOUTE GARANTIE QUI NE PEUVENT ETRE LIMITEE OU EXCLUE PAR LA LOI APPLICABLE DANS VOTRE JURIDICTION, TREATSTOCK NE DONNE AUCUNE GARANTIE, CONDITION, DÉCLARATION OU CONDITIONS, EXPLICITE OU IMPLICITE, QUE CE SOIT PAR LA LÉGISLATION EN VIGUEUR, USAGES OU COMME D\'AUTRES QUESTIONS, Y COMPRIS, SANS LIMITATION, DE NON-VIOLATION DES DROITS DE TIERS, TITRE, INTEGRATION, DE QUALITÉ MARCHANDE OU D\'ADAPTATION À UN USAGE PARTICULIER.</p>

    <h3>12. Limitation de responsabilité</h3>

    <p>VOUS RECONNAISSEZ ET ACCEPTEZ QUE TREATSTOCK NE SERA PAS RESPONSABLE DES DOMMAGES DIRECTS, INDIRECTS, SPÉCIAUX, INDIRECTS, COMPENSATOIRES, PUNITIFS OU EXEMPLAIRES, Y COMPRIS MAIS SANS S\'Y LIMITER, LES DOMMAGES POUR PERTE DE REVENUS, DE PROFITS, CONTRATS, DE DONNEES OU AUTRES PERTES INTANGIBLES DE QUELQUE NATURE QUE CE SOIT RÉSULTANT DE L\'UTILISATION OU L\'INCAPACITE D\'UTILISER LES SERVICES.</p>

    <p>EN AUCUN CAS TREATSTOCK OU NOS DIRIGEANTS, EMPLOYÉS, PARTENAIRES, FILIALES OU FOURNISSEURS, NE SERONT TENUS RESPONSABLES ENVERS VOUS OU UN TIERS DE TOUTE PERTE DE PROFITS, DOMMAGES SPÉCIAUX, ACCESSOIRES OU INDIRECTS DE QUELQUE NATURE QUE CE SOIT, OU TOUT DOMMAGE OU NON NOUS ONT ETE INFORME DE LA POSSIBILITE DE TELS DOMMAGES, ET LA THÉORIE DE LA RESPONSABILITÉ, PROVENANT DE OU EN RELATION AVEC NOTRE SITE, NOS SERVICES OU CE CONTRAT, TOUTEFOIS Y COMPRIS PROVENANT DE LA NÉGLIGENCE.</p>

    <p>DE PLUS, NOUS NE SERONS PAS RESPONSABLE DE QUELQUE MANIERE DE TIERS BIENS ET SERVICES OFFERTS PAR NOTRE SITE, Y COMPRIS, SANS LIMITATION LE TRAITEMENT DES ORDRES.</p>

    <p>CERTAINES JURIDICTIONS INTERDISENT L\'EXCLUSION OU LA LIMITATION DE RESPONSABILITÉ POUR LES DOMMAGES INDIRECTS OU INDIRECTS, LES LIMITATIONS CI-DESSUS PEUVENT NE PAS VOUS CONCERNÉS.</p>

    <h3>13. Les changements possibles</h3>

    <p class="p-l20">13.1. Treatstock se réserve le droit d\'apporter des modifications sur le Site, les Services, Accord et les Termes et Conditions à tout moment. Ces accords modifiés seront applicables aux nouveaux achats effectués sur le site après la date de son application.</p>

    <p class="p-l20">13.2. Tout accord modifié entrera en vigueur dès sa publication sur le Site ou par d\'autres moyens raisonnables de communication. Treatstock prendra les efforts nécessaires pour publier une annonce au moins un jour avant toute modification au présent Accord. S\'il vous plaît vérifier l\'accord publié sur ce site régulièrement pour vous assurer que vous êtes au courant de tous les termes régissant le Site et les Services.</p>

    <p class="p-l20">13.3. Si vous êtes d’accord avec les modifications de l’Accord, laisser vos publications sur le site. Si vous n\'êtes pas d\'accord - retirer vos modèles 3D à partir de la publication dans un temps opportun.</p>

    <h3>14. Droit applicable</h3>

    <p>Les présentes conditions sont régies par les lois de l\'État du Delaware, États-Unis, sans égard aux conflits de principes de droit de celui-ci qui appliquerait la loi de toute juridiction autre que le Delaware, États-Unis. Tout litige découlant de ces modalités et conditions, y compris les litiges relatifs à la validité de celui-ci, et tous les litiges liés à l\'utilisation des services Treatstock ou du site ou concernant les produits commandés et livrés, doivent être traduits devant les tribunaux fédéraux et d\'État situé dans l\'État du Delaware, États-Unis. La Convention des Nations Unies sur la vente internationale de marchandises ne sont pas applicables aux services Treatstock ou du site ou concernant les produits commandés et livrés.</p>

    <h3>15. Contact pour information</h3>

    <p>Si vous avez des questions concernant les présentes Conditions Générales ou les Services, vous pouvez nous contacter en envoyant un courriel à
        <script>
            var parts = ["support", "treatstock", "com", "&#46;", "&#64;"];
            var mailAddress = parts[0] + parts[4] + parts[1] + parts[3] + parts[2];
            document.write(\'<a href="mailto:\' + mailAddress + \'">\' + mailAddress + \'</a>\');
        </script>.
    </p>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '10', '5', 'en-US', 'Privacy Policy', '

    <h2>Privacy Statement</h2>

    <p>We know that your privacy is important to you, so it\'s important to us to keep it safe. This Privacy Policy is intended to assist you in making informed decisions when using our Site and any Services provided through it.</p>

    <h2>What Information Do We Collect and Why?</h2>

    <p>Treatstock Inc., its partners, and its affiliates, requires users to provide us with your personal data such as your name, user ID, address, city, state, postal code, telephone number, IP address, and email address in order to provide you with the Treatstock services. When you register on the Site, we ask for your name and email address in order to create an account for you. When you place an order, we will ask for your delivery address to be able to fulfill your order. Your email address or profile image will be used or to enable you to communicate with other users of the Site.</p>

    <p>We also use your information to communicate with you about our services. You may periodically receive emails and other communications from us relating to your account. You must opt in to receive commercial communications and may always opt out of receiving future commercial communications at any time in your user profile page. Also, we track the basic use of our Site (performance measurement) in order to make the Site better for our visitors and accommodate it to their needs. This is strictly statistical data about browsing actions and patterns and does not identify you as an individual.</p>

    <p>In your profile, you can also upload your profile image. Please be advised that your username and profile image will be displayed to the other users on our Website. If you do not want your personal information or photo to be visible to other users of the website, do not upload such information or photo to your profile. You can delete or modify the contents of your profile at any time or select an anonymous image for your comments on the site. You have a choice to disclose details about yourself, such as your profile, pictures and name on the Site. We are not responsible for any personal data you choose to use, as any personal data you share can be read.</p>

    <p>If you publish any 3D model as the Author, or establish a print service as Manufacture, Treatstock needs to know your citizenship, country of residence, and tax status, for tax reporting and for tax withholding purposes on royalties or business income as prescribed in US code cases. Particularly, Treatstock asks that you fill out necessary information for the forms W-8 or W-9, and specify the details for the withdrawal, such as PayPal email.</p>

    <h2>Security</h2>

    <p>We maintain the highest standards of security according to best practices such as firewall user access controls, and secure socket layer (SSL) technology; however the transmission of information via the internet is not completely secure. Any transmission of Information from you to us is at your own risk, we cannot guarantee its absolute security.</p>

    <h2>Information We Share</h2>

    <p>We do not sell, rent or license your personal data to third parties. However, please be informed that we may share your personal data with other businesses (our service providers) for example to enable them to provide their services to us or to you (for instance handling your orders). We require these service providers to protect your personal data adequately. You should be advised that these service providers may be located around the world, therefore you acknowledge that your personal information may be transferred from your home country to third parties located around the world. We do not collect financial information, as that information is collected and stored by our third party payment processors. However, we may from time to time request and receive some of your financial information from our payment processor for the purposes of completing transactions you have initiated through the Services, protecting against or identify possible fraudulent transactions, and otherwise as needed to manage our business. In some cases, we may choose to buy or sell assets. In these types of transactions, user information is typically one of the transferred business assets. Moreover, if we, or substantially all of our assets, were acquired, or if we go out of business or enter bankruptcy, user information would be one of the assets that is transferred or acquired by a third party. You acknowledge that such transfers may occur, and that any acquirer of us or our assets may continue to use your Personal Information as set forth in this policy. We also reserve the right to access, read, preserve, and disclose any information as we reasonably believe is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request, (ii) enforce our Terms and Conditions, including investigation of potential violations hereof, (iii) detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or (v) protect our rights, property or safety, our users and the public. This includes exchanging information with other companies and organizations for fraud protection and spam/malware prevention. Except as set forth in this Privacy Statement, you will be notified when your personal information may be shared with third parties, and will be able to prevent the sharing of this information.</p>

    <a name="cookie"></a>
    <h2>Cookies Policy</h2>

    <p>We collect certain information by automated means when you visit our Site, such as how many users visited our Sites and the pages accessed. By collecting this information, we learn how to best tailor the Sites to our visitors. We collect this information through various means such as "cookies," Google analytics "web beacons" and IP addresses, as explained below.</p>

    <ul>
        <li>Cookies
            <br>
            Like many companies, our Site may use "cookies." Cookies are bits of text that are placed on your computer\'s hard drive when you visit certain websites. We may use cookies to tell us, for example, whether you have visited us before or if you are a new visitor and to help us identify site features in which you may have the greatest interest. Cookies may enhance your online experience by saving your preferences while you are visiting a particular site.</li>

        <li>Google Analytics
            <br>
            Google Analytics, a web analytics service provided by Google, Inc. Google Analytics uses "cookies" to help analyze how users use the Site. The information generated by the cookie about your use of the Site will be transmitted to and stored by Google. Google will use this information to monitor your use of the Site, compiling reports on website activity for website operators and providing other services relating to website activity and internet usage. Google may transfer this information to third parties where required by law, or where such third parties process the information on Google\'s behalf. You may refuse the use of cookies by selecting the appropriate settings on your browser, however please note that if you do this you may not be able to use the full functionality of this website. By using this Website, you consent to the processing of data about you by Google in the manner and for the purposes set out above.</li>

        <li>IP Addresses
            <br>
            An IP address is a unique identifier that certain electronic devices use to identify and communicate with each other on the Internet. When you visit our Sites, we may view the IP address of the device you use to connect to the Internet. We use this information to determine the general physical location of the device and understand from what geographic regions our website visitors come. We also may use this information to enhance our Site and protect it against misuse.</li>
    </ul>

    <h2>Other Websites</h2>

    <p>Our Site may provide links to other websites for your convenience and information. Linked sites may have their own privacy notices or policies, our Policy no longer applies to your internet browsing. Your browsing and interaction on any other website, including websites which are linked to ours is subject to that website’s own rules and policies. Please see our Terms of Use for more information on links to third party websites.</p>

    <h2>Contact Us</h2>

    <p>Should you have questions or concerns with our use of your personal data or you have any comments regarding this Privacy Statement please contact using the <a href="/site/contact">contact form</a>.</p>

    <h2>Changes To The Privacy Statement</h2>

    <p>We reserve the right to change this privacy statement at any time in our sole discretion. If we decide to make changes to this privacy statement, we will publish a new version on the Site. We may also provide notice to you via email. Any such changes will be effective upon publishing the revisions. Therefore we encourage you to check the Site often to read the latest version. Your continued use of the Site following publishing of any changes constitutes your acceptance of such changes.</p>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '11', '5', 'ru', 'Политика Конфиденциальности', '

    <h2>Заявление о конфиденциальности</h2>

    <p>Мы знаем, что ваша конфиденциальность важна для вас, поэтому для нас важно хранить ее в безопасности. Эта политика конфиденциальности предназначена помочь вам принять обоснованные решения при использовании нашего сайта и любых услуги, предоставляемых через него.</p>

    <h2>Какую информацию мы собираем</h2>

    <p>Treatstock Inc., партнеры, и взаимосвязанные организации, собирают с пользователей личные данные, такие как имя, идентификация пользователя, адрес, город, штат, почтовый индекс, номер телефона, IP-адрес и адрес электронной почты для того, чтобы обеспечить Treatstock услугами. При регистрации на Сайте, мы спрашиваем ваше имя и адрес электронной почты для того, чтобы создать аккаунт для вас. Когда вы делаете заказ, мы спрашиваем ваш адрес доставки для  выполния заказа. Ваш адрес электронной почты или фото профиля будет использоваться, чтобы вы могли общаться с другими пользователями Сайта.</p>

    <p>Мы также используем вашу информацию для сообщения вам о наших услугах. Вы можете периодически получать сообщения по электронной почте и другую информацию от нас, относящиеся к вашему аккаунту. Вы должны согласиться, чтобы получать рекламу и всегда можете отказаться от получения коммерческих сообщений в любое время на странице профиля пользователя. Кроме того, мы отслеживаем основное использование нашего сайта (измерения производительности) для того, чтобы сделать сайт лучше для наших посетителей и приспособить ее к вашим предпочтениям. Это строго статистические данные о просмотре действий и моделей и никак не связана с Вами как с личностью.</p>

    <p>В вашем профиле, вы также можете загрузить фотографию. Обратите внимание, что Ваше имя пользователя и изображение профиля будет отображаться для других пользователей на нашем сайте. Если вы не хотите, чтобы ваша личная информация или фотография быля видимой для других пользователей сайта, в таком случае не загружайте такую ​​информацию. Вы можете удалить или изменить содержимое вашего профиля в любое время или выбрать анонимный изображение для ваших комментариев на сайте. У вас есть выбор, если вы хотите предоставить подробную информацию о себе, например, профиль, картинки и  ваше имя на Сайте. Мы не несем ответственности за личные данные, которые вы решили использовать, как и любые личные данные, могут быть прочитанны.</p>

    <p>Если вы опубликоваваете 3D-модель как автор, или создаете службу 3D печати с качестве Производителя, Treatstock спрашивает ваше гражданство, страну проживания, а также налоговый статус, для налоговой отчетности, как это предписано в США. В частности, Treatstock просит вас заполнить необходимую информацию для форм W-8 или W-9, а также уточнить детали для вывода денежных средст, такие как электронная почта PayPal.</p>

    <h2>Безопасность</h2>

    <p>Мы поддерживаем самые высокие стандарты безопасности в соответствии с лучшими практиками, такие как контроль доступа пользователей к межсетевой защите и технология Secure Socket Layer (SSL); однако передача информации через Интернет не является полностью безопасной. Любая передача информации от вас к нам проводится на свой страх и риск, мы не можем гарантировать ее абсолютную безопасность.</p>

    <h2>Информация, которой мы делимся</h2>

    <p>Мы не продаем, не передаем в пользование и не предоставляем вашу информацию третьим лицам. Тем не менее, обращаем Ваше внимание, что мы можем передавать вашу информацию  другим предприятиями (лицам, предоставляющим услуги), чтобы они могли предоставлять свои услуги нам или вам (для обработки ваших заказов, например). Поставщики услуг обязаны обеспечивать безопасность и конфиденциальность личной информации надлежащим образом. Следует иметь в виду, что лица, предоставляющие услуги могут быть расположены по всему миру, поэтому вы допускаете, что ваша личная информация может быть передана из вашей страны третьим лицам, расположенных по всему миру. Мы не собираем финансовую информацию, эта информация собирается и хранится операторами платежных систем. Тем не менее, мы можем время от времени запрашивать и получать некоторую финансовую информацию от операторов платежных систем для того, чтобы завершить транзакцию, которую вы инициировали посредством Услуг, защищая или выявляя возможные мошеннические операции, в прочих случаях по мере необходимости для управления нашим бизнесом. В некоторых случаях мы можем принять решение купить активы какой-либо другой компании или продать наши активы другим компаниям. Информация о пользователях может быть частью такой сделки. Более того, если мы или все наши активы будут уступлены третьему лицу, или если мы прекратим осуществлять коммерческую деятельность или обанкротимся,  информация о пользователях будет передана в рамках сделки новому владельцу. Вы признаете, что такая передача может произойти, и что любой приобретатель нашего бизнеса или наших активов может продолжать использовать ваши персональные данные, как указано в этой политике. </p>

    <p>Мы также оставляем за собой право на доступ, чтение, сохранение и раскрытие любой информации, которые мы обоснованно считаем подлежащими такому раскрытию в силу необходимости: (I) удовлетворения действующих законов, постановлений, требований юридического процесса или правительственного запроса; (ii) соблюдения настоящих Условий, включая расследование потенциальных нарушений; (iii)обнаружения, предотвращения или иного противодействия мошенническим действиям, или для решения вопросов безопасности или технических вопросов; (iv) реагирования на запросы пользователей в службу поддержки; (v)  защитить права, собственность или безопасность наших пользователей и общественности.Это включает в себя обмен информацией с другими компаниями и организациями для защиты от мошенничества и для предупреждения спама/вредоносных программ. За исключением вышеупомянутых случаев, вы будете предварительно уведомлены о возможности передачи вашей личной информации третьим лицам и организациям, и у вас будет возможность предотвратить подобную передачу информации.</p>

    <a name="cookie"></a>
    <h2>Политика COOKIES (куки)</h2>

    <p>Мы собираем определенную информацию, с помощью автоматизированных средств, когда вы посещаете наш сайт, например, сколько пользователей посетили наш сайт и какие страницы  пользователи посещали. Сайт использует куки для того, чтобы Вы получили максимальную отдачу от сайта и так, чтобы мы смогли адаптировать наши услуги под вас. Мы собираем эту информацию с помощью различных средств, таких как "куки", "веб-маяки" и IP-адреса Гугл аналитика (Google Analytics), как описано ниже.</p>

    <ul>
        <li>Файлы «Cookies»
            <br>
            Как многие компании мы используем файлы «cookies» или аналогичные механизмы на нашем веб-сайте. Файлы «cookies» представляют собой небольшие текстовые файлы, которые размещаются на жестком диске Вашего компьютера при посещении определенных веб-сайтов. Мы используем файлы «cookies» для того, чтобы знать, например, посещали ли Вы наш сайт ранее или являетесь новым пользователем и чтобы помочь нам определить, какие возможности на сайте представляют для вас наибольший интерес. Файлы «cookies» могут расширить ваши он-лайн возможности, запомнив ваши предпочтения при посещении определенного сайта.</li>

        <li>Гугл Аналитика
            <br>
            Данный веб-сайт использует сервис аналитики Google Analytics, предоставляемый компанией Google Inc. („Google“). Google Analytics использует для анализа использования веб-сайта так называемые "файлы cookie", это текстовые файлы, сохраняемые на вашем компьютере, чтобы помочь сайту проанализировать, как пользователи используют его. Google обрабатывает эту информацию от имени оператора веб-сайта с целью анализа использования вами веб-сайта, создания отчетов с информацией о его работе, а также с целью предоставления оператору веб-сайта других услуг, связанных с деятельностью в Интернете и использованием веб-сайта. Переданный вашим браузером в рамках работы службы Google Analytics IP-адрес не связывается с другими данными, хранящимися в Google. Вы можете отказаться от использования файлов cookie, выбрав соответствующие настройки в Вашем браузере, однако примите к сведению, что в таком случае вы, вероятно, не сможете пользоваться функциями веб-сайта в полном объеме. Используя этот сайт, Вы соглашаетесь на обработку данных о вас Google в порядке, и целях, изложенных выше. </li>

        <li>IP-адреса
            <br>
            IP адрес представляет собой уникальный идентификатор, который используют устройства для идентификации и соединения друг с другом в Интернет.  Когда вы посещаете наш сайт, мы можем просмотреть IP-адрес устройства, используемого для подключения к Интернету. Мы используем эту информацию, чтобы определить общее физическое местоположение устройства и понять, из каких географических регионов наши посетители сайта приходят. Мы также можем использовать эту информацию для улучшения нашего сайта и защитить его от неправильного использования.</li>
    </ul>

    <h2>Ссылки на другие сайты</h2>

    <p>Наш сайт может содержать ссылки на другие веб-сайты для вашего удобства и информации. Другие сайты могут иметь свои собственные уведомления или политику конфиденциальности, наша политика не будет относится к вашей работы в Интернете. Ваша работа в Интернете и взаимодействие с  другими веб-сайтами, включая веб-сайты, которые связаны с нашим сайтом подчиняется собственным правилам и политике использования. Пожалуйста, ознакомьтесь с нашими Условиями использования для получения дополнительной информации о ссылках на сайты третьих лиц.</p>

    <h2>Как с нами связаться</h2>

    <p>Если у Вас есть вопросы или замечания в отношении использования персональных данных, или у вас появились комментарии относительно Политики конфиденциальности, пожалуйста, свяжитесь с нами с помощью <a href="/site/contact">контактной формы</a>.</p>

    <h2>Уведомление об изменениях</h2>

    <p>Мы сохраняет за собой право вносить изменения в настоящую Политику конфиденциальности в любое время по своему усмотрению. В случае внесения изменений в существующую политику конфиденциальности мы опубликуем новую версию на Сайте. Мы также можем предоставить вам уведомление по электронной почте. Любые такие изменения будут эффективными при публикации изменений. Рекомендуем вам регулярно просматривать эту страницу, чтобы своевременно отслеживать изменения  в политике конфиденциальности. Ваше дальнейшее использование Сайта после публикации любых изменений означает Ваше согласие с такими изменениями.</p>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '12', '5', 'zh-CN', '隐私条款', '

    <h2>隐私概述</h2>

    <p>深知隐私之于用户的重要性，我们全力保护它不受侵犯。此隐私条款将帮助用户在使用网站或服务时做出知情的决策。</p>

    <h2>我们收集哪些信息？为什么？</h2>

    <p>为向客户提供Treatstock服务，Treatstock公司、其合作伙伴及其子公司要求用户提供诸如姓名、用户ID、地址、城市、国家、邮编、电话号码、IP地址和电子邮箱地址等个人数据。在注册时，网站需要用户提供姓名和电子邮箱地址以创建账户。在客户下单后，网站需要客户提供运送地址以完成订单。客户的邮箱地址或头像图片将用于或可用于联络其它网站用户。</p>

    <p>网站使用用户信息于联络服务事宜。网站可能定期通过电子邮件或与账户关联的其它联络方式与用户联络。用户在用户资料页面选择接收商业通信，也可以随时取消。另外，网站追踪用户在网站上基本使用（性能评测）以创建用户友好型网站。此为基于浏览操作和模式的严格的统计数据，将不已客户作为个体识别。</p>

    <p>在用户资料中，用户可以上传头像。请谨记用户名和头像将在网站中展示给其它用户。如果客户不愿个人信息或照片被网站其他用户浏览，请不要上传相关文件。用户可以随时删除或格式化个人资料内容或使用虚拟图片于网站评论。用户自行选择在网站上公开诸如档案、图片或姓名等个人信息。网站不对任何用户选择公开的个人数据负责。</p>

    <p>发布3d模型的设计师或提供打印服务的制造商需要向treatstoc提供国籍、居住国和税收情况，以完成税款申报、版税预扣税和依美国规定的商业所得税。尤其需要提供填写W-8或W-9必需的信息，并具体说明提款细节，例如PayPal邮箱。</p>

    <h2>安全</h2>

    <p>利用诸如用户访问约束防火墙及安全套接层 (SSL)加密技术，网站维持最高等级的安全保护。然而通过网络传输信息并非完全安全，客户对自己传送给网站的信息负全责，网站不保证其绝对安全。</p>

    <h2>共享信息</h2>

    <p>网站不允许客户贩卖、租借、授权个人数据给第三方。然而请知晓，为使其他公司或服务商向网站或用户提供服务（例如处理订单），网站可能与其共享客户个人数据。网站要求这些服务商严格保护客户个人信息。服务商分布于世界各地，因此用户须知自己的信息已从所在国分享于位于世界各地的第三方。网站不收集客户财务信息，然而处理付款第三方将收集并保存之。然而网站会偶尔因正常交易需要或防止、鉴定欺诈性交易需要或其他业务需要向处理付款第三方请求并接收一些用户财务信息。在一些情况下，网站可能购买或出售资产，而用户信息往往被作为公司资产的一部分。如果网站或所有网站资产被收购、遭遇停业或破产，用户信息将被作为公司资产的一部分转移于收购方。用户已知晓信息转移的可能性，并知晓任何收购方或资产将根据此条款继续使用用户个人信息。在有理由相信 (i)需要符合适用的法律、法规、法律程序或政府强制性要求，(ii)需要强制实施条款与协议，包括潜在违约行为调查，(iii) 需要检测、预防、处理欺诈、安全或技术问题，(iv)需要回应用户支持请求，或(v)需要保护网站权力、财产或安全，用户和公众，公司保留访问、阅读、保留或公开任何信息，。这包括与其他预防欺诈和垃圾邮件、恶意邮件的公司或组织交换信息。除此条款中提及的，在个人信息被第三方分享时，用户将被通知，并能够取消分享。</p>

    <a name="cookie"></a>
    <h2>cookie 条款</h2>

    <p>在客户浏览网页时，计算机会自动收集例如访客数量及点击页面等信息。此类信息可供网站做出最佳调整。此类信息往往通过“cookies”，谷歌分析“网络信标”，IP地址等方式收集。具体信息如下：</p>

    <ul>
        <li>Cookies
            <br>
            与其他许多公司相同，网站使用”cookies”。Cookies是在客户浏览特定网站时在硬盘中留下的文本。通过Cookies我们可以知道客户是否浏览过网站，以及客户的兴趣等信息。在客户浏览网站时，Cookies通过保存客户浏览偏好来提高使用体验。</li>

        <li>谷歌分析
            <br>
            谷歌分析是由谷歌公司提供的网站分析服务。谷歌分析通过使用Cookies来分析用户在网站上的行为。cookie信息将被传送并保存于谷歌，谷歌将利用这些信息检查客户使用网站的行为，为网页运行着和其他服务方、活动方、利益方提供使用报告。客户可以在设置中拒绝自己cookies信息被收集，但请注意这样可能造成您不能完全使用网页的所有功能。客户使用网站即表示您同意您的信息因上述的原因被谷歌公司收集。</li>

        <li>IP地址
            <br>
            ip地址是用于区分和联络电子设备的唯一标识符。在您浏览网站时，我们可能浏览您连接上网的ip地址。通过此信息我们可以确定设备所在的地理位置并知晓客户所在位置。我们也可能使用此信息改进网站，并防止其被误用。</li>
    </ul>

    <h2>其他网站</h2>

    <p>网站可能为了您的方便和信息向您提供其他网站的链接。其他网站也许有其自己的隐私声明或条款，本网站将不再适用于您的使用行为。您在其他网站浏览或互动行为将受制于该网站的条款和规定。请移步使用条款页阅读更多关于第三方网站的内容。</p>

    <h2>联系我们</h2>

    <p>如您对个人数据的使用有任何问题或意见，或对此隐私条款有任何评价，请通过“联系我们”页面联系。</p>

    <h2>隐私条款的更改</h2>

    <p>公司保留随时全权酌情更改隐私条款的权力。改动后的新版本将在网站上公布，公司也将发送提醒邮件至客户邮箱。新版本自发布之日起生效。因此公司鼓励客户经常阅读最新版本，客户继续使用网站将被视为同意更改的内容。</p>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '13', '5', 'ja', '個人情報保護方針', '

    <h2>個人情報に関する声明</h2>

    <p>私達はあなたの個人情報はあなたにとって重要であることを知っています、それ故に、それを安全に維持する事は私たちにとって重要です。この個人情報保護方針は、当社のサイトとそれを介して提供されるサービスを使用する際に、あなたが情報に基づいた意思決定を行う事を支援する目的で提供されています。</p>

    <h2>私達が収集する情報の種類と理由。</h2>

    <p>Treatstock社、そのパートナー、およびその関連会社は、Treatstockサービスをあなたに提供するために、あなたのお名前、ユーザーID、住所、市、州、郵便番号、電話番号、IPアドレス、および電子メールアドレスなどの個人データのご提供を必要とします。あなたがサイトに登録する際、私たちはあなたのアカウントを作成するために、あなたのお名前とメールアドレスを必要とします。ご注文の際、要求を満たすために配達先住所を必要とします。あなたのメールアドレスやプロフィール画像はサイトの他のユーザーとの通信を可能にするために使用されます。</p>

    <p>当社はまた、当社のサービスについてあなたと通信するためにあなたの情報を使用します。あなたは、私達から定期的にあなたのアカウントに関する電子メールやその他の通信を受信するかもしれません。あなたは商用通信を受信するにはオプトインする必要があり、ユーザーのプロフィールページからいつでも将来の商用通信の受信からオプトアウトすることができます。私達はまた，私達のサイトの訪問者と彼らの必要に応じてサイトを改善するために、私達のサイトの基本的（業績測定）な使用を追跡します。これは厳密にブラウジング行動やパターンについての統計データであり、個人としてあなたを特定するものではありません。</p>

    <p>また、あなたは自分のプロフィール上に、あなたのプロフィール画像をアップロードすることができます。ユーザー名とプロフィール画像は、当社のウェブサイト上の他のユーザーに表示されますのでご注意ください。あなたの個人情報や写真がウェブサイトの他のユーザーに表示されたくない場合は、自分のプロフィールにそのような情報や写真をアップロードしないで下さい。あなたは、いつでもプロファイルの内容を変更や削除したり、または、サイト上のコメントに匿名画像を選択することが出来ます。あなたは、このサイト上であなたのプロフィール、画像や名前など自分自身についての詳細を開示する選択肢を持っています。あなたが共有する個人データは全て見られることが可能なため、あなたが使用することを選択した任意の個人データについて私たちは責任を負いません。</p>

    <p>あなたが著者として任意の3Dモデルを公開、または製造等の印刷サービスを確立した場合、Treatstockは米国コード例規定の税務申告とロイヤルティや事業所得税の源泉徴収の目的のために、あなたの国籍、居住国、および税状況を知る必要があります。特に、Treatstockはあなたが8-WまたはW-9フォームに必要な情報を記入し、PayPalの電子メールなどの引き出しの詳細を指定する事を必要とします。</p>

    <h2>安全性</h2>

    <p>私たちは、ファイアウォールのユーザーアクセスコントロールやSSL（Secure Socket Layer）技術などのベストプラクティスに従った最高水準のセキュリティを維持していますが、インターネットを介した情報送信は完全に安全ではありません。あなたによる私たちへの任意の情報送信はご自身の責任で、私たちはその絶対的な安全性を保証することはできません。</p>

    <h2>共有される情報</h2>

    <p>私達は、あなたの個人情報を販売、賃貸、または第三者にライセンスしません。しかし、私たちは、私たちに、またはあなたに特定のサービス（例えば、あなたの注文の処理）を提供するのを可能にするあなたの個人データを他の事業（当社のサービスプロバイダ）と共有することが出来るので注意して下さい。私たちは、これらのサービスプロバイダにあなたの個人データを十分に保護する事を要求しています。これらのサービスプロバイダーは世界中に配置されている可能性があるため、あなたは、あなたの個人情報が自国から世界中の第三者に転送される可能性が有るのを認めます。財務情報は当社のサードパーティ決済プロセッサによって収集され、保存されているので、私たちはその情報を収集することはありません。しかし、我々は、あなたがサービスを介して開始した取引を完了させるために、不正取引からの保護や特定の目的のために、または、それ以外の事業を管理するために必要に応じて私たちの支払いプロセッサからあなたの財務情報の一部を随時的に要求し受信することが出来ます。いくつかのケースでは、我々は資産を購入または売却することを選択することができます。これらのタイプの取引では、典型的にユーザ情報は転送されるビジネス資産の一つです。また、当社、または、当社の資産のほぼすべてが取得された場合、または、我々が廃業や倒産した場合、ユーザ情報は第三者に譲渡や取得される資産の一つになりかねません。あなたは、このような転送が発生する可能性、および当社または当社の資産のいずれかの取得先は、このポリシーに記載された個人情報保護方針を使用し続けることができることを認めます。我々はまた、我々が合理的に必要と考える（i）適用する法規、司法手続または政府の要請への援助、（ⅱ）本契約の違反の可能性の調査を含む、これらの利用規約の施行、（iii）検出、防止、またはその他の不正行為、セキュリティまたは技術的な問題に対処（ⅳ）ユーザーのサポート要請に応える、または（v）我々の、ユーザーや公共の権利、財産、の安全を守るなどといった場合はアクセス、読み取り、保存、および任意の情報を開示する権利を留保しますこれには不正防止対策とスパム/不正プログラム予防のために他の企業や団体との情報交換が含まれています。この個人情報保護方針に記載されている場合を除き、あなたは、あなたの個人情報が第三者と共有される場合は通知され、その情報の共有を防止することができます。</p>

    <a name="cookie"></a>
    <h2>クッキー方針</h2>

    <p>あなたが私たちのサイトを訪問の際、自動化された手段によって訪問者数やアクセスしたページなど、特定の情報を収集します。この情報を収集することにより、我々はサイトをどのように訪問者にとって最適化すれば良いのかを学びます。「クッキー」、Google Analyticsの「ウェブビーコン」やIPアドレスなどの、以下に説明される様々な手段を通じてこの情報を収集します。</p>

    <ul>
        <li>クッキー
            <br>
            多くの企業と同様に、私たちのサイトは、「クッキー」を使用する場合があります。クッキーは、あなたが特定のウェブサイトを訪問したときに、あなたのコンピュータのハードドライブ上に配置されるテキストです。私達はクッキーを、例えばあなたが新しい訪問者かどうかなどを判断し、あなたが最大の関心を持っている可能性のあるサイトの機能を識別するのに役立ちます。クッキーは、あなたが特定のサイトを訪問している間、あなたの好みを保存することで、ユーザーのオンライン体験を向上させることができます。</li>

        <li>Google Analytics
            <br>
            株式会社Googleが提供するウェブ解析サービス、Google Analyticsは、ユーザーがサイトをどのように使用するかを分析するのに「クッキー」を使用しています。クッキーによって生成されるサイトの使用情報はGoogleに送信され、保存されます。Googleは、あなたのサイトの使用を監視するためにこの情報を使用し、ウェブサイト運営者にウェブサイトの活動に関するレポートをコンパイルし、ウェブサイトの活動とインターネット利用に関連する他のサービスを提供します。Googleは、法律によって要求される第三者に、または、そのような第三者がGoogleに代わって情報を処理する場所にこの情報を転送することができる。ご使用のブラウザで適切な設定を選択することにより、クッキーの使用を拒否することができますが、これを行う場合は、このウェブサイトのすべての機能を利用することができない場合がありますのでご注意ください。このウェブサイトを使用することで、あなたはこのの方法で上述した目的のためにGoogleがあなたについてのデータの処理に同意するものとします。</li>

        <li>IP アドレス
            <br>
            IPアドレスは、特定の電子機器が、インターネット上で互いを識別し通信するために使用する特異の識別子です。あなたが私たちのサイトを訪問するとき、我々はあなたがインターネットに接続するために使用したデバイスのIPアドレスを見ることができます。我々は、この情報をデバイスの一般的な物理的な位置を特定し、当社のウェブサイトの訪問者がどの地域から来ているのかを理解するために使用します。また、この情報を当社のサイトを強化し、不正使用から保護するために使用することもあります。</li>
    </ul>

    <h2>その他のウェブサイト</h2>

    <p>当サイトは、あなたの利便性のために他のウェブサイトへのリンクを提供する場合があります。リンク先のサイトは独自のプライバシー通知または方針を有している場合があり、私たちの方針は、もはやあなたのインターネットブラウジングに適用されません。私たちにリンクされているウェブサイトを含む他のウェブサイト上でのあなたのブラウジングと相互作用は、そのウェブサイトの独自の規則や方針に従うものとします。第三者のウェブサイトへのリンクについての詳細は、利用規約をご覧ください。</p>

    <h2>お問い合わせ</h2>

    <p>当社によるあなたの個人データの利用に関する質問や懸念、または、このプライバシー関する声明についてコメントがありましたら <a href="/site/contact">お問い合わせフォームを使ってご連絡ください</a> 。</p>

    <h2>プライバシーに関する声明の変更</h2>

    <p>当社は、独自の裁量でいつでもプライバシーに関する声明を変更する権利を有しています。私達は、このプライバシーに関する声明を変更することを決定した場合、サイト上で新しいバージョンを公開します。また、電子メールを通して通知を提供する場合があります。そのような変更は、改訂の公開時に有効となります。それ故に我々は、最新バージョンを読むためにサイトを時々チェックすることをお勧めします。あなたによる変更後のサイトの継続使用は、あなたが変更を受諾したことを構成します。</p>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '14', '5', 'fr', 'Politique de confidentialité', '   <h2>Déclaration de confidentialité</h2>

    <p>Nous savons à quel point le respect de la vie privée vous importe, c’est pourquoi nous y faisons particulièrement attention. Cette politique de confidentialité est là pour vous aider à prendre des décisions éclairées lorsque vous utilisez notre Site et tous les Services qui y sont proposés.</p>

    <h2>Les informations collectées</h2>

    <p>Treatstock Inc., ses partenaires et ses filiales, requièrent de vous, ses utilisateurs, de nous fournir des données personnelles telles que votre nom, identifiant, adresse, ville, état, code postal, numéro de téléphone, adresse IP et adresse e-mail afin de vous fournir les services de Treatstock. Lorsque vous vous enregistrez sur le Site, nous vous demandons votre nom et votre adresse e-mail afin de vous créer un compte. Lorsque vous placerez une commande, nous vous demanderons une adresse de livraison afin de compléter votre commande. Votre adresse e-mail ou votre image de profil sera utilisée pour que vous puissiez communiquer avec les autres utilisateurs du Site.</p>

    <p>Nous utilisons également vos informations personnelles pour communiquer avec vous à propos de notre activité. Il est possible que vous receviez de manière périodique des e-mails ainsi que d’autres messages de notre part à propos de votre compte. Vous ne recevrez nos offres commerciales que si vous y adhérez, et vous pourrez vous rétracter de cette liste de diffusion à tout moment. Nous effectuons également un suivi de l’utilisation standard de notre Site (mesures de performance) afin de l’améliorer en fonction des besoins des visiteurs. Ces données sont strictement statistiques et concernent les actions de navigation et les habitudes des utilisateurs, cela ne vous identifie en aucun cas en tant qu’individu.</p>

    <p>Dans votre profil, vous pouvez aussi télécharger votre propre image de profil. N’oubliez pas que votre nom d’utilisateur et votre image de profil seront accessibles aux autres utilisateurs de notre Site. Si vous ne souhaitez pas que vos informations personnelles et/ou votre photo soient visibles par les autres utilisateurs du site, il est préférable que vous choisissiez de ne pas utiliser de telles informations et/ou images. Vous pouvez supprimer ou modifier les éléments de votre profil quand vous le souhaitez, ou vous pouvez choisir une image neutre qui sera utilisée lorsque vous publierez un commentaire sur le site. Vous pouvez choisir les éléments vous concernant que vous rendez accessibles, tels que votre profil, photo et pseudonyme sur le Site. Nous ne sommes pas responsables des données personnelles que vous choisissez d’utiliser, puisque toute donnée personnelle que vous partagez est visible.</p>

    <p>Si vous publiez un modèle 3D comme Auteur, ou démarrez un service d’impression en tant que Producteur, Treatstock a besoin d’informations complémentaires, comme votre nationalité, votre pays de résidence, votre statut fiscal pour votre déclaration de revenus et vos retenues fiscales concernant le paiement des droits et le revenu commercial, comme prévu par le Code US. En particulier, Treatstock demande à ce que vous remplissiez le formulaire W-8 ou W-9 ainsi que les éléments concernant les retraits d’argent, comme par exemple votre adresse e-mail utilisée pour PayPal.</p>

    <h2>Sécurité</h2>

    <p>Nous nous conformons aux plus hauts standards de sécurité conseillés comme étant les meilleures pratiques, tels qu’un firewall pour contrôler les accès, le système de sécurité « secure socket layer ou SSL ». Cependant, la transmission d’informations sur internet ne peut jamais être entièrement sécurisée. Toute transmission d’information vous concernant se fait à vos risques, nous ne pouvons pas garantir sa confidentialité absolue.</p>

    <h2>Les informations partagées</h2>

    <p>Nous ne vendons, ne louons ni n’accordons de licence à des tierces parties en ce qui concerne vos données personnelles. Cependant, sachez que nous pouvons partager vos données personnelles avec d’autres sociétés (nos fournisseurs) afin que, par exemple, ils puissent nous ou vous fournir un service (comme la gestion de vos commandes). Nous exigeons de ces fournisseurs qu’ils protègent vos données personnelles de manière adéquate. Nous tenons à vous informer que ces fournisseurs peuvent être implantés partout dans le monde, ce qui, par conséquent, implique que vos données personnelles peuvent transiter dans un pays autre que votre pays de résidence vers des tierces parties implantées partout dans le monde. Nous ne conservons pas vos données financières, puisque cette information est recueillie et conservée par les organismes de paiement. Cependant, il peut ponctuellement nous arriver de demander (et de recevoir) aux organismes de paiement vos données financières dans le but de compléter des transactions que vous auriez commencé en utilisant nos Services, pour des raisons de protection ou d’identification en cas de fraude, ou encore en cas besoin dans le cadre de la gestion de notre entreprise. Dans quelques cas particuliers, nous pourrions choisir d’acheter ou de vendre des parts de la société. Dans le cadre de ce genre de transactions, les informations concernant les utilisateurs font généralement partie des parts transférées. De plus, si nous, ou la majeure partie de nos parts, étaient acquises, ou si nous cessions notre activité ou si nous étions rachetés par une tierce partie, les informations concernant nos utilisateurs feraient partie des pars transférées ou acquises par une tierce partie. Par la présente, vous reconnaissez qu’un tel transfert puisse avoir lieu, et que, quel que soit l’acquéreur de la société ou de nos parts, celui-ci puisse continuer à utiliser vos Informations personnelles, conformément à ces conditions générales. Par ailleurs, nous nous réservons également le droit d’accéder, de lire, de conserver et de divulguer toute information que nous jugerions nécessaires afin de (i) répondre à toute loi et réglementation applicables ainsi qu’à toute ordonnance ou réquisition judiciaire, (ii) appliquer nos Conditions d’Utilisation, y compris l’investigation potentielle de toute violation de celles-ci, (iii) détecter, prévenir ou encore traiter des activités frauduleuses, des atteintes à la sécurité ou tout problème d’ordre technique, (iv) répondre aux demandes d’aide de la part des utilisateurs ou (v) protéger les droits, les biens ou la sécurité de Treatstock, de nos utilisateurs et du public. Ceci inclut l’échange d’informations avec d’autres sociétés et organisations à des fins de protection contre les fraudes ainsi que les virus et les pourriels. À l’exception de ce qui est prévu dans cette politique de confidentialité, vous serez notifié lorsque vos informations personnelles sont susceptibles d’être partagées avec des tierces parties et vous serez ainsi en mesure d’empêcher la transmission de ces informations.</p>

    <a name="cookie"></a>
    <h2>Les Cookies</h2>

    <p>Nous recueillons automatiquement certaines informations lorsque vous visitez notre Site, comme par exemple le nombre de visiteurs ainsi que les pages visitées. Cette collecte d’informations nous permet de mieux adapter notre Site à nos visiteurs. Nous recueillons ces informations par plusieurs biais, dont les cookies, des balises Google Analytics et les adresses IP, comme expliqué ci-dessous.</p>

    <ul>
        <li>Les cookies
            <br>
            Comme la majeure partie des sociétés, notre Site peut utiliser des “cookies”. Les cookies sont des petits fichiers qui s’enregistrent sur votre disque dur lorsque vous naviguez sur certains sites internet. Nous pouvons utiliser des cookies afin de savoir, si, par exemple, vous avez déjà visité notre site dans le passé ou si vous êtes un nouveau visiteur et quelles sont les fonctions qui semblent vous intéresser le plus. Les cookies peuvent vous permettre d’améliorer votre expérience en ligne en vous permettant de sauvegarder vos préférences lors d’une visite d’un site spécifique.</li>

        <li>Google Analytics
            <br>
            Google Analytics est un service d’analyse internet fourni par Google, Inc. Google Analytics utilise des cookies afin d’analyser la manière dont les visiteurs utilisent le Site. L’information générée par les cookies concernant votre utilisation du Site sera transmise à et enregistrée par Google. Google utilisera cette information pour suivre votre utilisation du Site, générer des rapports synthétiques sur l’activité du site internet pour ses exploitants, et pour fournir d’autres services liés à l’activité du site internet et de l’utilisation d’internet. Google peut transmettre cette information à des tierces parties dans les cas où la loi l’exige et dans la mesure où une tierce partie traite ces données à la demande de Google. Vous êtes en droit de refuser l’utilisation des cookies en paramétrant votre navigateur ; cependant, sachez que, dans ce cas, vous ne seriez pas en mesure d’utiliser pleinement les fonctionnalités de ce site internet. En utilisant ce Site internet, vous consentez expressément au traitement de vos données nominatives par Google dans les conditions et pour les finalités décrites ci-dessus.</li>

        <li>Adresse IP
            <br>
            Une adresse IP est un identifiant unique que certains appareils électroniques utilisent pour s’identifier et communiquer entre eux sur internet. Lorsque vous visiter nos Sites, nous pouvons avoir accès à l’adresse IP de l’appareil que vous utilisez pour vous connecter à internet. Nous nous appuyons sur cette information pour déterminer la localisation générale de cet appareil et comprendre de quelles régions nos utilisateurs proviennent. Il est également possible que nous utilisions cette information afin d’améliorer notre Site et pour le protéger contre les abus.</li>
    </ul>

    <h2>Autres sites internet</h2>

    <p>Notre Site est susceptible de fournir des liens à d’autres sites internet à titre informatif et pour votre commodité. Ces sites peuvent avoir leur propres avis ou politiques de confidentialités, et notre politique de confidentialité ne s’appliquera pas sur cette partie de votre navigation internet. Votre navigation et vos interactions sur quelque autre site que le nôtre, y compris des sites internet liés au nôtre, sont dans ce cas sujettes aux règles et politiques d’utilisation relatives à ces domaines. Veuillez consultez nos conditions d’utilisation pour plus d’informations concernant les liens vers des sites internet tierces.</p>

    <h2>Contact</h2>

    <p>Pour toute question ou commentaire relatifs à notre utilisation de vos données personnelles ou à notre politique de confidentialité, vous pouvez utiliser notre formulaire de <a href="/site/contact">contact</a>.</p>

    <h2>Modifications de la Politique de Confidentialité</h2>

    <p>Nous nous réservons le droit de modifier à tout moment la présente déclaration. Si nous décidons de modifier notre politique de confidentialité, nous en publierons une nouvelle version sur le Site. Il est possible que nous vous envoyions une notification par e-mail à ce sujet. Tous les changements effectués seront valables dès publication de la nouvelle version. C’est pourquoi nous vous encourageons à vérifier le site régulièrement afin de lire la version la plus récente. Votre utilisation du Site suite à la publication de modifications à ces conditions indique que vous consentez aux changements apportés.</p>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '15', '6', 'en-US', 'PURCHASE PROTECTION AND RETURN POLICY', '
        <ol style="list-style-type: upper-roman;">
            <li>
                <p>
                    Treatstock allows the Customer to cancel their order and return money in full before the 3D Print has been started. After the 3D Print has been started, the Customer may not cancel their Order.
                </p>
            </li>
            <li>
                <p>
                    Since the product is custom made, we will refund the money only in the case of fault or defect of the 3D Print or in case of non-delivery of the 3D Print within a reasonable time.
                </p>
            </li>
            <li>
                <p>
                    Treatstock reserves the rights to delay payment to the Author and the Manufacturer until the Customer confirms an acceptable quality of the 3D Print.
                </p>
            </li>
            <li>
                <p>
                    If the Customer has not registered a complaint with Treatsock within 14 days after purchase, Treatstock assumes that quality is acceptable. We do not accept claims after 14 days after delivery.
                </p>
            </li>
            <li>
                <p>
                    After receiving the 3D Print and prior to using it, the Customer must ensure its quality. In case the quality does not satisfy set requirements, the User must immediately let us know and attach photos of the 3D Print and original packaging. Treatstock will not refund any payments for 3D Prints that have been used. The User shall be ready to return the 3D Print together with original packaging if we ask to do so. The Customer is responsible for returning the 3D Print, including any shipping costs.
                </p>
            </li>
            <li>
                <p>
                    Upon the receipt of a complaint from the Customer, Treatstock may terminate the Order and refund money to the Customer at its sole discretion. Alternatively, Treatstock may request the Manufacturer to print and deliver the 3D Print once more at their own cost, or use another solution to resolve the problem.
                </p>
            </li>
            <li>
                <p>
                    Treatstock reserves the rights to seek indemnification for damages from any party who violated this agreement.
                </p>
            </li>
            <li>
                <p>
                    Treatstock is not responsible for any broken or unprintable files that are ordered for print through affiliate websites, and for unpublished files that have not been checked by our moderation team. Once a Manufacturer begins to print a broken or unprintable file, the customer can no longer request a refund.
                </p>
            </li>
        </ol>' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '16', '6', 'ru', 'PURCHASE PROTECTION AND RETURN POLICY', '   <ol style="list-style-type: upper-roman;">
            <li>
                <p>
                    Treatstock позволяет клиенту отменить свой заказ и вернуть деньги в полном объеме до начала печати 3D изделия. После начала печати 3D изделия, Клиент не может отменить своего заказа.
                </p>
            </li>
            <li>
                <p>
                    Поскольку изделие изготовленно по заказу, мы возвратим деньги только в случае неисправности или дефекта 3D печати или в случае непоставки изделия в течение разумного периода времени.
                </p>
            </li>
            <li>
                <p>
                    Treatstock оставляет за собой право отсрочить выплату дизайнеру и производителю, пока клиент не подтвердит приемлемое качество 3D печати.
                </p>
            </li>
            <li>
                <p>
                    Если Клиент не зарегистрировал претензию в течение 30 дней после покупки, Treatstock предполагает, что качество является приемлемым. Мы не принимаем претензий по истечении 30 дней после доставки.
                </p>
            </li>
            <li>
                <p>
                    После получения 3D изделия и до его использования, Клиент должен обеспечить его качество. В случае, если качество не удовлетворяет установленным требованиям, пользователь должен немедленно сообщить нам об этом и приложить фотографии 3D изделия в оригинальной упаковке. Treatstock не будет возмещать никакие средства за 3D изделия, которые были использованы. Пользователь должен быть готов вернуть 3D изделие вместе с оригинальной упаковке, если возникнет необходимость. Клиент несет ответственность за возвращение 3D изделия, включая любые транспортные расходы.
                </p>
            </li>
            <li>
                <p>
                    После получения претензии от клиента, Treatstock может прекратить заказ и возвратить денеги Клиенту, по своему усмотрению. В качестве альтернативы, Treatstock может просить сервис 3D печати напечатать 3D изделие еще один раз за свой счет, или использовать другое решение, чтобы решить эту проблему.
                </p>
            </li>
            <li>
                <p>
                    Treatstock оставляет за собой право добиваться компенсации за ущерб от любого лица, которое нарушило это соглашение.
                </p>
            </li>
        </ol>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '17', '6', 'zh-CN', 'PURCHASE PROTECTION AND RETURN POLICY', ' <ol style="list-style-type: upper-roman;">
            <li>
                <p>
                    在订单开始3D打印之前，Treatstock允许客户取消订单并全款退还。一旦订单开始3D打印，客户将不能取消订单。
                </p>
            </li>
            <li>
                <p>
                    若是定制产品，公司只有在3D打印故障或出错的情况下或在超出合理时间未运送3D打印成品的情况下进行退款。
                </p>
            </li>
            <li>
                <p>
                    Treatstock有权延迟支付作者和制造商酬金直至客户确认满意3D打印成品。
                </p>
            </li>
            <li>
                <p>
                    若在购买的30天内，客户未向Treatstock投诉，公司默认客户为满意3D打印成品。公司不接受在交货30天后的索赔。
                </p>
            </li>
            <li>
                <p>
                    在收到3D打印成品和使用之前，客户需确认满意其质量符合设置。若客户认为产品质量不达标，需立即告知公司，并提交3D打印成品及原包装照片。若3D打印成品已被使用，Treatstock将不退还任何已付款项。若有需要，用户需做好将原包装与3D打印成品一同寄回公司的准备。客户将对退回的3D打印成品负责，并自行支付邮费。
                </p>
            </li>
            <li>
                <p>
                    在收到客户投诉后，Treatstock全权判断是否中止订单并退还已付款项。Treatstock可能要求制造商重新制作并运送3D打印成品或使用其他解决方法。
                </p>
            </li>
            <li>
                <p>
                    对于违反此条约的任何一方，Treatstock保留追究赔偿的权利。
                </p>
            </li>
        </ol>
' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '18', '6', 'ja', 'PURCHASE PROTECTION AND RETURN POLICY', ' <ol style="list-style-type: upper-roman;">
            <li>
                <p>
                    Treatstockでは、顧客は3D印刷が開始される前に注文をキャンセルし、全額を返金する事が出来ます。3D印刷が開始された後では、お客様は注文をキャンセルする事は出来ません。
                </p>
            </li>
            <li>
                <p>
                    製品はカスタムメイドされているので、我々は3Dプリントの不具合や欠陥の場合、または3Dプリントが合理的な期間内に届けられなかった場合のみに返金させて頂きます。
                </p>
            </li>
            <li>
                <p>
                    Treatstockは、お客様が3Dプリントの合格品質を確認するまで著者およびメーカーへの支払いを遅らせる権利を所有します。
                </p>
            </li>
            <li>
                <p>
                    お客様が購入後30日以内にTreatsockに苦情を申し立てない場合は、Treatstockは、品質が受け入れ可能と判断します。私たちは、お届けから30日後のクレームは受け付けておりません。
                </p>
            </li>
            <li>
                <p>
                    お客様は3Dプリントを受け取った後、それを使いはじめる前に品質を確認する必要があります。品質が要件を満たさない場合には、ユーザーはすぐに私たちにお知らせし、3Dプリントとオリジナル包装の写真を添付しなければなりません。Treatstockは、使用された3Dプリントのお支払いの返金は致しません。私たちがそう依頼する場合、ユーザーは元の包装と一緒に3Dプリントを返却する必要があります。お客様は、輸送費を含め、3Dプリントを返却する責任があります。
                </p>
            </li>
            <li>
                <p>
                    Treatstockはお客様からの苦情を受けて、独自の裁量で注文を終了し、お客様に返金する事が出来ます。また、Treatstockは問題を解決するために、メーカーに自費 でもう一度3Dプリントを印刷してお届けするように要求するか、別の解決策を使用する事が出来ます。
                </p>
            </li>
            <li>
                <p>
                    Treatstockは、本契約に違反した当事者からの損害について賠償を求める権利を所有します。
                </p>
            </li>
            <li>
                <p>
                    Treatstock is not responsible for any broken or unprintable files that are ordered for print through affiliate websites, and for unpublished files that have not been checked by our moderation team. Once a Manufacturer begins to print a broken or unprintable file, the customer can no longer request a refund.
                </p>
            </li>
        </ol>' );
INSERT INTO `static_page_intl`(`id`,`static_page_id`,`lang_iso`,`title`,`content`) VALUES ( '19', '6', 'fr', 'PURCHASE PROTECTION AND RETURN POLICY', ' <ol style="list-style-type: upper-roman;">
            <li>
                <p>
                    Treatstock autorise le Client à annuler sa commande et s’engage à lui rembourser la totalité de son achat avant le début de l’impression en 3D. Une fois que l’impression 3D a commencé, le Client ne peut plus annuler sa Commande
                </p>
            </li>
            <li>
                <p>
                    Une fois le produit créé sur mesure, nous ne serons en mesure d’effectuer un remboursement que si l’Impression 3D présente un défaut ou un dommage, ou en cas de non-livraison de l’Impression 3D dans un délai raisonnable.
                </p>
            </li>
            <li>
                <p>
                    Treatstock se réserve le droit de décaler le paiement à un Auteur et un Producteur jusqu’à ce que le Client confirme la qualité de l’Impression 3D.
                </p>
            </li>
            <li>
                <p>
                    Si le Client n’a pas déposé de plainte auprès de Treatstock dans les 30 jours suivant son achat, Treatstock se réserve le droit d’assumer que la qualité de l’impression 3D est acceptable. Nous n’acceptons aucune plainte passé ce délai de 30 jours après la livraison.
                </p>
            </li>
            <li>
                <p>
                    Après réception de l’Impression 3D et avant toute utilisation, le Client doit s’assurer de sa qualité. Dans le cas où la qualité ne correspondrait pas aux exigences, l’Utilisateur devra immédiatement nous en notifier et nous envoyer des photos de l’Impression 3D ainsi que de l’emballage d’origine. Treatstock ne sera pas en mesure d’effectuer un remboursement pour des Impressions 3D ayant été utilisées. L’Utilisateur doit être en mesure de renvoyer l’Impression 3D dans son emballage d’origine si nous le demandons. L’emballage et les frais d’expédition en cas de retour de l’Impression 3D sont à la charge du Client.
                </p>
            </li>
            <li>
                <p>
                    Suite à la réception d’une plainte déposée par un Client, Treatstock peut, librement et à sa seule discrétion, choisir d’annuler la commande et de rembourser le Client. Il est également possible que Treatstock requière du Producteur une nouvelle impression 3D ainsi qu’une nouvelle expédition à ses frais, ou choisisse une autre solution afin de résoudre le problème.
                </p>
            </li>
            <li>
                <p>
                    Treatstock se réserve le droit de demander une indemnisation pour tout dommage à quiconque des parties ayant porté atteinte à ces conditions d’utilisation.
                </p>
            </li>
            <li>
                <p>
                    Treatstock is not responsible for any broken or unprintable files that are ordered for print through affiliate websites, and for unpublished files that have not been checked by our moderation team. Once a Manufacturer begins to print a broken or unprintable file, the customer can no longer request a refund.
                </p>
            </li>
        </ol>
' );

