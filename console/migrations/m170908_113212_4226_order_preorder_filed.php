<?php

use yii\db\Migration;

class m170908_113212_4226_order_preorder_filed extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `store_order` 
            ADD COLUMN `preorder_id` INT(11) NULL AFTER `is_dispute_open`,
            ADD INDEX `fk_store_order_8_idx` (`preorder_id` ASC);
            ALTER TABLE `store_order` 
            ADD CONSTRAINT `fk_store_order_8`
              FOREIGN KEY (`preorder_id`)
              REFERENCES `cnc_preorder` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
    }

    public function safeDown()
    {
        $this->dropColumn("store_order", "preorder_id");
        return true;
    }
}
