<?php

use yii\db\Schema;
use yii\db\Migration;

class m150622_233246_geo_region_dump extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $path = realpath(dirname(__DIR__) . '/../db/');
        $sql = file_get_contents($path . '/geo_region.sql');
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->truncateTable('{{%geo_region}}');
        return 0;
    }
}
