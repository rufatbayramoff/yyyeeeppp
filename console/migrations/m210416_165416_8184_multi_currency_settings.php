<?php

use yii\db\Migration;

/**
 * Class m210416_165416_8184_multi_currency_settings
 */
class m210416_165416_8184_multi_currency_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('system_setting', ['key' => 'bank_details']);
        $this->delete('system_setting', ['key' => 'payment_details']);

        $this->insert('system_setting', [
            'group_id'   => 10,
            'key'        => 'bank_details',
            'value'      => "json",
            'json'       => json_encode([
                'USD' => "Bank of America\nAccount number: 325059041986\nRouting number: 121000358 (paper & electronic), 026009593 (wires)",
                'EUR' => "IBAN: BE97 9671 1274 1749\nSWIFT/BIC: TRWIBEB1XXX"]),
            'created_at' => '2020-04-16 01:01:01',
            'is_active'  => 1,
        ]);
        $this->insert('system_setting', [
            'group_id'   => 10,
            'key'        => 'payment_details',
            'value'      => "json",
            'json'       => json_encode([
                'USD' => "Treatstock Inc\nAddress 40 East Main Street, Suite 900\nNewark, DE 19711",
                'EUR' => "Treatstock Inc\nAddress: Avenue Louise 54, Room S52\nBrussels, 1050, Belgium"]),
            'created_at' => '2020-04-16 01:01:01',
            'is_active'  => 1,
        ]);
        $this->execute('ALTER TABLE `instant_payment` ADD CONSTRAINT `fk_instant_payment_currency` FOREIGN KEY (`currency`) REFERENCES `payment_currency`(`currency_iso`) ON DELETE RESTRICT ON UPDATE RESTRICT;');
        $this->execute("ALTER TABLE `preorder` ADD `currency` CHAR(5) NOT NULL DEFAULT 'USD' AFTER `budget`");
        $this->execute('ALTER TABLE `preorder` ADD CONSTRAINT `fk_preorder_currency` FOREIGN KEY (`currency`) REFERENCES `payment_currency`(`currency_iso`) ON DELETE RESTRICT ON UPDATE RESTRICT;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
