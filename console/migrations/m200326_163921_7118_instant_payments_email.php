<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200326_163921_7118_instant_payments_email
 */
class m200326_163921_7118_instant_payments_email extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(
            'email_template',
            [
                'code'          => 'newInstantPayment',
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => "You instant payment received",
                'description'   => 'Params: companyName, fromUser, link',
                'updated_at'    => dbexpr('NOW()'),
                'template_html' => "Hi %companyName%,

You’ve received a new Instant payment from \"%fromUser%\". To approve instant payment, click <a href=\"%link%\">here</a>.

Best,
Treatstock",
                'template_text' => ""
            ]
        );
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}