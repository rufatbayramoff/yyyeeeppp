<?php

use yii\db\Migration;

class m170925_060613_4768_positional_accurancy extends Migration
{
    public function safeUp()
    {
        $this->insert('system_setting', [
            'group_id' => $this->findPrinterSettingGroupId(),
            'key' => 'default_positional_accuracy',
            'value' => 0.3,
        ]);

        $this->addColumn('ps_printer', 'positional_accuracy', 'DECIMAL(8,3) NULL');
        $this->addColumn('printer', 'positional_accuracy', 'DECIMAL(8,3) NULL');
    }

    public function safeDown()
    {
        $this->delete('system_setting', [
            'group_id' => $this->findPrinterSettingGroupId(),
            'key' => 'default_positional_accuracy',
        ]);

        $this->dropColumn('ps_printer', 'positional_accuracy');
        $this->dropColumn('printer', 'positional_accuracy');

        return true;
    }



    private function findPrinterSettingGroupId() : int
    {
        return (int) $this->db
            ->createCommand("SELECT id FROM system_setting_group WHERE title = 'printer'")
            ->queryScalar();
    }
}
