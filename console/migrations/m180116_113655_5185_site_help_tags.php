<?php

use yii\db\Migration;

/**
 * Class m180116_113655_5185_site_help_tags
 */
class m180116_113655_5185_site_help_tags extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('site_help', 'tags', 'VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('site_help', 'tags');
        return true;
    }
}
