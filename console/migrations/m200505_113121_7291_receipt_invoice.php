<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200505_113121_7291_receipt_invoice
 */
class m200505_113121_7291_receipt_invoice extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_receipt', 'invoice_uuid', "varchar(6) after order_id");
        $this->createIndex('payment_receipt_invoice_idx', 'payment_receipt', 'invoice_uuid', false);
        $this->addForeignKey('fk_payment_receipt_invoice', 'payment_receipt', 'invoice_uuid', 'payment_invoice', 'uuid');
        $this->execute('ALTER TABLE `payment_receipt_invoice_comment` DROP INDEX `inx_pric_payment_invoice_uuid`, ADD INDEX `inx_pric_payment_invoice_uuid` (`payment_invoice_uuid`) USING BTREE');
        $this->execute('ALTER TABLE `payment_receipt_invoice_comment` ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_payment_receipt_invoice', 'payment_receipt');
        $this->dropColumn('payment_receipt', 'invoice_uuid');
    }
}