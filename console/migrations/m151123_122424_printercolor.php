<?php

use yii\db\Schema;
use yii\db\Migration;

class m151123_122424_printercolor extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer_color` CHANGE COLUMN `price_measure` `price_measure` CHAR(10) NOT NULL;");
    }

    public function down()
    { 
        
    }
 
}
