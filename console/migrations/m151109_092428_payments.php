<?php

use yii\db\Schema;
use yii\db\Migration;

class m151109_092428_payments extends Migration
{
 
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropColumn('payment_detail', 'vendor_transaction_id');
        $this->dropColumn('payment_detail', 'vendor');
        
        $this->execute("CREATE TABLE `payment_detail_process` (
            `id` int(11) NOT NULL,
            `processor` char(15) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `status` char(15) NOT NULL,
            `amount` decimal(9,4) NOT NULL,
            `currency` char(3) NOT NULL,
            `transaction_id` char(32) DEFAULT NULL,
            `payment_detail_id` int(11) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          ");
        
        $this->execute("ALTER TABLE `ps_printer_color` 
            CHANGE COLUMN `price_usd` `price_usd` DECIMAL(5,2) NOT NULL DEFAULT '0.0000' ;
        ");
    }

    public function safeDown()
    {
        $this->addColumn('payment_detail', 'vendor_transaction_id', 'CHAR(15) NULL');
        $this->addColumn('payment_detail', 'vendor', 'CHAR(15) NULL');
        
        $this->truncateTable('payment_detail_process');
        $this->dropTable('payment_detail_process');
    } 
}
