<?php

use yii\db\Migration;

/**
 * Class m180810_130106_5589_services_categories
 */
class m180810_130106_5589_services_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `company_service_category` 
            ADD COLUMN `position` INT(11) NOT NULL DEFAULT 1 AFTER `description`,
            ADD COLUMN `is_visible` TINYINT(1) NOT NULL DEFAULT 1 AFTER `position`,
            ADD COLUMN `is_active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `is_visible`;
            ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company_service_category', 'position');
        $this->dropColumn('company_service_category', 'is_visible');
        $this->dropColumn('company_service_category', 'is_active');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180810_130106_5589_services_categories cannot be reverted.\n";

        return false;
    }
    */
}
