<?php

use yii\db\Migration;

class m160714_111350_2421_convert_old_sms_numbers_to_e164 extends Migration
{
    public function safeUp()
    {
        $this->execute("UPDATE user_sms SET phone = SUBSTRING(phone, 3) WHERE phone LIKE('00%');");
        $this->execute("UPDATE user_sms SET phone = CONCAT('+', phone);");
    }

    public function safeDown()
    {
        $this->execute("UPDATE user_sms SET phone = CONCAT('00', SUBSTRING(phone, 2));");
        return true;
    }
}
