<?php

use yii\db\Migration;
use yii\db\Query;

class m170112_172021_3624_model3d_print_instructions extends Migration
{
    public function up()
    {
        $this->addColumn('model3d', 'print_instructions', 'TEXT NULL after description ');
        $this->addColumn('model3d_replica', 'print_instructions', 'TEXT NULL after description ');
    }

    public function down()
    {
        $this->dropColumn('model3d', 'print_instructions');
        $this->dropColumn('model3d_replica', 'print_instructions');
    }
}
