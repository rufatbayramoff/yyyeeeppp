<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180919_145521_5814_payment_transaction_detail_duplicates
 *
 * Only one payment vendor transaction may be linked by first_payment_detail
 */
class m180919_145521_5814_payment_transaction_detail_duplicates extends Migration
{
    /**
     * @return string
     * @throws Exception
     */
    protected function generateInvoiceUuid()
    {

        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_invoice')->where('uuid="' . $operationUuid . '"')->one();
        } while ($checkUuid);
        return $operationUuid;
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function generatePaymentDetailOperationUuid()
    {
        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_detail_operation')->where('uuid="' . $operationUuid . '"')->one();
        } while ($checkUuid);
        return $operationUuid;
    }

    protected function getInvoiceForOrder($order)
    {
        $invoice = (new Query())->select('*')->from('payment_invoice')->where(['order_id' => $order['id']])->one();
        if ($invoice) {
            return $invoice;
        }
        die('Not found invoice for order: ' . $order['id']);
    }

    protected function getOrCreatePaymentForInvoice($invoice)
    {
        $payment = (new Query())->select('*')->from('payment')->where(['payment_invoice_uuid' => $invoice['uuid']])->one();
        if ($payment) {
            return $payment;
        }
        $payment = [
            'payment_invoice_uuid' => $invoice['uuid'],
            'created_at'           => \common\components\DateHelper::now(),
            'description'          => 'Technical payment for order: ' . $invoice['order_id'],
            'status'               => 'cancelled',
        ];
        $this->insert('payment', $payment);
        $payment['id'] = $this->db->getLastInsertID();
        return $payment;
    }

    protected function createPaymentOperation($payment)
    {
        $paymentDetailOperation = [
            'uuid'       => $this->generatePaymentDetailOperationUuid(),
            'payment_id' => $payment['id'],
            'created_at' => \common\components\DateHelper::now()
        ];
        $this->insert('payment_detail_operation', $paymentDetailOperation);
        return $paymentDetailOperation;
    }

    protected function formDoubleEntryBrainTreeFailed($paymentTransaction)
    {
        $order = (new Query())->select('*')->from('store_order')->where(['id' => $paymentTransaction['order_id']])->one();
        $invoice = $this->getInvoiceForOrder($order);
        $payment = $this->getOrCreatePaymentForInvoice($invoice);
        $paymentDetailOperation = $this->createPaymentOperation($payment);
        // Fix double entry
        $paymentDetail = [
            'user_id'                       => 160,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed braintree payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 160,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed braintree payment transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail['id'] = $this->db->getLastInsertID();
        return $paymentDetail;
    }

    protected function formDoubleEntryMinusFailed($paymentTransaction)
    {
        $payment = [
            'payment_invoice_uuid' => null,
            'created_at'           => \common\components\DateHelper::now(),
            'description'          => 'Technical failed payout transaction: ' . $paymentTransaction['id'],
            'status'               => 'cancelled',
        ];
        $this->insert('payment', $payment);
        $payment['id'] = $this->db->getLastInsertID();
        $paymentDetailOperation = $this->createPaymentOperation($payment);
        // Fix double entry
        $paymentDetail = [
            'user_id'                       => 150,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed payout transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 150,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed payout transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail['id'] = $this->db->getLastInsertID();
        return $paymentDetail;
    }

    protected function formDoubleEntryThingiverseFailed($paymentTransaction)
    {
        $payment = [
            'payment_invoice_uuid' => null,
            'created_at'           => \common\components\DateHelper::now(),
            'description'          => 'Technical failed thingiverse: ' . $paymentTransaction['id'],
            'status'               => 'cancelled',
        ];
        $this->insert('payment', $payment);
        $payment['id'] = $this->db->getLastInsertID();
        $paymentDetailOperation = $this->createPaymentOperation($payment);
        // Fix double entry
        $paymentDetail = [
            'user_id'                       => 112,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed thingiverse transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 112,
            'payment_detail_operation_uuid' => $paymentDetailOperation['uuid'],
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $paymentTransaction['amount'],
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Failed thingiverse transaction: ' . $paymentTransaction['id'],
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail['id'] = $this->db->getLastInsertID();
        return $paymentDetail;
    }

    protected function createDoubleEntryFaildForPayments($payments)
    {
        foreach ($payments as $payment) {
            if ($payment['vendor'] === 'braintree') {
                $detail = $this->formDoubleEntryBrainTreeFailed($payment);
            } elseif ($payment['type'] === 'minus') {
                $detail = $this->formDoubleEntryMinusFailed($payment);
            } else {
                $detail = $this->formDoubleEntryThingiverseFailed($payment);
            }
            $this->update('payment_transaction', ['first_payment_detail_id' => $detail['id']], 'id=' . $payment['id']);
            $this->update('payment_transaction_history', ['payment_detail_id' => $detail['id']],
                'transaction_id=' . $payment['id'] . ' and payment_detail_id=' . $payment['first_payment_detail_id']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $paymentTransactions = $this->db->createCommand('SELECT payment_transaction.first_payment_detail_id, count(id) FROM `payment_transaction` WHERE 1 GROUP BY first_payment_detail_id HAVING count(id)>1')->queryAll();
        echo "\nTransaction duplicates count: " . count($paymentTransactions) . "\n";
        foreach ($paymentTransactions as $paymentTransaction) {
            echo "\n\nTransaction first payment detail: " . $paymentTransaction['first_payment_detail_id'] . "\n";
            $duplicatePayments = (new Query())->select('*')->from('payment_transaction')->where('first_payment_detail_id=' . $paymentTransaction['first_payment_detail_id'])->all();
            $firstPayment = $this->getFirstPayment($duplicatePayments, 'braintree', 'settled');
            if (!$firstPayment) {
                $firstPayment = $this->getFirstPayment($duplicatePayments, 'braintree', 'authorized');
                if (!$firstPayment) {
                    $firstPayment = $this->getFirstPayment($duplicatePayments, 'braintree', 'authorization_expired');
                    if (!$firstPayment) {
                        $firstPayment = $this->getFirstPayment($duplicatePayments, 'braintree', 'refunded');
                        if (!$firstPayment) {
                            $firstPayment = $this->getFirstPayment($duplicatePayments, 'braintree', 'voided');
                            if (!$firstPayment) {
                                $firstPayment = $this->getFirstPayment($duplicatePayments, 'thingiverse', 'settled');
                                if (!$firstPayment) {
                                    $firstPayment = $this->getFirstPayment($duplicatePayments, 'thingiverse', 'authorized');
                                    if (!$firstPayment) {
                                        $firstPayment = $this->getFirstPayment($duplicatePayments, 'paypal', 'SUCCESS');
                                        if (!$firstPayment) {
                                            if ($this->allPaymentsNotSetteled($duplicatePayments)) {
                                                $firstPayment = reset($duplicatePayments);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if ($firstPayment) {
                echo 'First payment: ' . $firstPayment['vendor'] . ' - ' . $firstPayment['status'] . "\n";

                $payments = $this->listWithoutPayment($duplicatePayments, $firstPayment);
                if ($this->allPaymentsNotSetteled($payments)) {
                    echo "All other failed. Create for them double entries.\n";
                    $this->createDoubleEntryFaildForPayments($payments);
                } else {
                    if (count($payments) == 1) {
                        // To one detail, linked pay and refund transaction
                        $payment = reset($payments);
                        if ($payment['type'] === 'refund') {
                            $currentPaymentDetail = (new Query())->select('*')->from('payment_detail')->where(['id' =>$payment['first_payment_detail_id']])->one();
                            // Fix double entry
                            $paymentDetail = [
                                'user_id'                       => 160,
                                'payment_detail_operation_uuid' => $currentPaymentDetail['payment_detail_operation_uuid'],
                                'updated_at'                    => \common\components\DateHelper::now(),
                                'amount'                        => -$payment['amount'],
                                'original_currency'             => 'USD',
                                'rate_id'                       => 1,
                                'description'                   => 'Failed braintree refund transaction: ' . $payment['id'],
                                'type'                          => 'payment'
                            ];
                            $this->insert('payment_detail', $paymentDetail);
                            $paymentDetail = [
                                'user_id'                       => 160,
                                'payment_detail_operation_uuid' => $currentPaymentDetail['payment_detail_operation_uuid'],
                                'updated_at'                    => \common\components\DateHelper::now(),
                                'amount'                        => $payment['amount'],
                                'original_currency'             => 'USD',
                                'rate_id'                       => 1,
                                'description'                   => 'Failed braintree refund transaction: ' . $payment['id'],
                                'type'                          => 'payment'
                            ];
                            $this->insert('payment_detail', $paymentDetail);
                            $paymentDetail['id'] = $this->db->getLastInsertID();
                            $this->update('payment_transaction', ['first_payment_detail_id' => $paymentDetail['id']], 'id=' . $payment['id']);
                            $this->update('payment_transaction_history', ['payment_detail_id' => $paymentDetail['id']],
                                'transaction_id=' . $payment['id'] . ' and payment_detail_id=' . $payment['first_payment_detail_id']);
                        } else {
                            $currentPaymentDetail = (new Query())->select('*')->from('payment_detail')->where(['id' =>$payment['first_payment_detail_id']])->one();
                            // Fix double entry
                            $paymentDetail = [
                                'user_id'                       => 160,
                                'payment_detail_operation_uuid' => $currentPaymentDetail['payment_detail_operation_uuid'],
                                'updated_at'                    => \common\components\DateHelper::now(),
                                'amount'                        => -$payment['amount'],
                                'original_currency'             => 'USD',
                                'rate_id'                       => 1,
                                'description'                   => 'Failed braintree payment transaction: ' . $payment['id'],
                                'type'                          => 'payment'
                            ];
                            $this->insert('payment_detail', $paymentDetail);
                            $paymentDetail = [
                                'user_id'                       => 160,
                                'payment_detail_operation_uuid' => $currentPaymentDetail['payment_detail_operation_uuid'],
                                'updated_at'                    => \common\components\DateHelper::now(),
                                'amount'                        => $payment['amount'],
                                'original_currency'             => 'USD',
                                'rate_id'                       => 1,
                                'description'                   => 'Failed braintree payment transaction: ' . $payment['id'],
                                'type'                          => 'payment'
                            ];
                            $this->insert('payment_detail', $paymentDetail);
                            $paymentDetail['id'] = $this->db->getLastInsertID();
                            $this->update('payment_transaction', ['first_payment_detail_id' => $paymentDetail['id']], 'id=' . $payment['id']);
                            $this->update('payment_transaction_history', ['payment_detail_id' => $paymentDetail['id']],
                                'transaction_id=' . $payment['id'] . ' and payment_detail_id=' . $payment['first_payment_detail_id']);
                        }
                    } else {
                        echo "Not all other failed\n";
                        exit;
                    }
                }
            } else {
                var_dump($duplicatePayments);
                echo "First payment not found\n";
                exit;
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    protected function getFirstPayment($duplicatePayments, $vendor, $status)
    {
        foreach ($duplicatePayments as $duplicatePayment) {
            if ($duplicatePayment['vendor'] === $vendor && $duplicatePayment['status'] === $status) {
                return $duplicatePayment;
            }
        }
        return null;
    }

    protected function listWithoutPayment($duplicatePayments, $successPayment)
    {
        $payments = [];
        foreach ($duplicatePayments as $payment) {
            if ($payment['id'] !== $successPayment['id']) {
                $payments[] = $payment;
            }
        }
        return $payments;
    }

    protected function allPaymentsNotSetteled($payments)
    {
        foreach ($payments as $payment) {
            if ($payment['status'] !== 'failed' && $payment['status'] !== 'voided' && $payment['status'] !== 'authorized' && $payment['status'] !== 'FAILED') {
                return false;
            }
        }
        return true;
    }
}
