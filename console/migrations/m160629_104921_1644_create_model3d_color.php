<?php

use yii\db\Migration;

class m160629_104921_1644_create_model3d_color extends Migration
{

    public function safeUp()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `model3d_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model3d_id` int(11) NOT NULL,
  `model3d_file_id` int(11) DEFAULT NULL,
  `printer_material_id` int(11) NOT NULL,
  `printer_color_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model3d_id` (`model3d_id`),
  KEY `model3d_file_id` (`model3d_file_id`),
  KEY `printer_material_id` (`printer_material_id`),
  KEY `printer_color_id` (`printer_color_id`),
  CONSTRAINT `model3d_color_printer_color_id` FOREIGN KEY (`printer_color_id`) REFERENCES `printer_color` (`id`),
  CONSTRAINT `model3d_color_model3d_file_id` FOREIGN KEY (`model3d_file_id`) REFERENCES `model3d_file` (`id`),
  CONSTRAINT `model3d_color_model3d_id` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `model3d_color_printer_material_id` FOREIGN KEY (`printer_material_id`) REFERENCES `printer_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

    }

    public function safeDown()
    {
        $this->dropTable('model3d_color');
    }
}
