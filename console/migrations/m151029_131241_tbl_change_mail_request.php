<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_131241_tbl_change_mail_request extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `user_email_change_request` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `confirm_key` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL,
  `create_at` datetime NOT NULL,
  `confirm_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_email_change_request_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

    }

    public function down()
    {
        echo "m151029_131241_tbl_change_mail_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
