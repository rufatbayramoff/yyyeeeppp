<?php

use yii\db\Migration;

/**
 * Class m180724_073810_5719_alter_category
 */
class m180724_073810_5719_alter_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `product_category`  ADD COLUMN `need_premoderation` TINYINT(1) NOT NULL DEFAULT 0 AFTER `full_description`;');
        $this->execute(
            "ALTER TABLE `product` 
CHANGE COLUMN `product_status` `product_status` ENUM('draft', 'published_public', 'published_updated', 'published_directly', 'publish_pending', 'rejected', 'private', 'publish_needmoderation') NOT NULL DEFAULT 'draft' ;
"
        );

        $this->execute('ALTER TABLE `product_category` ADD COLUMN `position` INT(11) NOT NULL DEFAULT 1 AFTER `need_premoderation`;');
        $this->execute(
            'ALTER TABLE `product_category` 
ADD COLUMN `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `position`;
'
        );
        $this->execute('update product_category set `position`=id  where id>0');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_category', 'need_moderation');
        $this->dropColumn('product_category', 'updated_at');
        $this->dropColumn('product_category', 'position');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180724_073810_5719_alter_category cannot be reverted.\n";

        return false;
    }
    */
}
