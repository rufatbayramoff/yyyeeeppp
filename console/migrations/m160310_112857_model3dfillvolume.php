<?php

use yii\db\Schema;
use yii\db\Migration;

class m160310_112857_model3dfillvolume extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `model3d_file_filament` 
            CHANGE COLUMN `volume` `volume` DOUBLE(14,4) UNSIGNED NOT NULL ,
            CHANGE COLUMN `area` `area` DOUBLE(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' ,
            CHANGE COLUMN `length` `length` DOUBLE(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' ;
        ");
    }

    public function down()
    {
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
