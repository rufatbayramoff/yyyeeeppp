<?php

use common\interfaces\FileBaseInterface;
use common\models\File;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180507_085519_5541_files_stored_name
 */
class m180507_085519_5541_files_stored_name extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('file', 'stored_name', 'varchar(255) NOT NULL after path');
        $this->addColumn('file_admin', 'stored_name', 'varchar(255) NOT NULL after path');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('file', 'stored_name');
        $this->dropColumn('file_admin', 'stored_name');
    }
}
