<?php

use yii\db\Migration;

class m160304_113433_fix_doble_fields_in_filament extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `model3d_file_filament` CHANGE `length` `length` DOUBLE(14,4)  NOT NULL  DEFAULT '0.0000'");
        $this->execute("ALTER TABLE `model3d_file_filament` CHANGE `volume` `volume` DOUBLE(14,4)  NOT NULL");
        $this->execute("ALTER TABLE `model3d_file_filament` CHANGE `area` `area` DOUBLE(14,4)  NOT NULL  DEFAULT '0.0000'");
    }

    public function down()
    {
        return true;
    }
}
