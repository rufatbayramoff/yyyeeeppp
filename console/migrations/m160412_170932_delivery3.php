<?php

use yii\db\Migration;

class m160412_170932_delivery3 extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `delivery_type` ADD COLUMN `is_carrier` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_active`");
        $this->execute("UPDATE `delivery_type` SET `is_active`=0
                        WHERE code IN('expedited', 'one_day', 'intl_expedited');");

        $this->execute("UPDATE `delivery_type` SET `is_carrier`=1, `title`='Domestic shipping' WHERE  code='standard';");
        $this->execute("UPDATE `delivery_type` SET `is_carrier`=1, `title`='International shipping' WHERE  code='intl';");
    }

    public function down()
    {
        $this->dropColumn("delivery_type", "is_carrier");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
