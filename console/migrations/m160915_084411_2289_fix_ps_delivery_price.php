<?php

use yii\db\Migration;
use yii\helpers\Json;

class m160915_084411_2289_fix_ps_delivery_price extends Migration
{
    public function safeUp()
    {
        $data = Yii::$app->db->createCommand("SELECT id, delivery_details FROM ps_printer_delivery")->queryAll();

        foreach ($data as $item){

            if(!$item['delivery_details']){
                $this->update('ps_printer_delivery', ['delivery_details' => '{"carrier":"treatstock"}'], ['id' => $item['id']]);
                continue;
            }

            $deliveryDetails = Json::decode($item['delivery_details']);

            if(!isset($deliveryDetails['carrier'])){
                $deliveryDetails['carrier'] = 'treatstock';
                $this->update('ps_printer_delivery', ['delivery_details' => Json::encode($deliveryDetails)], ['id' => $item['id']]);
            }

            if ($deliveryDetails['carrier'] == 'treatstock' && array_key_exists('carrier_price', $deliveryDetails)){
                unset($deliveryDetails['carrier_price']);
                $this->update('ps_printer_delivery', ['delivery_details' => Json::encode($deliveryDetails)], ['id' => $item['id']]);
            }
        }

        $data = Yii::$app->db->createCommand("SELECT id, ps_delivery_details FROM store_order_delivery")->queryAll();

        foreach ($data as $item){

            if(!$item['ps_delivery_details']){
                $this->update('store_order_delivery', ['ps_delivery_details' => '{"carrier":"treatstock"}'], ['id' => $item['id']]);
                continue;
            }

            $deliveryDetails = Json::decode($item['ps_delivery_details']);

            if(!isset($deliveryDetails['carrier'])){
                $deliveryDetails['carrier'] = 'treatstock';
                $this->update('store_order_delivery', ['ps_delivery_details' => Json::encode($deliveryDetails)], ['id' => $item['id']]);
            }

            if ($deliveryDetails['carrier'] == 'treatstock' && array_key_exists('carrier_price', $deliveryDetails)){
                unset($deliveryDetails['carrier_price']);
                $this->update('store_order_delivery', ['ps_delivery_details' => Json::encode($deliveryDetails)], ['id' => $item['id']]);
            }
        }
    }

    public function safeDown()
    {
        return true;
    }
}
