<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180924_164421_5814_payment_transaction_orderid
 */
class m180924_164421_5814_payment_transaction_orderid extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_payment_transaction_1', 'payment_transaction');
        $this->dropColumn('payment_transaction', 'order_id');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
