<?php

use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m211201_151553_8858_bad_emails.php
 */
class m211201_151553_8858_bad_emails extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute("
 CREATE TABLE `invalid_email` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` enum('mailDeliverySystem','localLog','') NOT NULL,
  `ignored` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB

        ");
        $this->execute("
CREATE TABLE `failed_delivery_check_mailbox` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `message_uid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`,`message_uid`) USING BTREE
) ENGINE=InnoDB
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
