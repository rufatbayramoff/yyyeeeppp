<?php

use yii\db\Migration;

class m161128_080613_2864_table_receipt extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `payment_receipt` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `order_id` int(11) DEFAULT NULL,
              `comment` varchar(245) DEFAULT NULL,
              `receipt_date` timestamp NULL DEFAULT NULL,
              `pdf_file_id` int(11) DEFAULT NULL,
              `html_file_id` int(11) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_payment_receipt_1_idx` (`order_id`),
              KEY `fk_payment_receipt_2_idx` (`user_id`),
              KEY `fk_payment_receipt_3_idx` (`pdf_file_id`),
              KEY `fk_payment_receipt_4_idx` (`html_file_id`),
              CONSTRAINT `fk_payment_receipt_1` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_payment_receipt_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_payment_receipt_3` FOREIGN KEY (`pdf_file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_payment_receipt_4` FOREIGN KEY (`html_file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;'
        );
    }

    public function down()
    {
        $this->dropTable('payment_receipt');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
