<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_140554_printer_id_to_ps_printer_id extends Migration
{
    public function up()
    {
        $sql = 'SET FOREIGN_KEY_CHECKS=0;
          ALTER TABLE ps_printer_to_property DROP FOREIGN KEY fk_printer_id_7;
          ALTER TABLE ps_printer_to_property CHANGE `printer_id` `ps_printer_id` INT(11) NOT NULL;
          ALTER TABLE ps_printer_to_property ADD CONSTRAINT `fk_ps_printer_id` FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;';
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function down()
    {
        echo "m150803_140554_printer_id_to_ps_printer_id cannot be reverted.\n";

        return false;
    }
}
