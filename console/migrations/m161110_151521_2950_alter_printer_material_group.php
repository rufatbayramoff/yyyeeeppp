<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_151521_2950_alter_printer_material_group extends Migration
{
    public function safeUp()
    {
        $this->addColumn('printer_material_group', 'wall_thickness', 'int DEFAULT 80 after density');
        $this->addColumn('printer_material_group', 'fill_percent', 'int DEFAULT 20 after wall_thickness');
    }

    public function safeDown()
    {
        $this->dropColumn('printer_material_group', 'wall_thickness');
        $this->dropColumn('printer_material_group', 'fill_percent');
    }
}

