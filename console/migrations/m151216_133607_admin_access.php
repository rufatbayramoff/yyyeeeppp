<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_133607_admin_access extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `user_admin_access` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `admin_id` int(11) NOT NULL,
            `access` char(45) NOT NULL,
            `is_active` tinyint(1) NOT NULL DEFAULT '0',
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            UNIQUE KEY `index2` (`admin_id`,`access`),
            CONSTRAINT `fk_user_admin_access_1` FOREIGN KEY (`admin_id`) REFERENCES `user_admin` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
        ");
    }

    public function down()
    {
        $this->dropTable('user_admin_access');
    }
}
