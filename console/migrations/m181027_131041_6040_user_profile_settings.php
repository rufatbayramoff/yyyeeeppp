<?php

use yii\db\Migration;

/**
 * Class m181027_131041_6040_user_profile_settings
 */
class m181027_131041_6040_user_profile_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `user_profile` ADD COLUMN `settings` JSON NULL AFTER `url_twitter`;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_profile', 'settings');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181027_131041_6040_user_profile_settings cannot be reverted.\n";

        return false;
    }
    */
}
