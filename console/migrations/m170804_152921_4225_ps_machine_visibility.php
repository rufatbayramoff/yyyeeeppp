<?php

use yii\db\Migration;

class m170804_152921_4225_ps_machine_visibility extends Migration
{
    public function safeUp()
    {
        $this->addColumn('ps_machine', 'visibility', "ENUM('everywhere','widgetOnly','nowhere','') NULL");
        $this->addColumn('ps_machine', 'moderator_status', "ENUM('new','approved', 'approvedReviewing', 'rejected','rejectedReviewing','unavailableReviewing') NULL");
    }

    public function safeDown()
    {
        $this->dropColumn('ps_machine', 'visibility');
        $this->dropColumn('ps_machine', 'moderator_status');
    }

}
