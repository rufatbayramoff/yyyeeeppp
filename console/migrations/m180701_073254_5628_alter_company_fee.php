<?php

use yii\db\Migration;

/**
 * Class m180701_073254_5628_alter_company_fee
 */
class m180701_073254_5628_alter_company_fee extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `ps` ADD COLUMN `fee_percent` DECIMAL(3,2) NULL DEFAULT NULL AFTER `incoterms`;');

        $this->execute(
            'ALTER TABLE `preorder`  ADD COLUMN `service_id` INT NULL AFTER `confirm_hash`,
ADD INDEX `fk_preorder_1_idx` (`service_id` ASC); ALTER TABLE `preorder` 
ADD CONSTRAINT `fk_preorder_1` FOREIGN KEY (`service_id`)
  REFERENCES `company_service` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('fee_percent');
        $this->execute('ALTER TABLE `preorder` 
            DROP FOREIGN KEY `fk_preorder_1`;
            ALTER TABLE `preorder` 
            DROP COLUMN `service_id`,
            DROP INDEX `fk_preorder_1_idx`;
        ');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180701_073254_5628_alter_company_fee cannot be reverted.\n";

        return false;
    }
    */
}
