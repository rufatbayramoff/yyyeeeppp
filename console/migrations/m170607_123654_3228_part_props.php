<?php

use yii\db\Migration;

class m170607_123654_3228_part_props extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `model3d_part_properties` ADD COLUMN `supports_volume` DOUBLE NULL AFTER `volume`;');
        $this->execute('ALTER TABLE `printer_material_group` 
ADD COLUMN `need_supports` TINYINT(1) DEFAULT 0 AFTER `is_active`;
');
    }

    public function safeDown()
    {
        $this->dropColumn('model3d_part_properties', 'supports_volume');
        $this->dropColumn('printer_material_group', 'need_supports');
    }
}
