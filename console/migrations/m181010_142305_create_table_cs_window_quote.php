<?php

use yii\db\Migration;

/**
 * Class m181010_142305_create_table_cs_window_quote
 */
class m181010_142305_create_table_cs_window_quote extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cs_window_quote', [
            'id'                     => $this->primaryKey(),
            'uid'                    => $this->char(10)->notNull(),
            'company_service_id'     => $this->integer()->notNull(),
            'cs_window_snapshot_uid' => $this->char(10)->notNull(),
            'user_session_id'        => $this->integer()->notNull(),
            'user_id'                => $this->integer(),
            'created_at'             => 'datetime default CURRENT_TIMESTAMP not null',
            'updated_at'             => 'datetime default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP',
            'viewing_at'             => 'datetime null',
            'phone'                  => $this->string(45)->null(),
            'email'                  => $this->string()->notNull(),
            'contact_name'           => $this->string()->notNull(),
            'status'                 => "enum ('wait_confirm', 'new', 'accepted', 'rejected', 'deleted') default 'new' not null",
            'address'                => $this->string()->null(),
            'notes'                  => $this->text()->null(),
            'total_price'            => $this->decimal(15, 4)->notNull(),
            'currency'               => $this->char(5)->notNull(),
            'measurement'            => $this->char(5)->notNull()
        ]);

        $this->createIndex('index_cswq_company_service_id', 'cs_window_quote', 'company_service_id');
        $this->createIndex('index_cswq_user_id', 'cs_window_quote', 'user_id');
        $this->createIndex('index_cswq_snapshot_uid', 'cs_window_quote', 'cs_window_snapshot_uid');
        $this->createIndex('index_cswq_uid', 'cs_window_quote', 'uid', true);

        $this->addForeignKey('fk_cswq_company_service_id', 'cs_window_quote', 'company_service_id', 'company_service', 'id');
        $this->addForeignKey('fk_cswq_user_id', 'cs_window_quote', 'user_id', 'user', 'id');
        $this->addForeignKey('fk_cswq_user_session', 'cs_window_quote', 'user_session_id', 'user_session', 'id');
        $this->addForeignKey('fk_cswq_snapshot_uid', 'cs_window_quote', 'cs_window_snapshot_uid', 'cs_window_snapshot', 'uid');

        $this->createTable('cs_window_quote_item', [
            'id'                  => $this->primaryKey(),
            'uid'                 => $this->char(10)->notNull(),
            'cs_window_quote_uid' => $this->char(10)->notNull(),
            'title'               => $this->string()->notNull(),
            'quote_parameters'    => 'json',
            'qty'                 => $this->integer()->notNull(),
            'cost'                => $this->decimal(15, 4)->notNull(),
            'frame_id'            => $this->integer()->notNull(),
            'glass_id'            => $this->integer()->notNull(),
            'profile_id'          => $this->integer()->notNull(),
            'furniture_id'        => $this->integer()->null(),
            'windowsill'          => $this->tinyInteger()->notNull()->defaultValue(0),
            'lamination'          => $this->tinyInteger()->notNull()->defaultValue(0),
            'slope'               => $this->tinyInteger()->notNull()->defaultValue(0),
            'tinting'             => $this->tinyInteger()->notNull()->defaultValue(0),
            'energy_saver'        => $this->tinyInteger()->notNull()->defaultValue(0),
            'installation'        => $this->tinyInteger()->notNull()->defaultValue(0),
        ]);

        $this->createIndex('index_cswqi_quote_uid', 'cs_window_quote_item', 'cs_window_quote_uid');
        $this->createIndex('index_cswqi_uid', 'cs_window_quote_item', 'uid', true);

        $this->addForeignKey('fk_cswqi_quote_uid', 'cs_window_quote_item', 'cs_window_quote_uid', 'cs_window_quote', 'uid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cs_window_quote_item');
        $this->dropTable('cs_window_quote');
    }
}
