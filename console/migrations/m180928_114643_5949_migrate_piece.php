<?php

use yii\db\Migration;

/**
 * Class m180928_114643_5949_migrate_piece
 */
class m180928_114643_5949_migrate_piece extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("update product set unit_type='pc.' where unit_type='piece' and id>0;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180928_114643_5949_migrate_piece cannot be reverted.\n";

        return false;
    }
    */
}
