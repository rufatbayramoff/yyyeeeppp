<?php

use yii\db\Migration;
use yii\db\Query;

class m160919_165121_2911_file_download_hash extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `file_download_hash` (
  `file_id` INT(11) NOT NULL,
  `start_date` DATETIME NOT NULL,
  `end_date` DATETIME NOT NULL,
  `hash` varchar(64) NOT NULL,
  KEY `file_id` (`file_id`),
  UNIQUE KEY `hash` (`hash`),
  CONSTRAINT `file_foreign_key_id` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function down()
    {
        $this->dropTable('file_download_hash');
    }
}
