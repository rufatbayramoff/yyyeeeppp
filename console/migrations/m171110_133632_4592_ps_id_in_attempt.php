<?php

use yii\db\Migration;

class m171110_133632_4592_ps_id_in_attempt extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `store_order_attemp` 
            ADD COLUMN `ps_id` INT(11) NULL AFTER `machine_id`,
            ADD INDEX `fk_store_order_attemp_2_idx` (`ps_id` ASC);
            ALTER TABLE `store_order_attemp` 
            ADD CONSTRAINT `fk_store_order_attemp_2`
              FOREIGN KEY (`ps_id`)
              REFERENCES `ps` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");

        $this->execute("
        
           UPDATE store_order_attemp, ps_machine 
           SET store_order_attemp.ps_id = ps_machine.ps_id
           WHERE store_order_attemp.machine_id = ps_machine.id;
            
        ");
    }

    public function safeDown()
    {
        $this->dropColumn("store_order_attemp", "ps_id");

        return true;
    }
}
