<?php

use yii\db\Migration;

/**
 * Class m180426_121929_5186_product_emails
 */
class m180426_121929_5186_product_emails extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(
            'email_template',
            [
                'code'          => 'company.product.moderation.reject',
                'group'         => 'service',
                'language_id'   => 'en-US',
                'title'         => 'Publishing of your %productTitle% on treatstock.com',
                'description'   => ' params: productTitle, reason, comment, companyName, rejectReason, productLink, username',
                'template_html' => 'Hello %companyName%,

The information you have entered about your product has not been approved by the moderator.  %rejectReason% Please <a href="%productLink%">edit your product</a>.

Best regards,
Treatstock',
                'updated_at'    => dbexpr('NOW()'),
                'is_active'     => true
            ]
        );

        $this->insert(
            'email_template',
            [
                'code'          => 'company.product.moderation.approved',
                'group'         => 'service',
                'language_id'   => 'en-US',
                'title'         => 'Publishing of your %productTitle% on treatstock.com',
                'description'   => ' params: productTitle, username, companyName, productLink',
                'template_html' => 'Hello %companyName%,

Your product has been approved! Please visit your <a href="%productLink%">product page</a>.

Best regards,
Treatstock',
                'updated_at'    => dbexpr('NOW()'),
                'is_active'     => true
            ]
        );

        $this->execute('ALTER TABLE `moder_log` ADD COLUMN `object_uuid` CHAR(45) NULL AFTER `object_id`;');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(
            'email_template',
            [
                'code' => 'company.product.moderation.reject',
            ]
        );
        $this->delete(
            'email_template',
            [
                'code' => 'company.product.moderation.approved',
            ]
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180426_121929_5186_product_emails cannot be reverted.\n";

        return false;
    }
    */
}
