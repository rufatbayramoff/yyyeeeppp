<?php

use yii\db\Migration;

/**
 * Class m210610_184810_8585_cashback
 */
class m210610_184810_8585_cashback extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ps', 'cashback_percent', 'DECIMAL(3,1) NOT NULL DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
