<?php

use common\interfaces\Model3dBaseImgInterface;
use yii\db\Migration;
use yii\db\Query;

class m160928_162621_3002_minimal_price_hash_table extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `printer_min_cost_hash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11),
  `printer_material_group_id` int(11) NOT NULL,
  `printer_color_id` int(11) NOT NULL,
  `size_max` char(41) NOT NULL,
  `weight_from` decimal(10,3) NOT NULL,
  `weight_to` decimal(10,3) NOT NULL,
  `price_formula` varchar(20) NOT NULL,
  `ps_printer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `printer_material_group_id` (`printer_material_group_id`),
  KEY `printer_color_id` (`printer_color_id`),
  KEY `ps_printer_id` (`ps_printer_id`),
  KEY `size_max` (`size_max`),
  KEY `weight_from` (`weight_from`),
  KEY `weight_to` (`weight_to`),
  CONSTRAINT `printer_min_cost_hash_printer` FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer` (`id`),
  CONSTRAINT `printer_min_cost_hash_country` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`),
  CONSTRAINT `printer_min_cost_hash_printer_color` FOREIGN KEY (`printer_color_id`) REFERENCES `printer_color` (`id`),
  CONSTRAINT `printer_min_cost_hash_printer_material_group` FOREIGN KEY (`printer_material_group_id`) REFERENCES `printer_material_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function down()
    {
        $this->dropTable('file_download_hash');
    }
}
