<?php

use yii\db\Migration;

class m170614_085009_4390_ready_sent_date extends Migration
{
    public function up()
    {
        $this->addColumn("store_order_attemp_dates", "ready_to_sent_at", "TIMESTAMP NULL");
        $this->execute("
          UPDATE store_order_attemp_dates, store_order_attemp 
          SET store_order_attemp_dates.ready_to_sent_at = NOW() 
          WHERE  store_order_attemp_dates.attemp_id = store_order_attemp.id AND store_order_attemp.status IN ('ready_send', 'sent', 'delivered', 'received')
        ");
    }

    public function down()
    {
        $this->dropColumn("store_order_attemp_dates", "ready_to_sent_at");
        return true;
    }
}
