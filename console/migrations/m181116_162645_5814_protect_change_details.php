<?php

use yii\db\Migration;

/**
 * Class m181116_162645_5814_protect_change_details
 */
class m181116_162645_5814_protect_change_details extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE IF NOT EXISTS `payment_warning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_detail_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `user_id` (`user_id`),
  KEY `payment_detail_id` (`payment_detail_id`),
  CONSTRAINT `fk_payment_warning_detail_id` FOREIGN KEY (`payment_detail_id`) REFERENCES `payment_detail` (`id`),
  CONSTRAINT `fk_payment_warning_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_admin` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');
        $this->execute('ALTER TABLE `payment_detail` CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NOT NULL');

        $this->execute('DROP TRIGGER IF EXISTS payment_detail_update_protect');
        $this->execute('CREATE DEFINER = CURRENT_USER TRIGGER `payment_detail_update_protect` BEFORE UPDATE ON `payment_detail` FOR EACH ROW SIGNAL SQLSTATE \'45000\' SET MESSAGE_TEXT = \'Payment detail update\', MYSQL_ERRNO = 1000;');
        $this->execute('DROP TRIGGER IF EXISTS payment_detail_delete_protect');
        $this->execute('CREATE DEFINER = CURRENT_USER TRIGGER `payment_detail_delete_protect` BEFORE DELETE ON `payment_detail` FOR EACH ROW SIGNAL SQLSTATE \'45000\' SET MESSAGE_TEXT = \'Payment detail delete\', MYSQL_ERRNO = 1000;');
        $this->execute('DROP TRIGGER IF EXISTS payment_detail_insert_protect');
        $this->execute('CREATE DEFINER = CURRENT_USER TRIGGER `payment_detail_insert_protect` BEFORE INSERT ON `payment_detail` FOR EACH ROW BEGIN SET NEW.updated_at = NOW(); END');
    }

    /*

     INSERT INTO `payment_detail` (`id`, `payment_detail_operation_uuid`, `payment_account_id`, `updated_at`, `amount`, `original_currency`, `rate_id`, `description`, `type`) VALUES (NULL, 'NU8ZZX', '1577', '2018-11-16 13:14:12', '700', 'USD', '1', 'eee', 'payment');

     */

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment_warning');
    }
}
