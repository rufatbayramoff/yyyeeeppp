<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190123_111154_6233_add_log_params_body
 */
class m190123_111154_6233_add_log_params_body extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('http_request_log', 'params_body', 'json default null after params_post');
    }

    public function safeDown()
    {
        $this->dropColumn('http_request_log', 'params_body');
    }
}