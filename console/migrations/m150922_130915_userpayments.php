<?php

use yii\db\Schema;
use yii\db\Migration;

class m150922_130915_userpayments extends Migration
{
     
    public function safeUp()
    {
        $this->execute("DROP TABLE IF EXISTS `user_payment`;");
        $this->execute("DROP TABLE IF EXISTS `payment_transaction_history`;");
        $this->execute("DROP TABLE IF EXISTS `payment_transaction`;");
        
        $this->execute("CREATE TABLE `payment_transaction` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `order_id` int(11) DEFAULT NULL,
            `transaction_id` varchar(45) NOT NULL,
            `vendor` char(15) DEFAULT 'braintree',
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `status` char(15) NOT NULL,
            `type` enum('payment','refund','plus','minus') NOT NULL,
            `amount` decimal(10,4) NOT NULL,
            `currency` char(5) NOT NULL,
            `comment` varchar(145) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_payment_transaction_1_idx` (`order_id`),
            KEY `fk_payment_transaction_2_idx` (`user_id`),
            CONSTRAINT `fk_payment_transaction_1` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_payment_transaction_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
        ");
        
         $this->execute("CREATE TABLE `payment_transaction_history` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `transaction_id` int(11) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `action_id` char(15) NOT NULL,
            `comment` varchar(245) DEFAULT NULL,
            `user_id` int(11) NOT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_payment_transaction_history_1_idx` (`transaction_id`),
            KEY `fk_payment_transaction_history_2_idx` (`user_id`),
            CONSTRAINT `fk_payment_transaction_history_1` FOREIGN KEY (`transaction_id`) REFERENCES `payment_transaction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_payment_transaction_history_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;"
        );
    }

    public function safeDown()
    {
        $this->execute("DROP TABLE IF EXISTS `user_payment`;");
    }
}
