<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190121_110921_6198_fix_updated_at_payment_details
 */
class m190121_110921_6198_fix_updated_at_payment_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(<<<SQL
        DROP TRIGGER IF EXISTS `payment_detail_delete_protect`;
DROP TRIGGER IF EXISTS `payment_detail_insert_protect`;
DROP TRIGGER IF EXISTS `payment_detail_update_protect`;
SQL
        );
        $this->execute(<<<SQL
        UPDATE payment_detail LEFT JOIN payment_detail_operation ON payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid SET payment_detail.updated_at=payment_detail_operation.created_at WHERE payment_detail.updated_at='2018-12-12 13:00:14';
SQL
        );
        $this->execute(<<<SQL
        CREATE TRIGGER `payment_detail_delete_protect` BEFORE DELETE ON `payment_detail`
 FOR EACH ROW SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Payment detail delete', MYSQL_ERRNO = 1000;
SQL
        );
        $this->execute(<<<SQL
  CREATE TRIGGER `payment_detail_insert_protect` BEFORE INSERT ON `payment_detail`
 FOR EACH ROW BEGIN SET NEW.updated_at = NOW(); END
SQL
        );
        $this->execute(<<<SQL
        CREATE TRIGGER `payment_detail_update_protect` BEFORE UPDATE ON `payment_detail`
 FOR EACH ROW SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Payment detail update', MYSQL_ERRNO = 1000
SQL
        );

    }

    public function safeDown()
    {

    }
}