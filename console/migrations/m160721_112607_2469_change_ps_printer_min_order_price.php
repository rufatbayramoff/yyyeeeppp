<?php

use yii\db\Migration;

class m160721_112607_2469_change_ps_printer_min_order_price extends Migration
{
    public function up()
    {
        $this->execute("
        
        UPDATE ps_printer 

LEFT JOIN `ps_printer_material` ON `ps_printer`.`id` = `ps_printer_material`.`ps_printer_id` 
LEFT JOIN `printer_material` ON `ps_printer_material`.`material_id` = `printer_material`.`id` 
LEFT JOIN `ps_printer_color` ON `ps_printer_material`.`id` = `ps_printer_color`.`ps_material_id` 

SET ps_printer.min_order_price = 10


WHERE 
(ps_printer.min_order_price IS NULL)
AND 
(
    (printer_material.recomend_price_per_gramm_min IS NULL AND ps_printer_color.price_usd < 0.2) 
    OR 
    (printer_material.recomend_price_per_gramm_min IS NOT NULL AND ps_printer_color.price_usd < printer_material.recomend_price_per_gramm_min)
)    
        ");

    }

    public function down()
    {
        return true;
    }

}
