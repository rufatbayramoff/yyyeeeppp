<?php

use yii\db\Migration;

class m160929_180021_3009_fix_catalog_cost extends Migration
{
    public function up()
    {
        $this->alterColumn('catalog_cost', 'cost_usd', 'decimal(10,2) DEFAULT NULL');
    }

    public function down()
    {
    }
}
