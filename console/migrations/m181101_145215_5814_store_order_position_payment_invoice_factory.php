<?php

use yii\db\Migration;

/**
 * Class m181101_145215_5814_store_order_position_payment_invoice_factory
 */
class m181101_145215_5814_store_order_position_payment_invoice_factory extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_position', 'primary_payment_invoice_uuid', $this->string(6));

        $this->createIndex('idx_sop_primary_payment_invoice_uuid', 'store_order_position', 'primary_payment_invoice_uuid', true);
        $this->addForeignKey(
            'fk_sop_primary_payment_invoice_uuid',
            'store_order_position',
            'primary_payment_invoice_uuid',
            'payment_invoice',
            'uuid'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_sop_primary_payment_invoice_uuid', 'store_order_position');
        $this->dropColumn('store_order_position', 'primary_payment_invoice_uuid');
    }

}
