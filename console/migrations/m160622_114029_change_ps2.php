<?php

use yii\db\Migration;

class m160622_114029_change_ps2 extends Migration
{
    
    public function safeUp()
    {
        $this->execute("UPDATE `email_template` SET `title`='We have found a new Print Service for your order!', `template_html`='Dear %clientName%, %oldPs% has declined your request to print order #%orderId%, but do not worry as we have found another print service named %newPs% that has accepted your order for print. You will not be charged any additional costs and you can contact the print service in Treatstock messages if you wish.\n\nIf your shipping option was set as pick up, you can view the new address for pick up in the order details.' WHERE `code`='clientPrintStartOfferedOrder' AND language_id='en-US';");

        $this->execute("INSERT IGNORE INTO `email_template` (`code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`) VALUES ('moderatorDeclineOrder', 'order', 'en-US', 'Order #%orderId% on Treatstock has been canceled.', '', NOW(), 'We are sorry to inform you that your order #%orderId% has been canceled due to: %reason%. %comment%')");
    }

    public function safeDown()
    {

    } 
}
