<?php

use yii\db\Migration;
use yii\db\Query;

class m160913_173121_2366_multicolor_color extends Migration
{
    public function safeUp()
    {
        $multicolorDescription = [
            'id'           => 102,
            'title'        => 'Multicolor',
            'is_active'    => 1,
            'rgb'          => '255,255,255',
            'render_color' => 'Multicolor',
            'ambient'      => 0,
        ];
        $existsColor102 = (new Query())
            ->select('*')
            ->from('printer_color')
            ->where(['id' => 102])->one();
        if ($existsColor102) {
            if ($existsColor102['title'] === 'Multicolor') {
                return true;
            } else {
                unset($multicolorDescription['id']);
                $this->update(
                    'printer_color',
                    $multicolorDescription,
                    ['id' => 102]
                );
            }
        } else {
            $this->insert(
                'printer_color',
                $multicolorDescription
            );
        }
    }

    public function safeDown()
    {
    }
}
