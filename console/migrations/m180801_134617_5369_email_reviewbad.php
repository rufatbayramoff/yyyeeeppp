<?php

use yii\db\Migration;

/**
 * Class m180801_134617_5369_email_reviewbad
 */
class m180801_134617_5369_email_reviewbad extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //
        $this->insert(
            'email_template',
            [
                'code'          => 'client.order.badreview',
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => 'Your rating for order #%orderId% on Treatstock',
                'description'   => 'Email sent if client gave bad review for order. params: orderId, clientName',
                'template_html' => "<p>Hello %clientName%! </p>
<p>We're sorry to hear that you weren't completely happy with your order. We're normally known for our exceptional attention to detail, and we regret that we missed the mark.</p> 
<p>If you need assistance, please <a href='mailto:support@treatstock.com'>contact us</a> and our friendly support staff will get in touch with you shortly. Thank you.</p>

<p>Best regards,
Treatstock</p>"
            ,
                'updated_at'    => dbexpr('NOW()'),
                'is_active'     => true
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'client.order.badreview']);
    }
}
