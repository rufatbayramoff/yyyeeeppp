<?php

use yii\db\Migration;

/**
 * Class m180507_100238_fix_email_templates_urls
 */
class m180507_100238_fix_email_templates_urls extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/printservice-order/print-requests/accepted', '/workbench/service-orders/accepted'), 
              template_text = REPLACE(template_text, '/my/printservice-order/print-requests/accepted', '/workbench/service-orders/accepted');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/printservice-order/print-requests/printed', '/workbench/service-orders/printed'), 
              template_text = REPLACE(template_text, '/my/printservice-order/print-requests/printed', '/workbench/service-orders/printed');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/printservice-order/print-requests/ready_send', '/workbench/service-orders/ready_send'), 
              template_text = REPLACE(template_text, '/my/printservice-order/print-requests/ready_send', '/workbench/service-orders/ready_send');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/printservice-order/print-requests/new', '/workbench/service-orders/new'),
              template_text = REPLACE(template_text, '/my/printservice-order/print-requests/new', '/workbench/service-orders/new');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/printservice-order/quotes', '/workbench/preorder/quotes'), 
              template_text = REPLACE(template_text, '/my/printservice-order/quotes', '/workbench/preorder/quotes');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/printservice/edit-ps', '/mybusiness/company/edit-ps'), 
              template_text = REPLACE(template_text, '/my/printservice/edit-ps', '/mybusiness/company/edit-ps');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/printservice', '/mybusiness/services'), 
              template_text = REPLACE(template_text, '/my/printservice', '/mybusiness/services');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/orders', '/workbench/orders'), 
              template_text = REPLACE(template_text, '/my/orders', '/workbench/orders');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/order', '/workbench/orders'), 
              template_text = REPLACE(template_text, '/my/order', '/workbench/orders');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/messages/index?folderAlias=orders', '/workbench/messages/index?folderAlias=orders'), 
              template_text = REPLACE(template_text, '/my/messages/index?folderAlias=orders', '/workbench/messages/index?folderAlias=orders');
              
            UPDATE email_template SET
              template_html = REPLACE(template_html, '/my/payments/transactions', '/mybusiness/settings'),
              template_text = REPLACE(template_text, '/my/payments/transactions', '/mybusiness/settings')
              WHERE `code` = 'ps.testorder.success';
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/payments/transactions', '/workbench/payments'), 
              template_text = REPLACE(template_text, '/my/payments/transactions', '/workbench/payments');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/taxes/step1', '/mybusiness/taxes/step1'), 
              template_text = REPLACE(template_text, '/my/taxes/step1', '/mybusiness/taxes/step1');
              
            UPDATE email_template SET 
              template_html = REPLACE(template_html, '/my/taxes', '/mybusiness/taxes'), 
              template_text = REPLACE(template_text, '/my/taxes', '/mybusiness/taxes');
        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return true;
    }
}
