<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m190822_141021_6796_thingiverse_order
 */
class m190823_101621_6796_thingiverse_commission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `payment_account` CHANGE `type` `type` ENUM('main','authorize','reserved','award','tax','invoice','manufactureAward','refundRequest','discount','easypost','refund','payout','correction','fee') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'main'");
        $this->insert('payment_account', ['id'=> 113,'user_id' => 112, 'type' => 'fee']);
    }

    public function safeDown()
    {
        $this->delete('payment_account', ['id' => 113]);
    }
}