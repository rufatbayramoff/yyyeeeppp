<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m211110_165853_8952_affiliate_company
 */
class m211110_165853_8952_affiliate_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute('ALTER TABLE `affiliate_source` ADD `user_id` INT(11) AFTER `created_at`, ADD INDEX (`user_id`);');
        $this->execute('ALTER TABLE `affiliate_source` ADD CONSTRAINT `affiliate_source_user_id` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->execute('UPDATE `affiliate_source` LEFT JOIN ps on ps.id=company_id SET affiliate_source.user_id = ps.user_id');
        $this->execute('ALTER TABLE affiliate_source DROP FOREIGN KEY `affiliate_source_company_id`');
        $this->execute('ALTER TABLE `affiliate_source` DROP `company_id`');
        $this->execute('ALTER TABLE `affiliate_source` CHANGE `user_id` `user_id` INT NOT NULL;');
        $this->execute("ALTER TABLE `store_order_review_share` CHANGE `social_type` `social_type` ENUM('link','google','facebook','facebook_im','reddit','linkedin','affiliate') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;");

    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
