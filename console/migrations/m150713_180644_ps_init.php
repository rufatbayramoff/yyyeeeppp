<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_180644_ps_init extends Migration
{
    public function up()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $path = realpath(dirname(__DIR__) . '/../db/');
        $sql = file_get_contents($path . '/PS.sql');
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function down()
    {
        echo 'cannot be undone';
        return 0;
    }
}
