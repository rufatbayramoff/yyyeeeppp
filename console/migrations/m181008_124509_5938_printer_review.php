<?php

use yii\db\Migration;

/**
 * Class m181008_124509_5938_printer_review
 */
class m181008_124509_5938_printer_review extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            'CREATE TABLE `printer_review` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `printer_id` int(11) NOT NULL,
                  `user_id` int(11) NOT NULL,
                  `created_at` datetime NOT NULL,
                  `print_quality` tinyint(4) NOT NULL,
                  `ease_of_use` tinyint(4) NOT NULL,
                  `failure_rate` tinyint(4) NOT NULL,
                  `software` tinyint(4) NOT NULL,
                  `community` tinyint(4) NOT NULL,
                  `build_quality` tinyint(4) NOT NULL,
                  `reliability` tinyint(4) NOT NULL,
                  `running_expenses` tinyint(4) NOT NULL,
                  `customer_service` tinyint(4) NOT NULL,
                  `value` tinyint(4) NOT NULL,
                  `status` char(45) NOT NULL DEFAULT \'new\',
                  `comments` text,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `index2` (`user_id`,`printer_id`),
                  KEY `index3` (`status`),
                  KEY `fk_printer_review_1` (`printer_id`),
                  CONSTRAINT `fk_printer_review_1` FOREIGN KEY (`printer_id`) REFERENCES `printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  CONSTRAINT `fk_printer_review_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB ;'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('printer_review');
    }

}
