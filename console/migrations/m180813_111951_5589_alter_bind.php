<?php

use yii\db\Migration;

/**
 * Class m180813_111951_5589_alter_bind
 */
class m180813_111951_5589_alter_bind extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `company_certification_bind` 
          ADD UNIQUE INDEX `index6` (`certification_id` ASC, `company_service_id` ASC, `product_uuid` ASC);');

        $this->execute('ALTER TABLE `company_certification_bind` DROP INDEX `index5` ;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('ALTER TABLE `company_certification_bind` 
        ADD UNIQUE INDEX `index5` (`certification_id` ASC, `product_uuid` ASC);
        ');
        $this->execute('ALTER TABLE `company_certification_bind` DROP INDEX `index6` ;');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180813_111951_5589_alter_bind cannot be reverted.\n";

        return false;
    }
    */
}
