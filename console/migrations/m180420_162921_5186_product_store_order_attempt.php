<?php

use yii\db\Migration;

/**
 * Class m180420_162921_5186_product_store_order_attempt
 */
class m180420_162921_5186_product_store_order_attempt extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('store_order_item', 'product_uuid', 'varchar(32) null');
        $this->createIndex('store_order_item_product_uuid', 'store_order_item', 'product_uuid');
        $this->addForeignKey('fk_store_order_item_product_uuid', 'store_order_item', 'product_uuid', 'product', 'uuid');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_store_order_item_product_uuid', 'store_order_item');
        $this->dropColumn('store_order_item', 'product_uuid');
    }
}


