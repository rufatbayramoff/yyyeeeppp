<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190521_160021_6573_user_session_ip
 */
class m190521_160021_6573_user_session_ip extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_session', 'ip' ,'char(15)');
    }

    public function safeDown()
    {
        $this->dropColumn('user_session', 'ip' );
    }
}