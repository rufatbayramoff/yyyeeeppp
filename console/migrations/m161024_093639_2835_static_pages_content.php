<?php

use yii\db\Migration;

class m161024_093639_2835_static_pages_content extends Migration
{
    public function safeUp()
    {
        $sql = file_get_contents(__DIR__.'/data/static_page.sql');
        $this->execute($sql);
    }

    public function safeDown()
    {
        $this->delete('static_page_intl');
        $this->delete('static_page');

        return true;
    }
}
