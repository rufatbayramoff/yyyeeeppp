<?php

use yii\db\Migration;

class m161028_085810_3196_sms_response extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `user_sms`  CHANGE COLUMN `response` `response` VARCHAR(1200) NULL DEFAULT NULL ;');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
