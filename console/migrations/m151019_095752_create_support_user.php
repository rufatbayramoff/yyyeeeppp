<?php

use yii\db\Schema;
use yii\db\Migration;

class m151019_095752_create_support_user extends Migration
{
    public function up()
    {

        $this->execute("

        INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `lastlogin_at`, `trustlevel`)
VALUES
	(1, 'support', '', '', NULL, 'support@treatstock.com', 1, 0, 0, 0, 'hight');

        ");

    }

    public function down()
    {
        echo "m151019_095752_create_support_user cannot be reverted.\n";

        return false;
    }
}
