<?php

use yii\db\Schema;
use yii\db\Migration;

class m161019_101937_language_update extends Migration
{
    public function up()
    {
        $this->truncateTable("system_lang_message");
        $this->execute("

INSERT IGNORE INTO system_lang (`id`,`title`,`title_original`,`iso_code`,`url`,`is_active`) VALUES ('5','English','English','en-US','en','0');
INSERT IGNORE INTO system_lang (`id`,`title`,`title_original`,`iso_code`,`url`,`is_active`) VALUES ('6','Russian','Русский','ru','ru','0');
INSERT IGNORE INTO system_lang (`id`,`title`,`title_original`,`iso_code`,`url`,`is_active`) VALUES ('7','Japanese','日本語','ja','ja','0');
INSERT IGNORE INTO system_lang (`id`,`title`,`title_original`,`iso_code`,`url`,`is_active`) VALUES ('8','Chinese','中文','zh-CN','cn','0');
INSERT IGNORE INTO system_lang (`id`,`title`,`title_original`,`iso_code`,`url`,`is_active`) VALUES ('9','French','Français','fr','fr','0');
INSERT IGNORE INTO system_lang (`id`,`title`,`title_original`,`iso_code`,`url`,`is_active`) VALUES ('10','German','Deutch','de','de','0');

    ");

        $path = realpath(dirname(__DIR__) . '/../db/');
        $sql = file_get_contents($path . '/m161019_101937_language_update.sql');
        $this->execute($sql);



    }

    public function down()
    {
        echo "m161019_101937_language_update cannot be reverted.\n";
        return false;
    }
}

