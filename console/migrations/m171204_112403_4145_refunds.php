<?php

use yii\db\Migration;

class m171204_112403_4145_refunds extends Migration
{
    public function safeUp()
    {
        $this->execute(
            'CREATE TABLE `payment_transaction_refund` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `transaction_id` int(11) NOT NULL,
                  `created_at` datetime NOT NULL,
                  `user_id` int(11) NOT NULL,
                  `amount` decimal(6,2) NOT NULL,
                  `currency` char(5) NOT NULL,
                  `status` varchar(15) NOT NULL DEFAULT \'new\',
                  `comment` varchar(245) NOT NULL,
                  `transaction_refund_id` int(11) DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  KEY `fk_payment_transaction_refund_1_idx` (`transaction_id`),
                  KEY `fk_payment_transaction_refund_2_idx` (`user_id`),
                  KEY `fk_payment_transaction_refund_3_idx` (`transaction_refund_id`),
                  CONSTRAINT `fk_payment_transaction_refund_1` FOREIGN KEY (`transaction_id`) REFERENCES `payment_transaction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  CONSTRAINT `fk_payment_transaction_refund_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  CONSTRAINT `fk_payment_transaction_refund_3` FOREIGN KEY (`transaction_refund_id`) REFERENCES `payment_transaction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
        );
    }

    public function safeDown()
    {
        $this->truncateTable('payment_transaction_refund');
        $this->dropTable('payment_transaction_refund');
    }

}
