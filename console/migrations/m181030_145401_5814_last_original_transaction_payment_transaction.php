<?php

use yii\db\Migration;

/**
 * Class m181030_145401_5814_last_original_transaction_payment_transaction
 */
class m181030_145401_5814_last_original_transaction_payment_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_transaction', 'last_original_transaction', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('payment_transaction', 'last_original_transaction');
    }
}
