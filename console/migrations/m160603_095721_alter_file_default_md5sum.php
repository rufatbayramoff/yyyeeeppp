<?php

use yii\db\Migration;

class m160603_095721_alter_file_default_md5sum extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `file` CHANGE `md5sum` `md5sum` CHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '';");
    }

    public function down()
    {
    }
 
}
