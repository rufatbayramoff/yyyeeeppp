<?php

use yii\db\Migration;

/**
 * Class m180914_143151_preorder_table_message_default_null
 */
class m180914_143151_preorder_table_message_default_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('preorder', 'message', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('preorder', 'message', $this->text()->notNull());
    }
}
