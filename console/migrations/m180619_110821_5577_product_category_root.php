<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180619_110821_5577_product_category_root
 */
class m180619_110821_5577_product_category_root extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('product_category', ['id'=>1, 'code'=>'root', 'title'=>'Root', 'is_active'=>1, 'parent_id'=>null]);
        $this->update('product_category', ['parent_id'=>1], 'parent_id is null and id!=1');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
