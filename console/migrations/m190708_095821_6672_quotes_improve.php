<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190708_095821_6672_quotes_improve
 */
class m190708_095821_6672_quotes_improve extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ps', 'max_outgoing_quotes', 'int(11) null');
        $this->addColumn('user', 'allow_incoming_quotes', 'int(11) not null default 1');
        $this->insert('system_setting', [
            'group_id' => '3',
            'key' => 'max_outgoing_quotes',
            'value' => '5',
            'created_at'=>'2019-07-08 14:58:21',
            'updated_at'=>'2019-07-08 14:58:21',
            'description' => 'Maximum allowed outgoing quotes for company',
            'is_active' => 1,
        ]);
    }

    public function safeDown()
    {
        $this->delete('system_setting', [
            'key' => 'max_outgoing_quotes'
        ]);
        $this->dropColumn('ps', 'max_outgoing_quotes');
        $this->dropColumn('user', 'allow_incoming_quotes');
    }
}