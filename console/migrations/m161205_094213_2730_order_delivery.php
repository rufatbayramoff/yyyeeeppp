<?php

use yii\db\Migration;

class m161205_094213_2730_order_delivery extends Migration
{
    public function up()
    {
        $this->execute("
        
        ALTER TABLE `store_order_delivery` 
        DROP COLUMN `status`;
        
        
        CREATE TABLE `store_order_attemp_delivery` (
          `order_attemp_id` INT UNSIGNED NOT NULL,
          `file_id` INT NULL,
          `tracking_number` VARCHAR(45) NULL,
          `tracking_shipper` VARCHAR(45) NULL,
          `send_message_at` TIMESTAMP NULL,
          PRIMARY KEY (`order_attemp_id`),
          INDEX `fk_store_order_attemp_delivery_1_idx` (`file_id` ASC),
          CONSTRAINT `fk_store_order_attemp_delivery_1`
            FOREIGN KEY (`file_id`)
            REFERENCES `file` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_store_order_attemp_delivery_2`
            FOREIGN KEY (`order_attemp_id`)
            REFERENCES `store_order_attemp` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);

        INSERT INTO store_order_attemp_delivery (
      
            SELECT 
                a.id as order_attemp_id,
                file_id,
                tracking_number,
                tracking_shipper,
                send_message_at
                
            FROM store_order_attemp a
            LEFT JOIN store_order_delivery d ON d.order_id = a.order_id 
        );
        
        ALTER TABLE `store_order_delivery` 
        DROP FOREIGN KEY `postal_label_file_fk`;
        ALTER TABLE `store_order_delivery` 
        DROP COLUMN `send_message_at`,
        DROP COLUMN `tracking_shipper`,
        DROP COLUMN `tracking_number`,
        DROP COLUMN `file_id`,
        DROP INDEX `postal_label_file_fk_idx` ;
        ");
    }





    public function down()
    {
        echo "m161205_094213_2730_order_delivery cannot be reverted.\n";

        return false;
    }

}
