<?php

use yii\db\Migration;

class m160524_110831_psdbfix1 extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps`  CHANGE COLUMN `is_active` `is_active` TINYINT(1) NOT NULL DEFAULT 1 ;");
    }

    public function down()
    {
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
