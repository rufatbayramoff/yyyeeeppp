<?php

use common\modules\payment\fee\FeePercentCondition;
use common\modules\payment\fee\FeePercentRuleForPrice;
use lib\money\Money;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m181106_081005_5814_store_order_user_session_id
 */
class m181106_081005_5814_store_order_user_session_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('UPDATE `store_order` SET user_session_id=1 WHERE `store_order`.`user_session_id` not in (SELECT `user_session`.id FROM `user_session`)');
        $this->addForeignKey('fk_store_order_user_session_id', 'store_order', 'user_session_id', 'user_session', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
