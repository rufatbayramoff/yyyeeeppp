<?php

use common\components\DateHelper;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m181211_104111_add_index_user_login_log
 */
class m181211_104111_add_index_user_login_log extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `user_login_log` ADD INDEX(`created_at`)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
