<?php

use yii\db\Migration;

class m161027_100820_2797_new_test_order_email_template extends Migration
{
    public function up()
    {
        $this->execute("
        INSERT INTO `email_template`(
    `id`,
    `code`,
    `group`,
    `language_id`,
    `title`,
    `description`,
    `updated_at`,
    `template_html`,
    `template_text`
) VALUES (
    NULL,
    'ps.testorder.new',
    'service',
    'en-US',
    'You have a New Test Order',
    NULL,
    NULL,
    'Hi %psName%! 
    
Thank you for joining Treatstock as a print service!

Would you like to take your new print service for a test run? We have created a new test order for you so you can familiarize yourself with how the orders process will actually work with your printers. To accept the test order click the Get started button.

<a href=\"%newOrdersLink%\">Get started</a>

Best regards,
Treatstock ',
    'Hi %psName%! 
Thank you for joining Treatstock!
We have created a new test order for you so you can see how the orders process will actually work with your printers. Go to your orders page to accept %newOrdersLink%.'
 );
");

    }

    public function down()
    {
        $this->delete('email_template', ['code' => 'ps.testorder.new']);
        return true;
    }
}
