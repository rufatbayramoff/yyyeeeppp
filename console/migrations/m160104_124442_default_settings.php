<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_124442_default_settings extends Migration
{
    public function up()
    {
        $group = app('db')->createCommand('SELECT * FROM system_setting_group WHERE title=:title')
            ->bindValue('title', 'store')
            ->queryOne(); 
        $groupId  = $group['id'];
        try{
            $this->execute("INSERT INTO `system_setting`
                (`group_id`,`key`,`value`, `created_user_id`,`updated_user_id`,`description`,`json`,`is_active`) VALUES
                (".$groupId. ",'min_free_price_standard', '20', 2, NULL, 'Minimum price for standart shipping - USD','',1);");

            $this->execute("INSERT INTO `system_setting`
                (`group_id`,`key`,`value`, `created_user_id`,`updated_user_id`,`description`,`json`,`is_active`) VALUES
                (".$groupId. ",'min_free_price_intl', '120', 2, NULL, 'Minimum price for international shipping - USD','',1);");

            $this->execute("INSERT INTO `system_setting`
                (`group_id`,`key`,`value`, `created_user_id`,`updated_user_id`,`description`,`json`,`is_active`) VALUES
                (".$groupId. ",'ts_fee', '0.15', 2, NULL, 'Our fee (15%)','',1);");
        }catch(\Exception $e){
            echo $e->getMessage();
        }
        return true;
    }

    public function down()
    {
        $group = app('db')->createCommand('SELECT * FROM system_setting_group WHERE title=:title')
            ->bindValue('title', 'store')
            ->queryOne(); 
        $groupId = $group['id'];
        $this->execute("DELETE FROM `system_setting` WHERE `key` = 'min_free_price_intl' AND group_id=$groupId;");
        $this->execute("DELETE FROM `system_setting` WHERE `key` = 'min_free_price_standard' AND group_id=$groupId;");
        $this->execute("DELETE FROM `system_setting` WHERE `key` = 'ts_fee' AND group_id=$groupId;");
    } 
}
