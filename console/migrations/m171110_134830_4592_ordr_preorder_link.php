<?php

use yii\db\Migration;

class m171110_134830_4592_ordr_preorder_link extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `store_order` 
            DROP FOREIGN KEY `fk_store_order_8`;
            ALTER TABLE `store_order` 
            CHANGE COLUMN `preorder_id` `cnc_preorder_id` INT(11) NULL DEFAULT NULL ;
            ALTER TABLE `store_order` 
            ADD CONSTRAINT `fk_store_order_8`
              FOREIGN KEY (`cnc_preorder_id`)
              REFERENCES `cnc_preorder` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");

        $this->execute("
            ALTER TABLE `store_order` 
            ADD COLUMN `preorder_id` INT(11) NULL AFTER `cnc_preorder_id`,
            ADD INDEX `fk_store_order_9_idx` (`preorder_id` ASC);
            ALTER TABLE `store_order` 
            ADD CONSTRAINT `fk_store_order_9`
              FOREIGN KEY (`preorder_id`)
              REFERENCES `preorder` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");

        $this->execute("
            ALTER TABLE `store_order` 
            ADD CONSTRAINT `fk_store_order_preorder`
              FOREIGN KEY (`preorder_id`)
              REFERENCES `preorder` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
    }

    public function safeDown()
    {
        echo "m171110_134830_4592_ordr_preorder_link cannot be reverted.\n";

        return false;
    }
}
