<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190607_202321_6584_remove_ps_is_active
 */
class m190607_202321_6584_remove_ps_is_active extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('ps','is_active');
    }

    public function safeDown()
    {
        return false;
    }
}