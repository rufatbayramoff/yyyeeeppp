<?php

use yii\db\Migration;
use yii\db\Query;

class m170116_111521_3659_fix_is_hidden_in_store extends Migration
{
    public function up()
    {
        $this->addColumn('store_unit', 'is_hidden_in_store', 'BOOLEAN NOT NULL DEFAULT \'0\'');
        $allModels = (new Query())
            ->select('*')
            ->from('model3d')->all();
        foreach ($allModels as $model3d) {
            $this->update('store_unit', ['is_hidden_in_store' => $model3d['is_hidden_in_store']], 'model3d_id=' . $model3d['id']);
        }
    }

    public function down()
    {
        echo 'no roll back';
        return false;
    }
}
