<?php

use yii\db\Migration;

/**
 * Class m180810_143521_5559_preorder_work_qty_float
 */
class m180810_143521_5559_preorder_work_qty_float extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('preorder', 'qty', 'decimal(15,4) null');
        $this->alterColumn('preorder_work', 'qty', 'decimal(15,4) null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
