<?php

use yii\db\Migration;

class m170322_121744_3965_log_users extends Migration
{
    public function up()
    {
        $this->addColumn('store_order_history', 'user_id', 'integer NULL');
        $this->addColumn('store_order_history', 'admin_user_id', 'integer NULL');

    }

    public function down()
    {
        $this->dropColumn('store_order_history', 'user_id');
        $this->dropColumn('store_order_history', 'admin_user_id');
        return true;
    }

}
