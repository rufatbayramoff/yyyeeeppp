<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m201021_102821_7679_view_orders_list
 */
class m201021_102821_7679_view_orders_list extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('drop view if exists view_orderlist');
        $this->execute(<<<SQL
CREATE VIEW `view_orderlist` AS
    SELECT 
        `o`.`id` AS `order_id`,
        `o`.`preorder_id` AS `preorder_id`,
        `o`.`user_id` AS `buyer_id`,
        `o`.`billed_at` AS `billed_at`,
        `product_common`.`user_id` AS `designer_id`,
        `o`.`created_at` AS `created_at`,
        `atdates`.`accepted_date` AS `accepted_at`,
        `oh2`.`created_at` AS `ready_sent_at`,
        `atdates`.`delivered_at` AS `delivered_at`,
        `atdates`.`shipped_at` AS `shipped_at`,
        `atdates`.`received_at` AS `received_at`,
        `geoc`.`iso_code` AS `to_country`,
        `shipto`.`city` AS `to_city`,
        `o`.`comment` as `comment`,
        `psp`.`id` AS `printer_id`,
        `psp`.`title` AS `printer_title`,
        `geoc2`.`iso_code` AS `ps_country`,
        `dt`.`title` AS `delivery_type`,
        CONCAT(`atdl`.`tracking_number`,
                ' - ',
                `atdl`.`tracking_shipper`) AS `delivery_opts`,
        `pi`.`total_amount` AS `total_amount`,
        `pi`.`currency` AS `currency`
    FROM
        ((((((((((((((((((`store_order` `o`
        LEFT JOIN `payment_invoice` `pi` ON ((`pi`.`uuid` = `o`.`primary_payment_invoice_uuid`)))
        LEFT JOIN `store_order_attemp` `attemp` ON ((`attemp`.`id` = `o`.`current_attemp_id`)))
        LEFT JOIN `store_order_attemp_dates` `atdates` ON ((`atdates`.`attemp_id` = `attemp`.`id`)))
        LEFT JOIN `store_order_attemp_delivery` `atdl` ON ((`atdl`.`order_attemp_id` = `attemp`.`id`))))
        LEFT JOIN `store_order_item` `soi` ON ((`soi`.`order_id` = `o`.`id`)))
        LEFT JOIN `store_unit` `su` ON ((`su`.`id` = `soi`.`unit_id`)))
        LEFT JOIN `model3d` `m3` ON ((`m3`.`id` = `su`.`model3d_id`)))
        LEFT JOIN `product_common` ON `product_common`.`uid`= `m3`.`product_common_uid`)
        LEFT JOIN `user_address` `shipto` ON ((`shipto`.`id` = `o`.`ship_address_id`)))
        LEFT JOIN `geo_country` `geoc` ON ((`geoc`.`id` = `shipto`.`country_id`)))
        LEFT JOIN `company_service` `psm` ON ((`psm`.`id` = `attemp`.`machine_id`)))
        LEFT JOIN `ps_printer` `psp` ON ((`psp`.`id` = `psm`.`ps_printer_id`)))
        LEFT JOIN `user_location` `l` ON ((`l`.`id` = `psm`.`location_id`)))
        LEFT JOIN `geo_country` `geoc2` ON ((`geoc2`.`id` = `l`.`country_id`)))
        LEFT JOIN `store_order_history` `oh2` ON (((`oh2`.`order_id` = `o`.`id`)
            AND (`oh2`.`action_id` = 'order_status')
            AND (`oh2`.`comment` = 'old[printed] new[ready_send]'))))
        LEFT JOIN `delivery_type` `dt` ON ((`dt`.`id` = `o`.`delivery_type_id`)))
        LEFT JOIN `store_order_history` `oh3` ON (((`oh3`.`order_id` = `o`.`id`)
            AND (`oh3`.`action_id` = 'order_status')
            AND (`oh3`.`comment` = 'old[printed] new[ready_send]'))))
    WHERE
        (`o`.`user_id` <> 1)
        ORDER BY `o`.`created_at` desc;
SQL
        );

    }

    public function safeDown()
    {
    }
}