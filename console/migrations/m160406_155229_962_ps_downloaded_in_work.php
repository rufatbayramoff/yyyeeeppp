<?php

use yii\db\Migration;

class m160406_155229_962_ps_downloaded_in_work extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps` ADD `dont_show_download_policy_modal` TINYINT  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `phone_country_iso`");
        $this->insert('system_setting', [
            'group_id' => 9,
            'key' => 'max_downloaded_in_work',
            'value' => 5,
            'created_user_id' => 1,
            'description' => 'Max downloaded file in work (issue 962)',
        ]);
    }

    public function down()
    {
        $this->dropColumn('ps', 'dont_show_download_policy_modal');
        return true;
    }
}
