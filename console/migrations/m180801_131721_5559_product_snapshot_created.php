<?php

use yii\db\Migration;

/**
 * Class m180801_131721_5559_product_snapshot_created
 */
class m180801_131721_5559_product_snapshot_created extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_snapshot', 'created_at', 'datetime null');
        $this->update('product_snapshot', ['created_at'=>\common\components\DateHelper::now()]);
        $this->alterColumn('product_snapshot', 'created_at', 'datetime not null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_snapshot', 'created_at');
    }
}
