<?php

use yii\db\Migration;

class m160606_083706_user_change_username extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user`  ADD COLUMN `can_change_username` TINYINT(1) NULL DEFAULT 0 AFTER `username`;");
    }

    public function down()
    {        
        $this->dropColumn('user', 'can_change_username');     
    }
 
}
