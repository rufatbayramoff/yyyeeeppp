<?php

use yii\db\Migration;

class m171226_145309_5114_auto_resource extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `affiliate_resource` 
ADD COLUMN `resource_type` CHAR(15) NOT NULL DEFAULT \'any\' AFTER `description`;
');
    }

    public function safeDown()
    {
        $this->dropColumn('affiliate_resource', 'resource_type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171226_145309_5114_auto_resource cannot be reverted.\n";

        return false;
    }
    */
}
