<?php

use yii\db\Migration;

class m171110_124328_5007_order_without_delivert extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `store_order` 
            DROP FOREIGN KEY `fk_store_order_1`,
            DROP FOREIGN KEY `fk_store_order_2`;
            ALTER TABLE `store_order` 
            CHANGE COLUMN `ship_address_id` `ship_address_id` INT(11) NULL ,
            CHANGE COLUMN `bill_address_id` `bill_address_id` INT(11) NULL ;
            ALTER TABLE `store_order` 
            ADD CONSTRAINT `fk_store_order_1`
              FOREIGN KEY (`bill_address_id`)
              REFERENCES `user_address` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION,
            ADD CONSTRAINT `fk_store_order_2`
              FOREIGN KEY (`ship_address_id`)
              REFERENCES `user_address` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");

    }

    public function safeDown()
    {
        echo "m171110_124328_5007_order_without_delivert cannot be reverted.\n";

        return false;
    }
}
