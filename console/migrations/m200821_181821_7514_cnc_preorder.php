<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200821_181821_7514_cnc_preorder
 */
class m200821_181821_7514_cnc_preorder extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->execute('ALTER TABLE cnc_preorder DROP FOREIGN KEY fk_cnc_preorder_1;');
        $this->execute('ALTER TABLE cnc_preorder DROP FOREIGN KEY fk_cnc_preorder_2;');
        $this->execute('ALTER TABLE cnc_preorder DROP FOREIGN KEY fk_cnc_preorder_3;');
        $this->execute('ALTER TABLE cnc_preorder DROP FOREIGN KEY fk_cnc_preorder_4;');
        $this->dropForeignKey('fk_store_order_8', 'store_order');
        $this->dropTable('cnc_preorder_file');
        $this->dropTable('cnc_preorder_preset');
        $this->dropTable('cnc_preorder_work');
        $this->delete('cnc_preorder');
        $this->dropColumn('store_order', 'cnc_preorder_id');
        $this->dropTable('cnc_preorder');

        $this->execute('CREATE TABLE `preorder_cnc_work` (
    `preorder_id` int(11) NOT NULL,
  `preorder_work_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `instrument` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
        $this->execute('ALTER TABLE `preorder_cnc_work`
  ADD UNIQUE KEY `preorder_work_id` (`preorder_work_id`),
  ADD KEY `preorder_id` (`preorder_id`);');
        $this->execute('ALTER TABLE `preorder_cnc_work`
  ADD CONSTRAINT `fk_preorder_cnc_work_preorder_id` FOREIGN KEY (`preorder_id`) REFERENCES `preorder` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_preorder_cnc_work_preorder_work_id` FOREIGN KEY (`preorder_work_id`) REFERENCES `preorder_work` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;');
        $this->delete('email_template', ['code'=>'ps.cnc.order.new']);
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}