<?php

use yii\db\Migration;

class m160526_162836_fix_user_timzone extends Migration
{
    public function up()
    {
        $this->execute("UPDATE user_profile SET timezone_id = 'America/Los_Angeles' WHERE timezone_id = '0'");
    }

    public function down()
    {
        echo "m160526_162836_fix_user_timzone cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
