<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_104836_mail_template_change_email extends Migration
{
    public function up()
    {
         $this->execute("INSERT INTO `email_template` (`code`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`) VALUES
('createChangeEmailRequest', 'en-US', 'Mail to old email on create change email request', NULL, NULL, '<p>Hello %username%</p>\n<p>Sameone, may be you, create request for change email of your account from %oldEmail% to %newEmail%. </p>\n<p>If it was not you - contact with support.</p>', 'Hello %username%!\nSameone, may be you, create request for change email of your account from %oldEmail% to %newEmail%.\nIf it was not you - contact with support.'),
('confirmChangeEmailRequest', 'en-US', 'Mail to new email for confirm new email', NULL, NULL, '<p>Hello %username%,</p>\n\n<p>Follow the link below to confirm your email, if you requested this operation on TS:</p>\n<a href=\"%link%\">%link%</a>', 'Hello %username%,\nFollow the link below to confirm your email, if you requested this operation on TS:\n%link%')"); 
    }

    public function down()
    {
        echo "m151029_104836_mail_template_createChamngeEmailRequest cannot be reverted.\n";

        return false;
    }
}
