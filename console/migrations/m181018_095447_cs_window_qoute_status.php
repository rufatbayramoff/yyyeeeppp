<?php

use yii\db\Migration;

/**
 * Class m181018_095447_cs_window_qoute_status
 */
class m181018_095447_cs_window_qoute_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('cs_window_quote', 'status', "enum ('new', 'viewed', 'deleted', 'resolved') default 'new' not null");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('cs_window_quote', 'status', "enum ('wait_confirm', 'new', 'accepted', 'rejected', 'deleted') default 'new' not null");
    }
}
