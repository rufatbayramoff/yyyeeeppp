<?php

use yii\db\Migration;

class m160331_125427_1253_templates_group extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `email_template` ADD `group` VARCHAR(30)  NOT NULL  DEFAULT 'system'  AFTER `code`;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `email_template` DROP `group`;");
        return true;
    }
}
