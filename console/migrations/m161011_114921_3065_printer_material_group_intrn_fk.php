<?php

use yii\db\Migration;
use yii\db\Query;

class m161011_114921_3065_printer_material_group_intrn_fk extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey('printer_material_group_intl_fk', 'printer_material_group_intl', 'group_id', 'printer_material_group', 'id');
        $this->addColumn('printer_material_group_intl', 'short_description', 'string');
        $this->addColumn('printer_material_group_intl', 'long_description', 'string');
        $this->addColumn('printer_material_group', 'priority', 'integer NOT NULL DEFAULT 0');
        $this->addColumn('printer_material_group', 'created_at', 'timestamp NULL DEFAULT NULL');
        $this->addColumn('printer_material_group', 'updated_at', 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
    }

    public function safeDown()
    {
        $this->dropForeignKey('printer_material_group_intl_fk', 'printer_material_group_intl');
        $this->dropColumn('printer_material_group_intl', 'short_description');
        $this->dropColumn('printer_material_group_intl', 'long_description');
        $this->dropColumn('printer_material_group', 'priority');
        $this->dropColumn('printer_material_group', 'created_at');
        $this->dropColumn('printer_material_group', 'updated_at');
        return true;
    }

}
