<?php

use yii\db\Migration;

/**
 * Class m180718_132823_5715_store_order_items
 */
class m180718_132823_5715_store_order_items extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_item', 'price_fee', 'decimal(10,4) after price');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('store_order_item', 'price_fee');
    }
}
