<?php

use yii\db\Migration;

class m171025_112434_4887_cnc_decline_reasons extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_cnc_preorder', 'CNC machine is unavailable', NULL, '1' );
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_cnc_preorder', 'The chosen material is absent', NULL, '1' );
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_cnc_preorder', 'The model is not suitable for CNC machining', NULL, '1' );
            INSERT INTO `system_reject`(`id`,`group`,`title`,`description`,`is_active`) VALUES ( NULL, 'decline_cnc_preorder', 'Other (Please specify)', NULL, '1' );
        ");
    }

    public function safeDown()
    {
        $this->delete('system_reject', ['group' =>'decline_cnc_preorder']);
        return true;
    }
}
