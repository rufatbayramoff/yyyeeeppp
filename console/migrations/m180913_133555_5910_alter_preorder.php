<?php

use yii\db\Migration;

/**
 * Class m180913_133555_5910_alter_preorder
 */
class m180913_133555_5910_alter_preorder extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `preorder` 
            ADD COLUMN `ship_to_id` INT NULL AFTER `product_snapshot_uuid`,
            ADD INDEX `fk_preorder_2_idx` (`ship_to_id` ASC);
            ALTER TABLE `preorder` 
            ADD CONSTRAINT `fk_preorder_2`
              FOREIGN KEY (`ship_to_id`)
              REFERENCES `user_address` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_preorder_2', 'preorder');
        $this->dropColumn('preorder', 'ship_to_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180913_133555_5910_alter_preorder cannot be reverted.\n";

        return false;
    }
    */
}
