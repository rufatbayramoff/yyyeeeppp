<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180827_105921_5814_payment_invoice_fill
 */
class m180827_105921_5814_payment_detail_operation extends Migration
{
    /**
     * @param $paymentDetail
     * @return string
     * @throws Exception
     */
    protected function createPaymentOperation($paymentDetail)
    {
        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_detail_operation')->where('uuid="' . $operationUuid . '"')->one();
        } while($checkUuid);

        $this->insert('payment_detail_operation', ['uuid' => $operationUuid, 'payment_id' => $paymentDetail['payment_id'], 'created_at' => $paymentDetail['created_at']]);
        return $operationUuid;
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $paymentDetails = (new Query())->select('*')->from('payment_detail')->orderBy('payment_id,id')->all();

        $createdAt = null;
        $lastPaymentId = null;
        $operationUuid = '';
        $paymentSum = 0;
        foreach ($paymentDetails as $paymentDetail) {
            if ($paymentDetail['id'] == 2402) {
                sleep(0);
            }
            if ($createdAt) {
                if (($paymentSum != (0-$paymentDetail['amount'])) || ($lastPaymentId !== $paymentDetail['payment_id'])) {
                    if ($paymentSum != 0) {
                        die('Invalid payment summ: ' . $paymentDetail['id']);
                    }
                    // next payment operation
                    $createdAt = $paymentDetail['created_at'];
                    $lastPaymentId = $paymentDetail['payment_id'];
                    $operationUuid = $this->createPaymentOperation($paymentDetail);
                    $this->update('payment_detail', ['payment_detail_operation_uuid' => $operationUuid], 'id=' . $paymentDetail['id']);
                    $paymentSum = $paymentDetail['amount'];
                } else {
                    // add next payment
                    $this->update('payment_detail', ['payment_detail_operation_uuid' => $operationUuid], 'id=' . $paymentDetail['id']);
                    $paymentSum += $paymentDetail['amount'];
                }

            } else {
                // First start
                $createdAt = $paymentDetail['created_at'];
                $lastPaymentId = $paymentDetail['payment_id'];
                $operationUuid = $this->createPaymentOperation($paymentDetail);
                $this->update('payment_detail', ['payment_detail_operation_uuid' => $operationUuid], 'id=' . $paymentDetail['id']);
                $paymentSum = $paymentDetail['amount'];
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
