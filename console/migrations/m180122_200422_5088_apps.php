<?php

use yii\db\Migration;

class m180122_200422_5088_apps extends Migration
{
    public function safeUp()
    {
        $this->execute(
            '
CREATE TABLE `promo_app` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(50) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `logo_file_id` INT(11) DEFAULT NULL,
  `is_beta` TINYINT(1) NOT NULL DEFAULT \'0\',
  `url` VARCHAR(1024) DEFAULT NULL,
  `short_descr` TEXT,
  `info` TEXT,
  `instructions` TEXT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `logo_file_id` (`logo_file_id`),
  CONSTRAINT `fk_apps_logo_file_id` FOREIGN KEY (`logo_file_id`) REFERENCES `file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'
        );

        $this->execute(
            '
CREATE TABLE `promo_app_intl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_descr` text NOT NULL,
  `info` text NOT NULL,
  `instructions` text NOT NULL,
  `lang_iso` char(7) NOT NULL,
  `is_active` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`),
  KEY `lang_iso` (`lang_iso`),
  CONSTRAINT `fk_promo_app_intl_lang_iso` FOREIGN KEY (`lang_iso`) REFERENCES `system_lang` (`iso_code`),
  CONSTRAINT `fk_promo_app_intl_model_id` FOREIGN KEY (`model_id`) REFERENCES `promo_app` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'
        );
    }

    public function safeDown()
    {
        $this->dropTable('promo_app_intl');
        $this->dropTable('promo_app');
    }

}
