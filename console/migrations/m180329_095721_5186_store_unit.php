<?php

use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m180329_095721_5186_products
 */
class m180329_095721_5186_store_unit extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('model3d', 'price_per_produce', "decimal(6,2) not null default '0.0'");
        $this->addColumn('model3d', 'price_currency', "char(5) not null default 'usd'");
        $this->addColumn('model3d', 'moderated_at', 'timestamp null after published_at');
        $this->addColumn('model3d_replica', 'price_per_produce', "decimal(6,2) not null default '0.0'");
        $this->addColumn('model3d_replica', 'price_currency', "	char(5) not null default 'usd'");
        $this->addColumn('model3d', 'product_status',
            "enum('draft', 'published_public', 'published_updated', 'published_directly', 'publish_pending', 'rejected', 'private') default 'draft' after status");
        $this->addColumn('model3d', 'is_active', "tinyint(1) not null default '1' after status");
        $this->addColumn('model3d_replica', 'is_active', "tinyint(1) not null default '1' after status");

        $this->execute('UPDATE model3d, store_unit SET `model3d`.`price_per_produce`=`store_unit`.`price_per_print` WHERE model3d.id=store_unit.model3d_id and `store_unit`.`price_per_print`>0');
        $this->execute('UPDATE model3d, store_unit SET `model3d`.`moderated_at`=`store_unit`.`moderated_at` WHERE model3d.id=store_unit.model3d_id');
        $this->execute('UPDATE model3d_replica, model3d SET `model3d_replica`.`price_per_produce`=`model3d`.`price_per_produce` WHERE model3d_replica.original_model3d_id=model3d.id');
        $this->execute('UPDATE model3d, model3d  as model3dOrg SET `model3d`.`price_per_produce`=`model3dOrg`.`price_per_produce` WHERE model3d.original_model3d_id=model3dOrg.id');


        $this->dropForeignKey('fk_store_unit_2', 'store_unit');
        $this->dropForeignKey('fk_custom_code_2', 'store_unit');

        $this->dropColumn('store_unit', 'custom_code_id');
        $this->dropColumn('store_unit', 'price');
        $this->dropColumn('store_unit', 'price_per_print');
        $this->dropColumn('store_unit', 'price_currency');
        $this->dropColumn('store_unit', 'created_at');
        $this->dropColumn('store_unit', 'updated_at');
        $this->dropColumn('store_unit', 'deleted_at');
        $this->dropColumn('store_unit', 'moderated_at');
        $this->dropColumn('store_unit', 'license_id');
        $this->dropColumn('store_unit', 'license_config');
        $this->dropColumn('store_unit', 'is_active');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

    }
}
