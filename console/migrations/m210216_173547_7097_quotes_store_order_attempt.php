<?php

use yii\db\Migration;
use yii\db\Query;

class m210216_173547_7097_quotes_store_order_attempt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_attemp','preorder_id', 'INT(11)  after order_id');
        $this->createIndex('indx_store_order_attemp_preorder_id', 'store_order_attemp', 'preorder_id');
        $this->addForeignKey(
            'fk_store_order_attemp_preorder_id',
            'store_order_attemp',
            'preorder_id',
            'preorder',
            'id'
        );

        $this->alterColumn('store_order_attemp', 'order_id', 'int(11) null');


        $fixedPreorders = [];
        $storeOrderAttempts = (new Query())->select('*')->from('store_order_attemp')->all();
        foreach ($storeOrderAttempts as $storeOrderAttempt) {
            $order = (new Query())->select('*')->from('store_order')->where(
                [
                    'id'=>$storeOrderAttempt['order_id']
                ]
            )->one();
            if ($order['preorder_id']) {
                $this->update('store_order_attemp', ['preorder_id' => $order['preorder_id']], ['id' => $storeOrderAttempt['id']]);
                $fixedPreorders[$order['preorder_id']] = $order['preorder_id'];
            }
        }

        $preorders = (new Query())->select('*')->from('preorder')->all();
        foreach ($preorders as $preorder) {
            if (array_key_exists($preorder['id'], $fixedPreorders)) {
                continue;
            }

            $status = 'new';
            if ($preorder['status'] === 'rejected' || $preorder['status'] === 'deleted') {
                $status = 'canceled';
            }

            $insertData = [
                'preorder_id' => $preorder['id'],
                'status'      => $status,
                'created_at'  => $preorder['created_at'],
                'ps_id'       => $preorder['ps_id'],
            ];
            $order = (new Query())->select('*')->from('store_order')->where(['store_order.preorder_id'=>$preorder['id']])->one();
            if ($order) {
                $insertData['order_id'] = $order['id'];
            }
            $this->insert('store_order_attemp', $insertData);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
