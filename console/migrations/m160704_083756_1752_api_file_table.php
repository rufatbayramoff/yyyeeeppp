<?php

use yii\db\Migration;

class m160704_083756_1752_api_file_table extends Migration
{
    public function up()
    {
        $this->execute(
            "CREATE TABLE `api_file` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `file_id` int(11) DEFAULT NULL,
              `user_id` int(11) NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `affiliate_price` decimal(10,4) DEFAULT '0.0000',
              `affiliate_currency` char(5) DEFAULT 'USD',
              `status` char(15) NOT NULL DEFAULT 'new',
              PRIMARY KEY (`id`),
              UNIQUE KEY `api_file_user_uq` (`file_id`,`user_id`),
              KEY `fk_api_file_2_idx` (`user_id`),
              CONSTRAINT `fk_api_file_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_api_file_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB"
        );

        $this->execute("ALTER TABLE `model3d` ADD COLUMN `source` CHAR(15) NULL DEFAULT 'website' AFTER `category_id`;");
    }

    public function down()
    {
        $this->truncateTable('api_file');
        $this->dropTable('api_file');

        $this->dropColumn('model3d', 'source');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
