<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\components\UuidHelper;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m201106_101720_7874_cutting_pack
 */
class m201106_101720_7874_cutting_pack extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createIndex('cutting_pack_user_session_indx', 'cutting_pack', 'user_session_id');
        $this->createIndex('cutting_pack_user_ind', 'cutting_pack', 'user_id');
        $this->addForeignKey('cutting_pack_user_session_fk', 'cutting_pack', 'user_session_id', 'user_session', 'id');
        $this->addForeignKey('cutting_pack_user_fk', 'cutting_pack', 'user_id', 'user', 'id');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}
