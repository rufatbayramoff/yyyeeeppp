<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_112553_ps_printer_history_data extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer_history` ADD `data` TEXT  NULL  AFTER `comment`;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `ps_printer_history` DROP `data`;");
        return true;
    }
}
