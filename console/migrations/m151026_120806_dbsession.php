<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_120806_dbsession extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `user_session` (
            `id` char(40) NOT NULL,
            `expire` int(11) DEFAULT NULL,
            `data` blob,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function down()
    {
        $this->dropTable('user_session');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
