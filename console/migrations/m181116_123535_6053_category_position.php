<?php

use yii\db\Migration;

/**
 * Class m181116_123535_6053_category_position
 */
class m181116_123535_6053_category_position extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181116_123535_6053_category_position cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181116_123535_6053_category_position cannot be reverted.\n";

        return false;
    }
    */
}
