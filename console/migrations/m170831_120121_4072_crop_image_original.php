<?php

use yii\db\Migration;

class m170831_120121_4072_crop_image_original extends Migration
{
    public function up()
    {
        $this->addColumn('model3d_img', 'original_file_id', 'int(11) null');
        $this->addForeignKey('model3d_img_original_file_fk', 'model3d_img', 'original_file_id', 'file', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('model3d_img_original_file_fk', 'model3d_img');
        $this->dropColumn('model3d_img', 'original_file_id');
    }
}
