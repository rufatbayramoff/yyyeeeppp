<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%ps_catalog}}`.
 */
class m210819_103435_8753_add_country_rating_column_to_ps_catalog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ps_catalog}}', 'country_rating', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ps_catalog}}', 'country_rating');
    }
}
