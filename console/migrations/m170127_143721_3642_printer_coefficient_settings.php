<?php

use yii\db\Migration;
use yii\db\Query;

class m170127_143721_3642_printer_coefficient_settings extends Migration
{
    public function up()
    {
        $coeffConfiguration = [
            'rating'        => [
                [
                    'begin'       => 0,
                    'end'         => 3,
                    'coefficient' => 1.3
                ],
                [
                    'begin'       => 3,
                    'end'         => 4,
                    'coefficient' => 1.1
                ],
                [
                    'begin'       => 3,
                    'end'         => 5,
                    'coefficient' => 1
                ],

            ],
            'certification' => [
                [
                    'value'       => 'no',
                    'coefficient' => 1.2
                ],
                [
                    'value'       => 'common',
                    'coefficient' => 1.1
                ],
                [
                    'value'       => 'professional',
                    'coefficient' => 1
                ],
            ],
            'ordersCount'   => [
                [
                    'begin'       => 30,
                    'end'         => 40,
                    'coefficient' => 1.05
                ],
                [
                    'begin'       => 20,
                    'end'         => 30,
                    'coefficient' => 1.1
                ],
                [
                    'begin'       => 10,
                    'end'         => 20,
                    'coefficient' => 1.2
                ],
                [
                    'begin'       => 0,
                    'end'         => 10,
                    'coefficient' => 1.3
                ],
            ],
            'distance'      => [
                [
                    'begin'       => 1000,
                    'end'         => 100000000,
                    'coefficient' => 1.3
                ],
                [
                    'begin'       => 150,
                    'end'         => 1000,
                    'coefficient' => 1.2
                ],
                [
                    'begin'       => 50,
                    'end'         => 150,
                    'coefficient' => 1.15
                ],
                [
                    'begin'       => 20,
                    'end'         => 50,
                    'coefficient' => 1.1
                ],
                [
                    'begin'       => 10,
                    'end'         => 20,
                    'coefficient' => 1.05
                ],
                [
                    'begin'       => 0,
                    'end'         => 10,
                    'coefficient' => 1
                ],
            ]
        ];
        $this->insert(
            'system_setting',
            [
                'group_id'        => '9',
                'key'             => 'sortCoefficients',
                'value'           => 'json',
                'json'            => json_encode($coeffConfiguration, JSON_PRETTY_PRINT),
                'created_at'      => date('Y-m-d H:i:s'),
                'updated_at'      => date('Y-m-d H:i:s'),
                'created_user_id' => null,
                'updated_user_id' => null,
                'description'     => 'Printer list sorting coefficients configuration.',
                'is_active'       => 1,
            ]
        );
    }

    public function down()
    {
        $this->delete(
            'system_setting',
            [
                'group_id' => '9',
                'key'      => 'sortCoefficients',
            ]
        );
    }
}
