<?php

use yii\db\Migration;

/**
 * Class m180827_121832_add_new_field_for_store_order_review
 */
class m180827_121832_add_new_field_for_store_order_review extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_review', 'answer', 'text NULL after comment');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('store_order_review', 'answer');
    }
}
