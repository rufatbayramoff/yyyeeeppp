<?php

use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m211222_151853_7422_thingiverse_import
 */
class m211222_151853_7422_thingiverse_import extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute("
CREATE TABLE `thingiverse_import` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_at` date NOT NULL,
  `thing_id` int NOT NULL,
  `info` json NOT NULL,
  `model3d_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model3d_id` (`model3d_id`) USING BTREE,
  KEY `thing_id` (`thing_id`),
  CONSTRAINT `fk_thingiverse_import_model_id` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
");
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
