<?php

use yii\db\Migration;
use yii\db\Query;

class m201218_132433_8010_inactive_models extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = 'SELECT model3d.product_common_uid, count(model3d_part.id) as cnt_part FROM `model3d`
LEFT JOIN product_common on product_common.uid=model3d.product_common_uid
LEFT JOIN model3d_part on model3d_part.model3d_id = model3d.id
WHERE product_common.is_active=1
GROUP BY model3d.product_common_uid
HAVING cnt_part=0
';
        $model3ds= \Yii::$app->db->createCommand($sql)->queryAll();

        foreach ($model3ds as $model3d) {
            $this->update('product_common', ['is_active'=>0], "uid='".$model3d['product_common_uid']."'");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
