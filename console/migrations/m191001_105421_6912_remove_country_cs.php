<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m191001_105421_6912_remove_country_cs
 */
class m191001_105421_6912_remove_country_cs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('user_address', ['country_id'=>252],'country_id=250');
        $this->delete('user_address', 'country_id=250');

    }

    public function safeDown()
    {

    }
}