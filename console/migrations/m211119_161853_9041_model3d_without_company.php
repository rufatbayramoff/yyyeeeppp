<?php

use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m211119_161853_9041_model3d_without_company
 */
class m211119_161853_9041_model3d_without_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $psExistsList = Yii::$app->db->createCommand("SELECT ps.id as ps_id, `product_common`.uid as product_uid
FROM `product_common`
LEFT JOIN `user` ON `product_common`.`user_id` = `user`.`id`
LEFT JOIN `ps` ON `ps`.`user_id` = `user`.`id`
WHERE (product_common.company_id is null)
AND (`product_common`.`product_status` IN ('published_public', 'published_updated'))
AND (`product_common`.`is_active`=1)
AND (`user`.`status` IN (10, 1))
AND (ps.id is not null)")->queryAll();

        foreach ($psExistsList as $psExists) {
            $this->update('product_common', ['company_id' => $psExists['ps_id']], ['uid' => $psExists['product_uid']]);
        }

        $productInfoList   = Yii::$app->db->createCommand("SELECT `product_common`.uid as product_uid, user.*
FROM `product_common`
LEFT JOIN `user` ON `product_common`.`user_id` = `user`.`id`
LEFT JOIN `product` ON `product_common`.`uid` = `product`.`product_common_uid`
LEFT JOIN `product_site_tag` ON `product`.`uuid` = `product_site_tag`.`product_uuid`
LEFT JOIN `model3d` ON `product_common`.`uid` = `model3d`.`product_common_uid`
LEFT JOIN `model3d_tag` ON `model3d`.`id` = `model3d_tag`.`model3d_id`
WHERE (product_common.company_id is null)
AND (`product_common`.`product_status` IN ('published_public', 'published_updated'))
AND (`product_common`.`is_active`=1)
AND (`user`.`status` IN (10, 1)) GROUP BY `product_common`.`uid`
")->queryAll();
        $insertedCompanies = [];
        foreach ($productInfoList as $productInfo) {
            $productUid = $productInfo['product_uid'];
            $userId     = $productInfo['id'];
            $username   = $productInfo['username'];
            if (array_key_exists($userId, $insertedCompanies)) {
                continue;
            }
            $insertedCompanies[$userId] = true;
            $this->insert('ps', [
                'user_id'          => $userId,
                'title'            => $username . ' Design',
                'moderator_status' => 'checked',
                'phone_status'     => 'new',
                'created_at'       => '2021-11-19 17:01:01',
                'updated_at'       => '2021-11-19 17:01:01',
                'url'              => $username
            ]);
            $companyId = $this->db->getLastInsertID();
            $this->update('product_common', ['company_id' => $companyId], ['user_id' => $userId]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
