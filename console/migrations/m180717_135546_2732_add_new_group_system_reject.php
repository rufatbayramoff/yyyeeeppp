<?php

use yii\db\Migration;

/**
 * Class m180717_135546_2732_add_new_group_system_reject
 */
class m180717_135546_2732_add_new_group_system_reject extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rejects = [
            [
                'group' => 'user_delete_request',
                'title' => 'I did not find what I was looking for'
            ],
            [
                'group' => 'user_delete_request',
                'title' => 'I found the website confusing. Please explain:'
            ],
            [
                'group' => 'user_delete_request',
                'title' => 'Other, please explain:'
            ]
        ];

        foreach ($rejects as $r) {
            $this->insert('system_reject', [
                'group' => $r['group'],
                'title' => $r['title']
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('system_reject', ['group' => 'user_delete_request']);
    }

}
