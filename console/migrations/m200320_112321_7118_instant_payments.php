<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200320_112321_7118_instant_payments
 */
class m200320_112321_7118_instant_payments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
CREATE TABLE `instant_payment` (
  `uuid` char(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `descr` varchar(1024) NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `sum` decimal(10,2) NOT NULL,
  `currency` char(3) NOT NULL,
  `status` enum('wait_for_confirm','declined','canceled','approved','not_payed','') NOT NULL,
  `payed_at` datetime DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  `primary_invoice_uuid` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
        $this->execute("
ALTER TABLE `instant_payment`
  ADD PRIMARY KEY (`uuid`),
  ADD UNIQUE KEY `primary_invoice_uuid` (`primary_invoice_uuid`),
  ADD KEY `currency` (`currency`),
  ADD KEY `from_user_id` (`from_user_id`),
  ADD KEY `to_user_id` (`to_user_id`);
");
        $this->execute("
ALTER TABLE `instant_payment`
  ADD CONSTRAINT `fk_instant_payment_from_user_id` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_instant_payment_primary_invoice` FOREIGN KEY (`primary_invoice_uuid`) REFERENCES `payment_invoice` (`uuid`),
  ADD CONSTRAINT `fk_instant_payment_to_user_id` FOREIGN KEY (`to_user_id`) REFERENCES `user` (`id`);
");
        $this->execute('
        ALTER TABLE `payment_invoice` ADD `instant_payment_uuid` CHAR(6) AFTER `preorder_id`, ADD INDEX (`instant_payment_uuid`);
        ');

        $this->execute('ALTER TABLE `payment_invoice` ADD CONSTRAINT `fk_payment_invoice_instant_payment` FOREIGN KEY (`instant_payment_uuid`) REFERENCES `instant_payment`(`uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT;');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}