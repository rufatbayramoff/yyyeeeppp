<?php

use yii\db\Migration;

class m160530_153621_alter_store_unit_rating extends Migration
{
    public function up()
    {
        $this->addColumn('store_unit', 'rating', 'integer NOT NULL DEFAULT 0');
        $this->createIndex('fk_store_unit_3_idx', 'store_unit', 'rating');
    }

    public function down()
    {
        $this->dropColumn('store_unit', 'rating');
    }
}
