<?php

use common\modules\payment\fee\FeeHelper;
use common\modules\payment\fee\FeePercentCondition;
use common\modules\payment\fee\FeePercentRuleForPrice;
use lib\money\Money;
use yii\db\Migration;
use yii\db\Query;


/**
 * Class m181025_121644_5814_create_payment_invoice_for_preorder
 */
class m181025_121644_5814_create_payment_invoice_for_preorder extends Migration
{
    protected $feeParams;

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $feeParams = app()->db->createCommand("select json from system_setting where `key` = 'productServiceFeeParams'")->queryScalar();
        $this->feeParams = \yii\helpers\Json::decode($feeParams);

        $preorders = (new Query())
            ->select([
                'p.*'
            ])
            ->from(['p' => 'preorder'])
            ->innerJoin(['pw' => 'preorder_work'], 'pw.preorder_id = p.id')
            ->where([
                'and',
                ['=', 'p.offered', 1],
                ['is', 'p.current_payment_invoice_uuid', dbexpr('null')]
            ])
            ->groupBy('p.id')
            ->all();

        foreach ($preorders as $preorder) {
            echo "Preorder: {$preorder['id']}: \n";
            $orders = app()->db->createCommand('select * from store_order where preorder_id = :preorder_id', ['preorder_id' => $preorder['id']])->queryAll();

            if (!empty($orders)) {
                foreach ($orders as $order) {
                    $invoice = app()->db->createCommand('select * from payment_invoice where store_order_id = :id order by created_at DESC limit 1', ['id' => $order['id']])->queryOne();

                    if ($invoice) {
                        echo " - Set invoice for orderId: {$order['id']}\n";
                        $formInvoiceItemsAndAccounting = $this->formInvoiceItemsAndAccounting($invoice, $preorder);

                        $this->delete('payment_invoice_item', ['payment_invoice_uuid' => $invoice['uuid']]);

                        $this->update('payment_invoice', [
                            'accounting' => $formInvoiceItemsAndAccounting['accounting'],
                            'total_amount' => $this->formTotalAmount($formInvoiceItemsAndAccounting['accounting']),
                            'preorder_id' => $preorder['id']
                        ], [
                            'uuid' => $invoice['uuid']
                        ]);

                        $this->saveItems($formInvoiceItemsAndAccounting['items']);

                        $this->update('preorder', [
                            'current_payment_invoice_uuid' => $invoice['uuid'],
                        ], [
                            'id' => $preorder['id']
                        ]);

                    } else {
                        echo " - Create new invoice for orderId: {$order['id']}\n";
                        $this->createInvoice($preorder, $order);
                    }
                }
            } else {
                echo " - Create new invoice\n";
                $this->createInvoice($preorder);
            }
            echo "--------------- \n";
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function generateInvoiceUuid()
    {
        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_invoice')->where('uuid="' . $operationUuid . '"')->one();
        } while ($checkUuid);

        return $operationUuid;
    }

    /**
     * @param $preorder
     * @param null $order
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    protected function createInvoice($preorder, $order = null)
    {
        $psUserId = app()->db->createCommand('select user_id from ps where id = :ps_id', ['ps_id' => $preorder['ps_id']])->queryScalar();

        $paymentInvoice = [
            'uuid'            => $this->generateInvoiceUuid(),
            'created_at'      => $preorder['created_at'],
            'date_expire'     => date('Y-m-d H:i:s', strtotime($preorder['created_at']) + (60 * 60 * 24 * 30)),
            'total_amount'    => null,
            'currency'        => 'USD',
            'rate_id'         => '1',
            'status'          => 'new',
            'user_id'         => $psUserId,
            'user_session_id' => '1',
            'store_order_id'  => $order['id'] ?? null,
            'preorder_id'     => $preorder['id'],
            'details'         => _t('store.order', 'Invoice for preorder: {preorderId}', ['preorderId' => $preorder['id']]),
            'accounting'      => null,
        ];

        $formInvoiceItemsAndAccounting = $this->formInvoiceItemsAndAccounting($paymentInvoice, $preorder);

        $paymentInvoice['accounting'] = $formInvoiceItemsAndAccounting['accounting'];
        $paymentInvoice['total_amount'] = $this->formTotalAmount($paymentInvoice['accounting']);

        $this->insert('payment_invoice', $paymentInvoice);
        $this->saveItems($formInvoiceItemsAndAccounting['items']);
        //$preorder

        $this->update('preorder', [
            'current_payment_invoice_uuid' => $paymentInvoice['uuid'],
        ], [
            'id' => $preorder['id']
        ]);
    }

    /**
     * @param $paymentInvoice
     * @param $preorder
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function formInvoiceItemsAndAccounting($paymentInvoice, $preorder): array
    {
        $items = [];
        $accounting['invoice_items'] = [];

        $pos = 1;

        $preorderWorks = app()->db->createCommand('select * from preorder_work where preorder_id = :id', ['id' => $preorder['id']])->queryAll();

        // manufacturing fee total
        $preorderTotalCost = $this->calcTotal($preorderWorks);

        $totalCostMoney = Money::create($preorderTotalCost, 'USD');

        $feePercentCondition = new FeePercentCondition();
        $feePercentCondition->totalPrice =$totalCostMoney;

        /** @var FeePercentRuleForPrice $ruleForPrice */
        $ruleForPrice = \Yii::createObject([
            'class'               => FeePercentRuleForPrice::class,
            'feePercentCondition' => $feePercentCondition,
            'feeParams'           => $this->feeParams
        ]);

        $manufacturingFeeTotal = $totalCostMoney->getAmount() * $ruleForPrice->detectFeePercent() / 100;
        // ------------------

        foreach ($preorderWorks as $work) {
            $percent = $manufacturingFeeTotal * 100 / $preorderTotalCost;

            $price = [
                'manufacturer' => round($work['cost'] - ($work['cost'] * $percent / 100), 2),
                'fee'          => $work['cost'] - round($work['cost'] - ($work['cost'] * $percent / 100), 2)
            ];

            $uuidItem = \common\components\UuidHelper::generateUuid(4);

            $items[] = [
                'uuid'                 => $uuidItem,
                'payment_invoice_uuid' => $paymentInvoice['uuid'],
                'pos'                  => $pos++,
                'title'                => $work['title'] ?: 'Item',
                'description'          => 'Invoice item for preorder: ' . $work['preorder_id'],
                'measure'              => 'item',
                'qty'                  => $work['qty'] ?? 1,
                'total_line'           => $work['cost'],
            ];

            $accounting['invoice_items'][$uuidItem] = [
                'preorderWorkId'         => $work['id'],
                'paymentInvoiceItemUuid' => $uuidItem,
                'manufacturer'           => [
                    'type'  => 'manufacturer',
                    'price' => $price['manufacturer']
                ],
                'fee'                    => [
                    'type'  => 'fee',
                    'price' => $price['fee']
                ]
            ];
        }

        $accounting = $this->checkAccounting($manufacturingFeeTotal, $accounting);

        return [
            'items'      => $items,
            'accounting' => $accounting
        ];
    }

    protected function checkAccounting($manufacturingFeeTotal, $accounting, $correctValue = null)
    {
        $accountingTotalFee = 0;

        foreach ($accounting['invoice_items'] as $item) {
            if ($correctValue === null) {
                $accountingTotalFee += $item['fee']['price'];
            } else {
                $accountingTotalFee += $item['fee']['price'] + $correctValue;
                $correctValue = null;
            }
        }

        $manufacturingFeeTotal = round($manufacturingFeeTotal, 2);
        $accountingTotalFee = round($accountingTotalFee, 2);

        if ($manufacturingFeeTotal !== $accountingTotalFee) {
            echo " - WARNING: Manufacturing fee not equal: total $manufacturingFeeTotal !== calc $accountingTotalFee\n";

            $correctValue = $manufacturingFeeTotal - $accountingTotalFee;
            $this->checkAccounting($manufacturingFeeTotal, $accounting, $correctValue);
        } else {
            echo " - Manufacturing fee equal\n";
        }

        return $accounting;
    }

    /**
     * @param $accounting
     *
     * @return float
     */
    protected function formTotalAmount($accounting): float
    {
        $totalAmount = 0;

        foreach ($accounting['invoice_items'] as $item) {
            $totalAmount += ($item['manufacturer']['price'] ?? 0)
                +($item['fee']['price'] ?? 0);
        }

        return $totalAmount;
    }

    protected function saveItems($items)
    {
        foreach ($items as $item) {
            $this->insert('payment_invoice_item', $item);
        }
    }

    protected function calcTotal($works): float
    {
        $total = 0;

        foreach ($works as $work) {
            $total += $work['cost'];
        }

        return $total;
    }
}
