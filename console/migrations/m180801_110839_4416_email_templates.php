<?php

use yii\db\Migration;

/**
 * Class m180801_110839_4416_email_templates
 */
class m180801_110839_4416_email_templates extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //
        $this->insert(
            'email_template',
            [
                'code'          => 'client.order.withoutTracking10days', // domestic
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => 'Have you received your order on Treatstock?',
                'description'   => 'sent after 10 days for orders without tracking number.  params: orderLink,orderId, orderClickLink, username, carrier',
                'template_html' => '<p>Hello %username%! </p>

<p>Your order %orderClickLink% was shipped with %carrier% without a tracking number. Could you please let us know if you have received your order in the mail?</p>

<p>Best regards,
Treatstock</p>
            ',
                'updated_at'    => dbexpr('NOW()'),
                'is_active'     => true
            ]
        );

        $this->insert(
            'email_template',
            [
                'code'          => 'client.order.withoutTracking20days', // international
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => 'Have you received your order on Treatstock?',
                'description'   => 'sent after 20 days for international orders without tracking number.  params: orderLink, orderId, orderClickLink, username, carrier',
                'template_html' => '<p>Hello %username%! </p>

<p>Your order %orderClickLink% was shipped with %carrier% without a tracking number. Could you please let us know if you have received your order in the mail?</p>

<p>Best regards,
Treatstock</p>
            ',
                'updated_at'    => dbexpr('NOW()'),
                'is_active'     => true
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'client.order.withoutTracking10days']);
        $this->delete('email_template', ['code' => 'client.order.withoutTracking20days']);
    }
}
