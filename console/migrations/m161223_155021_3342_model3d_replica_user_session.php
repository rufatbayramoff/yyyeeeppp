<?php

use yii\db\Migration;
use yii\db\Query;

class m161223_155021_3342_model3d_replica_user_session extends Migration
{

    public function up()
    {
        $this->addColumn('model3d_replica', 'user_session_id', 'integer(11) null after title');
        $this->update('model3d_replica', ['user_session_id' => 1]);
        $this->alterColumn('model3d_replica', 'user_session_id', 'integer(11) not null');
        $this->alterColumn('model3d_replica_img', 'user_id', 'integer(11) null');
        $this->addForeignKey('model3_replica_user_session_id_fk', 'model3d_replica', 'user_session_id', 'user_session', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('model3_replica_user_session_id_fk', 'model3d_replica');
        $this->dropColumn('model3d_replica', 'user_session_id');
    }
}
