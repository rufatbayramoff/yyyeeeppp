<?php

use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m220128_172553_9161_wiki_matrial_photo
 */
class m220214_162053_9282_pay_page_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute("ALTER TABLE `payment_pay_page_log_status` CHANGE `vendor` `vendor` ENUM('braintree','stripe','stripe_alipay','ts','bonus','invoice') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
