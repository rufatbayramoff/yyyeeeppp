<?php

use yii\db\Migration;

class m171116_141641_4965_comments extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('user_comment', [
            'id' => $this->primaryKey(),
            'model' => $this->string(64)->notNull()->defaultValue(''),
            'model_id' => $this->integer(),
            'user_id' => $this->integer(),
            'username' => $this->string(128),
            'email' => $this->string(128),
            'parent_id' => $this->integer()->comment('null-is not a reply, int-replied comment id'),
            'super_parent_id' => $this->integer()->comment('null-has no parent, int-1st level parent id'),
            'content' => $this->text(),
            'status' => $this->integer(1)->unsigned()->notNull()->defaultValue(0)->comment('0-pending,1-published,2-spam,3-deleted'),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
            'updated_by' => $this->integer(),
            'user_ip' => $this->string(15),
            'url' => $this->string(255),
        ], $tableOptions);

        $this->createIndex('comment_model', 'user_comment', 'model');
        $this->createIndex('comment_model_id', 'user_comment', ['model', 'model_id']);
        $this->createIndex('comment_status', 'user_comment', 'status');
        $this->createIndex('comment_reply', 'user_comment', 'parent_id');
        $this->createIndex('comment_super_parent_id', 'user_comment', 'super_parent_id');
    }

    public function safeDown()
    {
        $this->dropTable('user_comment');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171116_141641_4965_comments cannot be reverted.\n";

        return false;
    }
    */
}
