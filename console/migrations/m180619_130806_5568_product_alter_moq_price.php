<?php

use yii\db\Migration;

/**
 * Class m180619_130806_5568_product_alter_moq_price
 */
class m180619_130806_5568_product_alter_moq_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'moq_prices', 'json NULL');
        $this->execute("ALTER TABLE `product` 
            ADD COLUMN `supply_ability` INT NULL AFTER `dynamic_fields_values`,
            ADD COLUMN `supply_ability_time` ENUM('day', 'week', 'month', 'year') NULL AFTER `supply_ability`,
            ADD COLUMN `avg_production` INT NULL AFTER `supply_ability_time`,
            ADD COLUMN `avg_production_time` ENUM('day', 'week', 'month', 'year') NULL AFTER `avg_production`,
            ADD COLUMN `product_package` VARCHAR(125) NULL AFTER `avg_production_time`,
            ADD COLUMN `outer_package` VARCHAR(125) NULL AFTER `product_package`,
            ADD COLUMN `incoterms` CHAR(15) NULL AFTER `outer_package`,
            ADD COLUMN `ship_from_id` INT NULL AFTER `incoterms`,
            ADD COLUMN `unit_type` CHAR(15) NOT NULL DEFAULT 'piece' AFTER `ship_from_id`,
            ADD COLUMN `custom_properties` JSON NULL AFTER `unit_type`,
            ADD COLUMN `switch_quote_qty` INT NULL AFTER `custom_properties`;
        ");

        $this->execute('ALTER TABLE `user_location` ADD COLUMN `formatted_address` VARCHAR(245) NULL AFTER `user_data`;');
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'moq_prices');
        $this->dropColumn('product', 'supply_ability');
        $this->dropColumn('product', 'supply_ability_time');
        $this->dropColumn('product', 'avg_production');
        $this->dropColumn('product', 'avg_production_time');
        $this->dropColumn('product', 'product_package');
        $this->dropColumn('product', 'outer_package');
        $this->dropColumn('product', 'incoterms');
        $this->dropColumn('product', 'ship_from_id');
        $this->dropColumn('product', 'unit_type');
        $this->dropColumn('product', 'custom_properties');
        $this->dropColumn('product', 'switch_quote_qty');
        $this->dropColumn('user_location', 'formatted_address');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180619_130806_5568_product_alter_moq_price cannot be reverted.\n";

        return false;
    }
    */
}
