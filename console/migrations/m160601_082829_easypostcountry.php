<?php

use yii\db\Migration;

class m160601_082829_easypostcountry extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `geo_country`  
                    ADD COLUMN `is_easypost_intl` BIT(1) NOT NULL DEFAULT 0 AFTER `currency_symbol`,
                    ADD COLUMN `is_easypost_domestic` BIT(1) NOT NULL DEFAULT 0 AFTER `is_easypost_intl`;");
        
        $this->execute("UPDATE  `geo_country` SET `is_easypost_intl`=1, `is_easypost_domestic`=1 WHERE `iso_code`='US';");
        $this->execute("UPDATE  `geo_country` SET `is_easypost_intl`=1, `is_easypost_domestic`=1 WHERE `iso_code`='CA';");
        
    }

    public function down()
    {
         $this->dropColumn('geo_country', 'is_easypost_intl');
         $this->dropColumn('geo_country', 'is_easypost_domestic');
    }
 
}
