<?php

use yii\db\Schema;
use yii\db\Migration;

class m151016_121822_ps extends Migration
{
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("ALTER TABLE `ps`  ADD COLUMN `phone_status` ENUM('new', 'checking', 'checked', 'failed') NOT NULL DEFAULT 'new' AFTER `updated_at`;");
        $this->execute("ALTER TABLE `ps`  ADD COLUMN `sms_notify` TINYINT(1) NOT NULL DEFAULT 0 AFTER `phone_status`;");
        
        // init migration
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');
        $this->truncateTable('model3d_license');
        $this->insert('model3d_license', [
            'id'=>1, 'title' => 'Commercial License', 'description'=>'TS License default' , 'is_active'=>1, 'info'=>''
        ]);

        $this->insert('model3d_license', [
            'id'=>2, 'title' => 'Free License (Attribution)', 'description'=>'Attribution License' , 'is_active'=>1, 'info'=>''
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('ps', 'phone_status');
        $this->dropColumn('ps', 'sms_notify');
    }
     
}
