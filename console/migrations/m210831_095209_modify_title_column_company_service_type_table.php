<?php

use yii\db\Migration;

/**
 * Class m210831_095209_modify_title_column_company_service_type_table
 */
class m210831_095209_modify_title_column_company_service_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('company_service_type',['title' => 'Design'],['id' => 3]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update('company_service_type',['title' => 'Designer'],['id' => 3]);
    }
}
