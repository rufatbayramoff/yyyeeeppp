<?php

use yii\db\Migration;

class m160310_101823_delete_bad_parser_jobs extends Migration
{
    public function up()
    {
        $this->execute("DELETE FROM file_job WHERE result='{\"exec_result\":[]}'");

    }

    public function down()
    {
        return true;
    }
}
