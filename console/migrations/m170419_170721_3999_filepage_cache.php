<?php

use yii\db\Migration;

class m170419_170721_3999_filepage_cache extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `filepage_size` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `length` INT(11) NOT NULL,
  `width` INT(11) NOT NULL,
  `height` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `length` (`length`,`width`,`height`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL

        );

        $this->execute(
            <<<SQL
CREATE TABLE `filepage_cache` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `request_date` DATETIME NOT NULL,
  `update_date` DATETIME NOT NULL,
  `material_group_id` INT(11) NOT NULL,
  `country_id` INT(11) NOT NULL,
  `color_id` INT(11) NOT NULL,
  `size_id` INT(11) NOT NULL,
  `allowed_materials` VARCHAR(1000) NOT NULL DEFAULT '',
  `delivery_type_id` INT(11) NULL,
  `is_published` INT(11) NOT NULL,
  `any_texture_allowed` INT(11) NOT NULL,
  `volume` INT(11) NOT NULL,
  `printers_list` JSON NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq` (`material_group_id`,`country_id`,`color_id`,`size_id`,`allowed_materials`, `delivery_type_id`, `is_published`,`any_texture_allowed`,`volume`) USING BTREE,
  KEY `country_id` (`country_id`),
  KEY `color_id` (`color_id`),
  KEY `size_id` (`size_id`),
  KEY `material_group_id` (`material_group_id`),
  KEY `delivery_type_id` (`delivery_type_id`),
  CONSTRAINT `fk_filepage_cache_color_id` FOREIGN KEY (`color_id`) REFERENCES `printer_color` (`id`),
  CONSTRAINT `fk_filepage_cache_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`),
  CONSTRAINT `fk_filepage_cache_size_id` FOREIGN KEY (`size_id`) REFERENCES `filepage_size` (`id`),
  CONSTRAINT `fl_filepage_cache_material_group_id` FOREIGN KEY (`material_group_id`) REFERENCES `printer_material_group` (`id`),
  CONSTRAINT `fk_filepage_cache_allow_delivery_type_id` FOREIGN KEY (`delivery_type_id`) REFERENCES `delivery_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL

        );
        $this->execute(
            <<<SQL
CREATE TABLE `filepage_cache_color` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `request_date` DATETIME NOT NULL,
  `update_date` DATETIME NOT NULL,
  `country_id` INT(11) NOT NULL,
  `size_id` INT(11) NOT NULL,
  `allowed_materials` VARCHAR(1000) NOT NULL DEFAULT '',
  `delivery_type_id` INT(11) NULL,
  `is_published` INT(11) NOT NULL,
  `colors` JSON NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `size_id` (`size_id`),
  KEY `delivery_type_id` (`delivery_type_id`),
  UNIQUE KEY `uniq` (`country_id`,`size_id`, `allowed_materials`, `delivery_type_id`, `is_published`) USING BTREE,
  CONSTRAINT `fk_filepage_cache_colors_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`),
  CONSTRAINT `fk_filepage_cache_colors_size_id` FOREIGN KEY (`size_id`) REFERENCES `filepage_size` (`id`),
  CONSTRAINT `fk_filepage_cache_colors_allow_delivery_type_id` FOREIGN KEY (`delivery_type_id`) REFERENCES `delivery_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function down()
    {
        $this->dropTable('filepage_cache');
        $this->dropTable('filepage_cache_colors');
        $this->dropTable('filepage_size');
    }
}
