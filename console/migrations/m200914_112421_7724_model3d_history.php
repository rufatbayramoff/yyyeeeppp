<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\modules\translation\components\DbI18n;
use common\modules\translation\components\Statistics;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200914_112421_7724_model3d_history
 */
class m200914_112421_7724_model3d_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `model3d_history` ADD INDEX(`created_at`)');
        $this->execute('ALTER TABLE `file_job` ADD INDEX(`created_at`)');
        $this->execute('ALTER TABLE `payment_log` ADD INDEX(`created_at`)');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
    }
}
