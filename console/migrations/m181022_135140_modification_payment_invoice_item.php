<?php

use yii\db\Migration;

/**
 * Class m181022_135140_modification_payment_invoice_item
 */
class m181022_135140_modification_payment_invoice_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('payment_invoice_item', 'price', 'total_line');
        $this->alterColumn('payment_invoice_item', 'qty', 'decimal(15, 4) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('payment_invoice_item', 'total_line', 'price');
        $this->alterColumn('payment_invoice_item', 'qty', 'decimal(15, 4) not null');
    }
}
