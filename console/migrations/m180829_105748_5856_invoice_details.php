<?php

use yii\db\Migration;

/**
 * Class m180829_105748_5856_invoice_details
 */
class m180829_105748_5856_invoice_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('update system_setting set `key`="bank_details_new" where `key`="payment_details"');
        $this->execute('update system_setting set `key`="payment_details" where `key`="bank_details"');
        $this->execute('update system_setting set `key`="bank_details" where `key`="bank_details_new"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180829_105748_5856_invoice_details cannot be reverted.\n";

        return false;
    }
    */
}
