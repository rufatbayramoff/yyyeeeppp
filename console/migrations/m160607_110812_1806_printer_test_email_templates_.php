<?php

use yii\db\Migration;

class m160607_110812_1806_printer_test_email_templates_ extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`) VALUES (NULL, 'ps.test.failed', 'service', 'en-US', 'Certification of your %printerName% 3D printer on treatstock.com', NULL, NULL, 'Hello %psName%,\r\nYou recently attempted to get your %printerName% 3D printer certified on treatstock.com. Unfortunately, the printer was not certified due to the following reason: %rejectReason%.\r\nYou may try to obtain certification again at any time.\r\n\r\nBest regards,\r\nTreatstock', NULL);");
        $this->execute("INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`) VALUES (NULL, 'ps.test.success', 'service', 'en-US', 'Certification of your %printerName% 3D printer on treatstock.com', NULL, NULL, 'Hello %psName%,\r\nCongratulations! Your %printerName% 3D printer has been successfully certified on treatstock.com\r\nYou can check the new status of your printer here: <a href=\"%psLink%\">%psLink%</a>\r\n\r\nBest regards,\r\nTreatstock', NULL);");
    }

    public function down()
    {
        $this->delete('email_template', ['code' => 'ps.test.failed']);
        $this->delete('email_template', ['code' => 'ps.test.success']);
        return true;
    }
}
