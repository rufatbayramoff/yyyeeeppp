<?php

use yii\db\Migration;

class m170407_134639_3843_delete_3diax_fields extends Migration
{
    public function up()
    {
        $this->dropColumn('ps_printer_file_status', 'token');
        $this->dropColumn('ps_printer_file_status', 'progress');
        $this->dropColumn('ps_printer_file_status', 'print_status');
        $this->dropColumn('printer', 'cant_print_throught_3diax');
    }

    public function down()
    {
        echo "m170407_134639_3843_delete_2diax_fields cannot be reverted.\n";

        return false;
    }
}
