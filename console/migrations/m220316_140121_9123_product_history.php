<?php

use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m220316_140121_9123_product_history
 */
class m220316_140121_9123_product_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute("
 CREATE TABLE `product_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `model_uuid` varchar(32) NOT NULL,
  `created_at` DATETIME,
  `user_id` int DEFAULT NULL,
  `action_id` varchar(45) NOT NULL,
  `source` json DEFAULT NULL,
  `result` json DEFAULT NULL,
  `comment` longtext,
  PRIMARY KEY (`id`),
  KEY `fk_product_history_uuid` (`model_uuid`),
  KEY `fk_product_history_3_idx` (`user_id`),
  CONSTRAINT `fk_product_history_1` FOREIGN KEY (`model_uuid`) REFERENCES `product` (`uuid`),
  CONSTRAINT `fk_product_history_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB;
");
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

}
