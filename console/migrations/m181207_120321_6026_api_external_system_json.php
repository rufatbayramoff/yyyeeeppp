<?php

use yii\db\Migration;

/**
 * Class m181207_120321_6026_api_external_system_json
 */
class m181207_120321_6026_api_external_system_json extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('UPDATE `api_external_system` SET `json_config` = \'""\' WHERE `json_config` = \'\' or `json_config` is null');
        $this->execute('ALTER TABLE `api_external_system` CHANGE `json_config` `json_config` JSON NOT NULL;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
