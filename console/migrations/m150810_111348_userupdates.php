<?php

use yii\db\Schema;
use yii\db\Migration;

class m150810_111348_userupdates extends Migration
{
    public function up()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->execute("ALTER TABLE `user`  ADD COLUMN `trustlevel` VARCHAR(45) NOT NULL DEFAULT 'normal' AFTER `lastlogin_at`");
        return 0;
    }

    public function down()
    {
        $this->execute('ALTER TABLE `user`  DROP COLUMN `trustlevel`');
        return 0;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
