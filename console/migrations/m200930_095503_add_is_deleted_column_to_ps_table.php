<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%ps}}`.
 */
class m200930_095503_add_is_deleted_column_to_ps_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ps}}', 'is_deleted', $this->tinyInteger(1)->unsigned()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ps}}', 'is_deleted');
    }
}
