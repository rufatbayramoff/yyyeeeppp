<?php

use yii\db\Migration;

class m160608_144217_help_priority extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `site_help` ADD COLUMN `priority` INT(11) NOT NULL DEFAULT '0' AFTER `category_id`;");
    }

    public function down()
    {
        $this->dropColumn('site_help', 'priority');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
