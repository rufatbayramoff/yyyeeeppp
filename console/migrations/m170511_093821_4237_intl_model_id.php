<?php

use yii\db\Migration;

class m170511_093821_4237_intl_model_id extends Migration
{
    protected function getColumnNames()
    {
        return [
            'model3d_license_intl'              => 'license_id',
            'msg_folder_intl'                   => 'msg_folder_id',
            'printer_color_intl'                => 'printer_color_id',
            'printer_material_group_intl'       => 'group_id',
            'printer_material_group_usage_intl' => 'usage_id',
            'printer_material_intl'             => 'printer_material_id',
            'printer_properties_intl'           => 'printer_properties_id',
            'printer_technology_intl'           => 'printer_technology_id',
            'seo_page_intl'                     => 'seo_page_id',
            'site_help_category_intl'           => 'category_id',
            'site_help_intl'                    => 'help_id',
            'static_page_intl'                  => 'static_page_id',
            'store_category_intl'               => 'category_id',
            'system_reject_intl'                => 'system_reject_id',
        ];
    }

    protected function addIsActiveField($tableName)
    {
        // Is active field used in seopage ...
        try {
            $this->addColumn($tableName, 'is_active', 'bit(1) default 1');
        } catch (Exception $exception) {

        }
    }

    protected function dropIndexes($tableName)
    {
        $foreignKeys = $this->getDb()->createCommand(
            ' SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE information_schema.TABLE_CONSTRAINTS.CONSTRAINT_TYPE = \'FOREIGN KEY\' AND information_schema.TABLE_CONSTRAINTS.TABLE_NAME = \'' . $tableName . '\''
        )->queryAll();
        foreach ($foreignKeys as $foreignKey) {
            $fkName = $foreignKey['CONSTRAINT_NAME'];
            try {
                $this->dropForeignKey($fkName, $tableName);
            } catch (Exception $exception) {

            }
        }
        $indexes = $this->getDb()->createCommand('SHOW INDEX FROM ' . $tableName)->queryAll();

        foreach ($indexes as $index) {
            if ($index['Column_name'] == 'id') {
                continue;
            }
            try {
                $this->dropIndex($index['Key_name'], $tableName);
            } catch (Exception $exception) {

            }
        }
    }

    protected function addIndexes($tableName)
    {
        $sourceTableName = substr($tableName, 0, -5);
        $this->createIndex($tableName . '_unique_index', $tableName, ['model_id', 'lang_iso'], true);
        $this->createIndex($tableName . '_search_index', $tableName, ['model_id', 'lang_iso', 'is_active']);
        if ($tableName != 'msg_folder_intl') {
            // SKIP msg_folder_intl because unginded and signed id field names!
            $this->addForeignKey($tableName . '_fk_model_id', $tableName, 'model_id', $sourceTableName, 'id', 'CASCADE', 'CASCADE');
            return;
        }
        // Delete not exists langs
        $this->getDb()->createCommand(
            'DELETE `' . $tableName . '` FROM `' . $tableName . '` WHERE `lang_iso` not in (SELECT iso_code from `system_lang`)'
        )->query();
        $this->addForeignKey($tableName . '_fk_lang_iso', $tableName, 'lang_iso', 'system_lang', 'iso_code', 'CASCADE', 'CASCADE');
    }

    public function deleteDuples($tableName)
    {
        $this->getDb()->createCommand(
            'DELETE dupes FROM ' . $tableName . ' dupes, ' . $tableName . ' fullTable WHERE dupes.model_id = fullTable.model_id AND dupes.lang_iso  = fullTable.lang_iso AND dupes.id > fullTable.id'
        )->query();
    }

    public function up()
    {
        // fix char set
        try {
            $this->dropForeignKey('fk_model3d_license_intl_1', 'model3d_license_intl');
        } catch (Exception $exception) {

        }
        try {
            $this->dropIndex('uq_intl_lang', 'model3d_license_intl');
        } catch (Exception $exception) {

        }
        try {
            $this->dropIndex('fk_model3d_license_intl_1_idx', 'model3d_license_intl');
        } catch (Exception $exception) {

        }
        $this->alterColumn('model3d_license_intl', 'title', 'VARCHAR(145) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL');
        $this->alterColumn('model3d_license_intl', 'description', ' TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL');
        $this->alterColumn('model3d_license_intl', 'info', 'MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL');
        $this->alterColumn('model3d_license_intl', 'lang_iso', 'CHAR(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL');

        foreach ($this->getColumnNames() as $tableName => $columnName) {
            $this->addIsActiveField($tableName);
            $this->renameColumn($tableName, $columnName, 'model_id');
            $this->delete($tableName, 'model_id is null');

            $this->dropIndexes($tableName);
            $this->alterColumn($tableName, 'model_id', 'int(11) not null');
            $this->deleteDuples($tableName);
            $this->addIndexes($tableName);
        }
        return true;
    }

    public function down()
    {
        return false;
    }
}
