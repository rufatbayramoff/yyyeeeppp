<?php

use yii\db\Migration;

class m171208_155105_4145_email_tpls extends Migration
{
    public function safeUp()
    {

        $this->insert(
            'email_template',
            [
                'code'          => 'partialRefundOrder',
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => 'Partial refund for Order #%orderId%',
                'description'   => 'Customer gets this email when partial refund approved/requested by moderator. [%orderId%, %clientName%, %refundAmount%, %comment%]',
                'updated_at'    => dbexpr('NOW()'),
                'template_html' => '<p>Hello, %clientName%</p><p>Partial refund for order #%orderId%. Amount: %refundAmount%. Comment: %comment%</p><p> Thank you.</p><p>Please visit treatstock.com for details.</p>',
                'template_text' => ''
            ]
        );

    }

    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'partialRefundOrder', 'language_id' => 'en-US']);

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171208_155105_4145_email_tpls cannot be reverted.\n";

        return false;
    }
    */
}
