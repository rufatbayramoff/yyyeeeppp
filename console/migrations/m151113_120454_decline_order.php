<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_120454_decline_order extends Migration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('decline_order','Printer is unavailable','',1);");
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('decline_order','Filament is over','',1);");
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('decline_order','Model does not suitable for printer','',1);");

        $tplHtml = '<p>Hello %username%,</p>
            <p>
                Print Service %ps_title% declined the order %order_id% by reason %reason% with comments %comments%
            </p>
            <p>
                Thank you,
                TS Team</p>';
        $tplText = 'Hello %username%,

                Print Service %ps_title% declined the order %order_id% by reason %reason% with comments %comments%

                Thank you,
                TS Team';

        $this->execute("INSERT INTO `email_template` (`code`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`)
            VALUES ('psDeclineOrder','en-US','Your order has been declined by Print Service','this email sent after PS decline order',NULL,'{$tplHtml}','{$tplText}');");
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM `system_reject` WHERE `group` = 'decline_order'");

        $this->execute("DELETE FROM `email_template` WHERE `code` = 'psDeclineOrder' LIMIT 1;");
    }
}
