<?php

use yii\db\Migration;

/**
 * Class m180111_104658_5193_email_tpl_login
 */
class m180111_104658_5193_email_tpl_login extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update('email_template', ['template_html'=>'Hello %name%,<br />
The print shop for order #%orderId% has offered an additional service "%positionTitle%" for %positionAmount%. <br />
To review the additional service and accept or decline, <a href="%loginLink%">click here</a>. <br />
<br />
Best Regards, <br />
Treatstock'], ['code' => 'client.order.positionNew']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180111_104658_5193_email_tpl_login cannot be reverted.\n";

        return false;
    }
    */
}
