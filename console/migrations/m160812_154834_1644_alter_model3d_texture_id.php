<?php

use yii\db\Migration;

class m160812_154834_1644_alter_model3d_texture_id extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
ALTER TABLE `model3d_part` 
ADD COLUMN `model3d_texture_id` INT(11) NULL AFTER `model3d_part_properties_id`,
ADD UNIQUE INDEX `model3d_texture_id_UNIQUE` (`model3d_texture_id` ASC);
ALTER TABLE `model3d_part` 
ADD CONSTRAINT `fk_model3d_part_model3d_texture_id`
  FOREIGN KEY (`model3d_texture_id`)
  REFERENCES `model3d_texture` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SQL
        );
        $this->execute(
            <<<SQL
        ALTER TABLE `model3d` 
ADD COLUMN `model3d_texture_id` INT(11) NULL AFTER `source`,
ADD UNIQUE INDEX `model3d_texture_id_UNIQUE` (`model3d_texture_id` ASC);
ALTER TABLE `model3d` 
ADD CONSTRAINT `fk_model3d_texture_id`
  FOREIGN KEY (`model3d_texture_id`)
  REFERENCES `model3d_texture` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SQL
        );

        $this->execute(
            <<<SQL
ALTER TABLE `model3d_replica_part` 
ADD COLUMN `model3d_texture_id` INT(11) NULL AFTER `model3d_part_properties_id`,
ADD INDEX `model3d_replica_texture_id` (`model3d_texture_id` ASC);
ALTER TABLE `model3d_replica_part` 
ADD CONSTRAINT `fk_model3d_replica_part_model3d_texture_id`
  FOREIGN KEY (`model3d_texture_id`)
  REFERENCES `model3d_texture` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SQL
        );
        $this->execute(
            <<<SQL
        ALTER TABLE `model3d_replica` 
ADD COLUMN `model3d_texture_id` INT(11) NULL AFTER `source`,
ADD INDEX `model3d_replica_texture_id` (`model3d_texture_id` ASC);
ALTER TABLE `model3d` 
ADD CONSTRAINT `fk_model3d_replica_texture_id`
  FOREIGN KEY (`model3d_texture_id`)
  REFERENCES `model3d_texture` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SQL
        );
    }

    public function down()
    {
        echo "m160812_154834_1644_model3d_part_id_field cannot be reverted.\n";
        return false;
    }
}
