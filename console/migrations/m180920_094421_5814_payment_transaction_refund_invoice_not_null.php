<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180920_094421_5814_payment_transaction_refund_invoice_not_null
 */
class m180920_094421_5814_payment_transaction_refund_invoice_not_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `payment_transaction_refund` 
DROP FOREIGN KEY `payment_transaction_refund_invoice_fk`;
ALTER TABLE `payment_transaction_refund` 
CHANGE COLUMN `payment_invoice_uuid` `payment_invoice_uuid` VARCHAR(6) NOT NULL ;
ALTER TABLE `payment_transaction_refund` 
ADD CONSTRAINT `payment_transaction_refund_invoice_fk`
  FOREIGN KEY (`payment_invoice_uuid`)
  REFERENCES `payment_invoice` (`uuid`);
');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
