<?php

use yii\db\Migration;

class m160819_155643_1644_alter_model3d_replica_part_foreign_key extends Migration
{
    public function up()
    {
        $this->addForeignKey('model3d_replica_part_id', 'model3d_replica_part', 'model3d_replica_id', 'model3d_replica', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('model3d_replica_part_id', 'model3d_replica_part');
    }
}
