<?php

use yii\db\Migration;
use yii\db\Query;

class m161010_163531_create_multicolor_materials extends Migration
{
    public function safeUp()
    {
        $printerMaterialColor = (new Query())
            ->select('*')
            ->where('color_id=102')
            ->from('printer_material_to_color')->all();
        if (!$printerMaterialColor) {
            // Clean test stand
            $this->insert(
                'printer_material_to_color',
                [
                    'id' => 33,
                    'color_id'    => 102,
                    'material_id' => 1,
                    'is_active'   => 1
                ]
            );
            $this->insert(
                'printer_technology',
                [
                    'id'              => 11,
                    'title'           => 'CJP (ColorJet Printing)',
                    'created_at'      => '2016-04-04 11:48:29',
                    'updated_at'      => '2016-04-04 11:49:37',
                    'is_active'       => 1,
                    'print_speed_min' => 1,
                    'print_speed_max' => 1
                ]
            );
            $this->insert(
                'printer',
                [
                    'id'                        => 91,
                    'firm'                      => '3D Systems',
                    'title'                     => 'ProJet CJP 660Pro',
                    'description'               => '',
                    'created_at'                => '2016-04-04 11:56:53',
                    'updated_at'                => '2016-05-19 13:21:48',
                    'is_active'                 => 1,
                    'technology_id'             => 11,
                    'weight'                    => null,
                    'price'                     => null,
                    'website'                   => 'http://www.3dsystems.com/3d-printers/professional/projet-660pro',
                    'build_volume'              => '254 x 381 x 203 mm',
                    'have_unfixed_size'         => 0,
                    'cant_print_throught_3diax' => 0
                ]
            );
            $this->insert(
                'printer_to_property',
                [
                    'id'          => '572',
                    'printer_id'  => '91',
                    'property_id' => '1',
                    'value'       => '254 x 381 x 203 mm (10 x 15 x 8 in.)',
                    'is_editable' => 0,
                    'created_at'  => '2016-04-04 11:56:53',
                    'updated_at'  => '2016-04-04 11:56:53',
                    'is_active'   => '1'
                ]
            );
            $this->insert(
                'printer_to_property',
                [
                    'id'          => '573',
                    'printer_id'  => '91',
                    'property_id' => '2',
                    'value'       => '254',
                    'is_editable' => 0,
                    'created_at'  => '2016-04-04 11:56:53',
                    'updated_at'  => '2016-04-04 11:56:53',
                    'is_active'   => '1'
                ]
            );
            $this->insert(
                'printer_to_property',
                [
                    'id'          => '574',
                    'printer_id'  => '91',
                    'property_id' => '3',
                    'value'       => '381',
                    'is_editable' => 0,
                    'created_at'  => '2016-04-04 11:56:53',
                    'updated_at'  => '2016-04-04 11:56:53',
                    'is_active'   => '1'
                ]
            );
            $this->insert(
                'printer_to_property',
                [
                    'id'          => '575',
                    'printer_id'  => '91',
                    'property_id' => '4',
                    'value'       => '203',
                    'is_editable' => 0,
                    'created_at'  => '2016-04-04 11:56:53',
                    'updated_at'  => '2016-04-04 11:56:53',
                    'is_active'   => '1'
                ]
            );
            $this->insert(
                'printer_to_property',
                [
                    'id'          => '576',
                    'printer_id'  => '91',
                    'property_id' => '5',
                    'value'       => '100 micron (0.02 in.)',
                    'is_editable' => 0,
                    'created_at'  => '2016-04-04 11:56:53',
                    'updated_at'  => '2016-04-04 11:56:53',
                    'is_active'   => '1'
                ]
            );
            $this->insert(
                'printer_to_property',
                [
                    'id'          => '577',
                    'printer_id'  => '91',
                    'property_id' => '6',
                    'value'       => '100 micron (0.004 in.)',
                    'is_editable' => 0,
                    'created_at'  => '2016-04-04 11:56:53',
                    'updated_at'  => '2016-04-04 11:56:53',
                    'is_active'   => '1'
                ]
            );
            $this->insert(
                'printer_to_material',
                [
                    'id'          => '176',
                    'printer_id'  => 91,
                    'material_id' => 1,
                    'created_at'  => '2016-04-06 10:31:22',
                    'updated_at'  => '2016-04-06 10:31:22',
                    'is_active'   => '1'
                ]
            );
        }
    }

    public function safeDown()
    {
        return true;
    }

}
