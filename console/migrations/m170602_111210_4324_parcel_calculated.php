<?php

use yii\db\Migration;

class m170602_111210_4324_parcel_calculated extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `store_order_delivery` 
            ADD COLUMN `calculated_parcel_width` DECIMAL(7,2) UNSIGNED NULL DEFAULT NULL AFTER `ps_delivery_details`,
            ADD COLUMN `calculated_parcel_height` DECIMAL(7,2) UNSIGNED NULL DEFAULT NULL AFTER `calculated_parcel_width`,
            ADD COLUMN `calculated_parcel_length` DECIMAL(7,2) UNSIGNED NULL DEFAULT NULL AFTER `calculated_parcel_height`,
            ADD COLUMN `calculated_parcel_weight` DECIMAL(7,2) UNSIGNED NULL DEFAULT NULL AFTER `calculated_parcel_length`;'
        );
    }

    public function down()
    {
        $this->dropColumn('store_order_delivery', 'calculated_parcel_width');
        $this->dropColumn('store_order_delivery', 'calculated_parcel_height');
        $this->dropColumn('store_order_delivery', 'calculated_parcel_length');
        $this->dropColumn('store_order_delivery', 'calculated_parcel_weight');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
