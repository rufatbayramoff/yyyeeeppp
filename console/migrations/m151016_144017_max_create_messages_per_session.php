<?php

use yii\db\Schema;
use yii\db\Migration;

class m151016_144017_max_create_messages_per_session extends Migration
{
    public function up()
    { 
        $this->execute("INSERT INTO `system_setting` (`group_id`, `key`, `value`, `description`) 
            VALUES ('13', 'max_messages_per_session', '1000', 'How many messages/topic user can create per session');");
    }

    public function down()
    {
        echo "m151016_144017_max_create_messages_per_session cannot be reverted.\n";

        return false;
    }
}
