<?php

use yii\db\Migration;

class m171211_173821_5056_remove_printer_categories extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_printer_1', 'printer');
        $this->dropColumn('printer', 'category_id');
        $this->addColumn('printer', 'equipment_category_id', 'int(11) not null default \'2\' after images_json');
        $this->createIndex('printer_equipment_category_id_indx', 'printer', 'equipment_category_id');
        $this->addForeignKey('fk_printer_equipment_category_id', 'printer', 'equipment_category_id', 'equipment_category', 'id');
        $this->dropTable('category_intl');
        $this->dropTable('category');
    }

    public function safeDown()
    {
//        return false;

    }
}
