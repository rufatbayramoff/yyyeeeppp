<?php

use yii\db\Migration;

class m161110_122309_2620_user_email_sent extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `user_email_sent` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `email_code` varchar(145) NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `hash` varchar(245) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_user_email_sent_1_idx` (`user_id`),
              CONSTRAINT `fk_user_email_sent_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
        ');
    }

    public function down()
    {
        $this->truncateTable('user_email_sent');
        $this->dropTable('user_email_sent');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
