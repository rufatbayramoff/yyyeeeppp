<?php

use yii\db\Migration;

class m160807_164412_1644_model3d_part extends Migration
{
    public function up()
    {
        $this->renameTable('model3d_file', 'model3d_part');
        $this->execute(
            <<<SQL
        ALTER TABLE `model3d_history` 
DROP FOREIGN KEY `fk_model3d_history_2`;
ALTER TABLE `model3d_history` 
CHANGE COLUMN `model3d_file_id` `model3d_part_id` INT(11) NULL DEFAULT NULL ;
ALTER TABLE `model3d_history` 
ADD CONSTRAINT `fk_model3d_history_2`
  FOREIGN KEY (`model3d_part_id`)
  REFERENCES `model3d_part` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SQL
        );
    }

    public function down()
    {
        return false;
    }
}
