<?php

use common\models\SystemReject;
use yii\db\Migration;

/**
 * Class m180918_133259_add_new_items_reject_templates
 */
class m180918_133259_add_new_items_reject_templates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rejects = [
            [
                'group' => 'product_preorder_decline',
                'title' => 'Product requested out of stock'
            ],
            [
                'group' => 'product_preorder_decline',
                'title' => 'Can\'t meet the deadline'
            ],
            [
                'group' => 'product_preorder_decline',
                'title' => 'Minimum order quantity not met'
            ],
            [
                'group' => 'product_preorder_decline',
                'title' => 'Insufficient production capability/capacity'
            ],
            [
                'group' => 'product_preorder_decline',
                'title' => 'Cannot ship to the client\'s location'
            ],
            [
                'group' => 'product_preorder_decline',
                'title' => 'Other'
            ],

            [
                'group' => 'product_order_decline',
                'title' => 'Product requested out of stock'
            ],
            [
                'group' => 'product_order_decline',
                'title' => 'Can\'t meet the deadline'
            ],
            [
                'group' => 'product_order_decline',
                'title' => 'Customer has requested a refund'
            ],
            [
                'group' => 'product_order_decline',
                'title' => 'Other'
            ],
        ];

        foreach ($rejects as $r) {
            $this->insert('system_reject', [
                'group' => $r['group'],
                'title' => $r['title']
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('system_reject', ['group' => 'product_preorder_decline']);
        $this->delete('system_reject', ['group' => 'product_order_decline']);
    }
}
