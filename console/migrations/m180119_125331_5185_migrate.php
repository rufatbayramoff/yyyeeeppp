<?php

use yii\db\Migration;

/**
 * Class m180119_125331_5185_migrate
 */
class m180119_125331_5185_migrate extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('site_help_category', [
            'title' => 'Help Center',
            'info' => 'Help Center',
        ]);

        $id = Yii::$app->db->getLastInsertID();

        $this->insert('system_setting', [
            'group_id' => 13,
            'key' => 'site_help_category_id',
            'value' => (int) $id,
            'is_active' => 1
        ]);

        $this->update('site_help_category', ['parent_id' => $id], ['!=', 'id', $id]);



        $this->insert('site_help_category', [
            'title' => 'Guides',
            'info' => 'Guides',
        ]);

        $id = Yii::$app->db->getLastInsertID();

        $this->insert('system_setting', [
            'group_id' => 13,
            'key' => 'guides_category_id',
            'value' => (int) $id,
            'is_active' => 1
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->execute("SET foreign_key_checks = 0");

        $id = $this->db->createCommand("SELECT `value` FROM system_setting WHERE `key` = 'site_help_category_id'")->queryScalar();
        $this->update('site_help_category', ['parent_id' => null], ['parent_id' => $id]);
        $this->delete('site_help_category', ['id' => $id]);
        $this->delete('system_setting', ['key' => 'site_help_category_id']);

        $id = $this->db->createCommand("SELECT `value` FROM system_setting WHERE `key` = 'guides_category_id'")->queryScalar();
        $this->delete('site_help_category', ['id' => $id]);
        $this->delete('system_setting', ['key' => 'guides_category_id']);

        $this->execute("SET foreign_key_checks = 1");

        return true;
    }
}
