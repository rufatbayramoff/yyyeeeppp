<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m200115_103421_7038_affiliate2
 */
class m200115_103421_7038_affiliate2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('affiliate_award');
        $this->dropTable('affiliate_history');
        $this->dropTable('affiliate_user_source');
        $this->dropTable('affiliate_resource');
        $this->dropTable('affiliate_award_rule');
        $this->dropTable('affiliate');

        $this->execute(<<<SQL
CREATE TABLE `affiliate_source` (
  `uuid` char(7) NOT NULL,
  `created_at` datetime NOT NULL,
  `company_id` int(11) NOT NULL,
  `api_external_system_id` int(11) DEFAULT NULL,
  `type` enum('u','s','a','e') NOT NULL,
  `utm` varchar(8) DEFAULT NULL,
  `award_type` enum('treatstock_fee_percent','fixed') NOT NULL,
  `award_config` json NOT NULL,
  `followers_count` int(11) NOT NULL,
  `inactive` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uuid`) USING BTREE,
  KEY `company_id` (`company_id`),
  KEY `api_external_system` (`api_external_system_id`),
  CONSTRAINT `affiliate_api_ext_system` FOREIGN KEY (`api_external_system_id`) REFERENCES `api_external_system` (`id`),
  CONSTRAINT `affiliate_source_company_id` FOREIGN KEY (`company_id`) REFERENCES `ps` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(<<<SQL
CREATE TABLE `affiliate_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_source_uuid` char(7) NOT NULL,
  `price` decimal(14,2) NOT NULL,
  `currency` char(3) NOT NULL,
  `created_at` datetime NOT NULL,
  `invoice_uuid` varchar(6) NOT NULL,
  `accrued_payment_detail_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invoice_uuid` (`invoice_uuid`) USING BTREE,
  KEY `accrued_payment_detail_id` (`accrued_payment_detail_id`),
  KEY `affiliate_source_uuid_indx` (`affiliate_source_uuid`) USING BTREE,
  CONSTRAINT `affiliate_award_accrued_detail_id` FOREIGN KEY (`accrued_payment_detail_id`) REFERENCES `payment_detail` (`id`),
  CONSTRAINT `affiliate_award_invoice_uuid` FOREIGN KEY (`invoice_uuid`) REFERENCES `payment_invoice` (`uuid`),
  CONSTRAINT `affiliate_award_source_uuid` FOREIGN KEY (`affiliate_source_uuid`) REFERENCES `affiliate_source` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
        $this->execute(<<<SQL
CREATE TABLE `affiliate_source_client` (
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `affiliate_source_uuid` char(7) NOT NULL,
  UNIQUE KEY `user_id_2` (`user_id`),
  KEY `affiliate_source_uuid` (`affiliate_source_uuid`),
  CONSTRAINT `affiliate_user_bind_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_affiliate_source_uuid_client` FOREIGN KEY (`affiliate_source_uuid`) REFERENCES `affiliate_source` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
        $this->execute('drop view view_orderlist');
        $this->execute(<<<SQL
CREATE VIEW `view_orderlist` AS
    SELECT 
        `o`.`id` AS `order_id`,
        `o`.`user_id` AS `buyer_id`,
        `o`.`billed_at` AS `billed_at`,
        `m3`.`user_id` AS `designer_id`,
        `o`.`created_at` AS `created_at`,
        `atdates`.`accepted_date` AS `accepted_at`,
        `oh2`.`created_at` AS `ready_sent_at`,
        `atdates`.`delivered_at` AS `delivered_at`,
        `atdates`.`shipped_at` AS `shipped_at`,
        `atdates`.`received_at` AS `received_at`,
        `geoc`.`iso_code` AS `to_country`,
        `shipto`.`city` AS `to_city`,
        `o`.`comment` as `comment`,
        `psp`.`id` AS `printer_id`,
        `psp`.`title` AS `printer_title`,
        `geoc2`.`iso_code` AS `ps_country`,
        `dt`.`title` AS `delivery_type`,
        CONCAT(`atdl`.`tracking_number`,
                ' - ',
                `atdl`.`tracking_shipper`) AS `delivery_opts`,
        `pi`.`total_amount` AS `total_amount`,
        `pi`.`currency` AS `currency`
    FROM
        (((((((((((((((((`store_order` `o`
        LEFT JOIN `payment_invoice` `pi` ON ((`pi`.`uuid` = `o`.`primary_payment_invoice_uuid`)))
        LEFT JOIN `store_order_attemp` `attemp` ON ((`attemp`.`id` = `o`.`current_attemp_id`)))
        LEFT JOIN `store_order_attemp_dates` `atdates` ON ((`atdates`.`attemp_id` = `attemp`.`id`)))
        LEFT JOIN `store_order_attemp_delivery` `atdl` ON ((`atdl`.`order_attemp_id` = `attemp`.`id`))))
        LEFT JOIN `store_order_item` `soi` ON ((`soi`.`order_id` = `o`.`id`)))
        LEFT JOIN `store_unit` `su` ON ((`su`.`id` = `soi`.`unit_id`)))
        LEFT JOIN `model3d` `m3` ON ((`m3`.`id` = `su`.`model3d_id`)))
        LEFT JOIN `user_address` `shipto` ON ((`shipto`.`id` = `o`.`ship_address_id`)))
        LEFT JOIN `geo_country` `geoc` ON ((`geoc`.`id` = `shipto`.`country_id`)))
        LEFT JOIN `company_service` `psm` ON ((`psm`.`id` = `attemp`.`machine_id`)))
        LEFT JOIN `ps_printer` `psp` ON ((`psp`.`id` = `psm`.`ps_printer_id`)))
        LEFT JOIN `user_location` `l` ON ((`l`.`id` = `psm`.`location_id`)))
        LEFT JOIN `geo_country` `geoc2` ON ((`geoc2`.`id` = `l`.`country_id`)))
        LEFT JOIN `store_order_history` `oh2` ON (((`oh2`.`order_id` = `o`.`id`)
            AND (`oh2`.`action_id` = 'order_status')
            AND (`oh2`.`comment` = 'old[printed] new[ready_send]'))))
        LEFT JOIN `delivery_type` `dt` ON ((`dt`.`id` = `o`.`delivery_type_id`)))
        LEFT JOIN `store_order_history` `oh3` ON (((`oh3`.`order_id` = `o`.`id`)
            AND (`oh3`.`action_id` = 'order_status')
            AND (`oh3`.`comment` = 'old[printed] new[ready_send]'))))
    WHERE
        (`o`.`user_id` <> 1)
        ORDER BY `o`.`created_at` desc;
SQL
        );

    }

    public function safeDown()
    {
        return false;
    }
}