<?php

use yii\db\Migration;
use yii\helpers\Json;

class m161214_135825_3284_delivery_refactor extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `ps_printer_delivery` 
            ADD COLUMN `carrier` VARCHAR(20) NOT NULL AFTER `comment`,
            ADD COLUMN `carrier_price` DECIMAL(7,2) NULL AFTER `carrier`,
            ADD COLUMN `free_delivery` DECIMAL(7,2) NULL AFTER `carrier_price`;
        ");

        $data = Yii::$app->db->createCommand("SELECT id, delivery_details FROM ps_printer_delivery")->queryAll();
        foreach ($data as $item){
            $columns = Json::decode($item['delivery_details']);
            unset($columns['currency']);
            $this->update('ps_printer_delivery', $columns, ['id' => $item['id']]);
        }
        $this->dropColumn('ps_printer_delivery', 'delivery_details');
    }

    public function down()
    {
        echo "m161214_135825_3284_delivery_refactor cannot be reverted.\n";

        return false;
    }
}
