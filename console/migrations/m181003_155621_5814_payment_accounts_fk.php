<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m181003_155621_5814_payment_accounts_fk
 *
 * Set at least one transaction history record
 */
class m181003_155621_5814_payment_accounts_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `payment_detail` DROP FOREIGN KEY `fk_payment_detail_2`;');
        $this->dropColumn('payment_detail', 'user_id');

        $this->execute('ALTER TABLE `payment_detail` 
        DROP FOREIGN KEY `fk_payment_detail_4`;
        ALTER TABLE `payment_detail` 
        CHANGE COLUMN `payment_account_id` `payment_account_id` INT(11) NOT NULL ;
        ALTER TABLE `payment_detail` 
        ADD CONSTRAINT `fk_payment_detail_4`
          FOREIGN KEY (`payment_account_id`)
          REFERENCES `payment_account` (`id`);
        ');

        $this->addColumn('payment_invoice', 'user_session_id', 'integer(11) null after user_id');
        $this->update('payment_invoice', ['user_session_id' => 1]);
        $this->alterColumn('payment_invoice', 'user_session_id', 'integer(11) not null');
        $this->addForeignKey('payment_invoice_user_session_id_fk', 'payment_invoice', 'user_session_id', 'user_session', 'id');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
