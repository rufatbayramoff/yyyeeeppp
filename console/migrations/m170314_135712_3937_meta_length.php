<?php

use yii\db\Migration;

class m170314_135712_3937_meta_length extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `seo_page` 
            CHANGE COLUMN `meta_description` `meta_description` VARCHAR(180) NULL DEFAULT NULL ,
            CHANGE COLUMN `meta_keywords` `meta_keywords` VARCHAR(150) NULL DEFAULT NULL ;
        ');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
