<?php

use yii\db\Migration;

class m170412_152128_2261_attmpt_moderation extends Migration
{
    public function up()
    {

        $this->execute("
            CREATE TABLE `store_order_attempt_moderation` (
              `attempt_id` INT UNSIGNED NOT NULL,
              `status` ENUM('new', 'accepted', 'rejected') NOT NULL,
              `reject_reason_id` INT NULL,
              `reject_comment` TEXT NULL,
              PRIMARY KEY (`attempt_id`),
              INDEX `fk_store_order_attempt_moderation_2_idx` (`reject_reason_id` ASC),
              CONSTRAINT `fk_store_order_attempt_moderation_1`
                FOREIGN KEY (`attempt_id`)
                REFERENCES `store_order_attemp` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_store_order_attempt_moderation_2`
                FOREIGN KEY (`reject_reason_id`)
                REFERENCES `system_reject` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION);
        ");
        
        
        
        $this->execute("
        CREATE TABLE `store_order_attempt_moderation_file` (
          `id` INT UNSIGNED AUTO_INCREMENT,
          `attempt_id` INT UNSIGNED NOT NULL,
          `file_id` INT NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `fk_store_order_attempt_moderation_file_1_idx` (`file_id` ASC),
          INDEX `fk_store_order_attempt_moderation_file_2_idx` (`attempt_id` ASC),
          CONSTRAINT `fk_store_order_attempt_moderation_file_1`
            FOREIGN KEY (`file_id`)
            REFERENCES `file` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_store_order_attempt_moderation_file_2`
            FOREIGN KEY (`attempt_id`)
            REFERENCES `store_order_attempt_moderation` (`attempt_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);

        ");
        
    }

    public function down()
    {

        $this->dropTable('store_order_attempt_moderation_file');
        $this->dropTable('store_order_attempt_moderation');
        return true;
    }
}
