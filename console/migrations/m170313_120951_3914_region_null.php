<?php

use yii\db\Migration;

class m170313_120951_3914_region_null extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `user_location` 
            DROP FOREIGN KEY `fk_user_loc`;
            ALTER TABLE`user_location` 
            CHANGE COLUMN `region_id` `region_id` INT(11) NULL ;
            ALTER TABLE `user_location` 
            ADD CONSTRAINT `fk_user_loc`
              FOREIGN KEY (`region_id`)
              REFERENCES `geo_region` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
            ');
    }

    public function down()
    {
        $this->execute('ALTER TABLE `user_location` 
            DROP FOREIGN KEY `fk_user_loc`;
            ALTER TABLE `user_location` 
            CHANGE COLUMN `region_id` `region_id` INT(11) NOT NULL ;
            ALTER TABLE  `user_location` 
            ADD CONSTRAINT `fk_user_loc`
              FOREIGN KEY (`region_id`)
              REFERENCES  `geo_region` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
