<?php

use yii\db\Migration;

/**
 * Class m180417_155721_5186_product_price
 */
class m180417_155721_5186_product_price extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('product', 'single_price', 'decimal(6,2)');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'single_price');
    }
}
