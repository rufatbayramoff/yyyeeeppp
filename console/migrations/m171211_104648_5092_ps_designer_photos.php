<?php

use yii\db\Migration;

class m171211_104648_5092_ps_designer_photos extends Migration
{

    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `ps` 
            ADD COLUMN `designer_picture_file_ids` TEXT NULL AFTER `picture_file_ids`;
        ");
    }

    public function safeDown()
    {
        $this->dropColumn("ps", 'designer_picture_file_ids');
        return true;
    }
}
