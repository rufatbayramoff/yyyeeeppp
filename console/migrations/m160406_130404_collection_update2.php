<?php

use yii\db\Migration;

class m160406_130404_collection_update2 extends Migration
{
    public function up()
    {
        $this->execute("UPDATE user_collection SET title='My Favorites' WHERE title='My Likes'");
        $this->execute("ALTER TABLE `user_collection` 
            CHANGE COLUMN `items_count` `items_count` INT(11) UNSIGNED NOT NULL DEFAULT '0' ;");
    }

    public function down()
    {
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
