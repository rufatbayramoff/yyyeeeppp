<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;

/**
 * Class m190830_175621_1893_browser_push
 */
class m190830_175621_1893_browser_push_subscription extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
CREATE TABLE `browser_push_subscription` (
  `uid` char(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `user_agent` varchar(1024) NOT NULL,
  `endpoint` varchar(4048) NOT NULL,
  `expiration_time` datetime DEFAULT NULL,
  `key_auth` varchar(1024) NOT NULL,
  `key_p` varchar(4048) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        $this->execute('
ALTER TABLE `browser_push_subscription`
  ADD PRIMARY KEY (`uid`); ');

        $this->execute('
CREATE TABLE `browser_push_subscription_user`
(
    `uid`                          CHAR(6)  NOT NULL,
    `created_at`                   DATETIME NOT NULL,
    `browser_push_subscription_uid` CHAR(6)  NOT NULL,
    `user_id`                      INT(11)  NOT NULL,
    `ip`                           CHAR(16) NOT NULL,
    PRIMARY KEY (`uid`),
    INDEX `browser_push_subscription_uid_indx` (`browser_push_subscription_uid`),
    INDEX `broser_push_subscr_users_user_id_indx` (`user_id`)
) ENGINE = InnoDB;
        ');

        $this->execute('
ALTER TABLE `browser_push_subscription_user`
    ADD CONSTRAINT `fk_browser_push_subscription_uid` FOREIGN KEY (`browser_push_subscription_uid`) REFERENCES `browser_push_subscription` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `browser_push_subscription_user`
    ADD CONSTRAINT `fk_browser_push_subscription_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
        ');

        $this->execute('
        CREATE TABLE `notify_popup` (
  `uid` varchar(40) NOT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(4048) NOT NULL,
  `text` varchar(4048) NOT NULL,
  `url` varchar(2048) DEFAULT NULL,
  `expire_at` datetime NOT NULL,
  `code` varchar(1024) NULL,
  `params` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        $this->execute('
ALTER TABLE `notify_popup` ADD PRIMARY KEY (`uid`) USING BTREE;
');

        $this->execute('
ALTER TABLE `notify_popup`
  ADD CONSTRAINT `fk_notify_popup_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
');
        $this->execute('
CREATE TABLE `notify_popup_endpoint` ( `notify_popup_uid` VARCHAR(40) NOT NULL , `endpoint` VARCHAR(1024) NOT NULL , `received_at` DATETIME NULL , INDEX `notify_popup_endpoint_indx` (`endpoint`)) ENGINE = InnoDB;
');
        $this->execute('
ALTER TABLE `notify_popup_endpoint` ADD INDEX `notify_popup_uid_uid_indx` (`notify_popup_uid`);
        ');

        $this->execute('
ALTER TABLE `notify_popup_endpoint`
  ADD CONSTRAINT `fk_notify_popup_endpoint_uid` FOREIGN KEY (`notify_popup_uid`) REFERENCES `notify_popup` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;
');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_browser_push_subscription_uid', 'browser_push_subscription_user');
        $this->dropForeignKey('fk_browser_push_subscription_user_id', 'browser_push_subscription_user');
        $this->dropTable('browser_push_subscription_user');
        $this->dropTable('browser_push_subscription');
        $this->dropForeignKey('fk_notify_popup_endpoint_uid', 'notify_popup_endpoint');
        $this->dropTable('notify_popup_endpoint');
        $this->dropForeignKey('fk_notify_popup_user_id', 'notify_popup');
        $this->dropTable('notify_popup');
    }
}