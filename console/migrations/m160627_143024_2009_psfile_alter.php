<?php

use yii\db\Migration;

class m160627_143024_2009_psfile_alter extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("ALTER TABLE `ps_printer_file_status` ADD COLUMN `moderator_status` VARCHAR(15) NOT NULL DEFAULT 'new' AFTER `progress`;");
    }

    public function safeDown()
    {
        $this->dropColumn('ps_printer_file_status', 'moderator_status');
    }
}
