<?php

use yii\db\Migration;

/**
 * Class m180904_144729_add_mail_client_review_answer
 */
class m180904_144729_add_mail_client_review_answer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(
            'email_template',
            [
                'code'          => 'clientReviewAnswer',
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => "You've received a reply to your review for order #%orderId%",
                'description'   => 'Params: clientName, psName, orderId, link',
                'updated_at'    => dbexpr('NOW()'),
                'template_html' => "Hi %clientName%,

%psName% has replied to your review for order #%orderId%.
<a href=\"%link%\" style=\"display: inline-block;margin:10px 0;border-radius: 0px;font-family: sans-serif;padding: 5px 20px;min-width: 40px;font-size: 16px;font-weight: normal;line-height: 20px;border: none;text-decoration:none !important;color: #fff;background-color:#2d8ee0;\">View reply</a>

Best,
Treatstock",
                'template_text' => ""
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'clientReviewAnswer', 'language_id' => 'en-US']);
    }
}
