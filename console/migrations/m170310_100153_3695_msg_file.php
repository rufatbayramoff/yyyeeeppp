<?php

use yii\db\Migration;

class m170310_100153_3695_msg_file extends Migration
{
    public function up()
    {

        $this->execute("
            CREATE TABLE `msg_file` (
              `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
              `topic_id` INT UNSIGNED NOT NULL,
              `created_at` TIMESTAMP NOT NULL,
              `message_id` INT UNSIGNED NOT NULL,
              `user_id` INT NOT NULL,
              `file_id` INT NOT NULL,
              `status` VARCHAR(45) NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `message` (`message_id` ASC),
              INDEX `topic_user` (`topic_id` ASC, `user_id` ASC),
              INDEX `fk_user_idx` (`user_id` ASC),
              INDEX `fk_file_idx` (`file_id` ASC),
              CONSTRAINT `fk_msg_file_topic`
                FOREIGN KEY (`topic_id`)
                REFERENCES `msg_topic` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_msg_file_message`
                FOREIGN KEY (`message_id`)
                REFERENCES `msg_message` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_msg_file_user`
                FOREIGN KEY (`user_id`)
                REFERENCES `user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_msg_file_file`
                FOREIGN KEY (`file_id`)
                REFERENCES `file` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION);
        ");

        $this->execute("ALTER TABLE `msg_message` CHANGE COLUMN `text` `text` TEXT NULL ;");
    }

    public function down()
    {
        $this->dropTable('msg_file');
        $this->execute("ALTER TABLE `msg_message` CHANGE COLUMN `text` `text` TEXT NOT NULL ;");
        return true;
    }
}
