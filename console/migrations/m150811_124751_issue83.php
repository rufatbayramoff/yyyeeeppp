<?php

use yii\db\Schema;
use yii\db\Migration;

class m150811_124751_issue83 extends Migration
{
    public function up()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $sql = "ALTER TABLE  `store_units2category` 
                ADD CONSTRAINT `fk_store_units2category_3`
                  FOREIGN KEY (`unit_id`)
                  REFERENCES  `store_unit` (`id`)
                  ON DELETE NO ACTION
                  ON UPDATE NO ACTION";
        $sql2 = "ALTER TABLE `model3d` 
            CHANGE COLUMN `is_moderated` `is_moderated` TINYINT(1) NOT NULL DEFAULT 0 ,
            CHANGE COLUMN `is_printer_ready` `is_printer_ready` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'G code ready'";
        $this->execute($sql);
        $this->execute($sql2);
        
        $sql3 = "ALTER TABLE `osn_vendor` 
            CHANGE COLUMN `code` `code` CHAR(25) NOT NULL ;
            ALTER TABLE `osn_vendor` 
            ADD CONSTRAINT `fk_osn_vendor_1`
              FOREIGN KEY (`code`)
              REFERENCES `user_osn` (`osn_code`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;";
        $this->execute($sql3);
        $this->execute("ALTER TABLE `model3d_file`  DROP COLUMN `is_image`;");
        
        $this->execute("ALTER TABLE `model3d` CHANGE COLUMN `is_moderated` `is_published` TINYINT(1) NOT NULL DEFAULT '0'");
        return 0;
    }

    public function down()
    {
        echo "m150811_124751_issue83 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
