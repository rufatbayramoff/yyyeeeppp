<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200527_123421_7425_rejected_quotes
 */
class m200527_123421_7425_rejected_quotes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $preorders = (new Query())
            ->select('preorder.id')
            ->from('preorder')
            ->leftJoin('store_order', 'store_order.preorder_id=preorder.id')
            ->leftJoin('payment_invoice', 'payment_invoice.uuid = store_order.primary_payment_invoice_uuid')
            ->andWhere(['preorder.status'=>'accepted'])
            ->andWhere(['store_order.order_state'=>'removed'])
            ->andWhere(['payment_invoice.status'=>'new'])
            ->all();
        foreach ($preorders as $preorder) {
            echo "\nRejected preorder: ".$preorder['id'];
            $this->update('preorder', ['status' => 'rejected'], ['id'=>$preorder['id']]);
        }
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}