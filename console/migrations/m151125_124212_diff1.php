<?php

use yii\db\Schema;
use yii\db\Migration;

class m151125_124212_diff1 extends Migration
{
    public function up()
    {
        $dbtr = $this->db->beginTransaction();
        /**
         * printer
         * printer_to_property
         * printer_to_material
         */
        try{            
            $this->execute('SET FOREIGN_KEY_CHECKS=0');
            $this->truncateTable('printer');
            $this->truncateTable('printer_to_property');
            $this->truncateTable('printer_to_material');
            
            $this->truncateTable('store_category');
            $this->truncateTable('store_category_intl');            
            
            $this->importFile('diffs - 1. Two printers.sql');
            $this->importFile('diffs - 2. Other printers.sql');
            $this->importFile('diffs - 3. Printer - Material.sql');
            $this->importFile('diffs - Categories.sql');
            $dbtr->commit();
            $this->execute('SET FOREIGN_KEY_CHECKS=1');
        }catch(\Exception $e){
            $dbtr->rollBack();
            echo $e->getMessage();
        }
        return 0;
    }
    private function importFile($filePath)
    {
        $path = realpath(dirname(__DIR__) . '/../db/');
        if(!file_exists($path . '/' . $filePath)){
            return false;
        }
        $sqlString = file_get_contents($path . '/' . $filePath);        
        $sqls = explode(";", $sqlString);
        foreach($sqls as $sql){
            $sql = trim($sql);
            if(empty($sql)){
                continue;
            }
            $this->execute($sql);
        }
        return true;

    }
    public function down()
    {
         
    }
}
