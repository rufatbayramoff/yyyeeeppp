<?php

use yii\db\Migration;

class m170918_153212_4743_ps_count_orders extends Migration
{
    public function safeUp()
    {
        $this->execute(
            '
CREATE TABLE `ps_progress_orders_count` (
  `ps_id` INT(11) NOT NULL,
  `progress_orders_count` INT(11) NOT NULL,
  UNIQUE KEY `ps_progress_orders_count_ps_id` (`ps_id`),
  CONSTRAINT `fk_ps_progress_orders_count_ps_id` FOREIGN KEY (`ps_id`) REFERENCES `ps` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        '
        );
        $this->addColumn('ps', 'max_progress_orders_count', 'int(11) null');
    }

    public function safeDown()
    {
        $this->dropTable('ps_progress_orders_count');
        $this->dropColumn('ps', 'max_progress_orders_count');
    }
}
