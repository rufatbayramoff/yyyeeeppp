<?php

use yii\db\Migration;

/**
 * Class m180620_152906_5568_product_certification
 */
class m180620_152906_5568_product_certification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            'CREATE TABLE `product_certification` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `title` varchar(45) NOT NULL,
          `file_id` int(11) NOT NULL,
          `certifier` varchar(45) DEFAULT NULL,
          `application` varchar(45) DEFAULT NULL,
          `created_at` datetime NOT NULL,
          `issue_date` date DEFAULT NULL,
          `expire_date` date DEFAULT NULL,
          `product_uuid` VARCHAR(32) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `fk_product_certification_1_idx` (`product_uuid`),
          KEY `fk_product_certification_2_idx` (`file_id`),
          CONSTRAINT `fk_product_certification_1` FOREIGN KEY (`product_uuid`) REFERENCES `product` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
          CONSTRAINT `fk_product_certification_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        '
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('product_certification');
        $this->dropTable('product_certification');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180620_152906_5568_product_certification cannot be reverted.\n";

        return false;
    }
    */
}
