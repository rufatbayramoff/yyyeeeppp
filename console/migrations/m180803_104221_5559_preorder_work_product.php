<?php

use yii\db\Migration;

/**
 * Class m180803_104221_5559_preorder_work_product
 */
class m180803_104221_5559_preorder_work_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('preorder_work', 'product_uuid', 'varchar(32) null after title');
        $this->addColumn('preorder_work', 'company_service_id', 'int(11) null after product_uuid');
        $this->addColumn('preorder_work', 'qty', 'int(11) null after company_service_id');
        $this->alterColumn('preorder_work', 'title', 'varchar(255) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('preorder_work', 'product_uuid');
        $this->dropColumn('preorder_work', 'company_service_id');
        $this->dropColumn('preorder_work', 'qty');
    }
}
