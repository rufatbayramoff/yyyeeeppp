<?php

use yii\db\Migration;

/**
 * Class m180801_163921_5559_preorder_qty
 */
class m180801_163921_5559_preorder_qty extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('preorder', 'qty', 'int(11) null after message');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('preorder', 'qty');
    }
}
