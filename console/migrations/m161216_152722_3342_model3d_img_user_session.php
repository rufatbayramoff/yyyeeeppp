<?php

use yii\db\Migration;
use yii\db\Query;

class m161216_152722_3342_model3d_img_user_session extends Migration
{

    public function up()
    {
        $this->alterColumn('model3d_img', 'user_id', 'integer(11) null');
    }

    public function down()
    {
    }
}
