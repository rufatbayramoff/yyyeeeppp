<?php

use yii\db\Migration;

class m170929_110421_4028_model3d_title_required extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('model3d', 'title', 'varchar(45) not null DEFAULT ""');
    }

    public function safeDown()
    {
    }
}
