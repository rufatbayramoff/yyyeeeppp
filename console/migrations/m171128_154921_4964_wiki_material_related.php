<?php

use yii\db\Migration;

class m171128_154921_4964_wiki_material_related extends Migration
{
    public function safeUp()
    {
        $this->execute(
            "
CREATE TABLE `wiki_material_related` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wiki_material_id` INT(11) NOT NULL,
  `link_wiki_material_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unque_link` (`wiki_material_id`,`link_wiki_material_id`),
  KEY `wiki_material_id` (`wiki_material_id`),
  KEY `link_wiki_material_id` (`link_wiki_material_id`),
  CONSTRAINT `wiki_material_related_id` FOREIGN KEY (`wiki_material_id`) REFERENCES `wiki_material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wiki_material_related_link_id` FOREIGN KEY (`link_wiki_material_id`) REFERENCES `wiki_material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        "
        );

        $this->execute(
            "
CREATE TABLE `wiki_material_geo_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wiki_material_id` INT(11) NOT NULL,
  `geo_city_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unque_link` (`wiki_material_id`,`geo_city_id`),
  KEY `wiki_material_id` (`wiki_material_id`),
  KEY `geo_city_id` (`geo_city_id`),
  CONSTRAINT `wiki_material_geo_city_city_id` FOREIGN KEY (`geo_city_id`) REFERENCES `geo_city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wiki_material_geo_city_material_id` FOREIGN KEY (`wiki_material_id`) REFERENCES `wiki_material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        "
        );
    }

    public function safeDown()
    {
        $this->dropTable('wiki_material_related');
        $this->dropTable('wiki_material_geo_city');
    }
}
