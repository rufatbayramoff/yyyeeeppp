<?php

use yii\db\Migration;

class m170907_091406_4750_cart_to_machine extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `cart_item` 
            ADD COLUMN `machine_id` INT(11) NOT NULL AFTER `ps_printer_id`;"
        );

        $this->execute("
            UPDATE cart_item, ps_machine  
            SET machine_id = ps_machine.id
            WHERE cart_item.ps_printer_id = ps_machine.ps_printer_id;
        ");

        $this->execute("
            ALTER TABLE `cart_item` 
            ADD INDEX `fk_cart_item_1_idx` (`machine_id` ASC);
            ALTER TABLE `cart_item` 
            ADD CONSTRAINT `fk_cart_item_1`
              FOREIGN KEY (`machine_id`)
              REFERENCES `ps_machine` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;        
        ");


        $this->execute("
            ALTER TABLE `cart_item` 
            DROP FOREIGN KEY `cart_item_ps_printer_id`;
            ALTER TABLE `cart_item` 
            DROP COLUMN `ps_printer_id`,
            DROP INDEX `cart_item_ps_printer_id` ;
        ");


    }

    public function safeDown()
    {
        echo "m170907_091406_4750_cart_to_machine cannot be reverted.\n";

        return false;
    }
}
