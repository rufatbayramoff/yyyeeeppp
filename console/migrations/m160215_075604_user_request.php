<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_075604_user_request extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `user_request` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `status` varchar(15) NOT NULL DEFAULT 'new',
            `moderator_id` int(11) DEFAULT NULL,
            `request_type` varchar(45) NOT NULL DEFAULT 'delete',
            PRIMARY KEY (`id`),
            UNIQUE KEY `urq_user_status` (`user_id`,`status`),
            KEY `fk_user_request_2_idx` (`moderator_id`),
            CONSTRAINT `fk_user_request_2` 
                FOREIGN KEY (`moderator_id`) REFERENCES `user_admin` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_user_request_1` 
                FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $this->execute("ALTER TABLE `user_request` ADD COLUMN `code` VARCHAR(45) NOT NULL AFTER `request_type`;");
    }

    public function down()
    {
         $this->truncateTable("user_request");
         $this->dropTable("user_request");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
