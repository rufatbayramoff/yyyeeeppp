<?php

use yii\db\Migration;

/**
 * Class m180514_121719_5524_new_emails
 */
class m180514_121719_5524_new_emails extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('email_template', [
            'code' => 'ps.moderation.approved',
            'group' => 'service',
            'language_id' => 'en-US',
            'title' => 'Publishing of %companyName% on Treatstock',
            'description' =>  '',
            'template_html' => '
                Hi %companyName%,

                You "%companyName%" was published on Treatstock.
                
                Best,
                Treatstock
            ',
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);

        $this->insert('email_template', [
            'code' => 'ps.moderation.banned',
            'group' => 'service',
            'language_id' => 'en-US',
            'title' => 'Publishing of %companyName% on Treatstock',
            'description' =>  '',
            'template_html' => '
                Hi %companyName%,
    
                We\'re sorry but "%companyName%" was banned on Treatstock. Please take a look at the reason and comments below. Thank you.
                
                Reason: %reason%
                Comment: %comment%
                
                Best,
                Treatstock
            ',
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);

        $this->insert('email_template', [
            'code' => 'ps.service.modeartion.approved',
            'group' => 'service',
            'language_id' => 'en-US',
            'title' => 'Publishing of %serviceName% on Treatstock',
            'description' =>  '',
            'template_html' => '
                Hi %companyName%,
    
                You "%serviceName%" was published on Treatstock.
                
                Best,
                Treatstock
            ',
            'updated_at' => dbexpr('NOW()'),
            'is_active' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'ps.moderation.approved']);
        $this->delete('email_template', ['code' => 'ps.moderation.banned']);
        $this->delete('email_template', ['code' => 'ps.service.modeartion.approved']);
        return true;
    }
}
