<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.07.18
 * Time: 17:07
 */


use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180720_170721_5706_product_images
 */
class m180720_170721_5706_product_images extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `product_image` ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);');
        $this->addColumn('product_image', 'pos', 'int(11) not null default 0');
        $images = (new Query())->select('*')->from('product_image')->all();
        $pos = 1;
        foreach ($images as $image) {
            $this->update('product_image', ['pos' => $pos], "file_uuid='" . $image['file_uuid'] . "'");
            $pos++;
        }

        $products = (new Query())->select('*')->from('product')->where('cover_image_file_uuid is not null')->all();
        foreach ($products as $product) {
            $this->insert('product_image', ['product_uuid' => $product['uuid'], 'file_uuid' => $product['cover_image_file_uuid'], 'pos' => 0]);
        }
        $this->dropForeignKey('fk_product_cover_image_file_uuid', 'product');
        $this->dropColumn('product', 'cover_image_file_uuid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}

