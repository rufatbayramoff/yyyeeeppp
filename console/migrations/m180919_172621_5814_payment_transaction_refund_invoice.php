<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180919_172621_5814_payment_transaction_refund
 */
class m180919_172621_5814_payment_transaction_refund_invoice extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_transaction_refund', 'payment_invoice_uuid', 'varchar(6) after id');
        $this->createIndex('payment_transaction_refund_invoice_uuid_indx', 'payment_transaction_refund', 'payment_invoice_uuid');
        $this->addForeignKey('payment_transaction_refund_invoice_fk', 'payment_transaction_refund', 'payment_invoice_uuid', 'payment_invoice', 'uuid');

    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
