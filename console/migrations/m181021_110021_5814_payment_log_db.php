<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m181021_110021_5814_payment_log_db
 *
 * Payment log db
 */
class m181021_110021_5814_payment_log_db extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('payment_invoice', 'order_id', 'store_order_id');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
