<?php

use yii\db\Migration;

/**
 * Class m210303_162216_8327_intl_may_be_null
 */
class m210303_162216_8327_intl_may_be_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('company_service_type_intl', 'title', 'varchar(255) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210301_102216_update_ps_preorder_new_email_template cannot be reverted.\n";
        return false;
    }
}
