<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.03.18
 * Time: 10:33
 */

use yii\db\Migration;

/**
 * Class m180321_103321_5377_model3d_part_converted
 */
class m180321_103321_5377_model3d_part_converted extends Migration
{
    public function safeUp()
    {
        $this->addColumn('model3d_part', 'converted_from_part_id', 'int(11) NULL');
        $this->addColumn('model3d_replica_part', 'converted_from_part_id', 'int(11) NULL');

        $this->addForeignKey('fk_model3d_part_converted_from_id', 'model3d_part', 'converted_from_part_id', 'model3d_part', 'id');
        $this->addForeignKey('fk_model3d_replica_part_converted_from_id', 'model3d_replica_part', 'converted_from_part_id', 'model3d_part', 'id');

        $this->addColumn('model3d_part', 'is_common_view', 'tinyint(1) NOT NULL DEFAULT \'0\'');
        $this->addColumn('model3d_replica_part', 'is_common_view', 'tinyint(1) NOT NULL DEFAULT \'0\'');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_model3d_part_converted_from_id', 'model3d_part');
        $this->dropForeignKey('fk_model3d_replica_part_converted_from_id', 'model3d_replica_part');

        $this->dropColumn('model3d_part', 'converted_from_part_id');
        $this->dropColumn('model3d_replica_part', 'converted_from_part_id');

        $this->dropColumn('model3d_part', 'is_common_view');
        $this->dropColumn('model3d_replica_part', 'is_common_view');
    }
}