<?php

use yii\db\Migration;

class m170410_143527_3844_only_cert_printers extends Migration
{
    public function up()
    {
        $this->addColumn("api_external_system", "only_certificated_printers", "TINYINT(1) DEFAULT 0");
    }

    public function down()
    {
        $this->dropColumn("api_external_system", "only_certificated_printers");
        return true;
    }
}
