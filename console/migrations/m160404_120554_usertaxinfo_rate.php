<?php

use yii\db\Migration;

class m160404_120554_usertaxinfo_rate extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_tax_info` 
            ADD COLUMN `rsa_tax_rate` INT(11) NULL DEFAULT NULL COMMENT 'personal rsa tax rate for user' AFTER `encrypted`;");
        
        $this->execute("ALTER TABLE `user_tax_info` 
                            ADD COLUMN `email_notify` TINYINT(1) NOT NULL DEFAULT 1 AFTER `encrypted`;");
        
    }

    public function down()
    {
        $this->dropColumn('user_tax_info', 'rsa_tax_rate');
        $this->dropColumn("user_tax_info", "email_notify");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
