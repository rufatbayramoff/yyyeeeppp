<?php

use yii\db\Migration;
use yii\db\Query;

class m170711_131621_4499_order_dispute extends Migration
{
    public function up()
    {
        $this->addColumn('store_order', 'is_dispute_open', 'int(1) default 0 not null');
    }

    public function down()
    {
        $this->dropColumn('store_order', 'is_dispute_open');
    }

}
