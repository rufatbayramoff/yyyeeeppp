<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200402_162221_7256_user_session_index
 */
class m200402_162221_7256_user_session_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `user_session` ADD INDEX(`created_at`)');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}