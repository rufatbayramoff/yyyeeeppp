<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180919_173621_5814_payment_transaction_refund_invoice_data
 *
 * Invoice can be linked with several refunds, and several payments.
 */
class m180919_173621_5814_payment_transaction_refund_invoice_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $paymentTransactionRefunds = $this->db->createCommand('SELECT payment_transaction_refund.id, payment.payment_invoice_uuid  as uuid FROM `payment_transaction_refund` LEFT JOIN `payment_transaction` on payment_transaction_refund.transaction_id=payment_transaction.id LEFT JOIN payment_detail on payment_transaction.first_payment_detail_id=payment_detail.id LEFT JOIN payment_detail_operation on payment_detail_operation.uuid = payment_detail.payment_detail_operation_uuid LEFT JOIN payment on payment.id = payment_detail_operation.payment_id')->queryAll();
        foreach ($paymentTransactionRefunds as $paymentTransactionRefund) {
            $this->update('payment_transaction_refund', ['payment_invoice_uuid' => $paymentTransactionRefund['uuid']], 'id='.$paymentTransactionRefund['id']);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
