<?php

use yii\db\Migration;

class m160826_135823_2589_seo extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `seo_page` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `url` varchar(145) DEFAULT NULL,
          `title` varchar(145) DEFAULT NULL,
          `header` varchar(145) DEFAULT NULL,
          `meta_description` varchar(145) DEFAULT NULL,
          `meta_keywords` varchar(145) DEFAULT NULL,
          `header_text` mediumtext,
          `footer_text` mediumtext,
          `is_active` bit(1) NOT NULL DEFAULT b\'1\',
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`),
          UNIQUE KEY `url_UNIQUE` (`url`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      ');
    }

    public function down()
    {
        $this->truncateTable('seo_page');
        $this->dropTable('seo_page');

    }
}
