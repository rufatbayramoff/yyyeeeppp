<?php

use yii\db\Migration;

/**
 * Class m181030_150156_5814_paymentbt_transaction_migrate_full_response
 */
class m181030_150156_5814_paymentbt_transaction_migrate_full_response extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        app()->db->createCommand('update payment_transaction pt
set pt.last_original_transaction = (select full_response from paymentbt_transaction pbtt where pbtt.transaction_id = pt.transaction_id)')->execute();

        $this->dropForeignKey('fk_paymentbt_transaction_1', 'paymentbt_transaction');
        $this->dropForeignKey('fkstoreid', 'paymentbt_transaction');
        $this->dropForeignKey('fk_paymentbt_method_1', 'paymentbt_method');
        $this->dropForeignKey('fk_paymentbt_address_1', 'paymentbt_address');
        $this->dropForeignKey('fk_paymentbt_customer_1', 'paymentbt_customer');

        $this->dropTable('paymentbt_address');
        $this->dropTable('paymentbt_customer');
        $this->dropTable('paymentbt_method');
        $this->dropTable('paymentbt_transaction');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
