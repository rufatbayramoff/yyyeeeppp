<?php

use yii\db\Schema;
use yii\db\Migration;

class m150817_145000_pspm extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $sql = 'SET FOREIGN_KEY_CHECKS=0;
          ALTER TABLE ps_printer_material DROP FOREIGN KEY fk_printer_id_5;
          ALTER TABLE ps_printer_material CHANGE `printer_id` `ps_printer_id` INT(11) NOT NULL;
          ALTER TABLE ps_printer_material ADD CONSTRAINT `fk_printer_id_8` FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

          ALTER TABLE ps_printer_material CHANGE `title` `title` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
          ALTER TABLE ps_printer_color CHANGE `title` `title` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;';
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function safeDown()
    {
        echo 'cannot be undone';
    }
}
