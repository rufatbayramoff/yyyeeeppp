<?php

use yii\db\Migration;
use yii\db\Query;

class m161013_152631_2778_banned_words_settings extends Migration
{
    public function safeUp()
    {
        $this->insert(
            'system_setting',
            [
                'group_id'=>5,
                'key' => 'bannedWords',
                'value'=>'json',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'description' => 'Banned words list',
                'json' => '["fuck","bitch"]',
                'is_active' => 1
            ]
        );
    }

    public function safeDown()
    {
        return false;
    }

}
