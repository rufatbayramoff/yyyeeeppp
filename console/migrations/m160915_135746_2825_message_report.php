<?php

use yii\db\Migration;

class m160915_135746_2825_message_report extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('system_reject', ['group', 'title', 'is_active'], [
            ['report_message', 'Attempt to pay or communicate outside Treatstock', 1,],
            ['report_message', 'Solicitation services', 1,],
            ['report_message', 'Other', 1,],
        ]);

        $this->execute("
            CREATE TABLE `msg_report` ( 
                `id` Int( 11 ) UNSIGNED AUTO_INCREMENT NOT NULL,
                `message_id` Int( 11 ) UNSIGNED NOT NULL,
                `reason_id` Int( 11 ) NOT NULL,
                `create_user_id` Int( 11 ) NOT NULL,
                `comment` Text NULL,
                `moderated` Int( 1 ) UNSIGNED NOT NULL DEFAULT '0',
                `created_at` DateTime NOT NULL,
                PRIMARY KEY ( `id` ) )
            ENGINE = InnoDB
            AUTO_INCREMENT = 1;
        ");

        $this->execute("
            ALTER TABLE `msg_report`
                ADD CONSTRAINT `lnk_msg_report_user` FOREIGN KEY ( `create_user_id` )
                REFERENCES `user`( `id` )
                ON DELETE No Action
                ON UPDATE No Action;
        ");

        $this->execute("
            ALTER TABLE `msg_report`
                ADD CONSTRAINT `lnk_msg_report_msg_message` FOREIGN KEY ( `message_id` )
                REFERENCES `msg_message`( `id` )
                ON DELETE No Action
                ON UPDATE No Action;
	    ");

        $this->execute("
            ALTER TABLE `msg_report`
                ADD CONSTRAINT `lnk_msg_report_system_reject` FOREIGN KEY ( `reason_id` )
                REFERENCES `system_reject`( `id` )
                ON DELETE No Action
                ON UPDATE No Action;
	    ");
    }

    public function safeDown()
    {
        $this->delete('system_reject', ['group' => 'report_message']);
        $this->dropTable('msg_report');
        return true;
    }

}
