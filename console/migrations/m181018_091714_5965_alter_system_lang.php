<?php

use yii\db\Migration;

/**
 * Class m181018_091714_5965_alter_system_lang
 */
class m181018_091714_5965_alter_system_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `system_lang_source` ADD INDEX `index2` (`category` ASC);');
        $this->execute('ALTER TABLE `system_lang` ADD COLUMN `is_widget_active` TINYINT(1) NOT NULL DEFAULT 0 AFTER `url`;');

        $this->execute('update system_lang set is_widget_active=1 where is_active=1 AND id>0');
        $this->execute('ALTER TABLE `system_lang` ADD COLUMN `priority` INT(11) NOT NULL AFTER `is_widget_active`;');
        $this->execute('update system_lang set priority=id where id>0');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('system_lang', 'is_widget_active');
        $this->dropColumn('system_lang', 'priority');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181018_091714_5965_alter_system_lang cannot be reverted.\n";

        return false;
    }
    */
}
