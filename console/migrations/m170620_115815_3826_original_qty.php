<?php

use yii\db\Migration;

class m170620_115815_3826_original_qty extends Migration
{
    public function up()
    {

        $this->addColumn('model3d_replica_part', 'original_qty', 'INT(10) UNSIGNED NOT NULL');
        $this->execute("UPDATE model3d_replica_part SET original_qty = qty");
    }

    public function down()
    {
        $this->dropColumn('model3d_replica_part', 'original_qty');
        return true;
    }
}
