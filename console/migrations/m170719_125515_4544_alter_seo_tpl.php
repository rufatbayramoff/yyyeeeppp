<?php

use yii\db\Migration;

class m170719_125515_4544_alter_seo_tpl extends Migration
{
    public function safeUp()
    {
        $this->addColumn('seo_page_autofill_template', 'header_text', 'MEDIUMTEXT AFTER `is_apply_created`');
        $this->addColumn('seo_page_autofill_template', 'footer_text', 'MEDIUMTEXT AFTER `header_text`');
    }

    public function safeDown()
    {
        $this->dropColumn('seo_page_autofill_template', 'header_text');
        $this->dropColumn('seo_page_autofill_template', 'footer_text');
    }
}
