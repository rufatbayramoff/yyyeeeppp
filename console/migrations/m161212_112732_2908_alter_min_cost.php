<?php

use yii\db\Migration;

class m161212_112732_2908_alter_min_cost extends Migration
{
    public function up()
    {
        $this->truncateTable('printer_min_cost_hash');
        $this->alterColumn('printer_min_cost_hash', 'size_max', 'int not null');
        $this->addColumn('printer_min_cost_hash', 'size_max2', 'int not null after size_max');
        $this->addColumn('printer_min_cost_hash', 'size_max3', 'int not null after size_max2');
        $this->createIndex('printer_min_cost_hash_size_max', 'printer_min_cost_hash', ['size_max', 'size_max2', 'size_max3']);

    }

    public function down()
    {
        $this->dropIndex('printer_min_cost_hash_size_max', 'printer_min_cost_hash');
        $this->dropColumn('printer_min_cost_hash',  'size_max2');
        $this->dropColumn('printer_min_cost_hash', 'size_max3');

    }
}
