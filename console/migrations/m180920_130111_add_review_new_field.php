<?php

use yii\db\Migration;

/**
 * Class m180920_130111_add_review_new_field
 */
class m180920_130111_add_review_new_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_review', 'public_to_print', $this->tinyInteger(1)->defaultValue(0)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('store_order_review', 'public_to_print');
    }
}
