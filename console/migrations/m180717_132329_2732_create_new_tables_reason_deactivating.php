<?php

use yii\db\Migration;

/**
 * Class m180717_132329_2732_create_new_tables_reason_deactivating
 */
class m180717_132329_2732_create_new_tables_reason_deactivating extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_deactivating_log', [
            'id' => $this->primaryKey(),
            'reason_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull()->unique(),
            'created_at' => $this->dateTime()->notNull(),
            'reason_title' => $this->string(255),
            'reason_desc' => $this->string(255)
        ]);

        $this->createIndex('index_updl_reason_id', 'user_deactivating_log', 'reason_id');
        $this->createIndex('index_updl_user_id', 'user_deactivating_log', 'user_id');

        $this->addForeignKey('fk_updl_user_id', 'user_deactivating_log', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_deactivating_log');
    }
}
