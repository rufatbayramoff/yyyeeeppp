<?php

use yii\db\Migration;
use yii\db\Schema;

class m170307_161921_3923_view_widget_statistics extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `widget_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('psFacebook','psSite','printerSite','designerSite') NOT NULL,
  `hosting_page` VARCHAR(1024) NOT NULL,
  `ps_id` int(11) DEFAULT NULL,
  `ps_printer_id` int(11) DEFAULT NULL,
  `designer_id` int(11) DEFAULT NULL,
  `views_count` int(11) NOT NULL,
  `first_visit` datetime NOT NULL,
  `last_visit` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ps_id` (`ps_id`),
  KEY `ps_printer_id` (`ps_printer_id`),
  KEY `designer_id` (`designer_id`),
  CONSTRAINT `widget_statistics_designer` FOREIGN KEY (`designer_id`) REFERENCES `user` (`id`),
  CONSTRAINT `widget_statistics_ps_id` FOREIGN KEY (`ps_id`) REFERENCES `ps` (`id`),
  CONSTRAINT `widget_statistics_ps_printer_id` FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function down()
    {
        $this->dropTable('widget_statistics');
    }
}
