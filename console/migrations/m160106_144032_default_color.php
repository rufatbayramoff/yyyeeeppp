<?php

use yii\db\Schema;
use yii\db\Migration;

class m160106_144032_default_color extends Migration
{
    public function up()
    {
        $this->execute("INSERT IGNORE INTO `printer_color` (`title`, `is_active`,`rgb`,`cmyk`,`image`,`render_color`) 
                        VALUES ('White',  1,'255,255,255','','','White');");
        
        $color = app('db')->createCommand('SELECT * FROM printer_color WHERE title=:title')
            ->bindValue('title', 'White')
            ->queryOne(); 
        $colorId  = $color['id'];
        
        $material = app('db')->createCommand('SELECT * FROM printer_material WHERE filament_title=:title')
            ->bindValue('title', 'PLA')
            ->queryOne(); 
        $materialId  = $material['id'];
        
        $this->execute("INSERT IGNORE INTO printer_material_to_color (material_id, color_id, is_active, is_default)
            VALUES ($materialId, $colorId,   '1', '1')");        
        
    }

    public function down()
    {
        
    }
 
}
