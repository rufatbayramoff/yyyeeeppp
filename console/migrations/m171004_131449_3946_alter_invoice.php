<?php

use yii\db\Migration;

class m171004_131449_3946_alter_invoice extends Migration
{
    public function safeUp()
    {
        $this->addColumn('payment_invoice','comment','TEXT NULL');

    }

    public function safeDown()
    {
        $this->dropColumn('payment_invoice', 'comment');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171004_131449_3946_alter_invoice cannot be reverted.\n";

        return false;
    }
    */
}
