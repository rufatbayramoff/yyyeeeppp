<?php

use yii\db\Migration;

/**
 * Class m181109_133321_5814_payment_transaction_refund_item
 */
class m181109_133321_5814_payment_transaction_refund_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_transaction_refund', 'item', 'varchar(25) null after status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('payment_transaction_refund', 'item');
    }
}
