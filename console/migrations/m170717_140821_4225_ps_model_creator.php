<?php

use yii\db\Migration;
use yii\db\Query;

class m170717_140821_4225_ps_model_creator extends Migration
{
    public function up()
    {
        $this->execute(
            "
CREATE TABLE `ps_model_creator` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ps_id` INT(11) NOT NULL,
  `type` ENUM('printer','cnc') NOT NULL DEFAULT 'printer',
  `ps_printer_id` INT(11) NULL,
  `ps_cnc_service_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  KEY `ps_printer_id` (`ps_printer_id`),
  KEY `ps_cnc_service_id` (`ps_cnc_service_id`),
  KEY `ps_id` (`ps_id`),
  CONSTRAINT `ps_model_creator_cnc_id` FOREIGN KEY (`ps_cnc_service_id`) REFERENCES `ps_cnc_service` (`id`),
  CONSTRAINT `ps_model_creator_printer_id` FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer` (`id`),
  CONSTRAINT `ps_model_creator_ps_id` FOREIGN KEY (`ps_id`) REFERENCES `ps` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
        );
        $this->addColumn('ps_printer', 'ps_model_creator_id', 'int(11) NOT NULL AFTER id');
        $this->addColumn('ps_printer_delivery', 'ps_model_creator_id', 'int(11) NOT NULL AFTER id');
        $this->renameTable('ps_printer_delivery', 'ps_model_creator_delivery');
    }

    public function down()
    {
        $this->dropTable('ps_model_creator');
        $this->dropColumn('ps_printer', 'ps_model_creator_id');
        $this->renameTable('ps_model_creator_delivery', 'ps_printer_delivery');
        $this->dropColumn('ps_printer_delivery', 'ps_model_creator_id');

    }
}
