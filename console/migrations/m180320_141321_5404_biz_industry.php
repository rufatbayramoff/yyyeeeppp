<?php

use yii\db\Migration;

/**
 * Class m180320_141321_5404_biz_industry
 */
class m180320_141321_5404_biz_industry extends Migration
{
    public function safeUp()
    {
        $this->update('biz_industry', ['title'=>'Healthcare'], 'id=1');
    }

    public function safeDown()
    {
    }
}
