<?php

use yii\db\Schema;
use yii\db\Migration;

class m150930_111155_ps_printer_order_dates extends Migration
{
    public function safeUp()
    {
        // accepted_date - date, when Accepted button was clicked
        // fact_printed_at - date, when file was printed, set by 'Set as printed' button
        // plan_printed_at - date, set by Printer owner, he set here when he finish print
        // start_print_at - when he started printing ALL ORDER (not one 3d-file)
        // finish_print_at - when he finished printing ALL ORDER (not one 3d-file)
        // expired_at - date, deadline + some hours (set in Admin Panel)
        $this->execute("CREATE TABLE `ps_printer_order_dates` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `ps_printer_id` int(11) NOT NULL,
              `order_id` int(11) NOT NULL,
              `accepted_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
              `fact_printed_at` timestamp NULL DEFAULT NULL,
              `plan_printed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
              `start_print_at` timestamp NULL DEFAULT NULL,
              `finish_print_at` timestamp NULL DEFAULT NULL,
              `expired_at` timestamp NULL DEFAULT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `printer_order_dates_unique` (`ps_printer_id`,`order_id`),
              KEY `printer_dates_printer_id` (`ps_printer_id`),
              KEY `printer_dates_order_id` (`order_id`),
              CONSTRAINT `order_dates_order_fk`
                FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `order_dates_printer_fk`
                FOREIGN KEY (`ps_printer_id`) REFERENCES `ps_printer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE ps_printer_order_dates DROP FOREIGN KEY `order_dates_printer_fk`;');
        $this->execute('ALTER TABLE ps_printer_order_dates DROP FOREIGN KEY `order_dates_order_fk`;');
        $this->execute('ALTER TABLE ps_printer_order_dates DROP KEY `printer_dates_printer_id`;');
        $this->execute('ALTER TABLE ps_printer_order_dates DROP KEY `printer_dates_order_id`;');
        $this->execute('ALTER TABLE ps_printer_order_dates DROP KEY `printer_order_dates_unique`;');
        $this->execute('DROP TABLE ps_printer_order_dates;');
    }
}
