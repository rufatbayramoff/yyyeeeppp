<?php

use yii\db\Migration;

class m160811_164413_1644_drop_model3d_part_filament extends Migration
{
    public function up()
    {
        $this->dropTable('model3d_file_filament');
    }

    public function down()
    {
        echo "m160811_164413_1644_drop_model3d_part_filament cannot be reverted.\n";
        return false;
    }
}
