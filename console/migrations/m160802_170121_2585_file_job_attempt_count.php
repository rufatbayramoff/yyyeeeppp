<?php

use yii\db\Migration;

class m160802_170121_2585_file_job_attempt_count extends Migration
{
    public function up()
    {
        $this->addColumn('file_job', 'attempt_count', 'integer NOT NULL DEFAULT 0');
        
    }

    public function down()
    {
        $this->dropColumn('file_job', 'attempt_count');
        return true;
    }
}
