<?php

use yii\db\Migration;

class m170116_133721_3484_userthingiverse extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `user` 
            CHANGE COLUMN `email` `email` VARCHAR(255) CHARACTER SET \'utf8\' NULL ;
        ');

        $this->execute('CREATE TABLE IF NOT EXISTS `thingiverse_user` (
              `user_id` int(11) NOT NULL,
              `thingiverse_id` int(11) NOT NULL,
              `api_json` json DEFAULT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`user_id`),
              UNIQUE KEY `user_id_UNIQUE` (`user_id`),
              UNIQUE KEY `thingiverse_id_UNIQUE` (`thingiverse_id`),
              CONSTRAINT `fk_thingiverse_user_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        $this->execute('CREATE TABLE `thingiverse_thing` (
              `thing_id` int(11) NOT NULL,
              `model3d_id` int(11) DEFAULT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`thing_id`),
              UNIQUE KEY `thing_id_UNIQUE` (`thing_id`),
              UNIQUE KEY `model3d_id_UNIQUE` (`model3d_id`),
              CONSTRAINT `fk_thingiverse_thing_1` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->insert('user', [
            'username' => 'thingiverse',
            'created_at' => time(),
            'updated_at' => time(),
            'lastlogin_at' => time(),
            'id' => 112,
            'auth_key' => '-',
            'password_hash' => '-',
            'email' => 'thingiverse@treatstock.com'
        ]);
    }

    public function down()
    {
        $this->dropTable('thingiverse_user');
        $this->dropTable('thingiverse_thing');
        $this->delete('user', ['id'=>112]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
