<?php

use yii\db\Schema;
use yii\db\Migration;

class m151110_083544_remove_community_db extends Migration
{
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->truncateTable("community_message");
        $this->dropTable("community_message");        
        $this->truncateTable("community_message_type");
        $this->dropTable("community_message_type");
        
        $this->dropTable("community_contact");
        
        $this->execute("ALTER TABLE `user_admin`  ADD UNIQUE INDEX `username_UNIQUE` (`username` ASC); ");
    }

    public function safeDown()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `community_message_type` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(255) NOT NULL,
            `is_active` tinyint(1) NOT NULL DEFAULT '0',
            `sort` int(5) NOT NULL DEFAULT '1',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
          ");
        $this->execute("CREATE TABLE IF NOT EXISTS `community_message` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `from_user_id` int(11) NOT NULL,
            `to_user_id` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `title` varchar(255) DEFAULT NULL,
            `text` mediumtext NOT NULL,
            `received_at` timestamp NULL DEFAULT NULL,
            `status` char(15) NOT NULL DEFAULT 'new',
            `type_id` int(11) NOT NULL,
            `message_status` smallint(6) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_messages_1_idx` (`from_user_id`),
            KEY `fk_messages_2_idx` (`to_user_id`),
            KEY `fk_community_message_1_idx` (`type_id`),
            CONSTRAINT `fk_community_message_1` FOREIGN KEY (`type_id`) REFERENCES `community_message_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_messages_1` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_messages_2` FOREIGN KEY (`to_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          ");
        
        $this->execute("CREATE TABLE IF NOT EXISTS `community_contact` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `owner_id` int(11) NOT NULL,
            `user_id` int(11) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `group` varchar(45) NOT NULL DEFAULT 'user',
            PRIMARY KEY (`id`),
            UNIQUE KEY `owner_id_UNIQUE` (`owner_id`,`user_id`),
            KEY `fk_community_contact_1_idx` (`owner_id`),
            KEY `fk_community_contact_2_idx` (`user_id`),
            CONSTRAINT `fk_community_contact_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_community_contact_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          ");
        $this->dropIndex('username_UNIQUE', 'user_admin');
    } 
}
