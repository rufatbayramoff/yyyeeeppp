<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180828_164921_5814_payment_invoice_fill
 */
class m180828_164921_5814_payment_invoice_fill extends Migration
{

    /**
     * @return string
     * @throws Exception
     */
    protected function generateInvoiceUuid()
    {

        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_invoice')->where('uuid="' . $operationUuid . '"')->one();
        } while ($checkUuid);
        return $operationUuid;
    }

    protected function getOrderById($orderId)
    {
        return (new Query())->select('*')->from('store_order')->where(['id' => $orderId])->one();
    }

    protected function isEqual($a, $b)
    {
        return ((string)$a) == ((string)(float)$b);
    }

    protected function getAccountOneByType($accounting, $type)
    {
        foreach ($accounting as $accountOne) {
            if ($accountOne['type'] === $type) {
                return $accountOne;
            }
        }
        return null;
    }


    protected function calculateIncomingAmount($order)
    {
        $incomings = (new Query())->select('*')->from('payment_transaction')->where([
            'order_id' => $order['id'],
            'type'     => 'payment',
            'status'   => ['settled', 'refunded', 'authorization_expired']
        ])->all();
        $sum = null;
        foreach ($incomings as $incoming) {
            $sum += $incoming['amount'];
        }
        return $sum;
    }

    protected function calculateRefundAmount($order)
    {
        return 0;
    }


    protected function calculateAndFixTotalAmount(& $accounting, $order, $amountPay, $refund)
    {
        $total = 0;
        foreach ($accounting as $accountOne) {
            if ($accountOne['operation'] == '+') {
                $total += $accountOne['price'];
            }
            if ($accountOne['operation'] == '-') {
                $total -= $accountOne['price'];
            }
        }

        if ($amountPay && ($order['id'] != 24293)) {
            // Pay exists
            if (round($amountPay, 2) != round($total, 2)) {
                $transaction = $this->getAccountOneByType($accounting, 'transaction');
                $discount = $this->getAccountOneByType($accounting, 'discount');
                $addon = $this->getAccountOneByType($accounting, 'addon'); // Now only one addon used
                $a = $total - $transaction['price'];
                if ($transaction && $this->isEqual($a, $amountPay)) {
                    // Order 2531, ...
                    $accounting['fix_transaction'] = [
                        'operation' => '-',
                        'price'     => $transaction['price']
                    ];
                    echo "\nFix transaction order: " . $order['id'];
                } else {
                    die('Order not equal payment, Order: ' . $order['id']. 'total: ' . $total.  ' a: ' . $a . ' b : ' . $amountPay);
                }
            }
        } else {
            // Pay not exists, ignore total
        }
        return $total;
    }

    protected function formInvoiceItems($paymentInvoice, $order)
    {
        $paymentInvoiceItem = [
            'uuid'                 => \common\components\UuidHelper::generateUuid(4),
            'payment_invoice_uuid' => $paymentInvoice['uuid'],
            'pos'                  => 1,
            'title'                => 'Invoice for order: ' . $order['id'],
            'description'          => '',
            'measure'              => 'item',
            'qty'                  => '1',
            'price'                => $paymentInvoice['total_amount']
        ];
        $this->insert('payment_invoice_item', $paymentInvoiceItem);
    }

    protected function relinkPaymentToInvoice($order, $paymentInvoice)
    {
        if ($order['payment_id']) {
            $this->update('payment', ['payment_invoice_uuid' => $paymentInvoice['uuid']], 'id=' . $order['payment_id']);
        }
    }

    protected function manualFixVendorTransactions()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->manualFixVendorTransactions();

        $storePricesQuery = (new Query())->select('*, store_pricer_type.type as  type')->from('store_pricer')->leftJoin('store_pricer_type',
            'store_pricer_type.id=store_pricer.pricer_type')->orderBy('order_id');
        $storePrices = $storePricesQuery->all();

        $lastOrderId = null;
        $accounting = [];
        foreach ($storePrices as $storePrice) {
            if ($storePrice['order_id']) {
                if ($lastOrderId === null) {
                    $accounting = [
                        $storePrice['type'] => [
                            'type'      => $storePrice['type'],
                            'price'     => $storePrice['price'],
                            'currency'  => 'USD',
                            'operation' => $storePrice['operation']
                        ]
                    ];
                    $lastOrderId = $storePrice['order_id'];
                } elseif ($lastOrderId != $storePrice['order_id']) {
                    // Next order id
                    $order = $this->getOrderById($lastOrderId);

                    $incomingAmount = $this->calculateIncomingAmount($order);
                    $refundAmount = $this->calculateRefundAmount($order);

                    $totalAmount = $this->calculateAndFixTotalAmount($accounting, $order, $incomingAmount, $refundAmount);
                    $paymentInvoice = [
                        'uuid'         => $this->generateInvoiceUuid(),
                        'created_at'   => $storePrice['created_at'],
                        'date_expire'  => date('Y-m-d H:i:s', strtotime($storePrice['created_at']) + (60 * 60 * 24 * 30)),
                        'total_amount' => $totalAmount,
                        'currency'     => 'USD',
                        'rate_id'      => $storePrice['rate_id'],
                        'status'       => $order['payment_status'],
                        'user_id'      => $order['user_id'],
                        'order_id'     => $lastOrderId,
                        'details'      => 'Order: ' . $order['id'],
                        'accounting'   => $accounting,
                    ];

                    $this->insert('payment_invoice', $paymentInvoice);
                    $this->formInvoiceItems($paymentInvoice, $order);
                    $this->relinkPaymentToInvoice($order, $paymentInvoice);

                    $lastOrderId = $storePrice['order_id'];
                    $accounting = [
                        $storePrice['type'] =>
                        [
                            'type'      => $storePrice['type'],
                            'price'     => $storePrice['price'],
                            'currency'  => 'USD',
                            'operation' => $storePrice['operation']
                        ]
                    ];
                } else {
                    // Payment
                    $accounting[$storePrice['type']] = [
                        'type'      => $storePrice['type'],
                        'price'     => $storePrice['price'],
                        'currency'  => 'USD',
                        'operation' => $storePrice['operation']
                    ];
                }
            }
        }
        if ($lastOrderId != null) {
            // last order id
            $order = $this->getOrderById($lastOrderId);

            $incomingAmount = $this->calculateIncomingAmount($order);
            $refundAmount = $this->calculateRefundAmount($order);

            $totalAmount = $this->calculateAndFixTotalAmount($accounting, $order, $incomingAmount, $refundAmount);
            $paymentInvoice = [
                'uuid'         => $this->generateInvoiceUuid(),
                'created_at'   => $storePrice['created_at'],
                'date_expire'  => date('Y-m-d H:i:s', strtotime($storePrice['created_at']) + (60 * 60 * 24 * 30)),
                'total_amount' => $totalAmount,
                'currency'     => 'USD',
                'rate_id'      => $storePrice['rate_id'],
                'status'       => $order['payment_status'],
                'user_id'      => $order['user_id'],
                'order_id'     => $lastOrderId,
                'details'      => 'Order: ' . $order['id'],
                'accounting'   => $accounting,
            ];

            $this->insert('payment_invoice', $paymentInvoice);
            $this->formInvoiceItems($paymentInvoice, $order);
            $this->relinkPaymentToInvoice($order, $paymentInvoice);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
