<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180522_115222_5579_model3d_category
 */
class m180522_115222_5579_model3d_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameTable('store_category', 'model3d_category');
        $this->renameTable('store_category_intl', 'model3d_category_intl');
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
