<?php

use yii\db\Migration;

class m170403_102216_3962_transaction_fee extends Migration
{
    public function up()
    {
        $this->insert('store_pricer_type', [
            'type' => 'transaction',
            'description' => 'Transaction Fee',
            'operation' => '+'
        ]);
        $this->execute('ALTER TABLE `file` CHANGE COLUMN `extension` `extension` CHAR(7) NOT NULL ;');
    }

    public function down()
    {
        $this->execute('');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
