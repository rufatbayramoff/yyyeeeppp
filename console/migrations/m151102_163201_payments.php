<?php

use yii\db\Schema;
use yii\db\Migration;

class m151102_163201_payments extends Migration
{
     
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("CREATE TABLE `payment` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `description` varchar(245) NOT NULL,
            `status` char(15) NOT NULL DEFAULT 'new',
            `parent_id` int(11) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_payment_1_idx` (`parent_id`),
            CONSTRAINT `fk_payment_1` FOREIGN KEY (`parent_id`) REFERENCES `payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        ");
        
        $this->execute("CREATE TABLE `payment_detail` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `payment_id` int(11) NOT NULL,
            `user_id` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `vendor` char(15) NOT NULL DEFAULT 'ts',
            `vendor_transaction_id` char(32) DEFAULT NULL,
            `status` char(15) NOT NULL DEFAULT 'new',
            `amount` decimal(9,4) NOT NULL,
            `original_amount` decimal(9,4) NOT NULL,
            `original_currency` char(3) NOT NULL,
            `rate_id` int(11) NOT NULL,
            `description` varchar(245) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_payment_detail_1_idx` (`payment_id`),
            KEY `fk_payment_detail_2_idx` (`user_id`),
            KEY `fk_payment_detail_3_idx` (`rate_id`),
            CONSTRAINT `fk_payment_detail_1` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_payment_detail_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_payment_detail_3` FOREIGN KEY (`rate_id`) REFERENCES `payment_exchange_rate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        ");
        
        $this->execute("ALTER TABLE `store_order` 
            ADD COLUMN `payment_id` INT(11) NULL AFTER `order_state`,
            ADD INDEX `fk_store_order_4_idx` (`payment_id` ASC);
            ALTER TABLE  `store_order` 
            ADD CONSTRAINT `fk_store_order_4`
              FOREIGN KEY (`payment_id`)
              REFERENCES  `payment` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
        
    }

    public function safeDown()
    {
        $this->truncateTable('payment_detail');
        $this->truncateTable('payment');
        
        $this->execute("ALTER TABLE `store_order` 
        DROP FOREIGN KEY `fk_store_order_4`;
        ALTER TABLE `store_order` 
        DROP COLUMN `payment_id`,
        DROP INDEX `fk_store_order_4_idx` ;
        ");
        
        $this->dropTable('payment_detail');
        $this->dropTable('payment');
    } 
}
