<?php

use yii\db\Migration;

/**
 * Class m180420_154341_5347_ps_phone_not_required
 */
class m180420_154341_5347_ps_phone_not_required extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `ps` 
            CHANGE COLUMN `phone_code` `phone_code` VARCHAR(10) NULL ,
            CHANGE COLUMN `phone` `phone` VARCHAR(45) NULL ,
            CHANGE COLUMN `phone_status` `phone_status` ENUM('new', 'checking', 'checked', 'failed') NULL;
        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180420_154341_5347_ps_phone_not_required cannot be reverted.\n";
        return false;
    }
}
