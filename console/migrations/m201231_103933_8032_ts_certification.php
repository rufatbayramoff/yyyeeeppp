<?php

use common\components\DateHelper;
use yii\db\Migration;
use yii\db\Query;

class m201231_103933_8032_ts_certification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $psPrinters = (new Query())->select('*')->from('ps_printer')->where(['test_status'=>['professional']])->all();
        foreach ($psPrinters as $psPrinter) {
            $this->update('company_service', ['certification' => 'verified', 'certification_expire' => DateHelper::addNow('P1Y')], ['ps_printer_id'=>$psPrinter['id']]);
        }
        $this->dropColumn('ps_printer', 'test_status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
