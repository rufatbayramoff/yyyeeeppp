<?php

use yii\db\Migration;

class m160217_082457_merge_print_file_status_and_diax_token extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer_file_status` ADD `token` VARCHAR(45)  NULL  DEFAULT NULL  AFTER `check_status_at`;");
        $this->execute("ALTER TABLE `ps_printer_file_status` ADD `print_status` VARCHAR(20)  NULL  DEFAULT NULL  AFTER `token`;");
        $this->execute("ALTER TABLE `ps_printer_file_status` ADD `progress` SMALLINT  UNSIGNED  NULL  DEFAULT NULL  AFTER `print_status`;");

        $this->execute("
            UPDATE `ps_printer_file_status` p, ps_printer_3diax_token t
            SET p.`print_status` = t.status, p.`token` = t.`token`, p.`progress` = t.`progress`,  p.`check_status_at` = t.`check_status_at`
            WHERE p.id = t.`print_file_status_id`
        ");
        $this->execute("DROP TABLE `ps_printer_3diax_token`;");
    }

    public function down()
    {
        $this->execute("
            CREATE TABLE `ps_printer_3diax_token` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
              `order_id` int(11) NOT NULL,
              `model3d_file_id` int(11) NOT NULL,
              `token` varchar(45) NOT NULL,
              `status` varchar(30) NOT NULL DEFAULT 'new',
              `progress` smallint(5) DEFAULT NULL,
              `details` varchar(100) DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `check_status_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
              `print_file_status_id` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `idx_order_3diax` (`order_id`),
              KEY `idx_model3d_3diax` (`model3d_file_id`),
              KEY `file_status` (`print_file_status_id`),
              CONSTRAINT `file_status` FOREIGN KEY (`print_file_status_id`) REFERENCES `ps_printer_file_status` (`id`),
              CONSTRAINT `fk_3diax_model3d` FOREIGN KEY (`model3d_file_id`) REFERENCES `model3d_file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_3diax_order` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            INSERT INTO `ps_printer_3diax_token`
            SELECT null as id, order_id, model3d_file_id, token, `print_status` as `status`, progress, null as details, created_at, check_status_at, id as `print_file_status_id`
            FROM ps_printer_file_status
        ");

        $this->execute("ALTER TABLE `ps_printer_file_status` DROP `token`;");
        $this->execute("ALTER TABLE `ps_printer_file_status` DROP `print_status`;");
        $this->execute("ALTER TABLE `ps_printer_file_status` DROP `progress`;");

        return true;
    }

}
