<?php

use yii\db\Schema;
use yii\db\Migration;

class m150827_083624_filament extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `model3d_file_filament` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `model3d_file_id` int(11) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `length` double(10,4) NOT NULL,
            `volume` double(10,4) NOT NULL,
            `nozzle_diameter` double(8,4) DEFAULT NULL,
            `filament_diameter` double(8,4) DEFAULT NULL,
            `fill_density` double(8,4) DEFAULT NULL,
            `layer_height` double(8,4) DEFAULT NULL,
            `perimeters` double(8,4) DEFAULT NULL,
            `solid_infill_speed` double(8,4) DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `file_id_UNIQUE` (`model3d_file_id`),
            KEY `fk_model3d_file_filament_1_idx` (`model3d_file_id`),
            CONSTRAINT `fk_model3d_file_filament_1` FOREIGN KEY (`model3d_file_id`) REFERENCES `model3d_file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

        "); 
        
        $this->execute("ALTER TABLE `file_job` CHANGE COLUMN `operation` `operation` CHAR(25) NULL DEFAULT NULL");
        $this->execute("ALTER TABLE `file_job`  ADD INDEX `fk_oper_idx` (`operation` ASC)");
        $this->execute("ALTER TABLE `file_job`  CHANGE COLUMN `operation` `operation` CHAR(25) NOT NULL ;");
    }

    public function down()
    {
        $this->dropTable('model3d_file_filament');
        $this->dropIndex('fk_oper_idx', 'file_job');
        return true;
    }
}
