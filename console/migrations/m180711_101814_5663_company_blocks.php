<?php

use yii\db\Migration;

/**
 * Class m180711_101814_5663_company_blocks
 */
class m180711_101814_5663_company_blocks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            'CREATE TABLE `company_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `content` text,
  `created_at` datetime NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT \'1\',
  `position` tinyint(3) NOT NULL DEFAULT \'1\',
  `company_id` int(11) DEFAULT NULL,
  `videos` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_company_block_1_idx` (`company_id`),
  CONSTRAINT `fk_company_block_1` FOREIGN KEY (`company_id`) REFERENCES `ps` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;'
        );

        $this->execute(
            'CREATE TABLE `company_block_image` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `block_id` int(11) NOT NULL,
              `position` int(11) NOT NULL DEFAULT \'0\',
              `file_uuid` varchar(32) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `file_uuid` (`file_uuid`),
              KEY `index3` (`block_id`),
              CONSTRAINT `fk_company_block_image_1` FOREIGN KEY (`block_id`) REFERENCES `company_block` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
              CONSTRAINT `fk_company_block_image_file_uuid` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
           CREATE TABLE `company_block_bind` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_uuid` varchar(32) DEFAULT NULL,
  `company_service_id` int(11) DEFAULT NULL,
  `block_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index5` (`product_uuid`,`block_id`),
  KEY `fk_company_block_bind_1` (`block_id`),
  KEY `fk_company_block_bind_2_idx` (`company_service_id`),
  KEY `index4` (`product_uuid`),
  CONSTRAINT `fk_company_block_bind_1` FOREIGN KEY (`block_id`) REFERENCES `company_block` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_block_bind_2` FOREIGN KEY (`company_service_id`) REFERENCES `company_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_block_bind_3` FOREIGN KEY (`product_uuid`) REFERENCES `product` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


            '
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('company_block_image');
        $this->dropTable('company_block');
    }
}
