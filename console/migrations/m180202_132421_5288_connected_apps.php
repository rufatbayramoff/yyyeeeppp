<?php

use yii\db\Migration;

class m180202_132421_5288_connected_apps extends Migration
{
    public function safeUp()
    {
        $this->renameTable('promo_app_intl', 'connected_app_intl');
        $this->renameTable('promo_app', 'connected_app');
        $this->addColumn('connected_app', 'logo_file_uuid', 'varchar(32) after logo_file_id');
        $this->createIndex('connected_app_logo_file_indx', 'connected_app', 'logo_file_uuid');
        $this->addForeignKey('connected_app_logo_file_fk', 'connected_app', 'logo_file_uuid', 'file', 'uuid');
        $this->dropForeignKey('fk_apps_logo_file_id', 'connected_app');
        $this->dropColumn('connected_app', 'logo_file_id');
    }

    public function safeDown()
    {
        return true;
    }

}
