<?php

use yii\db\Migration;

class m170905_161221_4760_http_log_user_agent extends Migration
{
    public function safeUp()
    {
        $this->addColumn('http_request_log', 'host', 'varchar(1024) not null default "" after id');
        $this->addColumn('http_request_log', 'user_agent', 'varchar(1024) not null default ""');
        $this->addColumn('http_request_log', 'referrer', 'varchar(1024) not null default ""');
    }

    public function safeDown()
    {
        $this->dropColumn('http_request_log' ,'host');
        $this->dropColumn('http_request_log' ,'user_agent');
        $this->dropColumn('http_request_log' ,'referrer');
    }

}
