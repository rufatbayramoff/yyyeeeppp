<?php

use yii\db\Migration;

class m160629_135731_1644_drop_old_model3d_color_data extends Migration
{

    public function safeUp()
    {
        $this->dropColumn('model3d', 'default_color');
        $this->dropColumn('model3d', 'default_material');
    }

    public function safeDown()
    {
        return false;
    }
}
