<?php

use yii\db\Schema;
use yii\db\Migration;

class m151028_142234_taxcountry extends Migration
{
     
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `tax_country` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `country` char(2) NOT NULL,
            `rate_ps` tinyint(4) NOT NULL DEFAULT '0',
            `rate_model` tinyint(4) NOT NULL DEFAULT '0',
            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            UNIQUE KEY `country_UNIQUE` (`country`),
            CONSTRAINT `fk_tax_country_1` FOREIGN KEY (`country`) REFERENCES `geo_country` (`iso_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        ");
        $this->execute("insert into tax_country (country, rate_ps, rate_model)  select iso_code, 30, 30 from geo_country");
        $this->update('tax_country', ['rate_ps'=>0, 'rate_model'=>0], ['country'=>'US']);
    }

    public function safeDown()
    {
        $this->truncateTable('tax_country');
        $this->dropTable('tax_country');
    } 
}
