<?php

use yii\db\Migration;

class m160608_141216_usertax_dob extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_tax_info` ADD COLUMN `dob_date` DATE NULL AFTER `rsa_tax_rate`;");
    }

    public function down()
    {
        $this->dropColumn('user_tax_info', 'dob_date');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
