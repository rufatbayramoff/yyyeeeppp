<?php

use yii\db\Migration;

/**
 */
class m211005_161440_8770_country_eta_domestic extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `geo_country` ADD `eta_domestic` INT(11) NOT NULL DEFAULT '10' AFTER `is_bank_transfer_payout`;");

        $this->update('geo_country', ['eta_domestic' => 5], ['iso_code' => ['AU', 'BE', 'KR']]);
        $this->update('geo_country', ['eta_domestic' => 6], ['iso_code' => 'NL']);
        $this->update('geo_country', ['eta_domestic' => 7], ['iso_code' => 'NZ']);
        $this->update('geo_country', ['eta_domestic' => 8], ['iso_code' => 'IT']);
        $this->update('geo_country', ['eta_domestic' => 9], ['iso_code' => ['AT', 'CH', 'DE', 'DK', 'ES', 'GB']]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
