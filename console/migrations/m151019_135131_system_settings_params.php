<?php

use yii\db\Schema;
use yii\db\Migration;

class m151019_135131_system_settings_params extends Migration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO `system_setting`
            (`group_id`,`key`,`value`,`created_at`,`updated_at`,`created_user_id`,`updated_user_id`,`description`,`json`,`is_active`)
            VALUES (9,'printed_wait_time','120','2015-10-19 13:11:13','2015-10-19 13:11:13',2,NULL,'Wait time (in minutes) after push Printed button and before send letter','',1);");
        $this->execute("INSERT INTO `system_setting`
            (`group_id`,`key`,`value`,`created_at`,`updated_at`,`created_user_id`,`updated_user_id`,`description`,`json`,`is_active`)
            VALUES (9,'printed_wait_after_message','1440','2015-10-19 13:14:04','2015-10-19 13:14:04',2,NULL,'Wait time (in minutes) after send letter and before notify moderator','',1);");

        $this->execute("ALTER TABLE `store_order_delivery`
            ADD COLUMN `printed_at` TIMESTAMP NULL DEFAULT NULL AFTER `created_at`,
            ADD COLUMN `send_message_at` TIMESTAMP NULL DEFAULT NULL AFTER `printed_at`;");

        $this->execute("INSERT INTO `email_template` (`code`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`)
            VALUES ('deliveryWait','en-US','Wait for delivery order','this email sent after some hours after clicked Printed button',NULL,'<p>Hello %username%,</p>\r\n<p>\r\nWe are waiting for you, when you will send printed order!\r\n</p>\r\n<p>\r\nThank you,\r\nTS Team</p>','Hello %username%,\r\n\r\nWe are waiting for you, when you will send printed order!\r\n\r\nThank you,\r\nTS Team');");

        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('store_order_cancel','No filament','',1);");
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM system_setting WHERE group_id = 9 AND `key` = 'printed_wait_time';");
        $this->execute("DELETE FROM system_setting WHERE group_id = 9 AND `key` = 'printed_wait_after_message';");

        $this->dropColumn('store_order_delivery', 'printed_at');
        $this->dropColumn('store_order_delivery', 'send_message_at');

        $this->execute("DELETE FROM email_template WHERE `code` = 'deliveryWait' LIMIT 1;");

        $this->execute("DELETE FROM system_reject WHERE `group` = 'store_order_cancel' LIMIT 1;");
    }
}
