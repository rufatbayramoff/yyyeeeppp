<?php

use yii\db\Migration;

class m161026_113721_3187_create_printable_pack extends Migration
{
    public function safeUp()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `api_external_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `private_key` varchar(128) NOT NULL,
  `public_upload_key` varchar(128) NULL,
  `binded_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`private_key`),
  UNIQUE KEY `public_upload_key` (`public_upload_key`),
  KEY `binded_user_id` (`binded_user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`binded_user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
SQL
        );
        $this->execute(
            <<<SQL
CREATE TABLE `api_printable_pack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model3d_id` int(11) NOT NULL,
  `api_external_system_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `affiliate_price` decimal(10,4) NOT NULL,
  `affiliate_currency_id` int(11) NOT NULL,
  `public_token` varchar(512) NOT NULL,
  `status` enum('expired','active') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `model3d_id` (`model3d_id`),
  KEY `affiliate_currency_id` (`affiliate_currency_id`),
  KEY `api_external_system_id` (`api_external_system_id`),
  CONSTRAINT `fk_affiliate_currency_id` FOREIGN KEY (`affiliate_currency_id`) REFERENCES `payment_currency` (`id`),
  CONSTRAINT `fk_model3d_id` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function safeDown()
    {
        $this->dropTable('api_printable_pack');
        $this->dropTable('api_external_system');
        return true;
    }
}
