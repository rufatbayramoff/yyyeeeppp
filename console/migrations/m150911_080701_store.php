<?php

use yii\db\Schema;
use yii\db\Migration;

class m150911_080701_store extends Migration
{
    public function safeUp()
    {        
        $this->addColumn('store_order', 'printer_id', 'INT(11) NOT NULL');
        $this->addForeignKey('fk_store_order3', 'store_order', 'printer_id', 'ps_printer', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_store_order3', 'store_order');
        $this->dropColumn('store_order', 'printer_id');
        return true;
    }
}
