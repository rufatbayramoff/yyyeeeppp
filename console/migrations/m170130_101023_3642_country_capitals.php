<?php

use yii\db\Migration;
use yii\db\Query;

class m170130_101023_3642_country_capitals extends Migration
{
    const CAPITALS_LIST_FILE_NAME = __DIR__ . '/data/capitals.csv';

    public function getCapitalsList()
    {
        $capitals = [];
        if (($handle = fopen(self::CAPITALS_LIST_FILE_NAME, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000)) !== false) {
                $capital = [
                    'geoNameId' => $data[0],
                    'city'      => $data[1],
                    'country'   => $data[2],
                    'lat'       => $data[3],
                    'lon'       => $data[4]
                ];
                $capitals[$capital['country']] = $capital;
            }
            fclose($handle);
        }
        return $capitals;
    }


    public function findCapital($countryCode, $capitals)
    {
        foreach ($capitals as $capital) {
            if ($countryCode === $capital['country']) {
                return $capital;
            }
        }
        return null;
    }

    public function findCity($capital, $cities)
    {
        foreach ($cities as $city) {
            if ($city['geoname_id'] == $capital['geoNameId']) {
                return $city;
            }
        }
        return null;
    }

    public function formCity($capital, $country)
    {
        $city = [
            'title'       => $capital['city'],
            'country_id'  => $country['id'],
            'region_id'   => null,
            'timezone_id' => null,
            'lat'         => $capital['lat'],
            'lon'         => $capital['lon'],
            'fcode'       => 'capital',
            'geoname_id'  => $capital['geoNameId']
        ];
        $this->insert('geo_city', $city);
        $city['id'] = $this->db->getLastInsertID();
        return $city;
    }

    public function saveCountryCapital($country, $city)
    {
        $this->update('geo_country', ['capital_id' => $city['id']], 'id=' . $country['id']);
    }

    public function processCountryCapital($country, $capitals, $cities)
    {
        if (!array_key_exists($country['iso_code'], $capitals)) {
            return false;
        }
        $capital = $capitals[$country['iso_code']];
        $city = $this->findCity($capital, $cities);
        if (!$city) {
            $city = $this->formCity($capital, $country);
        }
        $this->saveCountryCapital($country, $city);
    }

    public function safeUp()
    {
        $capitals = $this->getCapitalsList();
        $currentCountries = (new Query())->select('*')->from('geo_country')->all();
        $cities = (new Query())->select('*')->from('geo_city')->where('geoname_id > 1')->all();
        foreach ($currentCountries as $country) {
            $this->processCountryCapital($country, $capitals, $cities);
        }
    }

    public function down()
    {

    }
}
