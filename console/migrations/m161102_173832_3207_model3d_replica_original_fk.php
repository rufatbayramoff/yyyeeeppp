<?php

use yii\db\Schema;
use yii\db\Migration;

class m161102_173832_3207_model3d_replica_original_fk extends Migration
{
    public function safeUp()
    {
        $this->execute(
            <<<SQL
ALTER TABLE `model3d_replica_part` 
DROP FOREIGN KEY `fk_model3d_part_original`;
ALTER TABLE `model3d_replica_part` 
ADD CONSTRAINT `fk_model3d_part_original`
  FOREIGN KEY (`original_model3d_part_id`)
  REFERENCES `model3d_part` (`id`)
  ON DELETE SET NULL
  ON UPDATE SET NULL;
SQL
        );
        $this->execute(
            <<<SQL
ALTER TABLE `model3d_replica_img` 
DROP FOREIGN KEY `model3d_replica_img_original_img`;
ALTER TABLE `model3d_replica_img` 
ADD CONSTRAINT `model3d_replica_img_original_img`
  FOREIGN KEY (`original_model3d_img_id`)
  REFERENCES `model3d_img` (`id`)
  ON DELETE SET NULL
  ON UPDATE SET NULL;
SQL
        );
    }

    public function safeDown()
    {
    }
}


