<?php

use yii\db\Migration;

class m170727_111555_4222_cnc_params extends Migration
{
    public function up()
    {

        $this->execute("
        
        CREATE TABLE `model3d_part_cnc_params` (
          `model3d_part_id` INT NOT NULL,
          `filepath` VARCHAR(255) NULL,
          `finefile` VARCHAR(255) NULL,
          `roughfile` VARCHAR(255) NULL,
          `analyze` TEXT NULL,
          PRIMARY KEY (`model3d_part_id`),
          CONSTRAINT `fk_cnc_params`
            FOREIGN KEY (`model3d_part_id`)
            REFERENCES `model3d_part` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);
        ");
    }

    public function down()
    {
        $this->dropTable('model3d_part_cnc_params');
        return true;
    }
}
