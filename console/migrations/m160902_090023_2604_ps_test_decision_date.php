<?php

use yii\db\Migration;

class m160902_090023_2604_ps_test_decision_date extends Migration
{
    public function up()
    {
        $this->addColumn('ps_printer_test', 'decision_at', 'datetime NULL');
    }

    public function down()
    {
        $this->dropColumn('ps_printer_test', 'decision_at');
        return true;
    }
}
