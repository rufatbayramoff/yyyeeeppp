<?php

use common\models\UserEmailSent;
use yii\db\Migration;

class m170523_155722_4316_email_to_db extends Migration
{
    public function up()
    {
        foreach($this->getEmails() as $code => $emailTpl){
            $this->insert('email_template', [
                'code' => $code,
                'group' => 'notify',
                'language_id' => 'en-US',
                'title' => $emailTpl['title'],
                'description' => 'Period ' . $emailTpl['period'] . ' params: ' . implode(',',$emailTpl['params']),
                'template_text' => $emailTpl['body'],
                'updated_at' => dbexpr('NOW()'),
                'is_active' => true
            ]);
        }

    }

    public function down()
    {
        foreach($this->getEmails() as $code => $emailTpl){
            $this->delete('email_template', [
                'code' => $code,
            ]);
        }
    }


    private function getEmails(){
        if(!class_exists('\common\models\base\UserEmailSent')){
           // return [];
        }
        $emails = [
            'author_publish_model' => [
                'description' => 'After user upload model (not published) with suggestion to publish it.',
                'period'      => '1 day',
                'params'      => ['name', 'modelLink'],
                'title'       => 'You have an unpublished model on Treatstock',
                'body'        =>
                    'Hi %name%,

You recently uploaded model %modelLink% which remains unpublished. 
If you like, you can publish the model so that you can sell it in our catalog of 3D printable models and earn money each time it is printed. 
If you need help with publishing your model, please contact support at support@treatstock.com and we will be more than happy to help.

Best Regards, 
Treatstock'
            ],
            'request_test_order'   => [
                'description' => ' After PS create printer with suggestion to Request Test Order',
                'period'      => '1 day',
                'params'      => ['name', 'psLink'],
                'title'       => 'Request test order',
                'body'        => 'Hi %name%,
Thank you for joining Treatstock as a print service!

To have your print service published, we require that you complete at least one test order on one of your 3D printers. 
Plus, it\'s a great way to familiarize yourself with how the orders process works and at the end, 
you will also receive a positive review which can help kick start real orders. 

Click \'Request test order\' in your print service page here to get started: %psLink%

Best Regards, 
Treatstock'
            ],

            'user_profile' => [
                'description' => ' After designer publish first model suggestion to fill his profile: photo, skills',
                'period'      => '1 day',
                'params'      => ['name', 'userLink'],
                'title'       => 'Please complete your profile',
                'body'        => 'Hi %name%,

Congratulations on uploading your first model on Treatstock!

If you haven\'t already, we would like to suggest that you upload a profile picture and fill in the \'About Me\' 
and \'My skills and professional achievements\' sections in your profile. 
They are like your marketing tools that can help you to reach out to your customers and convince them to print your models or select you when they want to hire a designer for their ideas.

You can visit your profile page here %userLink%.

Best Regards, 
Treatstock'
            ],

            'best_model' => [
                'description' => '4. After designer publish first model information about Model of Month competition',
                'period'      => '1 week',
                'params'      => ['name', 'uploadLink'],
                'title'       => 'Best Model of the Month competition',
                'body'        => 'Hi %name%,

Did you know that there is a monthly competition for designers on Treatstock? Each month, a 3D model is crowned the "Best 3D Model of the Month”, with the winner determined by receiving the most number of likes in the month it was published.

The prize for winning the competition is your winning model will have access to prime real estate and be displayed on our homepage for all to see. The more models you upload, the better your chances of winning!

You can upload more models here %uploadLink%

Best Regards, 
Treatstock'
            ],
            'user_pay_order' => [
                'title'  => 'Order Requires Payment',
                'period' => '3 hours',
                'params' => ['name', 'payLink'],
                'body'   => 'Hi %name%,

You recently attempted to create an order but didn\'t complete the checkout process. The order will no longer be available once 24 hours has elapsed from the time you attempted to create the order. 
To return to your order and make payment so printing can begin, please click on the following link:  %payLink%

Best Regards, 
Treatstock'
            ]

        ];
        return $emails;
    }
}
