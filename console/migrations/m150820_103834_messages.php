<?php

use yii\db\Schema;
use yii\db\Migration;

class m150820_103834_messages extends Migration
{
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->execute("CREATE TABLE `community_message_type` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(255) NOT NULL,
            `is_active` tinyint(1) NOT NULL DEFAULT '0',
            `sort` int(5) NOT NULL DEFAULT '1',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $this->execute("ALTER TABLE `community_message` DROP COLUMN `type`");
        $this->execute("ALTER TABLE `community_message` 
                    CHANGE COLUMN `status` `status` CHAR(15) NOT NULL DEFAULT 'new' ,
                    ADD COLUMN `type_id` INT(11) NOT NULL AFTER `status`");
        
        $this->execute("ALTER TABLE `community_message` 
            ADD INDEX `fk_community_message_1_idx` (`type_id` ASC);
            ALTER TABLE `community_message` 
            ADD CONSTRAINT `fk_community_message_1`
              FOREIGN KEY (`type_id`)
              REFERENCES `community_message_type` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;");
        
        $this->execute("CREATE TABLE `community_contact` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `owner_id` int(11) NOT NULL,
            `user_id` int(11) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `group` varchar(45) NOT NULL DEFAULT 'user',
            PRIMARY KEY (`id`),
            KEY `fk_community_contact_1_idx` (`owner_id`),
            KEY `fk_community_contact_2_idx` (`user_id`),
            CONSTRAINT `fk_community_contact_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_community_contact_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        
        $this->execute("ALTER TABLE `community_contact`  ADD UNIQUE INDEX `owner_id_UNIQUE` (`owner_id` ASC, `user_id` ASC);");
        
        $this->execute("INSERT INTO `community_message_type` (`id`, `title`, `is_active`, `sort`) VALUES
            (1, 'Personal Messages', 1, 1),
            (2, 'Support Service', 1, 2),
            (3, 'Print Jobs', 1, 3),
            (4, 'Your orders', 1, 4),
            (5, 'Archive', 1, 6),
            (6, 'Printed Jobs', 1, 10),
            (7, 'Reviews', 1, 12);");
        $this->execute("INSERT INTO `user`  
            (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `lastlogin_at`, `trustlevel`)
            VALUES (0, 'bot', '-', '-', NULL, '', 0, 0, 0, 0, 'normal')");
        $this->execute("UPDATE `user` SET `id`='0' WHERE `username`='bot'");
        
        
        return 0;
        
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->execute("ALTER TABLE `community_message` 
                    CHANGE COLUMN `status` `status` CHAR(15)  NULL DEFAULT 'new' ,
                    ADD COLUMN `type` INT(11) NOT NULL AFTER `status`");        
        $this->dropForeignKey('fk_community_message_1', 'community_message');
        $this->dropIndex('fk_community_message_1_idx', 'community_message');
        $this->dropColumn('community_message', 'type_id');
        $this->truncateTable('community_message_type');
        $this->dropTable('community_message_type');
        $this->dropTable('community_contact');
        $this->truncateTable('community_message');
        $this->delete('user', ['id'=>0]);
        return 0;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
