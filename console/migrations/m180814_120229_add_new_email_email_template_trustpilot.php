<?php

use yii\db\Migration;

/**
 * Class m180814_120229_add_new_email_email_template_trustpilot
 */
class m180814_120229_add_new_email_email_template_trustpilot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(
            'email_template',
            [
                'code'          => 'clientReceivedOrderTrustpilot',
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => 'Order #%orderId% has been received',
                'description'   => 'Params: orderId, clientName',
                'updated_at'    => dbexpr('NOW()'),
                'template_html' => "Hi %clientName%, \n\nThank you for your recent purchase on Treatstock! If you haven't already, you may leave a 5-star rating and review for the manufacturer and recommend their services to others. \n<a href=\"https://treatstock.com/my/orders\" style=\"display: inline-block;margin:10px 0;border-radius: 0px;font-family: sans-serif;padding: 5px 20px;min-width: 40px;font-size: 16px;font-weight: normal;line-height: 20px;border: none;text-decoration:none !important;color: #fff;background-color:#2d8ee0;\">Leave review</a>\n\nTo improve the satisfaction of our customers, we also use the online review community <a href=\"https://trustpilot.com/evaluate/treatstock.com\">Trustpilot</a> and would appreciate your feedback there too. Thank you! \n\nBest,\nTreatstock",
                'template_text' => ""
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'clientReceivedOrderTrustpilot', 'language_id' => 'en-US']);
    }
}
