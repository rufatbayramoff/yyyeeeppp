<?php

use yii\db\Migration;

class m161202_132058_2730_ps_printer_file_status extends Migration
{
    public function up()
    {
        $this->execute("
        
        ALTER TABLE `ps_printer_file_status` 
        ADD COLUMN `order_attemp_id` INT UNSIGNED NOT NULL AFTER `id`;
        
        UPDATE ps_printer_file_status f, store_order_attemp a 
        SET f.order_attemp_id = a.id
        WHERE f.order_id = a.order_id;
        
        
        ALTER TABLE `ps_printer_file_status` 
        DROP FOREIGN KEY `fk_3diax_order_2`;
        ALTER TABLE `ps_printer_file_status` 
        DROP COLUMN `order_id`,
        DROP INDEX `idx_order_3diax_2` ;
        
        
        ALTER TABLE `ps_printer_file_status` 
        ADD INDEX `fk_ps_printer_file_status_1_idx` (`order_attemp_id` ASC);
        
        ALTER TABLE `ps_printer_file_status` 
        ADD CONSTRAINT `fk_ps_printer_file_status_1`
          FOREIGN KEY (`order_attemp_id`)
          REFERENCES `store_order_attemp` (`id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION;

        
        ");

    }

    public function down()
    {
        echo "m161202_132058_2730_ps_printer_file_status cannot be reverted.\n";
        return false;
    }
}
