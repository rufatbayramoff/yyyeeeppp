<?php

use yii\db\Migration;
use yii\db\Query;

class m180202_174721_5184_email_templates extends Migration
{
    public function safeUp()
    {
        $templates = (new Query())
            ->select('*')
            ->from('email_template')
            ->where("`template_html` LIKE '%my/printservice%'")
            ->all();

        foreach ($templates as $template) {
            $newTemplateHtml = str_replace($template['template_html'], 'my/printservice', '/mybusiness/company');
            $this->update('email_template', ['template_html' => $newTemplateHtml], 'id=' . $template['id']);
        }
    }

    public function safeDown()
    {
    }
}
