<?php

use yii\db\Schema;
use yii\db\Migration;

class m151012_143417_smsinit extends Migration
{
   
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("CREATE TABLE `user_sms` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `user_id` int(11) NOT NULL,
        `created_at` timestamp NULL DEFAULT NULL,
        `phone` varchar(45) NOT NULL,
        `message` varchar(155) NOT NULL,
        `status` char(15) NOT NULL DEFAULT 'new',
        `type` char(15) NOT NULL DEFAULT 'confirm_phone',
        `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `response` varchar(145) DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_user_sms_1_idx` (`user_id`),
        CONSTRAINT `fk_user_sms_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;");
    }

    public function safeDown()
    {
        $this->truncateTable('user_sms');
        $this->dropTable('user_sms');
    }
     
}
