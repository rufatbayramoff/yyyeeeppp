<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200522_151521_7311_cutting_model
 */
class m200522_151521_7311_cutting_model extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(<<<SQL
CREATE TABLE `cutting_pack` (
  `uuid` varchar(32) NOT NULL,
  `title` varchar(45),
  `user_session_id` int(11) NOT NULL,
  `user_id` int(11),
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `source` char(15) NOT NULL,
  `source_details` json NOT NULL,
  `is_same_material_for_all_parts` tinyint(1) NOT NULL,
  `material_id` int(11) NOT NULL,
  `thickness` decimal(10,2) NOT NULL,
  `color_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `color_id` (`color_id`),
  KEY `color_id_2` (`color_id`),
  KEY `material_id` (`material_id`),
  CONSTRAINT `cutting_pack_color_id` FOREIGN KEY (`color_id`) REFERENCES `printer_color` (`id`),
  CONSTRAINT `cutting_pack_material_id` FOREIGN KEY (`material_id`) REFERENCES `cutting_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(<<<SQL
CREATE TABLE `cutting_pack_file` (
  `uuid` varchar(32) NOT NULL,
  `cutting_pack_uuid` varchar(32) NOT NULL,
  `type` enum('image','model','info','archive') NOT NULL,
  `file_uuid` varchar(32) NOT NULL,
  `svg_file_uuid` varchar(32) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `selections` json DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `file_uuid` (`file_uuid`) USING BTREE,
  UNIQUE KEY `svg_file_uuid` (`svg_file_uuid`),
  KEY `cutting_pack_uuid` (`cutting_pack_uuid`),
  CONSTRAINT `fk_cutting_pack_file_file_uuid` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`),
  CONSTRAINT `fk_cutting_pack_file_pack_uuid` FOREIGN KEY (`cutting_pack_uuid`) REFERENCES `cutting_pack` (`uuid`),
  CONSTRAINT `fk_cutting_pack_file_svg_file_uuid` FOREIGN KEY (`svg_file_uuid`) REFERENCES `file` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(<<<SQL
CREATE TABLE `cutting_pack_part` (
  `uuid` varchar(32) NOT NULL,
  `cutting_pack_file_uuid` varchar(32) NOT NULL,
  `qty` int(11) NOT NULL,
  `image` longblob,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `cutting_length` int(11) NOT NULL,
  `engraving_length` int(11) NOT NULL,
  `material_id` int(11) DEFAULT NULL,
  `thickness` decimal(10,2) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `color_id` (`color_id`),
  KEY `material_id` (`material_id`),
  KEY `cutting_pack_file_uuid` (`cutting_pack_file_uuid`) USING BTREE,
  CONSTRAINT `fk_cutting_pack_file_id` FOREIGN KEY (`cutting_pack_file_uuid`) REFERENCES `cutting_pack_file` (`uuid`),
  CONSTRAINT `fk_cutting_pack_part_color_id` FOREIGN KEY (`color_id`) REFERENCES `printer_color` (`id`),
  CONSTRAINT `fk_cutting_pack_part_material_id` FOREIGN KEY (`material_id`) REFERENCES `cutting_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(<<<SQL
ALTER TABLE `wiki_material` ADD `type_of_manufacturer` ENUM('3dprinting','laser_cutting') NOT NULL DEFAULT '3dprinting' AFTER `widget_disadvantages`;
SQL
        );
        $this->execute(<<<SQL
ALTER TABLE `store_order_item` ADD `cutting_pack_uuid` VARCHAR(32)  AFTER `product_uuid`, ADD INDEX (`cutting_pack_uuid`);
SQL
        );
        $this->execute(<<<SQL
ALTER TABLE `store_order_item` ADD CONSTRAINT `fk_store_order_cutting_pack_uuid` FOREIGN KEY (`cutting_pack_uuid`) REFERENCES `cutting_pack`(`uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT;
SQL
        );

    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}