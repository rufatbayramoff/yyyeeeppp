<?php

use yii\db\Migration;

class m171121_164721_4964_wiki_material extends Migration
{
    public function safeUp()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `biz_industry` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `photo_url` VARCHAR(4000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `biz_industry_intl` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `lang_iso` CHAR(7) NOT NULL,
  `is_active` BIT(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`),
  KEY `lang_iso` (`lang_iso`),
  UNIQUE KEY `unque_link` (`lang_iso`,`model_id`),
  CONSTRAINT `fk_biz_industry_inl_lang_iso` FOREIGN KEY (`lang_iso`) REFERENCES `system_lang` (`iso_code`),
  CONSTRAINT `fk_biz_industry_inl_model_id` FOREIGN KEY (`model_id`) REFERENCES `biz_industry` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `biz_manufacture_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` CHAR(15) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `about` TEXT NOT NULL,
  `description` TEXT NOT NULL,
  `is_active` BIT(1) NOT NULL DEFAULT b'1',
  UNIQUE KEY `unque_cod` (`code`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_intl` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `lang_iso` CHAR(7) NOT NULL,
  `is_active` BIT(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`),
  KEY `lang_iso` (`lang_iso`),
  UNIQUE KEY `unque_link` (`lang_iso`,`model_id`),
  CONSTRAINT `fk_wiki_materials_model_id` FOREIGN KEY (`model_id`) REFERENCES `wiki_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_feature` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_feature_intl` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `lang_iso` CHAR(7) NOT NULL,
  `is_active` BIT(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`),
  KEY `lang_iso` (`lang_iso`),
  UNIQUE KEY `unque_link` (`lang_iso`,`model_id`),
  CONSTRAINT `fk_wiki_material_feature_intl_lang_iso` FOREIGN KEY (`lang_iso`) REFERENCES `system_lang` (`iso_code`),
  CONSTRAINT `fk_wiki_material_feature_intl_model_id` FOREIGN KEY (`model_id`) REFERENCES `wiki_material_feature` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material2feature` (
  `wiki_material_id` INT(11) NOT NULL,
  `wiki_material_feature_id` INT(11) NOT NULL,
  KEY `wiki_material_id` (`wiki_material_id`),
  KEY `wiki_material_feature_id` (`wiki_material_feature_id`),
  CONSTRAINT `fk_wiki_material2feature_feature_id` FOREIGN KEY (`wiki_material_feature_id`) REFERENCES `wiki_material_feature` (`id`),
  CONSTRAINT `fk_wiki_material2feature_id` FOREIGN KEY (`wiki_material_id`) REFERENCES `wiki_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_manufacture_type` (
  `wiki_material_id` INT(11) NOT NULL,
  `manufacture_type_id` INT(11) NOT NULL,
  UNIQUE KEY `unque_link` (`wiki_material_id`,`manufacture_type_id`),
  KEY `wiki_material_id` (`wiki_material_id`),
  KEY `manufacture_type_id` (`manufacture_type_id`),
  CONSTRAINT `wiki_material_manufacture_type_ibfk_1` FOREIGN KEY (`wiki_material_id`) REFERENCES `wiki_material` (`id`),
  CONSTRAINT `wiki_material_manufacture_type_ibfk_2` FOREIGN KEY (`manufacture_type_id`) REFERENCES `biz_manufacture_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_printer_material` (
  `wiki_material_id` INT(11) NOT NULL,
  `printer_material_id` INT(11) NOT NULL,
  UNIQUE KEY `unque_link` (`wiki_material_id`,`printer_material_id`),
  KEY `wiki_material_id` (`wiki_material_id`),
  KEY `printer_material_id` (`printer_material_id`),
  CONSTRAINT `fk_wiki_materials_printer_material_print_id` FOREIGN KEY (`printer_material_id`) REFERENCES `printer_material` (`id`),
  CONSTRAINT `fk_wiki_materials_printer_material_wiki_id` FOREIGN KEY (`wiki_material_id`) REFERENCES `wiki_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wiki_material_id` int(11) NOT NULL,
  `code` CHAR(5) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unque_cod` (`code`),
  KEY `wiki_material_id` (`wiki_material_id`),
  CONSTRAINT `wiki_material_properties_model_id` FOREIGN KEY (`wiki_material_id`) REFERENCES `wiki_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_properties_intl` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  `comment` VARCHAR(255) NOT NULL,
  `lang_iso` CHAR(7) NOT NULL,
  `is_active` BIT(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`),
  KEY `lang_iso` (`lang_iso`),
  UNIQUE KEY `unque_link` (`lang_iso`,`model_id`),
  CONSTRAINT `fk_wiki_material_properties_intl_lang_iso` FOREIGN KEY (`lang_iso`) REFERENCES `system_lang` (`iso_code`),
  CONSTRAINT `fk_wiki_material_properties_intl_model_id` FOREIGN KEY (`model_id`) REFERENCES `wiki_material_properties` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wiki_material_id` int(11) NOT NULL,
  `file_uuid` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wiki_material_id` (`wiki_material_id`),
  KEY `file_uuid` (`file_uuid`),
  CONSTRAINT `fk_wiki_material_files` FOREIGN KEY (`wiki_material_id`) REFERENCES `wiki_material` (`id`),
  CONSTRAINT `fk_wiki_material_files_file_id` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wiki_material_id` int(11) NOT NULL,
  `file_uuid` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wiki_material_id` (`wiki_material_id`),
  KEY `wiki_material_id_2` (`wiki_material_id`),
  KEY `file_uuid` (`file_uuid`),
  CONSTRAINT `fk_wiki_material_file_uuid` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`),
  CONSTRAINT `fk_wiki_material_photo` FOREIGN KEY (`wiki_material_id`) REFERENCES `wiki_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(
            <<<SQL
CREATE TABLE `wiki_material_photo_intl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `lang_iso` char(7) NOT NULL,
  `is_active` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unque_link` (`lang_iso`,`model_id`),
  KEY `model_id` (`model_id`),
  KEY `lang_iso` (`lang_iso`),
  CONSTRAINT `fk_wiki_material_photo_intl_lang_iso` FOREIGN KEY (`lang_iso`) REFERENCES `system_lang` (`iso_code`),
  CONSTRAINT `fk_wiki_material_photo_intl_model_id` FOREIGN KEY (`model_id`) REFERENCES `wiki_material_photo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );


    }

    public function safeDown()
    {
        $this->dropTable('biz_industry_intl');
        $this->dropTable('biz_industry');
        $this->dropTable('wiki_material2feature');
        $this->dropTable('wiki_material_feature_intl');
        $this->dropTable('wiki_material_feature');
        $this->dropTable('wiki_material_intl');
        $this->dropTable('wiki_material_manufacture_type');
        $this->dropTable('biz_manufacture_type');
        $this->dropTable('wiki_material_printer_material');
        $this->dropTable('wiki_material_properties_intl');
        $this->dropTable('wiki_material_properties');
        $this->dropTable('wiki_material_file');
        $this->dropTable('wiki_material_photo_intl');
        $this->dropTable('wiki_material_photo');
        $this->dropTable('wiki_material');
    }
}
