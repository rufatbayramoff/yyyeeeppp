<?php

use yii\db\Migration;

class m170915_133553_4796_redir_table extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `seo_redir` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `from_url` varchar(245) NOT NULL,
          `to_url` varchar(245) NOT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `user_admin_id` int(11) NOT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `from_url_UNIQUE` (`from_url`)
        )');
    }

    public function safeDown()
    {
        $this->dropTable('seo_redir');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170915_133553_4796_redir_table cannot be reverted.\n";

        return false;
    }
    */
}
