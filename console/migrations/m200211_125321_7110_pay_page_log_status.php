<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200211_125321_7110_pay_page_log_status
 */
class m200211_125321_7110_pay_page_log_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(<<<SQL
CREATE TABLE `payment_pay_page_log_process` (
  `uuid` varchar(32) NOT NULL,
  `process_err` json NOT NULL,
  `process_payload` json NOT NULL,
  `process_message` varchar(1024),
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $this->execute(<<<SQL
CREATE TABLE `payment_pay_page_log_status` (
  `uuid` varchar(32) NOT NULL,
  `status_date` datetime NOT NULL,
  `status` enum('press_button','processed','done') NOT NULL,
  KEY `uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );

        $logs = (new Query())->select('*')->from('payment_pay_page_log')->all();
        foreach ($logs as $log) {
            $this->insert('payment_pay_page_log_status', ['uuid' => $log['uuid'], 'status' => $log['status'], 'status_date' => $log['created_at']]);
            $this->insert('payment_pay_page_log_process', [
                'uuid'            => $log['uuid'],
                'process_err'     => json_decode($log['process_err']),
                'process_payload' => json_decode($log['process_payload']),
                'process_message' => $log['process_message']
            ]);
        }
        $this->dropColumn('payment_pay_page_log', 'status');
        $this->dropColumn('payment_pay_page_log', 'process_err');
        $this->dropColumn('payment_pay_page_log', 'process_message');
        $this->dropColumn('payment_pay_page_log', 'process_payload');
    }

    public function safeDown()
    {

    }
}