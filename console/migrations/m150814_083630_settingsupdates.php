<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_083630_settingsupdates extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `system_setting`  ADD COLUMN `is_active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `json`;");
        $this->execute("CREATE TABLE `moder_log` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL DEFAULT '1',
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `started_at` timestamp NULL DEFAULT NULL,
            `finished_at` timestamp NULL DEFAULT NULL,
            `action` char(15) NOT NULL,
            `result` text NOT NULL,
            `object_type` char(15) NOT NULL DEFAULT 'store_unit',
            `object_id` int(11) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
          ");
        return 0;
    }

    public function down()
    {
        $this->execute("ALTER TABLE `system_setting` DROP COLUMN `is_active`;");
        $this->execute("DROP TABLE IF EXISTS `moder_log`;");
        return 0;
    } 
}
