<?php

use yii\db\Migration;

class m160225_103645_print_overdue_settings extends Migration
{
    public function up()
    {

        $this->insert('system_setting', [
            'group_id' => 9,
            'key' => 'max_print_overdue',
            'value' => 48,
            'created_user_id' => 1,
            'description' => 'Print expired in hours after that order will closed (https://redmine.tsdev.work/issues/182)',
        ]);
    }

    public function down()
    {
        $this->delete('system_setting', ['key' => 'max_print_overdue']);
        return true;
    }
}
