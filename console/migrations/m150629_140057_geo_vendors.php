<?php

use yii\db\Schema;
use yii\db\Migration;

class m150629_140057_geo_vendors extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $path = realpath(dirname(__DIR__) . '/../db/');
        $sql = file_get_contents($path . '/geo_vendors.sql');
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropIndex('geoname_id', '{{%geo_city}}');
        $this->dropForeignKey('fk_vendor_id', '{{%geo_location_vendor_results}}');
        $this->dropForeignKey('fk_vendor_id_2', '{{%geo_vendor_settings}}');

        $this->dropTable('{{%geo_location_vendor_results}}');
        $this->dropTable('{{%geo_vendor}}');
        $this->dropTable('{{%geo_vendor_settings}}');
        $this->dropTable('{{%geo_log}}');
        return 0;
    }
}
