<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_090806_model3d_file_rotate extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `model3d_file` 
            ADD COLUMN `rotated_x` SMALLINT NOT NULL DEFAULT 0 AFTER `file_src_id`,
            ADD COLUMN `rotated_y` SMALLINT NOT NULL DEFAULT 0 AFTER `rotated_x`,
            ADD COLUMN `rotated_z` SMALLINT NOT NULL DEFAULT 0 AFTER `rotated_y`;
            ");
    }

    public function down()
    {
        $this->dropColumn('model3d_file', 'rotated_x');
        $this->dropColumn('model3d_file', 'rotated_y');
        $this->dropColumn('model3d_file', 'rotated_z');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
