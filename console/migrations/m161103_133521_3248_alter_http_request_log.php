<?php

use yii\db\Schema;
use yii\db\Migration;

class m161103_133521_3248_alter_http_request_log extends Migration
{
    /*
    public function init()
    {
        $this->db = 'db2';
        parent::init(); 
    }*/

    public function safeUp()
    {
        $this->addColumn('http_request_log', 'url', 'varchar(1024) NULL AFTER id');
        $this->addColumn('http_request_log', 'remote_addr', 'varchar(50) NULL AFTER url');
        $this->addColumn('http_request_log', 'session', 'json NULL AFTER params_files');
        $this->addColumn('http_request_log', 'cookie', 'json NULL AFTER session');
        $this->update('http_request_log', ['url'=>'']);
        $this->alterColumn('http_request_log', 'url', 'varchar(1024) NOT NULL');
        $this->update('http_request_log', ['remote_addr'=>'']);
        $this->alterColumn('http_request_log', 'remote_addr', 'varchar(50) NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('http_request_log', 'url');
        $this->dropColumn('http_request_log', 'remote_addr');
        $this->dropColumn('http_request_log', 'session');
        $this->dropColumn('http_request_log', 'cookie');
    }
}
