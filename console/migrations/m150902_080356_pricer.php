<?php

use yii\db\Schema;
use yii\db\Migration;

class m150902_080356_pricer extends Migration
{
    
    public function safeUp()
    {
        
        $this->execute("CREATE TABLE IF NOT EXISTS `payment_exchange_rate` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `timestamp` int(11) NOT NULL,
            `base` char(5) NOT NULL DEFAULT 'USD',
            `rates` text NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `timestamp_UNIQUE` (`timestamp`,`base`)
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        ");
        
        $this->execute("CREATE TABLE `user_address` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `user_id` int(11) NOT NULL,
            `contact_name` varchar(45) NOT NULL DEFAULT '',
            `country_id` int(11) NOT NULL,
            `region` varchar(45) NOT NULL,
            `city` varchar(45) NOT NULL,
            `address` varchar(45) NOT NULL,
            `extended_address` varchar(45) DEFAULT NULL,
            `zip_code` varchar(45) DEFAULT NULL,
            `comment` varchar(155) DEFAULT NULL,
            `type` varchar(45) NOT NULL DEFAULT 'ship',
            PRIMARY KEY (`id`),
            KEY `fk_user_address_1_idx` (`user_id`),
            KEY `fk_user_address_2_idx` (`country_id`),
            CONSTRAINT `fk_user_address_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_user_address_2` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
          ");
        $this->execute("CREATE TABLE `store_order` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `ship_address_id` int(11) NOT NULL,
            `bill_address_id` int(11) NOT NULL,
            `price_total` decimal(10,4) NOT NULL,
            `price_currency` char(5) NOT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `billed_at` timestamp NULL DEFAULT NULL,
            `shipped_at` timestamp NULL DEFAULT NULL,
            `received_at` timestamp NULL DEFAULT NULL,
            `order_status` char(25) NOT NULL DEFAULT 'new',
            PRIMARY KEY (`id`),
            KEY `fk_store_order_1_idx` (`user_id`),
            KEY `fk_store_order_1_idx1` (`bill_address_id`),
            KEY `fk_store_order_2` (`ship_address_id`),
            CONSTRAINT `fk_store_order_1` FOREIGN KEY (`bill_address_id`) REFERENCES `user_address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_store_order_2` FOREIGN KEY (`ship_address_id`) REFERENCES `user_address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_store_order_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
        ");
         $this->execute("CREATE TABLE `payment_transaction` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `order_id` int(11) NOT NULL,
            `transaction_id` varchar(45) NOT NULL,
            `vendor` char(15) DEFAULT 'braintree',
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `status` char(15) NOT NULL,
            `type` varchar(45) DEFAULT NULL,
            `amount` decimal(10,4) NOT NULL,
            `currency` char(5) NOT NULL DEFAULT 'USD',
            `merchant_acount_id` varchar(45) DEFAULT NULL,
            `channel` varchar(45) DEFAULT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `transaction_id_UNIQUE` (`transaction_id`),
            KEY `fk_payment_transaction_1_idx` (`order_id`),
            CONSTRAINT `fk_payment_transaction_1` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          ");
        $this->execute("CREATE TABLE `store_pricer_type` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `type` char(45) NOT NULL,
            `description` varchar(145) DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `operation` char(1) NOT NULL DEFAULT '+',
            PRIMARY KEY (`id`),
            UNIQUE KEY `type_UNIQUE` (`type`)
          ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
          ");
        $this->execute("CREATE TABLE `store_order_history` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `order_id` int(11) NOT NULL,
            `action_id` char(15) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `comment` varchar(245) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_store_order_history_1_idx` (`order_id`),
            CONSTRAINT `fk_store_order_history_1` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        ");
        
        $this->execute("CREATE TABLE `store_order_item` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `order_id` int(11) NOT NULL,
            `unit_id` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `price` decimal(10,4) NOT NULL,
            `currency` char(5) NOT NULL,
            `qty` tinyint(1) DEFAULT '1',
            `material` varchar(45) NOT NULL,
            `color` char(15) NOT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            KEY `fk_store_order_item_1_idx` (`order_id`),
            KEY `fk_store_order_item_2_idx` (`unit_id`),
            CONSTRAINT `fk_store_order_item_1` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_store_order_item_2` FOREIGN KEY (`unit_id`) REFERENCES `store_unit` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
          ");
        
        $this->execute("CREATE TABLE `store_pricer` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `order_id` int(11) NOT NULL,
            `pricer_type` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `price` decimal(10,4) NOT NULL,
            `currency` char(5) NOT NULL,
            `rate_id` int(11) NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `store_pricer_uniq` (`order_id`,`pricer_type`),
            KEY `fk_store_pricer_1_idx` (`order_id`),
            KEY `fk_store_pricer_2_idx` (`pricer_type`),
            KEY `fk_store_pricer_3_idx` (`rate_id`),
            CONSTRAINT `fk_store_pricer_3` FOREIGN KEY (`rate_id`) REFERENCES `payment_exchange_rate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_store_pricer_1` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_store_pricer_2` FOREIGN KEY (`pricer_type`) REFERENCES `store_pricer_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        
        $this->execute("CREATE TABLE `store_pricer_history` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `pricer_id` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `old_price` decimal(10,4) NOT NULL,
            `new_price` decimal(10,4) NOT NULL,
            `user_id` int(11) NOT NULL,
            `comment` varchar(145) DEFAULT NULL,
            `action_id` int(11) NOT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_store_pricer_history_1_idx` (`user_id`),
            KEY `fk_store_pricer_history_2_idx` (`pricer_id`),
            CONSTRAINT `fk_store_pricer_history_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_store_pricer_history_2` FOREIGN KEY (`pricer_id`) REFERENCES `store_pricer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          ");
        // pricer data
        $data = [
            ['type'=>'model', 'description'=>'Model Price', 'operation'=> '+'],
            ['type'=>'tax', 'description'=>'Tax Price', 'operation'=> '+'],
            ['type'=>'fee', 'description'=>'Service Fee', 'operation'=> '+'],
            ['type'=>'shipping', 'description'=>'Shipping Fee', 'operation'=> '+'],
            ['type'=>'package', 'description'=>'Pack Fee', 'operation'=> '+'],
            ['type'=>'refund', 'description'=>'', 'operation'=> '+'],
            ['type'=>'discount', 'description'=>'Discount Fee', 'operation'=> '-'],
            ['type'=>'ps_print', 'description'=>'Print Fee', 'operation'=> '+'],
            ['type'=>'ps_discount', 'description'=>'Print Discount Fee', 'operation'=> '-'],
            ['type'=>'ps_postprint', 'description'=>'PS after print work fee', 'operation'=> '+'],
            ['type'=>'total', 'description'=>'Total Cost', 'operation'=> '.'],
        ];
        foreach($data as $row){
            $this->insert('store_pricer_type', $row);
        }
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $tables = [
            'store_pricer_history',
            'store_pricer',
            'store_order_item',
            'store_order_history',
            'store_pricer_type',
            'payment_transaction',
            'store_order',
            'user_address',
          //  'payment_exchange_rate'
        ];
        foreach($tables as $table){
            $this->truncateTable($table);
            $this->dropTable($table);
        }
        return true;
    }
 
}
