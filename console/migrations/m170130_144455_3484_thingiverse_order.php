<?php

use yii\db\Migration;

class m170130_144455_3484_thingiverse_order extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `thingiverse_order` (
              `thingiverse_order_id` int(11) NOT NULL,
              `order_id` int(11) DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT NULL,
              `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `is_shipped` tinyint(4) NOT NULL DEFAULT \'0\',
              `is_cancelled` tinyint(4) NOT NULL DEFAULT \'0\',
              `refund_note` varchar(5000) DEFAULT NULL,
              `note` varchar(500) DEFAULT NULL,
              `status` varchar(45) DEFAULT NULL,
              `api_json` varchar(2500) DEFAULT NULL,
              PRIMARY KEY (`thingiverse_order_id`),
              UNIQUE KEY `thingiverse_order_id_UNIQUE` (`thingiverse_order_id`),
              UNIQUE KEY `order_id_UNIQUE` (`order_id`),
              CONSTRAINT `fk_thingiverse_order_1` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute('ALTER TABLE `ps_printer` ADD COLUMN `is_ready_thingiverse` TINYINT(1) NOT NULL DEFAULT 0 AFTER `is_test_order_resolved`;');

        $this->execute('ALTER TABLE `thingiverse_user` ADD COLUMN `access_token` VARCHAR(45) NULL AFTER `created_at`;');
        $this->execute('ALTER TABLE `thingiverse_user` ADD COLUMN `updated_at` TIMESTAMP NULL AFTER `access_token`;');
    }

    public function down()
    {
        $this->truncateTable('thingiverse_order');
        $this->dropTable('thingiverse_order');

        $this->dropColumn('thingiverse_user', 'access_token');
        $this->dropColumn('thingiverse_user', 'updated_at');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
