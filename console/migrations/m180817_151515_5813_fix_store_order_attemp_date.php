<?php

use yii\db\Migration;

/**
 * Class m180817_151515_5813_fix_store_order_attemp_date
 */
class m180817_151515_5813_fix_store_order_attemp_date extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $columnList = [
            'msg_file',
            'payment',
            'payment_exchange_rate',
            'payment_transaction_history',
            'preorder_history' => 'time',
            'ps_cnc_machine_history',
            'ps_machine_history',
            'ps_printer_test',
            'site_tag',
            'store_order_attemp',
            'store_order_delivery',
            'system_setting',
            'system_setting_group',
            'user_access'
            ];
        $this->alterColumn('site_tag', 'updated_at', 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
        foreach ($columnList as $key=>$value) {
            if (is_int($key)) {
                $table = $value;
                $column = 'created_at';
            } else {
                $table = $key;
                $column = $value;
            }
            $this->alterColumn($table, $column, 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
