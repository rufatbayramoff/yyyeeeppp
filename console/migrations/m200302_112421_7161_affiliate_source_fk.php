<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200302_112421_7161_affiliate_source_fk
 */
class m200302_112421_7161_affiliate_source_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `affiliate_award` DROP FOREIGN KEY `affiliate_award_accrued_detail_id`; ALTER TABLE `affiliate_award` ADD CONSTRAINT `affiliate_award_accrued_detail_id` FOREIGN KEY (`accrued_payment_detail_id`) REFERENCES `payment_detail`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `affiliate_award` DROP FOREIGN KEY `affiliate_award_invoice_uuid`; ALTER TABLE `affiliate_award` ADD CONSTRAINT `affiliate_award_invoice_uuid` FOREIGN KEY (`invoice_uuid`) REFERENCES `payment_invoice`(`uuid`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `affiliate_award` DROP FOREIGN KEY `affiliate_award_source_uuid`; ALTER TABLE `affiliate_award` ADD CONSTRAINT `affiliate_award_source_uuid` FOREIGN KEY (`affiliate_source_uuid`) REFERENCES `affiliate_source`(`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->execute('ALTER TABLE `affiliate_source` DROP FOREIGN KEY `affiliate_api_ext_system`; ALTER TABLE `affiliate_source` ADD CONSTRAINT `affiliate_api_ext_system` FOREIGN KEY (`api_external_system_id`) REFERENCES `api_external_system`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `affiliate_source` DROP FOREIGN KEY `affiliate_source_company_id`; ALTER TABLE `affiliate_source` ADD CONSTRAINT `affiliate_source_company_id` FOREIGN KEY (`company_id`) REFERENCES `ps`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->execute('ALTER TABLE `affiliate_source_client` DROP FOREIGN KEY `affiliate_user_bind_user_id`; ALTER TABLE `affiliate_source_client` ADD CONSTRAINT `affiliate_user_bind_user_id` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `affiliate_source_client` DROP FOREIGN KEY `fk_affiliate_source_uuid_client`; ALTER TABLE `affiliate_source_client` ADD CONSTRAINT `fk_affiliate_source_uuid_client` FOREIGN KEY (`affiliate_source_uuid`) REFERENCES `affiliate_source`(`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}