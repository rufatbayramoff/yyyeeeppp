<?php

use yii\db\Migration;

class m160218_140612_fix_snap_model3d extends Migration
{
    public function up()
    {

        $this->execute("
            INSERT INTO store_order_model3d_file
            SELECT null id, NOW() as created_at, i.id  order_item_id, mf.id as model3d_file_id
            FROM store_order_item i
            LEFT JOIN store_unit as u ON i.unit_id = u.id
            LEFT JOIN model3d as m ON m.id = u.model3d_id
            LEFT JOIN model3d_file as mf ON mf.model3d_id = m.id
            WHERE mf.user_status <> 'incative' AND mf.moderator_status <> 'banned'
        ");

    }

    public function down()
    {
        return true;
    }

}
