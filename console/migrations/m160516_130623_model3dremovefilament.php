<?php

use yii\db\Migration;

class m160516_130623_model3dremovefilament extends Migration
{
     
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        
        $this->execute("
            ALTER TABLE `model3d_file_properties` 
            ADD COLUMN `volume` DOUBLE(14,4) UNSIGNED NULL  DEFAULT '0.0000' AFTER `faces`,
            ADD COLUMN `area` DOUBLE(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' AFTER `volume`"
        );
        
        $this->execute("ALTER TABLE `model3d_file_properties` 
                    ADD COLUMN `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER `area`;");

    }

    public function safeDown()
    {
        $this->dropColumn('model3d_file_properties', 'volume');
        $this->dropColumn('model3d_file_properties', 'area');
        $this->dropColumn('model3d_file_properties', 'updated_at');
    } 
}
