<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\modules\translation\components\DbI18n;
use common\modules\translation\components\Statistics;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200904_172321_7311_cutting_category
 */
class m200904_172321_7311_cutting_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('company_service_category', ['code'=>'cutting', 'slug'=>'cutting', 'title'=>'Cutting'], ['code'=>'laser-cutting']);
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}
