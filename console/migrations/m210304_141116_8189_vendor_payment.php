<?php

use yii\db\Migration;

/**
 * Class m210304_141116_8189_vendor_payment
 */
class m210304_141116_8189_vendor_payment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('payment_invoice_payment_method', 'vendor', "enum('braintree', 'ts', 'thingiverse', 'invoice', 'stripe')");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
