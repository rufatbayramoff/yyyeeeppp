<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180925_153121_5814_payments_invert_thingiverse
 *
 * Set at least one transaction history record
 */
class m180925_153121_5814_payments_invert_thingiverse extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $thingiverseDetails = (new Query())->select('*')->from('payment_detail')->where("user_id=112 and type='order'")->all();
        foreach ($thingiverseDetails as $thingiverseDetail) {
            $linkedDetails = (new Query())->select('*')->from('payment_detail')->where("id!=".$thingiverseDetail['id']." and payment_detail_operation_uuid='".$thingiverseDetail['payment_detail_operation_uuid']."'")->all();
            if (count($linkedDetails)!=1) {
                echo "Too many Details in thingiverse operation\n";
                exit;
            }
            $linkedDetail = reset($linkedDetails);
            if ($linkedDetail['user_id']<1000) {
                echo "Transfer to system user\n";
                exit;
            }
            $this->db->createCommand("UPDATE payment_detail SET amount=-amount WHERE payment_detail_operation_uuid='".$thingiverseDetail['payment_detail_operation_uuid']."'")->execute();
        }
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
