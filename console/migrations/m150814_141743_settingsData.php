<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_141743_settingsData extends Migration
{
    public function up()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $path = realpath(dirname(__DIR__) . '/../db/');
        $sqlString = file_get_contents($path . '/settingsData.sql');
        $sqls = explode(";", $sqlString);
        foreach($sqls as $sql){
            $sql = trim($sql);
            if(empty($sql)){
                continue;
            }
            $this->execute($sql);
        }
        return 0;
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->execute("TRUNCATE system_setting_group");
        $this->execute("TRUNCATE system_setting");
        return 0;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
