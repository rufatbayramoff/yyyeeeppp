<?php

use yii\db\Schema;
use yii\db\Migration;

class m161028_144521_3207_create_http_request_log extends Migration
{
    /*
    public function init()
    {
        $this->db = 'db2';
        parent::init(); 
    }*/

    public function safeUp()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `http_request_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params_get` json NOT NULL,
  `params_post` json NOT NULL,
  `params_files` json NOT NULL,
  `answer` json NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `run_time_milisec` int(11) NULL,
  `request_type` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
        $this->execute(
            <<<SQL
CREATE TABLE `http_request_ext_data_log` (
  `request_id` int(11) NOT NULL,
  `log_text` varchar(512) NOT NULL,
  `ext_data` json NOT NULL,
  KEY `request_id` (`request_id`),
  CONSTRAINT `http_request_fk` FOREIGN KEY (`request_id`) REFERENCES `http_request_log` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
    }

    public function safeDown()
    {
        $this->dropTable('http_request_log');
        $this->dropTable('http_request_ext_data_log');
    }
}
