<?php

use yii\db\Migration;

class m171017_114119_4887_cnc_preorder_coutomer_comment extends Migration
{
    public function safeUp()
    {
        $this->addColumn("cnc_preorder", "customer_comment", "TEXT DEFAULT NULL");
    }

    public function safeDown()
    {
        $this->dropColumn("cnc_preorder", "customer_comment");
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171017_114119_4887_cnc_preorder_coutomer_comment cannot be reverted.\n";

        return false;
    }
    */
}
