<?php

use yii\db\Migration;

class m170421_130521_4132_alter_materials extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `printer_material_group_intl` 
          CHANGE COLUMN `long_description` `long_description` TEXT NULL DEFAULT NULL;');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
