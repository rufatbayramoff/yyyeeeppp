<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m181003_134721_5814_payment_accounts
 *
 * Set at least one transaction history record
 */
class m181003_134721_5814_payment_accounts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
CREATE TABLE `payment_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` enum('main','authorize','reserved','award','tax','invoice','manufactureAward','refundRequest','discount','easypost') NOT NULL DEFAULT 'main',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_account` (`user_id`,`type`) USING BTREE,
  CONSTRAINT `fk_payment_account_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
        $this->execute("ALTER TABLE `payment_detail` ADD `payment_account_id` INT(11) AFTER `user_id`, ADD INDEX (`payment_account_id`);");
        $this->execute("ALTER TABLE `payment_detail` ADD CONSTRAINT `fk_payment_detail_4` FOREIGN KEY (`payment_account_id`) REFERENCES `payment_account`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
