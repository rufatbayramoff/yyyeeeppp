<?php

use yii\db\Migration;

/**
 * Class m180724_133624_5730_store_order_position_fee
 */
class m180724_133624_5730_store_order_position_fee extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('store_order_position', 'fee_include', 'decimal(10,4) null after fee');
        $this->alterColumn('store_order_position', 'fee', 'decimal(10,4) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('store_order_position', 'fee_include');
    }
}
