<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200402_175721_7229_ts_fee_remove
 */
class m200402_175721_7229_ts_fee_remove extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('system_setting', ['key'=>'ts_fee']);
        $this->delete('system_setting', ['key'=>'ts_min_fee_usd']);
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}