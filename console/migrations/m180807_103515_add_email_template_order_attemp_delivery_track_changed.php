<?php

use yii\db\Migration;

/**
 * Class m180807_103515_add_email_template_order_attemp_delivery_track_changed
 */
class m180807_103515_add_email_template_order_attemp_delivery_track_changed extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(
            'email_template',
            [
                'code'          => 'clientDeliveryTrackChanged',
                'group'         => 'order',
                'language_id'   => 'en-US',
                'title'         => 'Updated tracking number for order #%orderId%',
                'description'   => 'The customer receives a message when the order tracking number is changed',
                'updated_at'    => dbexpr('NOW()'),
                'template_html' => "Hi %name%, \n\nThe tracking number for order #%orderId% has been changed to %newTrackingNumber%. \n<a href=\"%link%\" style=\"display: inline-block;margin:10px 0;border-radius: 0px;font-family: sans-serif;padding: 5px 20px;min-width: 40px;font-size: 16px;font-weight: normal;line-height: 20px;border: none;text-decoration:none !important;color: #fff;background-color:#2d8ee0;\">View order</a>\n\nBest,\nTreatstock",
                'template_text' => "Hi %name%, \n\nThe tracking number for order #%orderId% has been changed to %newTrackingNumber%. \nView order: %link% \n\nBest, \nTreatstock"
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'clientDeliveryTrackChanged', 'language_id' => 'en-US']);
    }
}
