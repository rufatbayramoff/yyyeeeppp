<?php

use yii\db\Migration;

class m161020_143128_3100_help_intl extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `site_help_intl` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `help_id` int(11) DEFAULT NULL,
              `title` varchar(155) NOT NULL,
              `content` mediumtext NOT NULL,
              `lang_iso` char(7) NOT NULL DEFAULT \'en-US\',
              PRIMARY KEY (`id`),
              UNIQUE KEY `index4` (`help_id`,`lang_iso`),
              CONSTRAINT `fk_site_help_intl_1` FOREIGN KEY (`help_id`) REFERENCES `site_help` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );

        $this->execute('CREATE TABLE `site_help_category_intl` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `category_id` int(11) DEFAULT NULL,
              `title` varchar(500) NOT NULL,
              `info` text NOT NULL,
              `lang_iso` char(7) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_site_help_category_intl_1_idx` (`category_id`),
              CONSTRAINT `fk_site_help_category_intl_1` FOREIGN KEY (`category_id`) REFERENCES `site_help_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

    }

    public function down()
    {
         $this->truncateTable('site_help_intl');
         $this->truncateTable('site_help_category_intl');
         $this->dropTable('site_help_intl');
         $this->dropTable('site_help_category_intl');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
