<?php

use yii\db\Migration;

/**
 * Class m180522_084321_5186_product_bind_fix2
 */
class m180522_084321_5186_product_bind_fix2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `product_bind` DROP FOREIGN KEY `fk_product_bind_model3d`; ALTER TABLE `product_bind` ADD CONSTRAINT `fk_product_bind_model3d` FOREIGN KEY (`model3d_uuid`) REFERENCES `model3d`(`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->execute("ALTER TABLE `product_bind` DROP FOREIGN KEY `fk_product_bind_product`; ALTER TABLE `product_bind` ADD CONSTRAINT `fk_product_bind_product` FOREIGN KEY (`product_uuid`) REFERENCES `product`(`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
