<?php

use yii\db\Schema;
use yii\db\Migration;

class m151111_094331_args_column_in_jobs_table extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `file_job` ADD `args` TEXT  CHARACTER SET utf8  NULL  AFTER `updated_at`;");
        $this->execute("ALTER TABLE `file_job` MODIFY COLUMN `args` TEXT CHARACTER SET utf8 AFTER `status`;");

    }

    public function down()
    {
        echo "m151111_094331_args_column_in_jobs_table cannot be reverted.\n";

        return false;
    }
}
