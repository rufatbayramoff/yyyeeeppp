<?php

use yii\db\Migration;

class m171124_151221_4964_file_path_version3 extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('file', 'path_version', 'enum("v1", "v2", "v3") default "v2" NOT NULL');
    }

    public function safeDown()
    {
    }
}
