<?php

use yii\db\Migration;
use yii\db\Query;

class m170310_162721_3932_fb_several_page extends Migration
{
    public function up()
    {
        $this->execute(
            <<<SQL
CREATE TABLE `ps_facebook_page` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ps_id` INT(11) NOT NULL,
  `facebook_page_id` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `facebook_page_id` (`facebook_page_id`),
  KEY `ps_id` (`ps_id`),
  CONSTRAINT `ps_facebook_page_ps` FOREIGN KEY (`ps_id`) REFERENCES `ps` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
        );
        $rows = (new Query())->select('*')->from('ps')->where('facebook_page_id is not null')->all();
        foreach ($rows as $ps) {
            $this->insert(
                'ps_facebook_page',
                [
                    'ps_id'            => $ps['id'],
                    'facebook_page_id' => $ps['facebook_page_id']
                ]
            );
        }
        $this->dropColumn('ps', 'facebook_page_id');
    }

    public function down()
    {
        echo 'Can`t roll back migration';
        return false;
    }
}
