<?php

use yii\db\Schema;
use yii\db\Migration;

class m161107_113231_3244_alter_catalog_cost_color extends Migration
{
    public function safeUp()
    {
        $this->execute(
            <<<SQL
 CREATE TABLE `model3d_texture_catalog_cost` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `printer_material_id` INT(11) DEFAULT NULL,
  `printer_color_id` INT(11) NOT NULL,
  `printer_material_group_id` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `printer_material_id` (`printer_material_id`),
  KEY `printer_color_id` (`printer_color_id`),
  KEY `model3d_texture_catalog_material_group_id` (`printer_material_group_id`),
  CONSTRAINT `model3d_catalog_color_printer_color_id` FOREIGN KEY (`printer_color_id`) REFERENCES `printer_color` (`id`),
  CONSTRAINT `model3d_catalog_color_printer_material_id` FOREIGN KEY (`printer_material_id`) REFERENCES `printer_material` (`id`),
  CONSTRAINT `model3d_catalog_texture_material_group_id` FOREIGN KEY (`printer_material_group_id`) REFERENCES `printer_material_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
SQL
        );
        $this->addColumn('catalog_cost', 'texture_id', 'int (11) NULL AFTER cost_usd');
        $this->addColumn('catalog_cost', 'ps_printer_id', 'int(11) NULL AFTER texture_id');
        $this->addForeignKey('fk_catalog_cost_texture_id', 'catalog_cost', 'texture_id', 'model3d_texture_catalog_cost', 'id');
        $this->addForeignKey('fk_catalog_ps_printer_id', 'catalog_cost', 'ps_printer_id', 'ps_printer', 'id');
        $this->addColumn('printer_material_group', 'code', 'varchar(50) after id');
        $this->execute(
            <<<SQL
            UPDATE `printer_material_group` SET `code`=`title`
SQL
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_catalog_cost_texture_id', 'catalog_cost');
        $this->dropForeignKey('fk_catalog_ps_printer_id', 'catalog_cost');
        $this->dropColumn('catalog_cost', 'ps_printer_id');
        $this->dropColumn('catalog_cost', 'texture_id');
        $this->dropColumn('printer_material_group', 'code');
        $this->dropTable('model3d_texture_catalog_cost');
    }
}

