<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190617_130521_6522_preoprder_client_created
 */
class m190617_130521_6522_preoprder_client_created extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('preorder', 'created_by', "ENUM('ps', 'client') NOT NULL DEFAULT 'client'");
        $this->addColumn('preorder', 'is_client_created', 'int(1) not null default 0');
    }

    public function safeDown()
    {
        $this->dropColumn('preorder', 'is_client_created');
    }
}