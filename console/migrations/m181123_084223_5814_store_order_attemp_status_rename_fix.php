<?php

use yii\db\Migration;

/**
 * Class m181123_084223_5814_store_order_attemp_status_rename_fix
 */
class m181123_084223_5814_store_order_attemp_status_rename_fix extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("UPDATE store_order_attemp set status = 'canceled' where status = 'cancelled'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181123_084223_5814_store_order_attemp_status_rename_fix cannot be reverted.\n";
        return false;
    }
}
