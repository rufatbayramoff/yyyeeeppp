<?php

use yii\db\Schema;
use yii\db\Migration;

class m150916_150822_addressFormat extends Migration
{
 
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("CREATE TABLE `delivery_address_format` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `country_id` int(11) NOT NULL,
            `format` varchar(245) NOT NULL,
            `is_active` tinyint(1) NOT NULL DEFAULT '1',
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            KEY `fk_delivery_address_format_1_idx` (`country_id`),
            CONSTRAINT `fk_delivery_address_format_1` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );
        $format = "%title%\n".
            "%street% %street2%\n".
            "%city% %region% %zip_code%\n".
            "%country%";
        $this->insert('delivery_address_format', [
            'country_id' => 233,
            'format' => $format,
            'created_at' => dbexpr('now()')
        ]);
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');        
        $this->truncateTable('delivery_address_format');
        $this->dropTable('delivery_address_format');
    }
    
}
