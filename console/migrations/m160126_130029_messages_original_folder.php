<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_130029_messages_original_folder extends Migration
{
    public function up()
    {
        $this->db->createCommand()->checkIntegrity(false)->execute();
        $this->truncateTable('msg_message');
        $this->truncateTable('msg_member');
        $this->truncateTable('msg_topic');
        $this->db->createCommand()->checkIntegrity(true)->execute();

        $this->execute("ALTER TABLE `msg_member` ADD `original_folder` INT(11)  UNSIGNED  NOT NULL  AFTER `folder_id`;");
        $this->execute("ALTER TABLE `msg_member` ADD FOREIGN KEY (`original_folder`) REFERENCES `msg_folder` (`id`);");
    }

    public function down()
    {
        $this->dropForeignKey('msg_member_ibfk_4', 'msg_member');
        $this->dropColumn('msg_member', 'original_folder');
        return true;
    }
}
