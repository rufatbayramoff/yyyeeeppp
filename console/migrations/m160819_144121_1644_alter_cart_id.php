<?php

use yii\db\Migration;

class m160819_144121_1644_alter_cart_id extends Migration
{
    public function up()
    {
        $this->dropForeignKey('cart_item_cart_id', 'cart_item');
        $this->alterColumn('cart', 'id', 'integer(11) NOT NULL auto_increment');
        $this->addForeignKey('cart_item_cart_id', 'cart_item', 'cart_id', 'cart', 'id');
    }

    public function down()
    {
        echo 'Can`t revert migration m160819_144121_1644_alter_cart_id';
        return false;
    }
}
