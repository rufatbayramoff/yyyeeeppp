<?php

use yii\db\Migration;

class m170524_104421_4292_printer_min_cost_hash extends Migration
{
    public function up()
    {
        $this->addColumn('printer_min_cost_hash', 'price_formula_fixed', 'DECIMAL(11,2) NOT NULL AFTER weight_to');
        $this->addColumn('printer_min_cost_hash', 'price_formula_multiply', 'DECIMAL(30,20) NOT NULL AFTER price_formula_fixed');
        $this->dropColumn('printer_min_cost_hash', 'price_formula');
    }

    public function down()
    {
        $this->dropColumn('printer_min_cost_hash', 'price_formula_fixed');
        $this->dropColumn('printer_min_cost_hash', 'price_formula_multiply');
        $this->addColumn('printer_min_cost_hash', 'price_formula', '	varchar(20) not null AFTER weight_to');
    }
}
