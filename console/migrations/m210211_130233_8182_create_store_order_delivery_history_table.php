<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%store_order_delivery_history}}`.
 */
class m210211_130233_8182_create_store_order_delivery_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('{{%store_order_delivery_history}}', [
            'id'         => $this->primaryKey(),
            'order_id'   => $this->integer()->notNull()->unique(),
            'width'      => $this->decimal(7, 2)->notNull(),
            'height'     => $this->decimal(7, 2)->notNull(),
            'length'     => $this->decimal(7, 2)->notNull(),
            'weight'     => $this->decimal(7, 2)->notNull(),
            'measure'    => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull()
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            'idx-store_order_delivery_history-order_id',
            'store_order_delivery_history',
            'order_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-store_order_delivery_history-order_id',
            'store_order_delivery_history',
            'order_id',
            'store_order',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-store_order_delivery_history-order_id',
            'store_order_delivery_history'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-store_order_delivery_history-order_id',
            'store_order_delivery_history'
        );

        $this->dropTable('{{%store_order_delivery_history}}');
    }
}
