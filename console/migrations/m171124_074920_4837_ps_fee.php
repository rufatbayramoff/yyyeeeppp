<?php

use yii\db\Migration;

class m171124_074920_4837_ps_fee extends Migration
{
    public function safeUp()
    {
        $this->insert('store_pricer_type', [
            'type' => 'ps_fee',
            'description' => 'PS Fee',
            'operation' => ''
        ]);
    }

    public function safeDown()
    {
        $this->delete('store_pricer_type', ['type'=>'ps_fee']);
    }
}
