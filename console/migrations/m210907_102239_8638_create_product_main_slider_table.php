<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_main_slider}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%file_admin}}`
 */
class m210907_102239_8638_create_product_main_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_main_slider}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'url' => $this->string(),
            'button' => $this->string(),
            'file_uuid' => $this->string(),
        ]);

        // creates index for column `file_uuid`
        $this->createIndex(
            '{{%idx-product_main_slider-file_uuid}}',
            '{{%product_main_slider}}',
            'file_uuid'
        );


        $this->addForeignKey(
            '{{%fk-product_main_slider-file_uuid}}',
            '{{%product_main_slider}}',
            'file_uuid',
            '{{%file_admin}}',
            'uuid'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%file_admin}}`
        $this->dropForeignKey(
            '{{%fk-product_main_slider-file_uuid}}',
            '{{%product_main_slider}}'
        );

        // drops index for column `file_uuid`
        $this->dropIndex(
            '{{%idx-product_main_slider-file_uuid}}',
            '{{%product_main_slider}}'
        );

        $this->dropTable('{{%product_main_slider}}');
    }
}
