<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190109_103821_6184_geo_country_not_exists
 */
class m190109_103821_6184_geo_country_not_exists extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cities = (new Query())
            ->select('country_id, geo_city.id')
            ->from('geo_city')
            ->leftJoin('geo_country', 'geo_country.id=geo_city.country_id')
            ->where('geo_country.id is null')
            ->all();
        foreach ($cities as $city) {
                $this->delete('geo_city', ['id'=>$city['id']]);
        }
        $regions = (new Query())
            ->select('country_id, geo_region.id')
            ->from('geo_region')
            ->leftJoin('geo_country', 'geo_country.id=geo_region.country_id')
            ->where('geo_country.id is null')
            ->all();
        foreach ($regions as $region) {
            $this->delete('geo_region', ['id'=>$region['id']]);
        }
    }

    public function safeDown()
    {

    }
}