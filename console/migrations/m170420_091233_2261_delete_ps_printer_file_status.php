<?php

use yii\db\Migration;

class m170420_091233_2261_delete_ps_printer_file_status extends Migration
{
    public function up()
    {
        $this->dropTable("ps_printer_file_status");
    }

    public function down()
    {
        echo "m170420_091233_2261_delete_ps_printer_file_status cannot be reverted.\n";
        return false;
    }
}
