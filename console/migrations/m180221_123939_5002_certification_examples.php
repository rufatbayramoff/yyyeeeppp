<?php

use yii\db\Migration;

/**
 * Class m180221_123939_5002_certification_examples
 */
class m180221_123939_5002_certification_examples extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `printer_technology_certification_example` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `technology_id` int(11) NOT NULL,
              `file_uuid` varchar(32) NOT NULL,
              `type` enum('common','professional') NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk_printer_technology_certification_example_technology_id_idx` (`technology_id`),
              KEY `fk_printer_technology_certification_example_file_admin_idx` (`file_uuid`),
              CONSTRAINT `fk_printer_technology_certification_example_file_admin` FOREIGN KEY (`file_uuid`) REFERENCES `file_admin` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
              CONSTRAINT `fk_printer_technology_certification_example_preinter_technology` FOREIGN KEY (`technology_id`) REFERENCES `printer_technology` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('printer_technology_certification_example');
        return true;
    }
}
