<?php

use yii\db\Migration;

class m160812_095411_1644_model3d_replica_img extends Migration
{
    public function up()
    {
            $this->execute(
            <<<SQL
    CREATE TABLE `model3d_replica_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model3d_id` int(11) NOT NULL,
  `size` int(11) DEFAULT NULL COMMENT 'File size (KB)',
  `title` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `moderated_at` timestamp NULL DEFAULT NULL,
  `is_moderated` bit(1) NOT NULL DEFAULT b'0',
  `basename` varchar(250) DEFAULT NULL,
  `extension` char(5) NOT NULL DEFAULT 'png',
  `user_id` int(11) NOT NULL COMMENT 'User ID who owns file',
  `type` set('owner','review','user') NOT NULL,
  `review_id` int(11) DEFAULT NULL,
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_model3d_replica_files_1_idx` (`model3d_id`),
  KEY `fk_model3d_replica_img_2_idx` (`user_id`),
  KEY `fk_model3d_replica_img_3_idx` (`file_id`),
  CONSTRAINT `fk_model3d_replica_img_1` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_model3d_replica_img_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_model3d_replica_img_3` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='Images for 3D model replica';
SQL
        );
    }

    public function down()
    {
        echo "m160811_164414_1644_model3d_part_properties cannot be reverted.\n";
        return false;
    }
}
