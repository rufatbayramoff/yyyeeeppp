<?php

use yii\db\Migration;

/**
 * Class m181210_112221_fix_user_paypal_history
 */
class m181210_112221_fix_user_paypal_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `user_paypal_history` CHANGE `comment` `comment` VARCHAR(4048) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
