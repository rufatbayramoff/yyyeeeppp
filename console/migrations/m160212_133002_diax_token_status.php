<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_133002_diax_token_status extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer_3diax_token` ADD `status` VARCHAR(30)  NOT NULL  DEFAULT 'new'  AFTER `token`;");
        $this->execute("ALTER TABLE `ps_printer_3diax_token` CHANGE `check_status_at` `check_status_at` TIMESTAMP  NOT NULL;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `ps_printer_3diax_token` DROP `status`;");
        $this->execute("ALTER TABLE `ps_printer_3diax_token` CHANGE `check_status_at` `check_status_at` TIMESTAMP  NULL  DEFAULT NULL;");
        return true;
    }
}
