<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_094051_useraddress_limitsup extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_address` 
            CHANGE COLUMN `contact_name` `contact_name` VARCHAR(145) NOT NULL DEFAULT '' ,
            CHANGE COLUMN `region` `region` VARCHAR(145) NOT NULL ,
            CHANGE COLUMN `city` `city` VARCHAR(145) NOT NULL ,
            CHANGE COLUMN `address` `address` VARCHAR(145) NOT NULL ,
            CHANGE COLUMN `extended_address` `extended_address` VARCHAR(145) NULL DEFAULT NULL ,
            CHANGE COLUMN `company` `company` VARCHAR(145) NULL DEFAULT NULL ;
        ");
    }

    public function down()
    {
         
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
