<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m181023_105230_5814_payment_invoice_company_id
 *
 * Payment log db
 */
class m181023_105230_5814_payment_invoice_company_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payment_invoice', 'company_id', 'integer(11) null after status');
        $this->addForeignKey('payment_invoice_company_fk', 'payment_invoice', 'company_id', 'ps', 'id');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('payment_invoice_company_fk', 'payment_invoice');
        $this->dropColumn('payment_invoice', 'company_id');
    }
}
