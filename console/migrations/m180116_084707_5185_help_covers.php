<?php

use yii\db\Migration;

/**
 * Class m180116_084707_5185_help_covers
 */
class m180116_084707_5185_help_covers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `site_help` 
            ADD COLUMN `cover_file_id` INT(11) NULL,
            ADD INDEX `fk_site_help_cover_file_idx` (`cover_file_id` ASC);
            ALTER TABLE `site_help` 
            ADD CONSTRAINT `fk_site_help_cover_file_idx`
              FOREIGN KEY (`cover_file_id`)
              REFERENCES `file` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");

        $this->execute("
            ALTER TABLE `site_help_category` 
            ADD COLUMN `cover_file_id` INT(11) NULL,
            ADD INDEX `fk_site_help_category_cover_file_idx` (`cover_file_id` ASC);
            ALTER TABLE `site_help_category` 
            ADD CONSTRAINT `fk_site_help_category_cover_file_idx`
              FOREIGN KEY (`cover_file_id`)
              REFERENCES `file` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_site_help_cover_file_idx', 'site_help');
        $this->dropColumn('site_help', 'cover_file_id');

        $this->dropForeignKey('fk_site_help_category_cover_file_idx', 'site_help_category');
        $this->dropColumn('site_help_category', 'cover_file_id');

        return true;
    }
}
