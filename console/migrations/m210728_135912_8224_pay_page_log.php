<?php

use yii\db\Migration;

/**
 * Class m210728_135912_8224_pay_page_log
 */
class m210728_135912_8224_pay_page_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `payment_pay_page_log_status` CHANGE `status` `status` ENUM('press_button','processed','server_process', 'done','failed', 'authorize_failed') NOT NULL");
    }
}