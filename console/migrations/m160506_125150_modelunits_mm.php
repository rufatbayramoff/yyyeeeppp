<?php

use yii\db\Migration;

class m160506_125150_modelunits_mm extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `model3d`  CHANGE COLUMN `model_units` `model_units` CHAR(4) NOT NULL DEFAULT 'mm' ;");
        // default is always in mm
        $this->execute("UPDATE `model3d` SET `model_units`='mm' WHERE `id`>0");
    }

    public function down()
    {
        
    } 
}
