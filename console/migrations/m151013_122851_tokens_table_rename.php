<?php

use yii\db\Schema;
use yii\db\Migration;

class m151013_122851_tokens_table_rename extends Migration
{
    public function safeUp()
    {
        $this->renameTable('ps_printer_3diax_tokens', 'ps_printer_3diax_token');
    }

    public function safeDown()
    {
        $this->renameTable('ps_printer_3diax_token', 'ps_printer_3diax_tokens');
    }
}
