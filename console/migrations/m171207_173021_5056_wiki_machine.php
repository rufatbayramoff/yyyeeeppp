<?php

use yii\db\Migration;

class m171207_173021_5056_wiki_machine extends Migration
{
    public function safeUp()
    {
        $this->execute(
            "
CREATE TABLE `equipment_category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `lft` INT(11) NOT NULL,
  `rgt` INT(11) NOT NULL,
  `depth` INT(11) NOT NULL,
  `code` VARCHAR(20) NOT NULL,
  `slug` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
        );

        $this->execute(
            "
  CREATE TABLE `equipment_category_intl` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `lang_iso` CHAR(7) NOT NULL,
  `is_active` BIT(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
        );

        $this->execute(
            "
CREATE TABLE `wiki_machine` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `equipment_category_id` INT(11) NOT NULL,
  `code` VARCHAR(20) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `main_photo_file_uuid` VARCHAR(32) NULL,
  `description` TEXT NOT NULL,
  `web_site_url` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `equipment_category_id` (`equipment_category_id`),
  KEY `main_photo_file_uuid` (`main_photo_file_uuid`),
  CONSTRAINT `fk_main_photo_file_uuid` FOREIGN KEY (`main_photo_file_uuid`) REFERENCES `file` (`uuid`),
  CONSTRAINT `fk_wiki_machine_equipment` FOREIGN KEY (`equipment_category_id`) REFERENCES `equipment_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        "
        );

        $this->execute(
            "
  CREATE TABLE `wiki_machine_intl` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `lang_iso` CHAR(7) NOT NULL,
  `is_active` BIT(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`),
  KEY `lang_iso` (`lang_iso`),
  CONSTRAINT `fk_wiki_machine_intl_lang_iso` FOREIGN KEY (`lang_iso`) REFERENCES `system_lang` (`iso_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_wiki_machine_intl_model_id` FOREIGN KEY (`model_id`) REFERENCES `wiki_machine` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        "
        );


        $this->execute(
            "
CREATE TABLE `wiki_machine_photo` (
  `wiki_machine_id` INT(11) NOT NULL,
  `photo_file_uuid` VARCHAR(32) NOT NULL,
  KEY `wiki_machine_id` (`wiki_machine_id`),
  UNIQUE KEY `photo_file_uuid` (`photo_file_uuid`),
  CONSTRAINT `fk_wiki_machine_photo_machine_id` FOREIGN KEY (`wiki_machine_id`) REFERENCES `wiki_machine` (`id`),
  CONSTRAINT `fk_wiki_machine_photo_photo_id` FOREIGN KEY (`photo_file_uuid`) REFERENCES `file` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
        );

        $this->execute(
            "
CREATE TABLE `wiki_machine_file` (
  `wiki_machine_id` INT(11) NOT NULL,
  `file_uuid` VARCHAR(32) NOT NULL,
  KEY `wiki_machine_id` (`wiki_machine_id`),
  UNIQUE KEY `file_uuid` (`file_uuid`),
  CONSTRAINT `fk_wiki_machine_file_machine_id` FOREIGN KEY (`wiki_machine_id`) REFERENCES `wiki_machine` (`id`),
  CONSTRAINT `fk_wiki_machine_file_file_uuid` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
        );

        $this->execute(
            "
CREATE TABLE `wiki_machine_available_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `show_in_catalog` enum('dont_show','in_top','in_bottom') NOT NULL DEFAULT 'dont_show',
  `title` varchar(255) NOT NULL,
  `equipment_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `equipment_category_id` (`equipment_category_id`),
  CONSTRAINT `fk_ wiki_machine_available_property_equipment_id` FOREIGN KEY (`equipment_category_id`) REFERENCES `equipment_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
"
        );

        $this->execute(
            "
CREATE TABLE `wiki_machine_available_property_intl` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `lang_iso` CHAR(7) NOT NULL,
  `is_active` BIT(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
        );


        $this->execute(
            "
  CREATE TABLE `wiki_machine_property` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `wiki_machine_id` INT(11) NOT NULL,
  `wiki_machine_available_property_id` INT(11) NOT NULL,
  `value` VARCHAR(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wiki_machine_id` (`wiki_machine_id`),
  KEY `wiki_machine_available_property_id` (`wiki_machine_available_property_id`),
  UNIQUE KEY `unique_prop` (`wiki_machine_id`, `wiki_machine_available_property_id`),
  CONSTRAINT `fk_wiki_machine_id` FOREIGN KEY (`wiki_machine_id`) REFERENCES `wiki_machine` (`id`),
  CONSTRAINT `fk_wiki_machine_property_id` FOREIGN KEY (`wiki_machine_available_property_id`) REFERENCES `wiki_machine_available_property` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
        );

        $this->execute(
            "
  CREATE TABLE `wiki_machine_property_intl` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_id` INT(11) NOT NULL,
  `value` VARCHAR(1024) NOT NULL,
  `lang_iso` VARCHAR(7) NOT NULL,
  `is_active` BIT(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`),
  KEY `lang_iso` (`lang_iso`),
  CONSTRAINT `fk_wiki_machine_property_intl_lang_iso` FOREIGN KEY (`lang_iso`) REFERENCES `system_lang` (`iso_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_wiki_machine_property_intl_model_id` FOREIGN KEY (`model_id`) REFERENCES `wiki_machine_property` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
        );


    }

    public function safeDown()
    {
        $this->dropTable('wiki_machine_property_intl');
        $this->dropTable('wiki_machine_property');
        $this->dropTable('wiki_machine_available_property_intl');
        $this->dropTable('wiki_machine_available_property');
        $this->dropTable('wiki_machine_file');
        $this->dropTable('wiki_machine_photo');
        $this->dropTable('wiki_machine_intl');
        $this->dropTable('wiki_machine');
        $this->dropTable('equipment_category_intl');
        $this->dropTable('equipment_category');

    }
}
