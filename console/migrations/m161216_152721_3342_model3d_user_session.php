<?php

use yii\db\Migration;
use yii\db\Query;

class m161216_152721_3342_model3d_user_session extends Migration
{

    public function up()
    {
        $this->update('model3d', ['published_at' => null ], "CAST(published_at as CHAR(20)) = '0000-00-00 00:00:00'");
        $this->addColumn('model3d', 'user_session_id', 'integer(11) null after title');
        $existsFirstSession = (new Query())
                ->select('id')
                ->from('model3d')
                ->where('id=1')
                ->one();
        if ($existsFirstSession) {
            $this->update(
                'user_session',
                [
                    'expire'  => null,
                    'data'    => null,
                    'uuid'    => 'j2bpt2arh4eju75m8b41i3urv3',
                    'user_id' => null,
                ],
                'id = 1'
            );
        } else {
            $this->insert(
                'user_session',
                [
                    'id'      => 1,
                    'expire'  => null,
                    'data'    => null,
                    'uuid'    => 'j2bpt2arh4eju75m8b41i3urv3',
                    'user_id' => null,
                ]
            );
        }
        $this->update('model3d', ['user_session_id' => 1]);
        $this->alterColumn('model3d', 'user_session_id', 'integer(11) not null');
        $this->addForeignKey('model3_user_session_id_fk', 'model3d', 'user_session_id', 'user_session', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('model3_user_session_id_fk', 'model3d');
        $this->dropColumn('model3d', 'user_session_id');
    }
}
