<?php

use yii\db\Migration;

class m160817_173345_1644_alter_model3d_replica_model3d_texture extends Migration
{
    public function up()
    {
        $this->addForeignKey('model3d_replica_model3d_texture', 'model3d_replica', 'model3d_texture_id', 'model3d_texture', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('model3d_replica_model3d_texture', 'model3d_replica');
    }
}
