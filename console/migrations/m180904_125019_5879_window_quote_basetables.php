<?php

use yii\db\Migration;

/**
 * Class m180904_125019_5879_window_quote_basetables
 */
class m180904_125019_5879_window_quote_basetables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            'CREATE TABLE `cs_window` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `uid` char(10) NOT NULL,
                  `company_service_id` int(11) NOT NULL,
                  `measurement` char(5) DEFAULT \'mm\',
                  `windowsill` decimal(8,2) NOT NULL,
                  `lamination` decimal(8,2) NOT NULL,
                  `slopes` decimal(8,2) NOT NULL,
                  `tinting` decimal(8,2) NOT NULL,
                  `energy_saver` decimal(8,2) NOT NULL,
                  `installation` int(11) NOT NULL,
                  `created_at` datetime NOT NULL,
                  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `index2` (`company_service_id`),
                  UNIQUE KEY `unique_uid` (`uid`),
                  CONSTRAINT `fk_cs_window_1` FOREIGN KEY (`company_service_id`) REFERENCES `company_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE=InnoDB;'
        );
        $this->execute(
            'CREATE TABLE `cs_window_furniture` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `title` varchar(245) NOT NULL,
              `cs_window_uid` CHAR(10) NOT NULL,
              `price` decimal(8,2) NOT NULL,
              `price_swivel` decimal(8,2) NOT NULL,
              `price_folding` decimal(8,2) NOT NULL,
              `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              KEY `fk_cs_window_furniture_1_idx` (`cs_window_uid`),
              CONSTRAINT `fk_cs_window_furniture_1` FOREIGN KEY (`cs_window_uid`) REFERENCES `cs_window` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB;
        '
        );
        $this->execute(
            'CREATE TABLE `cs_window_glass` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  `title` varchar(245) NOT NULL,
                  `cs_window_uid` CHAR(10) NOT NULL,
                  `thickness` smallint(6) NOT NULL,
                  `chambers` smallint(6) NOT NULL,
                  `noise_reduction` smallint(6) DEFAULT NULL,
                  `thermal_resistance` smallint(6) DEFAULT NULL,
                  `price` decimal(8,2) NOT NULL,
                  PRIMARY KEY (`id`),
                  KEY `fk_cs_window_glass_1_idx` (`cs_window_uid`),
                  CONSTRAINT `fk_cs_window_glass_1` FOREIGN KEY (`cs_window_uid`) REFERENCES `cs_window` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE=InnoDB;
                '
        );
        $this->execute(
            'CREATE TABLE `cs_window_history` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `cs_window_id` INT(11) NOT NULL,
                  `user_id` int(11) DEFAULT NULL,
                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `action_id` varchar(45) NOT NULL,
                  `comment` text,
                  `data` text,
                  PRIMARY KEY (`id`),
                  KEY `fk_ps_id_history` (`cs_window_id`),
                  KEY `fk_ps_id_user_id` (`user_id`),
                  CONSTRAINT `fk_cs_window_history_1` FOREIGN KEY (`cs_window_id`) REFERENCES `cs_window` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  CONSTRAINT `fk_cs_window_history_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE=InnoDB;
                '
        );
        $this->execute(
            'CREATE TABLE `cs_window_profile` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  `title` varchar(245) NOT NULL,
                  `cs_window_uid` CHAR(10) NOT NULL,
                  `thickness` smallint(6) NOT NULL,
                  `chambers` smallint(6) NOT NULL,
                  `max_glass` smallint(6) NOT NULL,
                  `max_width` smallint(6) NOT NULL,
                  `max_height` smallint(6) NOT NULL,
                  `noise_reduction` smallint(6) DEFAULT NULL,
                  `thermal_resistance` smallint(6) DEFAULT NULL,
                  `price` decimal(8,2) DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  KEY `fk_cs_window_profile_1_idx` (`cs_window_uid`),
                  CONSTRAINT `fk_cs_window_profile_1` FOREIGN KEY (`cs_window_uid`) REFERENCES `cs_window` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE=InnoDB;
                '
        );
        $this->execute(
            'CREATE TABLE `cs_window_snapshot` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `uid` char(10) NOT NULL,
                  `created_at` datetime NOT NULL,
                  `cs_window_uid` CHAR(10) NOT NULL,
                  `furniture` json DEFAULT NULL,
                  `glass` json DEFAULT NULL,
                  `profile` json DEFAULT NULL,
                  `service` json DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `uid_UNIQUE` (`uid`),
                  KEY `fk_cs_window_snapshot_1_idx` (`cs_window_uid`),
                  CONSTRAINT `fk_cs_window_snapshot_1` FOREIGN KEY (`cs_window_uid`) REFERENCES `cs_window` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE=InnoDB;
                '
        );

        $this->execute(
            'CREATE TABLE `cs_window_location` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `cs_window_uid` CHAR(10) NOT NULL,
                  `location_id` int(11) NOT NULL,
                  `radius` int(11) NOT NULL,
                  PRIMARY KEY (`id`),
                  KEY `fk_cs_window_location_1_idx` (`cs_window_uid`),
                  KEY `fk_cs_window_location_2_idx` (`location_id`),
                  CONSTRAINT `fk_cs_window_location_1` FOREIGN KEY (`cs_window_uid`) REFERENCES `cs_window` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  CONSTRAINT `fk_cs_window_location_2` FOREIGN KEY (`location_id`) REFERENCES `user_location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE=InnoDB;'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cs_window_snapshot');
        $this->dropTable('cs_window_location');
        $this->dropTable('cs_window_profile');
        $this->dropTable('cs_window_history');
        $this->dropTable('cs_window_glass');
        $this->dropTable('cs_window_furniture');
        $this->dropTable('cs_window');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180904_125019_5879_window_quote_basetables cannot be reverted.\n";

        return false;
    }
    */
}
