<?php

use yii\db\Migration;

class m160630_143925_1752_user_alter_token extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'access_token', 'string null');
        $this->createIndex('access_token_uq', 'user', 'access_token', true);
    }

    public function down()
    {
        $this->dropIndex('access_token_uq', 'user');
        $this->dropColumn('user', 'access_token');
    }
}
