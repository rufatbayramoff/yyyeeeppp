<?php

use yii\db\Migration;
use yii\helpers\Json;

class m170816_112706_4575_add_allowed_ext extends Migration
{
    public function safeUp()
    {
        $data = $this->db->createCommand("SELECT * FROM system_setting WHERE `key` = 'allowextension'")->queryAll();
        foreach ($data as $item) {

            $formats = Json::decode($item['json']);

            $formats[] = 'step';
            $formats[] = 'stp';
            $formats[] = 'iges';
            $formats[] = 'igs';

            $formats = array_unique($formats);

            $formats = Json::encode($formats);

            $this->db->createCommand("UPDATE system_setting SET json = '{$formats}' WHERE id = $item[id]")->execute();
        }
    }

    public function safeDown()
    {
        $data = $this->db->createCommand("SELECT * FROM system_setting WHERE `key` = 'allowextension'")->queryAll();
        foreach ($data as $item) {

            $formats = Json::decode($item['json']);

            if(($key = array_search('step', $formats)) !== false) {
                unset($formats[$key]);
            }
            if(($key = array_search('stp', $formats)) !== false) {
                unset($formats[$key]);
            }
            if(($key = array_search('iges', $formats)) !== false) {
                unset($formats[$key]);
            }
            if(($key = array_search('igs', $formats)) !== false) {
                unset($formats[$key]);
            }

            $formats = array_unique($formats);

            $formats = Json::encode($formats);

            $this->db->createCommand("UPDATE system_setting SET json = '{$formats}' WHERE id = $item[id]")->execute();
        }
    }
}
