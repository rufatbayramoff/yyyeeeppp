<?php

use yii\db\Migration;

class m161110_111844_3283_template_active extends Migration
{
    public function up()
    {

        $this->execute("
        ALTER TABLE `email_template` ADD COLUMN `is_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1;

        ");
    }

    public function down()
    {
        $this->dropColumn('email_template', 'is_active');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
