<?php

use yii\db\Migration;

/**
 * Class m180822_165517_5817_product_fill_info_debug
 */
class m180822_165517_5817_product_fill_info_debug extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('system_setting_group',['id'=>21, 'title'=>'product', 'created_at'=>'2018-08-22 16:57:11', 'updated_at'=>'2018-08-22 16:57:11', 'description'=>'Product settings']);
        $this->insert('system_setting', ['group_id'=>21, 'key'=>'productFillInfoLog', 'value'=>1, 'created_at'=>'2018-08-22 16:57:11', 'updated_at'=>'2018-08-22 16:57:11', 'description'=>'If not zerro, will write product fill info into @runtime/logs/product_fill_info.log.', 'is_active'=>1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('system_setting', ['key'=>'productFillInfoLog']);
        $this->delete('system_setting_group', 'id=21');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180820_113717_5817_file_to_cad cannot be reverted.\n";

        return false;
    }
    */
}
