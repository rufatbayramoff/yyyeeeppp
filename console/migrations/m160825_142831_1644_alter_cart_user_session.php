<?php

use yii\db\Migration;

class m160825_142831_1644_alter_cart_user_session extends Migration
{
    public function up()
    {
        $this->dropForeignKey('cart_user_id', 'cart');
        $this->dropColumn('cart', 'user_id');
        $this->addColumn('cart', 'user_session_id', 'integer(11) not null');
        $this->addForeignKey('cart_user_session', 'cart', 'user_session_id', 'user_session', 'id');
    }

    public function down()
    {
        echo 'Can`t revert migration m160825_142831_1644_alter_cart_user_session';
        return false;
    }
}
