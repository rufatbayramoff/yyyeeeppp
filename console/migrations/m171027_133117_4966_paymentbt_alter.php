<?php

use yii\db\Migration;

class m171027_133117_4966_paymentbt_alter extends Migration
{
    public function safeUp()
    {
        $sql = 'select * from paymentbt_transaction where updated_at="0000-00-00 00:00:00"';

        $rows = $this->db->createCommand($sql)->queryAll();
        foreach($rows as $k=>$v){
            $this->update('paymentbt_transaction', ['updated_at'=>$v['created_at']], ['id'=>$v['id']]);
        }
        $this->execute('ALTER TABLE `paymentbt_transaction` 
              CHANGE COLUMN `created_at` `created_at` DATETIME NOT NULL ,
              CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ;
          ');

    }

    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171027_133117_4966_paymentbt_alter cannot be reverted.\n";

        return false;
    }
    */
}
