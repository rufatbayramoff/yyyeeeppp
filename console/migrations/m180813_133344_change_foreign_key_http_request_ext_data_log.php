<?php

use yii\db\Migration;

/**
 * Class m180813_133344_change_foreign_key_http_request_ext_data_log
 */
class m180813_133344_change_foreign_key_http_request_ext_data_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE http_request_ext_data_log DROP FOREIGN KEY http_request_fk;
ALTER TABLE http_request_ext_data_log
ADD CONSTRAINT http_request_fk
FOREIGN KEY (request_id) REFERENCES http_request_log (id) ON DELETE CASCADE ON UPDATE CASCADE;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('ALTER TABLE http_request_ext_data_log DROP FOREIGN KEY http_request_fk;
ALTER TABLE http_request_ext_data_log
ADD CONSTRAINT http_request_fk
FOREIGN KEY (request_id) REFERENCES http_request_log (id);');
    }

}
