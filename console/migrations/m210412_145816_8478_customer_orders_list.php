<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m210412_145816_8478_customer_orders_list
 */
class m210412_145816_8478_customer_orders_list extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $ids = (new Query())->select('store_order_attemp.*')->from('store_order_attemp')
            ->leftJoin('preorder', 'store_order_attemp.preorder_id=preorder.id')
            ->where("(preorder.status = 'rejected' or preorder.status = 'deleted') and store_order_attemp.status='quote' and store_order_attemp.order_id is null")
            ->all();
        $this->update('store_order_attemp', ['status'=>'canceled'], ['id'=>$ids]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
