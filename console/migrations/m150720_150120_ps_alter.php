<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_150120_ps_alter extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $path = realpath(dirname(__DIR__) . '/../db/');
        $sql = file_get_contents($path . '/ps_data2.sql');
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function safeDown()
    {
        echo 'Cannot be reverted...';
    }
}
