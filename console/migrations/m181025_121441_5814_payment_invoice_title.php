<?php

use yii\db\Migration;

/**
 * Class m181025_121441_5814_payment_invoice_title
 */
class m181025_121441_5814_payment_invoice_title extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('payment_invoice_item', 'title', $this->string(255)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('payment_invoice_item', 'title', $this->string(250)->notNull());
    }
}
