<?php

use yii\db\Migration;

class m160701_105122_2329_add_file_owner extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('file', 'ownerClass', 'string');
        $this->addColumn('file', 'ownerField', 'string');
        $this->addColumn('file', 'is_public', 'bool');
    }

    public function safeDown()
    {
        $this->dropColumn('file', 'ownerClass');
        $this->dropColumn('file', 'ownerField');
        $this->dropColumn('file', 'is_public');
    }
}
