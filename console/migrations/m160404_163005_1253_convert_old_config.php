<?php

use yii\db\Migration;
use yii\helpers\Json;

class m160404_163005_1253_convert_old_config extends Migration
{
    public function up()
    {

        $oldConfgs = Yii::$app->db->createCommand("SELECT * FROM user_notify")->queryAll();

        $newConfigs = [];
        foreach($oldConfgs as $oldConfigItem)
        {
            $newConfigs[$oldConfigItem['user_id']][$oldConfigItem['notify_type']]['email'] = $oldConfigItem['email'] ? 'immediately' : 'never';
            $newConfigs[$oldConfigItem['user_id']][$oldConfigItem['notify_type']]['sms'] = $oldConfigItem['message'] ? 'immediately' : 'never';
        }

        foreach($newConfigs as $userId => $config)
        {
            Yii::$app->db->createCommand()->update('user', ['notify_config' => Json::encode($config)], ['id' => $userId])->execute();
        }
    }

    public function down()
    {
        $users = Yii::$app->db->createCommand("SELECT id, notify_config FROM user WHERE notify_config IS NOT NULL")->queryAll();

        foreach($users as $user)
        {
            $userId = $user['id'];

            $config = Json::decode($user['notify_config']);


            foreach($config as $notifyType => $nConfig)
            {
                Yii::$app->db->createCommand()->insert('user_notify', [
                    'user_id' => $userId,
                    'notify_type' => $notifyType,
                    'email' => isset($nConfig['email']) && $nConfig['email'] == 'never' ? 0 : 1,
                    'message' => isset($nConfig['sms']) && $nConfig['sms'] == 'never' ? 0 : 1,
                ])->execute();
            }
        }

        return true;
    }
}
