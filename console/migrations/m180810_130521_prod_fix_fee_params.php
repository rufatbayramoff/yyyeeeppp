<?php

use yii\db\Migration;

/**
 * Class m180810_130521_prod_fix_fee_params
 */
class m180810_130521_prod_fix_fee_params extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('system_setting', ['json' => dbexpr('\'{"A":2500,"B":100,"C":1.7,"minPercent":0,"maxPercent":25}\'')], "`key`='productServiceFeeParams'");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
