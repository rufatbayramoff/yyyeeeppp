<?php

use yii\db\Migration;
use yii\helpers\Json;

class m160311_114930_fix_free_delivery_value_type extends Migration
{
    public function up()
    {
        $data = Yii::$app->db
            ->createCommand("SELECT id, delivery_details FROM `ps_printer_delivery` WHERE `delivery_details` NOT LIKE ''")
            ->queryAll();

        foreach($data as $item)
        {
            if(($details = Json::decode($item['delivery_details'])) && isset($details['free_delivery']))
            {
                $details['free_delivery'] = floatval($details['free_delivery']);
                Yii::$app->db->createCommand()->update('ps_printer_delivery', ['delivery_details' => Json::encode($details)], ['id' => $item['id']])->execute();
            }
        }
    }

    public function down()
    {
        return true;
    }
}
