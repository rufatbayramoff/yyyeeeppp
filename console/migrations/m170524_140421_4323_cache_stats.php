<?php

use yii\db\Migration;

class m170524_140421_4323_cache_stats extends Migration
{
    public function up()
    {
        $this->execute(
            "
CREATE TABLE `statistics_cache` (
  `type` VARCHAR(255) NOT NULL,
  `cache_hit_count` INT(11) NOT NULL,
  `cache_miss_count` INT(11) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );
        $this->insert('statistics_cache', ['type' => 'filepageCache', 'cache_hit_count' => 0, 'cache_miss_count' => 0]);
    }

    public function down()
    {
        $this->dropTable('statistics_cache');
    }
}