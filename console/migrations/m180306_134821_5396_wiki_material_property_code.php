<?php

use yii\db\Migration;

/**
 * Class m180306_134821_5396_wiki_material_property_code
 */
class m180306_134821_5396_wiki_material_property_code extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('wiki_material_properties', 'code');
    }

    public function safeDown()
    {
        return false;
    }
}
