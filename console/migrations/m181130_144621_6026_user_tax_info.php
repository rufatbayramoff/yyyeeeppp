<?php

use yii\db\Migration;

/**
 * Class m181130_144621_6026_user_tax_info
 */
class m181130_144621_6026_user_tax_info extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `user_tax_info_history` CHANGE `comment` `comment` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');
        $this->execute('ALTER TABLE `user_tax_info` CHANGE `classification` `classification` ENUM(\'Individual\',\'Simple trust\',\'Disregarded entity\',\'Corporation\',\'C Corporation\',\'S Corporation\',\'LLC\',\'Partnership\',\'Trust/estate\',\'Other\',\'Private foundation\',\'Government\', \'Tax-exempt organization\') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
