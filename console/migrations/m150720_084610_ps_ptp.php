<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_084610_ps_ptp extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('{{%printer}}', 'website', 'string(250) NOT NULL');
        $this->alterColumn('{{%printer_to_property}}', 'is_editable', 'TINYINT(1) NOT NULL');

        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $path = realpath(dirname(__DIR__) . '/../db/');
        $sql = file_get_contents($path . '/ps_data.sql');
        $this->execute($sql);
        echo 'done';
        return 0;
    }

    public function safeDown()
    {
        $this->alterColumn('{{%printer}}', 'website', 'string(100) NOT NULL');
        $this->alterColumn('{{%printer_to_property}}', 'is_editable', 'BIT(1) NOT NULL');
    }
}
