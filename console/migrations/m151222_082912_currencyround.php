<?php

use yii\db\Schema;
use yii\db\Migration;

class m151222_082912_currencyround extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `payment_currency`  ADD COLUMN `round` TINYINT NOT NULL DEFAULT 2 AFTER `title_original`;");
    }

    public function down()
    {
        $this->dropColumn('payment_currency', 'round'); 
    }
}