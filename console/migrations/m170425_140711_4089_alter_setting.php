<?php

use yii\db\Migration;

class m170425_140711_4089_alter_setting extends Migration
{
    // This function to save _t translations in db
    public function translateFix()
    {
        $data = [
            "prototyping"    => [
                "title"       => _t('filepage.usage', "Prototyping"),
                "groups"      => ["Nylon", "Wood", "Polyester", "Flexible Plastic", "PLA", "ABS"],
                "image"       => "-",
                "description" => "-"
            ],
            "hq-prototyping" => [
                "title"       => _t('filepage.usage', "HD Prototyping"),
                "groups"      => ["Resin", "Nylon Powder", "Thermoplastic Powder"],
                "image"       => "-",
                "description" => "-"
            ],
            "jewelry"        => [
                "title"       => _t('filepage.usage', "Jewelry"),
                "groups"      => ["Jewerly Wax", "Resin"],
                "image"       => "-",
                "description" => "-"
            ],
            "dental"         => [
                "title"       => _t('filepage.usage', "Dental"),
                "groups"      => ["Resin"],
                "image"       => "-",
                "description" => "-"
            ],
            "colored"        => [
                "title"       => _t('filepage.usage', "Colored"),
                "groups"      => ["Paper", "Full-Color Sandstone", "Full color Plastic"],
                "image"       => "-",
                "description" => "-"
            ],
            "metal"          => [
                "title"       => _t('filepage.usage', "Metal"),
                "groups"      => ["Metals and Alloys"],
                "image"       => "-",
                "description" => "-"
            ]
        ];
    }

    public function up()
    {
        $data = [
            "prototyping"    => [
                "title"       => 'Prototyping',
                "groups"      => ["Nylon", "Wood", "Polyester", "Flexible Plastic", "PLA", "ABS"],
                "image"       => "-",
                "description" => "-"
            ],
            "hq-prototyping" => [
                "title"       => 'HD Prototyping',
                "groups"      => ["Resin", "Nylon Powder", "Thermoplastic Powder"],
                "image"       => "-",
                "description" => "-"
            ],
            "jewelry"        => [
                "title"       => 'Jewelry',
                "groups"      => ["Jewerly Wax", "Resin"],
                "image"       => "-",
                "description" => "-"
            ],
            "dental"         => [
                "title"       => 'Dental',
                "groups"      => ["Resin"],
                "image"       => "-",
                "description" => "-"
            ],
            "colored"        => [
                "title"       => 'Colored',
                "groups"      => ["Paper", "Full-Color Sandstone", "Full color Plastic"],
                "image"       => "-",
                "description" => "-"
            ],
            "metal"          => [
                "title"       => 'Metal',
                "groups"      => ["Metals and Alloys"],
                "image"       => "-",
                "description" => "-"
            ]
        ];
        $this->insert('system_setting',
            ['group_id' =>  '10',
                'key' => 'usage_groups',
                'value' => 'json',
                'created_at' => '2017-04-25 16:45:52',
                 'updated_at' =>  '2017-04-25 16:45:52',
                'created_user_id'=> '2',
                'updated_user_id' => '2',
                'description' => '',
                'json' => json_encode($data, JSON_PRETTY_PRINT),                
                'is_active' => '1'
            ]
        );
    }

    public function down()
    {
        $this->delete('system_setting', ['key'=>'usage_groups', 'group_id'=>10]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
