<?php

use yii\db\Migration;
use yii\helpers\Json;

class m170829_111521_4580_model3d_remove_files_count extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('model3d', 'files_count');
        $this->dropColumn('model3d_replica', 'files_count');
    }

    public function safeDown()
    {
        return false;
    }
}
