<?php

use yii\db\Migration;

/**
 * Class m210619_163910_8585_store_order_manual_ps_id
 */
class m210619_163910_8585_store_order_manual_ps_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `store_order` ADD `manual_setted_ps_id` INT(11) AFTER `source`');
        $this->execute('ALTER TABLE `store_order` ADD CONSTRAINT `fk_store_order_manual_setted_ps_id` FOREIGN KEY (`manual_setted_ps_id`) REFERENCES `ps`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
