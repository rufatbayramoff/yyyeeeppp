<?php

use yii\db\Migration;

/**
 * Class m181113_111331_5814_payment_account_add_type_payout
 */
class m181113_111331_5814_payment_account_add_type_payout extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('payment_account', 'type', "enum ('main', 'authorize', 'reserved', 'award', 'tax', 'invoice', 'manufactureAward', 'refundRequest', 'discount', 'easypost', 'refund', 'payout') default 'main' not null");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('payment_account', 'type', "enum ('main', 'authorize', 'reserved', 'award', 'tax', 'invoice', 'manufactureAward', 'refundRequest', 'discount', 'easypost', 'refund') default 'main' not null");
    }
}
