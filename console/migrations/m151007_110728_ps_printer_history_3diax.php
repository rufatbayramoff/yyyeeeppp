<?php

use yii\db\Schema;
use yii\db\Migration;

class m151007_110728_ps_printer_history_3diax extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("CREATE TABLE `ps_printer_3diax_tokens` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `order_id` int(11) NOT NULL,
            `model3d_file_id` int(11) NOT NULL,
            `token` varchar(45) NOT NULL,
            `status` varchar(20) NULL DEFAULT NULL,
            `details` varchar(100) DEFAULT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `check_status_at` timestamp NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `idx_order_3diax` (`order_id`),
            KEY `idx_model3d_3diax` (`model3d_file_id`),
            CONSTRAINT `fk_3diax_order`
              FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_3diax_model3d`
              FOREIGN KEY (`model3d_file_id`) REFERENCES `model3d_file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

        $this->execute("CREATE TABLE `ps_printer_file_status` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `order_id` int(11) NOT NULL,
            `model3d_file_id` int(11) NOT NULL,
            `file_id` int(11) NULL,
            `status` varchar(20) NULL DEFAULT NULL,
            `details` varchar(100) DEFAULT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `check_status_at` timestamp NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `idx_order_3diax_2` (`order_id`),
            KEY `idx_model3d_3diax_2` (`model3d_file_id`),
            KEY `idx_file_id_3diax` (`file_id`),
            UNIQUE KEY `idx_order_model3d` (`order_id`, `model3d_file_id`),
            CONSTRAINT `fk_3diax_order_2`
              FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_3diax_model3d_2`
              FOREIGN KEY (`model3d_file_id`) REFERENCES `model3d_file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_3diax_file_id`
              FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE ps_printer_3diax_tokens DROP FOREIGN KEY `fk_3diax_order`;');
        $this->execute('ALTER TABLE ps_printer_3diax_tokens DROP FOREIGN KEY `fk_3diax_model3d`;');
        $this->execute('ALTER TABLE ps_printer_3diax_tokens DROP KEY `idx_order_3diax`;');
        $this->execute('ALTER TABLE ps_printer_3diax_tokens DROP KEY `idx_model3d_3diax`;');
        $this->execute('DROP TABLE ps_printer_3diax_tokens;');

        $this->execute('ALTER TABLE ps_printer_file_status DROP FOREIGN KEY `fk_3diax_order_2`;');
        $this->execute('ALTER TABLE ps_printer_file_status DROP FOREIGN KEY `fk_3diax_model3d_2`;');
        $this->execute('ALTER TABLE ps_printer_file_status DROP FOREIGN KEY `fk_3diax_file_id`;');
        $this->execute('ALTER TABLE ps_printer_file_status DROP KEY `idx_order_3diax_2`;');
        $this->execute('ALTER TABLE ps_printer_file_status DROP KEY `idx_model3d_3diax_2`;');
        $this->execute('ALTER TABLE ps_printer_file_status DROP KEY `idx_file_id_3diax`;');
        $this->execute('ALTER TABLE ps_printer_file_status DROP KEY `idx_order_model3d`;');
        $this->execute('DROP TABLE ps_printer_file_status;');
    }
}
