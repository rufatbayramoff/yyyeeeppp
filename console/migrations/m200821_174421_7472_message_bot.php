<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200811_174421_7472_message_bot
 */
class m200821_174421_7472_message_bot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `content_filter_checked_messages` DROP FOREIGN KEY `fk_content_checked_message_id`; ALTER TABLE `content_filter_checked_messages` ADD CONSTRAINT `fk_content_checked_message_id` FOREIGN KEY (`message_id`) REFERENCES `msg_message`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->execute('ALTER TABLE `content_filter_checked_topics` DROP FOREIGN KEY `fk_content_checked_topic_id`; ALTER TABLE `content_filter_checked_topics` ADD CONSTRAINT `fk_content_checked_topic_id` FOREIGN KEY (`topic_id`) REFERENCES `msg_topic`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->delete('msg_message', 'topic_id=1 or topic_id=2');
        $topic = (new Query())
            ->select('*')
            ->from('msg_topic')->where('id=1 or id=2')->all();
        if (!$topic) {
            $this->insert('msg_topic', ['id' => 1, 'creator_id' => 1, 'created_at' => '2020-08-10 11:26:20', 'title' => 'Treatstock information']);
            $this->insert('msg_member', ['user_id' => 1, 'topic_id' => 1, 'folder_id' => 2, 'original_folder' => 2, 'have_unreaded_messages'=>0]);

            $this->insert('msg_topic', ['id' => 2, 'creator_id' => 1, 'created_at' => '2020-08-10 11:26:20', 'title' => 'Treatstock companies information']);
            $this->insert('msg_member', ['user_id' => 1, 'topic_id' => 2, 'folder_id' => 2, 'original_folder' => 2, 'have_unreaded_messages'=>0]);
        } else {
            $this->update('msg_member', [
                'folder_id' => 2, 'original_folder' => 2
            ], ['user_id' => 1, 'topic_id' => [1, 2]]);
            $this->update('msg_topic', ['title' => 'Treatstock information'], ['id' => 1]);
            $this->update('msg_topic', ['title' => 'Treatstock companies information'], ['id' => 2]);
            $this->delete('msg_member', '(topic_id=1 or topic_id=2) and user_id!=1');
        }
        $this->delete('msg_message', ['topic_id' => [1, 2]]);
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}