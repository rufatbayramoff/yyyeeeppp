<?php

use yii\db\Migration;

class m170605_082250_2593_website_url extends Migration
{
    public function up()
    {
        $this->execute("SET sql_mode = '';");
        $this->execute('ALTER TABLE `user_profile` CHANGE COLUMN `website` `website` VARCHAR(250) NULL DEFAULT NULL ;');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
