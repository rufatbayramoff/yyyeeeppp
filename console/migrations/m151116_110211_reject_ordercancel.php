<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_110211_reject_ordercancel extends Migration
{
     public function safeUp()
    {
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('user_cancel_order','Order is delayed','',1);");
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('user_cancel_order','Received item is broken','',1);");
        $this->execute("INSERT INTO `system_reject` (`group`,`title`,`description`,`is_active`)
            VALUES ('user_cancel_order','Other reason','',1);");

        $this->execute("INSERT INTO `email_template` (`code`,`language_id`,`title`,`description`,`updated_at`,`template_html`,`template_text`)
            VALUES ('psCancelOrder','en-US',
            'Order is cancelled', 'this email sent after User cancel order',NULL,
            '<p>Hello %username%,</p>\r\n<p>\r\nYour order has been cancelled!\r\n</p>\r\n<p>\r\nThank you,\r\nTS Team</p>',
            'Hello %username%,\r\n\r\nYour order has been cancelled!\r\n\r\nThank you,\r\nTS Team');");
        
        $this->execute("ALTER TABLE `store_order_history` CHANGE COLUMN `action_id` `action_id` CHAR(25) NOT NULL ; ");
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM `system_reject` WHERE `group` = 'user_cancel_order'");

        $this->execute("DELETE FROM `email_template` WHERE `code` = 'psCancelOrder' LIMIT 1;");
    }
}
