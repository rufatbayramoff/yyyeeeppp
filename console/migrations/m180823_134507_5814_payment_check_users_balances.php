<?php

use common\components\ArrayHelper;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180823_134507_5814_payment_check_users_balances
 */
class m180823_134507_5814_payment_check_users_balances extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $amounts = (new Query())
            ->select('payment_detail.user_id as user_id, SUM(amount) as amount')
            ->from('payment_detail')
            ->where("(`type` IN ('award_print', 'award_model', 'payout', 'tax', 'payment')) and user_id>999")
            ->groupBy('user_id')
            ->all();
        $amountsMap = ArrayHelper::map($amounts, 'user_id', 'amount');
        file_put_contents(Yii::getAlias('@runtime').'/checkUsersBalances2018.json', json_encode($amountsMap));
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
