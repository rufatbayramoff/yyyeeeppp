<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190614_144421_6522_preoprder_name
 */
class m190614_144421_6522_preoprder_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('preorder', 'name', 'varchar(255) NULL');
        $this->alterColumn('preorder', 'status', "ENUM('wait_confirm','new','accepted','rejected','draft') NOT NULL DEFAULT 'new'");
        $this->alterColumn('preorder', 'user_id', "int(11) NULL");
    }

    public function safeDown()
    {
        return true;
    }
}