<?php

use yii\db\Migration;

class m160513_093911_1860_change_max_sms_length extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_sms` CHANGE `message` `message` VARCHAR(1216)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `user_sms` CHANGE `message` `message` VARCHAR(155)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';");
        return true;
    }
}
