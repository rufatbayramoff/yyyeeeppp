<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200310_104621_7173_user_login_log
 */
class m200310_104621_7173_user_login_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `user_login_log` CHANGE `login_type` `login_type` ENUM(\'form\',\'osn\',\'auto-create\') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}