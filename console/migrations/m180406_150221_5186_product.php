<?php

use yii\db\Migration;

/**
 * Class m180406_150221_5186_product
 */
class m180406_150221_5186_product extends Migration
{
    public function safeUp()
    {
        $this->execute("
CREATE TABLE `product` (
  `uuid` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `cover_image_file_uuid` varchar(32) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `product_status` enum('draft','published_public','published_updated','published_directly','publish_pending','rejected','private') NOT NULL DEFAULT 'draft',
  `moderated_at` datetime NULL,
  `published_at` datetime NULL,
  `is_active` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  
  PRIMARY KEY (`uuid`),
  KEY `cover_image_file_uuid` (`cover_image_file_uuid`),
  KEY `category_id` (`category_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `fk_product_category_id` FOREIGN KEY (`category_id`) REFERENCES `store_category` (`id`),
  CONSTRAINT `fk_product_cover_image_file_uuid` FOREIGN KEY (`cover_image_file_uuid`) REFERENCES `file` (`uuid`),
  CONSTRAINT `fk_product_cover_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $this->execute("
CREATE TABLE `product_image` (
  `product_uuid` varchar(32) NOT NULL,
  `file_uuid` varchar(32) NOT NULL,
  KEY `product_uuid` (`product_uuid`),
  KEY `file_uuid` (`file_uuid`),
  CONSTRAINT `fk_product_images_file_uuid` FOREIGN KEY (`file_uuid`) REFERENCES `file` (`uuid`),
  CONSTRAINT `fk_product_images_product_uuid` FOREIGN KEY (`product_uuid`) REFERENCES `product` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $this->execute("
CREATE TABLE `product_site_tag` (
  `product_uuid` varchar(32) NOT NULL,
  `site_tag_id` int(11) NOT NULL,
  UNIQUE KEY `uniq_tag` (`product_uuid`,`site_tag_id`),
  KEY `product_uuid` (`product_uuid`),
  KEY `site_tag_id` (`site_tag_id`),
  CONSTRAINT `fk_product_site_tag_tag_id` FOREIGN KEY (`site_tag_id`) REFERENCES `site_tag` (`id`),
  CONSTRAINT `fk_product_site_tag_uuid` FOREIGN KEY (`product_uuid`) REFERENCES `product` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->dropTable('product_site_tag');
        $this->dropTable('product_images');
        $this->dropTable('product');
    }
}
