<?php

use yii\db\Migration;

/**
 * Class m180802_081013_create_table_home_page_category_block
 */
class m180802_081013_create_table_home_page_category_block extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('home_page_category_block', [
            'id'                  => $this->primaryKey(),
            'created_at'          => $this->dateTime()->notNull(),
            'is_active'           => $this->tinyInteger(1)->defaultValue(1)->notNull(),
            'product_category_id' => $this->integer()->null(),
            'title'               => $this->string(255)->null(),
            'url'                 => $this->string(255)->null(),
            'position'            => $this->integer()->notNull()->defaultValue(1)
        ]);

        $this->createIndex('index_hpcb_product_category_id', 'home_page_category_block', 'product_category_id');
        $this->addForeignKey('fk_hpcb_product_category_id', 'home_page_category_block', 'product_category_id', 'product_category', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('home_page_category_block');
    }
}
