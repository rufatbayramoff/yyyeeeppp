<?php

use yii\db\Migration;

/**
 * Class m210910_180553_8930_seo_page_autofill
 */
class m210910_180553_8930_seo_page_autofill extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE `seo_page_autofill` DROP FOREIGN KEY `fk_seo_page_autofill_1`; ALTER TABLE `seo_page_autofill` ADD CONSTRAINT `fk_seo_page_autofill_1` FOREIGN KEY (`seo_page_id`) REFERENCES `seo_page`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `seo_page_autofill` DROP FOREIGN KEY `fk_seo_page_autofill_2`; ALTER TABLE `seo_page_autofill` ADD CONSTRAINT `fk_seo_page_autofill_2` FOREIGN KEY (`template_id`) REFERENCES `seo_page_autofill_template`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `seo_page_autofill` DROP FOREIGN KEY `fk_seo_page_autofill_3`; ALTER TABLE `seo_page_autofill` ADD CONSTRAINT `fk_seo_page_autofill_3` FOREIGN KEY (`seo_page_intl_id`) REFERENCES `seo_page_intl`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

}
