<?php

use yii\db\Migration;

/**
 * Class m180518_063458_5560_company_profile
 */
class m180518_063458_5560_company_profile extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE IF NOT EXISTS `company_certification` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `title` varchar(45) NOT NULL,
          `file_id` int(11) NOT NULL,
          `certifier` varchar(45) DEFAULT NULL,
          `application` varchar(45) DEFAULT NULL,
          `created_at` datetime NOT NULL,
          `issue_date` date DEFAULT NULL,
          `expire_date` date DEFAULT NULL,
          `company_id` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `fk_company_certification_1_idx` (`company_id`),
          KEY `fk_company_certification_2_idx` (`file_id`),
          CONSTRAINT `fk_company_certification_1` FOREIGN KEY (`company_id`) REFERENCES `ps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
          CONSTRAINT `fk_company_certification_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute('ALTER TABLE `ps` 
            ADD COLUMN `ownership` VARCHAR(125) NULL AFTER `cover_file_id`,
            ADD COLUMN `total_employees` VARCHAR(45) NULL AFTER `ownership`,
            ADD COLUMN `year_established` YEAR NULL AFTER `total_employees`,
            ADD COLUMN `annual_turnover` INT NULL AFTER `year_established`;
            ');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
       $this->dropColumn('ps', 'ownership');
       $this->dropColumn('ps', 'total_employees');
       $this->dropColumn('ps', 'year_established');
       $this->dropColumn('ps', 'annual_turnover');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180518_063458_5560_company_profile cannot be reverted.\n";

        return false;
    }
    */
}
