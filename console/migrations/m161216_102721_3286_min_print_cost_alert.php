<?php

use yii\db\Migration;

class m161216_102721_3286_min_print_cost_alert extends Migration
{

    public function up()
    {
        $this->insert(
            'system_setting',
            [
                'group_id'        => 9,
                'key'             => 'moderation_minimum_print_cost_per_gram',
                'value'           => 0.3,
                'created_user_id' => 1,
                'description'     => 'Minimum cost per gram without printer moderation',
            ]
        );
        $this->insert(
            'system_setting',
            [
                'group_id'        => 9,
                'key'             => 'moderation_minimum_print_cost_per_order',
                'value'           => 5,
                'created_user_id' => 1,
                'description'     => 'Minimum cost per order without printer moderation',
            ]
        );
    }

    public function down()
    {
        $this->delete(
            'system_setting',
            [
                'group_id' => 9,
                'key'      => 'moderation_minimum_print_cost_per_gram',
            ]
        );
        $this->delete(
            'system_setting',
            [
                'group_id' => 9,
                'key'      => 'moderation_minimum_print_cost_per_order',
            ]
        );
    }
}
