<?php

use yii\db\Query;
use yii\db\Schema;
use yii\db\Migration;

class m161115_174732_3097_model3d_history extends Migration
{
    public function safeUp()
    {
        $this->addColumn('model3d_history', 'source', 'json null after action_id');
        $this->addColumn('model3d_history', 'result', 'json null after source');
    }

    public function safeDown()
    {
        $this->dropColumn('model3d_history', 'source');
        $this->dropColumn('model3d_history', 'result');
    }
}

