<?php

use yii\db\Migration;

/**
 * Class m181120_083243_5814_promocode_usage_migration
 */
class m181120_083243_5814_promocode_usage_migration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('promocode_usage', 'invoice_uuid', 'varchar(6) NOT NULL after promocode_id');

        $promocodesUsage = app()->db->createCommand('select * from promocode_usage')->queryAll();

        foreach ($promocodesUsage as $promocode) {
            $invoiceUuid = app()->db->createCommand('select primary_payment_invoice_uuid from store_order where id = :id', ['id' => $promocode['order_id']])->queryScalar();

            if ($invoiceUuid) {
                $this->update('promocode_usage', [
                    'invoice_uuid' => $invoiceUuid
                ], [
                    'id' => $promocode['id']
                ]);
                echo "Promocode order id: {$promocode['order_id']} => invoice uuid: {$invoiceUuid}\n";
            } else {
                echo "order not invoice\n";
                die;
            }
        }

        $this->dropForeignKey('fk_promocode_usage_3', 'promocode_usage');
        $this->dropIndex('fk_promocode_usage_3_idx', 'promocode_usage');

        $this->dropColumn('promocode_usage', 'order_id');


        $this->execute('CREATE UNIQUE INDEX idx_promocode_usage_payment_invoice_uuid ON promocode_usage (invoice_uuid)');

        $this->execute('ALTER TABLE promocode_usage
                                    ADD CONSTRAINT fk_promocode_usage_payment_invoice_uuid
                                    FOREIGN KEY (invoice_uuid) REFERENCES payment_invoice (uuid)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181120_083243_5814_promocode_usage_migration cannot be reverted.\n";
        return false;
    }
}
