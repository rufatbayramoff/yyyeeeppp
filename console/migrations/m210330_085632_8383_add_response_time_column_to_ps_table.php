<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%ps}}`.
 */
class m210330_085632_8383_add_response_time_column_to_ps_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%ps}}', 'response_time', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%ps}}', 'response_time');
    }
}
