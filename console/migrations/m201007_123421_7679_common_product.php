<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\components\UuidHelper;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m201007_123421_7679_common_product
 */
class m201007_123421_7679_common_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute('RENAME TABLE `product_bind` TO `product_common`');

        $this->addColumn('product_common', 'category_id', 'int(11) null');
        $this->addColumn('product_common', 'company_id', 'int(11) null');
        $this->addColumn('product_common', 'user_id', 'int(11) null');
        $this->addColumn('product_common', 'product_status', "enum('draft','published_public','published_updated','published_directly','publish_pending','rejected','private') NOT NULL DEFAULT 'draft'");
        $this->addColumn('product_common', 'created_at', 'datetime null');
        $this->addColumn('product_common', 'updated_at', 'datetime null');

        $this->addColumn('product_common', 'is_active', 'tinyint(1) not null');
        $this->addColumn('product_common', 'single_price', 'decimal(7,2) null');
        $this->addColumn('product_common', 'price_currency', 'char(5)');

        $this->addColumn('product_common', 'title', "varchar(255) not null");
        $this->addColumn('product_common', 'description', 'text null');

        $this->createIndex('product_common_company_indx', 'product_common', 'company_id');
        $this->createIndex('product_common_user_indx', 'product_common', 'user_id');
        $this->createIndex('product_common_category_indx', 'product_common', 'category_id');

        $this->addForeignKey('product_common_company_fk', 'product_common', 'company_id', 'ps', 'id');
        $this->addForeignKey('product_common_user_fk', 'product_common', 'user_id', 'user', 'id');
        $this->addForeignKey('product_common_category_fk', 'product_common', 'category_id', 'product_category', 'id');


        $this->addColumn('model3d', 'product_common_uid', 'char(6) null after uuid');
        $this->addColumn('product', 'product_common_uid', 'char(6) null after uuid');

        $this->createIndex('model3d_product_common_uid_indx', 'model3d', 'product_common_uid', true);
        $this->createIndex('product_product_common_uid_indx', 'product', 'product_common_uid', true);
        $this->addForeignKey('model3d_product_common_uid_fk', 'model3d', 'product_common_uid', 'product_common', 'uid');
        $this->addForeignKey('product_product_common_uid_fk', 'product', 'product_common_uid', 'product_common', 'uid');

        $productsCommon = (new Query())->select('*')->from('product_common')->where('uid is null')->all();
        echo "Generate product common uuid: ";
        $this->compact = true;
        $i             = 0;
        foreach ($productsCommon as $productCommon) {
            do {
                $notUpdated = true;
                try {
                    $productCommonUid = UuidHelper::generateRandCode(6);
                    $this->update('product_common', ['uid' => $productCommonUid], ['id' => $productCommon['id']]);
                    $notUpdated = false;
                } catch (Exception $exception) {
                    echo "x";
                }
            } while ($notUpdated);
            $i++;
            if ($i > 1000) {
                $i = 1;
                echo '.';
            }
        }
        $this->compact = false;
        echo " DONE\n";

        $this->execute('UPDATE product SET product_common_uid = (SELECT product_common.uid FROM product_common WHERE product_common.product_uuid = product.uuid)');
        $this->execute('UPDATE model3d SET product_common_uid = (SELECT product_common.uid FROM product_common WHERE product_common.model3d_uuid = model3d.uuid)');

        $this->update('product_common', ['price_currency' => 'usd']);


        $this->execute("             
            UPDATE product_common INNER JOIN product ON product.product_common_uid=product_common.uid
            SET
            product_common.category_id   =product.category_id,
            product_common.company_id    = product.company_id,
            product_common.user_id       = product.user_id,
            product_common.product_status= product.product_status,
            product_common.created_at    = product.created_at,
            product_common.updated_at    = product.updated_at,
            product_common.single_price  = product.single_price,
            product_common.is_active     = product.is_active,
            product_common.title         = product.title,
            product_common.description   = product.description
            WHERE product_common.product_uuid is not null
        ");

        $this->execute("
            UPDATE product_common INNER JOIN model3d ON model3d.product_common_uid=product_common.uid
            SET 
            product_common.category_id    = model3d.product_category_id,
            product_common.company_id     = model3d.company_id,
            product_common.user_id        = model3d.user_id,
            product_common.product_status = model3d.product_status,
            product_common.created_at     = model3d.created_at,
            product_common.updated_at     = model3d.updated_at,
            product_common.single_price   = model3d.price_per_produce,
            product_common.is_active      = model3d.is_active,
            product_common.title          = model3d.title,
            product_common.description    = model3d.description
            WHERE product_common.model3d_uuid is not null
        ");

        $this->alterColumn('product_common', 'created_at', 'datetime not null');
        $this->alterColumn('product_common', 'updated_at', 'datetime not null');
        $this->alterColumn('product_common', 'single_price', 'decimal(7,2) not null');
        $this->alterColumn('product_common', 'price_currency', 'char(5) not null');
        $this->dropForeignKey('model3d_product_common_uid_fk', 'model3d');
        $this->dropForeignKey('product_product_common_uid_fk', 'product');
        $this->alterColumn('product_common', 'uid', 'char(6) not null');
        $this->alterColumn('model3d', 'product_common_uid', 'char(6) not null');
        $this->alterColumn('product', 'product_common_uid', 'char(6) not null');
        $this->addForeignKey('model3d_product_common_uid_fk', 'model3d', 'product_common_uid', 'product_common', 'uid');
        $this->addForeignKey('product_product_common_uid_fk', 'product', 'product_common_uid', 'product_common', 'uid');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}
