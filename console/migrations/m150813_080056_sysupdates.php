<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_080056_sysupdates extends Migration
{
    public function up()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $sql = "ALTER TABLE `system_lang_message` 
            DROP FOREIGN KEY `fk_message_source_message`;
            ALTER TABLE `system_lang_message` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
            ALTER TABLE `system_lang_message` 
            ADD CONSTRAINT `fk_message_source_message`
              FOREIGN KEY (`id`)
              REFERENCES `system_lang_source` (`id`)
              ON DELETE CASCADE;";
        $this->execute($sql);
        
        $sql = "ALTER TABLE `store_units2category` 
            DROP FOREIGN KEY `fk_store_units2category_3`;
            ALTER TABLE `store_units2category` 
            ADD CONSTRAINT `fk_store_units2category_3`
              FOREIGN KEY (`unit_id`)
              REFERENCES `store_unit` (`id`)
              ON DELETE CASCADE
              ON UPDATE NO ACTION";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150813_080056_sysupdates cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
