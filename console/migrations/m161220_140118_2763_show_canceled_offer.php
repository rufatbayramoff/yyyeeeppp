<?php

use yii\db\Migration;

class m161220_140118_2763_show_canceled_offer extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `store_order_attemp` 
            ADD COLUMN `cancelled_at` TIMESTAMP NULL AFTER `is_offer`,
            ADD COLUMN `cancel_initiator` TINYINT UNSIGNED NULL AFTER `cancelled_at`;
        ");

        $this->execute("
            UPDATE store_order_attemp a, store_order_attemp_dates d 
            SET a.cancel_initiator = 0, a.cancelled_at = d.expired_at
            WHERE a.id = d.attemp_id AND a.status = 'cancelled' AND NOT ISNULL(d.expired_at);
        ");

        $this->execute("
            UPDATE store_order_attemp a
            SET a.cancel_initiator = 0, a.cancelled_at = a.created_at
            WHERE a.status = 'cancelled' AND ISNULL(a.cancelled_at);
        ");
    }

    public function down()
    {
        $this->dropColumn('store_order_attemp', 'cancelled_at');
        $this->dropColumn('store_order_attemp', 'cancel_initiator');
        return true;
    }
}
