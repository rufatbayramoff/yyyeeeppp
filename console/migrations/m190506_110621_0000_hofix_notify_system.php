<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m190506_110621_0000_hofix_notify_system
 */
class m190506_110621_0000_hofix_notify_system extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `notify_message` CHANGE `params` `params` JSON NULL DEFAULT NULL;');
    }

    public function safeDown()
    {

    }
}