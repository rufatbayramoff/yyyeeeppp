<?php

use yii\db\Migration;

class m160922_151734_2779_category_alter extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `store_category` ADD COLUMN `description` TEXT NULL AFTER `total_count`;');
        $this->execute('ALTER TABLE `store_category_intl`
	                  CHANGE COLUMN `description` `description` TEXT NULL DEFAULT NULL AFTER `title`;');

        $this->execute('ALTER TABLE `store_category` ADD COLUMN `full_description` MEDIUMTEXT NULL AFTER `description`;');
        $this->execute('ALTER TABLE `store_category_intl` ADD COLUMN `full_description` MEDIUMTEXT NULL AFTER `description`;');

    }

    public function down()
    {
        $this->dropColumn('store_category', 'description');
        $this->dropColumn('store_category', 'full_description');
        $this->dropColumn('store_category_intl', 'full_description');
    }
}
