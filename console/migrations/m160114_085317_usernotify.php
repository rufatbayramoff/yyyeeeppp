<?php

use yii\db\Schema;
use yii\db\Migration;

class m160114_085317_usernotify extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `user_notify` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `notify_type` varchar(45) NOT NULL,
            `email` tinyint(1) NOT NULL DEFAULT '0',
            `message` tinyint(1) NOT NULL DEFAULT '0',
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            UNIQUE KEY `index2` (`user_id`,`notify_type`),
            CONSTRAINT `fk_user_notify_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        
        $this->execute("CREATE TABLE `user_notify_history` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_notify_id` int(11) NOT NULL,
            `user_id` int(11) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `action_id` varchar(45) NOT NULL,
            `comment` tinytext,
            PRIMARY KEY (`id`),
            KEY `fk_user_notify_history_1_idx` (`user_notify_id`),
            KEY `fk_user_notify_history_2_idx` (`user_id`),
            CONSTRAINT `fk_user_notify_history_1` FOREIGN KEY (`user_notify_id`) REFERENCES `user_notify` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_user_notify_history_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
          ");
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
         $this->truncateTable('user_notify_history');
        $this->dropTable('user_notify_history');
        
        $this->truncateTable('user_notify');
        $this->dropTable('user_notify');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
