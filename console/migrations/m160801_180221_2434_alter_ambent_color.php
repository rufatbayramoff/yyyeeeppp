<?php

use yii\db\Migration;

class m160801_180221_2434_alter_ambent_color extends Migration
{
    public function up()
    {
        $this->update('printer_color', ['ambient' => '0'], 'render_color!="White" and render_color!="Red"');
    }

    public function down()
    {
    }
}
