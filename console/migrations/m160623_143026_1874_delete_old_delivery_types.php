<?php

use yii\db\Migration;

class m160623_143026_1874_delete_old_delivery_types extends Migration
{
    public function safeUp()
    {
        $this->execute("UPDATE ps_printer_delivery SET delivery_type_id = 2 WHERE delivery_type_id IN (3,4)");
        $this->execute("UPDATE ps_printer_delivery SET delivery_type_id = 5 WHERE delivery_type_id = 6");

        $this->delete('delivery_type', ['code' => 'expedited']);
        $this->delete('delivery_type', ['code' => 'one_day']);
        $this->delete('delivery_type', ['code' => 'intl_expedited']);
    }

    public function safeDown()
    {
        $this->execute("INSERT INTO `delivery_type`(
    `id`,
    `title`,
    `code`,
    `is_free_delivery`,
    `is_work_time`,
    `is_active`,
    `is_carrier`,
    `created_at`,
    `updated_at`
) VALUES (
    3,
    'Expedited shipping (1-3 business days)',
    'expedited',
    0,
    0,
    0,
    0,
    '2015-08-18 11:36:38',
    '2015-08-18 11:36:38'
 );
");
        $this->execute("INSERT INTO `delivery_type`(
    `id`,
    `title`,
    `code`,
    `is_free_delivery`,
    `is_work_time`,
    `is_active`,
    `is_carrier`,
    `created_at`,
    `updated_at`
) VALUES (
    4,
    'One day shipping (24 hours)',
    'one_day',
    0,
    0,
    0,
    0,
    '2015-08-18 11:37:09',
    '2015-08-18 11:37:09'
 );
");
        $this->execute("INSERT INTO `delivery_type`(
    `id`,
    `title`,
    `code`,
    `is_free_delivery`,
    `is_work_time`,
    `is_active`,
    `is_carrier`,
    `created_at`,
    `updated_at`
) VALUES (
    6,
    'International expedited shipping (3-7 business days)',
    'intl_expedited',
    0,
    0,
    0,
    0,
    '2015-09-10 08:25:17',
    '2015-09-10 08:25:17'
 );
");
        return true;
    }
}
