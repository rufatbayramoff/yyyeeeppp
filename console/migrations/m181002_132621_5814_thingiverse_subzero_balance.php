<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m181002_132621_5814_thingiverse_subzero_balance
 *
 * Set at least one transaction history record
 */
class m181002_132621_5814_thingiverse_subzero_balance extends Migration
{
    /**
     * @return string
     * @throws Exception
     */
    protected function generatePaymentDetailOperationUuid()
    {
        do {
            $operationUuid = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_detail_operation')->where('uuid="' . $operationUuid . '"')->one();
        } while ($checkUuid);
        return $operationUuid;
    }

    public function correctSummToZerro($user, $sum)
    {
        $lastOperation = (new Query())->select('payment_detail_operation.*')
            ->from('payment_detail')
            ->leftJoin('payment_detail_operation', 'payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid')
            ->where(['user_id'=>$user['id']])
            ->orderBy('payment_detail_operation.created_at desc')
            ->limit(1)
            ->one();

        $paymentDetailOperationUuid = $this->generatePaymentDetailOperationUuid();
        $paymentDetailOperation = ['uuid'=>$paymentDetailOperationUuid, 'payment_id'=>$lastOperation['payment_id'], 'created_at'=>\common\components\DateHelper::now()];
        $this->insert('payment_detail_operation', $paymentDetailOperation);
        $paymentDetail = [
            'user_id'                       => $user['id'],
            'payment_detail_operation_uuid' => $paymentDetailOperationUuid,
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => -$sum,
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Thingiverse users fix zerro balance',
            'type'                          => 'payment'
        ];

        $this->insert('payment_detail', $paymentDetail);
        $paymentDetail = [
            'user_id'                       => 110,
            'payment_detail_operation_uuid' => $paymentDetailOperationUuid,
            'updated_at'                    => \common\components\DateHelper::now(),
            'amount'                        => $sum,
            'original_currency'             => 'USD',
            'rate_id'                       => 1,
            'description'                   => 'Thingiverse users fix zerro balance',
            'type'                          => 'payment'
        ];
        $this->insert('payment_detail', $paymentDetail);
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $thingiverseUsers = (new Query())->select('*')
            ->from('user')
            ->leftJoin('thingiverse_user', 'thingiverse_user.user_id=user.id')
            ->where('thingiverse_user.thingiverse_id is not null')
            ->all();
        foreach ($thingiverseUsers as $user) {
            $sum = $this->db->createCommand('SELECT sum(amount) FROM `payment_detail` WHERE user_id='.$user['id'])->queryScalar();
            if ($sum>0) {
                $this->correctSummToZerro($user, $sum);
            } elseif ($sum<0) {
                // Skip if summ more zerro
                // echo 'Invalid summ: '.$user['username'].' Sum: '.$sum."\n";
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
