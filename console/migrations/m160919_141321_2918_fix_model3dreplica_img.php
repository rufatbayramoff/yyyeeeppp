<?php

use common\components\ArrayHelper;
use yii\db\Migration;
use yii\db\Query;

class m160919_141321_2918_fix_model3dreplica_img extends Migration
{
    public function up()
    {
        $model3dReplicas = (new Query())
            ->select('*')
            ->from('model3d_replica')->all();

        $model3dReplicaImgs = (new Query())
            ->select('*')
            ->from('model3d_replica_img')->all();

        $model3dReplicaParts = (new Query())
            ->select('*')
            ->from('model3d_replica_part')->all();

        foreach ($model3dReplicas as $model3d) {

            $coverFileId = $model3d['cover_file_id'];
            $model3dId = $model3d['id'];

            if ($imgExists = ArrayHelper::findAR($model3dReplicaImgs, ['file_id'=>$coverFileId, 'model3d_id'=>$model3dId])) {
                continue;
            }

            if ($partExists = ArrayHelper::findAR($model3dReplicaParts, ['file_id'=>$coverFileId, 'model3d_replica_id'=>$model3dId])) {
                continue;
            }

            $part = ArrayHelper::findAR($model3dReplicaParts, ['model3d_replica_id'=>$model3dId]);
            if (!$part) {
                // Invalid model3dreplica. model3dpart not extis
                continue;
            }
            $this->update(
                'model3d_replica',
                [
                    'cover_file_id' => $part['file_id']
                ],
                'id=' . $model3d['id']
            );
        }
    }

    public function down()
    {

    }
}
