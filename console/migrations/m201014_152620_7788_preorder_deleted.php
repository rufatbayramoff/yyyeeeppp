<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\components\UuidHelper;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m201014_152620_7788_preorder_deleted
 */
class m201014_152620_7788_preorder_deleted extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
       $this->execute("ALTER TABLE `preorder` CHANGE `status` `status` ENUM('wait_confirm','new','accepted','rejected','draft','deleted') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'new';");
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}
