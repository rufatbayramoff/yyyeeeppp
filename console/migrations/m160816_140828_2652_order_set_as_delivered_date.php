<?php

use yii\db\Migration;

class m160816_140828_2652_order_set_as_delivered_date extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ps_printer_order_dates` ADD COLUMN `delivered_at` datetime NULL;");
    }

    public function down()
    {
        $this->dropColumn('ps_printer_order_dates', 'delivered_at');
        return true;
    }
}
