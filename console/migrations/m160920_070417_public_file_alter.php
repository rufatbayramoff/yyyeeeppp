<?php

use common\interfaces\Model3dBaseImgInterface;
use yii\db\Migration;

class m160920_070417_public_file_alter extends Migration
{
    public function up()
    {
        $this->execute(sprintf('UPDATE file SET is_public=1 WHERE extension IN("%s") AND id>1', implode('","', Model3dBaseImgInterface::ALLOWED_FORMATS)));
    }

    public function down()
    {

    }
}
