<?php

use yii\db\Migration;

class m151228_075113_change_email_template_delivery_wait extends Migration
{
    public function up()
    {
        $this->execute("UPDATE `email_template` SET `template_html` = '<p>Hello %username%,</p>\r\n<p>\r\nWe are waiting for you, when you will send printed order #%orderId%!\r\n</p>\r\n<p>\r\nThank you,\r\nTS Team</p>', `template_text` = 'Hello %username%,\r\n\r\nWe are waiting for you, when you will send printed order #%orderId%!\r\n\r\nThank you,\r\nTS Team' WHERE `code` = 'deliveryWait';");
    }

    public function down()
    {
        return true;
    }
}
