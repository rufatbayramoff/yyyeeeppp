<?php

use yii\db\Migration;
use yii\db\Query;

class m201222_172933_8032_ts_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
CREATE TABLE `ts_internal_purchase` (
  `uid` varchar(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `primary_invoice_uuid` varchar(6) DEFAULT NULL,
  `type` enum('certification') NOT NULL,
  `status` enum('new','payed','canceled') NOT NULL DEFAULT 'new',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `primary_invoice` (`primary_invoice_uuid`),
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `ts_internal_purchase_primary_invoice` FOREIGN KEY (`primary_invoice_uuid`) REFERENCES `payment_invoice` (`uuid`),
  CONSTRAINT `ts_internal_purchase_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
        $this->execute("
CREATE TABLE `ts_internal_purchase_certification` (
  `ts_internal_purchase_uid` varchar(6) NOT NULL,
  `company_service_id` int(11) NOT NULL,
  `type` enum('verified') NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `expire_date` date NOT NULL,
  PRIMARY KEY (`ts_internal_purchase_uid`),
  KEY `company_service_id` (`company_service_id`),
  CONSTRAINT `ts_internal_purchase_certification_company_service` FOREIGN KEY (`company_service_id`) REFERENCES `company_service` (`id`),
  CONSTRAINT `ts_internal_purchase_certification_uid` FOREIGN KEY (`ts_internal_purchase_uid`) REFERENCES `ts_internal_purchase` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

        $this->addColumn('company_service', 'certification', "ENUM('progress', 'verified', 'expired', 'rejected', '') NOT NULL DEFAULT ''");
        $this->addColumn('company_service', 'certification_expire', "DATETIME");
        $this->addColumn('payment_invoice', 'ts_internal_purchase_uid', 'varchar(6) after instant_payment_uuid');

        $this->insert('system_setting', [
            'group_id' => 10,
            'key' => 'paymentForCertification',
            'value' => 100,
            'created_user_id' => 1,
            'description' => 'Payment for service certification',
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('DROP TABLE ts_internal_purchase_certification');
        $this->execute('DROP TABLE ts_internal_purchase');
        $this->dropColumn('company_service', 'certification');
        $this->dropColumn('payment_invoice', 'ts_internal_purchase_uid');
        $this->delete('system_setting', '`key`="paymentForCertification"');
    }
}
