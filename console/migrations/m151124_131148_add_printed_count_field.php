<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_131148_add_printed_count_field extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `store_unit` ADD `printed_count` INT  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `license_config`");
    }

    public function down()
    {
        echo "m151124_131148_add_printed_count_field cannot be reverted.\n";

        return false;
    }
}
