<?php

use yii\db\Migration;

class m160701_165828_1752_api_request extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `api_request` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `created_at` DATETIME NOT NULL,
            `finished_at` DATETIME NULL DEFAULT NULL,
            `status` CHAR(15) NOT NULL DEFAULT 'new',
            `in` VARCHAR(1500) NULL DEFAULT NULL,
            `out` VARCHAR(1500) NULL DEFAULT NULL,
            `api_key` CHAR(250) NOT NULL,
            PRIMARY KEY (`id`)
        )
        COMMENT='holds every api request, in order to debug, measure and stats analysis'
        ENGINE=InnoDB;
        ");
    }

    public function down()
    {
        $this->truncateTable('api_request');
        $this->dropTable('api_request');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
