<?php

use yii\db\Schema;
use yii\db\Migration;

class m160202_080653_model3d_remove_license extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `model3d` 
            DROP FOREIGN KEY `fk_model3d_2`;
            ALTER TABLE  `model3d` 
            DROP COLUMN `license_id`,
            DROP INDEX `fk_model3d_2_idx` ;
        ");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `model3d` ADD COLUMN `license_id` INT(11) NULL AFTER `default_material`;");
        $this->execute("ALTER TABLE `model3d` 
        ADD INDEX `fk_model3d_2_idx` (`license_id` ASC);
        ALTER TABLE `model3d` 
        ADD CONSTRAINT `fk_model3d_2`
          FOREIGN KEY (`license_id`)
          REFERENCES  `model3d_license` (`id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION;
        ");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
