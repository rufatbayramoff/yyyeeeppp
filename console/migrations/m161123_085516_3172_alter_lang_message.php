<?php

use yii\db\Migration;

class m161123_085516_3172_alter_lang_message extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `system_lang_message`
	ADD COLUMN `is_auto_translate` BIT NOT NULL DEFAULT b'0' AFTER `translation`;");
    }

    public function down()
    {
        $this->dropColumn('system_lang_message', 'is_auto_translate');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
