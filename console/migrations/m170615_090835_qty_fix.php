<?php

use yii\db\Migration;

class m170615_090835_qty_fix extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `store_order_item` 
            CHANGE COLUMN `qty` `qty` INT UNSIGNED NULL DEFAULT '1' ;
        ");
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
