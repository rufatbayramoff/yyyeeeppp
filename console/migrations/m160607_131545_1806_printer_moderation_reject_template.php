<?php

use yii\db\Migration;

class m160607_131545_1806_printer_moderation_reject_template extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`) VALUES (NULL, 'ps.printer.moderation.reject', 'service', 'en-US', 'Publishing of your %printerName% 3D printer on treatstock.com', NULL, NULL, 'Hello %psName%,\r\nYou recently attempted to publish your %printerName% 3D printer on treatstock.com. Unfortunately, the printer was not published due to the following reason: %rejectReason%.\r\nYou can make the changes and try to publish it again at any time.\r\n\r\nBest regards,\r\nTreatstock\r\n', NULL);");
    }

    public function down()
    {
        $this->delete('email_template', ['code' => 'ps.printer.moderation.reject']);
        return true;
    }
}
