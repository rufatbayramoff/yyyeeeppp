<?php

use yii\db\Migration;

/**
 * Class m180626_130459_5568_company_alter_inco_location
 */
class m180626_130459_5568_company_alter_inco_location extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            'ALTER TABLE `ps` 
                ADD COLUMN `location_id` INT NULL AFTER `logo_file_id`,
                ADD COLUMN `incoterms` VARCHAR(45) NULL AFTER `location_id`,
                ADD INDEX `fk_ps_3_idx` (`location_id` ASC);
                ALTER TABLE `ps` 
                ADD CONSTRAINT `fk_ps_3`
                  FOREIGN KEY (`location_id`)
                  REFERENCES `user_location` (`id`)
                  ON DELETE NO ACTION
                  ON UPDATE NO ACTION;'
        );
        $this->execute('ALTER TABLE `product_delivery` ADD UNIQUE INDEX `index3` (`express_delivery_counrty_id` ASC, `uuid` ASC)');

        $this->execute(
            'ALTER TABLE `product_delivery` 
            DROP FOREIGN KEY `fk_product_delivery_uuid`;
            ALTER TABLE `product_delivery` 
            CHANGE COLUMN `uuid` `product_uuid` VARCHAR(32) NOT NULL ,
            ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT AFTER `express_delivery_following_item`,
            DROP PRIMARY KEY,
            ADD PRIMARY KEY (`id`);
            ALTER TABLE `product_delivery` 
            ADD CONSTRAINT `fk_product_delivery_uuid`
              FOREIGN KEY (`product_uuid`)
              REFERENCES `product` (`uuid`);
            '
        );

        $this->execute(
            'ALTER TABLE `product_delivery` 
                DROP FOREIGN KEY `fk_product_delivery_country_id`;
                ALTER TABLE `product_delivery` 
                CHANGE COLUMN `express_delivery_counrty_id` `express_delivery_country_id` INT(11) NULL DEFAULT NULL ;
                ALTER TABLE `product_delivery` 
                ADD CONSTRAINT `fk_product_delivery_country_id`
                  FOREIGN KEY (`express_delivery_country_id`)
                  REFERENCES `geo_country` (`id`);'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute(
            'ALTER TABLE `ps`  DROP FOREIGN KEY `fk_ps_3`;
        ALTER TABLE `ps`  DROP COLUMN `incoterms`, DROP COLUMN `location_id`, DROP INDEX `fk_ps_3_idx` ;'
        );

        $this->dropIndex('index3', 'product_delivery');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180626_130459_5568_company_alter_inco_location cannot be reverted.\n";

        return false;
    }
    */
}
