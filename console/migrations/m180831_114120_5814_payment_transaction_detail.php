<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180831_114120_5814_payment_transaction_detail
 *
 * Every payment_transaction_history element can change balance and linked with payment_detail
 *
 */
class m180831_114120_5814_payment_transaction_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Fix transaction history
        $this->addColumn('payment_transaction_history', 'payment_detail_id', 'int(11) null after transaction_id');
        $this->createIndex('payment_transaction_history_indx', 'payment_transaction_history', 'payment_detail_id');
        $this->addForeignKey('payment_transaction_history_fk', 'payment_transaction_history', 'payment_detail_id', 'payment_detail', 'id');
        $this->execute('ALTER TABLE `payment_transaction_history` CHANGE `comment` `comment` JSON NOT NULL;');

        // First payment detail into transaction
        $this->addColumn('payment_transaction', 'first_payment_detail_id', 'int(11) null after transaction_id');
        $this->createIndex('payment_transaction_first_detail_indx', 'payment_transaction', 'first_payment_detail_id', false);
        $this->addForeignKey('payment_transaction_first_detail_fk', 'payment_transaction', 'first_payment_detail_id', 'payment_detail', 'id');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
