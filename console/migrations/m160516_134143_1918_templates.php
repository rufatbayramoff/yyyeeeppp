<?php

use yii\db\Migration;

class m160516_134143_1918_templates extends Migration
{
    public function up()
    {
        $this->execute("
        
        INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`)
VALUES
	(NULL, 'userNewOrderMessage', 'personalMessages', 'en-US', 'You received new message about order #%orderId%', NULL, NULL, 'Hello %username%!\nYou received new message about order #%orderId% from %messageUsername% at %messageDate%\n\nBest regards,\nTreatstock', 'Hello %username%!\nYou received new message about order #%orderId% from %messageUsername% at %messageDate%\n\nBest regards,\nTreatstock'),
	(NULL, 'userNewMessage', 'personalMessages', 'en-US', 'You have a new message on Treatstock', NULL, NULL, 'Hello %username%!\nYou have received a new message on treatstock.com from %messageUsername% at %messageDate%\nYou can view your message here: <a href=\"%messageLink%\">%messageLink%</a>\n\nBest regards,\nTreatstock', 'Hello %username%!\nYou have received a new message on treatstock.com from %messageUsername% at %messageDate%\nYou can view your message here: %messageLink%\n\nBest regards,\nTreatstock');

        
        ");

    }




    public function down()
    {
        echo "m160516_134143_1918_templates cannot be reverted.\n";
        return false;
    }
}
