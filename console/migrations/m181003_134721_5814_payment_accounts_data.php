<?php

use common\components\exceptions\AssertHelper;
use common\models\PaymentInvoice;
use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m181003_134721_5814_payment_accounts_data
 *
 * Set at least one transaction history record
 */
class m181003_134721_5814_payment_accounts_data extends Migration
{
    public function setTypeWithIds($users, $type)
    {
        $returnValue = [];
        foreach ($users as $userInfo) {
            $returnValue[] = [
                'id'      => $userInfo['user_id'],
                'user_id' => $userInfo['user_id'],
                'type'    => $type
            ];
        }
        return $returnValue;
    }

    public function setTypes($users, $type)
    {
        $returnValue = [];
        foreach ($users as $userInfo) {
            $returnValue[] = [
                'user_id' => $userInfo['user_id'],
                'type'    => $type
            ];
        }
        return $returnValue;
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $systemUserIds = (new Query())
            ->select('id as user_id')
            ->from('user')
            ->where('id>0 and id<1000')
            ->all();
        $userIds = (new Query())
            ->select('payment_detail.user_id')
            ->from('payment_detail')
            ->where('user_id>999')
            ->groupBy('user_id')
            ->all();

        // Create mail payments
        $this->batchInsert('payment_account', ['id', 'user_id', 'type'], $this->setTypeWithIds($systemUserIds, 'main'));
        $this->batchInsert('payment_account', ['id', 'user_id', 'type'], $this->setTypeWithIds($userIds, 'main'));

        $this->batchInsert('payment_account', ['user_id', 'type'], $this->setTypes($userIds, 'authorize'));
        $this->batchInsert('payment_account', ['user_id', 'type'], $this->setTypes($userIds, 'reserved'));
        $this->batchInsert('payment_account', ['user_id', 'type'], $this->setTypes($userIds, 'award'));
        $this->batchInsert('payment_account', ['user_id', 'type'], $this->setTypes($userIds, 'tax'));
        $this->batchInsert('payment_account', ['user_id', 'type'], $this->setTypes($userIds, 'invoice'));
        $this->batchInsert('payment_account', ['user_id', 'type'], $this->setTypes($userIds, 'manufactureAward'));
        $this->batchInsert('payment_account', ['user_id', 'type'], $this->setTypes($userIds, 'refundRequest'));



        // Discount
        $this->insert('payment_account', [
            'id'      => 115,
            'user_id' => '110',
            'type'    => 'discount'
        ]);
        // Easypost
        $this->insert('payment_account', [
            'id'      => 116,
            'user_id' => '110',
            'type'    => 'easypost'
        ]);
        // Anonim
        $this->insert('payment_account', [
            'id'      => 900,
            'user_id' => 998,
            'type'    => 'authorize'
        ]);
        $this->insert('payment_account', [
            'id'      => 901,
            'user_id' => 998,
            'type'    => 'reserved'
        ]);
        $this->insert('payment_account', [
            'id'      => 902,
            'user_id' => 998,
            'type'    => 'award'
        ]);
        $this->insert('payment_account', [
            'id'      => 903,
            'user_id' => 998,
            'type'    => 'tax'
        ]);
        $this->insert('payment_account', [
            'id'      => 904,
            'user_id' => 998,
            'type'    => 'invoice'
        ]);
        $this->execute("UPDATE `payment_detail` SET `payment_detail`.`payment_account_id`=(SELECT `id` FROM `payment_account` WHERE `payment_account`.`user_id`=`payment_detail`.`user_id` and `payment_account`.`type`='main')");
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
