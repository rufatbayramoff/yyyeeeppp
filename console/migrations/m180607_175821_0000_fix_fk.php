<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180607_175821_0000_fix_fk
 */
class m180607_175821_0000_fix_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_model3d_replica_img_original_id', 'model3d_replica_img', 'original_model3d_img_id', 'model3d_img', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_model3d_replica_img_original_id', 'model3d_replica_img');
    }
}
