<?php

use yii\db\Migration;

class m170803_122529_4222_ps_cnc_functional_avaliable extends Migration
{
    public function safeUp()
    {
        $this->addColumn('ps', 'is_cnc_allowed', 'TINYINT NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('ps', 'is_cnc_allowed');
        return true;
    }

}
