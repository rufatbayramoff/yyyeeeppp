<?php

use yii\db\Migration;

/**
 * Class m181004_143112_add_new_status_store_order_review
 */
class m181004_143112_add_new_status_store_order_review extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('store_order_review', 'verified_answer_status', "enum ('new', 'moderated', 'rejected') default 'moderated' not null after answer");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('store_order_review', 'verified_answer_status', "enum ('moderated', 'rejected') default 'moderated' not null after answer");
    }

}
