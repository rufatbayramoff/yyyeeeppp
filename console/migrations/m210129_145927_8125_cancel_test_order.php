<?php

use yii\db\Migration;
use yii\db\Query;

class m210129_145927_8125_cancel_test_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $psPrinters = (new Query())->select('*')->from('company_service')->where('ps_printer_id is not null and is_deleted=1')->all();
        foreach ($psPrinters as $machine) {
            $attempts = (new Query())->select('store_order_attemp.*')->from('store_order_attemp')->leftJoin('store_order', 'store_order.id=store_order_attemp.order_id')->where("store_order_attemp.status in ('new', 'printing', 'accepted') and store_order.user_id=1 and store_order_attemp.machine_id=" . $machine['id'])->all();
            foreach ($attempts as $attemp) {
                $this->update('store_order_attemp', ['status' => 'canceled'], ['id' => $attemp['id']]);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
