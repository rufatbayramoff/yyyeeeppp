<?php

use yii\db\Migration;

class m160510_114235_1796_new_templates extends Migration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO `email_template` (`id`, `code`, `group`, `language_id`, `title`, `description`, `updated_at`, `template_html`, `template_text`) VALUES (21, 'declineTaxInfo', 'system', 'en-US', 'Tax information declined.', 'On moderator decline PS tax info', NULL, 'Hello!\nYour tax information has been declined. The reason: %reason%.\nYou can change or update your tax form here: %taxLink%.\n\nBest regards,\nTreatstock', 'Hello!\nYour tax information has been declined. The reason: %reason%.\nYou can change or update your tax form here: %taxLink%.\n\nBest regards,\nTreatstock');");

    }

    public function safeDown()
    {
        $this->delete('email_template', ['code' => 'declineTaxInfo']);

        return true;
    }
}
