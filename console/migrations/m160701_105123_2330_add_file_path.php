<?php

use yii\db\Migration;

class m160701_105123_2330_add_file_path extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('file', 'path_version', 'enum("v1", "v2") default "v1" NOT NULL');
        $this->createIndex('pathVersionIndex', 'file', 'path_version');
    }

    public function safeDown()
    {
        $this->dropColumn('file', 'path_version');
    }
}
