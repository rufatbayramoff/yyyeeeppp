<?php

use yii\db\Migration;

/**
 * Class m180810_141706_add_new_field_to_http_request_ext_data_log
 */
class m180810_141706_add_new_field_to_http_request_ext_data_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('http_request_ext_data_log', 'type', $this->string(50));
        $this->addColumn('http_request_ext_data_log', 'object_type', $this->string(50));
        $this->addColumn('http_request_ext_data_log', 'object_id', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('http_request_ext_data_log', 'type');
        $this->dropColumn('http_request_ext_data_log', 'object_type');
        $this->dropColumn('http_request_ext_data_log', 'object_id');
    }
}
