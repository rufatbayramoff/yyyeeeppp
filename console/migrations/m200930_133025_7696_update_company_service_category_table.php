<?php

use yii\db\Migration;

/**
 * Class m200930_133025_7696_update_company_service_category_table
 */
class m200930_133025_7696_update_company_service_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("INSERT IGNORE INTO company_service_category (id,lft,rgt,`depth`,code,slug,title,description,`position`,is_visible,is_active) VALUES 
	 (1,1,38,1,'root','company-services','Root','Treatstock has selected the best service providers from all over the world. Our strict vetting process ensures we only work with reliable designers and manufacturers that demonstrate a client centric and professional approach to every project. Outsource your custom project on our platform with options to pick it up or get it delivered to your doorstep.',1,1,1),
	 (2,2,3,2,'manufacturing-services','manufacturing-services','Manufacturing Services',NULL,1,1,1),
	 (3,4,5,2,'designing-services','designing-services','3D Design','Do you need a custom 3D model? Check out online 3D modeling, CAD and design services on demand. In this list, you can find a vendor for freelance engineering, 3D design services or other type of modeling work. Explore designer’s work samples and rates and submit your project through Get a Quote function to get in touch with them. Most of the vendors from the list are experienced in prototyping and modeling for special applications, so we hope you can find a suitable professional for your goals.',1,1,1),
	 (4,6,7,2,'cnc-manufacturing','cnc-manufacturing','CNC Machining','',1,1,1),
	 (5,8,9,2,'injection-molding','injection-molding','Injection Molding','',1,1,1),
	 (6,10,11,2,'signage-services','signage-services','Signage','',6,1,1),
	 (7,12,13,2,'electronics-service','electronics-manufacturing-service','Electronics Manufacturing Service','',8,1,1),
	 (8,14,15,2,'3d-scanning','3d-scanning','3D Scanning','',7,1,1),
	 (9,16,17,2,'painting','painting','Painting','',9,1,1),
	 (10,18,19,2,'other','other','Other','',20,1,1)");

        $this->execute("INSERT IGNORE INTO company_service_category (id,lft,rgt,`depth`,code,slug,title,description,`position`,is_visible,is_active) VALUES 
	  (11,20,21,2,'cutting','cutting','Cutting','Looking for laser cut wedding invitations or metal signs? Find a reliable laser cutting service near you and worldwide. Click on the service to read the full description of their service capabilities, rates and production times. You can check out pictures of their projects on that page as well.',3,1,1),
	 (12,22,23,2,'Urethane-Casting','Urethane-Casting','Urethane Casting','',9,1,1),
	 (13,24,25,2,'Sheet-Fabrication','Sheet-Fabrication','Sheet Fabrication','',11,1,1),
	 (14,26,27,2,'Metal-Casting','Metal-Casting','Metal Casting','',12,1,1),
	 (15,28,29,2,'Vacuum-Forming','Vacuum-Forming','Vacuum Forming','',13,1,1),
	 (16,30,31,2,'Mold-Making','Mold-Making','Mold Making','',14,1,1),
	 (17,32,33,2,'Metal-Stamping','Metal-Stamping','Metal Stamping','',15,1,1),
	 (18,34,35,2,'CNC-turning','CNC-turning','CNC Turning','',16,1,1),
	 (19,36,37,2,'3d-printing-services','3d-printing-services','3D Printing','',21,1,1)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
