<?php

use yii\db\Migration;

/**
 * Class m180623_115624_5648_product_block_alter
 */
class m180623_115624_5648_product_block_alter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `product_block` 
            ADD COLUMN `is_visible` TINYINT(1) NOT NULL DEFAULT 1 AFTER `product_uuid`,
            ADD COLUMN `position` TINYINT(3) NOT NULL DEFAULT 1 AFTER `is_visible`;
        ');
        $this->execute('ALTER TABLE `product_certification` CHANGE COLUMN `issue_date` `issue_date` DATE NOT NULL ;');}

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_block', 'is_visible');
        $this->dropColumn('product_block', 'position');
    }

}
