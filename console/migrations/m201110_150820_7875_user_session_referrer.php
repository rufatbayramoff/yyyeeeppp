<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.19
 * Time: 10:39
 */

use common\components\UuidHelper;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m201110_150820_7875_user_session_referrer
 */
class m201110_150820_7875_user_session_referrer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('user_session', 'referrer', 'varchar(1024)');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {

    }
}
