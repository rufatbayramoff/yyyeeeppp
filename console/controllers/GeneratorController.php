<?php

namespace console\controllers;

use common\components\ConsoleController;
use common\models\User;
use common\models\UserAdmin;
use frontend\models\user\FrontUser;
use Yii;
use yii\helpers\Console;
use yii\helpers\Inflector;

class GeneratorController extends ConsoleController
{
    public $modelNamespace = 'common\\models';
    /**
     * @var string namespace path for crud controller
     */
    public $crudControllerNamespace = 'backend\\controllers\\crud';
    /**
     * @var string namespace path for crud search models
     */
    public $crudSearchModelNamespace = 'backend\\models\\search';
    /**
     * @var string namespace path for crud views
     */
    public $crudViewPath = '@backend/views/crud';

    public $createModels = true;

    public function options($id)
    {
        return array_merge(
            parent::options($id),
            [
                'createModels'
            ]
        );
    }

    /**
     *
     * @param string $table
     * @return int
     * @throws \yii\console\Exception
     */
    public function actionCrud($table = '')
    {
        $this->stdout("Generating CRUD in backend\n");
        $config       = $this->getYiiConfiguration();
        $config['id'] = 'tmp-console';
        $connection   = Yii::$app->db;
        /** @var \yii\db\mysql\Schema * */
        $dbSchema    = $connection->schema;
        $tables      = $dbSchema->getTableNames();
        $totalTables = count($tables);

        Console::startProgress(0, $totalTables);
        $app = \Yii::$app;

        foreach ($tables as $k => $tbl) {
            if (!empty($table) && $table != $tbl) {
                continue;
            }
            if (mb_strpos($tbl, 'off_') !== false) {
                continue;
            }
            $modelClass = Inflector::id2camel($tbl, '_');
            $name       = Inflector::camelize($tbl);
            if (!class_exists($this->modelNamespace . '\\' . $name) && empty($table)) {
                continue;
            }

            $tblDir = str_replace("_", '-', $tbl);
            echo $tbl . ' class[' . $modelClass . ']' . PHP_EOL;
            $params = [
                'interactive'         => 0,
                'overwrite'           => !empty($table),
                'modelClass'          => $this->modelNamespace . '\\' . $name,
                'searchModelClass'    => $this->crudSearchModelNamespace . '\\' . $name . 'Search',
                'controllerClass'     => $this->crudControllerNamespace . '\\' . $name . 'Controller',
                'viewPath'            => $this->crudViewPath . DIRECTORY_SEPARATOR . $tblDir,
                'enableI18N'          => false,
                'messageCategory'     => 'app',
                'baseControllerClass' => 'backend\components\AdminController'
            ];
            $temp   = new \yii\console\Application($config);
            $temp->runAction('gii/crud', $params);
            unset($temp);
            Console::updateProgress(($k + 1), $totalTables);
        }
        \Yii::$app = $app;
        Console::endProgress();
        return 0;
    }

    public function actionModels($table = '')
    {
        $this->println('Generating base db models (ActiveRecord)');

        if (!$this->createModels) {
            return 0;
        }

        $tables = $table ? [$table] : array_filter(\Yii::$app->db->schema->getTableNames(), function ($tableName) {
            return !(mb_strpos($tableName, 'auth_') === 0 || mb_strpos($tableName, 'off_') === 0 || $tableName === 'migration');
        });

        $totalTables = count($tables);

        Console::startProgress(0, $totalTables);

        foreach ($tables as $index => $tableName) {
            $modelClass = Inflector::id2camel($tableName, '_');
            $this->println("{$tableName} class[{$modelClass}]");
            $this->run('/gii/model-base', [
                'tableName'   => $tableName,
                'modelClass'  => $modelClass,
                'interactive' => 0,
                'overwrite'   => true,
            ]);
            if (!file_exists(Yii::getAlias("@common/models/{$modelClass}.php"))) {
                $this->run('/gii/model-work', [
                    'modelClass'  => $modelClass,
                    'interactive' => 0,
                    'overwrite'   => false,
                ]);
            }
            Console::updateProgress($index + 1, $totalTables);
        }

        Console::endProgress();
        return 0;
    }

    public function actionSetPassword($username, $password)
    {
        $obj = FrontUser::tryFindByPk(['username' => $username]);
        $obj->setPassword($password);
        $obj->safeSave();
        echo 'Done';
    }

    public function actionCreateAdmin($password)
    {
        // create admin
        $obj           = new \common\models\UserAdmin();
        $obj->username = 'admin';
        $data          = [
            'username'   => $obj->username,
            'auth_key'   => Yii::$app->getSecurity()->generateRandomString(),
            'email'      => $obj->username . '@tsdev.work',
            'status'     => 10,
            'created_at' => time(),
            'updated_at' => time()
        ];
        $obj->load(['data' => $data], 'data');
        $obj->setPassword($password);
        if ($obj->save()) {
            echo 'admin created' . PHP_EOL;
        } else {
            echo \yii\helpers\Html::errorSummary($obj, []);
        }
        $userId = $obj->id;

        $siteUser = \common\models\User::findByPk($userId);
        if (!$siteUser) {
            \common\models\User::addRecord([
                "id"            => $userId,
                "username"      => $obj->username,
                "auth_key"      => "-",
                'password_hash' => '-',
                'email'         => $obj->username . '@tsdev.work',
                'status'        => 10,
                'created_at'    => 0,
                'updated_at'    => 0,
                'lastlogin_at'  => 0,
                'trustlevel'    => 'high'
            ]);
        }
        return 0;
    }

    public function actionModule()
    {
        echo 'module';
    }

    private function getYiiConfiguration()
    {
        if (isset($GLOBALS['config'])) {
            $config = $GLOBALS['config'];
        } else {
            $config = \yii\helpers\ArrayHelper::merge(
                require(\Yii::getAlias('@app') . '/../common/config/main.php'),
                (is_file(\Yii::getAlias('@app') . '/../common/config/main-local.php')) ?
                    require(\Yii::getAlias('@app') . '/../common/config/main-local.php')
                    : [],
                require(\Yii::getAlias('@app') . '/../console/config/main.php'),
                (is_file(\Yii::getAlias('@app') . '/../console/config/main-local.php')) ?
                    require(\Yii::getAlias('@app') . '/../console/config/main-local.php')
                    : []
            );
        }
        return $config;
    }
}

