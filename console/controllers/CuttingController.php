<?php

namespace console\controllers;

use common\components\ConsoleController;
use common\models\File;
use common\models\FileJob;
use common\modules\cutting\services\CuttingConverter;
use console\jobs\QueueGateway;

class CuttingController extends ConsoleController
{

    /** @var CuttingConverter */
    protected $cuttingConverter;

    /**
     * @param CuttingConverter $cuttingConverter
     */
    public function injectDependencies(CuttingConverter $cuttingConverter)
    {
        $this->cuttingConverter = $cuttingConverter;
    }

    /**
     * Convert cutting job
     *
     * @param $jobFileId
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     * @internal param $id
     */
    public function actionConvertCuttingJob($jobFileId)
    {
        $fileJob = FileJob::find()->withoutStaticCache()->where(['id'=>$jobFileId])->tryOne();
        $args = json_decode($fileJob->args, true);
        $file = File::find()->withoutStaticCache()->id($args['file_id'])->tryOne();
        $cuttingPackFile = $file->cuttingPackFile;
        $this->cuttingConverter->convert($cuttingPackFile, true);
        $fileJob->status = QueueGateway::JOB_STATUS_COMPLETED;
        $fileJob->save(false);
    }
}
