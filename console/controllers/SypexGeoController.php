<?php

/**
 * Class for Sypex Geo update
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */

namespace console\controllers;;
use Yii;
use yii\console\Controller;
use lib\geo\GeoException;
use yii\helpers\VarDumper;

class SypexGeoController extends Controller
{
    public function actionUpdate()
    {
        try {
            $path = $this->getPath();

            if(!is_writable($path)) {
                throw new GeoException("Directory must be writable Dir=" . $path);
            }

            $zip_file_name = $path . 'SypexGeoData.zip';
            file_put_contents($zip_file_name, fopen('https://sypexgeo.net/files/SxGeoCity_utf8.zip', 'r'));

            if(!file_exists($zip_file_name)) {
                throw new GeoException("File doesn't exists File=" . $zip_file_name);
            }

            $result = $this->unzip($path, $zip_file_name);

            if(!$result) {
                throw new GeoException('Unzip file failed File=' . $zip_file_name);
            }

            $unzipped_file = $path . 'SxGeoCity.dat';

            if(!file_exists($unzipped_file)) {
                throw new GeoException("File doesn't exists File=" . $unzipped_file);
            }

            unlink($zip_file_name);
            rename($unzipped_file, $path . 'SxGeoCityMax.dat');
            echo "\nSuccessfully updated " . date("Y-m-d H:i:s") . "\n";
            
            $this->actionUpdateMaxmind();
        }
        catch(GeoException $e)
        {
            logException($e, 'geo');
        }
    }
    
    public function actionUpdateMaxmind()
    {
        die('Maxmind database is closed url now');
        $path = Yii::getAlias('@vendor') . '/geoip2/geoip2/maxmind-db/';
        if(!is_writable($path)) {
            throw new GeoException("Directory must be writable Dir=" . $path);
        }
        
        $zipFile = $path . 'GeoLite2-City-NewVersion.mmdb.gz';
        file_put_contents($zipFile, fopen('http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz', 'r'));

        if(!file_exists($zipFile)) {
            throw new GeoException("File doesn't exists File=" . $zipFile);
        }

        #$result = $this->unzip($path, $zipFile);
        system("gunzip -f $zipFile");
        $this->verifyMaxmind();
        echo "\nSuccessfully updated maxmind " . date("Y-m-d H:i:s") . "\n";
        //unlink($zipFile);        
    }

    /**
     * get information about IP
     * 
     * @param $ip
     */
    public function actionCheckIp($domain, $ip)
    {
        try{
            $result = \Yii::$app->geo->getLocationByDomainOrIp($domain, $ip, false);
            $this->stdout(VarDumper::dumpAsString($result, 20, false));
        }catch(\Exception $e){
            $this->stderr("\n");
            $this->stderr($e->getMessage(), \yii\helpers\Console::BG_RED);
            $this->stderr("\n\n");
        }
    }
    
    public function verifyMaxmind()
    {
        $path = Yii::getAlias('@vendor') . '/geoip2/geoip2/maxmind-db/';
        $newDb = $path . 'GeoLite2-City-NewVersion.mmdb';
        
        $reader = new \GeoIp2\Database\Reader($newDb);

        // Replace "city" with the appropriate method for your database, e.g.,
        // "country".
        $record = $reader->city('128.101.101.101');

        print($record->country->isoCode . "\n"); // 'US'
        print($record->country->name . "\n"); // 'United States' 

        if($record->city->name=='Minneapolis'){
            $updatedDb = str_replace("GeoLite2-City-NewVersion.mmdb", "GeoLite2-City.mmdb", $newDb);
            rename($newDb, $updatedDb);
        }
    }

    private function getPath()
    {
        $ds = DIRECTORY_SEPARATOR;
        $vendor_path = Yii::getAlias('@vendor');

        return $vendor_path . $ds . 'jisoft' . $ds . 'yii2-sypexgeo' . $ds;
    }

    private function unzip($path, $zip_file_name)
    {
        $zip = new \ZipArchive;
        $zip->open($zip_file_name);

        $result = $zip->extractTo($path);
        $zip->close();

        if($result) {
            return true;
        }
        return false;
    }
}
