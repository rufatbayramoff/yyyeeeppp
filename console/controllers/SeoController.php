<?php
/**
 * Date: 21.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace console\controllers;


use common\components\ConsoleController;
use common\models\Product;
use common\models\SeoPage;
use common\models\SeoPageAutofill;
use common\models\SeoPageAutofillTemplate;
use common\modules\seo\components\SeoPageRepository;
use common\modules\seo\services\SeoAutofillService;
use yii\helpers\Url;
use yii\helpers\VarDumper;

class SeoController extends ConsoleController
{

    public function actionAutofill($type = '')
    {
        $product    = Product::find()->one();
        $seoService = new SeoAutofillService();
        $types      = [];
        $skip       = false;
        if ($type) {
            $skip  = true;
            $types = [$type];
        }
        $result = $seoService->autofillAll($skip, $types);
        $this->stdout(var_dump($result));

    }

    public function actionAutofillModel3d()
    {
        $seoService = new SeoAutofillService();
        $result = $seoService->autofillModel3d();
        $this->stdout(var_dump($result));
    }

    public function actionAutofillProductsCatalogTags()
    {
        $seoService = new SeoAutofillService();
        $result = $seoService->autofillProductsCatalogTags();
        $this->stdout(var_dump($result));
    }

    public function actionAutofillWikiMachines()
    {
        $seoService = new SeoAutofillService();
        $result = $seoService->autofillWikiMachines();
        $this->stdout(var_dump($result));
    }

    public function actionAutofillCompanyPage()
    {
        $seoService = new SeoAutofillService();
        $result = $seoService->autofillAll(true, [SeoAutofillService::TYPE_COMPANYPAGE]);
        $this->stdout(var_dump($result));
    }

    public function actionAutofillCompanyProducts()
    {
        $seoService = new SeoAutofillService();
        $result = $seoService->autofillAll(true, [SeoAutofillService::TYPE_COMPANYPRODUCTS]);
        $this->stdout(var_dump($result));
    }

    public function actionAutofillCompanyCnc()
    {
        $seoService = new SeoAutofillService();
        $result = $seoService->autofillAll(true, [SeoAutofillService::TYPE_COMPANYCNC]);
        $this->stdout(var_dump($result));
    }

    public function actionAutofillCatalog()
    {
        $seoService = new SeoAutofillService();
        $result     = $seoService->autofillCatalog();
        $this->stdout(var_dump($result));
    }

    public function actionAutofillProducts()
    {
        $seoService = new SeoAutofillService();
        $result     = $seoService->autofillProducts();
        $this->stdout(var_dump($result));
    }

    public function actionAutofillServices()
    {
        $seoService = new SeoAutofillService();
        $result     = $seoService->autofillServices();
        $this->stdout(var_dump($result));
    }

    public function actionAutofillClear()
    {
        $sp      = SeoPageAutofill::find()->all();
        $total   = 0;
        $skipped = 0;
        foreach ($sp as $autoFill) {
            /**
             * @var $autoFill SeoPageAutofill
             */
            $seoPage = $autoFill->seoPage;

            if (!$seoPage) {
                continue;
            }

            if ($seoPage->is_locked) {
                $skipped++;
                continue;
            }

            $seoPage->delete();
            $total++;
        }
        $this->println(sprintf('Total deleted: %d Skipped: %d', $total, $skipped));
    }
}