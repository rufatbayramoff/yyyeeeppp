<?php

namespace console\controllers;

use common\components\ConsoleController;
use common\components\FileDirHelper;
use common\interfaces\FileBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\File;
use common\models\FileJob;
use common\models\Model3d;
use common\models\Model3dPart;
use common\models\PrinterColor;
use common\modules\model3dRender\models\RenderFileParams;
use console\jobs\QueueGateway;
use Yii;
use yii\console\ExitCode;

class RenderController extends ConsoleController
{
    /**
     * To do render job
     *
     * @param $jobFileId
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     * @internal param $id
     */
    public function actionRenderJob($jobFileId)
    {
        $fileJob = FileJob::find()->withoutStaticCache()->where(['id'=>$jobFileId])->tryOne();
        $args = json_decode($fileJob->args, true);
        $file = File::find()->withoutStaticCache()->id($args['file_id'])->tryOne();
        /** @var RenderFileParams $renderFilePathParams */
        $renderFilePathParams = Yii::createObject(
            [
                'class' => RenderFileParams::class,
            ]
        );
        $model3dPart = null; // Not exists for cad files from products
        if ($file->model3dParts) {
            $parts = $file->model3dParts;
            $model3dPart = reset($parts);
            if (!$model3dPart) {
                $model3dParts = $file->model3dReplicaParts;
                $model3dPart = reset($model3dParts);
            }
            /** @var RenderFileParams $renderFilePathParams */
            $renderFilePathParams->pb = $model3dPart->model3d->getCadFlag();
        }

        $result = $this->doModel3dFileRender($file, $renderFilePathParams);
        $renderFilePathParams = clone $renderFilePathParams;
        $renderFilePathParams->ambient = 40;
        $resultAmbient = $this->doModel3dFileRender($file, $renderFilePathParams);
        if ($model3dPart && $model3dPart->isMulticolorFormat()) {
            $renderFilePathParams = clone $renderFilePathParams;
            $renderFilePathParams->color = PrinterColor::MULTICOLOR;
            $renderFilePathParams->ambient = 0;
            $resultMulticolor = $this->doModel3dFileRender($file, $renderFilePathParams);
            $this->stdout("\nResult multicolor: " . json_encode($resultMulticolor));
        }


        $this->stdout("\nResult: " . json_encode($result));
        $this->stdout("\nResult ambient: " . json_encode($resultAmbient));

        if ($result && $result['success'] && $resultAmbient && $resultAmbient['success']) {
            $fileJob->status = QueueGateway::JOB_STATUS_COMPLETED;
            $fileJob->result = json_encode($result);
            $fileJob->save(false);
            $fullPath = Yii::$app->getModule('model3dRender')->renderer->getRenderImagePath($file, $renderFilePathParams);
        }
    }

    protected function doModel3dFileRender(FileBaseInterface $file, $renderParams)
    {
        $fullPath = Yii::$app->getModule('model3dRender')->renderer->getRenderImagePath($file, $renderParams);

        Yii::$app->getModule('model3dRender')->renderer->renderFile($file, $renderParams);

        $result = [
            'success' => true
        ];
        if (!file_exists($fullPath)) {
            $result = [
                'success'        => false,
                'resultFileTest' => 'File not exists'
            ];
        };
        return ($result['success'] === true ? $result : null);
    }

    /**
     * render 3d model (kit) files by id
     *
     * @param int $modelId
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRenderModel($modelId)
    {
        $model3d = Model3d::tryFindByPk($modelId);

        $totalJobs = 0;
        foreach ($model3d->model3dParts as $model3dPart) {
            $this->actionRenderModelPart($model3dPart->id);
        }
    }

    /**
     * To render 1 specific file
     *
     * @param int $model3dPartId
     * @return array|mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRenderModelPart($model3dPartId)
    {
        $model3dPart = Model3dPart::tryFindByPk($model3dPartId);
        $filteredModel3dParts = [];
        $this->formModel3dPartRenderFilesList($model3dPart, $filteredModel3dParts, true);
        $this->stdout(
            "\nStart render Model: " . $model3dPart->model3d_id . ' Model3dPart: ' . $model3dPart->id . ' File: ' . $model3dPart->file->id
        );
        $this->renderFilteredModel3dParts($filteredModel3dParts);
    }

    /**
     * Get not exists images list
     *
     */
    public function actionGetNotExistsImagesList()
    {
        $this->stdout("not Realized\n");
    }


    /**
     * @param Model3dBasePartInterface $model3dPart
     * @param array $filteredModel3dParts
     * @param bool $onlyBaseModels
     * @throws \yii\base\InvalidConfigException
     */
    protected function formModel3dPartRenderFilesList(Model3dBasePartInterface $model3dPart, &$filteredModel3dParts, $onlyBaseModels = false, $checkFileExists=true)
    {
        if (!$model3dPart->file->testIsExists()) {
            $this->stdout('Original stl file not exists: ' . $model3dPart->id . ' ' . $model3dPart->title . "\n");
            return;
        }
        $step = Yii::$app->getModule('model3dRender')->anglePreloadStep;
        $finishStep = 360 - $step;
        $az = 0;
        $pb = $model3dPart->model3d->getCadFlag();
        for ($ax = 0; $ax <= $finishStep; $ax += $step) {
            for ($ay = -90; $ay <= 90; $ay += $step) {
                if ($onlyBaseModels) {
                    $ax = 180;
                    $ay = 30;
                }
                $axMinus = $ax - 180;
                $ayMinus = $ay;
                if ($ayMinus == -90) {
                    $ayMinus = -89;
                }
                if ($ayMinus == 90) {
                    $ayMinus = 89;
                }
                // Red color
                /** @var RenderFileParams $renderFilePathParams */
                $renderFilePathParams = Yii::createObject(
                    [
                        'class' => RenderFileParams::class,
                        'ax'    => $axMinus,
                        'ay'    => $ayMinus,
                        'az'    => $az,
                        'pb'    => $pb
                    ]
                );
                $path = Yii::$app->getModule('model3dRender')->renderer->getRenderImagePath($model3dPart->file, $renderFilePathParams);
                if (!file_exists($path) || !$checkFileExists) {
                    $hash = $model3dPart->file->md5sum . '_ff0000_0_' . $axMinus . '_' . $ayMinus . '_' . $az;
                    $filteredModel3dParts[$hash] = [
                        'model3dPart'  => $model3dPart,
                        'renderParams' => $renderFilePathParams
                    ];
                }

                // TODO: REMOVE AMBIENT
                $renderFilePathParams = clone $renderFilePathParams;
                $renderFilePathParams->ambient = 40;
                $path = Yii::$app->getModule('model3dRender')->renderer->getRenderImagePath($model3dPart->file, $renderFilePathParams);
                if (!file_exists($path)  || !$checkFileExists) {
                    $hash = $model3dPart->file->md5sum . '_ff0000_40_' . $axMinus . '_' . $ayMinus . '_' . $az;
                    $filteredModel3dParts[$hash] = [
                        'model3dPart'  => $model3dPart,
                        'renderParams' => $renderFilePathParams
                    ];
                }

                // Multicolor format
                if ($model3dPart->isMulticolorFormat()) {
                    $renderFilePathParams = clone $renderFilePathParams;
                    $renderFilePathParams->color = PrinterColor::MULTICOLOR;
                    $path = Yii::$app->getModule('model3dRender')->renderer->getRenderImagePath($model3dPart->file, $renderFilePathParams);
                    if (!file_exists($path)  || !$checkFileExists) {
                        $hash = $model3dPart->file->md5sum . '_multicolor_0_' . $axMinus . '_' . $ayMinus . '_' . $az;
                        $filteredModel3dParts[$hash] =
                            [
                                'model3dPart'  => $model3dPart,
                                'renderParams' => $renderFilePathParams
                            ];
                    }
                }
                if ($onlyBaseModels) {
                    break 2;
                }
            }
        }
    }

    public function renderFilteredModel3dParts($filteredModel3dParts)
    {
        $totalCount = count($filteredModel3dParts);

        $this->stdout("\nTotal parts: " . $totalCount);
        $doneJobs = 0;
        foreach ($filteredModel3dParts as $hash => $renderInfo) {
            /** @var Model3dPart $model3dPart */
            $model3dPart = $renderInfo['model3dPart'];
            $renderParams = $renderInfo['renderParams'];
            $file = $model3dPart->file;
            $doneJobs++;
            $path = Yii::$app->getModule('model3dRender')->renderer->getRenderImagePath($model3dPart->file, $renderParams);
            $this->stdout(
                "\nStart render " . $doneJobs . '/' . $totalCount . '. Model: ' . $model3dPart->model3d_id . ' Model3dPart: ' . $model3dPart->id .
                ' File: ' . $file->id . '(' . $file->getLocalTmpFilePath() . ') Path: ' . $path
            );
            try {
                $this->doModel3dFileRender($model3dPart->file, $renderParams);
            } catch (\Exception $e) {
                $this->stdout('Exception: ' . $e->getMessage());
                sleep(2);
            }
        }
        $this->stdout("\nTotal Render Jobs: $doneJobs \n\n", \yii\helpers\Console::FG_GREEN);
    }

    protected function globRecursive($directory, &$directories)
    {
        foreach (glob($directory, GLOB_ONLYDIR | GLOB_NOSORT) as $folder) {
            $directories[] = $folder;
            $this->globRecursive("{$folder}/*", $directories);
        }
    }

    protected function findOldFiles($directory, $extension)
    {
        $directories = [];
        $this->globRecursive($directory, $directories);
        $files = [];
        foreach ($directories as $oneDirectory) {
            foreach (glob("{$oneDirectory}/*.{$extension}") as $file) {
                $files[] = $file;
            }
        }
        return $files;
    }

    protected function parceOldFiles($allRenderFiles)
    {
        foreach ($allRenderFiles as $filePath) {
            if (strpos($filePath, '_360')) {
                echo 'unlink: ' . $filePath . "\n";
                unlink($filePath);
                continue;
            }
            $fileInfo = pathinfo($filePath);
            $baseName = $fileInfo['basename'];
            $md5 = substr($baseName, 0, 32);
            if (strpos($baseName, '_ambient_40_Red') || strpos($baseName, '_Red')) {
                if (strpos($baseName, '_Red')) {
                    $ambient = 0;
                }
                if (strpos($baseName, '_ambient_40_Red')) {
                    $ambient = 40;
                }

                $renderParams = Yii::createObject(
                    [
                        'class'   => RenderFileParams::class,
                        'ambient' => $ambient
                    ]
                );
                $file = new File();
                $file->md5sum = $md5;
                $newPath = Yii::$app->getModule('model3dRender')->renderer->getRenderImagePath($file, $renderParams);
                $fileInfo = pathinfo($newPath);
                echo 'rename: ' . $filePath . ' => ' . $newPath . "\n";
                echo $fileInfo['dirname'];
                FileDirHelper::createDir($fileInfo['dirname']);
                FileDirHelper::renameForNfs($filePath, $newPath);
            }
        }
    }

    public function actionRenameOldNames()
    {
        if(!$this->confirm('Are you sure you want to rename old names?')){
            return ExitCode::OK;
        }
        $path = Yii::getAlias('@static/render');
        $allRenderFiles = $this->findOldFiles($path, 'png');
        $this->parceOldFiles($allRenderFiles);
    }

    /**
     * To render all files
     * add "true" parameter if want render only not exists
     *
     * @param bool $onlyBaseModels
     * @param bool $checkFileExists
     */
    public function actionRenderAll($onlyBaseModels = true,  $checkFileExists=true)
    {
        if(!$this->confirm('Render all?')){
            return ExitCode::OK;
        }
        /** @var Model3dPart[] $model3dParts */
        $model3dParts = Model3dPart::find()
            ->with('file')
            ->joinWith(['file'])
            ->orderBy('file.last_access_at ASC')->all();
        $filteredModel3dParts = [];
        foreach ($model3dParts as $model3dPart) {
            $this->formModel3dPartRenderFilesList($model3dPart, $filteredModel3dParts, $onlyBaseModels, $checkFileExists);
        }
        $this->renderFilteredModel3dParts($filteredModel3dParts);
    }
}
