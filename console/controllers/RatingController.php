<?php

namespace console\controllers;

use common\components\ConsoleController;
use common\models\Company;
use common\models\GeoCountry;
use common\models\PaymentInvoice;
use common\models\Preorder;
use common\models\PsCatalog;
use common\services\rating\Manager;

/**
 *
 * @author Nikolay Koshmin <analitic1983@gmail.com>
 */
class RatingController extends ConsoleController
{
    public function actionWorkOrder()
    {
        $query = Company::find()->active();
        /** @var Company $company */
        foreach($query->each() as $company) {
            $preorderCount = Preorder::find()->forPs($company)->notDraft()->count();
            $payOrder = PaymentInvoice::find()->isPayedStatus()->joinWith(['storeOrder.currentAttempt'])->where(['store_order_attemp.ps_id' => $company->id])->count();
            //$completedOrder = StoreOrder::find()->joinWith(['currentAttempt'])->
        }
    }

    public function actionCount()
    {
        $manager = \Yii::createObject(Manager::class);
        foreach (Company::find()->notDeleted()->each() as $company) {
            $company = $manager->count($company);
            $company->psCatalog->safeSave();
        }
    }

    public function actionTop()
    {
        $countries = GeoCountry::find()->all();
        PsCatalog::updateAll(['country_rating' => 0]);
        foreach($countries as $country) {
            $companies = Company::find()
                ->active()
                ->innerJoinWith(['country','psCatalog'])
                ->andWhere(['geo_country.id' => $country->id])
                ->orderBy(['ps_catalog.rating' => SORT_DESC])
                ->limit(100)
                ->all();

            /** @var Company $company */
            foreach ($companies as $rating => $company) {
                $company->psCatalog->addToLog([
                    PsCatalog::COUNTRY_RATING => $rating + 1,
                    PsCatalog::COUNTRY_RATING.'_prev' => $company->psCatalog->countryRating()
                ]);
                $company->psCatalog->country_rating = $rating + 1;
                $company->psCatalog->safeSave();
            }
        }
    }


}