<?php
namespace console\controllers;

use common\components\ConsoleController;

/**
 * Filament controller.
 * Helps to generate G-Code based on filament diameter and density
 * Helps to find filament weight required for printing (uses FilamentEstimator to find weight)
 * Helps to calculate price for printing model based on material and material weight
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class FilamentController  extends ConsoleController
{

    /**
     * get file info using slic3r
     * 
     * @param type $file
     * @return type
     */
    public function actionFileInfo($file)
    {
        $result = \lib\d3\SlicerWrapper::init()->info()->file($file)->run();
        pre($result);
        return $result;
    }

    /**
     * repair specified file
     * 
     * @param type $file
     * @return type
     */
    public function actionRepair($file)
    {
        $result = \lib\d3\SlicerWrapper::init(true)->repair($file);
        pre($result);
        return $result;
    }

    /**
     * run slic3r with specified params
     * 
     * @param type $file
     * @param type $diameter
     * @param type $density
     * @return type
     */
    public function actionRunSlicer($file, $diameter = 2.85, $density = 0.2)
    {        
        $path = pathinfo($file, PATHINFO_DIRNAME);
        $result = \lib\d3\SlicerWrapper::init()
            ->nozzleDiameter(0.35)->filamentDiameter($diameter)
            ->fillDensity($density)
            ->output($file.'gcode')
            ->file($file)->run();
        return $result;
    }

    /**
     * get filament information from gcode file
     * 
     * @param type $gCodeFile
     * @return int
     */
    public function actionInfo($gCodeFile)
    {
        $result = \lib\d3\FilamentEstimator::getFilamentDetails($gCodeFile);
        $r = \lib\d3\FilamentEstimator::debug($result);
        pre($r);
        return 0;
    }
    
    
    /**
     * prepare colors information in database.
     * we should match color title and render_title - in order to create renders
     * in required color
     * 
     * based on http://www.f-lohmueller.de/pov_tut/tex/tex_150e.htm documentation     
     */
    public function actionPovrayColors()
    {
        $colorTxt = \common\models\PrinterColor::getRenderColors();
        $colorsData = "/usr/share/povray-3.7/include/colors.inc";
        $content = file($colorsData);
        $result = [];
        foreach($content as $line)
        {
            // #declare Red     = rgb <1, 0, 0>;
            // color red 0.329412 green 0.329412 blue 0.329412
            if(strpos($line, '#declare')===false){
                continue;
            }
            $rowParsed = explode("=", $line);
            $colorT = str_replace("#declare ", "", $rowParsed[0]);
            $colorTitle = trim($colorT);
            if(!in_array($colorTitle, $colorTxt)){
                continue;
            }
            
            // found color
            $colorPart = $rowParsed[1];
            $rgb = false;
            if(strpos($colorPart, 'rgb')!==false){                
                $parsed = str_replace(["rgb", ">", "<"], "", $colorPart); 
                $matches = explode(", ", $parsed);
                $matches = array_map("floatval", $matches);
                if(count($matches) < 3){
                    continue;
                }
                $red = $matches[0] * 255;
                $green = $matches[1]  * 255;
                $blue = $matches[2]  * 255; 
                $rgb = [$red, $green, $blue];
            }else if(strpos($colorPart, 'color')!==false){
                $floatExpr = "\d+.?\d+?";
                preg_match("#red ($floatExpr) green ($floatExpr) blue ($floatExpr)#", $colorPart, $matches);
                $matches = array_map("floatval", $matches);
                if(count($matches) < 3){
                    continue;
                }
                $red = $matches[1] * 255;
                $green = $matches[2]  * 255;
                $blue = $matches[3]  * 255;
                $rgb = [$red, $green, $blue];                
            }
            if($rgb){
                $rgb = array_map("round", $rgb);
                $rgbTxt = implode(",", $rgb);
                $result[$colorTitle] = $rgbTxt;
                $cmykT = array_map("round", $this->rgb2cmyk($rgb[0], $rgb[1], $rgb[2]));
                $cmyk = implode("%, ", $cmykT);
                
                $r = \common\models\PrinterColor::findOne(['title'=>$colorTitle]);
                if($r===null){
                    \common\models\PrinterColor::addRecord([
                        'title' => $colorTitle,
                        'render_color' => $colorTitle,
                        'created_at' => dbexpr('now()'),
                        'is_active' => 1,
                        'rgb' => $rgbTxt,
                        //'cmyk' => $cmyk . '%'
                    ]);
                    echo ("Color $colorTitle rgb $rgbTxt \n");
                }else{
                    \common\models\PrinterColor::updateRecord($r->id, [
                        'title' => $colorTitle,
                        'render_color' => $colorTitle,
                        'updated_at' => dbexpr('now()'), 
                        'rgb' => $rgbTxt,
                        'cmyk' => ''
                    ]);
                    echo ("UPDATE Color $colorTitle rgb $rgbTxt \n");
                }
            }
        }
    }
    
    private function rgb2cmyk($var1,$g=0,$b=0) {
        if (is_array($var1)) {
           $r = $var1['r'];
           $g = $var1['g'];
           $b = $var1['b'];
        } else {
           $r=$var1;
        }
        $cyan    = 255 - $r;
        $magenta = 255 - $g;
        $yellow  = 255 - $b;
        $black  = min($cyan, $magenta, $yellow);
        $cyan    = @(($cyan    - $black) / (255 - $black)) * 255;
        $magenta = @(($magenta - $black) / (255 - $black)) * 255;
        $yellow  = @(($yellow  - $black) / (255 - $black)) * 255;
        return array($cyan / 255 * 100,
                     $magenta / 255* 100,
                     $yellow / 255* 100,
                     $black / 255* 100);
     }
}