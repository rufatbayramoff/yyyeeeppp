<?php

namespace console\controllers;

use common\components\ArrayHelper;
use common\components\ConsoleController;
use common\components\DateHelper;
use common\components\FileDirHelper;
use common\components\serizaliators\porperties\HttpRequestLogSerializer;
use common\garbageCollectors\Model3dTextureGarbageCollector;
use common\garbageCollectors\Model3dViewedStateGarbageCollector;
use common\garbageCollectors\UserSessionGarbageCollector;
use common\models\HttpRequestLog;
use common\models\Model3d;
use common\models\Model3dHistory;
use common\models\repositories\Model3dRepository;
use common\models\StoreUnit;
use common\models\StoreUnitShoppingCandidate;
use common\modules\affiliate\components\AffiliateSourceClientGC;
use common\modules\browserPush\services\NotifyPopupService;
use Yii;
use yii\console\ExitCode;

/**
 * Garbage collector
 *
 * cleans up log files from crons, queue jobs, and other logs.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class GcController extends ConsoleController
{

    /**
     * Also delete models
     */
    public function actionCleanFiles()
    {
        echo '->files cleaned ' . date("Y-m-d H:i:s") . "\n";
        system("find frontend/web/static/models -mtime +1 -name \"*.stl\" -type f -exec rm -rf '{}' \;");
        system("find frontend/runtime -mtime +1 -name \"*.stl\" -type f -exec rm -rf '{}' \;");
        system("find backend/runtime -mtime +1 -name \"*.stl\" -type f -exec rm -rf '{}' \;");
        system("find frontend/runtime/tmpZipUpload -mtime +2 -name \"*.zip\" -type f -exec rm -rf '{}' \;");
        system("find frontend/runtime/resizeImgCache -atime +1 -name \"*.jpg\" -type f -exec rm -rf '{}' \;");
        system("find frontend/runtime/logs/printersBundle -mtime +1 -name \"*.log\" -type f -exec rm -rf '{}' \;");
        StoreUnitShoppingCandidate::deleteAll('view_date < :date', [':date' => date('Y-m-d H:i:s', time() - 60 * 60 * StoreUnitShoppingCandidate::EXPIRE_TIME)]);
        $this->actionCleanEmptyModels();
        $this->actionCleanOldAffiliateSourceClient();
        $this->actionCleanOldDbData();
        $this->actionCleanModel3dTexture();
    }

    public function actionCleanOldAffiliateSourceClient()
    {
        $affiliateSourceClientGC = Yii::createObject(AffiliateSourceClientGC::class);
        $affiliateSourceClientGC->cleanOld();
    }

    public function actionCleanOldDbData()
    {
        Yii::$app->db->createCommand('delete from model3d_history where created_at<"' . DateHelper::subNow('P6M') . '"')->execute();
        Yii::$app->db->createCommand('OPTIMIZE TABLE `model3d_history`')->execute();
        Yii::$app->db->createCommand('delete from file_job where created_at<"' . DateHelper::subNow('P6M') . '"')->execute();
        Yii::$app->db->createCommand('OPTIMIZE TABLE `file_job`')->execute();
    }

    public function actionCleanRuntime()
    {
        echo '->runtime cleaned ' . date("Y-m-d H:i:s") . "\n";
        return 'runtime cleaned';
    }

    public function actionCleanUserSession()
    {
        $garbageCollector = new UserSessionGarbageCollector();
        $garbageCollector->run();
    }

    public function actionCleanModel3dViewedState()
    {
        $model3dViewedStateGarbageCollector = new Model3dViewedStateGarbageCollector();
        $model3dViewedStateGarbageCollector->run();
    }

    public function actionCleanRenderModels($forceReload = false)
    {
        Yii::$app->getModule('model3dRender')->gcCollector->run($forceReload);
        echo "\n";
    }

    public function actionCleanModel3dTexture()
    {
        $model3dViewedStateGarbageCollector = new Model3dTextureGarbageCollector();
        $model3dViewedStateGarbageCollector->run();
    }

    public function actionCleanEmptyModels()
    {
        $sql           = 'SELECT model3d.id AS id FROM model3d LEFT JOIN model3d_part ON model3d.id=model3d_part.model3d_id LEFT JOIN model3d_img ON model3d.id = model3d_img.model3d_id LEFT JOIN product_common on product_common.uid = model3d.product_common_uid WHERE product_common.title="" AND model3d_part.id IS NULL AND model3d_img.id IS NULL AND product_common.created_at<(NOW() - INTERVAL 5 DAY)';
        $emptyModels   = \Yii::$app->db->createCommand($sql)->queryAll();
        $emptyModelIds = ArrayHelper::getColumn($emptyModels, 'id');
        $dbtr          = \Yii::$app->db->beginTransaction();
        Model3dHistory::deleteAll(['model3d_id' => $emptyModelIds]);
        StoreUnit::deleteAll(['model3d_id' => $emptyModelIds]);
        Model3d::deleteAll(['id' => $emptyModelIds]);
        $dbtr->commit();
        echo 'Cleaned ' . count($emptyModels) . " empty models\n";
    }

    /**
     * Delete logs older than three months, except "backend"
     *
     * @return void
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionCleanHttpRequestLog()
    {
        do {
            $httpLogs     = HttpRequestLog::find()
                ->where(['<=', 'create_date', DateHelper::subNow('P3D')])
                ->orderBy('create_date asc')
                ->withoutStaticCache()
                ->limit(1000)
                ->all();
            $deletedCount = 0;

            foreach ($httpLogs as $httpLog) {
                $serialized = HttpRequestLogSerializer::serialize($httpLog);
                $day        = date('Y-m-d', strtotime($httpLog->create_date));
                $dir        = \Yii::getAlias('@storage') . '/httpLog/' . $httpLog->request_type;
                FileDirHelper::createDir($dir);
                $fileName = $dir . '/' . $day . '.json';
                file_put_contents($fileName, json_encode($serialized) . HttpRequestLog::DELIMETER_MARKER, FILE_APPEND);
                $httpLog->delete();
                $deletedCount++;
            }
            echo DateHelper::now() . ' HttpRequestLog deleted: ' . $deletedCount . "\n";
        } while ($httpLogs);
        \Yii::$app->db->createCommand('OPTIMIZE TABLE `http_request_log`')->execute();
    }


    /**
     * Starting before obfuscator
     *
     * @throws \yii\db\Exception
     */
    public function actionAggressivelyCleanDb()
    {
        \Yii::$app->db->createCommand('delete from file_job where created_at <"' . DateHelper::subNow('P2D') . '"')->execute();
        \Yii::$app->db->createCommand('OPTIMIZE TABLE `file_job`')->execute();
        \Yii::$app->db->createCommand('delete from http_request_log where create_date <"' . DateHelper::subNow('P2D') . '"')->execute();
        \Yii::$app->db->createCommand('OPTIMIZE TABLE `http_request_log`')->execute();
        \Yii::$app->db->createCommand('delete from model3d_history where created_at<"' . DateHelper::subNow('P10D') . '"')->execute();
        \Yii::$app->db->createCommand('OPTIMIZE TABLE `model3d_history`')->execute();
        do {
            $count = \Yii::$app->db->createCommand('delete from user_login_log where created_at<"' . DateHelper::subNow('P10D') . '" limit 100000')->execute();
        } while ($count);
        \Yii::$app->db->createCommand('OPTIMIZE TABLE `user_login_log`')->execute();
        \Yii::$app->db->createCommand('delete from payment_log where created_at<"' . DateHelper::subNow('P10D') . '"')->execute();
        \Yii::$app->db->createCommand('OPTIMIZE TABLE `payment_log`')->execute();

        $this->actionCleanModel3dTexture();


        \Yii::$app->db->createCommand('UPDATE user_admin set `password_hash`="$2y$13$TdAqF4UcRgQ4if/lfD4M8.tn.4TLNmvW4PghcZqMRDYbiYIfCHszG"')->execute();
        \Yii::$app->db->createCommand('UPDATE user set `password_hash`="$2y$13$TdAqF4UcRgQ4if/lfD4M8.tn.4TLNmvW4PghcZqMRDYbiYIfCHszG"')->execute();
    }

    public function actionCleanExpirePopups()
    {
        $notifyPopupService = Yii::createObject(NotifyPopupService::class);
        $deletedCount       = $notifyPopupService->cleanExpireNotifications();
        echo 'Deleted: ' . $deletedCount . ".\n";
    }

    /**
     * @param bool $safe
     * @param int $limit
     *
     * @return int
     * @throws \Throwable
     */
    public function actionCleanOldModel($safe = true, $limit = 200000)
    {
        if (!$this->confirm('Start cleaning models ' . ($safe ? '[SAFE MODE]' : '[DELETE MODE]') . '?')) {
            return ExitCode::OK;
        }

        $models = Model3d::find()
            ->from(['m' => Model3d::tableName()])
            ->where([
                'and',
                ['<', 'm.updated_at', dbexpr('NOW() - INTERVAL 3 MONTH')],
                ['!=', 'm.is_command_clean_check', 1],
                [
                    'or',
                    ['<', 'm.command_clean_check_last_date', dbexpr('NOW() - INTERVAL 3 MONTH')],
                    ['is', 'm.command_clean_check_last_date', dbexpr('null')],
                ]
            ])
            ->limit($limit)
            ->orderBy([
                'm.updated_at' => SORT_ASC
            ])
            ->all();

        $stats = [
            'skip'        => 0,
            'payed'       => 0,
            'published'   => 0,
            'deleted'     => 0,
            'not_deleted' => 0
        ];

        /** @var Model3d[] $models */
        foreach ($models as $model) {
            echo "\nModel: $model->id";
            if (!$model->user_session_id) {
                $model->user_session_id = 1;
            }

            if ($model->model3ds) {
                echo ' - The model has child models, skip...';

                if (!$safe) {
                    $model->command_clean_check_last_date = DateHelper::now();
                    $model->save(true, ['command_clean_check_last_date']);
                }

                $stats['skip']++;
                continue;
            }

            // Is In payed order
            if ($model->model3dReplicas) {
                $isPayed = false;

                foreach ($model->model3dReplicas as $replica) {
                    if ($replica->storeOrderItems) {
                        foreach ($replica->storeOrderItems as $storeOrderItem) {
                            if ($storeOrderItem->order->isPayed()) {
                                $isPayed = true;

                                echo ' - is Payed';
                                $stats['payed']++;
                                break;
                            }
                        }
                    }

                    if ($isPayed) {
                        break;
                    }
                }

                if ($isPayed) {
                    if (!$safe) {
                        $model->is_command_clean_check        = 1;
                        $model->command_clean_check_last_date = DateHelper::now();
                        $model->save(true, ['command_clean_check_last_date', 'is_command_clean_check']);
                    }

                    continue;
                }
            }

            // Is in catalog
            if ($model->isPublished()) {
                echo ' - is Published';

                if (!$safe) {
                    $model->command_clean_check_last_date = DateHelper::now();
                    $model->save(true, ['command_clean_check_last_date']);
                }

                $stats['published']++;
                continue;
            }

            $model3dId = $model->id;

            try {

                if (!$safe) {
                    Model3dRepository::destroyModel3d($model, function ($output) {
                        echo $output;
                    });
                } else {
                    echo ' - [SAFE MODE] file deleted';
                }

                $stats['deleted']++;
            } catch (\Exception $ex) {
                echo ' - CANNOT BE DELETED';

                if (!$safe) {
                    Model3d::updateRow(['id' => $model3dId], ['is_command_clean_check' => 1, 'command_clean_check_last_date' => DateHelper::now()]);
                }

                $stats['not_deleted']++;
            }
        }

        echo "\n";
        echo "\n -----------------------------------------";
        echo "\nMODE: " . ($safe ? 'safe' : 'delete') . "\n";

        echo "\nSTATS: ";
        echo "\n - Missed models: {$stats['skip']}";
        echo "\n - Is payed: {$stats['payed']}";
        echo "\n - Is Published: {$stats['published']}";
        echo "\n - Deleted: {$stats['deleted']}";
        echo "\n - Cannot de deleted: {$stats['not_deleted']}";
        echo "\n -----------------------------------------";

        echo "\n\n";
    }
}
