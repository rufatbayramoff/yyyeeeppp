<?php

namespace console\controllers;


use common\components\ConsoleController;
use common\components\DateHelper;
use common\models\Ps;
use common\models\UserSms;

/**
 * Class PhoneController
 * @package console\controllers
 */
class PhoneController extends ConsoleController
{
    /**
     *
     */
    public function actionValidateNumbers()
    {
        $failedSms = UserSms::find()
            ->select('user_sms.phone as phone, count(user_sms.id) as cnt, any_value(ps.id) as ps_id')
            ->leftJoin('ps', 'ps.user_id=user_sms.user_id')
            ->andWhere(['ps.phone_status' => 'checked'])
            ->andWhere(['user_sms.status' => UserSms::STATUS_FAILED])
            ->andWhere(['>', 'user_sms.created_at', DateHelper::subNow('P10D')])
            ->groupBy('user_sms.phone')
            ->asArray()
            ->all();
        echo "\nMark as new ps: ";
        foreach ($failedSms as $oneFailedSms) {
            if ($oneFailedSms['cnt']>10) {
                echo $oneFailedSms['ps_id'].', ';
                Ps::updateRow(['id'=>$oneFailedSms['ps_id']], ['phone_status'=>PS::PHONE_STATUS_NEW]);
            }
        }
        echo "\n";
    }
}