<?php namespace console\controllers;

use common\components\ConsoleController;
use common\components\Emailer;
use Yii;
/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class TaxesController extends ConsoleController
{
    
    /**
     * check that user has tax information, after he got awarded money
     * if more then 30 days, we will send email to support to notify them to send email
     * each 10 days, we will send email to user to notify him to fullfill tax information
     */
    public function actionCheckTaxForm()
    {
        $sql = "select
                   pa.user_id,
                   a.updated_at,
                   a.amount,
                   pdo.payment_id,
                   a.type,
                   t.email_notify,
                   t.id as tax_id
            from payment_detail a
                   join payment_account pa on pa.id = a.payment_account_id
                   join payment_detail_operation pdo on pdo.uuid = a.payment_detail_operation_uuid
                   left join user_tax_info t ON (t.user_id = pa.user_id)
            where a.type IN ('award_print', 'award_model')
              AND (t.id is null OR t.status = 'new')
              AND a.updated_at BETWEEN CURDATE() - INTERVAL 50 DAY AND CURDATE();";
        
        $rows = Yii::$app->getDb()->createCommand($sql)->queryAll();
        $cronUser = \common\models\User::USER_CRON;
        foreach($rows as $k=>$v){            
            $userId = $v['user_id'];
            if(empty($v['email_notify']) && !empty($v['tax_id'])){ // dont send email if tax info has & email notify off
                continue;
            }
            $updateTime = strtotime($v['updated_at']);
            $now = time();
            $diff = round( ($now - $updateTime) / 60 / 60 / 24 );
            if ($diff == 10) { // send email to user
                $user = \common\models\User::findByPk($userId);
                $emailer = new \common\components\Emailer();
                $emailer->sendFirstTaxMessage($user, 20); // second time time
            }
            if ($diff == 20) { // send email to user
                $user = \common\models\User::findByPk($userId);
                $emailer = new \common\components\Emailer();
                $emailer->sendFirstTaxMessage($user, 10); // second third time
                \common\models\UserTaxInfo::taxNotifyOff($user); // turned off now
            }
            if($diff > 30 && $diff < 33){ // 3 days to send :)
                // check if empty tax after 30 days for user
                $msg = "Tax information not submited after 30 days. User ID: $userId";
                $emailer = new Emailer();
                $emailer->sendSupportMessage("User ID: $userId - Tax info not submited after 30 days", $msg);
                
            } 
        }
    }
    
    public function actionApplyTaxes()
    {
        die("turned off #1519");
        $sql = " 
        select a.user_id, a.updated_at, a.amount, a.payment_id, a.type
        from payment_detail a
        left join payment_detail_tax_rate b ON (a.id=b.payment_detail_id)
        where a.type IN('award_print', 'award_model') AND b.id is null;";
        
        $rows = Yii::$app->getDb()->createCommand($sql)->queryAll();
        
        $cronUser = \common\models\User::USER_CRON;
        foreach($rows as $k=>$v){            
            $userId = $v['user_id'];
            // turned off by 1519;
            //$res = \common\models\PaymentDetailTaxRate::addUserTaxes($userId, $cronUser);
            $res = true;
            if($res){
                $this->println("Tax apply user: $userId");
            }else{
                $updateTime = strtotime($v['updated_at']);
                $now = time();
                $diff = ($now - $updateTime) / 60 / 60 / 24;
                if($diff > 30 && $diff < 33){ // 3 days to send :)
                    // check if empty tax after 30 days for user
                    $msg = "Tax information not submited after 30 days. User ID: $userId";
                    $emailer = new Emailer();
                    $emailer->sendSupportMessage("User ID: $userId - Tax info not submited after 30 days", $msg);
                }
            }
        }
        
    }
    
    
}
