<?php
/**
 * Date: 05.07.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace console\controllers;


use common\components\ConsoleController;
use common\models\ApiFile;

class ApiFilesController extends ConsoleController
{

    public function actionCheckExpired()
    {
        $expireAfterHours = 24;

        $expiredFiles = ApiFile::find()
            ->andWhere(['status' => [ApiFile::STATUS_UPLOADED, ApiFile::STATUS_NEW]])
            ->andWhere(['and', "created_at<=NOW() - INTERVAL $expireAfterHours HOUR"])
            ->all();

        foreach($expiredFiles as $apiFile)
        {
            $apiFile->status = ApiFile::STATUS_EXPIRED;
            $apiFile->safeSave();
            $this->println(sprintf('Api file expired id[%d]', $apiFile->id));
        }
    }
}