<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.10.16
 * Time: 16:38
 */

namespace console\controllers;

use common\components\ConsoleController;
use common\modules\contentAutoBlocker\components\contentFilters\BannedPhraseFilter;
use Yii;
use yii\helpers\VarDumper;


class ContentBlockerController extends ConsoleController
{
    public function actionCheckNewContent()
    {
        Yii::$app->getModule('contentAutoBlocker')->contentBlocker->checkNewContent();
    }

    public function actionCheckPhrase($text)
    {
        /**
         * @var $filter BannedPhraseFilter
         */
        $filter = Yii::createObject(BannedPhraseFilter::class);
        $f = $filter->isAlarmText($text);
        $this->println($f . ' result for [' . $text . ']');
        echo VarDumper::dumpAsString($filter);
    }
}