<?php

namespace console\controllers;

use Braintree\Test\Transaction;
use common\components\ConsoleController;
use common\models\PaymentbtTransaction;
use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\modules\payment\gateways\vendors\BraintreeGateway;
use common\modules\payment\processors\BraintreeProcessor;
use common\modules\payment\services\PaymentAutoUpdateStatusService;
use yii\helpers\VarDumper;

/**
 * update exchange rates using openexchangerates.org
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaymentController extends ConsoleController
{
    /**
     * @var BraintreeProcessor
     */
    protected $braintreeProcessor;

    /**
     * @var BraintreeGateway
     */
    protected $braintreeGateway;

    /**
     * @var PaymentAutoUpdateStatusService
     */
    protected $paymentAutoUpdateStatusService;

    /**
     * @param PaymentAutoUpdateStatusService $paymentAutoUpdateStatusService
     */
    public function injectDependencies(PaymentAutoUpdateStatusService $paymentAutoUpdateStatusService, BraintreeProcessor $braintreeProcessor, BraintreeGateway $braintreeGateway)
    {
        $this->paymentAutoUpdateStatusService = $paymentAutoUpdateStatusService;
        $this->braintreeProcessor = $braintreeProcessor;
        $this->braintreeGateway = $braintreeGateway;
    }

    /**
     * update open rates
     *
     * @return boolean
     * @throws \Exception
     */
    public function actionUpdateRates()
    {
        \common\models\PaymentExchangeRate::refreshRate();
        echo "updated \n\n";
        return true;
    }

    /**
     * @param $transactionid
     * @throws \yii\base\InvalidConfigException
     */
    public function actionTestSettle($transactionid)
    {
        $this->braintreeGateway->init();
        $r = $this->braintreeGateway->testSettle($transactionid);
        var_dump($r);
    }

    public function actionBraintreeUpdate($debug = false)
    {
        $this->paymentAutoUpdateStatusService->updateStatusesBraintree();
    }

    public function actionTransaction($id)
    {
        $gateway = new BraintreeGateway();
        $gateway->init();
        $result = $gateway->getTransaction($id);
        \Yii::info(VarDumper::dumpAsString($result));
        var_dump($result);
    }

    private function refresh($id)
    {
        $tr = $this->mainModel->tryFindByPk($id);
        $btTransaction = $this->braintreeProcessor->getTransaction($tr->transaction_id);
        return $btTransaction;
    }
}
