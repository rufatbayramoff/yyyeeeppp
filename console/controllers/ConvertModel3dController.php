<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.03.18
 * Time: 17:33
 */

namespace console\controllers;


use common\components\ConsoleController;
use common\models\File;
use common\models\FileJob;
use common\models\repositories\Model3dRepository;
use console\jobs\QueueGateway;
use Yii;
use yii\helpers\Json;

class ConvertModel3dController extends ConsoleController
{
    /** @var Model3dRepository */
    public $model3dRepository;

    public function injectDependencies(Model3dRepository $model3dRepository)
    {
        $this->model3dRepository = $model3dRepository;
    }

    public function convertModel3d($model3dUid)
    {
        $model3d = $this->model3dRepository->getByUid($model3dUid);
        if (!Yii::$app->getModule('convert')->converter->needToBeConverted($model3d)) {
            $this->stdout("\nModel3d " . $model3d->getUid() . ' has not convertable parts.');
            return;
        }
        $data = Yii::$app->getModule('convert')->converter->convertParts($model3d);
    }

    /**
     * To do render job
     *
     * @param $jobFileId
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionConvertJob($jobFileId)
    {
        $fileJob = FileJob::find()->withoutStaticCache()->where(['id' => $jobFileId])->tryOne();
        $args = $fileJob->args;
        if (!is_array($args)) {
            $args = Json::decode($args);
        }
        $model3dPart = $this->model3dRepository->getPartByUid($args['model3dPartUid']);
        $convertResult = Yii::$app->getModule('convert')->converter->convertPart($model3dPart);
        if ($convertResult) {
            $fileJob->status = QueueGateway::JOB_STATUS_COMPLETED;
            $fileJob->result = json_encode(['success' => 1]);
            $fileJob->save(false);

        }

    }


    /**
     * @param $model3dUid
     */
    public function actionConvertModel3d($model3dUid)
    {
        $this->convertModel3d($model3dUid);
    }
}