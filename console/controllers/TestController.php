<?php

namespace console\controllers;

use common\components\ArrayHelper;
use common\components\ConsoleController;
use common\components\exceptions\AssertHelper;
use common\components\UuidHelper;
use common\models\factories\FileFactory;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\repositories\FileRepository;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use common\modules\product\interfaces\ProductInterface;
use common\models\File;
use common\models\Model3d;
use common\models\StoreUnit;
use common\models\User;
use Exception;
use frontend\components\image\ImageHtmlHelper;
use lib\message\senders\SenderInterface;
use lib\money\Currency;
use lib\money\Money;
use Yii;
use yii\db\ActiveRecordInterface;
use yii\db\Query;


class TestController extends ConsoleController
{

    protected function formValues($values)
    {
        $line = '';
        foreach ($values as $key => $val) {
            if ($val === null) {
                $line .= 'NULL, ';
            } else {
                $val = str_replace('"', '\"', $val);
                $val = str_replace("\n", '\\n', $val);
                $line .= '"' . $val . '", ';
            }
        }
        $line = substr($line, 0, -2);
        return $line;

    }

    public function actionGenerateProductCommonUid()
    {
        $productsCommon = (new Query())->select('*')->from('product_common')->where('uid is null')->all();
        echo "Generate product common uuid: ";
        $i = 0;
        foreach ($productsCommon as $productCommon) {
            do {
                $notUpdated = true;
                try {
                    $productCommonUid = UuidHelper::generateRandCode(6);
                    $sql = "update product_common set uid='".$productCommonUid."' where id=".$productCommon['id'];
                    Yii::$app->db->createCommand($sql)->execute();
                    $notUpdated = false;
                } catch (\Exception $exception) {
                    echo "x";
                }
            } while ($notUpdated);
            $i++;
            if ($i>1000) {
                $i = 1;
                echo '.';
            }
        }
        echo " DONE\n";
    }

    public function actionMakeD()
    {
        $reviews = \Yii::$app->getDb()->createCommand('SELECT a.* FROM `yii2advanced_b1`.`store_order_review` a WHERE a.id not in (SELECT b.id FROM `yii2advanced`.`store_order_review` b)')->queryAll();
        $dumpFile = fopen('./reviews_2019_03_29.sql', 'w');
        $filesListFile = fopen('./review_files_2019_03_29.txt', 'w');
        foreach ($reviews as $review) {
            // Check exists store unit
            $storeUnit = StoreUnit::findByPk($review['store_unit_id']);
            if (!$storeUnit) {
                $review['store_unit_id'] = null;
            }

            // Review
            $line = 'insert into `store_order_review` values (' . $this->formValues($review) . ');';
            fwrite($dumpFile, $line . "\n");
            // Review files
            $reviewFiles = \Yii::$app->getDb()->createCommand('SELECT a.* FROM `yii2advanced_b1`.`store_order_review_file` a WHERE a.review_id=' . $review['id'])->queryAll();
            foreach ($reviewFiles as $reviewFile) {
                $line = 'insert into `store_order_review_file` values (' . $this->formValues($reviewFile) . ');';
                fwrite($dumpFile, $line . "\n");
                if ($reviewFile['file_uuid']) {
                    $fileExists = File::findOne(['uuid'=>$reviewFile['file_uuid']]);
                    if (!$fileExists) {
                        $file = \Yii::$app->getDb()->createCommand('SELECT a.* FROM `yii2advanced_b1`.`file` a WHERE a.uuid=\'' . $reviewFile['file_uuid'] . '\'')->queryOne();
                        $line = 'insert into `file` values (' . $this->formValues($file) . ');';
                        fwrite($dumpFile, $line . "\n");

                        $fileModel = new File();
                        $fileModel->setAttributes($file);

                        $path = $fileModel->getLocalTmpFilePath();
                        fwrite($filesListFile, $path . "\n");
                    }
                }
            }
            // Shared
            $reviewShares = \Yii::$app->getDb()->createCommand('SELECT a.* FROM `yii2advanced_b1`.`store_order_review_share` a WHERE a.review_id=' . $review['id'])->queryAll();
            foreach ($reviewShares as $reviewShare) {
                $line = 'insert into `store_order_review_share` values (' . $this->formValues($reviewShare) . ');';
                fwrite($dumpFile, $line . "\n");
            }
        }
        fclose($dumpFile);
        echo "Done.\n";
    }


    public function actionTest1()
    {
        $sqlPaymentTransactions = PaymentTransaction::find()
            ->where(['vendor' => 'braintree'])
            ->andWhere(['!=', 'transaction_id', '-'])
            ->andWhere([
                'not in',
                'status',
                [
                    PaymentTransaction::STATUS_SETTLED,
                    PaymentTransaction::STATUS_AUTHORIZED_EXPIRED,
                    PaymentTransaction::STATUS_VOIDED,
                    PaymentTransaction::STATUS_REFUNDED,
                ]
            ])
            ->andWhere(['<', PaymentTransaction::column('updated_at'), dbexpr('NOW() - INTERVAL 3 MINUTE')])
            ->limit(200);

        $paymentTransactions = $sqlPaymentTransactions->all();
        $paymentTransactions;
    }


    private function deleteModel(Model3d $model)
    {

        $this->println("{$model->id} Delete model");

        $t = \Yii::$app->db->beginTransaction();

        try {

            /** @var File[] $filesForDelete */
            $filesForDelete = [];
            $texturesForDelete = [];

            addForDelete($filesForDelete, getRelation($model, 'model3dReplicas.model3dReplicaParts.file'));
            addForDelete($filesForDelete, getRelation($model, 'model3dReplicas.model3dReplicaParts.fileSrc'));
            addForDelete($filesForDelete, getRelation($model, 'model3dReplicas.model3dReplicaImgs.file'));

            addForDelete($filesForDelete, getRelation($model, 'model3dImgs.file'));
            addForDelete($filesForDelete, getRelation($model, 'model3dImgs.originalFile'));
            addForDelete($filesForDelete, getRelation($model, 'model3dParts.file'));
            addForDelete($filesForDelete, getRelation($model, 'model3dParts.fileSrc'));

            deleteRelation($model, 'model3dReplicas.cncPreorders.cncPreorderPresets');
            deleteRelation($model, 'model3dReplicas.cncPreorders.cncPreorderWorks');
            deleteRelation($model, 'model3dReplicas.cncPreorders');

            addForDelete($texturesForDelete, getRelation($model, 'model3dReplicas.model3dTexture'));
            addForDelete($texturesForDelete, getRelation($model, 'model3dReplicas.model3dReplicaParts.model3dTexture'));

            addForDelete($texturesForDelete, getRelation($model, 'model3dParts.model3dTexture'));
            addForDelete($texturesForDelete, getRelation($model, 'model3dTexture'));

            deleteRelation($model, 'model3dReplicas.model3dReplicaParts');
            deleteRelation($model, 'model3dReplicas.model3dReplicaImgs');
            deleteRelation($model, 'model3dReplicas.cartItems');

            deleteRelation($model, 'model3dReplicas');

            deleteRelation($model, 'model3dParts');
            deleteRelation($model, 'model3dImgs');
            deleteRelation($model, 'model3dParts.model3dPartCncParams');
            deleteRelation($model, 'model3dParts.model3dPartProperties');

            deleteRelation($model, 'storeUnit.catalogCosts');
            deleteRelation($model, 'storeUnit.catalogCosts');
            deleteRelation($model, 'storeUnit.storeUnitShoppingCandidates');
            deleteRelation($model, 'storeUnit');

            addForDelete($filesForDelete, getRelation($model, 'thingiverseThing.thingiverseThingFiles.file'));
            deleteRelation($model, 'thingiverseThing.thingiverseThingFiles');
            deleteRelation($model, 'thingiverseThing');

            deleteRelation($model, 'apiPrintablePacks');

            deleteRelation($model, 'model3dTags');
            deleteRelation($model, 'model3dViewedStates');
            deleteRelation($model, 'userCollectionModel3ds');
            deleteRelation($model, 'model3dHistories');

            AssertHelper::assert($model->delete());

            foreach ($filesForDelete as $file) {
                deleteRelation($file, 'apiFiles');
                $file->delete();
            }

            foreach ($texturesForDelete as $file) {
                $file->delete();
            }

            $t->commit();
        } catch (\Throwable $e) {
            $t->rollBack();
            \Yii::error($e);
            $this->println($e->getMessage());

            //  throw $e;
        }
    }

    /**
     * @param Model3d $model3d
     * @return File[]
     */
    public function badModelFiles(Model3d $model3d)
    {
        $files = ArrayHelper::getColumn($model3d->model3dParts, 'file');
        return $this->findBadFiles($files);
    }

    /**
     * @param Model3d $model3d
     * @return File[]
     */
    public function badImagesFiles(Model3d $model3d)
    {
        $files = ArrayHelper::getColumn($model3d->model3dImgs, 'file');
        return $this->findBadFiles($files);
    }

    /**
     * @param array $files
     * @return File[]
     */
    private function findBadFiles(array $files)
    {
        return array_filter($files, function (File $file) {

            $path = $file->getLocalTmpFilePath();
            if (!file_exists($path)) {
                return true;
            }
            $diskMd5 = md5_file($path);

            return $diskMd5 != $file->md5sum;
        });
    }

    private function sendUnpublishedEMails(array $sendUnpublishedEmailModels)
    {
        foreach ($sendUnpublishedEmailModels as $models) {

            /** @var User $user */
            $user = ArrayHelper::first($models)->user;


            /** @var SenderInterface $sender */
            $sender = \Yii::$app->message->getSenderByType('email');

            $modelsTitles = join(', ', ArrayHelper::getColumn($models, 'title'));

            $messagText = "Dear {$user->username}, \n we found errors in the next models : {$modelsTitles}. \n Please reload the file if you want it to be published on the Treatstock.";
            $messageHtml = "Dear {$user->username}, <br/> we found errors in the next models : {$modelsTitles}. <br/> Please reload the file if you want it to be published on the Treatstock.";

            $sender->send($user->getEmail(), 'Found errors in your 3D models', $messagText, $messageHtml, $user);
        }
    }


    public function actionMovePsCovers()
    {
        $pss = Ps::find()->with('user.userProfile')->all();

        $fileFactory = Yii::createObject(FileFactory::class);

        foreach ($pss as $ps) {

            if ($ps->user->userProfile->background_url) {

                $oldPath = Yii::getAlias('@static' . $ps->user->userProfile->background_url);

                if (!file_exists($oldPath)) {
                    continue;
                }

                $file = $fileFactory->createFileFromPath($oldPath);
                $file->setPublicMode(true);
                $file->setOwner(Ps::class, 'cover_file_id');

                // this method also save file
                ImageHtmlHelper::stripExifInfo($file);
                $ps->cover_file_id = $file->id;
                $ps->safeSave();
            }
        }
    }

    public function getSuccessPayment(User $user)
    {
        $invoices = $user->paymentInvoices;
        foreach ($invoices as $invoice) {
            $payment = $invoice->getActivePayment();
            if ($payment && $payment->paymentDetails && $invoice->status === PaymentInvoice::STATUS_PAID) {
                return $payment;
            }
        }
        $paymentDetail = PaymentDetail::find()->joinWith('paymentAccount')->where('payment_account.user_id=' . $user->id . ' and payment_account.type=\'main\'')->one();
        $payment = $paymentDetail->paymentDetailOperation->payment;
        return $payment;
    }

    /**
     * @return array
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionZeroPaymentDetails()
    {
        /** @var PaymentService $paymentService */
        $paymentService = \Yii::createObject(PaymentService::class);
        $paymentAccountService = \Yii::createObject(PaymentAccountService::class);

        // Select all main accounts with minus balance
        $sql = <<<SQL
select pa.user_id, sum(pd.amount) as summ from user
left join payment_account pa on pa.user_id=user.id
left join payment_detail pd on pd.payment_account_id=pa.id
WHERE
pa.type='main' and
pa.user_id>999
group by pa.user_id
having sum(pd.amount) <0
SQL;
        $accountWithMinus = \Yii::$app->db->createCommand($sql)->queryAll();
        $transaction = app("db")->beginTransaction();
        foreach ($accountWithMinus as $accountWithMinus) {
            $summ = round($accountWithMinus['summ'], 2);
            if ($summ >= -0.01 && $summ <= 0.01) {
                continue;
            }
            $user = User::tryFindByPk($accountWithMinus['user_id']);
            echo "\n" . $user->id . ' (' . $user->username . ') : ' . $summ;
            if ($user->company && (($user->company->products) || ($user->company->psPrinters))) {
                echo 'Minus Company. UserId: ' . $user->id . ' Summ:' . $summ;
                exit;
            }

            $payment = $this->getSuccessPayment($user);
            $money = Money::create(abs($summ), Currency::USD);

            $userPaymentAccount = $paymentAccountService->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_MAIN);
            $treatstockCorrectionAccount = $paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_CORRECTION);

            $paymentService->transferMoney(
                $payment,
                $treatstockCorrectionAccount,
                $userPaymentAccount,
                $money,
                PaymentDetail::TYPE_CORRECTION,
                'Correction minus balance'
            );
        }
        $transaction->commit();
        echo "\nDone";
    }

}

function deleteRelation(ActiveRecordInterface $model, string $relation)
{
    /** @var ActiveRecordInterface[]|ActiveRecordInterface $models */
    $models = getRelation($model, $relation);

    if (is_array($models)) {
        foreach ($models as $model) {

            if ($model == null) {
                continue;
            }

            $model->delete();
        }
    } else {
        if ($models != null) {
            $models->delete();
        }
    }

}

function addForDelete(&$array, $obj)
{
    if ($obj === null) {
        return;
    }

    $obj = is_array($obj) ? $obj : [$obj];

    foreach ($obj as $item) {
        if ($item === null) {
            continue;
        }

        $key = get_class($item) . $item->id;
        $array[$key] = $item;
    }
}

function getRelation(ActiveRecordInterface $model3d, string $relation)
{
    $res = $model3d;
    foreach (explode('.', $relation) as $name) {
        $res = getFn($res, $name);
    }
    return $res;
}

function getFn($obj, $name)
{

    if (is_array($obj)) {

        $res = [];

        foreach ($obj as $item) {

            if ($item === null) {
                continue;
            }

            $itemRes = $item->$name;

            if (is_array($itemRes)) {
                $res = ArrayHelper::merge($res, $itemRes);
            } else {
                $res[] = $itemRes;
            }
        }

        return $res;
    } elseif ($obj !== null) {
        return $obj->$name;
    }

    return null;
}