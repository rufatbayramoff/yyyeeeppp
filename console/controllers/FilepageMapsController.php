<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace console\controllers;


use common\components\BaseActiveQuery;
use common\models\UserAddress;
use common\modules\printersList\models\RequestInfo;
use common\modules\printersList\services\RequestInfoService;
use Yii;
use yii\console\Controller;

class FilepageMapsController extends Controller
{
    public function actionGetTypicalPrintersSizes()
    {
        $list = Yii::$app->getModule('psPrinterListMapsModule')->psPrinterSizesMapCalculator->getMinimizedPrinterSizesList();
    }

    public function actionGetTypicalColors()
    {
        $list = Yii::$app->getModule('psPrinterListMapsModule')->colorsMapCalculator->getMinimizedColorsList();
    }

    public function actionGetLocations()
    {
        $list = Yii::$app->getModule('psPrinterListMapsModule')->locationsMapCalculator->getMinimizedList();
    }

    public function actionGetWeight()
    {
        $list = Yii::$app->getModule('psPrinterListMapsModule')->weightMapCalculator->getMinimizedList();
    }

    public function actionClearCacheRecalcMaps()
    {
        $this->actionGetTypicalPrintersSizes();
        $this->actionGetTypicalColors();
        $this->actionGetLocations();
        $this->actionGetWeight();
        $basePath = Yii::getAlias('@common/config/filepageMaps/');
        copy($basePath . 'last-sizes-map.php', $basePath . 'sizes-map.php');
    }

    /**
     * @param UserAddress $address
     * @param int $mode
     * @return array|null
     */
    public function getLatLonUAddress($address, $mode = 0)
    {
        sleep(1);
        $q = '';

        echo "\nMode: " . $mode;
        if ($address->region && $mode <= 1) {
            $q .= $address->region . ', ';
            echo "\nRegion: " . $address->region;

        };
        if ($address->city && $mode != 3 && $mode != 4) {
            $city = explode(', ', $address->city);
            if (count($city)) {
                $city = reset($city);
            }
            $city = explode(',', $city);
            if (count($city)) {
                $city = reset($city);
            }

            $q .= $city . ', ';
            echo "\nCity: " . $city;
        }
        if ($address->zip_code && ($mode == 0 || $mode == 3) && $mode != 4) {
            $q .= $address->zip_code . ', ';
            echo "\nZip code: " . $address->zip_code;
        }
        $q = substr($q, 0, -2);
        $q = urlencode($q);

        $url = 'http://nominatim.openstreetmap.org/search/?format=json&country=' . $address->country->iso_code . '&q=' . $q;
        echo "\nURL: " . $url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $output = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode != 200) {
            echo "\n$url\n";
            die('Invalid answer!');
        }
        echo "\nResult: " . $output . "\n";
        $result = json_decode($output, true);
        if (!array_key_exists(0, $result)) {
            return null;
        }
        $firstResult = reset($result);
        return [
            'lat' => $firstResult['lat'],
            'lon' => $firstResult['lon']
        ];
    }

    public function actionGetLatLonUserAddress()
    {
        /** @var UserAddress[] $userAddress */
        $userAddress = UserAddress::find()->where("(lat is null) and zip_code!=''")->all();
        echo "\nCount: " . count($userAddress);
        foreach ($userAddress as $address) {
            $coord = null;
            if ($address->zip_code) {
                $coord = $this->getLatLonUAddress($address, 0);
            }
            if (!$coord && $address->region) {
                $coord = $this->getLatLonUAddress($address, 1);
            }
            if (!$coord) {
                $coord = $this->getLatLonUAddress($address, 2);
            }
            if (!$coord) {
                $coord = $this->getLatLonUAddress($address, 3);
            }
            if (!$coord) {
                $coord = $this->getLatLonUAddress($address, 4);
            }
            if (!$coord) {
                echo "Skip null\n";
                continue;
            }
            $address->lat = $coord['lat'];
            $address->lon = $coord['lon'];
            $address->save(false);
        }
        echo "\nFinished\n";
    }

    public function actionFilepagePrintersDaemon()
    {
        $printersListDaemon = Yii::$app->getModule('printersList')->printersListDaemon;
        $printersListDaemon->listen();
    }

    public function actionPrintersTreeUpdateDaemon()
    {
        $this->actionPrintersTreeUpdate();
        $this->debugOutput('Start listen update printers daemon');
        Yii::$app->rabbitMq->listenQueue('treatstock.updatePrintersTree', [$this, 'actionPrintersTreeUpdate']);
    }

    public function actionPrintersTreeUpdate()
    {
        if (!Yii::$app->db->isActive) {
            Yii::$app->db->open();
        }
        BaseActiveQuery::$staticCacheGlobalEnable = false;
        $this->debugOutput('Start rebuild tree');
        $printersTreeUpdater = Yii::$app->getModule('printersList')->printersTreeUpdater;
        $printersTreeUpdater->update();
        $updateRequest = new RequestInfo();
        $updateRequest->operationType = RequestInfo::OPERATION_TYPE_UPDATE_PRINTERS_TREE;
        $printersListDaemon = Yii::$app->getModule('printersList')->printersListDaemon;
        $this->debugOutput('Rebuiled. Send request.');
        $printersListDaemon->sendRequest($updateRequest);
        $this->debugOutput("Done");
        Yii::$app->db->close();
    }

    public function actionSendTestRequest($direct = false)
    {
        $printersListDaemon = Yii::$app->getModule('printersList')->printersListDaemon;
        if ($direct) {
            $printersListDaemon->initPrintersProcessor();
        }
        $requestInfoService = new RequestInfoService();
        $requests = [];//$requestInfoService->formTestRequests();
        //$request = unserialize('O:46:"common\modules\printersList\models\RequestInfo":9:{s:16:"allowedMaterials";a:0:{}s:9:"countryId";i:233;s:4:"size";s:26:"00000120x00000120x00000120";s:16:"onlyCertificated";i:0;s:7:"volumes";a:3:{i:88314;O:52:"common\modules\printersList\models\RequestVolumeInfo":2:{s:11:"textureInfo";O:46:"common\modules\printersList\models\TextureInfo":3:{s:15:"materialGroupId";i:16;s:10:"materialId";N;s:15:"materialColorId";i:90;}s:6:"volume";i:65;}i:88315;O:52:"common\modules\printersList\models\RequestVolumeInfo":2:{s:11:"textureInfo";O:46:"common\modules\printersList\models\TextureInfo":3:{s:15:"materialGroupId";i:12;s:10:"materialId";N;s:15:"materialColorId";i:102;}s:6:"volume";i:3;}i:88316;O:52:"common\modules\printersList\models\RequestVolumeInfo":2:{s:11:"textureInfo";O:46:"common\modules\printersList\models\TextureInfo":3:{s:15:"materialGroupId";i:16;s:10:"materialId";N;s:15:"materialColorId";i:90;}s:6:"volume";i:30;}}s:17:"allowResetTexture";b:1;s:4:"sort";s:7:"default";s:11:"locationLat";d:34.05224;s:11:"locationLon";d:-118.24368;}');
        //var_dump($request);
        //$requests[] = $request;

        $this->debugOutput('Request formed count: ' . count($requests) . '.');
        $i = 1;
        $startTime = microtime(true);
        foreach ($requests as $request) {
            if ($direct) {
                $answerInfo = $printersListDaemon->processRequest($request);
            } else {
                $answerInfo = $printersListDaemon->sendRequest($request);
            }
            $this->debugOutput('Pos: ' . $i . ' Country: ' . $request->countryId . ' Size: ' . $request->size . ' Printers:' . count($answerInfo->offers));
            $i++;
        }
        $endTime = microtime(true);
        $requestsPerSec = round(($i - 1) / ($endTime - $startTime), 3);
        $this->debugOutput('Count: ' . ($i - 1) . ' Request per sec: ' . $requestsPerSec . ' ' . ($endTime - $startTime));
    }

    public function actionFormApc()
    {
        $printersListDaemon = Yii::$app->getModule('printersList')->printersListDaemon;
        $processor = $printersListDaemon->initPrintersProcessor();
        Yii::$app->redis->set('printersProcessor', serialize($processor));
    }


    protected function debugOutput($str)
    {
        echo "\n" . date('Y-m-d H:i:s') . ' ' . $str . ' Usage ' . round(memory_get_usage() / 1024 / 1024, 2) . ' mb.';
    }
}