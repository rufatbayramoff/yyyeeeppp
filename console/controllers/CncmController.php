<?php
/**
 * Created by mitaichik
 */

namespace console\controllers;


use common\models\FileJob;
use common\modules\cnc\jobs\AnalyzeJob;
use console\jobs\QueueGateway;
use yii\console\Controller;

class CncmController extends Controller
{
    public function actionAnalyzeJob($jobFileId)
    {
        /** @var FileJob $fileJob */
        $fileJob = FileJob::find()->withoutStaticCache()->where(['id'=>$jobFileId])->tryOne();

        /** @var AnalyzeJob $job */
        $job = \Yii::createObject(AnalyzeJob::class);
        $job->args = $fileJob->getArgsData();
        QueueGateway::configureJob($job, $fileJob->getArgsData());

        try{
            $result = $job->doJob();
            $fileJob->setResult($result);
            $fileJob->markAsCompleted();
        }
        catch (\Exception $e) {
            $fileJob->markAsFailed($e->getMessage());
            throw $e;
        }
    }
}