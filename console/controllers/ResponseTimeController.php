<?php


namespace console\controllers;


use common\components\ConsoleController;
use common\models\Company;
use common\modules\company\repositories\ResponseTimeCalculator;

class ResponseTimeController extends ConsoleController
{
    /**
     * @var ResponseTimeCalculator
     */
    private $timeCalculator;

    public function __construct($id, $module, ResponseTimeCalculator $timeCalculator, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->timeCalculator = $timeCalculator;
    }

    public function actionRun()
    {
        $times = $this->timeCalculator->calc();
        foreach ($times as $time) {
            $company = Company::tryFindByPk($time['id']);
            if($time['hours'] !== null){
                $hours = explode(',', $time['hours']);
                sort($hours);
                $middleHour = $this->median($hours);
                $company->response_time = (int)$middleHour;
                $company->safeSave();
            }
        }
    }

    protected function median(array $values)
    {
        $count = count($values);
        $middleval = floor(($count-1)/2);
        if($count % 2) {
            $median = $values[$middleval];
        } else {
            $low = $values[$middleval];
            $high = $values[$middleval+1];
            $median = (($low+$high)/2);
        }
        return $median;
    }
}