<?php
/**
 * Date: 23.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace console\controllers;

use common\components\ConsoleController;
use lib\seo\sitemap\provider\SeoPageSitemapProvider;
use yii\console\Controller;
use yii\helpers\VarDumper;

use lib\seo\sitemap\SitemapBuilder;
use lib\seo\sitemap\SitemapConfig;
use lib\seo\sitemap\SitemapPing;
use lib\seo\sitemap\provider\CatalogLinkProvider;
use lib\seo\sitemap\provider\HelpLinkProvider;
use lib\seo\sitemap\provider\UserLinkProvider;
use lib\seo\sitemap\provider\BlogLinkProvider;
use lib\seo\sitemap\provider\Model3dLinkProvider;
use lib\seo\sitemap\provider\StaticPageLinkProvider;

class SitemapController extends ConsoleController
{
    protected function getWebRootPath()
    {
        return \Yii::getAlias(param('sitemapAlias'));
    }

    /**
     * build sitemap
     *
     * @throws \InvalidArgumentException
     */
    public function actionRun()
    {
        $sitemapBuilder = new SitemapBuilder();
        $sitemapConfig = new SitemapConfig([
            'host' =>  '', //param('server'),
            'outputDir' => $this->getWebRootPath(),
            'providersList' => [
                StaticPageLinkProvider::class,
                BlogLinkProvider::class,
                Model3dLinkProvider::class,
                HelpLinkProvider::class,
                UserLinkProvider::class,
                SeoPageSitemapProvider::class
            ]
        ]);
        $result = $sitemapBuilder->build($sitemapConfig);

        $this->stdout(VarDumper::dumpAsString($result) . "\n");
    }

    public function actionPing()
    {
        $ping = new SitemapPing();
        $result = $ping->pingGoogle(param('server') . '/sitemap_index.xml');
        $this->stdout('ping google result: ' . strip_tags($result) . "\n");
    }
}