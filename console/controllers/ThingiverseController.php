<?php
/**
 * User: nabi
 */

namespace console\controllers;


use common\components\ArrayHelper;
use common\components\ConsoleController;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\ThingiverseOrder;
use common\models\ThingiverseReport;
use common\models\User;
use common\models\UserEmailSent;
use common\modules\thingPrint\components\ThingiverseFacade;
use common\modules\thingPrint\components\ThingiverseOrderReview;
use common\modules\thingPrint\components\ThingiverseReportUpdater;
use common\services\StoreOrderService;
use frontend\models\ps\PsFacade;
use Yii;

class ThingiverseController extends ConsoleController
{

    /**
     * check for delivered and sent orders to send review email, default limit 10. usage with <limit=10> <expireDays=30>
     * get's order for last 30 days.
     *
     * @param int $limit
     * @param int $days
     */
    public function actionSendReview($limit = 10, $days = 30)
    {
        $thingiverseOrders = ThingiverseOrder::find()
            ->joinWith('order')
            ->joinWith('order.currentAttemp')
            ->joinWith('order.currentAttemp.dates')
            ->with('order')
            ->where(['store_order_attemp.status' => [StoreOrderAttemp::STATUS_DELIVERED, StoreOrderAttemp::STATUS_RECEIVED]])
            ->andWhere(['<', 'store_order_attemp_dates.delivered_at', dbexpr('NOW() - INTERVAL 24 HOUR')])
            ->andWhere(['>', 'store_order_attemp_dates.delivered_at', dbexpr('NOW() - INTERVAL ' . $days . ' DAY')])
            ->limit(1000)
            ->all();
        $emailer           = new Emailer();
        $limitIn           = $limit;
        foreach ($thingiverseOrders as $thingOrder) {
            if ($limit <= 0) {
                $this->println('[INFO] Limit finished: ' . $limitIn);
                break;
            }

            /** @var ThingiverseOrder $thingOrder */
            if (!$this->canSendEmail($thingOrder->order)) {
                continue;
            }
            $userEmail = ThingiverseOrderReview::getThingiverseUserEmail($thingOrder);
            if (!$userEmail) {
                $this->println('[WARNING] Email not specified by thing user ' . $thingOrder->order->user_id);
                continue;
            }
            $emailer->sendOrderReviewEmailThingiverse($thingOrder->order, $userEmail);
            $this->println('[INFO] Email sent to ' . $userEmail);
            $limit--;
        }
    }

    public function actionSetShipped($orderId)
    {
        $order = StoreOrder::tryFindByPk($orderId);
        if ($order->isThingiverseOrder()) {
            ThingiverseFacade::updateOrder($order, 1);
        }
        $this->println("Set as shipped " . $order->id);
    }

    private function canSendEmail(StoreOrder $order)
    {
        $code      = UserEmailSent::ORDER_REVIEW;
        $hash      = UserEmailSent::makeHash([$code => $order->id]);
        $sentEmail = UserEmailSent::find()->where(['user_id' => $order->user_id, 'email_code' => $code, 'hash' => $hash])->one();
        if ($sentEmail) {
            $this->stdout('[WARNING] Email already sent ' . $order->user_id . "\n");
            return false;
        }
        UserEmailSent::addRecord(
            [
                'user_id'    => $order->user_id,
                'email_code' => $code,
                'created_at' => dbexpr('NOW()'),
                'hash'       => $hash
            ]
        );
        return true;
    }

    public function actionDownloadReport()
    {
        $login    = param('thingiverse.login');
        $password = param('thingiverse.password');
        if (!$login || !$password) {
            echo "\nEmpty login or password. Skip report";
            return;
        }
        $thingiverseReportService = Yii::createObject(ThingiverseReportUpdater::class);
        $totalCount               = $thingiverseReportService->loadReport($login, $password);
        $this->stdout('Total added: ' . $totalCount);
    }

    public function actionFillNotExistsReports()
    {
        $existsOrders      = ThingiverseReport::find()->select('order_id')->asArray()->all();
        $existsOrderIds    = ArrayHelper::getColumn($existsOrders, 'order_id');
        $thingiverseOrders = ThingiverseOrder::find()
            ->where(['not in', 'thingiverse_order_id', $existsOrderIds])
            ->andWhere(['>', 'created_at', '2017-01-01'])
            ->all();
        foreach ($thingiverseOrders as $thingOrder) {
            $report           = Yii::createObject(ThingiverseReport::class);
            $report->order_id = $thingOrder->thingiverse_order_id;
            $report->status   = $thingOrder->status;
            $report->date     = $thingOrder->created_at;
            $report->thing_id = $thingOrder->thing_id . '';
            $report->safeSave();
        }
    }
}