<?php
namespace console\controllers;

use common\components\ConsoleController;
use common\modules\translation\components\Statistics;
use lib\translate\AutoTranslate;
use lib\translate\AutoTranslateRepository;
use lib\translate\GoogleTranslateApi;
use Yii;
use yii\helpers\Console;

class TranslateController extends ConsoleController
{
    /**
     * Form js translate files
     *
     */
    public function actionFormJs()
    {
        \Yii::$app->getModule('translation')->jsI18n->formAllTempAssetsFile();
    }

    /**
     * Scan files for new translations
     *
     */
    public function actionScanForTranslations()
    {
        \Yii::$app->getModule('translation')->scanner->scanForTranslations();
    }

    /**
     *
     * @param int $limit limit to translate
     * @param string $ln language
     */
    public function actionAuto($limit, $lang)
    {
        $isoCodes = AutoTranslateRepository::findAllActiveLanguages();
        if(!in_array($lang, $isoCodes)){
            $this->stderr('Language not found in ' . implode(",", $isoCodes) . "\n", Console::BG_RED);
            exit;
        }
        $autoTranslate = new AutoTranslate(new GoogleTranslateApi());
        //for ($i = 0; $i < $limit * 10; $i = $i + $limit) {
            AutoTranslateRepository::$limit = $limit;
            try{
                $this->stdout('Translating '); $this->stdout($lang, Console::BG_GREEN); $this->println('');
                $result = AutoTranslateRepository::findAllToTranslate($lang, true);
                $toTranslate =  $result['total'];
                if(count($toTranslate)===0 || $toTranslate===0){
                    $this->stderr("Nothing to translate\n", Console::BG_YELLOW);
                    exit;
                }
                $count = $autoTranslate->translate($toTranslate, $lang);
                $this->println(sprintf('Translated phrases %d out of %d, skipped: %d', $count, count($toTranslate), $result['skip']));
                sleep(1);
            }catch (\Exception $e){
                $this->stderr($e->getMessage());
                $this->println('');
            }
        //}
    }

    public function actionFormDbTranslations()
    {
        Yii::$app->getModule('translation')->dbI18n->formEmptyDbTranslations();
        echo "Done";
    }

    public function actionWordsCountCreate()
    {
        Statistics::fixWordscountSqlFunction();
    }
}