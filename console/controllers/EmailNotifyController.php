<?php
/**
 * Date: 30.11.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace console\controllers;


use common\components\ConsoleController;
use common\components\DateHelper;
use common\components\Emailer;
use common\components\order\TestOrderFactory;
use common\models\PaymentInvoice;
use common\modules\payment\components\PaymentUrlHelper;
use common\modules\product\interfaces\ProductInterface;
use common\models\PsPrinter;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\Model3d;
use common\models\User;
use common\models\UserEmailSent;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;
use yii\helpers\Html;
use yii\helpers\Json;

class EmailNotifyController extends ConsoleController
{
    public $sentToUser = [];

    public function actionSendNotify()
    {
        $this->sentToUser = [];
        // $this->sendPublishModelNotify();
        $this->sendPrinterTestOrderNotify();
        $this->sendDesignerAfterFirstPublish();
        // $this->sendDesignerMoreModelsUpload();
        $this->sendUserUnpaidNewOrder();
    }


    public function sendPublishModelNotify()
    {
        /** @var Model3d[] $model3ds */
        $model3ds = Model3d::find()
            ->joinWith('product_common')
            ->where(['product_common.product_status' => ProductInterface::STATUS_DRAFT, 'source' =>Model3d::SOURCE_WEBSITE])
            ->andWhere(['<', 'created_at', dbexpr('NOW() - INTERVAL 1 DAY')])
            ->andWhere(['>', 'created_at', dbexpr('NOW() - INTERVAL 3 DAY')])
            ->all();
        $dbTransaction = app('db')->beginTransaction();
        try {
            $unpublishedLink = Html::a(param('server') . '/my/store/category/-1', param('server') . '/my/store/category/-1');
            foreach ($model3ds as $model) {
                $hash = UserEmailSent::makeHash([UserEmailSent::AUTHOR_PUBLISH_MODEL => 1]); // only once
                $user = $model->user;
                if(!$user){
                    continue;
                }
                if(in_array($user->id, $this->sentToUser)){
                    continue;
                }
                $this->sendInnerMessage(
                    $user,
                    $hash,
                    UserEmailSent::AUTHOR_PUBLISH_MODEL,
                    [
                        'name'      => UserFacade::getFormattedUserName($user),
                        'modelLink' => $unpublishedLink
                    ]
                );
                $this->sentToUser[] = $user->id;
            }
            $dbTransaction->commit();
        } catch (\Exception $e) {
            $dbTransaction->rollBack();
            throw $e;
        }
    }


    private function sendPrinterTestOrderNotify()
    {
        $testUserId = TestOrderFactory::getTestOrderUserId();
        /** @var PsPrinter[] $psPrinters */
        $psPrinters = PsPrinter::find()->visible()
            ->withoutResolvedTestOrder()
            ->andWhere(['<', 'company_service.updated_at', dbexpr('NOW() - INTERVAL 1 DAY')])
            ->andWhere(['>', 'company_service.updated_at', dbexpr('NOW() - INTERVAL 3 DAY')])
            ->all();

        $psLink = Html::a(param('server') . '/mybusiness/company', param('server') . '/mybusiness/company');
        $dbTransaction = app('db')->beginTransaction();
        try {
            foreach ($psPrinters as $psPrinter) {
                // has test order or not?
                $hasTestOrder = StoreOrderAttemp::find()
                    ->joinWith('machine')
                    ->innerJoin('store_order', 'store_order_attemp.order_id=store_order.id')
                    ->where(['company_service.ps_printer_id' => $psPrinter->id, 'store_order.user_id' => $testUserId])->one();

                $hash = UserEmailSent::makeHash([UserEmailSent::REQUEST_TEST_ORDER => $psPrinter->ps->user_id]);
                if ($hasTestOrder) {
                    $this->sentToUser[] = $psPrinter->ps->user_id;
                    UserEmailSent::addRecord(
                        [
                            'user_id'    => $psPrinter->ps->user_id,
                            'email_code' => UserEmailSent::REQUEST_TEST_ORDER,
                            'created_at' => dbexpr('NOW()'),
                            'hash'       => $hash
                        ]
                    );
                    $this->stdout('[WARNING] Printer has test order: Printer id: ' . $psPrinter->id . "\n");
                    continue;
                }
                $user = $psPrinter->ps->user;
                $this->sendInnerMessage(
                    $user,
                    $hash,
                    UserEmailSent::REQUEST_TEST_ORDER,
                    [
                        'name'   => UserFacade::getFormattedUserName($user),
                        'psLink' => $psLink
                    ]
                );
                $this->sentToUser[] = $user->id;
            }
            $dbTransaction->commit();
        } catch (\Exception $e) {
            $dbTransaction->rollBack();
            throw $e;
        }
    }

    private function sendDesignerAfterFirstPublish()
    {
        /** @var Model3d[] $model3ds */
        $model3ds = Model3d::find()
            ->joinWith('productCommon')
            ->where(['product_common.product_status' => ProductInterface::STATUS_PUBLISHED_PUBLIC, 'source' =>Model3d::SOURCE_WEBSITE])
            ->andWhere(['<', 'product_common.created_at', dbexpr('NOW() - INTERVAL 2 DAY')])
            ->andWhere(['>', 'product_common.created_at', dbexpr('NOW() - INTERVAL 5 DAY')])
            ->all();
        $dbTransaction = app('db')->beginTransaction();
        try {
            foreach ($model3ds as $model) {
                $hash = UserEmailSent::makeHash([UserEmailSent::FULFILL_PROFILE => $model->user_id]);
                $user = $model->user;
                if(!$user){
                    continue;
                }
                if (in_array($user->id, $this->sentToUser)) {
                    $this->stdout('[WARNING][' . UserEmailSent::FULFILL_PROFILE . '] Email already sent to user ' . $user->id . "\n");
                    continue;
                } else {
                }
                $userPublicProfileUrl = UserUtils::getUserPublicProfileUrl($user, false);
                $publicProfileLink = Html::a($userPublicProfileUrl, $userPublicProfileUrl);
                $this->sendInnerMessage(
                    $user,
                    $hash,
                    UserEmailSent::FULFILL_PROFILE,
                    [
                        'name'     => UserFacade::getFormattedUserName($user),
                        'userLink' => $publicProfileLink,
                        'userURL' => $userPublicProfileUrl
                    ]
                );
                $this->sentToUser[] = $user->id;
            }
            $dbTransaction->commit();
        } catch (\Exception $e) {
            $dbTransaction->rollBack();
            throw $e;
        }
    }

    private function sendDesignerMoreModelsUpload()
    {
        /** @var Model3d[] $model3ds */
        $model3ds = Model3d::find()
            ->joinWith('productCommon')
            ->where(['product_common.product_status' => ProductInterface::STATUS_PUBLISHED_PUBLIC , 'source' =>Model3d::SOURCE_WEBSITE])
            ->andWhere(['is_hidden_in_store' => 0])
            ->andWhere(['<', 'product_common.created_at', dbexpr('NOW() - INTERVAL 7 DAY')])
            ->andWhere(['>', 'product_common.created_at', dbexpr('NOW() - INTERVAL 9 DAY')])
            ->all();
        $dbTransaction = app('db')->beginTransaction();
        try {
            $uploadLink = Html::a(param('server') . '/upload/', param('server') . '/upload/');
            foreach ($model3ds as $model) {
                $hash = UserEmailSent::makeHash([UserEmailSent::AUTHOR_BEST_MODEL => $model->user_id]);
                $user = $model->user;
                if(!$user){
                    continue;
                }
                if (in_array($user->id, $this->sentToUser)) {
                    $this->stdout('[WARNING][' . UserEmailSent::AUTHOR_BEST_MODEL . '] Email already sent to user ' . $user->id . "\n");
                    continue;
                } else {
                }

                $this->sendInnerMessage(
                    $user,
                    $hash,
                    UserEmailSent::AUTHOR_BEST_MODEL,
                    [
                        'name'       => UserFacade::getFormattedUserName($user),
                        'uploadLink' => $uploadLink
                    ]
                );
                $this->sentToUser[] = $user->id;
            }
            $dbTransaction->commit();
        } catch (\Exception $e) {
            $dbTransaction->rollBack();
            throw $e;
        }
    }

    private function sendUserUnpaidNewOrder()
    {
        $unpaidOrders = StoreOrder::find()
            ->inProcess()
            ->forPaymentStatuses(PaymentInvoice::STATUS_NEW)
            ->andWhere(['<', 'store_order.created_at', DateHelper::subNowSec(60*60*2)])
            ->andWhere(['>', 'store_order.created_at', DateHelper::subNowSec(60*60*5)])
            ->all();
        $dbTransaction = app('db')->beginTransaction();
        try {
            foreach ($unpaidOrders as $order) {
                $paidOrders = StoreOrder::find()
                    ->inProcess()
                    ->payed()
                    ->forUser($order->user)
                    ->andWhere(['<', 'store_order.created_at', DateHelper::subNowSec(60*60*1)])
                    ->andWhere(['>', 'store_order.created_at', DateHelper::subNowSec(60*60*24)])
                    ->count();
                if ($paidOrders > 0) {
                    $this->sentToUser[] = $order->user_id;
                    continue;
                }
                $link = PaymentUrlHelper::payInvoice($order->getPrimaryInvoice(), true);
                $hash = UserEmailSent::makeHash([UserEmailSent::USER_PAY_ORDER => $order->id]);
                $user = $order->user;
                if(!$user){
                    continue;
                }
                $this->sendInnerMessage(
                    $user,
                    $hash,
                    UserEmailSent::USER_PAY_ORDER,
                    [
                        'name'    => UserFacade::getFormattedUserName($user),
                        'htmlLink' => Html::a($link, $link),
                        'payLink' =>  $link
                    ]
                );
                $this->sentToUser[] = $user->id;
            }
            $dbTransaction->commit();
        } catch (\Exception $e) {
            $dbTransaction->rollBack();
            throw $e;
        }
    }


    private function getTemplate($key)
    {
        $emails = [
            UserEmailSent::AUTHOR_PUBLISH_MODEL => [
                'description' => 'After user upload model (not published) with suggestion to publish it.',
                'period'      => '1 day',
                'params'      => ['name', 'modelLink'],
                'title'       => 'You have an unpublished model on Treatstock',
                'body'        =>
                    'Hi %name%,

You recently uploaded model %modelLink% which remains unpublished. 
If you like, you can publish the model so that you can sell it in our catalog of 3D printable models and earn money each time it is printed. 
If you need help with publishing your model, please contact support at support@treatstock.com and we will be more than happy to help.

Best Regards, 
Treatstock'
            ],
            UserEmailSent::REQUEST_TEST_ORDER   => [
                'description' => ' After PS create printer with suggestion to Request Test Order',
                'period'      => '1 day',
                'params'      => ['name', 'psLink'],
                'title'       => 'Request test order',
                'body'        => 'Hi %name%,
Thank you for joining Treatstock as a print service!

To have your print service published, we require that you complete at least one test order on one of your 3D printers. 
Plus, it\'s a great way to familiarize yourself with how the orders process works and at the end, 
you will also receive a positive review which can help kick start real orders. 

Click \'Request test order\' in your print service page here to get started: %psLink%

Best Regards, 
Treatstock'
            ],

            UserEmailSent::FULFILL_PROFILE => [
                'description' => ' After designer publish first model suggestion to fill his profile: photo, skills',
                'period'      => '1 day',
                'params'      => ['name', 'userLink'],
                'title'       => 'Please complete your profile',
                'body'        => 'Hi %name%,

Congratulations on uploading your first model on Treatstock!

If you haven\'t already, we would like to suggest that you upload a profile picture and fill in the \'About Me\' 
and \'My skills and professional achievements\' sections in your profile. 
They are like your marketing tools that can help you to reach out to your customers and convince them to print your models or select you when they want to hire a designer for their ideas.

You can visit your profile page here %userLink%.

Best Regards, 
Treatstock'
            ],

            UserEmailSent::AUTHOR_BEST_MODEL => [
                'description' => '4. After designer publish first model information about Model of Month competition',
                'period'      => '1 week',
                'params'      => ['name', 'uploadLink'],
                'title'       => 'Best Model of the Month competition',
                'body'        => 'Hi %name%,

Did you know that there is a monthly competition for designers on Treatstock? Each month, a 3D model is crowned the "Best 3D Model of the Month”, with the winner determined by receiving the most number of likes in the month it was published.

The prize for winning the competition is your winning model will have access to prime real estate and be displayed on our homepage for all to see. The more models you upload, the better your chances of winning!

You can upload more models here %uploadLink%

Best Regards, 
Treatstock'
            ],
            UserEmailSent::USER_PAY_ORDER    => [
                'title'  => 'Order Requires Payment',
                'period' => '3 hours',
                'params' => ['name', 'payLink'],
                'body'   => 'Hi %name%,

You recently attempted to create an order but didn\'t complete the checkout process. The order will no longer be available once 24 hours has elapsed from the time you attempted to create the order. 
To return to your order and make payment so printing can begin, please click on the following link:  %payLink%

Best Regards, 
Treatstock'
            ]

        ];
        return $emails[$key];

    }

    private function sendInnerMessage(User $user, $hash, $code, $params)
    {
        $notifyConfig = !empty($user->notify_config) ? Json::decode($user->notify_config) : [];

        if (!empty($notifyConfig['news']) && !empty($notifyConfig['news']['email']) && $notifyConfig['news']['email'] === 'never') {
            $this->sentToUser[] = $user->id;
            $this->stdout('[INFO][news] user settings never notify - user ' . $user->id . "\n");
            return;
        }
        $sentEmail = UserEmailSent::find()->where(
            [
                'user_id'    => $user->id,
                'email_code' => $code,
                'hash'       => $hash
            ]
        )->one();
        if ($sentEmail) {
            $this->stdout('[WARNING] Email already sent ' . $user->id . "\n");
            return;
        }
        UserEmailSent::addRecord(
            [
                'user_id'    => $user->id,
                'email_code' => $code,
                'created_at' => dbexpr('NOW()'),
                'hash'       => $hash
            ]
        );
        if (in_array($user->id, $this->sentToUser)) {
            $this->stdout('[WARNING][' . $code . '] email already  sent to user ' . $user->id . "\n");
            return;
        } else {
            $this->stdout('[INFO][' . $code . '] email sent to user ' . $user->id . "\n");
        }
        if(empty($user->email)){
            return;
        }
        $emailer = new Emailer();
        $emailer->sendForce($user, $code, $params);

        /*
        $msg = MessageRenderHelper::renderTemplate(
            $tpl['body'],
            $params
        );
        $emailer->sendMessage($user->email, $tpl['title'], $msg);
        */
    }

}