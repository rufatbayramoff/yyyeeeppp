<?php

/**
 * Class for manage orders statuses
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */

namespace console\controllers;

use common\components\ConsoleController;
use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\Emailer;
use common\models\DeliveryType;
use common\models\FileJob;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\PaymentbtTransaction;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderAttempDelivery;
use common\models\StoreOrderReview;
use common\models\UserEmailSent;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\services\StoreOrderService;
use console\jobs\model3d\PrepareDowloadFilesJob;
use console\jobs\QueueGateway;
use frontend\models\order\CancelOrderForm;
use lib\delivery\carrier\models\Constants;
use lib\delivery\delivery\DeliveryFacade;
use Yii;
use yii\base\Module;
use common\components\ActiveQuery;

/**
 * Class OrdersController
 * @package console\controllers
 *
 * @property Emailer $emailer
 */
class OrdersController extends ConsoleController
{
    /**
     * @var StoreOrderService
     */
    private $orderService;

    protected $emailer;

    public function __construct($id, Module $module, StoreOrderService $orderService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->orderService = $orderService;

    }

    /**
     * @param Emailer $emailer
     */
    public function injectDependencies(Emailer $emailer): void
    {
        $this->emailer = $emailer;
    }

    /**
     * Cancel unpaid orders
     *
     * refs #1155
     */
    public function actionCancelUnpaid()
    {
        $expireAfterHours = param('unpaid_order_expire', 48);

        /** @var StoreOrder[] $orders */
        $ordersQuery = StoreOrder::find()
            ->notTest()
            ->notDispute()
            ->withoutBankInvoices()
            ->forPaymentStatuses(PaymentInvoice::STATUS_NEW)
            ->andWhere(['store_order.order_state' => StoreOrder::STATE_PROCESSING])
            ->andWhere(['<', 'store_order.created_at', DateHelper::subNowSec($expireAfterHours * 60 * 60)]);
        $orders      = $ordersQuery->all();

        foreach ($orders as $orderObj) {
            try {
                $this->orderService->declineOrder($orderObj, ChangeOrderStatusEvent::INITIATOR_SYSTEM, new CancelOrderForm(['reasonDescription' => 'Unpaid order cancelled']));
                $this->println("order " . $orderObj->id . " cancelled");
            } catch (\Exception $e) {
                $this->printErrln("order " . $orderObj->id . " cannot be cancelled");
            }
        }
    }

    /**
     *
     */
    public function actionSetChangePrinter()
    {
        $changePrinterHours = param('order_change_printer', 24);

        /** @var StoreOrderAttemp[] $attemps */
        $attemps = StoreOrderAttemp::find()
            ->withoutStaticCache()
            ->inStatus(StoreOrderAttemp::STATUS_NEW)
            ->joinWith(['order' => function (StoreOrderQuery $query) {
                $query
                    ->notTest()
                    ->notDispute()
                    ->canChangePs()
                    ->forPaymentStatuses([PaymentInvoice::STATUS_AUTHORIZED, PaymentInvoice::STATUS_PAID]);

            }])
            ->andWhere(['<', StoreOrderAttemp::column('created_at'), DateHelper::subNowSec($changePrinterHours * 60 * 60)])
            ->andWhere(StoreOrderAttemp::column('id') . ' = ' . StoreOrder::column('current_attemp_id'))
            ->all();


        foreach ($attemps as $attemp) {
            try {
                if (!empty($attemp->cancelled_at)) {
                    $cancelled = strtotime($attemp->cancelled_at);
                    $timeout   = strtotime(DateHelper::subNowSec(60 * 60 * $changePrinterHours)); // 1 day
                    if ($cancelled > $timeout) {
                        $this->println("order {$attemp->order_id} skipped ");
                        continue;
                    }
                }
                $this->println("order {$attemp->order->id} change printer status");
                $this->orderService->declineAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM, new CancelOrderForm(['reasonDescription' => "Go to change offer by attemp expired"]));
            } catch (\Exception $e) {
                $this->printErrln($e->getMessage());
            }
        }
    }

    /**
     *
     */
    public function actionCheckExpired()
    {
        $printedWaitTimeInSeconds = app('setting')->get('printer.printed_wait_time') * 60;

        /** @var StoreOrderAttemp[] $expiredAttemps */
        $expiredAttemps = StoreOrderAttemp::find()
            ->joinWith(['order' => function (StoreOrderQuery $query) {
                $query->notTest()->notDispute();
            }, 'delivery'])
            ->with('dates')
            ->inStatus([StoreOrderAttemp::STATUS_READY_SEND])
            ->andWhere([StoreOrderAttempDelivery::column('send_message_at') => null])
            ->all();

        $mailer = new Emailer();

        foreach ($expiredAttemps as $attemp) {
            if (!DeliveryFacade::getIsPostDelivery($attemp->order)) {
                continue;
            }

            $diff = time() - strtotime($attemp->dates->finish_print_at);
            if ($attemp->status == StoreOrderAttemp::STATUS_READY_SEND) {
                $diffRedyToSend = time() - strtotime($attemp->dates->ready_to_sent_at);
                if ($diffRedyToSend < 23 * 60 * 60) {
                    // wait 23 hours
                    $this->println('[SKIP] Order:' . $attemp->order_id . ' - wait 24 hours');
                    continue;
                } else {
                    $delivery                  = $attemp->delivery;
                    $delivery->send_message_at = dbexpr('NOW()');
                    AssertHelper::assertSave($delivery);

                    $mailer->sendDeliveryWait($attemp);
                    $this->println("Letter sent to user with order {$attemp->id}");
                }
            }
            /*
            if($diff >= $printedWaitTimeInSeconds)
            {
                $delivery = $attemp->delivery;
                $delivery->send_message_at = dbexpr('NOW()');
                AssertHelper::assertSave($delivery);

                $mailer->sendDeliveryWait($attemp);
                $this->println("Letter sent to user with order {$attemp->id}");
            } */
        }
    }


    /**
     * Cancel orders with overdue deadline more than 2 day
     * @link https://redmine.tsdev.work/issues/182
     */
    public function actionCancelPrintingOverdueOrders()
    {
        $maxOverdue   = ((int)\Yii::$app->setting->get('printer.max_print_overdue', 48)) * 3600;
        $deadlineTime = date('Y-m-d H:i:s', time() - $maxOverdue);

        $emailer = new Emailer();

        /** @var StoreOrderAttemp[] $overdueOrdersAttemps */
        $overdueOrdersAttemps = StoreOrderAttemp::find()
            ->inStatus([StoreOrderAttemp::STATUS_ACCEPTED, StoreOrderAttemp::STATUS_PRINTING])
            ->joinWith([
                'order' => function (StoreOrderQuery $query) {
                    $query->notTest()->notDispute();
                },
                'dates' => function (ActiveQuery $query) use ($deadlineTime) {
                    $query->andWhere(['<', 'plan_printed_at', $deadlineTime]);
                }
            ], false)
            ->all();

        $this->println('Found ' . count($overdueOrdersAttemps) . ' overdue orders');

        foreach ($overdueOrdersAttemps as $attemp) {
            try {
                $emailer->sendClientCancelOrderPrintingTimeExpired($attemp);
                $this->orderService->declineAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM, new CancelOrderForm(['reasonDescription' => 'Printing time expired']));
                $this->println("Attemp #{$attemp->id} success canceled");
            } catch (\Exception $e) {
                \Yii::error($e, 'order');
                $this->println("Attemp on cancel order #{$attemp->id}: {$e->getMessage()}");
            }
        }
    }

    /**
     *
     */
    public function actionSendMessages()
    {
        Yii::$app->orderNotify->sendPostponed();
    }

    /**
     * Set order in status DELIVERED to RECEIVED after it 7 days in DELIVERED status
     * @link https://redmine.tsdev.work/issues/2652
     */
    public function actionSetAsReceivedByTimeout()
    {
        $expiredTime = (new \DateTime())->sub(new \DateInterval("P7D"))->format('Y-m-d H:i:00');

        $this->println("Expired order date: {$expiredTime}");

        /** @var StoreOrderAttemp[] $expiredAttemps */
        $expiredAttemps = StoreOrderAttemp::find()
            ->inStatus(StoreOrderAttemp::STATUS_DELIVERED)
            ->joinWith([
                    'dates' => function (ActiveQuery $query) use ($expiredTime) {
                        $query->andWhere(['<', 'delivered_at', $expiredTime]);
                    },
                    'order' => function (StoreOrderQuery $query) {
                        $query->notTest()->notDispute();
                    }]
                , false)->all();
        //debugSql($expiredAttemps, true); exit;

        $this->println('Expired orders count: ' . count($expiredAttemps));


        foreach ($expiredAttemps as $attemp) {
            if ($attemp->order->thingiverseOrder) {
                continue;
            }
            try {
                $this->orderService->receivedAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM);
                $this->println("Expired order {$attemp->order->id} set as received");
            } catch (\Exception $e) {
                Yii::error($e);
                $this->println("[ERROR] Expired order {$attemp->order->id}. \n" . $e->getTraceAsString());

            }
        }

        /** Thingiverse orders - after 21 days */

        $expiredTime = (new \DateTime())->sub(new \DateInterval("P21D"))->format('Y-m-d H:i:00');
        $this->println("Expired thingiverse order date: {$expiredTime}");

        /** @var StoreOrderAttemp[] $expiredAttemps */
        $expiredAttemps = StoreOrderAttemp::find()
            ->inStatus(StoreOrderAttemp::STATUS_DELIVERED)
            ->joinWith([
                    'dates' => function (ActiveQuery $query) use ($expiredTime) {
                        $query->andWhere(['<', 'delivered_at', $expiredTime]);
                    },
                    'order' => function (StoreOrderQuery $query) {
                        $query->notTest()->notDispute();
                    }]
                , false)
            ->all();

        $this->println('Expired orders count: ' . count($expiredAttemps));

        foreach ($expiredAttemps as $attemp) {
            $this->orderService->receivedAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM);
            $this->println("Expired order {$attemp->order->id} set as received");
        }

        // pickup orders
        // 3. Pick up заказы после модерации результатов печати переходят в received через 7 дней.
        $expiredTimeWeek = (new \DateTime())->sub(new \DateInterval("P8D"))->format('Y-m-d 00:00:00');
        $this->println("Expired pickup order date: {$expiredTimeWeek}");
        $pickupAttemps = StoreOrderAttemp::find()
            ->inStatus([StoreOrderAttemp::STATUS_DELIVERED, StoreOrderAttemp::STATUS_SENT, StoreOrderAttemp::STATUS_READY_SEND])
            ->joinWith([
                    'dates' => function (ActiveQuery $query) use ($expiredTimeWeek) {
                        $query->andWhere(['<', 'delivered_at', $expiredTimeWeek]);
                    },
                    'order' => function (StoreOrderQuery $query) {
                        $query->notTest()->notDispute()->deliveryPickup();
                    }]
                , false)
            ->all();
        foreach ($pickupAttemps as $attemp) {
            $this->orderService->receivedAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM);
            $this->println("Pickup order {$attemp->order->id} set as received");
        }
    }

    /**
     * Set order as delivered
     */
    public function actionSetAsDelivered($debug = false)
    {
        // if it in easypost
        /** @var StoreOrderAttemp[] $orders */
        $attemps = StoreOrderAttemp::find()
            ->inStatus(StoreOrderAttemp::STATUS_SENT)
            ->joinWith(['order' => function (StoreOrderQuery $query) {
                $query
                    ->notTest()
                    ->notDispute()
                    ->notPickup()
                    ->treatstockCarrier();
            }], false)
            ->with('delivery');

        if ($debug) {
            debugSql($attemps, true);
            exit;
        }
        $attemps = $attemps->all();
        foreach ($attemps as $attemp) {
            $this->println("[INFO] Tracking: [" . $attemp->delivery->tracking_number . "] Carrier:  " . $attemp->delivery->tracking_shipper ?? '');
            try {
                $parcelStatus = \Yii::$app->carrierService->getParcelStatus($attemp->delivery->tracking_number, $attemp->delivery->tracking_shipper);
                if ($parcelStatus == Constants::DELIVERY_STATE_DELIVERED) {
                    $this->orderService->deliveredAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM);
                    $this->println("easypost order {$attemp->order->id} set as received");
                }
            } catch (\Exception $e) {
                Yii::error($e);
                $this->println('[ERROR] ' . $e->getMessage());
            }

        }

        // others
        // domestic shipping
        $expiredTime = (new \DateTime())->sub(new \DateInterval("P30D"))->format('Y-m-d');

        $this->println("Receive domestic order date: {$expiredTime}");
        $attempsDomestic = $this->getShippedAttemps($expiredTime, DeliveryType::STANDART_ID);
        foreach ($attempsDomestic as $attemp) {
            $this->orderService->deliveredAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM);
            $this->println("domestic order {$attemp->order->id} set as received");
        }

        // international shipping
        $expiredTimeIntl = (new \DateTime())->sub(new \DateInterval("P45D"))->format('Y-m-d');
        $this->println("Receive intl order date: {$expiredTimeIntl}");
        $attempsDomestic = $this->getShippedAttemps($expiredTimeIntl, DeliveryType::INTERNATIONAL_STANDART_ID);
        foreach ($attempsDomestic as $attemp) {
            $this->orderService->deliveredAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM);
            $this->println("intl order {$attemp->order->id} set as received");
        }
    }

    private function getShippedAttemps($expiredTime, $deliveryType)
    {
        $attemps = StoreOrderAttemp::find()
            ->inStatus(StoreOrderAttemp::STATUS_SENT)
            ->joinWith([
                'dates' => function (ActiveQuery $query) use ($expiredTime) {
                    $query->andWhere(['<', 'shipped_at', $expiredTime]);
                },
                'order' => function (StoreOrderQuery $query) use ($deliveryType) {
                    $query
                        ->notTest()
                        ->notDispute()
                        ->deliveryType($deliveryType)
                        ->notPickup();
                }], false)
            ->with('delivery')->all();
        return $attemps;
    }

    public function actionUpdatePaymentStatus()
    {
        $paymentTransactions = PaymentTransaction::find()->where([
            'type'   => PaymentTransaction::TYPE_PAYMENT,
            'vendor' => PaymentTransaction::VENDOR_BRAINTREE
        ])->andWhere(['status' => [
            PaymentGatewayTransaction::STATUS_AUTHORIZED,
            PaymentGatewayTransaction::STATUS_SETTLING,
            PaymentGatewayTransaction::STATUS_SUBMITED_SETTLE,
        ]])->andWhere(['<', 'updated_at', DateHelper::subNowSec(60 * 60 * 24)])->all();

        foreach ($paymentTransactions as $k => $paymentTransaction) {
            $checkoutObj = new PaymentCheckout($paymentTransaction->vendor);
            $transaction = $checkoutObj->getTransaction($paymentTransaction->transaction_id);

            if ($transaction && !empty($transaction['success']) && $transaction['success'] === true) {

                PaymentTransaction::updateTransactionStatus(
                    $paymentTransaction->transaction_id,
                    $paymentTransaction->vendor,
                    $transaction['status']
                );
                return true;
            }
            sleep(1); // api calls with delay
        }
        return null;
    }


    /**
     *
     */
    public function actionNotifyNeedSettled()
    {
        $emailer = new Emailer();

        /** @var PaymentTransaction[] $transactions */
        $transactions = PaymentTransaction::find()
            ->where(['<', 'created_at', dbexpr('NOW() - INTERVAL 4 DAY')])
            ->andWhere(['is_notified_need_settled' => 0, 'status' => PaymentGatewayTransaction::STATUS_AUTHORIZED])
            ->andWhere(['not in', 'vendor', [PaymentTransaction::VENDOR_THINGIVERSE, PaymentTransaction::VENDOR_TS, PaymentTransaction::VENDOR_BONUS]])
            ->all();

        foreach ($transactions as $transaction) {

            $transaction->is_notified_need_settled = 1;
            AssertHelper::assertSave($transaction);
            $storeOrder = $transaction->firstPaymentDetail->paymentDetailOperation->payment->paymentInvoice->storeOrder ?? null;
            if ($storeOrder) {
                $orderId          = $storeOrder->id;
                $backendOrderLink = Yii::$app->params['backendServer'] . '/store/store-order/view?id=' . $orderId;
                $emailer->sendSupportMessage(
                    "Set payment status for order {$orderId} to settled",
                    "Link to order : <a href='{$backendOrderLink}'>{$backendOrderLink}</a>"
                );

                $this->println("Send message for order {$orderId}");
            }
        }
    }

    public function actionNotifySentWithoutTracking()
    {
        $deliveryNoTrackingAttemps = StoreOrderAttemp::find()
            ->select(['store_order_attemp.id', 'store_order_attemp_dates.shipped_at', 'store_order.delivery_type_id'])
            ->joinWith(['order' => function (StoreOrderQuery $query) {
                $query->notTest()->notDispute();
            }, 'delivery'], false)
            ->joinWith('dates', false)
            ->inStatus([StoreOrderAttemp::STATUS_SENT])
            ->andWhere(dbexpr('IFNULL(store_order_attemp_delivery.tracking_number, "")="" AND NOW() > DATE_ADD(store_order_attemp_dates.shipped_at, INTERVAL 10 DAY)'))
            ->asArray()
            ->all();
        if (!$deliveryNoTrackingAttemps) {
            $this->println('Nothing to notify');
            return;
        }
        $emailer = new Emailer();
        foreach ($deliveryNoTrackingAttemps as $row) {
            $send = false;
            if ($row['delivery_type_id'] == DeliveryType::STANDART_ID) {
                $send = true;
            } else if ($row['delivery_type_id'] == DeliveryType::INTERNATIONAL_STANDART_ID) { // international
                if (strtotime($row['shipped_at']) <= strtotime("-20 days")) {
                    $send = true;
                }
            }
            if ($send) {
                $attemp  = StoreOrderAttemp::tryFindByPk($row['id']);
                $hash    = UserEmailSent::makeHash([UserEmailSent::ORDER_ATTEMPT_NO_TRACKING => $row['id']]);
                $hasSent = UserEmailSent::find()->where(
                    [
                        'email_code' => UserEmailSent::ORDER_ATTEMPT_NO_TRACKING,
                        'hash'       => $hash
                    ]
                )->withoutStaticCache()->exists();
                if ($hasSent) {
                    $this->stdout('[WARN] Email was already sent: ' . $row['id'] . "\n");
                    continue;
                }
                $emailer->sendOrderClientDomesticNoTrackingNumber($attemp);
                $this->println("[INFO] Send email for order {$attemp->order_id}");
                UserEmailSent::addRecord(
                    [
                        'user_id'    => $attemp->order->user_id,
                        'email_code' => UserEmailSent::ORDER_ATTEMPT_NO_TRACKING,
                        'created_at' => dbexpr('NOW()'),
                        'hash'       => $hash
                    ]
                );
            }
        }
    }

    /**
     * @param $jobFileId
     * @throws \Exception
     */
    public function actionPrepareDownloadFiles($jobFileId)
    {
        /** @var FileJob $fileJob */
        $fileJob = FileJob::find()->withoutStaticCache()->where(['id' => $jobFileId])->tryOne();

        /** @var PrepareDowloadFilesJob $job */
        $job       = \Yii::createObject(PrepareDowloadFilesJob::class);
        $job->args = $fileJob->getArgsData();
        QueueGateway::configureJob($job, $fileJob->getArgsData());

        try {
            $result = $job->doJob();
            $fileJob->setResult($result);
            $fileJob->markAsCompleted();
            echo "Done\n";
        } catch (\Exception $e) {
            $fileJob->markAsFailed($e->getMessage());
            throw $e;
        }
    }

    /**
     * Dispatch of notices of receipt of the order after an hour
     *
     * @throws \yii\web\NotFoundHttpException
     * @link https://redmine.tsdev.work/issues/5275
     */
    public function actionNotifySentClientReceivedOrder(): void
    {
        /** @var StoreOrderAttemp[] $attempts */
        $attempts = StoreOrderAttemp::find()
            ->from(['attemp' => StoreOrderAttemp::tableName()])
            ->where([
                'and',
                ['=', 'attemp.status', StoreOrderAttemp::STATUS_RECEIVED],
                [
                    'and',
                    ['<=', 'attemp.created_at', dbexpr('(CURRENT_TIMESTAMP() - INTERVAL 1 HOUR)')],
                    ['>=', 'attemp.created_at', dbexpr('(CURRENT_TIMESTAMP() - INTERVAL 2 HOUR)')]
                ]
            ])
            ->all();

        if (empty($attempts)) {
            $this->println('Nothing to notify');
            return;
        }

        foreach ($attempts as $attemp) {
            $hash = UserEmailSent::makeHash([UserEmailSent::ORDER_ATTEMPT_CLIENT_RECEIVED_ORDER => $attemp['id']]);

            $hasSent = UserEmailSent::find()->where([
                'email_code' => UserEmailSent::ORDER_ATTEMPT_CLIENT_RECEIVED_ORDER,
                'hash'       => $hash
            ])->withoutStaticCache()->exists();

            if ($hasSent) {
                $this->println("[WARN] Email was already sent StoreOrderAttemp: {$attemp['id']}");
                continue;
            }

            $attemp = StoreOrderAttemp::tryFindByPk($attemp['id']);

            $review = $attemp->getAttempReview();

            if ($review && $review->getRating() >= 4) {
                $this->emailer->sendClientReceivedOrderTrustpilot($attemp->order);
            } else {
                $this->emailer->sendClientReceivedOrder($attemp->order);
            }

            UserEmailSent::addRecord([
                'user_id'    => $attemp->order->user_id,
                'email_code' => UserEmailSent::ORDER_ATTEMPT_CLIENT_RECEIVED_ORDER,
                'created_at' => dbexpr('NOW()'),
                'hash'       => $hash
            ]);

            $this->println("[INFO] Send email for order {$attemp->order_id}");
        }
    }

    public function actionLastOrderMinutes()
    {
        date_default_timezone_set('UTC');
        $lastStoreOrder = StoreOrder::find()->payed()->notTest()->orderBy('store_order.created_at desc')->one();
        $diffTime       = time() - strtotime($lastStoreOrder->created_at);
        echo round($diffTime / 60) . '.' . $lastStoreOrder->id;
    }
}