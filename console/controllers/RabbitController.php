<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 02.03.17
 * Time: 16:07
 */

namespace console\controllers;

use AMQPEnvelope;
use common\components\ConsoleController;
use common\components\DateHelper;
use common\models\FileJob;
use console\jobs\QueueGateway;
use DateTime;
use DateTimeZone;
use Yii;
use yii\base\Exception;
use yii\helpers\Json;

class RabbitController extends ConsoleController
{
    public const QUEUE_RENDER                 = 'treatstock.render';
    public const QUEUE_ANALYZE_JOB            = 'treatstock.analyze-job';
    public const QUEUE_PARSER                 = 'treatstock.parser';
    public const QUEUE_PREPARE_DOWNLOAD_FILES = 'treatstock.prepare-download-files';
    public const QUEUE_CONVERT                = 'treatstock.converter';
    public const QUEUE_CUTTING_CONVERT        = 'treatstock.cutting-converter';

    public const LISTENER_MAP = [
        self::QUEUE_RENDER                 => [
            'command' => 'render/render-job',
            'timeout' => 600
        ],
        self::QUEUE_ANALYZE_JOB            => [
            'command' => 'cncm/analyze-job',
            'timeout' => 7200,
        ],
        self::QUEUE_PARSER                 => [
            'command' => 'parser/parser-job',
            'timeout' => 65
        ],
        self::QUEUE_PREPARE_DOWNLOAD_FILES => [
            'command' => 'orders/prepare-download-files',
            'timeout' => 1800
        ],
        self::QUEUE_CONVERT                => [
            'command' => 'convert-model3d/convert-job',
            'timeout' => 600,
        ],
        self::QUEUE_CUTTING_CONVERT        => [
            'command' => 'cutting/convert-cutting-job',
            'timeout' => 600,
        ]
    ];

    protected $listenQueueName;

    public function output($str)
    {
        $this->stdout("\n" . (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s') . ': ' . $str);
    }

    public function showPossibleQueueNames()
    {
        $this->output("\nPossible queue names: \n  " . implode("\n  ", array_keys(self::LISTENER_MAP)) . "\n");
    }

    public function actionListen($queueName = null)
    {
        if (!$queueName) {
            $this->showPossibleQueueNames();
            return;
        }
        $this->listenQueueName = $queueName;

        Yii::$app->db
            ->createCommand('SET wait_timeout = 259200; SET interactive_timeout = 259200;')
            ->execute();

        $this->output('Start listen: ' . $queueName . " \n");
        Yii::$app->rabbitMq->listenQueue($queueName, [$this, 'listener']);
    }

    /**
     * @param AMQPEnvelope $message
     * @throws Exception
     */
    public function listener(AMQPEnvelope $message)
    {
        [$class, $id] = Json::decode($message->getBody());
        $jobCommandInfo = self::LISTENER_MAP[$this->listenQueueName];
        $this->startJob($jobCommandInfo['command'], $jobCommandInfo['timeout'], $id);
    }

    protected function startJob($jobCommand, $jobTimeout, $jobId)
    {
        $this->checkDbConnection();

        $fileJob = FileJob::find()->withoutStaticCache()->where(['id' => $jobId])->tryOne();
        if ($fileJob) {
            if ($fileJob->status === QueueGateway::JOB_STATUS_WAITING || 1) {
                $this->output('Running ' . $fileJob->id . ': ' . $fileJob->operation);
                $fileJob->status = QueueGateway::JOB_STATUS_RUNNING;
                $fileJob->save(false);
                $this->runCommand($jobCommand, $jobTimeout, $fileJob);
            } else {
                $messageOutput = 'Job "' . $jobCommand . '" already in status: ' . $fileJob->status;
                $this->output($messageOutput);
                Yii::warning($messageOutput);
            }
        } else {
            $messageOutput = 'Job "' . $jobCommand . '" not found by id: ' . $jobId;
            $this->output($messageOutput);
            Yii::warning($messageOutput);
        }
    }

    /**
     * Check db connection and reopen it if db has gone away
     */
    private function checkDbConnection()
    {
        try {
            Yii::$app->db->createCommand('SELECT 1')->execute();
        } catch (Exception $e) {
            Yii::$app->db->close();
            Yii::$app->db->open();
        }
    }

    /**
     * @param string $jobCommand
     * @param FileJob $fileJob
     * @return mixed
     */
    protected function runCommand($jobCommand, $jobTimeout, $fileJob)
    {
        $command = 'timeout -k ' . ($jobTimeout + 10) . ' ' . $jobTimeout . ' php yii ' . $jobCommand . ' ' . $fileJob->id . ' 2>&1';
        $this->output('-------- Start command: ' . $command . ' --------');
        exec($command, $output, $returnValue);
        $outputString = substr(implode("\n", $output), 0, 60000);
        $fileJob      = FileJob::find()->withoutStaticCache()->where(['id' => $fileJob->id])->tryOne();
        if ($fileJob->status == QueueGateway::JOB_STATUS_RUNNING) {
            $fileJob->status = QueueGateway::JOB_STATUS_FAILED;
        }
        echo "\n" . $outputString;
        $fileJob->output      = $outputString;
        $fileJob->finished_at = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $fileJob->save(false);
        $this->output('=========== Finished ===========');
        return $output;
    }

    public function actionMarkFailedJobs()
    {
        QueueGateway::markFailedJobs();
    }

    public function actionRestartFailed($lastNHours)
    {
        $date = DateHelper::subNow('PT' . $lastNHours . 'H');
        $this->output('From date: ' . $date);

        if ($lastNHours > 1000) {
            $this->output('Too long date');
        }

        $fileJobs = FileJob::find()->withoutStaticCache()
            ->where(['>', 'created_at', $date])
            ->andWhere(['status' => [QueueGateway::JOB_STATUS_FAILED, QueueGateway::JOB_STATUS_RUNNING, QueueGateway::JOB_STATUS_WAITING]])
            ->andWhere(['<>', 'operation', FileJob::JOB_CUTTING_CONVERT])
            ->all();
        $doneList = [];
        foreach ($fileJobs as $fileJob) {
            $doneKey = $fileJob->operation . ':' . $fileJob->args;
            if (array_key_exists($doneKey, $doneList)) {
                $this->output("Skip repeat: " . $fileJob->id);
                continue;
            }
            $this->output("Restart: " . $fileJob->id);
            QueueGateway::repeat($fileJob);
            $doneList[$doneKey] = 1;
        }
        $this->output("Total: " . count($fileJobs) . "\n");
    }
}