<?php

namespace console\controllers;

use common\components\ConsoleController;
use common\components\DateHelper;
use common\models\PaymentBankInvoice;
use common\modules\payment\services\InvoiceBankService;

/**
 *
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class InvoiceBankController extends ConsoleController
{

    /**
     * @var InvoiceBankService
     */
    private $invoiceService;

    public function injectDependencies(InvoiceBankService $invoiceService)
    {
        $this->invoiceService = $invoiceService;
    }

    /**
     *
     */
    public function actionCheckExpired()
    {
        $invoices = PaymentBankInvoice::find()->where(['status' => PaymentBankInvoice::STATUS_NEW])
            ->andWhere(['<', 'date_due', DateHelper::now()])
            ->all();

        foreach ($invoices as $invoice) {
            $this->invoiceService->expireInvoice($invoice);
        }
    }
}
