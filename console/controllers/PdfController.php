<?php
namespace console\controllers;

use common\components\ConsoleController;
use Yii;

/**
 * Description of PdfController
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PdfController extends ConsoleController
{

    /**
     * 
     * @param type $file
     */
    public function actionGenerateHtmlFile($file)
    {
        //$fileText = '/home/user/web/tsgit/project2/doc/templates/tax/fw9_filled.pdf';
        $file = Yii::getAlias('@doc/templates/' . $file);
        $pdfForm = new \lib\pdf\PdfForm($file, 0);
        $pdfForm->generateHtml('output/autohtml', true);        
    }
    
    /**
     * get fields from given PDF file
     * 
     * @param type $file
     */
    public function actionGetFdf($file)
    {
        $file = Yii::getAlias('@doc/templates/' . $file);
        $pdfForm = new \lib\pdf\PdfForm($file, 0);
        $userData = range(0, 150);
        foreach($userData as $k=>$v){
            $userData[$k] = "{IN_" . $k. "}";
        }
        $pdfForm->fillForm($userData);
        $this->stdout("Done", \yii\helpers\Console::FG_GREEN);
    }
    
    /**
     * generate HTML templates from PDF
     * turned off for now, 
     */
    public function actionGenerateHtml()
    {
        $this->stderr("Use pdf/generate-html-file <file>", \yii\helpers\Console::BG_RED);
        $this->stderr("\n");
        return false;
        /*
        $files = glob(\Yii::getAlias('@doc/templates/tax/*')); 
        
        $userData = range(0, 150);
        foreach($userData as $k=>$v){
            $userData[$k] = "{IN_" . $k. "}";
        }
        foreach($files as $k=>$v){
            if(strpos($v, '.pdf')===false){
                continue;
            }
            $pdfForm = new \lib\pdf\PdfForm($v, 0);
            $result = $pdfForm->fillForm($userData);
            $this->stdout($result . "\n");
            // generate html template
            $pdfForm2 = new \lib\pdf\PdfForm($result, 0);
            $pdfForm2->generateHtml('autohtml');
            
        } */
    }
}
