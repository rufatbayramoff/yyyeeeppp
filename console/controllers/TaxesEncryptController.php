<?php namespace console\controllers;

use common\components\ConsoleController;

/**
 * Description of TaxesController
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class TaxesEncryptController  extends ConsoleController
{
    
    public function actionEncrypt($userId = 0)
    {
        $form = new \common\components\security\EncrypterForm();
        $where = ['encrypted'=>0];
        if($userId > 0){
            $where['user_id'] = $userId;
        }
        $taxes = \common\models\base\UserTaxInfo::findAll($where);
        foreach($taxes as $tax)
        {
            $encrypted = $form->encrypt($tax->identification);
            $tax->identification = $encrypted;
            $tax->encrypted = 1;
            $tax->safeSave(); 
            echo "\nEncoded:". $tax->id . "\n";
        }
    }

    /**
     * decrypt all identification in user_tax_info
     * and save as encrypted=0
     * Use taxes-encrypt/encrypt to encrypt again.
     *
     * @param $password
     */
    public function actionDecrypt($password)
    {
        try{
            $form = new \common\components\security\EncrypterForm();
            $form->passphrase = $password;
            $where = ['encrypted'=>1];
            $taxes = \common\models\base\UserTaxInfo::findAll($where);
            foreach($taxes as $tax)
            {
                $encrypted = $form->decrypt($tax->identification);
                echo $tax->id , " : " , $encrypted . "\n\n";
                $tax->identification = $encrypted;
                $tax->encrypted = 0;
                $tax->safeSave();
            }
        }catch(\Exception $e){
            echo $e->getMessage() . "\n\n";
        }
    }
    
    public function actionDecode($password, $userId = 0)
    {
        try{
            $form = new \common\components\security\EncrypterForm();
            $form->passphrase = $password;
            $where = ['encrypted'=>1];
            if($userId){
                $where = ['user_id'=>$userId];
            }
            $taxes = \common\models\base\UserTaxInfo::findAll($where);

            foreach($taxes as $tax)
            {
                $encrypted = $form->decrypt($tax->identification);
                echo $tax->id , " : " , $encrypted . "\n\n";
            }
        }catch(\Exception $e){
            echo $e->getMessage() . "\n\n";
            // 411111111
        }
    }
}
