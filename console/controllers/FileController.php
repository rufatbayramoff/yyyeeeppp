<?php

namespace console\controllers;

use common\components\ConsoleController;
use common\components\DateHelper;
use common\components\FileDirHelper;
use common\models\ApiPrintablePack;
use common\models\File;
use common\models\FileCompressed;
use common\models\repositories\FileRepository;
use common\models\repositories\Model3dRepository;
use Yii;
use yii\console\ExitCode;

class FileController extends ConsoleController
{

    public function checkFile(File $file)
    {
        $path = $file->getLocalTmpFilePath();

    }

    public function log($text)
    {
        echo date('Y-m-d H:i:s') . ' ' . $text . "\n";
    }

    public function actionCreateDbFilesMap()
    {
        /** @var File[] $allFiles */
        $this->log('Create db files map.');
        $allFiles = File::find()->all();
        $filesDbPath = [];
        $this->log('Db loaded.');
        foreach ($allFiles as $file) {
            $path = $file->getLocalTmpFilePath();
            $path = str_replace('//', '/', $path);
            $filesDbPath[$path] = $file->id;
        }
        file_put_contents(Yii::getAlias('@runtime') . '/filesDbMap.json', json_encode($filesDbPath));
        $this->log('Done.');
    }

    protected function generateFilesList($folder)
    {
        $extList = [];
        foreach (param('fileCompression')['decompressor'] as $ext => $compressorParams) {
            $extList[] = '.' . $ext;
        }
        $allPathList = [];
        $iti = new \RecursiveDirectoryIterator($folder, \RecursiveDirectoryIterator::SKIP_DOTS);
        foreach (new \RecursiveIteratorIterator($iti) as $file) {
            $path = $file->getPathname();
            if (strpos($path, '/forDelete/')) {
                continue;
            }
            if (!is_file($path)) {
                continue;
            }
            $path = str_replace($extList, '', $path);
            $allPathList[$path] = filesize($path);
        }
        return $allPathList;
    }

    public function actionCreateDiskFilesMap()
    {
        if (!$this->confirm('Are you sure you want to create disk files map?')) {
            return ExitCode::OK;
        }
        /** @var File[] $allFiles */
        $this->log('Create disk files map.');
        $dirsListStorage = $this->generateFilesList(Yii::getAlias('@storage'));
        $dirsListStatic = $this->generateFilesList(Yii::getAlias('@static/files'));
        $dirsListStaticUser = $this->generateFilesList(Yii::getAlias('@static/user'));

        $allFilesList = $dirsListStorage + $dirsListStatic + $dirsListStaticUser;
        file_put_contents(Yii::getAlias('@runtime') . '/filesDiskMap.json', json_encode($allFilesList));
        $this->log('Done.');


    }

    protected function filterNotExistsInDisk($filesList)
    {
        $resultList = [];
        foreach ($filesList as $filePath => $fileId) {
            $file = File::tryFindByPk($fileId);
            if ($file->status === 'active') {
                $resultList[$filePath] = $fileId;
            }
        }
        return $resultList;
    }

    /**
     * Find files witch not exists in disk, and exists in database.
     * Find files witch not exists in database, and exists in disk.
     *
     * @param bool $recalcMaps
     */
    public function actionFilesDbDiskDiff($recalcMaps = true)
    {
        if (!$this->confirm('Are you sure you want to run files db disk diff?')) {
            return ExitCode::OK;
        }
        if ($recalcMaps) {
            $this->actionCreateDbFilesMap();
            $this->actionCreateDiskFilesMap();
        }
        $this->log('Calc diff maps.');
        $filesDbPath = json_decode(file_get_contents(Yii::getAlias('@runtime') . '/filesDbMap.json'), true);
        $filesDiskPath = json_decode(file_get_contents(Yii::getAlias('@runtime') . '/filesDiskMap.json'), true);
        $notExistsInDb = array_diff_key($filesDiskPath, $filesDbPath);
        $notExistsInDisk = array_diff_key($filesDbPath, $filesDiskPath);
        $notExistsInDisk = $this->filterNotExistsInDisk($notExistsInDisk);

        file_put_contents(Yii::getAlias('@runtime') . '/filesNotExistsInDbMap.json', json_encode($notExistsInDb, JSON_PRETTY_PRINT));
        file_put_contents(Yii::getAlias('@runtime') . '/filesNotExistsInDiskMap.json', json_encode($notExistsInDisk, JSON_PRETTY_PRINT));
        $this->log('Done.');
    }

    public function actionMoveToDeleteNotUsedFiles()
    {
        if (!$this->confirm('Are you sure you want to MOVE to delete not used files?')) {
            return ExitCode::OK;
        }
        $this->log('Move files to "forDelete" dir.');
        FileDirHelper::createDir('/var/www/treatstock/frontend/storage/forDelete/');
        $notExistsInDbFiles = json_decode(file_get_contents(Yii::getAlias('@runtime') . '/filesNotExistsInDbMap.json'), true);
        foreach ($notExistsInDbFiles as $path => $size) {
            if (strpos($path, '/var/www/treatstock/frontend/storage/') === 0) {
                if (file_exists($path)) {
                    $this->log($path);
                    $newPath = str_replace('/var/www/treatstock/frontend/storage/', '/var/www/treatstock/frontend/storage/forDelete/', $path);
                    $newPathDir = substr($newPath, 0, strrpos($newPath, '/'));
                    FileDirHelper::createDir($newPathDir);
                    FileDirHelper::renameForNfs($path, $newPath);
                }
            }
            if (strpos($path, '/var/www/treatstock/frontend/web/static/files/') === 0) {
                if (file_exists($path)) {
                    $this->log($path);
                    $newPath = str_replace('/var/www/treatstock/frontend/web/static/', '/var/www/treatstock/frontend/web/static/forDelete/', $path);
                    $newPathDir = substr($newPath, 0, strrpos($newPath, '/'));
                    FileDirHelper::createDir($newPathDir);
                    FileDirHelper::renameForNfs($path, $newPath);
                }
            }
            if (strpos($path, '/var/www/treatstock/frontend/web/static/user/') === 0) {
                if (strpos($path, '/render/') || strpos($path, '/model')) {
                    if (file_exists($path)) {
                        $this->log($path);
                        $newPath = str_replace('/var/www/treatstock/frontend/web/static/', '/var/www/treatstock/frontend/web/static/forDelete/', $path);
                        $newPathDir = substr($newPath, 0, strrpos($newPath, '/'));
                        FileDirHelper::createDir($newPathDir);
                        FileDirHelper::renameForNfs($path, $newPath);
                    }
                }
            }
        }
        $this->log('Done.');
    }


    public function actionArchiveFiles()
    {
        $params = param('fileCompression');
        echo "\nCreate files list.\n";
        /** @var File[] $files */
        $files = File::find()
            ->where(['extension' => $params['compressExtension'], 'is_public' => 0])
            ->andWhere(['<', 'created_at', DateHelper::subNowSec($params['compressFileExpire'])])
            ->joinWith('fileCompressed')
            ->andWhere('file_compressed.file_id is null')
            ->limit(10000)
            ->all();
        $countFiles = count($files);
        echo 'Count files: ' . $countFiles . "\n";
        $i = 1;
        foreach ($files as $file) {
            $currentPath = $file->getLocalTmpFilePath();
            if (!file_exists($currentPath)) {
                echo 'File not exists: ' . $currentPath . "\n";
                continue;
            }
            $command = sprintf($params['compressor'], $currentPath);
            echo 'Compress ' . $i . '/' . $countFiles . '. Id: ' . $file->id . ' Name: ' . $file->getFileName() . ' Path:' . $file->getLocalTmpFilePath() . ' Size:' . $file->size . ' ';
            exec($command, $output, $returnVar);
            if ($returnVar !== 0) {
                echo "- failed.\n";
                return;
            }
            $fileCompressed = new FileCompressed();
            $fileCompressed->date = DateHelper::now();
            $fileCompressed->file_id = $file->id;
            $fileCompressed->unpacked = 0;
            $fileCompressed->safeSave();
            echo "- done.\n";
            $i++;
        }
    }

    public function actionCleanArchiveUnpackedFiles()
    {
        $params = param('fileCompression');
        $files = File::find()
            ->where(['<', 'last_access_at', DateHelper::subNowSec($params['compressFileExpire'])])
            ->joinWith('fileCompressed')
            ->andWhere('file_compressed.unpacked = 1')
            ->limit(1000)
            ->all();

        $countFiles = count($files);
        echo 'Count files: ' . $countFiles . "\n";
        $i = 1;

        foreach ($files as $file) {
            $file->setIgnoreAccessDateMode(true);
            $path = $file->getLocalTmpFilePath();
            echo 'Compress ' . $i . '/' . $countFiles . '. Id: ' . $file->id . ' Name: ' . $file->getFileName() . ' Path:' . $file->getLocalTmpFilePath() . ' Size:' . $file->size . ' ';
            $notExists = true;
            foreach (param('fileCompression')['decompressor'] as $ext => $compressorParams) {
                if (file_exists($path . '.' . $ext)) {
                    unlink($path);
                    $file->fileCompressed->unpacked = 0;
                    $file->fileCompressed->safeSave();
                    echo "- done.\n";
                    $notExists = false;
                }
            }
            if ($notExists) {
                echo "- skipped ($ext file not exists).\n";
            }
        }
    }

    public function actionFixEmptyMd5()
    {
        if (!$this->confirm('Are you sure you want to fix empty md5 files in database?')) {
            return ExitCode::OK;
        }
        $files = File::find()->md5('')->all();
        foreach ($files as $file) {
            $path = $file->getLocalTmpFilePath();
            if (file_exists($path)) {
                $file->md5sum = md5_file($file->getLocalTmpFilePath());
                $file->safeSave();
            } else {
                echo "\n Not exits. Id: " . $file->id . " Path: " . $path;
            }
        }
    }

    public function actionInvalidList()
    {
        if (!$this->confirm('Are you sure you want to get invalid list of files by md5 in database?')) {
            return ExitCode::OK;
        }
        $files = File::find()->all();
        foreach ($files as $file) {
            echo "\nId: " . $file->id;
            $path = $file->getLocalTmpFilePath();
            if (!file_exists($path)) {
                echo "\nNot exits. Id: " . $file->id . ' Path: ' . $path;
                continue;
            }
            $diskMd5 = md5_file($path);
            if ($diskMd5 != $file->md5sum) {
                echo "\nIvalid md5. Id: " . $file->id . ' Path: ' . $path . ' BaseMd5: ' . $file->md5sum . ' DiskMd5: ' . $diskMd5;
                continue;
            }

        }
    }

    /**
     * @param int $debug - if enabled, the files are not deleted
     * @param int $allDelete - 0 - Delete only files older than 7 days / 1 - Delete all files older than 7 - all days
     * @param int $limit
     * @return int
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionCleanOldApiFiles($debug = 1, $allDelete = 0, $limit = 1000)
    {
        echo "\nStart with flags debug: " . $debug . '. All delete: ' . $allDelete . '. Limit: ' . $limit . ".\n";
        $query = ApiPrintablePack::find()
            ->from(['app' => ApiPrintablePack::tableName()])
            ->limit($limit);

        $query->andWhere(['<', 'created_at', DateHelper::subNow('P14D')]);

        if (!$allDelete) {
            $query->andWhere(['>', 'created_at', DateHelper::subNow('P16D')]);
        }

        /** @var ApiPrintablePack[] $apiPrintablePacks */
        $apiPrintablePacks = $query->all();

        if (!$apiPrintablePacks) {
            echo "ApiPrintablePack: no files\n";
            return ExitCode::OK;
        }

        foreach ($apiPrintablePacks as $apiPrintablePack) {
            if ($apiPrintablePack->model3d->canDeleted()) {
                if (!$debug) {
                    Model3dRepository::destroyModel3d($apiPrintablePack->model3d);

                    echo "REMOVED: file id: {$apiPrintablePack->model3d->cover_file_id} file date: {$apiPrintablePack->created_at}, model id: {$apiPrintablePack->model3d->id}\n";
                } else {
                    echo "file id: {$apiPrintablePack->model3d->cover_file_id} file date: {$apiPrintablePack->created_at}, model id: {$apiPrintablePack->model3d->id}\n";
                }
            } else {
                echo "Model id: {$apiPrintablePack->model3d->id} - can not be deleted\n";
            }
        }

        return ExitCode::OK;
    }

    /**
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionFixPublicFiles()
    {
        $fileRepository = \Yii::createObject(FileRepository::class);
        $brokenFiles = File::find()->where("`is_public`=1 and `server`!='localhost'")->all();
        foreach ($brokenFiles as $brokenFile) {
            $pathOld = $path = $brokenFile->getLocalTmpFilePath();
            if (!file_exists($path)) {
                echo "\nSkip " . $brokenFile->id . ' not exists: ' . $path;
                continue;
            }

            $brokenFile->setOldAttribute('is_public', 0);
            $fileRepository->save($brokenFile);
            echo "\nProcessed: " . $brokenFile->id . ' old path: ' . $pathOld . ' new path: ' . $brokenFile->getLocalTmpFilePath();
        }
        echo "\n";
    }
}

