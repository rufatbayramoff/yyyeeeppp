<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace console\controllers;


use common\modules\message\services\CheckDeliveryService;
use common\modules\unsubscribe\services\AutoUnsubscribeService;
use lib\message\Worker;
use Yii;

class MessagesController extends \common\components\ConsoleController
{

    /**
     * Send messages from queue
     */
    public function actionSendMessages()
    {
        $worker = new Worker();
        $worker->work();
    }

    public function actionCheckDeliveryImap()
    {
        $module               = Yii::$app->getModule('message');
        $checkDeliveryService = Yii::createObject(CheckDeliveryService::class);
        $checkDeliveryService->autoCheckDeliveryImap(
            host: $module->checkDeliveryConfig['host'],
            port: $module->checkDeliveryConfig['port'],
            login: $module->checkDeliveryConfig['login'],
            password: $module->checkDeliveryConfig['password']
        );
        echo "Done\n";
    }

    public function actionCheckDeliveryLog()
    {
        $module               = Yii::$app->getModule('message');
        $checkDeliveryService = Yii::createObject(CheckDeliveryService::class);
        $checkDeliveryService->parseFailedAttemptsLog($module->localDeliveryLogPath);
        echo "Done\n";
    }
}