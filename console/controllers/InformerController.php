<?php
namespace console\controllers;


use common\components\ConsoleController;
use common\components\Emailer;
use common\modules\informer\services\InformerService;

class InformerController  extends ConsoleController
{
    /**
     * @var InformerService $informerService
     */
    protected $informerService;

    /**
     * @param Emailer $emailer
     */
    public function injectDependencies(InformerService $informerService)
    {
        $this->informerService = $informerService;
    }


    public function actionRemoveOldNotifications()
    {
        $this->informerService->removeOldInformer();
    }
}