<?php


namespace console\controllers;
use common\components\ConsoleController;
use common\components\FileDirHelper;
use Yii;
use yii\helpers\Console;
use yii\helpers\Inflector;

class BaseModelsController extends ConsoleController
{
    /**
     * 
     * @param string $user
     * @param string $password
     * @return int
     */
    public function actionCreateUser($user, $password)
    {
        // create admin
        $obj = new \common\models\UserAdmin();
        $obj->username = $user;
        $data = [
            'username' => $obj->username ,
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'email' => $obj->username  . '@tsdev.work',
            'status' => 10,
            'created_at' => time(),
            'updated_at' => time()
        ];
        $obj->load(['data'=>$data], 'data');
        $obj->setPassword($password);
        if($obj->save()){
            echo 'admin created' . PHP_EOL;
        }else{
            echo \yii\helpers\Html::errorSummary($obj, []);
        }
        $userId = $obj->id;

        $siteUser = \common\models\User::findByPk($userId);
        if(!$siteUser){
            \common\models\User::addRecord([
                "id" => $userId,
                "username" => $obj->username ,
                "auth_key" => "-",
                'password_hash' => '-',
                'email' => $obj->username  . '@tsdev.work',
                'status' => 10,
                'created_at' => 0,
                'updated_at' => 0,
                'lastlogin_at' => 0,
                'trustlevel' => 'high'
            ]);
        }
        return 0;
    }

    public function actionCncGenerateModels()
    {
        Yii::$app->getModule('cnc')->schemaParser->generateModels();
    }

    /**
     * @return array|mixed
     * @todo after refactor run application to runAction (like self::actionGenerate) this method will be removed
     */
    private function getYiiConfiguration()
    {
        if (isset($GLOBALS['config'])) {
            $config = $GLOBALS['config'];
        } else {
            $config = \yii\helpers\ArrayHelper::merge(
                require(\Yii::getAlias('@app') . '/../common/config/main.php'),
                (is_file(\Yii::getAlias('@app') . '/../common/config/main-local.php')) ?
                    require(\Yii::getAlias('@app') . '/../common/config/main-local.php')
                    : [],
                require(\Yii::getAlias('@app') . '/../console/config/main.php'),
                (is_file(\Yii::getAlias('@app') . '/../console/config/main-local.php')) ?
                    require(\Yii::getAlias('@app') . '/../console/config/main-local.php')
                    : []
            );
        }
        return $config;
    }
}