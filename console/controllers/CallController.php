<?php
/**
 * User: nabi
 */

namespace console\controllers;


use common\components\ConsoleController;
use lib\sms\TwilioGateway;

class CallController extends ConsoleController
{

    public function actionCall($to)
    {
        /** @var TwilioGateway $tw */
        $tw = \Yii::createObject([
            'class' => '\lib\sms\TwilioGateway',
        ]);
        $tw->call($to);
    }
}