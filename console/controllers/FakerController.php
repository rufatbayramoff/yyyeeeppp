<?php namespace console\controllers;

use common\components\ArrayHelper;
use common\components\ConsoleController;
use common\models\BrowserPushSubscription;
use common\models\CompanyService;
use common\models\ContentFilterStatistics;
use common\models\DynamicField;
use common\models\File;
use common\models\GeoLocation;
use common\models\HttpRequestLog;
use common\models\Model3dPartCncParams;
use common\models\Model3dPartProperties;
use common\models\Model3dTexture;
use common\models\Model3dTextureCatalogCost;
use common\models\ModerLog;
use common\models\MsgTopic;
use common\models\NotifyMessage;
use common\models\Payment;
use common\models\PaymentExchangeRate;
use common\models\PaymentLog;
use common\models\PaymentPayPageLogProcess;
use common\models\PaymentPayPageLogStatus;
use common\models\PaymentTransaction;
use common\models\Preorder;
use common\models\Product;
use common\models\ProductCategory;
use common\models\ProductCommon;
use common\models\PsPrinter;
use common\models\SeoPage;
use common\models\SeoPageAutofill;
use common\models\StoreOrderAttempDates;
use common\models\SystemLangMessage;
use common\models\SystemLangPage;
use common\models\SystemLangSource;
use common\models\ThingiverseReport;
use common\models\User;
use common\models\UserAdmin;
use common\models\UserCollection;
use common\models\UserComment;
use common\models\UserEmailChangeRequest;
use common\models\UserEmailLogin;
use common\models\UserEmailSent;
use common\models\UserHistory;
use common\models\UserLoginLog;
use common\models\UserOsn;
use common\models\UserSession;
use common\models\UserSms;
use common\models\WidgetStatistics;
use common\modules\dynamicField\services\DynamicFieldService;
use common\modules\product\factories\ProductFactory;
use common\modules\product\populators\ProductPopulator;
use common\modules\product\repositories\ProductRepository;
use lib\money\Currency;
use Yii;
use yii\console\ExitCode;
use yii\helpers\Console;
use yii\helpers\Json;

/**
 * FakerController - fake data generator for database
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class FakerController extends ConsoleController
{
    private $totalUserCreateTries = 0;

    public function init()
    {
    }

    private function execute($sql)
    {
        app('db')->createCommand($sql)->execute();
    }

    /**
     * @param int $limit
     * @param int $categoryId
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionProducts($limit = 1000, $categoryId = 28)
    {
        $category = ProductCategory::tryFindByPk($categoryId);

        $productPopulator  = Yii::createObject(ProductPopulator::class);
        $productRepository = Yii::createObject(ProductRepository::class);
        $productFactory    = Yii::createObject(ProductFactory::class, [
            'user' => User::tryFindByPk(1017)
        ]);

        $dynamicFieldService   = Yii::createObject(DynamicFieldService::class);
        $dynamicFieldsCategory = $dynamicFieldService->getModelFieldsInCategory(Product::class, $category);
        $faker                 = \Faker\Factory::create();
        for ($i = 0; $i < $limit; $i++) {
            $product             = $productFactory->create();
            $dynamicFieldsValues = $this->generateDynamicFields($product, $dynamicFieldsCategory);
            $post                = [
                $product->formName() => [
                    'title'                 => $faker->words(mt_rand(1, 4), true),
                    'description'           => $faker->words(mt_rand(1, 4), true),
                    'user_id'               => mt_rand(1017, 1025),
                    'category_id'           => $categoryId,
                    'company_id'            => 14,
                    'dynamic_fields_values' => $dynamicFieldsValues
                ]
            ];
            $productPopulator
                ->populate($product, $post);
            $product->company_id = 14;

            if ($product->validate()) {
                $productRepository->saveProduct($product);
            } else {
                print_r($product->getErrors());
            }
        }
    }

    /**
     * @param $product
     * @param $dynamicFieldsCategory
     * @return array
     */
    public function generateDynamicFields(Product $product, $dynamicFieldsCategory)
    {
        $result = [];
        $faker  = \Faker\Factory::create();

        foreach ($dynamicFieldsCategory as $field) {
            /** @var DynamicField $field */
            switch ($field->type) {
                case DynamicField::TYPE_STRING:
                    $result[$field->code] = $faker->text(50);
                    break;
                case  DynamicField::TYPE_BOOLEAN:
                    $result[$field->code] = $faker->boolean;
                    break;
                case  DynamicField::TYPE_NUMBER:
                    $result[$field->code] = $faker->numberBetween(1, 1000);
                    break;
                case  DynamicField::TYPE_ENUM:
                    $enumValues           = Json::decode($field->type_params);
                    $rand                 = mt_rand(1, count($enumValues));
                    $result[$field->code] = $enumValues[$rand - 1];
                    break;
                default:
                    $result[$field->code] = 0;
            }
        }
        return $result;
    }

    public function actionPrintersClean()
    {
        $this->execute("SET FOREIGN_KEY_CHECKS=0;");
        $this->execute("DELETE FROM user WHERE id>1000");
        $this->execute("DELETE FROM user_location WHERE id>1000");
        $this->execute("DELETE FROM ps WHERE id>1000");
        $this->execute("DELETE FROM ps_printer WHERE id>1000");
        $this->execute("DELETE FROM ps_printer_material WHERE id>1000");
        $this->execute("DELETE FROM ps_printer_color WHERE id>1000");
        $this->execute("DELETE FROM ps_printer_delivery WHERE ps_printer_id>1000");
        echo 'done';
    }

    private function generateUser($city, $i)
    {
        $faker = \Faker\Factory::create();
        if ($i == 0)
            $i = '';
        try {
            /** @var \common\models\User $user */
            $user = \common\models\User::addRecord([
                'username'      => $faker->userName . $i,
                'email'         => $faker->email,
                'status'        => 1,
                'created_at'    => $faker->unixTime(),
                'updated_at'    => $faker->unixTime(),
                'lastlogin_at'  => $faker->unixTime(),
                'trustlevel'    => 'normal',
                'auth_key'      => 'fake',
                'password_hash' => 'fake'
            ]);
        } catch (\Exception $e) {
            $this->totalUserCreateTries++;
            if ($this->totalUserCreateTries < 5) {
                return $this->generateUser($city, $i);
            }
            return false;
        }
        \common\models\UserProfile::addRecord([
            'user_id'              => $user->id,
            'dob_date'             => $faker->dateTimeBetween('-40 years', '-20 years')->format("Y-m-d"),
            //'address' => $faker->address,
            'website'              => substr($faker->url, 0, 30),
            'phone'                => '',
            'phone_confirmed'      => 0,
            'updated_at'           => $faker->dateTimeBetween('-40 days')->format('Y-m-d H:i:s'),
            'current_lang'         => 'en-US',
            'current_currency_iso' => $city->country->currency_code,
            'current_metrics'      => $city->country->iso_code == 'US' ? 'in' : 'cm',
            'timezone_id'          => $faker->timezone,
            'full_name'            => $faker->firstName . ' ' . $faker->lastName,
            'is_printservice'      => 1,
            'is_modelshop'         => 1,
            'default_license_id'   => 1,
        ]);
        return $user;
    }


    private function getCurrencyByCountry($countryId)
    {
        $list = [
            233 => Currency::USD,
            76  => 'GBP',
            56  => 'EUR',
            37  => Currency::USD,
        ];
        return $list[$countryId];
    }

    public function actionIndex($tableName)
    {
        $fixturePath = '@tests/unit/fixtures/data/' . $tableName . '.php';
        $fixtureData = Yii::getAlias($fixturePath);
        if (!file_exists($fixtureData)) {
            echo 'fixture data not found ' . $fixturePath;
            return 1;
        } else {
            $data = require_once($fixtureData);
        }
        $tblClass = implode("", array_map('ucfirst', explode("_", $tableName)));

        $modelClass = 'common\\models\\' . $tblClass;
        if (!class_exists($modelClass)) {
            echo 'model class not found for table : ' . $modelClass;
            return 1;
        }
        // generate
        $this->runAction('fixture/generate', ['table' => $tableName, 'count' => $count]);

        // get table model
        $tableModelOrig = Yii::createObject($modelClass);
        $totalRows      = count($data);
        Console::startProgress(0, $totalRows);
        foreach ($data as $k2 => $row) {
            $tableModel = clone $tableModelOrig;
            foreach ($row as $k => $v) {
                $tableModel->$k = $v;
            }
            $tableModel->safeSave();
            unset($tableModel);
            Console::updateProgress(($k2 + 1), $totalRows);
        }
        Console::endProgress();
        return 0;
    }

    public function actionUsers()
    {
        $tableName          = 'user';
        $tableProfile       = 'user_profile';
        $fixturePath        = '@tests/unit/fixtures/data/' . $tableName . '.php';
        $fixturePathProfile = '@tests/unit/fixtures/data/' . $tableProfile . '.php';
        $fixtureData        = Yii::getAlias($fixturePath);
        $fixturePathProfile = Yii::getAlias($fixturePathProfile);
        if (!file_exists($fixtureData)) {
            echo 'fixture data not found ' . $fixturePath;
            return 1;
        } else if (!file_exists($fixturePathProfile)) {
            echo 'fixture data not found ' . $fixturePathProfile;
            return 1;
        } else {
            $data         = require_once($fixtureData);
            $dataProfiles = require_once($fixturePathProfile);
        }
        $tblClass  = implode("", array_map('ucfirst', explode("_", $tableName)));
        $tblClass2 = implode("", array_map('ucfirst', explode("_", $tableProfile)));

        $modelClass   = 'common\\models\\' . $tblClass;
        $modelProfile = 'common\\models\\' . $tblClass2;

        if (!class_exists($modelClass)) {
            echo 'model class not found for table : ' . $modelClass;
            return 1;
        }

        // get table model
        $tableModelOrig        = Yii::createObject($modelClass);
        $tableModelProfileOrig = Yii::createObject($modelProfile);
        $totalRows             = count($data);
        Console::startProgress(0, $totalRows);
        foreach ($data as $k2 => $row) {
            $tableModel = clone $tableModelOrig;
            foreach ($row as $k => $v) {
                $tableModel->$k = $v;
            }
            $tableModel->updated_at   = time();
            $tableModel->lastlogin_at = time();
            $tableModel->safeSave();
            if ($tableModel->hasErrors()) {
                print_r($tableModel->getErrors());
                exit;
            }
            $userId = $tableModel->id;
            unset($tableModel);
            // create profile
            $profileModel          = clone $tableModelProfileOrig;
            $dataProfile           = $dataProfiles[$k2];
            $profileModel->user_id = $userId;
            foreach ($dataProfile as $k => $v) {
                if ($k == 'trust_level') {
                    continue;
                }
                $profileModel->$k = $v;
            }
            $profileModel->safeSave();
            Console::updateProgress(($k2 + 1), $totalRows);
        }
        Console::endProgress();
        return 0;
    }

    public function actionDataTestCascadeMigrate()
    {
        if (YII_ENV !== 'dev') {
            echo "Only for the development environment\n";
            return ExitCode::OK;
        }
        $schema = Yii::$app->db->getSchema();
        $tables = $schema->getTableNames();
//        $skipTables = true;
        foreach ($tables as $tableName) {
//            if ($tableName =='user_osn') {
//                $skipTables = false;
//            }
//            if ($skipTables) {
//                continue;
//            }
            $tableSchema = Yii::$app->db->getTableSchema($tableName);
            foreach ($tableSchema->foreignKeys as $fkName => $fkDescr) {
                $fkTableRef = $fkDescr[0];
                unset($fkDescr[0]);
                $fkField         = key($fkDescr);
                $fkTableRefField = $fkDescr[$fkField];
                $sqlCommand      = 'ALTER TABLE `' . $tableName . '` DROP FOREIGN KEY `' . $fkName . '`; ALTER TABLE `' . $tableName . '` ADD CONSTRAINT `' . $fkName . '` FOREIGN KEY (`' . $fkField . '`) REFERENCES `' . $fkTableRef . '`(`' . $fkTableRefField . '`) ON DELETE CASCADE ON UPDATE RESTRICT;';
                $timeStart       = time();
                echo "\n" . $sqlCommand . ' ';
                Yii::$app->db->createCommand($sqlCommand)->execute();
                echo (time() - $timeStart) . ' sec';
            }
        }
        echo "\nDone";
    }

    public function actionDataTestClean()
    {
        echo "\nDeleting user ...";
        Yii::$app->db->createCommand('set innodb_lock_wait_timeout=1000')->execute();
        $users = User::find()
            ->notSystem()
            ->notDeveloperAccounts()
            ->notTreatstockAccounts()
            ->where('id>1100')
            ->asArray()
            ->all();
        foreach ($users as $user) {
            User::deleteAll(['id'=>$user['id']]);
            echo '.';
        }

        echo "\nDeleting user_session ...";
        do {
            $rows = UserSession::deleteAll('id!=1 limit 20000');
            echo '.';
        } while ($rows);

        echo "\nBrowserPushSubscription ...";
        BrowserPushSubscription::deleteAll();
        echo "\nContentFilterStatistics ...";
        ContentFilterStatistics::deleteAll();
        echo "\nNotifyMessage ...";
        NotifyMessage::deleteAll();
        echo "\nMsgMember ...";
        MsgTopic::deleteAll('id>3');
        echo "\nModerLog ...";
        ModerLog::deleteAll();
        echo "\nModel3dTextureCatalogCost ...";
        do {
            $rows = Model3dTextureCatalogCost::deleteAll('1=1 limit 100000');
            echo '.';
        } while ($rows);
        echo "\nModel3dTexture ...";
        do {
            $rows = Model3dTexture::deleteAll('1=1 limit 100000');
            echo '.';
        } while ($rows);
        echo "\nModel3dPartProperties ...";
        do {
            $rows = Model3dPartProperties::deleteAll('1=1 limit 1000000');
            echo '.';
        } while ($rows);
        echo "\nModel3dPartCncParams ...";
        Model3dPartCncParams::deleteAll();
        echo "\nHttpRequestLog ...";
        HttpRequestLog::deleteAll();
        echo "\nGeoLocation ...";
        GeoLocation::deleteAll();
        echo "\nPayment ...";
        Payment::deleteAll('id!=1');
        echo "\nPaymentLog ...";
        PaymentLog::deleteAll();
        echo "\nPaymentPayPageLogProcess ...";
        PaymentPayPageLogProcess::deleteAll();
        echo "\nPaymentPayPageLogStatus ...";
        PaymentPayPageLogStatus::deleteAll();
        echo "\nProduct common ...";
        ProductCommon::deleteAll();
        echo "\nSeoPageAutofill ...";
        SeoPage::deleteAll();
        echo "\nSeoPageAutofill ...";
        SeoPageAutofill::deleteAll();
        echo "\nStoreOrderAttempDates ...";
        StoreOrderAttempDates::deleteAll();
        echo "\nSystemLangMessage ...";
        SystemLangMessage::deleteAll();
        echo "\nSystemLangPage ...";
        SystemLangPage::deleteAll();
        echo "\nSystemLangSource ...";
        SystemLangSource::deleteAll();
        echo "\nThingiverseReport ...";
        ThingiverseReport::deleteAll();
        echo "\nUserComment ...";
        UserComment::deleteAll();
        echo "\nUserCollection ...";
        UserCollection::deleteAll();
        echo "\nUserLoginLog ...";
        UserLoginLog::deleteAll();
        echo "\nUserEmailSent ...";
        UserEmailSent::deleteAll();
        echo "\nUserHistory ...";
        UserHistory::deleteAll();
        echo "\nWidgetStatistics ...";
        WidgetStatistics::deleteAll();
        echo "\nPaymentExchangeRate ...";
        PaymentExchangeRate::deleteAll('id!=1');
        echo "\nFile ...";
        do {
            $rows = File::deleteAll('1=1 limit 10000');
            echo '.';
        } while ($rows);
        echo "\nPsPrinter ...";
        $psPrinterIds = ArrayHelper::getColumn(CompanyService::find()->where('ps_printer_id is not null')->all(), 'ps_printer_id');
        PsPrinter::deleteAll(['not in', 'id', $psPrinterIds]);
    }

    /**
     * @param int $printRes
     * @param int $limit
     *
     * @return int
     */
    public function actionDataBaseObfuscator($printRes = 1, $limit = 500000)
    {
        if (YII_ENV !== 'dev') {
            echo "Only for the development environment\n";
            return ExitCode::OK;
        }

        $fakeNames = Yii::getAlias("@console/fixture/fakeNames.php");

        if (!file_exists($fakeNames)) {
            echo "fakeNames data not found\n";
            return ExitCode::OK;
        }

        $fakeNames = require_once($fakeNames);

        $names    = $fakeNames['names'];
        $surnames = $fakeNames['surnames'];

        $namesCount    = count($names);
        $surnamesCount = count($surnames);

        /** @var User[] $users */
        $users = User::find()
            ->notSystem()
            ->notDeveloperAccounts()
            ->notTreatstockAccounts()
            ->limit($limit)
            ->all();

        $usersCount = count($users);

        foreach ($users as $k => $user) {
            // new name
            $obfName     = $names[$user->id % $namesCount];
            $obfSurname  = $surnames[$user->id % $surnamesCount];
            $obfFullName = $names[$user->id % $namesCount] . ' ' . $surnames[$user->id % $surnamesCount];
            $login       = mb_strtolower(str_replace([' ', '’'], '_', $obfFullName)) . '_' . $user->id;
            $webSite     = 'http://www.' . mb_strtolower(str_replace(' ', '-', $obfFullName)) . '.com';

            // new email
            if (strpos($user->email, '@') !== false) {
                $obfEmail = $login . substr($user->email, strpos($user->email, '@'), \strlen($user->email));
            } else {
                $obfEmail = $login . "@fake.com";
            }

            if (strlen($obfEmail) > 45) {
                $obfEmail = $user->id . "@fake.com";
            }

            // new phone
            $obfPhone = '+1';

            $lenFullName = \strlen($obfFullName) - 1;

            for ($i = 0; $i <= 9; $i++) {
                $n        = $lenFullName < $i ? $lenFullName : $i;
                $obfPhone .= (ord($obfFullName[$n]) % 10);
            }

            $user->email         = $obfEmail;
            $user->username      = $login;
            $user->password_hash = '$2y$13$TdAqF4UcRgQ4if/lfD4M8.tn.4TLNmvW4PghcZqMRDYbiYIfCHszG';

            if ($user->save(true, ['email', 'username', 'password_hash'])) {
                // userProfile
                $userProfile = $user->userProfile;

                if ($userProfile) {
                    $userProfile->full_name = $obfFullName;
                    $userProfile->firstname = $obfName;
                    $userProfile->lastname  = $obfSurname;
                    $userProfile->phone     = $obfPhone;
                    $userProfile->website   = $webSite;

                    $userProfile->save(true, ['full_name', 'firstname', 'lastname', 'phone', 'website']);
                }
                // ------

                // userAddresses
                $userAddresses = $user->userAddresses;

                foreach ($userAddresses as $address) {
                    $address->contact_name = !empty($address->contact_name) ? $obfFullName : '';
                    $address->phone        = !empty($address->phone) ? $obfPhone : '';
                    $address->email        = !empty($address->email) ? $obfEmail : '';

                    $address->save(true, ['contact_name', 'phone', 'email']);
                }
                // ------

                // userEmailChangeRequests
                UserEmailChangeRequest::updateAll(['email' => $obfEmail], ['user_id' => $user->id]);
                // ------

                // userEmailLogins
                UserEmailLogin::updateAll(['email' => $obfEmail], ['user_id' => $user->id]);
                // ------

                // userOsns
                UserOsn::updateAll(['email' => $obfEmail, 'username' => $obfFullName], ['user_id' => $user->id]);
                // ------

                Preorder::updateAll(['email' => 'quote-' . $obfEmail, 'name' => $obfFullName], ['user_id' => $user->id]);

                // userPaypal
                $userPaypal = $user->userPaypal;

                if ($userPaypal) {
                    $userPaypal->email    = $obfEmail;
                    $userPaypal->fullname = $obfFullName;

                    $userPaypal->save(true, ['email', 'fullname']);
                }
                // ------

                // userSms
                UserSms::updateAll(['phone' => $obfPhone], ['user_id' => $user->id]);
                // ------

                // userTaxInfo
                $userTaxInfo = $user->userTaxInfo;

                if ($userTaxInfo) {
                    $userTaxInfo->name = !empty($userTaxInfo->name) ? $obfFullName : '';

                    $userTaxInfo->save(true, ['name']);
                }
                // ------

                // company
                $company = $user->company;

                if ($company) {
                    $company->phone   = str_replace('+1', '', $obfPhone);
                    $company->website = $webSite;
                    $company->save(true, ['phone', 'phone_code', 'website']);
                }
                // ------

                if ($printRes) {
                    echo "#" . ($k + 1) . " / " . $usersCount . "\n";
                    echo "ID:        $user->id\n";
                    echo "full name: $obfFullName\n";
                    echo "Login:     $login\n";
                    echo "email:     $obfEmail\n";
                    echo "phone:     $obfPhone\n";
                    echo "web site:  $webSite\n";
                    echo "Memory:    " . ((memory_get_usage() / 1024) / 1024) . " mb.\n";
                    echo "\n";

                    //print_r([$obfFullName, $login, $webSite, $obfEmail, $obfPhone]);
                }
            }

            // Reset admin pass
            UserAdmin::updateAll(['password_hash' => '$2y$13$TdAqF4UcRgQ4if/lfD4M8.tn.4TLNmvW4PghcZqMRDYbiYIfCHszG']);
        }
        PaymentTransaction::updateAll(['amount' => '3', 'details' => ["accountDetails" => "To bill gates"]], ['vendor' => 'bank_payout']);
    }
}
