<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.09.16
 * Time: 15:28
 */

namespace console\controllers;


use common\components\ConsoleController;

class CatalogCostController extends ConsoleController
{
    public function actionResetAllCatalogCosts()
    {
        \Yii::$app->db->createCommand()->truncateTable('catalog_cost')->execute();
        \Yii::$app->db->createCommand('DELETE FROM model3d_texture_catalog_cost')->execute();
    }
}