<?php
/**
 * Created by mitaichik
 */

namespace console\controllers;


use common\components\ConsoleController;
use common\components\DateHelper;
use common\components\order\TestOrderFactory;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\Preorder;
use common\services\StoreOrderService;
use frontend\modules\preorder\components\PreorderService;

/**
 * Class PreorderController
 * @package console\controllers
 */
class PreorderController extends ConsoleController
{
    /** @var PreorderService */
    protected $preorderService;

    public function injectDependencies(PreorderService $preorderService): void
    {
        $this->preorderService = $preorderService;
    }

    /**
     * Decline preorders who wait confirm and created more than 7 days
     */
    public function actionCloseWaitConfirm()
    {
        /** @var Preorder[] $preorders */
        $preorders = Preorder::find()
            ->newOrWaitConfirm()
            ->andWhere(['<=',Preorder::column('created_at'), DateHelper::subNow('P2M')])
            ->all();

        foreach ($preorders as $preorder) {
            $this->preorderService->declineOfferByModerator($preorder);
        }
    }
}