<?php

namespace console\controllers;

use common\components\ConsoleController;
use common\components\DateHelper;
use common\models\InstantPayment;
use common\modules\instantPayment\services\InstantPaymentService;
use common\modules\payment\services\PaymentService;

/**
 *
 */
class InstantPaymentController extends ConsoleController
{

    /**
     * @var InstantPaymentService
     */
    protected $instantPaymentService;

    /** @var PaymentService */
    protected $paymentService;

    public function injectDependencies(InstantPaymentService $instantPaymentService, PaymentService $paymentService)
    {
        $this->instantPaymentService = $instantPaymentService;
        $this->paymentService = $paymentService;
    }


    public function actionAutoSettle()
    {
        /** @var InstantPayment[] $instantPayments */
        $instantPayments = InstantPayment::find()
            ->waitingForConfirm()
            ->authorized()
            ->andWhere(['<','payed_at',DateHelper::subNow('P3D')])->all();

        foreach ($instantPayments as $instantPayment) {
            try {
                $this->paymentService->submitForSettleInvoice($instantPayment->primaryInvoice);
            } catch (\Braintree\Exception $braintreeException) {
                echo "\nInstant payment ".$instantPayment->uuid." settle failed: ".$braintreeException->getMessage();
            }
            echo "\nSettled ".$instantPayment->uuid;
        }
    }

    /**
     * Approve expire Dates
     */
    public function actionApproveExpired()
    {
        /** @var InstantPayment[] $instantPayments */
        $instantPayments = InstantPayment::find()->waitingForConfirm()->andWhere(['<','payed_at',DateHelper::subNow('P1M')])->all();

        foreach ($instantPayments as $instantPayment) {
            $this->instantPaymentService->approve($instantPayment);
            echo "\nApproved ".$instantPayment->uuid;
        }
    }
}
