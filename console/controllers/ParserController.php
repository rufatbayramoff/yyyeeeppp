<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.08.16
 * Time: 10:54
 */

namespace console\controllers;

use common\components\ConsoleController;
use common\components\DateHelper;
use common\components\Emailer;
use common\models\File;
use common\models\FileJob;
use common\models\Model3dPart;
use console\jobs\model3d\ParserJob;
use console\jobs\QueueGateway;
use lib\message\senders\EmailSender;
use yii\db\Exception;


class ParserController extends ConsoleController
{
    /** @var  Emailer $emailer */
    public $emailer;

    public function injectDependencies(Emailer $emailer)
    {
        $this->emailer = $emailer;
    }

    public function actionParserJob($jobFileId)
    {
        /** @var FileJob $fileJob */
        $fileJob = FileJob::find()->withoutStaticCache()->where(['id' => $jobFileId])->tryOne();

        /** @var ParserJob $job */
        $job = \Yii::createObject(ParserJob::class);
        $job->args = $fileJob->getArgsData();
        QueueGateway::configureJob($job, $fileJob->getArgsData());
        $job->setFileJob($fileJob);

        try {
            $result = $job->doJob();
            $fileJob->setResult($result);
            $fileJob->markAsCompleted();
            $this->stdout("\nResult : " . json_encode($result));
        } catch (\Exception $e) {
            $fileJob->markAsFailed($e->getMessage());
            throw $e;
        }
    }

    /**
     * You can set directly model3dPartId
     *
     * @param null $model3dPart
     */
    public function actionParseAllFiles($emptySupports = null, $model3dPartId = null)
    {
        /** @var Model3dPart[] $model3dParts */
        $findCriteria = Model3dPart::find()->joinWith('model3dPartProperties');
        if ($model3dPartId) {
            $model3dPartId = explode(",", $model3dPartId);
            $findCriteria->where(['id' => $model3dPartId]);
        }
        if ($emptySupports) {
            $findCriteria->where(['model3d_part_properties.supports_volume' => null]);
        }
        $model3dParts = $findCriteria->all();
        $this->stdout('Total model3dparts: ' . count($model3dParts));
        foreach ($model3dParts as $model3dPart) {
            $pJob = ParserJob::create($model3dPart->file);
            $fileJob = new FileJob();
            $fileJob->args = '{"file_id":' . $model3dPart->file->id . '}';
            $fileJob->operation = FileJob::JOB_PARSER;
            $fileJob->file_id = $model3dPart->file->id;
            $fileJob->created_at = DateHelper::now();
            $fileJob->status = QueueGateway::JOB_STATUS_RUNNING;
            $fileJob->safeSave();
            $pJob->setFileJob($fileJob);
            $this->stdout("\nModel3d " . $model3dPart->model3d->id . " PartId: " . $model3dPart->id . ' - ');
            try {
                $result = $pJob->doJob();
                $fileJob->setResult($result);
                $this->stdout('Success: ' . json_encode($result));
                $fileJob->markAsCompleted();
            } catch (\Exception $e) {
                $fileJob->output = $e->getMessage();
                $this->stdout('Error: ' . $e->getMessage());
                $fileJob->markAsFailed();
            }
        }
        $this->stdout("\n");
    }
}