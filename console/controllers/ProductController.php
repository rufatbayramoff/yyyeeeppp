<?php

namespace console\controllers;


use common\components\BaseActiveQuery;
use common\components\ConsoleController;
use common\models\ProductCategory;
use common\models\ProductCommon;
use common\models\query\PsQuery;
use common\modules\product\interfaces\ProductInterface;

/**
 * Class ProductController
 * @package console\controllers
 */
class ProductController extends ConsoleController
{
    /**
     *
     */
    public function actionSetVisibleCategories()
    {
        // Select only child categories
        /** @var ProductCategory[] $productCategories */

        $transaction = \Yii::$app->db->beginTransaction();
        ProductCategory::updateAll(['is_visible'=>0]);
        $productCategories = ProductCategory::find()->leftJoin('product_category as pc', 'product_category.id=pc.parent_id')->where('pc.id is null')->all();
        $totalCnt          = count($productCategories);
        echo 'Total: ' . $totalCnt . " child categories\n";
        $i          = 0;
        $percentOld = null;
        foreach ($productCategories as $productCategory) {
            if($productCategory->id==97) {
                sleep(0);
            }
            $productsQuery = $productCategory->getProductCommons()->joinWith(['company' => function (PsQuery $psQuery) {
                $psQuery->active();
                $psQuery->moderated();
            }])
            ->andWhere(['product_common.is_active' => 1])
            ->andWhere(['product_common.product_status' => ProductInterface::PUBLIC_STATUSES]);
            $count = $productsQuery->count();
            if ($count>0) {
                $productCategory->setAsVisible();
            }
            $percent = (int)($i / $totalCnt * 10);
            if ($percent != $percentOld) {
                echo $percent . '0% ';
                $percentOld = $percent;
            }
            $i++;
        }
        echo "100%\n";
        $transaction->commit();
        unlink(BaseActiveQuery::getCacheDependencyFilePath(ProductCategory::class));
    }


    public function actionCount()
    {
        $products = ProductCommon::find()
            ->joinWith(['company' => function (PsQuery $psQuery) {
                $psQuery->active();
                $psQuery->moderated();
            }],false)
            ->andWhere(['product_common.is_active' => 1])
            ->andWhere(['product_common.product_status' => ProductInterface::PUBLIC_STATUSES])
            ->andWhere(['IS NOT','product_common.category_id',null])
            ->groupBy('product_common.category_id')
            ->select(['cnt' => 'count(*)','category_id' => 'product_common.category_id'])
            ->asArray()
            ->all();
        foreach ($products as $product) {
            $productCategory = ProductCategory::findOne($product['category_id']);
            /** @var ProductCategory[] $parents */
            $parents = $productCategory->allParents();
            foreach ($parents as $parent) {
                if($parent->parent_id) {
                    $parent->total_count += $product['cnt'];
                    $parent->safeSave();
                }
            }
            $productCategory->total_count = $product['cnt'];
            $productCategory->safeSave();
        }
    }
}