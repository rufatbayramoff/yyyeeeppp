<?php
/**
 * Date: 05.07.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace console\controllers;


use common\components\ConsoleController;
use common\models\ApiFile;
use common\models\ApiPrintablePack;

class ApiPrintablePackageController extends ConsoleController
{

    public function actionCheckExpired()
    {
        $expireAfterHours = 24*5;

        $expiredPackages = ApiPrintablePack::find()
            ->andWhere(['status' => [ApiPrintablePack::STATUS_ACTIVE]])
            ->andWhere(['and', "created_at<=NOW() - INTERVAL $expireAfterHours HOUR"])
            ->all();

        foreach($expiredPackages as $printablePackage)
        {
            $printablePackage->status = ApiPrintablePack::STATUS_EXPIRED;
            $printablePackage->safeSave();
            $this->println(sprintf('Printable package expired id[%d]', $printablePackage->id));
        }
    }
}