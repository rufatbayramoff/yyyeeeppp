<?php

namespace console\controllers;

use common\components\ConsoleController;

/**
 * Indexer - for site search
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class IndexerController extends ConsoleController
{
    /**
     * update sphinx indexer data
     */
    public function actionRun()
    {
        exec("/usr/bin/indexer --config /etc/sphinxsearch/sphinx.conf --all --rotate", $result);
        $result['time'] = date("d.m.Y H:i:s");
        var_dump($result);
    }
}