<?php

namespace console\controllers;

use common\components\ConsoleController;

/**
 * Backup to AWS
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class BackupController extends ConsoleController
{
    /**
     * backup file by full filepath name
     * 
     * @param type $filepath
     */
    public function actionSite($filepath)
    {
        $s3 = new \Aws\S3\S3Client([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'credentials' => [
                'key'    => param('aws_key'),
                'secret' => param('aws_secret')
            ]
        ]); 
        
        $name = pathinfo($filepath, PATHINFO_BASENAME);
        $result = $s3->putObject(array(
            'Bucket'       => 'backuptz',
            'Key'          => date("m_d_y") . $name,
            'SourceFile'   => $filepath 
        ));

        dd($result); 
    }
}