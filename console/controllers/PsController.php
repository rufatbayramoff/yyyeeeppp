<?php

namespace console\controllers;

use common\components\ConsoleController;
use common\components\Emailer;
use common\models\CompanyCategory;
use common\models\CompanyService;
use common\models\GeoCountry;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\User;
use common\models\UserEmailSent;
use common\modules\tsCertification\services\TsCertificationService;
use common\services\PsProgressOrdersService;
use frontend\modules\mybusiness\services\CreateMockCompanyService;

/**
 * Print service related console actions for cron or cmd
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class PsController extends ConsoleController
{
    /** @var  PsProgressOrdersService */
    public $psProgressOrdersService;

    /** @var TsCertificationService */
    public $tsCertificationService;

    public function injectDependencies(PsProgressOrdersService $psProgressOrdersService, TsCertificationService $tsCertificationService)
    {
        $this->psProgressOrdersService = $psProgressOrdersService;
        $this->tsCertificationService  = $tsCertificationService;
    }

    public function actionCalculateOrdersCount()
    {
        $this->psProgressOrdersService->reCalculateOrdersCount();
    }

    public function actionZeroOrdersCount()
    {
        $this->psProgressOrdersService->zeroOrdersCount();
    }

    public function actionRefreshRating()
    {
        $this->psProgressOrdersService->reCalculateCatlog();
    }

    /**
     * send notification to printer which is not certified
     * after 3 days after registration.
     *
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionNotifyCertifyPrinter()
    {
        $psPrinters = PsPrinter::find()->visible()
            ->joinWith('ps')
            ->joinWith('companyService')
            ->leftJoin('ps_printer_test', 'ps_printer_test.ps_printer_id=ps_printer.id')
            ->where(
                [
                    'company_service.certification'    => '',
                    'company_service.moderator_status' => CompanyService::pendingModerationStatusList(),
                    'company_service.visibility'       => CompanyService::VISIBILITY_EVERYWHERE
                ]
            )
            ->andWhere(['<', 'company_service.created_at', dbexpr('NOW() - INTERVAL 3 DAY')])
            ->andWhere(['>', 'company_service.created_at', dbexpr('NOW() - INTERVAL 5 DAY')])
            ->andWhere(['ps_printer_test.ps_printer_id' => null])
            ->limit(50)
            ->all();
        #$sql  = $psPrinters->createCommand()->getRawSql(); die($sql);
        $emailer       = new Emailer();
        $cronUser      = User::findOne(User::USER_CRON);
        $dbTransaction = app('db')->beginTransaction();
        try {
            $sentToPs = [];
            foreach ($psPrinters as $psPrinter) {
                $hash      = UserEmailSent::makeHash(['ps_printer_id' => $psPrinter->id]);
                $userId    = $psPrinter->ps->user_id;
                $sentEmail = UserEmailSent::find()->where(
                    [
                        'user_id'    => $userId,
                        'email_code' => UserEmailSent::PS_PRINTER_CERTIFY,
                        'hash'       => $hash
                    ]
                )->one();
                if ($sentEmail) {
                    $this->stdout('[WARNING] Email already sent to PS printer ' . $psPrinter->id . "\n");
                    continue;
                }
                UserEmailSent::addRecord(
                    [
                        'user_id'    => $userId,
                        'email_code' => UserEmailSent::PS_PRINTER_CERTIFY,
                        'created_at' => dbexpr('NOW()'),
                        'hash'       => $hash
                    ]
                );
                if (in_array($psPrinter->ps->id, $sentToPs)) {
                    $this->stdout('[WARNING] email sent to PS ' . $psPrinter->ps->id . "\n");
                    continue;
                } else {
                    $this->stdout('[INFO] email sent to ps printer ' . $psPrinter->id . "\n");
                }
                // send email
                $emailer->sendCertifyPrinterEmail($psPrinter, $cronUser);
                $sentToPs[] = $psPrinter->ps->id;
            }
            // now select all printers for given ps and mark them as already sent
            $this->markPrintersAsSent($sentToPs);
            $dbTransaction->commit();
        } catch (\Exception $e) {
            $dbTransaction->rollBack();
            throw $e;
        }
    }

    private function markPrintersAsSent($sentToPs)
    {
        $psPrintersPs = PsPrinter::find()->visible()
            ->joinWith('ps')
            ->joinWith('companyService')
            ->leftJoin('ps_printer_test', 'ps_printer_test.ps_printer_id=ps_printer.id')
            ->where(
                [
                    'company_service.certification' => '',
                    'company_service.moderator_status'   => CompanyService::moderatedStatusList(),
                ]
            )
            ->andWhere(['>', 'company_service.created_at', dbexpr('NOW() - INTERVAL 5 DAY')])
            ->andWhere(['ps_printer_test.ps_printer_id' => null])
            ->andWhere(['ps.id' => $sentToPs])
            ->all();

        foreach ($psPrintersPs as $oldPsPrinter) {
            $hash      = UserEmailSent::makeHash(['ps_printer_id' => $oldPsPrinter->id]);
            $userId    = $oldPsPrinter->ps->user_id;
            $sentEmail = UserEmailSent::find()->where(
                [
                    'user_id'    => $userId,
                    'email_code' => UserEmailSent::PS_PRINTER_CERTIFY,
                    'hash'       => $hash
                ]
            )->one();
            if ($sentEmail) {
                continue;
            }
            UserEmailSent::addRecord(
                [
                    'user_id'    => $userId,
                    'email_code' => UserEmailSent::PS_PRINTER_CERTIFY,
                    'created_at' => dbexpr('NOW()'),
                    'hash'       => $hash
                ]
            );
            $this->stdout('[INFO] marked as sent to ps printer ' . $oldPsPrinter->id . "\n");
        }
    }

    public function actionSetCountry()
    {
        $psAll = Ps::find()->with(['user', 'companyServices'])->where(['country_id' => null])->all();

        $totalUpdated = 0;
        $noCountry    = [];
        foreach ($psAll as $ps) {
            $user    = $ps->user;
            $country = null;
            if ($user->userTaxInfo) {
                $country = GeoCountry::findOne(['iso_code' => $user->userTaxInfo->place_country]);
            }
            $psMachines = $ps->companyServices;
            foreach ($psMachines as $psMachine) {
                if ($psMachine->location) {
                    $country = $psMachine->location->country;
                    break;
                }
            }

            if (!empty($ps->phone_country_iso)) {
                $country = GeoCountry::findOne(['iso_code' => $ps->phone_country_iso]);
            }

            if (!$country) { // try from shipping info
                if ($user->userProfile && $user->userProfile->address) {
                    $country = $user->userProfile->address->country;
                }
            }
            if ($country) {
                $ps->country_id = $country->id;
                $ps->safeSave();
                $totalUpdated++;
            } else {
                $noCountry[] = $ps->id;
            }
        }
        $this->stdout(sprintf("Total found: %s, Total updated: %s, Skipped: %s \n\n", count($psAll), $totalUpdated, implode(", ", $noCountry)));
    }

    public function actionExpireCertification()
    {
        $this->tsCertificationService->fixExpired();
    }

    public function actionCreateMockService()
    {
        $service    = \Yii::createObject(CreateMockCompanyService::class);
        $categories = CompanyCategory::find()->all();
        foreach ($categories as $companyServiceCategory) {
            $service->create($companyServiceCategory);
        }
    }
}
