<?php
/**
 * Created by mitaichik
 */

namespace backend\models\blog;


use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\models\BlogPost;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use Yii;
use yii\web\UploadedFile;

class BlogPostForm extends \common\models\BlogPost
{
    /**
     * @var UploadedFile
     */
    public $imageFileUploaded;

    /**
     * @var UploadedFile
     */
    public $imagePreviewFileUploaded;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['imageFileUploaded', 'validateFilesRequired', 'skipOnEmpty' => false],
            [['imageFileUploaded', 'imagePreviewFileUploaded'], 'image'],
            ['alias', 'match', 'pattern' => '/^[A-Za-z0-9-]*$/i'],
        ]);
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [self::SCENARIO_DEFAULT => [
            'date', 'title', 'alias', 'content', 'description', 'imageFileUploaded', 'imagePreviewFileUploaded', 'is_active'
        ]];
    }

    /**
     * Validate required files
     */
    public function validateFilesRequired()
    {
        if(!$this->image_file_id && !$this->imageFileUploaded){
            $this->addError('imageFileUploaded', 'File cant be empty');
        }

        if(!$this->image_preview_file_id && !$this->imagePreviewFileUploaded){
            $this->addError('imagePreviewFileUploaded', 'File cant be empty');
        }
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)){
            $this->imageFileUploaded = UploadedFile::getInstance($this, 'imageFileUploaded');
            $this->imagePreviewFileUploaded =  UploadedFile::getInstance($this, 'imagePreviewFileUploaded');
            return true;
        }
        return false;
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidParamException
     * @throws \InvalidArgumentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \Exception
     */
    public function beforeSaveMethod($insert)
    {
        if(parent::beforeSaveMethod($insert)){
            /** @var FileFactory $fileFactory */
            $fileFactory = Yii::createObject(FileFactory::class);
            $fileRepository =Yii::createObject(FileRepository::class);
            if($this->imageFileUploaded){
                $file = $fileFactory->createFileFromUploadedFile($this->imageFileUploaded);
                $file->setOwner(BlogPost::class, 'image_file_id');
                $file->is_public = true;
                $fileRepository->save($file);
                $this->image_file_id = $file->id;
            }

            if($this->imagePreviewFileUploaded){
                $file = $fileFactory->createFileFromUploadedFile($this->imagePreviewFileUploaded);
                $file->setOwner(BlogPost::class, 'image_preview_file_id');
                $file->is_public = true;
                $fileRepository->save($file);
                $this->image_preview_file_id = $file->id;
            }

            return true;
        }
        return false;
    }
}