<?php namespace backend\models\user;

use common\components\Emailer;
use Yii;
use \common\models\UserTaxInfoHistory;

/**
 * User tax information model for backend
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UserTaxInfo extends \common\models\UserTaxInfo
{

    public function behaviors()
    {
        return [
        ];
    }
    /**
     * moderator approves user tax information
     * @return boolean
     */
    public function approve()
    {
        if ($this->status == self::STATUS_CONFIRMED) {
            return false;
        }
        UserTaxInfoHistory::addRecord([
          'user_tax_info_id' => $this->id,
          'user_id' =>  app('user')->getIdentity()->id,
          'created_at' => dbexpr("NOW()"),
          'action_id' => UserTaxInfoHistory::ACTION_APPROVE,
          'comment' => ""
        ]);
        $this->status = self::STATUS_CONFIRMED;
        return $this->save();
    }

    /**
     * moderator declined tax information
     * @param string $reason
     * @return bool
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function decline($reason = '')
    {
        if ($this->status == self::STATUS_DECLINE) {
            return false;
        }
        try {
            $this->status = self::STATUS_DECLINE;
            $this->safeSave();
            UserTaxInfoHistory::addRecord([
                'user_tax_info_id' => $this->id,
                'user_id' =>  app('user')->getIdentity()->id,
                'created_at' => dbexpr("NOW()"),
                'action_id' => UserTaxInfoHistory::ACTION_DECLINE,
                'comment' => $reason
            ]);
            /* turned off after TPSO
            // now try to disable all printers and models
            $ps = \common\models\Ps::findOne(['user_id'=>$this->user_id]);
            if ($ps) {
                $userPrinters = \common\models\PsPrinter::findAll(['ps_id' => $ps->id]);
                foreach ($userPrinters as $printer) {
                    $printer->moderator_status = \common\models\PsPrinter::MSTATUS_BANNED;
                    $printer->safeSave();
                }
            }
            // ban models
            $models = \common\models\Model3d::findAll(['user_id' => $this->user_id]);
            foreach ($models as $model3d) {
                $model3d->status = 'inactive';
                $model3d->is_published = 0; // removes from store
                $model3d->safeSave();
            }
            */
            
            // email 
            $emailer = new Emailer();
            $emailer->sendTaxDeclineMessage($this->user, $reason);
        } catch (\Exception $e) {
            throw $e;
        }
    }


}
