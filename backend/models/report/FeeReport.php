<?php

namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderHistory;
use common\modules\payment\exception\PaymentException;
use lib\geo\GeoService;
use Yii;
use yii\base\UserException;

class FeeReport implements BaseReportInterface
{
    public $ordersList = [];

    /** @var string */
    public $dateFilter;

    /** @var string */
    public $statusFilter;

    /**
     * @var GeoService
     */
    protected $geoService;

    const STATUS_RECEIVED = 'received';


    public function setGeoService(GeoService $geoService)
    {
        $this->geoService = $geoService;
    }

    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'order_id'            => 'Order_id',
            'invoice_uuid'        => 'Invoice uuid',
            'created_at'          => 'Created at',
            'user_id'             => 'Buyer id',
            'ps_id'               => 'Company id',
            'model'               => 'Model fee',
            'ps_fee'              => 'Ps fee',
            'ps_print'            => 'Ps print',
            'shipping'            => 'Shipping',
            'shipping_fact'       => 'Shipping fact',
            'package'             => 'Package',
            'items_fee'           => 'Items fee',
            'items_manufacturer'  => 'Items manufacturer',
            'affiliate_fee_out'   => 'Affiliate fee out',
            'affiliate_fee_in'    => 'Affiliate fee in',
            'total'               => 'Total',
            'refund_ps_print'     => 'Refund ps_print',
            'refund_shipping'     => 'Refund shipping',
            'refund_fee'          => 'Refund fee',
            'refund_package'      => 'Refund package',
            'refund_ps_fee'       => 'Refund ps_fee',
            'refund_manufacturer' => 'Refund manufacturer',
            'refund_ts'           => 'Refund ts',
            'refund_n'            => 'Refund none',
            'refund_all'          => 'Refund all',
            'transaction_vendor'  => 'Transaction vendor',
            'transaction_status'  => 'Transaction status',
            'to_country'          => 'To country',
            'track_number'        => 'Track number',
            'qty'                 => 'Total items qty',
            'modelVolume'         => 'Total items volume',
            'orderStatus'         => 'Order status',
            'history'             => 'History',
            'attemptCount'        => 'Attempts count',
            'cert'                => 'Certification',
            'country_ip'          => 'Country',
            'sort_by'             => 'Sort by'
        ];
    }

    /**
     * @param StoreOrderHistory[] $historyArray
     */
    protected function historyToString($historyArray)
    {
        $str = '';
        foreach ($historyArray as $historyItem) {
            $str .= $historyItem->created_at . ': ' . $historyItem->comment . ' '
                . (!empty($historyItem->data['reason']) ? $historyItem->data['reason'] . ' ' : '')
                . (!empty($historyItem->data['comment']) ? $historyItem->data['comment'] : '') . "\n";
        }
        $str = substr($str, 0, -1);
        return '"' . $str . '"';
    }

    public function getItems()
    {
        if (!$this->ordersList && !$this->dateFilter && !$this->statusFilter) {
            throw new UserException('No filter params');
        }
        /** @var PaymentInvoice[] $invoices */
        $invoicesQuery = PaymentInvoice::find()
            ->andWhere(['payment_invoice.status' => [PaymentInvoice::STATUS_VOID, PaymentInvoice::STATUS_REFUNDED, PaymentInvoice::STATUS_CANCELED, PaymentInvoice::STATUS_PAID]])
            ->joinWith('storeOrder')
            ->joinWith('paymentInvoiceAnalytic')
            ->joinWith('storeOrder.orderItem')
            ->joinWith('storeOrder.storeOrderHistories')
            ->joinWith('payments')
            ->joinWith('userSession');

        if ($this->ordersList) {
            $invoicesQuery->andWhere(['store_order_id' => $this->ordersList]);
        }
        if ($this->dateFilter) {
            $invoicesQuery->andWhere("`payment_invoice`.`created_at` like '" . addslashes($this->dateFilter) . "-%'");
        }
        if ($this->statusFilter) {
            $invoicesQuery->joinWith('storeOrder.currentAttemp');
            if ($this->statusFilter === self::STATUS_RECEIVED) {
                $invoicesQuery->andWhere(['store_order_attemp.status' => [StoreOrderAttemp::STATUS_RECEIVED, StoreOrderAttemp::STATUS_DELIVERED]]);
            }
        }

        $invoicesQuery->orderBy('payment_invoice.created_at');

        $count         = $invoicesQuery->count();
        $sql           = $invoicesQuery->createCommand()->getRawSql();
        $queryCache    = $sql . '_' . $count;
        $cacheFileName = Yii::getAlias('@runtime') . '/fee_report_' . md5($queryCache) . '.txt';
        $cacheData     = [
            'data'       => [],
            'countTotal' => $count,
            'offset'     => 0
        ];
        $queryStep     = 10000;
        if (file_exists($cacheFileName)) {
            $cache     = file_get_contents($cacheFileName);
            $cacheData = json_decode($cache, true);
            $invoicesQuery->offset($cacheData['offset']);
        }
        $invoicesQuery->limit($queryStep);

        $items    = [];
        $invoices = $invoicesQuery->all();
        if (!$invoices) {
            return $cacheData['data'];
        }

        foreach ($invoices as $invoice) {
            if (!$invoice->hasPayedPayments()) {
                continue;
            }

            $firstTransaction = $invoice->getActivePaymentFirstTransaction();
            $storeOrder       = $invoice->storeOrder;
            $psPrinter        = $storeOrder && $storeOrder->currentAttemp && $storeOrder->currentAttemp->machine && $storeOrder->currentAttemp->machine->psPrinter ? $storeOrder->currentAttemp->machine->psPrinter : null;

            $item['order_id']            = $invoice->store_order_id;
            $item['invoice_uuid']        = $invoice->uuid;
            $item['created_at']          = $invoice->created_at;
            $item['user_id']             = $invoice->user_id;
            $item['ps_id']               = $storeOrder && $storeOrder->currentAttemp ? $storeOrder->currentAttemp->ps_id : '';
            $item['model']               = 0;
            $item['ps_fee']              = 0;
            $item['ps_print']            = 0;
            $item['shipping']            = 0;
            $item['shipping_fact']       = 0;
            $item['package']             = 0;
            $item['items_fee']           = 0;
            $item['items_manufacturer']  = 0;
            $item['affiliate_fee_out']   = 0;
            $item['affiliate_fee_in']    = 0;
            $item['refund_ps_print']     = 0;
            $item['refund_shipping']     = 0;
            $item['refund_fee']          = 0;
            $item['refund_package']      = 0;
            $item['refund_ps_fee']       = 0;
            $item['refund_manufacturer'] = 0;
            $item['refund_ts']           = 0;
            $item['refund_n']            = 0;
            $item['refund_all']          = 0;
            $item['transaction_vendor']  = $firstTransaction->vendor;
            $item['transaction_status']  = $firstTransaction->status;
            $item['to_country']          = $storeOrder->shipAddress->country->iso_code ?? '';
            $item['track_number']        = 'T:' . ($storeOrder->currentAttemp->delivery->tracking_number ?? '');
            $item['total']               = $invoice->getAmountTotal()->getAmount();
            $item['qty']                 = $storeOrder && $storeOrder->hasModel() ? $storeOrder->getFirstItem()->model3dReplica->getTotalQty() : '';
            $item['modelVolume']         = $storeOrder && $storeOrder->hasModel() ? $storeOrder->getFirstItem()->model3dReplica->getTotalVolume() : '';
            $item['orderStatus']         = $storeOrder ? $storeOrder->order_state : '';
            $item['history']             = $storeOrder ? $this->historyToString($storeOrder->storeOrderHistories) : '';
            $item['attemptCount']        = $storeOrder ? count($storeOrder->attemps) : '';
            $item['cert']                = $psPrinter ? $psPrinter->companyService->getCertificationLabel() : '';
            $item['country_ip']          = $invoice->userSession ? $this->geoService->getLocationStringByIp($invoice->userSession->ip) : '';
            $item['sort_by']              = $invoice?->paymentInvoiceAnalytic?->sort;

            if (array_key_exists(PaymentInvoice::ACCOUNTING_TYPE_ITEMS, $invoice->accounting)) {
                foreach ($invoice->accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS] as $accountingItem) {
                    if (array_key_exists(PaymentInvoice::ACCOUNTING_TYPE_FEE, $accountingItem)) {
                        $item['items_fee'] += $accountingItem[PaymentInvoice::ACCOUNTING_TYPE_FEE]['price'];
                    }
                    if (array_key_exists(PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER, $accountingItem)) {
                        $item['items_manufacturer'] += $accountingItem[PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER]['price'];
                    }
                }
            } else {
                foreach ($invoice->accounting as $accountingKey => $accountingVale) {
                    $item[$accountingKey] = $accountingVale['price'];
                }
                foreach ($invoice->payments as $payment) {
                    /** @var PaymentDetailOperation $easyPostOperation */
                    $easyPostOperation = $payment->getPaymentDetailOperations()->easyPostMain()->one();
                    if ($easyPostOperation) {
                        try {
                            $toEasyPostDetail = $easyPostOperation->toPaymentDetail();
                        } catch (PaymentException $exception) {
                            $toEasyPostDetail = null;
                        }
                        if ($toEasyPostDetail) {
                            $easyPostSum           = $toEasyPostDetail->getMoneyAmount()->getAmount();
                            $item['shipping_fact'] += $easyPostSum;
                        }
                    }
                }
            }
            //if ($invoice-> $item[shipping])
            $refunds = $invoice->getPaymentTransactionRefunds()->approved()->all();
            foreach ($refunds as $refund) {
                $type                    = $refund->refund_type ?? 'n';
                $item['refund_' . $type] += $refund->amount;
                $item['refund_all']      += $refund->amount;
            }
            $items[] = $item;
        }

        $cacheData['data']   = array_merge($cacheData['data'], $items);
        $cacheData['offset'] += $queryStep;
        $cacheData['count']  = count($cacheData['data']);
        file_put_contents($cacheFileName, json_encode($cacheData));

        return 'RELOAD';
    }

    public function formCsv()
    {
        $time1 = time();
        $title = $this->getColumnsNames();
        echo implode(';', $title) . "\n";
        $items = $this->getItems();
        foreach ($items as $item) {
            if (is_array($item)) {
                $item = (object)$item;
            }
            foreach ($title as $key => $value) {
                echo $item->$key . ';';
            }
            echo "\n";
        }
        $time2 = time();
    }

    public function setOrdersList($ordersList)
    {
        if (is_string($ordersList)) {
            $ordersList   = explode("\n", $ordersList);
            $filteredList = [];
            foreach ($ordersList as $ordersId) {
                if ((int)$ordersId) {
                    $filteredList[] = (int)$ordersId;
                }
            }
            $ordersList = $filteredList;
        }
        $this->ordersList = $ordersList;
    }

    public function setDate(string $dateFilter)
    {
        $this->dateFilter = $dateFilter;
    }

    public function setStatus(string $statusFilter)
    {
        $this->statusFilter = $statusFilter;
    }

    public function setParams(array $params)
    {
    }
}
