<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 28.12.2016
 * Time: 12:10
 */

namespace backend\models\report;


use backend\modules\statistic\reports\BaseReportInterface;
use common\models\User;
use common\models\UserProfile;

class UserWeekReport implements BaseReportInterface
{
    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'user_id'         => 'User id',
            'referrer'        => 'User source',
            'email'           => 'Email',
            'is_printservice' => 'Is PS',
            'is_modelshop'    => 'Is Designer',
            'created_at'      => 'Registered At',
            'deleted_at'      => 'Request Delete At',
            'status'          => 'Status',
            'current_lang'    => 'Language',
        ];
    }

    /**
     * recheck user profiles and set correct values for ps or designer flags
     * before export
     */
    private function updateUserProfiles()
    {


        $sql = "select u.id, ps.id as is_printerservice, m.id as is_modelshop from user as u 
                left join ps ON ps.user_id=u.id
                left join model3d m ON m.user_id=u.id and m.published_at is not null
                where from_unixtime(u.created_at) >  ADDDATE(NOW(), INTERVAL - 1 WEEK) ";
        $rows = \Yii::$app->db->createCommand($sql)->queryAll();
        foreach($rows as $k=>$v){
            $isPs = !empty($v['is_printservice']) ? 1 : 0;
            $isShop = !empty($v['is_modelshop']) ? 1 : 0;
            UserProfile::updateRow(['user_id'=>$v['id']], ['is_printservice'=>$isPs, 'is_modelshop'=>$isShop]);
        }
    }

    public function getItems()
    {
        $this->updateUserProfiles();
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);
        $userIds = join(',', $userIds);

        $sql = "select u.id as user_id, u.email, ps.id as is_printservice, up.is_modelshop, from_unixtime(u.created_at) as created_at, 
              u.status, up.current_lang, source.referrer, ur.created_at as deleted_at
            from user as u
            join user_profile up ON (u.id=up.user_id)
            left join ps ON ps.user_id=u.id
            left join affiliate_user_source source ON (source.user_id=u.id)
            left join user_request ur ON (ur.user_id=u.id AND ur.request_type='delete')
            where 
				(from_unixtime(u.created_at) >  ADDDATE(NOW(), INTERVAL - 1 WEEK) OR 
				 ur.created_at  > ADDDATE(NOW(), INTERVAL - 1 WEEK)
				) and u.id>1000 AND u.id NOT IN($userIds)";
        $rows = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        $statuses = User::getStatuses();
        foreach($rows as $k=>$row){
            $row->status = array_key_exists($row->status, $statuses) ? $statuses[$row->status] : $row->status;
            $rows[$k] = $row;
        }
        return $rows;

    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}