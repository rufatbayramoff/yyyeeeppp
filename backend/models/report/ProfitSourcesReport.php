<?php

namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\interfaces\Model3dBaseInterface;
use common\models\StoreOrder;
use DateTime;

class ProfitSourcesReport implements BaseReportInterface
{
    public const DATE_START = '2019-04-01';

    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'date'     => 'Date',
            'order_id' => 'Order id',
            'source'   => 'Source',
            'total'    => 'Total',
            'fee'      => 'Treatstock fee',
        ];
    }

    public function getItems()
    {
        $orders = StoreOrder::find()
            ->andWhere(['>', 'created_at', self::DATE_START])
            ->forState([StoreOrder::STATE_COMPLETED])
            ->all();
        foreach ($orders as $order) {
            $source = $order->source ? $order->source : 'website';
            $invoice = $order->getPrimaryInvoice();
            $total = $invoice->getAmountTotal()->getAmount();
            if ($total < 0.01) {
                continue;
            }
            if ($order->preorder) {
                $source = 'preorder';
                $fee = $invoice->preorderAmount->getAmountFeeWithRefund()->getAmount();
            } else {
                $model3d = $order->getFirstReplicaItem();
                $fee = $invoice->storeOrderAmount->getAmountPrintFeeWithRefund()->getAmount();
                if ($order->user->id === 27463) { // ALL3DP
                    $source = 'all3dp';
                } elseif ($order->isThingiverseOrder()) {
                    $source = 'thingiverse';
                } elseif ($model3d->source == Model3dBaseInterface::SOURCE_PS_WIDGET) {
                    $source = 'psWidget';
                } elseif ($model3d->source == Model3dBaseInterface::SOURCE_PS_FACEBOOK) {
                    $source = 'facebook';
                }
            }
            $orderInfo = [
                'date'     => $order->created_at,
                'order_id' => $order->id,
                'source'   => $source,
                'total'    => $total,
                'fee'      => $fee,
            ];
            $data[] = (object)$orderInfo;
        }
        return $data;
    }

    public function setParams(array $params)
    {

    }
}