<?php
/**
 * Created by mitaichik
 */

namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;

class AttemptDeclineReason implements BaseReportInterface
{
    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'order_id'       => 'Order Id',
            'created_at'     => 'Created At',
            'reason'        => 'reason',
            'comment'         => 'Comment',
            'current_status'   => 'Current Status',
            'order_state' => 'Order State',
            'ps_name' => 'Ps Name'
        ];
    }

    public function getItems()
    {
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);
        $userIds = join(',', $userIds);

        $sql = "
            SELECT 
                store_order_history.order_id, 
                store_order_history.created_at, 
                REPLACE(REPLACE(SUBSTRING(`data`, LOCATE('reason\":', `data`) + 8, LOCATE(',\"comment\"', `data`) - LOCATE('reason\":', `data`) - 8), 'null', ''), '\"', '') as reason, 
                REPLACE(REPLACE(SUBSTRING(`data`, LOCATE('\"comment\"', `data`) + 11), '\"}', ''), 'ull}', '') as `comment`,
                store_order_attemp.status as current_status,
                store_order.order_state as order_state,
                ps.title as ps_name
                FROM store_order_history 
                LEFT JOIN store_order on store_order_history.order_id = store_order.id
                LEFT JOIN store_order_attemp ON store_order_attemp.id = store_order.current_attemp_id 
                LEFT JOIN company_service ON store_order_attemp.machine_id=company_service.id
                LEFT JOIN ps ON company_service.ps_id = ps.id
                WHERE action_id = 'decline_reason' AND store_order.user_id NOT IN($userIds)
        ";
        $rows = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        return $rows;

    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}