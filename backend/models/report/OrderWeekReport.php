<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 28.12.2016
 * Time: 12:10
 */

namespace backend\models\report;


use backend\modules\statistic\reports\BaseReportInterface;

class OrderWeekReport implements BaseReportInterface
{
    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'order_id'      => 'Order id',
            'created_at'    => 'Created At',
            'billed_at'     => 'Billed At',
            'accepted_date' => 'Accepted At',
            'shipped_at'    => 'Shipped At',
            'delivered_at'  => 'Delivered At',
            'received_at'   => 'Received At',
        ];
    }

    public function getItems()
    {
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);
        $userIds = join(',', $userIds);

        $sql = "select 
            o.id as order_id,
            o.created_at,
            o.billed_at,
            atmpd.accepted_date, atmpd.delivered_at, atmpd.shipped_at,
            atmpd.received_at
            from store_order o
            join store_order_attemp atmp ON (atmp.id=o.current_attemp_id)
            join store_order_attemp_dates atmpd ON (atmpd.attemp_id=atmp.id)
            where 
            (o.created_at >  ADDDATE(NOW(), INTERVAL - 1 WEEK) 
            OR atmpd.received_at >  ADDDATE(NOW(), INTERVAL - 1 WEEK))
             AND o.user_id NOT IN($userIds)";
        $rows = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        return $rows;
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}