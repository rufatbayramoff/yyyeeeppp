<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 28.12.2016
 * Time: 12:10
 */

namespace backend\models\report;


use backend\modules\statistic\reports\BaseReportInterface;
use common\components\DateHelper;
use common\models\Model3d;
use common\models\UserCollectionModel3d;
use common\modules\product\interfaces\ProductInterface;

class Model3dReport implements BaseReportInterface
{
    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'model_id'       => 'Model id',
            'created_at'     => 'Created at',
            'user_id'        => 'User id',
            'product_status' => 'Status',
            'published_at'   => 'Published At',
            'collections'    => 'Collections'
        ];
    }

    public function getItems()
    {
        $userIds  = \Yii::$app->setting->get('user.excludeReport', [1]);
        $model3dQuery = Model3d::find()
            ->select('model3d.id, product_common.created_at, product_common.user_id, product_common.product_status, model3d.published_at')
            ->leftJoin('product_common', 'model3d.product_common_uid=product_common.uid')
            ->andWhere('product_common.product_status != \''.ProductInterface::STATUS_DRAFT.'\'')
            ->andWhere('product_common.user_id is not null')
            ->andWhere(['not in', 'product_common.user_id', $userIds])
            ->andWhere(['>', 'product_common.created_at', DateHelper::subNow('P12M')])
            ->asArray();
        $model3ds = $model3dQuery->all();
        $rows     = [];
        foreach ($model3ds as $model3d) {
            $userCollectionsModel3d =  UserCollectionModel3d::find()->where(['model3d_id' => $model3d['id']])->all();
            $collections = [];
            foreach ($userCollectionsModel3d as $userCollectionModel3d) {
                $collection = $userCollectionModel3d->collection;
                $collections[$collection->system_type] = (empty($collections[$collection->system_type]) ? 0 : $collections[$collection->system_type]) + 1;
            }
            $collectionsString = '';
            foreach ($collections as $collectionType => $count) {
                if (!$collectionType) {
                    continue;
                }
                $collectionsString .= $collectionType . ': ' . $count . ',';
            }
            $row    = [
                'model_id'       => $model3d['id'],
                'created_at'     => $model3d['created_at'],
                'user_id'        => $model3d['user_id'],
                'product_status' => $model3d['product_status'],
                'published_at'   => $model3d['published_at'],
                'collections'    => $collectionsString
            ];
            $rows[] = (object)$row;
        }
        return $rows;
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}