<?php

namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\components\exceptions\InvalidModelException;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\StoreOrderAttemp;
use common\modules\payment\exception\PaymentException;
use yii\base\UserException;

class BraintreeTransactionsReport implements BaseReportInterface
{
    /** @var string */
    public $dateFilter;

    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'order_id'            => 'Order_id',
            'invoice_uuid'        => 'Invoice uuid',
            'created_at'          => 'Created at',
            'price'               => 'Price',
        ];
    }

    public function getItems()
    {
        $items = [];

        /** @var PaymentTransaction[] $invoices */
        $transactionQuery = PaymentTransaction::find()
            ->andWhere(['payment_transaction.vendor' => [PaymentTransaction::VENDOR_BRAINTREE, PaymentTransaction::VENDOR_PAYPAL]])
            ->andWhere(['payment_transaction.status' => [PaymentTransaction::STATUS_SETTLED]]);
        $transactions = $transactionQuery->all();
        foreach ($transactions as $transaction) {
            /* @var $invoice \common\models\PaymentInvoice */
            $invoice = $transaction->firstPaymentDetail->payment->paymentInvoice;

            $item['order_id']     = $invoice->store_order_id;
            $item['invoice_uuid'] = $invoice->uuid;
            $item['created_at']   = $transaction['created_at'];
            $item['price']        = $transaction['amount'];
            $items[] = (object)$item;
        }

        return $items;

    }

    public function setParams(array $params)
    {
    }
}
