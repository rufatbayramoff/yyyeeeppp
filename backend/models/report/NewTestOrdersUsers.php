<?php
/**
 * Created by mitaichik
 */

namespace backend\models\report;


use backend\modules\statistic\reports\BaseReportInterface;

class NewTestOrdersUsers implements BaseReportInterface
{
    /**
     * @return NewTestOrdersUsers
     */
    public static function create()
    {
        return new self;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedTime()
    {
        return new \DateTime();
    }

    /**
     * @return array
     */
    public function getColumnsNames()
    {
        return [
            'username' => 'Username',
            'email' => 'email'
        ];
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);
        $userIds = join(',', $userIds);

        return \Yii::$app->db->createCommand("
            SELECT DISTINCT user.username, user.email FROM ps_printer
			LEFT JOIN company_service psm ON (psm.ps_printer_id=ps_printer.id)
            LEFT JOIN ps ON psm.ps_id = ps.id
            LEFT JOIN store_order_attemp ON ps_printer.id = store_order_attemp.printer_id
            LEFT JOIN store_order ON store_order_attemp.id = store_order.current_attemp_id
            LEFT JOIN user ON ps.user_id = user.id
            WHERE test_status = 'no' AND is_test_order_resolved = 0 AND ps_printer.is_deleted = 0
              AND store_order.user_id = 1 AND ps.user_id NOT IN($userIds)   
        ")->queryAll(\PDO::FETCH_CLASS);
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}