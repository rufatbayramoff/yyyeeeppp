<?php
/**
 * User: nabi
 */

namespace backend\models\report;


use backend\modules\statistic\reports\BaseReportInterface;
use common\models\CompanyService;

class PsPrinterLocationReport implements BaseReportInterface
{

    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'ps_id'           => 'PS Id',
            'ps_title'        => 'PS Title',
            'id'              => 'PS Printer Id',
            'printer_id'      => 'Printer Id',
            'printer_title'      => 'Printer Title',
            'email'           => 'Email',
            'title'           => 'Title',
            'country'         => 'Country',
            'region'          => 'Region',
            'city'            => 'City',
            'lat'             => 'Lat',
            'lon'             => 'Lon',
            'materials'       => 'Materials',
            'material_groups' => 'Material Groups',
            'technology'      => 'Technology',
            'created_at'      => 'Created At',
        ];
    }

    public function getItems()
    {

        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);
        $userIds = join(',', $userIds);

        $sql = "SELECT 
                    MIN(p.id) AS ps_id,
                    GROUP_CONCAT(DISTINCT p.title) AS ps_title,
                    a.id,
                    a.printer_id,
                    min(printer.title) as printer_title,
                    min(user.email) as email,
                    a.title,
                    min(g1.iso_code) AS country,
                    min(g2.title) AS region,
                    min(g3.title) AS city,
                    min(g3.lat) as lat,
                    min(g3.lon) as lon,
                    GROUP_CONCAT(pm2.filament_title) AS materials,
                    GROUP_CONCAT(pmg.code) AS material_groups,
                    tech.title AS technology,
                    ANY_VALUE(psm.moderator_status) as moderator_status,
                    ANY_VALUE(psm.visibility) as user_status,
                    min(psm.created_at) as created_at
                FROM
                    ps_printer a
                        JOIN
                    company_service psm ON (psm.ps_printer_id = a.id)
                    
                        JOIN
                    ps p ON psm.ps_id = p.id
                      JOIN user ON user.id=p.user_id
                        LEFT JOIN
                    user_location u ON u.id = psm.location_id
                        LEFT JOIN
                    geo_country g1 ON g1.id = u.country_id
                        LEFT JOIN
                    geo_region g2 ON g2.id = u.region_id
                        LEFT JOIN
                    geo_city g3 ON g3.id = u.city_id
                        LEFT JOIN
                    ps_printer_material pm ON pm.ps_printer_id = a.id
                        AND pm.is_active = 1
                        LEFT JOIN
                    printer_material pm2 ON (pm2.id = pm.material_id)
                        LEFT JOIN
                    printer_material_group pmg ON (pmg.id = pm2.group_id)
                        LEFT JOIN
                    printer ON printer.id = a.printer_id
                        LEFT JOIN
                    printer_technology tech ON (tech.id = printer.technology_id)
                WHERE
                    psm.moderator_status in ('" . implode("', '", CompanyService::moderatedStatusList()) . "')
                    AND p.user_id NOT IN($userIds)
        group by a.id";

        $rows = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        return $rows;

    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}