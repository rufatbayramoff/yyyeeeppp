<?php


namespace backend\models\report;


use backend\modules\statistic\reports\BaseReportInterface;
use Yii;
use yii\db\Expression;
use yii\db\Query;

class AffiliateAwardReport implements BaseReportInterface
{

    public static function create()
    {
        return new self;
    }

    public function getColumnsNames()
    {
        return [
            'id' => Yii::t('app', 'Order id'),
            'created_at' => Yii::t('app', 'Created at'),
            'number' => Yii::t('app', 'Affiliate award Id'),
            'title' => Yii::t('app', 'Affiliate source'),
            'price' => Yii::t('app', 'Price'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getItems()
    {
        $query = new Query();
        $query->from(['aa' => 'affiliate_award']);
        $query->innerJoin(['as2' => 'affiliate_source'], 'as2.uuid = aa.affiliate_source_uuid');
        $query->innerJoin(['c' => 'ps'], 'c.id = as2.company_id');
        $query->innerJoin(['pi2' => 'payment_invoice'], 'pi2.uuid = aa.invoice_uuid');
        $query->innerJoin(['so' => 'store_order'], 'so.id = pi2.store_order_id');
        $query->select([
            'so.id',
            'aa.created_at',
            'number' => 'aa.id',
            'title' => new Expression("concat(c.title,' - ',as2.uuid)"),
            'aa.price',
            new Expression("
            case
  when aa.accrued_payment_detail_id > 0 then 'done'
  when pi2.status = 'authorized' then 'pending'
  when pi2.status = 'paid' then 'pending'
  else 'new'
end as status
            ")
        ]);
        $query->andWhere(['>=','aa.created_at',new Expression('DATE(NOW()) - INTERVAL 7 DAY')]);
        $command = $query->createCommand();
        return $command->queryAll(\PDO::FETCH_CLASS);
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}