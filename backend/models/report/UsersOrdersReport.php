<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.04.19
 * Time: 16:19
 */
namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\models\StoreOrder;

class UsersOrdersReport implements BaseReportInterface
{
    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'user_id'     => 'User id',
            'created_at'  => 'User created at',
            'order_id'    => 'Order id',
            'status'      => 'Order state',
            'order_date'  => 'Order date',
            'price'       => 'Price',
            'refund'      => 'Refund',
        ];
    }

    public function getItems()
    {
        $orders = StoreOrder::find()->payed()->limit(10000000)->all();
        $report = [];
        /** @var StoreOrder $order */
        foreach ($orders as $order) {
            if ($order->getIsTestOrder()) {
                continue;
            }
            $line = new \stdClass();
            if ($order->user) {
                $line->user_id = $order->user->id;
                $line->created_at = date('Y-m-d', $order->user->created_at);
            } else {
                $line->user_id = '';
                $line->created_at = '';
            }
            $line->order_id = $order->id;
            $line->status = $order->order_state;
            $line->order_date = $order->created_at;
            $line->price = 0;
            $line->refund = 0;
            foreach ($order->getPaidInvoices() as $paidInvoice) {
                $line->price+= $paidInvoice->getAmountTotal()->getAmount();
                $line->refund+= $paidInvoice->getAmountRefund()->getAmount();
            }
            $report[]=$line;
        }
        return $report;

    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}