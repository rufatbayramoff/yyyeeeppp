<?php

namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\FileTypesHelper;
use common\components\order\TestOrderFactory;
use common\models\StoreOrder;


class OrdersFiles implements BaseReportInterface
{

    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'order_id' => 'OrderId',
            'date'     => 'Date',
            'type'     => 'Type',
            'image1'   => 'Image1',
            'image2'   => 'Image2',
            'image3'   => 'Image3',
            'image4'   => 'Image4',
            'image5'   => 'Image5',
            'images'   => 'Images',
            'files'    => 'Files',
        ];
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);

        $orders = StoreOrder::find()
            ->where(['not in', 'user_id', $userIds])
            ->andWhere("created_at>'" . DateHelper::subNow('P3M') . "'")
            ->orderBy('created_at')
            ->all();

        $rows = [];
        /** @var StoreOrder $order */
        foreach ($orders as $order) {
            $row['order_id'] = $order['id'];
            $row['date'] = $order->created_at;
            $row['image1']   = '';
            $row['image2']   = '';
            $row['image3']   = '';
            $row['image4']   = '';
            $row['image5']   = '';
            $row['images']   = '';
            $row['files']    = '';
            $images          = [];
            if ($model3d = $order->getFirstReplicaItem()) {
                $row['type'] = 'Instant printing';
                foreach ($model3d->model3dImgs as $model3dImg) {
                    $row['images'] .= $model3dImg->file->getFileUrl() . "\n";
                    $images[]      = $model3dImg->file->getFileUrl();
                }
                foreach ($model3d->model3dParts as $model3dPart) {
                    $row['files'] .= $model3dPart->file->getFileUrl() . "\n";
                }
            } elseif ($cuttingPack = $order->getCuttingPack()) {
                $row['type'] = 'Instant cutting';
                foreach ($cuttingPack->activeCuttingPackFiles as $cuttingPackFile) {
                    $row['images'] .= $cuttingPackFile->file->getFileUrl() . "\n";
                    $images[]      = $cuttingPackFile->file->getFileUrl() . "\n";
                }
            } elseif ($preorder = $order->preorder) {
                $row['type'] = 'Quote';
                foreach ($preorder->files as $file) {
                    if (FileTypesHelper::isImage($file)) {
                        $row['images'] .= $file->getFileUrl() . "\n";
                        $images[]      = $file->getFileUrl() . "\n";
                    } else {
                        $row['files'] .= $file->getFileUrl() . "\n";
                    }
                }
            }
            $row['image1'] = substr($images[0] ?? '', 0, -1);
            $row['image2'] = substr($images[1] ?? '', 0, -1);
            $row['image3'] = substr($images[2] ?? '', 0, -1);
            $row['image4'] = substr($images[3] ?? '', 0, -1);
            $row['image5'] = substr($images[4] ?? '', 0, -1);
            $row['images'] = '"' . substr($row['images'], 0, -1) . '"';
            $row['files']  = '"' . substr($row['files'], 0, -1) . '"';
            $rows[]        = (object)$row;
        }

        return $rows;
    }

    public function formCsv()
    {
        $time1 = time();
        $title = $this->getColumnsNames();
        echo implode(';', $title) . "\n";
        $items = $this->getItems();
        foreach ($items as $item) {
            if (is_array($item)) {
                $item = (object)$item;
            }
            foreach ($title as $key => $value) {
                echo $item->$key . ';';
            }
            echo "\n";
        }
        $time2 = time();
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}
