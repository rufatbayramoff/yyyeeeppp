<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 10.02.17
 * Time: 15:15
 */

namespace backend\models\report;


use backend\modules\statistic\reports\BaseReportInterface;
use common\models\StoreOrder;
use stdClass;

class OrdersWithBlockMessages implements BaseReportInterface
{
    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'order_id'             => 'Order id',
            'created_at'           => 'Created At',
            'block_messages_count' => 'Block messages count',
        ];
    }

    public function getItems()
    {
        /** @var StoreOrder[] $orders */
        $orders = StoreOrder::find()->notTest()->all();
        $rows = [];
        foreach ($orders as $order) {
            $row = new stdClass();
            $row->order_id = $order->id;
            $row->created_at = $order->created_at;

            $clientUser = $order->user;

            $blockedMessagesCount = 0;
            foreach ($order->attemps as $attemp) {
                $psUser= $attemp->ps->user;

                $blockedMessagesFromUser = $clientUser->getMsgMessages()->toUser($psUser)->blocked()->all();
                $blockedMessagesFromPs =  $psUser->getMsgMessages()->toUser($clientUser)->blocked()->all();
                $blockedMessagesCount+=count($blockedMessagesFromUser) + count($blockedMessagesFromPs);

            }
            $row->block_messages_count = $blockedMessagesCount;
            $rows[] = $row;
        }
        return $rows;
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}