<?php
/**
 * User: DeFacto
 */
namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;

class PsPrinterWeekReport implements BaseReportInterface
{

    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'printer_id'       => 'Printer Id',
            'created_at'       => 'Created At',
            'ps_id'            => 'Ps Id',
            'country'          => 'Country',
            'user_status'      => 'Status',
            'changed_at'       => 'Changed At',
            'moderator_status' => 'Moderator Status',
            'title'            => 'Title',
            'updated_at'       => 'Updated At',
        ];
    }
    /**
     * @return array
     */
    public function getItems()
    {
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);
        $userIds = join(',', array_map('intval',$userIds));

        $sql = "select
               p.id as printer_id, psm.created_at, psm.ps_id , gc.iso_code as country, 
                p.user_status, p.moderator_status, p.title, psm.updated_at, h.created_at as changed_at 
            from ps_printer p
             join company_service psm ON (psm.ps_printer_id=p.id)
             join ps_printer_history h ON (p.id=h.ps_printer_id 
				AND h.action_id='update' and h.`comment`='{\"user_status\":\"active\"}')
             join user_location l ON (l.id=psm.location_id)
            left join geo_country gc ON (gc.id=l.country_id)
            where psm.created_at > ADDDATE(NOW(), INTERVAL - 1 WEEK)
            OR h.created_at > ADDDATE(NOW(), INTERVAL - 1 WEEK) AND l.user_id NOT IN($userIds)";
        $rows = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        return $rows;
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}