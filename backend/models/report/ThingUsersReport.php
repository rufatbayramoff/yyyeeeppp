<?php
namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\models\ThingiverseReport as ThingiverseReportAlias;
use common\models\ThingiverseUser;
use DateTime;

class ThingUsersReport implements BaseReportInterface
{
    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'created_at' => 'Date',
            'email'      => 'Email',
        ];
    }

    public function getItems()
    {
        $allUsers = [];
        /** @var ThingiverseUser[] $tgUsers */
        $tgUsers = ThingiverseUser::find()->all();
        foreach ($tgUsers as $tgUser) {
            if ($tgUser->email) {
                $allUsers[$tgUser->email] = (object)[
                    'created_at' => $tgUser->created_at,
                    'email'      => $tgUser->email
                ];
            }
        }
        return $allUsers;
    }

    public function setParams(array $params)
    {
    }
}