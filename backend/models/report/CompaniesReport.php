<?php


namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\models\Company;
use common\models\query\PsQuery;
use common\models\StoreOrder;
use Yii;

class CompaniesReport implements BaseReportInterface
{

    public static function create()
    {
        return new self;
    }

    public function getColumnsNames()
    {
        return [
            'id' => 'Company id',
            'created_at' => 'Created at',
            'user'   =>'User',
            'title' => 'Title',
            'rating' => 'Rating',
            'conversion' =>  'Conversion %',
            'ordersByPreordersCount' =>'Orders from preorders',
            'quotesCount' => 'Count of Quotes',
            'instantPaymentsCount' => 'Instant Payments',
            'ordersCount' => 'Orders count',
            'paidOrdersCount' => 'Paid order count',
            'completedOrdersCount' => 'Completed orders count',
            'moderatorStatus' => Yii::t('app', 'Moderator status'),
        ];
    }

    public function getItems()
    {
        /** @var Company[] $companies */
        $companies = Company::find()->all();
        $items = [];
        foreach ($companies as $company) {
            $psCatalog = $company->psCatalog;
            $item = [
                'id' => $company->id,
                'created_at' => $company->created_at,
                'user'   => $company->user->username,
                'title' => $company->title,
                'rating' => $psCatalog->rating,
                'conversion' =>  $psCatalog->rating_log,
                'ordersByPreordersCount' => $company->getOrders()->andWhere('store_order.preorder_id is not null')->groupBy('store_order.id')->count(),
                'quotesCount' => $company->getPreorders()->groupBy('preorder.id')->count(),
                'instantPaymentsCount' => $company->user->getInstantPayments()->count(),
                 'ordersCount' => $company->getOrders()->groupBy('store_order.id')->count(),
                 'paidOrdersCount' => $company->getOrders()->payed()->groupBy('store_order.id')->count(),
                 'completedOrdersCount' => $company->getOrders()->isCompleated()->groupBy('store_order.id')->count(),
                 'moderatorStatus' => $company->moderator_status
                ];
            $items[]= (object) $item;
        }
        return $items;
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}