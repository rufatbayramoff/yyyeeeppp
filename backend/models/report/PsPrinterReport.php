<?php
/**
 * @author Nabi Defacto
 */

namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\components\ArrayHelper;
use common\components\order\TestOrderFactory;
use common\models\PsPrinter;
use Yii;


class PsPrinterReport implements BaseReportInterface
{

    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'printer_id'         => 'Printer Id',
            'user_id'            => 'User Id',
            'ps_id'              => 'Ps Id',
            'printer_title'      => 'Printer Title',
            'ps_title'           => 'Ps Title',
            'email'              => 'email',
            'phone'              => 'phone',
            'phone_status'       => 'Phone status',
            'moderator_status'   => 'Moderator status',
            'is_available'       => 'Is available',
            'test_order_id'      => 'Test Order Id',
            'test_order_status'  => 'Test Order Status',
            'test_order_created' => 'Test Order Created On',
            'user_status'        => 'User status',
            'created_at'         => 'Created At',
            'updated_at'         => 'Updated At',
            'country'            => 'Country',
        ];
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);

        $searchModel  = Yii::createObject(\backend\models\search\PsPrinterSearch::class);
        $dataProvider = $searchModel->search([]);
        $dataProvider->pagination->setPageSize(PHP_INT_MAX);
        /** @var PsPrinter[] $models */
        $models = $dataProvider->getModels();
        $items  = [];
        foreach ($models as $psPrinter) {
            $company = $psPrinter->company;
            if (in_array($company->user_id, $userIds)) {
                continue;
            }
            $items[] = (object)[
                'printer_id' => $psPrinter->id,
                'user_id'    => $company->user_id,
                'ps_id'      => $company->id,
                'printer_title' => $psPrinter->title,
                'ps_title'      => $company->title,
                'email'         => $company->user->email,
                'phone'        => $company->phone_code . $psPrinter->company->phone,
                'phone_status' => $company->phone_status,
                'moderator_status'   => $psPrinter->companyService->moderator_status,
                'is_available'       => $psPrinter->isAvailable(),
                'test_order_id'      => $psPrinter->testOrder?->id,
                'test_order_status'  => $psPrinter->testOrder?->order_state,
                'test_order_created' => $psPrinter->testOrder?->created_at,
                'user_status'        => $company->user->status,
                'created_at'         => $psPrinter->companyService->created_at,
                'updated_at'         => $psPrinter->companyService->updated_at,
                'country'            => $company->country?$company->country->iso_code:''

            ];
        }
        return $items;
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}