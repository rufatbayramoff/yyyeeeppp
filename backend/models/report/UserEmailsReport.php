<?php

namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\components\ArrayHelper;
use common\models\GeoCountry;
use common\models\InvalidEmail;
use common\models\StoreOrder;
use common\models\User;
use common\modules\payment\services\PaymentService;

class UserEmailsReport implements BaseReportInterface
{
    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new \DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'user_id'          => 'User id',
            'name'             => 'Name',
            'username'         => 'UserName',
            'created_at'       => 'User created at',
            'email'            => 'Email',
            'is_invalid_email' => 'Is invalid email',
            'status'           => 'Status',
            'location'         => 'Location'
        ];
    }

    public function getItems()
    {
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);

        $invalidEmails = InvalidEmail::find()->where(['ignored' => 0])->all();
        $invalidEmails = ArrayHelper::getColumn($invalidEmails, 'email');
        $countries     = GeoCountry::find()->all();
        $countries     = ArrayHelper::map($countries, 'id', 'title');

        $usersQ = User::find()
            ->leftJoin('user_profile', 'user_profile.user_id=user.id')
            ->leftJoin('user_address', 'user_address.id=user_profile.address_id')
            ->leftJoin('user_address as address', 'address.user_id=user.id')
            ->notSystem()
            ->andWhere(['NOT IN', 'user.id', $userIds])
            ->select('user.*,
             user_profile.full_name, user_profile.firstname, user_profile.lastname, user_profile.timezone_id,
             user_address.country_id as profile_country_id, user_address.region as profile_region, user_address.city as profile_city,
             address.country_id as country_id, address.region as region, address.city as city
             ')
            ->asArray();

        $users = $usersQ->all();

        $items = [];
        foreach ($users as $userArray) {
            $line          = new \stdClass();
            $line->user_id = $userArray['id'];
            $line->name    = $userArray['full_name'];
            if (!$line->name) {
                $line->name = $userArray['firstname'] . ' ' . $userArray['lastname'];
            }
            $line->username   = $userArray['username'];
            $line->created_at = date('Y-m-d', $userArray['created_at']);
            $line->email      = '';
            if (!$userArray['email']) {
                $user = new User();
                User::populateRecord($user, $userArray);
                if ($user->thingiverseUser->email) {
                    $line->email = $user->thingiverseUser->email;
                }
                if (!$line->email && $user->storeOrders) {
                    foreach ($user->storeOrders as $storeOrder) {
                        if ($storeOrder->shipAddress->email) {
                            $line->email = $storeOrder->shipAddress->email;
                            break;
                        }
                    }
                }
            } else {
                $line->email = $userArray['email'];
            }
            if ($userArray['profile_country_id']) {
                $line->location = $countries[$userArray['profile_country_id']] .
                    ($userArray['profile_region'] ? ', ' . $userArray['profile_region'] : '') .
                    ($userArray['profile_city'] ? ', ' . $userArray['profile_city'] : '');
            } elseif ($userArray['country_id']) {
                $line->location = $countries[$userArray['country_id']] .
                    ($userArray['region'] ? ', ' . $userArray['region'] : '') .
                    ($userArray['city'] ? ', ' . $userArray['city'] : '');
            } elseif ($userArray['timezone_id']) {
                $line->location = $userArray['timezone_id'];
            } else {
                $line->location = '';
            }
            $line->is_invalid_email = in_array($line->email, $invalidEmails) ? 1 : 0;
            $line->status           = $userArray['status'];
            $items[]                = $line;
        }
        return $items;
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}