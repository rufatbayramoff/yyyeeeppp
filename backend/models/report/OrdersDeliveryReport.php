<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.06.19
 * Time: 10:50
 */

namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\models\DeliveryType;
use common\models\StoreOrderDelivery;

class OrdersDeliveryReport implements BaseReportInterface
{
    /**
     * @return OrdersDeliveryReport
     */
    public static function create()
    {
        return new self;
    }

    public function getColumnsNames()
    {
        return [
            'id'                 => 'Order id',
            'created_at'         => 'Date',
            'common_contact_name'       => 'Contact name',
            'phone'              => 'Phone number',
            'delivery_type_text' => 'Delivery type',
            'tracking_shipper'   => 'Delivery service'
        ];
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);
        $userIds = join(',', $userIds);

        $sql = "
SELECT
  store_order.id,
  store_order.created_at,
  user_address.common_contact_name,
  user_address.phone,
  store_order_delivery.delivery_type_id,
  store_order_delivery.tracking_shipper
FROM
  store_order
LEFT JOIN
  store_order_delivery
ON
  store_order_delivery.order_id = store_order.id
LEFT JOIN
  user_address
ON
  user_address.id = store_order.ship_address_id
WHERE
  (
    store_order.order_state = 'completed' OR store_order.order_state = 'closed'
  ) AND store_order.user_id NOT IN($userIds)
ORDER BY
  store_order.id ASC
        ";
        $allRecords = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        foreach ($allRecords as $key => $item) {
            $typeId = $allRecords[$key]->delivery_type_id;
            $typeText = '';
            if ($typeId == DeliveryType::PICKUP_ID) {
                $typeText = 'pickup';
            } elseif ($typeId == DeliveryType::STANDART_ID) {
                $typeText = 'standard';
            } elseif ($typeId == DeliveryType::INTERNATIONAL_STANDART_ID) {
                $typeText = 'international';
            }
            $allRecords[$key]->delivery_type_text = $typeText;
        }
        return $allRecords;
    }

    public function setParams(array $params)
    {
    }
}
