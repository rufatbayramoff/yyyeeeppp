<?php
namespace backend\models\report;

use backend\modules\statistic\reports\BaseReportInterface;
use common\models\ThingiverseReport as ThingiverseReportAlias;
use DateTime;

class ThingBalanceReport implements BaseReportInterface
{
    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'order_id'                   => 'Thingiverse order id',
            'date'                       => 'Date',
            'thing_id'                   => 'Thing id',
            'status'                     => 'Status',
            'earnings'                   => 'Earnings',
            'platform_fee'               => 'Platform_fee',
            'transaction_fee'            => 'Transaction fee',
            'app_transaction_code'       => 'Treatstock order id',
            'printing_fee'               => 'Printing fee',
            'shipping'                   => 'Shipping',
            'refund'                     => 'Refund',
            'treatstock_transaction_fee' => 'Treatstock transaction fee'
        ];
    }

    public function getItems()
    {
        $thingiverseReports = ThingiverseReportAlias::find()->orderBy('date')->all();
        return $thingiverseReports;
    }

    public function setParams(array $params)
    {
    }
}