<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Model3dPartCncParams;

/**
 * Model3dPartCncParamsSearch represents the model behind the search form about `common\models\Model3dPartCncParams`.
 */
class Model3dPartCncParamsSearch extends Model3dPartCncParams
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['filepath', 'finefile', 'roughfile', 'analyze'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Model3dPartCncParams::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'filepath', $this->filepath])
            ->andFilterWhere(['like', 'finefile', $this->finefile])
            ->andFilterWhere(['like', 'roughfile', $this->roughfile])
            ->andFilterWhere(['like', 'analyze', $this->analyze]);

        return $dataProvider;
    }
}
