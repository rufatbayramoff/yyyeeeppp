<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreOrderAttempDates;

/**
 * StoreOrderAttempDatesSearch represents the model behind the search form about `common\models\StoreOrderAttempDates`.
 */
class StoreOrderAttempDatesSearch extends StoreOrderAttempDates
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attemp_id'], 'integer'],
            [['request_reason', 'accepted_date', 'fact_printed_at', 'plan_printed_at', 'start_print_at', 'finish_print_at', 'expired_at', 'delivered_at', 'shipped_at', 'received_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreOrderAttempDates::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'attemp_id' => $this->attemp_id,
            'accepted_date' => $this->accepted_date,
            'fact_printed_at' => $this->fact_printed_at,
            'plan_printed_at' => $this->plan_printed_at,
            'start_print_at' => $this->start_print_at,
            'finish_print_at' => $this->finish_print_at,
            'expired_at' => $this->expired_at,
            'delivered_at' => $this->delivered_at,
            'shipped_at' => $this->shipped_at,
            'received_at' => $this->received_at,
        ]);

        $query->andFilterWhere(['like', 'request_reason', $this->request_reason]);

        return $dataProvider;
    }
}
