<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreOrderReviewShare;

/**
 * StoreOrderReviewShareSearch represents the model behind the search form about `common\models\StoreOrderReviewShare`.
 */
class StoreOrderReviewShareSearch extends StoreOrderReviewShare
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'review_id', 'photo_id'], 'integer'],
            [['social_type', 'text', 'created_at', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreOrderReviewShare::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'review_id' => $this->review_id,
            'photo_id' => $this->photo_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'social_type', $this->social_type])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'status', $this->status]);

        $query->addOrderBy(['created_at' => SORT_DESC]);

        return $dataProvider;
    }
}
