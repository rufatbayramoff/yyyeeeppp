<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ThingiverseOrder;

/**
 * ThingiverseOrderSearch represents the model behind the search form about `common\models\ThingiverseOrder`.
 */
class ThingiverseOrderSearch extends ThingiverseOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['thingiverse_order_id', 'order_id', 'is_shipped', 'is_cancelled'], 'integer'],
            [['created_at', 'updated_at', 'refund_note', 'note', 'status', 'api_json'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ThingiverseOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'thingiverse_order_id' => $this->thingiverse_order_id,
            'order_id' => $this->order_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_shipped' => $this->is_shipped,
            'is_cancelled' => $this->is_cancelled,
        ]);

        $query->andFilterWhere(['like', 'refund_note', $this->refund_note])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'api_json', $this->api_json]);

        return $dataProvider;
    }
}
