<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BrowserPushSubscriptionUser;

/**
 * BrowserPushSubscriptionUserSearch represents the model behind the search form about `common\models\BrowserPushSubscriptionUser`.
 */
class BrowserPushSubscriptionUserSearch extends BrowserPushSubscriptionUser
{

    public $user_agent;

    public $endpoint;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'created_at', 'browser_push_subscription_uid', 'ip', 'endpoint', 'user_agent'], 'safe'],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BrowserPushSubscriptionUser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('browserPushSubscription');

        // grid filtering conditions
        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'user_id'    => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'browser_push_subscription_uid', $this->browser_push_subscription_uid])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'browser_push_subscription.endpoint', $this->endpoint])
            ->andFilterWhere(['like', 'browser_push_subscription.user_agent', $this->user_agent]);

        return $dataProvider;
    }
}
