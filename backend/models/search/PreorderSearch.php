<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Preorder;

/**
 * PreorderSearch represents the model behind the search form about `common\models\Preorder`.
 */
class PreorderSearch extends Preorder
{
    public $offered;

    public $preorders300 = 0;

    public $serviceType = '';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ps_id', 'budget', 'estimate_time', 'decline_initiator', 'offer_estimate_time', 'offered'], 'integer'],
            [['user_id'], 'string'],
            [['status', 'created_at', 'name', 'description', 'message', 'email', 'decline_reason', 'decline_comment', 'offer_description', 'preorders300', 'serviceType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'serviceType' => 'Service type'
            ]

        );
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Preorder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'                  => $this->id,
            'created_at'          => $this->created_at,
            'ps_id'               => $this->ps_id,
            'budget'              => $this->budget,
            'estimate_time'       => $this->estimate_time,
            'decline_initiator'   => $this->decline_initiator,
            'offer_estimate_time' => $this->offer_estimate_time,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'decline_reason', $this->decline_reason])
            ->andFilterWhere(['like', 'decline_comment', $this->decline_comment])
            ->andFilterWhere(['like', 'offer_description', $this->offer_description]);

        if ($this->user_id) {
            if (is_numeric($this->user_id)) {
                $query->andFilterWhere(['user_id' => $this->user_id]);
            } else {
                $query->joinWith('user');
                $query->andWhere(['or', ['like', 'user.email', $this->user_id], ['like', 'user.username', $this->user_id]]);
            }
        } else {
            $internalUserIds = app('setting')->get('store.internalOrdersCustomerUserId');
            $query->andWhere(['not in', 'user_id', $internalUserIds]);
        }

        if ($this->preorders300) {
            $query->alarmPreorder300();
        }

        if ($this->serviceType) {
            $query->serviceType($this->serviceType);
        }

        $query->orderBy(Preorder::column('id') . " DESC");

        if ($this->offered !== '' && $this->offered !== null) {
            $this->offered = (int)$this->offered;
            if ($this->offered === 1) {
                $query->andWhere(['is not', 'primary_payment_invoice_uuid', dbexpr('null')]);
            } elseif ($this->offered === 0) {
                $query->andWhere(['is', 'primary_payment_invoice_uuid', dbexpr('null')]);
            }
        }
        return $dataProvider;
    }
}
