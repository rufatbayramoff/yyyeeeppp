<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentInvoiceItem;

/**
 * PaymentInvoiceItemSearch represents the model behind the search form about `common\models\PaymentInvoiceItem`.
 */
class PaymentInvoiceItemSearch extends PaymentInvoiceItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'payment_invoice_uuid', 'title', 'description', 'measure'], 'safe'],
            [['pos'], 'integer'],
            [['qty', 'price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentInvoiceItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pos' => $this->pos,
            'qty' => $this->qty,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'payment_invoice_uuid', $this->payment_invoice_uuid])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'measure', $this->measure]);

        return $dataProvider;
    }
}
