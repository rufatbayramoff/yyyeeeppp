<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserLoginLog;

/**
 * UserLoginLogSearch represents the model behind the search form about `common\models\UserLoginLog`.
 */
class UserLoginLogSearch extends UserLoginLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['login_type', 'result', 'remote_addr', 'http_user_agent', 'created_at', 'logout_at', 'http_referer', 'post_data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserLoginLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['created_at'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'logout_at' => $this->logout_at,
        ]);
        
        $query->andFilterWhere(['like', 'login_type', $this->login_type])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'remote_addr', $this->remote_addr])
            ->andFilterWhere(['like', 'http_user_agent', $this->http_user_agent])
            ->andFilterWhere(['like', 'http_referer', $this->http_referer])
            ->andFilterWhere(['like', 'post_data', $this->post_data]);

        return $dataProvider;
    }
}
