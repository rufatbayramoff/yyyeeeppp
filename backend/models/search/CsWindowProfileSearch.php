<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CsWindowProfile;

/**
 * CsWindowProfileSearch represents the model behind the search form about `common\models\CsWindowProfile`.
 */
class CsWindowProfileSearch extends CsWindowProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'thickness', 'chambers', 'max_glass', 'max_width', 'max_height', 'noise_reduction', 'thermal_resistance'], 'integer'],
            [['updated_at', 'title', 'cs_window_uid'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CsWindowProfile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'updated_at' => $this->updated_at,
            'thickness' => $this->thickness,
            'chambers' => $this->chambers,
            'max_glass' => $this->max_glass,
            'max_width' => $this->max_width,
            'max_height' => $this->max_height,
            'noise_reduction' => $this->noise_reduction,
            'thermal_resistance' => $this->thermal_resistance,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'cs_window_uid', $this->cs_window_uid]);

        return $dataProvider;
    }
}
