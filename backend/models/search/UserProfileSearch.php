<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserProfile;

/**
 * UserProfileSearch represents the model behind the search form about `common\models\UserProfile`.
 */
class UserProfileSearch extends UserProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'address_id', 'is_printservice', 'is_modelshop', 'default_license_id'], 'integer'],
            [['full_name', 'dob_date', 'website', 'phone', 'updated_at', 'current_lang', 'current_currency_iso', 'current_metrics', 'timezone_id', 'firstname', 'lastname', 'avatar_url', 'background_url', 'gender', 'printservice_title', 'modelshop_title', 'info_about', 'info_skills'], 'safe'],
            [['phone_confirmed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserProfile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'dob_date' => $this->dob_date,
            'address_id' => $this->address_id,
            'phone_confirmed' => $this->phone_confirmed,
            'updated_at' => $this->updated_at,
            'is_printservice' => $this->is_printservice,
            'is_modelshop' => $this->is_modelshop,
            'default_license_id' => $this->default_license_id,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'current_lang', $this->current_lang])
            ->andFilterWhere(['like', 'current_currency_iso', $this->current_currency_iso])
            ->andFilterWhere(['like', 'current_metrics', $this->current_metrics])
            ->andFilterWhere(['like', 'timezone_id', $this->timezone_id])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'avatar_url', $this->avatar_url])
            ->andFilterWhere(['like', 'background_url', $this->background_url])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'printservice_title', $this->printservice_title])
            ->andFilterWhere(['like', 'modelshop_title', $this->modelshop_title])
            ->andFilterWhere(['like', 'info_about', $this->info_about])
            ->andFilterWhere(['like', 'info_skills', $this->info_skills]);

        return $dataProvider;
    }
}
