<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreUnitShoppingCandidate;

/**
 * StoreUnitShoppingCandidateSearch represents the model behind the search form about `common\models\StoreUnitShoppingCandidate`.
 */
class StoreUnitShoppingCandidateSearch extends StoreUnitShoppingCandidate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_unit_id', 'user_session_id'], 'integer'],
            [['type', 'create_date', 'view_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreUnitShoppingCandidate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_unit_id' => $this->store_unit_id,
            'user_session_id' => $this->user_session_id,
            'create_date' => $this->create_date,
            'view_date' => $this->view_date,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
