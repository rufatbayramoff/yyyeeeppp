<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TsCertificationClass;

/**
 * TsCertificationClassSearch represents the model behind the search form about `common\models\TsCertificationClass`.
 */
class TsCertificationClassSearch extends TsCertificationClass
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'max_orders_count'], 'integer'],
            [['title', 'description', 'service_type', 'currency'], 'safe'],
            [['price', 'max_order_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TsCertificationClass::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'max_orders_count' => $this->max_orders_count,
            'max_order_price' => $this->max_order_price,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'service_type', $this->service_type])
            ->andFilterWhere(['like', 'currency', $this->currency]);

        return $dataProvider;
    }
}
