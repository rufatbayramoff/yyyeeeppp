<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Model3dReplica;

/**
 * Model3dReplicaSearch represents the model behind the search form about `common\models\Model3dReplica`.
 */
class Model3dReplicaSearch extends Model3dReplica
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_unit_id', 'user_id', 'is_published', 'is_printer_ready', 'cover_file_id', 'stat_views', 'category_id', 'model3d_texture_id', 'original_model3d_id'], 'integer'],
            [['title', 'description', 'created_at', 'updated_at', 'published_at', 'dimensions', 'status', 'model_units', 'source'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Model3dReplica::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_unit_id' => $this->store_unit_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'published_at' => $this->published_at,
            'is_printer_ready' => $this->is_printer_ready,
            'cover_file_id' => $this->cover_file_id,
            'stat_views' => $this->stat_views,
            'category_id' => $this->category_id,
            'model3d_texture_id' => $this->model3d_texture_id,
            'original_model3d_id' => $this->original_model3d_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'dimensions', $this->dimensions])
            ->andFilterWhere(['like', 'model_units', $this->model_units])
            ->andFilterWhere(['like', 'source', $this->source]);

        return $dataProvider;
    }
}
