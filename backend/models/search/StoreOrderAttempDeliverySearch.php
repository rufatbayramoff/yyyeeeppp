<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreOrderAttempDelivery;

/**
 * StoreOrderAttempDeliverySearch represents the model behind the search form about `common\models\StoreOrderAttempDelivery`.
 */
class StoreOrderAttempDeliverySearch extends StoreOrderAttempDelivery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_attemp_id', 'file_id'], 'integer'],
            [['tracking_number', 'tracking_shipper', 'send_message_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreOrderAttempDelivery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_attemp_id' => $this->order_attemp_id,
            'file_id' => $this->file_id,
            'send_message_at' => $this->send_message_at,
        ]);

        $query->andFilterWhere(['like', 'tracking_number', $this->tracking_number])
            ->andFilterWhere(['like', 'tracking_shipper', $this->tracking_shipper]);

        return $dataProvider;
    }
}
