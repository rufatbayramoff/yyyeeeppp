<?php

namespace backend\models\search;

use common\models\ConnectedApp;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PromoAppSearch represents the model behind the search form about `common\models\PromoApp`.
 */
class ConnectedAppSearch extends ConnectedApp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_beta'], 'integer'],
            [['code', 'title', 'url', 'short_descr', 'info', 'instructions'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ConnectedApp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_beta' => $this->is_beta,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'short_descr', $this->short_descr])
            ->andFilterWhere(['like', 'info', $this->info])
            ->andFilterWhere(['like', 'instructions', $this->instructions]);
        $query->orderBy('order');

        return $dataProvider;
    }
}
