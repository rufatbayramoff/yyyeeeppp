<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MsgMember;

/**
 * MsgMemberSearch represents the model behind the search form about `common\models\MsgMember`.
 */
class MsgMemberSearch extends MsgMember
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'topic_id', 'folder_id', 'have_unreaded_messages', 'is_deleted'], 'integer'],
            [['hide_message_before'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MsgMember::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'topic_id' => $this->topic_id,
            'folder_id' => $this->folder_id,
            'have_unreaded_messages' => $this->have_unreaded_messages,
            'is_deleted' => $this->is_deleted,
            'hide_message_before' => $this->hide_message_before,
        ]);

        return $dataProvider;
    }
}
