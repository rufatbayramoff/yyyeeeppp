<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserTaxInfo;

/**
 * UserTaxInfoSearch represents the model behind the search form about `common\models\UserTaxInfo`.
 */
class UserTaxInfoSearch extends UserTaxInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'is_usa', 'address_id', 'mailing_address_id', 'encrypted', 'email_notify', 'rsa_tax_rate'], 'integer'],
            [['created_at', 'updated_at', 'identification_type', 'identification', 'classification', 'name', 'business_name', 'status', 'place_country', 'signature', 'sign_date', 'sign_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UserTaxInfo::find()->with(['address', 'user'])
            ->joinWith('address');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['updated_at'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            UserTaxInfo::column('user_id') => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_usa' => $this->is_usa,
            'address_id' => $this->address_id,
            'mailing_address_id' => $this->mailing_address_id,
            'rsa_tax_rate' => $this->rsa_tax_rate,
            'email_notify' => $this->email_notify,
            'sign_date' => $this->sign_date,
            'sign_type' => $this->sign_type
        ]);

        $query->andFilterWhere(['like', 'identification_type', $this->identification_type])
            ->andFilterWhere(['like', 'identification', $this->identification])
            ->andFilterWhere(['like', 'classification', $this->classification])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'business_name', $this->business_name])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'place_country', $this->place_country])
            ->andFilterWhere(['like', 'signature', $this->signature]);
        /*
        $whitelist = \frontend\models\site\DenyCountry::getWhiteList();
        $isoCodes2 = array_keys($whitelist);
        if(!empty($isoCodes2)){
            $query->andWhere(['IN', 'place_country', $isoCodes2]);
        }
        foreach($whitelist as $iso=>$country){
            //$query->andWhere(['=', 'place_country', $iso]);
        }
         * 
         */
        return $dataProvider;
    }

    /**
     *
     */
    const SIGN_TYPE_LABELS = [
        'form' => 'form',
        'email' => 'email'
    ];
}
