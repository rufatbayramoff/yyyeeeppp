<?php namespace backend\models\search;

use common\models\Model3d;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\StoreUnit;
use common\modules\product\interfaces\ProductInterface;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\UserProfile;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{

    public $full_name;
    public $language;

    public $user_type;

    const USER_TYPE_AUTHOR = 'author';
    const USER_TYPE_PS = 'ps';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status','is_company_creating_in_progress'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'trustlevel'], 'safe'],
            [['full_name', 'user_type', 'language'], 'safe'],
            [['created_at', 'updated_at', 'lastlogin_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        $query->joinWith('userProfile');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['full_name'] = [
            'asc' => [UserProfile::column('full_name') => SORT_ASC],
            'desc' => [UserProfile::column('full_name') => SORT_DESC],
        ];
        $dataProvider->sort->attributes['language'] = [
            'asc' => [UserProfile::column('current_lang') => SORT_ASC],
            'desc' => [UserProfile::column('current_lang') => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'is_company_creating_in_progress' => $this->is_company_creating_in_progress
        ]);
        if (!empty($this->created_at)) {
            $query->andFilterDate('created_at', $this->created_at);
        }
        if (!empty($this->lastlogin_at)) {
            $query->andFilterDate('lastlogin_at', $this->lastlogin_at);
        }
        if (!empty($this->updated_at)) {
            $query->andFilterDate('updated_at', $this->updated_at);
        }
        $query->andFilterWhere(['like', UserProfile::column('full_name'), $this->full_name])
            ->andFilterWhere([
                UserProfile::column('current_lang') => $this->language
        ]);
        $query->andFilterWhere(['trustlevel'=>$this->trustlevel]);
        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }

    public function filterDate($query, $field, $value)
    {
        $parts = explode("-", $value);
        $from = strtotime($parts[0] . ' 00:00:00');
        $to = strtotime($parts[1] . ' 23:59:59');
        $query->andWhere(
            "$field >= :date_from AND $field <= :date_to", [':date_to' => $to, ':date_from' => $from]
        );
        return $query;
    }
}
