<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\WikiMaterialManufactureType;

/**
 * WikiMaterialManufactureTypeSearch represents the model behind the search form about `common\models\WikiMaterialManufactureType`.
 */
class WikiMaterialManufactureTypeSearch extends WikiMaterialManufactureType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wiki_material_id', 'manufacture_type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WikiMaterialManufactureType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'wiki_material_id' => $this->wiki_material_id,
            'manufacture_type_id' => $this->manufacture_type_id,
        ]);

        return $dataProvider;
    }
}
