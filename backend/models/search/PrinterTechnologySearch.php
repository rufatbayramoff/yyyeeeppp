<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PrinterTechnology;

/**
 * PrinterTechnologySearch represents the model behind the search form about `common\models\PrinterTechnology`.
 */
class PrinterTechnologySearch extends PrinterTechnology
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'print_speed_min', 'print_speed_max'], 'integer'],
            [['title', 'created_at', 'updated_at'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PrinterTechnology::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_active' => $this->is_active,
            'print_speed_min' => $this->print_speed_min,
            'print_speed_max' => $this->print_speed_max,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

    public function getActiveInactive()
    {
        return [1 => 'Active', 0 => 'Inactive'];
    }
}
