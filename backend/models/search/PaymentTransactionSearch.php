<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentTransaction;

/**
 * PaymentTransactionSearch represents the model behind the search form about `common\models\PaymentTransaction`.
 */
class PaymentTransactionSearch extends PaymentTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'first_payment_detail_id'], 'integer'],
            [['transaction_id', 'vendor', 'created_at', 'updated_at', 'status', 'type', 'currency', 'comment', 'is_notified_need_settled', 'refund_id', 'last_original_transaction', 'details'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['created_at'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'first_payment_detail_id' => $this->first_payment_detail_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'transaction_id', $this->transaction_id])
            ->andFilterWhere(['like', 'vendor', $this->vendor])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'is_notified_need_settled', $this->is_notified_need_settled])
            ->andFilterWhere(['like', 'refund_id', $this->refund_id])
            ->andFilterWhere(['like', 'last_original_transaction', $this->last_original_transaction]);
        return $dataProvider;
    }

   public function searchBankPayout($params)
   {
       $this->vendor  = PaymentTransaction::VENDOR_BANK_PAYOUT;
       return $this->search($params);
   }
}
