<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HomePageFeaturedProduct;

/**
 * HomePageFeaturedProductSearch represents the model behind the search form about `common\models\HomePageFeaturedProduct`.
 */
class HomePageFeaturedProductSearch extends HomePageFeaturedProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['featured_category_id'], 'integer'],
            [['product_uuid'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HomePageFeaturedProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'featured_category_id' => $this->featured_category_id,
        ]);

        $query->andFilterWhere(['like', 'product_uuid', $this->product_uuid]);

        return $dataProvider;
    }
}
