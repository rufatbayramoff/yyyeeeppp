<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SeoPageAutofillTemplate;

/**
 * SeoPageAutofillTemplateSearch represents the model behind the search form about `common\models\SeoPageAutofillTemplate`.
 */
class SeoPageAutofillTemplateSearch extends SeoPageAutofillTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['type', 'template_name', 'is_apply_created', 'title', 'meta_description', 'meta_keywords', 'header', 'created_at', 'lang_iso'], 'safe'],
            [['is_default'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SeoPageAutofillTemplate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at
        ]);
        if($this->is_default!=='' && $this->is_default!==null){
            $query->andFilterWhere(['is_default' => (int)$this->is_default]);
        }
        if($this->is_apply_created!=='' && $this->is_apply_created!==null){
            $query->andFilterWhere(['is_apply_created' => (int)$this->is_apply_created]);
        }

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'template_name', $this->template_name])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'header', $this->header])
            ->andFilterWhere(['like', 'lang_iso', $this->lang_iso]);

        return $dataProvider;
    }
}
