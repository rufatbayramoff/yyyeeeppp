<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\WikiMaterial;

/**
 * WikiMaterialSearch represents the model behind the search form about `common\models\WikiMaterial`.
 */
class WikiMaterialSearch extends WikiMaterial
{
    public $search;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['code', 'title', 'about', 'description', 'is_active'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WikiMaterial::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 21,
                ],
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (array_key_exists('search', $params)) {
            $this->search = $params['search'];
            $query->andFilterWhere(
                [
                    'like',
                    'title',
                    $this->search
                ]
            );
        }
        $query->andFilterCompare('is_active', $this->is_active);

        // grid filtering conditions
        $query->andFilterWhere(
            [
                'id' => $this->id,
            ]
        );

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'about', $this->about])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);


        return $dataProvider;
    }
}
