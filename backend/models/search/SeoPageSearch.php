<?php

namespace backend\models\search;

use common\models\base\SeoPageAutofillTemplate;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SeoPage;

/**
 * SeoPageSearch represents the model behind the search form about `common\models\SeoPage`.
 */
class SeoPageSearch extends SeoPage
{
    /**
     * autofill page type used
     * @var
     */
    public $object_type;

    /**
     * autofill template id used for this page
     * @var
     */
    public $autofill_template;

    public $lang = 'en-US';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'autofill_template'], 'integer'],
            [['url', 'title', 'lang', 'object_type', 'header', 'meta_description', 'meta_keywords', 'header_text', 'footer_text', 'updated_at'], 'safe'],
            [['is_active', 'is_locked', 'is_sitemap', 'need_review'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SeoPage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $query->joinWith('seoPageIntls');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!empty($this->object_type)){
            //$query->joinWith('seoPageAutofill');
            if($this->object_type==='None'){
                $query->andWhere(['seo_page_autofill.id'=>null]);
            }else{
                $query->andFilterWhere(['seo_page_autofill.object_type'=>$this->object_type]);
            }
        }
        if (!empty($this->autofill_template)) {
            if ($this->autofill_template == '-1') {
                $query->joinWith('seoPageAutofill.template');
                $query->andWhere(['seo_page_autofill_template.id' => null]);
            } else {
                $template = SeoPageAutofillTemplate::findOne($this->autofill_template);
                $this->lang = $template->lang_iso;
                if ($this->lang == 'en-US' || $this->lang == '') {
                    $query->joinWith('seoPageAutofill.template');
                    $query->andFilterWhere(['seo_page_autofill_template.id' => $this->autofill_template]);
                } else {
                    $query->joinWith('seoPageIntls.seoPageAutofill.template');
                    $query->andFilterWhere(['seo_page_autofill_template.id' => $this->autofill_template]);
                }
            }
        } else {
            $query->joinWith('seoPageAutofill.template');
        }

        if(!empty($this->lang) && $this->lang!='en-US'){
            $query->andFilterWhere(['seo_page_intl.lang_iso'=>$this->lang]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'seo_page.id' => $this->id,
            'seo_page.updated_at' => $this->updated_at,
        ]);

        if($this->is_active!=='' && $this->is_active!==null){
            $query->andFilterWhere(['seo_page.is_active' => (int)$this->is_active]);
        }
        if($this->is_locked!=='' && $this->is_locked!==null){
            $query->andFilterWhere(['seo_page.is_locked' => (int)$this->is_locked]);
        }
        if($this->need_review!=='' && $this->need_review!==null){
            $query->andFilterWhere(['seo_page.need_review' => (int)$this->need_review]);
        }
        if($this->is_sitemap!=='' && $this->is_sitemap!==null){
            $query->andFilterWhere(['seo_page.is_sitemap' => (int)$this->is_sitemap]);
        }
        if(!empty($this->meta_description)){
            $query->andFilterWhere(['like', 'seo_page.meta_description', $this->meta_description]);
        }
        if(!empty($this->meta_keywords)){
            $query->andFilterWhere(['like', 'seo_page.meta_keywords', $this->meta_keywords]);
        }
        if(!empty($this->url)){
            if(strpos($this->url, '%')!==false){
                $query->andFilterWhere(['like', 'seo_page.url', dbexpr('"'.$this->url.'"')]);
            }else{
                $query->andFilterWhere(['like', 'seo_page.url', $this->url]);
            }
        }
        $query->andFilterWhere(['like', 'seo_page.title', $this->title])
            ->andFilterWhere(['like', 'seo_page.header', $this->header])
            ->andFilterWhere(['like', 'seo_page.header_text', $this->header_text])
            ->andFilterWhere(['like', 'seo_page.footer_text', $this->footer_text]);
        $query->groupBy('seo_page.id');

        return $dataProvider;
    }
}
