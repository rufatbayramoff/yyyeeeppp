<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CsWindowSnapshot;

/**
 * CsWindowSnapshotSearch represents the model behind the search form about `common\models\CsWindowSnapshot`.
 */
class CsWindowSnapshotSearch extends CsWindowSnapshot
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['uid', 'created_at', 'cs_window_uid', 'furniture', 'glass', 'profile', 'service'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CsWindowSnapshot::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'cs_window_uid', $this->cs_window_uid])
            ->andFilterWhere(['like', 'furniture', $this->furniture])
            ->andFilterWhere(['like', 'glass', $this->glass])
            ->andFilterWhere(['like', 'profile', $this->profile])
            ->andFilterWhere(['like', 'service', $this->service]);

        return $dataProvider;
    }
}
