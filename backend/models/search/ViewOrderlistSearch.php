<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ViewOrderlist;

/**
 * ViewOrderlistSearch represents the model behind the search form about `common\models\ViewOrderlist`.
 */
class ViewOrderlistSearch extends ViewOrderlist
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'buyer_id', 'designer_id', 'printer_id'], 'integer'],
            [['billed_at', 'created_at', 'accepted_at', 'ready_sent_at', 'delivered_at', 'shipped_at', 'received_at', 'to_country', 'to_city', 'comment', 'printer_title', 'ps_country', 'delivery_type', 'delivery_opts', 'currency'], 'safe'],
            [['total_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ViewOrderlist::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'buyer_id' => $this->buyer_id,
            'billed_at' => $this->billed_at,
            'designer_id' => $this->designer_id,
            'created_at' => $this->created_at,
            'accepted_at' => $this->accepted_at,
            'ready_sent_at' => $this->ready_sent_at,
            'delivered_at' => $this->delivered_at,
            'shipped_at' => $this->shipped_at,
            'received_at' => $this->received_at,
            'printer_id' => $this->printer_id,
            'total_amount' => $this->total_amount,
        ]);

        $query->andFilterWhere(['like', 'to_country', $this->to_country])
            ->andFilterWhere(['like', 'to_city', $this->to_city])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'printer_title', $this->printer_title])
            ->andFilterWhere(['like', 'ps_country', $this->ps_country])
            ->andFilterWhere(['like', 'delivery_type', $this->delivery_type])
            ->andFilterWhere(['like', 'delivery_opts', $this->delivery_opts])
            ->andFilterWhere(['like', 'currency', $this->currency]);

        return $dataProvider;
    }
}
