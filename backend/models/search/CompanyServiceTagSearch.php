<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CompanyServiceTag;

/**
 * CompanyServiceTagSearch represents the model behind the search form about `common\models\CompanyServiceTag`.
 */
class CompanyServiceTagSearch extends CompanyServiceTag
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_service_id', 'tag_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyServiceTag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_service_id' => $this->company_service_id,
            'tag_id' => $this->tag_id,
        ]);

        return $dataProvider;
    }
}
