<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PsMachineDelivery;

/**
 * PsMachineDeliverySearch represents the model behind the search form about `common\models\PsMachineDelivery`.
 */
class PsMachineDeliverySearch extends PsMachineDelivery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ps_machine_id', 'delivery_type_id'], 'integer'],
            [['comment', 'carrier'], 'safe'],
            [['carrier_price', 'free_delivery', 'packing_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PsMachineDelivery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ps_machine_id' => $this->ps_machine_id,
            'delivery_type_id' => $this->delivery_type_id,
            'carrier_price' => $this->carrier_price,
            'free_delivery' => $this->free_delivery,
            'packing_price' => $this->packing_price,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'carrier', $this->carrier]);

        return $dataProvider;
    }
}
