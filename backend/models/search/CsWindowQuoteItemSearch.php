<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CsWindowQuoteItem;

/**
 * CsWindowQuoteItemSearch represents the model behind the search form about `common\models\CsWindowQuoteItem`.
 */
class CsWindowQuoteItemSearch extends CsWindowQuoteItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'qty', 'frame_id', 'glass_id', 'profile_id', 'furniture_id'], 'integer'],
            [['uid', 'cs_window_quote_uid', 'title', 'quote_parameters', 'windowsill', 'lamination', 'slopes', 'tinting', 'energy_saver', 'installation'], 'safe'],
            [['cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CsWindowQuoteItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'qty' => $this->qty,
            'cost' => $this->cost,
            'frame_id' => $this->frame_id,
            'glass_id' => $this->glass_id,
            'profile_id' => $this->profile_id,
            'furniture_id' => $this->furniture_id,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'cs_window_quote_uid', $this->cs_window_quote_uid])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'quote_parameters', $this->quote_parameters])
            ->andFilterWhere(['like', 'windowsill', $this->windowsill])
            ->andFilterWhere(['like', 'lamination', $this->lamination])
            ->andFilterWhere(['like', 'slopes', $this->slopes])
            ->andFilterWhere(['like', 'tinting', $this->tinting])
            ->andFilterWhere(['like', 'energy_saver', $this->energy_saver])
            ->andFilterWhere(['like', 'installation', $this->installation]);

        return $dataProvider;
    }
}
