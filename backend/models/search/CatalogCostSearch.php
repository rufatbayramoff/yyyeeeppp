<?php

namespace backend\models\search;

use common\models\CatalogCost;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CatalogCostSearch represents the model behind the search form about `common\models\CatalogCost`.
 */
class CatalogCostSearch extends CatalogCost
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_unit_id', 'country_id'], 'integer'],
            [['cost_usd'], 'number'],
            [['update_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogCost::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'store_unit_id' => $this->store_unit_id,
            'country_id' => $this->country_id,
            'cost_usd' => $this->cost_usd,
            'update_date' => $this->update_date,
        ]);

        return $dataProvider;
    }
}
