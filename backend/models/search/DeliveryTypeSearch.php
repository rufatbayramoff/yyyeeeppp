<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DeliveryType;

/**
 * DeliveryTypeSearch represents the model behind the search form about `common\models\DeliveryType`.
 */
class DeliveryTypeSearch extends DeliveryType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_free_delivery', 'is_work_time', 'is_active', 'is_carrier'], 'integer'],
            [['title', 'code', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeliveryType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_free_delivery' => $this->is_free_delivery,
            'is_work_time' => $this->is_work_time,
            'is_active' => $this->is_active,
            'is_carrier' => $this->is_carrier,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }
}
