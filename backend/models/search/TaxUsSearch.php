<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaxUs;

/**
 * TaxUsSearch represents the model behind the search form about `common\models\TaxUs`.
 */
class TaxUsSearch extends TaxUs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['zip_code', 'state_abbrevation', 'county_name', 'city_name', 'is_tax_shipping_alone', 'is_tax_ship_handling', 'created_at', 'updated_at'], 'safe'],
            [['state_sales_tax', 'county_sales_tax', 'city_sales_tax', 'state_use_tax', 'county_use_tax', 'city_use_tax', 'total_sales_tax', 'total_use_tax'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaxUs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'state_sales_tax' => $this->state_sales_tax,
            'county_sales_tax' => $this->county_sales_tax,
            'city_sales_tax' => $this->city_sales_tax,
            'state_use_tax' => $this->state_use_tax,
            'county_use_tax' => $this->county_use_tax,
            'city_use_tax' => $this->city_use_tax,
            'total_sales_tax' => $this->total_sales_tax,
            'total_use_tax' => $this->total_use_tax,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'zip_code', $this->zip_code])
            ->andFilterWhere(['like', 'state_abbrevation', $this->state_abbrevation])
            ->andFilterWhere(['like', 'county_name', $this->county_name])
            ->andFilterWhere(['like', 'city_name', $this->city_name])
            ->andFilterWhere(['like', 'is_tax_shipping_alone', $this->is_tax_shipping_alone])
            ->andFilterWhere(['like', 'is_tax_ship_handling', $this->is_tax_ship_handling]);

        return $dataProvider;
    }
}
