<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AffiliateAwardRule;

/**
 * AffiliateAwardRuleSearch represents the model behind the search form about `common\models\AffiliateAwardRule`.
 */
class AffiliateAwardRuleSearch extends AffiliateAwardRule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'visitor_days_valid', 'max_orders_per_user', 'reward_amount'], 'integer'],
            [['title', 'updated_at', 'reward_type'], 'safe'],
            [['order_type_api', 'order_type_upload', 'order_type_widget', 'order_type_any'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AffiliateAwardRule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'visitor_days_valid' => $this->visitor_days_valid,
            'max_orders_per_user' => $this->max_orders_per_user,
            'order_type_api' => $this->order_type_api,
            'order_type_upload' => $this->order_type_upload,
            'order_type_widget' => $this->order_type_widget,
            'order_type_any' => $this->order_type_any,
            'updated_at' => $this->updated_at,
            'reward_amount' => $this->reward_amount,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'reward_type', $this->reward_type]);

        return $dataProvider;
    }
}
