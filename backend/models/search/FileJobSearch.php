<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FileJob;

/**
 * FileJobSearch represents the model behind the search form about `common\models\FileJob`.
 */
class FileJobSearch extends FileJob
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'file_id'], 'integer'],
            [['created_at', 'finished_at', 'operation', 'token', 'status', 'result', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FileJob::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'sort' => ['defaultOrder'=>['created_at'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'file_id' => $this->file_id,
            'created_at' => $this->created_at,
            'finished_at' => $this->finished_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'operation', $this->operation])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'result', $this->result]);

        return $dataProvider;
    }
}
