<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ApiPrintablePack;

/**
 * ApiPrintablePackSearch represents the model behind the search form about `common\models\ApiPrintablePack`.
 */
class ApiPrintablePackSearch extends ApiPrintablePack
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'model3d_id', 'api_external_system_id', 'affiliate_currency_id'], 'integer'],
            [['created_at', 'status'], 'safe'],
            [['affiliate_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApiPrintablePack::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'model3d_id' => $this->model3d_id,
            'api_external_system_id' => $this->api_external_system_id,
            'created_at' => $this->created_at,
            'affiliate_price' => $this->affiliate_price,
            'affiliate_currency_id' => $this->affiliate_currency_id,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
