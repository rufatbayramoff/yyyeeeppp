<?php

namespace backend\models\search;

use common\models\ThingiverseReport;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ThingiverseReportSearch represents the model behind the search form about `common\models\ThingiverseReport`.
 */
class ThingiverseReportSearch extends ThingiverseReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
            [['earnings', 'date', 'thing_id', 'status', 'app_transaction_code', 'printing_fee', 'shipping', 'refund', 'treatstock_transaction_fee'], 'safe'],
            [['platform_fee', 'transaction_fee'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ThingiverseReport::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => ['pageSize' => 500],
            'sort'       => ['defaultOrder' => ['date' => SORT_DESC]]

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id'        => $this->order_id,
            'platform_fee'    => $this->platform_fee,
            'transaction_fee' => $this->transaction_fee,
        ]);
        if ($this->date) {
            $query->andWhere(['>=', 'date', $this->date . ' 00-00-00']);
        }
        if ($this->app_transaction_code === 'null') {
            $query->andWhere(['app_transaction_code' => null]);
        } else {
            $query->andFilterWhere(['app_transaction_code' => $this->app_transaction_code]);
        }
        if ($this->earnings === 'null') {
            $query->andWhere(['earnings' => null]);
        } else {
            $query->andFilterWhere(['earnings' => $this->earnings]);
        }

        $query->andFilterWhere(['like', 'thing_id', $this->thing_id])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'printing_fee', $this->printing_fee])
            ->andFilterWhere(['like', 'shipping', $this->shipping])
            ->andFilterWhere(['like', 'refund', $this->refund])
            ->andFilterWhere(['like', 'treatstock_transaction_fee', $this->treatstock_transaction_fee]);
        return $dataProvider;
    }
}
