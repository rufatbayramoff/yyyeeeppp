<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StatisticsCache;

/**
 * StatisticsCacheSearch represents the model behind the search form about `common\models\StatisticsCache`.
 */
class StatisticsCacheSearch extends StatisticsCache
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'safe'],
            [['cache_hit_count', 'cache_miss_count'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StatisticsCache::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cache_hit_count' => $this->cache_hit_count,
            'cache_miss_count' => $this->cache_miss_count,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
