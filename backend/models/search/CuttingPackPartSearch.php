<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CuttingPackPart;

/**
 * CuttingPackPartSearch represents the model behind the search form about `common\models\CuttingPackPart`.
 */
class CuttingPackPartSearch extends CuttingPackPart
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'cutting_pack_file_uuid', 'image', 'is_active'], 'safe'],
            [['qty', 'width', 'height', 'cutting_length', 'engraving_length', 'material_id', 'color_id'], 'integer'],
            [['thickness'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CuttingPackPart::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'qty' => $this->qty,
            'width' => $this->width,
            'height' => $this->height,
            'cutting_length' => $this->cutting_length,
            'engraving_length' => $this->engraving_length,
            'material_id' => $this->material_id,
            'thickness' => $this->thickness,
            'color_id' => $this->color_id,
        ]);

        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'cutting_pack_file_uuid', $this->cutting_pack_file_uuid])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'is_active', $this->is_active]);

        return $dataProvider;
    }
}
