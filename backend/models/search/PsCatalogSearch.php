<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PsCatalog;

/**
 * PsCatalogSearch represents the model behind the search form about `common\models\PsCatalog`.
 */
class PsCatalogSearch extends PsCatalog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ps_id', 'rating_count'], 'integer'],
            [['rating_avg', 'rating_med', 'price_low'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PsCatalog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ps_id' => $this->ps_id,
            'rating_avg' => $this->rating_avg,
            'rating_med' => $this->rating_med,
            'rating_count' => $this->rating_count,
            'price_low' => $this->price_low,
        ]);

        return $dataProvider;
    }
}
