<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentWarning;

/**
 * PaymentWarningSearch represents the model behind the search form about `common\models\PaymentWarning`.
 */
class PaymentWarningSearch extends PaymentWarning
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment_detail_id', 'user_id'], 'integer'],
            [['created_at', 'uuid', 'date', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentWarning::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'payment_detail_id' => $this->payment_detail_id,
            'created_at' => $this->created_at,
            'user_id' => $this->user_id,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
