<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NotifyMessage;

/**
 * NotifyMessageSearch represents the model behind the search form about `common\models\NotifyMessage`.
 */
class NotifyMessageSearch extends NotifyMessage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'is_sended'], 'integer'],
            [['message_type', 'params', 'sender', 'sent_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotifyMessage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'sent_datetime' => $this->sent_datetime,
            'is_sended' => $this->is_sended,
        ]);

        $query->andFilterWhere(['like', 'message_type', $this->message_type])
            ->andFilterWhere(['like', 'params', $this->params])
            ->andFilterWhere(['like', 'sender', $this->sender]);

        return $dataProvider;
    }
}
