<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreOrderModel3dPart;

/**
 * StoreOrderModel3dPartSearch represents the model behind the search form about `common\models\StoreOrderModel3dPart`.
 */
class StoreOrderModel3dPartSearch extends StoreOrderModel3dPart
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_item_id', 'model3d_part_id'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreOrderModel3dPart::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'order_item_id' => $this->order_item_id,
            'model3d_part_id' => $this->model3d_part_id,
        ]);

        return $dataProvider;
    }
}
