<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProductExpressDelivery;

/**
 * ProductExpressDeliverySearch represents the model behind the search form about `common\models\ProductExpressDelivery`.
 */
class ProductExpressDeliverySearch extends ProductExpressDelivery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'product_uuid'], 'safe'],
            [['country_id'], 'integer'],
            [['first_item', 'following_item'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductExpressDelivery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'country_id' => $this->country_id,
            'first_item' => $this->first_item,
            'following_item' => $this->following_item,
        ]);

        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'product_uuid', $this->product_uuid]);

        return $dataProvider;
    }
}
