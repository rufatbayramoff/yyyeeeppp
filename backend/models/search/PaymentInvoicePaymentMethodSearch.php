<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentInvoicePaymentMethod;

/**
 * PaymentInvoicePaymentMethodSearch represents the model behind the search form about `common\models\PaymentInvoicePaymentMethod`.
 */
class PaymentInvoicePaymentMethodSearch extends PaymentInvoicePaymentMethod
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_invoice_uuid', 'vendor'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentInvoicePaymentMethod::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'payment_invoice_uuid', $this->payment_invoice_uuid])
            ->andFilterWhere(['like', 'vendor', $this->vendor]);

        return $dataProvider;
    }
}
