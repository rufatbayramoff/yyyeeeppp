<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Model3dImg;

/**
 * Model3dImgSearch represents the model behind the search form about `common\models\Model3dImg`.
 */
class Model3dImgSearch extends Model3dImg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'model3d_id', 'size', 'user_id', 'review_id', 'file_id'], 'integer'],
            [['title', 'created_at', 'updated_at', 'deleted_at', 'moderated_at', 'basename', 'extension', 'type'], 'safe'],
            [['is_moderated'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Model3dImg::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'model3d_id' => $this->model3d_id,
            'size' => $this->size,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'moderated_at' => $this->moderated_at,
            'is_moderated' => $this->is_moderated,
            'user_id' => $this->user_id,
            'review_id' => $this->review_id,
            'file_id' => $this->file_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'basename', $this->basename])
            ->andFilterWhere(['like', 'extension', $this->extension])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
