<?php namespace backend\models\search;

use \common\models\SystemLangSource;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SystemLangMessage;
use yii\helpers\ArrayHelper;

/**
 * SystemLangMessageSearch represents the model behind the search form about `common\models\SystemLangMessage`.
 */
class SystemLangMessageSearch extends SystemLangMessage
{

    public $sourceMessage;
    public $sourceCategory;
    public $date;
    public $dateFrom;
    public $dateTo;

    public $not_found_in_source = 0;
    public $is_checked = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [
                [
                    'language',
                    'translation',
                    'is_checked',
                    'is_auto_translate',
                    'not_found_in_source',
                    'sourceMessage',
                    'sourceCategory',
                    'date',
                    'updated_at',
                    'translated_by',
                    'dateFrom',
                    'dateTo'
                ],
                'safe'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        $query->joinWith(['source']);
        $this->load($params);

        $dataProvider                                     = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );
        $dataProvider->sort->attributes['sourceCategory'] = [
            'asc'  => [SystemLangSource::tableName() . '.category' => SORT_ASC],
            'desc' => [SystemLangSource::tableName() . '.category' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['sourceMessage']  = [
            'asc'  => [SystemLangSource::tableName() . '.message' => SORT_ASC],
            'desc' => [SystemLangSource::tableName() . '.message' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['date'] = [
            'asc'  => [SystemLangSource::tableName() . '.date' => SORT_ASC],
            'desc' => [SystemLangSource::tableName() . '.date' => SORT_DESC],
        ];

        if ($this->is_auto_translate !== null && $this->is_auto_translate !== '') {
            $query->andWhere(['is_auto_translate' => $this->is_auto_translate]);
        }

        if ($this->date) {
            $query->andFilterWhere(['like', SystemLangSource::column('date'), $this->date]);
        }

        if ($this->updated_at) {
            $query->andFilterWhere(['like', 'system_lang_message.updated_at', $this->updated_at]);
        }

        if ($this->dateFrom) {
            $query->andWhere(['>=', 'system_lang_message.updated_at', date('Y-m-d 00:00:00', strtotime($this->dateFrom))]);
        }

        if ($this->dateTo) {
            $query->andWhere(['<=', 'system_lang_message.updated_at', date('Y-m-d 23:59:59', strtotime($this->dateTo))]);
        }

        if ($this->translated_by) {
            if (is_numeric($this->translated_by)) {
                $query->andFilterWhere(['like', 'system_lang_message.translated_by', $this->translated_by]);
            } else {
                $query->joinWith(['translatedBy']);
                $query->andFilterWhere(['like', 'user.username', $this->translated_by]);
            }
        }

        if (!($this->validate())) {
            return $dataProvider;
        }

        if (ArrayHelper::getValue($params, 'SystemLangMessageSearch.translation') == '-') {
            $query->andWhere(
                [
                    SystemLangMessage::column('translation') => null
                ]
            )->orWhere([SystemLangMessage::column('translation') => ''])
                ->orWhere([SystemLangMessage::column('is_auto_translate') => 1]);
        } else {
            $query->andFilterWhere(['like', 'translation', $this->translation]);
        }
        if ($this->not_found_in_source !== null) {
            $query->andWhere([SystemLangSource::column('not_found_in_source') => $this->not_found_in_source]);
        }
        $query->andFilterWhere(
            [
                self::tableName() . '.id' => $this->id,
            ]
        );
        $query->andFilterWhere(
            [
                SystemLangSource::tableName() . '.category' => $this->sourceCategory,
            ]
        );
        $query->andFilterWhere(
            [
                'like',
                SystemLangSource::tableName() . '.message',
                $this->sourceMessage,
            ]
        );
        if (!empty($this->language) && $this->language != 'en-US') {
            $query->andWhere(['language' => $this->language]);
        }


        if ($this->is_checked !== null && $this->is_checked !== '') {
            $query->andWhere(['is_checked' => $this->is_checked]);
            if (empty($this->is_checked)) {
                $query->andWhere(['language' => 'de']);
            }
        }

        # debugSql($query); exit;
        return $dataProvider;
    }
}
