<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.11.16
 * Time: 13:26
 */

namespace backend\models\search;

use common\models\HttpRequestExtDataLog;
use common\models\HttpRequestLog;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

/**
 * Class HttpRequestLogSearch
 * @package backend\models\search
 *
 * @property string $extData
 * @property string $extObjectType
 * @property string $extObjectId
 */
class HttpRequestLogSearch extends HttpRequestLog
{

    public $extData;

    public $extObjectType;

    public $extObjectId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['params_get', 'params_post', 'params_files', 'session', 'cookie', 'answer', 'extData', 'extObjectType', 'extObjectId'], 'string'],
            [['create_date'], 'safe'],
            [['id', 'run_time_milisec', 'user_session_id', 'responce_code'], 'integer'],
            [['url'], 'string', 'max' => 1024],
            [['remote_addr'], 'string', 'max' => 50],
            [['request_type'], 'string', 'max' => 64],
        ];
    }

    public function search($params)
    {
        $query = HttpRequestLog::find()
            ->from(['log' => HttpRequestLog::tableName()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
              return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'log.id' => $this->id,
            'log.request_type' => $this->request_type,
        ]);

        $query->andFilterWhere(['like', 'log.url', $this->url]);
        $query->andFilterWhere(['like', 'log.answer', $this->answer]);
        $query->andFilterWhere(['log.remote_addr' => $this->remote_addr]);
        $query->andFilterWhere(['log.user_session_id' => $this->user_session_id]);
        $query->andFilterWhere(['log.responce_code' => $this->responce_code]);

        if ($this->create_date) {
            $query->andFilterWhere(['like', 'log.create_date', $this->create_date]);
        }

        if ($this->run_time_milisec) {
            $query->andFilterWhere(['>', 'log.run_time_milisec', $this->run_time_milisec]);
        }

        if ($this->extData || $this->extObjectType || $this->extObjectId) {
            $query->leftJoin([
                'exp' => HttpRequestExtDataLog::tableName()
            ], 'exp.request_id = log.id');

            if ($this->extData) {
                $query->andFilterWhere(['or', ['like', 'exp.log_text', $this->extData], ['like', 'exp.ext_data', $this->extData]]);
            }

            if ($this->extObjectType) {
                $query->andFilterWhere(['exp.object_type' => $this->extObjectType]);
            }

            if ($this->extObjectId) {
                $query->andFilterWhere(['exp.object_id' => $this->extObjectId]);
            }
        }
        if (!array_key_exists('sort', $params)) {
            $query->orderBy('log.create_date desc');
        }

        return $dataProvider;
    }

    public function getRequestTypeSelect()
    {
        return HttpRequestLog::find()->select(['request_type'])->groupBy(['request_type'])->indexBy('request_type')->column();
    }

    public function getExtObjectTypeSelect()
    {
        return HttpRequestExtDataLog::find()->select(['object_type'])->where(['!=', 'object_type', ''])->groupBy(['object_type'])->indexBy('object_type')->column();
    }

}