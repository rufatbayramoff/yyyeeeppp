<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreOrderReviewFile;

/**
 * StoreOrderReviewFileSearch represents the model behind the search form about `common\models\StoreOrderReviewFile`.
 */
class StoreOrderReviewFileSearch extends StoreOrderReviewFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['review_id', 'sort_index'], 'integer'],
            [['file_uuid', 'is_main', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreOrderReviewFile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'review_id' => $this->review_id,
            'created_at' => $this->created_at,
            'sort_index' => $this->sort_index,
        ]);

        $query->andFilterWhere(['like', 'file_uuid', $this->file_uuid])
            ->andFilterWhere(['like', 'is_main', $this->is_main]);

        return $dataProvider;
    }
}
