<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CuttingPackFile;

/**
 * CuttingPackFileSearch represents the model behind the search form about `common\models\CuttingPackFile`.
 */
class CuttingPackFileSearch extends CuttingPackFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'cutting_pack_uuid', 'type', 'file_uuid', 'svg_file_uuid', 'is_active', 'selections'], 'safe'],
            [['qty'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CuttingPackFile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'qty' => $this->qty,
        ]);

        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'cutting_pack_uuid', $this->cutting_pack_uuid])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'file_uuid', $this->file_uuid])
            ->andFilterWhere(['like', 'svg_file_uuid', $this->svg_file_uuid])
            ->andFilterWhere(['like', 'is_active', $this->is_active])
            ->andFilterWhere(['like', 'selections', $this->selections]);

        return $dataProvider;
    }
}
