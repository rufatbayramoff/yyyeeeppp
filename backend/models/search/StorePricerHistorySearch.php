<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StorePricerHistory;

/**
 * StorePricerHistorySearch represents the model behind the search form about `common\models\StorePricerHistory`.
 */
class StorePricerHistorySearch extends StorePricerHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pricer_id', 'user_id', 'action_id'], 'integer'],
            [['created_at', 'updated_at', 'comment'], 'safe'],
            [['old_price', 'new_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StorePricerHistory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pricer_id' => $this->pricer_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'old_price' => $this->old_price,
            'new_price' => $this->new_price,
            'user_id' => $this->user_id,
            'action_id' => $this->action_id,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
