<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CsWindow;

/**
 * CsWindowSearch represents the model behind the search form about `common\models\CsWindow`.
 */
class CsWindowSearch extends CsWindow
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_service_id', 'installation'], 'integer'],
            [['uid', 'measurement', 'status', 'is_active', 'created_at', 'updated_at', 'price_currency'], 'safe'],
            [['windowsill', 'lamination', 'slopes', 'tinting', 'energy_saver'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CsWindow::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_service_id' => $this->company_service_id,
            'windowsill' => $this->windowsill,
            'lamination' => $this->lamination,
            'slopes' => $this->slopes,
            'tinting' => $this->tinting,
            'energy_saver' => $this->energy_saver,
            'installation' => $this->installation,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'measurement', $this->measurement])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'is_active', $this->is_active])
            ->andFilterWhere(['like', 'price_currency', $this->price_currency]);

        return $dataProvider;
    }
}
