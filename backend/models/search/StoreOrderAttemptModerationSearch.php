<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreOrderAttemptModeration;

/**
 * StoreOrderAttemptModerationSearch represents the model behind the search form about `common\models\StoreOrderAttemptModeration`.
 */
class StoreOrderAttemptModerationSearch extends StoreOrderAttemptModeration
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attempt_id', 'reject_reason_id'], 'integer'],
            [['status', 'reject_comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreOrderAttemptModeration::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'attempt_id' => $this->attempt_id,
            'reject_reason_id' => $this->reject_reason_id,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'reject_comment', $this->reject_comment]);

        return $dataProvider;
    }
}
