<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CuttingWorkpieceMaterial;

/**
 * CuttingWorkpieceMaterialSearch represents the model behind the search form about `common\models\CuttingWorkpieceMaterial`.
 */
class CuttingWorkpieceMaterialSearch extends CuttingWorkpieceMaterial
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'material_id'], 'integer'],
            [['width', 'height', 'thickness', 'price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CuttingWorkpieceMaterial::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'material_id' => $this->material_id,
            'width' => $this->width,
            'height' => $this->height,
            'thickness' => $this->thickness,
            'price' => $this->price,
        ]);

        return $dataProvider;
    }
}
