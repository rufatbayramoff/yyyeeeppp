<?php

namespace backend\models\search;

use common\models\Company;
use common\models\GeoCountry;
use common\models\Ps;
use frontend\modules\mybusiness\models\CompanyProfileForm;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PsSearch represents the model behind the search form about `common\models\Ps`.
 */
class PsSearch extends Company
{
    /**
     * Flag for find only ps for moderation
     * @var int|null
     */
    public $newForModerate;

    public $user_email;

    public $location;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['currency'], 'string'],
            [['title', 'user_id', 'website', 'ownership',
                'total_employees',
                'location',
                'year_established', 'annual_turnover', 'country_id', 'description', 'logo', 'phone_code', 'phone', 'moderator_status', 'moderated_at', 'created_at', 'updated_at', 'user_email','area_of_activity'], 'safe'],
            [['is_excluded_from_printing'], 'boolean'],
            [['newForModerate'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
                'sort' => ['defaultOrder'=>['created_at'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->newForModerate){
            $query->andWhere(['moderator_status' => [Ps::PHONE_STATUS_CHECKING, Ps::MSTATUS_NEW, Ps::MSTATUS_UPDATED]]);
        }
        if(is_numeric($this->area_of_activity) && array_key_exists($this->area_of_activity,$this->businessTypeList)) {
            $businessType = $this->businessTypeList[$this->area_of_activity];
            $query->andFilterWhere(['area_of_activity' => $businessType]);
        }
        $query->andFilterWhere([
            'ps.id' => $this->id,
            'is_excluded_from_printing' => $this->is_excluded_from_printing,
            'moderated_at' => $this->moderated_at,
            'updated_at' => $this->updated_at,
        ]);

        if(!empty($params['PsSearch']) && !empty($params['PsSearch']['location'])){
            $location = $params['PsSearch']['location'];
            if (strlen($location)==2) {
                $country = GeoCountry::findOne(['iso_code'=>$location]);
                if ($country) {
                    $query->andFilterWhere(['country_id' => $country->id]);
                }
            } else {
                $query->joinWith('location');
                $query->joinWith('location.city');
                $query->andFilterWhere(['like', 'geo_city.title', $params['PsSearch']['location']]);
            }
        }
        if (!empty($this->user_id)) {
            $query->joinWith(['user']);
            if (is_numeric($this->user_id)) {
                $query->andFilterWhere(['user.id' => $this->user_id]);
            } else {
                $query->andFilterWhere(['like', 'user.username', $this->user_id]);
            }
        }
        if (!empty($this->user_email)) {
            $query->joinWith(['user']);
            $query->andFilterWhere(['like', 'user.email', trim($this->user_email)]);
        }
        if (!empty($this->created_at)) {
            $query->andFilterDate(Ps::column('created_at'), $this->created_at);
        }
        $query->andFilterWhere(['like', 'ps.title', $this->title])
            ->andFilterWhere(['like', 'ps.description', $this->description])
            ->andFilterWhere(['like', 'ps.logo', $this->logo])
            ->andFilterWhere(['like', 'ps.phone_code', $this->phone_code])
            ->andFilterWhere(['like', 'ps.phone', $this->phone])
            ->andFilterWhere(['like', 'ps.currency', $this->currency])
            ->andFilterWhere(['ps.moderator_status' => $this->moderator_status]);

        return $dataProvider;
    }

    /**
     * Return options for moderation statuse
     * @return string[]
     */
    public function getStatusesOptions()
    {
        return [
            Ps::MSTATUS_NEW => 'New',
            Ps::MSTATUS_UPDATED => 'Updated',
            Ps::MSTATUS_CHECKING => 'Checking',
            Ps::MSTATUS_CHECKED => 'Checked',
            Ps::MSTATUS_BANNED => 'Banned',
            Ps::MSTATUS_REJECTED => 'Rejected'
        ];
    }

    public function getActiveInactive()
    {
        return [1 => 'Active', 0 => 'Inactive'];
    }

    public function getBusinessTypeList(): array
    {
        return CompanyProfileForm::getBusinessTypeList();
    }
}
