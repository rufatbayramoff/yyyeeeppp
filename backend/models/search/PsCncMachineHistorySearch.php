<?php

namespace backend\models\search;

use common\models\loggers\ModelHistory;
use common\models\PsCncMachineHistory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;

/**
 * PsCncMachineHistorySearch represents the model behind the search form about `common\models\PsCncMachineHistory`.
 */
class PsCncMachineHistorySearch
{

    public $ps_machine_id;
    public $ps_cnc_machine_id;


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sql = "SELECT ps_machine_history.*, concat('m', ps_machine_history.id) AS id1 FROM ps_machine_history WHERE ps_machine_history.model_id=" . $this->ps_machine_id . " UNION SELECT ps_cnc_machine_history.*, concat('c', ps_cnc_machine_history.id) AS id1 FROM ps_cnc_machine_history WHERE ps_cnc_machine_history.model_id=" . intval($this->ps_cnc_machine_id) . " ORDER BY created_at DESC ";
        $all = Yii::$app->db->createCommand($sql)->queryAll();
        $historyRecords = [];
        foreach ($all as $one) {
            $one['id'] = $one['id1'];
            unset($one['id1']);
            $one['source'] = json_decode($one['source']);
            $one['result'] = json_decode($one['result']);

            $modelHistory = new ModelHistory($one);
            $historyRecords[] = $modelHistory;
        }
        $dataProvider = new ArrayDataProvider(
            [
                'allModels' => $historyRecords
            ]
        );
        return $dataProvider;
    }
}
