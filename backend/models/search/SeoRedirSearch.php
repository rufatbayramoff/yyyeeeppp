<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SeoRedir;

/**
 * SeoRedirSearch represents the model behind the search form about `common\models\SeoRedir`.
 */
class SeoRedirSearch extends SeoRedir
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_admin_id'], 'integer'],
            [['from_url', 'to_url', 'created_at'], 'safe'],
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SeoRedir::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'user_admin_id' => $this->user_admin_id,
        ]);

        $query->andFilterWhere(['like', 'from_url', $this->from_url])
            ->andFilterWhere(['like', 'to_url', $this->to_url]);

        return $dataProvider;
    }
}
