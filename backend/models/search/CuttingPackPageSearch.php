<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CuttingPackPage;

/**
 * CuttingPackPageSearch represents the model behind the search form about `common\models\CuttingPackPage`.
 */
class CuttingPackPageSearch extends CuttingPackPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'cutting_pack_file_uuid', 'file_uuid', 'selections'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CuttingPackPage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'cutting_pack_file_uuid', $this->cutting_pack_file_uuid])
            ->andFilterWhere(['like', 'file_uuid', $this->file_uuid])
            ->andFilterWhere(['like', 'selections', $this->selections]);

        return $dataProvider;
    }
}
