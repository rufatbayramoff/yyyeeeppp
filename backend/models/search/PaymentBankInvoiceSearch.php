<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentBankInvoice;

/**
 * PaymentBankInvoiceSearch represents the model behind the search form about `common\models\PaymentBankInvoice`.
 */
class PaymentBankInvoiceSearch extends PaymentBankInvoice
{
    public $order_id;
    public $user_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'payment_invoice_uuid', 'created_at', 'date_due', 'status', 'ship_to', 'bill_to', 'bank_details', 'payment_details', 'comment', 'order_id', 'user_id'], 'safe'],
            [['transaction_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentBankInvoice::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['created_at' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $userId = $this->user_id;

        $query->joinWith(['paymentInvoice' => function ($q) use ($userId) {
            $q->joinWith('storeOrder0');
            if ($userId) {
                if (is_int($userId)) {
                    $q->andFilterWhere(['user_id' => $userId]);
                }
                if (is_string($userId)) {
                    $q->joinWith('user');
                    $q->andFilterWhere(['like','user.username', $userId]);
                }
            }
            return $q;
        }]);


        // grid filtering conditions
        $query->andFilterWhere([
            'created_at'     => $this->created_at,
            'date_due'       => $this->date_due,
            'transaction_id' => $this->transaction_id,
        ]);

        $query->andFilterWhere(['like', 'payment_bank_invoice.uuid', $this->uuid])
            ->andFilterWhere(['like', 'payment_invoice_uuid', $this->payment_invoice_uuid])
            ->andFilterWhere(['like', 'payment_bank_invoice.status', $this->status])
            ->andFilterWhere(['like', 'ship_to', $this->ship_to])
            ->andFilterWhere(['like', 'bill_to', $this->bill_to])
            ->andFilterWhere(['like', 'bank_details', $this->bank_details])
            ->andFilterWhere(['like', 'payment_details', $this->payment_details])
            ->andFilterWhere(['like', 'store_order.id', $this->order_id])
            ->andFilterWhere(['like', 'comment', $this->comment]);
        return $dataProvider;
    }
}
