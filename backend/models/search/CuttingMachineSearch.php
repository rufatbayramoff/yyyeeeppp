<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CuttingMachine;

/**
 * CuttingMachineSearch represents the model behind the search form about `common\models\CuttingMachine`.
 */
class CuttingMachineSearch extends CuttingMachine
{
    public $serviceTitle;
    public $serviceStatus;
    public $serviceDescription;
    public $serviceIsDeleted;
    public $psUser;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'serviceIsDeleted'], 'integer'],
            [['serviceTitle', 'psUser', 'serviceStatus', 'serviceDescription'], 'string'],
            [['max_width', 'max_length', 'min_order_price'], 'number'],
        ];
    }

    protected function validateCompanyService()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CuttingMachine::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('companyService');
        $query->andFilterWhere(['like','company_service.title', $this->serviceTitle]);
        $query->andFilterWhere(['like','company_service.description', $this->serviceDescription]);
        $query->andFilterWhere([
            'company_service.moderator_status'      => $this->serviceStatus,
            'company_service.is_deleted'  => $this->serviceIsDeleted,
        ]);

        $query->andFilterWhere([
            'cutting_machine.id'              => $this->id,
            'max_width'        => $this->max_width,
            'max_length'      => $this->max_length,
            'min_order_price' => $this->min_order_price,
        ]);

        if ($this->psUser) {
            $query->joinWith('companyService.ps');
            $query->andFilterWhere(['like','ps.title', $this->psUser]);
        }

        return $dataProvider;
    }
}
