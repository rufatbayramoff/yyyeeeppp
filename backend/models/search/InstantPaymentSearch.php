<?php

namespace backend\models\search;

use common\components\exceptions\InvalidModelException;
use common\models\Company;
use common\models\User;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InstantPayment;

/**
 * InstantPaymentSearch represents the model behind the search form about `common\models\InstantPayment`.
 */
class InstantPaymentSearch extends InstantPayment
{
    const FILTER_BY_ALL              = 'filter-all';
    const FILTER_BY_ALL_PAYED        = 'filter-payed';
    const FILTER_BY_WAIT_FOR_CONFIRM = 'wait-for-confirm';
    const FILTER_BY_ACTIVE           = 'filter-active';
    const FILTER_BY_ACTIVE_PAYED     = 'filter-active-payed';
    const FILTER_BY_CANCEL           = 'filter-cancel';

    public $searchText = '';
    public $filterStatus = self::FILTER_BY_ACTIVE;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'filterStatus', 'created_at', 'descr', 'currency', 'payed_at', 'finished_at'], 'safe'],
            [['from_user_id', 'to_user_id'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    public function getFilterStatusList()
    {
        return [
            self::FILTER_BY_ALL              => 'All',
            self::FILTER_BY_ALL_PAYED        => 'Payed',
            self::FILTER_BY_WAIT_FOR_CONFIRM => 'Wait for confirm'
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InstantPayment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            throw new InvalidModelException($this);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'instant_payment.created_at'  => $this->created_at,
            'instant_payment.sum'         => $this->sum,
            'instant_payment.payed_at'    => $this->payed_at,
            'instant_payment.finished_at' => $this->finished_at,
        ]);

        if ($this->from_user_id) {
            if (is_numeric($this->from_user_id)) {
                $query->andFilterWhere(['from_user_id' => $this->from_user_id]);
            } else {
                $query->leftJoin('user as from_user', 'from_user.id = instant_payment.from_user_id');
                $query->andWhere(['or', ['like', 'from_user.email', $this->from_user_id], ['like', 'from_user.username', $this->from_user_id]]);
            }
        }
        if ($this->to_user_id) {
            if (is_numeric($this->to_user_id)) {
                $query->andFilterWhere(['to_user_id' => $this->to_user_id]);
            } else {
                $query->leftJoin('user as to_user', 'to_user.id = instant_payment.to_user_id');
                $query->leftJoin('ps as to_ps', 'to_ps.user_id = instant_payment.to_user_id');
                $query->andWhere(['or', ['like', 'to_user.email', $this->to_user_id], ['like', 'to_user.username', $this->to_user_id], ['like', 'to_ps.title', $this->to_user_id]]);
            }
        }

        if ($this->searchText) {
            $query->andFilterWhere(['or', ['instant_payment.uuid' => $this->searchText], ['like', 'descr', $this->searchText]]);
        } else {
            if ($this->filterStatus == self::FILTER_BY_ALL_PAYED) {
                $query->andFilterWhere(['<>', 'instant_payment.status', self::STATUS_NOT_PAYED]);
            } elseif ($this->filterStatus == self::FILTER_BY_WAIT_FOR_CONFIRM) {
                $query->andFilterWhere(['instant_payment.status' => [self::STATUS_WAIT_FOR_CONFIRM]]);
            } elseif ($this->filterStatus == self::FILTER_BY_ACTIVE) {
                $query->andFilterWhere(['instant_payment.status' => [self::STATUS_NOT_PAYED, self::STATUS_WAIT_FOR_CONFIRM, self::STATUS_APPROVED]]);
            } elseif ($this->filterStatus == self::FILTER_BY_ACTIVE_PAYED) {
                $query->andFilterWhere(['instant_payment.status' => [self::STATUS_WAIT_FOR_CONFIRM, self::STATUS_APPROVED]]);
            } elseif ($this->filterStatus == self::FILTER_BY_CANCEL) {
                $query->andFilterWhere(['instant_payment.status' => [self::STATUS_CANCELED, self::STATUS_DECLINED]]);
            }
        }
        $query->onlyWithInvoices();

        $query->andFilterWhere(['like', 'instant_payment.uuid', $this->uuid])
            ->andFilterWhere(['like', 'instant_payment.descr', $this->descr])
            ->andFilterWhere(['like', 'instant_payment.currency', $this->currency]);
        return $dataProvider;
    }

    public function filterSearchForm($searchForm)
    {
        if ($searchForm) {
            $this->searchText   = array_key_exists('searchText', $searchForm) ? $searchForm['searchText'] : '';
            $this->filterStatus = array_key_exists('filterBy', $searchForm) ? $searchForm['filterBy'] : '';
        }
    }

    public function searchMySales(Company $company, $searchForm)
    {
        $this->to_user_id = $company->user_id;
        $this->filterSearchForm($searchForm);
        if ($this->filterStatus == self::FILTER_BY_ACTIVE) {
            $this->filterStatus = self::FILTER_BY_ACTIVE_PAYED;
        } elseif ($this->filterStatus == self::FILTER_BY_ALL) {
            $this->filterStatus = self::FILTER_BY_ALL_PAYED;
        }
        return $this->search([]);
    }

    public function searchMyPayments(User $client, $searchForm)
    {
        $this->from_user_id = $client->id;
        $this->filterSearchForm($searchForm);
        return $this->search([]);
    }
}
