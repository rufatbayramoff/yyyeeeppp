<?php

namespace backend\models\search;

use common\interfaces\Model3dBasePartInterface;
use common\modules\product\interfaces\ProductInterface;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreUnit;
use common\models\Model3d;

/**
 * StoreUnitSearch represents the model behind the search form about `common\models\StoreUnit`.
 */
class StoreUnitSearch extends StoreUnit
{
    public $user_id;

    /**
     * model3d titles
     *
     * @var type
     */
    public $title;

    public $published_at;

    public $moderated_at;

    public $updated_at;

    public $toModerate;

    public $source;

    public $product_status;

    public $onlyDeleted = false;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        $safeAttributes = [
            'title',
            'user_id',
            'published_at',
            'moderated_at',
            'updated_at',
            'user_id',
            'toModerate',
            'source',
            'product_status'
        ];

        if (!$this->onlyDeleted) {
            $safeAttributes[] = 'status';
        }

        return [
            [['id', 'model3d_id'], 'integer'],
            [$safeAttributes, 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreUnit::find()->joinWith([
                'model3d',
                'model3d.productCommon.user'
            ]
        )->leftJoin('model3d_part', 'model3d_part.model3d_id = model3d.id');
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort'  => ['defaultOrder' => ['updated_at'=>SORT_DESC]]
            ]
        );

        $dataProvider->sort->attributes['id'] = [
            'asc'  => [Model3d::column('id') => SORT_ASC],
            'desc' => [Model3d::column('id') => SORT_DESC],
        ];
        $dataProvider->sort->attributes['updated_at'] = [
            'asc'  => ['product_common.updated_at' => SORT_ASC],
            'desc' => ['product_common.updated_at' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['published_at'] = [
            'asc'  => [Model3d::column('published_at') => SORT_ASC],
            'desc' => [Model3d::column('published_at') => SORT_DESC],
        ];
        $dataProvider->sort->attributes['source'] = [
            'asc'  => [Model3d::column('source') => SORT_ASC],
            'desc' => [Model3d::column('source') => SORT_DESC],
        ];

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->joinWith('model3d.productCommon');

        if (!empty($this->published_at)) {
            $this->published_at = date("Y-m-d", strtotime($this->published_at));
            $query->andWhere(
                "DATE(" . Model3d::column("published_at") . ")= :date_to",
                [':date_to' => $this->published_at]
            );
        }
        if (!empty($this->moderated_at)) {
            $this->moderated_at = date("Y-m-d", strtotime($this->moderated_at));
            $query->andWhere(
                "DATE(" . Model3d::column("moderated_at") . ")= :date_to",
                [':date_to' => $this->moderated_at]
            );
        }

        $query->andFilterWhere(['product_common.product_status' => $this->product_status]);

        $query->andFilterWhere(['like', 'product_common.title', $this->title]);
        if ($this->source) {
            $query->andFilterWhere(['model3d.source' => $this->source]);
        }
        $query->andFilterWhere(['like', 'user.username', $this->user_id]);

        if ($this->product_status == ProductInterface::STATUS_PUBLISH_PENDING) {
            $query->andFilterWhere(['product_common.is_active'=>1]);
        }

        if (!$this->product_status && $this->toModerate) {
            $query->andFilterWhere(['product_common.is_active'=>1]);
            $query->andFilterWhere(
                [
                    'product_common.product_status' => [
                        ProductInterface::STATUS_PUBLISHED_UPDATED,
                        ProductInterface::STATUS_PUBLISH_PENDING
                    ],
                ]
            );
        }
        $query->andWhere('model3d_part.id is not null');
        $query->andFilterWhere(
            [
                'store_unit.id' => $this->id,
                'model3d.id'    => $this->model3d_id,
            ]
        );
        $query->groupBy('model3d.id');
        return $dataProvider;
    }
}
