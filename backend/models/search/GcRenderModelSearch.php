<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GcRenderModel;

/**
 * GcRenderModelSearch represents the model behind the search form about `common\models\GcRenderModel`.
 */
class GcRenderModelSearch extends GcRenderModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['webdavUrl', 'path', 'loaded_at', 'processed_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GcRenderModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'loaded_at' => $this->loaded_at,
            'processed_at' => $this->processed_at,
        ]);

        $query->andFilterWhere(['like', 'webdavUrl', $this->webdavUrl])
            ->andFilterWhere(['like', 'path', $this->path]);

        return $dataProvider;
    }
}
