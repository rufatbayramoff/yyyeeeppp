<?php

namespace backend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreOrderReview;

/**
 * StoreOrderReviewSearch represents the model behind the search form about `common\models\StoreOrderReview`.
 */
class StoreOrderReviewSearch extends StoreOrderReview
{

    public $orderUser;

    public $psUser;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'store_unit_id', 'ps_id', 'rating_speed', 'rating_quality', 'rating_communication'], 'integer'],
            [['comment', 'file_ids', 'status', 'created_at', 'orderUser', 'psUser', 'verified_answer_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreOrderReview::find()->orderBy('store_order_review.id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'store_unit_id' => $this->store_unit_id,
            'ps_id' => $this->ps_id,
            'rating_speed' => $this->rating_speed,
            'rating_quality' => $this->rating_quality,
            'rating_communication' => $this->rating_communication,
            'created_at' => $this->created_at,
            'verified_answer_status' => $this->verified_answer_status
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'status', $this->status]);


        if ($this->orderUser) {
            if(is_numeric($this->orderUser)){
                $query->joinWith('order order');
                $query->andFilterWhere(['order.user_id'=>$this->orderUser]);
            }else{
                $query->joinWith('order.user order_user');
                $query->andFilterWhere(['like', 'order_user.username', $this->orderUser]);
            }
        }

        if ($this->psUser) {
            if(is_numeric($this->psUser)){
                $query->joinWith('ps');
                $query->andFilterWhere(['ps.user_id'=>$this->psUser]);
            }else{
                $query->joinWith('ps.user ps_user');
                $query->andFilterWhere(['like', 'ps_user.username', $this->psUser]);
            }
        }

        return $dataProvider;
    }
}
