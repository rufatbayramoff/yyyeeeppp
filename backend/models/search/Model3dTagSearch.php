<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Model3dTag;

/**
 * Model3dTagSearch represents the model behind the search form about `common\models\Model3dTag`.
 */
class Model3dTagSearch extends Model3dTag
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'model3d_id', 'tag_id'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Model3dTag::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'model3d_id' => $this->model3d_id,
            'tag_id' => $this->tag_id,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
