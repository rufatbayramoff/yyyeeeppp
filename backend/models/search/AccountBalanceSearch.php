<?php

namespace backend\models\search;

use yii\base\Model;

/**
 *
 */
class AccountBalanceSearch extends Model
{
    /** @var string */
    public $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user'], 'string'],
        ];
    }
}
