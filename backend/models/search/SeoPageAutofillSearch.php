<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SeoPageAutofill;

/**
 * SeoPageAutofillSearch represents the model behind the search form about `common\models\SeoPageAutofill`.
 */
class SeoPageAutofillSearch extends SeoPageAutofill
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'object_id', 'seo_page_id', 'template_id'], 'integer'],
            [['object_type', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SeoPageAutofill::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'object_id' => $this->object_id,
            'created_at' => $this->created_at,
            'seo_page_id' => $this->seo_page_id,
            'template_id' => $this->template_id,
        ]);

        $query->andFilterWhere(['like', 'object_type', $this->object_type]);

        return $dataProvider;
    }
}
