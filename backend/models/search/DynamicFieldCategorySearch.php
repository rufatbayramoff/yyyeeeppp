<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DynamicFieldCategory;

/**
 * DynamicFieldCategorySearch represents the model behind the search form about `common\models\DynamicFieldCategory`.
 */
class DynamicFieldCategorySearch extends DynamicFieldCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'is_required', 'is_active', 'has_filter', 'position'], 'integer'],
            [['field_code', 'default_value', 'type_params'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DynamicFieldCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'is_required' => $this->is_required,
            'is_active' => $this->is_active,
            'has_filter' => $this->has_filter,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'field_code', $this->field_code])
            ->andFilterWhere(['like', 'default_value', $this->default_value])
            ->andFilterWhere(['like', 'type_params', $this->type_params]);

        return $dataProvider;
    }
}
