<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserCollectionModel3d;

/**
 * UserCollectionModel3dSearch represents the model behind the search form about `common\models\UserCollectionModel3d`.
 */
class UserCollectionModel3dSearch extends UserCollectionModel3d
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'collection_id', 'model3d_id'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserCollectionModel3d::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'collection_id' => $this->collection_id,
            'model3d_id' => $this->model3d_id,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
