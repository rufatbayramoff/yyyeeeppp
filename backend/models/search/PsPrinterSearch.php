<?php

namespace backend\models\search;

use common\components\order\TestOrderFactory;
use common\components\ps\material\PsColorPriceExceedSpecification;
use common\models\CompanyService;
use common\models\Printer;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrder;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PsPrinter;

/**
 * PsPrinterSearch represents the model behind the search form about `common\models\PsPrinter`.
 */
class PsPrinterSearch extends PsPrinter
{
    /**
     * For user id
     * @var int
     */
    public $userId;

    /**
     * Only printers witch exceed min or max recomend price for color
     * @var int
     */
    public $priceExceed;

    public $country;

    public $printer_title;

    public $ps_id;

    public $visibility;

    public $moderator_status;

    public $is_deleted;

    public $created_at;

    public $updated_at;

    public $testOrderStatus;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'printer_id', 'is_online', 'is_deleted', 'priceExceed'], 'integer'],
            [['title', 'description', 'image', 'moderator_status', 'created_at', 'updated_at', 'userId', 'country', 'visibility'], 'safe'],
            [['price_per_hour', 'price_per_volume', 'min_order_price'], 'number'],
            [['ps_id', 'printer_title', 'testOrderStatus'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PsPrinter::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes'   => [
                'id',
                'ps_id',
                'is_deleted',
                'country',
                'user_id',
                'moderator_status',
                'updated_at' => [
                    'asc'  => ['company_service.updated_at' => SORT_ASC],
                    'desc' => ['company_service.updated_at' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['updated_at' => SORT_DESC],
        ]);
        $this->load($params);

        $query->joinWith('companyService');

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ps_printer.id'               => $this->id,
            'ps_printer.printer_id'       => $this->printer_id,
            'ps_printer.price_per_hour'   => $this->price_per_hour,
            'ps_printer.price_per_volume' => $this->price_per_volume,
            'ps_printer.is_online'        => $this->is_online,
            'company_service.visibility'  => $this->visibility,
        ]);
        switch ($this->moderator_status) {
            case 'active':
                $query->andFilterWhere(['company_service.moderator_status' => CompanyService::moderatedStatusList()]);
                break;

            case 'waiting':
                $query->andFilterWhere(['company_service.moderator_status' => CompanyService::pendingModerationStatusList()])->andWhere(['company_service.is_deleted' => 0]);
                break;
            case 'inactive':
                $query->andFilterWhere(
                    [
                        'company_service.moderator_status' => [
                            CompanyService::MODERATOR_STATUS_NEW, CompanyService::MODERATOR_STATUS_REJECTED, CompanyService::MODERATOR_STATUS_REJECTED_REVIEWING
                        ]
                    ]
                )->orWhere(['company_service.visibility' => CompanyService::VISIBILITY_NOWHERE])->andWhere(['company_service.is_deleted' => 0]);
                break;

            default:
                $query->andFilterWhere(['company_service.moderator_status' => $this->moderator_status]);
                break;
        }
        if ($this->printer_title) {
            $query->joinWith('printer');
            $query->andFilterWhere(['like', Printer::column('title'), $this->printer_title]);
        }
        $query->andFilterWhere(['like', 'ps_printer.title', $this->title])
            ->andFilterWhere(['like', 'ps_printer.description', $this->description])
            ->andFilterWhere(['like', 'ps_printer.image', $this->image]);


        $query->joinWith('psMachine.ps');
        if (is_numeric($this->ps_id)) {
            $query->andFilterWhere(['ps_id' => $this->ps_id]);
        } else {
            $query->andFilterWhere(['like', 'ps.title', $this->ps_id]);
        }
        if (!empty($this->country)) {
            $query->joinWith('psMachine.location.country');
            $query->andFilterWhere(['geo_country.iso_code' => $this->country]);
        }

        if ($this->userId) {
            if (is_numeric($this->userId)) {

                $query->andFilterWhere(['ps.user_id' => $this->userId]);
            } else {
                $query->leftJoin('user', 'ps.user_id=user.id');
                $query->andFilterWhere(['like', 'user.username', $this->userId]);
            }
        }

        if ($this->priceExceed) {
            /** @var PsColorPriceExceedSpecification $resolver */
            $resolver = \Yii::createObject(PsColorPriceExceedSpecification::class);
            $resolver->satisfiedQuery($query);
        }


        if (!is_null($this->is_deleted)) {
            $query->andFilterWhere(['company_service.is_deleted' => $this->is_deleted]);
        }

        if ($this->testOrderStatus) {
            switch ($this->testOrderStatus) {
                case 'no':
                    $query
                        ->andWhere([
                            PsPrinter::column('is_test_order_resolved') => 0,
                            StoreOrder::column('id')                    => null,
                        ])
                        ->joinWith('psMachine.storeOrderAttemps', false)
                        ->join('LEFT JOIN', StoreOrder::tableName(), '`store_order_attemp`.`order_id` = `store_order`.`id` AND `store_order`.`user_id` = ' . TestOrderFactory::getTestOrderUserId());
                    $query->groupBy('company_service.ps_printer_id');
                    $query->orderBy(null);
                    $dataProvider->setSort(false);
                    break;


                case 'resolved':
                    $query->andWhere([PsPrinter::column('is_test_order_resolved') => 1]);
                    break;


                case 'inprogress':
                    $query
                        ->andWhere([
                            PsPrinter::column('is_test_order_resolved') => 0
                        ])
                        ->joinWith(['psMachine.storeOrderAttemps.order' => function (StoreOrderQuery $query) {
                            $query->onlyTest();
                        }], false);

                    break;
            }
        }
        return $dataProvider;
    }


    public function getIs_deleted()
    {
        return $this->is_deleted;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getUpdated_at()
    {
        return $this->updated_at;
    }


    public function getOnlineOffline()
    {
        return [1 => 'Online', 0 => 'Offline'];
    }


    public static function getTestOrderFilter(): array
    {
        return [
            'no'         => 'No',
            'resolved'   => 'Resolved',
            'inprogress' => 'In process'
        ];
    }

}
