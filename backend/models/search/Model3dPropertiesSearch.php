<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Model3dProperties;

/**
 * Model3dPropertiesSearch represents the model behind the search form about `common\models\Model3dProperties`.
 */
class Model3dPropertiesSearch extends Model3dProperties
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'model3d_part_id'], 'integer'],
            [['length', 'width', 'height'], 'number'],
            [['metric', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Model3dProperties::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'model3d_part_id' => $this->model3d_part_id,
            'length' => $this->length,
            'width' => $this->width,
            'height' => $this->height,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'metric', $this->metric]);

        return $dataProvider;
    }
}
