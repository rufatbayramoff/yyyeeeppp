<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\WikiMachinePhoto;

/**
 * WikiMachinePhotoSearch represents the model behind the search form about `common\models\WikiMachinePhoto`.
 */
class WikiMachinePhotoSearch extends WikiMachinePhoto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wiki_machine_id'], 'integer'],
            [['photo_file_uuid'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WikiMachinePhoto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'wiki_machine_id' => $this->wiki_machine_id,
        ]);

        $query->andFilterWhere(['like', 'photo_file_uuid', $this->photo_file_uuid]);

        return $dataProvider;
    }
}
