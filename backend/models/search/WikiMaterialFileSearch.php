<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\WikiMaterialFile;

/**
 * WikiMaterialFileSearch represents the model behind the search form about `common\models\WikiMaterialFile`.
 */
class WikiMaterialFileSearch extends WikiMaterialFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'wiki_material_id'], 'integer'],
            [['file_uuid'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WikiMaterialFile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'wiki_material_id' => $this->wiki_material_id,
        ]);

        $query->andFilterWhere(['like', 'file_uuid', $this->file_uuid]);

        return $dataProvider;
    }
}
