<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Model3dPart;

/**
 * Model3dPartSearch represents the model behind the search form about `common\models\Model3dPart`.
 */
class Model3DPartSearch extends Model3dPart
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'model3d_id', 'file_id', 'file_src_id', 'rotated_x', 'rotated_y', 'rotated_z'], 'integer'],
            [['format', 'name', 'title', 'antivirus_checked_at', 'moderator_status', 'moderated_at', 'user_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Model3dPart::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'model3d_id' => $this->model3d_id,
            'antivirus_checked_at' => $this->antivirus_checked_at,
            'moderated_at' => $this->moderated_at,
            'file_id' => $this->file_id,
            'file_src_id' => $this->file_src_id,
            'rotated_x' => $this->rotated_x,
            'rotated_y' => $this->rotated_y,
            'rotated_z' => $this->rotated_z,
        ]);

        $query->andFilterWhere(['like', 'format', $this->format])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'moderator_status', $this->moderator_status])
            ->andFilterWhere(['like', 'user_status', $this->user_status]);

        return $dataProvider;
    }
}
