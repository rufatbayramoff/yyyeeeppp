<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ThingiverseThingFile;

/**
 * ThingiverseThingFileSearch represents the model behind the search form about `common\models\ThingiverseThingFile`.
 */
class ThingiverseThingFileSearch extends ThingiverseThingFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['thingfile_id', 'thing_id', 'size', 'file_id'], 'integer'],
            [['name', 'url', 'download_url', 'thumb', 'render', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ThingiverseThingFile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'thingfile_id' => $this->thingfile_id,
            'thing_id' => $this->thing_id,
            'size' => $this->size,
            'created_at' => $this->created_at,
            'file_id' => $this->file_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'download_url', $this->download_url])
            ->andFilterWhere(['like', 'thumb', $this->thumb])
            ->andFilterWhere(['like', 'render', $this->render]);

        return $dataProvider;
    }
}
