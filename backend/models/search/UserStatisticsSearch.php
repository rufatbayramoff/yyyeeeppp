<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserStatistics;

/**
 * UserStatisticsSearch represents the model behind the search form about `common\models\UserStatistics`.
 */
class UserStatisticsSearch extends UserStatistics
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'downloads', 'prints', 'favorites', 'views'], 'integer'],
            [['updated_at', 'lastonline_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserStatistics::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'downloads' => $this->downloads,
            'prints' => $this->prints,
            'favorites' => $this->favorites,
            'views' => $this->views,
            'updated_at' => $this->updated_at,
            'lastonline_at' => $this->lastonline_at,
        ]);

        return $dataProvider;
    }
}
