<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Printer;

/**
 * PrinterSearch represents the model behind the search form about `common\models\Printer`.
 */
class PrinterSearch extends Printer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'technology_id', 'weight'], 'integer'],
            [['firm', 'title', 'description', 'created_at', 'updated_at', 'comments', 'website', 'build_volume'], 'safe'],
            [['is_active'], 'boolean'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Printer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_active' => $this->is_active,
            'technology_id' => $this->technology_id,
            'weight' => $this->weight,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'firm', $this->firm])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'build_volume', $this->build_volume]);

        return $dataProvider;
    }

    public function getActiveInactive()
    {
        return [1 => 'Active', 0 => 'Inactive'];
    }
}
