<?php

namespace backend\models\search;

use common\components\ActiveQuery;
use common\models\query\PaymentInvoiceQuery;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AffiliateAward;

/**
 * AffiliateAwardSearch represents the model behind the search form about `common\models\AffiliateAward`.
 */
class AffiliateAwardSearch extends AffiliateAward
{
    public $affiliateSourceText;
    public $status;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'accrued_payment_detail_id'], 'integer'],
            [['affiliate_source_uuid', 'currency', 'created_at', 'invoice_uuid', 'affiliateSourceText', 'status'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AffiliateAward::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'created_at' => $this->created_at,
            'accrued_payment_detail_id' => $this->accrued_payment_detail_id,
        ]);

        $query->andFilterWhere(['like', 'affiliate_source_uuid', $this->affiliate_source_uuid])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'invoice_uuid', $this->invoice_uuid]);

        if ($this->affiliateSourceText) {
            $query->joinWith('affiliateSource.company');
            $query->andFilterWhere(['like', 'ps.title', $this->affiliateSourceText]);
        }

        if ($this->status === self::STATUS_PENDING) {
            $affiliateAwardSearch = $this;
            $query->joinWith(['invoice' => function(PaymentInvoiceQuery $query) use ($affiliateAwardSearch){
                $query->isPayedStatus();
            }], false);
            $query->joinWith('accruedPaymentDetail');
            $query->andWhere('payment_detail.id is null');
        } elseif ($this->status === self::STATUS_DONE) {
            $query->joinWith('accruedPaymentDetail');
            $query->andWhere('payment_detail.id is not null');
        } elseif ($this->status === self::STATUS_NEW) {
            $affiliateAwardSearch = $this;
            $query->joinWith(['invoice' => function(PaymentInvoiceQuery $query) use ($affiliateAwardSearch){
                $query->isNotPayedStatus();
            }], false);
            $query->joinWith('accruedPaymentDetail');
            $query->andWhere('payment_detail.id is null');
        }

        return $dataProvider;
    }
}
