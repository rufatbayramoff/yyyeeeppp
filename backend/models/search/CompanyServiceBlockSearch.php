<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CompanyServiceBlock;

/**
 * CompanyServiceBlockSearch represents the model behind the search form about `common\models\CompanyServiceBlock`.
 */
class CompanyServiceBlockSearch extends CompanyServiceBlock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_service_id'], 'integer'],
            [['title', 'content', 'created_at', 'is_visible', 'position'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyServiceBlock::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'company_service_id' => $this->company_service_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'is_visible', $this->is_visible])
            ->andFilterWhere(['like', 'position', $this->position]);

        return $dataProvider;
    }
}
