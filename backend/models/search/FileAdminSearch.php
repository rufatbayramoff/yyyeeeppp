<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FileAdmin;

/**
 * FileAdminSearch represents the model behind the search form about `common\models\FileAdmin`.
 */
class FileAdminSearch extends FileAdmin
{
    /**
     * @inheritdoc
     */
    public function rules():array
    {
        return [
            [['uuid', 'name', 'path', 'extension', 'created_at', 'updated_at', 'deleted_at', 'server', 'status', 'md5sum', 'last_access_at', 'ownerClass', 'ownerField', 'path_version', 'expire'], 'safe'],
            [['size', 'user_id', 'is_public'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FileAdmin::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'size' => $this->size,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'user_id' => $this->user_id,
            'last_access_at' => $this->last_access_at,
            'is_public' => $this->is_public,
            'expire' => $this->expire,
        ]);

        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'path', $this->path])
            ->andFilterWhere(['like', 'extension', $this->extension])
            ->andFilterWhere(['like', 'server', $this->server])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'md5sum', $this->md5sum])
            ->andFilterWhere(['like', 'ownerClass', $this->ownerClass])
            ->andFilterWhere(['like', 'ownerField', $this->ownerField])
            ->andFilterWhere(['like', 'path_version', $this->path_version]);

        return $dataProvider;
    }
}
