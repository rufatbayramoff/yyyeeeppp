<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentInvoice;

/**
 * PaymentInvoiceSearch represents the model behind the search form about `common\models\PaymentInvoice`.
 */
class PaymentInvoiceSearch extends PaymentInvoice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'created_at', 'date_expire', 'currency', 'status', 'details', 'accounting'], 'safe'],
            [['total_amount'], 'number'],
            [['rate_id', 'user_id', 'user_session_id', 'store_order_id', 'order_position_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentInvoice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'date_expire' => $this->date_expire,
            'total_amount' => $this->total_amount,
            'rate_id' => $this->rate_id,
            'user_id' => $this->user_id,
            'user_session_id' => $this->user_session_id,
            'store_order_id' => $this->store_order_id,
            'order_position_id' => $this->order_position_id,
        ]);

        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'details', $this->details])
            ->andFilterWhere(['like', 'accounting', $this->accounting]);

        return $dataProvider;
    }
}
