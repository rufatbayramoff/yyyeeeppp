<?php

namespace backend\models\search;

use common\models\PaymentInvoice;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PromocodeUsage;

/**
 * PromocodeUsageSearch represents the model behind the search form about `common\models\PromocodeUsage`.
 */
class PromocodeUsageSearch extends PromocodeUsage
{

    public $filterDate;
    public $startDate;
    public $endDate;

    public $order_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'promocode_id', 'order_id'], 'integer'],
            [['used_at', 'amount_currency', 'filterDate'], 'safe'],
            ['invoice_uuid', 'string'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $pageSize = 20)
    {
        $query = PromocodeUsage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                 'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);
        if(!empty($this->filterDate)){
            list($this->startDate, $this->endDate) = explode(' - ', $this->filterDate);

            $query->andWhere(
                'used_at >= :date_from AND used_at <= :date_to', [':date_from' => $this->startDate .' 00:00:01', ':date_to' => $this->endDate . ' 23:59:59' ]
            );
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'promocode_id' => $this->promocode_id,
            'invoice_uuid' => $this->invoice_uuid,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'amount_currency', $this->amount_currency]);

        if ($this->order_id) {
            $query
                ->innerJoin(['pi' => PaymentInvoice::tableName()], PromocodeUsage::column('invoice_uuid') . ' = pi.uuid')
                ->andWhere([
                    'pi.store_order_id' => $this->order_id
                ]);
        }

        return $dataProvider;
    }
}
