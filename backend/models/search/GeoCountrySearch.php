<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GeoCountry;

/**
 * GeoCountrySearch represents the model behind the search form about `common\models\GeoCountry`.
 */
class GeoCountrySearch extends GeoCountry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'capital_id'], 'integer'],
            [['title', 'iso_code', 'phone_code', 'iso_alpha3', 'currency_code', 'currency_name', 'currency_symbol'], 'safe'],
            [['is_easypost_intl', 'is_easypost_domestic', 'region_required', 'paypal_payout','is_europa'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeoCountry::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_easypost_intl' => $this->is_easypost_intl,
            'is_easypost_domestic' => $this->is_easypost_domestic,
            'is_europa' => $this->is_europa,
            'capital_id' => $this->capital_id,
            'region_required' => $this->region_required,
            'paypal_payout' => $this->paypal_payout,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'iso_code', $this->iso_code])
            ->andFilterWhere(['like', 'phone_code', $this->phone_code])
            ->andFilterWhere(['like', 'iso_alpha3', $this->iso_alpha3])
            ->andFilterWhere(['like', 'currency_code', $this->currency_code])
            ->andFilterWhere(['like', 'currency_name', $this->currency_name])
            ->andFilterWhere(['like', 'currency_symbol', $this->currency_symbol]);

        return $dataProvider;
    }
}
