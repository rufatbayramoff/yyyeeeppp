<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaxCountry;

/**
 * TaxCountrySearch represents the model behind the search form about `common\models\TaxCountry`.
 */
class TaxCountrySearch extends TaxCountry
{
    
    public $title;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rate_ps', 'rate_model', 'is_default'], 'integer'],
            [['country', 'updated_at', 'title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaxCountry::find();
        $query->joinWith('country0');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            \common\models\GeoCountry::column('title') => $this->title
        ]);
        $query->andFilterWhere([
            'id' => $this->id,
            'rate_ps' => $this->rate_ps,
            'rate_model' => $this->rate_model,
            'updated_at' => $this->updated_at,
            'is_default' => $this->is_default
        ]);

        $query->andFilterWhere(['like', 'country', $this->country]);

        return $dataProvider;
    }
}
