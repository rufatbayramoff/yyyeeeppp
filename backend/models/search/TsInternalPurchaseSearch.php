<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TsInternalPurchase;

/**
 * TsInternalPurchaseSearch represents the model behind the search form about `common\models\TsInternalPurchase`.
 */
class TsInternalPurchaseSearch extends TsInternalPurchase
{
    /** @var string */
    public $companyServiceTitle;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'created_at', 'primary_invoice_uuid', 'type'], 'safe'],
            [['user_id'], 'integer'],
            [['companyServiceTitle'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TsInternalPurchase::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if ($this->companyServiceTitle) {
            $query->joinWith('tsInternalPurchaseCertification.companyService.psPrinter.ps');
            $query->andFilterWhere(['or',
                [
                    'like',
                    'ps.title',
                    $this->companyServiceTitle
                ],
                [
                    'like',
                    'ps_printer.title',
                    $this->companyServiceTitle
                ]
            ]);
        }

        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'user_id'    => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'primary_invoice_uuid', $this->primary_invoice_uuid])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
