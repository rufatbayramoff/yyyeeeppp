<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentPayPageLogProcess;

/**
 * PaymentPayPageLogProcessSearch represents the model behind the search form about `common\models\PaymentPayPageLogProcess`.
 */
class PaymentPayPageLogProcessSearch extends PaymentPayPageLogProcess
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'process_err', 'process_payload', 'process_message'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentPayPageLogProcess::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'process_err', $this->process_err])
            ->andFilterWhere(['like', 'process_payload', $this->process_payload])
            ->andFilterWhere(['like', 'process_message', $this->process_message]);

        return $dataProvider;
    }
}
