<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreOrderDelivery;

/**
 * StoreOrderDeliverySearch represents the model behind the search form about `common\models\StoreOrderDelivery`.
 */
class StoreOrderDeliverySearch extends StoreOrderDelivery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'file_id', 'delivery_type_id'], 'integer'],
            [['tracking_number', 'tracking_shipper', 'created_at', 'send_message_at', 'ps_delivery_details'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreOrderDelivery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'file_id' => $this->file_id,
            'delivery_type_id' => $this->delivery_type_id,
            'created_at' => $this->created_at,
            'send_message_at' => $this->send_message_at,
        ]);

        $query->andFilterWhere(['like', 'tracking_number', $this->tracking_number])
            ->andFilterWhere(['like', 'tracking_shipper', $this->tracking_shipper])
            ->andFilterWhere(['like', 'ps_delivery_details', $this->ps_delivery_details]);

        return $dataProvider;
    }
}
