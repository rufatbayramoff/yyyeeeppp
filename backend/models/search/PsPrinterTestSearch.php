<?php

namespace backend\models\search;

use common\models\query\PsPrinterQuery;
use common\models\query\PsQuery;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PsPrinterTest;

/**
 * PsPrinterTestSearch represents the model behind the search form about `common\models\PsPrinterTest`.
 */
class PsPrinterTestSearch extends PsPrinterTest
{
    /**
     * For search by printer name
     * @var string
     */
    public $printer;

    /**
     * For search by user name
     * @var string
     */
    public $user;

    /**
     * For search by ps name
     * @var string
     */
    public $ps;

    /** @var int */
    public $certClassId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ps_printer_id', 'certClassId'], 'integer'],
            [['type', 'status', 'image_file_ids', 'comment', 'created_at'], 'safe'],
            [['printer', 'user', 'ps', 'decision_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PsPrinterTest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ps_printer_id' => $this->ps_printer_id,
            'created_at'    => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'image_file_ids', $this->image_file_ids])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        if ($this->ps) {
            $query->joinWith(['psPrinter.ps' => function (PsQuery $query) {
                $query->filterWhere(['like', 'ps.title', $this->ps]);
            }], false);
        }

        if ($this->certClassId) {
            $query->joinWith('tsInternalPurchase.tsInternalPurchaseCertification');
            $query->andWhere(['ts_internal_purchase_certification.ts_certification_class_id' => $this->certClassId]);
        }

        if ($this->printer) {
            if (is_numeric($this->printer)) {
                $query->andWhere(['ps_printer_id' => $this->printer]);
            } else {
                $query->joinWith(['psPrinter psPrinter' => function (PsPrinterQuery $query) {
                    $query->filterWhere(['like', 'psPrinter.title', $this->printer]);
                }], false);
            }
        }

        if ($this->user) {
            $query->joinWith(['psPrinter.ps.user' => function (\common\models\query\UserQuery $query) {
                if (is_numeric($this->user)) {
                    $query->andWhere(['user.id' => (int)$this->user]);
                } else {
                    $query->likeUsername($this->user);
                }
            }], false);
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_NEW     => 'New',
            self::STATUS_CHECKED => 'Checked',
            self::STATUS_FAILED  => 'Rejected',
            self::STATUS_REPEAT  => 'Repeat',
        ];
    }

    /**
     * @return array
     */
    public function getTypesList()
    {
        return [
            self::TYPE_COMMON       => 'Common',
            self::TYPE_PROFESSIONAL => 'Professional',
        ];
    }
}
