<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CompanyService;

/**
 * PsMachineSearch represents the model behind the search form about `common\models\PsMachine`.
 */
class CompanyServiceSearch extends CompanyService
{
    /**
     * For user id
     *
     * @var int
     */
    public $userId;

    public $country;

    public $title;

    public $notModerated;

    /** @var bool without printers types */
    public $withoutPrinters=false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'ps_printer_id', 'ps_cnc_machine_id', 'location_id', 'is_deleted', 'is_location_change'], 'integer'],
            [['title', 'ps_id', 'country', 'type', 'visibility', 'moderator_status', 'description', 'created_at', 'updated_at', 'userId', 'notModerated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyService::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,     'sort' => ['defaultOrder'=>['id'=>SORT_DESC]]
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->leftJoin('ps', 'company_service.ps_id = ps.id');
        $query->leftJoin('ps_printer', 'company_service.ps_printer_id = ps_printer.id');
        $query->leftJoin('ps_cnc_machine', 'company_service.ps_cnc_machine_id = ps_cnc_machine.id');

        if (is_numeric($this->ps_id)) {
            $query->andFilterWhere(['ps_id' => $this->ps_id]);
        } else {
            $query->andFilterWhere(['like', 'ps.title', $this->ps_id]);
        }

        if ($this->userId) {
            if (is_numeric($this->userId)) {

                $query->andFilterWhere(['ps.user_id' => $this->userId]);
            } else {
                $query->leftJoin('user', 'ps.user_id = user.id');
                $query->andFilterWhere(['like', 'user.username', $this->userId]);
            }
        }

        if ($this->country) {
            $query->joinWith('location.country');
            $query->andFilterWhere(['geo_country.iso_code' => $this->country]);
        }

        if ($this->title) {
            $query->andFilterWhere(['or',
                ['like', 'ps_printer.title', $this->title],
                ['like', 'ps_cnc_machine.code', $this->title],
                ['like', 'company_service.title', $this->title]
                ]);
        }

        if (!empty($this->created_at)) {
            $query->andFilterDate(CompanyService::column('created_at'), $this->created_at);
        }
        // grid filtering conditions
        $query->andFilterWhere(
            [
                'company_service.id'                                       => $this->id,
                'company_service.ps_printer_id'                            => $this->ps_printer_id,
                'company_service.ps_cnc_machine_id'                        => $this->ps_cnc_machine_id,
                'company_service.location_id'                              => $this->location_id,
                'company_service.is_deleted'                               => $this->is_deleted,
                'company_service.category_id'                              => $this->category_id,
                'company_service.updated_at'                               => $this->updated_at,
                'company_service.visibility'                               => $this->visibility,
                CompanyService::column('moderator_status') => $this->moderator_status,
                'is_location_change'                       => $this->is_location_change,
            ]
        );
        if ($this->description) {
            $query->andWhere(['like', 'company_service.description', $this->description]);
        }

        if ($this->type) {
            $query->andFilterWhere(['type' => $this->type]);
        }


        if ($this->notModerated) {
            $query->needModeration()->notDeleted()->visibleEverywhere()->andWhere('company_service.ps_printer_id is null');
        }


        return $dataProvider;
    }


}
