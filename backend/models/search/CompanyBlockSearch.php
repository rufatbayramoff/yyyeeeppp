<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CompanyBlock;

/**
 * CompanyBlockSearch represents the model behind the search form about `common\models\CompanyBlock`.
 */
class CompanyBlockSearch extends CompanyBlock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['company_id'], 'string'],
            [['title', 'content', 'created_at', 'is_visible', 'position', 'videos'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyBlock::find();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['id'=>SORT_DESC]]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
        ]);

        if(is_numeric($this->company_id)){
            $query->andFilterWhere(['company_id' => $this->company_id]);
        }else{
            $query->joinWith('company');
            $query->andFilterWhere(['like', 'ps.title', $this->company_id]);
        }

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'is_visible', $this->is_visible])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'videos', $this->videos]);

        return $dataProvider;
    }
}
