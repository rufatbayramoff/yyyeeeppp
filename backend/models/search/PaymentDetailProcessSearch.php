<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentDetailProcess;

/**
 * PaymentDetailProcessSearch represents the model behind the search form about `common\models\PaymentDetailProcess`.
 */
class PaymentDetailProcessSearch extends PaymentDetailProcess
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment_detail_id'], 'integer'],
            [['processor', 'created_at', 'status', 'currency', 'transaction_id'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentDetailProcess::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'amount' => $this->amount,
            'payment_detail_id' => $this->payment_detail_id,
        ]);

        $query->andFilterWhere(['like', 'processor', $this->processor])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'transaction_id', $this->transaction_id]);

        return $dataProvider;
    }
}
