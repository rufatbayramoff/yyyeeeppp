<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProductDelivery;

/**
 * ProductDeliverySearch represents the model behind the search form about `common\models\ProductDelivery`.
 */
class ProductDeliverySearch extends ProductDelivery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid'], 'safe'],
            [['express_delivery_country_id'], 'integer'],
            [['express_delivery_first_item', 'express_delivery_following_item'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductDelivery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'express_delivery_country_id' => $this->express_delivery_country_id,
            'express_delivery_first_item' => $this->express_delivery_first_item,
            'express_delivery_following_item' => $this->express_delivery_following_item,
        ]);

        $query->andFilterWhere(['like', 'uuid', $this->uuid]);

        return $dataProvider;
    }
}
