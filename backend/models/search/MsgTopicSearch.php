<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MsgTopic;

/**
 * MsgTopicSearch represents the model behind the search form about `common\models\MsgTopic`.
 */
class MsgTopicSearch extends MsgTopic
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'creator_id', 'bind_id', 'last_support_user_id', 'hold_support_user_id'], 'integer'],
            [['bind_to', 'created_at', 'title', 'last_message_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MsgTopic::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'creator_id' => $this->creator_id,
            'bind_id' => $this->bind_id,
            'created_at' => $this->created_at,
            'last_message_time' => $this->last_message_time,
            'last_support_user_id' => $this->last_support_user_id,
            'hold_support_user_id' => $this->hold_support_user_id,
        ]);

        $query->andFilterWhere(['like', 'bind_to', $this->bind_to])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
