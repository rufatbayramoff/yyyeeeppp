<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SiteHelpVote;

/**
 * SiteHelpVoteSearch represents the model behind the search form about `common\models\SiteHelpVote`.
 */
class SiteHelpVoteSearch extends SiteHelpVote
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'site_help_id'], 'integer'],
            [['vote_hash', 'vote_answer', 'date_added'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteHelpVote::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'site_help_id' => $this->site_help_id,
            'date_added' => $this->date_added,
        ]);

        $query->andFilterWhere(['like', 'vote_hash', $this->vote_hash])
            ->andFilterWhere(['like', 'vote_answer', $this->vote_answer]);

        return $dataProvider;
    }
}
