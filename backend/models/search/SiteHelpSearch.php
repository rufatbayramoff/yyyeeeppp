<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SiteHelp;

/**
 * SiteHelpSearch represents the model behind the search form about `common\models\SiteHelp`.
 */
class SiteHelpSearch extends SiteHelp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'is_active', 'clicks', 'is_ok', 'is_bad', 'category_id', 'priority', 'views'], 'integer'],
            [['created_at', 'updated_at', 'alias', 'title', 'content'], 'safe'],
            [['is_popular'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteHelp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
            'is_active' => $this->is_active,
            'clicks' => $this->clicks,
            'is_ok' => $this->is_ok,
            'is_bad' => $this->is_bad,
            'category_id' => $this->category_id,
            'priority' => $this->priority,
            'is_popular' => $this->is_popular,
            'views' => $this->views,
        ]);

        $query->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
