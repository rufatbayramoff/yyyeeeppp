<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentDetailTaxRate;

/**
 * PaymentDetailTaxRateSearch represents the model behind the search form about `common\models\PaymentDetailTaxRate`.
 */
class PaymentDetailTaxRateSearch extends PaymentDetailTaxRate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment_detail_id', 'parent_id', 'child_id', 'tax_rate', 'user_id'], 'integer'],
            [['updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentDetailTaxRate::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'payment_detail_id' => $this->payment_detail_id,
            'parent_id' => $this->parent_id,
            'child_id' => $this->child_id,
            'tax_rate' => $this->tax_rate,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
        ]);

        return $dataProvider;
    }
}
