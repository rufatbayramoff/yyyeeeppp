<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ApiExternalSystem;

/**
 * ApiExternalSystemSearch represents the model behind the search form about `common\models\ApiExternalSystem`.
 */
class ApiExternalSystemSearch extends ApiExternalSystem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'binded_user_id'], 'integer'],
            [['name', 'private_key', 'public_upload_key', 'json_config'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApiExternalSystem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'binded_user_id' => $this->binded_user_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'private_key', $this->private_key])
            ->andFilterWhere(['like', 'public_upload_key', $this->public_upload_key])
            ->andFilterWhere(['like', 'json_config', $this->json_config]);

        return $dataProvider;
    }
}
