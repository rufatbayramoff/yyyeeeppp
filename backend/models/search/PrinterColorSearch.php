<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PrinterColor;

/**
 * PrinterColorSearch represents the model behind the search form about `common\models\PrinterColor`.
 */
class PrinterColorSearch extends PrinterColor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ambient', 'is_active'], 'integer'],
            [['title', 'created_at', 'updated_at', 'rgb', 'cmyk', 'image', 'render_color'], 'safe'],
        ];
    }
    
    
    public function getActiveInactive()
    {
        return [1 => 'Active', 0 => 'Inactive'];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PrinterColor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_active' => $this->is_active,
            'ambient' => $this->ambient,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'rgb', $this->rgb])
            ->andFilterWhere(['like', 'cmyk', $this->cmyk])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'render_color', $this->render_color]);

        return $dataProvider;
    }
}
