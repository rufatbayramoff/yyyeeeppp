<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CartItem;

/**
 * CartItemSearch represents the model behind the search form about `common\models\CartItem`.
 */
class CartItemSearch extends CartItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cart_id', 'model3d_replica_id', 'ps_printer_id', 'currency_id', 'qty', 'can_change_ps'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CartItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cart_id' => $this->cart_id,
            'model3d_replica_id' => $this->model3d_replica_id,
            'ps_printer_id' => $this->ps_printer_id,
            'price' => $this->price,
            'currency_id' => $this->currency_id,
            'qty' => $this->qty,
            'can_change_ps' => $this->can_change_ps,
        ]);

        return $dataProvider;
    }
}
