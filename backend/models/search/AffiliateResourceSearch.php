<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AffiliateResource;

/**
 * AffiliateResourceSearch represents the model behind the search form about `common\models\AffiliateResource`.
 */
class AffiliateResourceSearch extends AffiliateResource
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'affiliate_id', 'award_rule_id', 'priority'], 'integer'],
            [['url_referrer', 'params', 'status', 'created_at', 'description' ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AffiliateResource::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('affiliate');

        // grid filtering conditions
        $query->andFilterWhere([
            'affiliate_resource.id' => $this->id,
            'affiliate_resource.affiliate_id' => $this->affiliate_id,
            'affiliate_resource.award_rule_id' => $this->award_rule_id,
            'affiliate_resource.priority' => $this->priority,
            'affiliate_resource.created_at' => $this->created_at,
            'affiliate_resource.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'affiliate_resource.url_referrer', $this->url_referrer])
            ->andFilterWhere(['like', 'affiliate_resource.params', $this->params])
            ->andFilterWhere(['like', 'affiliate_resource.description', $this->description]);


        return $dataProvider;
    }
}
