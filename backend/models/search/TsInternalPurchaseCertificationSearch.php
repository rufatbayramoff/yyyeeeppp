<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TsInternalPurchaseCertification;

/**
 * TsInternalPurchaseCertificationSearch represents the model behind the search form about `common\models\TsInternalPurchaseCertification`.
 */
class TsInternalPurchaseCertificationSearch extends TsInternalPurchaseCertification
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ts_internal_purchase_uid', 'type', 'expire_date'], 'safe'],
            [['company_service_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TsInternalPurchaseCertification::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'company_service_id' => $this->company_service_id,
            'expire_date' => $this->expire_date,
        ]);

        $query->andFilterWhere(['like', 'ts_internal_purchase_uid', $this->ts_internal_purchase_uid])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
