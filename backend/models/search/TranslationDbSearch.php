<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TranslationDb;

/**
 * TranslationDbSearch represents the model behind the search form about `common\models\TranslationDb`.
 */
class TranslationDbSearch extends TranslationDb
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'line_id', 'translated_by'], 'integer'],
            [['table_name', 'column_name', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TranslationDb::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'line_id' => $this->line_id,
            'updated_at' => $this->updated_at,
            'translated_by' => $this->translated_by,
        ]);

        $query->andFilterWhere(['like', 'table_name', $this->table_name])
            ->andFilterWhere(['like', 'column_name', $this->column_name]);

        return $dataProvider;
    }
}
