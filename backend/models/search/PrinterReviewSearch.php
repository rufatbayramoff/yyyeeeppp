<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PrinterReview;

/**
 * PrinterReviewSearch represents the model behind the search form about `common\models\PrinterReview`.
 */
class PrinterReviewSearch extends PrinterReview
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'printer_id', 'user_id'], 'integer'],
            [['created_at', 'print_quality', 'ease_of_use', 'failure_rate', 'software', 'community', 'build_quality', 'reliability', 'running_expenses', 'customer_service', 'value', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PrinterReview::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'printer_id' => $this->printer_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'print_quality', $this->print_quality])
            ->andFilterWhere(['like', 'ease_of_use', $this->ease_of_use])
            ->andFilterWhere(['like', 'failure_rate', $this->failure_rate])
            ->andFilterWhere(['like', 'software', $this->software])
            ->andFilterWhere(['like', 'community', $this->community])
            ->andFilterWhere(['like', 'build_quality', $this->build_quality])
            ->andFilterWhere(['like', 'reliability', $this->reliability])
            ->andFilterWhere(['like', 'running_expenses', $this->running_expenses])
            ->andFilterWhere(['like', 'customer_service', $this->customer_service])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
