<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CsWindowQuote;

/**
 * CsWindowQuoteSearch represents the model behind the search form about `common\models\CsWindowQuote`.
 */
class CsWindowQuoteSearch extends CsWindowQuote
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_service_id', 'user_session_id', 'user_id'], 'integer'],
            [['uid', 'cs_window_snapshot_uid', 'created_at', 'updated_at', 'viewing_at', 'phone', 'email', 'contact_name', 'status', 'address', 'notes', 'currency', 'measurement'], 'safe'],
            [['total_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CsWindowQuote::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_service_id' => $this->company_service_id,
            'user_session_id' => $this->user_session_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'viewing_at' => $this->viewing_at,
            'total_price' => $this->total_price,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'cs_window_snapshot_uid', $this->cs_window_snapshot_uid])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'measurement', $this->measurement]);

        return $dataProvider;
    }
}
