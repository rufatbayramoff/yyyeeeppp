<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Model3dViewedState;

/**
 * Model3dViewedStateSearch represents the model behind the search form about `common\models\Model3dViewedState`.
 */
class Model3dViewedStateSearch extends Model3dViewedState
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'model3d_id', 'user_session_id'], 'integer'],
            [['date', 'model3dTextureInfo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Model3dViewedState::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'model3d_id' => $this->model3d_id,
            'user_session_id' => $this->user_session_id,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'model3dTextureInfo', $this->model3dTextureInfo]);

        return $dataProvider;
    }
}
