<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PrinterToProperty;

/**
 * PrinterToPropertySearch represents the model behind the search form about `common\models\PrinterToProperty`.
 */
class PrinterToPropertySearch extends PrinterToProperty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'printer_id', 'property_id'], 'integer'],
            [['value', 'created_at', 'updated_at'], 'safe'],
            [['is_editable', 'is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PrinterToProperty::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'printer_id' => $this->printer_id,
            'property_id' => $this->property_id,
            'is_editable' => $this->is_editable,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }

    public function getActiveInactive()
    {
        return [1 => 'Active', 0 => 'Inactive'];
    }
}
