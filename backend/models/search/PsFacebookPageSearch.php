<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PsFacebookPage;

/**
 * PsFacebookPageSearch represents the model behind the search form about `common\models\PsFacebookPage`.
 */
class PsFacebookPageSearch extends PsFacebookPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ps_id'], 'integer'],
            [['facebook_page_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PsFacebookPage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ps_id' => $this->ps_id,
        ]);

        $query->andFilterWhere(['like', 'facebook_page_id', $this->facebook_page_id]);

        return $dataProvider;
    }
}
