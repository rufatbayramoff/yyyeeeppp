<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentReceiptInvoiceComment;

/**
 * PaymentReceiptInvoiceCommentSearch represents the model behind the search form about `common\models\PaymentReceiptInvoiceComment`.
 */
class PaymentReceiptInvoiceCommentSearch extends PaymentReceiptInvoiceComment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['receipt_id'], 'integer'],
            [['payment_invoice_uuid', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentReceiptInvoiceComment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'receipt_id' => $this->receipt_id,
        ]);

        $query->andFilterWhere(['like', 'payment_invoice_uuid', $this->payment_invoice_uuid])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
