<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PsPrinterColor;

/**
 * PsPrinterColorSearch represents the model behind the search form about `common\models\PsPrinterColor`.
 */
class PsPrinterColorSearch extends PsPrinterColor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ps_material_id', 'color_id', 'is_active'], 'integer'],
            [['title', 'created_at', 'updated_at', 'price_measure', 'price_usd_updated'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PsPrinterColor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'                => $this->id,
            'ps_material_id'    => $this->ps_material_id,
            'created_at'        => $this->created_at,
            'updated_at'        => $this->updated_at,
            'color_id'          => $this->color_id,
            'price'             => $this->price,
            'is_active'         => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'price_measure', $this->price_measure]);

        return $dataProvider;
    }
}
