<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserDeactivatingLog;

/**
 * UserDeactivatingLogSearch represents the model behind the search form about `common\models\UserDeactivatingLog`.
 */
class UserDeactivatingLogSearch extends UserDeactivatingLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'reason_id', 'user_id'], 'integer'],
            [['created_at', 'reason_title', 'reason_desc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserDeactivatingLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'reason_id' => $this->reason_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'reason_title', $this->reason_title])
            ->andFilterWhere(['like', 'reason_desc', $this->reason_desc]);

        return $dataProvider;
    }
}
