<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CuttingPack;

/**
 * CuttingPackSearch represents the model behind the search form about `common\models\CuttingPack`.
 */
class CuttingPackSearch extends CuttingPack
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'title', 'created_at', 'updated_at', 'is_active', 'source', 'source_details', 'is_same_material_for_all_parts'], 'safe'],
            [['user_session_id', 'user_id', 'material_id', 'color_id'], 'integer'],
            [['thickness'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CuttingPack::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['created_at'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_session_id' => $this->user_session_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'material_id' => $this->material_id,
            'thickness' => $this->thickness,
            'color_id' => $this->color_id,
        ]);

        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'is_active', $this->is_active])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'source_details', $this->source_details])
            ->andFilterWhere(['like', 'is_same_material_for_all_parts', $this->is_same_material_for_all_parts]);

        return $dataProvider;
    }
}
