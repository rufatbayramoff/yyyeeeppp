<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentPayPageLog;

/**
 * PaymentPayPageLogSearch represents the model behind the search form about `common\models\PaymentPayPageLog`.
 */
class PaymentPayPageLogSearch extends PaymentPayPageLog
{

    /** @var int */
    public $order_id;

    /** @var float */
    public $summ;

    public $status;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'created_at', 'ip', 'user_agent', 'invoice_uuid', 'status', 'process_err', 'process_payload', 'order_id', 'summ'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function load($data, $formName = null)
    {
        \common\models\base\PaymentPayPageLog::load($data, $formName);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentPayPageLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if ($this->created_at) {
            if (strlen($this->created_at) < strlen('2000-01-01')) {
                $this->created_at .= substr('2021-01-01', strlen($this->created_at), 255);
            }

            $query->andFilterWhere(['>=', 'created_at', $this->created_at]);
        }
        if ($this->order_id) {
            $query->joinWith('invoice')
                ->andFilterWhere(['payment_invoice.store_order_id' => $this->order_id]);
        }
        if ($this->summ) {
            $query->joinWith('invoice')
                ->andFilterWhere(['>=', 'payment_invoice.total_amount', $this->summ]);
        }

        if ($this->status) {
            if ($this->status === PaymentPayPageLog::STATUS_PRESS_BUTTON) {
                $query->andWhere("uuid not in (SELECT uuid FROM payment_pay_page_log_status WHERE status in ('processed', 'done', 'authorized_failed'))");
            }
            if ($this->status === PaymentPayPageLog::STATUS_PROCESSED) {
                $query->andWhere("payment_pay_page_log.uuid not in (SELECT uuid FROM payment_pay_page_log_status WHERE status in ('done', 'failed', 'authorized_failed', 'server_process'))");
                $query->andWhere("payment_pay_page_log.uuid in (SELECT uuid FROM payment_pay_page_log_status WHERE status in ('processed'))");
            }
            if ($this->status === PaymentPayPageLog::STATUS_SERVER_PROCESS) {
                $query->andWhere("payment_pay_page_log.uuid not in (SELECT uuid FROM payment_pay_page_log_status WHERE status in ('done', 'failed', 'authorized_failed'))");
                $query->andWhere("payment_pay_page_log.uuid in (SELECT uuid FROM payment_pay_page_log_status WHERE status in ('server_process'))");
            }
            if ($this->status === PaymentPayPageLog::STATUS_DONE) {
                $query->andWhere("payment_pay_page_log.uuid not in (SELECT uuid FROM payment_pay_page_log_status WHERE status in ('authorize_failed'))");
                $query->andWhere("uuid in (SELECT uuid FROM payment_pay_page_log_status WHERE status = 'done')");
            }
            if ($this->status === PaymentPayPageLog::STATUS_AUTHORIZE_FAILED) {
                $query->andWhere("payment_pay_page_log.uuid not in (SELECT uuid FROM payment_pay_page_log_status WHERE status in ('done'))");
                $query->andWhere("uuid in (SELECT uuid FROM payment_pay_page_log_status WHERE status = 'authorize_failed')");
            }
        }

        $query->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'user_agent', $this->user_agent])
            ->andFilterWhere(['like', 'invoice_uuid', $this->invoice_uuid]);
        return $dataProvider;
    }
}
