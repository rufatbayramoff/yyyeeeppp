<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ApiFile;

/**
 * ApiFileSearch represents the model behind the search form about `common\models\ApiFile`.
 */
class ApiFileSearch extends ApiFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'file_id', 'user_id'], 'integer'],
            [['created_at', 'affiliate_currency', 'status'], 'safe'],
            [['affiliate_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApiFile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'file_id' => $this->file_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'affiliate_price' => $this->affiliate_price,
        ]);

        $query->andFilterWhere(['like', 'affiliate_currency', $this->affiliate_currency])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
