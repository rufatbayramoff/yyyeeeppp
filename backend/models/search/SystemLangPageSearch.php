<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SystemLangPage;

/**
 * SystemLangPageSearch represents the model behind the search form about `common\models\SystemLangPage`.
 */
class SystemLangPageSearch extends SystemLangPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'system_lang_source_id', 'user_id', 'isPost', 'isJs'], 'integer'],
            [['url', 'referer', 'created_at', 'lang_iso', 'getParams', 'postParams'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SystemLangPage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'system_lang_source_id' => $this->system_lang_source_id,
            'created_at' => $this->created_at,
            'user_id' => $this->user_id,
            'isPost' => $this->isPost,
            'isJs' => $this->isJs,
        ]);

        $query->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'referer', $this->referer])
            ->andFilterWhere(['like', 'lang_iso', $this->lang_iso])
            ->andFilterWhere(['like', 'getParams', $this->getParams])
            ->andFilterWhere(['like', 'postParams', $this->postParams]);

        return $dataProvider;
    }
}
