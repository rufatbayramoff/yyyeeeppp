<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SiteTag;

/**
 * SiteTagSearch represents the model behind the search form about `common\models\SiteTag`.
 */
class SiteTagSearch extends \common\models\base\SiteTag
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'qty_used'], 'integer'],
            [['text', 'status', 'created_at', 'updated_at', 'object_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteTag::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'qty_used' => $this->qty_used,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'object_type', $this->object_type]);

        return $dataProvider;
    }
}
