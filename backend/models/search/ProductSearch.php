<?php

namespace backend\models\search;

use common\models\Product;
use common\models\ProductCategory;
use common\models\ProductCommon;
use common\models\query\ProductQuery;
use common\modules\dynamicField\filterTemplates\DynamicFieldFilterInterface;
use common\modules\dynamicField\services\DynamicFieldFilterService;
use Psr\Log\InvalidArgumentException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ProductSearch represents the model behind the search form about `common\models\Product`.
 */
class ProductSearch extends Product
{

    /** @var string */
    public $search;

    /** @var string */
    public $listSort;

    /** @var string */
    public $sortDirection;

    public $searchFormName = 'productSearch';

    const SORT_PRICE       = 'price';
    const SORT_UPDATE_DATE = 'update-date';

    public $needModeration = 0;
    public $single_price;
    public $category_id;
    public $company_id;
    public $is_active;
    public $updated_at;
    public $user_id;
    public $title;
    public $description;
    public $product_status;

    /**
     * @var DynamicFieldFilterInterface[]
     */
    public $dfFilters = [];

    private $dfSearchFilters = [];

    private $panelLayoutTemplate = 'product';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'uuid',
                    'title',
                    'needModeration',
                    'dynamic_fields_values',
                    'description',
                    'cover_image_file_uuid',
                    'product_status',
                    'moderated_at',
                    'published_at'
                ],
                'safe'
            ],
            [['company_id', 'category_id', 'is_active'], 'integer'],
            [['single_price', 'sortDirection'], 'number'],
            [['listSort', 'search', 'user_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function formName()
    {
        return $this->searchFormName;
    }

    public function getCategory(): ?ProductCategory
    {
        return ProductCategory::find()->where(['id' => $this->category_id])->one();
    }

    /**
     * @param $params
     * @return \common\models\query\ProductQuery
     * @throws \SolrClientException
     * @throws \SolrServerException
     * @throws \yii\web\NotFoundHttpException
     */
    public function generateSearchQuery($params)
    {
        $query = Product::find();

        $this->load($params);
        if ($this->is_active === null) {
            $this->is_active = 1;
        } else {
            $this->is_active = (int)$this->is_active;
        }

        if (!$this->validate()) {
            throw new InvalidArgumentException('Invalid product search' . \yii\helpers\Html::errorSummary($this));
        }
        $categoryId = $this->category_id;
        if ($categoryId) {
            $productCategory = ProductCategory::tryFindByPk($categoryId);
            $childIds        = ArrayHelper::getColumn($productCategory->allChilds(), 'id');
            $categoryId      = array_merge([$categoryId], $childIds);
        }

        $query->joinWith('productCommon');

        $query->andFilterWhere([
            'product_common.category_id'  => $categoryId,
            'product_common.updated_at'   => $this->updated_at,
            'product.moderated_at'        => $this->moderated_at,
            'product.published_at'        => $this->published_at,
            'product_common.single_price' => $this->single_price,
            'product_common.company_id'   => $this->company_id,
        ]);

        if ($this->user_id) {
            if (is_numeric($this->user_id)) {
                $query->andWhere(['product_common.user_id' => $this->user_id]);
            } else {
                $query->joinWith('productCommon.user');
                $query->andWhere(['or', ['like', 'user.email', $this->user_id], ['like', 'user.username', $this->user_id]]);
            }
        }

        if ($this->needModeration) {
            $query->andFilterWhere(['product_common.product_status' => [Product::STATUS_PUBLISH_PENDING, Product::STATUS_PUBLISH_NEED_MODERATION]])
                ->andWhere(['product_common.is_active' => 1]);
        } else {
            $query->andFilterWhere(['product_common.is_active' => $this->is_active]);
        }

        $query->andFilterWhere(['like', 'product.uuid', $this->uuid])
            ->andFilterWhere(['like', 'product_common.title', $this->title])
            ->andFilterWhere(['like', 'product_common.description', $this->description])
            ->andFilterWhere(['product_common.product_status' => $this->product_status]);

        if ($this->search) {
            $query->joinWith('productSiteTags.siteTag');
            $query->andFilterWhere(['like', 'site_tag.`text`', $this->search])->orFilterWhere(['like', 'product.title', $this->search]);
        }
        $this->searchDynamicFields($query);
        return $query;
    }

    public function generateDataProvider(ProductQuery $query)
    {
        $dataProviderConfig = [
            'query' => $query,
        ];

        if ($this->listSort == 'price') {
            $dataProviderConfig['sort'] = ['defaultOrder' => ['price' => $this->sortDirection ? SORT_DESC : SORT_ASC]];
        } elseif ($this->listSort == 'update-date') {
            $dataProviderConfig['sort'] = ['defaultOrder' => ['update-date' => $this->sortDirection ? SORT_ASC : SORT_DESC]];
        } else {
            $dataProviderConfig['sort'] = ['defaultOrder' => ['create-date' => $this->sortDirection ? SORT_ASC : SORT_DESC]];
        }
        $dataProvider = new ActiveDataProvider($dataProviderConfig);

        $dataProvider->sort->attributes['price']       = [
            'asc'  => [ProductCommon::column('single_price') => SORT_ASC],
            'desc' => [ProductCommon::column('single_price') => SORT_DESC],
        ];
        $dataProvider->sort->attributes['update-date'] = [
            'asc'  => [ProductCommon::column('updated_at') => SORT_ASC],
            'desc' => [ProductCommon::column('updated_at') => SORT_DESC],
        ];
        $dataProvider->sort->attributes['create-date'] = [
            'asc'  => [ProductCommon::column('created_at') => SORT_ASC],
            'desc' => [ProductCommon::column('created_at') => SORT_DESC],
        ];
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws \yii\web\NotFoundHttpException
     */
    public function search($params)
    {
        $query        = $this->generateSearchQuery($params);
        $dataProvider = $this->generateDataProvider($query);
        return $dataProvider;
    }

    /**
     * @param $params
     * @return \common\models\query\ProductQuery
     * @throws \SolrClientException
     * @throws \SolrServerException
     * @throws \yii\web\NotFoundHttpException
     */
    public function generateSearchCatalogQuery($params)
    {
        $query = $this->generateSearchQuery($params);

        $query->joinWith(['productCommon.company', 'productCommon.category'])
            ->isAvailableInCatalog()
            ->groupBy('product.uuid');

        return $query;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     * @throws \SolrClientException
     * @throws \SolrServerException
     * @throws \yii\web\NotFoundHttpException
     */
    public function searchCatalog($params)
    {
        $query        = $this->generateSearchCatalogQuery($params);
        $dataProvider = $this->generateDataProvider($query);

        $dataProvider->id = null;
        return $dataProvider;

    }


    /**
     * @param $params
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadDynamicFields($params)
    {
        $dfParams = [];
        foreach ($params as $paramName => $paramValue) {
            if (strpos($paramName, 'df_') === 0) {
                $dfName            = substr($paramName, 3, 1024);
                $dfParams[$dfName] = $paramValue;
            }
        }
        $category                  = $this->category;
        $dynamicFieldFilterService = \Yii::createObject(DynamicFieldFilterService::class);

        if (!$category) {
            $category = ProductCategory::findByPk(1);
        }

        $this->dfFilters = $dynamicFieldFilterService->getFiltersListForCategory($category);
        $searchFilters   = [];
        foreach ($this->dfFilters as $key => $filter) {
            if (array_key_exists($filter->dynamicField->code, $dfParams)) {
                $filter->load($dfParams[$filter->dynamicField->code]);
                if ($dfParams[$filter->dynamicField->code] !== '') {
                    $searchFilters[$key] = $filter;
                }
            };
        }
        $this->dfSearchFilters = $searchFilters;
    }

    /**
     * @param $params
     * @throws \SolrClientException
     * @throws \SolrServerException
     */
    public function searchDynamicFields(ProductQuery $productQuery)
    {
        if (!param('allowDynamicFields', false)) {
            return;
        }
        if (!$this->dfSearchFilters) {
            return;
        }
        $query = new \SolrQuery();
        $query->setQuery('*:*');
        $query->setFacet(true);
        $applyQuery = false;
        foreach ($this->dfSearchFilters as $filter) {
            $applyQuery = $filter->buildQuery($query) || $applyQuery;
        }
        if (!$applyQuery) {
            return;
        }
        try {
            $response = \Yii::$app->solrClient->client->query($query);
        } catch (\SolrException $exception) {
            if (strpos($exception->getMessage(), 'undefined field ') === 0) {
                $productQuery->andWhere('0=1');
                return;
            }
            throw new $exception;
        }
        $solrResult = $response->getResponse();
        $uuids      = [];
        foreach ($solrResult->response->docs as $document) {
            $uuids[] = $document->id;
        }
        $productQuery->andWhere(['uuid' => $uuids]);
    }

    public function setSearchPanelTemplate($layout)
    {

        $this->panelLayoutTemplate = $layout;
    }

    public function getSearchPanelTemplate()
    {
        return $this->panelLayoutTemplate;
    }
}
