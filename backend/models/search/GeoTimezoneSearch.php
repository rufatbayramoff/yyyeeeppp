<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GeoTimezone;

/**
 * GeoTimezoneSearch represents the model behind the search form about `common\models\GeoTimezone`.
 */
class GeoTimezoneSearch extends GeoTimezone
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'utc_offset'], 'integer'],
            [['title'], 'safe'],
            [['daylight'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeoTimezone::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'utc_offset' => $this->utc_offset,
            'daylight' => $this->daylight,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
