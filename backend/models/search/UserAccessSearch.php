<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserAccess;

/**
 * UserAccessSearch represents the model behind the search form about `common\models\UserAccess`.
 */
class UserAccessSearch extends UserAccess
{
    public $userName;
    public $accessName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'access_id'], 'integer'],
            [['created_at', 'userName', 'accessName'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserAccess::find();
        $query->joinWith('access');
        $query->joinWith('user');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
            \common\models\Access::column('code')=>$this->accessName,
            \common\models\User::column('username') => $this->userName
        ]);
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'access_id' => $this->access_id,
            'created_at' => $this->created_at,
            'is_active' => $this->is_active
        ]);

        return $dataProvider;
    }
}
