<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Promocode;

/**
 * PromocodeSearch represents the model behind the search form about `common\models\Promocode`.
 */
class PromocodeSearch extends Promocode
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'discount_amount'], 'integer'],
            [['code', 'usage_type', 'discount_type', 'discount_currency', 'discount_for', 'valid_from', 'valid_to', 'description'], 'safe'],
            [['is_active', 'is_valid'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Promocode::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if($this->is_active!==null && $this->is_active!==''){
            $this->is_active = (int)$this->is_active;
        }
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'discount_amount' => $this->discount_amount,
            'valid_from' => $this->valid_from,
            'valid_to' => $this->valid_to,
            'is_active' => $this->is_active,
            'is_valid' => $this->is_valid,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'usage_type', $this->usage_type])
            ->andFilterWhere(['like', 'discount_type', $this->discount_type])
            ->andFilterWhere(['like', 'discount_currency', $this->discount_currency])
            ->andFilterWhere(['like', 'discount_for', $this->discount_for])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
