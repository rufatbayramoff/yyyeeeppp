<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SiteSearchQuery;

/**
 * SiteSearchQuerySearch represents the model behind the search form about `common\models\SiteSearchQuery`.
 */
class SiteSearchQuerySearch extends SiteSearchQuery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query'], 'safe'],
            [['qty'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteSearchQuery::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'qty' => $this->qty,
        ]);

        $query->andFilterWhere(['like', 'query', $this->query]);

        return $dataProvider;
    }
}
