<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HomePageCategoryCard;

/**
 * HomePageCategoryCardSearch represents the model behind the search form about `common\models\HomePageCategoryCard`.
 */
class HomePageCategoryCardSearch extends HomePageCategoryCard
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_category_id', 'position'], 'integer'],
            [['created_at', 'is_active', 'title', 'url'], 'safe'],
            [['cover_file_uuid'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HomePageCategoryCard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'product_category_id' => $this->product_category_id,
            'cover_file_uuid' => $this->cover_file_uuid,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'is_active', $this->is_active])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
