<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Model3dPartProperties;

/**
 * Model3dPartPropertiesSearch represents the model behind the search form about `common\models\Model3dPartProperties`.
 */
class Model3dPartPropertiesSearch extends Model3dPartProperties
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'model3d_part_id', 'vertices', 'faces'], 'integer'],
            [['length', 'width', 'height'], 'number'],
            [['metric', 'created_at', 'parser_result'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Model3dPartProperties::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'model3d_part_id' => $this->model3d_part_id,
            'length' => $this->length,
            'width' => $this->width,
            'height' => $this->height,
            'created_at' => $this->created_at,
            'vertices' => $this->vertices,
            'faces' => $this->faces,
        ]);

        $query->andFilterWhere(['like', 'metric', $this->metric])
            ->andFilterWhere(['like', 'parser_result', $this->parser_result]);

        return $dataProvider;
    }
}
