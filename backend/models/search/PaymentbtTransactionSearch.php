<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentbtTransaction;

/**
 * PaymentbtTransactionSearch represents the model behind the search form about `common\models\PaymentbtTransaction`.
 */
class PaymentbtTransactionSearch extends PaymentbtTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'success', 'customer_id'], 'integer'],
            [['transaction_id', 'created_at', 'updated_at', 'status', 'type', 'currency', 'amount', 'merchant_acount_id', 'channel', 'full_response'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentbtTransaction::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'order_id' => $this->order_id,
            'success' => $this->success,
            'customer_id' => $this->customer_id,
        ]);

        $query->andFilterWhere(['like', 'transaction_id', $this->transaction_id])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'merchant_acount_id', $this->merchant_acount_id])
            ->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'full_response', $this->full_response]);

        return $dataProvider;
    }
}
