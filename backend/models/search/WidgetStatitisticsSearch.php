<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\WidgetStatitistics;

/**
 * WidgetStatitisticsSearch represents the model behind the search form about `common\models\WidgetStatitistics`.
 */
class WidgetStatitisticsSearch extends WidgetStatitistics
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'hosting_page', 'ps_id', 'ps_printer_id', 'designer_id', 'views_count'], 'integer'],
            [['type', 'first_visit', 'last_visit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WidgetStatitistics::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'hosting_page' => $this->hosting_page,
            'ps_id' => $this->ps_id,
            'ps_printer_id' => $this->ps_printer_id,
            'designer_id' => $this->designer_id,
            'views_count' => $this->views_count,
            'first_visit' => $this->first_visit,
            'last_visit' => $this->last_visit,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
