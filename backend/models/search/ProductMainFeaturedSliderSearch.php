<?php


namespace backend\models\search;


use common\models\ProductMainFeaturedSlider;
use yii\data\ActiveDataProvider;

class ProductMainFeaturedSliderSearch extends ProductMainFeaturedSlider
{
    public function search(array $params = [])
    {
        $query = ProductMainFeaturedSlider::find();
        $query->with(['productCommon']);
        return new ActiveDataProvider([
            'query' => $query
        ]);
    }
}