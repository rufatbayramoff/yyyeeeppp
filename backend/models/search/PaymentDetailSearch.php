<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentDetail;

/**
 * PaymentDetailSearch represents the model behind the search form about `common\models\PaymentDetail`.
 */
class PaymentDetailSearch extends PaymentDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment_account_id', 'rate_id'], 'integer'],
            [['payment_detail_operation_uuid', 'updated_at', 'original_currency', 'description', 'type'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'payment_account_id' => $this->payment_account_id,
            'updated_at' => $this->updated_at,
            'amount' => $this->amount,
            'rate_id' => $this->rate_id,
        ]);

        $query->andFilterWhere(['like', 'payment_detail_operation_uuid', $this->payment_detail_operation_uuid])
            ->andFilterWhere(['like', 'original_currency', $this->original_currency])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
