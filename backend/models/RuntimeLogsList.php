<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.01.17
 * Time: 17:40
 */

namespace backend\models;


use Yii;
use yii\data\ArrayDataProvider;

class RuntimeLogsList
{
    public function getLogFilesList()
    {
        $filesList = array_merge(
            glob(Yii::getAlias('@frontend/runtime/logs/*')),
            glob(Yii::getAlias('@frontend/runtime/logs/printersBundle/*')),
            glob(Yii::getAlias('@backend/runtime/logs/*')),
            glob(Yii::getAlias('@common/runtime/logs/*')),
            glob(Yii::getAlias('@common/runtime/logs/api/*')),
            glob(Yii::getAlias('@common/runtime/logs/store/*'))
        );
        return $filesList;
    }

    public function getProvider()
    {
        $filesList = $this->getLogFilesList();
        $providerList = [];
        foreach ($filesList as $file) {
            $providerList[] = [
                'date' => date('Y-m-d H:i:s',filemtime($file)),
                'name' => $file
            ];
        }
        $provider = new ArrayDataProvider(
            [
                'key'        => 'name',
                'allModels'  => $providerList,
                'pagination' => [
                    'pageSize' => 10000,
                ],
            ]
        );
        return $provider;
    }


}