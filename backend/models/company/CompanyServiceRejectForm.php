<?php
/**
 * User: nabi
 */

namespace backend\models\company;

use common\components\exceptions\AssertHelper;
use common\components\reject\BaseRejectForm;
use common\models\CompanyService;
use common\models\SystemReject;
use yii\helpers\Json;

/**
 * Reject Form
 * Display in popup window
 *
 */
class CompanyServiceRejectForm extends BaseRejectForm
{
    private $user;
    /**
     * Save status and create log item
     *
     * @param $id
     * @param $userId
     */
    public function saveRejectStatus(CompanyService $service)
    {
        $service->moderator_status = CompanyService::MODERATOR_STATUS_REJECTED;
        AssertHelper::assert($service->save(false));

        $model = new \common\models\ModerLog();
        $model->user_id = $this->user->id;
        $model->created_at = dbexpr('NOW()');
        $model->started_at = dbexpr('NOW()');
        $model->action = CompanyService::MODERATOR_STATUS_REJECTED;
        $model->result = Json::encode(['reason'=>$this->getReason(), 'descr'=> $this->getComment()]);
        $model->object_type = 'company_service';
        $model->object_id = $service->id;
        AssertHelper::assertSave($model);
        return $model;
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::COMPANY_SERVICE_REJECT;
    }

    public function setUser($currentUser)
    {
        $this->user = $currentUser;
    }
}