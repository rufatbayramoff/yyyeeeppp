<?php
namespace backend\models\access;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class AccessForm extends \common\components\BaseForm
{
    public $accessList = [];
    public $groupAccesList = [];
    public function rules()
    {
        return [
            [['accessList', 'groupAccesList'], 'safe']
        ];
    }
    
    /**
     * Format group access 
     * 
     * @param array $accessGroup
     * @return array
     */
    public function formatGroupAccess($accessGroup)
    {
        $accessListInner = [];
        foreach($accessGroup['access'] as $k=>$accessTitle){
            if(strpos($k, '.')!==false){
                $accessKey = $k;
            }else{
                $accessKey = $accessGroup['group'] . '.' . $k;
            }
            $accessListInner[$accessKey] = $accessTitle;
        }
        return $accessListInner;
    }
    
    
    /**
     * init user group access
     * 
     * @param \common\models\base\UserAdminGroup $userGroup
     */
    public function initUserGroupAccessList(\common\models\base\UserAdminGroup $userGroup)
    {
        $access = [];
        foreach($userGroup->userAdminGroupAccesses as $k=>$v){
            if(!empty($v->is_active)){
                $access[] = $v->access;
            }
        }
        $this->groupAccesList = $access;
    }
    
    /**
     * 
     * @param \common\models\base\UserAdminGroup $userGroup
     */
    public function updateGroupAccessList(\common\models\base\UserAdminGroup $userGroup)
    {
        $dbtr = app('db')->beginTransaction();
        try{
            $hasAccess = [];
            // search to delete
            foreach($userGroup->userAdminGroupAccesses as $k=>$v){
                if(!in_array($v->access, $this->groupAccesList)){ // delete
                    $v->is_active = 0;
                    $v->update();
                }else{
                    $hasAccess[] = $v->access;
                    if(empty($v->is_active)){
                        $v->is_active = 1;
                        $v->update();
                    }
                }
            }
            // now search to add
            foreach($this->groupAccesList as $k=>$accessCode){
                if(in_array($accessCode, $hasAccess)){
                    continue;
                }
                \common\models\UserAdminGroupAccess::addRecord([
                    'group_id' => $userGroup->id,
                    'access' => $accessCode,
                    'is_active' => 1,
                    'created_at' => dbexpr('NOW()'),
                    'updated_at' => dbexpr('NOW()')
                ]);
                $hasAccess[] = $accessCode;
            }
            // now update user access by group access
            self::updateUserInGroupAccess($userGroup->id);
            $dbtr->commit();
        }catch(\Exception $e){
            $dbtr->rollBack();
            throw $e;
        }
    }
    
    /**
     * 
     * @param array $hasAccess
     */
    public static function updateUserInGroupAccess($userGroupId, $onlyUserId = false)
    {
        $userAdmins = \common\models\UserAdmin::findAll(['group_id' => $userGroupId]);
        
        $userGroup = \common\models\UserAdminGroup::findOne(['id'=>$userGroupId]); // refresh group
        $userGroupAccessList = [];
        foreach ($userGroup->userAdminGroupAccesses as $k => $v) {
            if (!empty($v->is_active)) {
                $userGroupAccessList[] = $v->access;
            }
        } 
        foreach ($userAdmins as $userAdmin) {
            if($onlyUserId && $onlyUserId!=$userAdmin->id){
                continue;
            }
            $hasOldAccess = [];
            if(!empty($userAdmin->userAdminAccesses)){
                foreach ($userAdmin->userAdminAccesses as $k => $v) {
                    if (!in_array($v->access, $userGroupAccessList)) { // delete
                        $v->is_active = 0;
                        $v->update();
                    } else {
                        $hasOldAccess[] = $v->access;
                        if (empty($v->is_active)) {
                            $v->is_active = 1;
                            $v->update(false);
                        }
                    }
                }
            }
            foreach ($userGroupAccessList as $k => $v) {
                if (in_array($v, $hasOldAccess)) {
                    continue;
                }
                \common\models\UserAdminAccess::addRecord([
                    'admin_id' => $userAdmin->id,
                    'access' => $v,
                    'is_active' => 1,
                    'created_at' => dbexpr('NOW()'),
                    'updated_at' => dbexpr('NOW()'),
                    'by_group' => 1
                ]);
            }
        }
    }

    /**
     * 
     * @param \common\models\base\UserAdmin $user
     */
    public function initUserAccessList(\common\models\base\UserAdmin $user)
    {
        $access = [];
        foreach($user->userAdminAccesses as $k=>$v){
            if(!empty($v->is_active)){
                $access[] = $v->access;
            }
        }         
        $this->accessList = $access;
    }
    
    /**
     * 
     * @param \common\models\base\UserAdmin $user
     */
    public function updateAccessList(\common\models\base\UserAdmin $user)
    {
        $dbtr = app('db')->beginTransaction();
        try{
            $hasAccess = [];
            foreach($user->userAdminAccesses as $k=>$v){
                if(!in_array($v->access, $this->accessList)){ // delete
                    $v->is_active = 0;
                    $v->by_group = 0;
                    $v->update();
                }else{
                    $hasAccess[] = $v->access;
                    if(empty($v->is_active)){
                        $v->is_active = 1;
                        $v->by_group = 0;
                        $v->update();
                    }
                }
            }
            // now search to add
            foreach($this->accessList as $k=>$v){
                if(in_array($v, $hasAccess)){
                    continue;
                }
                \common\models\UserAdminAccess::addRecord([
                    'admin_id' => $user->id,
                    'access' => $v,
                    'is_active' => 1,
                    'created_at' => dbexpr('NOW()'),
                    'updated_at' => dbexpr('NOW()'),
                    'by_group' => 0
                ]);
            }
            
            $dbtr->commit();
        }catch(\Exception $e){
            $dbtr->rollBack();
            throw $e;
        }
    }
}