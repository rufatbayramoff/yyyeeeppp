<?php
namespace backend\models\seo;

use common\components\ArrayHelper;
use common\models\SeoPageIntl;
use lib\collection\CollectionDb;
use yii\data\ActiveDataProvider;

/**
 * Date: 20.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class SeoPageActionsIntlForm extends \common\components\BaseForm
{

    /**
     * selected ids 1,2,3
     *
     * @var string
     */
    public $ids;

    public $idsArray;

    /**
     * specific action
     *
     * @var string
     */
    public $submitAction;

    public $resultMessage = 'Done';

    /**
     * @var ActiveDataProvider
     */
    public $dataProvider;

    public function rules()
    {
        return [
            [['ids'], 'safe'],
            [['submitAction'], 'required']
        ];
    }

    public function getActions()
    {
        $activations = [
            [
                'action' => 'activateSelected',
                'title'  => 'Activate Selected',
                'icon'   => 'fa fa-check-circle'
            ],
            [
                'action' => 'activateFound',
                'title'  => 'Activate Found',
                'icon'   => 'fa fa-check'
            ],
            [
                'action' => 'inactivateSelected',
                'title'  => 'Deactivate Selected',
                'icon'   => 'fa fa-toggle-off'
            ],
            [
                'action' => 'inactivateFound',
                'title'  => 'Deactivate Found',
                'icon'   => 'fa fa-toggle-off'
            ],
        ];
        $actions = [$activations];
        return $actions;
    }

    /**
     * run form logic
     *
     * @return mixed
     */
    public function submit()
    {
        $this->idsArray = explode(',', $this->ids);
        $this->runCommand($this->submitAction);
        return $this->resultMessage;
    }

    private function runCommand($cmd)
    {
        return call_user_func([$this, $cmd]);
    }


    /**
     * commands from grid tools
     * - lock, unlock
     * - activate, diactivate
     * - mass operations
     */

    public function activateFound()
    {
        $totalUpdates = 0;
        foreach ($this->dataProvider->query->batch(50) as $rows) {
            $ids = CollectionDb::getColumn($rows, 'id');
            $totalUpdates += SeoPageIntl::updateAll(['is_active' => 1], ['id' => $ids]);
        }
        $this->resultMessage = $totalUpdates . ' rows updated';
        return $totalUpdates;
    }

    public function activateSelected()
    {
        $updatesCount = SeoPageIntl::updateAll(['is_active' => 1], ['id' => $this->idsArray]);
        $this->resultMessage = $updatesCount . ' rows updated';
        return $updatesCount;
    }

    public function inactivateFound()
    {
        $totalUpdates = 0;
        foreach ($this->dataProvider->query->batch(50) as $rows) {
            $ids = CollectionDb::getColumn($rows, 'id');
            $totalUpdates += SeoPageIntl::updateAll(['is_active' => 0], ['id' => $ids]);
        }
        $this->resultMessage = $totalUpdates . ' rows updated';
        return $totalUpdates;
    }

    public function inactivateSelected()
    {
        $updatesCount = SeoPageIntl::updateAll(['is_active' => 0], ['id' => $this->idsArray]);
        $this->resultMessage = $updatesCount . ' rows updated';
        return $updatesCount;
    }

    public function setDataProvider(ActiveDataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }
}