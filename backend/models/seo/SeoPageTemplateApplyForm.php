<?php
/**
 * Date: 20.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\models\seo;


use common\components\BaseForm;
use common\models\SeoPage;
use common\models\SeoPageAutofill;
use common\models\SeoPageAutofillTemplate;
use common\modules\seo\services\SeoAutofillService;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class SeoPageTemplateApplyForm
 *
 * form to apply given template to selected/found/etc pages
 *
 * @package backend\models\seo
 */
class SeoPageTemplateApplyForm extends BaseForm
{

    const APPLY_SELECTED = 'selected';
    const APPLY_FOUND = 'found';
    const APPLY_NONE = 'none';

    public $ids;
    public $template_id;
    public $apply_type;

    /**
     * @var ActiveDataProvider
     */
    protected $dataProvider;

    /**
     * @var array
     */
    public static $applyTypes = [ 
        'selected' => 'Selected',
        'found'    => 'All Found'
    ];

    protected $resultMessage = 'Done';

    /**
     * @var SeoAutofillService
     */
    protected $seoAutoService;

    protected $idsArray = [];

    /**
     * current language. if en-US, skips generating _intl records.
     * @var string
     */
    private $lang;

    public function injectDependencies(SeoAutofillService $seoAutoService)
    {
        $this->seoAutoService = $seoAutoService;
    }

    /**
     * if inject dependencies doesn't work, ensure seoAutoService exists.
     */
    public function init()
    {
        parent::init();
        $this->seoAutoService = $this->seoAutoService ?: new SeoAutofillService();
    }

    public function rules()
    {
        return [
            [['ids'], 'safe'],
            [['template_id', 'apply_type'], 'required'],
        ];
    }

    /**
     *
     * run form logic
     */
    public function submit()
    {
        $this->idsArray = explode(',', $this->ids);

        switch ($this->apply_type) {
            case self::APPLY_FOUND:
                if($this->template_id==-1){
                    $this->clearTemplateFound();
                }else{
                    $this->applyTemplateFound();
                }
                break;
            case self::APPLY_SELECTED:
                if($this->template_id==-1){
                    $this->clearTemplateSelected();
                }else {
                    $this->applyTemplateSelected();
                }
                break;

        }
        return $this->resultMessage;
    }

    /**
     * apply template for selected
     *
     * @return int
     */
    public function applyTemplateSelected()
    {
        $template = SeoPageAutofillTemplate::findByPk($this->template_id);
        $totalUpdates = 0;
        $skipped = 0;
        $rows = SeoPage::find()->where(['id' => $this->idsArray])->all();
        foreach ($rows as $seoPage) {
            if($seoPage->is_locked){
                $skipped++;
                continue;
            }
            /* @var $seoPage SeoPage */
            if (empty($seoPage->seoPageAutofill)) {
                $res = $this->seoAutoService->applyNewTemplate($seoPage, $template);
            } else {
                if($template->type!==$seoPage->seoPageAutofill->object_type){
                    $skipped++;
                    continue;
                }
                $res = $this->seoAutoService->applyTemplate($seoPage, $template);
            }
            if($res){
                $totalUpdates++;
            }else{
                $skipped++;
            }
        }
        $this->resultMessage = sprintf('%d rows updated. %d skipped.', $totalUpdates, $skipped);
        return $totalUpdates;
    }

    /**
     * apply template to all found records.
     * data provider is used from grid
     *
     * @return int
     */
    public function applyTemplateFound()
    {
        $totalUpdates = 0;
        $skipped = 0;
        $template = SeoPageAutofillTemplate::findByPk($this->template_id);
        /** @var SeoPage[] $rows */
        foreach ($this->dataProvider->query->batch(100) as $rows) {
            // work around new ones
            foreach ($rows as $seoPage) {
                if($seoPage->is_locked){
                    $skipped++;
                    continue;
                }
                if (empty($seoPage->seoPageAutofill)) {
                    $res = $this->seoAutoService->applyNewTemplate($seoPage, $template);
                    if(!$res){
                        $skipped++;
                    }
                } else {
                    if($template->type!==$seoPage->seoPageAutofill->object_type){
                        $skipped++;
                        continue;
                    }
                    $this->seoAutoService->applyTemplate($seoPage, $template);
                    $totalUpdates++;
                }
            }
        }
        $this->resultMessage = sprintf('%d rows updated. %d skipped.', $totalUpdates, $skipped);
        return $totalUpdates;
    }

    /**
     * used by apply found action
     *
     * @see applyTemplateFound
     * @param ActiveDataProvider $dataProvider
     */
    public function setDataProvider(ActiveDataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    /**
     * get apply types: selected, all found
     *
     * @return array
     */
    public function getApplyTypes()
    {
        return self::$applyTypes;

    }

    /**
     * get list of templates to show in filter combo box grouped by language
     *
     * @return array
     */
    public function getAutofillTemplates()
    {
        $templates = SeoPageAutofillTemplate::find()->asArray()->all();
        $result = [];
        foreach($templates as $template){
            $result[$template['lang_iso']][$template['id']] = $template['template_name'];
        }
        $result['system'][-1] = '---Clear Template---';
        return $result;
    }

    /**
     * set language to use to generate seo_page_intl records
     * if en-US - just skipped
     *
     * @param $lang
     */
    public function setLang($lang)
    {
        if(empty($lang)){
            $lang = 'en-US';
        }
        $this->lang = $lang;
    }

    private function clearTemplateSelected()
    {
        $totalUpdates = 0;
        $skipped = 0;
        $rows = SeoPage::find()->where(['id' => $this->idsArray, 'is_locked'=>0])->all();
        foreach ($rows as $seoPage) {
            /* @var $seoPage SeoPage */
            if ($seoPage->seoPageAutofill) {
                $seoPage->seoPageAutofill->delete();
                $totalUpdates++;
            }else{
                $skipped++;
            }
        }
        $this->resultMessage = sprintf('%d rows updated. %d skipped.', $totalUpdates, $skipped);
        return $totalUpdates;
    }

    private function clearTemplateFound()
    {
        $totalUpdates = 0;
        $skipped = 0;
        $template = SeoPageAutofillTemplate::findByPk($this->template_id);
        /** @var SeoPage[] $rows */
        foreach ($this->dataProvider->query->batch(100) as $rows) {
            // work around new ones
            foreach ($rows as $seoPage) {
                if($seoPage->is_locked){
                    $skipped++;
                    continue;
                }
                if (empty($seoPage->seoPageAutofill)) {
                    $seoPage->seoPageAutofill->delete();
                }
                $totalUpdates++;
            }
        }
        $this->resultMessage = sprintf('%d rows updated. %d skipped.', $totalUpdates, $skipped);
        return $totalUpdates;
    }
}