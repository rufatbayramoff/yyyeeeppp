<?php
namespace backend\models\seo;

use backend\models\search\SeoPageSearch;
use backend\modules\statistic\reports\ReportExcelWriter;
use common\components\ArrayHelper;
use common\models\SeoPage;
use common\models\SeoPageIntl;
use lib\collection\CollectionDb;
use yii\data\ActiveDataProvider;

/**
 * Date: 20.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class SeoPageActionsForm extends \common\components\BaseForm
{

    /**
     * selected ids 1,2,3
     *
     * @var string
     */
    public $ids;

    public $idsArray;

    /**
     * specific action
     *
     * @var string
     */
    public $submitAction;

    public $resultMessage = 'Done';

    /**
     * @var ActiveDataProvider
     */
    public $dataProvider;

    public function rules()
    {
        return [
            [['ids'], 'safe'],
            [['submitAction'], 'required']
        ];
    }

    public function getActions()
    {
        $locks = [
            [
                'action' => 'lockSelected',
                'title'  => 'Lock Selected',
                'icon'   => 'glyphicon glyphicon-lock'
            ],
            [
                'action' => 'lockFound',
                'title'  => 'Lock Found',
                'icon'   => 'glyphicon glyphicon-lock'
            ],

            [
                'action' => 'unlockSelected',
                'title'  => 'Unlock Selected',
                'icon'   => 'fa fa-unlock-alt'
            ],
            [
                'action' => 'unlockFound',
                'title'  => 'Unlock Found',
                'icon'   => 'fa fa-unlock'
            ],
        ];
        $activations = [
            [
                'action' => 'activateSelected',
                'title'  => 'Activate Selected',
                'icon'   => 'fa fa-check-circle'
            ],
            [
                'action' => 'activateFound',
                'title'  => 'Activate Found',
                'icon'   => 'fa fa-check'
            ],
            [
                'action' => 'inactivateSelected',
                'title'  => 'Deactivate Selected',
                'icon'   => 'fa fa-toggle-off'
            ],
            [
                'action' => 'inactivateFound',
                'title'  => 'Deactivate Found',
                'icon'   => 'fa fa-toggle-off'
            ],
            [
                'action' => 'deleteSelected',
                'title'  => 'Delete Selected',
                'icon'   => 'fa fa-trash'
            ],
            [
                'action' => 'deleteFound',
                'title'  => 'Delete Found',
                'icon'   => 'fa fa-trash'
            ],
        ];

        $export = [
            [
                'action' => 'exportFound',
                'title' => 'Export Found',
                'icon' => 'fa fa-file-excel-o'
            ],
            [
                'action' => 'exportSelected',
                'title' => 'Export Selected',
                'icon' => 'fa fa-file-excel-o'
            ]
        ];
        $actions = [$locks, $activations]; //, $export];
        return $actions;
    }

    /**
     * run form logic
     *
     * @return mixed
     */
    public function submit()
    {
        $this->idsArray = explode(',', $this->ids);
        $this->runCommand($this->submitAction);
        return $this->resultMessage;
    }

    private function runCommand($cmd)
    {
        return call_user_func([$this, $cmd]);
    }


    /**
     * commands from grid tools
     * - lock, unlock
     * - activate, diactivate
     * - mass operations
     */

    public function exportFound()
    {
        // TODO
    }
    public function exportSelected()
    {
        // TODO
        $report = new SeoPageSearch();
        $reportWriter = new ReportExcelWriter('SeoPageExport');
        \Yii::$app->getDb()->createCommand('SET SESSION SQL_BIG_SELECTS=1')->execute();
        return \Yii::$app->response->sendContentAsFile($reportWriter->write($report), 'seopages.xlsx', ['inline' => true]);
    }

    /**
     * @return int
     */
    public function lockSelected()
    {
        $updatesCount = SeoPage::updateAll(['is_locked' => 1], ['id' => $this->idsArray]);
        $this->resultMessage = $updatesCount . ' rows updated';
        return $updatesCount;
    }

    /**
     * @return int
     */
    public function lockFound()
    {
        $totalUpdates = 0;
        foreach ($this->dataProvider->query->batch(50) as $rows) {
            $ids = CollectionDb::getColumn($rows, 'id');
            $totalUpdates += SeoPage::updateAll(['is_locked' => 1], ['id' => $ids]);
        }
        $this->resultMessage = $totalUpdates . ' rows updated';
        return $totalUpdates;
    }

    /**
     * @return int
     */
    public function unlockSelected()
    {
        $updatesCount = SeoPage::updateAll(['is_locked' => 0, 'need_review'=>0], ['id' => $this->idsArray]);
        $this->resultMessage = $updatesCount . ' rows updated';
        return $updatesCount;
    }

    public function unlockFound()
    {
        $totalUpdates = 0;
        foreach ($this->dataProvider->query->batch(50) as $rows) {
            $ids = CollectionDb::getColumn($rows, 'id');
            $totalUpdates += SeoPage::updateAll(['is_locked' => 0, 'need_review'=>0], ['id' => $ids]);
        }
        $this->resultMessage = $totalUpdates . ' rows updated';
        return $totalUpdates;
    }

    public function activateFound()
    {
        $totalUpdates = 0;
        foreach ($this->dataProvider->query->batch(50) as $rows) {
            $ids = CollectionDb::getColumn($rows, 'id');
            $totalUpdates += SeoPage::updateAll(['is_active' => 1, 'need_review'=>0], ['id' => $ids, 'is_locked' => 0]);
        }
        $this->resultMessage = $totalUpdates . ' rows updated';
        return $totalUpdates;
    }

    public function activateSelected()
    {
        $updatesCount = SeoPage::updateAll(['is_active' => 1, 'need_review'=>0], ['id' => $this->idsArray, 'is_locked' => 0]);
        $this->resultMessage = $updatesCount . ' rows updated';
        return $updatesCount;
    }

    public function inactivateFound()
    {
        $totalUpdates = 0;
        foreach ($this->dataProvider->query->batch(50) as $rows) {
            $ids = CollectionDb::getColumn($rows, 'id');
            $totalUpdates += SeoPage::updateAll(['is_active' => 0], ['id' => $ids, 'is_locked' => 0]);
        }
        $this->resultMessage = $totalUpdates . ' rows updated';
        return $totalUpdates;
    }

    public function inactivateSelected()
    {
        $updatesCount = SeoPage::updateAll(['is_active' => 0], ['id' => $this->idsArray, 'is_locked' => 0]);
        $this->resultMessage = $updatesCount . ' rows updated';
        return $updatesCount;
    }

    public function deleteFound()
    {
        $totalUpdates = 0;
        foreach ($this->dataProvider->query->batch(50) as $rows) {
            $ids = CollectionDb::getColumn($rows, 'id');
            $totalUpdates += SeoPage::deleteAll(['id' => $ids]);
        }
        $this->resultMessage = $totalUpdates . ' rows updated';
        return $totalUpdates;
    }

    public function deleteSelected()
    {
        $updatesCount = SeoPage::deleteAll(['id' => $this->idsArray]);
        $this->resultMessage = $updatesCount . ' rows updated';
        return $updatesCount;
    }

    public function setDataProvider(ActiveDataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }
}