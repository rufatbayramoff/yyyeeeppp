<?php


namespace backend\models\store;


use common\components\BaseForm;

class StoreDownloadReportForm extends BaseForm
{
    public $from_date;

    public $to_date;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from_date', 'to_date'], 'date','format' => 'php:Y-m-d']
        ];
    }

    /**
     * @return string|null
     * @throws \Exception
     */
    public function getToDate(): ?string
    {
        if(!$this->to_date) {
            return null;
        }
        return (new \DateTime($this->to_date))->modify('+1 day')->format('Y-m-d');
    }
}