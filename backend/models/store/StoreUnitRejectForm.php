<?php

namespace backend\models\store;

/**
 * Store unit reject form
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class StoreUnitRejectForm extends \common\components\BaseForm
{
    
    /**
     * list of reasons to select in reject form
     * 
     * @var type 
     */
    public $reasonSuggest;
    
    /**
     * Reason description why rejected. 
     * 
     * @var string
     */
    public $reasonDescription;
    
    private $settingKey = 'reject';
    /**
     * which store unit is rejected to publish
     * 
     * @var int
     */
    public $unitId;
    
    /**
     * @inheritdoc
     * 
     * @return array
     */
    public function rules()
    {
        return [
            [['unitId'], 'required'],
            ['reasonSuggest', 'safe'],
            ['reasonDescription', 'required', 'when' => function($model) {
                return $model->reasonSuggest==null;
            }]
        ];
    }
    
    /**
     * Get suggest reject list.
     * Currently returns from system_settings table, 
     * can  be updated to use db table
     * 
     * @return array
     */
    public function getSuggestList()
    {
        $list = app('setting')->getList($this->settingKey);
        return $list;
    }
    /**
     * 
     * @param type $suggestCode
     * @return type
     */
    public function getSuggestText($suggestCode)
    {
        $list = $this->getSuggestList();
        return isset($list[$suggestCode]) ? $list[$suggestCode] : '';
    }
    
    /**
     * Get reject result as string for moderation logging.
     * 
     * @return string
     */
    public function getResult()
    {
        $result = [];

        if($header = $this->getSuggestText($this->reasonSuggest))
        {
            $result[] = $header;
        }

        if($this->reasonDescription)
        {
            $result[] = $this->reasonDescription;
        }

        return implode(PHP_EOL, $result);
    }
}