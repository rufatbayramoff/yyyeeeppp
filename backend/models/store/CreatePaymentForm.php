<?php
/**
 * Created by mitaichik
 */

namespace backend\models\store;


use common\components\PaymentExchangeRateConverter;
use common\models\PaymentDetail;
use common\models\StoreOrder;
use common\models\User;
use lib\payment\PaymentManager;
use Yii;
use yii\base\Model;

/**
 * Class CreatePaymentForm
 * @package backend\models\store
 */
class CreatePaymentForm extends Model
{
    /**
     * @var int
     */
    public $fromUserId;

    /**
     * @var int
     */
    public $toUserId;

    /**
     * @var float
     */
    public $amount;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var StoreOrder
     */
    private $order;

    /**
     * CreatePaymentForm constructor.
     * @param StoreOrder $order
     */
    public function __construct(StoreOrder $order)
    {
        parent::__construct();
        $this->order = $order;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fromUserId', 'toUserId', 'amount', 'comment'], 'required'],
            ['fromUserId', 'valudateUsers'],
            ['amount', 'number'],
        ];
    }

    /**
     * @param $attribute
     */
    public function valudateUsers($attribute)
    {
        if ($this->fromUserId == $this->toUserId){
            $this->addError($attribute, 'You cant create payment between same user');
        }
    }

    /**
     *
     */
    public function process()
    {
        $fromUser = User::findOne($this->fromUserId);
        $toUser = User::findOne($this->toUserId);

        $paymentExchangeRateConverter = Yii::createObject(PaymentExchangeRateConverter::class);
        $xRate = $paymentExchangeRateConverter->getLastPaymentExchangeRate();

        PaymentManager::init($xRate)
            ->withPayment($this->order->payment)
            ->addDetails([
                'user_id' => $fromUser->id,
                'amount' => -$this->amount,
                'created_at' => dbexpr("NOW()"),
                'type' => PaymentDetail::TYPE_PAYMENT,
                'description' => $this->comment
            ])->addDetails([
                'user_id' => $toUser->id,
                'amount' => $this->amount,
                'created_at' => dbexpr("NOW()"),
                'type' => PaymentDetail::TYPE_PAYMENT,
                'description' => $this->comment
            ])
            ->submit();
    }

    /**
     * @return StoreOrder
     */
    public function getOrder()
    {
        return $this->order;
    }
}