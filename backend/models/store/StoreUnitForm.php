<?php

namespace backend\models\store;

use common\components\DateHelper;
use common\models\ModerLog;
use common\models\StoreUnit;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class StoreUnitForm extends \common\components\BaseForm
{
    /** @var StoreUnit */
    public $storeUnit = null;
    public $categoryId;

    public static function create(StoreUnit $storeUnit)
    {
        $form             = new StoreUnitForm();
        $form->storeUnit  = $storeUnit;
        $form->categoryId = $storeUnit->model3d->productCategory->id ?? null;
        return $form;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(
            $this->storeUnit->attributeLabels(),
            [
                'categoryId' => 'Category',
            ]
        );
    }


    public function getCorrection_sort_catalog_coefficient()
    {
        return $this->storeUnit->correction_sort_catalog_coefficient;
    }


    public function rules()
    {
        return [
            [['categoryId'], 'required'],
            [['categoryId'], 'integer'],
        ];
    }

    public function load($data, $formName = null)
    {
        $formData            = $data[$formName ?: 'StoreUnitForm']??[];
        $productCategoryData = $data['ProductCategory'];
        if (array_key_exists('id', $productCategoryData)) {
            ModerLog::addRecord(
                [
                    'created_at'  => DateHelper::now(),
                    'started_at'  => DateHelper::now(),
                    'action'      => 'change_category',
                    'object_type' => 'model3d',
                    'object_id'   => $this->storeUnit->model3d_id,
                    'result'      => sprintf('Category changed %d to %d', $this->storeUnit->model3d->productCategory->id ?? null, $productCategoryData['id'])
                ]
            );
            $this->categoryId = $this->storeUnit->model3d->productCommon->category_id = $productCategoryData['id'];
        }
        $this->storeUnit->load($data, $formName);
    }

    /**
     * @return bool
     */
    public function validate($att = null, $clear = true)
    {
        $validation = parent::validate($att, $clear);
        $validation &= $this->storeUnit->validate();
        $validation &= $this->storeUnit->model3d->validate();
        return $validation;
    }

    public function save()
    {
        return $this->storeUnit->save() &&
            $this->storeUnit->model3d->save() &&
            $this->storeUnit->model3d->productCommon->save();
    }
}