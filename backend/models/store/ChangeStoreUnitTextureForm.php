<?php
/**
 * Created by mitaichik
 */

namespace backend\models\store;


use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\models\Model3dPart;
use common\models\PrinterColor;
use common\models\PrinterMaterialGroup;
use common\models\StoreUnit;
use common\models\Model3dTexture;
use common\services\PrinterMaterialService;
use yii\base\Model;

class ChangeStoreUnitTextureForm extends Model
{
    /**
     * @var bool
     */
    public $forAllFiles = false;

    /**
     * @var array
     */
    public $allFilesTexture = [];

    /**
     * @var array
     */
    public $filesTextures = [];

    /**
     * @var StoreUnit
     */
    private $storeUnit;


    /**
     * ChangeTextureForm constructor.
     * @param StoreUnit $storeUnit
     */
    public function __construct(StoreUnit $storeUnit)
    {
        parent::__construct();
        $this->storeUnit = $storeUnit;
        $model3d         = $storeUnit->model3d;

        if (!$model3d->getLargestPrintingModel3dPart()) {
            return ;
        }

        if ($model3d->isOneTextureForKit()) {
            $texture               = $this->getModelOneTexture();
            $this->forAllFiles     = true;
            $this->allFilesTexture = [
                'groupId' => $texture->calculatePrinterMaterialGroup()->id,
                'colorId' => $texture->printer_color_id,
            ];
        } else {
            foreach ($model3d->getActiveModel3dParts() as $modelPart) {
                $texture                             = $modelPart->getTexture();
                $this->filesTextures[$modelPart->id] = [
                    'groupId' => $texture->calculatePrinterMaterialGroup()->id,
                    'colorId' => $texture->printer_color_id
                ];
            }
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['forAllFiles', 'boolean'],
            ['allFilesTexture', 'validateAllFilesAttribute'],
            ['filesTextures', 'validateFilesTextures'],
        ];
    }

    /**
     *
     */
    public function validateAllFilesAttribute()
    {
        if ($this->forAllFiles && (empty($this->allFilesTexture['groupId']) || empty($this->allFilesTexture['groupId']))) {
            $this->addError('allFilesTexture', 'Invalid data');
        }
    }

    /**
     *
     */
    public function validateFilesTextures()
    {
        if (!$this->forAllFiles) {
            foreach ($this->filesTextures as $filesTexture) {
                if (empty($filesTexture['groupId']) || empty($filesTexture['groupId'])) {
                    $this->addError('filesTextures', 'Invalid data');
                }
            }
        }
    }

    /**
     * @return Model3dTexture
     */
    public function getModelOneTexture()
    {
        if (!$this->storeUnit->model3d->getLargestModel3dPart()) {
            return null;
        }
        return $this->storeUnit->model3d->getKitModel3dTexture();
    }

    /**
     * @return bool
     */
    public function canModifyParts()
    {
        return $this->storeUnit->model3d->isKit();
    }

    /**
     * @return \common\components\ps\locator\materials\MaterialColorsItem[]
     */
    public function getAllowedMaterialsGroupsAndColors()
    {
        static $cache;
        if (!$cache) {
            $cache = PrinterMaterialService::formingGroupMaterialsListForColorSelector();
        }
        return $cache;
    }

    /**
     * @param bool $withEmpty
     * @return \string[]
     */
    public function getMaterialGropusOptions($withEmpty = true)
    {
        return ($withEmpty ? ['' => ''] : []) + ArrayHelper::map($this->getAllowedMaterialsGroupsAndColors(), 'materialGroup.id', 'materialGroup.title');
    }

    /**
     * @param Model3dTexture $texture
     * @return array
     */
    public function getColorOptions(Model3dTexture $texture = null)
    {
        if (!$texture) {
            return ['' => ''];
        }
        $groupId       = $texture->calculatePrinterMaterialGroup()->id;
        $allowedGroups = $this->getAllowedMaterialsGroupsAndColors();
        return ArrayHelper::map($allowedGroups[$groupId]->colors, 'id', 'title');
    }

    /**
     *
     */
    public function process()
    {
        $model3d = $this->storeUnit->model3d;

        if ($this->forAllFiles) {
            $group = PrinterMaterialGroup::findByPk($this->allFilesTexture['groupId']);
            $color = PrinterColor::findByPk($this->allFilesTexture['colorId']);
            $model3d->setKitModelMaterialGroupAndColor($group, $color);
            AssertHelper::assertSave($model3d->model3dTexture);
            AssertHelper::assertSave($model3d);

            /** @var Model3dPart $part */
            // TODO Move to part update logic
            foreach ($model3d->getActiveModel3dParts() as $part) {
                if ($partTexture = $part->model3dTexture) {
                    $part->model3d_texture_id = null;
                    $part->populateRelation('model3dTexture', null);
                    AssertHelper::assertSave($part);
                    AssertHelper::assert($partTexture->delete());
                }
            }
        } else {

            if ($model3d->kitModel3dTexture) {
                $model3d->kitModel3dTexture->isMarkForDelete = true;
                AssertHelper::assertSave($model3d);
            }

            foreach ($model3d->getActiveModel3dParts() as $part) {
                $partTextureData = $this->filesTextures[$part->id];
                $group           = PrinterMaterialGroup::findByPk($partTextureData['groupId']);
                $color           = PrinterColor::findByPk($partTextureData['colorId']);
                $part->setMaterialGroupAndColor($group, $color);
                AssertHelper::assertSave($part->model3dTexture);
                AssertHelper::assertSave($part);
            }
        }
    }
}