<?php

namespace backend\models\store;

use common\components\reject\BaseRejectForm;
use common\models\SystemReject;

/**
 * Store cancel by moderator
 * Display in popup window
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class StoreOrderCancelModeratorForm extends BaseRejectForm
{
    public function attributeLabels()
    {
        return [
            'reasonId' => \_t('app', 'Cancel reason'),
            'reasonDescription' => \_t('app', 'Cancel description')
        ];
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::STORE_ORDER_CANCEL;
    }
}