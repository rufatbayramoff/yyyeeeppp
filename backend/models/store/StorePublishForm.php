<?php namespace backend\models\store;

use common\components\BaseForm;

/**
 * StorePublishForm
 * should contain all logic to publish model3d to store
 *
 * @TODO - move all publish logic functions to here, from Model3d, StoreFacade and etc.
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class StorePublishForm extends BaseForm
{
    
}
