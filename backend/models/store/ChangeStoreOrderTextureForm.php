<?php
/**
 * Created by mitaichik
 */

namespace backend\models\store;


use common\components\exceptions\AssertHelper;
use common\models\Model3dReplica;
use common\models\Model3dTexture;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\repositories\Model3dRepository;
use Yii;
use yii\base\Model;

/**
 * Class ChangeStoreOrderTextureForm
 * @package backend\models\store
 * @todo merge with ChangeStoreUnitTextureForm
 */
class ChangeStoreOrderTextureForm extends Model
{
    /**
     * @var Model3dReplica
     */
    private $replica;

    /**
     * @var bool
     */
    public $forAllFiles = false;

    /**
     * @var array
     */
    public $allFilesTexture = [];

    /**
     * @var boolean
     */
    public $canModifyParts;

    /**
     * @var array
     */
    public $filesTextures = [];

    /** @var Model3dRepository */
    protected $model3dRepository;

    /**
     * ChangeStoreOrderTextureForm constructor.
     * @param Model3dReplica $replica
     */
    public function __construct(Model3dReplica $replica)
    {
        parent::__construct();
        $this->replica = $replica;
        $this->model3dRepository = Yii::createObject(Model3dRepository::class);

        $this->canModifyParts = $this->canModifyParts();

        if (!$this->replica->getLargestPrintingModel3dPart()) {
            return ;
        }

        if (!$this->canModifyParts() || $replica->isOneTextureForKit()){
            $texture = $this->getModelOneTexture();
            $this->forAllFiles = true;
            $this->allFilesTexture = [
                'materialId' => $texture->printer_material_id,
                'colorId' => $texture->printer_color_id,
                'infill'  => $texture->calculateInfill()
            ];
        }


        if ($this->canModifyParts() && !$replica->isOneTextureForKit()) {
            $parts = $replica->getActiveModel3dParts();
            $texture = reset($parts)->getTexture();
            $this->allFilesTexture = [
                'materialId' => $texture->printer_material_id,
                'colorId' => $texture->printer_color_id,
                'infill'  => $texture->calculateInfill()
            ];
        }





        if($this->canModifyParts()) {

            if($replica->isOneTextureForKit()) {
                $texture = $this->getModelOneTexture();
                foreach ($replica->getActiveModel3dParts() as $modelPart){
                    $this->filesTextures[$modelPart->id] = [
                        'materialId' => $texture->printer_material_id,
                        'colorId' => $texture->printer_color_id,
                        'infill' => $modelPart->getCalculatedTexture()->calculateInfill()
                    ];
                }
            }
            else {
                foreach ($replica->getActiveModel3dParts() as $modelPart){
                    $texture =  $modelPart->getTexture();
                    $this->filesTextures[$modelPart->id] = [
                        'materialId' => $texture->printer_material_id,
                        'colorId' => $texture->printer_color_id,
                        'infill' => $modelPart->getCalculatedTexture()->calculateInfill()
                    ];
                }
            }
        }
    }


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['forAllFiles', 'boolean'],
            ['allFilesTexture', 'validateAllFilesAttribute'],
            ['filesTextures', 'validateFilesTextures'],
        ];
    }

    /**
     *
     */
    public function validateAllFilesAttribute()
    {
        if ($this->forAllFiles && (empty($this->allFilesTexture['materialId']) || empty($this->allFilesTexture['colorId']))){
            $this->addError('allFilesTexture', 'Invalid data');
        }
    }

    /**
     *
     */
    public function validateFilesTextures()
    {
        if (!$this->forAllFiles){
            foreach ($this->filesTextures as $filesTexture){
                if (empty($filesTexture['materialId']) || empty($filesTexture['colorId'])){
                    $this->addError('filesTextures', 'Invalid data');
                }
            }
        }
    }

    /**
     * @return Model3dTexture
     */
    public function getModelOneTexture()
    {
        return $this->replica->kitModel3dTexture ?: $this->replica->getLargestPrintingModel3dPart()->getTexture();
    }

    /**
     * @return bool
     */
    public function canModifyParts()
    {
        return $this->replica->isKit();
    }

    /**
     * @return PrinterMaterial[]
     */
    public static function getAllowedMaterialsAndColors()
    {
        return PrinterMaterial::find()->active()->with('colors')->orderBy('printer_material.title ASC')->all();
    }

    /**
     *
     */
    public function process()
    {
        $model3d = $this->replica;

        if ($this->forAllFiles) {
            $material = PrinterMaterial::tryFindByPk($this->allFilesTexture['materialId']);
            $color = PrinterColor::findByPk($this->allFilesTexture['colorId']);
            $infill = $this->allFilesTexture['infill'];
            $model3d->setKitModelMaterialAndColor($material, $color, $infill);
            $this->model3dRepository->save($model3d);
        } else {
            if ($model3d->kitModel3dTexture) {
                $model3d->kitModel3dTexture->isMarkForDelete = true;
            }
            foreach ($model3d->getActiveModel3dParts() as $part) {
                $partTextureData = $this->filesTextures[$part->id];
                $material = PrinterMaterial::findByPk($partTextureData['materialId']);
                $color = PrinterColor::findByPk($partTextureData['colorId']);
                $infill = $partTextureData['infill'];
                $part->setMaterialAndColor($material, $color, $infill);
            }
        }
        $this->model3dRepository->save($model3d);
    }
}