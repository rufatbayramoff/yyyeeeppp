<?php

namespace backend\models;

use backend\components\AdminAccess;
use common\models\User;
use frontend\components\UserUtils;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Url;
use function H;

/**
 * Backend util class
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Backend extends Model
{

    /**
     * Format information about user by user_id.
     * Should be used only in admin.
     *
     * @param int|User $userId
     * @return string
     */
    public static function displayUser($userId)
    {
        /** @var User $userObj */
        if (is_numeric($userId)) {
            $userObj = User::findByPk($userId);
        } else {
            $userObj = $userId;
        }
        if ($userObj === null) {
            return $userId;
        }
        $trustLevelInfo = User::getTrustLevel($userObj->trustlevel);

        $link = $userObj->status == User::STATUS_DELETED
            ? H($userObj->username)
            : Html::a(H($userObj->username), UserUtils::getUserPublicProfileUrl($userObj), ['target' => '_blank', 'data-pjax' => 0]);

        $adminLink = Url::toRoute(['user/user/view', 'id' => $userObj->id]);
        return sprintf(
            '%s %s <a href="%s" data-pjax="0"><span style="background: %s">[%d]</span></a>',
            $userObj->userProfile ? H($userObj->userProfile->full_name) : '',
            $link,
            $adminLink,
            $trustLevelInfo ? $trustLevelInfo['color'] : '',
            $userObj->id
        );
    }

    /**
     * Get autocreated crud sections.
     * To generate crud for new  table use :
     * <pre>
     *  yii base-models/generate
     *  yii base-models/crud
     * </pre>
     *
     * @return string[]
     */
    private function getCruds()
    {
        $directory = \Yii::getAlias('@backend/views/crud');
        if (!is_dir($directory)) {
            return [];
        }
        $filenames = [];
        $iterator = new \DirectoryIterator($directory);
        foreach ($iterator as $fileinfo) {
            if ($fileinfo->isDir()) {
                $fn = $fileinfo->getFilename();
                if ($fn == '.' || $fn == '..') {
                    continue;
                }
                $filenames[$fn] = $fn;
            }
        }
        ksort($filenames);
        return $filenames;
    }

    private function getPsMenu()
    {
        return [
            [
                'label'   => 'Company (PS)',
                'url'     => ['/ps/ps'],
                'icon'    => 'fa fa-list-ul',
                'visible' => AdminAccess::can('ps.view'),
            ],
            [
                'label'   => 'Company Services',
                'url'     => ['/company/company-service'],
                'icon'    => 'fa fa-building',
                'visible' => AdminAccess::can('psmachine.view'),
            ],
            [
                'label'   => 'Printers Moderation',
                'url'     => ['/ps/ps-printer'],
                'icon'    => 'fa fa-balance-scale',
                'visible' => AdminAccess::can('psprinter.view'),
            ],
            [
                'label'   => 'Printer Certification',
                'url'     => ['/ps/ps-printer-test'],
                'icon'    => 'fa fa-thumbs-up',
                'visible' => AdminAccess::can('psprintertest.view'),
            ],
            [
                'label'   => 'Class of Certification',
                'url'     => ['/tsCertification/certification-class'],
                'icon'    => 'fa fa-thumbs-up',
                'visible' => AdminAccess::can('psmachine.view'),
            ],
            [
                'label'   => 'Printer Test Orders',
                'url'     => ['/ps/test-order'],
                'icon'    => 'fa fa-shopping-cart',
                'visible' => AdminAccess::can('psprinter.testorder.view')
            ],

            [
                'label'   => 'Locations',
                'url'     => ['/ps/user-location'],
                'icon'    => 'fa fa-map-marker',
                'visible' => false
            ],
            [
                'label'   => 'Company Services Categories',
                'url'     => ['/company/company-service-category'],
                'icon'    => 'fa fa-tags',
                'visible' => AdminAccess::can('psmachine.view'),
            ],
            [
                'label' => 'Companies (Catalog)',
                'url' => ['/company/company'],
                'icon' => 'fa fa-briefcase',
                'visible' => AdminAccess::can('company.view')
            ],
            [
                'label' => 'Company blocks',
                'url' => ['/company/company-block'],
                'icon' => 'fa fa-briefcase',
                'visible' => AdminAccess::can('company.view')
            ],

        ];
    }

    private function getCncMenu()
    {
        return [
            [
                'label' => 'Cnc Materials ',
                'url'   => ['/cnc/cnc-material'],
                'icon'  => 'fa fa-database',
            ],
            [
                'label' => 'Cnc Materials group ',
                'url'   => ['/cnc/cnc-material-group'],
                'icon'  => 'fa fa-database',
            ]
        ];
    }

    protected function getCuttingMenu()
    {
        return [
            [
                'label' => 'Cutting Machines',
                'url'   => ['/cutting/cutting-machine'],
                'icon'  => 'fa fa-building',
            ],
            [
                'label' => 'Cutting Materials',
                'url'   => ['/cutting/cutting-material'],
                'icon'  => 'fa fa-database',
            ],
        ];
    }

    private function getPrinterMenu()
    {
        return [
            [
                'label' => 'Printer ',
                'url'   => ['/ps/printer'],
                'icon'  => 'fa fa-print',
            ],[
                'label' => 'Printer Reviews ',
                'url'   => ['/ps/printer-review'],
                'icon'  => 'fa fa-star',
            ],
            [
                'label' => 'Printer Properties',
                'url'   => ['/ps/printer-properties'],
                'icon'  => 'fa fa-sliders',
            ],
            [
                'label' => 'Printer To Property',
                'url'   => ['/ps/printer-to-property'],
                'icon'  => 'fa fa-sliders',
            ],
            [
                'label' => 'Printer Materials ',
                'url'   => ['/ps/printer-material'],
                'icon'  => 'fa fa-database',
            ],
            [
                'label' => 'Printer Materials group ',
                'url'   => ['/ps/printer-material-group'],
                'icon'  => 'fa fa-database',
            ],
            [
                'label' => 'Printer To Material ',
                'url'   => ['/ps/printer-to-material'],
                'icon'  => 'fa fa-database',
            ],
            [
                'label' => 'Printer Colors ',
                'url'   => ['/ps/printer-color'],
                'icon'  => 'fa fa-eraser',
            ],
            [
                'label' => 'Printer Material Colors ',
                'url'   => ['/ps/printer-material-to-color'],
                'icon'  => 'fa fa-magic',
            ],
            [
                'label' => 'Printer Technology ',
                'url'   => ['/ps/printer-technology'],
                'icon'  => 'fa fa-wrench',
            ]
        ];
    }

    private function getStoreMenu()
    {
        return [
            [
                'label'   => 'Orders',
                'url'     => ['/store/store-order'],
                'icon'    => 'fa fa-shopping-cart',
                'visible' => AdminAccess::can('store_order.view')
            ],
            [
                'label'   => 'Orders notify',
                'url'     => ['/store/store-notify'],
                'icon'    => 'fa fa-bell-o',
                'visible' => AdminAccess::can('store_order.view')
            ],
            [
                'label'   => 'Order reviews',
                'url'     => ['/store/reviews'],
                'icon'    => 'fa fa-star-o',
                'visible' => AdminAccess::can('store_order_reviews.view')
            ],
            [
                'label'   => 'Order share',
                'url'     => ['/store/store-order-review-share'],
                'icon'    => 'fa fa-star-o',
                'visible' => AdminAccess::can('store_order_reviews.view')
            ],
            [
                'label'   => 'Store units',
                'url'     => ['/store/store-unit'],
                'icon'    => 'fa fa-file-powerpoint-o',
                'visible' => AdminAccess::can('storeunit.view')
            ],
            [
                'label'   => 'Cutting packs',
                'url'     => ['/store/cutting-pack'],
                'icon'    => 'fa fa-file-powerpoint-o',
                'visible' => AdminAccess::can('storeunit.view')
            ],
            [
                'label'   => 'Deleted store units',
                'url'     => ['/store/store-unit/deleted'],
                'icon'    => 'fa fa-file-powerpoint-o',
                'visible' => AdminAccess::can('storeunit.view')
            ],
            [
                'label'   => 'Payments',
                'url'     => ['#'],
                'icon'    => 'fa fa-money',
                'visible' => AdminAccess::can('payment.view'),
                'items'   => [
                    [
                        'label'   => 'Bank Invoices',
                        'url'     => ['/store/payment-bank-invoice'],
                        'icon'    => 'fa fa-money',
                        'visible' => AdminAccess::can('payment.view')
                    ],

                    [
                        'label'   => 'Payments',
                        'url'     => ['/store/payment'],
                        'icon'    => 'fa fa-money',
                        'visible' => AdminAccess::can('payment.view')
                    ],
                    [
                        'label'   => 'Payment Transactions',
                        'url'     => ['/store/payment-transaction'],
                        'icon'    => 'fa fa-sitemap',
                        'visible' => AdminAccess::can('payment.view')
                    ],
                    [
                        'label'   => 'Bank Transfer Payout',
                        'url'     => ['/store/payment-bank-payout'],
                        'icon'    => 'fa fa-sitemap',
                        'visible' => AdminAccess::can('payment.view')
                    ],
                    [
                        'label'   => 'Payments correction',
                        'url'     => ['/store/payment-correction'],
                        'icon'    => 'fa fa-money',
                        'visible' => AdminAccess::can('payment.view')
                    ],
                    [
                        'visible' => AdminAccess::can('payment.reports'),
                        'label'   => 'Balance reports',
                        'url'     => ['/payment/balance-report/index'],
                        'icon'    => 'fa fa-files-o',
                    ],
                    [
                        'visible' => AdminAccess::can('payment.logs'),
                        'label'   => 'Payment logs',
                        'url'     => ['/payment/log/index'],
                        'icon'    => 'fa fa-files-o',
                    ],
                    [
                        'visible' => AdminAccess::can('payment.warnings'),
                        'label'   => 'Payment warnings',
                        'url'     => ['/payment/warning/index'],
                        'icon'    => 'fa  fa-warning',
                    ],
                    [
                        'visible' => AdminAccess::can('payment.view'),
                        'label'   => 'Bonus list',
                        'url'     => ['/payment/bonus/index'],
                        'icon'    => 'fa fa-gift',
                    ],
                    [
                        'visible' => AdminAccess::can('payment.view'),
                        'label'   => 'Currency conversion',
                        'url'     => ['/payment/conversion/index'],
                        'icon'    => 'fa fa-arrows-h',
                    ],
                ]
            ],

            [
                'label'   => 'Promo Codes',
                'url'     => ['/promocode/promocode'],
                'icon'    => 'fa fa-id-card-o',
                'visible' => AdminAccess::can('promocode.view'),
                'items'   => [
                    [
                        'label'   => 'Promo Codes',
                        'url'     => ['/promocode/promocode'],
                        'icon'    => 'fa fa-id-card-o',
                        'visible' => AdminAccess::can('promocode.view'),
                    ],
                    [
                        'label'   => 'Promo Codes History',
                        'url'     => ['/promocode/promocode-history'],
                        'icon'    => 'fa fa-history',
                        'visible' => AdminAccess::can('promocodehistory.view'),
                    ],
                    [
                        'label'   => 'Promo Codes Usage',
                        'url'     => ['/promocode/promocode-usage'],
                        'visible' => AdminAccess::can('promocodeusage.view'),
                        'icon'    => 'fa fa-calendar-o',
                    ]
                ]
            ],
            [
                'label'   => '3D Models',
                'url'     => '#',
                'icon'    => 'fa fa-cubes',
                'visible' => AdminAccess::can('model3d.view'),
                'items'   => $this->getModelsMenu()
            ],
            [
                'label'   => 'Preorders',
                'url'     => ['/store/preorder'],
                'icon'    => 'fa fa-send',
                'visible' => AdminAccess::can('store_order.view'),
            ],
            [
                'label'   => 'Instant payments',
                'url'     => ['/store/instant-payment'],
                'icon'    => 'fa fa-shopping-cart',
                'visible' => AdminAccess::can('store_order.view')
            ],
            [
                'label'   => 'TS internal purchase',
                'url'     => ['/store/ts-internal-purchase'],
                'icon'    => 'fa fa-shopping-cart',
                'visible' => AdminAccess::can('store_order.view')
            ],
        ];
    }

    private function getSystemMenu()
    {
        return [
            [
                'label'   => 'Printers (System)',
                'url'     => ['#'],
                'icon'    => '',
                'visible' => AdminAccess::can('printer.view'),
                'items'   => $this->getPrinterMenu()
            ],
            [
                'label'   => 'Cnc (System)',
                'url'     => ['#'],
                'icon'    => '',
                'visible' => AdminAccess::can('printer.view'),
                'items'   => $this->getCncMenu()
            ],
            [
                'label'   => 'Cutting (System)',
                'url'     => ['#'],
                'icon'    => '',
                'visible' => AdminAccess::can('printer.view'),
                'items'   => $this->getCuttingMenu()
            ],
            [
                'label'   => 'Settings',
                'url'     => ['site/setting'],
                'icon'    => 'fa fa-cogs',
                'visible' => AdminAccess::can('setting.view'),

            ],
            [
                'label'   => 'Moderator Accesses',
                'url'     => ['moderator/access'],
                'icon'    => 'fa fa-bolt',
                'visible' => AdminAccess::can('system.view_access'),
            ],
            [
                'label'   => 'Content filter',
                'url'     => ['/site/content-filter-banned-phrase'],
                'icon'    => 'fa  fa-ban',
                'visible' => AdminAccess::can('system.content_filter'),
            ],
            [
                'label'   => 'Cache',
                'url'     => ['/site/cache'],
                'icon'    => 'fa  fa-eraser',
                'visible' => AdminAccess::can('site.cache'),
            ],
            [
                'label'   => 'File Jobs',
                'url'     => ['/model3d/jobs'],
                'visible' => AdminAccess::can('file_job.view'),
                'icon'    => 'fa fa-file-image-o',

            ],
            [
                'label'   => 'Moderator Logs',
                'url'     => ['moderator/moder-log'],
                'icon'    => 'fa fa-bolt',
                'visible' => AdminAccess::can('moderlog.view'),
            ],
            [
                'label'   => 'Http log',
                'url'     => ['/site/http-request-log'],
                'icon'    => 'fa  fa-archive',
                'visible' => AdminAccess::can('httplog.view'),
            ],
            [
                'label'   => 'Runtime files log',
                'url'     => ['/site/runtime-files-log'],
                'icon'    => 'fa  fa-archive',
                'visible' => AdminAccess::can('httplog.view'),
            ],
            [
                'label'   => 'Countries',
                'icon'    => 'fa fa-globe',
                'visible' => AdminAccess::can('geocountry.view'),
                'url'     => ['site/geo-country']
            ],
            [
                'label'   => 'Model3d Watermark Decoder',
                'icon'    => 'fa fa-file',
                'visible' => AdminAccess::can('watermark.view'),
                'url'     => ['model3d/watermark']
            ],
            [
                'label' => 'Images',
                'url'   => ['/system/file-manager'],
                'icon'  => 'fa fa-image',
            ],
            [
                'label'   => 'Custom & Taxes',
                'url'     => ['#'],
                'visible' => AdminAccess::can('custom.view'),
                'items'   => [
                    [
                        'label' => 'Country  Taxes',
                        'url'   => ['store/tax-country'],
                        'icon'  => 'fa fa-institution'
                    ]
                ]
            ],

            /*[
                'label' => 'OSN Vendors',
                'url' => ['/site/osn-vendor'],
                'icon' => 'fa fa-google',
            ],
            [
                'label' => 'Gii',
                'url' => ['/gii'],
                'icon' => 'fa fa-file-code-o',
                'linkOptions' => ['target' => '_blank']
            ],
            [
                'label' => 'Debug',
                'url' => ['/debug'],
                'icon' => 'fa fa-bug',
                'linkOptions' => ['target' => '_blank']
            ],
            [
                'label' => 'Api Doc',
                'url' => ['../../doc/api/index.html'],
                'icon' => 'fa fa-file-text-o',
                'linkOptions' => ['target' => '_blank']
            ],
             */
        ];
    }

    private function getModelsMenu()
    {
        return [
            [
                'label'   => 'Licenses',
                'url'     => ['/model3d/license'],
                'icon'    => 'fa fa-copyright',
                'visible' => AdminAccess::can('license.view'),
            ],
            [
                'label'   => 'Images',
                'url'     => ['/model3d/image'],
                'visible' => AdminAccess::can('model3d_image.view'),
                'icon'    => 'fa fa-file-image-o',

            ],
        ];
    }

    private function getSeoMenu()
    {
        return [
            [
                'label'   => 'SEO - Pages',
                'icon'    => 'fa fa-search',
                'visible' => AdminAccess::can('seo_page.view'),
                'url'     => ['site/seo-page']
            ],

            [
                'label'   => 'SEO - Redirects',
                'icon'    => 'fa fa-link',
                'visible' => AdminAccess::can('seo_redir.view'),
                'url'     => ['site/seo-redir']
            ],
            [
                'label'   => 'SMM - Store Units',
                'icon'    => 'fa fa-file-text-o',
                'visible' => AdminAccess::can('siteblock.smm'),
                'url'     => ['site/smm']
            ],
            [
                'template' => '',
                'url'      => ['/site/seo-page-intl']
            ],
            [
                'template' => '',
                'url'      => ['/site/seo-page-autofill-template']
            ],
        ];
    }

    private function getContentMenu()
    {
        return [
            [
                'label'   => 'Help Pages Categories',
                'icon'    => 'fa fa-question-circle',
                'visible' => AdminAccess::can('sitehelp.view'),
                'url'     => ['site/site-help-category']
            ],
            [
                'label'   => 'Help Pages',
                'icon'    => 'fa fa-question-circle',
                'visible' => AdminAccess::can('sitehelp.view'),
                'url'     => ['site/site-help']
            ],
            [
                'visible' => AdminAccess::can('blog.view'),
                'label'   => 'Blog Posts',
                'url'     => ['/blog/blog-post/index'],
                'icon'    => 'fa fa-book',
            ],
            [
                'visible' => AdminAccess::can('sitetag.view'),
                'label'   => 'Site tags',
                'url'     => ['/site/site-tag'],
                'icon'    => 'fa fa-tag',
            ],
            [
                'label'   => 'Email Templates',
                'url'     => ['site/email-template'],
                'icon'    => 'fa fa-envelope-o',
                'visible' => AdminAccess::can('emailtemplate.view'),
            ],
            [
                'label'   => 'Website blocks',
                'icon'    => 'fa fa-file-text-o',
                'visible' => AdminAccess::can('siteblock.view'),
                'url'     => ['site/site-block']
            ],
            [
                'label'   => 'Static Pages',
                'visible' => AdminAccess::can('static_page.view'),
                'icon'    => 'fa fa-file-o',
                'url'     => ['/site/static-page']
            ],
            [
                'label'   => 'Hire designer',
                'visible' => AdminAccess::can('hiredesigner.view'),
                'icon'    => 'fa fa-pencil',
                'url'     => ['/site/hire-designer']
            ],
            [
                'label'   => 'Reject and Ban Templates',
                'url'     => ['/ps/system-reject'],
                'icon'    => 'fa fa-thumbs-down',
                'visible' => AdminAccess::can('systemreject.view'),
            ],
            [
                'label'   => 'Message Folders',
                'visible' => AdminAccess::can('website.view_translaction'),
                'icon'    => 'fa fa-folder',
                'url'     => ['/site/msg-folder']
            ],
            [
                'label'   => 'Wiki materials',
                'visible' => AdminAccess::can('website.wiki_material'),
                'icon'    => 'fa  fa-wikipedia-w',
                'url'     => ['/site/wiki-material']
            ],
            [
                'label'   => 'Wiki material features',
                'visible' => AdminAccess::can('website.wiki_material'),
                'icon'    => 'fa  fa-wikipedia-w',
                'url'     => ['/site/wiki-material-feature']
            ],
            [
                'label'   => 'Equipment categories',
                'visible' => AdminAccess::can('website.wiki_machine'),
                'icon'    => 'fa fa-align-left',
                'url'     => ['/site/equipment-category']
            ],
            [
                'label'   => 'Wiki machines',
                'visible' => AdminAccess::can('website.wiki_machine'),
                'icon'    => 'fa fa-building',
                'url'     => ['/site/wiki-machine']
            ],
            [
                'label'   => 'Wiki machine properties',
                'visible' => AdminAccess::can('website.wiki_machine'),
                'icon'    => 'fa fa-database',
                'url'     => ['/site/wiki-machine-available-property']
            ],
            [
                'label'   => 'Connected apps',
                'visible' => AdminAccess::can('website.connected_app'),
                'icon'    => 'fa fa-sellsy',
                'url'     => ['/site/connected-app']
            ],
            [
                'template' => '',
                'url'      => ['/site/msg-folder-intl']
            ],
        ];
    }

    private function getWebsiteMenu()
    {
        $menu = [
            [
                'label'   => 'Translation',
                'icon'    => 'fa fa-language',
                'visible' => AdminAccess::can('website.view_translaction'),
                'url'     => ['site/lang']
            ],
            [
                'label'   => 'Translation DB',
                'icon'    => 'fa fa-language',
                'visible' => AdminAccess::can('website.view_translaction'),
                'url'     => ['site/lang-db']
            ],
            [
                'label'   => 'Translation statistics',
                'icon'    => 'fa fa-language',
                'visible' => AdminAccess::can('website.view_translaction'),
                'url'     => ['site/lang-statistic']
            ],
            [
                'label'   => 'Languages',
                'icon'    => 'fa fa-globe',
                'visible' => AdminAccess::can('website.view_lang'),
                'url'     => ['site/system-lang']
            ],
            [
                'label'   => 'Currency',
                'icon'    => 'fa fa-dollar',
                'visible' => AdminAccess::can('website.view_currency'),
                'url'     => ['site/payment-currency']
            ],
            [
                'label'   => 'Widget statistics',
                'icon'    => 'fa fa-object-group',
                'visible' => AdminAccess::can('widget.statistics'),
                'url'     => ['site/widget-statistics']
            ],
            [
                'label' => 'Home Page',
                'url' => ['/site/home-page-category-block'],
                'icon' => 'fa fa-tasks',
                'visible' => AdminAccess::can('home_page.edit'),
                'items'=> [
                    [
                        'label' => 'Category block',
                        'url' => ['/site/home-page-category-block/index'],
                        'icon' => 'fa fa-tasks',
                    ],
                    [
                        'label' => 'Category cards',
                        'url' => ['/site/home-page-category-card/index'],
                        'icon' => 'fa fa-tasks',
                    ],
                    [
                        'label' => 'Featured category',
                        'url' => ['/site/home-page-featured/index'],
                        'icon' => 'fa fa-tasks',
                    ]
                ]
            ],
        ];
        return $menu;
    }

    /**
     * get left menu links for backend part
     *
     * @return array
     */
    public function getMenu()
    {
        $menu = [
            [
                'label'   => 'Store',
                'url'     => '#',
                'icon'    => 'fa fa-shopping-cart',
                'visible' => AdminAccess::can('storeunit.view'),
                'items'   => $this->getStoreMenu()
            ],
            [
                'label' => 'Products',
                'url' => '#',
                'icon' => 'fa fa-product-hunt',
                'visible' => AdminAccess::can('product.view'),
                'items' => $this->getProductsMenu()
            ],
            [
                'label'   => 'Company (Print Services)',
                'url'     => '#',
                'icon'    => 'fa fa-print',
                'visible' => AdminAccess::can('ps.view') ||
                    AdminAccess::can('printer.view') ||
                    AdminAccess::can('psprintertest.view') ||
                    AdminAccess::can('psprinter.view'),
                'items'   => $this->getPsMenu()
            ],
            [
                'label'   => 'Users',
                'url'     => ['/user'],
                'icon'    => 'fa fa-users',
                'visible' => AdminAccess::can('user.view') || AdminAccess::can('user_request.view'),
                'items'   => $this->getUsersMenu()
            ],
            [
                'label'   => 'Support',
                'url'     => '#',
                'icon'    => 'fa fa-life-ring',
                'visible' => AdminAccess::can('support.view'),
                'items'   => $this->getSupportMenu()
            ],
            [
                'label'   => 'System',
                'url'     => '#',
                'icon'    => 'fa fa-cogs',
                'visible' => AdminAccess::can('system.view'),
                'items'   => $this->getSystemMenu()
            ],
            [
                'label'   => 'Content',
                'url'     => '#',
                'icon'    => 'fa fa-file-text',
                'visible' => AdminAccess::can('sitehelp.view'),
                'items'   => $this->getContentMenu()
            ],
            [
                'label'   => 'Website',
                'url'     => '#',
                'icon'    => 'fa fa-television',
                'visible' => AdminAccess::can('website.view'),
                'items'   => $this->getWebsiteMenu()
            ],
            [
                'label'   => 'SEO',
                'url'     => '#',
                'visible' => AdminAccess::can('seo_page.view'),
                'icon'    => 'fa fa-indent',
                'items'   => $this->getSeoMenu()
            ],


            [
                'label'   => 'Statistic',
                'url'     => '#',
                'icon'    => 'fa fa-signal',
                'visible' => AdminAccess::can('statistic.view'),
                'items'   => [
                    [
                        'visible' => AdminAccess::can('statistic.view'),
                        'label'   => 'Reports',
                        'url'     => ['/statistic/reports/index'],
                        'icon'    => 'fa fa-files-o',
                    ],
                    [
                        'visible' => AdminAccess::can('statistic.payments'),
                        'label'   => 'Payments',
                        'url'     => ['/statistic/payments'],
                        'icon'    => 'fa fa-th',
                    ],
                    [
                        'visible' => AdminAccess::can('statistic.view_filepage_maps'),
                        'label'   => 'Filepage Maps',
                        'url'     => ['/statistic/filepage-maps'],
                        'icon'    => 'fa fa-th',
                    ],
                    [
                        'visible' => AdminAccess::can('statistic.pay_page_log'),
                        'label'   => 'Pay page log',
                        'url'     => ['/statistic/pay-page-log'],
                        'icon'    => 'fa fa-th',
                    ],
                ],
            ],
            [
                'label'   => 'Affiliate and API',
                'url'     => '#',
                'icon'    => 'fa fa-link',
                'visible' => AdminAccess::can('affiliate.view'),
                'items'   => $this->getAffiliateMenu()
            ]
        ];


        // create crud menu

        $crudMenu = [
            'label'   => 'CRUD tools',
            'url'     => '#',
            'icon'    => 'fa fa-tasks',
            'visible' => AdminAccess::can('crud.view'),
            'items'   => []
        ];

        foreach ($this->getCruds() as $action) {
            $crudMenu['items'][] = [
                'label' => $action,
                'url'   => ['/crud/' . $action],
                'icon'  => 'fa fa-server'
            ];
        }

        $menu[] = $crudMenu;

        return $menu;
    }

    private function getUsersMenu()
    {
        return [
            [
                'label' => 'Users list',
                'url'   => ['/user/user']
            ],
            [
                'label'   => 'User Comments',
                'url'     => ['/user/user-comment'],
                'visible' => AdminAccess::can('user_comment.view'),
            ],
            [
                'label'   => 'User Notifications',
                'url'     => ['/user/user-notification'],
                'visible' => AdminAccess::can('user.view'),
            ],
            [
                'label'   => 'User Subscriptions',
                'url'     => ['/user/user-push-subscription'],
                'visible' => AdminAccess::can('user.view'),
            ],
            [
                'label'   => 'Login logs',
                'url'     => ['/user/user/logins'],
                'visible' => AdminAccess::can('user.view'),
            ],
            [
                'label'   => 'Users Tax Info',
                'visible' => AdminAccess::can('usertaxinfo.view'),
                'url'     => ['/user/user-tax-info']
            ],
            [
                'label'   => 'User Account Requests',
                'url'     => ['user/user-request/index'],
                'visible' => AdminAccess::can('user_request.view'),
            ],
            [
                'label' => 'Invalid emails',
                'url'   => ['/user/invalid-email']
            ],
        ];
    }

    /**
     * Return menu for support module
     *
     * @return array
     */
    private function getSupportMenu()
    {
        return [
            [
                'label' => 'Active issues',
                'url'   => ['/support/active-topics/index'],
                'icon'  => 'fa fa-ticket',
            ],
            [
                'label' => 'Archived issues',
                'url'   => ['/support/resolved-topics/index'],
                'icon'  => 'fa fa-archive',
            ],
            [
                'label'   => 'Message reports',
                'url'     => ['/support/message-reports'],
                'icon'    => 'fa fa-hand-peace-o',
                'visible' => AdminAccess::can('support.message_reports'),
            ],
            [
                'label'   => 'Message bot',
                'url'     => ['/support/message-bot'],
                'icon'    => 'fa fa-envelope-open-o',
            ]
        ];
    }

    private function getAffiliateMenu()
    {
        return [
            [
                'label'   => 'Api systems',
                'url'     => ['/site/api-external-system'],
                'icon'    => 'fa  fa-random',
                'visible' => AdminAccess::can('api_systems.settings'),
            ],
            [
                'label' => 'Affiliate sources',
                'url'   => ['/affiliate/affiliate-source'],
                'icon'  => 'fa fa-link',
                'visible' => AdminAccess::can('affiliate.view'),
            ],
            [
                'label' => 'Affiliate awards',
                'url'   => ['/affiliate/affiliate-award'],
                'icon'  => 'fa fa-link',
                'visible' => AdminAccess::can('affiliate.view'),
            ],
        ];
    }

    private function getProductsMenu()
    {
        return [
            [
                'label'   => 'Products',
                'url'     => ['/product/product'],
                'icon'    => 'fa fa-product-hunt',
                'visible' => AdminAccess::can('product.view'),
            ],
            [
                'label'   => 'Product categories',
                'url'     => ['/product/product-category'],
                'icon'    => 'fa fa-sitemap',
                'visible' => AdminAccess::can('store_categories.view'),
                'items' => [
                    [
                        'label' => 'Product categories',
                        'url' => ['/product/product-category'],
                        'icon' => 'fa fa-sitemap',
                        'visible' => AdminAccess::can('product.view'),
                    ],
                    [
                        'label' => 'Category keywords',
                        'url' => ['/product/product-category-keywords'],
                        'icon' => 'fa fa-file-text-o ',
                        'visible' => AdminAccess::can('product.view'),
                    ]
                ]
            ],
            [
                'label' => 'Dynamic Fields',
                'url' => ['/product/fields/dynamic-field'],
                'icon' => 'fa fa-tasks',
                'visible' => AdminAccess::can('dynamic_field.view'),

                'items'=> [
                    [
                        'label' => 'Dynamic Fields',
                        'url' => ['/product/fields/dynamic-field'],
                        'icon' => 'fa fa-tasks',
                    ],
                    [
                        'label' => 'Fields Categories',
                        'url' => ['/product/fields/dynamic-field-category'],
                        'icon' => 'fa fa-tasks',
                        'visible' => AdminAccess::can('dynamic_field.view')
                    ]
                ]
            ],
            [
                'label'   => 'Product main page',
                'icon'    => 'fa fa-sitemap',
                'visible' => AdminAccess::can('store_categories.view'),
                'items'   => [
                    [
                        'label' => 'Product cards',
                        'url' => ['/product/product-main-card'],
                        'icon' => 'fa fa-tasks',
                        'visible' => AdminAccess::can('store_categories.view'),
                    ],
                    [
                        'label' => 'Product categories',
                        'url' => ['/product/product-main-category'],
                        'icon' => 'fa fa-tasks',
                        'visible' => AdminAccess::can('store_categories.view'),
                    ],
                    [
                        'label' => 'Product sliders',
                        'url' => ['/product/product-main-slider'],
                        'icon' => 'fa fa-tasks',
                        'visible' => AdminAccess::can('store_categories.view'),
                    ],
                    [
                        'label' => 'Product promo bar',
                        'url' => ['/product/promo-bar/update?id=1'],
                        'icon' => 'fa fa-tasks',
                        'visible' => AdminAccess::can('store_categories.view'),
                    ]
                ]
            ],
        ];
    }
}
