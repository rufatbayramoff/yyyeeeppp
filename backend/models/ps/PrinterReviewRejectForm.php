<?php
/**
 * User: nabi
 */

namespace backend\models\ps;


use common\components\exceptions\AssertHelper;
use common\components\reject\BaseRejectForm;
use common\models\ModerLog;
use common\models\PrinterReview;
use common\models\SystemReject;
use yii\helpers\Json;

class PrinterReviewRejectForm extends BaseRejectForm
{
    private $user;
    /**
     * Save status and create log item
     *
     * @param $id
     * @param $userId
     */
    public function saveRejectStatus(PrinterReview $printerReview)
    {
        $printerReview->status = PrinterReview::STATUS_REJECTED;
        AssertHelper::assert($printerReview->save(false));

        $model = new \common\models\ModerLog();
        $model->user_id = $this->user->id;
        $model->created_at = dbexpr('NOW()');
        $model->started_at = dbexpr('NOW()');
        $model->action = PrinterReview::STATUS_REJECTED;
        $model->result = Json::encode(['reason'=>$this->getReason(), 'descr'=> $this->getComment()]);
        $model->object_type = ModerLog::TYPE_PRINT_REVIEW;
        $model->object_id = $printerReview->id;
        AssertHelper::assertSave($model);
        return $model;
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::PRINTER_REVIEW_REJECT;
    }

    public function setUser($currentUser)
    {
        $this->user = $currentUser;
    }
}