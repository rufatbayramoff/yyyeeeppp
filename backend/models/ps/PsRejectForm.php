<?php

namespace backend\models\ps;

use common\components\reject\BaseRejectForm;
use \common\models\Ps;
use common\models\SystemReject;
use yii\helpers\Json;

/**
 * PS Reject Form
 * Display in popup window
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class PsRejectForm extends BaseRejectForm
{
    /**
     * Save status and create log item
     * @param $id
     * @param $userId
     */
    public function saveRejectStatus($id, $userId)
    {
        $model = Ps::findByPk($id);
        $model->setStatus($id, $userId, Ps::MSTATUS_REJECTED, JSON::encode(['reason' => $this->reasonId, 'descr' => $this->reasonDescription]));
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::PS_REJECT;
    }
}