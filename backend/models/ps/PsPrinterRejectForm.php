<?php

namespace backend\models\ps;

use common\components\reject\BaseRejectForm;
use common\models\CompanyService;
use \common\models\PsPrinter;
use common\models\SystemReject;

/**
 * PS Printer Reject Form
 * Display in popup window
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class PsPrinterRejectForm extends BaseRejectForm
{
    /**
     * @param $id
     * @param $userId
     */
    public function saveRejectStatus(int $id, int $userId)
    {
        $model = PsPrinter::findByPk($id);

        $log = $model->setStatus($id, $userId, CompanyService::MODERATOR_STATUS_REJECTED);
        $log->result = json_encode(['reason' => $this->reasonId, 'descr' => $this->reasonDescription]);
        $log->safeSave();
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::PS_PRINTER_REJECT;
    }
}