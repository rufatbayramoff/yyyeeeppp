<?php

namespace backend\models\ps;

use common\components\reject\BaseRejectForm;
use \common\models\Ps;
use common\models\SystemReject;

/**
 * PS Banned Form
 * Display in popup window
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class PsBannedForm extends BaseRejectForm
{
    /**
     * @param $id
     * @param $userId
     */
    public function saveBannedStatus($id, $userId)
    {
        $model = Ps::findByPk($id);
        $data = $model->setStatus($id, $userId, Ps::MSTATUS_BANNED);

        $data->result = ['reason' => $this->reasonId, 'descr' => $this->reasonDescription];
        $data->save();
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::PS_BANNED;
    }
}