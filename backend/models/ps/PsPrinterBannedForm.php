<?php

namespace backend\models\ps;

use common\components\reject\BaseRejectForm;
use \common\models\PsPrinter;
use common\models\SystemReject;

/**
 * PS Printer Banned Form
 * Display in popup window
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class PsPrinterBannedForm extends BaseRejectForm
{
    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::PS_PRINTER_BANNED;
    }
}