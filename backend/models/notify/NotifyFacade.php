<?php namespace backend\models\notify;

use backend\models\search\CompanyServiceSearch;
use backend\models\search\ProductSearch;
use backend\models\search\StoreOrderSearch;
use backend\models\search\SystemLangMessageSearch;
use common\components\ps\material\PsColorPriceExceedSpecification;
use common\models\CompanyService;
use common\models\Model3d;
use common\models\MsgReport;
use common\models\PaymentBankInvoice;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\Preorder;
use common\models\PrinterReview;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\PsPrinterTest;
use common\models\query\StoreOrderQuery;
use common\models\SeoPage;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderReview;
use common\modules\product\interfaces\ProductInterface;

/**
 * NotifyFacade
 *
 * @TODO replace with NotifyService - where notifiers can be added,
 *       notifiers must extends BaseModelSearch  model (for example StoreUnitSearch)
 *       with given params. NotifyItem::getNotifyDecorations must be updated for service usage,
 *      params in getNotifyDecorations function should include search params, caching and etc.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class NotifyFacade
{

    public static function getNotifies()
    {
        $data = false; //app('cache')->get('back.notify.data'); // 45 secs
        if (!$data) {
            $data = [
                self::getStoreUnitsToModerate(),
                self::getPsToModerate(),
                self::getPsPrintersToModerate(),
                self::getServicesToModerate(),
                self::getProductsToModerate(),
                self::getTaxInfoToModerate(),
                self::getSupportActiveTopics(),
                self::getOrdersRefundRequest(),
                self::getNewUserRequests(),
                self::getPreorderMore300(),
                self::getPrinterTestings(),
                self::getPrinterChangeOrders(), // get orders where printer change is required
                self::getPrintedOrders(),
                self::getPsColorPriceExceed(),
                self::getNewMessageReports(),
                self::getNewOrderReviews(),
                self::getOrderNewReviewAnswers(),
                self::getTestOrders(),
                self::getSeoPageInactive(),
                self::getNoTranslations('ru', NotifyItem::TYPE_TO_TRANSLATE_RU),
                self::getNoTranslations('de', NotifyItem::TYPE_TO_TRANSLATE_DE),
                self::getNoTranslations('zh-CN', NotifyItem::TYPE_TO_TRANSLATE_CN),
                self::getNoTranslations('ja', NotifyItem::TYPE_TO_TRANSLATE_JA),
                self::getNoTranslations('fr', NotifyItem::TYPE_TO_TRANSLATE_FR),
                self::getTranslateToCheck(),
                self::getPrintReviewNew(),
                self::getBankInvoice(),
                self::getBankPayoutInvoice()

            ];
            app('cache')->set('back.notify.data', $data, 60); // 1 minutes
        }
        $notifies = self::buildNotifyItems($data);
        return $notifies;
    }

    /**
     *
     * @param array $data
     * @return ModeratorNotify[]
     */
    private static function buildNotifyItems($data)
    {
        $result = [];
        foreach ($data as $row) {
            if (empty($row) or empty($row['count'])) {
                continue;
            }
            $row['class'] = NotifyItem::className();
            $result[]     = \Yii::createObject($row);
        }
        return $result;
    }

    /**
     *
     * @return array
     */
    private static function getStoreUnitsToModerate()
    {
        $count = Model3d::find()
            ->productStatus([ProductInterface::STATUS_PUBLISH_PENDING, ProductInterface::STATUS_PUBLISHED_UPDATED])
            ->active()
            ->count();

        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_STORE_UNIT
        ];
    }

    /**
     * Preorders with price more 300 dollars
     *
     * @return array
     */
    private static function getPreorderMore300()
    {
        $count = Preorder::find()->alarmPreorder300()->count();
        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_PREORDER300
        ];
    }

    private static function getNewUserRequests()
    {
        $count = \common\models\UserRequest::find()->where(['status' => 'new'])->count();
        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_USER_REQUESTS
        ];
    }

    private static function getPsToModerate()
    {
        $count = \common\models\Ps::find()->where(['moderator_status' => [Ps::MSTATUS_NEW, Ps::MSTATUS_CHECKING, Ps::MSTATUS_UPDATED]])->count();
        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_NEW_PS
        ];
    }

    private static function getPsPrintersToModerate()
    {
        $count = \common\models\PsPrinter::find()
            ->leftJoin('company_service', 'company_service.ps_printer_id=ps_printer.id and company_service.type="printer"')
            ->where(['company_service.moderator_status' => CompanyService::pendingModerationStatusList()])
            ->andWhere(['company_service.is_deleted' => 0])
            ->count();

        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_NEW_PRINTER
        ];
    }

    private static function getServicesToModerate()
    {
        $search                  = new CompanyServiceSearch();
        $search->withoutPrinters = true;
        $count                   = $search->search(['CompanyServiceSearch' => ['notModerated' => '1']])->getTotalCount();


        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_NEW_SERVICE
        ];
    }

    private static function getTaxInfoToModerate()
    {
        $search = new \backend\models\search\UserTaxInfoSearch();
        $count  = $search->search(['UserTaxInfoSearch' => ['status' => 'submitted']])->getTotalCount();
        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_NEW_TAX
        ];
    }

    private static function getSupportActiveTopics()
    {
        $userAdmin = app('user')->getIdentity();
        $total     = \backend\modules\support\components\SupportTopicFacade::getOpenedTopicsCount($userAdmin);
        $unhold    = \backend\modules\support\components\SupportTopicFacade::getUnholdTopicsCount();
        return [
            'count' => $total + $unhold,
            'type'  => NotifyItem::TYPE_NEW_SUPPORT
        ];
    }

    private static function getNewUnpaidOrders()
    {
        $count = StoreOrder::find()
            ->notTest()
            ->inProcess()
            ->forPaymentStatuses(PaymentInvoice::STATUS_NEW)
            ->forCurrentAttempStatus(StoreOrderAttemp::STATUS_NEW)
            ->count();

        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_NEW_UNPAID_ORDER
        ];
    }

    private static function getNewOrders()
    {
        $count = StoreOrder::find()
            ->notTest()
            ->inProcess()
            ->forCurrentAttempStatus(StoreOrderAttemp::STATUS_NEW)
            ->count();

        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_NEW_ORDER
        ];
    }

    /**
     * @return array
     */
    private static function getTestOrders()
    {
        $count = StoreOrderAttemp::find()
            ->joinWith(['order' => function (StoreOrderQuery $query) {
                $query->onlyTest();
            }], false)
            ->inStatus(StoreOrderAttemp::STATUS_PRINTED)
            ->count();

        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_TEST_ORDER
        ];
    }

    private static function getSeoPageInactive()
    {
        $count = SeoPage::find()->where(['is_active' => 0])->count();
        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_SEO_INACTIVE
        ];
    }

    /**
     * @return array
     */
    private static function getPrinterTestings()
    {
        return [
            'count' => PsPrinterTest::find()->newTests()->count(),
            'type'  => NotifyItem::TYPE_PRINTER_TEST
        ];
    }

    /**
     * order printer change required
     * @return array
     */
    private static function getPrinterChangeOrders()
    {
        $count = StoreOrder::find()->inChangePrinter()->count();
        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_ORDER_CHANGE_PRINTER
        ];
    }

    /**
     * @return array
     */
    private static function getPrintedOrders()
    {
        $count = StoreOrderAttemp::find()
            ->notTest()
            ->inStatus(StoreOrderAttemp::STATUS_PRINTED)
            ->count();

        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_ORDER_PRINTED
        ];
    }

    /**
     * @return array
     */
    private static function getPsColorPriceExceed()
    {
        /** @var PsColorPriceExceedSpecification $resolver */
        $resolver = \Yii::createObject(PsColorPriceExceedSpecification::class);
        return [
            'count' => $resolver->satisfiedCount(),
            'type'  => NotifyItem::TYPE_PS_COLOR_PRICE_EXCEED
        ];
    }

    /**
     * @param bool $lang
     * @param string $type
     * @return array
     */
    private static function getNoTranslations($lang = false, $type = NotifyItem::TYPE_TO_TRANSLATE)
    {
        $version  = app('cache')->get('git_version', 1);
        $cacheKey = 'notranslationcount_' . $version . $lang . $type;
        if (app('cache')->exists($cacheKey)) {
            return [
                'count' => app('cache')->get($cacheKey),
                'type'  => $type
            ];
        }
        $search       = new SystemLangMessageSearch();
        $searchParams = ['SystemLangMessageSearch' => ['not_found_in_source' => 0, 'translation' => '-', 'is_checked' => 1]];
        if ($lang) {
            $searchParams['SystemLangMessageSearch']['language'] = $lang;
        }

        $count = $search->search($searchParams)->getTotalCount();
        app('cache')->set($cacheKey, $count, 3600);
        return [
            'count' => $count,
            'type'  => $type
        ];
    }

    private static function getTranslateToCheck()
    {
        $version  = app('cache')->get('git_version', 1);
        $cacheKey = 'getTranslateToCheck' . $version;
        if (app('cache')->exists($cacheKey)) {
            return [
                'count' => app('cache')->get($cacheKey),
                'type'  => NotifyItem::TYPE_TRANSLATE_CHECK
            ];
        }
        $search       = new SystemLangMessageSearch();
        $searchParams = ['SystemLangMessageSearch' => ['is_checked' => 0]];
        $count        = $search->search($searchParams)->getTotalCount();
        app('cache')->set($cacheKey, $count, 3600);
        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_TRANSLATE_CHECK
        ];
    }

    /**
     * @return array
     */
    private static function getNewMessageReports()
    {
        return [
            'count' => MsgReport::find()->onlyNew()->count(),
            'type'  => NotifyItem::TYPE_NEW_MESSAGE_REPORTS,
        ];
    }

    /**
     * @return array
     */
    private static function getNewOrderReviews()
    {
        return [
            'count' => StoreOrderReview::find()->notModreated()->count(),
            'type'  => NotifyItem::TYPE_ORDER_REVIEW,
        ];
    }

    /**
     * @return array
     */
    private static function getOrderNewReviewAnswers()
    {
        return [
            'count' => StoreOrderReview::find()->newAnswer()->count(),
            'type'  => NotifyItem::TYPE_ORDER_REVIEW_ANSWERS,
        ];
    }

    /**
     * @return array
     */
    private static function getBankInvoice()
    {
        return [
            'count' => PaymentBankInvoice::find()->where(['status' => PaymentBankInvoice::STATUS_NEW])->count(),
            'type'  => NotifyItem::TYPE_BANK_INVOICE,
        ];
    }

    private static function getBankPayoutInvoice()
    {
        return [
            'count' => PaymentTransaction::find()->bankPayoutAuthorized()->count(),
            'type'  => NotifyItem::TYPE_BANK_PAYOUT,
        ];
    }

    private static function getOrdersRefundRequest()
    {

        $search       = new StoreOrderSearch();
        $searchParams = ['StoreOrderSearch' => ['order_status' => 'refund_requests']];
        $count        = $search->search($searchParams)->getTotalCount();
        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_ORDERS_REFUND_REQUEST
        ];
    }

    private static function getProductsToModerate()
    {
        $search       = new ProductSearch();
        $searchParams = [$search->formName() => ['needModeration' => 1]];
        $count        = $search->search($searchParams)->getTotalCount();
        return [
            'count' => $count,
            'type'  => NotifyItem::TYPE_NEW_PRODUCTS
        ];
    }

    private static function getPrintReviewNew()
    {
        return [
            'count' => PrinterReview::find()->where(['status' => PrinterReview::STATUS_NEW])->count(),
            'type'  => NotifyItem::TYPE_PRINTER_REVIEW,
        ];
    }
}
