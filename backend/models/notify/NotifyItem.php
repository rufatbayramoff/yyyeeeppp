<?php namespace backend\models\notify;

use common\models\PaymentInvoice;
use common\models\Product;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderReview;

/**
 * NotifyItem
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class NotifyItem extends \yii\base\BaseObject implements \ArrayAccess
{

    //put your code here


    public $title;
    public $count;
    public $type;
    public $icon;
    public $url;

    const TYPE_STORE_UNIT = 'store_unit';
    const TYPE_NEW_UNPAID_ORDER = 'new_unpaid_order';
    const TYPE_NEW_PAID_ORDER = 'new_paid_order';
    const TYPE_ORDERS_REFUND_REQUEST = 'orders_refund_request';
    const TYPE_NEW_ORDER = 'new_order';
    const TYPE_EXPIRED_ORDER = 'expired_order';
    const TYPE_NEW_PS = 'new_ps';
    const TYPE_NEW_PRINTER = 'new_printer';
    const TYPE_NEW_SERVICE = 'new_cnc_machine';
    const TYPE_NEW_PRODUCTS = 'new_products';
    const TYPE_PREORDER300 = 'preorder300';

    const TYPE_NEW_TAX = 'new_tax';
    const TYPE_NEW_SUPPORT = 'new_support';
    const TYPE_USER_REQUESTS = 'user_requests';
    const TYPE_PRINTER_TEST = 'printer_test';
    const TYPE_ORDER_CHANGE_PRINTER = 'order_printer_change';
    const TYPE_ORDER_PRINTED = 'order_printed';
    const TYPE_PS_COLOR_PRICE_EXCEED = 'ps_color_price_exced';
    const TYPE_TO_TRANSLATE = 'to_translate';
    const TYPE_NEW_MESSAGE_REPORTS = 'new_messages-reports';
    const TYPE_ORDER_REVIEW = 'order_reviews';
    public const TYPE_ORDER_REVIEW_ANSWERS = 'order_review_answers';
    public const TYPE_BANK_INVOICE = 'bank_invoice';
    public const TYPE_BANK_PAYOUT = 'bank_payout';
    const TYPE_TEST_ORDER = 'ps_test_orders';

    const TYPE_TO_TRANSLATE_JA = 'to_translate_ja';
    const TYPE_TO_TRANSLATE_FR = 'to_translate_fr';
    const TYPE_TO_TRANSLATE_CN = 'to_translate_cn';
    const TYPE_TO_TRANSLATE_DE = 'to_translate_de';
    const TYPE_TO_TRANSLATE_RU = 'to_translate_ru';
    const TYPE_TRANSLATE_CHECK = 'translate_check';

    const TYPE_SEO_INACTIVE = 'seo_inactive';
    const TYPE_PRINTER_REVIEW = 'printer_review';

    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->decorateNotifyByType($this->type);
    }

    /**
     * 
     * @param string $type
     * @throws \yii\base\UserException
     */
    private function decorateNotifyByType($type)
    {
        $notifyDecorators = $this->getNotifyDecorations();
        if (!isset($notifyDecorators[$type])) {
            throw new \yii\base\UserException("Unsupported notify type $type");
        }
        $notifyDecorator = $notifyDecorators[$type];
        // format
        $this->title = $this->formatTitle($notifyDecorator['title']);
        $this->url = $notifyDecorator['url'];
        $this->icon = $notifyDecorator['icon'];
    }

    private function formatTitle($title)
    {
        return sprintf("<b>%d</b> %s", $this->count, $title);
    }

    public static function isTranslate($v)
    {
        if (in_array($v->type, [
            self::TYPE_TO_TRANSLATE_JA,
            self::TYPE_TO_TRANSLATE_FR, self::TYPE_TO_TRANSLATE_CN, self::TYPE_TO_TRANSLATE_DE, self::TYPE_TO_TRANSLATE_RU,
            self::TYPE_TRANSLATE_CHECK,
            self::TYPE_SEO_INACTIVE])){
            return true;
        }
        return false;

    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return $this->$offset !== null;
    }

    /**
     * @param mixed $offset 
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    /**
     * @param mixed $offset 
     */
    public function offsetUnset($offset)
    {
        $this->$offset = null;
    }

    /**
     *
     * @return array
     */
    public function getNotifyDecorations()
    {
        return [
            // Store units
            self::TYPE_STORE_UNIT => [
                'title' => 'new store units',
                'icon' => 'fa fa-file-powerpoint-o text-green',
                'url' => ['/store/store-unit', 'StoreUnitSearch[toModerate]' =>  ['1']]
            ],
            // Orders
            self::TYPE_NEW_UNPAID_ORDER => [
                'title' => 'new Unpaid Orders',
                'icon' => 'fa fa-shopping-cart text-orange',
                'url' => ['/store/store-order',
                    'StoreOrderSearch[order_status]' => StoreOrderAttemp::STATUS_NEW,
                    'StoreOrderSearch[payment_status]' => PaymentInvoice::STATUS_NEW]
            ],
            self::TYPE_NEW_PAID_ORDER => [
                'title' => 'New Orders',
                'icon' => 'fa fa-shopping-cart text-green',
                'url' => ['/store/store-order',
                    'StoreOrderSearch[order_status]' => StoreOrderAttemp::STATUS_NEW]
            ],
            self::TYPE_NEW_ORDER => [
                'title' => 'New Orders',
                'icon' => 'fa fa-shopping-cart text-green',
                'url' => ['/store/store-order',
                    'StoreOrderSearch[order_status]' => StoreOrderAttemp::STATUS_NEW]
            ],
            self::TYPE_ORDERS_REFUND_REQUEST => [
                'title' => 'Order Refund Requests',
                'icon' => 'fa fa-credit-card text-red',
                'url' => ['/store/store-order',
                    'StoreOrderSearch[order_status]' => 'refund_requests']
            ],
            self::TYPE_EXPIRED_ORDER => [
                'title' => 'expired Orders',
                'icon' => 'fa fa-shopping-cart text-red',
                'url' => ['/store/store-order', 'StoreOrderSearch[status]' => 'expired']
            ],
            self::TYPE_PREORDER300 => [
                'title' => 'Preoreder 300 dollars',
                'icon' => 'fa fa-shopping-cart text-green',
                'url' => ['/store/preorder', 'PreorderSearch[preorders300]' => 1]
            ],
            // PS & Printers
            self::TYPE_NEW_PS      => [
                'title' => 'new PrintServices',
                'icon' => 'fa fa-print text-blue',
                'url' => ['/ps/ps', 'PsSearch[newForModerate]' => 1]
            ],
            self::TYPE_NEW_PRINTER => [
                'title' => 'new PS Printers',
                'icon' => 'fa fa-balance-scale text-green',
                'url' => ['/ps/ps-printer', 'PsPrinterSearch[moderator_status]' => 'waiting']
            ],
            self::TYPE_NEW_SERVICE => [
                'title' => 'New Company Services',
                'icon' => 'fa fa-balance-scale text-green',
                'url' => ['/company/company-service', 'CompanyServiceSearch[notModerated]' => 1]
            ],
            self::TYPE_NEW_PRODUCTS => [
                'title' => 'New Products',
                'icon' => 'fa fa-balance-scale text-green',
                'url' => ['/product/product', 'productSearch' => ['needModeration'=>1]]
            ],
            self::TYPE_PRINTER_REVIEW => [
                'title' => 'New Reviews',
                'icon' => 'fa fa-star text-green',
                'url' => ['/ps/printer-review', 'PrinterReview[status]' => 'new']
            ],

            // User tax & support
            self::TYPE_NEW_TAX     => [
                'title' => 'new user taxes',
                'icon' => 'fa fa-user text-red',
                'url' => ['/user/user-tax-info', 'UserTaxInfoSearch'=>['status'=>'submitted']]
            ],
            self::TYPE_NEW_SUPPORT => [
                'title' => 'new Support Messages',
                'icon' => 'fa fa-warning text-yellow',
                'url' => ['/support/active-topics/index']
            ],
            self::TYPE_USER_REQUESTS => [
                'title' => 'new user requests',
                'icon' => 'fa fa-user text-red',
                'url' => ['/user/user-request', 'UserRequestSearch'=>['status'=>'new']]
            ],
            self::TYPE_PRINTER_TEST=> [
                'title' => 'new printer tests',
                'icon' => 'fa fa-thumbs-up text-red',
                'url' => ['/ps/ps-printer-test']
            ],

            self::TYPE_ORDER_CHANGE_PRINTER => [
                'title' => 'Change Order Printer',
                'icon' => 'fa fa-shopping-cart text-red',
                'url' => ['/store/store-order',
                    'StoreOrderSearch[changePrinter]' => 1
                ]
            ],

            self::TYPE_ORDER_PRINTED => [
                'title' => 'Verify Printed Order',
                'icon' => 'fa fa-cubes text-green',
                'url' => ['/store/store-order',
                    'StoreOrderSearch[order_status]' => [StoreOrderAttemp::STATUS_PRINTED]]
            ],

            self::TYPE_PS_COLOR_PRICE_EXCEED => [
                'title' => 'Printer color price exceed',
                'icon' => 'fa fa-warning text-yellow',
                'url' => ['/ps/ps-printer', 'PsPrinterSearch[priceExceed]' => 1]
            ],

            self::TYPE_ORDER_REVIEW => [
                'title' => 'Order reviews',
                'icon' => 'fa fa-star text-yellow',
                'url' => ['/store/reviews', 'StoreOrderReviewSearch[status]' => StoreOrderReview::STATUS_NEW]
            ],

            self::TYPE_ORDER_REVIEW_ANSWERS => [
                'title' => 'Order review answers',
                'icon' => 'fa fa-star text-yellow',
                'url' => ['/store/reviews', 'StoreOrderReviewSearch[verified_answer_status]' => StoreOrderReview::ANSWER_STATUS_NEW]
            ],

            self::TYPE_TO_TRANSLATE_CN => [
                'title' => 'To Translate CN',
                'icon' => 'fa fa-flag text-yellow',
                'url' => ['/site/lang', 'SystemLangMessageSearch[not_found_in_source]' => 0,  'SystemLangMessageSearch[is_checked]' => 1, 'SystemLangMessageSearch[translation]' => '-',  'SystemLangMessageSearch[language]' => 'zh-CN', 'sort'=>'-id']
            ],

            self::TYPE_TO_TRANSLATE_DE => [
                'title' => 'To Translate DE',
                'icon' => 'fa fa-flag text-yellow',
                'url' => ['/site/lang', 'SystemLangMessageSearch[not_found_in_source]' => 0, 'SystemLangMessageSearch[is_checked]' => 1,  'SystemLangMessageSearch[translation]' => '-',  'SystemLangMessageSearch[language]' => 'de', 'sort'=>'-id']
            ],

            self::TYPE_TO_TRANSLATE_FR => [
                'title' => 'To Translate FR',
                'icon' => 'fa fa-flag text-yellow',
                'url' => ['/site/lang', 'SystemLangMessageSearch[not_found_in_source]' => 0, 'SystemLangMessageSearch[is_checked]' => 1, 'SystemLangMessageSearch[translation]' => '-',  'SystemLangMessageSearch[language]' => 'fr', 'sort'=>'-id']
            ],

            self::TYPE_TO_TRANSLATE_JA => [
                'title' => 'To Translate JA',
                'icon' => 'fa fa-flag text-yellow',
                'url' => ['/site/lang', 'SystemLangMessageSearch[not_found_in_source]' => 0, 'SystemLangMessageSearch[is_checked]' => 1, 'SystemLangMessageSearch[translation]' => '-',  'SystemLangMessageSearch[language]' => 'ja', 'sort'=>'-id']
            ],

            self::TYPE_TO_TRANSLATE_RU => [
                'title' => 'To Translate RU',
                'icon' => 'fa fa-flag text-yellow',
                'url' => ['/site/lang', 'SystemLangMessageSearch[not_found_in_source]' => 0,
                                        'SystemLangMessageSearch[is_checked]' => 1,
                                        'SystemLangMessageSearch[translation]' => '-',  'SystemLangMessageSearch[language]' => 'ru', 'sort'=>'-id']
            ],
            self::TYPE_TO_TRANSLATE => [
                'title' => 'To Translate',
                'icon' => 'fa fa-flag text-yellow',
                'url' => ['/site/lang', 'SystemLangMessageSearch[not_found_in_source]' => 0, 'SystemLangMessageSearch[is_checked]' => 1, 'SystemLangMessageSearch[translation]' => '-', 'sort'=>'-id']
            ],
            self::TYPE_TRANSLATE_CHECK => [
                'title' => 'Translates To Check',
                'icon' => 'fa fa-flag text-red',
                'url' => ['/site/lang', 'SystemLangMessageSearch[is_checked]' => 0, 'sort'=>'-id']
            ],
            self::TYPE_NEW_MESSAGE_REPORTS => [
                'title' => 'New message reports',
                'icon' => 'fa fa-flag text-yellow',
                'url' => ['support/message-reports']
            ],
            self::TYPE_TEST_ORDER => [
                'title' => 'Printed test orders',
                'icon' => 'fa fa-shopping-cart text-green',
                'url' => ['/ps/test-order', 'StoreOrderSearch[order_status]' => StoreOrderAttemp::STATUS_PRINTED]
            ],
            self::TYPE_SEO_INACTIVE => [
                'title' => 'SEO Inactive',
                'icon' => 'fa fa-search text-red',
                'url' => ['/site/seo-page', 'SeoPageSearch[is_active]' => 0]
            ],
            self::TYPE_BANK_INVOICE => [
                'title' => 'Bank invoice',
                'icon' => 'fa fa-money text-red',
                'url' => ['/store/payment-bank-invoice', 'PaymentBankInvoiceSearch[status]' => 'new']
            ],
            self::TYPE_BANK_PAYOUT => [
                'title' => 'Bank payout',
                'icon' => 'fa fa-money text-red',
                'url' => ['/store/payment-bank-payout', 'PaymentTransactionSearch[status]' => 'authorized']
            ],

        ];
    }
}