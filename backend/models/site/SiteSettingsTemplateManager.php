<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.02.17
 * Time: 15:59
 */

namespace backend\models\site;

use backend\models\site\siteSettingsTemplates\JsonSchemeInterface;
use Yii;
use \Exception;

class SiteSettingsTemplateManager
{
    public static function getSchema($group, $key)
    {
        $camelCaseKey = str_replace('_', '', ucwords($key, '_'));
        $className = '\backend\models\site\siteSettingsTemplates\Scheme' . ucfirst($group) . $camelCaseKey;
        $templateObj = null;
        try {
            /**
             * @var $templateObj JsonSchemeInterface
             */
            $templateObj = Yii::createObject($className);
        } catch (Exception $e) {

        }
        if ($templateObj) {
            return $templateObj->getScheme();
        } else {
            return [];
        }
    }
}