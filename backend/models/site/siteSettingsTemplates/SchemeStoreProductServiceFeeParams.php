<?php

namespace backend\models\site\siteSettingsTemplates;

class SchemeStoreProductServiceFeeParams implements JsonSchemeInterface
{
    /**
     * @return array
     */
    public function getScheme(): array
    {
        return
            [
                'title'      => 'Product service fee params',
                'type'       => 'object',
                'properties' => [
                    'A' => [
                        'type' => 'number'
                    ],
                    'B' => [
                        'type' => 'number'
                    ],
                    'C' => [
                        'type' => 'number'
                    ],
                    'minPercent' => [
                        'type' => 'number'
                    ],
                    'maxPercent' => [
                        'type' => 'number'
                    ]
                ],
            ];
    }
}