<?php

namespace backend\models\site\siteSettingsTemplates;

class SchemeFileAllowextension implements JsonSchemeInterface
{
    public function getScheme()
    {
        return
            [
                'title'  => 'Extensions',
                'type'   => 'array',
                'format' => 'table',
                'items'  => [
                    'type'  => 'string',
                    'title' => 'extension',
                ]
            ];
    }
}