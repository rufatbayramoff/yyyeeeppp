<?php
/**
 * Date: 14.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\models\site\siteSettingsTemplates;


interface JsonSchemeInterface
{
    /**
     * @return array
     */
    public function getScheme(): array;
}