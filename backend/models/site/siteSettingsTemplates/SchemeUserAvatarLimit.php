<?php

namespace backend\models\site\siteSettingsTemplates;

class SchemeUserAvatarLimit implements JsonSchemeInterface
{
    public function getScheme()
    {
        return
            [
                'title'      => 'Coefficients',
                'type'       => 'object',
                'properties' => [
                    'extension' => [
                        'type'   => 'array',
                        'format' => 'table',
                        'items'  => [
                            'type'  => 'string',
                            'title' => 'extension',
                        ]
                    ],
                    'size'      => [
                        'type' => 'integer',
                    ],
                    'width'     => [
                        'type' => 'integer',
                    ],
                    'height'    => [
                        'type' => 'integer',
                    ]
                ]
            ];
    }
}