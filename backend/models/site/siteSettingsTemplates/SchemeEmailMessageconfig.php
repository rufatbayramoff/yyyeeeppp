<?php

namespace backend\models\site\siteSettingsTemplates;

class SchemeEmailMessageconfig implements JsonSchemeInterface
{
    public function getScheme()
    {
        return
            [
                'title'      => 'Email message config',
                'type'       => 'object',
                'properties' => [
                    'title'   => [
                        'type'       => 'object',
                        'properties' => [
                            'append'  => [
                                'type' => 'string',
                            ],
                            'prepend' => [
                                'type' => 'string'
                            ]
                        ],

                    ],
                    'message' => [
                        'type'       => 'object',
                        'properties' => [
                            'footer' => [
                                'type' => 'string'
                            ]
                        ]
                    ]
                ],
            ];
    }
}