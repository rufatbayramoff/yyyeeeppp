<?php

namespace backend\models\site\siteSettingsTemplates;

class SchemePrinterSortCoefficients implements JsonSchemeInterface
{
    public function getScheme(): array
    {
        return
            [
                'title'      => 'Coefficients',
                'type'       => 'object',
                'properties' => [
                    'rating'        => [
                        'type'   => 'array',
                        'format' => 'table',
                        'items'  => [
                            'type'       => 'object',
                            'title'      => 'coefficient',
                            'properties' => [
                                'begin'       => [
                                    'type' => 'number',
                                ],
                                'end'         => [
                                    'type' => 'number',
                                ]
                                ,
                                'coefficient' => [
                                    'type' => 'number',
                                ]
                            ]
                        ]
                    ],
                    'certification' => [
                        'type'   => 'array',
                        'format' => 'table',
                        'items'  => [
                            'type'       => 'object',
                            'title'      => 'certification',
                            'properties' => [
                                'value'       => [
                                    'type'    => 'string',
                                    'default' => 'no'
                                ],
                                'coefficient' => [
                                    'type' => 'number',
                                ]
                            ]
                        ]
                    ],
                    'ordersCount'   => [
                        'type'   => 'array',
                        'format' => 'table',
                        'items'  => [
                            'type'       => 'object',
                            'title'      => 'coefficient',
                            'properties' => [
                                'begin'       => [
                                    'type' => 'number',
                                ],
                                'end'         => [
                                    'type' => 'number',
                                ]
                                ,
                                'coefficient' => [
                                    'type' => 'number',
                                ]
                            ]
                        ]
                    ],
                    'distance'      => [
                        'type'   => 'array',
                        'format' => 'table',
                        'items'  => [
                            'type'       => 'object',
                            'title'      => 'coefficient',
                            'properties' => [
                                'begin'       => [
                                    'type' => 'number',
                                ],
                                'end'         => [
                                    'type' => 'number',
                                ]
                                ,
                                'coefficient' => [
                                    'type' => 'number',
                                ]
                            ]
                        ]
                    ]
                ]

            ];
    }
}