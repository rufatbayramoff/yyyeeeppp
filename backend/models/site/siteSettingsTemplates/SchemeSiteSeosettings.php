<?php
/**
 * Date: 14.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\models\site\siteSettingsTemplates;


use common\modules\seo\services\SeoAutofillService;

class SchemeSiteSeosettings implements JsonSchemeInterface
{

    /**
     * @return array
     */
    public function getScheme()
    {

        $service = new SeoAutofillService();
        $placeholders = $service->getPlaceholders();
        $result = [];
        foreach($placeholders as $type=>$params){
            $result[] = "<b>$type</b>: " . implode(', ', $params);
        }
        return [
                'title'      => 'SEO Autofill Templates',
                'type'       => 'array',
                'description' => 'Template vars<br/>'  . implode('<br/> ', $result),
                'items' => [
                    'type' => 'object',
                    'title' => 'Page Type',
                    'properties' => [
                        'type' => [
                            'type' =>'string',
                            'enum' => $service->getTypes(),

                        ],
                        'title'      => [
                            'type' => 'string',
                            'description' => 'For example: %modelTitle% in %modelCategory% by %modelUser%'
                        ],
                        'meta_description'     => [
                            'type' => 'string',
                        ],
                        'meta_keywords'    => [
                            'type' => 'string',
                        ],
                        'header' => [
                            'type' => 'string',
                            'description' => 'H1 text'
                        ]
                    ],

                ],
            ];
    }
}