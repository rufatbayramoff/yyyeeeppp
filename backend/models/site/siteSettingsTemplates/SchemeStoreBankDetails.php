<?php
namespace backend\models\site\siteSettingsTemplates;

use lib\money\Currency;

class SchemeStoreBankDetails implements JsonSchemeInterface
{
    public function getScheme(): array
    {
        return
            [
                'title'      => 'Bank invoice bank details',
                'type'       => 'object',
                'properties' => [
                    Currency::USD   => [
                        'type' => 'string',
                        'format' => 'textarea'
                    ],
                    Currency::EUR => [
                        'type' => 'string',
                        'format' => 'textarea'
                    ],
                ],
            ];
    }
}