<?php

namespace backend\models\site\siteSettingsTemplates;

class SchemeUserTrustlevel implements JsonSchemeInterface
{
    public function getScheme()
    {
        return
            [
                'title' => 'Trust level',
                'type'  => 'array',
                'items' => [
                    'type'       => 'object',
                    'title'      => 'trust level',
                    'properties' => [
                        'title' => [
                            'type' => 'string'
                        ],
                        'color' => [
                            'type' => 'string',
                        ],
                        'code'  => [
                            'type' => 'string',
                        ]
                    ]
                ]
            ];
    }
}