<?php
/**
 * Date: 14.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\models\site\siteSettingsTemplates;


class SchemeSeoSettings implements JsonSchemeInterface
{

    /**
     * @return array
     */
    public function getScheme()
    {
        return [
                'title'      => 'SEO Settings config',
                'type'       => 'object',
                'properties' => [
                    'pageType'   => [
                        'type'       => 'object',
                        'properties' => [
                            'append'  => [
                                'type' => 'string',
                            ],
                            'prepend' => [
                                'type' => 'string'
                            ]
                        ],

                    ],
                    'size'      => [
                        'type' => 'integer',
                    ],
                    'width'     => [
                        'type' => 'integer',
                    ],
                    'height'    => [
                        'type' => 'integer',
                    ],
                    'message' => [
                        'type'       => 'object',
                        'properties' => [
                            'footer' => [
                                'type' => 'string'
                            ]
                        ]
                    ]
                ],
            ];
    }
}