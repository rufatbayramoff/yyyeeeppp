<?php

namespace backend\models\site\siteSettingsTemplates;

class SchemeUserDisallowusernames implements JsonSchemeInterface
{
    public function getScheme(): array
    {
        return
            [
                'title'  => 'User names',
                'type'   => 'array',
                'format' => 'table',
                'items'  => [
                    'type'  => 'string',
                    'title' => 'user name',
                ]
            ];
    }
}