<?php
namespace backend\models\site\siteSettingsTemplates;

class SchemeAffiliateSourceAward implements JsonSchemeInterface
{
    public function getScheme(): array
    {
        return
            [
                'title'      => 'Award config',
                'type'       => 'object',
                'properties' => [
                    'treatstock_fee_percent'   => [
                        'type' => 'string',
                    ],
                    'fixed' => [
                        'type' => 'string'
                    ],
                ],
            ];
    }
}