<?php

namespace backend\models\system\import;

use backend\models\system\ExcelImportForm;


/**
 * User: nabi
 */
class ImportPrinter extends ExcelImportForm
{
    public function insertRow($index, $row)
    {
        if(empty($row['have_unfixed_size'])){
            $row['have_unfixed_size'] = 0;
        }
        $row['images_json'] = json_decode($row['images_json']);
        $row['files_json'] = json_decode($row['files_json']);
        return parent::insertRow($index, $row);
    }
}