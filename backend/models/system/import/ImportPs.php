<?php
/**
 * User: nabi
 */

namespace backend\models\system\import;

use backend\models\system\ExcelImportForm;
use backend\modules\company\factories\CompanyFactory;
use common\models\populators\CompanyPopulator;
use common\models\Ps;
use common\models\User;
use yii\base\InvalidArgumentException;

/**
 * used to import new companies
 */
class ImportPs extends ImportCompany
{
}