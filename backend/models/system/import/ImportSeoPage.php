<?php

namespace backend\models\system\import;

use backend\models\system\ExcelImportForm;


/**
 * User: nabi
 */
class ImportSeoPage extends ExcelImportForm
{
    public function insertRow($index, $row)
    {
        if(empty($row['url'])){
            return sprintf("Row %d. URL is required. Skipped", $index);
        }
        return parent::insertRow($index, $row);
    }
}