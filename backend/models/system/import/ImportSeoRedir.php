<?php

namespace backend\models\system\import;

use backend\models\system\ExcelImportForm;


/**
 * User: nabi
 */
class ImportSeoRedir extends ExcelImportForm
{
    public function insertRow($index, $row)
    {
        if(empty($row['user_admin_id'])){
            $row['user_admin_id'] = app('user')->getId();
        }
        return parent::insertRow($index, $row);
    }
}