<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.06.18
 * Time: 16:03
 */

namespace backend\models\system\import;

use backend\models\system\ExcelImportForm;
use common\models\factories\CompanyFactory;
use common\models\factories\UserFactory;
use common\models\populators\CompanyPopulator;
use common\models\Ps;
use common\models\repositories\CompanyRepository;
use common\models\repositories\UserRepository;
use common\models\User;
use yii\base\InvalidArgumentException;

/**
 * used to import new companies
 */
class ImportCompany extends ExcelImportForm
{
    /** @var CompanyPopulator */
    public $companyPopulator;
    /** @var CompanyFactory */
    public $companyFactory;
    /** @var CompanyRepository */
    public $companyRepository;

    /** @var UserRepository */
    public $userRepository;
    /** @var UserFactory */
    public $userFactory;


    public function injectDependencies(
        CompanyPopulator $companyPopulator,
        CompanyFactory $companyFactory,
        CompanyRepository $companyRepository,
        UserRepository $userRepository,
        UserFactory $userFactory
    ) {
        $this->companyPopulator = $companyPopulator;
        $this->companyFactory = $companyFactory;
        $this->companyRepository = $companyRepository;
        $this->userRepository = $userRepository;
        $this->userFactory = $userFactory;
    }

    /**
     * @param $index
     * @param $row
     * @return bool|string
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \libphonenumber\NumberParseException
     */
    protected function importRow($index, $row)
    {
        $row = array_combine($this->xlsColumns, $row);
        // create user
        $user = $this->userFactory->createUserByEmail($row['email']);
        $user->status = User::STATUS_DRAFT_COMPANY;
        $company = $this->companyFactory->createCompanyByUser($user);
        $company->is_backend = 1;
        $companyRow = [
            $company->formName() => $row
        ];
        $this->companyPopulator->populate($company, $companyRow);
        $this->companyPopulator->populateProtectedAttributes($company, $companyRow);
        if ($company->phone) {
            $companyService = \Yii::createObject(\backend\modules\company\services\CompanyService::class);
            $companyService->processPhone($company);
        }
        // insert user with email first
        $validateUserResult = $user->validate() && $user->validateEmail();
        $validateUserProfileResult = $user->userProfile->validate();
        $validateCompanyResult = $company->validate();
        if ($validateUserResult && $validateCompanyResult && $validateUserProfileResult) {
            $this->userRepository->save($user);
            $this->companyRepository->saveCompany($company);
            return true;
        }
        if ($user->hasErrors()) {
            $this->importErrors[$index]['user'] = $user->getErrors();
        }
        if ($user->userProfile->hasErrors()) {
            $this->importErrors[$index]['userProfile'] = $user->userProfile->getErrors();
        }
        if ($company->hasErrors()) {
            $this->importErrors[$index]['company'] = $company->getErrors();
        }
        return false;
    }

    public function getTemplateFields()
    {
        return [
            'email' => ['required' => true],
            'title' => ['required' => true],
            'description',
            'phone_with_code',
            'country_iso'=> ['required' => true],
            'ownership',
            'total_employees',
            'year_established',
            'annual_turnover',
            'website',
            'facebook',
            'instagram',
            'twitter',
            'is_active',
            'location'
        ];
    }
}