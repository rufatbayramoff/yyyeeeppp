<?php

namespace backend\models\system\export;

use backend\modules\statistic\reports\BaseReportInterface;

/**
 * User: nabi
 */
class ExportPs implements BaseReportInterface
{
    public $columns = [
        'ps_id'            => 'PS ID',
        'ps_country'       => 'PS Country',
        'created_at'       => 'Created',
        'ps_title'         => 'PS Name',
        'user_link'        => 'User Link',
        'country'          => 'Country',
        'email'            => 'Email',
        'phone'            => 'Phone',
        'moderator_status' => 'Moderator Status',
        'user_id'          => 'User Id',
        'description'      => 'Description'
    ];
    public $items;

    public function getColumnsNames()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        if (!empty($this->items)) {
            return $this->items;
        }

        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);
        $userIds = join(',', $userIds);

        $sql = "select ps.id as ps_id, g2.iso_code as ps_country, ps.title as ps_title, ps.description, min(g1.iso_code) as country, user.email,
             ps.created_at, concat(ps.phone_code, '', ps.phone) as phone, ps.moderator_status,
             concat('https://www.treatstock.com/u/', '', user.username) as user_link, user.id as user_id
         from ps
         LEFT JOIN company_service psm ON psm.ps_id=ps.id
         LEFT JOIN ps_printer a ON psm.ps_printer_id=a.id
         LEFT JOIN user ON (user.id=ps.user_id)
        left join user_location u ON u.id=psm.location_id
        left join geo_country g1 ON g1.id=u.country_id
        left join geo_country g2 ON g2.id=ps.country_id 
        where 1 AND user.id NOT IN($userIds)
        group by ps.id";

        $this->items = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        return $this->items;
    }

    public function setParams(array $params)
    {
    }

    public static function create()
    {
        return new self;
    }
}