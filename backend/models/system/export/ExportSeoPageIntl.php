<?php
/**
 * User: nabi
 */

namespace backend\models\system\export;


use backend\models\search\SeoPageIntlSearch;
use backend\modules\statistic\reports\BaseReportInterface;

class ExportSeoPageIntl implements BaseReportInterface
{
    public $columns;
    public $items;

    public function __construct()
    {
        $exportModel = new SeoPageIntlSearch();
        $this->columns  = array_merge($exportModel->attributeLabels(), [
            'url' => '_URL',
            'en_title' => '_Title'
        ]);
    }

    public function getColumnsNames()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        if(!empty($this->items)){
            return $this->items;
        }
        $sql = "select a.id, IFNULL(a.model_id, b.id) as model_id, 
                  a.lang_iso, a.title, a.header, a.meta_description, a.meta_keywords,
                   a.header_text, a.footer_text, a.is_active, a.updated_at,
                    b.url, b.title as en_title 
                from seo_page b
                left join seo_page_intl a on b.id = a.model_id;";

        $this->items = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        return $this->items;
    }

    public function setParams(array $params)
    {
    }

    public static function create()
    {
        return new self;
    }
}