<?php
/**
 * User: nabi
 */

namespace backend\models\system\export;


use backend\models\search\CategoryIntlSearch;
use backend\modules\statistic\reports\BaseReportInterface;

class ExportCategoryIntl implements BaseReportInterface
{
    public $columns;
    public $items;

    public function __construct()
    {
        $exportModel = new CategoryIntlSearch();
        $this->columns  = array_merge($exportModel->attributeLabels(), [
            'en_title' => '_Title',
        ]);
    }

    public function getColumnsNames()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        if(!empty($this->items)){
            return $this->items;
        }
        $sql = "select a.id, a.title, a.lang_iso, a.description, IFNULL(a.model_id, b.id) as model_id, b.title as en_title 
                from category b
                left join category_intl a on b.id = a.model_id;";

        $this->items = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        return $this->items;
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }

    public static function create()
    {
        return new self;
    }
}