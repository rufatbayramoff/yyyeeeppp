<?php

namespace backend\models\system\export;

use backend\components\BaseReportWrapper;
use backend\models\search\PsSearch;
use backend\modules\statistic\reports\BaseReport;
use backend\modules\statistic\reports\BaseReportInterface;
use common\models\Ps;
use Yii;

/**
 * User: nabi
 */
class ExportCompany implements BaseReportInterface
{

    private $getParams = [];

    /**
     * Return item class
     *
     * @return string
     */
    public function getItemClass()
    {
        return Ps::class;
    }

    public function getColumnsNames()
    {
        return [
            'id'                => Yii::t('app', 'ID'),
            'user_id'           => Yii::t('app', 'User ID'),
            'title'             => Yii::t('app', 'Title'),
            'description'       => Yii::t('app', 'Description'),
            'phone_code'        => Yii::t('app', 'Phone Code'),
            'phone'             => Yii::t('app', 'Phone'),
            'is_active'         => Yii::t('app', 'Is Active'),
            'created_at'        => Yii::t('app', 'Created At'),
            'updated_at'        => Yii::t('app', 'Updated At'),
            'phone_status'      => Yii::t('app', 'Phone Status'),
            'phone_country_iso' => Yii::t('app', 'Phone Country Iso'),
            'url'               => Yii::t('app', 'Url'),
            'publicCompanyLink' => Yii::t('app', 'Public company url'),
            'website'           => Yii::t('app', 'Website'),
            'facebook'          => Yii::t('app', 'Facebook'),
            'instagram'         => Yii::t('app', 'Instagram'),
            'twitter'           => Yii::t('app', 'Twitter'),
            'country_id'        => Yii::t('app', 'Country ID'),
            'currency'          => Yii::t('app', 'Currency'),
            'ownership'         => Yii::t('app', 'Ownership'),
            'total_employees'   => Yii::t('app', 'Total Employees'),
            'year_established'  => Yii::t('app', 'Year Established'),
            'annual_turnover'   => Yii::t('app', 'Annual Turnover'),
            'is_backend'        => Yii::t('app', 'Is Backend'),
        ];
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $searchModel = new PsSearch();
        $searchModel->load($this->getParams, '');
        $dataProvider = $searchModel->search([]);
        $dataProvider->query->andWhere(['is_backend' => 1]);
        $report = BaseReportWrapper::createFromDataProvider($dataProvider, $searchModel);
        return $report->getItems();
    }

    public function setParams(array $params)
    {
        $this->getParams = $params;
    }

    public static function create()
    {
        return new self;
    }
}