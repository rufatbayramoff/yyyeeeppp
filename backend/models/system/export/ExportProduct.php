<?php


namespace backend\models\system\export;


use backend\models\search\ProductSearch;
use backend\modules\statistic\reports\BaseReportInterface;
use common\components\ArrayHelper;

class ExportProduct implements BaseReportInterface
{

    private array $params;
    private ProductSearch $searchModel;

    public function __construct()
    {
        $this->searchModel = new ProductSearch();
    }

    public static function create()
    {
        return new self;
    }

    public function getColumnsNames(): array
    {
        return ArrayHelper::merge(
            $this->searchModel->attributeLabels(),
            ['date_created' => 'Date created']
        );
    }

    public function getItems()
    {
        $this->searchModel->load($this->params, '');
        $dataProvider = $this->searchModel->search([]);
        $dataProvider->query->addSelect(['product.*','date_created' => 'product_common.created_at']);
        $command = $dataProvider->query->createCommand();

        return $command->queryAll(\PDO::FETCH_CLASS);
    }

    public function setParams(array $params)
    {
        $this->params = $params;
    }
}