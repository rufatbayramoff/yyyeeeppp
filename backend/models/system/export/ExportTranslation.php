<?php

namespace backend\models\system\export;


use backend\models\search\SeoPageIntlSearch;
use backend\modules\statistic\reports\BaseReportInterface;

class ExportTranslation implements BaseReportInterface
{
    public $columns = [
        'id'                  => 'Id',
        'language'            => 'Language',
        'category'            => 'Category',
        'message'             => 'Message',
        'not_found_in_source' => 'Not found in source',
        'is_checked'          => 'Is checked',
        'date'                => 'Original text date',
        'translation'         => 'Translation',
        'is_auto_translate'   => 'Is auto translate',
        'updated_at'          => 'Updated at',
        'translated_by'       => 'Translated by',
    ];
    public $items;

    public function getColumnsNames()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        if (!empty($this->items)) {
            return $this->items;
        }

        $sql = "SELECT `system_lang_message`.*, `system_lang_source`.`category`, `system_lang_source`.`message`,  `system_lang_source`.`not_found_in_source`, `system_lang_source`.`is_checked`, `system_lang_source`.`date`
FROM `system_lang_message` LEFT JOIN `system_lang_source` on `system_lang_source`.id=`system_lang_message`.id";
        $this->items = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        return $this->items;
    }

    public function setParams(array $params)
    {
    }

    public static function create()
    {
        return new self;
    }
}
