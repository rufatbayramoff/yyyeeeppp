<?php
/**
 * User: nabi
 */

namespace backend\models\system\export;


use backend\modules\statistic\reports\BaseReportInterface;
use common\models\Preorder;

class ExportPreorder implements BaseReportInterface
{
    public $columns;
    public $items;

    /**
     * @var array
     */
    public $params;

    public function __construct()
    {
        $exportModel = new Preorder();
        $this->columns = array_merge(
            [
                'order_id'       => 'Order',
                'attempt_status' => 'Order status',
                'price_total'    => 'Total price',
                'payment_status' => 'Payment status',
            ],
            $exportModel->attributeLabels()
        );
    }

    public function getColumnsNames()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        if (!empty($this->items)) {
            return $this->items;
        }
        $sql = "select p.*, o.id as order_id, payment_invoice.status as payment_status, payment_invoice.total_amount as price_total, a.status as attempt_status
                from preorder p 
                left join store_order o on p.id = o.preorder_id
                left join payment_invoice on payment_invoice.uuid = o.primary_payment_invoice_uuid
                left join store_order_attemp a on o.current_attemp_id = a.id";

        $this->items = \Yii::$app->db->createCommand($sql)->queryAll(\PDO::FETCH_CLASS);
        return $this->items;
    }

    public function setParams(array $params)
    {
        $this->params = $params;
    }

    public static function create()
    {
        return new self;
    }
}