<?php

namespace backend\models\system\export;


use backend\models\search\SeoPageIntlSearch;
use backend\modules\statistic\reports\BaseReportInterface;
use common\modules\translation\models\search\I18nTableElementSearch;
use Yii;

class ExportTranslationDB implements BaseReportInterface
{
    public $columns = [
        'tableName'    => 'TableName',
        'modelId'      => 'ModelId',
        'fieldName'    => 'FieldName',
        'valueOrig'    => 'Message',
        'fieldValue'   => 'Field value',
        'langIso'      => 'Lang ISO',
        'isActive'     => 'Is active',
        'updatedAt'    => 'Updated at',
        'translatedBy' => 'Translated by',
    ];

    public $items;

    public function getColumnsNames()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        if (!empty($this->items)) {
            return $this->items;
        }

        $searchModel = new I18nTableElementSearch();
        $queryParams = Yii::$app->request->queryParams;
        $items       = $searchModel->searchItems(Yii::$app->request->queryParams, false);
        return $items;
    }

    public function setParams(array $params)
    {
    }

    public static function create()
    {
        return new self;
    }
}
