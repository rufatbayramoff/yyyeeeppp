<?php
/**
 * User: nabi
 */

namespace backend\models\system;


use common\modules\equipments\helpers\PrinterImageHelper;
use common\modules\equipments\repositories\PrinterImageRepository;
use lib\file\UploadException;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public $uploadPath;

    /**
     * @var PrinterImageRepository
     */
    private $imageRepository;
    /**
     * @var PrinterImageHelper
     */
    private $imageHelper;

    public function init()
    {
        /**
         * @var $imageHelper PrinterImageHelper
         */
        $this->imageHelper = \Yii::createObject(PrinterImageHelper::class);
        if(empty($this->uploadPath)){
            $this->uploadPath = $this->imageHelper->getScanPath();
        }

        $this->imageRepository = \Yii::createObject(['class'=>PrinterImageRepository::class ]);
    }
    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, gif, PNG, JPG, GIF, JPEG, zip, ZIP', 'maxFiles' => 50],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $fileInfo = pathinfo($file);
                if($fileInfo['extension']=='zip'){
                    $za = new \ZipArchive();
                    if ($za->open($file->tempName) !== true) {
                        throw new UploadException(_t('site.upload', 'Cannot open zip file.'));
                    }

                    $za->extractTo($this->uploadPath );
                    $za->close();
                    continue;
                }
                $file->saveAs($this->uploadPath . '/' . $file->baseName . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }

    public function findFiles($only)
    {
        $files = $this->imageRepository->findAll($this->uploadPath, $only);
        $result = $this->imageHelper->formatImagesAsMap($files);
        return $result;
    }
}