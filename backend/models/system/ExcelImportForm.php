<?php
/**
 * User: nabi
 */

namespace backend\models\system;


use common\components\BaseAR;
use common\components\BaseForm;
use common\components\FileDirHelper;
use InvalidArgumentException;
use Yii;
use yii\base\UserException;
use yii\helpers\Inflector;

class ExcelImportForm extends BaseForm
{
    /**
     * @var \yii\web\UploadedFile file attribute
     */
    public $file;

    /**
     * @var string
     */
    public $tableName;

    /**
     * @var array
     */
    protected $columns;

    /**
     * @var array
     */
    protected $xlsColumns;

    public $importErrors = [];

    private $_skipped = 0;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['tableName'], 'string'],
            [['tableName'], 'required'],
            [['file'], 'file', 'skipOnEmpty' => false, 'checkExtensionByMimeType' => false, 'extensions' => 'xls, xlsx'],
        ];
    }

    public function formName()
    {
        return 'ExcelImportForm';
    }

    /**
     * @param $tableName
     * @return ExcelImportForm
     */
    public static function create($tableName)
    {
        $modelClass = Inflector::id2camel($tableName, '_');
        $className = 'backend\models\system\import\\Import' . $modelClass;
        if (class_exists($className)) {
            return Yii::createObject($className);
        }
        return Yii::createObject(self::class);
    }

    /**
     * @return bool|string
     */
    public function import()
    {
        if (!$this->validate()) {
            return false;
        }
        if (empty($this->columns)) {
            $this->columns = $this->getColumns();
        }
        $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->file->tempName);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $highestRow = $objWorksheet->getHighestRow() - 1; // total rows
        $imported = $skipped = 0;
        $logs = [];
        foreach ($objWorksheet->getRowIterator() as $i => $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $row = [];
            foreach ($cellIterator as $cell) {
                $row[] = $cell->getValue();
            }
            if ($i == 1) {
                $this->setXlsColumns($row);
                continue;
            }
            $res = $this->importRow($i, $row);
            if (is_string($res)) {
                $logs[] = $res;
                $skipped++;
                continue;
            }
            $res ? $imported++ : $skipped++;
        }
        $this->_skipped = $skipped;
        $logs = implode('<br />', $logs);
        $returnValue = sprintf("Total rows: %d, Rows imported: %d, Skipped: %d <br /> %s", $highestRow, $imported, $skipped, $logs);
        if ($this->importErrors) {
            $url = $this->formImportProtocolUrl();
            $returnValue .= "Errors log: <a href='" . $url . "'  target='_blank'>" . $url . '</a>';
        }
        return $returnValue;
    }


    /**
     * @throws \yii\base\ErrorException
     */
    public function formImportProtocolUrl()
    {
        $now = time();
        $protocol = 'Import protocol: ' . $this->file->name . " " . date('Y-m-d H:i:s', $now) . "\n";
        foreach ($this->importErrors as $lineId => $importError) {
            $protocol .= "Line: $lineId \n";
            $protocol .= json_encode($importError, JSON_PRETTY_PRINT) . "\n";
        }
        $protocolDir = Yii::getAlias('@runtime') . '/importProtocol';
        FileDirHelper::createDir($protocolDir);
        $uid = date('Y-m-d_H-i-s') . '_' . str_replace('.', '_', microtime(true));
        $protocolPath = $protocolDir . '/' . $uid . '.txt';
        file_put_contents($protocolPath, $protocol);
        return param('backendServer') . '/system/import/view-protocol?uid=' . $uid;

    }

    public function getSkipped()
    {
        return $this->_skipped;
    }

    public function getTemplateFields()
    {
        return [];
    }

    public function getTemplateFieldNames()
    {
        $returnValue = [];
        foreach ($this->getTemplateFields() as $fieldKey => $fieldInfo) {
            if (is_string($fieldInfo)) {
                $fieldName = $fieldInfo;
                $fieldInfo = null;
            } else {
                if (array_key_exists('required', $fieldInfo) && $fieldInfo['required']) {
                    $fieldName = $fieldKey . '*';
                }
            }
            $returnValue[] = $fieldName;
        }
        return $returnValue;
    }

    /**
     * @param $row
     * @throws \yii\base\UserException
     */
    protected function setXlsColumns($row)
    {
        $rowFiltered = [];
        foreach ($row as $element) {
            $element = $element ? str_replace('*', '', $element) : $element;
            $rowFiltered[] = $element;
        }
        $row = $rowFiltered;

        $fields = $this->getTemplateFields();
        $requiredFields = [];
        foreach ($fields as $fieldKey => $fieldInfo) {
            if (is_string($fieldInfo)) {
                $fieldName = $fieldInfo;
                $fieldInfo = null;
            } else {
                if (array_key_exists('required', $fieldInfo) && $fieldInfo['required']) {
                    $requiredFields[] = $fieldKey;
                }
            }
        }
        $notExistsFields = array_diff($requiredFields, $row);
        if ($notExistsFields) {
            throw new UserException('Not exists fields: ' . json_encode(array_values($notExistsFields)));
        }

        $this->xlsColumns = $row;
    }

    protected function getColumns()
    {
        $modelClass = Inflector::id2camel($this->tableName, '_');
        $className = 'backend\models\search\\' . $modelClass . 'Search';
        /** @var BaseAR $searchModel */
        $searchModel = Yii::createObject($className);
        return array_flip($searchModel->attributeLabels());
    }

    protected function importRow($index, $rowData)
    {
        if (count(array_values($this->columns)) !== count(array_values($rowData))) {
            $rowData = array_slice($rowData, 0, count($this->columns));
        }
        $row = array_combine(array_values($this->columns), array_values($rowData));
        return $this->insertRow($index, $row);
    }

    protected function insertRow($index, $row)
    {
        try {
            if (array_key_exists('is_active', $row)) {
                $row['is_active'] = (boolean)$row['is_active'];
            }
            if (!empty($row['id'])) {
                $res = \Yii::$app->db->createCommand()->update($this->tableName, $row, ['id' => $row['id']])->execute();
            } else {
                $res = \Yii::$app->db->createCommand()->insert($this->tableName, $row)->execute();
            }
        } catch (\Exception $e) {
            \Yii::error('app', $e->getMessage());
            $res = $e->getMessage();
            if (strpos($res, 'Dupl') !== false) {
                $res = sprintf("Row %d. Duplicate data. Skipped.", $index);
            } else {
                $res = substr($res, 0, strpos($res, 'The SQL being executed was'));
                $res = sprintf("Row %d. Error %s. Skipped. %s", $index, !empty($row['id']) ? 'updating' : 'creating', $res);
            }
        }
        return $res;
    }

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function getExcelRows()
    {
        $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->file->tempName);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $imported = $skipped = 0;
        $logs = [];
        $result = [];
        $columns = [];
        foreach ($objWorksheet->getRowIterator() as $i => $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $row = [];
            foreach ($cellIterator as $k => $cell) {
                if (!empty($columns) && count($row) === count($columns)) {
                    continue;
                }
                $row[] = (string)$cell->getValue();
            }
            if ($i == 1) {
                $row = array_filter($row);
                $columns = array_map('strtolower', $row);
                continue;
            }
            if (!$columns) {
                continue;
            }
            yield array_combine($columns, $row);
        }
    }

}
