<?php
namespace backend\models\system;

use common\components\FileDirHelper;

/**
 * Date: 07.04.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class WatermarkDecodeForm extends \common\components\BaseForm
{
    /**
     * @var \yii\web\UploadedFile file attribute
     */
    public $file;
    public $password;
    /**
     * @var
     */
    private $uploadPath;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['file'], 'file'],
            ['password', 'safe']
        ];
    }

    public function init()
    {
        $this->uploadPath = \Yii::getAlias('@storage') . '/decoded/';
        FileDirHelper::createDir($this->uploadPath);
        parent::init();
    }

    /**
     * @return mixed
     */
    public function getUploadFolder()
    {
        return $this->uploadPath;
    }

    /**
     * @return bool
     */
    public function saveFile()
    {
        $filePath = $this->getUploadFolder() . $this->file->baseName . '.' . $this->file->extension;
        $result = $this->file->saveAs($filePath);
        return $result ? $filePath : false;
    }
}