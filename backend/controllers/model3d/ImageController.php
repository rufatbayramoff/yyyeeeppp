<?php

namespace backend\controllers\model3d;
 
/**
 * Model3dImgController implements the CRUD actions for Model3dImg model.
 */
class ImageController  extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/model3d/model3d-img';
    public $accessGroup = 'model3d_image';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\Model3dImgSearch();
        $this->mainModel = new \common\models\Model3dImg();
    }
}
