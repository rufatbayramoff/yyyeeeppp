<?php

namespace backend\controllers\model3d;

use Yii; 

/**
 * Model3dPartController implements the CRUD actions for Model3dPart model.
 */
class FileController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/model3d/model3d-file';
   
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\Model3DPartSearch();
        $this->mainModel = new \common\models\Model3dPart();
    }
}
