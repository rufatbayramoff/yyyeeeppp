<?php
/**
 * Date: 07.04.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\controllers\model3d;


use backend\components\AdminController;
use backend\models\system\WatermarkDecodeForm;
use common\models\model3d\Model3dWatermarkService;
use yii\helpers\Html;
use yii\web\UploadedFile;
use Yii;
class WatermarkController extends AdminController
{
    protected $viewPath = '@backend/views/model3d/watermark';

    public function actionIndex()
    {
        $model = new WatermarkDecodeForm();

        $decode = '';
        if (Yii::$app->request->isPost) {
            $model->load(app('request')->post());
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file && $model->validate()) {
                $filePath = $model->saveFile();
                if($filePath){
                    \Yii::$app->watermarker->setPassword($model->password);
                    $decode = \Yii::$app->watermarker->decode($filePath);
                }
            } else {
                $this->setFlashMsg(false, Html::errorSummary($model));
            }
        }
        $model->password = Model3dWatermarkService::PASSPHRASE;
        return $this->render('index', ['model' => $model, 'result' => $decode]);
    }
}