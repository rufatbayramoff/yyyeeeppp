<?php

namespace backend\controllers\model3d;

use console\jobs\QueueGateway;
use Yii;

/**
 * 
 */
class JobsController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/model3d/file-job';
    protected $accessGroup = "file_job";
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\FileJobSearch();
        $this->mainModel = new \common\models\FileJob();
    }

    public function actionRepeat($id)
    {
        /** @var \common\models\FileJob $model */
        $model = $this->findModel($id);
        QueueGateway::repeat($model);
        return $this->redirect(['index']);
    }
}
