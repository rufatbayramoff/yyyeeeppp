<?php

namespace backend\controllers\model3d;

/**
 * LicenseController implements the CRUD actions for Model3dLicense model.
 */
class LicenseIntlController  extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/model3d/model3d-license-intl';

    protected $accessGroup = 'license';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\Model3dLicenseIntlSearch();
        $this->mainModel = new \common\models\Model3dLicenseIntl();
    }
}
