<?php

namespace backend\controllers\crud;

use Yii;
use common\models\PreorderFile;
use backend\models\search\PreorderFileSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * PreorderFileController implements the CRUD actions for PreorderFile model.
 */
class PreorderFileController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PreorderFile models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new PreorderFileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PreorderFile model.
     * @param integer $preorder_id
     * @param integer $file_id
     * @return mixed
     */
    public function actionView($preorder_id, $file_id)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($preorder_id, $file_id),
        ]);
    }

    /**
     * Creates a new PreorderFile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new PreorderFile();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'preorder_id' => $model->preorder_id, 'file_id' => $model->file_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PreorderFile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $preorder_id
     * @param integer $file_id
     * @return mixed
     */
    public function actionUpdate($preorder_id, $file_id)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($preorder_id, $file_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'preorder_id' => $model->preorder_id, 'file_id' => $model->file_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PreorderFile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $preorder_id
     * @param integer $file_id
     * @return mixed
     */
    public function actionDelete($preorder_id, $file_id)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($preorder_id, $file_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PreorderFile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $preorder_id
     * @param integer $file_id
     * @return PreorderFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($preorder_id, $file_id)
    {
        if (($model = PreorderFile::findByPk(['preorder_id' => $preorder_id, 'file_id' => $file_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
