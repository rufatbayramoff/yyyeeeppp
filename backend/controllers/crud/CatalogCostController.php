<?php

namespace backend\controllers\crud;

use Yii;
use common\models\CatalogCost;
use backend\models\search\CatalogCostSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * CatalogCostController implements the CRUD actions for CatalogCost model.
 */
class CatalogCostController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CatalogCost models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new CatalogCostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CatalogCost model.
     * @param integer $store_unit_id
     * @param integer $country_id
     * @return mixed
     */
    public function actionView($store_unit_id, $country_id)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($store_unit_id, $country_id),
        ]);
    }

    /**
     * Creates a new CatalogCost model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new CatalogCost();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'store_unit_id' => $model->store_unit_id, 'country_id' => $model->country_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CatalogCost model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $store_unit_id
     * @param integer $country_id
     * @return mixed
     */
    public function actionUpdate($store_unit_id, $country_id)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($store_unit_id, $country_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'store_unit_id' => $model->store_unit_id, 'country_id' => $model->country_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CatalogCost model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $store_unit_id
     * @param integer $country_id
     * @return mixed
     */
    public function actionDelete($store_unit_id, $country_id)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($store_unit_id, $country_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CatalogCost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $store_unit_id
     * @param integer $country_id
     * @return CatalogCost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($store_unit_id, $country_id)
    {
        if (($model = CatalogCost::findByPk(['store_unit_id' => $store_unit_id, 'country_id' => $country_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
