<?php

namespace backend\controllers\crud;

use Yii;
use common\models\WikiMaterialManufactureType;
use backend\models\search\WikiMaterialManufactureTypeSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * WikiMaterialManufactureTypeController implements the CRUD actions for WikiMaterialManufactureType model.
 */
class WikiMaterialManufactureTypeController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WikiMaterialManufactureType models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new WikiMaterialManufactureTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WikiMaterialManufactureType model.
     * @param integer $wiki_material_id
     * @param integer $manufacture_type_id
     * @return mixed
     */
    public function actionView($wiki_material_id, $manufacture_type_id)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($wiki_material_id, $manufacture_type_id),
        ]);
    }

    /**
     * Creates a new WikiMaterialManufactureType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new WikiMaterialManufactureType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'wiki_material_id' => $model->wiki_material_id, 'manufacture_type_id' => $model->manufacture_type_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing WikiMaterialManufactureType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $wiki_material_id
     * @param integer $manufacture_type_id
     * @return mixed
     */
    public function actionUpdate($wiki_material_id, $manufacture_type_id)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($wiki_material_id, $manufacture_type_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'wiki_material_id' => $model->wiki_material_id, 'manufacture_type_id' => $model->manufacture_type_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing WikiMaterialManufactureType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $wiki_material_id
     * @param integer $manufacture_type_id
     * @return mixed
     */
    public function actionDelete($wiki_material_id, $manufacture_type_id)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($wiki_material_id, $manufacture_type_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WikiMaterialManufactureType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $wiki_material_id
     * @param integer $manufacture_type_id
     * @return WikiMaterialManufactureType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($wiki_material_id, $manufacture_type_id)
    {
        if (($model = WikiMaterialManufactureType::findByPk(['wiki_material_id' => $wiki_material_id, 'manufacture_type_id' => $manufacture_type_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
