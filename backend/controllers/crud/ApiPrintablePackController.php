<?php

namespace backend\controllers\crud;

use Yii;
use common\models\ApiPrintablePack;
use backend\models\search\ApiPrintablePackSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * ApiPrintablePackController implements the CRUD actions for ApiPrintablePack model.
 */
class ApiPrintablePackController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ApiPrintablePack models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new ApiPrintablePackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApiPrintablePack model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ApiPrintablePack model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new ApiPrintablePack();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ApiPrintablePack model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ApiPrintablePack model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApiPrintablePack model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApiPrintablePack the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApiPrintablePack::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
