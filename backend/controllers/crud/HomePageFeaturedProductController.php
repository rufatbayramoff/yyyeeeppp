<?php

namespace backend\controllers\crud;

use Yii;
use common\models\HomePageFeaturedProduct;
use backend\models\search\HomePageFeaturedProductSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * HomePageFeaturedProductController implements the CRUD actions for HomePageFeaturedProduct model.
 */
class HomePageFeaturedProductController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HomePageFeaturedProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new HomePageFeaturedProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HomePageFeaturedProduct model.
     * @param integer $featured_category_id
     * @param string $product_uuid
     * @return mixed
     */
    public function actionView($featured_category_id, $product_uuid)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($featured_category_id, $product_uuid),
        ]);
    }

    /**
     * Creates a new HomePageFeaturedProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new HomePageFeaturedProduct();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'featured_category_id' => $model->featured_category_id, 'product_uuid' => $model->product_uuid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing HomePageFeaturedProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $featured_category_id
     * @param string $product_uuid
     * @return mixed
     */
    public function actionUpdate($featured_category_id, $product_uuid)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($featured_category_id, $product_uuid);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'featured_category_id' => $model->featured_category_id, 'product_uuid' => $model->product_uuid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing HomePageFeaturedProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $featured_category_id
     * @param string $product_uuid
     * @return mixed
     */
    public function actionDelete($featured_category_id, $product_uuid)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($featured_category_id, $product_uuid)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HomePageFeaturedProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $featured_category_id
     * @param string $product_uuid
     * @return HomePageFeaturedProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($featured_category_id, $product_uuid)
    {
        if (($model = HomePageFeaturedProduct::findByPk(['featured_category_id' => $featured_category_id, 'product_uuid' => $product_uuid])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
