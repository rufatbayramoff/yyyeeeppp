<?php

namespace backend\controllers\crud;

use Yii;
use common\models\PreorderWorkFile;
use backend\models\search\PreorderWorkFileSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * PreorderWorkFileController implements the CRUD actions for PreorderWorkFile model.
 */
class PreorderWorkFileController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PreorderWorkFile models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new PreorderWorkFileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PreorderWorkFile model.
     * @param integer $work_id
     * @param integer $file_id
     * @return mixed
     */
    public function actionView($work_id, $file_id)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($work_id, $file_id),
        ]);
    }

    /**
     * Creates a new PreorderWorkFile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new PreorderWorkFile();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'work_id' => $model->work_id, 'file_id' => $model->file_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PreorderWorkFile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $work_id
     * @param integer $file_id
     * @return mixed
     */
    public function actionUpdate($work_id, $file_id)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($work_id, $file_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'work_id' => $model->work_id, 'file_id' => $model->file_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PreorderWorkFile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $work_id
     * @param integer $file_id
     * @return mixed
     */
    public function actionDelete($work_id, $file_id)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($work_id, $file_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PreorderWorkFile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $work_id
     * @param integer $file_id
     * @return PreorderWorkFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($work_id, $file_id)
    {
        if (($model = PreorderWorkFile::findByPk(['work_id' => $work_id, 'file_id' => $file_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
