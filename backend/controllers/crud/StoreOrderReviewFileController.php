<?php

namespace backend\controllers\crud;

use Yii;
use common\models\StoreOrderReviewFile;
use backend\models\search\StoreOrderReviewFileSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * StoreOrderReviewFileController implements the CRUD actions for StoreOrderReviewFile model.
 */
class StoreOrderReviewFileController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StoreOrderReviewFile models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new StoreOrderReviewFileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StoreOrderReviewFile model.
     * @param integer $review_id
     * @param string $file_uuid
     * @return mixed
     */
    public function actionView($review_id, $file_uuid)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($review_id, $file_uuid),
        ]);
    }

    /**
     * Creates a new StoreOrderReviewFile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new StoreOrderReviewFile();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'review_id' => $model->review_id, 'file_uuid' => $model->file_uuid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StoreOrderReviewFile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $review_id
     * @param string $file_uuid
     * @return mixed
     */
    public function actionUpdate($review_id, $file_uuid)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($review_id, $file_uuid);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'review_id' => $model->review_id, 'file_uuid' => $model->file_uuid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StoreOrderReviewFile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $review_id
     * @param string $file_uuid
     * @return mixed
     */
    public function actionDelete($review_id, $file_uuid)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($review_id, $file_uuid)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StoreOrderReviewFile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $review_id
     * @param string $file_uuid
     * @return StoreOrderReviewFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($review_id, $file_uuid)
    {
        if (($model = StoreOrderReviewFile::findByPk(['review_id' => $review_id, 'file_uuid' => $file_uuid])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
