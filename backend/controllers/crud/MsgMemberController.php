<?php

namespace backend\controllers\crud;

use Yii;
use common\models\MsgMember;
use backend\models\search\MsgMemberSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * MsgMemberController implements the CRUD actions for MsgMember model.
 */
class MsgMemberController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MsgMember models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new MsgMemberSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MsgMember model.
     * @param integer $user_id
     * @param integer $topic_id
     * @return mixed
     */
    public function actionView($user_id, $topic_id)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($user_id, $topic_id),
        ]);
    }

    /**
     * Creates a new MsgMember model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new MsgMember();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'user_id' => $model->user_id, 'topic_id' => $model->topic_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MsgMember model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $user_id
     * @param integer $topic_id
     * @return mixed
     */
    public function actionUpdate($user_id, $topic_id)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($user_id, $topic_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'user_id' => $model->user_id, 'topic_id' => $model->topic_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MsgMember model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $user_id
     * @param integer $topic_id
     * @return mixed
     */
    public function actionDelete($user_id, $topic_id)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($user_id, $topic_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MsgMember model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @param integer $topic_id
     * @return MsgMember the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user_id, $topic_id)
    {
        if (($model = MsgMember::findByPk(['user_id' => $user_id, 'topic_id' => $topic_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
