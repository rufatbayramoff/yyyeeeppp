<?php

namespace backend\controllers\crud;

use Yii;
use common\models\PaymentInvoicePaymentMethod;
use backend\models\search\PaymentInvoicePaymentMethodSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * PaymentInvoicePaymentMethodController implements the CRUD actions for PaymentInvoicePaymentMethod model.
 */
class PaymentInvoicePaymentMethodController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentInvoicePaymentMethod models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new PaymentInvoicePaymentMethodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaymentInvoicePaymentMethod model.
     * @param string $payment_invoice_uuid
     * @param string $vendor
     * @return mixed
     */
    public function actionView($payment_invoice_uuid, $vendor)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($payment_invoice_uuid, $vendor),
        ]);
    }

    /**
     * Creates a new PaymentInvoicePaymentMethod model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new PaymentInvoicePaymentMethod();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'payment_invoice_uuid' => $model->payment_invoice_uuid, 'vendor' => $model->vendor]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PaymentInvoicePaymentMethod model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $payment_invoice_uuid
     * @param string $vendor
     * @return mixed
     */
    public function actionUpdate($payment_invoice_uuid, $vendor)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($payment_invoice_uuid, $vendor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'payment_invoice_uuid' => $model->payment_invoice_uuid, 'vendor' => $model->vendor]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PaymentInvoicePaymentMethod model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $payment_invoice_uuid
     * @param string $vendor
     * @return mixed
     */
    public function actionDelete($payment_invoice_uuid, $vendor)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($payment_invoice_uuid, $vendor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaymentInvoicePaymentMethod model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $payment_invoice_uuid
     * @param string $vendor
     * @return PaymentInvoicePaymentMethod the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($payment_invoice_uuid, $vendor)
    {
        if (($model = PaymentInvoicePaymentMethod::findByPk(['payment_invoice_uuid' => $payment_invoice_uuid, 'vendor' => $vendor])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
