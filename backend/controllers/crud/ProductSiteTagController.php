<?php

namespace backend\controllers\crud;

use Yii;
use common\models\ProductSiteTag;
use backend\models\search\ProductSiteTagSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * ProductSiteTagController implements the CRUD actions for ProductSiteTag model.
 */
class ProductSiteTagController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductSiteTag models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new ProductSiteTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductSiteTag model.
     * @param string $product_uuid
     * @param integer $site_tag_id
     * @return mixed
     */
    public function actionView($product_uuid, $site_tag_id)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($product_uuid, $site_tag_id),
        ]);
    }

    /**
     * Creates a new ProductSiteTag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new ProductSiteTag();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'product_uuid' => $model->product_uuid, 'site_tag_id' => $model->site_tag_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProductSiteTag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $product_uuid
     * @param integer $site_tag_id
     * @return mixed
     */
    public function actionUpdate($product_uuid, $site_tag_id)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($product_uuid, $site_tag_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'product_uuid' => $model->product_uuid, 'site_tag_id' => $model->site_tag_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductSiteTag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $product_uuid
     * @param integer $site_tag_id
     * @return mixed
     */
    public function actionDelete($product_uuid, $site_tag_id)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($product_uuid, $site_tag_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductSiteTag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $product_uuid
     * @param integer $site_tag_id
     * @return ProductSiteTag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($product_uuid, $site_tag_id)
    {
        if (($model = ProductSiteTag::findByPk(['product_uuid' => $product_uuid, 'site_tag_id' => $site_tag_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
