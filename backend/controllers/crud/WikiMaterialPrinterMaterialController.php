<?php

namespace backend\controllers\crud;

use Yii;
use common\models\WikiMaterialPrinterMaterial;
use backend\models\search\WikiMaterialPrinterMaterialSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * WikiMaterialPrinterMaterialController implements the CRUD actions for WikiMaterialPrinterMaterial model.
 */
class WikiMaterialPrinterMaterialController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WikiMaterialPrinterMaterial models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new WikiMaterialPrinterMaterialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WikiMaterialPrinterMaterial model.
     * @param integer $wiki_material_id
     * @param integer $printer_material_id
     * @return mixed
     */
    public function actionView($wiki_material_id, $printer_material_id)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($wiki_material_id, $printer_material_id),
        ]);
    }

    /**
     * Creates a new WikiMaterialPrinterMaterial model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new WikiMaterialPrinterMaterial();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'wiki_material_id' => $model->wiki_material_id, 'printer_material_id' => $model->printer_material_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing WikiMaterialPrinterMaterial model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $wiki_material_id
     * @param integer $printer_material_id
     * @return mixed
     */
    public function actionUpdate($wiki_material_id, $printer_material_id)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($wiki_material_id, $printer_material_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'wiki_material_id' => $model->wiki_material_id, 'printer_material_id' => $model->printer_material_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing WikiMaterialPrinterMaterial model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $wiki_material_id
     * @param integer $printer_material_id
     * @return mixed
     */
    public function actionDelete($wiki_material_id, $printer_material_id)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($wiki_material_id, $printer_material_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WikiMaterialPrinterMaterial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $wiki_material_id
     * @param integer $printer_material_id
     * @return WikiMaterialPrinterMaterial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($wiki_material_id, $printer_material_id)
    {
        if (($model = WikiMaterialPrinterMaterial::findByPk(['wiki_material_id' => $wiki_material_id, 'printer_material_id' => $printer_material_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
