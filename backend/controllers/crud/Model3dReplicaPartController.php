<?php

namespace backend\controllers\crud;

use Yii;
use common\models\Model3dReplicaPart;
use backend\models\search\Model3dReplicaPartSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use backend\components\AdminAccess;
use yii\filters\VerbFilter;

/**
 * Model3dReplicaPartController implements the CRUD actions for Model3dReplicaPart model.
 */
class Model3dReplicaPartController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Model3dReplicaPart models.
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('crud.view');
        $searchModel = new Model3dReplicaPartSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Model3dReplicaPart model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess('crud.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Model3dReplicaPart model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('crud.view');
        $model = new Model3dReplicaPart();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Model3dReplicaPart model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        AdminAccess::validateAccess('crud.view');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Model3dReplicaPart model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        AdminAccess::validateAccess('crud.view');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Model3dReplicaPart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Model3dReplicaPart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Model3dReplicaPart::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
