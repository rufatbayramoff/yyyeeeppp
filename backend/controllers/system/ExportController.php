<?php

namespace backend\controllers\system;

use backend\components\AdminController;
use backend\components\BaseReportWrapper;
use backend\modules\statistic\reports\BaseReportInterface;
use backend\modules\statistic\reports\ReportCsvWriter;
use backend\modules\statistic\reports\ReportExcelWriter;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;
use yii\web\HttpException;


/**
 * User: nabi
 */
class ExportController extends AdminController
{

    public function actionIndex()
    {
        $params = app('request')->getQueryParams();
        if (empty($params['tableName']) && empty($params['exportClass'])) {
            throw new HttpException(400);
        }
        if (!empty($params['exportClass'])) {
            $modelClass = $params['exportClass'];
        } else {
            $modelClass = Inflector::id2camel($params['tableName'], '_');
        }
        $className = 'backend\models\search\\' . $modelClass . 'Search';

        $exportClassName = 'backend\models\system\export\\Export' . $modelClass;
        if (class_exists($exportClassName)) {
            /** @var BaseReportInterface $exporter */
            $report = new $exportClassName();
            $report->setParams($params);
        } else {
            $searchModel = new $className();
            $searchModel->load($params, '');
            $dataProvider = $searchModel->search([]);
            $report       = BaseReportWrapper::createFromDataProvider($dataProvider, $searchModel);
        }
        $writerClass = ReportExcelWriter::class;
        if (array_key_exists('writerClass', $params)) {
            $writerClass = 'backend\modules\statistic\reports\\'.$params['writerClass'];
        }
        $reportWriter = Yii::createObject($writerClass, [$modelClass]);
        return $reportWriter->write($report);
    }
}