<?php
/**
 * User: nabi
 */

namespace backend\controllers\system;

use backend\components\AdminController;
use backend\models\system\ExcelImportForm;
use backend\modules\importer\etl\EtlSchema;
use backend\modules\importer\etl\extractors\ArrayExtractor;
use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\loaders\BaseLoader;
use backend\modules\importer\etl\loaders\CategoriesMigrateLoader;
use backend\modules\importer\etl\Runner;
use backend\modules\importer\etl\transformers\CallbackTransformer;
use backend\modules\importer\etl\transformers\GetPrevRowTransformer;
use common\components\ArrayHelper;
use common\models\Product;
use common\models\ProductCategory;
use common\modules\product\repositories\ProductCategoryRepository;
use yii\web\UploadedFile;


/**
 * Class MigrateCategoriesProductsController
 *
 * used for one time migration tool
 *
 * @package backend\controllers\system
 */
class MigrateCategoriesProductsController extends AdminController
{

    public function actionIndex()
    {
        $model = new ExcelImportForm();
        return $this->render('index', ['model' => $model]);
    }

    public function actionCategories()
    {
        $model = new ExcelImportForm();
        $model->load(app('request')->post(), 'ExcelImportForm');
        $model->file = UploadedFile::getInstance($model, 'file');
        $model->tableName = 'blank';

        if ($model->file && $model->validate()) {
            $loader = new CategoriesMigrateLoader();
            $etlSchema = (new EtlSchema())
                ->extract(['class' => ArrayExtractor::class])
                ->transform($this->getImportTransformers())
                ->load($loader);
            $etlRunner = new Runner(false);
            $etlRunner->run($etlSchema, $model->getExcelRows());
            $importReport = $etlRunner->getReport();
            $this->setFlashMsg(true, $importReport->getString());
            // mark old one as inactive
        }
        return $this->redirect('index');
    }

    public function actionProducts()
    {
        /*$productCategoryRepo = \Yii::createObject(ProductCategoryRepository::class);

        $fromCategory = $productCategoryRepo->findCategoryByTitle('Industrial Parts & Tools');
        dd($childIds = ArrayHelper::getColumn($fromCategory->allChilds(), 'id'));*/

        $model = new ExcelImportForm();
        $model->load(app('request')->post(), 'ExcelImportForm');
        $model->file = UploadedFile::getInstance($model, 'file');
        $model->tableName = 'blank';

        if ($model->file && $model->validate()) {
            $loader = new BaseLoader();
            $etlSchema = (new EtlSchema())
                ->extract(['class' => ArrayExtractor::class])
                ->transform($this->getMigrateProdutsTransformers())
                ->load($loader);
            $etlRunner = new Runner(true);
            $result = $etlRunner->run($etlSchema, $model->getExcelRows());
            $this->moveProducts($result);
            $importReport = $etlRunner->getReport();
            $this->setFlashMsg(true, $importReport->getString());
            // mark old one as inactive
        }
        return $this->redirect('index');
    }

    public function actionRefresh()
    {
        #ProductCategory::updateAll(['is_active' => 0], 'id<62 AND id!=1');
        ProductCategory::updateAll(['is_visible'=>0], 'id>0');
        $products = Product::find()->active()->all();
        foreach ($products as $product) {
            $product->category->setAsVisible();
        }
        $this->setFlashMsg(true, 'Done');
        return $this->redirect('index');
    }

    private function moveProducts($result)
    {
        $moveProductsById = true;
        $productCategoryRepo = \Yii::createObject(ProductCategoryRepository::class);
        /** @var ProductCategoryRepository $loader */
        foreach ($result as $loader) {
            /** @var $loader BaseLoader */
            $rawData = $loader->getRawData();
            $productId = $rawData['product id'];
            $mainCategory = $rawData['root category'];
            $subCategory = $rawData['subcategory'];
            $subSubCategory = $rawData['sub-sub-category'];

            if ($productId === 'Root category (old)') {
                $moveProductsById = false;
                continue;
            }
            if ($moveProductsById) {
                /** @var ProductCategory $category */
                $category = $productCategoryRepo->findCategoryByTitles($mainCategory, $subCategory, $subSubCategory);
                $product = Product::find()->where(['uuid' => $productId])->one();
                if ($category && $product) {
                    $product->category_id = $category->id;
                    $product->safeSave();
                    $category->setAsVisible();
                }
            } else {
                // move products from category to category
                $mainOldCategory = $productId;
                $fromCategory = $productCategoryRepo->findCategoryByTitle($mainOldCategory);
                $toCategory = $productCategoryRepo->findCategoryByTitles($mainCategory, $subCategory, $subSubCategory);
                if ($fromCategory && $toCategory) {
                    $this->moveProductsFromToCategory($fromCategory, $toCategory);
                }
            }
        }
    }
    private function moveProductsFromToCategory(ProductCategory $fromCategory, ProductCategory $toCategory)
    {
        $fromCategoryIds = ArrayHelper::getColumn($fromCategory->allChilds(false), 'id');
        $fromCategoryIds[] = $fromCategory->id;
        $fromCategoryIds = implode(", ", $fromCategoryIds);
        Product::updateAll(['category_id'=>$toCategory->id], sprintf('category_id IN(%s) ', $fromCategoryIds));
        $toCategory->setAsVisible();
    }

    private function getMigrateProdutsTransformers()
    {
        return [
            [
                'class'    => CallbackTransformer::class,
                'callback' => function (ElementInterface $el) {
                    $data = $el->getData();
                    foreach ($data as $k => $v) {
                        $k = trim($k);
                        if (is_string($v)) {
                            $data[$k] = trim($v);
                        }
                    }
                    if (empty($data['sub-sub-category'])) {
                        return null;
                    }
                    $el->setData($data);
                    return $el;
                }
            ],
        ];
    }

    private function getImportTransformers()
    {
        return [
            [
                'class'    => GetPrevRowTransformer::class,
                'callback' => function (ElementInterface $cur, ElementInterface $prev = null) {
                    $dataCurrent = $cur->getData();
                    if (!empty($dataCurrent['root categories'])) {
                        return null; // we need root
                    }
                    if (!empty($dataCurrent['subcategories'])) {
                        $dataCurrent['root categories'] = $prev->getData()['root categories'];
                        return $dataCurrent;
                    } else { // subsub categories
                        $dataCurrent['root categories'] = $prev->getData()['root categories'];
                        $dataCurrent['subcategories'] = $prev->getData()['subcategories'];
                        return $dataCurrent;
                    }
                    return null;
                },
            ],
            [
                'class'    => CallbackTransformer::class,
                'callback' => function (ElementInterface $el) {
                    $data = $el->getData();
                    foreach ($data as $k => $v) {
                        $k = trim($k);
                        if (is_string($v)) {
                            $data[$k] = trim($v);
                        }
                    }
                    if (empty($data['sub-subcategories'])) {
                        return null;
                    }
                    $el->setData($data);
                    return $el;
                }
            ],
        ];
    }

}