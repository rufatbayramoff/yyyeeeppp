<?php
/**
 * User: nabi
 */

namespace backend\controllers\system;


use backend\components\AdminController;
use backend\components\BaseReportWrapper;
use backend\models\system\ExcelImportForm;
use backend\modules\statistic\reports\ReportExcelWriter;
use common\components\FileDirHelper;
use common\models\File;
use yii\helpers\Html;
use yii\web\Response;
use yii\web\UploadedFile;

class ImportController extends AdminController
{

    public function actionIndex()
    {
        $model = ExcelImportForm::create(app('request')->post('ExcelImportForm')['tableName']);

        if (\Yii::$app->request->isPost) {
            $model->load(app('request')->post(), 'ExcelImportForm');
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file && $model->validate()) {
                $msg = $model->import();
                $this->setFlashMsg($model->getSkipped()==0, "Import result: " . $msg);
            } else {
                $this->setFlashMsg(false, Html::errorSummary($model));
            }
        }
        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionViewProtocol($uid)
    {
        $protocolDir = \Yii::getAlias('@runtime') . '/importProtocol';
        $protocolPath = $protocolDir.'/'.File::cleanFileName($uid).'.txt';
        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        \Yii::$app->response->headers->add('Content-Type', 'text/plain');
        return file_get_contents($protocolPath);
    }

    /**
     * @param $tableName
     * @return string
     * @throws \PHPExcel_Exception
     */
    public function actionTemplate($tableName)
    {
        $model = ExcelImportForm::create($tableName);
        $templateFields = $model->getTemplateFieldNames();
        $report = new BaseReportWrapper();
        $report->columns = $templateFields;
        $report->setItems([]);
        $reportWriter = new ReportExcelWriter($tableName.'_template');
        return $reportWriter->write($report);
    }

    public function actionImport()
    {
        return $this->render('import');
    }
}