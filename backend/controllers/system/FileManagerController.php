<?php
/**
 * User: nabi
 */

namespace backend\controllers\system;


use backend\components\AdminController;
use backend\models\system\UploadForm;
use yii\web\UploadedFile;

class FileManagerController extends AdminController
{

    public function actionIndex()
    {
        $form = new UploadForm();

        if (\Yii::$app->request->isPost) {
            $form->imageFiles = UploadedFile::getInstances($form, 'imageFiles');
            if ($form->upload()) {
                return $this->redirect(app('request')->referrer);
            }
        }
        $only = [];
        if(!empty(app('request')->get('search'))){
            $searchStr = app('request')->get('search');
            $only = ['*'.$searchStr .'*'];
        }

        $result  = $form->findFiles($only);


        return $this->render('index', ['files'=>$result, 'uploadForm' =>$form]);
    }

    public function actionDelete($filename)
    {
        $form = new UploadForm();
        $filename = str_replace("..", "", $filename);
        unlink($form->uploadPath . '/' . $filename);
        return $this->redirect(app('request')->referrer);
    }

}