<?php
/**
 * User: nabi
 */

namespace backend\controllers\system;


use backend\components\AdminController;

class ApcController extends AdminController
{
    public function actionIndex()
    {
        return $this->render('apcu.php');
    }

    public function actionApc()
    {
        $this->layout = 'plain.php';
        return $this->render('apc.php');
    }
}
