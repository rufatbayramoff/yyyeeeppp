<?php

namespace backend\controllers\affiliate;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\AffiliateSourceClientSearch;
use backend\models\search\AffiliateSourceSearch;
use common\components\exceptions\OnlyPostRequestException;
use common\models\AffiliateSource;
use common\models\AffiliateSourceClient;
use Yii;
use yii\data\ActiveDataProvider;

/**
 *
 */
class AffiliateSourceController extends AdminController
{
    protected $viewPath = '@backend/views/affiliate/affiliate-source';

    public function actionIndex()
    {
        AdminAccess::validateAccess('affiliate.view');

        $searchModel  = new AffiliateSourceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single row.
     * @param integer $id
     * @return mixed
     * @throws \yii\base\UserException
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess('affiliate.view');
        $affiliateSource   = AffiliateSource::find()->where(['uuid' => $id])->one();
        $bindUsersProvider = new ActiveDataProvider([
            'query' => $affiliateSource->getAffiliateSourceClients(),
        ]);

        $referrersProvider = new ActiveDataProvider([
            'query' => $affiliateSource->getAffiliateSourceReferrers(),
            'sort'  => ['defaultOrder' => ['date' => SORT_DESC]]
        ]);

        return $this->render('view', [
            'model'             => $affiliateSource,
            'bindUsersProvider' => $bindUsersProvider,
            'referrersProvider' => $referrersProvider
        ]);
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        AdminAccess::validateAccess('affiliate.view');
        $affiliateSource           = AffiliateSource::find()->where(['uuid' => $id])->one();
        $affiliateSource->inactive = (int)!$affiliateSource->inactive;
        $affiliateSource->safeSave();
        return $this->redirect('/affiliate/affiliate-source');
    }

    public function actionUpdate($id)
    {
        AdminAccess::validateAccess('affiliate.view');
        $affiliateSource = AffiliateSource::find()->where(['uuid' => $id])->one();

        if (\Yii::$app->request->isPost) {
            $affiliateSource->loadWithAwardConfig(\Yii::$app->request->post());
            if ($affiliateSource->validate()) {
                $affiliateSource->safeSave();
                $this->setFlashMsg(true, _t('site.common', 'Updated'));
                return $this->redirect('/affiliate/affiliate-source/view?id=' . $affiliateSource->uuid);
            }
        }

        return $this->render('update', [
            'model' => $affiliateSource,
        ]);
    }

}
