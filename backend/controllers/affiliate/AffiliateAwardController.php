<?php

namespace backend\controllers\affiliate;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\AffiliateAwardSearch;
use common\models\AffiliateAward;
use Yii;

/**
 *
 */
class AffiliateAwardController extends AdminController
{
    protected $viewPath = '@backend/views/affiliate/affiliate-award';

    public function actionIndex()
    {
        AdminAccess::validateAccess('affiliate.view');

        $searchModel  = new AffiliateAwardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single row.
     * @param integer $id
     * @return mixed
     * @throws \yii\base\UserException
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess('affiliate.view');
        $affiliateSource = AffiliateAward::find()->where(['id' => $id])->one();

        return $this->render('view', [
            'model' => $affiliateSource,
        ]);
    }
}
