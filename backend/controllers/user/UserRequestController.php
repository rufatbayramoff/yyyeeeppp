<?php

namespace backend\controllers\user;

use backend\components\AdminAccess;
use backend\models\search\UserRequestSearch;
use common\components\Emailer;
use common\components\exceptions\BusinessException;
use common\interfaces\Model3dBaseInterface;
use common\models\Model3d;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\StoreUnit;
use common\models\UserRequest;
use common\models\StoreOrder;
use common\models\User;
use common\services\UserProfileService;
use DateTime;
use DateTimeZone;
use yii\base\UserException;

/**
 * UserRequestController - Requests to delete, restore user account - for now
 * other request can be added here.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserRequestController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/user/user-request';

    public $accessGroup = 'user_request';

    public function init()
    {
        parent::init();
        $this->searchModel = new UserRequestSearch();
        $this->mainModel = new UserRequest();
    }

    /**
     *
     * @param int $id
     * @return string
     */
    public function actionAccountDelete($id)
    {
        $userRequest = UserRequest::findByPk($id);
        if ($userRequest->status != UserRequest::STATUS_NEW) {
            $this->setFlashMsg(false, "Status not new. Cannot be approved");
            return $this->redirect(['user/user-request']);
        }
        if ($userRequest->request_type == UserRequest::REQUEST_TYPE_DELETE) {
            AdminAccess::validateAccess('user_request.account_delete');
            $this->deleteUser($userRequest);
        } else {
            if ($userRequest->request_type == UserRequest::REQUEST_TYPE_RESTORE) {
                AdminAccess::validateAccess('user_request.account_restore');
                $this->restoreUser($userRequest);
            }
        }
        app('cache')->set('back.notify.data', '');
        $this->setFlashMsg(true, "Done");
        return $this->redirect(['user/user-request']);
    }

    /**
     *
     * @param int $id
     * @return string
     */
    public function actionAccountRestore($id)
    {
        AdminAccess::validateAccess('user_request.account_restore');

        $userRequest = UserRequest::findByPk($id);
        if ($userRequest->request_type == UserRequest::REQUEST_TYPE_DELETE && $userRequest->status == UserRequest::STATUS_DONE) {
            $this->restoreUser($userRequest);
            $this->setFlashMsg(true, "Done. Link to restore account was emailed to user.");
        } else {
            $this->setFlashMsg(false, "This account cannot be restored. Not a delete request or wrong status.");
        }
        return $this->redirect(['user/user-request']);
    }

    /**
     *
     * @param int $id
     * @return string
     */
    public function actionCancel($id)
    {
        $userRequest = UserRequest::findByPk($id);
        if ($userRequest->status != UserRequest::STATUS_NEW) {
            $this->setFlashMsg(false, "Status not new. Cannot be cancelled");
            return $this->redirect(['user/user-request']);
        }
        AdminAccess::validateAccess('user_request.cancel');
        $dbtr = app('db')->beginTransaction();
        try {
            $currentUser = $this->getCurrentUser();
            UserRequest::updateRecord(
                $userRequest->id,
                [
                    'moderator_id' => $currentUser->id,
                    'status'       => UserRequest::STATUS_CANCEL
                ]
            );
            User::updateRecord($userRequest->user_id, ['status' => User::STATUS_ACTIVE]);
            $dbtr->commit();
            $this->setFlashMsg(true, "Done");
            app('cache')->set('back.notify.data', '');
        } catch (\Exception $e) {
            $dbtr->rollBack();
            $this->setFlashMsg(false, $e->getMessage());
        }
        return $this->redirect(['user/user-request']);
    }

    /**
     *
     * @param UserRequest $userRequest
     * @throws UserException
     */
    private static function restoreUser(UserRequest $userRequest)
    {
        $emailer = new Emailer();
        $emailer->sendRestoreRequest($userRequest);

        $userRequest->status = 'closed';
        $userRequest->save();
    }

    /**
     * check if we can delete this user,
     * we check orders for his printer
     *
     * @param \common\models\User $user
     * @throws UserException
     */
    private static function validateDelete(\common\models\User $user)
    {
        $existUnclosedOrders = StoreOrder::find()
            ->forUser($user)
            ->forState(StoreOrder::STATE_PROCESSING)
            ->exists();

        if ($existUnclosedOrders) {
            throw new BusinessException(_t('site.error', 'This account cannot be deleted because it still has pending/incomplete orders.'));
        }
    }

    /**
     * use want to delete himself from the system
     * 1. we check if he has print jobs
     * 2. we delete
     *
     * @param UserRequest $userRequest
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws UserException
     */
    private static function deleteUser(UserRequest $userRequest)
    {
        $currentUser = app('user')->getIdentity();
        $userProfileService = \Yii::createObject(UserProfileService::class);

        $dbtr = app('db')->beginTransaction();
        try {
            $user = $userRequest->user;
            // validate delete
            self::validateDelete($user);

            $userProfileService->deleteAccount($user);

            // log moderator
            UserRequest::updateRecord(
                $userRequest->id,
                [
                    'moderator_id' => $currentUser->id,
                    'status'       => UserRequest::STATUS_DONE
                ]
            );
            // maybe send email with link to complete delete request?

            $dbtr->commit();
        } catch (UserException $e) {
            $dbtr->rollBack();
            throw $e;
        } catch (\Exception $ex) {
            $dbtr->rollBack();
            logException($ex, 'tsdebug');
            throw new BusinessException(
                _t("site.error", "Server error [{time}]. Please contact us to find out problem", ['time' => time()])
            );
        }
        return true;

    }
}
