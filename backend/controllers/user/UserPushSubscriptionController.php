<?php

namespace backend\controllers\user;

use backend\models\search\BrowserPushSubscriptionUserSearch;
use backend\models\search\UserCommentSearch;
use common\models\BrowserPushSubscriptionUser;
use Yii;

/**
 */
class UserPushSubscriptionController extends \backend\components\CrudController
{

    protected $viewPath = '@backend/views/user/user-push-subscription';

    public function init()
    {
        parent::init();

        $this->searchModel = new BrowserPushSubscriptionUserSearch();
        $this->mainModel = new BrowserPushSubscriptionUser();
    }
}
