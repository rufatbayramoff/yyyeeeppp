<?php

namespace backend\controllers\user;

use backend\components\AdminAccess;
use backend\models\search\UserSearch;
use common\components\exceptions\BusinessException;
use common\components\exceptions\OnlyPostRequestException;
use common\components\PaymentExchangeRateFacade;
use common\components\UuidHelper;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentCurrency;
use common\models\PaymentDetail;
use common\models\PaymentTransaction;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrder;
use common\models\User;
use common\models\UserTaxInfo;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\payment\processors\PayPalPayoutProcessor;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use common\modules\payment\services\PayoutService;
use common\services\UserProfileService;
use frontend\models\user\FrontUser;
use frontend\modules\preorder\components\PreorderService;
use lib\money\Currency;
use lib\money\Money;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends \backend\components\CrudController
{

    protected $viewPath = '@backend/views/user';

    /** @var PreorderService */
    protected $preorderService;

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /** @var UserProfileService */
    protected $userProfileService;


    public function init()
    {
        parent::init();
        /** @var UserSearch * */
        $this->searchModel = new UserSearch();
        $this->mainModel = new \common\models\User();
        $this->preorderService = Yii::createObject(PreorderService::class);
        $this->paymentAccountService = Yii::createObject(PaymentAccountService::class);
        $this->userProfileService = Yii::createObject(UserProfileService::class);
    }

    public function actionUpdate($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.update');

        /** @var $model User */
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        app('cache')->set('back.notify.data', '');
        $this->trigger(
            self::EVENT_BEFORE_MODEL_LOAD, new Event(['sender' => ['model' => $model, 'postData' => $post]])
        );
        if ($model->load($post)) {

            if (empty($model->access_token)) {
                $model->access_token = null;
            }
            $this->trigger(
                self::EVENT_BEFORE_UPDATE, new Event(['sender' => $model])
            );
            if ($model->save()) {
                if ($model->status == User::STATUS_ACTIVE) {
                    $this->preorderService->confirmAllNotConfirmed($model);
                }

                return $this->redirect(['index']);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');
        /** @var User $model */
        $model = $this->findModel($id);
        $anonymousOrdersProvider = new ActiveDataProvider(['query' => StoreOrder::find()->anonymousOrders()->deliveryEmail($model->email)]);

        return $this->render('view', [
            'model' => $model,
            'anonymousOrdersProvider' => $anonymousOrdersProvider
        ]);
    }

    public function actionPayout()
    {
        $user = User::tryFindByPk(app()->request->post('user_id'));
        $withdrawMoney = Money::create(app()->request->post('amount'), Currency::USD);

        try {
            /** @var PayPalPayoutProcessor $payPalPayoutProcessor */
            $payPalPayoutProcessor = Yii::createObject(PayPalPayoutProcessor::class);
            $payPalPayoutProcessor->payOut($user, $withdrawMoney);

            $this->setFlashMsg(true, _t('front.site', 'Payout accepted'));

            UserTaxInfo::taxNotifyOff($user);
        } catch (\Exception $e) {
            \Yii::error($e);
            $this->setFlashMsg(false, $e->getMessage());
        }

        return $this->redirect(['user/user/view', 'id' => $user->id]);
    }

    /**
     *
     * @param int $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     * @throws BusinessException
     */
    public function actionDelete($id)
    {
        throw new BusinessException("For deleting user use Deactivate accout request");
    }

    /**
     * view user updates logs
     *
     *
     * @return string
     */
    public function actionLogs()
    {
        $searchModel = new \backend\models\search\UserLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'user-log/index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }


    /**
     * view user logins using login form
     * ip/agent/time
     *
     * @return string
     */
    public function actionLogins()
    {
        $searchModel = new \backend\models\search\UserLoginLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'user-login-log/index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @param $userId
     * @throws \Exception
     */
    public function actionLogin($userId)
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $key = UuidHelper::generateRandCode(15);
        Yii::$app->cache->set('loginAsKey', ['uuid' => $key, 'userId' => $userId], 60);
        $url = \Yii::$app->params['siteUrl'] . '/user/user/login-as?key=' . $key;
        $this->redirect($url);
    }

    /**
     * @param $userId
     * @return Response
     * @throws \yii\web\NotFoundHttpException|null
     */
    public function actionDocumentImage($userId)
    {
        if (!\backend\components\AdminAccess::can('psprintertest.docs')) {
            return null;
        }

        /** @var User $user */
        $user = User::tryFindByPk($userId);
        return Yii::$app->response->sendFile($user->getLastDocument()->file->getLocalTmpFilePath(), null, ['inline' => true]);
    }

    /**
     * @param $userId
     * @param $from
     * @param $to
     * @param bool $pdf
     *
     * @return string
     * @throws UserException
     * @throws \common\components\exceptions\DeprecatedException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionReport($userId, $from, $to, $pdf = false)
    {
        $this->view->title = _t('site.payments', 'Financial Report');
        $user = User::findByPk($userId);
        $this->layout = '@backend/views/yii2-app/layouts/plain.php';
        $limit = 100000;

        $paymentDetails = $this->paymentAccountService->getPaymentDetailsProvider($user, 'income', $from, $to, $limit);
        $paymentsOutDetails =  $this->paymentAccountService->getPaymentDetailsProvider($user, 'payout', $from, $to, $limit);

        $fromDate = date("Y-m-d", strtotime($from));
        $toDate = date("Y-m-d", strtotime($to));

        $accountsBalanceEx = $this->paymentAccountService->calculateAccountsBalanceEx(PaymentAccount::find()->where(['user_id' => $user->id, 'type' => PaymentAccount::ACCOUNT_TYPE_MAIN]));

        $to = null; // disable total to filter by date.
        return $this->render('@frontend/modules/workbench/views/payments/paymentsReport.php', [
            'user'               => $user,
            'paymentDetails'     => $paymentDetails,
            'paymentsOutDetails' => $paymentsOutDetails,
            'fromDate'           => $fromDate,
            'toDate'             => $toDate,
            'pdf'                => true,
            'totalIncome'        => displayAsMoney($accountsBalanceEx[0]->balancePlus),
            'totalWithdraw'      => displayAsMoney($accountsBalanceEx[0]->balanceMinus),
            'totalBalance'       => displayAsMoney($this->paymentAccountService->getUserMainAmount($user))
        ]);
    }

    public function actionWipe($userId)
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }

        $user = FrontUser::findByPk($userId);
        $this->userProfileService->deleteAccount($user, true);
        $this->setFlashMsg(true, _t('front.site', 'Was wiped'));

        $this->redirect('/user/user/view?id=' . $user->id);
    }
}
