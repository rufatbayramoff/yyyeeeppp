<?php

namespace backend\controllers\user;

use backend\models\search\BrowserPushSubscriptionUserSearch;
use backend\models\search\NotifyPopupSearch;
use common\models\BrowserPushSubscriptionUser;
use common\models\NotifyPopup;

/**
 */
class UserNotificationController extends \backend\components\CrudController
{

    protected $viewPath = '@backend/views/user/user-notification';

    public function init()
    {
        parent::init();

        $this->searchModel = new NotifyPopupSearch();
        $this->mainModel = new NotifyPopup();
    }
}
