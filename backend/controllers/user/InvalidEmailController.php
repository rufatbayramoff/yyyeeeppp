<?php

namespace backend\controllers\user;

use backend\components\AdminAccess;
use backend\models\search\InvalidEmailSearch;
use backend\models\search\TsCertificationClassSearch;
use backend\models\search\UserSearch;
use common\components\exceptions\BusinessException;
use common\components\exceptions\OnlyPostRequestException;
use common\components\PaymentExchangeRateFacade;
use common\components\UuidHelper;
use common\models\InvalidEmail;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentCurrency;
use common\models\PaymentDetail;
use common\models\PaymentTransaction;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrder;
use common\models\TsCertificationClass;
use common\models\User;
use common\models\UserTaxInfo;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\payment\processors\PayPalPayoutProcessor;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use common\modules\payment\services\PayoutService;
use common\services\UserProfileService;
use frontend\models\user\FrontUser;
use frontend\modules\preorder\components\PreorderService;
use lib\money\Currency;
use lib\money\Money;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * InvalidEmailController
 */
class InvalidEmailController extends \backend\components\AdminController
{
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('user.view');
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $invalidEmailSearch = Yii::createObject(InvalidEmailSearch::class);
        $dataProvider         = $invalidEmailSearch->search(Yii::$app->request->queryParams);
        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel'=>$invalidEmailSearch]);
    }

    public function actionUpdate($id)
    {
        $invalidEmail = InvalidEmail::tryFindByPk($id);
        if (Yii::$app->request->isPost) {
            $invalidEmail->load(Yii::$app->request->post());
            if ($invalidEmail->validate()) {
                $invalidEmail->safeSave();
                return $this->redirect('/user/invalid-email');
            } else {
                $this->setFlashMsg(false, Html::errorSummary($invalidEmail));
            }
        }
        return $this->render('update', ['invalidEmail' => $invalidEmail]);
    }
}
