<?php 
namespace backend\controllers\user;

use backend\components\CrudController;
use backend\models\search\UserAccessSearch;
use common\models\UserAccess;

/**
 * UserAccessController implements the CRUD actions for UserAccess model.
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserAccessController extends CrudController
{
    protected $viewPath = '@backend/views/user/user-access';
   
    public function init()
    {
        parent::init();
        $this->searchModel = new UserAccessSearch();
        $this->mainModel = new UserAccess();
    }
}
