<?php 

namespace backend\controllers\user;

use Yii;
use common\models\Access;
use backend\models\search\AccessSearch;

/**
 * AccessController implements the CRUD actions for Access model.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 * 
 */
class AccessController extends \backend\components\CrudController
{

    protected $viewPath = '@backend/views/sys/access';

    public function init()
    {
        parent::init();
        $this->searchModel = new AccessSearch();
        $this->mainModel = new Access();
    }

    /**
     * create access group
     * @return type
     */
    public function actionCreateGroup()
    {
        $model = new \common\models\AccessGroup();
        $formConfig = [];
        if($this->isAjax){
            $formConfig['enableAjaxValidation'] =  true;
        }
        
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            if ($model->save()) {
                if($this->isAjax){
                    return $this->jsonSuccess(['message'=>'Group created']) ;
                }
                Yii::$app->getSession()->setFlash('success', 'Group created');            
                return $this->redirect(['index']);
            }else{
                if(count($model->getErrors()) > 0){
                    $msg = \yii\helpers\Html::errorSummary($model);
                    return $this->jsonError($msg);
                }                
            }
        }
        return $this->renderPartial('createGroup', [
            'model' => $model, 'formConfig' => $formConfig
        ]);
    }
}
