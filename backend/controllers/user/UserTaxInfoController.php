<?php

namespace backend\controllers\user;

use backend\models\user\UserTaxInfo;
use common\modules\informer\InformerModule;
use common\modules\informer\models\TaxInformer;
use yii;
use yii\base\UserException;

/**
 * UserTaxInfoController implements the CRUD actions for UserTaxInfo model.
 */
class UserTaxInfoController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/user/user-tax-info';

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\UserTaxInfoSearch();
        $this->mainModel = new \backend\models\user\UserTaxInfo();
    }

    /**
     * Approve tax form
     *
     * @param $id
     *
     * @return yii\web\Response
     * @throws UserException
     * @throws yii\base\InvalidConfigException
     * @throws yii\web\NotFoundHttpException
     */
    public function actionApprove($id)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        \backend\components\AdminAccess::validateAccess('usertaxinfo.approve');
        $model = $this->findModel($id);
        if ($model->approve()) {
            $this->setFlashMsg(true, 'Tax information approved');
        }
        app('cache')->set('back.notify.data', '');
        InformerModule::addInformer($model->user, TaxInformer::class);

        return $this->redirect(['user/user-tax-info', 'id' => $id]);
    }

    /**
     * Decline tax form
     *
     * @param $id
     *
     * @return string|yii\web\Response
     * @throws UserException
     * @throws yii\base\InvalidConfigException
     * @throws yii\db\Exception
     * @throws yii\web\NotFoundHttpException
     */
    public function actionDecline($id)
    {
        \backend\components\AdminAccess::validateAccess($this->accessGroup . '.reject');
        /** @var UserTaxInfo $model */
        $model = $this->findModel($id);
        if (app('request')->isPost) {
            $msg = Yii::$app->request->post('TaxFormDeclineForm')['reasonDescription'];
            if ($model->decline($msg)) {
                $this->setFlashMsg(true, 'Declined OK');
            }
            InformerModule::addInformer($model->user, TaxInformer::class);
            app('cache')->set('back.notify.data', '');
            return $this->redirect(['user/user-tax-info/view', 'id' => $id]);
        }
        return $this->renderAdaptive('decline', ['id' => $id]);
    }

    /**
     * @param int $id
     * @param bool $moderate
     *
     * @return mixed|string
     * @throws UserException
     * @throws yii\web\NotFoundHttpException
     */
    public function actionView($id, $moderate = false)
    {
        \backend\components\AdminAccess::validateAccess($this->accessGroup . '.view');
        $model = $this->findModel($id);
        if ($moderate) {
            \backend\components\AdminAccess::validateAccess($this->accessGroup . '.moderate');
            if ($model->status != \common\models\UserTaxInfo::STATUS_REVIEW) {
                $model->status = \common\models\UserTaxInfo::STATUS_REVIEW;
                $this->setFlashMsg(false, 'Status is in REVIEW now. Please click APPROVE or DECLINE to complete moderation.');
                $model->save();
            }
        }
        $paramsGet = app('request')->queryParams;
        $historyModel = new \backend\models\search\UserTaxInfoHistorySearch();
        $historyProvider = $historyModel->search(
            \yii\helpers\ArrayHelper::merge(
                isset($paramsGet['UserTaxInfoHistorySearch']) ? $paramsGet['UserTaxInfoHistorySearch'] : [],
                ['UserTaxInfoHistorySearch' => ['user_tax_info_id' => $id]]
            )
        );

        $searchModelLogin = new \backend\models\search\UserLoginLogSearch();
        $dataProviderLogin = $searchModelLogin->search(
            \yii\helpers\ArrayHelper::merge(
                isset($paramsGet['UserLoginLogSearch']) ? $paramsGet['UserLoginLogSearch'] : [],
                ['UserLoginLogSearch' => ['user_id' => $model->user_id]]
            )
        );

        return $this->render(
            'view',
            [
                'model'    => $model,
                'moderate' => $moderate,
                'loginlog' => [
                    'searchModel'  => $searchModelLogin,
                    'dataProvider' => $dataProviderLogin,
                ],
                'history'  => [
                    'searchModel'  => $historyModel,
                    'dataProvider' => $historyProvider,
                ]
            ]
        );
    }
}
