<?php

namespace backend\controllers\user;

use backend\models\search\UserCommentSearch;
use Yii;

/**
 */
class UserCommentController extends \backend\components\CrudController
{

    protected $viewPath = '@backend/views/user/user-comment';

    public $accessGroup = 'user_comment';

    public function init()
    {
        parent::init();

        $this->searchModel = new UserCommentSearch();
        $this->mainModel = new \common\models\UserComment();
    }
}
