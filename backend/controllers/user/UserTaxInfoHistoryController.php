<?php

namespace backend\controllers\user;

use Yii; 

/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UserTaxInfoHistoryController  extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/user/user-tax-info-history';
    
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\UserTaxInfoHistorySearch();
        $this->mainModel = new \common\models\UserTaxInfoHistory();
    }
}
