<?php

namespace backend\controllers\moderator;

use Yii;
use yii\base\UserException;

/**
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserAdminGroupController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/moderator/user-admin-group';
    protected $accessGroup = 'access';   
    
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\UserAdminGroupSearch();
        $this->mainModel = new \common\models\UserAdminGroup();
    }
     
     /**
     * get view user access list
     * 
     * @param int $groupId
     * @return string
     */
    public function actionViewAccess($groupId)
    {
        $this->view->title = 'Update Group Access';
        \backend\components\AdminAccess::validateAccess('system.view_access');
        $userGroup = \common\models\UserAdminGroup::tryFindByPk($groupId);
        return $this->render('groupAccess', [
            'userGroup' => $userGroup,
        ]);
    }

    /**
     *
     * @param int $groupId
     * @return \yii\web\Response
     * @throws \yii\base\UserException
     */
    public function actionUpdateAccess($groupId)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        \backend\components\AdminAccess::validateAccess('system.edit_access');
        
        $accessForm = new \backend\models\access\AccessForm();
        $accessForm->load(app('request')->post());
        
        $userGroup = \common\models\UserAdminGroup::tryFindByPk($groupId);
        $accessForm->updateGroupAccessList($userGroup);        
        $this->setFlashMsg(true, 'Accesses updated');
        
        return $this->redirect(['moderator/user-admin-group/view-access', 'groupId'=>$groupId]);
    }
    
    public function actionDelete($id)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        \backend\components\AdminAccess::validateAccess($this->accessGroup . '.delete');
        // delete access rights first
        $dbtr = app('db')->beginTransaction();
        try{
            $model = $this->findModel($id);
            if($model->userAdminGroupAccesses){
                foreach($model->userAdminGroupAccesses as $grAccess){
                    $grAccess->delete();
                }
            }
            $model->delete();     
            $dbtr->commit();
        }catch(\Exception $e){
            $dbtr->rollBack();
            $this->setFlashMsg(false, $e->getMessage());
        }
        return $this->redirect(['index']);
    }
}
