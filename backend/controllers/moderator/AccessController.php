<?php namespace backend\controllers\moderator;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class AccessController extends \backend\components\CrudController
{
    
    protected $viewPath = '@backend/views/moderator';
    public function init()
    {
        parent::init();
        $this->view->title = 'Moderator accesses';
    }
    
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge([ 
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'update-access' => ['post']
                ],
            ],
        ], parent::behaviors());
    }
    
    /**
     * 
     * @return string
     */
    public function actionIndex()
    {        
        $searchModel = new \backend\models\search\UserAdminSearch();
        $dataProvider = $searchModel->search(app('request')->queryParams);
        return $this->render('user-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }
    
    /**
     * get view user access list
     * 
     * @param int $userId
     * @return string
     */
    public function actionViewAccess($userId)
    {
        \backend\components\AdminAccess::validateAccess('system.view_access');
        $user = \common\models\UserAdmin::tryFindByPk($userId);
        return $this->render('access-list', [
            'user' => $user,
        ]);
    }
    
    /**
     * 
     * @param int $userId     
     */
    public function actionUpdateAccess($userId)
    {
        \backend\components\AdminAccess::validateAccess('system.edit_access');
        
        $accessForm = new \backend\models\access\AccessForm();
        $accessForm->load(app('request')->post());
        
        $user = \common\models\UserAdmin::tryFindByPk($userId);
        $accessForm->updateAccessList($user);        
        $this->setFlashMsg(true, 'Accesses updated');
        
        return $this->redirect(['moderator/access/view-access', 'userId'=>$userId]);
    }
}
