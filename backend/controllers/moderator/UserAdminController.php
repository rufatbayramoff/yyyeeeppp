<?php

namespace backend\controllers\moderator;

use Yii;
use backend\components\AdminAccess;

/**
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserAdminController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/moderator/user-admin';
   
    protected $accessGroup = 'access';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\UserAdminSearch();
        $this->mainModel = new \common\models\UserAdmin();
    }
    public function actionIndex()
    {
        return $this->redirect(['moderator/access']);
    }

    public function actionCreateUsers()
    {
        $moders =  \common\models\UserAdmin::find()->all();
        $created = 0;
        foreach($moders as $moder){
            $oldUser = \common\models\User::findByPk($moder->id);
            if($oldUser){
                continue;
            }
            \common\models\User::addRecord([
               "id" => $moder->id,
               "username" => $moder->username . "_moderator",
               "auth_key" => "-",
               'password_hash' => '-',
               'email' => $moder->username . "-moderator@treatstock.com",
               'status' => 10,
               'created_at' => 0,
               'updated_at' => 0,
               'lastlogin_at' => 0,
               'trustlevel' => 'high'
           ]);
            $created++;
        }
        $this->setFlashMsg(true, "$created users created");
        return $this->redirect(["moderator/access"]);
    }
    public function actionCreate()
    {
        $model = $this->mainModel;
        $post = app('request')->post();
        AdminAccess::validateAccess($this->accessGroup . '.add');
        $formConfig = [];
        if ($model->load($post)) {
            $dbtr = app('db')->beginTransaction();
            try{
                $model->auth_key = Yii::$app->getSecurity()->generateRandomString();
                $model->created_at = time();
                $model->updated_at = time();
                $model->setPassword($post['UserAdmin']['password']);

                if ($model->save()) {
                    $this->setFlashMsg(true, 'Successfully created');

                }else{
                    $msg = \yii\helpers\Html::errorSummary($model);
                    $this->setFlashMsg(false, $msg);
                }
                $userId = $model->id;
                $siteUser = \common\models\User::findByPk($userId);
                if(!$siteUser){
                    \common\models\User::addRecord([
                       "id" => $userId,
                       "username" => $model->username .  "_moderator" ,
                       "auth_key" => "-",
                       'password_hash' => '-',
                       'email' => $model->username  . '-moderator@treatstock.com',
                       'status' => 10,
                       'created_at' => 0,
                       'updated_at' => 0,
                       'lastlogin_at' => 0,
                       'trustlevel' => 'high'
                   ]);
                }
                $dbtr->commit();
                if ($model->group_id) {
                    \backend\models\access\AccessForm::updateUserInGroupAccess($model->group_id, $model->id);
                }
                return $this->redirect(['index']);
            }catch(\Exception $e){
                $dbtr->rollBack();
            }

        }  
        return $this->render('create', [
                'model' => $model, 'formConfig' => $formConfig
        ]);
    }
    
    public function actionUpdate($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.update');
        $model = $this->findModel($id);
        $post = app('request')->post();
        $oldGroup = $model->group_id;
        if(app('request')->isPost){
            $password = $post['UserAdmin']['password'];
            unset($post['UserAdmin']['password']);
            if ($model->load($post)) {
                if(!empty($password)){
                    $model->setPassword($password);
                }
                if ($model->save()) {
                    if (!empty($model->group_id) && $oldGroup != $model->group_id) { // group updated
                        \backend\models\access\AccessForm::updateUserInGroupAccess($model->group_id, $id);
                    }
                    return $this->redirect(['moderator/access']);
                }
            }
        }
        return $this->render('update', [
                'model' => $model,
        ]);
    }
}
