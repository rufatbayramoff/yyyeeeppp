<?php

namespace backend\controllers\moderator;

use yii;
/**
 * ModerLogController implements the CRUD actions for ModerLog model.
 */
class ModerLogController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/moderator/moder-log';
    
    
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\ModerLogSearch();
        $this->mainModel = new \common\models\ModerLog();
    }
}
