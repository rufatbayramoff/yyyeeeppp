<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.05.18
 * Time: 10:09
 */

namespace backend\controllers\product;

use backend\components\AdminController;
use common\modules\product\actions\ActionGetCountriesList;

/**
 *
 */
class AjaxController extends AdminController
{
    public function actions()
    {
        return array_merge(parent::actions(), [
            'select-country' => ActionGetCountriesList::class
        ]);
    }
}