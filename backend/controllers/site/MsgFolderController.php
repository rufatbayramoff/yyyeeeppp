<?php

namespace backend\controllers\site;

use Yii;
use common\models\MsgFolder;
use backend\models\search\MsgFolderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MsgFolderController implements the CRUD actions for MsgFolder model.
 */
class MsgFolderController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/msg-folder';
    protected $accessGroup = 'setting';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\MsgFolderSearch();
        $this->mainModel = new \common\models\MsgFolder();
    }
}
