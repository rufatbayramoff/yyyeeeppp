<?php

namespace backend\controllers\site;

use Yii; 

/**
 * OsnVendorController implements the CRUD actions for OsnVendor model.
 */
class OsnVendorController extends \backend\components\CrudController
{
    
    protected $viewPath = '@backend/views/sys/osn-vendor';
    
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\OsnVendorSearch();
        $this->mainModel = new \common\models\OsnVendor();
    }
}
