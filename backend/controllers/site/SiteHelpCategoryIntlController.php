<?php
 

namespace backend\controllers\site;

/** 
 *
 * @author DeFacto
 */
  
class SiteHelpCategoryIntlController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/site-help-category-intl';
    protected $accessGroup = 'sitehelp';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\SiteHelpCategoryIntlSearch();
        $this->mainModel = new \common\models\SiteHelpCategoryIntl();
    }
}
