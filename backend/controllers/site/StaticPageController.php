<?php
/**
 * Created by mitaichik
 */

namespace backend\controllers\site;


use backend\components\AdminAccess;
use backend\models\search\StaticPageSearch;
use common\models\StaticPage;
use common\models\StaticPageIntl;
use yii\helpers\ArrayHelper;

class StaticPageController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/static-page';
    protected $accessGroup = 'static_page';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->searchModel = new StaticPageSearch();
        $this->mainModel = new StaticPage();
    }

    public function actionUpdate($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.update');
        $model = $this->findModel($id);
        $postData = $this->formModelLoadData($model);
        if ($model->load($postData)) {
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Successfully saved.');
            } else {
                \Yii::$app->getSession()->setFlash('error', $model->getFirstError());
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param int $id
     * @return StaticPage
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = StaticPage::find()->where(['id' => $id])->one();
        if(!$model){
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }

    /**
     * @param $id
     * @param $lang
     */
    public function actionClearLanguage($id, $lang)
    {
        AdminAccess::validateAccess($this->accessGroup . '.update');
        $page = $this->findModel($id);
        foreach ($page->staticPageIntls as $translation){
            if ($translation->lang_iso == $lang){
                $translation->delete();
            }
        }
    }
}