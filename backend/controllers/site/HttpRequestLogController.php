<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.10.16
 * Time: 15:54
 */

namespace backend\controllers\site;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\ApiExternalSystemSearch;
use backend\models\search\HttpRequestLogSearch;
use common\models\ApiExternalSystem;
use common\models\base\HttpRequest;
use common\models\HttpRequestLog;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ApiExternalSystemController implements the CRUD actions for ApiExternalSystem model.
 */
class HttpRequestLogController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * Lists all ApiExternalSystem models.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('httplog.view');
        $searchModel = new HttpRequestLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApiExternalSystem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess('httplog.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the ApiExternalSystem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApiExternalSystem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HttpRequestLog::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
