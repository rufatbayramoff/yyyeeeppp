<?php

namespace backend\controllers\site;

/**
 * SiteBlockController implements the CRUD actions for SiteBlock model.
 */
class SiteBlockController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/site-block';
   
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\SiteBlockSearch();
        $this->mainModel = new \common\models\SiteBlock();
    }
}
