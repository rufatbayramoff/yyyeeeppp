<?php

namespace backend\controllers\site;

use common\modules\homePage\factories\HomePageProductFactory;
use Yii;
use common\models\HomePageCategoryCard;
use backend\models\search\HomePageCategoryCardSearch;
use backend\components\AdminController;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HomePageCategoryCardController implements the CRUD actions for HomePageCategoryCard model.
 */
class HomePageCategoryCardController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HomePageCategoryCard models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HomePageCategoryCardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HomePageCategoryCard model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HomePageCategoryCard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return string|\yii\web\Response
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function actionCreate()
    {
        /** @var HomePageProductFactory $productBlockFactory */
        $productBlockFactory = Yii::createObject(HomePageProductFactory::class);
        $productBlockCategory = $productBlockFactory->createCategoryCard();

        if ($productBlockCategory->loadData(Yii::$app->request->post()) && $productBlockCategory->saveCategoryCard()) {
            return $this->redirect(['view', 'id' => $productBlockCategory->id]);
        } else {
            return $this->render('create', [
                'model' => $productBlockCategory,
            ]);
        }
    }

    /**
     * Updates an existing HomePageCategoryCard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->setScenario(HomePageCategoryCard::SCENARIO_UPDATE);

        if ($model->loadData(Yii::$app->request->post()) && $model->saveCategoryCard()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing HomePageCategoryCard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HomePageCategoryCard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HomePageCategoryCard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HomePageCategoryCard::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
