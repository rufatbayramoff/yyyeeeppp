<?php namespace backend\controllers\site;

use backend\models\search\SystemLangMessageSearch;
use common\components\DateHelper;
use common\models\SystemLangMessage;
use common\models\SystemLangSource;
use Yii;
use yii\base\UserException;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;

/**
 *
 * Translation module in backend
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 *
 */
class LangController extends \backend\components\AdminController
{

    protected $viewPath = '@backend/views/lang';

    public function actionIndex()
    {
        $searchModel = new SystemLangMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (Yii::$app->request->post('hasEditable')) {
            $combinedKey = Json::decode(Yii::$app->request->post('editableKey'));
            return $this->ajaxUpdate($combinedKey);
        }

        // non-ajax - render the grid by default
        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel'  => $searchModel
            ]
        );
    }

    /**
     *
     * @param array $combinedKey
     * @return type
     */
    private function ajaxUpdate(array $combinedKey)
    {
        $model = SystemLangMessage::findOne($combinedKey);
        $out = Json::encode(['output' => '', 'message' => '']);
        $post = [];
        $posted = current(Yii::$app->request->post('SystemLangMessageSearch'));
        $posted['is_auto_translate'] = 0;
        $post['SystemLangMessage'] = $posted;
        if ($model->load($post)) {
            $model->updated_at = DateHelper::now();
            $model->translated_by = $this->getCurrentUser()->id;
            $model->save();
            $output = '';
            $out = Json::encode(['output' => $output, 'message' => '']);
        }
        return $out;
    }

    /**
     * Displays a single SystemLangMessage model.
     *
     * @param integer $id
     * @param string $language
     * @return mixed
     */
    public function actionView($id, $language)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id, $language),
            ]
        );
    }

    public function actionPagesList($id)
    {
        $systemLangSource = SystemLangSource::tryFindByPk($id);
        $pagesList = $systemLangSource->systemLangPages;
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $pagesList
        ]);
        return $this->renderAjax(
            'pagesList',
            [
                'dataProvider' => $dataProvider
            ]
        );
    }

    public function actionChecked($id)
    {
        $res = SystemLangSource::tryFindByPk($id);

        $res->is_checked = 1;
        $res->save();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Creates a new SystemLangMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SystemLangMessage();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'language' => $model->language]);
        }
        return $this->render(
            'create',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Updates an existing SystemLangMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @param string $language
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws NotFoundHttpException
     * @throws UserException
     */
    public function actionUpdate($id, $language)
    {
        $model = $this->findModel($id, $language);

        if (app('request')->isPost && $model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'language' => $model->language]);
        }
        return $this->render(
            'update',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Deletes an existing SystemLangMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @param string $language
     * @return mixed
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \Exception
     */
    public function actionDelete($id, $language)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        $this->findModel($id, $language)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Form Js dictionary files
     *
     * @return \yii\web\Response
     * @throws UserException
     */
    public function actionFormJsFiles()
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        \Yii::$app->getModule('translation')->jsI18n->formAllTempAssetsFile();
        Yii::$app->getSession()->setFlash('success', _t('backend.lang', 'Javascript files was successfully saved.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the SystemLangMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @param string $language
     * @return SystemLangMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $language)
    {
        if (($model = SystemLangMessage::findOne(['id' => $id, 'language' => $language])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
