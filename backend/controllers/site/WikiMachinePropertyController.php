<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use Psr\Log\InvalidArgumentException;
use Yii;
use common\models\WikiMachineProperty;
use backend\models\search\WikiMachinePropertySearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * WikiMachinePropertyController implements the CRUD actions for WikiMachineProperty model.
 */
class WikiMachinePropertyController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        AdminAccess::validateAccess('website.wiki_machine');
        return parent::beforeAction($action);
    }

    /**
     * Lists all WikiMachineProperty models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WikiMachinePropertySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single WikiMachineProperty model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new WikiMachineProperty model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WikiMachineProperty();

        if (Yii::$app->request->get('wikiMachineId')) {
            $model->wiki_machine_id = Yii::$app->request->get('wikiMachineId');
        }

        return $this->renderAjax(
            'create',
            [
                'model' => $model,
            ]
        );
    }

    public function actionValidate()
    {

        if ($id = Yii::$app->request->get('id')) {
            $model = $this->findModel($id);
        } else {
            $model = new WikiMachineProperty();
        }
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    /**
     * Updates an existing WikiMachineProperty model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate()
    {
        /** @var WikiMachineProperty $model */
        if ($id = Yii::$app->request->get('id')) {
            $model = $this->findModel($id);
        } else {
            $model = new WikiMachineProperty();
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->safeSave();
            return $this->redirect('/site/wiki-machine/update?id=' . $model->wiki_machine_id . '#wikiMachineProperties');
        }
        return $this->renderAjax(
            'update',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Deletes an existing WikiMachineProperty model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect('/site/wiki-machine/update?id=' . $model->wiki_machine_id . '#wikiMachineProperties');
    }

    /**
     * Finds the WikiMachineProperty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return WikiMachineProperty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WikiMachineProperty::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
