<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use backend\components\AdminController;
use common\components\exceptions\BusinessException;
use common\components\hiredesigner\HireDesigner;
use common\components\hiredesigner\HireDesignerItem;
use common\components\hiredesigner\HireDesignerRepository;
use common\components\serizaliators\AbstractProperties;
use common\models\Model3d;
use common\models\model3d\CoverInfo;
use common\models\query\Model3dQuery;
use common\models\User;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\model3d\Model3dFacade;
use Yii;
use yii\base\UserException;
use yii\helpers\Json;
use yii\web\Response;

/**
 * HireDesignerController
 * @property HireDesignerRepository repository
 */
class HireDesignerController extends AdminController
{
    /**
     * @var HireDesignerRepository
     */
    private $repository;

    /**
     * @param HireDesignerRepository $repository
     */
    public function injectDependencies(HireDesignerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->enableCsrfValidation = false;
        AdminAccess::validateAccess('hiredesigner.view');
    }

    /**
     * Lists all SystemLang models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Return list of designers
     * @param string $query Query for username of designer
     * @param int[] $excludedIds Ids of designers who already in list
     * @return array
     * @throws BusinessException
     */
    public function actionDesigners($query, array $excludedIds = [])
    {
        if (mb_strlen($query) < 2){
            throw new BusinessException("Query is small");
        }

        $designers = User::find()
            ->select(User::column('*'))
            ->distinct()
            ->notSystem()
            ->active()
            ->excludeIds($excludedIds)
            ->likeUsername($query)
            ->joinWith(['model3ds' => function(Model3dQuery $query){
                $query->published();
            }], false)
            ->all();

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return HireDesignerProperties::serialize($designers);
    }

    /**
     * Return list of models for designer
     * @param int $designerId
     * @param int[] $excludedIds
     * @return array
     */
    public function actionModels($designerId, array $excludedIds = [])
    {
        $user = User::tryFindByPk($designerId);

        /** @var Model3d[] $models */
        $models = Model3d::find()
            ->storePublished($user)
            ->excludeIds($excludedIds)
            ->all();

        $result = [];
        foreach ($models as $model) {
            if ($model->isPublished()){
                $result[] = $model;
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return HireDesignerProperties::serialize($result);
    }

    /**
     * @return array
     */
    public function actionLoad()
    {
        $hireDesigner = $this->repository->load();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return HireDesignerProperties::serialize($hireDesigner);
    }


    /**
     *
     */
    public function actionSave()
    {
        $data = Json::decode(Yii::$app->request->getRawBody());
        $hireDesigner = $this->createHireDesignerFromPost($data);
        $this->repository->save($hireDesigner);
    }

    /**
     * @param array $data
     * @return HireDesigner
     */
    protected function createHireDesignerFromPost(array $data)
    {
        $hireDesigner = new HireDesigner();

        foreach ($data['items'] as $itemData){
            $item = new HireDesignerItem();
            $item->designer = User::tryFindByPk($itemData['designer']['id']);
            foreach ($itemData['models'] as $modelData) {
                $item->models[] = Model3d::tryFindByPk($modelData['id']);
            }
            $hireDesigner->items[] = $item;
        }
        return $hireDesigner;
    }
}

/**
 * Class DesignerProperties
 * @package backend\controllers\site
 */
class HireDesignerProperties extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            User::class => [
                'id',
                'username'
            ],
            Model3d::class => [
                'id',
                'title',
                'coverUrl' => function(Model3d $model) {
                    $cover = new CoverInfo(Model3dFacade::getCover($model));
                    return ImageHtmlHelper::getThumbUrl($cover->image, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
                }
            ],
        ];
    }
}
