<?php


namespace backend\controllers\site;

use common\interfaces\FileBaseInterface;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use common\models\SiteHelpCategory;
use yii\base\Event;
use yii\web\UploadedFile;

/**
 *
 * @author DeFacto
 */
class SiteHelpCategoryController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/site-help-category';
    protected $accessGroup = 'sitehelp';


    /**
     * @var FileFactory
     */
    private $fileFactory;

    /** @var FileRepository */
    public $fileRepository;


    /**
     * @param FileFactory $fileFactory
     */
    public function injectDependencies(FileFactory $fileFactory, FileRepository $fileRepository)
    {
        $this->fileFactory = $fileFactory;
        $this->fileRepository = $fileRepository;
    }

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\SiteHelpCategorySearch();
        $this->mainModel = new \common\models\SiteHelpCategory();

        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'uploadCoverFile']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'uploadCoverFile']);
    }

    /**
     * Upload and save cover file
     *
     * @param Event $event
     * @throws \Exception
     */
    public function uploadCoverFile(Event $event): void
    {
        /** @var SiteHelpCategory $model */
        $model = $event->sender;

        if ($coverFile = UploadedFile::getInstance($model, 'coverFile')) {
            $file = $this->fileFactory->createFileFromUploadedFile($coverFile, FileBaseInterface::TYPE_FILE_ADMIN);
            $file->setPublicMode(true);
            $file->setOwner(SiteHelpCategory::class, 'cover_file_id');
            $file->setFixedPath('site-help');
            $this->fileRepository->save($file);

            $model->cover_file_uuid = $file->uuid;
            $file->safeSave();
        }
    }
}
