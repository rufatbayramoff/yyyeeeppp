<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use backend\models\search\SystemLangMessageSearch;
use common\components\ArrayHelper;
use common\models\SystemLang;
use common\models\SystemLangMessage;
use common\models\SystemLangSource;
use common\modules\translation\models\I18nTableElement;
use common\modules\translation\models\search\I18nTableElementSearch;
use common\modules\translation\models\search\TranslateStatisticSearch;
use Yii;
use yii\base\UserException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;

class LangStatisticController extends \backend\components\AdminController
{

    protected $viewPath = '@backend/views/lang-statistic';


    public function actionIndex()
    {
        $searchModel = new TranslateStatisticSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (empty($queryParams[$searchModel->formName()]['userId'])) {
            $translatedByAllList                             = Yii::$app->getModule('translation')->statistics->getTranslatorUsersList();
            $firstTranslator                                 = ArrayHelper::arrayKeyFirst($translatedByAllList);
            $queryParams[$searchModel->formName()]['userId'] = $firstTranslator;
        }
        if (!AdminAccess::can('website.translation_statistics')) {
            $queryParams[$searchModel->formName()]['userId'] = $this->getCurrentUser()->id;
        }
        $dataProvider = $searchModel->search($queryParams);

        $translatorUsersList = Yii::$app->getModule('translation')->statistics->getTranslatorUsersList();

        return $this->render(
            'index',
            [
                'currentUser'         => $this->getCurrentUser(),
                'translatorUsersList' => $translatorUsersList,
                'dataProvider'        => $dataProvider,
                'searchModel'         => $searchModel
            ]
        );
    }
}
