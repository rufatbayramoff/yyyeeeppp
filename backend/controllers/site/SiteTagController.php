<?php namespace backend\controllers\site;

use backend\components\AdminAccess;
use backend\models\search\SiteTagSearch;
use backend\models\search\SystemLangMessageSearch;
use common\components\ArrayHelper;
use common\components\DateHelper;
use common\models\SiteTag;
use common\models\SiteTagLink;
use common\models\SystemLangMessage;
use common\models\SystemLangSource;
use Yii;
use yii\base\UserException;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;

/**
 *
 */
class SiteTagController extends \backend\components\AdminController
{

    protected $viewPath = '@backend/views/site/tags';

    public function actionIndex()
    {
        AdminAccess::validateAccess('sitetag.view');
        $searchModel  = new SiteTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel'  => $searchModel
            ]
        );
    }

    /**
     *
     * @param integer $id
     */
    public function actionUpdate($id)
    {
        AdminAccess::validateAccess('sitetag.view');
        $siteTag = SiteTag::tryFindByPk($id);

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();
            if (array_key_exists('link_tag', $postData)) {
                $siteTag->addLinkTag($postData['link_tag']);
            } else {
                $siteTag->load(Yii::$app->request->post());
                $siteTag->safeSave();
            }
            $this->setFlashMsg(true, 'Was saved');
        }

        return $this->render(
            'update',
            [
                'siteTag' => $siteTag,
            ]
        );
    }

    /**
     * @param $site_tag_id
     * @param $site_tag_tow_id
     * @return void
     * @throws NotFoundHttpException
     */
    public function actionDeleteLink($site_tag_id, $site_tag_tow_id)
    {
        $siteTag = SiteTag::tryFindByPk($site_tag_id);
        $link    = SiteTagLink::find()->where([
                'or',
                [
                    'site_tag_one' => $site_tag_tow_id,
                    'site_tag_two' => $site_tag_id
                ],
                [
                    'site_tag_two' => $site_tag_tow_id,
                    'site_tag_one' => $site_tag_id
                ]
            ]
        )->one();

        $link->delete();
        return $this->redirect('/site/site-tag/update?id=' . $siteTag->id);
    }

    /**
     */
    public function actionSearch($q)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out                         = [];
        $siteTags                    = SiteTag::find()->andWhere(['like', 'text', $q])->limit(50)->all();
        foreach ($siteTags as $siteTag) {
            $text       = str_replace('#', '', $siteTag->text);
            $out[$text] = [
                'id'   => $siteTag->id,
                'text' => $text
            ];
        }
        $out = array_values($out);
        return ['results' => $out];
    }
}
