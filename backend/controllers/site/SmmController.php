<?php
namespace backend\controllers\site;

use backend\components\AdminController;
use common\models\Model3d;
use common\models\StoreUnit;
use \frontend\components\image\ImageHtmlHelper;

/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class SmmController extends AdminController
{
    protected $viewPath = '@backend/views/site/smm';
    
    public function actionIndex()
    { 
        $products = [];
        if(!empty(app('request')->post('product_ids'))){
            $ids = explode(",", app('request')->post('product_ids'));
            $productsObj = Model3d::findAll(['id'=>$ids]);
            $productsTpl = [];
            foreach($productsObj as $product){
                /** @var StoreUnit $product  **/
                $cover = \frontend\models\model3d\Model3dFacade::getCover($product->model3d);
                $user = $product->model3d->user;
                $author = !empty($user->userProfile->full_name) ? $user->userProfile->full_name :  $user->username;
                $productsTpl[$product->model3d_id] = [
                    'id' => $product->model3d_id,
                    'title' => $product->model3d->title,
                    'author' => $author,
                    'author_id' => $product->model3d->user_id,
                    'author_avatar' => \frontend\components\UserUtils::getAvatarUrl($user),
                    'img' => $cover['image'],
                ];
            }
            $products = [];
            foreach($ids as $id){
                if(isset($productsTpl[$id])){
                    $products[] = $productsTpl[$id];
                }
            }
        }
        return $this->render('index', ['products'=>$products]);
    }
}