<?php namespace backend\controllers\site;

use backend\models\search\SystemSettingSearch;
use common\models\SystemSetting;
use Yii;
use backend\components\AdminAccess;
/**
 * SettingController
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class SettingController extends \backend\components\CrudController
{
    
    protected $viewPath = '@backend/views/setting';
    //protected $accessGroup = 'setting';
    
    public function init()
    {
        parent::init();
        $this->searchModel = new SystemSettingSearch();
        $this->mainModel = new SystemSetting();

        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'beforeCreate']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);
    }
     
    public function beforeCreate($event)
    {
        $event->sender->created_user_id = $this->getCurrentUser()->id;
    }

    public function beforeUpdate($event)
    {
        \Yii::$app->cache_settings->flush();
        $event->sender->updated_user_id = $this->getCurrentUser()->id;
    }

    /**
     * create access group
     * @return type
     */
    public function actionCreateGroup()
    {
        $this->viewPath = '@backend/views/setting/group';
        $model = new \common\models\SystemSettingGroup();
        $formConfig = ['isAjax' => $this->isAjax];
        if ($this->isAjax) {
            $formConfig['enableAjaxValidation'] = true;
        }
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $model->created_user_id = $this->getCurrentUser()->id;
            $model->updated_user_id = $this->getCurrentUser()->id;
            $model->created_at = new \yii\db\Expression('NOW()');
            $model->updated_at = new \yii\db\Expression('NOW()');

            if ($model->save()) {
                if ($this->isAjax) {
                    return $this->jsonSuccess(['message' => 'Group created']);
                }
                Yii::$app->getSession()->setFlash('success', 'Group created');
                return $this->redirect(['index']);
            } else {
                if (count($model->getErrors()) > 0) {
                    $msg = \yii\helpers\Html::errorSummary($model);
                    return $this->jsonError($msg);
                }
            }
        }
        return $this->renderPartial('create', [
                'model' => $model, 'formConfig' => $formConfig
        ]);
    }
}
