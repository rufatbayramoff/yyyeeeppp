<?php
/**
 * Date: 26.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\controllers\site;


use backend\models\search\SeoPageAutofillTemplateSearch;
use common\models\SeoPageAutofillTemplate;

class SeoPageAutofillTemplateController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/seo-page-autofill-template';
    protected $accessGroup = 'seo_page';
    public function init()
    {
        parent::init();
        $this->searchModel = new SeoPageAutofillTemplateSearch();
        $this->mainModel = new SeoPageAutofillTemplate();
    }
}
