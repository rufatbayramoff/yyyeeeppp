<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.10.16
 * Time: 15:54
 */

namespace backend\controllers\site;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\RuntimeLogsList;
use common\models\ApiExternalSystem;
use common\models\HttpRequestLog;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;


class RuntimeFilesLogController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * Lists all ApiExternalSystem models.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('httplog.view');

        $runtimeFilesList = new RuntimeLogsList();

        return $this->render('index', [
            'runtimeFilesList' => $runtimeFilesList,
        ]);
    }

    /**
     * Displays a single ApiExternalSystem model.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess('httplog.view');
        $runtimeFilesList = new RuntimeLogsList();
        $filesList = $runtimeFilesList->getLogFilesList();
        if (!in_array($id, $filesList, true)) {
            throw new InvalidParamException('Invalid file name');
        }
        Yii::$app->response->format = Response::FORMAT_RAW;
        header('Content-Type: text/plain');
        if (strpos($id, '.gz')) {
            $toPath = Yii::getAlias('@runtime') .'/runtime_unpack_log.txt';
            $command = 'gunzip -c "'.$id.'" > '.$toPath;
            exec($command);
            $id = $toPath;
        }
         // Show only last 1000 lines
        $toPath = Yii::getAlias('@runtime') .'/cat_log.txt';
        $command = 'tail -1000 "'.$id.'" > "'.$toPath.'"';
        exec($command);
        $id = $toPath;

        echo file_get_contents($id);
    }

    /**
     * Finds the ApiExternalSystem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApiExternalSystem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HttpRequestLog::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
