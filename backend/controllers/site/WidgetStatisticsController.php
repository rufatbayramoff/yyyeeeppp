<?php
namespace backend\controllers\site;

use backend\components\AdminController;
use backend\models\search\WidgetStatisticsSearch;
use common\models\WidgetStatistics;
use Yii;

class WidgetStatisticsController extends AdminController
{
    protected $viewPath = '@backend/views/site/widgetStatistics';

    public function actionIndex()
    {
        $searchModel = new WidgetStatisticsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single WidgetStatistics model.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = WidgetStatistics::tryFindByPk($id);
        return $this->render(
            'view',
            [
                'model' => $model,
            ]
        );
    }
}