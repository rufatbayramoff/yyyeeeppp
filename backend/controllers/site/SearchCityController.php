<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.11.17
 * Time: 11:58
 */

namespace backend\controllers\site;

use backend\components\AdminController;
use common\actions\SearchCityAction;

class SearchCityController extends AdminController
{
    public function actions()
    {
        return ['index' => [
            'class' => SearchCityAction::class
        ]];
    }
}