<?php
namespace backend\controllers\site;

use backend\components\CrudController;
use backend\models\search\EmailTemplateSearch;
use common\models\EmailTemplate;
use Yii;
/**
 * Email controller
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class EmailTemplateController extends CrudController
{
 
    protected $viewPath = '@backend/views/sys/email-template';
    
    /**
     * 
     */
    public function init()
    {
        parent::init();
        $this->mainModel = new EmailTemplate();
        $this->searchModel = new EmailTemplateSearch();
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);
    } 
     public function beforeUpdate($event)
    {
        $event->sender->updated_at = new \yii\db\Expression('NOW()');
    }

    public function actionDelete($id)
    {
        $this->setFlashMsg(false, 'Cannot delete');
        return $this->redirect(app('request')->referrer);
    }
}