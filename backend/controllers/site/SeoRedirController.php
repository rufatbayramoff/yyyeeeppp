<?php
/**
 * Date: 26.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\controllers\site;


use backend\components\AdminAccess;
use backend\models\search\SeoPageIntlSearch;

use backend\models\search\SeoPageSearch;
use backend\models\search\SeoRedirSearch;
use backend\models\seo\SeoPageActionsIntlForm;
use common\models\SeoPageIntl;
use common\models\SeoRedir;
use yii\helpers\Url;

class SeoRedirController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/seo-redir';
    protected $accessGroup = 'seo_redir';
    public function init()
    {
        parent::init();
        $this->searchModel = new SeoRedirSearch();
        $this->mainModel = new SeoRedir();

        $this->on(self::EVENT_BEFORE_CREATE, function($event){
            $model = $event->sender;
            $model->user_admin_id = app('user')->getId();
        });
    }

}
