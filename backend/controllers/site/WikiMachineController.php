<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\WikiMachineSearch;
use common\interfaces\FileBaseInterface;
use common\models\factories\FileFactory;
use common\models\repositories\WikiMachineRepository;
use common\models\WikiMachine;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * WikiMachineController implements the CRUD actions for WikiMachine model.
 *
 */
class WikiMachineController extends AdminController
{
    /** @var WikiMachineRepository */
    public $wikiMachineRepository;

    /** @var FileFactory */
    public $fileFactory;

    public function injectDependencies(WikiMachineRepository $wikiMachineRepository, FileFactory $fileFactory)
    {
        $this->wikiMachineRepository = $wikiMachineRepository;
        $this->fileFactory = $fileFactory;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        AdminAccess::validateAccess('website.wiki_machine');
        return parent::beforeAction($action);
    }

    /**
     * Lists all WikiMachine models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WikiMachineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single WikiMachine model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * @param WikiMachine $wikiMachine
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     * @throws \yii\db\StaleObjectException
     */
    public function saveWikiMachine(WikiMachine $wikiMachine)
    {
        $wikiMachine->load(Yii::$app->request->post());

        $uploadMainPhoto = UploadedFile::getInstance($wikiMachine, 'mainPhotoFile');
        if ($uploadMainPhoto) {
            $mainPhotoFile = $this->fileFactory->createFileFromUploadedFile($uploadMainPhoto, FileBaseInterface::TYPE_FILE_ADMIN);
            $wikiMachine->setMainPhotoFile($mainPhotoFile);
        }

        $uploadPhotoFiles = UploadedFile::getInstances($wikiMachine, 'photoFiles');
        if ($uploadPhotoFiles) {
            $photoFiles = $this->fileFactory->createFilesFromUploadedFiles($uploadPhotoFiles, FileBaseInterface::TYPE_FILE_ADMIN);
            $wikiMachine->addPhotoFiles($photoFiles);
        }

        $uploadFiles = UploadedFile::getInstances($wikiMachine, 'files');
        if ($uploadFiles) {
            $files = $this->fileFactory->createFilesFromUploadedFiles($uploadFiles, FileBaseInterface::TYPE_FILE_ADMIN);
            $wikiMachine->addFiles($files);
        }


        if ($wikiMachine->validate()) {
            $this->wikiMachineRepository->save($wikiMachine);
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new WikiMachine model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $wikiMachine = new WikiMachine();


        if (Yii::$app->request->isPost) {
            $this->saveWikiMachine($wikiMachine);
        }
        return $this->render(
            'create',
            [
                'wikiMachine' => $wikiMachine,
            ]
        );

    }

    /**
     * Updates an existing WikiMachine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $wikiMachine = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $this->saveWikiMachine($wikiMachine);
        }
        return $this->render(
            'update',
            [
                'wikiMachine' => $wikiMachine,
            ]
        );

    }

    /**
     * Deletes an existing WikiMachine model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the WikiMachine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return WikiMachine the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WikiMachine::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
