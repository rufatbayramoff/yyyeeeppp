<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\WikiMaterialSearch;
use common\interfaces\FileBaseInterface;
use common\models\factories\FileFactory;
use common\models\repositories\WikiMaterialRepository;
use common\models\WikiMaterial;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * WikiMaterialController implements the CRUD actions for WikiMaterial model.
 */
class WikiMaterialController extends AdminController
{
    /** @var  FileFactory */
    public $fileFactory;

    /** @var  WikiMaterialRepository */
    public $wikiMaterialRepository;


    public function injectDependencies(FileFactory $fileFactory, WikiMaterialRepository $wikiMaterialRepository)
    {
        $this->fileFactory = $fileFactory;
        $this->wikiMaterialRepository = $wikiMaterialRepository;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        AdminAccess::validateAccess('website.wiki_material');
        return parent::beforeAction($action);
    }

    /**
     * Lists all WikiMaterial models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WikiMaterialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single WikiMaterial model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * @param WikiMaterial $wikiMaterial
     * @return \yii\web\Response
     * @throws \yii\base\ErrorException
     * @throws \yii\base\UserException
     */
    protected function saveModel(WikiMaterial $wikiMaterial)
    {
        $wikiMaterial->load(Yii::$app->request->post());
        $uploadPhotoFiles = UploadedFile::getInstances($wikiMaterial, 'photos');
        $photoFiles = $this->fileFactory->createFilesFromUploadedFiles($uploadPhotoFiles, FileBaseInterface::TYPE_FILE_ADMIN);
        if ($photoFiles) {
            $wikiMaterial->addPhotos($photoFiles);
        };
        $uploadDataFiles = UploadedFile::getInstances($wikiMaterial, 'files');
        $dataFiles = $this->fileFactory->createFilesFromUploadedFiles($uploadDataFiles, FileBaseInterface::TYPE_FILE_ADMIN);
        if ($dataFiles) {
            $wikiMaterial->addFiles($dataFiles);
        };
        if ($wikiMaterial->validate()) {
            $this->wikiMaterialRepository->save($wikiMaterial);
            $this->setFlashMsg(true, 'Was saved.');
            return $this->redirect('/site/wiki-material');
        }
    }

    /**
     * Creates a new WikiMaterial model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $wikiMaterial = new WikiMaterial();

        if (Yii::$app->request->isPost) {
            $this->saveModel($wikiMaterial);
        }

        return $this->render(
            'create',
            [
                'model' => $wikiMaterial,
            ]
        );
    }

    /**
     * Updates an existing WikiMaterial model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $wikiMaterial = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $this->saveModel($wikiMaterial);
        }

        return $this->render(
            'update',
            [
                'model' => $wikiMaterial,
            ]
        );
    }

    /**
     * Deletes an existing WikiMaterial model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $wikiMaterial = $this->findModel($id);
        $this->wikiMaterialRepository->delete($wikiMaterial);
        return $this->redirect(['index']);
    }

    /**
     * Finds the WikiMaterial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return WikiMaterial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WikiMaterial::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
