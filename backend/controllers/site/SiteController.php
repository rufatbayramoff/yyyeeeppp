<?php

namespace backend\controllers\site;

use backend\models\user\UserAdminLoginForm;
use frontend\controllers\actions\DownloadFileAction;
use Yii;

/**
 * Site controller
 */
class SiteController extends \backend\components\AdminController
{

    protected $viewPath = '@backend/views/site';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
            'download-file' => DownloadFileAction::class
        ];
    }

    /**
     * shows dashboard
     *
     * @return type
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * login to backend part
     *
     * @return type
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new UserAdminLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            if (app('request')->isPost) {
                $result = [
                    'ip'       => app('request')->getUserIP(),
                    'username' => $model->username,
                    'agent'    => app('request')->getUserAgent(),
                ];
                // log
                \common\models\ModerLog::addRecord([
                    'user_id'     => 1,
                    'action'      => 'login_failed',
                    'result'      => \yii\helpers\Json::encode($result),
                    'object_type' => 'admin',
                    'object_id'   => 0
                ]);
                \Yii::warning('admin login failed. IP[' . $result['ip'] . ']. username [' . $result['username'] . ']', 'app');
            }
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * logout
     *
     * @return type
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
