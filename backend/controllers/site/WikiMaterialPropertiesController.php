<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use Yii;
use common\models\WikiMaterialProperties;
use backend\models\search\WikiMaterialPropertiesSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WikiMaterialPropertiesController implements the CRUD actions for WikiMaterialProperties model.
 */
class WikiMaterialPropertiesController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        AdminAccess::validateAccess('website.wiki_material');
        return parent::beforeAction($action);
    }

    /**
     * Creates a new WikiMaterialProperties model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WikiMaterialProperties();

        if (Yii::$app->request->get('wikiMaterialId')) {
            $model->wiki_material_id = Yii::$app->request->get('wikiMaterialId');
        }

        if ($model->load(Yii::$app->request->post()) && $model->safeSave()) {
            return $this->redirect('/site/wiki-material/update?id=' . $model->wiki_material_id.'#printerMaterialProperties');
        } else {
            return $this->renderAjax(
                'create',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing WikiMaterialProperties model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('/site/wiki-material/update?id=' . $model->wiki_material_id.'#printerMaterialProperties');
        } else {
            return $this->renderAjax(
                'update',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Deletes an existing WikiMaterialProperties model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect('/site/wiki-material/update?id=' . $model->wiki_material_id.'#printerMaterialProperties');
    }

    /**
     * Finds the WikiMaterialProperties model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return WikiMaterialProperties the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WikiMaterialProperties::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
