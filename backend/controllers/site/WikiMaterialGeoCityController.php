<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use Yii;
use common\models\WikiMaterialGeoCity;
use backend\models\search\WikiMaterialGeoCitySearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WikiMaterialGeoCityController implements the CRUD actions for WikiMaterialGeoCity model.
 */
class WikiMaterialGeoCityController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        AdminAccess::validateAccess('website.wiki_material');
        return parent::beforeAction($action);
    }

    /**
     * Lists all WikiMaterialGeoCity models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WikiMaterialGeoCitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single WikiMaterialGeoCity model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new WikiMaterialGeoCity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WikiMaterialGeoCity();

        $wikiMaterialId = Yii::$app->request->get('wikiMaterialId');
        $model->wiki_material_id = $wikiMaterialId;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('/site/wiki-material/update?id=' . $model->wikiMaterial->id . '#wikiMaterialCities');
        } else {
            return $this->renderAjax(
                'create',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing WikiMaterialGeoCity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'update',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Deletes an existing WikiMaterialGeoCity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect('/site/wiki-material/update?id=' . $model->wikiMaterial->id . '#wikiMaterialCities');
    }

    /**
     * Finds the WikiMaterialGeoCity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return WikiMaterialGeoCity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WikiMaterialGeoCity::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
