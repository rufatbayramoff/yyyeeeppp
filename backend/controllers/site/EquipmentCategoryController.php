<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use common\models\repositories\EquipmentCategoryRepository;
use Yii;
use common\models\EquipmentCategory;
use backend\models\search\EquipmentCategorySearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EquipmentCategoryController implements the CRUD actions for EquipmentCategory model.
 */
class EquipmentCategoryController extends AdminController
{
    /**
     * @var EquipmentCategoryRepository
     */
    public $equipmentCategoryRepository;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        AdminAccess::validateAccess('website.wiki_machine');
        return parent::beforeAction($action);
    }


    /**
     * @param EquipmentCategoryRepository $equipmentCategoryRepository
     */
    public function injectDependencies(EquipmentCategoryRepository $equipmentCategoryRepository)
    {
        $this->equipmentCategoryRepository = $equipmentCategoryRepository;
    }

    /**
     * Lists all EquipmentCategory models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EquipmentCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single EquipmentCategory model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'equipmentCategory' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new EquipmentCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $equipmentCategory = new EquipmentCategory();

        if ($equipmentCategory->load(Yii::$app->request->post()) && $equipmentCategory->validate()) {
            $this->equipmentCategoryRepository->save($equipmentCategory);
            return $this->redirect(['/site/equipment-category']);
        } else {
            return $this->render(
                'create',
                [
                    'model' => $equipmentCategory,
                ]
            );
        }
    }

    /**
     * Updates an existing EquipmentCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $equipmentCategory = $this->findModel($id);

        if ($equipmentCategory->load(Yii::$app->request->post()) && $equipmentCategory->validate()) {
            $this->equipmentCategoryRepository->save($equipmentCategory);
            return $this->redirect(['/site/equipment-category']);
        } else {
            return $this->render(
                'update',
                [
                    'model' => $equipmentCategory,
                ]
            );
        }
    }

    /**
     * Deletes an existing EquipmentCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EquipmentCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return EquipmentCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EquipmentCategory::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
