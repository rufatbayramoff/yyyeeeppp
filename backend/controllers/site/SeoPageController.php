<?php
/**
 * Date: 26.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\controllers\site;


use backend\components\AdminAccess;
use backend\models\search\SeoPageSearch;
use backend\models\seo\SeoPageActionsForm;
use backend\models\seo\SeoPageTemplateApplyForm;
use common\models\SeoPage;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\UrlManager;

class SeoPageController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/seo-page';
    protected $accessGroup = 'seo_page';

    public function init()
    {
        parent::init();
        $this->searchModel = new SeoPageSearch();
        $this->mainModel = new SeoPage();
        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'beforeCreate']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);

    }

    public function actionIndex()
    {
        /** @var SeoPageSearch $searchModel */
        $searchModel = $this->searchModel;
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        AdminAccess::validateAccess($this->accessGroup . '.view');

        $templateApplyForm = new SeoPageTemplateApplyForm();
        $templateApplyForm->setDataProvider($dataProvider);
        $pageActionsForm = new SeoPageActionsForm();
        $pageActionsForm->setDataProvider($dataProvider);
        if (app('request')->isPost) {
            // replace url manager
            $rules = require(__DIR__ . '/../../../frontend/config/url-rules.php');
            unset($rules['site/<action>/<id>']);
            $rulesOld = app('urlManager')->rules;
            \Yii::$app->set(
                'urlManager',
                [
                    'class'           => UrlManager::class,
                    'enablePrettyUrl' => true,
                    'showScriptName'  => false,
                    'baseUrl'         => '',
                    'rules'           => $rules
                ]
            );

            $pageActionsForm->load(app('request')->post());
            $msg = null;
            if ($pageActionsForm->validate()) {
                /**
                 * Mass Action logic
                 */
                $msg = $pageActionsForm->submit();
            } else {
                /**
                 * Apply Template logic
                 */
                $templateApplyForm->load(app('request')->post());
                if (!empty($templateApplyForm->template_id) && $templateApplyForm->validate()) {
                    $msg = $templateApplyForm->submit();
                }
            }
            if ($msg) $this->setFlashMsg(true, $msg);
            // set it  back
            \Yii::$app->set(
                'urlManager',
                [
                    'class'           => UrlManager::class,
                    'enablePrettyUrl' => true,
                    'showScriptName'  => false,
                    'rules'           => $rulesOld
                ]
            );
            return $this->redirect(Url::current());
        }
        return $this->render(
            'index',
            [
                'searchModel'       => $searchModel,
                'templateApplyForm' => $templateApplyForm,
                'dataProvider'      => $dataProvider,
                'seoPageActions'    => $pageActionsForm->getActions()
            ]
        );
    }

    public function beforeCreate($event)
    {
        $url = $event->sender->url;
        $url = str_replace('https://www.treatstock.com/', '', $url);
        $event->sender->url = $url;
    }

    public function beforeUpdate($event)
    {
        $url = $event->sender->url;
        $url = str_replace('https://www.treatstock.com/', '', $url);
        $event->sender->url = $url;
        $event->sender->need_review = false;
    }

}
