<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\ConnectedAppSearch;
use common\interfaces\FileBaseInterface;
use common\models\ConnectedApp;
use common\models\factories\FileFactory;
use common\models\repositories\ConnectedAppRepository;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ConnectedAppController implements the CRUD actions for connectedApp model.
 */
class ConnectedAppController extends AdminController
{
    /** @var  FileFactory */
    public $fileFactory;

    /** @var  ConnectedAppRepository */
    public $connectedAppRepository;


    public function injectDependencies(FileFactory $fileFactory, ConnectedAppRepository $connectedAppRepository)
    {
        $this->fileFactory = $fileFactory;
        $this->connectedAppRepository = $connectedAppRepository;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('website.connected_app');
        return parent::beforeAction($action);
    }

    /**
     * Lists all connectedApp models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConnectedAppSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single connectedApp model.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * @param connectedApp $connectedApp
     * @return \yii\web\Response
     * @throws \yii\base\UserException
     */
    protected function saveModel(connectedApp $connectedApp)
    {
        $connectedApp->load(Yii::$app->request->post());

        $logoFileUpload = UploadedFile::getInstance($connectedApp, 'logoFile');
        if ($logoFileUpload) {
            $logoFile = $this->fileFactory->createFileFromUploadedFile($logoFileUpload, FileBaseInterface::TYPE_FILE_ADMIN);
            $logoFile->setOwner(ConnectedApp::class, 'logo_file_id');
            $logoFile->setPublicMode(true);
            $logoFile->setFixedPath('connectedApp');
            $connectedApp->setLogoFile($logoFile);
        };

        if ($connectedApp->validate()) {
            $this->connectedAppRepository->save($connectedApp);
            $this->setFlashMsg(true, 'Was saved.');
            return $this->redirect('/site/connected-app');
        }
    }

    /**
     * Creates a new ConnectedApp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\UserException
     */
    public function actionCreate()
    {
        $connectedApp = new ConnectedApp();

        if (Yii::$app->request->isPost) {
            $this->saveModel($connectedApp);
        }

        return $this->render(
            'create',
            [
                'connectedApp' => $connectedApp,
            ]
        );
    }

    /**
     * Updates an existing connectedApp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\UserException
     */
    public function actionUpdate($id)
    {
        $connectedApp = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $this->saveModel($connectedApp);
        }

        return $this->render(
            'update',
            [
                'connectedApp' => $connectedApp,
            ]
        );
    }

    /**
     * Deletes an existing connectedApp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the connectedApp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return ConnectedApp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ConnectedApp::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
