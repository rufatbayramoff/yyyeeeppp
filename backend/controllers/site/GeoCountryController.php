<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use backend\models\search\GeoTopCitySearch;
use common\components\ArrayHelper;
use common\models\GeoCity;
use common\models\GeoTopCity;
use Yii;
use common\models\GeoCountry;
use backend\models\search\GeoCountrySearch;
use yii\helpers\Html;

/**
 * GeoCountryController
 *
 */
class GeoCountryController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/geo-country';
    protected $accessGroup = 'geocountry';

    public function init()
    {
        parent::init();
        $this->searchModel = new  GeoCountrySearch();
        $this->mainModel   = new GeoCountry();


    }

    public function actionUpdate($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.update');
        $model = $this->findModel($id);
        $post  = Yii::$app->request->post();
        app('cache')->set('back.notify.data', '');
        if ($model->load($post)) {
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        if (!empty($post['GeoTopCity'])) {
            $topCityModel = new GeoTopCity();
            $topCityModel->load($post);
            $hasCity = GeoTopCity::findOne(['city_id' => $topCityModel->city_id]);
            if ($hasCity) {
                $this->setFlashMsg(false, 'This city already added to top cities');
            } else if ($topCityModel->validate()) {
                $topCityModel->save();
            } else {
                $this->setFlashMsg(false, Html::errorSummary($topCityModel));
            }
        }
        $searchModel             = new GeoTopCitySearch();
        $searchModel->country_id = $model->id;
        $dataProvider            = $searchModel->search([]);

        return $this->render('update', [
            'model'        => $model,
            'topModel'     => new GeoTopCity(),
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteCity($countryId, $deleteCity)
    {
        $topCity = GeoTopCity::findOne(['country_id' => $countryId, 'id' => $deleteCity]);
        if (!$topCity) {
            return $this->redirect(['update', 'id' => $countryId]);
        }
        $topCity->delete();

        return $this->redirect(['update', 'id' => $countryId]);
    }

    public function actionView($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCityList($q, $country_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $geoCities = GeoCity::find()
            ->andWhere(['country_id' => $country_id])
            ->andWhere(['like', 'title', $q])
            ->limit(30)
            ->all();
        $result    = [];
        foreach ($geoCities as $geoCity) {
            $result[] = [
                'id'    => $geoCity->id,
                'text' => ($geoCity->region ? $geoCity->region->title . ' / ' : '') . $geoCity->title
            ];
        }
        return ['results'=>$result];
    }

}
