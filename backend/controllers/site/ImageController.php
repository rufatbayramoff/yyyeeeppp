<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 13.01.2017
 * Time: 10:22
 */

namespace backend\controllers\site;


use backend\components\AdminController;
use common\models\File;
use common\models\repositories\FileRepository;
use yii\base\InvalidParamException;
use yii\imagine\Image;

class ImageController extends AdminController
{

    /** @var FileRepository */
    public $fileRepository;

    public function injectDependencies(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param null $fileId
     * @param null $fileUuid
     * @param $direction
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \yii\base\ErrorException
     */
    public function actionRotate($fileId = null, $fileUuid = null, $direction)
    {
        if ($fileId) {
            $file = File::findByPk($fileId);
        } else {
            $file = $this->fileRepository->getByUuid($fileUuid);
        }
        if (!$file) {
            throw new InvalidParamException('File not found.');
        }

        $location = $file->getLocalTmpFilePath();

        $angle = $direction == 'left' ? -90 : 90;
        $d = Image::frame($location, 0)->rotate($angle)->save($location);
        $file->publishFileToServerAndSave($location);

        return $this->redirect(app('request')->referrer);
    }
}