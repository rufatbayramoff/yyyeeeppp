<?php

namespace backend\controllers\site;

use common\modules\homePage\factories\HomePageProductFactory;
use Yii;
use common\models\HomePageFeatured;
use backend\models\search\HomePageFeaturedSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HomePageFeaturedController implements the CRUD actions for HomePageFeatured model.
 */
class HomePageFeaturedController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HomePageFeatured models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HomePageFeaturedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HomePageFeatured model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HomePageFeatured model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        /** @var HomePageProductFactory $homePageFactory */
        $homePageFactory = Yii::createObject(HomePageProductFactory::class);

        /** @var HomePageFeatured $homePageFeatured */
        $homePageFeatured = $homePageFactory->createFeatured();

        if ($homePageFeatured->load(Yii::$app->request->post()) && $homePageFeatured->saveFeatured()) {
            return $this->redirect(['view', 'id' => $homePageFeatured->id]);
        } else {
            return $this->render('create', [
                'model' => $homePageFeatured,
            ]);
        }
    }

    /**
     * Updates an existing HomePageFeatured model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $homePageFeatured = $this->findModel($id);

        if ($homePageFeatured->load(Yii::$app->request->post()) && $homePageFeatured->saveFeatured()) {
            return $this->redirect(['view', 'id' => $homePageFeatured->id]);
        } else {
            return $this->render('update', [
                'model' => $homePageFeatured,
            ]);
        }
    }

    /**
     * Deletes an existing HomePageFeatured model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HomePageFeatured model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HomePageFeatured the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HomePageFeatured::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
