<?php
 

namespace backend\controllers\site;

/** 
 *
 * @author DeFacto
 */
  
class SiteHelpIntlController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/site-help-intl';
    protected $accessGroup = 'sitehelp';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\SiteHelpIntlSearch();
        $this->mainModel = new \common\models\SiteHelpIntl();
    }
}
