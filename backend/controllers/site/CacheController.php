<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use backend\components\AdminController;
use common\components\BaseActiveQuery;
use common\models\repositories\CatalogCostRepository;
use common\models\StatisticsCache;
use Yii;

class CacheController extends AdminController
{

    protected $viewPath = '@backend/views/site/cache';

    public function beforeAction($action)
    {
        if (!is_guest()) {
            AdminAccess::validateAccess('site.cache');
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $cacheTypes = [
            'printersTree' => 'Printers tree',
            'catalogCosts' => 'Catalog costs cache',
            'apc' => 'Clear APC Cache',
        ];
        $infos = apcu_cache_info('user');

        return $this->render(
            'cacheList',
            [
                'cacheTypes'      => $cacheTypes,
                'apcInfo' => $infos
            ]
        );
    }

    public function actionClear($type)
    {
        if ($type === 'printersTree') {
            Yii::$app->getModule('printersList')->printersTreeUpdater->asyncUpdate();
        }
        if ($type === 'catalogCosts') {
            CatalogCostRepository::invalidateAllCosts();
        }
        if ($type == 'apc') {
            apcu_clear_cache();
            BaseActiveQuery::resetCacheDependency();
        }
        $this->setFlashMsg(true, 'Cache was reset.');
        return $this->redirect('/site/cache');
    }
}
