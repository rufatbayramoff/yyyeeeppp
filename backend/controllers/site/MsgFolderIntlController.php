<?php

namespace backend\controllers\site;

use backend\components\CrudController;
use Yii;
use common\models\MsgFolderIntl;
use backend\models\search\MsgFolderIntlSearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MsgFolderIntlController implements the CRUD actions for MsgFolderIntl model.
 */
class MsgFolderIntlController extends CrudController
{
    protected $viewPath = '@backend/views/site/msg-folder-intl';
    protected $accessGroup = 'setting';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\MsgFolderIntlSearch();
        $this->mainModel = new \common\models\MsgFolderIntl();
    }
}
