<?php namespace backend\controllers\site;

use Yii;

/**
 * EmailReader - used for testing on dev env
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class EmailReaderController extends \backend\components\AdminController
{

    protected $viewPath = '@backend/views/sys';
    protected $emailDir = '@frontend/runtime/mail';
    public function actionIndex()
    {
        $emails = $this->getEmails();
        return $this->render('email', ['emails' => $emails]);
    }
    
    /**
     * remove all test emails
     * 
     * @return type
     */
    public function actionClear()
    {
        $directory = \Yii::getAlias($this->emailDir);
        $files = glob($directory . '/*'); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file)){
                unlink($file); 
            }
        }
        return $this->redirect('@web/email-reader');
    }

    /**
     * get list of emails in runtime/mail folder
     * 
     * @return type
     * @throws \Exception
     */
    private function getEmails()
    {
        $directory = \Yii::getAlias($this->emailDir);

        if (!is_dir($directory)) {
            throw new \Exception('Setup email directory');
        }
        $filenames = array();
        $iterator = new \DirectoryIterator($directory);
        foreach ($iterator as $k => $fileinfo) {
            if ($fileinfo->isDir()) {
                continue;
            }
            $fn = $fileinfo->getFilename();
            if ($fn == '.' || $fn == '..') {
                continue;
            }
            $fdate = $fileinfo->getMTime();
            $size = $fileinfo->getSize();
            $filenames[$fn] = ['id' => $k + 1, 'file' => $fn, 'size' => $size, 'date' => $fdate];
        }
        ksort($filenames);
        return $filenames;
    }

    /**
     * read email from runtime/mail dir
     * @param type $email
     * @return type
     */
    private function getEmail($email)
    {
        $directory = \Yii::getAlias('@frontend/runtime/mail');
        $fileName = ($directory . '/' . $email);
        $fileContent = file_get_contents($fileName);
        $fileContent = quoted_printable_decode($fileContent);
        $fileContent = str_replace("=\r\n", "", $fileContent);
        $fileContent = str_replace("=3D", "=", $fileContent);
        $fileContent = str_replace("=20", "\r\n", $fileContent);
        return $fileContent;
    }

    public function actionRead($file)
    {
        $emails = $this->getEmails();
        $email = $this->getEmail($file);
        return $this->render('email', ['emails' => $emails, 'email' => $email]);
    }
}
