<?php namespace backend\controllers\site;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class NotifyController extends \backend\components\AdminController
{
    protected $viewPath = '@backend/views/site';
    public function actionGetNotifies()
    {
        $notifiesData = \backend\models\notify\NotifyFacade::getNotifies();
        
        if($this->isAjax){
            return $this->renderPartial('top_notify.php', ['notifiesData'=>$notifiesData]);
        }
        return $this->render('top_notify.php', ['notifiesData'=>$notifiesData]);
    }    
}
