<?php

namespace backend\controllers\site;

use backend\models\search\SystemLangMessageSearch;
use common\components\exceptions\OnlyPostRequestException;
use common\models\SystemLang;
use common\models\SystemLangMessage;
use common\models\SystemLangSource;
use common\modules\translation\models\I18nTableElement;
use common\modules\translation\models\search\I18nTableElementSearch;
use Yii;
use yii\base\UserException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;

class LangDbController extends \backend\components\AdminController
{

    protected $viewPath = '@backend/views/lang-db';


    public function actionIndex()
    {
        $searchModel = new I18nTableElementSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!array_key_exists($searchModel->formName(), $queryParams) || (!array_key_exists('tableName', $queryParams[$searchModel->formName()]) && !array_key_exists('translatedBy', $queryParams[$searchModel->formName()]))) {
            $queryParams[$searchModel->formName()]['tableName'] = $searchModel::DEFALUT_TABLE_NAME;
        }
        $dataProvider = $searchModel->search($queryParams);

        // non-ajax - render the grid by default
        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel'  => $searchModel,
                'isShortForm'  => Yii::$app->request->get('shortForm') && $searchModel->tableName && $searchModel->modelId
            ]
        );
    }

    protected function getPrevUrl()
    {
        return Yii::$app->request->referrer ?: '/site/lang-db';
    }

    /**
     * Displays a single model.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate($id)
    {
        [$langIso, $table, $fieldId, $modelId] = explode(I18nTableElement::ID_DELIMETER, $id);
        $i18nTableElement = Yii::$app->getModule('translation')->dbI18n->getI18nElement($langIso, $table, $fieldId, $modelId);
        if ($postValues = Yii::$app->request->post('I18nTableElement')) {
            $oldFieldValue = $i18nTableElement->fieldValue;
            $i18nTableElement->populateParams($postValues);
            if ($i18nTableElement->fieldValue != $oldFieldValue) {
                $i18nTableElement->updateTranslator($this->getCurrentUser());
            }
            Yii::$app->getModule('translation')->dbI18n->saveI18nElement($i18nTableElement);
            $this->setFlashMsg(true, 'Was saved');
            return $this->redirect(Yii::$app->request->post('prevUrl'));
        }
        return $this->render(
            'update',
            [
                'i18nTableElement' => $i18nTableElement,
                'prevUrl'          => $this->getPrevUrl()
            ]
        );
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        [$langIso, $table, $fieldId, $modelId] = explode(I18nTableElement::ID_DELIMETER, $id);
        $i18nTableElement = Yii::$app->getModule('translation')->dbI18n->getI18nElement($langIso, $table, $fieldId, $modelId);
        Yii::$app->getModule('translation')->dbI18n->deleteI18nElement($i18nTableElement);
        $this->setFlashMsg(true, 'Was deleted');
        return $this->redirect($this->getPrevUrl());
    }

}
