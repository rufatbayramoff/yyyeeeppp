<?php

namespace backend\controllers\site;

use backend\components\AdminAccess;
use common\interfaces\FileBaseInterface;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use common\models\SiteHelpRelated;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * SiteHelpController implements the CRUD actions for SiteHelp model.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class SiteHelpController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/site-help';
    protected $accessGroup = 'sitehelp';

    /**
     * @var FileFactory
     */
    private $fileFactory;

    /** @var FileRepository */
    public $fileRepository;

    /**
     * @param FileFactory $fileFactory
     * @param FileRepository $fileRepository
     */
    public function injectDependencies(FileFactory $fileFactory, FileRepository $fileRepository)
    {
        $this->fileFactory = $fileFactory;
        $this->fileRepository = $fileRepository;
    }


    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\SiteHelpSearch();
        $this->mainModel = new \common\models\SiteHelp();

        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'beforeCreate']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);
    }

    public function beforeUpdate($event)
    {
        $user = $this->getCurrentUser();
        if (!$user) {
            return $this->redirect('/');
        }
        // set related helps
        $post = app('request')->post();
        if (!empty($post)) {
            SiteHelpRelated::updateRelated($event->sender, $post);
            $this->uploadCoverFile($event->sender);
        }
        $event->sender->user_id = $user->id;
        return null;
    }

    public function beforeCreate($event)
    {
        $user = $this->getCurrentUser();
        if (!$user) {
            return $this->redirect('/');
        }
        $now = new \yii\db\Expression('NOW()');
        $event->sender->created_at = $now;
        $event->sender->user_id = $user->id;
    }

    public function actionCreate()
    {
        $model = $this->mainModel;
        $post = Yii::$app->request->post();
        AdminAccess::validateAccess($this->accessGroup . '.add');
        $formConfig = ['isAjax' => $this->isAjax];
        if ($model->load($post)) {
            $this->trigger(
                self::EVENT_BEFORE_CREATE,
                new Event(['sender' => $model])
            );
            if ($model->save()) {
                // bind related
                SiteHelpRelated::addRelated($model, app('request')->post());
                $this->uploadCoverFile($model);
                Yii::$app->getSession()->setFlash('success', 'Successfully created');
                return $this->redirect(['index']);
            } else {
                $msg = \yii\helpers\Html::errorSummary($model);
                Yii::$app->getSession()->setFlash('error', $msg);
            }
        }
        app('cache')->set('back.notify.data', '');
        return $this->render(
            'create',
            [
                'model'      => $model,
                'formConfig' => $formConfig
            ]
        );
    }

    /**
     * Upload and save cover file
     *
     * @param SiteHelp $model
     * @throws \Exception
     */
    private function uploadCoverFile(SiteHelp $model): void
    {
        if ($coverFile = UploadedFile::getInstance($model, 'coverFile')) {
            $file = $this->fileFactory->createFileFromUploadedFile($coverFile, FileBaseInterface::TYPE_FILE_ADMIN);
            $file->setPublicMode(true);
            $file->setOwner(SiteHelp::class, 'cover_file_id');
            $file->setFixedPath('site-help');
            $this->fileRepository->save($file);

            $model->cover_file_uuid = $file->uuid;
            $model->safeSave();
        }
    }

    /**
     * @todo move it to separate action and include where it need
     * @throws \Exception
     */
    public function actionUpload()
    {
        $uploadedFile = \yii\web\UploadedFile::getInstanceByName('upload');
        $fileFactory = \Yii::createObject(FileFactory::class);
        $fileRepository = \Yii::createObject(FileRepository::class);
        $file = $fileFactory->createFileFromUploadedFile($uploadedFile);
        $file->setPublicMode(true);
        $file->setOwner(SiteHelp::class, 'id');
        $file->setFixedPath('ck');
        try {
            $fileRepository->save($file);
            $message = 'File added.';
            $url = $file->getFileUrl();
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $url = '';
        }
        $funcNum = (int)$_GET['CKEditorFuncNum'];
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
    }


    /**
     * Return formatted list of categories for select input.
     *
     * @return array
     */
    public static function categoriesList()
    {
        $cats = SiteHelpCategory::find()->root()->all();

        /**
         * @param SiteHelpCategory[] $categories
         * @param $level
         * @param $fn
         * @return array
         */
        $fn = function (array $categories, $level, $fn) {
            $items = [];
            $nextLevel = $level + 1;
            foreach ($categories as $category) {

                $items[$category->id] = str_repeat('-', $level * 2) . ' ' . $category->title;
                $items = ArrayHelper::merge($items, $fn($category->siteHelpCategories, $nextLevel, $fn));
            }
            return $items;
        };

        return $fn($cats, 0, $fn);

    }

}
