<?php
/**
 * Date: 26.08.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\controllers\site;


use backend\components\AdminAccess;
use backend\models\search\SeoPageIntlSearch;

use backend\models\search\SeoPageSearch;
use backend\models\seo\SeoPageActionsIntlForm;
use common\models\SeoPageIntl;
use yii\helpers\Url;

class SeoPageIntlController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/site/seo-page-intl';
    protected $accessGroup = 'seo_page';
    public function init()
    {
        parent::init();
        $this->searchModel = new SeoPageIntlSearch();
        $this->mainModel = new SeoPageIntl();
    }

    public function actionIndex()
    {
        /** @var SeoPageSearch $searchModel */
        $searchModel = $this->searchModel;
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        AdminAccess::validateAccess($this->accessGroup . '.view');
        $pageActionsForm = new SeoPageActionsIntlForm();
        $pageActionsForm->setDataProvider($dataProvider);
        if (app('request')->isPost) {
            $pageActionsForm->load(app('request')->post());
            $msg = null;
            if ($pageActionsForm->validate()) {
                /**
                 * Mass Action logic
                 */
                $msg = $pageActionsForm->submit();
            }
            if ($msg) $this->setFlashMsg(true, $msg);
            return $this->redirect(Url::current());
        }
        return $this->render(
            'index',
            [
                'searchModel'       => $searchModel,
                'dataProvider'      => $dataProvider,
                'seoPageActions'    => $pageActionsForm->getActions()
            ]
        );
    }
}
