<?php


namespace backend\controllers\promocode;

use backend\components\AdminAccess;
use backend\components\CrudController;
use Yii;
use common\models\PromocodeUsage;
use backend\models\search\PromocodeUsageSearch;


/**
 *
 */
class PromocodeUsageController extends CrudController
{
    protected $viewPath = '@backend/views/promocode/promocode-usage';

    public function init()
    {
        parent::init();
        $this->searchModel = new PromocodeUsageSearch();
        $this->mainModel = new PromocodeUsage();
    }

    /**
     * list records
     *
     * @return mixed
     * @throws \yii\base\UserException
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $searchModel = $this->searchModel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 100);

        AdminAccess::validateAccess($this->accessGroup . '.view');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
