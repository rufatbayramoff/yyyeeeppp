<?php


namespace backend\controllers\promocode;

use backend\components\CrudController;
use Yii;
use common\models\PromocodeHistory;
use backend\models\search\PromocodeHistorySearch;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 *
 */
class PromocodeHistoryController extends CrudController
{
    protected $viewPath = '@backend/views/promocode/promocode-history';

    public function init()
    {
        parent::init();
        $this->searchModel = new PromocodeHistorySearch();
        $this->mainModel = new PromocodeHistory();
    }
}
