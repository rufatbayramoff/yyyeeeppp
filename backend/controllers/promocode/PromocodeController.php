<?php

namespace backend\controllers\promocode;

use backend\components\CrudController;
use Yii;
use common\models\Promocode;
use backend\models\search\PromocodeSearch;

/**
 *
 */
class PromocodeController extends CrudController
{
    protected $viewPath = '@backend/views/promocode/promocode';

    public function init()
    {
        parent::init();
        $this->searchModel = new PromocodeSearch();
        $this->mainModel = new Promocode();
    }
}
