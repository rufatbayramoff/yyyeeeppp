<?php namespace backend\controllers\api;

use common\components\FileDirHelper;
use common\models\PaymentTransaction;
use common\modules\payment\processors\PayPalPayoutProcessor;
use common\modules\payment\services\PaymentService;

/**
 * PaymentTransaction
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaymentTransactionController extends \yii\web\Controller
{
    /** @var PayPalPayoutProcessor */
    protected $payoutProcessor;

    /** @var PaymentService */
    protected $paymentService;

    public function injectDependencies(PayPalPayoutProcessor $payoutProcessor, PaymentService $paymentService)
    {
        $this->payoutProcessor = $payoutProcessor;
        $this->paymentService  = $paymentService;
    }

    public function init()
    {
        parent::init();
        $this->enableCsrfValidation = false;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    '*' => ['get', 'post', 'put', 'delete'],
                ],
            ],
        ];
    }

    /**
     * add some security checks
     *
     * @param type $from
     * @return type
     * @throws \yii\base\UserException
     */
    public function actionGet($token = '', $from = 0)
    {
        $ourToken = param('ts_api_token');

        if ($token != $ourToken) {
            return \yii\helpers\Json::encode(['success' => false, 'message' => 'API Token invalid']);
        }

        $query = PaymentTransaction::find()->select([
            PaymentTransaction::column('*'),
            \common\models\UserPaypal::column('email') . ' as receiver',
            dbexpr('"EMAIL" as receiver_type')
        ])
            ->innerJoin('user_paypal', 'user_paypal.user_id=payment_transaction.user_id AND user_paypal.check_status="ok"')
            ->where([
                PaymentTransaction::column('vendor') => PaymentTransaction::VENDOR_PP_PAYOUT,
                PaymentTransaction::column('status') => [PaymentTransaction::STATUS_PENDING, PaymentTransaction::STATUS_AUTHORIZED, PaymentTransaction::STATUS_SUBMITTED_FOR_SETTLEMENT],
                PaymentTransaction::column('type')   => ['minus']
            ]);

        $query->limit(500);
        //        debugSql($query);
        $paymentTransactions     = $query->all();
        $paymentTransactionArray = $query->asArray()->all();

        $this->setTransactionSubmittedForSettlement($paymentTransactions);

        return \yii\helpers\Json::encode($paymentTransactionArray);
    }

    /**
     * @param $token
     * @return string
     * @throws \yii\base\ErrorException
     */
    public function actionUpdate($token)
    {
        $ourToken = param('ts_api_token');
        if ($token != $ourToken) {
            return \yii\helpers\Json::encode(['success' => false, 'message' => 'API Token invalid']);
        }

        $updates = app('request')->post('data');
        $data    = \yii\helpers\Json::decode($updates);

        $dirLog = \Yii::getAlias('@runtime') . '/paymentTransactionUpdate';
        FileDirHelper::createDir($dirLog);

        file_put_contents($dirLog . '/' . date('Y-m-d H-i-s.json'), json_encode($data));

        $dbtr    = app('db')->beginTransaction();
        $success = true;
        $message = 'Updated';
        try {
            foreach ($data as $k => $v) {
                $id                = (int)$v['id'];
                $transactionId     = $v['transaction_id'] ?? '';
                $transactionStatus = $v['transaction_status'] ?? '';
                $this->payoutProcessor->updateStatus($id, $transactionId, $transactionStatus);
            }
            $dbtr->commit();
        } catch (\Exception $e) {
            logException($e, 'api');
            $message = $e->getMessage();
            $success = false;
        }
        return \yii\helpers\Json::encode(['success' => $success, 'message' => $message]);
    }

    /**
     * @param PaymentTransaction[] $paymentTransactions
     * @throws \yii\base\UserException
     */
    protected function setTransactionSubmittedForSettlement($paymentTransactions): void
    {
        foreach ($paymentTransactions as $paymentTransaction) {
            if (in_array($paymentTransaction->status, [PaymentTransaction::STATUS_PENDING, PaymentTransaction::STATUS_AUTHORIZED])) {
                $this->paymentService->updateTransactionStatus($paymentTransaction, PaymentTransaction::STATUS_SUBMITTED_FOR_SETTLEMENT);
            }
        }
    }
}
