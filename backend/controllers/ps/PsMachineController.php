<?php

namespace backend\controllers\ps;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\PsCncMachineHistorySearch;
use backend\models\search\CompanyServiceSearch;
use common\components\exceptions\AssertHelper;
use common\models\populators\PsCncMachinePopulator;
use common\models\CompanyService;
use common\modules\cnc\repositories\PsCncMachineRepository;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;


/**
 * PsMachineController implements the CRUD actions for PsMachine model.
 */
class PsMachineController extends AdminController
{
    /**
     * @var PsCncMachineRepository
     */
    protected $psCncMachineRepository;

    /**
     * @var PsCncMachinePopulator
     */
    protected $psCncMachinePopulator;


    public function injectDependencies(
        PsCncMachineRepository $psCncMachineRepository,
        PsCncMachinePopulator $psCncMachinePopulator
    ) {
        $this->psCncMachineRepository = $psCncMachineRepository;
        $this->psCncMachinePopulator = $psCncMachinePopulator;
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete'          => ['POST'],
                    'save-cnc-schema' => ['POST'],
                    'save-ps-machine' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all PsMachine models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('psmachine.view');
        $searchModel = new CompanyServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single PsMachine model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess('psmachine.view');
        $psMachine = $this->findModel($id);

        if ($psMachine->type == CompanyService::TYPE_CNC) {
            $historySearch = new PsCncMachineHistorySearch();
            $historySearch->ps_machine_id = $psMachine->id;
            $historySearch->ps_cnc_machine_id = $psMachine->ps_cnc_machine_id;
            $historyProvider = $historySearch->search(Yii::$app->request->queryParams);
        } else {
            $historySearch = null;
            $historyProvider = null;
        }

        return $this->render(
            'view',
            [
                'model'           => $psMachine,
                'historySearch'   => $historySearch,
                'historyProvider' => $historyProvider,
            ]
        );
    }

    public function actionSavePsMachine($id)
    {
        AdminAccess::validateAccess('psmachine.moderate');
        $psMachine = $this->findModel($id);
        $postParams = Yii::$app->request->post();
        $psMachine->load($postParams);
        $psMachine->safeSave();
        $this->setFlashMsg(true, 'Ps machine info saved.');
        $this->redirect('view?id=' . $psMachine->id);
    }


    /**
     * Updates an existing PsMachine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionSaveCncSchema($psMachineId)
    {
        AdminAccess::validateAccess('psmachine.moderate');
        $post = Yii::$app->request->getBodyParams();
        $psMachine = CompanyService::tryFindByPk($psMachineId);
        $psCncMachine = $this->psCncMachineRepository->getByPsMachine($psMachine);
        $this->psCncMachinePopulator->populate($psCncMachine, $post);
        $this->psCncMachineRepository->save($psCncMachine);

        $resultJson = Yii::$app->getModule('cnc')->psCncJsonGenerator->generateJsonServiceInfo($psCncMachine->psCnc);
        $this->setFlashMsg(true, 'Schema saved.');
        return $this->jsonReturn($resultJson);
    }

    public function actionApproveMachine($psMachineId)
    {
        AdminAccess::validateAccess('psmachine.moderate');

        $psMachine = CompanyService::tryFindByPk($psMachineId);
        $psMachine->moderator_status = CompanyService::MODERATOR_STATUS_APPROVED;
        AssertHelper::assertSave($psMachine);

        $this->setFlashMsg(true, 'Machine successful approved.');
        $this->redirect('view?id=' . $psMachine->id);
    }

    /**
     * Finds the PsMachine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return CompanyService the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyService::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
