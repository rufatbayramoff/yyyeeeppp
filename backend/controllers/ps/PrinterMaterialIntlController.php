<?php

namespace backend\controllers\ps;

/**
 *
 */
class PrinterMaterialIntlController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer-material-intl';
    public $accessGroup = 'printer';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterMaterialIntlSearch();
        $this->mainModel = new \common\models\PrinterMaterialIntl();
    }
}
