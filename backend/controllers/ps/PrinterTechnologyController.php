<?php

namespace backend\controllers\ps;
use common\interfaces\FileBaseInterface;
use common\models\factories\FileFactory;
use common\models\FileAdmin;
use common\models\PrinterTechnology;
use common\models\PrinterTechnologyCertificationExample;
use common\models\PsPrinterTest;
use common\models\repositories\FileRepository;
use frontend\components\image\ImageHtmlHelper;
use yii\base\Event;
use yii\web\UploadedFile;

/**
 *
 */
class PrinterTechnologyController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer-technology';

    public $accessGroup = 'printer';

    /**
     * @var FileFactory
     */
    public $fileFactory;

    /**
     * @var FileRepository
     */
    private $fileRepository;

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterTechnologySearch();
        $this->mainModel = new \common\models\PrinterTechnology();
        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'beforeCreate']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);

        $this->on(self::EVENT_AFTER_SAVE, [$this, 'saveCetrificationExamples']);
    }

    /**
     * @param FileFactory $fileFactory
     * @param FileRepository $fileRepository
     */
    public function injectDependencies(FileFactory $fileFactory, FileRepository $fileRepository)
    {
        $this->fileFactory = $fileFactory;
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param $event
     */
    public function beforeCreate($event)
    {
        $now = new \yii\db\Expression('NOW()');
        $event->sender->created_at = $now;
        $event->sender->updated_at = $now;
    }

    /**
     * @param $event
     */
    public function beforeUpdate($event)
    {
        $event->sender->updated_at = new \yii\db\Expression('NOW()');
    }

    /**
     * @param Event $event
     */
    public function saveCetrificationExamples(Event $event)
    {
        $processFilesListFn = function (string $name, string $type, PrinterTechnology $technology) {

            $uploads = UploadedFile::getInstances($technology, $name);
            /** @var FileAdmin[] $files */
            $files = $this->fileFactory->createFilesFromUploadedFiles($uploads, FileBaseInterface::TYPE_FILE_ADMIN);

            foreach ($files as $file) {
                $file->setPublicMode(true);
                $file->setFixedPath("printer-technology/certification-exmaples/{$technology->code}");
                $file->setOwner(PrinterTechnologyCertificationExample::class, 'file_id');
                ImageHtmlHelper::stripExifInfo($file);
                $this->fileRepository->save($file);


                $example = new PrinterTechnologyCertificationExample();
                $example->technology_id = $technology->id;
                $example->file_uuid = $file->uuid;
                $example->type = $type;
                $example->safeSave();
            }

            $filesForDelete = $_POST["PrinterTechnology"][$name] ?? [];

            foreach (array_keys($filesForDelete) as $fileUuid) {
                foreach ($technology->printerTechnologyCertificationExamples as $example) {
                    if ($example->file_uuid == $fileUuid) {
                        $example->delete();
                        $this->fileRepository->delete($example->file);
                    }
                }
            }
        };

        /** @var PrinterTechnology $technology */
        $technology = $event->sender;

        $processFilesListFn('commonCertificationFiles', PsPrinterTest::TYPE_COMMON, $technology);
        $processFilesListFn('professionalCertificationFiles', PsPrinterTest::TYPE_PROFESSIONAL, $technology);
    }
}
