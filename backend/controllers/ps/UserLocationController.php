<?php

namespace backend\controllers\ps;

/**
 *
 */
class UserLocationController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/user-location';

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\UserLocationSearch();
        $this->mainModel = new \common\models\UserLocation();
    }
}
