<?php

namespace backend\controllers\ps;

/**
 *
 */
class PrinterTechnologyIntlController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer-technology-intl';
    public $accessGroup = 'printer';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterTechnologyIntlSearch();
        $this->mainModel = new \common\models\PrinterTechnologyIntl();
    }

}
