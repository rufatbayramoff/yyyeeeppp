<?php

namespace backend\controllers\ps;

/**
 *
 */
class PrinterColorController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer-color';
    public $accessGroup = 'printer';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterColorSearch();
        $this->mainModel = new \common\models\PrinterColor();
        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'beforeCreate']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);
    }

    public function beforeCreate($event)
    {
        $now = new \yii\db\Expression('NOW()');
        $event->sender->created_at = $now;
        $event->sender->updated_at = $now;
    }

    public function beforeUpdate($event)
    {
        $event->sender->updated_at = new \yii\db\Expression('NOW()');
    }
}
