<?php

namespace backend\controllers\ps;

use common\models\Printer;
use common\models\TsCertificationClass;
use common\services\FileService;
use frontend\models\ps\PsFacade;
use Yii;
use yii\base\Event;
use yii\helpers\Json;
use yii\web\UploadedFile;

/**
 *
 */
class PrinterController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer';
    public $accessGroup = 'printer';

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterSearch();
        $this->mainModel = new \common\models\Printer();
        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'beforeCreate']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);
    }

    public function beforeCreate($event)
    {
        $now = new \yii\db\Expression('NOW()');
        $event->sender->created_at = $now;
        $event->sender->updated_at = $now;
    }

    public function beforeUpdate($event)
    {
        $event->sender->updated_at = new \yii\db\Expression('NOW()');
    }

    public function actionCreate()
    {
        $model = $this->mainModel;
        $model->ts_certification_class_id = TsCertificationClass::DEFAULT_PRINTERS_CERT;

        $properties = PsFacade::properties()->getProperties();

        $post = Yii::$app->request->post();

        $formConfig = ['isAjax' => $this->isAjax];
        if ($model->load($post)) {
            $this->trigger(
                self::EVENT_BEFORE_CREATE,
                new Event(['sender' => $model])
            );
            if ($this->isAjax) {
                $formConfig['enableAjaxValidation'] = true;
                if ($model->save()) {
                    return $this->jsonSuccess(
                        ['message' => 'Successfully created']
                    );
                } else {
                    if (count($model->getErrors()) > 0) {
                        $msg = \yii\helpers\Html::errorSummary($model);
                        return $this->jsonError($msg);
                    }
                }
            } else {
                if ($model->save()) {
                    $printerId = $model->getPrimaryKey();
                    if (empty($post['Editable'])) {
                        $post['Editable'] = [];
                    }
                    PsFacade::properties($printerId)->updateProperties($post['Property'], $post['Editable']);
                    Yii::$app->getSession()->setFlash('success', 'Successfully created');

                    return $this->redirect(['index']);
                }
            }
        }

        return $this->renderAdaptive(
            'create',
            [
                'model'      => $model,
                'properties' => $properties,
                'formConfig' => $formConfig
            ]
        );
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();

        $properties = PsFacade::properties($id)->getProperties();

        if ($model->load($post)) {

            $this->trigger(
                self::EVENT_BEFORE_UPDATE,
                new Event(['sender' => $model])
            );
            $model = $this->processFiles($model);

            if ($model->save()) {
                if (empty($post['Editable'])) {
                    $post['Editable'] = [];
                }
                PsFacade::properties($id)->updateProperties($post['Property'], $post['Editable']);


                return $this->redirect(['index']);
            }
        }

        return $this->render(
            'update',
            [
                'model'      => $model,
                'properties' => $properties
            ]
        );
    }

    /**
     * @param $id
     * @param $fileId
     * @return \yii\web\Response
     */
    public function actionDeleteFile($id, $fileId)
    {
        /**
         * @var $model Printer
         */
        $model = $this->findModel($id);
        $fileIds = (array)$model->files_json;
        foreach ($fileIds as $k => $fId) {
            if ($fId == $fileId) {
                unset($fileIds[$k]);
            }
        }
        $model->files_json = $fileIds;
        $model->safeSave();
        return $this->redirect(app('request')->referrer);
    }

    /**
     * @param $id
     * @param $fileId
     * @return \yii\web\Response
     */
    public function actionDeleteImage($id, $fileId)
    {
        /**
         * @var $model Printer
         */
        $model = $this->findModel($id);
        $fileIds = (array)$model->images_json;
        if(!empty($fileIds)) {
            foreach ($fileIds as $k => $fId) {
                if ($fId == $fileId) {
                    unset($fileIds[$k]);
                }
            }
            $model->images_json = $fileIds;
            $model->save();
        }
        return $this->redirect(app('request')->referrer);
    }

    private function processFiles(Printer $model)
    {
        if(!is_dir(Yii::getAlias('@frontend').'/web/static/uploads/printers/')){
            mkdir(Yii::getAlias('@frontend').'/web/static/uploads/printers/');
        }
        $moreImages = UploadedFile::getInstances($model, 'moreImages');
        $fileIds = (array)$model->images_json;
        if (!empty($moreImages)) {
            foreach ($moreImages as $uploadedFile) {
                if(!FileService::isImageExtension($uploadedFile->extension)){
                    continue;
                }
                $baseNameFiltered = FileService::filterNameForThumbs($uploadedFile->baseName);
                $uploadedFile->saveAs(Yii::getAlias('@frontend').'/web/static/uploads/printers/' . $baseNameFiltered . '.' . $uploadedFile->extension);
                $fileIds[] = '/static/uploads/printers/' . $baseNameFiltered . '.' . $uploadedFile->extension;
            }
        }
        $model->images_json = $fileIds;

        // files
        $printerFiles = UploadedFile::getInstances($model, 'printerFiles');
        $printerFileIds = (array)$model->files_json;
        if (!empty($printerFiles)) {
            foreach ($printerFiles as $uploadedFile) {
                $baseNameFiltered = $uploadedFile->baseName;
                if(in_array(strtolower($uploadedFile->extension), ['php', 'pl', 'cgi', 'exe', 'bat', 'js', 'com', 'cmd', 'ps', 'ps1'])){
                    continue;
                }
                $uploadedFile->saveAs(Yii::getAlias('@frontend') . '/web/static/uploads/printers/' . $baseNameFiltered . '.' . $uploadedFile->extension);
                $printerFileIds[] = '/static/uploads/printers/' . $baseNameFiltered . '.' . $uploadedFile->extension;
            }
        }
        $model->files_json = $printerFileIds;

        return $model;
    }
}
