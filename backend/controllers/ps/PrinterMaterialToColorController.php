<?php

namespace backend\controllers\ps;

use common\models\Model3d;
use frontend\models\model3d\Model3dFacade;

/**
 *
 */
class PrinterMaterialToColorController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer-material-to-color';
    public $accessGroup = 'printer';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterMaterialToColorSearch();
        $this->mainModel = new \common\models\PrinterMaterialToColor();
        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'beforeCreate']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);
    }

    public function beforeCreate($event)
    {
        $now = new \yii\db\Expression('NOW()');
        $event->sender->created_at = $now;
        $event->sender->updated_at = $now;
    }

    public function beforeUpdate($event)
    {
        $event->sender->updated_at = new \yii\db\Expression('NOW()');
    }
    
    public function actionCreate()
    {
        \backend\components\AdminAccess::validateAccess($this->accessGroup . '.add');
        
        $model = $this->mainModel;
        if(app('request')->isPost){
            $post = app('request')->post();
            $model->load($post);        
            $colors = $model->color_id;
            $msg = [];
            foreach($colors as $k=>$colorId){
                $modelNew = clone $model;
                $modelNew->color_id = $colorId; 
                
                if ($modelNew->save()) {
                   
                }else{
                    $msg[] = \yii\helpers\Html::errorSummary($modelNew);
                    
                }
            }
            if(!empty($msg)){
                \Yii::$app->getSession()->setFlash('error', implode("<br />", $msg));
            }else{
                \Yii::$app->getSession()->setFlash('success', 'Successfully created');
            }
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }
}
