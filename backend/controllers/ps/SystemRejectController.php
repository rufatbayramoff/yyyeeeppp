<?php

namespace backend\controllers\ps;

use backend\components\AdminAccess;
use yii\base\UserException;

/**
 *
 */
class SystemRejectController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/system-reject';

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\SystemRejectSearch();
        $this->mainModel = new \common\models\SystemReject();
    }

    public function actionDelete($id)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        app('cache')->set('back.notify.data', '');
        AdminAccess::validateAccess($this->accessGroup . '.delete');

        $model = $this->findModel($id);
        if($model){
            $this->setFlashMsg(true, 'Marked as inactive.');
            $model->is_active = 0;
            $model->save();
        }
        return $this->redirect(['index']);
    }
}
