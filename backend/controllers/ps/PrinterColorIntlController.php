<?php

namespace backend\controllers\ps;

/**
 *
 */
class PrinterColorIntlController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer-color-intl';
    public $accessGroup = 'printer';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterColorIntlSearch();
        $this->mainModel = new \common\models\PrinterColorIntl();
    }
}
