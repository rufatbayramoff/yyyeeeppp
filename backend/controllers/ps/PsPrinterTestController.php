<?php

namespace backend\controllers\ps;

use backend\components\AdminAccess;
use backend\models\search\PsPrinterTestSearch;
use common\components\DateHelper;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\interfaces\FileBaseInterface;
use common\models\CompanyService;
use common\models\PsPrinter;
use common\models\PsPrinterTest;
use common\models\repositories\FileRepository;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyServiceInformer;
use common\modules\tsCertification\services\TsCertificationService;
use common\modules\tsInternalPurchase\services\TsInternalPurchaseService;
use common\services\PsPrinterService;
use lib\crypto\Crypto;
use Yii;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 *
 */
class PsPrinterTestController extends \backend\components\AdminController
{
    protected $viewPath = '@backend/views/ps/printer-test';
    public $accessGroup = 'psprintertest';

    /** @var TsInternalPurchaseService */
    public $tsInternalPurchaseService;

    public function injectDependencies(TsInternalPurchaseService $tsInternalPurchaseService)
    {
        $this->tsInternalPurchaseService = $tsInternalPurchaseService;
    }

    /**
     * List of test requests
     * @return string
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('psprintertest.view');
        $searchModel = new PsPrinterTestSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => $searchModel->search(\Yii::$app->request->queryParams)->query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort'=> [
                'defaultOrder' => [
                    'updated_at' => SORT_DESC,
                ],
                'attributes' => [
                    'created_at',
                    'updated_at',
                    'decision_at'
                ],
            ]
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $searchModel]);
    }

    /**
     * @param $psPrinterId
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionApprove($psPrinterId)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        AdminAccess::validateAccess('psprintertest.approve');
        /** @var PsPrinter $psPrinter */
        $psPrinter = PsPrinter::tryFindByPk($psPrinterId);
        $psPrinter->test->status = PsPrinterTest::STATUS_CHECKED;
        $psPrinter->test->decision_at = DateHelper::now();
        AssertHelper::assertSave($psPrinter->test);

        $psPrinter->companyService->certification = CompanyService::CERT_TYPE_VERIFIED;
        $psPrinter->companyService->certification_expire = DateHelper::addNow('P1Y');
        AssertHelper::assertSave($psPrinter->companyService);
        AssertHelper::assertSave($psPrinter);

        if ($psPrinter->psPrinterTest->tsInternalPurchase) {
            $this->tsInternalPurchaseService->transferFromReservedToTreatstock($psPrinter->psPrinterTest->tsInternalPurchase);
        }

        /** @var FileRepository $fileRepository */
        $fileRepository = Yii::createObject(FileRepository::class);

        foreach ($psPrinter->test->getFiles() as $imageFile) {
            $imageFile->setPublicMode(true);
            $fileRepository->save($imageFile);
        }

        InformerModule::addInformer($psPrinter->ps->user, CompanyServiceInformer::class);

        $emailer = new Emailer();
        $emailer->sendPrinterTestSuccess($psPrinter->test);

        return $this->redirect('index');
    }

    public function actionRepeat($psPrinterId)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        $psPrinter = PsPrinter::tryFindByPk($psPrinterId);
        $psPrinter->test->status = PsPrinterTest::STATUS_REPEAT;
        $psPrinter->test->safeSave();
        $psPrinter->companyService->certification = CompanyService::CERT_TYPE_PROGRESS;
        $psPrinter->companyService->safeSave();
        return $this->redirect('index');
    }

    /**
     * Reject test
     *
     * @param $psPrinterId
     * @return \yii\web\Response
     * @throws UserException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReject($psPrinterId)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        AdminAccess::validateAccess('psprintertest.reject');
        /** @var PsPrinter $psPrinter */
        $psPrinter = PsPrinter::tryFindByPk($psPrinterId);
        $psPrinter->test->status = PsPrinterTest::STATUS_FAILED;
        $psPrinter->test->comment = Yii::$app->request->post('comment', null);
        $psPrinter->test->decision_at = new Expression("NOW()");
        AssertHelper::assertSave($psPrinter->test);

        $psPrinter->companyService->certification = CompanyService::CERT_TYPE_REJECTED;
        $psPrinter->companyService->certification_expire = null;
        AssertHelper::assertSave($psPrinter->companyService);
        AssertHelper::assertSave($psPrinter);

        $emailer = new Emailer();
        $emailer->sendPrinterTestFailed($psPrinter->test);
        InformerModule::addInformer($psPrinter->ps->user, CompanyServiceInformer::class);
        return $this->redirect('index');
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param int $id
     * @return mixed|void
     * @throws UserException
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        AdminAccess::validateAccess('psprintertest.view');
        AdminAccess::validateAccess('psprintertest.approve');
        AdminAccess::validateAccess('psprintertest.reject');
        $model = $this->findModel($id);
        $model->currentFormName = 'psPrinterTest';

        if (app('request')->isPost) {
            $model->load(\Yii::$app->request->post());
            $model->safeSave();
            $imageFiles = UploadedFile::getInstances($model, 'imageFiles');
            $model->updatePhotoFiles(\Yii::$app->request->post(), $imageFiles);
            PsPrinterService::updatePrintersCache();
            return $this->redirect('index');
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * @param $id
     *
     * @return PsPrinterTest
     * @throws NotFoundHttpException
     */
    protected function findModel($id): PsPrinterTest
    {
        if (($model = PsPrinterTest::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
