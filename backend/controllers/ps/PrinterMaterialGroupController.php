<?php

namespace backend\controllers\ps;

use common\interfaces\FileBaseInterface;
use common\models\factories\FileFactory;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use common\models\repositories\FileRepository;
use common\services\FileService;
use Yii;
use yii\web\UploadedFile;

/**
 *
 */
class PrinterMaterialGroupController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer-material-group';
    public $accessGroup = 'printer';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterMaterialGroupSearch();
        $this->mainModel = new \common\models\PrinterMaterialGroup();
        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'beforeCreate']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);
        $this->on(self::EVENT_BEFORE_MODEL_LOAD, [$this, 'beforeModelLoad']);
    }

    public function beforeCreate($event)
    {
        $now = new \yii\db\Expression('NOW()');
        $event->sender->created_at = $now;
        $event->sender->updated_at = $now;
    }

    /**
     * @param $event
     * @throws \Exception
     */
    public function beforeUpdate($event)
    {
        $event->sender->updated_at = new \yii\db\Expression('NOW()');
        if ($event->sender->photoFile) {
            $fileRepository = Yii::createObject(FileRepository::class);
            $fileRepository->save($event->sender->photoFile);
        }
    }

    /**
     * @param $event
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeModelLoad($event)
    {
        // TODO: Рассмотреть возможность переноса функционала сохранения файлов в основной контроллер
        if (isset($event->sender['model'])) {
            $printerMaterialGroup = $event->sender['model'];
            $uploadedFile = UploadedFile::getInstance($printerMaterialGroup, 'photoFile');
            if ($uploadedFile) {
                /** @var FileFactory $fileFactory */
                $fileFactory = Yii::createObject(FileFactory::class);
                $photoFile = $fileFactory->createFileFromUploadedFile($uploadedFile, FileBaseInterface::TYPE_FILE_ADMIN);
                $photoFile->setPublicMode(true);
                $photoFile->setFixedPath('printer-material-group');
                $photoFile->setOwner(PrinterMaterialGroup::class, 'photo_file_uuuid');
                if ($photoFile) {
                    $printerMaterialGroup->photoFile = $photoFile;
                }
            }
        }
    }
}
