<?php

namespace backend\controllers\ps;

/**
 *
 */
class PrinterPropertiesIntlController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer-properties-intl';
    public $accessGroup = 'printer';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterPropertiesIntlSearch();
        $this->mainModel = new \common\models\PrinterPropertiesIntl();
    }
}
