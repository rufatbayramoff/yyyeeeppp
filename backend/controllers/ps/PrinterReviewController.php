<?php
/**
 * User: nabi
 */

namespace backend\controllers\ps;


use backend\components\AdminAccess;
use backend\components\CrudController;
use backend\models\ps\PrinterReviewRejectForm;
use common\models\PrinterReview;

class PrinterReviewController extends CrudController
{
    protected $viewPath = '@backend/views/ps/printer-review';
    public $accessGroup = 'printer_review';

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterReviewSearch();
        $this->mainModel = new \common\models\PrinterReview();
    }


    public function actionPublish($id)
    {
        AdminAccess::validateAccess('printer_review.update');
        /** @var PrinterReview $model */
        $model = $this->findModel($id);
        $model->status = PrinterReview::STATUS_PUBLISHED;
        $model->safeSave();
        return $this->redirect(app('request')->referrer);
    }
    /**
     *
     * @transacted
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\UserException
     */
    public function actionReject($id)
    {
        AdminAccess::validateAccess('printer_review.update');
        $model = $this->findModel($id);
        $rejectForm = new PrinterReviewRejectForm();
        $currentUser = $this->getCurrentUser();

        $post = app('request')->post();
        if ($post) {
            $rejectForm->load($post);
            $rejectForm->setUser($currentUser);
            $rejectForm->saveRejectStatus($model);
            return $this->jsonSuccess(['reload' => true]);
        }

        return $this->renderAdaptive(
            '@backend/views/ps/ps/reject.php',
            [
                'model'      => $model,
                'rejectForm' => $rejectForm
            ]
        );
    }
}