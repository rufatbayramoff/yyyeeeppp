<?php
/**
 * Created by mitaichik
 */

namespace backend\controllers\ps;


use backend\controllers\store\StoreOrderController;

class TestOrderController extends StoreOrderController
{
    public $accessGroup = 'psprinter.testorder';
    public $notTest = false;
    public $onlyTest = true;

}