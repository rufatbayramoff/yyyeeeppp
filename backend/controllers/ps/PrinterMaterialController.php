<?php

namespace backend\controllers\ps;

use common\components\DateHelper;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use common\services\FileService;
use Yii;
use yii\web\UploadedFile;

/**
 *
 */
class PrinterMaterialController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer-material';
    public $accessGroup = 'printer';

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterMaterialSearch();
        $this->mainModel = new \common\models\PrinterMaterial();
        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'beforeCreate']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);
        $this->on(self::EVENT_BEFORE_MODEL_LOAD, [$this, 'beforeModelLoad']);
    }

    public function beforeCreate($event)
    {
        $now = new \yii\db\Expression('NOW()');
        $event->sender->created_at = $now;
        $event->sender->updated_at = $now;
    }

    /**
     * @param $event
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeUpdate($event)
    {
        $event->sender->updated_at = DateHelper::now();
    }

    /**
     * @param $event
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function beforeModelLoad($event)
    {
        // TODO: Рассмотреть возможность переноса функционала сохранения файлов в основной контроллер
        if (isset($event->sender['model'])) {
            $printerMaterial = $event->sender['model'];
            $uploadedFile = UploadedFile::getInstance($printerMaterial, 'photoFile');
            if ($uploadedFile) {
                /** @var FileFactory $fileFactory */
                $fileFactory = Yii::createObject(FileFactory::class);
                $photoFile = $fileFactory->createFileFromUploadedFile($uploadedFile);
                if ($photoFile) {
                    $printerMaterial->photoFile = $photoFile;
                }
                FileService::bindModel($printerMaterial->photoFile, $printerMaterial, 'photo_file_id');
            }
        }
    }
}
