<?php

namespace backend\controllers\ps;

use common\models\PrinterToMaterial;

/**
 *
 */
class PrinterToMaterialController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/printer-to-material';
    public $accessGroup = 'printer';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PrinterToMaterialSearch();
        $this->mainModel = new \common\models\PrinterToMaterial();
        $this->on(self::EVENT_BEFORE_CREATE, [$this, 'beforeCreate']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);
    }

    public function beforeCreate($event)
    {
        $now = new \yii\db\Expression('NOW()');
        $event->sender->created_at = $now;
        $event->sender->updated_at = $now;
    }

    public function beforeUpdate($event)
    {
        $event->sender->updated_at = new \yii\db\Expression('NOW()');
    }
     
    public function actionCreate()
    {
        \backend\components\AdminAccess::validateAccess($this->accessGroup . '.add');

        /** @var PrinterToMaterial $model */
        $model = $this->mainModel;
        if(app('request')->isPost){
            $post = app('request')->post();
            $model->load($post);        
            $materials = $model->material_id;
            $msg = [];
            $now = new \yii\db\Expression('NOW()');

            /** @var int[] $materials */
            foreach($materials as $k => $materialId){
                $modelNew = clone $model;
                $modelNew->material_id = $materialId;

                $modelNew->created_at = $now;
                $modelNew->updated_at = $now;
                
                if ($modelNew->save()) {
                   
                }else{
                    $msg[] = \yii\helpers\Html::errorSummary($modelNew);
                    
                }
            }
            if(!empty($msg)){
                \Yii::$app->getSession()->setFlash('error', implode("<br />", $msg));
            }else{
                \Yii::$app->getSession()->setFlash('success', 'Successfully created');
            }
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }
}
