<?php

namespace backend\controllers\ps;
 

/**
 * 
 */
class GeoLocationController  extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/geo-location';

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\GeoLocationSearch();
        $this->mainModel = new \common\models\GeoLocation();
    }
}
