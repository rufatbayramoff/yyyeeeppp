<?php

namespace backend\controllers\ps;

/**
 *
 */
class SystemRejectIntlController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/ps/system-reject-intl';

    protected $accessGroup = 'systemreject';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\SystemRejectIntlSearch();
        $this->mainModel = new \common\models\SystemRejectIntl();
    }
}
