<?php

namespace backend\controllers\ps;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\report\PsPrinterReport;
use backend\modules\statistic\reports\ReportExcelWriter;
use common\components\BaseActiveQuery;
use common\components\DateHelper;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\components\order\TestOrderFactory;
use common\models\CompanyService;
use common\models\PsPrinter;
use common\models\StoreOrder;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyServiceInformer;
use common\services\PsPrinterService;
use common\services\StoreOrderService;
use Yii;
use yii\base\UserException;
use yii\web\Response;

/**
 *
 */
class PsPrinterController extends AdminController
{
    /**
     * @var StoreOrderService
     */
    private $orderService;

    /**
     * @var TestOrderFactory
     */
    private $testOrderFactory;

    /**
     * @param StoreOrderService $orderService
     * @param TestOrderFactory $testOrderFactory
     */
    public function injectDependencies(StoreOrderService $orderService, TestOrderFactory $testOrderFactory): void
    {
        $this->orderService     = $orderService;
        $this->testOrderFactory = $testOrderFactory;
    }


    protected $viewPath = '@backend/views/ps/ps-printer';


    public function actionIndex()
    {
        AdminAccess::validateAccess('psprinter.view');
        $searchModel  = Yii::createObject(\backend\models\search\PsPrinterSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        \backend\components\AdminAccess::validateAccess('psprinter.view');
        $currentUser = $this->getCurrentUser();

        $searchLogModel = new \backend\models\search\ModerLogSearch();
        $model          = PsPrinter::tryFindByPk($id);

        $paramsGet = app('request')->queryParams;

        $params          = \yii\helpers\ArrayHelper::merge(
            $paramsGet,
            ['ModerLogSearch' => ['object_id' => $id, 'object_type' => 'ps_printer']]
        );
        $logDataProvider = $searchLogModel->search($params);

        $searchHistoryModel  = new \backend\models\search\PsPrinterHistorySearch();
        $params2             = \yii\helpers\ArrayHelper::merge(
            $paramsGet,
            ['PsPrinterHistorySearch' => ['ps_printer_id' => $id]]
        );
        $dataHistoryProvider = $searchHistoryModel->search($params2);

        $moderate = false;
        $orders   = StoreOrder::find()->forPrinterId($id)->all();

        return $this->render(
            'view',
            [
                'model'        => $model,
                'moderate'     => $moderate,
                'orders'       => $orders,
                'log'          => [
                    'searchModel'  => $searchLogModel,
                    'dataProvider' => $logDataProvider
                ],
                'modelHistory' => [
                    'searchModel'  => $searchHistoryModel,
                    'dataProvider' => $dataHistoryProvider
                ]
            ]
        );
    }

    /**
     * @param $id
     * @throws UserException
     * @throws \backend\components\NoAccessException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionApprove($id)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        \backend\components\AdminAccess::validateAccess('psprinter.moderate');
        $currentUser = $this->getCurrentUser();

        /** @var PsPrinter $model */
        $model = PsPrinter::tryFindByPk($id);

        $printerUser = $model->ps->user;

        // validate tax information

        if (!empty($printerUser->userTaxInfo)) {
            $res = \frontend\models\site\DenyCountry::checkCountry($printerUser->userTaxInfo->place_country, false);
            if (!empty($printerUser->userTaxInfo->address)) {
                $res = $res && \frontend\models\site\DenyCountry::checkCountry($printerUser->userTaxInfo->address->country->iso_code, false);
            }
            if (!$res) {
                #throw new \yii\base\UserException("This printer cannot be approved, user tax information filled with country from black list");
            }
        }
        if (empty($model->materials)) {
            $this->setFlashMsg(false, 'Cannot be published. No color/materials prices.');
        } else {
            if (empty($model->deliveries)) {
                $this->setFlashMsg(false, 'Cannot be published. No delivery information.');
            } else {
                if ($printerUser->status == \common\models\User::STATUS_ACTIVE) {
                    InformerModule::addInformer($printerUser, CompanyServiceInformer::class);
                    $model->setStatus($id, $currentUser->id, CompanyService::MODERATOR_STATUS_APPROVED);

                    $emailer = new Emailer();
                    $emailer->sendPsServiceApproved($model->companyService);


                    app('cache')->set('back.notify.data', '');
                } else {
                    $this->setFlashMsg(false, 'This printer cannot be approved. This user account is not confirmed or not active!');
                }
            }
        }

        $this->redirect('/ps/ps-printer');
    }

    /**
     * @param $id
     * @return array|string
     * @throws \backend\components\NoAccessException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReject($id)
    {
        \backend\components\AdminAccess::validateAccess('psprinter.moderate');
        $currentUser = $this->getCurrentUser();

        /** @var PsPrinter $model */
        $model = PsPrinter::tryFindByPk($id);

        $rejectForm = new \backend\models\ps\PsPrinterRejectForm();

        $post = app('request')->post();
        if ($post) {

            $rejectForm->load($post);
            $rejectForm->saveRejectStatus($id, $currentUser->id);

            $model->companyService->moderator_status = CompanyService::MODERATOR_STATUS_REJECTED;
            $model->companyService->updated_at       = DateHelper::now();
            AssertHelper::assertSave($model->companyService);
            AssertHelper::assertSave($model);

            $emailer = new Emailer();
            $emailer->sendPrinterModerationReject($model, $rejectForm);

            InformerModule::addInformer($model->ps->user, CompanyServiceInformer::class);
            app('cache')->set('back.notify.data', '');
            Yii::$app->getSession()->setFlash('success', 'Printer successfully rejected!');
            return $this->jsonSuccess(['message' => 'reload']);
        }

        return $this->renderAjax(
            'reject',
            [
                'model'      => $model,
                'rejectForm' => $rejectForm
            ]
        );
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionSendTestOrder($id)
    {
        /** @var PsPrinter $model */
        $model = PsPrinter::tryFindByPk($id);
        $this->testOrderFactory->create($model);
        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @return Response
     */
    public function actionDownloadReport()
    {
        $report       = new PsPrinterReport();
        $reportWriter = new ReportExcelWriter('PsPrinters');
        \Yii::$app->getDb()->createCommand('SET SESSION SQL_BIG_SELECTS=1')->execute();
        return \Yii::$app->response->sendContentAsFile($reportWriter->write($report), 'ps_printers.xlsx', ['inline' => true]);
    }

    public function actionVisibility($id, $status = 'active')
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        \backend\components\AdminAccess::validateAccess('psprinter.moderate');
        $currentUser = $this->getCurrentUser();

        /** @var PsPrinter $model */
        $model       = PsPrinter::tryFindByPk($id);
        $printerUser = $model->ps->user;
        InformerModule::addInformer($printerUser, CompanyServiceInformer::class);
        $model->companyService->visibility = $status == CompanyService::VISIBILITY_EVERYWHERE ? CompanyService::VISIBILITY_EVERYWHERE : CompanyService::VISIBILITY_NOWHERE;
        $model->companyService->updated_at = DateHelper::now();
        $model->companyService->safeSave();
        $model->safeSave();
        PsPrinterService::updatePrintersCache();
        app('cache')->set('back.notify.data', '');
        $this->setFlashMsg(true, 'Printer status now "' . $model->companyService->visibility . '"');
        // return $this->redirect('/ps/ps-printer');

        return $this->redirect(\Yii::$app->request->referrer);
    }
}
