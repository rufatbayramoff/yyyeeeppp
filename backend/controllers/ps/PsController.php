<?php

namespace backend\controllers\ps;

use backend\components\AdminAccess;
use backend\components\CrudController;
use backend\models\ps\PsBannedForm;
use backend\models\ps\PsRejectForm;
use backend\models\search\ModerLogSearch;
use backend\models\search\PsSearch;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\models\Company;
use common\models\GeoCountry;
use common\models\ModerLog;
use common\models\Ps;
use common\models\CompanyService;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyInformer;
use common\services\PsPrinterService;
use common\services\StoreOrderService;
use Yii;
use yii\base\UserException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 *
 */
class PsController extends CrudController
{
    protected $viewPath = '@backend/views/ps/ps';

    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @var StoreOrderService
     */
    protected $storeOrderService;

    /**
     * @param Emailer $emailer
     * @param StoreOrderService $storeOrderService
     */
    public function injectDependencies(
        Emailer $emailer,
        StoreOrderService $storeOrderService
    ): void {
        $this->emailer = $emailer;
        $this->storeOrderService = $storeOrderService;
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(),[
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete-ps' => ['delete'],
                    'restore-ps' => ['post'],
                ]
            ]
        ]);
    }

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->searchModel = new PsSearch();
        $this->mainModel = new Company();
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionSetCountry()
    {
        $psAll = Ps::find()->with(['user', 'companyServices'])->where(['country_id' => null])->all();

        $noCountry = [];
        foreach ($psAll as $ps) {
            $user = $ps->user;
            $country = null;
            if ($user->userTaxInfo) {
                $country = GeoCountry::findOne(['iso_code' => $user->userTaxInfo->place_country]);
            }
            $psMachines = $ps->companyServices;
            foreach ($psMachines as $psMachine) {
                if ($psMachine->location) {
                    $country = $psMachine->location->country;
                    break;
                }
            }

            if (!empty($ps->phone_country_iso)) {
                $country = GeoCountry::findOne(['iso_code' => $ps->phone_country_iso]);
            }

            if (!$country) { // try from shipping info
                if ($user->userProfile && $user->userProfile->address) {
                    $country = $user->userProfile->address->country;
                }
            }
            if ($country) {
                $ps->country_id = $country->id;
                $ps->safeSave();
            } else {
                $noCountry[] = $ps->id;
            }
        }
        return $this->renderContent(
            sprintf("Found %s, not set: %s", count($psAll), implode(',', $noCountry))
        );

    }

    /**
     * @param int $id
     * @return mixed|string
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');
        $currentUser = $this->getCurrentUser(true);

        $searchLogModel = new ModerLogSearch();
        /** @var Ps $model */
        $model = $this->findModel($id);

        $paramsGet = app('request')->queryParams;

        $params = ArrayHelper::merge(
            $paramsGet,
            ['ModerLogSearch' => ['object_id' => $id, 'object_type' => 'ps']]
        );
        $logDataProvider = $searchLogModel->search($params);

        $already_moderated = $model->checkPsIsModerated($currentUser->id);

        $moderate = false;
        $get = Yii::$app->request->get();
        if (isset($get['moderate'])) {
            AdminAccess::validateAccess($this->accessGroup . '.moderate');
            $moderate = true;
            if (in_array($model->moderator_status, [Ps::MSTATUS_CHECKED, Ps::MSTATUS_REJECTED, Ps::MSTATUS_BANNED])) {
            } else {
                if (Ps::MSTATUS_CHECKING != $model->moderator_status) {
                    $model->setStatus($id, $currentUser->id, Ps::MSTATUS_CHECKING, 'In moderation');
                }
            }
        }

        return $this->render('view', [
            'model'             => $model,
            'moderate'          => $moderate,
            'already_moderated' => $already_moderated,
            'log'               => [
                'searchModel'  => $searchLogModel,
                'dataProvider' => $logDataProvider
            ]
        ]);
    }

    /**
     * @param int $id
     * @return mixed|void
     * @throws UserException
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $this->getCurrentUser(true);

        /** @var Ps $model */
        $model = $this->findModel($id);
        $model->load(Yii::$app->request->post());
        $model->safeSave();
        PsPrinterService::updatePrintersCache();
        $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionApprove($id)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $currentUser = $this->getCurrentUser(true);

        /** @var Ps $model */
        $model = $this->findModel($id);
        CompanyService::updateRow(['ps_id' => $model->id, 'type' => CompanyService::TYPE_PRINTER], ['updated_at' => dbexpr('NOW()')]);
        Yii::$app->cache->set('back.notify.data', '');
        $model->setStatus($id, $currentUser->id, Ps::MSTATUS_CHECKED, 'approved');
        InformerModule::addInformer($model->user, CompanyInformer::class);
        $this->emailer->sendPsModerationApproved($model);

        PsPrinterService::updatePrintersCache();
        $this->redirect('/ps/ps');
    }

    /**
     * @param $id
     * @return array|string
     * @throws UserException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReject($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $currentUser = $this->getCurrentUser(true);

        /** @var Ps $model */
        $model = $this->findModel($id);

        $rejectForm = new PsRejectForm();

        $post = Yii::$app->request->post();
        if ($post) {
            $rejectForm->load($post);
            $rejectForm->saveRejectStatus($id, $currentUser->id);
            CompanyService::updateRow(['ps_id' => $model->id, 'type' => CompanyService::TYPE_PRINTER], ['updated_at' => dbexpr('NOW()')]);
            $model->moderator_status = Ps::MSTATUS_REJECTED;
            AssertHelper::assertSave($model);

            $this->emailer->sentPsModerationReject($model, $rejectForm);
            InformerModule::addInformer($model->user, CompanyInformer::class);
            Yii::$app->cache->set('back.notify.data', '');
            PsPrinterService::updatePrintersCache();
            return $this->jsonSuccess(['message' => 'Print Service successfully rejected!']);
        }

        return $this->renderAjax('reject', [
            'model' => $model, 'rejectForm' => $rejectForm
        ]);
    }

    /**
     * @param $id
     * @return array|string
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionBanned($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $currentUser = $this->getCurrentUser(true);

        /** @var Ps $model */
        $model = $this->findModel($id);

        $bannedForm = new PsBannedForm();

        $post = Yii::$app->request->post();
        if ($post) {
            CompanyService::updateRow(['ps_id' => $model->id, 'type' => CompanyService::TYPE_PRINTER], ['updated_at' => dbexpr('NOW()')]);
            $bannedForm->load($post);
            $bannedForm->saveBannedStatus($id, $currentUser->id);

            $model->moderator_status = Ps::MSTATUS_BANNED;
            $model->safeSave();

            $this->emailer->sendPsBanned($model, $bannedForm);

            Yii::$app->cache->set('back.notify.data', '');
            PsPrinterService::updatePrintersCache();
            InformerModule::addInformer($model->user, CompanyInformer::class);
            return $this->jsonSuccess(['message' => 'Print Service successfully banned!']);
        }

        return $this->renderAjax('banned', [
            'model' => $model, 'bannedForm' => $bannedForm
        ]);
    }

    /**
     * @param int $id Id of ps
     * @param bool $isExclude is exclude from printing
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSetExcludeFromPrinting($id, $isExclude)
    {
        /** @var Ps $model */
        $model = $this->findModel($id);
        $model->setIsExcludedFromPrinting((bool)$isExclude);

        $user = $this->getCurrentUser();
        $model = new ModerLog();
        $model->user_id = $user->id;
        $model->created_at = dbexpr('NOW()');
        $model->started_at = dbexpr('NOW()');
        $model->action = 'exclude';
        $model->result = $isExclude;
        $model->object_type = 'ps';
        $model->object_id = $id;
        AssertHelper::assertSave($model);

        $this->redirect(['view', 'id' => $id, 'moderate' => 1]);
    }

    /**
     * @param int $id Id of ps
     * @param bool $isExclude is exclude from printing
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSetAllowCnc($id, $isExclude)
    {
        /** @var Ps $model */
        $model = $this->findModel($id);
        $model->is_cnc_allowed = $isExclude;
        AssertHelper::assertSave($model);
        $this->redirect(['view', 'id' => $id, 'moderate' => 1]);
    }


    public function actionUpdateFee($id)
    {
        $post = Yii::$app->request->post();
        $fee = null;
        if (empty($post['Ps']) || $post['Ps']['fee_percent'] === '') {
        } else {
            $fee = (float)$post['Ps']['fee_percent'];
        }
        $model = Ps::tryFindByPk($id);
        $model->fee_percent = $fee;
        AssertHelper::assertSave($model);

        $user = $this->getCurrentUser();
        $model = new ModerLog();
        $model->user_id = $user->id;
        $model->created_at = dbexpr('NOW()');
        $model->started_at = dbexpr('NOW()');
        $model->action = 'fee';
        $model->result = (string)$fee;
        $model->object_type = 'ps';
        $model->object_id = $id;

        AssertHelper::assertSave($model);

        $this->setFlashMsg(true, 'Personal company fee now <b>' . $fee * 100 . '%</b> !');
        $this->redirect(app('request')->referrer);
    }

    public function actionDeletePs($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $currentUser = $this->getCurrentUser(true);
        /** @var Ps $ps */
        $ps = $this->findModel($id);
        $this->storeOrderService->checkActiveOrder($ps->user);
        try {
            $ps->softDelete();
            $ps->safeSave();
            $model = ModerLog::createPsLog($ps,'deletePs', $currentUser);
            AssertHelper::assertSave($model);
        } catch (\DomainException $exception){
            $this->setFlashMsg(false, $exception->getMessage());
        }
        $this->redirect(['view', 'id' => $id, 'moderate' => 1]);
    }

    public function actionRestorePs($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $currentUser = $this->getCurrentUser(true);
        try {
            /** @var Ps $ps */
            $ps = $this->findModel($id);
            $ps->restore();
            $ps->safeSave();
            $model = ModerLog::createPsLog($ps,'restorePs', $currentUser);
            AssertHelper::assertSave($model);
        } catch (\DomainException $exception){
            $this->setFlashMsg(false, $exception->getMessage());
        }
        $this->redirect(['view', 'id' => $id, 'moderate' => 1]);
    }
}
