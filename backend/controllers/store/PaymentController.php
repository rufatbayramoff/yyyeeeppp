<?php namespace backend\controllers\store;

use common\models\PaymentInvoice;
use common\modules\payment\services\PaymentReceiptService;
use frontend\models\community\TopicMerger;
use Yii;
use common\models\Payment;
use backend\models\search\PaymentSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends \backend\components\CrudController
{

    protected $viewPath = '@backend/views/payment/payment';

    /** @var PaymentReceiptService */
    public $receiptService;

    public function injectDependencies(PaymentReceiptService $paymentReceiptService)
    {
        $this->receiptService = $paymentReceiptService;
    }

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PaymentSearch();
        $this->mainModel = new \common\models\Payment();
    }

    public function actionView($id)
    {
        \backend\components\AdminAccess::validateAccess($this->accessGroup . '.view');

        /** @var Payment $model */
        $model = $this->findModel($id);

        $dataProviderDetail = new ActiveDataProvider([
            'query' => $model->getPaymentDetails()->sort()
        ]);

        return $this->render('view', [
                'model' => $model,
                'dataProviderDetail' => $dataProviderDetail
        ]);
    }

    public function actionViewInvoice($uuid)
    {
        $paymentInvoice = PaymentInvoice::tryFind(['uuid'=>$uuid]);
        $receipt = $this->receiptService->getReceiptByInvoiceForUser($paymentInvoice, $paymentInvoice->user);
        $filePdf = $this->receiptService->getPdfReceipt($receipt);
        $this->response->setDownloadHeaders($receipt->getCombinedId() . '.pdf', 'application/pdf', true);
        return $this->response->sendFile($filePdf);
    }
}
