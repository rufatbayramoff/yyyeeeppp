<?php

namespace backend\controllers\store;

use backend\components\AdminAccess;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentDetailOperation;
use common\models\User;
use common\modules\payment\exception\FatalPaymentException;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use lib\money\Money;
use yii\data\ActiveDataProvider;

/**
 * Class PaymentCorrectionController
 *
 * @package backend\controllers\store
 *
 * @property PaymentAccountService $paymentAccountService
 * @property PaymentService $paymentService
 */
class PaymentCorrectionController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/store/payment-correction';

    protected $accessGroup = 'payment';

    protected $paymentAccountService;

    protected $paymentService;

    public function injectDependencies(
        PaymentAccountService $paymentAccountService,
        PaymentService $paymentService
    ) {
        $this->paymentAccountService = $paymentAccountService;
        $this->paymentService = $paymentService;
    }

    public function init()
    {
        parent::init();
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'transfer-money' => ['post']
                ]
            ],
        ];
    }

    /**
     * @return mixed|string
     * @throws \yii\base\UserException
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');

        $query = Payment::find()
            ->from(['p' => Payment::tableName()])
            ->innerJoin(['o' => PaymentDetailOperation::tableName()], 'p.id = o.payment_id')
            ->innerJoin(
                ['detail' => PaymentDetail::tableName()],
                'o.uuid = detail.payment_detail_operation_uuid and detail.type = :type',
                ['type' => PaymentDetail::TYPE_CORRECTION]
            )
            ->groupBy(['p.id']);

        $dataProviderDetail = new ActiveDataProvider([
            'query' => $query
        ]);

        $dataProviderDetail->sort = ['defaultOrder' => ['created_at' => SORT_DESC]];

        return $this->render('index', [
            'dataProviderDetail' => $dataProviderDetail
        ]);
    }

    protected function getAccountTypesList()
    {
        return array_merge(
            PaymentAccount::getUserAccountTypes(),
            PaymentAccount::getTreatstockAccountTypes()
        );
    }

    /**
     * @return mixed|string
     * @throws \yii\base\UserException
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');

        $payment = Payment::findOne(app()->request->get('paymentId'));
        $user = User::findOne(app()->request->get('userId'));
        $dataProviderDetail = null;

        if ($payment) {
            $dataProviderDetail = new ActiveDataProvider([
                'query' => $payment->getPaymentDetails()
            ]);

            $dataProviderDetail->sort = ['defaultOrder' => ['updated_at' => SORT_DESC]];
        }

        $userAccountTypes = [];

        foreach ($this->getAccountTypesList() as $type) {
            $userAccountTypes[$type] = ucfirst($type);
        }

        return $this->render('create', [
            'user'               => $user,
            'userAccountTypes'   => $userAccountTypes,
            'payment'            => $payment,
            'dataProviderDetail' => $dataProviderDetail
        ]);
    }

    /**
     * @return \yii\web\Response
     * @throws PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionTransferMoney()
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');

        $user = User::tryFindByPk(app()->request->post('user_id'));
        $payment = Payment::tryFindByPk(app()->request->post('payment_id'));
        $accountType = app()->request->post('account_type');
        $money = Money::create(app()->request->post('amount'), app()->request->post('currency'));
        $direction = app()->request->post('direction');

        if (!\in_array($accountType, $this->getAccountTypesList(), true)) {
            throw new FatalPaymentException('Incorrect Account type');
        }

        $userPaymentAccount = $this->paymentAccountService->getUserPaymentAccount($user, $accountType, $money->getCurrency());
        $treatstockCorrectionAccount = $this->paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_CORRECTION, $money->getCurrency());

        $from = $direction === 'from_u_to_t' ? $userPaymentAccount : $treatstockCorrectionAccount;
        $to = $direction === 'from_u_to_t' ? $treatstockCorrectionAccount : $userPaymentAccount;

        $this->paymentService->transferMoney(
            $payment,
            $from,
            $to,
            $money,
            PaymentDetail::TYPE_CORRECTION,
            app()->request->post('comment')
        );

        return $this->redirect(['/store/payment-correction/create', 'paymentId' => $payment->id, 'userId' => $user->id]);
    }
}
