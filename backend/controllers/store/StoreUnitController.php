<?php

namespace backend\controllers\store;

use backend\components\AdminAccess;
use backend\models\search\Model3dHistorySearch;
use backend\models\search\ModerLogSearch;
use backend\models\search\StoreUnitSearch;
use backend\models\store\ChangeStoreUnitTextureForm;
use backend\models\store\StoreUnitForm;
use common\actions\CropImageAction;
use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\interfaces\Model3dBaseInterface;
use common\models\File;
use common\models\FileJob;
use common\models\message\builders\TopicBuilder;
use common\models\Model3d;
use common\models\Model3dImg;
use common\models\Model3dPart;
use common\models\ModerLog;
use common\models\repositories\Model3dRepository;
use common\models\StoreUnit;
use common\modules\informer\InformerModule;
use common\modules\informer\models\ProductInformer;
use common\modules\product\interfaces\ProductInterface;
use common\services\AntiVirusService;
use common\services\Model3dImageService;
use common\services\Model3dPartService;
use common\services\Model3dService;
use console\jobs\model3d\ParserJob;
use console\jobs\QueueGateway;
use frontend\models\model3d\Model3dFacade;
use lib\render\RenderConfigRotate;
use lib\render\RenderLocalRotate;
use lib\render\RenderLocalScale;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\helpers\Html;

/**
 * StoreUnitController implements the CRUD actions for StoreUnit model.
 * Give functions to publish or reject publish store units.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class StoreUnitController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/store/unit';

    /** @var  Model3dImageService */
    protected $model3dImageService;

    /** @var Model3dRepository */
    protected $model3dRepository;

    public function injectDependencies(
        Model3dImageService $model3dImageService,
        Model3dRepository $model3dRepository
    )
    {
        $this->model3dImageService = $model3dImageService;
        $this->model3dRepository   = $model3dRepository;
    }

    public function init()
    {
        parent::init();
        $this->searchModel = new StoreUnitSearch();
        $this->mainModel   = new \common\models\StoreUnit();
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'crop-submit' => [
                'class' => CropImageAction::class
            ]
        ];
    }

    /**
     * list records
     *
     * @return mixed
     */
    public function actionDeleted()
    {
        /** @var StoreUnitSearch $searchModel */
        $searchModel              = $this->searchModel;
        $searchModel->onlyDeleted = true;
        $dataProvider             = $searchModel->search(Yii::$app->request->queryParams);

        AdminAccess::validateAccess($this->accessGroup . '.view');

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     *
     * @param int $id
     * @param bool $moderate
     * @return mixed
     * @throws Exception
     * @throws \yii\base\UserException
     */
    public function actionView($id, $moderate = false)
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');

        if ($moderate) {
            AdminAccess::validateAccess($this->accessGroup . '.moderate');
        }
        $searchLogModel = new ModerLogSearch();
        /** @var \common\models\StoreUnit $model */
        $model     = $this->findModel($id);
        $model3dId = $model->model3d->id;

        // try to rotate
        if (app('request')->get('rotate', false)) {
            Yii::$app->session->close();
            $model3dPartId = app('request')->get('model3dPart');
            $direction     = app('request')->get('rotate');
            $this->rotate($model3dPartId, $direction);
            $this->setFlashMsg(true, 'Model rotated.');
            return $this->redirect(['store/store-unit/view', 'id' => $id, 'moderate' => $moderate]);
        }

        // try to zoom
        if (app('request')->get('zoom', false)) {
            Yii::$app->session->close();
            try {
                $this->zoomModel(app('request')->get('model3dPart'), app('request')->get('zoom'));
                $this->setFlashMsg(true, 'Zoom done.');
            } catch (\Exception $ex) {
                $this->setFlashMsg(false, $ex->getMessage());
            }
            return $this->redirect(['store/store-unit/view', 'id' => $id, 'moderate' => $moderate]);
        }

        if (app('request')->get('revert', false)) {
            Yii::$app->session->close();
            $model3dPartId = app('request')->get('model3dPart');
            $this->revertOriginal($model3dPartId);
            $this->setFlashMsg(true, 'Model reverted to original.');
            return $this->redirect(['store/store-unit/view', 'id' => $id, 'moderate' => $moderate]);
        }

        // providers
        $paramsGet       = app('request')->queryParams;
        $params          = ArrayHelper::merge(
            $paramsGet,
            ['ModerLogSearch' => ['object_id' => $model3dId, 'object_type' => 'model3d']]
        );
        $logDataProvider = $searchLogModel->search($params);

        $searchHistoryModel  = new Model3dHistorySearch();
        $params2             = ArrayHelper::merge(
            $paramsGet,
            ['Model3dHistorySearch' => ['model3d_id' => $model3dId]]
        );
        $dataHistoryProvider = $searchHistoryModel->search($params2);

        return $this->render(
            'view',
            [
                'model'        => $model,
                'moderate'     => $moderate,
                'log'          => [
                    'searchModel'  => $searchLogModel,
                    'dataProvider' => $logDataProvider
                ],
                'modelHistory' => [
                    'searchModel'  => $searchHistoryModel,
                    'dataProvider' => $dataHistoryProvider
                ]
            ]
        );
    }

    /**
     * copy file jobs from old 3d model, to new rotated one.
     * we keep the same job results, because this is the same model and no need
     * for antivirus, parser to work again
     *
     * @param File $fileOld
     * @param File $fileNew
     */
    private function copyFileJobs(File $fileOld, File $fileNew)
    {
        // copy now file jobs
        $fileJobs = FileJob::findAll(
            [
                'file_id' => $fileOld->id,
                'status'  => QueueGateway::JOB_STATUS_COMPLETED
            ]
        );
        foreach ($fileJobs as $job) {
            $jobData = $job->getAttributes();
            unset($jobData['id']);
            $jobData['file_id'] = $fileNew->id;
            FileJob::addRecord($jobData);
        }
    }

    /**
     * @param $model3dPartId
     * @param $direction
     * @throws UserException
     */
    private function rotate($model3dPartId, $direction)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        $model3dPart  = Model3dPart::findByPk($model3dPartId);
        $oldFile      = File::findByPk($model3dPart->file_id);
        $rotateConfig = new RenderConfigRotate();
        $rotateConfig->rotateModelFile($direction);

        $localRender = new RenderLocalRotate();
        $localRender->run($model3dPart, $rotateConfig);

        $this->copyFileJobs($oldFile, File::findByPk($model3dPart->file_id));
        $this->updateModel3dCover($model3dPart, $model3dPart->file_id);
        Model3dPartService::checkAndRunFileRender($model3dPart);

        ModerLog::addRecord(
            [
                'object_type' => 'model3d',
                'object_id'   => $model3dPart->model3d->id,
                'user_id'     => \Yii::$app->getUser()->id,
                'action'      => 'rotate',
                'created_at'  => dbexpr('NOW()'),
                'result'      => sprintf('direction %s', $direction)
            ]
        );
    }


    /**
     * zoom model and log action
     *
     * @param $model3dPartId
     * @param $zoom
     * @return \lib\render\RenderRemoteResult|boolean
     * @throws UserException
     */
    private function zoomModel($model3dPartId, $zoom)
    {
        $zoomBy = (float)str_replace(',', '.', $zoom);
        if ($zoomBy <= 0 || $zoomBy > 50) {
            throw new BusinessException('Zoom must be > 0 and < 50');
        }
        $model3dPart  = Model3dPart::findByPk($model3dPartId);
        $oldFile      = File::findByPk($model3dPart->file_id);
        $rotateConfig = new RenderConfigRotate();
        $rotateConfig->zoomByPercent($zoomBy);

        $localRender  = new RenderLocalScale();
        $renderResult = $localRender->run($model3dPart, $rotateConfig);
        $this->copyFileJobs($oldFile, File::findByPk($model3dPart->file_id));
        $this->updateModel3dCover($model3dPart, $model3dPart->file_id);

        ModerLog::addRecord(
            [
                'object_type' => 'model3d',
                'object_id'   => $model3dPart->model3d->id,
                'user_id'     => \Yii::$app->getUser()->id,
                'action'      => 'zoom',
                'created_at'  => dbexpr('NOW()'),
                'result'      => sprintf('zoom by %s', $zoomBy)
            ]
        );
        return $renderResult;
    }

    /**
     * @param $model3dPart
     * @param $fileRotatedId
     * @return bool
     */
    private function updateModel3dCover(Model3dPart $model3dPart, $fileRotatedId)
    {
        if ($model3dPart->file_id === $model3dPart->model3d->cover_file_id ||
            ($model3dPart->model3d->isKit() && count($model3dPart->model3d->getActiveModel3dImages()) === 0)
        ) {
            if ($model3dPart->model3d->isKit() || $model3dPart->model3d->hasImages()) {
                return false;
            }
            $model3dObj                = $model3dPart->model3d;
            $model3dObj->cover_file_id = $fileRotatedId;
            return $model3dObj->safeSave();
        }
        return false;
    }

    /**
     * revert original 3d model
     *
     * @param $model3dPartId
     * @return bool
     * @throws Exception
     * @throws UserException
     */
    private function revertOriginal($model3dPartId)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }

        $model3dPart = Model3dPart::findByPk($model3dPartId);
        ModerLog::addRecord(
            [
                'object_type' => 'model3d',
                'object_id'   => $model3dPart->model3d->id,
                'user_id'     => \Yii::$app->getUser()->id,
                'action'      => 'revert_original',
                'created_at'  => dbexpr('NOW()'),
                'result'      => sprintf('old file_id %d, new file id %d', $model3dPart->file_id, $model3dPart->file_src_id)
            ]
        );

        $model3dPart->file_id                    = $model3dPart->file_src_id;
        $model3dPart->file_src_id                = null;
        $model3dPart->model3d_part_properties_id = null;
        $model3dPart->safeSave();
        if ($model3dPart->model3d->isKit() && count($model3dPart->model3d->getActiveModel3dImages()) === 0) {
            $model3dObj                = $model3dPart->model3d;
            $model3dObj->cover_file_id = $model3dPart->file_id;
            $model3dObj->safeSave();
        }
        QueueGateway::addFileJob(ParserJob::create(File::findOne(['id' => $model3dPart->file_id])), QueueGateway::ADD_STRATEGY_ANYWAY);
        return true;
    }

    /**
     * open crom image dialog
     *
     * @param int $id
     * @return string
     */
    public function actionCropImage($id)
    {
        $imgObj = Model3dImg::tryFindByPk($id);
        $this->model3dImageService->createOriginalCopyIfNotExists($imgObj);
        $imgPathOrig    = $imgObj->originalFile->getLocalTmpFilePath();
        $imgUrlOrig     = app('urlManager')->createUrl(['store/store-unit/proxy-image/?id=' . $imgObj->id]);
        $submitUrl      = app('urlManager')->createUrl(['store/store-unit/crop-submit/?id=' . $imgObj->id]);
        $this->viewPath = '@frontend/views/user/my';
        return $this->renderAjax(
            'model3dItemCrop',
            [
                'img'       => $imgObj,
                'imgPath'   => $imgPathOrig,
                'imgUrl'    => $imgUrlOrig,
                'submitUrl' => $submitUrl
            ]
        );
    }

    public function actionProxyImage($id)
    {
        $imgObj      = Model3dImg::tryFindByPk($id);
        $imgPathOrig = $imgObj->originalFile ? $imgObj->originalFile->getLocalTmpFilePath() : $imgObj->file->getLocalTmpFilePath();
        $fileExtType = str_replace("jpg", "jpeg", $imgObj->file->getFileExtension());
        header('Content-Type: image/' . $fileExtType);
        readfile($imgPathOrig);
        exit ;
    }

    public function actionChangeCae($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.publish');
        $model = $this->findModel($id);

        $model3d      = Model3d::tryFindByPk($model->model3d_id);
        $model3d->cae = $model3d->cae === Model3dBaseInterface::CAE_CAM ? Model3dBaseInterface::CAE_CAD : Model3dBaseInterface::CAE_CAM;
        $model3d->save();
        $this->setFlashMsg(true, 'Done');
        return $this->redirect(['store/store-unit/view', 'id' => $id, 'moderate' => true]);
    }

    /**
     * publish model in store
     *
     * @param int $id
     * @return \yii\web\Response
     * @throws UserException
     * @throws \backend\components\NoAccessException
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPublish($id)
    {
        if (!app('request')->isPost) {
            throw new BusinessException('Invalid request method. Need post method.');
        }
        AdminAccess::validateAccess($this->accessGroup . '.publish');

        /** @var StoreUnit $storeUnit */
        $storeUnit   = $this->findModel($id);
        $message     = 'Done';
        $result      = true;
        $transaction = app('db')->beginTransaction();
        try {
            if ($storeUnit->model3d->is_active && $storeUnit->model3d->product_status == ProductInterface::STATUS_PUBLISHED_PUBLIC) {
                throw new \yii\base\NotSupportedException(
                    'This model is already active. Cannot publish again!'
                );
            }
            if (!$storeUnit->model3d->isActive()) {
                throw new \yii\base\NotSupportedException(
                    'This model is deleted by owner. Cannot publish.'
                );
            }
            // check render/parser jobs

            $storeUnit->model3d->moderated_at                  = DateHelper::now();
            $storeUnit->model3d->productCommon->updated_at     = DateHelper::now();
            $storeUnit->model3d->productCommon->product_status = ProductInterface::STATUS_PUBLISHED_PUBLIC;

            if (!$storeUnit->validate()) {
                throw new Exception(Html::errorSummary($storeUnit));
            }

            $storeUnit->model3d->setScenario(Model3d::SCENARIO_PUBLISH);
            if (!$storeUnit->model3d->validate()) {
                throw new Exception(Html::errorSummary($storeUnit->model3d));
            }

            AssertHelper::assertSave($storeUnit);

            $user = app('user')->getIdentity();
            ModerLog::addRecord(
                [
                    'object_type' => 'model3d',
                    'object_id'   => $storeUnit->model3d->id,
                    'user_id'     => $user->id,
                    'action'      => 'published',
                    'created_at'  => dbexpr("NOW()"),
                    'result'      => 'published'
                ]
            );
            $storeUnit->model3d->publish();
            $transaction->commit();
            app('cache')->set('back.notify.data', '');
            InformerModule::addInformer($storeUnit->model3d->user, ProductInformer::class);
        } catch (\Exception $e) {
            $transaction->rollback();
            $result  = false;
            $message = $e->getMessage();
            $this->setFlashMsg($result, $message);
            return $this->redirect(['store/store-unit/view', 'id' => $id, 'moderate' => true]);
        }
        $this->setFlashMsg($result, $message);
        return $this->redirect(['store/store-unit/index']);
    }

    /**
     * reject publishing model in store.
     *
     * @param int $id
     * @return array
     */
    public function actionPublishReject($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.reject');
        /** @var \common\models\StoreUnit $model */
        $model      = $this->findModel($id);
        $rejectForm = new \backend\models\store\StoreUnitRejectForm();
        $post       = app('request')->post();
        if ($post) {
            $rejectForm->load($post);
            $rejectForm->unitId = $id;
            if ($rejectForm->validate()) {
                $transaction = app('db')->beginTransaction();
                try {
                    /** @var \common\models\Model3d $model3dObj */
                    $model3dObj = \common\models\Model3d::tryFindByPk($model->model3d_id);
                    $this->doReject($model3dObj, $rejectForm);

                    $model3dObj->productCommon->product_status = ProductInterface::STATUS_REJECTED;
                    $model3dObj->productCommon->updated_at     = DateHelper::now();
                    $model3dObj->productCommon->safeSave();
                    $model3dObj->safeSave();

                    $transaction->commit();
                    app('cache')->set('back.notify.data', '');
                    InformerModule::addInformer($model3dObj->user, ProductInformer::class);

                    return $this->jsonSuccess(['redir' => \yii\helpers\Url::toRoute(['store/store-unit/index'])]);
                } catch (\Exception $e) {
                    $transaction->rollback();
                    logException($e, 'tsdebug');
                    return $this->jsonError($e->getMessage());
                }
            } else {
                $msg = Html::errorSummary($rejectForm);
                return $this->jsonError($msg);
            }
        }
        return $this->renderAdaptive(
            'reject',
            [
                'model'      => $model,
                'rejectForm' => $rejectForm
            ]
        );
    }

    /**
     * save moderation reject log
     *
     * @param $model3dObj
     * @param \backend\models\store\StoreUnitRejectForm $rejectForm
     * @throws \yii\base\UserException
     */
    private function doReject($model3dObj, \backend\models\store\StoreUnitRejectForm $rejectForm)
    {
        // save log
        $moderLog              = new ModerLog();
        $moderLog->user_id     = 1;
        $moderLog->action      = 'reject';
        $moderLog->result      = $rejectForm->getResult();
        $moderLog->created_at  = dbexpr('NOW()');
        $moderLog->started_at  = dbexpr('NOW()');
        $moderLog->object_id   = $model3dObj->id;
        $moderLog->object_type = 'model3d';
        $moderLog->safeSave();
        /** @var \common\models\User $owner */
        $owner = \common\models\User::findByPk($model3dObj->user_id);
        $data  = [
            'username' => $owner->username,
            'model'    => $model3dObj->title,
            'reason'   => $moderLog->result,
            'title'    => _t('site.email', 'Your publication has been rejected')
        ];
        // send reject message
        try {
            $content = _t(
                "backend.publish",
                "Your model \"{model}\" cannot be published. Reason: {reason}",
                $data
            );

            TopicBuilder::builder()
                ->fromSupport()
                ->to($model3dObj->user)
                ->setTitle(_t('site.catalog', 'Your publication has been rejected'))
                ->setMessage($content)
                ->create();

        } catch (\Exception $e) {
            logException($e, 'tsdebug');
        }
    }

    /**
     * run antivirus for files used for store_unit
     *
     * @param int $id
     * @param bool $moderate
     * @return mixed
     * @throws \yii\base\NotSupportedException
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRunAntivirus($id, $moderate = false)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        /** @var \common\models\StoreUnit $model */
        $model = $this->findModel($id);
        AntiVirusService::startAntiVirusCheck($model->model3d);
        $this->setFlashMsg(true, 'Antivirus started ');
        $url = \yii\helpers\Url::toRoute(['store/store-unit/view', 'id' => $model->id, 'moderate' => $moderate]);
        return $this->redirect($url);
    }


    /**
     * Repeat file job
     *
     * @param int $jobId
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRepeatJob($jobId)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        /** @var FileJob $job */
        $job = FileJob::tryFindByPk($jobId);
        QueueGateway::repeat($job);
        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRepeatAllJobs($model3dId)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        $model3d = Model3d::tryFindByPk($model3dId);
        Model3dFacade::repeatAllJobs($model3d);
        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     *
     * @param int $id
     * @return \yii\web\Response
     */
    public function actionDownloadModel($id)
    {
        AdminAccess::validateAccess('model3d.download');
        $file = File::findByPk($id);
        $path = $file->getLocalTmpFilePath();
        app('response')->setDownloadHeaders(pathinfo($path, PATHINFO_BASENAME), 'application/octet-stream', true);
        return app('response')->sendFile($path);
    }

    /**
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionChange($id)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }

        AdminAccess::validateAccess('storeunit.moderate');

        $storeUnit = StoreUnit::tryFindByPk($id);
        $form      = StoreUnitForm::create($storeUnit);
        $form->load(app('request')->post(), 'StoreUnitForm');
        if ($form->validate()) {
            $form->save();
            $this->setFlashMsg(true, 'Saved');
        } else {
            $this->setFlashMsg(false, Html::errorSummary($form));
        }
        $this->redirect(app('request')->referrer);
    }

    /**
     * @param $id
     */
    public function actionChangeTexture($id)
    {
        /** @var StoreUnit $storeUnit */
        $storeUnit = $this->findModel($id);
        $form      = new ChangeStoreUnitTextureForm($storeUnit);
        AssertHelper::assert($form->load(Yii::$app->request->bodyParams));
        AssertHelper::assertValidate($form);
        $form->process();
        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @param $modelPartId
     * @throws UserException
     */
    public function actionDeletePart($id, $modelPartId)
    {
        $part       = $this->resloveModelPart($id, $modelPartId);
        $model      = $part->model3d;
        $modelParts = $model->getActiveModel3dParts();

        if (count($modelParts) > 1) {
            Model3dService::deleteModel3dPart($part, false);
        } elseif (count($modelParts) == 1 && $part->equals(reset($modelParts))) {
            $model->deleteModel();
        } else {
            throw new UserException("Unknown model state");
        }

        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @param $modelPartId
     */
    public function actionRestorePart($id, $modelPartId)
    {
        $part = $this->resloveModelPart($id, $modelPartId);
        Model3dFacade::restoreModelFile($part);
        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     */
    public function actionRestoreModel($id)
    {
        $storeUnit = $this->findModel($id);

        Model3dFacade::restoreModel($storeUnit);
        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @param $modelPartId
     * @throws UserException
     */
    public function actionChangePartQty($id, $modelPartId)
    {
        $qty       = Yii::$app->request->post('qty');
        $part      = $this->resloveModelPart($id, $modelPartId);
        $part->qty = $qty;

        if ($qty < 1) {
            throw new BusinessException("Qty less 1");
        }

        AssertHelper::assertSave($part);
        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param int $storeUnitId
     * @param int $modelPartId
     * @return Model3dPart
     */
    public function resloveModelPart($storeUnitId, $modelPartId)
    {
        /** @var StoreUnit $storeUnit */
        $storeUnit = $this->findModel($storeUnitId);
        $model     = $storeUnit->model3d;

        /** @var Model3dPart $modelPart */
        $modelPart = ArrayHelper::first(
            $model->model3dParts,
            function (Model3dPart $part) use ($modelPartId) {
                return $part->id == $modelPartId;
            }
        );

        AssertHelper::assert($modelPart);
        return $modelPart;
    }
}
