<?php

namespace backend\controllers\store;

use backend\components\AdminAccess;
use backend\models\search\CuttingPackSearch;
use common\components\FileTypesHelper;
use common\models\CuttingPack;
use common\models\CuttingPackFile;
use common\models\CuttingPackPart;
use Yii;
use yii\web\Response;

/**
 * StoreOrderController implements the CRUD actions for StoreOrder model.
 */
class CuttingPackController extends \backend\components\CrudController
{
    protected $accessGroup = 'store_order';

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');

        $searchModel           = new CuttingPackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function actionDownloadPackFile($uuid)
    {
        $cuttingPackFile = CuttingPackFile::tryFind(['uuid'=>$uuid]);
        $file = $cuttingPackFile->file;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_RAW;
        $path = $file->getLocalTmpFilePath();

        $isInline = false;
        if (FileTypesHelper::isImage($file)) {
            $isInline = true;
        }
        $response->sendFile($path, $file->getFileName(), ['inline' => $isInline]);
    }

    public function actionDownloadPartImage($uuid)
    {
        $cuttingPackPart = CuttingPackPart::tryFind(['uuid'=>$uuid]);
        $rawImage = $cuttingPackPart->getImageContent();
        return \Yii::$app->response->sendContentAsFile($rawImage, $cuttingPackPart->getTitle().'.svg', [
            'mimeType' => $cuttingPackPart->getImageType(),
            'inline' => 1
        ]);
    }

    public function actionView($uuid)
    {
        $cuttingPack = CuttingPack::find()->where(['uuid' => $uuid])->one();
        return $this->render(
            'view',
            [
                'cuttingPack'         => $cuttingPack,
            ]
        );
    }
}