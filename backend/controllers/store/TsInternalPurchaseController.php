<?php

namespace backend\controllers\store;

use backend\components\AdminController;
use backend\models\search\InstantPaymentSearch;
use backend\models\search\TsInternalPurchaseSearch;
use common\models\InstantPayment;
use common\models\TsInternalPurchase;
use common\modules\payment\components\PaymentUrlHelper;
use common\modules\payment\services\InvoiceBankService;
use common\modules\tsInternalDeposit\services\TsInternalDepositService;
use common\modules\tsInternalPurchase\services\TsInternalPurchaseService;
use frontend\modules\tsInternalPurchase\forms\DepositForm;
use Yii;
use yii\base\UserException;
use yii\filters\VerbFilter;

class TsInternalPurchaseController extends AdminController
{
    /**
     * @var TsInternalPurchaseService
     */
    private $tsInternalPurchaseService;

    /**
     * @var TsInternalDepositService
     */
    protected $depositService;

    /** @var InvoiceBankService Bank invoice */
    protected $invoiceBankService;

    /**
     * @param TsInternalPurchaseService $tsInternalPurchaseService
     */
    public function injectDependencies(
        TsInternalPurchaseService $tsInternalPurchaseService,
        TsInternalDepositService $depositService,
        InvoiceBankService $invoiceBankService
    )
    {
        $this->tsInternalPurchaseService = $tsInternalPurchaseService;
        $this->depositService = $depositService;
        $this->invoiceBankService = $invoiceBankService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Instant payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new TsInternalPurchaseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $tsInternalPurchase = TsInternalPurchase::tryFind(['uid' => $id]);
        return $this->render('view', [
            'tsInternalPurchase' => $tsInternalPurchase,
        ]);
    }

    public function actionDeposit($id)
    {
        $tsInternalPurchase = TsInternalPurchase::tryFind(['uid' => $id]);
        return $this->render('deposit', [
            'tsInternalPurchase' => $tsInternalPurchase,
        ]);
    }

    public function actionCreateDeposit()
    {
        $depositForm = Yii::createObject(DepositForm::class);
        if (Yii::$app->request->isPost) {
            $depositForm->load(Yii::$app->request->post());
            $depositForm->validateOrException();
            $depositForm->validateUserOrException();
            $tsInternalPurchase = $this->depositService->createOrder($depositForm);
            $bankInvoice = $this->invoiceBankService->createBankInvoiceByInvoice($tsInternalPurchase->primaryInvoice);
            $this->redirect('deposit?id='.$tsInternalPurchase->uid);
        }
        return $this->render('createDeposit', [
            'depositForm' => $depositForm
        ]);
    }

    public function actionCancel($id)
    {
        $tsInternalPurchase = TsInternalPurchase::tryFind(['uid' => $id]);
        if ($tsInternalPurchase->allowCancel()) {
            $this->tsInternalPurchaseService->cancel($tsInternalPurchase);
        }
        $this->setFlashMsg(true, 'Was canceled');
        return $this->redirect(['view', 'id' =>$tsInternalPurchase->uid]);
    }
}
