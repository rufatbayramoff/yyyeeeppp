<?php

namespace backend\controllers\store;

use backend\components\AdminAccess;
use backend\models\search\PaymentTransactionHistorySearch;
use common\components\exceptions\BusinessException;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionRefund;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\modules\payment\gateways\PaymentGatewayProvider;
use common\modules\payment\gateways\PaymentGatewayResult;
use common\modules\payment\models\RefundRequestForm;
use common\modules\payment\processors\PayPalPayoutProcessor;
use common\modules\payment\services\PaymentAutoUpdateStatusService;
use common\modules\payment\services\PaymentService;
use common\modules\payment\services\PaymentStoreOrderService;
use common\modules\payment\services\RefundService;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\helpers\Html;

/**
 * PaymentTransactionController implements the CRUD actions for PaymentTransaction model.
 */
class PaymentTransactionController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/store/payment-transaction';

    protected $accessGroup = 'payment';

    /**
     * @var RefundService
     */
    protected $refundService;

    /**
     * @var PaymentGatewayProvider
     */
    protected $gatewayProvider;

    /**
     * @var PaymentAutoUpdateStatusService
     */
    protected $paymentAutoUpdateStatusService;

    /**
     * @var PaymentService
     */
    protected $paymentService;

    /**
     * @var PaymentStoreOrderService
     */
    protected $paymentStoreOrderService;

    /**
     * @param RefundService $refundService
     * @param PaymentGatewayProvider $gatewayProvider
     * @param PaymentAutoUpdateStatusService $paymentAutoUpdateStatusService
     * @param PaymentStoreOrderService $paymentStoreOrderService
     * @param PaymentService $paymentService
     */
    public function injectDependencies(
        RefundService $refundService,
        PaymentGatewayProvider $gatewayProvider,
        PaymentAutoUpdateStatusService $paymentAutoUpdateStatusService,
        PaymentStoreOrderService $paymentStoreOrderService,
        PaymentService $paymentService
    )
    {
        $this->refundService                  = $refundService;
        $this->gatewayProvider                = $gatewayProvider;
        $this->paymentAutoUpdateStatusService = $paymentAutoUpdateStatusService;
        $this->paymentStoreOrderService       = $paymentStoreOrderService;
        $this->paymentService                 = $paymentService;
    }

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PaymentTransactionSearch();
        $this->mainModel   = new \common\models\PaymentTransaction();
    }

    public function actionView($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');

        $searchModelHistory = new PaymentTransactionHistorySearch();

        $dataProviderHistory = $searchModelHistory->search(app()->request->get());
        $dataProviderHistory->query->andWhere(['transaction_id' => $id]);

        return $this->render(
            'view',
            [
                'model'               => $this->findModel($id),
                'searchModelHistory'  => $searchModelHistory,
                'dataProviderHistory' => $dataProviderHistory
            ]
        );
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRefresh($id)
    {
        $tr = PaymentTransaction::tryFindByPk($id);

        $this->paymentAutoUpdateStatusService->updateVendorTransaction($tr);

        $this->setFlashMsg(true, "updated " . $tr->transaction_id);
        return $this->redirect(app('request')->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionVoid($id)
    {
        $tr = PaymentTransaction::tryFindByPk($id);

        if ($this->paymentStoreOrderService->cancelPaymentTransaction($tr)) {
            $this->setFlashMsg(true, 'Voided: ' . $tr->transaction_id);
        } else {
            $this->setFlashMsg(false, 'Void failed');
        }
        return $this->redirect(app('request')->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSettle($id)
    {
        $tr        = PaymentTransaction::tryFindByPk($id);
        $operation = $tr->firstPaymentDetail->paymentDetailOperation;

        $this->paymentService->submitForSettlePaymentOperation($operation);
        $this->setFlashMsg(true, 'Settled: ' . $tr->transaction_id);
        return $this->redirect(app('request')->referrer);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRefundDelete($id)
    {
        $refundModel = PaymentTransactionRefund::tryFindByPk($id);
        if ($refundModel->status === PaymentTransactionRefund::STATUS_NEW) {
            $refundModel->delete();
            $this->setFlashMsg(true, 'Refund deleted');
        } else {
            $this->setFlashMsg(false, 'Refund cannot be deleted.');
        }
        return $this->redirect(app('request')->referrer);
    }

    public function actionCancelPayout($id)
    {
        $transaction = PaymentTransaction::findByPk($id);
        $this->paymentService->allowCancelPayout($transaction);
        $paypalPayoutProcessor = Yii::createObject(PayPalPayoutProcessor::class);
        $paypalPayoutProcessor->updateStatus($transaction->id, 0, PaymentTransaction::STATUS_UP_FAILED);
        $this->setFlashMsg(true, 'Was canceled');
        return $this->redirect(app('request')->referrer);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionApproveRefund($id)
    {
        $refundModel = PaymentTransactionRefund::tryFindByPk($id);

        if ($refundModel->status !== PaymentTransactionRefund::STATUS_NEW) {
            $this->setFlashMsg(false, 'Refund cannot be approved.');
            return $this->redirect(app('request')->referrer);
        }

        $dbTransaction = app('db')->beginTransaction();
        try {
            $refundModel->status       = PaymentTransactionRefund::STATUS_IN_PROGRESS;
            $refundModel->isFullRefund = $refundModel->isFullRefund();

            $this->refundService->processRefund($refundModel);

            $this->setFlashMsg(true, 'Refund approved');
            $dbTransaction->commit();
            return $this->redirect(app('request')->referrer);
        } catch (\Braintree\Exception\NotFound $braintree) {
            $this->setFlashMsg(false, 'Refund failed: Braintree transaction not found');
            $dbTransaction->rollBack();
            return $this->redirect(app('request')->referrer);
        } catch (\Exception $e) {
            $this->setFlashMsg(false, 'Refund failed: ' . $e->getMessage());
            $dbTransaction->rollBack();
            return $this->redirect(app('request')->referrer);
        }
    }

    /**
     *
     * refund requested by moderator - so we should settle refund.
     *
     * @param $id
     * @return array|string
     * @throws BusinessException
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRequestRefund($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.update');
        $tr = PaymentTransaction::tryFindByPk($id);

        $refundModel = new PaymentTransactionRefund();
        $refundModel->initWithTransaction($tr);

        if (app('request')->isPost) {
            $refundModel->load(app('request')->post());
            if (!$refundModel->validate()) {
                throw new BusinessException(Html::errorSummary($refundModel));
            }
            $dbTransaction = app('db')->beginTransaction();
            try {
                $refundModel->safeSave();
                $this->refundService->processRefund($refundModel);
                $dbTransaction->commit();
                return $this->jsonMessage('reload');
            } catch (Exception $e) {
                $dbTransaction->rollBack();
                return $this->jsonError($e->getMessage());
            }
        }

        $availableTypes = $refundModel->getAvailableTypes();

        return $this->renderAdaptive(
            'refund',
            [
                'refundModel'    => $refundModel,
                'availableTypes' => $availableTypes
            ]
        );
    }
}
