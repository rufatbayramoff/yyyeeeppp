<?php namespace backend\controllers\store;

use common\models\PaymentBankInvoice;
use common\models\PaymentPayPageLog;
use common\models\PaymentPayPageLogStatus;
use common\models\PaymentTransaction;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyQuoteToOrderInformer;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\services\InvoiceBankService;
use common\services\StoreOrderService;
use frontend\modules\preorder\components\PreorderEmailer;
use Yii;

/**
 * Class PaymentBankInvoiceController
 *
 * @package backend\controllers\store
 *
 * @property InvoiceBankService $invoiceService
 * @property StoreOrderService $orderService
 * @property PreorderEmailer $preorderEmailer
 */
class PaymentBankInvoiceController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/payment/payment-invoice';

    protected $invoiceService;

    protected $orderService;

    protected $preorderEmailer;

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PaymentBankInvoiceSearch();
        $this->mainModel = new \common\models\PaymentBankInvoice();
    }

    public function injectDependencies(
        InvoiceBankService $invoiceService,
        StoreOrderService $orderService,
        PreorderEmailer $preorderEmailer
    )
    {
        $this->invoiceService = $invoiceService;
        $this->orderService = $orderService;
        $this->preorderEmailer = $preorderEmailer;
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionVoid($id)
    {
        $model = $this->findModel($id);
        $this->invoiceService->void($model);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSettle($id)
    {
        $model = $this->findModel($id);
        $amount = Yii::$app->request->post('amount', 0);

        if (!$model->isPaid()) {
            $model->paymentInvoice->setPrimaryInvoiceForRelatedObjects();

            $paymentCheckout = new PaymentCheckout(PaymentTransaction::VENDOR_BANK_TRANSFER);
            $paymentCheckout->pay($model->paymentInvoice, $paymentCheckout->generateClientToken($model->paymentInvoice), $amount);
            $this->invoiceService->settle($model);

            if ($paymentPagePageLog =  PaymentPayPageLog::find()->where(['invoice_uuid'=>$model->paymentInvoice->uuid])->one()) {
                PaymentPayPageLogStatus::logStatus($paymentPagePageLog->uuid, PaymentTransaction::VENDOR_BANK_TRANSFER, PaymentPayPageLog::STATUS_DONE);
            }

            /* Add invoice change current attempt status */
            $storeOrder = $model->paymentInvoice->storeOrder;

            if ($storeOrder && $storeOrder->isForPreorder()) {
                $this->preorderEmailer->psCustomerAcceptOffer($storeOrder->preorder);

                InformerModule::addCompanyOrderInformer($storeOrder->currentAttemp);
            }
        }
        if ($this->isAjax) {
            return $this->jsonReturn(['success' => true, 'message' => 'Settled', 'reload' => true]);
        } else {
            $this->setFlashMsg('true', 'Set payed');
            return "<script>history.back()</script>";
        }
    }

    public function actionExtend($id)
    {
        $model = $this->findModel($id);
        $this->invoiceService->extend($model);
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionPayAmountPopup($id)
    {
        $model = $this->findModel($id);

        return $this->renderAdaptive(
            'payAmountPopup',
            [
                'paymentBankInvoice'    => $model,
            ]
        );
    }

    /**
     * @param int $id
     *
     * @return PaymentBankInvoice
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = PaymentBankInvoice::findOne(['uuid' => $id])) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }
}
