<?php

namespace backend\controllers\store;

use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\StoreOrder;
use common\services\StoreOrderService;
use Yii;
use yii\base\Module;
use yii\base\UserException;

/**
 * StoreNotifyController - notify moderator about problems with orders
 */

class StoreNotifyController  extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/store/order/store-notify';

    /**
     * @var StoreOrderService
     */
    private $orderService;

    public function __construct($id, Module $module, StoreOrderService $orderService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->orderService = $orderService;
    }


    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\StoreOrderSearch();
        $this->mainModel = new \common\models\StoreOrder();
    }

    public function actionIndex()
    {
        /** @var \backend\models\search\StoreOrderSearch $searchModel */
        $searchModel = $this->searchModel;
        $searchModel->order_expire = true;

        return $this->render('index', [
            'searchModel' => $searchModel,
        ]);
    }

    public function actionCancel($id)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }

        $model = $this->findModel($id);

        $cancelForm = new \backend\models\store\StoreOrderCancelModeratorForm();

        $post = app('request')->post();
        if($post) {
            $cancelForm->load($post);
            $order = StoreOrder::findByPk($id);
            $this->orderService->declineOrder($order,  ChangeOrderStatusEvent::INITIATOR_MODERATOR, $cancelForm);
            return $this->jsonSuccess(['message' => 'Order successfully canceled!']);
        }

        return $this->renderAjax('cancel', [
            'model' => $model, 'cancelForm' => $cancelForm
        ]);
    }
}