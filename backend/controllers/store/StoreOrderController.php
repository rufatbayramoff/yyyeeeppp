<?php

namespace backend\controllers\store;

use backend\components\AdminAccess;
use backend\components\ChangePrinterSerializer;
use backend\models\search\StoreOrderSearch;
use backend\models\store\ChangeStoreOrderTextureForm;
use backend\models\store\CreatePaymentForm;
use backend\models\store\StoreDownloadReportForm;
use backend\models\store\StoreOrderCancelModeratorForm;
use backend\modules\statistic\reports\ReportExcelWriter;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\OnlyPostRequestException;
use common\components\order\anonim\OrderMover;
use common\components\order\history\models\ChangeTextureItem;
use common\components\order\history\OrderHistoryService;
use common\components\orderOffer\OrderOfferFilter;
use common\components\serizaliators\porperties\MaterialAndColorsProperties;
use common\components\serizaliators\porperties\ModelReplicaProperties;
use common\components\serizaliators\Serializer;
use common\components\UserSessionOwnersFixer;
use common\interfaces\DeliveryExpensiveInterface;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\factories\FileFactory;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\PaymentInvoice;
use common\models\PsPrinter;
use common\models\repositories\FileRepository;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderAttempDelivery;
use common\models\StoreOrderAttemptModerationFile;
use common\models\StoreOrderPosition;
use common\models\User;
use common\models\ViewOrderlist;
use common\modules\informer\InformerModule;
use common\modules\payment\models\StoreOrderPositionForm;
use common\modules\payment\services\PaymentAutoUpdateStatusService;
use common\modules\payment\services\PaymentStoreOrderService;
use common\modules\payment\services\RefundService;
use common\modules\payment\services\StoreOrderPositionService;
use common\modules\storeOrder\forms\ParcelForm;
use common\modules\storeOrder\services\DummyExpensive;
use common\modules\thingPrint\components\ThingiverseFacade;
use common\modules\thingPrint\models\ThingiverseOrderPayForm;
use common\services\ChangeOfferService;
use common\services\PrintAttemptModeratonService;
use common\services\StoreOrderService;
use frontend\models\order\CancelOrderForm;
use frontend\models\ps\AddResultImageForm;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\models\ps\TrackingNumberForm;
use lib\delivery\delivery\DeliveryService;
use lib\money\Money;
use lib\money\MoneyMath;
use Yii;
use yii\base\DynamicModel;
use yii\base\Module;
use yii\base\UserException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use ZipArchive;

/**
 * StoreOrderController implements the CRUD actions for StoreOrder model.
 */
class StoreOrderController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/store/order/store-order';
    protected $accessGroup = 'store_order';


    public $notTest = true;
    public $onlyTest = false;

    /**
     * @var ChangeOfferService
     */
    private $changeOfferService;

    /**
     * @var StoreOrderService
     */
    private $orderService;

    /**
     * @var PrintAttemptModeratonService
     */
    private $printAttemptModeratonService;

    /**
     * @var PrintAttemptModeratonService
     */
    private $orderHistoryService;

    /**
     * @var StoreOrderPositionService
     */
    private $orderPositionService;

    /**
     * @var RefundService
     */
    private $refundService;

    /**
     * @var PaymentStoreOrderService
     */
    protected $paymentStoreOrderService;

    /**
     * @var Emailer
     */
    private $emailer;

    /** @var OrderOfferFilter */
    protected $orderOfferFilter;

    /**
     * @var PaymentAutoUpdateStatusService
     */
    protected $paymentAutoUpdateStatusService;

    /**
     * StoreOrderController constructor.
     *
     * @param string $id
     * @param Module $module
     * @param ChangeOfferService $changeOfferService
     * @param StoreOrderService $orderService
     * @param PrintAttemptModeratonService $printAttemptModeratonService
     * @param OrderHistoryService $orderHistoryService
     * @param StoreOrderPositionService $positionService
     * @param Emailer $emailer
     * @param RefundService $refundService
     * @param PaymentStoreOrderService $paymentStoreOrderService
     * @param OrderOfferFilter $orderOfferFilter
     * @param PaymentAutoUpdateStatusService $paymentAutoUpdateStatusService
     */
    public function __construct(
        $id,
        Module $module,
        ChangeOfferService $changeOfferService,
        StoreOrderService $orderService,
        PrintAttemptModeratonService $printAttemptModeratonService,
        OrderHistoryService $orderHistoryService,
        StoreOrderPositionService $positionService,
        Emailer $emailer,
        RefundService $refundService,
        PaymentStoreOrderService $paymentStoreOrderService,
        OrderOfferFilter $orderOfferFilter,
        PaymentAutoUpdateStatusService $paymentAutoUpdateStatusService,
        array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->changeOfferService             = $changeOfferService;
        $this->orderService                   = $orderService;
        $this->printAttemptModeratonService   = $printAttemptModeratonService;
        $this->orderHistoryService            = $orderHistoryService;
        $this->orderPositionService           = $positionService;
        $this->refundService                  = $refundService;
        $this->emailer                        = $emailer;
        $this->paymentStoreOrderService       = $paymentStoreOrderService;
        $this->orderOfferFilter               = $orderOfferFilter;
        $this->paymentAutoUpdateStatusService = $paymentAutoUpdateStatusService;
    }

    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\StoreOrderSearch();
        $this->mainModel   = new  StoreOrder();
    }

    public function beforeAction($action)
    {
        if ($action->id === 'send-print-requests') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }


    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
        /** @var StoreOrderSearch $searchModel */
        $searchModel           = $this->searchModel;
        $searchModel->notTest  = $this->notTest;
        $searchModel->onlyTest = $this->onlyTest;
        $dataProvider          = $searchModel->search(Yii::$app->request->queryParams);

        AdminAccess::validateAccess($this->accessGroup . '.view');

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function actionChangeCarrier()
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $storeOrder = StoreOrder::tryFindByPk(Yii::$app->request->post('orderId'));
        $newType    = Yii::$app->request->post('deliveryType');
        $newStatus  = Yii::$app->request->post('carrierBy');
        $this->orderService->changeCarrier($storeOrder, $newType, $newStatus);
        $this->setFlashMsg(true, 'Carrier was changed');
        return $this->redirect('/store/store-order/view?id=' . $storeOrder->id);
    }

    public function actionAddPosition()
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $form = new StoreOrderPositionForm();
        $form->load(app('request')->post());
        if (!$form->validate()) {
            $this->setFlashMsg(false, Html::errorSummary($form));
            return $this->redirect(app('request')->referrer);
        }

        $orderPosition = $form->createPosition();
        $this->orderPositionService->addOrderPosition($orderPosition, false);
        $this->setFlashMsg(true, 'Position added');
        return $this->redirect(app('request')->referrer);
    }

    public function actionDeletePosition($id)
    {
        $storeOrderPosition = StoreOrderPosition::tryFindByPk($id);

        $storeOrderPosition->status = StoreOrderPosition::STATUS_CANCELED;
        $storeOrderPosition->safeSave(['status']);

        if ($storeOrderPosition->primaryPaymentInvoice) {
            $storeOrderPosition->primaryPaymentInvoice->status = PaymentInvoice::STATUS_CANCELED;
            $storeOrderPosition->primaryPaymentInvoice->safeSave(['status']);
        }

        $this->setFlashMsg(true, 'Position deleted');
        return $this->redirect(app('request')->referrer);
    }

    public function actionView($id)
    {
        /**
         * @var $model StoreOrder
         */
        $model = $this->findModel($id);

        if (app('request')->get('thingiverseOrderFetch')) {
            try {
                $thingiverseOrder = ThingiverseFacade::refreshOrderByApi($model);
                if ($thingiverseOrder->status == 'Disputed' && !$model->is_dispute_open) {
                    $this->orderService->switchDispute($model);
                }
            } catch (\Exception $e) {
                $this->setFlashMsg(false, $e->getMessage());
            }
            return $this->redirect(app('request')->referrer);
        }
        if (app('request')->get('thingiverseOrderUpdate')) {
            try {
                ThingiverseFacade::updateOrder($model, null, true, 'Other reason', 'Model is unprintable');
            } catch (\Exception $e) {
                $this->setFlashMsg(false, $e->getMessage());
            }
            return $this->redirect(app('request')->referrer);
        }
        $paramsGet = app('request')->queryParams;

        // moder log
        $params          = \yii\helpers\ArrayHelper::merge(
            $paramsGet,
            ['ModerLogSearch' => ['object_id' => $id, 'object_type' => 'store_order']]
        );
        $searchLogModel  = new \backend\models\search\ModerLogSearch();
        $logDataProvider = $searchLogModel->search($params);

        // history
        $historyModel    = new \backend\models\search\StoreOrderHistorySearch();
        $historyProvider = $historyModel->search(
            \yii\helpers\ArrayHelper::merge(
                isset($paramsGet['StoreOrderHistorySearch']) ? $paramsGet['StoreOrderHistorySearch'] : [],
                ['StoreOrderHistorySearch' => ['order_id' => $id]]
            )
        );

        $customerLogs = \common\models\UserLoginLog::find()
            ->select(['remote_addr'])
            ->asArray()
            ->where(['user_id' => $model->user_id])
            ->orderBy('created_at desc')
            ->limit(1)
            ->one();
        $userPsIp     = ' - ';
        $customerIp   = ' - ';
        if (!empty($customerLogs)) {
            $customerIp = $customerLogs['remote_addr'];
        }

        if ($model->currentAttemp) {
            $userPsIpLogs = \common\models\UserLoginLog::find()
                ->select(['remote_addr'])
                ->asArray()
                ->where(['user_id' => $model->currentAttemp->ps->user_id])
                ->orderBy('created_at DESC')
                ->limit(1)
                ->one();
        } else {
            $userPsIpLogs = [];
        }

        $psEarning = $model->currentAttemp ? $this->paymentStoreOrderService->getProductionInvoicePsProfit($model->currentAttemp) : null;

        if (!empty($userPsIpLogs)) {
            $userPsIp = $userPsIpLogs['remote_addr'];
        }
        return $this->render(
            'view/layout',
            [
                'refundService' => $this->refundService,
                'model'         => $model,
                'psEarning'     => $psEarning,
                'customerIp'    => $customerIp,
                'userPsIp'      => $userPsIp,
                'log'           => [
                    'searchModel'  => $searchLogModel,
                    'dataProvider' => $logDataProvider
                ],
                'history'       => [
                    'searchModel'  => $historyModel,
                    'dataProvider' => $historyProvider
                ]
            ]
        );
    }

    public function actionManualCancelInvoice($uuid)
    {
        $paymentInvoice = PaymentInvoice::find()->where(['uuid' => $uuid])->one();
        if (!$paymentInvoice || !$paymentInvoice->allowManualCancel()) {
            throw new UserException('Not allowed manual cancel');
        }
        $paymentInvoice->status = PaymentInvoice::STATUS_CANCELED;
        $paymentInvoice->safeSave();
        $this->orderHistoryService->logManualCancel($paymentInvoice);
        $this->setFlashMsg(true, 'Was canceled');
        return $this->redirect('/store/store-order/view?id=' . $paymentInvoice->store_order_id);
    }

    /**
     * @param $id
     * @throws UserException
     */
    public function actionBindAnonymousOrder($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $storeOrder = StoreOrder::tryFindByPk($id);
        if ($storeOrder->user_id !== User::USER_ANONIM) {
            throw new BusinessException('Alreasdy binded with user.');
        }
        $user                = $storeOrder->getPossibleUser();
        $storeOrder->user_id = $user->id;
        $storeOrder->safeSave();
        UserSessionOwnersFixer::fixModelsOwnersForUser($storeOrder->userSession, $user);
        $this->setFlashMsg(true, 'Was binded with user.');
        return $this->redirect(Url::toRoute(['store/store-order/view', 'id' => $storeOrder->id]));
    }

    public function actionCancel($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');

        $order = StoreOrder::tryFindByPk($id);
        $this->view->title = 'Cancel order #' . $id;

        $this->paymentAutoUpdateStatusService->updateTransactions($order);
        $orderCancelForm = new StoreOrderCancelModeratorForm();

        if (app('request')->isPost) {
            $orderCancelForm->load(app('request')->post());
            try {
                if ($orderCancelForm->validate()) {
                    $order  = StoreOrder::findOne(['id' => $id]);
                    $result = $this->orderService->declineOrder($order, ChangeOrderStatusEvent::INITIATOR_MODERATOR, $orderCancelForm);
                    app('cache')->set('back.notify.data', '');
                    if ($result === StoreOrderService::DECLINE_SUCCESSFULL) {
                        return $this->jsonSuccess([
                            'message' => _t('site.store', 'Order successfully cancelled!'),
                            'reload'  => 1
                        ]);
                    } elseif ($result === StoreOrderService::DECLINE_WITHOUT_PAYMENT) {
                        return $this->jsonReturn(
                            [
                                'success' => true,
                                'warning' => _t('site.store', 'Order is canceled. Payment not refunded!'),
                                'reload'  => 1,
                            ]);
                    } else {
                        return $this->jsonReturn(
                            [
                                'success' => true,
                                'warning' => _t('site.store', 'Order is canceled. Something wrong:') . $result,
                                'reload'  => 1
                            ]);
                    }
                } else {
                    $msg = \yii\helpers\Html::errorSummary($orderCancelForm);
                }
            } catch (\yii\base\UserException $e) {
                $msg = $e->getMessage();
            }
            return $this->jsonFailure(['message' => $msg, 'reload' => 1]);
        }

        return $this->renderAdaptive('cancelOrder', ['orderId' => $id, 'cancelForm' => $orderCancelForm]);
    }

    public function actionChangePrinter($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $order   = StoreOrder::tryFindByPk($id);
        $invoice = $order->getPrimaryInvoice();

        $totalPrice    = $printPrice = $invoice->storeOrderAmount->getAmountPrintForPsWithRefund();
        $shippingPrice = $invoice->storeOrderAmount->getAmountShippingWithRefund();
        if ($shippingPrice) {
            $totalPrice = MoneyMath::sum($printPrice, $shippingPrice);
        }
        $packagePrice = $invoice->storeOrderAmount->getAmountPackageWithRefund();
        if ($packagePrice) {
            $totalPrice = MoneyMath::sum($totalPrice, $packagePrice);
        }
        $treatstockFee = $invoice->getAmountTreatstockFeeWithRefund();

        $this->layout = 'plain.php';
        return $this->render('changePrinter', [
            'storeOrder'    => $order,
            'totalPrice'    => $totalPrice,
            'producePrice'  => MoneyMath::sum($printPrice, $packagePrice),
            'shippingPrice' => $shippingPrice ? $shippingPrice : Money::zero(),
            'treatstockFee' => $treatstockFee,
        ]);
    }

    /**
     * @param $orderId
     * @param bool $exactMaterialAndColor
     * @param bool $hideCancelled
     * @return array
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \common\components\exceptions\IllegalStateException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionChangePrinterAjax($orderId, bool $exactMaterialAndColor = false, bool $hideCancelled = false, $sameCurrency = true)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $order           = StoreOrder::tryFindByPk($orderId);
        $offers          = $this->changeOfferService->findOffersForStoreOrder($order, $exactMaterialAndColor, $hideCancelled, $sameCurrency);
        $formattedOffers = $this->changeOfferService->getFormatedOffers($order, $offers);
        $formattedOffers = $this->orderOfferFilter->filterCanceled($formattedOffers, $hideCancelled);

        $response   = [
            'offers'   => $formattedOffers,
            'warnings' => $this->changeOfferService->getWarnings($order)
        ];
        $serializer = new Serializer(new ChangePrinterSerializer($order));
        $response   = $serializer->serialize($response);
        return $this->jsonReturn($response);
    }

    public function actionCalcDeliveryTbcAjax($orderId, $printerId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $order         = StoreOrder::tryFindByPk($orderId);
        $printer       = PsPrinter::tryFindByPk($printerId);
        $data          = $this->changeOfferService->getDeliveryPrice($order, $printer);
        $shippingPrice = $data['shipping'];
        return $this->jsonReturn([
            'shippingPrice' => $shippingPrice,
        ]);
    }

    public function actionDisputeSwitch($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $order = StoreOrder::tryFindByPk($id);
        $this->orderService->switchDispute($order);
        $this->setFlashMsg(true, 'Dispute is ' . ($order->is_dispute_open ? 'open' : 'closed'));
        return $this->redirectToView($order);
    }


    /**
     * send print request (we just add records to database)
     *
     * @return Response
     * @internal param $orderId
     */
    public function actionSendPrintRequests()
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $orderId       = app('request')->post('orderId');
        $printerIds    = app('request')->post('printerIds');
        $totalPrinters = count($printerIds);
        $order         = StoreOrder::tryFindByPk($orderId);

        if ($totalPrinters > 0) {
            /** @var PsPrinter[] $printers */
            $printers = PsPrinter::findAll(['id' => $printerIds]);
            $this->changeOfferService->makeOffers($order, $printers);
        }
        Yii::$app->getSession()->setFlash('success', sprintf('For order: %d new printer request sent, total printers: %d', $orderId, $totalPrinters), true);
        return $this->jsonSuccess();
    }


    /**
     * renders reject form and handles form submit to reject printed
     *
     * @param $orderId
     * @return array|string
     * @throws UserException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionPrintReviewReject($orderId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $model = new DynamicModel(['reason']);

        $model->addRule(['reason'], 'string', ['max' => 1024])
            ->addRule('reason', 'required');

        $order = StoreOrder::tryFindByPk($orderId);
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if (!$model->validate()) {
                    throw new BusinessException(Html::errorSummary($model));
                }
                $rejectReason = $model->reason;

                $this->emailer->sendPsPrintRejectOrder($order->currentAttemp, $model);

                $this->orderHistoryService->logModeratorRejectPrinting($order->currentAttemp, $rejectReason);
                // return to printing
                $this->orderService->printingAttemp($order->currentAttemp);
                $order->currentAttemp->moderation->rejectModeration($rejectReason);
                InformerModule::addCompanyOrderInformer($order->currentAttemp);
                $order->clearOrderLike();
                $transaction->commit();
                $msgResult = sprintf('Order #%d print result rejected.', $orderId);
                $success   = true;
            } catch (\Exception $e) {
                logException($e, 'tsdebug');
                $transaction->rollBack();
                $msgResult = $e->getMessage();
                $success   = false;
            }
            $this->setFlashMsg(true, $msgResult);
            $url = Url::toRoute(['store/store-order/view', 'id' => $orderId]);
            InformerModule::addCompanyOrderInformer($order->currentAttemp);
            InformerModule::addCustomerOrderInformer($order);
            return $this->jsonReturn(['message' => $msgResult, 'redir' => $url, 'success' => $success]);
        }
        return $this->renderAdaptive('printReviewReject', ['orderId' => $orderId, 'model' => $model]);
    }

    /**
     *
     * @param $orderId
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPrintReviewOk($orderId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $order         = StoreOrder::tryFindByPk($orderId);
        $dbTransaction = \Yii::$app->db->beginTransaction();
        try {
            $order->currentAttemp->moderation->acceptModeration();

            if ($order->getIsTestOrder()) {
                $this->orderService->receivedAttemp($order->currentAttemp, ChangeOrderStatusEvent::INITIATOR_MODERATOR);

                $printer                                   = $order->currentAttemp->machine->asPrinter();
                $printer->is_test_order_resolved           = 1;
                $printer->companyService->moderator_status = CompanyService::MODERATOR_STATUS_PENDING;
                AssertHelper::assertSave($printer);
                AssertHelper::assertSave($printer->companyService);

            } else {
                if ($order->hasDelivery()) {
                    $this->orderService->readyToSendAttemp($order->currentAttemp);
                } else {
                    $this->orderService->readyToSendAttemp($order->currentAttemp);
                }
            }
            // mark as completed
            if ($order->isThingiverseOrder()) {
                ThingiverseFacade::updateOrder($order, null, null, null, 'Completed');
            }

            $dbTransaction->commit();
            InformerModule::addCompanyOrderInformer($order->currentAttemp);
            InformerModule::addCustomerOrderInformer($order);
        } catch (\Exception $e) {
            $dbTransaction->rollBack();
            logException($e, 'tsdebug');
            $this->setFlashMsg(false, $e->getMessage());
        }
        return $this->redirectToView($order);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionCreatePayment($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        /** @var StoreOrder $order */
        $order = $this->findModel($id);
        $order->payment->id;

        $form = new CreatePaymentForm($order);

        if (Yii::$app->request->isPost) {

            AssertHelper::assert($form->load(Yii::$app->request->getBodyParams()));

            if ($form->validate()) {
                $form->process();
                return $this->redirect(['view', 'id' => $order->id]);
            }
        }

        return $this->render('create-payment', ['form' => $form]);
    }

    /**
     * @param $query
     * @return array
     */
    public function actionUserList($query)
    {
        $users = User::find()->likeUsername($query)
            ->select(['id', dbexpr('CONCAT(username, \' [\', id, \']\') text')])
            ->asArray()
            ->all();

        return $this->jsonReturn(
            [
                'results' => $users
            ]
        );
    }


    /**
     * @param $attemptId
     * @return Response
     */
    public function actionSetAsDelivered($attemptId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $attemp = StoreOrderAttemp::findByPk($attemptId);
        $this->orderService->deliveredAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_MODERATOR);
        return $this->redirectToView($attemp->order);
    }


    /**
     * @param $attemptId
     * @return Response
     */
    public function actionSetAsReceived($attemptId)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $attemp = StoreOrderAttemp::findByPk($attemptId);
        $this->orderService->receivedAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_MODERATOR);
        $transaction->commit();
        return $this->redirectToView($attemp->order);
    }

    /**
     * @param $attempId
     * @return Response
     */
    public function actionCancelAttempt($attempId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $attemp = StoreOrderAttemp::findByPk($attempId);
        $this->orderService->declineAttemp(
            $attemp,
            ChangeOrderStatusEvent::INITIATOR_MODERATOR,
            new CancelOrderForm(['reasonDescription' => _t('store.order', 'Decline by moderator')])
        );
        return $this->redirectToView($attemp->order);
    }

    public function actionReofferAttempt($attempId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $attemp               = StoreOrderAttemp::findByPk($attempId);
        $attemp->status       = StoreOrderAttemp::STATUS_NEW;
        $attemp->cancelled_at = dbexpr('NOW()'); // we reset cancelled at in order CRON job to check correctly
        $attemp->safeSave();
        $this->orderHistoryService->log($attemp->order, 'reoffer', ['attemp_id' => $attempId], 'Re-offered by moderator');
        StoreOrder::updateRow(['id' => $attemp->order_id], ['current_attemp_id' => $attempId]);
        return $this->redirectToView($attemp->order);
    }

    /**
     * @return string|\yii\console\Response|Response
     * @throws UserException
     * @throws \PHPExcel_Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\RangeNotSatisfiableHttpException
     * @throws \Exception
     */
    public function actionDownloadReport()
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $form = new StoreDownloadReportForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $report = new ViewOrderlist();
            $report->setParams(['from_date' => $form->from_date, 'to_date' => $form->getToDate()]);
            $reportWriter = new ReportExcelWriter('OrdersList');
            \Yii::$app->getDb()->createCommand('SET SESSION SQL_BIG_SELECTS=1')->execute();
            return \Yii::$app->response->sendContentAsFile($reportWriter->write($report), 'orders_list.xlsx', ['inline' => true]);
        }
        return $this->render('download-report', ['model' => $form]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionChangeClient($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $order = StoreOrder::tryFind($id);
        AssertHelper::assert($order->isAnonim());

        $clientId = \Yii::$app->request->post('clientId');
        AssertHelper::assert($clientId);
        $client = User::tryFind($clientId);

        $orderMover = new OrderMover();
        $orderMover->move($order, $order->user, $client);

        return $this->redirectToView($order);
    }

    /**
     * @param $attemptId
     * @return \yii\web\Response
     */
    public function actionSetAsPrinted($attemptId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $attempt = StoreOrderAttemp::tryFind($attemptId);
        $this->orderService->printedAttemp($attempt, ChangeOrderStatusEvent::INITIATOR_MODERATOR);
        return $this->redirectToView($attempt->order);
    }

    /**
     * @param $attemptId
     * @return string
     */
    public function actionTrackingNumberForm($attemptId)
    {
        $attempt = StoreOrderAttemp::tryFind($attemptId);
        $form    = new TrackingNumberForm();
        return $this->renderAdaptive('view/_trackingNumberForm', ['form' => $form, 'attempt' => $attempt]);
    }

    public function actionSent($id, DeliveryService $deliveryService)
    {
        $order = StoreOrder::tryFind($id);
        $form  = new ParcelForm($order->storeOrderDelivery);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $rate           = $deliveryService->getCarrierRateForOrderAttemp($order->currentAttemp, $form->parcel());
            $form->rate     = $rate->rate;
            $form->currency = $rate->currency;
        }
        return $this->render('sent',
            [
                'model' => $form,
                'order' => $order
            ]
        );
    }

    /**
     * @param $attemptId
     * @return Response
     */
    public function actionSetAsSent($attemptId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $attempt      = StoreOrderAttemp::tryFind($attemptId);
        $printRequest = StoreOrderAttemptProcess::create($attempt);
        AssertHelper::assert($printRequest->canSetAsSent());

        if ($attempt->order->deliveryType && $attempt->order->deliveryType->code != DeliveryType::PICKUP) {
            if ($printRequest->canSetAsSentWithTrackingNumber()) {

                $form = new TrackingNumberForm();
                $form->load(Yii::$app->request->post());
                AssertHelper::assertValidate($form);
                $attempt->delivery->setTrackingNumber($form->trackingNumber, $form->shipper);
            } else {
                try {
                    $form = new ParcelForm();
                    if ($form->load(Yii::$app->request->post()) && $form->validate()) {
                        Yii::$container->set(DeliveryExpensiveInterface::class, DummyExpensive::class);
                        $this->orderService->buyDeliveryAndLoadPostalImage($attempt, $form->parcel());
                    }
                } catch (\EasyPost\Error $error) {
                    $this->setFlashMsg(false, $error->getMessage());
                    \Yii::error($error, 'easypost');
                }
            }
        }

        $this->orderService->sendAttemp($attempt, ChangeOrderStatusEvent::INITIATOR_PS);
        return $this->redirectToView($attempt->order);
    }

    /**
     * @param $attemptId
     * @return string|Response
     */
    public function actionPrintAttemptResult($attemptId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $attempt = StoreOrderAttemp::tryFind($attemptId);
        //  AssertHelper::assert(PrintRequest::create($attempt)->canPrintOrder(), _t('front.site', 'You can\'t upload photo for this order.'), UserException::class);

        $model = new AddResultImageForm();

        if (Yii::$app->request->isPost) {
            AssertHelper::assert($model->load(Yii::$app->request->bodyParams));
            AssertHelper::assertValidate($model);
            $attempt->moderation->clearPreviousResult();
            $this->printAttemptModeratonService->addResult($attempt, $model);

            return $this->redirectToView($attempt->order);
        }
        return $this->renderAdaptive('view/_printAttemptResult', ['model' => $model, 'attempt' => $attempt]);
    }


    /**
     * @param $orderId
     * @return array|null
     */
    public function actionChangeTexture($orderId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');
        $order = StoreOrder::tryFind($orderId);
        $form  = new ChangeStoreOrderTextureForm($order->getFirstReplicaItem());

        if (Yii::$app->request->isPost) {
            $oldTexture = new ChangeTextureItem(clone $order);
            AssertHelper::assert($form->load(Yii::$app->request->getBodyParams(), ''));
            AssertHelper::assert($form->validate());
            $form->process();
            $this->orderHistoryService->logChangeTextures($order, $oldTexture);
        }
        $materials = MaterialAndColorsProperties::serialize($form->getAllowedMaterialsAndColors());
        return $this->jsonReturn(
            [
                'form'      => $form,
                'materials' => $materials,
                'replica'   => ModelReplicaProperties::serialize($order->orderItem->model3dReplica)
            ]
        );
    }

    public function actionSaveComment()
    {
        $orderId    = app('request')->post('order_id');
        $storeOrder = StoreOrder::tryFindByPk($orderId);
        $this->orderService->saveComment($storeOrder, app('request')->post('comment'));
        return $this->redirectToView($storeOrder);
    }

    public function actionSaveUserComment()
    {
        $orderId             = app('request')->post('order_id');
        $storeOrder          = StoreOrder::tryFindByPk($orderId);
        $storeOrder->comment = app('request')->post('comment');
        $storeOrder->safeSave();
        $this->orderHistoryService->log($storeOrder, 'change_user_comment', [], 'User comment updated by moderator');
        return $this->redirectToView($storeOrder);
    }

    /**
     * @param $id
     * @return Response
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDeletePrintedImage($id)
    {
        $m                   = StoreOrderAttemptModerationFile::findByPk($id);
        $orderAttempt        = StoreOrderAttemp::tryFindByPk($m->attempt_id);
        $data                = [
            'file_id' => $m->file_id,
        ];
        $m->file->deleted_at = dbexpr('NOW()');
        $m->file->safeSave();
        $m->delete();

        $order = $orderAttempt->order;
        $this->orderHistoryService->log($order, 'upload_printed_file', $data, 'Printed file deleted by moderator');
        $this->setFlashMsg(true, 'Image deleted');
        return $this->redirect(app('request')->referrer);
    }

    public function actionSetMainReviewImage($id)
    {
        $m            = StoreOrderAttemptModerationFile::tryFindByPk($id);
        $orderAttempt = StoreOrderAttemp::tryFindByPk($m->attempt_id);

        StoreOrderAttemptModerationFile::updateAll(['is_main_review_file' => 0], ['attempt_id' => $m->attempt_id]);

        $data = [
            'file_id' => $m->file_id,
        ];

        $m->is_main_review_file = 1;
        $m->safeSave();

        $order = $orderAttempt->order;
        $this->orderHistoryService->log($order, 'set_main_review_image', $data, 'Installed as the main image for a review letter.');
        $this->setFlashMsg(true, 'Image set');
        return $this->redirect(app('request')->referrer);
    }

    /**
     * @return Response
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    public function actionUploadPrintedImages()
    {
        $moreImages     = UploadedFile::getInstancesByName('DynamicModel[moreImages]');
        $fileRepository = Yii::createObject(FileRepository::class);
        $fileFactory    = Yii::createObject(FileFactory::class);
        $attemptId      = app('request')->get('attempt_id');
        if (!empty($moreImages)) {
            $photoFiles   = $fileFactory->createFilesFromUploadedFiles($moreImages);
            $orderAttempt = StoreOrderAttemp::tryFindByPk($attemptId);
            $order        = $orderAttempt->order;
            $mIds         = $fileIds = [];
            foreach ($photoFiles as $file) {
                $file->setPublicMode(true);
                $file->setOwner(StoreOrderAttemptModerationFile::class, 'file_id');
                $fileRepository->save($file);

                $moderationFile             = new StoreOrderAttemptModerationFile();
                $moderationFile->attempt_id = $attemptId;
                $moderationFile->file_id    = $file->id;
                $moderationFile->safeSave();
                $mIds[]    = $moderationFile->id;
                $fileIds[] = $file->id;
            }

            $this->orderHistoryService->log($order, 'upload_printed_file', ['file_id' => join(",", $fileIds), 'm_id' => join(',', $mIds)],
                'Printed files upload by moderator');
        }
        return $this->redirect(app('request')->referrer);
    }

    /**
     * @param StoreOrder $order
     * @return \yii\web\Response
     */
    private function redirectToView(StoreOrder $order)
    {
        return $this->redirect(['/store/store-order/view', 'id' => $order->id]);
    }

    /**
     * @param $attemptId
     * @return $this
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionModerationDownloadAll($attemptId)
    {
        AdminAccess::validateAccess($this->accessGroup . '.moderate');

        /**
         * @var $attempt StoreOrderAttemp
         */
        $attempt = StoreOrderAttemp::tryFindByPk($attemptId);
        $zip     = new ZipArchive();
        $key     = [];
        foreach ($attempt->moderation->files as $moderationFile) {
            $key[] = $moderationFile->file->md5sum;
        }
        $key  = md5(implode('.', $key));
        $path = sprintf('%s/%s_%s_%s.zip', Yii::getAlias('@runtime'), $attempt->order_id, $attemptId, $key);

        if (!file_exists($path)) {
            $zip->open($path, ZIPARCHIVE::CREATE);
            foreach ($attempt->moderation->files as $moderationFile) {
                $zip->addFile($moderationFile->file->getLocalTmpFilePath(), $moderationFile->file->name);
            }
            $zip->close();
        }
        app('response')->setDownloadHeaders(pathinfo($path, PATHINFO_BASENAME), 'application/octet-stream', true);
        return app('response')->sendFile($path);
    }


    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionTracknumberChange($id)
    {
        /** @var StoreOrderAttempDelivery $model */
        $model   = $this->findStoreOrderAttempDelivery($id);
        $request = app()->request;

        if ($request->isPost) {
            $post = $request->post()[$model->formName()];

            $attemp = $model->orderAttemp;

            $this->orderService->logChangeTracking(
                $attemp,
                $attemp->delivery->tracking_shipper,
                $attemp->delivery->tracking_number,
                $post['tracking_shipper'],
                $post['tracking_number']
            );

            $attemp->delivery->setTrackingNumber($post['tracking_number'], $post['tracking_shipper']);

            if (!$attemp->isResolved()) {
                $this->orderService->sendAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_MODERATOR);
            }

            return $this->redirect(['view', 'id' => $attemp->order_id]);
        }

        return $this->render('view/tracknumberChange', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionFixPayed($id)
    {
        $storeOrder = StoreOrder::tryFindByPk($id);
        if (!$storeOrder->thingiverseOrder) {
            throw new BusinessException('No thingiverse order exists');
        }
        $orderForm = \Yii::createObject(ThingiverseOrderPayForm::class);
        $orderForm->load([
            'orderId'            => $storeOrder->id,
            'thingId'            => $storeOrder->thingiverseOrder->thing_id,
            'thingiverseOrderId' => $storeOrder->thingiverseOrder->thingiverse_order_id,
        ]);
        $orderForm->updateAndCheckThingiverseOrderStatus();

        if ($storeOrder->thingiverseOrder->is_cancelled) {
            throw new BusinessException('Thingivers order is canceled');
        }
        if ($storeOrder->getPrimaryInvoice()->getActivePayment() && $storeOrder->getPrimaryInvoice()->getActivePayment()->getPaymentDetails()) {
            throw new BusinessException('Already payed');
        }
        $orderForm->payOrder();
        $this->setFlashMsg(true, 'Thingiverse order is payed now');
        return $this->redirect('/store/store-order/view?id=' . $storeOrder->id);
    }

    /**
     * @param $id
     *
     * @return StoreOrderAttempDelivery|null
     * @throws NotFoundHttpException
     */
    protected function findStoreOrderAttempDelivery($id)
    {
        if (($model = StoreOrderAttempDelivery::findOne(['order_attemp_id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPartsList($orderId)
    {
        $order        = StoreOrder::findOne($orderId);
        $this->layout = '@backend/views/yii2-app/layouts/plain.php';
        return $this->render('@frontend/modules/workbench/views/service-orders/report-part-list.php', ['order' => $order, 'attemp' => $order->currentAttemp]);
    }
}