<?php
/**
 * Created by mitaichik
 */

namespace backend\controllers\store;


use backend\components\AdminAccess;
use common\models\StoreOrderReview;
use common\modules\informer\InformerModule;
use common\modules\informer\models\ReviewInformer;
use Yii;
use yii\base\UserException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;

class ReviewsController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/store/reviews';
    public $accessGroup = 'store_order_reviews';

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\StoreOrderReviewSearch();
        $this->mainModel = new \common\models\StoreOrderReview();
    }

    public function formModelLoadData($model)
    {
        $loadData = parent::formModelLoadData($model);
        $loadData['files'] = UploadedFile::getInstances($model, 'files');
        return $loadData;
    }

    /**
     * @param $id
     *
     * @throws UserException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionApprove($id)
    {
        /** @var \common\models\StoreOrderReview $model */
        $model = $this->findModel($id);
        \Yii::$app->reviewService->approveOrderReview($model);

        if($model->ps){
            \Yii::$app->cache->delete('psreviewcount_'.$model->ps->id . $model->ps->updated_at);
            \Yii::$app->cache->delete('psrate_'.$model->ps->id . $model->ps->updated_at);

        }
        InformerModule::addInformer($model->ps->user, ReviewInformer::class);
        \Yii::$app->cache->delete('back.notify.data');
        $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @param $id
     *
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReject($id)
    {
        /** @var \common\models\StoreOrderReview $model */
        $model = $this->findModel($id);
        \Yii::$app->reviewService->rejectOrderReview($model);
        \Yii::$app->cache->delete('back.notify.data');
        $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @param $id
     *
     * @return array|string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionAnswerReject($id)
    {
        /** @var \common\models\StoreOrderReview $model */
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost){
            $model->setScenario(\common\models\StoreOrderReview::SCENARIO_ANSWER_MODERATION);
            $url = Url::toRoute(['/store/reviews/update', 'id' => $model->id]);

            if ($model->load(Yii::$app->request->post())) {
                $model->verified_answer_status = \common\models\StoreOrderReview::ANSWER_STATUS_REJECTED;

                if ($model->save()) {
                    return $this->jsonReturn(['message' => 'Answer rejected', 'redir' => $url, 'success' => true]);
                }

                return $this->jsonReturn(['message' => $model->errors, 'redir' => $url, 'success' => true]);
            }
        }


        return $this->renderAdaptive('partial/answerReviewReject', ['model' => $model]);
    }

    /**
     * @param $id
     *
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionAnswerApprove($id)
    {
        /** @var \common\models\StoreOrderReview $model */
        $model = $this->findModel($id);
        Yii::$app->reviewService->approveAnswerReview($model);
        $this->redirect(Yii::$app->request->referrer);
    }

    /***
     * @param int $id
     *
     * @return mixed|string
     * @throws UserException
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.update');

        /** @var StoreOrderReview $model */
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            Yii::$app->reviewService->saveOrderReviewAdmin($model, Yii::$app->request->post());
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}