<?php

namespace backend\controllers\store;

use backend\components\AdminController;
use backend\models\search\InstantPaymentSearch;
use common\models\InstantPayment;
use common\modules\instantPayment\services\InstantPaymentService;
use Yii;
use yii\filters\VerbFilter;

/**
 * InstantPaymentController implements the CRUD actions for Instant paymnet model.
 */
class InstantPaymentController extends AdminController
{
    /**
     * @var InstantPaymentService
     */
    private $instantPaymentService;

    /**
     * @param InstantPaymentService $instantPaymentService
     */
    public function injectDependencies(
        InstantPaymentService $instantPaymentService
    )
    {
        $this->instantPaymentService = $instantPaymentService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Instant payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new InstantPaymentSearch();
        $searchModel->filterStatus = InstantPaymentSearch::FILTER_BY_ALL_PAYED;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $instantPayment = InstantPayment::tryFind(['uuid' => $id]);
        return $this->render('view', [
            'instantPayment' => $instantPayment,
        ]);
    }

    public function actionRefund($id)
    {
        $instantPayment = InstantPayment::tryFind(['uuid' => $id]);
        if ($instantPayment->allowCancel()) {
            $this->instantPaymentService->cancelPayment($instantPayment);
        }
        $this->setFlashMsg(true, 'Was canceled');
        return $this->redirect(['update', 'id' => $instantPayment->uuid]);
    }
}
