<?php

namespace backend\controllers\store;

use Yii;
use common\models\PaymentDetail;
use backend\models\search\PaymentDetailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentDetailController implements the CRUD actions for PaymentDetail model.
 */
class PaymentDetailController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/payment/payment-detail';
    protected $accessGroup = 'payment'; 
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PaymentDetailSearch();
        $this->mainModel = new \common\models\PaymentDetail();
    }
}
