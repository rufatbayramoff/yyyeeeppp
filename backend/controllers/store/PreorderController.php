<?php

namespace backend\controllers\store;

use backend\components\AdminController;
use backend\models\search\PreorderSearch;
use common\models\Preorder;
use frontend\modules\preorder\components\PreorderEmailer;
use frontend\modules\preorder\components\PreorderService;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * PreorderController implements the CRUD actions for Preorder model.
 */
class PreorderController extends AdminController
{

    /**
     * @var PreorderEmailer
     */
    protected $emailer;


    /** @var PreorderService */
    protected $preorderService;

    /**
     * @param PreorderEmailer $emailer
     */
    public function injectDependencies(
        PreorderEmailer $emailer,
        PreorderService $preorderService
    )
    {
        $this->emailer         = $emailer;
        $this->preorderService = $preorderService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Preorder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new PreorderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Preorder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $preorder                 = $this->findModel($id);
        $preorderWorkDataProvider = PreorderService::getWorksDataProvider($preorder);
        return $this->render('view', [
            'preorder'                 => $preorder,
            'preorderWorkDataProvider' => $preorderWorkDataProvider
        ]);
    }

    /**
     * Creates a new Preorder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Preorder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Preorder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $preorder = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $preorder->load(Yii::$app->request->post());
            if ($preorder->validate()) {
                $preorder->save();
                return $this->redirect(['view', 'id' => $preorder->id]);
            }
        }

        return $this->render('update', [
            'model' => $preorder,
        ]);

    }

    /**
     * Deletes an existing Preorder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionDecline($id)
    {
        $model = $this->findModel($id);
        $this->preorderService->declineOfferByModerator($model);
        return $this->redirect(['view', 'id' => $model->id]);
    }


    /**
     * Finds the Preorder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Preorder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Preorder::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
