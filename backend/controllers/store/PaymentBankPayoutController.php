<?php

namespace backend\controllers\store;

use backend\components\AdminAccess;
use backend\models\search\PaymentTransactionHistorySearch;
use backend\models\search\PaymentTransactionSearch;
use common\models\Payment;
use common\models\PaymentDetail;
use common\models\PaymentDetailOperation;
use common\models\PaymentTransaction;
use common\modules\payment\processors\BankPayoutProcessor;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use Yii;
use yii\data\ActiveDataProvider;


/**
 * Class PaymentBankPayout
 *
 * @package backend\controllers\store
 *
 * @property PaymentAccountService $paymentAccountService
 * @property PaymentService $paymentService
 */
class PaymentBankPayoutController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/store/payment-bank-payout';

    protected $accessGroup = 'payment';

    protected $paymentAccountService;

    protected $paymentService;

    /** @var BankPayoutProcessor */
    protected $bankPayoutProcessor;

    public function injectDependencies(
        PaymentAccountService $paymentAccountService,
        PaymentService $paymentService,
        BankPayoutProcessor $bankPayoutProcessor
    )
    {
        $this->paymentAccountService = $paymentAccountService;
        $this->paymentService        = $paymentService;
        $this->bankPayoutProcessor   = $bankPayoutProcessor;
    }

    public function init()
    {
        parent::init();
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'transfer-money' => ['post']
                ]
            ],
        ];
    }

    /**
     * @return mixed|string
     * @throws \yii\base\UserException
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');

        $searchModel  = Yii::createObject(PaymentTransactionSearch::class);
        $dataProvider = $searchModel->searchBankPayout(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionApprove(int $id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');
        $paymentTransaction = PaymentTransaction::tryFindByPk($id);
        $this->bankPayoutProcessor->updateToSuccess($paymentTransaction);
        $this->setFlashMsg(true, 'Approved');
        return $this->redirect('/store/payment-bank-payout/view?id='.$id);
    }

    public function actionDecline(int $id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');
        $paymentTransaction = PaymentTransaction::tryFindByPk($id);
        $this->bankPayoutProcessor->updateToFailed($paymentTransaction);
        $this->setFlashMsg(true, 'Declined');
        return $this->redirect('/store/payment-bank-payout/view?id='.$id);
    }

    public function actionView($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');
        $paymentTransaction  = PaymentTransaction::tryFindByPk($id);
        $searchModelHistory  = new PaymentTransactionHistorySearch();
        $dataProviderHistory = $searchModelHistory->search([]);
        $dataProviderHistory->query->andWhere(['transaction_id' => $id]);

        return $this->render('view', [
            'paymentTransaction'  => $paymentTransaction,
            'searchModelHistory'  => $searchModelHistory,
            'dataProviderHistory' => $dataProviderHistory
        ]);
    }
}