<?php

namespace backend\controllers\store;


/**
 * Tax CRUD controller
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class TaxCountryController extends \backend\components\CrudController
{
    protected $viewPath = '@backend/views/store/tax-country';
    public $accessGroup = 'custom';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\TaxCountrySearch();
        $this->mainModel = new \common\models\TaxCountry();
    }
}