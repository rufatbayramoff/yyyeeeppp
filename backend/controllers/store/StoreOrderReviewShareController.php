<?php
/**
 * Created by mitaichik
 */

namespace backend\controllers\store;


use backend\components\CrudController;
use backend\models\search\StoreOrderReviewShareSearch;
use common\models\StoreOrderReviewShare;

class StoreOrderReviewShareController extends CrudController
{
    protected $viewPath = '@backend/views/store/share';

    public $accessGroup = 'store_order_reviews';

    public function init()
    {
        parent::init();
        $this->mainModel = new StoreOrderReviewShare();
        $this->searchModel = new StoreOrderReviewShareSearch();
    }

    /**
     * @param $id
     */
    public function actionApprove($id)
    {
        /** @var StoreOrderReviewShare $model */
        $model = $this->findModel($id);
        $model->status = StoreOrderReviewShare::STATUS_APPROVED;
        $model->safeSave();
        $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @param $id
     */
    public function actionReject($id)
    {
        /** @var StoreOrderReviewShare $model */
        $model = $this->findModel($id);
        $model->status = StoreOrderReviewShare::STATUS_REJECTED;
        $model->safeSave();
        $this->redirect(\Yii::$app->request->referrer);
    }
}