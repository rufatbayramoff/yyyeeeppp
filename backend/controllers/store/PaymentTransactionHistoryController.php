<?php

namespace backend\controllers\store;

class PaymentTransactionHistoryController  extends  \backend\components\CrudController
{
    protected $viewPath = '@backend/views/store/payment-transaction-history';
    
    protected $accessGroup = 'payment'; 
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\PaymentTransactionHistorySearch();
        $this->mainModel = new \common\models\PaymentTransactionHistory();
    }
}