<?php
/**
 * Created by mitaichik
 */

namespace backend\assets;


use frontend\components\angular\AngularAsset;
use frontend\components\angular\AngularLibAsset;
use yii\web\AssetBundle;

class AngularAppAsset extends AssetBundle
{
    public $css = [
        '/app/hire-designer/hire-designer.css'
    ];

    public $js = [
        '/app/modal.js',
        '/app/notify.js',
        '/app/router-backend.js',
        '/app/hire-designer/hire-designer.js',
        '/app/store-order/store-order.js',
    ];

    public $depends = [
        AngularLibAsset::class,
        AngularAsset::class
    ];
}