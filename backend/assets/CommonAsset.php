<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.12.16
 * Time: 17:01
 */

namespace backend\assets;

use common\modules\translation\assets\TranslateFunctionTAsset;
use frontend\assets\SiteParamsAsset;
use yii\web\AssetBundle;

class CommonAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/js';

    public $js = [
        'underscore-min.js',
        'ts/commonJs.js',
        'ts/UuidHelper.js',
    ];

    public $depends = [
        SiteParamsAsset::class,
        TranslateFunctionTAsset::class
    ];
}
