<?php

use backend\models\site\SiteSettingsTemplateManager;
use beowulfenator\JsonEditor\JsonEditorWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\SystemSetting */
/* @var $form yii\widgets\ActiveForm */

sleep(0);
?>

<div class="system-setting-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?php
    $ajaxSubmit = !empty($formConfig['isAjax']) ? 'ajax-submit' : '';
    echo $form->field($model, 'group_id')->widget(
        Select2::classname(),
        [
            'data'          => yii\helpers\ArrayHelper::map(common\models\SystemSettingGroup::find()->asArray()->all(), 'id', 'title'),
            'options'       => ['placeholder' => 'Select', 'style' => 'width:100%'],
            'pluginLoading' => !empty($formConfig['isAjax']) ? false : true,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]
    );

    ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'value')->textarea(['rows' => 3]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'json', ['options' => ['class' => 'form-group jsonEditorField']])->widget(
        JsonEditorWidget::className(),
        [
            'schema'        => SiteSettingsTemplateManager::getSchema($model->group?$model->group->title:'', $model->key),
            'enableSelectize' => true,
            'clientOptions' => [
                'theme' => 'bootstrap3'
            ]
        ]
    ) ?>

    <div class="form-group">
        <label class="control-label col-sm-4"></label>
        <?= Html::submitButton(
            $model->isNewRecord ?
                Yii::t('app', 'Create') :
                Yii::t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success ' . $ajaxSubmit : 'btn btn-primary ' . $ajaxSubmit]
        ) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
