<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SystemSetting */

$this->title = Yii::t('app', 'Create System Setting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-setting-create">
 

    <?= $this->render('_form', [
        'model' => $model,
        'formConfig' => $formConfig ? : []
    ]) ?>

</div>
