<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SystemSettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'System Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-setting-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a(Yii::t('app', 'Create System Setting'), ['create'], ['class' => 'btn btn-success']) ?>
    
        <?php
            echo Html::button('Create setting group', [
                'class' => 'btn btn-default btn-ajax-modal',
                'value' => yii\helpers\Url::to('@web/site/setting/create-group'),
                'data-target' => '#modal_addsettinggroups',
            ]); 
        ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'groupTitle',
                'value' => 'group.title',
                'filter' => Html::activeDropDownList(
                    $searchModel, 
                    'group_id', 
                    yii\helpers\ArrayHelper::map(common\models\SystemSettingGroup::find()->asArray()->all(), 'id', 'title' ),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'key',
            'value',
            //'created_at:date',
             'updated_at:datetime',
            // 'created_user_id',
            // 'updated_user_id',
            'description',
            [
                'attribute' => 'json',
                'value' => function ($model) {
                    return json_encode(json_decode($model->json), JSON_PRETTY_PRINT);
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <hr />
    <p>Usage example:
  <pre> $denyUsernames = \Yii::$app->setting->get('user.disallowusernames'); </pre>
</p>
</div>
