<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SystemSetting */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-setting-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(
            Yii::t('app', 'Delete'),
            ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method'  => 'post',
                ],
            ]
        ) ?>
    </p>

    <?= DetailView::widget(
        [
            'model'      => $model,
            'attributes' => [
                'id',
                'group.title',
                'key',
                'value',
                'created_at:datetime',
                'updated_at:datetime',
                'user.username',
                'updater.username',
                'description',
                [
                    'format' => 'html',
                    'label'  => 'Json',
                    'value'  => str_replace([' ', "\n"], ['&nbsp;', "<br>\n"],json_encode(json_decode($model->json), JSON_PRETTY_PRINT)),
                ],
            ],
        ]
    ) ?>

    <p>
        Usage:
    <pre>
           $<?php echo $model->key; ?>Config = \Yii::$app->setting->get('<?php echo $model->group->title; ?>.<?php echo $model->key; ?>'); 
         </pre>
    </p>
</div>
