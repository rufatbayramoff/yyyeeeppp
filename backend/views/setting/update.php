<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SystemSetting */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'System Setting',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="system-setting-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
