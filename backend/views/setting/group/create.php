<?php



/* @var $this yii\web\View */
/* @var $model common\models\SystemSettingGroup */

$this->title = Yii::t('app', 'Create System Setting Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Setting Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-setting-group-create">
    <?= $this->render('_form', [
        'model' => $model,
        'formConfig'=> $formConfig
    ]) ?>

</div>
