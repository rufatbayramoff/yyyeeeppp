<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SystemSettingGroup */
/* @var $form yii\widgets\ActiveForm */
$ajaxSubmit = !empty($formConfig['isAjax']) ? 'ajax-submit' : '';
?>

<div class="system-setting-group-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 
            Yii::t('app', 'Create') : Yii::t('app', 'Update'), 
            ['class' => $model->isNewRecord ? 'btn btn-success ' . $ajaxSubmit : 'btn btn-primary ' . $ajaxSubmit])
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
