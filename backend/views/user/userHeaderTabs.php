<?php
$links = [
   '@web/user/user' => 'Users',
  // '@web/user/user-access/index' => 'Users accesses',
   '@web/user/user/logs' => 'Users Logs',
   '@web/user/user/logins' => 'Users login logs',
];
$items = [];
$curUrl = (\Yii::$app->request->getPathInfo());
foreach($links as $k=>$v){
    $item = ['label'=>$v, 'url'=>$k];
    
    if('@web/'.$curUrl==$k){
        $item['active'] = true;
    }
    $items[] = $item;
    
}
echo \yii\bootstrap\Tabs::widget([
    'items' => $items,
    'options' => ['style'=>'margin-bottom:20px;']
]); 