<?php

use backend\components\AdminAccess;
use backend\models\search\UserLoginLogSearch;
use backend\modules\support\components\SupportTopicFacade;
use common\models\message\helpers\UrlHelper;
use common\models\MsgTopic;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\models\UserAddress;
use common\models\UserSession;
use common\modules\payment\services\PaymentService;
use lib\money\Currency;
use lib\money\Money;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title                   = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/** @var \common\modules\payment\services\PaymentAccountService $paymentAccountService */
$paymentService        = Yii::createObject(PaymentService::class);
$paymentAccountService = Yii::createObject(\common\modules\payment\services\PaymentAccountService::class);
$paymentAccountMainUsd = $paymentAccountService->getUserPaymentAccount($model, PaymentAccount::ACCOUNT_TYPE_MAIN);
$paymentAccountMainEur = $paymentAccountService->getUserPaymentAccount($model, PaymentAccount::ACCOUNT_TYPE_MAIN);
$mainAccountBalanceUsd = $paymentAccountMainUsd ? $paymentAccountService->calculateAccountBalance($paymentAccountMainUsd) : null;
$mainAccountBalanceEur = $paymentAccountMainEur ? $paymentAccountService->calculateAccountBalance($paymentAccountMainEur) : null;
$invalidEmailMarker    = $model->invalidEmailMarker();
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if (AdminAccess::can('user.wipe')) {
            echo Html::a(Yii::t('app', 'Wipe'), ['wipe', 'userId' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => Yii::t('app', 'USER will be WIPED!'),
                    'method'  => 'post',
                ],
            ]);
        }
        ?>
        <?= Html::a(Yii::t('app', 'Send message'),
            ['/support/active-topics/create-topic-form', 'withUserId' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?php
        if (AdminAccess::can('user.login_as')) {
            echo Html::a(Yii::t('app', 'Login via user'),
                ['/user/user/login', 'userId' => $model->id], ['class' => 'btn btn-danger', 'data-confirm' => 'Are you sure?', 'data-method' => 'post']);
        }
        ?>
    </p>

    <div class="row">
        <div class="col-lg-4">
            <div class="box box-solid">
                <div class="box-body">
                    <b>Moderator Comments</b>
                    <?php
                    echo \common\modules\comments\widgets\Comments::widget(['model' => 'backend_user', 'model_id' => $model->id]);
                    ?>
                </div>
            </div>


            <?= DetailView::widget([
                'model'      => $model,
                'attributes' => [
                    'id',
                    'username', [
                        'attribute' => 'email',
                        'format'    => 'raw',
                        'value'     => H($model->email) . ' ' . ($invalidEmailMarker ? ' <a class="form-message" href="/user/invalid-email/update?id=' . $invalidEmailMarker->id . '">INVALID EMAIL</a>' : '')
                    ],
                    [
                        'attribute' => 'status',
                        'value'     => $model->getStatusText($model->status),
                    ],
                    [
                        'label'  => 'Comapny',
                        'format' => 'raw',
                        'value'  => $model->company ? '<a href="/ps/ps/view?id=' . $model->company->id . '">' . H($model->company->title) . '</a>' : ''
                    ],
                    [
                        'label' => 'Deactivating reason text',
                        'value' => $model->userDeactivatingLog ? $model->userDeactivatingLog->getDeactivatingReasonText() : ''
                    ],
                    'created_at:datetime',
                    'updated_at:datetime',
                    'shadow_message_ban_from:datetime',
                    'uid',
                    'userProfile.website',
                    'userProfile.url_facebook',
                    'userProfile.url_twitter',
                    'userProfile.url_instagram',
                    [
                        'label' => 'Last online',
                        'value' => $model->userStatistics ? $model->userStatistics->lastonline_at : ''
                    ]
                ],
            ]) ?>

        </div>
        <div class="col-lg-4">
            <?php
            if ($model->userProfile) {
                echo DetailView::widget([
                    'model'      => $model->userProfile,
                    'attributes' => [
                        'full_name',
                        'dob_date',
                        'website',
                        'phone',
                        'phone_confirmed:boolean',
                        'updated_at',
                        'current_lang',
                        'current_metrics',
                        'timezone_id',
                        'firstname',
                        'lastname',
                        'is_printservice',
                        'is_modelshop',
                        'gender',
                        'printservice_title',
                        'modelshop_title',

                    ],
                ]);
            } ?>
            <?php
            $contentBlockUserStatistics = Yii::$app->getModule('contentAutoBlocker')->statistics->formStatisticForUser($model);
            echo DetailView::widget([
                'model'      => $contentBlockUserStatistics,
                'attributes' => [
                    'trySendBannedMessageCount',
                    'trySendBannedMessageToInterlocutorCount',
                    'tryReceiveBannedMessageCount',
                    'tryReceiveBannedMessageFromInterlocutorCount',
                ],
            ]);
            ?>

        </div>
        <div class="col-lg-4">
            <?php
            if (backend\components\AdminAccess::can('usertaxinfo.payout')) {
                if ($model->hasDocument()):?>
                    <h3>Document</h3>
                    <?= Html::a(Html::img(Url::to(['document-image', 'userId' => $model->id]),
                        ['style' => 'width: 200px']), Url::to(['document-image', 'userId' => $model->id]), ['target' => '_blank']) ?>
                <?php endif;
            }
            ?>

            <h3>Support message</h3>

            <?php echo GridView::widget([
                'dataProvider' => SupportTopicFacade::getUserSupportTopicsDataProvider($model),
                'columns'      => [
                    [
                        'attribute' => 'Subject',
                        'format'    => 'raw',
                        'value'     => function (MsgTopic $model) {
                            $link = Html::a(\H($model->title), ['/support/active-topics/view-topic', 'topicId' => $model->id]);

                            if ($model->lastMessage) {
                                $text = $model->lastMessage->text;

                                if ($text) {
                                    $link .= Html::tag('p', \H(StringHelper::truncate($text, '50')));
                                }
                            }

                            return $link;
                        }
                    ],
                    [
                        'attribute' => 'Bind',
                        'format'    => 'raw',
                        'value'     => function (MsgTopic $model) {
                            if (!$model->getBindedObject()) {
                                return null;
                            }

                            return Html::a(
                                $model->bind_to . ': #' . $model->getBindedObject()->id,
                                UrlHelper::bindedObjectBackendRoute($model->getBindedObject()),
                                ['target' => '_blank']
                            );
                        }
                    ]
                ],
            ]); ?>

        </div>
    </div>
    <?php
    $user = $model;
    if (backend\components\AdminAccess::can('usertaxinfo.payout')):
        if ($user->userPaypal) {
            ActiveForm::begin(['action' => ['user/user/payout']]);
            echo '<table><tr><td>';
            echo "<td>" . "Has amount : " . displayAsMoney($mainAccountBalanceUsd) . ' / ' . displayAsMoney($mainAccountBalanceEur) . " &nbsp;</td><td>";
            echo Html::hiddenInput('user_id', $user->id);
            echo Html::textInput('amount', null, ['class' => 'form-control']);
            echo "</td><td> &nbsp; </td><td>";
            echo Html::submitButton(_t("site.payout", 'Manual Withdraw Now'), ['class' => 'btn btn-danger']);
            echo "<p></p>";
            echo "</td></tr></table>";
            ActiveForm::end();
        }
        ?>
        <div class="row">
            <div class="col-lg-5">
                <h3>User Payments History</h3>

                <?php
                $formHtml = '';
                ob_start();
                $form = \yii\bootstrap\ActiveForm::begin([
                    'action'  => '/user/user/report?userId=' . $user->id,
                    'layout'  => 'inline',
                    'method'  => 'get',
                    'options' => ['target' => '_blank']
                ]); ?>
                <div class="form-group">
                    <label class="payments-create-report__label"><?= _t('site.payout', 'From'); ?></label>
                    <input type="date" name="from" class="form-control input-sm payments-create-report__input"
                           value="<?= date("Y-m-d", $user->created_at); ?>">
                </div>
                <div class="form-group">
                    <label class="payments-create-report__label"><?= _t('site.payout', 'to'); ?></label>
                    <input type="date" name="to" class="form-control input-sm payments-create-report__input"
                           value="<?= date("Y-m-d"); ?>">
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-primary btn-ghost payments-create-report__btn"><?= _t('site.payout', 'Create report'); ?></button>
                </div>
                <?php \yii\bootstrap\ActiveForm::end();
                $formHtml = ob_get_contents();
                ob_end_clean();
                echo $formHtml;
                ?>

                <?php
                echo GridView::widget([
                    'rowOptions'   => function (PaymentDetail $model, $index, $widget, $grid) {
                        if ($model->amount < 0) {
                            return [
                                'style' => 'background: #f0afaf;'
                            ];
                        }
                    },
                    'dataProvider' => $paymentAccountService->getPaymentDetailsProvider($model),
                    'columns'      => [
                        'id',
                        [
                            'label' => 'Payment description',
                            'value' => function (PaymentDetail $paymentDetail) {
                                return $paymentDetail->paymentDetailOperation->payment->description;
                            }
                        ],
                        'formDate:datetime',
                        [
                            'label' => 'Amount',
                            'value' => function (PaymentDetail $paymentDetail) {
                                return displayAsMoney($paymentDetail->getMoneyAmount());
                            }
                        ],
                        [
                            'label' => 'Result',
                            'value' => function (PaymentDetail $paymentDetail) {
                                return displayAsMoney($paymentDetail->getResultAmount());
                            }
                        ],
                        [
                            'label' => 'Transaction description',
                            'value' => function (PaymentDetail $paymentDetail) {
                                return $paymentDetail->description;
                            }
                        ]
                    ],
                ]);
                ?>
                Has Amount: <b><?php echo $mainAccountBalanceUsd ? displayAsMoney($mainAccountBalanceUsd) : '' ?></b> / <b><?php echo $mainAccountBalanceEur ? displayAsMoney($mainAccountBalanceEur) : '' ?></b>

                <div>
                    <h3>Anonymous orders</h3>

                    <?= GridView::widget([
                        'dataProvider' => $anonymousOrdersProvider,
                        'columns'      => [
                            [
                                'attribute' => 'id',
                                'format'    => 'raw',
                                'value'     => function (StoreOrder $model) {
                                    $url = Url::toRoute(['store/store-order/view', 'id' => $model->id]);
                                    return Html::a('#' . $model->id, $url,
                                        ['class' => 'btn  btn-ghost btn-info', 'target' => '_blank', 'style' => '']
                                    );
                                }
                            ],
                            'created_at:datetime',
                            [
                                'label' => 'Price',
                                'value' => function (StoreOrder $storeOrder) {
                                    return displayAsMoney($storeOrder->primaryPaymentInvoice->getAmountTotal());
                                }
                            ],
                            [
                                'label'  => 'Address',
                                'format' => 'raw',
                                'value'  => function (StoreOrder $storeOrder) {
                                    if (!$storeOrder->deliveryType) {
                                        return '-';
                                    }
                                    $delivery = $storeOrder->deliveryType->title;
                                    return $storeOrder->shipAddress ? '<small>' .
                                        UserAddress::formatAddress($storeOrder->shipAddress, false) . '<br>' . $delivery . '</small>' : 'No information';
                                }
                            ]
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-lg-7">
                <h3>User Payment Transactions</h3>

                <?= GridView::widget([
                    'dataProvider' => PaymentTransaction::getDataProvider(['user_id' => $user->id], 10, ['sort' => ['defaultOrder' => ['id' => SORT_DESC]]]),
                    'rowOptions'   => function (PaymentTransaction $model, $index, $widget, $grid) {
                        if ($model->status === 'SUCCESS') {
                            return [
                                'style' => 'background: #aff0af;'
                            ];
                        }
                    },
                    'columns'      => [
                        'id',
                        'transaction_id',
                        'vendor',
                        'created_at:datetime',
                        'status',
                        [
                            'label' => 'amount',
                            'value' => function (PaymentTransaction $paymentTransaction) {
                                return $paymentTransaction->amount . ' ' . $paymentTransaction->currency;
                            }
                        ],
                        'type',
                        [
                            'label'  => 'comment',
                            'format' => 'raw',
                            'value'  => function (PaymentTransaction $paymentTransaction) use ($paymentService) {
                                if ($paymentService->allowCancelPayout($paymentTransaction)) {
                                    return '<a href="/store/payment-transaction/cancel-payout?id=' . $paymentTransaction->id . '" class="btn btn-default" onclick="return confirm(\'Are you shure?\')" data-method="post">Cancel</a>';
                                }
                            }
                        ],
                    ],
                ]); ?>
            </div>
            <div class="col-lg-7">
                <h3>User logins</h3>

                <?php
                $userLoginLogSearch = Yii::createObject(UserLoginLogSearch::class);
                echo GridView::widget([
                    'dataProvider' => $userLoginLogSearch->search([$userLoginLogSearch->formName() => ['user_id' => $user->id]]),
                    'columns'      => [
                        'created_at',
                        'login_type',
                        'remote_addr',
                        'http_user_agent',
                        'http_referer'
                    ],
                ]); ?>
            </div>
            <div class="col-lg-7">
                <h3>User sessions</h3>

                <?php
                $userSession = Yii::createObject(UserSession::class);
                echo GridView::widget([
                    'dataProvider' => $userLoginLogSearch->search([$userSession->formName() => ['user_id' => $user->id]]),
                    'columns'      => [
                        'created_at',
                        'ip',
                        'referrer',
                    ],
                ]); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
