<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserLoginLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Login Logs');
$this->params['breadcrumbs'][] = $this->title;

echo $this->renderFile(Yii::getAlias('@backend/views/user/userHeaderTabs.php'));

?>
<div class="user-login-log-index">

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'user_id',
            'login_type',
            'result',
            'remote_addr',
            'http_user_agent:ntext',
            'created_at:datetime',
            'logout_at:datetime',
            'http_referer:url',
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model) {
                        $customurl = Yii::$app->getUrlManager()->createUrl(['crud/user-login-log/view', 'id' => $model['id']]); //$model->id для AR
                        return \yii\helpers\Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                $customurl, ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                    }
                    ],
                    'template' => '{view}  ',
                ],
            ],
        ]);

        ?>

</div>
