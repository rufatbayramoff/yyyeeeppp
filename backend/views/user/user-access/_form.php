<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\components\SimpleSelect2;
/* @var $this yii\web\View */
/* @var $model common\models\UserAccess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-access-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?php 
    $ajaxSubmit = !empty($formConfig['isAjax']) ? 'ajax-submit' : '';
    $initValue = $model->user ? $model->user->username : '';
    echo $form->field($model, 'user_id')->widget(Select2::classname(), 
        SimpleSelect2::getAjax(['user/list'], 'id,username', $initValue)
    );
    $obj = new common\models\SystemSettingGroup();
    echo $form->field($model, 'access_id')->widget(
        Select2::classname(), SimpleSelect2::get($obj)
    ); 
    ?> 
    <?php # echo $form->field($model, 'access_id')->textInput(); ?>

    <?= $form->field($model, 'is_active')->checkbox([], false) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', 
         ['class' => $model->isNewRecord ? 'btn btn-success ' . $ajaxSubmit : 'btn btn-primary ' . $ajaxSubmit]) ?>
         
    </div>
    <?php ActiveForm::end(); ?>
</div>
