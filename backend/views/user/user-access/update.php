<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserAccess */

$this->title = 'Update User Access: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Accesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-access-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
