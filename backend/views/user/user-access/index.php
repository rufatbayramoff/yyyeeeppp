<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserAccessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Accesses');
$this->params['breadcrumbs'][] = $this->title;

echo $this->renderFile(Yii::getAlias('@backend/views/user/userHeaderTabs.php'));

$this->registerJsFile(
    '@web/js/admin.js',
    ['depends'=>'backend\assets\AppAsset']
);

?>

<div class="user-access-index">

    <p>
        <?= Html::a(Yii::t('app', 'Add User Access'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(['options' => ['class' => 'pjax-wraper']]); ?>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'userName',
                'value' => function($model) {
                    return $model->user->username;
                }
            ],
            [
                'attribute' => 'accessName',
                'value' => function($data) {
                    return $data->access->group->title . '.' . $data->access->code;
                }
            ],
            'created_at:datetime',
            [
                'attribute' => 'is_active',
                'format' => 'raw',
                'value' => function($model) {
                    $url = \yii\helpers\Url::toRoute(['/user-access/change-active', 'id' => $model->id]);
                    $val = $model->is_active ? 'Yes' : 'No';
                    $kVal = $model->is_active ? 'ok' : 'remove';
                    return Html::a(sprintf('<span class="glyphicon glyphicon-%s"></span> %s',$kVal, $val), $url, [
                            'title' => 'Change',
                            'data-pjax' => '0',
                            'class' => 'grid-action'
                    ]);
                }
            ], 
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    ?>
    <?php \yii\widgets\Pjax::end(); ?>
    
</div>
