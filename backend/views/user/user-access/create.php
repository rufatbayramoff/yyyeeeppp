<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserAccess */

$this->title = 'Add User Access';
$this->params['breadcrumbs'][] = ['label' => 'User Accesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-access-create">

    <?= $this->render('_form', [
        'model' => $model,
        'formConfig' => $formConfig?:[]
    ]) ?>

</div>
