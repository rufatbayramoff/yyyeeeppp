<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.05.18
 * Time: 10:56
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
?>
    <div class="form-group field-storeunitrejectform-reasondescription">
        <label class="control-label" for="declineform-reasondescription">Reason Description</label>
        <textarea id="declineform-reasondescription" class="form-control" name="TaxFormDeclineForm[reasonDescription]"></textarea>

        <div class="help-block"></div>
    </div>
<?= Html::submitButton('Reject publish', ['class' => 'btn btn-warning ajax-submit']) ?>
<?php ActiveForm::end(); ?>