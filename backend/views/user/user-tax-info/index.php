<?php

use backend\models\search\UserTaxInfoSearch;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserTaxInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Tax Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-tax-info-index">
  
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $index, $widget, $grid){
            $res =\frontend\models\site\DenyCountry::checkCountry($model->place_country, false);
            if($model->address){
                $res = $res && \frontend\models\site\DenyCountry::checkCountry($model->address->country->iso_code, false);
            }else{
                $res = false;
            }
            if(!$res || $model->status==\common\models\UserTaxInfo::STATUS_DECLINE){
                return [
                    'style'=>'background: #ffdfaf;'
                ];
             }
        },
        'columns' => [
             [
                'attribute'=>'id', 
                'format' => 'raw',
                'value' => function($model){
                    $url = yii\helpers\Url::toRoute(['user/user-tax-info/view', 'id'=>$model->id]);
                    $link1 = yii\helpers\Html::a('View ID:' .$model->id, $url, ['class'=>'btn btn-success btn-block']);
                    $url = yii\helpers\Url::toRoute(['user/user-tax-info/view', 'id'=>$model->id, 'moderate'=>1]);
                    $link2 =  yii\helpers\Html::a('Moderation', $url, ['class'=>'btn btn-ghost btn-info btn-block']);
                    return $link1 . $link2;
                }
            ], 
            'user_id',
                'user.email',
                'user.username',
            'updated_at:datetime',
            'is_usa',
            'address_id', 
            'address_id',
            [
                'format' => 'raw',
                'attribute' => 'identification_type',
                'value'=>function ($model) {
                    $filledStatus = empty($model->identification) ? "Empty" : "Complete";
                    return $model->identification_type . "<br> " . $filledStatus;
                }
            ],
            //'identification',
            'classification',
            'name',
            'business_name',
            // 'mailing_address_id',
            [
                'attribute' => 'status',
                'value'=>function ($model) {
                    return $model->status;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    $searchModel::getStatuses(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'place_country',
            'signature',
            'sign_date',
            [
                'attribute' => 'sign_type',
                'filter' => UserTaxInfoSearch::SIGN_TYPE_LABELS
            ],

            ['class' => 'yii\grid\ActionColumn', 'template'=>'{view}',],
        ],
    ]); ?>

</div>
