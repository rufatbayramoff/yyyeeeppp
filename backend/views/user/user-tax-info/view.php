<?php

use common\models\PaymentAccount;
use lib\money\Currency;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserTaxInfo */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'User Tax Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$geoCountry                    = \common\models\GeoCountry::findOne(['iso_code' => $model->place_country]);
$placeCountryTitle             = 'Not set';
if ($geoCountry) {
    $placeCountryTitle = $geoCountry->title;
}
if (empty($model->identification_type)) {
    $model->identification_type = 'Foreign tax identifying';
}
?>
<div class="user-tax-info-view row">
    <div class="col-md-5">
        <h3>Form type: <?php
            echo $model->getFormType(true);
            ?></h3>
        <?= DetailView::widget(
            [
                'model'      => $model,
                'attributes' => [
                    'user.username',
                    'created_at:datetime',
                    'updated_at:datetime',
                    'is_usa',
                    [
                        'attribute' => 'address_id',
                        'format'    => 'raw',
                        'value'     => $model->address ? \common\models\UserAddress::formatAddress($model->address, true) : '',
                    ],
                    'identification_type',
                    'identification:boolean',
                    'classification',
                    'name',
                    'business_name',
                    'rsa_tax_rate',
                    'email_notify:boolean',
                    'status',
                    [
                        'attribute' => 'place_country',
                        'format'    => 'raw',
                        'value'     => $placeCountryTitle,
                    ],
                    'signature',
                    'dob_date:date'
                ],
            ]
        ) ?>
        <?php if ($moderate): ?>
            <?= Html::a('Approve', ['approve', 'id' => $model->id], ['class' => 'btn btn-success', 'data-method' => 'post']) ?>
            <?= Html::button(
                'Decline',
                [
                    'title'       => 'Decline reason',
                    'class'       => 'btn btn-warning btn-ajax-modal',
                    'data-pjax'   => 0,
                    'value'       => \yii\helpers\Url::toRoute(['/user/user-tax-info/decline', 'id' => $model->id]),
                    'data-target' => '#model_reject'
                ]
            ); ?>
        <?php endif; ?>



        <?php

        if ($model->user->userPaypal) {
            $status = $model->user->userPaypal->check_status;
            if ($status != 'ok') {
                echo "<div style='background: #ffaaaa'>";
            } else {
                // ok payout
                if (backend\components\AdminAccess::can('usertaxinfo.payout')) {
                    /** @var \common\modules\payment\services\PaymentAccountService $paymentAccountService */
                    $paymentAccountService = Yii::createObject(\common\modules\payment\services\PaymentAccountService::class);
                    $hasAmountUsd          = $paymentAccountService->calculateAccountBalance($paymentAccountService->getUserPaymentAccount($model->user, PaymentAccount::ACCOUNT_TYPE_MAIN));

                    ActiveForm::begin(['action' => ['user/user/payout']]);
                    echo '<table><tr><td>';
                    echo "<td>" . "Has amount : " . displayAsMoney($hasAmountUsd) . " &nbsp;</td><td>";
                    echo Html::hiddenInput('user_id', $model->user->id);
                    echo Html::textInput('amount', null, ['class' => 'form-control']);
                    echo "</td><td> &nbsp; </td><td>";
                    echo Html::submitButton(_t("site.payout", 'Manual Withdraw Now'), ['class' => 'btn btn-danger']);
                    echo "<p></p>";
                    echo "</td></tr></table>";
                    ActiveForm::end();
                }
                echo "<div>";
            }
            echo " <h3>Paypal Information - $status</h3>";

            if ($model->user->userPaypal) {
                echo DetailView::widget(
                    [
                        'model'      => $model->user->userPaypal,
                        'attributes' => [
                            'updated_at:datetime',
                            'fullname',
                            'email:email',
                            'address',
                            'country_iso' => [
                                'attribute' => 'place_country',
                                'format'    => 'raw',
                                'value'     => $model->user->userPaypal->country_iso . ($model->user->userPaypal->country_iso != ($geoCountry->iso_code ?? 'none') ? ' <span style="background-color:orange">(Different tax form country)</span>' : ''),
                            ],
                            'check_status',
                        ],
                    ]
                );
            } else {
                echo "No";
            }
            echo "</div>";
            $paypalDp = common\models\UserPaypalHistory::getDataProvider(['user_id' => $model->user_id], 10);
            $paypalDp->query->orderBy('created_at desc');
            echo GridView::widget(
                [
                    'dataProvider' => $paypalDp,
                    'columns'      => [
                        'id',
                        'created_at:datetime',
                        'action_id',
                        [
                            'format' => 'raw',
                            'value'  => function ($model) {
                                $comment = $model->comment;
                                if (!is_array($comment)) {
                                    return "";
                                }
                                return \yii\helpers\VarDumper::dumpAsString($comment, 10, true);
                            }
                        ]
                    ]
                ]
            );

        }

        ?>

    </div>
    <div class="col-md-7">

        <h3>Login logs</h3>
        <?=
        \yii\grid\GridView::widget(
            [
                'dataProvider' => $loginlog['dataProvider'],
                'filterModel'  => $loginlog['searchModel'],
                'columns'      => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'login_type',
                    'result',
                    'remote_addr',
                    'created_at:datetime'
                ]
            ]
        );

        ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= \yii\grid\GridView::widget(
            [
                'dataProvider' => $history['dataProvider'],
                'filterModel'  => $history['searchModel'],
                'rowOptions'   => function ($model, $index, $widget, $grid) {
                    if ($model->action_id == 'decline') {
                        return [
                            'style' => 'background: #ffafaf;'
                        ];
                    }
                },
                'columns'      => [
                    'id',
                    'user.username',
                    'created_at:datetime',
                    'action_id',
                    [
                        'format' => 'raw',
                        'value'  => function ($model) {
                            $comment = $model->comment;
                            if (!is_array($comment)) {
                                return $comment;
                            }
                            unset($comment['identification']);
                            return \yii\helpers\VarDumper::dumpAsString($comment, 10, true);
                        }
                    ]
                ],
            ]
        ); ?>
    </div>
</div>