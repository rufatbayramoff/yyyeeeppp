<?php

use backend\components\columns\UserColumn;
use common\models\BrowserPushSubscriptionUser;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BrowserPushSubscriptionUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Push browser subscriptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-comment-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'uid',
            'created_at',
            [

                'label'     => 'User',
                'attribute' => 'user_id',
                'relation'  => 'user',
                'class'     => UserColumn::class,
            ],
            'ip',
            [
                'attribute' => 'user_agent',
                'value'     => 'browserPushSubscription.user_agent'
            ],
            [
                'attribute' => 'endpoint',
                'format'    => 'raw',
                'value'     => function (BrowserPushSubscriptionUser $model) {
                    return '<div style="max-width: 80%;word-wrap: break-word; ">' . H($model->browserPushSubscription->endpoint) . '</div>';
                }
            ]
        ],
    ]); ?>
</div>
