<?php

use common\models\User;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;

echo $this->renderFile(Yii::getAlias('@backend/views/user/userHeaderTabs.php'));
?>

<div class="user-index">  
    <?= GridView::widget([
        'responsiveWrap' => false,
        'export' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'uid',
            [ 'attribute'=>'id',  'contentOptions'=>['style'=>'width: 100px;']],
            [ 
                'attribute'=>'username',
                'format' => 'raw',
                'value' => function($model){
                    $link = Html::a('Login log', 
                            ['user/user/logins', 'UserLoginLogSearch[user_id]' =>$model->id], 
                            ['class' => 'link']);
                    $linkLog = Html::a('Logs', 
                            ['user/user/logs', 'UserLogSearch[user_id]' =>$model->id], 
                            ['class' => 'link']);
                    $linkAccess = ""; //Html::a('Accesses',  ['user/user-access/index', 'UserAccessSearch[user_id]' =>$model->id],  ['class' => 'link']);
                    
                    return sprintf(
                        '%s <br /> %s | %s  %s', 
                        $model->username, 
                        $link, 
                        $linkAccess,
                        $linkLog
                    );
                }
            ],
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'lastlogin_at', 
                'headerOptions'=>['class'=>'kv-sticky-column'],
                'contentOptions'=>['class'=>'kv-sticky-column'],
                'filterType'=>GridView::FILTER_DATE_RANGE,
                'format'=>['datetime'],
            ],  
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'created_at',  
                'filterType'=>GridView::FILTER_DATE_RANGE,
                'format'=>['date'],
            ],  
            [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'updated_at', 
                'headerOptions'=>['class'=>'kv-sticky-column'],
                'contentOptions'=>['class'=>'kv-sticky-column'],
                'filterType'=>GridView::FILTER_DATE_RANGE,
                'format'=>['datetime'],
            ], 
            ['attribute' => 'full_name', 'value' => 'userProfile.full_name'],
            'email:email',
             [
                 'attribute' => 'status',
                 'value'=>function ($model) {                     
                     return $model->getStatusText($model->status);
                 },
                'filter' => Html::activeDropDownList(
                    $searchModel, 
                    'status', 
                    $searchModel->getStatuses(),
                    ['class'=>'form-control','prompt' => 'All']),
            ], 
            [
                'attribute' => 'trustlevel',
                'value'=>function ($model) {                     
                     return $model->formatTrustLevel($model->trustlevel);
                },   'format' => 'raw',
                'filter' => Html::activeDropDownList(
                    $searchModel, 
                    'trustlevel', 
                    \lib\collection\CollectionDb::getList($searchModel->getTrustLevels(), 'code', 'title'),
                    ['class'=>'form-control','prompt' => 'All']),
            ], 
                     
            [
                'attribute' => 'language',
                'value' => 'userProfile.current_lang',
                'filter' => Html::activeDropDownList(
                    $searchModel, 
                    'language', 
                        \yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->asArray()->all(), 'iso_code', 'title'),
                        ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'Company create in progress',
                'value' => function(User $model){
                    return $model->is_company_creating_in_progress == 1 ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_company_creating_in_progress',
                    [1 => 'Yes', 0 => 'No'],
                    ['class' => 'form-control', 'prompt' => 'All']),
            ],
            ['class' => yii\grid\ActionColumn::class, 'template' => '{view} {update}'],
        ],
    ]); ?>

</div>
