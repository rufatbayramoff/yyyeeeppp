<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\UserRequest;
use \backend\components\AdminAccess;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Account Requests - Delete & Restore accounts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-request-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [ 
             [ 
                'attribute'=>'user_id',
                'format' => 'raw',
                'value' => function(UserRequest $model){
                    $link = Html::a('Login log',
                            ['user/user/logins', 'UserLoginLogSearch[user_id]' =>$model->user->id], 
                            ['class' => 'link']);

                    $linkLog = Html::a('Logs', 
                            ['user/user/logs', 'UserLogSearch[user_id]' =>$model->user->id], 
                            ['class' => 'link']);
                    $user  = \backend\models\Backend::displayUser($model->user);
                    return sprintf(
                        '%s <br /> %s | %s  ', 
                        $user, 
                        $link, 
                        $linkLog
                    );
                }
            ],  
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'moderator_id',
                'value' => function($model){
                    if($model->moderator){
                        return $model->moderator->username . ' - id[' . $model->moderator_id. ']';
                    }
                    return null;
                }
            ],
            'request_type',
            'status',
            [  
                'format' => 'raw',
                'value' => function($model){
                    $txt1 = $txt2 = "";
                    if($model->request_type==UserRequest::REQUEST_TYPE_DELETE){
                        if ($model->status == UserRequest::STATUS_NEW) {
                            if (AdminAccess::can('user_request.account_delete')) {
                                $url = yii\helpers\Url::toRoute(['user/user-request/account-delete', 'id' => $model->id]);
                                $txt1 = yii\helpers\Html::a('Delete Account', $url, ['class' => 'btn btn-danger']);
                            }
                        }else if($model->status==UserRequest::STATUS_DONE){
                            // allow to restore
                            if (AdminAccess::can('user_request.account_restore')) {
                                $url = yii\helpers\Url::toRoute(['user/user-request/account-restore', 'id' => $model->id]);
                                $txt1 = yii\helpers\Html::a('Restore Account', $url, ['class' => 'btn btn-info']);
                            }
                        }
                        if($model->status == UserRequest::STATUS_NEW && AdminAccess::can('user_request.cancel')){
                            $url = yii\helpers\Url::toRoute(['user/user-request/cancel', 'id'=>$model->id]);
                            $txt2 = yii\helpers\Html::a('Cancel', $url, ['class'=>'btn btn-ghost btn-warning']);
                        }
                    }
                    return $txt1 . " " .  $txt2;                   
                }
            ] 
        ],
    ]); ?>

</div>
