<?php

use yii\helpers\Html;
use kartik\widgets\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(
        $model->getStatuses(),           // Flat array ('id'=>'label')
        ['prompt' => '']    // options
    )
    ?>
    <?= $form->field($model, 'shadow_message_ban_from')->widget(DatePicker::class, [
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
        ],
    ]); ?>

    <?= $form->field($model, 'trustlevel')->dropDownList(
        \lib\collection\CollectionDb::getList($model->getTrustLevels(), 'code', 'title'),           // Flat array ('id'=>'label')
        ['prompt' => '']    // options
    )
    ?>

    <?= $form->field($model, 'access_token')->textInput(['maxlength' => true]) ?>
    <p>Generated access token : <?php
        echo md5($model->id . $model->email);
        ?></p>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
