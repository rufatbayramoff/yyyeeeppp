<?php

use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $invalidEmail \common\models\InvalidEmail */

$this->title                   = 'Update: ' . $invalidEmail->email;
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-request-update">

    <?= DetailView::widget([
        'model'      => $invalidEmail,
        'attributes' => [
            'id',
            'created_at',
            'email',
            'type'
        ],
    ]) ?>

    <div class="user-request-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($invalidEmail, 'ignored')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
