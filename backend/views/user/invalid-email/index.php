<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\InvalidEmailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('app', 'Invalid Emails');
$this->params['breadcrumbs'][] = $this->title;

?>
<div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            'created_at',
            'email',
            'type',
            [
                'attribute' => 'ignored',
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'ignored',
                    ['0' => 'No', '1' => 'Yes'],
                    ['class' => 'form-control', 'prompt' => 'All']
                ),
            ],
            [
                'label' => 'Users list',
                'format' => 'raw',
                'value' => function (\common\models\InvalidEmail $invalidEmail) {
                    $url = '/user/user?UserSearch[email]='.urlencode($invalidEmail->email);
                    return '<a href="'.$url.'">users list</a>';
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>

</div>
