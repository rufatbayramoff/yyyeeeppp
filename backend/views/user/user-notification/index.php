<?php

use backend\components\columns\UserColumn;
use common\models\NotifyPopup;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\NotifyPopupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-comment-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'uid',
            'created_at',
            'expire_at',
            [

                'label'     => 'User',
                'attribute' => 'user_id',
                'relation'  => 'user',
                'class'     => UserColumn::class,
            ],
            'title',
            'text',
            [
                'attribute' => 'url',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return Html::a($model->url, $model->url, ['target' => '_blank']);
                }
            ],
            [
                'attribute' => 'Receive status',
                'format'    => 'raw',
                'value'     => function (NotifyPopup $model) {
                    $status = '<div style="width:500px">';
                    foreach ($model->notifyPopupEndpoints as $endpoint) {
                        $status .= Html::A(substr($endpoint->endpoint, 0, 42), $endpoint->endpoint) . ' - ' . $endpoint->received_at . '<br>';
                    }
                    $status .= '</div>';
                    return $status;
                }
            ],

        ],
    ]); ?>
</div>
