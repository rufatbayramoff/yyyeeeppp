<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserPaypalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Paypals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-paypal-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Paypal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'updated_at',
            'user_id',
            'fullname',
            // 'email:email',
            // 'address',
            // 'country_iso',
            // 'check_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
