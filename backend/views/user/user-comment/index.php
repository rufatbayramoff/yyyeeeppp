<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-comment-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'model',
            #'model_id',
            'user_id',
            'content:ntext',
            [
            'attribute'=>'url',
            'format' => 'raw',
            'value' => function($model){
                return Html::a($model->url, param('server'). $model->url, ['target'=>'_blank']);
            }
            ],
            'username',
             'email:email',
             'parent_id',
            // 'super_parent_id',
              'status',
            [
                'attribute' => 'status',
                'value'     => function (\common\models\UserComment $model) {
                    $commentsStatus = \common\modules\comments\models\Comment::getStatusList();
                    return $commentsStatus[$model->status]??'';
                },
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    \common\modules\comments\models\Comment::getStatusList(),
                    ['class' => 'form-control', 'prompt' => 'All']
                ),
            ],
              'created_at',
             # 'updated_at',
            # 'updated_by',
              'user_ip',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
