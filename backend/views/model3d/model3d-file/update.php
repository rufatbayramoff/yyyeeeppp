<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dPart */

$this->title = 'Update Model3d File: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Model3d Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="model3d-file-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
