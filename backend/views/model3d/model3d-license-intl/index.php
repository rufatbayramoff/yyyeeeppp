<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dLicenseIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3d License Translates';
$this->params['breadcrumbs'][] = $this->title;

echo $this->renderFile(Yii::getAlias('@backend/views/model3d/model3d-license/_topTabs.php'));
?>
<div class="model3d-license-intl-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model3d License Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model_id',
            'title',
            'description:ntext',
            'lang_iso',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
