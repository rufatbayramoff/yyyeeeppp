<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dLicenseIntl */

$this->title = 'Update Model3d License Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Model3d License Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="model3d-license-intl-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
