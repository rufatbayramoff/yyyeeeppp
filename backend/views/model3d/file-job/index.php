<?php

use console\jobs\QueueGateway;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FileJobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'File Jobs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-job-index">

    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'rowOptions'   => function ($model, $index, $widget, $grid) {
                if ($model->status == QueueGateway::JOB_STATUS_COMPLETED) {
                    return [
                        'style' => 'background: #ddffdd;'
                    ];
                } else {
                    if ($model->status == 'running' || $model->status == 'waiting') {
                        return [
                            'style' => 'background: #ffdfa3;'
                        ];
                    } else {
                        if ($model->status == 'failed') {
                            return [
                                'style' => 'background: #ffaaaa;'
                            ];
                        }
                    }
                }
            },
            'columns'      => [
                'id',
                'operation',
                [
                    'attribute' => 'file_id',
                    'value'     => function ($model) {
                        return "" . $model->file_id . " (" . $model->file->name . ")";
                    }

                ],
                'created_at:datetime',
                'finished_at:datetime',
                'token',
                'status',
                [
                    'attribute' => 'result',
                    'format'    => 'raw',
                    'value'     => function ($model) {
                        $args = Json::decode($model->args);
                        $result = Json::decode($model->result);
                        return '
                        <div class="result-wrapper">
                        <a href="javascript:return false;" onclick="$(this).parent().find(\'.result-txt\').toggle();return false;">Show/hide args and result</a>
                        <div class="result-txt" style="display:none;">
                        Arguments:<br>' .
                            VarDumper::dumpAsString($args, 10, true) .
                            '<br>Result:<br>' .
                            VarDumper::dumpAsString($result, 10, true) .
                            '</div>
                     </div>';
                    }
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]
    ); ?>

</div>
