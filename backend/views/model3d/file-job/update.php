<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FileJob */

$this->title = 'Update File Job: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'File Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="file-job-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
