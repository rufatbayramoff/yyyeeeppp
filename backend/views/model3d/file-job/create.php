<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FileJob */

$this->title = 'Create File Job';
$this->params['breadcrumbs'][] = ['label' => 'File Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-job-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
