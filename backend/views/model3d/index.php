<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3ds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-index">

    <p>
        <?= Html::a('Create Model3d', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'user_id',
            'description',
            'print_instructions',
            'created_at:date',
             'updated_at:datetime',
              'published_at', 
              'is_printer_ready:boolean',
             'dimensions',
             'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
