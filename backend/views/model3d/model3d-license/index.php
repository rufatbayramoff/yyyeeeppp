<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dLicenseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3d Licenses';
$this->params['breadcrumbs'][] = $this->title;
$langs = common\models\SystemLang::find()->all();
echo $this->renderFile(Yii::getAlias('@backend/views/model3d/model3d-license/_topTabs.php'));
?>
<div class="model3d-license-index">

    <p>
        <?= Html::a('Create Model3d License', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

 <?php \yii\widgets\Pjax::begin(['options' => ['class' => 'pjax-wraper']]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',        
            'description',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(\common\models\Model3dLicense $model) use($langs){
                    $result = [];
                    $map = \lib\collection\CollectionDb::getMap($model->model3dLicenseIntls, 'lang_iso');
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->title : false;
                        if($val){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['model3d/license-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['model3d/license-intl/create']);
                        }
                    }
                    $result = Html::tag("h5", $model->title) .  implode("<br />", $result);
                    return $result;
                }
            ],
            [
                'attribute' => 'is_active',
                'format' => 'raw',
                'value' => function($model) {
                    $url = \yii\helpers\Url::toRoute(['/model3d/license/change-active', 'id' => $model->id]);
                    $val = $model->is_active ? 'Yes' : 'No';
                    $kVal = $model->is_active ? 'ok' : 'remove';
                    return Html::a(sprintf('<span class="glyphicon glyphicon-%s"></span> %s',$kVal, $val), $url, [
                            'title' => 'Change',
                            'data-pjax' => '0',
                            'class' => 'grid-action'
                    ]);
                }
            ], 

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>
</div>
