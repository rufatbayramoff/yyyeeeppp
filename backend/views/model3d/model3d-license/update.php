<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dLicense */

$this->title = 'Update Model3d License: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Model3d Licenses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="model3d-license-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
