<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dImg */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Model3d Imgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-img-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'model3d_id',
            'size',
            'title',
            'created_at',
            'updated_at',
            'deleted_at',
            'moderated_at',
            'is_moderated:boolean',
            'basename',
            'extension',
            'user_id',
            'type',
            'review_id',
            'file_id',
        ],
    ]) ?>

</div>
