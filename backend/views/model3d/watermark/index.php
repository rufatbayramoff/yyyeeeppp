<?php
/**
 * Date: 07.04.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
$this->title = 'Watermark Decoder';
?>


<?php
use yii\widgets\ActiveForm;

if(!empty($result)){
    echo '<code>';
    echo $result;
    echo '</code>';
}
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'file')->fileInput() ?>
    <?= $form->field($model, 'password')->textInput() ?>

    <button>Decode</button>

<?php ActiveForm::end() ?>