<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.01.18
 * Time: 16:13
 */

use common\models\SystemLangPage;
use yii\helpers\Html;

echo \kartik\grid\GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'columns'      => [
            'id',
            [
                'label'  => 'Url',
                'format' => 'raw',
                'value'  => function (SystemLangPage $model) {
                    $url = \Yii::$app->params['siteUrl'] . $model->url;
                    return HTML::a($url, $url, ['target'=>'_blank']);
                },
            ],
            [
                'label'  => 'Referer',
                'format' => 'raw',
                'value'  => function (SystemLangPage $model) {
                    return HTML::a($model->referer, $model->referer, ['target'=>'_blank']);
                }
            ],
            'created_at',
            'lang_iso',
            [
                'label' => 'User',
                'value' => function (SystemLangPage $model) {
                    return $model->user->username ?? '';
                }
            ],
            'isPost',
            'isJs',
            'getParams',
            'postParams'
        ]
    ]
);
