<?php

use common\models\SystemLangMessage;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SystemLangMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('app', 'Translation');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-message-index">

    <?php #echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Form JS dictionary files', ['form-js-files'], ['class' => 'btn btn-default', 'data-method' => 'post']) ?>
        <?= (new \backend\components\GridViewDataExporter($searchModel, 'Translation')) ?>
    </p>

    <?php
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'      => 'id',
            'format'         => 'raw',
            'value'          => 'id',
            'contentOptions' => ['style' => 'width: 90px;']
        ],
        [
            'attribute'      => 'language',
            'value'          => 'language',
            'contentOptions' => ['style' => 'width: 120px;'],
            'filter'         => Html::activeDropDownList(
                $searchModel,
                'language',
                \yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->asArray()->all(), 'iso_code', 'title'),
                ['class' => 'form-control', 'prompt' => 'All']
            ),
        ],
        [
            'attribute'      => 'is_auto_translate',
            'format'         => 'boolean',
            'contentOptions' => ['style' => 'width: 100px;'],
            'filter'         => Html::activeDropDownList(
                $searchModel,
                'is_auto_translate',
                ['1' => 'Yes', '0' => 'No'],
                ['class' => 'form-control', 'prompt' => 'All']
            ),
        ],
        [
            'label' => 'Auto translate',
            'value' => function ($model) {
                if ($model->is_auto_translate) {
                    return $model->translation;
                }
            }
        ],
        [
            'class'           => 'kartik\grid\EditableColumn',
            'attribute'       => 'translation',
            'value'           => function (\common\models\SystemLangMessage $model) {
                if ($model->is_auto_translate) {
                    $model->translation = null;
                }
                return $model->translation;
            },
            'editableOptions' => [
                'header'    => 'Translation',
                'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
            ]
        ],
        [
            'label'          => 'Found In Source',
            'format'         => 'raw',
            'contentOptions' => ['style' => 'width: 100px;'],
            'value'          => function ($model) {
                return $model->source ? ($model->source->not_found_in_source ? 'No' : 'Yes') : 'No source';
            },
            'filter'         => Html::activeDropDownList(
                $searchModel,
                'not_found_in_source',
                ['0' => 'Yes', '1' => 'No'],
                ['class' => 'form-control', 'prompt' => 'All']
            ),
        ],
        ['attribute' => 'sourceCategory', 'value' => 'source.category', 'contentOptions' => ['style' => 'width: 100px;'],],
        ['attribute' => 'sourceMessage', 'value' => 'source.message'],
        [
            'attribute' => 'usePages',
            'label'     => 'Use pages',
            'format'    => 'raw',
            'value'     => function ($model) {
                if (!$model->systemLangSource) {
                    return '';
                }
                $url = '/site/lang/pages-list?id=' . $model->systemLangSource->id;
                return '<a class="btn-link btn-ajax-modal" href="' . $url . '" value="' . $url . '" data-target="#systemLangPages" title="Pages list">' . $model->getUsagePagesCount() . '</a>';
            }
        ],
        [
                'attribute' => 'date',
                'value' => function (SystemLangMessage $model) {
                    return substr($model->systemLangSource->date,0, 10);
                }
        ],
        [
            'attribute'      => 'is_checked',
            'value'          => function (SystemLangMessage $model) {
                if (!$model->source) {
                    return 'No source';
                }
                $flag = $model->source->is_checked ? 'YES ' : 'No ';
                if ($model->source->is_checked) {
                    return $flag;
                }
                return $flag . Html::a('Checked', ['site/lang/checked', 'id' => $model->source->id], ['class' => 'btn  btn-primary', 'data-method' => 'post']);
            },
            'format'         => 'raw',
            'contentOptions' => ['style' => 'width: 100px;'],
            'filter'         => Html::activeDropDownList(
                $searchModel,
                'is_checked',
                ['1' => 'Yes', '0' => 'No'],
                ['class' => 'form-control', 'prompt' => 'All']
            ),
        ],
        'updated_at',
        [
            'label'     => 'Translated by',
            'format'    => 'raw',
            'attribute' => 'translated_by',
            'relation'  => 'translatedBy',
            'class'     => \backend\components\columns\UserColumn::class,
        ],
        ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
    ];
    echo \kartik\grid\GridView::widget(
        [
            'export'       => false,
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => $gridColumns
        ]
    );
    ?>

</div>
