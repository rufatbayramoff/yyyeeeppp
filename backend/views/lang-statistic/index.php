<?php

use backend\components\AdminAccess;
use common\modules\translation\helpers\TranslateAdminUrlHelper;
use common\modules\translation\models\search\TranslateStatisticSearch;
use common\modules\translation\models\TranslateStatistic;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel TranslateStatisticSearch */
/* @var $dataProvider \yii\data\ArrayDataProvider */
/* @var $translatorUsersList \common\models\User[] */

$this->title                   = Yii::t('app', 'Translation Statistics');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-message-index">
    <?php
    $form = ActiveForm::begin(['method' => 'get']);
    ?>
    <div class="row" style="padding-bottom: 20px;">
        <?php if (AdminAccess::can('website.translation_statistics')) { ?>
            <div class="col-md-2">
                <label>Select user</label>
                <?= Html::activeDropDownList(
                    $searchModel,
                    'userId',
                    $translatorUsersList,
                    ['class' => 'form-control', 'prompt' => 'All']
                ); ?>
            </div>
        <?php } ?>
        <div class="col-md-2">
            <?= $form->field($searchModel, 'dateFrom')->textInput(); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($searchModel, 'dateTo')->textInput(); ?>
        </div>

    </div>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php

    $form = ActiveForm::end();
    ?>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'columns'      => [
                'date',
                [
                        'label' => 'Translated words count',
                    'attribute' => 'tranlsatedCount',
                    'format'    => 'raw',
                    'value'     => function (TranslateStatistic $item) use ($searchModel) {
                        return '<a href="' . TranslateAdminUrlHelper::translated($item->date, $item->date, $searchModel->userId) . '" >' . $item->tranlsatedCount . '</a>';
                    }
                ],
                [
                    'label' => 'Translated Database words count',
                    'attribute' => 'tranlsatedDbCount',
                    'format'    => 'raw',
                    'value'     => function (TranslateStatistic $item) use ($searchModel) {
                        return '<a href="' . TranslateAdminUrlHelper::translatedDb($item->date, $item->date, $searchModel->userId) . '" >' . $item->tranlsatedDbCount . '</a>';
                    }
                ]
            ]
        ]
    );
    ?>
</div>
