<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SystemLangMessage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Lang Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-message-view">

    <h1><?= \H($this->title) ?></h1>



    <?= DetailView::widget(
        [
            'model'      => $model,
            'attributes' => [
                'id',
                'language',
                'category' => [
                    'label' => 'Category',
                    'value' =>  $model->getSource()->one()->category

                ],
                'message' =>[
                    'label' => 'Message',
                    'value' =>  $model->getSource()->one()->message
                ],
                'translation:ntext',
                'not_found_in_source' => [
                    'label' => 'Not found in source',
                    'value' =>  $model->getSource()->one()->not_found_in_source
                ],
            ],
        ]
    ) ?>

</div>
