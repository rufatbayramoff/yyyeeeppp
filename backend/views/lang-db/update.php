<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $i18nTableElement \common\modules\translation\models\I18nTableElement */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Db Lang Message',
]) . ' ' . $i18nTableElement->getId();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'DB Lang Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="system-lang-message-update">

    <h1><?= \H($this->title)?></h1>


    <?= DetailView::widget(
        [
            'model'      => $i18nTableElement,
            'attributes' => [
                'tableName',
                'modelId',
                'fieldName',
                'langIso',
                'valueOrig:raw'
            ],
        ]
    ) ?>


    <div class="system-lang-message-form">

        <?php $form = ActiveForm::begin(); ?>

        <?php
        if (Yii::$app->getModule('translation')->dbI18n->isWysiwygEditor($i18nTableElement)) {
            echo $form->field($i18nTableElement, 'fieldValue')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults());
        } else {
            echo $form->field($i18nTableElement, 'fieldValue')->textarea(['rows'=>6]);
        }
        ?>
        <?= $form->field($i18nTableElement,'isActive')->checkbox();?>

        <input type="hidden" name="prevUrl" value="<?=$prevUrl?>">

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Back'), $prevUrl, ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
