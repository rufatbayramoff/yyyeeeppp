<?php

use common\components\ArrayHelper;
use common\modules\translation\models\I18nTableElement;
use common\modules\translation\models\search\I18nTableElementSearch;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\translation\models\search\I18nTableElementSearch */
/* @var $dataProvider \yii\data\ArrayDataProvider */
/* @var $isShortForm bool */

$this->title                   = Yii::t('app', 'Translation DB') . ($isShortForm ? ' - ' . $searchModel->tableName . ' line ' . $searchModel->modelId : '');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(
    [
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal'
    ]
); ?>
<div class="row">
    <div class="col-md-3">
        <?= $form->field($searchModel, 'emptyFieldValue')->dropDownList(['' => 'All', I18nTableElementSearch::EMPTY_FIELD_VALUE => 'Only empty',]) ?>
    </div>
    <div class="col-md-1">
        <input type="submit" value="Search" class="btn btn-primary searchModel-btn"/>
    </div>
    <div class="col-sm-1">
        <?= (new \backend\components\GridViewDataExporter(null, 'TranslationDB', 'ReportCsvWriter')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<div class="system-lang-message-index">
    <?php
    $gridColumns = [
        ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        [
            'attribute' => 'langIso',
            'value'     => 'langIso',
            'filter'    => Html::activeDropDownList(
                $searchModel,
                'langIso',
                \yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->where(['<>', 'iso_code', 'en-US'])->asArray()->all(), 'iso_code', 'title'),
                ['class' => 'form-control', 'prompt' => 'All']
            ),
        ],
    ];
    if ($isShortForm) {

    } else {
        $gridColumns = array_merge(
            $gridColumns,
            [
                [
                    'attribute' => 'tableName',
                    'value'     => 'tableName',
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'tableName',
                        Yii::$app->getModule('translation')->dbI18n->getI18nTablesList(),
                        ['class' => 'form-control']
                    ),
                ],
                [
                    'attribute' => 'modelId',
                    'value'     => 'modelId',
                    'filter'    => HTML::activeTextInput($searchModel, 'modelId', ['class' => 'form-control', 'prompt' => 'All']),
                ],
            ]
        );
    }
    $gridColumns = array_merge(
        $gridColumns,
        [
            [
                'attribute' => 'fieldName',
                'value'     => 'fieldName',
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'fieldName',
                    Yii::$app->getModule('translation')->dbI18n->getI18nFields($searchModel->tableName),
                    ['class' => 'form-control', 'prompt' => 'All']
                ),
            ],
            [
                'attribute' => 'fieldValueOrig',
                'value'     => function (I18nTableElement $obj) {
                    return mb_substr(strip_tags($obj->getValueOrig()), 0, 500);
                },
                'filter'    => HTML::activeTextInput($searchModel, 'fieldValueOrig', ['class' => 'form-control', 'prompt' => 'All']),
                'format'    => 'raw',
            ],
            [
                'attribute' => 'fieldValue',
                'value'     => function (I18nTableElement $obj) {
                    return mb_substr(strip_tags($obj->fieldValue), 0, 500);
                },
                'filter'    => HTML::activeTextInput($searchModel, 'fieldValue', ['class' => 'form-control', 'prompt' => 'All']),
                'format'    => 'raw'
            ],
            'updatedAt',
            [
                'attribute' => 'translatedBy',
                'relation'  => 'translatedByUser',
                'label'     => 'Translated by',
                'format'    => 'raw',
                'filter'    => Html::activeTextInput($searchModel, 'translatedBy', ['class' => 'form-control']),
                'class'     => \backend\components\columns\UserColumn::class,
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}']
        ]
    );
    echo GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => $gridColumns
        ]
    );
    ?>

</div>
