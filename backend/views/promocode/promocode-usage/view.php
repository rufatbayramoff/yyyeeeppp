<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PromocodeUsage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Promocode Usages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-usage-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'promocode_id',
            'used_at',
            [
                'label' => 'Order Id',
                'value' => function (\common\models\PromocodeUsage $model) {
                    return $model->invoice->store_order_id;
                }
            ],
            'amount',
            'amount_currency',
        ],
    ]) ?>

</div>
