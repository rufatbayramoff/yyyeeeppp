<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PromocodeUsageSearch */
/* @var $form yii\widgets\ActiveForm */
if(!$model->startDate){
   # $model->startDate = date("Y-m-d", strtotime("-1 month"));
    #$model->endDate = date("Y-m-d");
}

?>

<div class="promocode-usage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>


    <?= $form->field($model, 'promocode_id')->widget(\kartik\select2\Select2::className(), [
        'name' => 'PromocodeUsageSearch[promocode_id]',
        'initValueText' => $model->promocode?$model->promocode->code:'',
        'pluginOptions' => [
            'minimumInputLength' => 1,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['promocode/promocode/list']),
                'dataType' => 'json',
                'data' => new yii\web\JsExpression('function(params) { return {q:params.term, page:params.page, k:"id,code"}; }')
            ],
        ],
    ]) ?>

    <?php echo $form->field($model, 'order_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?php
    $filterDateConfig = [
        'convertFormat'=>false,
        'pluginOptions'=>[
            'locale' => [
                'format'=>'Y-M-D',
            ],
            'separator'=> ' - ',
        ]
    ];
    if(!empty($model->startDate)){
        $filterDateConfig['startAttribute'] = 'startDate';
        $filterDateConfig['endAttribute'] =  'endDate';
    }
    echo $form->field($model, 'filterDate')->widget(\kartik\daterange\DateRangePicker::className(), $filterDateConfig); ?>

    <?php echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'amount_currency') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <a href="<?=\yii\helpers\Url::toRoute(['/promocode/promocode-usage']);?>" class="btn btn-default btn-sm">Clear filters</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
