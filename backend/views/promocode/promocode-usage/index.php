<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PromocodeUsageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promocode Usages';
$this->params['breadcrumbs'][] = $this->title;
$details = [
    'totalOrder' => 0,
    'fee' => 0,
    'discount' => 0
];
?>
<div class="promocode-usage-index row">

    <div class="col-lg-10">
        <div class="box box-solid box-default">
            <div class="box-header"></div> <div class="box-body">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            'id',
            'user_id',
            [
                'attribute' => 'promocode_id',
                'format' => 'raw',
                'value' => function($model){
                    $link =  Html::a($model->promocode->code, ['promocode/promocode/view', 'id'=>$model->promocode_id],
                        ['target'=>'_blank', 'class'=>'btn']);
                    $txt = ''; //'<br /><span>' . $model->promocode->description . '</span>';
                    return $link . $txt;
                }
            ],
            [
                'attribute' => 'promocode_info',
                'format' => 'raw',
                'value' => function(\common\models\PromocodeUsage $model){
                    $prom = $model->promocode;
                    if($prom->discount_type==\common\models\Promocode::DISCOUNT_TYPE_FIXED){
                        $price =  displayAsCurrency($prom->discount_amount, $prom->discount_currency);
                    }else{
                        $price = $prom->discount_amount . '%';
                    }
                    return $price;
                }
            ],
            'promocode.usage_type',
            [
                'attribute' => 'discount_for',
                'format' => 'raw',
                'value' => function(\common\models\PromocodeUsage $model) {
                    return  $model->promocode->discount_for;
                }
            ],
            'used_at:datetime',
            [
                'label' => 'Order Id',
                'format' => 'raw',
                'value' => function (\common\models\PromocodeUsage $model) {
                    return Html::a($model->invoice->store_order_id, ['store/store-order/view', 'id'=> $model->invoice->store_order_id], ['target'=>'_blank', 'class'=>'btn btn-default']);
                }
            ],
            [
                'attribute' => 'order_amount',
                'value' => function(\common\models\PromocodeUsage $model) use(&$details){
                    $details['totalOrder'] += $model->invoice->getAmountTotal()->getAmount();
                    return displayAsMoney($model->invoice->getAmountTotal());
                }
            ],
            [
                'attribute' => 'our_order_fee',
                'value' => function(\common\models\PromocodeUsage $model) use(&$details){
                    $pricer = $model->invoice->getAmountTreatstockFeeWithRefund();

                    if ($pricer) {
                        $details['fee'] += $pricer->getAmount();
                        return displayAsMoney($pricer);
                    }

                    return null;
                }
            ],
            [
                'attribute' => 'discount_amount',
                'value' => function(\common\models\PromocodeUsage $model) use (&$details){
                    $details['discount'] += $model->getAmountTotal()->getAmount();
                    return displayAsMoney($model->getAmountTotal());
                }
            ]

        ],
    ]);
                ?>   </div>
        </div>
    </div>

    <div class="col-lg-2 ">
        <div class="box box-solid box-success">
            <div class="box-header">Search
            </div> <div class="box-body">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>

        <div class="box box-solid box-warning">
            <div class="box-header">Usage Report Details
            </div>
            <div class="box-body">

                <table class="table table-condensed">
                    <tbody><tr>
                        <th style=""> </th>
                        <th style=""></th>
                    </tr>
                    <tr>
                        <td> Total orders price:</td>
                        <td>
                            <?=$details['totalOrder']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td> Total orders fee:
                        </td>
                        <td>
                            <?=$details['fee']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>   Total discount: </td>
                        <td>
                            <?=$details['discount']; ?>
                        </td>
                    </tr>
                    </tbody>
                </table>




            </div>
        </div>


    </div>
</div>
