<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PromocodeHistory */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Promocode Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-history-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'promocode_id',
            'created_at',
            'action_id',
            'comment',
            'user_id',
        ],
    ]) ?>

</div>
