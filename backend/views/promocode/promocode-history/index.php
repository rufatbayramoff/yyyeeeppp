<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PromocodeHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promocode Histories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-history-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'promocode_id',
            'created_at',
            'action_id',
            'comment',
             'user_id',
        ],
    ]); ?>
</div>
