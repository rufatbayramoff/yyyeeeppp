<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Promocode */

$this->title = 'Create Promo code';
$this->params['breadcrumbs'][] = ['label' => 'Promo codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
