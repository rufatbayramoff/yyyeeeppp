<?php

use common\models\Promocode;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PromocodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promocodes';
$this->params['breadcrumbs'][] = $this->title;
$promoValidator = new \common\modules\promocode\components\PromocodeUsageValidator();
?>
<div class="promocode-index">


    <p>
        <?= Html::a('Create Promocode', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'rowOptions' => function ($model, $index, $widget, $grid) use($promoValidator){
                $toValidate = true;
                if($model->usage_type== Promocode::USAGE_TYPE_ONE_PER_USER){
                    $toValidate = false;
                    if(!$model->is_active){
                        $toValidate = true;
                    }
                }
                if($toValidate && !$promoValidator->validate($model)){
                    $model->is_valid = false;
                    return [
                        'style'=>'background: #ffdfaf;color:#aaa!important;'
                    ];
                }
            },
            'columns'      => [
                'id',
                'code',
                'usage_type',
                'discount_type',
                'discount_amount',
                'discount_currency',
                'discount_for',
                'valid_from',
                'valid_to',
                'description',
               # 'is_active:boolean',
                [
                    'attribute' => 'is_active',
                    'value'=>function ($model) {
                        return $model->is_active?'Yes':'No';
                    },
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'is_active',
                        [1=>'Yes', 0=>'No'],
                        ['class'=>'form-control','prompt' => 'All']),
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'visibleButtons' => [
                        'delete' => false
                    ]
                ],
            ],
        ]
    ); ?>
</div>
