<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Promocode */

$this->title = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Promo codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'usage_type',
            'discount_type',
            'discount_amount',
            'discount_currency',
            'discount_for',
            'valid_to',
            'description',
            'order_total_from',
            'is_active:boolean',
            'is_valid:boolean',
        ],
    ]) ?>
    <div class="row">
        <div class="col-lg-6">

    <h2>History</h2>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => \common\models\PromocodeHistory::getDataProvider(['promocode_id'=>$model->id]),
        'columns' => [
            'id',
            'created_at:datetime',
            'action_id',
            'comment',
            'user.username',
        ],
    ]); ?>
            </div>
            <div class="col-lg-6">
    <h2>Usage</h2>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => \common\models\PromocodeUsage::getDataProvider(['promocode_id'=>$model->id]),
        'columns' => [
            'id',
            'user_id',
            'used_at:datetime',
            [
                'label' => 'Order Id',
                'value' => function (\common\models\PromocodeUsage $model) {
                    return $model->invoice->store_order_id;
                }
            ],
            'amount',
            'amount_currency',
        ],
    ]); ?>
        </div>
    </div>

</div>
