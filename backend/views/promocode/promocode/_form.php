<?php

use kartik\select2\Select2;
use lib\money\Currency;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Promocode */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->discount_currency = Currency::USD;
    $model->order_total_from = 0;
}
?>

<div class="promocode-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usage_type')->dropDownList($model->getUsageTypes());?>
    <?= $form->field($model, 'discount_for')->dropDownList($model->getDiscountsFor());?>
    <?= $form->field($model, 'discount_type')->dropDownList($model->getDiscountTypes());?>
    <?= $form->field($model, 'discount_amount')->textInput() ?>
    <?= $form->field($model, 'discount_currency')->widget(Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\PaymentCurrency::find()->where(['is_active'=>1])->asArray()->all(), 'currency_iso', 'title' ),
        'options' => ['placeholder' => 'Select', 'style' => 'width:100%'],
    ]);?>
    <?= $form->field($model, 'valid_from')->widget(\kartik\widgets\DatePicker::className(), [
        'pluginOptions' => [
            'format' => 'yyyy-m-d',
        ]
    ]) ?>
    <?= $form->field($model, 'valid_to')->widget(\kartik\widgets\DatePicker::className(), [
        'pluginOptions' => [
          'format' => 'yyyy-m-d',
        ]
    ]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_total_from')->textInput()->label('With Discount For from (USD)') ?>
    <span><p>In USD. If order price for print 15$ and with price fee from is 20$, promo cannot be used. If 0.00 - no price limits to use.</p></span>
    <?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
