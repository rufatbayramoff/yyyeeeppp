<?php
/**
 * User: nabi
 */

$this->registerJsFile('/js/jquery-sortable-min.js');
$this->title = 'Import';

$ps = new \common\models\Ps();
?>

<?php
echo \backend\widgets\ExcelImportWidget::widget(['tableName'=> 'ps']); ?>
<div class="row">
    <div class="col-lg-8">
        <h3>Map Columns to import</h3>
        <div class="row">
            <div class="col-lg-6">
                <h4>Columns from Excel</h4>
                <ol class="simple_with_drop vertical">
                    <li>
                        <i class="icon-move"></i>
                        Item 2
                    </li>
                    <li>
                        <i class="icon-move"></i>
                        Item 3
                    </li>
                    <li>
                        <i class="icon-move"></i>
                        Item 4
                    </li>
                    <li>
                        <i class="icon-move"></i>
                        Item 5
                    </li><li class="" style="">
                        <i class="icon-move"></i>
                        Item 1
                    </li>
                    <li>
                        <i class="icon-move"></i>
                        Item 6
                    </li>
                </ol>
            </div>
            <div class="col-lg-6">
                <h4>In Database</h4>
                <ol class="simple_with_drop vertical">
                    <?php foreach($ps->attributeLabels() as $attributeLabel): ?>
                    <li>
                        <i class="icon-move"></i>
                        <?=$attributeLabel;?>
                    </li>
                    <?php endforeach; ?>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs('$("ol.simple_with_drop").sortable({
});');
?>
<style>
    body.dragging, body.dragging * {
        cursor: move !important; }

    /* line 4, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    .dragged {
        position: absolute;
        top: 0;
        opacity: 0.5;
        z-index: 2000; }

    /* line 10, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical {
        margin: 0 0 9px 0;
        min-height: 10px; }
    /* line 13, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical li {
        display: block;
        margin: 5px;
        padding: 5px;
        border: 1px solid #cccccc;
        color: #0088cc;
        background: #eeeeee; }
    /* line 20, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical li.placeholder {
        position: relative;
        margin: 0;
        padding: 0;
        border: none; }
    /* line 25, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical li.placeholder:before {
        position: absolute;
        content: "";
        width: 0;
        height: 0;
        margin-top: -5px;
        left: -5px;
        top: -4px;
        border: 5px solid transparent;
        border-left-color: red;
        border-right: none; }
</style>
