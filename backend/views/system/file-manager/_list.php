<?php
/**
 * User: nabi
 */

use yii\helpers\Html;

?>

<div class="col-lg-3">
    <div class="box">
        <div class="box-header with-border">
            <b ><?=$model['pathinfo']['basename'];?></b>
            <div class="box-tools pull-right">
                <a  href="/system/file-manager/delete?filename=<?=$model['pathinfo']['basename'];?>" class="btn btn-warning btn-xs">delete</a>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body img-size">
            <?php
            echo Html::img($model['url']);
            ?>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <?php
            echo sprintf('File size: %s, Date: %s', app('formatter')->asShortSize($model['size']), app('formatter')->asDatetime($model['filemtime']));
            ?>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</div>