<?php
/**
 * User: nabi
 */

use yii\bootstrap\ActiveForm;

$this->title = 'Images';

$provider = new \yii\data\ArrayDataProvider([
    'allModels' => $files,
    'pagination' => [
        'pageSize' => 48,
    ],
    'sort' => [
        'attributes' => ['size', 'filemtime'],
    ],
]);


?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'layout'=>'inline']) ?>

<?= $form->field($uploadForm, 'imageFiles[]')->fileInput(['multiple' => true]) ?>

<button>Upload images</button>
<?php ActiveForm::end() ?>
<div class="pull-right">
<form>Search: <input type="text" name="search" value="<?=H(app('request')->get('search'));?>" />
<button>OK</button>
    <a href="/system/file-manager">Reset</a>
</form>
</div>
<div class="system-file-manager" style="margin-top:10px;">
<?php
echo \yii\widgets\ListView::widget([
    'layout' => "{sorter}{summary}\n{items}\n<br class='clearfix'/>{pager}",
    'dataProvider' => $provider,
    'itemView' => '_list',
]);
?>
</div>
<br clear="all" />
<style>
    .img-size img{
        max-width: 250px;
        height: 150px;
    }
    ul.sorter li{
        display: inline-block;
    }
    ul.sorter a{
        margin: 10px; padding: 4px;
        border: 1px solid #afafaf;
    }
</style>