<?php

/* @var $this \yii\web\View */
/* @var $model \backend\models\system\ExcelImportForm */

$this->title = 'Migrate categories and products';
?>


<h2>New categories</h2>
<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$form = ActiveForm::begin([
    'action' => '/system/migrate-categories-products/categories',
    'options' => ['enctype' => "multipart/form-data",],
    'layout'  => 'inline'
]);
?>

<?= $form->field($model, 'file')->fileInput() ?>
<?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>


<br />
<br />
<br />
<h2>Move products</h2>

<?php


$form = ActiveForm::begin([
    'action' => '/system/migrate-categories-products/products',
    'options' => ['enctype' => "multipart/form-data",],
    'layout'  => 'inline'
]);
?>

<?= $form->field($model, 'file')->fileInput() ?>
<?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>

<br />
<br />
<br />
<br />
<a href="/system/migrate-categories-products/refresh">REFERSH PRODUCTS & CATEGRORIES</a>

