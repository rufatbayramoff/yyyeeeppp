<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.05.17
 * Time: 10:41
 */

/* @var $this yii\web\View */
/* @var $statisticsCache \common\models\StatisticsCache */

$this->title = 'Clear selected cache';

?>

<?php

foreach ($cacheTypes as $typeCode => $typeDescription) {
    echo '<a href="/site/cache/clear?type=' . $typeCode . '" class="btn btn-default">' . $typeDescription . "</a><br><br />\n";
}

echo sprintf(
    "
<b>APC Cache:</b> 
Hits: <span class='label label-info'>%d</span> 
Misses: <span class='label label-info'>%d</span>
Inserts: <span class='label label-info'>%d</span>
",
    $apcInfo['num_hits'],
    $apcInfo['num_misses'],
    $apcInfo['num_inserts']
);
