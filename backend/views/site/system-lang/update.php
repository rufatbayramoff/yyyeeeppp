<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SystemLang */

$this->title = 'Update System Lang: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'System Langs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="system-lang-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
