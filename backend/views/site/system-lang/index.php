<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SystemLangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'System Langs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-index">

    <p>
        <?= Html::a('Create System Lang', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Form new lang translations', ['scan-source-files'], ['class' => 'btn btn-default', 'data-method' => 'post']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'priority',
            'title',
            'title_original',
            'iso_code',
            'is_active:boolean',
            'is_widget_active:boolean',
            // 'url:url',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
