<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GeoCountry */

$this->params['breadcrumbs'][] = ['label' => 'Geo Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="geo-country-update">

    <h1><?= \H($this->title) ?></h1>
    <div class="row">
        <div class="col-lg-4">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
        <div class="col-lg-8">
            <h3>Top Country Cities</h3>
            <?= \yii\grid\GridView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'columns'      => [
                        'id',
                        [
                            'label' => 'Title',
                            'value' => function (\common\models\GeoTopCity $geoTopCity) {
                                return ($geoTopCity->city->region->title ? $geoTopCity->city->region->title . ' / ' : '') . $geoTopCity->city->title;
                            }
                        ],
                        [
                            'class'      => 'yii\grid\ActionColumn',
                            'controller' => '/site/geo-country',
                            'buttons'    => [
                                'delete' => function ($url, $topModel, $key) use ($model) {
                                    return Html::a('Delete',
                                        ['/site/geo-country/delete-city', 'countryId' => $model->id, 'deleteCity' => $topModel->id]);
                                }
                            ],
                            'template'   => '{delete}'
                        ],
                    ],
                ]
            ); ?>

            <h3>Add City</h3>
            <div class="geo-top-city-form">

                <?php $form           = ActiveForm::begin();
                $topModel->country_id = $model->id; ?>

                <?php
                echo $form->field($topModel, 'city_id')->widget(kartik\select2\Select2::classname(), [
                    'initValueText' => '',
                    'options'       => ['placeholder' => 'Search'],
                    'pluginOptions' => [
                        'allowClear'         => true,
                        'minimumInputLength' => 2,
                        'ajax'               => [
                            'url'      => \yii\helpers\Url::to(['/site/geo-country/city-list?country_id=' . $model->id]),
                            'dataType' => 'json',
                            'data'     => new yii\web\JsExpression('function(params) { return {q:params.term, page:params.page}; }')
                        ],
                    ],
                ]);
                ?>
                <?= $form->field($topModel, 'country_id')->hiddenInput()->label(false); ?>

                <div class="form-group">
                    <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>

        </div>
    </div>
</div>
