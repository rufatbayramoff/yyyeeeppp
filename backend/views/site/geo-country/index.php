<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\GeoCountrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Geo Countries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-country-index">

 
    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            'iso_code',
            'phone_code',
            'iso_alpha3',
             'currency_code',
             'currency_name',
             'currency_symbol',
             'is_easypost_intl:boolean',
             'is_europa:boolean',
             'is_easypost_domestic:boolean',
             'region_required:boolean',
             'paypal_payout:boolean',
             'is_bank_transfer_payout:boolean',

            ['class' => 'yii\grid\ActionColumn', 
                'buttons'=> [
                    'delete'=>function(){return false;}
                ]
            ],
        ],
    ]); ?>
</div>
