<?php

use backend\widgets\FilesListWidget;
use common\components\ArrayHelper;
use common\components\FileTypesHelper;
use common\models\BizIndustry;
use common\models\PrinterMaterial;
use common\models\WikiMaterial;
use common\models\WikiMaterialFeature;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterial */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="wiki-material-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">Photos</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'formPrefix'             => $model->formName(),
                    'formAttribute'          => 'photos',
                    'filesList'              => $model->wikiMaterialPhotoFiles,
                    'rights'                 => [
                        FilesListWidget::ALLOW_DELETE,
                        FilesListWidget::ALLOW_ADD,
                        FilesListWidget::ALLOW_MULTIPLE,
                        FilesListWidget::ALLOW_ROTATE
                    ],
                    'filesAllowedExtensions' => FileTypesHelper::ALLOW_IMAGES_EXTENSIONS
                ]
            ) ?>
        </div>
    </div>

    <?= $form->field($model, 'about')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>

    <?= $form->field($model, 'description')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>

    <div class="row">
        <div class="col-md-12">
            <h3>Widget</h3>
            <div style="padding: 30px;">
            <?= $form->field($model, 'widget_about')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>
            <?= $form->field($model, 'widget_advantages')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>
            <?= $form->field($model, 'widget_disadvantages')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'type_of_manufacturer')->dropDownList(WikiMaterial::TYPE_OF_MANUFACTURER_LIST) ?>

    <?= $form->field($model, 'industryIds')->checkboxList(ArrayHelper::map(BizIndustry::find()->all(), 'id', 'title')) ?>

    <?= $form->field($model, 'featureIds')->checkboxList(ArrayHelper::map(WikiMaterialFeature::find()->all(), 'id', 'title')) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>
    <?= $form->field($model, 'show_widget')->checkbox(['label' => 'Show only widget']) ?>


    <?php
    if ($model->id) {
        ?>
        <div class="form-group field-storeorderreview-comment">
            <label class="control-label" for="storeorderreview-comment">Properties</label>
            <div>
                <?= GridView::widget(
                    [
                        'dataProvider' => $dataProvider = new ActiveDataProvider(['query' => $model->getWikiMaterialProperties()]),
                        'columns'      => [
                            'title',
                            'value',
                            'comment',
                            [
                                'class'   => ActionColumn::class,
                                'buttons' =>
                                    array_merge(
                                        Yii::$app->getModule('translation')->dbI18n->getGridButton('wiki_material_properties'),
                                        [
                                            'update' => function ($url, $model) {
                                                $urlRes = Html::a(
                                                    '<span class="glyphicon glyphicon-pencil"></span>',
                                                    '/site/wiki-material-properties/update?id=' . $model->id,
                                                    [
                                                        'class'       => 'btn-ajax-modal',
                                                        'title'       => 'Update',
                                                        'data-target' => '#addMaterial',
                                                        'value'       => '/site/wiki-material-properties/update?id=' . $model->id,
                                                    ]
                                                );
                                                return $urlRes;
                                            },
                                            'delete' => function ($url, $model) {
                                                $urlRes = Html::a(
                                                    '<span class="glyphicon glyphicon-trash"></span>',
                                                    '/site/wiki-material-properties/delete?id=' . $model->id,
                                                    [
                                                        'title'        => 'Delete',
                                                        'data-confirm' => 'Are you sure you want to delete this item?',
                                                        'data-method'  => 'post'
                                                    ]
                                                );
                                                return $urlRes;
                                            }
                                        ]
                                    ),

                                'template' => '{translate} &nbsp; {update} &nbsp; {delete} ',
                            ]
                        ]
                    ]
                );
                ?>
                <?= Html::button(
                    'Add property',
                    [
                        'class'       => 'btn btn-success btn-ajax-modal',
                        'title'       => 'Add property',
                        'data-target' => '#addMaterial',
                        'value'       => '/site/wiki-material-properties/create?wikiMaterialId=' . $model->id
                    ]
                ) ?>
            </div>
        </div>
        <a id="printerMaterialProperties" name="printerMaterialProperties"></a>
        <script>
            if (window.location.href.indexOf('printerMaterialProperties') !== -1) {
                $(document).ready(function () {
                    setTimeout(function () {
                        window.location.href = window.location.href;
                    }, 1000);

                });
            }
        </script>
        <?php
    }
    ?>

    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">Files</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'formPrefix'             => $model->formName(),
                    'formAttribute'          => 'files',
                    'filesList'              => $model->wikiMaterialFileFiles,
                    'rights'                 => [
                        FilesListWidget::ALLOW_DELETE,
                        FilesListWidget::ALLOW_ADD,
                        FilesListWidget::ALLOW_MULTIPLE,
                    ],
                    'filesAllowedExtensions' => FileTypesHelper::ALLOW_DOCS_EXTENSIONS
                ]
            ) ?>
        </div>
    </div>


    <?= $form->field($model, 'printerMaterialIds')->widget(
        Select2::classname(),
        [
            'data'          => ArrayHelper::map(PrinterMaterial::find()->active()->all(), 'id', 'title'),
            'options'       => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple'   => true
            ],
        ]
    )->label('Printer materials'); ?>

    <?= $form->field($model, 'wikiMaterialRelatedIds')->widget(
        Select2::classname(),
        [
            'data'          => ArrayHelper::map(WikiMaterial::find()->where(['and', ['!=', 'id', $model->id], ['is_active' => 1]])->all(), 'id', 'title'),
            'options'       => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple'   => true
            ],
        ]
    )->label('Wiki material related'); ?>

    <?= $form->field($model, 'geoCityIds')->widget(
        Select2::classname(),
        [
            'data'          => ArrayHelper::map($model->geoCities, 'id', 'fullTitle'),
            'options'       => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear'         => true,
                'multiple'           => true,
                'minimumInputLength' => 3,
                'ajax'               => [
                    'url' => '/site/search-city',
                ]
            ],
        ]
    )->label('Geo cities'); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
