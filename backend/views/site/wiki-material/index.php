<?php

use common\models\WikiMaterial;
use kartik\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WikiMaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wiki Materials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-material-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Wiki Material', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                'code',
                'title',
                [
                    'attribute' => 'about',
                    'format'    => 'raw',
                    'value'     => function (WikiMaterial $wikiMaterial) {
                        return mb_substr(strip_tags($wikiMaterial->about), 0, 500);
                    }
                ],
                [
                    'attribute' => 'description',
                    'format'    => 'raw',
                    'value'     => function (WikiMaterial $wikiMaterial) {
                        return mb_substr(strip_tags($wikiMaterial->description), 0, 500);
                    }
                ],
                [
                    'class'          => ActionColumn::class,
                    'buttons'        => Yii::$app->getModule('translation')->dbI18n->getGridButton('wiki_material'),
                    'template'       => '{translate} &nbsp; {update} &nbsp; {delete} ',
                    'contentOptions' => ['style' => 'width: 80px;']
                ],  [
                    'attribute' => 'is_active',
                    'format' => 'boolean',
                    'filter' =>  Html::activeDropDownList(
                        $searchModel,
                        'is_active',
                        [1=>'Yes', 0=>'No'],
                        ['class'=>'form-control','prompt' => 'All']),
                ],

            ],
        ]
    ); ?>
</div>
