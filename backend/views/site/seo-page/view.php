<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SeoPage */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Seo Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-page-view">



    <div class="row">
        <div class="col-lg-4">
            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'url:url',
            'title',
            'header',
            'meta_description',
            'meta_keywords',
            'header_text:ntext',
            'footer_text:ntext',
            'is_active:boolean',
            'is_sitemap:boolean',
            'updated_at',
            'seoPageAutofill.template.id',
            'seoPageAutofill.template.type',
            'seoPageAutofill.template.template_name'
        ],
    ]) ?>

        </div>
        <div class="col-lg-8">
        <h3>Translations</h3>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => \common\models\SeoPageIntl::getDataProvider(['model_id'=>$model->id]),
        'columns' => [
            'id',
            'model_id',
            [
                'attribute' => 'seo_page.url',
                'format' => 'url',
                'value' => function($seoIntl){
                    return param('server') . '/' . $seoIntl->model->url;
                }
            ],
            'lang_iso',
            'title',
            'header',
            // 'meta_description',
            // 'meta_keywords',
            // 'header_text:ntext',
            // 'footer_text:ntext',
            'is_active:boolean',
            'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


        </div>
    </div>
</div>
