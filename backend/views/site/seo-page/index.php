<?php

use backend\components\GridViewDataExporter;
use backend\widgets\ExcelImportWidget;
use yii\bootstrap\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SeoPageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $templateApplyForm \backend\models\seo\SeoPageTemplateApplyForm */
$this->title = 'Seo Pages';
$this->params['breadcrumbs'][] = $this->title;
$langs = common\models\SystemLang::find()->all();
echo $this->renderFile(Yii::getAlias('@backend/views/site/seo-page/_topTabs.php'));

?>
<div class="seo-page-index" >
    <div class="">
        <?= Html::a('Create Seo Page', ['create'], ['class' => 'btn btn-success']) ?>

        <?= Html::a('Need Reviews', ['index', 'SeoPageSearch[need_review]'=>1], ['class' => 'btn btn-warning']) ?>
    </div>


    <div class="row" style="border-top: 1px solid white;margin-top:5px;padding-top:10px;">
        <div class="col-lg-5">
            <?php $form = \yii\widgets\ActiveForm::begin();?>
                <input type="hidden" value="" name="SeoPageActionsForm[submitAction]" id="submitAction"/>
                <input type="hidden" value="" name="SeoPageActionsForm[ids]" id="selectedIdsAction" />
                <?php foreach($seoPageActions as $k=>$v): ?>
                    <div class="btn-group js-submit">
                        <?php $first = $v[0]; ?>
                        <a href="#" data-submit="<?=$first['action'];?>" class="btn btn-sm btn-default"><span class="<?=$first['icon'];?>"></span> <?=$first['title'];?></a>
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <?php foreach($v as $k2=>$v2): if($k2===0) continue; ?>
                                <li><a href="#" data-submit="<?=$v2['action'];?>"  ><span class="<?=$v2['icon'];?>"></span><?=$v2['title'];?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endforeach; ?>

            <?php \yii\widgets\ActiveForm::end();?>
        </div>
        <div class="col-lg-2"></div>
        <div class="col-lg-5">
            <?php $form = \yii\widgets\ActiveForm::begin();?>
            <input type="hidden" value="" name="SeoPageTemplateApplyForm[ids]" id="selectedIdsApply" />
            <div class="row">
                <div class="col-lg-3">
                    Apply To:
                    <?= Html::activeDropDownList(
                        $templateApplyForm,
                        'apply_type',
                         $templateApplyForm->getApplyTypes(),
                        ['class' => 'form-control']
                    ); ?>
                </div>
                <div class="col-lg-9">
                    Select Template
                    <div class="input-group">
                        <?php
                        $templateApplyForm->template_id = 0;

                        echo Html::activeDropDownList(
                            $templateApplyForm,
                            'template_id',
                            $templateApplyForm->getAutofillTemplates(),
                            ['class' => 'form-control']
                        ); ?>

                        <span class="input-group-btn">
                            <?= Html::submitButton('Apply', ['class' => 'btn btn-default']); ?>
                        </span>
                    </div>
                </div>
            </div>
            <?php \yii\widgets\ActiveForm::end();?>

        </div>
    </div>
    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
                                        new GridViewDataExporter($searchModel) ,
                                        ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function (\common\models\SeoPage $model, $index, $widget, $grid){
            if($model->need_review){
                return [
                    'style'=>'background: #e2e2ff;'
                ];
            }
            if($model->is_locked){
                return [
                    'style'=>'background: #ffdfdf;'
                ];
            }
            if(!$model->is_active){
                return [
                    'style'=>'color:rgba(1,1,1,0.3);'
                ];
            }
        },
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'cssClass' => 'checkcolumn',
                // you may configure additional properties here
            ],
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width:70px;'],
            ],
            [
                'attribute' => 'lang',
                'filter' => yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->asArray()->all(), 'iso_code', 'title'),
                'value' => function($model) use ($searchModel){
                    return $searchModel->lang;
                }
            ],
            [
                'attribute' => 'object_type',
                'label' => 'Page Type',
                'value' => function(\common\models\SeoPage $model)
                {
                    return $model->seoPageAutofill?$model->seoPageAutofill->object_type:null;
                },
                'filter' => \common\modules\seo\SeoModule::getTypesCombo()
            ],
            [
                'attribute' => 'autofill_template',
                'format' => 'raw',
                'value' => function(\common\models\SeoPage $model) use ($searchModel) {
                    if($searchModel->lang && $searchModel->lang!='en-US'){
                        $map = \lib\collection\CollectionDb::getMap($model->seoPageIntls, 'lang_iso');
                        if(!empty($map[$searchModel->lang]))
                            $model = $map[$searchModel->lang];
                    }
                    if($model->seoPageAutofill&&$model->seoPageAutofill->template){
                        return Html::a(
                            $model->seoPageAutofill->template->template_name,
                            ['site/seo-page-autofill-template/update', 'id' => $model->seoPageAutofill->template->id],
                            ['target'=>'_blank']
                        );
                    }
                    return null;
                },
                'filter' =>  $templateApplyForm->getAutofillTemplates()
            ],
            [
                'attribute' => 'url',
                'format' => 'raw',
                'value' => function($model) use ($searchModel){
                    $addon = '';
                    if(!empty($searchModel->lang)){
                        $addon = strpos($model->url, '?')!==false?'&':'?' . 'ln='.$searchModel->lang;
                    }
                    return Html::a($model->url, param('server') . '/' . $model->url . $addon, ['target'=>'_blank']);
                }
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(\common\models\SeoPage $model) use($searchModel, $langs){
                    //return $model->title . ' <br /><code>H1:</code>' . $model->header;
                    $map = \lib\collection\CollectionDb::getMap($model->seoPageIntls, 'lang_iso');
                    $result = [];
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        if($searchModel->lang==$v->iso_code){
                            $model = $map[$v->iso_code];
                        }else if($searchModel->lang!='en-US' && !empty($searchModel->lang)){
                            continue;
                        }
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->title : false;
                        $val = \H($val);
                        if(!empty($map[$v->iso_code])){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['site/seo-page-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['site/seo-page-intl/create',
                                'SeoPageIntl'=>['model_id'=>$model->id, 'lang_iso'=>$v->iso_code]]);
                        }
                    }
                    $result = Html::tag("h5", \H($model->title)) .  implode("<br />", $result);
                    return $result;
                }
            ],
            [
                'attribute' => 'meta_description',
                'format' => 'raw',
                'value' => function($model) use ($searchModel){
                    $map = \lib\collection\CollectionDb::getMap($model->seoPageIntls, 'lang_iso');
                    if(array_key_exists($searchModel->lang, $map)){
                        $model = $map[$searchModel->lang];
                    }

                    return sprintf('<code>Description:</code> %s <br /> <code>Keywords:</code> %s', \H($model->meta_description), \H($model->meta_keywords));
                }
            ],
            // 'header_text:ntext',
            // 'footer_text:ntext',
            [
                'attribute' => 'is_active',
                'format' => 'boolean',
                'filter' =>  Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    [1=>'Yes', 0=>'No'],
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'is_locked',
                'format' => 'boolean',
                'filter' =>  Html::activeDropDownList(
                    $searchModel,
                    'is_locked',
                    [1=>'Yes', 0=>'No'],
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'need_review',
                'format' => 'boolean',
                'filter' =>  Html::activeDropDownList(
                    $searchModel,
                    'need_review',
                    [1=>'Yes', 0=>'No'],
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'is_sitemap',
                'format' => 'boolean',
                'filter' =>  Html::activeDropDownList(
                    $searchModel,
                    'is_sitemap',
                    [1=>'Yes', 0=>'No'],
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

    <script>
        <?php $this->beginBlock('js1');?>
        function removeA(arr) {
            var what, a = arguments, L = a.length, ax;
            while (L > 1 && arr.length) {
                what = a[--L];
                while ((ax= arr.indexOf(what)) !== -1) {
                    arr.splice(ax, 1);
                }
            }
            return arr;
        }
        $('.js-submit a').on('click', function(){
            $form = $(this).closest("form");
            $('#submitAction').val($(this).data('submit'));
            $form.submit();
        });
        var checked = [];
        $(".checkcolumn").change(function(e) {
            if(this.checked) {
                checked.push(this.value);
            }else{
                removeA(checked, this.value);
            }
            $('#selectedIdsAction').val(checked.join(','));
            $('#selectedIdsApply').val(checked.join(','));
        });
        $('.select-on-check-all').change(function(e){
            checked = [];
            $('input[name="selection[]"]:checked').each(function() {
                checked.push(this.value);
            });
            $('#selectedIdsAction').val(checked.join(','));
            $('#selectedIdsApply').val(checked.join(','));

        });
        <?php $this->endBlock(); ?>
    </script>
<?=$this->registerJs($this->blocks['js1']);?>