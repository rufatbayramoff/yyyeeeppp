<?php

use common\components\ArrayHelper;
use common\models\EquipmentCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineProperty */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wiki-machine-property-form">

    <?php $form = ActiveForm::begin(
        [
            'id' => 'my-form-id-machine-property',
            'enableAjaxValidation' => true,
            'validationUrl'        => '/site/wiki-machine-property/validate' . ($model->isNewRecord ? '' : '?id=' . $model->id),
            'action'               => '/site/wiki-machine-property/update' . ($model->isNewRecord ? '' : '?id=' . $model->id),
        ]
    ); ?>
    <div class="hidden">
        <?= $form->field($model, 'wiki_machine_id')->textInput() ?>
    </div>

    <?= $form->field($model, 'wiki_machine_available_property_id')->dropDownList(
        ArrayHelper::map($model->wikiMachine->equipmentCategory->wikiMachineAvailableProperties, 'id', 'title')
    ) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
