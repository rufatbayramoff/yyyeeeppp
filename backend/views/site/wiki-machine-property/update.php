<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineProperty */

$this->title = 'Update Wiki Machine Property: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wiki-machine-property-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
