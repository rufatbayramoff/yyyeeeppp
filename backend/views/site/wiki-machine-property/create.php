<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineProperty */

$this->title = 'Create Wiki Machine Property';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-property-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
