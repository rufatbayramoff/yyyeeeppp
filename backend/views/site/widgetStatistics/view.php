<?php

use common\models\WidgetStatistics;
use yii\widgets\DetailView;

/* @var $this yii\web\View */


$this->title = 'Widget statistics ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Widget statistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-view">

    <?= DetailView::widget(
        [
            'model'      => $model,
            'attributes' => [
                'id',
                'type',
                'hosting_page',

                'ps'        => [
                    'label'  => 'Ps',
                    'format' => 'raw',
                    'value'  => $model->ps ? '<a href="/ps/ps/view?id=' . $model->ps->id . '">' . htmlspecialchars($model->ps->title) . '</a>' : ''

                ],
                'psPrinter' => [
                    'label' => 'psPrinter',
                    'format' => 'raw',
                    'value' => $model->psPrinter ? '<a href="/ps/ps-printer/view?id=' . $model->psPrinter->id . '">' . htmlspecialchars($model->psPrinter->title) . '</a>' : ''

                ],
                'designer'  => [
                    'label' => 'Designer',
                    'format' => 'raw',
                    'value' => $model->designer ? '<a href="/user/user/view?id=' . $model->designer->id . '">' . $model->designer->username . '</a>' : ''
                ],
                'views_count',
                'first_visit',
                'last_visit',
            ],
        ]
    ) ?>

</div>
