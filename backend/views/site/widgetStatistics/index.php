<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 07.03.17
 * Time: 16:07
 */

use common\models\WidgetStatistics;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WidgetStatisticsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Widget statistics pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="static-page-index">
    <?php
    echo new \backend\components\GridViewDataExporter($searchModel)
    ?>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                [
                    'attribute' => 'type',
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'type',
                        WidgetStatistics::getTypes(),
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                    'value' => function(WidgetStatistics $ws){
                        $type = $ws->type;
                        $types = WidgetStatistics::getTypes();
                        return array_key_exists($type, $types) ? $types[$type] : $type;
                    }
                ],
                'hosting_page' =>[
                        'label' => 'Hosting page',
                    'format' => 'raw',
                    'value' => function($ws){
                        return Html::a(substr($ws->hosting_page,0,50).'...', $ws->hosting_page);
                    }
                ],
                'ps'        => [
                    'label'  => 'Ps',
                    'format' => 'raw',
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'ps_id',
                        WidgetStatistics::getWidgetPsList(),
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                    'value'  => function (WidgetStatistics $widgetStatistics) {
                        return $widgetStatistics->ps ? '<a href="/ps/ps/view?id=' . $widgetStatistics->ps->id . '">' . htmlspecialchars(
                                $widgetStatistics->ps->title
                            ) . '</a>' : '';
                    }
                ],
                'psPrinter' => [
                    'label'  => 'psPrinter',
                    'format' => 'raw',
                    'value'  => function (WidgetStatistics $widgetStatistics) {
                        return $widgetStatistics->psPrinter ? '<a href="/ps/ps-printer/view?id=' . $widgetStatistics->psPrinter->id . '">' . htmlspecialchars(
                                $widgetStatistics->psPrinter->title
                            ) . '</a>' : '';
                    }
                ],
                'designer'  => [
                    'label'  => 'Designer',
                    'format' => 'raw',
                    'value'  => function (WidgetStatistics $widgetStatistics) {
                        return $widgetStatistics->designer ? '<a href="/user/user/view?id=' . $widgetStatistics->designer->id . '">' . $widgetStatistics->designer->username . '</a>' : '';
                    }
                ],
                'views_count',
                [
                    'class'               => 'kartik\grid\DataColumn',
                    'attribute'           => 'first_visit',
                    'filterType'          => GridView::FILTER_DATE_RANGE,
                    'format'              => ['date'],
                    'filterWidgetOptions' => [
                        'attribute'      => 'first_visit',
                        'presetDropdown' => true,
                        'convertFormat'  => false,
                        'pluginOptions'  => [
                            'opens'     => 'left',
                            'separator' => ' - ',
                            'format'    => 'YYYY-MM-DD',
                            'locale'    => [
                                'format' => 'YYYY-MM-DD'
                            ],
                        ],
                        'pluginEvents'   => [
                            "apply.daterangepicker" => "function() { apply_filter('only_date') }",
                        ],
                    ]
                ],
                [
                    'class'               => 'kartik\grid\DataColumn',
                    'attribute'           => 'last_visit',
                    'filterType'          => GridView::FILTER_DATE_RANGE,
                    'format'              => ['date'],
                    'filterWidgetOptions' => [
                        'attribute'      => 'last_visit',
                        'presetDropdown' => true,
                        'convertFormat'  => false,
                        'pluginOptions'  => [
                            'opens'     => 'left',
                            'separator' => ' - ',
                            'format'    => 'YYYY-MM-DD',
                            'locale'    => [
                                'format' => 'YYYY-MM-DD'
                            ],
                        ],
                        'pluginEvents'   => [
                            "apply.daterangepicker" => "function() { apply_filter('only_date') }",
                        ],
                    ]
                ],
                [
                    'class'    => \yii\grid\ActionColumn::class,
                    'template' => '{view}',
                ],
            ],
        ]
    ); ?>
</div>
