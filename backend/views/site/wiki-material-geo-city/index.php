<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WikiMaterialGeoCitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wiki Material Geo Cities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-material-geo-city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Wiki Material Geo City', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'wiki_material_id',
            'geo_city_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
