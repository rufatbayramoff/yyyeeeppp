<?php

use common\components\ArrayHelper;
use common\models\GeoCity;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialGeoCity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wiki-material-geo-city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wiki_material_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'geo_city_id')->widget(Select2::class, [
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => false,
            'minimumInputLength' => 3,
            'ajax' => [
                'url' => '/site/search-city',
            ]
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
