<?php
use backend\models\notify\NotifyItem;

$notifies = count($notifiesData);
$notifiesObjects = 0;
$soundObjectsCount = 0;
foreach ($notifiesData as $k => $v) {
    if (\backend\models\notify\NotifyItem::isTranslate($v)) {
        continue;
    }
    if (($v->type != NotifyItem::TYPE_TO_TRANSLATE) && ($v->type != NotifyItem::TYPE_TRANSLATE_CHECK)) {
        $soundObjectsCount += (int)$v->count;
    }
    $notifiesObjects = $notifiesObjects + intval($v->count);
}
if (Yii::$app->session->get('lastNotifiesObject') != $soundObjectsCount) {
}
Yii::$app->session->set('lastNotifiesObject', $soundObjectsCount);
?>

<a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-bell-o"></i>
    <span class="label label-danger" style="font-size:12px;"><?php echo $notifiesObjects; ?></span><br/>

</a>
<ul class="dropdown-menu">
    <li class="header">You have <span class="notify-count"><?php echo $notifiesObjects; ?></span> notifications</li>
    <li>
        <ul class="menu">
            <?php foreach ($notifiesData as $k => $v):
                if (\backend\models\notify\NotifyItem::isTranslate($v)) {
                    continue;
                }
                ?>
                <li>
                    <a href="<?php echo \yii\helpers\Url::toRoute($v->url); ?>">
                        <i class="fa <?php echo $v->icon; ?>"></i> <?php echo $v->title; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </li>
</ul>