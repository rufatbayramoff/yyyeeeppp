<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ApiExternalSystemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Api External Systems';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="api-external-system-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Api External System', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                'private_key',
                'binded_user_id',
                'json_config' => [
                    'attribute' => 'json_config',
                    'value' => function (\common\models\ApiExternalSystem $apiExternalSystem) {
                        return $apiExternalSystem->json_config ? json_encode($apiExternalSystem->json_config):'';
                    },
                ],
                'model3d_owner',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]
    ); ?>
</div>
