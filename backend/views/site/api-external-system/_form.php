<?php

use common\models\ApiExternalSystem;
use common\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ApiExternalSystem */
/* @var $form yii\widgets\ActiveForm */

$model->json_config = stripslashes(json_encode($model->json_config));
?>

<div class="api-external-system-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'private_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'public_upload_key')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'binded_user_id')->widget(kartik\select2\Select2::classname(), [
        'initValueText' => $model->bindedUser?$model->bindedUser->username:'',
        'options' => ['placeholder' => 'Search'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/user/user/list']),
                'dataType' => 'json',
                'data' => new yii\web\JsExpression('function(params) { return {q:params.term, page:params.page, k:"id,username"}; }')
            ],
        ],
    ]);
    ?>
    <?= $form->field($model, 'json_config')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'model3d_owner')->dropDownList(ApiExternalSystem::ALLOWED_MODEL3D_OWNER_TYPES) ?>

    <?= $form->field($model, 'only_certificated_printers')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
