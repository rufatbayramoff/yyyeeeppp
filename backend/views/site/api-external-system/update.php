<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ApiExternalSystem */

$this->title = 'Update Api External System: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Api External Systems', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="api-external-system-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
