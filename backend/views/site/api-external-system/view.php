<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ApiExternalSystem */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Api External Systems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-external-system-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?= Html::a('Bind affiliate award', ['affiliate-bind', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'private_key',
            'public_upload_key',
            [
                'format' => 'raw',
                'label' => 'Binded user',
                'value' => '<a href="/user/user/view?id='.$model->binded_user_id.'">'.$model->bindedUser->username.'</a>'
            ],
            [
                'format' => 'raw',
                'label' => 'Affiliate source',
                'value' => $model->getAffiliateSource()?'<a href="/affiliate/affiliate-source/view?id='.$model->getAffiliateSource()->uuid.'">'.$model->getAffiliateSource()->uuid.'</a>':''
            ],
            'model3d_owner'
        ],
    ]) ?>

</div>
