<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryCard */

$this->title = 'Create Home Page Category Card';
$this->params['breadcrumbs'][] = ['label' => 'Home Page Category Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-category-card-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
