<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \common\models\HomePageCategoryCard;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryCard */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Category Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="home-page-category-card-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'is_active',
            'product_category_id',
            [
                'attribute' => 'title',
                'value'     => function (HomePageCategoryCard $model) {
                    return $model->getTitleText();
                }
            ],
            [
                'attribute' => 'url',
                'value'     => function (HomePageCategoryCard $model) {
                    if (!empty($model->url)) {
                        return $model->url;
                    } elseif ($productCategory = $model->productCategory) {
                        return '/products/'.$productCategory->code;
                    } else {
                        return '';
                    }
                }
            ],
            'cover_file_uuid',
            [
                'attribute' => 'Cover image',
                'value' => function (HomePageCategoryCard $model) {
                    return $model->coverFile ? Html::img($model->coverFile->getFileUrl(), ['height' => '200px']) : '';
                },
                'format' => 'html'
            ],
            'position',
        ],
    ]) ?>

</div>
