<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\HomePageCategoryCard;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\HomePageCategoryCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Home Page Category Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-category-card-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Home Page Category Card', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'is_active',
            'product_category_id',
            [
                'attribute' => 'title',
                'value'     => function (HomePageCategoryCard $model) {
                    return $model->getTitleText();
                }
            ],
            [
                'attribute' => 'url',
                'value'     => function (HomePageCategoryCard $model) {
                    if (!empty($model->url)) {
                        return $model->url;
                    } elseif ($productCategory = $model->productCategory) {
                        return '/products/'.$productCategory->code;
                    } else {
                        return '';
                    }
                }
            ],
            'position',
            [
                'class'          => \yii\grid\ActionColumn::class,
                'buttons'        => Yii::$app->getModule('translation')->dbI18n->getGridButton(HomePageCategoryCard::tableName()),
                'template'       => '{translate} &nbsp; {view} &nbsp; {update} &nbsp; {delete} ',
                'contentOptions' => ['style' => 'width: 120px;']
            ],
        ],
    ]); ?>
</div>
