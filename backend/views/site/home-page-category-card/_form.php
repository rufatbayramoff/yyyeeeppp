<?php

use common\models\ProductCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryCard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="home-page-category-card-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php echo $form->field($model, 'product_category_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(ProductCategory::find()->asArray()->all(), 'id', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'coverImageFile')->fileInput(['multiple' => false, 'accept' => 'image/*'])->label('Cover Image File'); ?>

    <div style="margin-bottom: 15px">Picture size 750x500px</div>

    <?php if ($model->coverFile): ?>
        <?php echo Html::img($model->coverFile->getFileUrl(), ['height' => '200px', 'style' => 'margin-bottom: 15px']); ?>
    <?php endif; ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
