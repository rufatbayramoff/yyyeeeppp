<?php

use backend\controllers\site\SiteHelpController;
use common\models\SiteHelpCategory;
use frontend\components\image\ImageHtmlHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SiteHelp */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord){
    $model->priority = 0;
}
if(!$model->isNewRecord){
    $relatedHelpIds = \common\components\ArrayHelper::getColumn($model->getRelatedHelps()->all(), 'id');
    $relatedModel =  new \common\models\SiteHelpRelated();
    $relatedModel->help_id = $model->id;
    $relatedModel->related_help_id = $relatedHelpIds;
}else{
    $relatedModel =  new \common\models\SiteHelpRelated();
    $relatedModel->related_help_id = [];
}
?>

<div class="site-help-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php echo $form->field($model, 'category_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => SiteHelpController::categoriesList(),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);
 ?>
    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>
    <?= $form->field($model, 'is_active')->checkbox() ?>
    <?= $form->field($model, 'is_popular')->checkbox() ?>
    <?= $form->field($model, 'priority')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'tags')->textInput()->hint("Use | as delimiter."); ?>

    <!-- COVER FILE -->

    <?= $form->field($model, 'coverFile')->fileInput(['accept' => '.jpg,.jpeg,.png,.gif']) ?>
    <?php if($model->coverFile) :?>
        <div>
            <?= Html::img(ImageHtmlHelper::getThumbUrlForFile($model->coverFile, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL)); ?>
        </div>
    <?php endif; ?>

    <!-- END COVER FILE -->

    <?php echo $form->field($relatedModel, 'related_help_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\SiteHelp::find()->select(['id', 'concat(id, ". ", title) as title'])->where(['is_active'=>1])->asArray()->all(), 'id', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'multiple' => true,
            'allowClear' => true
        ],
    ])->label('Related help');
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
