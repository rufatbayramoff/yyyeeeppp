<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model common\models\SiteHelp */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Site Helps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-help-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class='row'>
        <div class='col-md-8'>
             <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'updated_at',
            'user_id',
            'alias',
            'title',
            'content:html',
            'is_active',
            'clicks',
            'is_ok',
            'is_bad',
        ],
    ]) ?>
        </div>
        <div class='col-md-4'>
            
    
    <?= GridView::widget([
        'dataProvider' => common\models\SiteHelpVote::getDataProvider(['site_help_id'=>$model->id], 20), 
        'columns' => [  
            [
                'attribute' => 'vote_answer',
                'format' => 'raw',
                'value' => function($model) {
                    $voteAnswers = common\models\SiteHelpVote::getWhyNo();
                    return isset($voteAnswers[$model->vote_answer]) ? $voteAnswers[$model->vote_answer] : $model->vote_answer;
                }
            ], 
            'date_added:datetime',
 
        ],
    ]); ?>
        </div>
        
    </div>
   
</div>
