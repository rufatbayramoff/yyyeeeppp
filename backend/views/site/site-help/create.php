<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteHelp */

$this->title = 'Create Site Help';
$this->params['breadcrumbs'][] = ['label' => 'Site Helps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-help-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
