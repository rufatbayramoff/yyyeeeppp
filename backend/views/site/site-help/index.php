<?php

use common\models\SiteHelp;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteHelpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Helps';
$this->params['breadcrumbs'][] = $this->title;

echo $this->renderFile(Yii::getAlias('@backend/views/site/site-help/_topTabs.php'));

$langs = common\models\SystemLang::find()->all();
?>
<div class="site-help-index">

    <p>
        <?= Html::a('Create Site Help', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'format' => 'raw',
                'value' => function($model){
                    $url = param('server') . \yii\helpers\Url::toRoute(['help/article', 'id' => $model->id]);
                  
                    return Html::a('[' .$model->id . ']' , $url, ['target'=>'blank']);
                }
            ],
            
            //'created_at:datetime',
            [
                'attribute' => 'alias',
                'format' => 'raw',
                'value' => function(SiteHelp $article) {

                    $html = $article->alias;

                    if ($article->is_popular) {
                        $html.= '<br/>'."<span class=\"label label-success\">popular</span>";
                    }

                    return $html;
                }
            ],
            'priority',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(SiteHelp $model) use($langs){
                    $result = [];
                    $map = \lib\collection\CollectionDb::getMap($model->siteHelpIntls, 'lang_iso');
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->title : false;
                        if($val){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['site/site-help-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['site/site-help-intl/create',
                                'SiteHelpIntl'=>['model_id'=>$model->id, 'lang_iso'=>$v->iso_code]]);
                        }
                    }
                    $result = Html::tag("h5", $model->title) .  implode("<br />", $result);
                    return $result;
                }
            ],
            // 'content:ntext',
              [
                'attribute' => 'is_active',
                'format' => 'raw',
                'value' => function($model) {
                    $url = \yii\helpers\Url::toRoute(['site/site-help/change-active', 'id' => $model->id]);
                    $val = $model->is_active ? 'Yes' : 'No';
                    $kVal = $model->is_active ? 'ok' : 'remove';
                    return Html::a(sprintf('<span class="glyphicon glyphicon-%s"></span> %s',$kVal, $val), $url, [
                        'title' => 'Change', 
                        'class' => 'grid-action'
                    ]);
                }
            ], 
            'clicks',
            'is_ok', 
            'views',
            [
                'attribute' => 'is_bad',
                'format' => 'raw',
                'value' => function($model) {
                    $url = \yii\helpers\Url::toRoute(['site/site-help/view', 'id' => $model->id]);                    
                    return Html::a($model->is_bad . ' [view]', $url, [
                        
                    ]);
                }
            ], 
            'user.username',
            [
                'attribute' => 'category_id',
                'value' => function($model){
                    if(!empty($model->category_id) && !empty($model->category)){ return $model->category->title; }
                    return $model->category_id;
                }
            ],
            'updated_at:datetime',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    Usage example in code:
    <pre>
        echo frontend\widgets\SiteHelpWidget::widget(['alias'=>'ps.delivery.freedelivery']); 
    </pre>
</div>
