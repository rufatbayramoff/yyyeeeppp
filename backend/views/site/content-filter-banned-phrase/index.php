<?php

use common\models\ContentFilterBannedPhrase;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ContentFilterBannedPhraseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Content Filter Banned Phrases';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-filter-banned-phrase-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Content Filter Banned Phrase', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Import data', ['import'], ['class' => 'btn btn-primary']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'phrase',
                [
                    'attribute' => 'lang',
                    'format'    => 'raw',
                    'value'     => function (ContentFilterBannedPhrase $model) {
                        return $model->lang->title;
                    }

                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]
    ); ?>
</div>
