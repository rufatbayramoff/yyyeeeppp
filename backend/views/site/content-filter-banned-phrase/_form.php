<?php

use common\components\ArrayHelper;
use common\models\SystemLang;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContentFilterBannedPhrase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-filter-banned-phrase-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'phrase')->textInput(['maxlength' => true]) ?>

    <?php
    $sourceRows = SystemLang::find()->all();
    echo $form->field($model, 'lang_id')->widget(
        Select2::class,
        [
            'data'          => ArrayHelper::map($sourceRows, 'id', 'title'),
            'options'       => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]
    ); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
