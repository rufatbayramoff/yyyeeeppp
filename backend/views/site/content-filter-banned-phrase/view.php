<?php

use common\modules\contentAutoBlocker\components\contentFilters\BannedPhraseFilter;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContentFilterBannedPhrase */

$this->title = $model->id . '-' . $model->phrase;
$this->params['breadcrumbs'][] = ['label' => 'Content Filter Banned Phrases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
/** @var BannedPhraseFilter $bannedPhraseFilter */
$bannedPhraseFilter = Yii::createObject(BannedPhraseFilter::class);
?>
<div class="content-filter-banned-phrase-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(
            'Delete',
            ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ]
        ) ?>
    </p>

    <?= DetailView::widget(
        [
            'model'      => $model,
            'attributes' => [
                'id',
                'phrase',
                [
                    'label'  => 'Full regular',
                    'value' => $bannedPhraseFilter->getFullRegularExpression($model->phrase)
                ],
                [
                    'attribute' => 'lang',
                    'format'    => 'raw',
                    'value'     => $model->lang->title
                ]
            ],
        ]
    ) ?>

</div>
