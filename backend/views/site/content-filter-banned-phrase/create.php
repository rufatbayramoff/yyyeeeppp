<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContentFilterBannedPhrase */

$this->title = 'Create Content Filter Banned Phrase';
$this->params['breadcrumbs'][] = ['label' => 'Content Filter Banned Phrases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-filter-banned-phrase-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
