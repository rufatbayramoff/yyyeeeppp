<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.11.16
 * Time: 16:13
 */

use common\models\ContentFilterBannedPhrase;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ContentFilterBannedPhraseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Content Filter Banned Phrases';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-filter-banned-phrase-index">

    <h1><?=_t('site', 'Import')?></h1>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div>
      <input type="file" name="import-file" accept=".csv">
    </div>
    <br>
    <div class="form-group">
        <?= Html::submitButton('Import', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>