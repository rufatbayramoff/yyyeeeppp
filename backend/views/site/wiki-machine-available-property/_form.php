<?php

use common\components\ArrayHelper;
use common\models\EquipmentCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineAvailableProperty */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wiki-machine-available-property-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'show_in_catalog')->dropDownList(['dont_show' => 'dont_show', 'in_top' => 'in_top', 'in_bottom' => 'in_bottom']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'equipment_category_id')->dropDownList(ArrayHelper::map(EquipmentCategory::find()->withoutRoot()->all(), 'id', 'titleWithCode')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
