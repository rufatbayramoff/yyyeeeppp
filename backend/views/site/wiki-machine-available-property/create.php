<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineAvailableProperty */

$this->title = 'Create Wiki Machine Available Property';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Available Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-available-property-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
