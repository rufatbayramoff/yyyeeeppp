<?php

use common\models\EquipmentCategory;
use common\models\WikiMachineAvailableProperty;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WikiMachineAvailablePropertySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wiki Machine Available Properties';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-available-property-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Wiki Machine Available Property', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                'code',
                'show_in_catalog',
                'title',
                [
                    'attribute' => 'equipment_category_id',
                    'value'     => function (WikiMachineAvailableProperty $property) {
                        return $property->equipmentCategory->getTitleWithCode();
                    }
                ],
                [
                    'class'          => ActionColumn::class,
                    'buttons'        => Yii::$app->getModule('translation')->dbI18n->getGridButton('wiki_machine_available_property'),
                    'template'       => '{translate} &nbsp; {view} &nbsp; {update} &nbsp; {delete} ',
                    'contentOptions' => ['style' => 'width: 100px;']
                ]
            ],
        ]
    ); ?>
</div>
