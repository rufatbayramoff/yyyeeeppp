<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $equipmentCategory common\models\EquipmentCategory */

$this->title = $equipmentCategory->title;
$this->params['breadcrumbs'][] = ['label' => 'Equipment Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipment-category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $equipmentCategory->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(
            'Delete',
            ['delete', 'id' => $equipmentCategory->id],
            [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ]
        ) ?>
    </p>

    <?= DetailView::widget(
        [
            'model'      => $equipmentCategory,
            'attributes' => [
                'id',
                'lft',
                'rgt',
                'depth',
                [
                    'label'  => 'Parent',
                    'format' => 'raw',
                    'value'  => $equipmentCategory->parentCategory->getTitleWithCode()
                ],
                'code',
                'slug',
                'title',
                'description',
            ],
        ]
    ) ?>

</div>
