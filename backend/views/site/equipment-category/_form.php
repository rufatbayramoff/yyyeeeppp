<?php

use common\components\ArrayHelper;
use common\models\EquipmentCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\EquipmentCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="equipment-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parentCategoryId')->dropDownList(ArrayHelper::map(EquipmentCategory::find()->all(), 'id', 'titleWithCode')) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
