<?php

use common\models\EquipmentCategory;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EquipmentCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Equipment Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipment-category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Equipment Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                [
                    'label'  => 'Parent',
                    'format' => 'raw',
                    'value'  => function (EquipmentCategory $category) {
                        return $category->parentCategory?$category->parentCategory->getTitleWithCode():'';
                    }
                ],
                'code',
                'slug',
                'title',
                [
                    'class'          => ActionColumn::class,
                    'buttons'        => Yii::$app->getModule('translation')->dbI18n->getGridButton('equipment_category'),
                    'template'       => '{translate} &nbsp; {view} &nbsp; {update} &nbsp; {delete} ',
                    'contentOptions' => ['style' => 'width: 100px;']
                ]
            ],
        ]
    ); ?>
</div>
