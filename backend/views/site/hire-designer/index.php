<?php
/**
 * Created by mitaichik
 */
use backend\assets\AngularAppAsset;

/** @var \yii\web\View $this */
$this->title = "Hire Designer";
$this->registerAssetBundle(AngularAppAsset::class);
?>



<div class="hire-designer-conent" ng-controller="HireDesignerListController">

    <div class="hire-designer-controls">
        <button
            ng-click="openAddDesignerModal()"
            type="button" class="btn btn-primary">Add designer</button>

        <button
            ng-click="save()"
            type="button" class="btn btn-success">Save</button>
    </div>


    <div class="hire-designer-designers">

        <div ng-if="items.length == 0" class="text-center">
            <h4>No designers</h4>
        </div>

        <div ng-repeat="item in items"
             class="hire-designer-designers__designer row">


            <div class="col-md-3">
                <h3 class="hire-designer-designers__designer-header">{{item.designer.username}}</h3>
                <div>
                    <button
                        ng-click="moveUp(item)"
                        ng-disabled="!canMoveUp(item)"
                        type="button" class="btn btn-default btn-sm"><i class="fa fa-arrow-up"></i></button>
                    <button
                        ng-click="moveDown(item)"
                        ng-disabled="!canMoveDown(item)"
                        type="button" class="btn btn-default btn-sm"><i class="fa fa-arrow-down"></i></button>
                </div>

            </div>

            <div class="col-md-7">

                <div
                    ng-repeat="model in item.models"
                    class="thumbnail hire-designer-designers__model-block">

                    <div
                        ng-click="removeModel(item, model)"
                        class="delete-model">
                        <button class="btn btn-default"><i class="fa fa-times"></i></button>
                    </div>

                    <img ng-src="{{model.coverUrl}}" style="height: 100%; width: 100%; display: block;">
                </div>

                <div
                    ng-if="canAddModel(item)"
                    ng-click="openAddModelModal(item)"
                    class="thumbnail hire-designer-designers__model-block add-placeholder">
                    <i class="fa fa-plus"></i>
                </div>

            </div>

            <div class="col-md-2 text-right">
                <button
                    ng-click="remove(item)"
                    type="button" class="btn btn-warning btn-sm">Remove</button>
            </div>
        </div>

    </div>

</div>