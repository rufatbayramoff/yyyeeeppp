<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.01.17
 * Time: 17:48
 */

use common\models\HttpRequestLog;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $runtimeFilesList \backend\models\RuntimeLogsList */

$this->title = 'Runtime files log';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="runtime-files-log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget(
        [
            'dataProvider' => $runtimeFilesList->getProvider(),
            'columns'      => [
                'date',
                'name',
                ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
            ],
        ]
    ); ?>
</div>
