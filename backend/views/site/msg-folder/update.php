<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MsgFolder */

$this->title = 'Update Msg Folder: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Msg Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="msg-folder-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
