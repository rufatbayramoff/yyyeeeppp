<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MsgFolder */

$this->title = 'Create Msg Folder';
$this->params['breadcrumbs'][] = ['label' => 'Msg Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-folder-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
