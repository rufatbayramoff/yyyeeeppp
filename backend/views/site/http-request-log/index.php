<?php

use common\models\HttpRequestLog;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\HttpRequestLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Http request log';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <?php $form = ActiveForm::begin([
            'action'  => ['index'],
            'method'  => 'get',
            'options' => [
                'class' => 'row'
            ]
        ]); ?>
        <div class="col-md-2">
            <?php echo $form->field($searchModel, 'extData') ?>
        </div>

        <div class="col-md-2">
            <?php echo $form->field($searchModel, 'extObjectType')->dropDownList($searchModel->getExtObjectTypeSelect(), ['prompt' => '-']) ?>
        </div>

        <div class="col-md-2">
            <?php echo $form->field($searchModel, 'extObjectId') ?>
        </div>

        <div class="col-md-2">
            <?php echo $form->field($searchModel, 'answer') ?>
        </div>

        <div class="col-md-2">
            <input type="submit" value="Search" class="btn btn-primary searchModel-btn"/>
            <style>
                @media (min-width: 769px) {
                    .searchModel-btn {
                        margin-top: 25px;
                    }
                }
            </style>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="api-external-system-index">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                'url'              => [
                    'label'     => 'Url',
                    'format'    => 'raw',
                    'attribute' => 'url',
                    'value'     => function (HttpRequestLog $log) {
                        return '<div style="word-wrap: break-word; max-width:650px;">' . htmlspecialchars($log->url) . '</div>';
                    }
                ],
                'remote_addr',
                'request_type'     => [
                    'label'     => 'Request Type',
                    'format'    => 'raw',
                    'attribute' => 'request_type_select',
                    'filter'    => Html::activeDropDownList($searchModel, 'request_type', $searchModel->getRequestTypeSelect(), ['class' => 'form-control', 'prompt' => '-']),
                    'value'     => function (HttpRequestLog $log) {
                        return $log->request_type;
                    }
                ],
                'create_date',
                'run_time_milisec' => [
                    'label'     => 'Run time milliseconds',
                    'format'    => 'raw',
                    'attribute' => 'run_time_milisec',
                    'value'     => function (HttpRequestLog $log) {
                        return number_format($log->run_time_milisec, '0', '.', ' ');
                    }
                ],
                'size'             => [
                    'label' => 'AnswerSize',
                    'value' => function (HttpRequestLog $log) {
                        $answer = $log->answer;
                        if (is_array($answer)) {
                            $answer = json_encode($answer);
                        }
                        return number_format(strlen($answer), '0', '.', ' ');
                    }
                ],
                'user_session_id'  => [
                    'label'     => 'User session id',
                    'attribute' => 'user_session_id',
                    'value'     => function (HttpRequestLog $log) {
                        return $log->userSession->id ?? '';
                    }
                ],
                'responce_code',
                ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
            ],
        ]
    ); ?>
</div>
