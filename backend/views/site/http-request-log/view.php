<?php

use backend\models\search\HttpRequestLogSearch;
use common\models\HttpRequestExtDataLog;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model HttpRequestLogSearch */

$this->title = 'Http request: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'HTTP request log', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="http-request-log-view">

    <h1>Http request: <?= Html::encode($model->id) ?></h1>

    <?= DetailView::widget(
        [
            'model'      => $model,
            'attributes' => [
                'id',
                'host',
                'url',
                'remote_addr',
                'params_get'   => [
                    'label' => 'Params get',
                    'value' => json_encode($model->params_get)
                ],
                'params_post'  => [
                    'label' => 'Params post',
                    'value' => json_encode($model->params_post)
                ],
                'params_put'  => [
                    'label' => 'Params body',
                    'value' => json_encode($model->params_body)
                ],
                'params_files' => [
                    'label' => 'Files',
                    'value' => json_encode($model->params_files)
                ],
                'session'      => [
                    'label' => 'Session',
                    'value' => json_encode($model->session)
                ],
                'cookie'       => [
                    'label' => 'Cookie',
                    'value' => json_encode($model->cookie)
                ],
                'answer'       => [
                    'label' => 'Answer',
                    'value' => json_encode($model->answer)
                ],
                'responce_code',
                'headers'      => [
                    'label' => 'Headers',
                    'value' => json_encode($model->headers)
                ],
                'create_date',
                'run_time_milisec',
                'request_type',
                'user_agent',
                'referrer',
                'user_session' => [
                    'label' => 'User session',
                    'value' => json_encode($model->userSession->attributes ?? '')
                ]
            ]
        ]
    ) ?>
    <?php

    $dataProvider = new ActiveDataProvider(
        [
            'query'      => $model->getHttpRequestExtDataLogs(),
            'pagination' => [
                'pageSize' => 200000,
            ],
        ]
    );

    echo GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'summary'      => '',
            'columns'      => [
                'log_text',
                'type',
                'ext_data' => [
                    'label'  => 'Ext data',
                    'format' => 'raw',
                    'value'  => function (HttpRequestExtDataLog $model) {
                        if ($model->type == HttpRequestExtDataLog::TYPE_AR) {
                            $html = '';

                            $html .= Html::tag('div', '<b>' . $model->object_type . '</b>: ' . $model->object_id);

                            foreach ($model->ext_data as $attr => $value) {
                                $oldValue = is_array($value['oldValue'])?json_encode($value['oldValue']):$value['oldValue'];
                                $newValue = is_array($value['newValue'])?json_encode($value['newValue']):$value['newValue'];
                                $old = Html::tag('span', $oldValue, ['style' => 'color: red; margin-right: 5px; text-decoration: line-through']);
                                $new = Html::tag('span', $newValue, ['style' => 'color: green']);

                                $html .= Html::tag('div', $model->getAttributeLabel($attr) . ': ' . $old . $new);
                            }

                            return $html;
                        }

                        return json_encode($model->ext_data);
                    }
                ],
            ]
        ]
    );
    ?>
</div>
