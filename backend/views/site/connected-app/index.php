<?php

use common\components\ArrayHelper;
use common\models\EquipmentCategory;
use common\models\ConnectedApp;
use common\models\WikiMachine;
use frontend\components\image\ImageHtmlHelper;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ConnectedAppSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Connected apps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connected-app-index">
    <p>
        <?= Html::a('Create Connected App', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                'code',
                'title',
                [
                    'header' => 'Logo photo',
                    'format' => 'raw',
                    'value'  => function (ConnectedApp $connectedApp) {
                        return ImageHtmlHelper::getClickableThumb($connectedApp->logoFile);
                    }
                ],
                'short_descr',
                [
                    'class'          => ActionColumn::class,
                    'buttons'        => Yii::$app->getModule('translation')->dbI18n->getGridButton('connected_app'),
                    'template'       => '{translate} &nbsp; {view} &nbsp; {update} &nbsp; {delete} ',
                    'contentOptions' => ['style' => 'width: 100px;']
                ]
            ],
        ]
    ); ?>
</div>
