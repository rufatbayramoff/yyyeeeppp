<?php

use backend\widgets\FilesListWidget;
use common\components\ArrayHelper;
use common\components\FileTypesHelper;
use common\models\EquipmentCategory;
use frontend\components\image\ImageHtmlHelper;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $connectedApp common\models\WikiMachine */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-app-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($connectedApp, 'code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($connectedApp, 'order')->textInput(['maxlength' => true]) ?>

    <?= $form->field($connectedApp, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($connectedApp, 'is_beta')->checkbox(); ?>

    <?= $form->field($connectedApp, 'logoFile')->fileInput(['accept' => 'image/*'])->label('Logo photo') ?>
    <?= $form->field($connectedApp, 'logoFileDelete')->checkbox(); ?>

    <?= $form->field($connectedApp, 'url')->textInput(['maxlength' => true]) ?>
    <?= $form->field($connectedApp, 'short_descr')->textInput() ?>
    <?= $form->field($connectedApp, 'info')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>
    <?= $form->field($connectedApp, 'instructions')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>

    <div class="form-group">
        <?= Html::submitButton($connectedApp->isNewRecord ? 'Create' : 'Update', ['class' => $connectedApp->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
