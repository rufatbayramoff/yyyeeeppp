<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $connectedApp common\models\ConnectedApp */

$this->title = 'Update Connected App: ' . $connectedApp->title;
$this->params['breadcrumbs'][] = ['label' => 'Connected Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $connectedApp->title, 'url' => ['view', 'id' => $connectedApp->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="connected-app-update">

    <?= $this->render('_form', [
        'connectedApp' => $connectedApp,
    ]) ?>

</div>
