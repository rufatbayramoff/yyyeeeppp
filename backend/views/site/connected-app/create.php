<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $connectedApp common\models\ConnectedApp */

$this->title = 'Create Connected App';
$this->params['breadcrumbs'][] = ['label' => 'Connected Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-create">

    <?= $this->render('_form', [
        'connectedApp' => $connectedApp,
    ]) ?>

</div>
