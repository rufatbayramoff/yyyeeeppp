<?php

use frontend\components\image\ImageHtmlHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ConnectedApp */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Connected Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connected-app-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(
            'Delete',
            ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ]
        ) ?>
    </p>

    <?php
    echo DetailView::widget(
        [
            'model'      => $model,
            'attributes' => [
                'id',
                'code',
                'title',
                [
                    'label'  => 'Logo File',
                    'format' => 'raw',
                    'value'  => ImageHtmlHelper::getClickableThumb($model->logoFile)
                ],
                [
                    'attribute' => 'url',
                    'format'    => 'url'

                ],
                'short_descr',
                [
                    'attribute' => 'info',
                    'format'    => 'html'

                ],
                [
                    'attribute' => 'instructions',
                    'format'    => 'html'

                ],
            ]
        ]
    ) ?>
</div>
