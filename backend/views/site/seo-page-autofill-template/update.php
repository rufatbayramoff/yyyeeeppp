<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SeoPageAutofillTemplate */

$this->title = 'Update Seo Page Autofill Template: ' . $model->template_name;
$this->params['breadcrumbs'][] = ['label' => 'Seo Page Autofill Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->template_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="seo-page-autofill-template-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
