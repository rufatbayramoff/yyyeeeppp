<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SeoPageAutofillTemplate */
/* @var $form yii\widgets\ActiveForm */
$service = new \common\modules\seo\services\SeoAutofillService();
$types = $service->getTypes();

$placeholders = $service->getPlaceholders();
$result = [];
foreach($placeholders as $type=>$params){
    $result[] = "<b>$type</b>: " . implode(', ', $params);
}
?>
<h4>Do not change template names for pscatalog templates, use the same template names for language versions. Otherwise, it will not work.</h4>
<?php
echo 'Template vars<br/>'  . implode('<br/> ', $result);
?>

<div class="seo-page-autofill-template-form">

    <?php $form = ActiveForm::begin(); ?>


    <?php echo $form->field($model, 'type')->widget(\kartik\select2\Select2::classname(), [
        'data' => array_combine($types, $types),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'template_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'header_text')->textarea(['rows'=>5]) ?>
    <?= $form->field($model, 'footer_text')->textarea(['rows'=>5]) ?>

    <?php echo $form->field($model, 'lang_iso')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->asArray()->all(), 'iso_code', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?= $form->field($model, 'is_default')->checkbox() ?>
    <?= $form->field($model, 'is_apply_created')->label('Apply after creation')->checkbox() ?>
    <small>If checked, new seo pages with this template will be active after creation.</small>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
