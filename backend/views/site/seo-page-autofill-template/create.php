<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SeoPageAutofillTemplate */

$this->title = 'Create Seo Page Autofill Template';
$this->params['breadcrumbs'][] = ['label' => 'Seo Page Autofill Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-page-autofill-template-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
