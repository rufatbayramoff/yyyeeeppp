<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SeoPageAutofillTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seo Page Autofill Templates';
$this->params['breadcrumbs'][] = $this->title;


echo $this->renderFile(Yii::getAlias('@backend/views/site/seo-page/_topTabs.php'));
#dd( yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->asArray()->all(), 'iso_code', 'title' ));
?>
<div class="seo-page-autofill-template-index">


    <p>
        <?= Html::a('Create Template', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $index, $widget, $grid){
            if($model->is_default){
                return [
                    'style'=>'background: #dfefdf;'
                ];
            }
        },
        'columns' => [
            'id',
            [
                'attribute' => 'type',
                'filter' => \common\modules\seo\SeoModule::getTypesCombo()
            ],
            'template_name',
            'title',
            'meta_description',
            'meta_keywords',
            'header',
            'created_at',
            [
                'attribute' => 'lang_iso',
                'filter' => yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->asArray()->all(), 'iso_code', 'title' )
            ],
            [
                'attribute' => 'is_default',
                'format' => 'boolean',
                'filter' =>  Html::activeDropDownList(
                    $searchModel,
                    'is_default',
                    [1=>'Yes', 0=>'No'],
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'is_apply_created',
                'format' => 'boolean',
                'filter' =>  Html::activeDropDownList(
                    $searchModel,
                    'is_apply_created',
                    [1=>'Yes', 0=>'No'],
                    ['class'=>'form-control','prompt' => 'All']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
