<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SeoPageIntl */

$this->title = 'Update Seo Page Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Seo Page Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="seo-page-intl-update">
 
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
