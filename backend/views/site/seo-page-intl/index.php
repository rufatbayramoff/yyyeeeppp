<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SeoPageIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'SEO Page Intls';
$this->params['breadcrumbs'][] = $this->title;
$langs = common\models\SystemLang::find()->all();
echo $this->renderFile(Yii::getAlias('@backend/views/site/seo-page/_topTabs.php'));
?>
<div class="seo-page-intl-index">
 
    <p>
        <?= Html::a('Create Seo Page Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php $form = \yii\widgets\ActiveForm::begin();?>
    <input type="hidden" value="" name="SeoPageActionsIntlForm[submitAction]" id="submitAction"/>
    <input type="hidden" value="" name="SeoPageActionsIntlForm[ids]" id="selectedIdsAction" />
    <?php foreach($seoPageActions as $k=>$v): ?>
        <div class="btn-group js-submit">
            <?php $first = $v[0]; ?>
            <a href="#" data-submit="<?=$first['action'];?>" class="btn btn-sm btn-default"><span class="<?=$first['icon'];?>"></span> <?=$first['title'];?></a>
            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
            <ul class="dropdown-menu" role="menu">
                <?php foreach($v as $k2=>$v2): if($k2===0) continue; ?>
                    <li><a href="#" data-submit="<?=$v2['action'];?>"  ><span class="<?=$v2['icon'];?>"></span><?=$v2['title'];?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endforeach; ?>

    <?php \yii\widgets\ActiveForm::end();?>

    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $index, $widget, $grid){
            if(!$model->is_active){
                return [
                    'style'=>'color:rgba(1,1,1,0.3);'
                ];
            }
        },
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'cssClass' => 'checkcolumn',
                // you may configure additional properties here
            ],
            'id',
            'model_id',
            [
                'attribute' => 'seo_page.url',
                'format' => 'url',
                'value' => function($modelIntl){
                    return param('server') . '/' . $modelIntl->model->url;
                }
            ],
            [
                'attribute' => 'lang_iso',
                'filter' => yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->asArray()->all(), 'iso_code', 'title'),
            ],
            'title',
            'header',
            // 'meta_description',
            // 'meta_keywords',
            // 'header_text:ntext',
            // 'footer_text:ntext',
            [
                'attribute' => 'is_active',
                'format' => 'boolean',
                'filter' =>  Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    [1=>'Yes', 0=>'No'],
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

    <script>
        <?php $this->beginBlock('js1');?>
        function removeA(arr) {
            var what, a = arguments, L = a.length, ax;
            while (L > 1 && arr.length) {
                what = a[--L];
                while ((ax= arr.indexOf(what)) !== -1) {
                    arr.splice(ax, 1);
                }
            }
            return arr;
        }
        $('.js-submit a').on('click', function(){
            $form = $(this).closest("form");
            $('#submitAction').val($(this).data('submit'));
            $form.submit();
        });
        var checked = [];
        $(".checkcolumn").change(function(e) {
            if(this.checked) {
                checked.push(this.value);
            }else{
                removeA(checked, this.value);
            }
            $('#selectedIdsAction').val(checked.join(','));
            $('#selectedIdsApply').val(checked.join(','));
        });
        $('.select-on-check-all').change(function(e){
            checked = [];
            $('input[name="selection[]"]:checked').each(function() {
                checked.push(this.value);
            });
            $('#selectedIdsAction').val(checked.join(','));
            $('#selectedIdsApply').val(checked.join(','));

        });
        <?php $this->endBlock(); ?>
    </script>
<?=$this->registerJs($this->blocks['js1']);?>