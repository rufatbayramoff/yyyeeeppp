<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\HomePageFeatured;

/* @var $this yii\web\View */
/* @var $model HomePageFeatured */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Featureds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-featured-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'is_active',
            'product_category_id',
            'position',
            'productUuids',
            [
                'attribute' => 'Product relation',
                'value' => function (HomePageFeatured $model) {
                    $homePageFeaturedProducts = $model->homePageFeaturedProducts;

                    $res = '';

                    foreach ($homePageFeaturedProducts as $product) {
                        if ($product->product) {
                            $res .= Html::a($product->product->getTitle(), param('siteUrl').$product->product->getPublicPageUrl(), [
                                'class' => 'btn btn-ghost btn-warning',
                                'target' => '_blank',
                                'style' => 'margin-right: 5px'
                            ]);
                        }
                    }

                    return $res;
                },
                'format' => 'raw'
            ],
        ],
    ]) ?>

</div>
