<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageFeatured */

$this->title = 'Update Home Page Featured: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Featureds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-page-featured-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
