<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HomePageFeatured */

$this->title = 'Create Home Page Featured';
$this->params['breadcrumbs'][] = ['label' => 'Home Page Featureds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-featured-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
