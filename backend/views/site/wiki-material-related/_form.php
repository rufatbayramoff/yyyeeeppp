<?php

use common\models\WikiMaterial;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialRelated */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wiki-material-related-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wiki_material_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'link_wiki_material_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(WikiMaterial::find()->where(['<>', 'id', $model->wiki_material_id])->andWhere(['is_active' => 1])->all(), 'id', 'title'),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
