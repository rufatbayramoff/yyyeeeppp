<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WikiMaterialRelatedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wiki Material Relateds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-material-related-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Wiki Material Related', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'wiki_material_id',
            'link_wiki_material_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
