<?php

use backend\controllers\site\SiteHelpController;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpCategoryIntl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-help-category-intl-form">

    <?php $form = ActiveForm::begin();
    if($model->isNewRecord){
        $model->load(app('request')->getQueryParams());
    }
    ?>
    <?php
    echo $form->field($model, 'model_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => SiteHelpController::categoriesList(),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'info')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>

    <?php echo $form->field($model, 'lang_iso')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->where(['!=', 'iso_code', 'en-US'])->asArray()->all(), 'iso_code', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
