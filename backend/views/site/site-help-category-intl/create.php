<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpCategoryIntl */

$this->title = 'Create Site Help Category Intl';
$this->params['breadcrumbs'][] = ['label' => 'Site Help Category Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-help-category-intl-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
