<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpCategoryIntl */

$this->title = 'Update Site Help Category Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Site Help Category Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-help-category-intl-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
