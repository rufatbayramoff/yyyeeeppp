<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteHelpCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Help Categories';
$this->params['breadcrumbs'][] = $this->title;

$langs = common\models\SystemLang::find()->all();
echo $this->renderFile(Yii::getAlias('@backend/views/site/site-help-category/_topTabs.php'));
?>
<div class="site-help-category-index">
 
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [ 

            'id',
            'parent_id',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(\common\models\SiteHelpCategory $model) use($langs){
                    $result = [];
                    $map = \lib\collection\CollectionDb::getMap($model->siteHelpCategoryIntls, 'lang_iso');
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->title : false;
                        if($val){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['site/site-help-category-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['site/site-help-category-intl/create',
                                'SiteHelpCategoryIntl'=>['model_id'=>$model->id, 'lang_iso'=>$v->iso_code]]);
                        }
                    }
                    $result = Html::tag("h5", $model->title) .  implode("<br />", $result);
                    return $result;
                }
            ],
            'slug',
            'priority',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
