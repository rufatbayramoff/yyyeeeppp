<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpCategory */

$this->title = 'Create Site Help Category';
$this->params['breadcrumbs'][] = ['label' => 'Site Help Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-help-category-create">
 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
