<?php

use backend\controllers\site\SiteHelpController;
use frontend\components\image\ImageHtmlHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-help-category-form">

    <?php $form = ActiveForm::begin(); ?>
 
 <?php

 echo $form->field($model, 'parent_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => SiteHelpController::categoriesList(),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
 ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'info')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'priority')->textInput(['type' => 'number']) ?>

    <!-- COVER FILE -->

    <?= $form->field($model, 'coverFile')->fileInput(['accept' => '.jpg,.jpeg,.png,.gif']) ?>
    <?php if($model->coverFile) :?>
        <div>
            <?= Html::img(ImageHtmlHelper::getThumbUrlForFile($model->coverFile, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL)); ?>
        </div>
        <br/>
        <br/>
    <?php endif; ?>


    <!-- END COVER FILE -->


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
