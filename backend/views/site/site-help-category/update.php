<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpCategory */

$this->title = 'Update Site Help Category: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Site Help Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-help-category-update">
 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
