<?php

use frontend\components\image\ImageHtmlHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMachine */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(
            'Delete',
            ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ]
        ) ?>
    </p>

    <?php
    $photoFilesList = '';
    $filesList = '';
    foreach ($model->photoFiles as $file) {
        $photoFilesList .= ImageHtmlHelper::getClickableThumb($file) . ' ';
    }
    foreach ($model->files as $file) {
        $filesList .= '<div>' . HTML::a($file->getFileName(), $file->getFileUrl()) . '</div>';
    }
    echo DetailView::widget(
        [
            'model'      => $model,
            'attributes' => [
                'id',
                'equipment_category_id',
                'code',
                'title',
                [
                    'label'  => 'Main Photo File',
                    'format' => 'raw',
                    'value'  => ImageHtmlHelper::getClickableThumb($model->mainPhotoFile)
                ],
                'description:ntext',
                'web_site_url:url',
                [
                    'label'  => 'Photos',
                    'format' => 'raw',
                    'value'  => $photoFilesList
                ],
                [
                    'label'  => 'Files',
                    'format' => 'raw',
                    'value'  => $filesList
                ],
            ],
        ]
    ) ?>
    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">Properties</label>
        <div>
            <?= GridView::widget(
                [
                    'dataProvider' => $dataProvider = new ActiveDataProvider(['query' => $model->getWikiMachineProperties()]),
                    'columns'      => [
                        'code',
                        'title',
                        'value',
                    ]
                ]
            );
            ?>
        </div>
    </div>

</div>
