<?php

use common\components\ArrayHelper;
use common\models\EquipmentCategory;
use common\models\WikiMachine;
use frontend\components\image\ImageHtmlHelper;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WikiMachineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wiki Machines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-index">
    <p>
        <?= Html::a('Create Wiki Machine', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                [
                    'attribute' => 'equipment_category_id',
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'equipment_category_id',
                        ArrayHelper::map(EquipmentCategory::find()->withoutRoot()->all(), 'id', 'title'),
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                    'value'     => function (WikiMachine $wikiMachine) {
                        return $wikiMachine->equipmentCategory->title;
                    }
                ],

                'code',
                'title',
                [
                    'header' => 'Main Photo',
                    'format' => 'raw',
                    'value'  => function (WikiMachine $wikiMachine) {
                        return ImageHtmlHelper::getClickableThumb($wikiMachine->mainPhotoFile);
                    }
                ],
                // 'description:ntext',
                // 'web_site_url:url',
                [
                    'class'          => ActionColumn::class,
                    'buttons'        => Yii::$app->getModule('translation')->dbI18n->getGridButton('wiki_machine'),
                    'template'       => '{translate} &nbsp; {view} &nbsp; {update} &nbsp; {delete} ',
                    'contentOptions' => ['style' => 'width: 100px;']
                ]
            ],
        ]
    ); ?>
</div>
