<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $wikiMachine common\models\WikiMachine */

$this->title = 'Create Wiki Machine';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-create">

    <?= $this->render('_form', [
        'wikiMachine' => $wikiMachine,
    ]) ?>

</div>
