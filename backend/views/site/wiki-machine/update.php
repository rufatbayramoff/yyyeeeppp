<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $wikiMachine common\models\WikiMachine */

$this->title = 'Update Wiki Machine: ' . $wikiMachine->title;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $wikiMachine->title, 'url' => ['view', 'id' => $wikiMachine->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wiki-machine-update">

    <?= $this->render('_form', [
        'wikiMachine' => $wikiMachine,
    ]) ?>

</div>
