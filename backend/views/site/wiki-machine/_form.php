<?php

use backend\assets\AngularAppAsset;
use backend\widgets\FilesListWidget;
use common\components\ArrayHelper;
use common\components\FileTypesHelper;
use common\models\EquipmentCategory;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $wikiMachine common\models\WikiMachine */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="wiki-machine-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($wikiMachine, 'equipment_category_id')->dropDownList(ArrayHelper::map(EquipmentCategory::find()->withoutRoot()->all(), 'id', 'titleWithCode')) ?>

    <?= $form->field($wikiMachine, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($wikiMachine, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($wikiMachine, 'mainPhotoFile')->fileInput(['accept' => 'image/*'])->label('Main Photo') ?>

    <div class="form-group field-wikimachine-deletedmainphotofile">
        <label><input type="checkbox" id="wikimachine-deletedmainphotofile" name="WikiMachine[deleteMainPhotoFile]"> Delete Main Photo File</label>
        <div class="help-block"></div>
    </div>

    <?= $form->field($wikiMachine, 'description')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults())->textarea(['rows' => 6]) ?>

    <?= $form->field($wikiMachine, 'web_site_url')->textInput(['maxlength' => true]) ?>

    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">Photos</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'formPrefix'             => $wikiMachine->formName(),
                    'formAttribute'          => 'photoFiles',
                    'filesList'              => $wikiMachine->photoFiles,
                    'rights'                 => [
                        FilesListWidget::ALLOW_DELETE,
                        FilesListWidget::ALLOW_ADD,
                        FilesListWidget::ALLOW_MULTIPLE,
                        FilesListWidget::ALLOW_ROTATE
                    ],
                    'filesAllowedExtensions' => FileTypesHelper::ALLOW_IMAGES_EXTENSIONS
                ]
            ) ?>
        </div>
    </div>

    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">Files</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'formPrefix'             => $wikiMachine->formName(),
                    'formAttribute'          => 'files',
                    'filesList'              => $wikiMachine->files,
                    'rights'                 => [
                        FilesListWidget::ALLOW_DELETE,
                        FilesListWidget::ALLOW_ADD,
                        FilesListWidget::ALLOW_MULTIPLE,
                    ],
                    'filesAllowedExtensions' => FileTypesHelper::ALLOW_DOCS_EXTENSIONS
                ]
            ) ?>
        </div>
    </div>

    <?php
    if ($wikiMachine->id) {
        ?>
        <div class="form-group field-storeorderreview-comment">
            <label class="control-label" for="storeorderreview-comment">Properties</label>
            <div>
                <?= GridView::widget(
                    [
                        'dataProvider' => $dataProvider = new ActiveDataProvider(['query' => $wikiMachine->getWikiMachineProperties()]),
                        'columns'      => [
                            'code',
                            'title',
                            'value',
                            [
                                'class'   => ActionColumn::class,
                                'buttons' =>
                                    array_merge(
                                        Yii::$app->getModule('translation')->dbI18n->getGridButton('wiki_machine_property'),
                                        [
                                            'update' => function ($url, $model) {
                                                $urlRes = Html::a(
                                                    '<span class="glyphicon glyphicon-pencil"></span>',
                                                    '/site/wiki-machine-property/update?id=' . $model->id,
                                                    [
                                                        'class'       => 'btn-ajax-modal',
                                                        'title'       => 'Update',
                                                        'data-target' => '#addMaterial',
                                                        'value'       => '/site/wiki-machine-property/update?id=' . $model->id,
                                                    ]
                                                );
                                                return $urlRes;
                                            },
                                            'delete' => function ($url, $model) {
                                                $urlRes = Html::a(
                                                    '<span class="glyphicon glyphicon-trash"></span>',
                                                    '/site/wiki-machine-property/delete?id=' . $model->id,
                                                    [
                                                        'title'        => 'Delete',
                                                        'data-confirm' => 'Are you sure you want to delete this item?',
                                                        'data-method'  => 'post'
                                                    ]
                                                );
                                                return $urlRes;
                                            }
                                        ]
                                    ),

                                'template' => '{translate} &nbsp; {update} &nbsp; {delete} ',
                            ]
                        ]
                    ]
                );
                ?>
                <?= Html::button(
                    'Add property',
                    [
                        'class'       => 'btn btn-success btn-ajax-modal',
                        'title'       => 'Add property',
                        'data-target' => '#addMaterial',
                        'value'       => '/site/wiki-machine-property/create?wikiMachineId=' . $wikiMachine->id
                    ]
                ) ?>
            </div>
        </div>
        <a id="wikiMachineProperties" name="wikiMachineProperties"></a>
        <script>
            if (window.location.href.indexOf('wikiMachineProperties') !== -1) {
                $(document).ready(function () {
                    setTimeout(function () {
                        window.location.href = window.location.href;
                    }, 1000);

                });
            }
        </script>
        <?php
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($wikiMachine->isNewRecord ? 'Create' : 'Update', ['class' => $wikiMachine->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
