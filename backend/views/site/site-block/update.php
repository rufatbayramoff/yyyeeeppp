<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteBlock */

$this->title = 'Update Site Block: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Site Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-block-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
