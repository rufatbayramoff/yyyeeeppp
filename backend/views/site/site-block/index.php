<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteBlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Blocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-block-index">
    <p>
        <?= Html::a('Create Site Block', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

 <?php \yii\widgets\Pjax::begin(['options' => ['class' => 'pjax-wraper']]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'code',
            'description',
            //'content:ntext',
            [
                'attribute' => 'is_active',
                'format' => 'raw',
                'value' => function($model) {
                    $url = \yii\helpers\Url::toRoute(['site/site-block/change-active', 'id' => $model->id]);
                    $val = $model->is_active ? 'Yes' : 'No';
                    $kVal = $model->is_active ? 'ok' : 'remove';
                    return Html::a(sprintf('<span class="glyphicon glyphicon-%s"></span> %s',$kVal, $val), $url, [
                            'title' => 'Change',
                            'data-pjax' => '0',
                            'class' => 'grid-action'
                    ]);
                }
            ], 
            // 'config',
            'updated_at:datetime',
            'language',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
    Usage example: 
    <pre>
        $block = lib\site\Helper::getBlock('model3d.upload.help');
    </pre>
</div>
