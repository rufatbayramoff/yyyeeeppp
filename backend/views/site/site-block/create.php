<?php



/* @var $this yii\web\View */
/* @var $model common\models\SiteBlock */

$this->title = 'Create Site Block';
$this->params['breadcrumbs'][] = ['label' => 'Site Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-block-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
