<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MsgFolderIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Msg Folder Intls';
$this->params['breadcrumbs'][] = $this->title;
echo $this->renderFile(Yii::getAlias('@backend/views/site/msg-folder/_topTabs.php'));
?>
<div class="msg-folder-intl-index">


    <p>
        <?= Html::a('Create Msg Folder Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model_id',
            'lang_iso',
            'title',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
