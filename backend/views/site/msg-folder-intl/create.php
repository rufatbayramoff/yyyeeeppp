<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MsgFolderIntl */

$this->title = 'Create Msg Folder Intl';
$this->params['breadcrumbs'][] = ['label' => 'Msg Folder Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-folder-intl-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
