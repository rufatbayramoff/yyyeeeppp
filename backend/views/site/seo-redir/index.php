<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SeoRedirSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seo Redirs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-redir-index">

    <p>
        <?= Html::a('Create Seo Redir', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'caption'      => sprintf(
                '<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
                new \backend\components\GridViewDataExporter($searchModel),
                \backend\widgets\ExcelImportWidget::widget(['tableName' => $searchModel->tableName()])
            ),
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'attribute' => 'from_url',
                    'format' => 'raw',
                    'value' => function($model){
                        return Html::a($model->from_url, param('server') . '/' . $model->from_url, ['target'=>'_blank']);
                    }
                ],
                [
                    'attribute' => 'to_url',
                    'format' => 'raw',
                    'value' => function($model){
                        return Html::a($model->to_url, param('server') . '/' . $model->to_url, ['target'=>'_blank']);
                    }
                ],
                'created_at',
               // 'user_admin_id',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]
    ); ?>
</div>
