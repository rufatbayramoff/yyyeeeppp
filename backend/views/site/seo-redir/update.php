<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SeoRedir */

$this->title = 'Update Seo Redir: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Seo Redirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="seo-redir-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
