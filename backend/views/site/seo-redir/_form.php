<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SeoRedir */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seo-redir-form">

    <?php $form =  ActiveForm::begin(['layout'=>'horizontal']); ?>

    <?= $form->field($model, 'from_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_url')->textInput(['maxlength' => true]) ?>


    <div class=" col-lg-offset-3" >
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
