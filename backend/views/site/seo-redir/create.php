<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SeoRedir */

$this->title = 'Create Seo Redir';
$this->params['breadcrumbs'][] = ['label' => 'Seo Redirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-redir-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
