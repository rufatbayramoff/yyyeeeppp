<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentCurrency */

$this->title = 'Create Payment Currency';
$this->params['breadcrumbs'][] = ['label' => 'Payment Currencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-currency-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
