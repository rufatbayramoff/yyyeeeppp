<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentCurrencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Currencies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-currency-index">

    <p>
        <?= Html::a('Create Payment Currency', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'is_active',
            'currency_iso',
            'title_original',
            // 'round',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
