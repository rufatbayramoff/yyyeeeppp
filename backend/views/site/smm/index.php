<?php
use yii\widgets\ActiveForm;

$this->title = 'SMM - Store Unit Blocks';
 ?>
 
 <?php $form = ActiveForm::begin() ?>
 	Product Ids (for example: 126,125): <input type="text" name="product_ids" />
 	
 	<input type="submit" value="Generate HTML" />
 <?php $form->end() ?>
 
 
 <?php if(!empty($products)): ?>
     <textarea style="width:100%; min-height:250px;">
     <?php echo $this->render('storeUnits.php', compact('products')); ?>
     </textarea>
     
     <h2>Preview</h2> 
     <style>
         .collapse{
             display: block!important;
         }
         table.collapse{
             display: inherit!important;
         }
    </style>
      <?php echo $this->render('storeUnits.php', compact('products')); ?>     
 <?php endif; ?>