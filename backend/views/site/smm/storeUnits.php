<?php
use frontend\components\UserUtils;
?>

<div style="height:100%; margin:0; padding:0; width:100%; background-color:#f4f7fc">
  <center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse; height:100%; margin:0; padding:0; width:100%; background-color:#f4f7fc">
      <tbody>
      <tr>
        <td align="center" valign="top" style="height:100%; margin:0; padding:0; width:100%; border:0">
          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse; border:0; max-width:600px!important; background-color:#f4f7fc;">
            <tbody>
            <tr>
              <!-- BODY -->
              <td valign="top" style="background-color:#f4f7fc; border:0; padding:0 0 0 0;">
              <?php

              for($i = 0; $i < count($products); $i++):
              $product1 = $products[$i]; $i++;
              if(!isset($products[$i])){
                  continue;
              }
              $product2 = $products[$i];

              ?>
                <table cellpadding="0" cellspacing="0" width="100%" border="0" style="min-width:100%; border-collapse:collapse">
                  <tbody>
                  <tr>
                    <td valign="top" style=" padding:0 8px 0 16px; width: 50%;">

                      <table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="max-width:100%; min-width:100%; border-collapse:collapse;">
                        <tbody>
                        <tr>

                          <td valign="top" style="color:#333333; font-size:14px; line-height:1.4; text-align:center; width: 100%;">
                              <a href="https://www.treatstock.com/3dmodels/<?= $product1['id']; ?>" target="_blank" style="display: block; margin: 0 0 15px; color: #ffffff; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; line-height: 1; padding: 0; text-align: center; text-decoration: none; overflow: hidden; border-radius: 5px 5px 5px 5px;">
                                  <img alt="" src="<?php echo $product1['img']; ?>" style="-ms-interpolation-mode: bicubic; border: none; display: block; height: auto; margin: 0 0 0 0; max-width: 100%; outline: none; text-decoration: none; width: 100%; border-radius: 5px 5px 5px 5px;" width="100%" height="auto">
                              </a>

                            <h4 style="line-height:20px; margin: 0 0 5px; padding:0; color:#404448; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-size:14px; font-weight: bold; text-align: left;">
                                <?php echo $product1['title']; ?>
                            </h4>

                            <div style="line-height:20px; margin: 0 0 15px; padding:0; color: #2d8ee0; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-size:14px; font-weight: bold; text-align: left;">
                                <a href="<?= UserUtils::getUserPublicProfileUrl(\common\models\User::findByPk($product1['author_id']))?>" target="_blank" style="margin: 0 5px 0 0; color: #2d8ee0; display: inline-block; vertical-align: middle; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; line-height: 20px; padding: 0; text-align: left; text-decoration: none; width: 20px; height: 20px; overflow: hidden; border-radius: 50% 50% 50% 50%;">
                                    <img alt="" src="<?php echo $product1['author_avatar']; ?>" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; max-width: 20px; outline: none; width: 20px; height: 20px; min-height: 20px;" width="20px" height="20px">
                                </a>
                                <a href="<?= UserUtils::getUserPublicProfileUrl(\common\models\User::findByPk($product1['author_id']))?>" target="_blank" style="margin: 0; color: #2d8ee0; display: inline-block; vertical-align: middle; font-family: 'Helvetica Neue', 'Arial', sans-serif; line-height: 20px; padding: 0; text-align: left; text-decoration: none;">
                                    <span style="margin: 0; color: #2d8ee0; display: inline-block; vertical-align: middle; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: bold; line-height: 20px; padding: 0; text-align: left; font-size: 14px;">
                                        <?php echo $product1['author']; ?>
                                    </span>
                                </a>
                            </div>

                            <a href="https://www.treatstock.com/3dmodels/<?php echo $product1['id']; ?>" target="_blank" style="display: block; margin: 0 0 40px; background: #e00457; border: none; border-radius: 5px; color: #ffffff; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-size: 16px; font-weight: bold; line-height: 20px; padding: 10px 10px 10px 10px; text-align: center; text-decoration: none; word-wrap: normal; word-break:keep-all; white-space:nowrap;">
                                Print&nbsp;It
                            </a>
                          </td>

                        </tr>
                        </tbody>
                      </table>

                    </td>

                    <td valign="top" style=" padding:0 16px 0 8px; width: 50%;">

                      <table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="max-width:100%; min-width:100%; border-collapse:collapse;">
                        <tbody>
                        <tr>

                          <td valign="top" style="color:#333333; font-size:14px; line-height:1.4; text-align:center; width: 100%;">
                              <a href="https://www.treatstock.com/3dmodels/<?= $product2['id']; ?>" target="_blank" style="display: block; margin: 0 0 15px; color: #ffffff; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; line-height: 1; padding: 0; text-align: center; text-decoration: none; overflow: hidden; border-radius: 5px 5px 5px 5px;">
                                  <img alt="" src="<?php echo $product2['img']; ?>" style="-ms-interpolation-mode: bicubic; border: none; display: block; height: auto; margin: 0 0 0 0; max-width: 100%; outline: none; text-decoration: none; width: 100%; border-radius: 5px 5px 5px 5px;" width="100%" height="auto">
                              </a>

                            <h4 style="line-height:20px; margin: 0 0 5px; padding:0; color:#404448; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-size:14px; font-weight: bold; text-align: left;">
                                <?php echo $product2['title']; ?>
                            </h4>

                            <div style="line-height:20px; margin: 0 0 15px; padding:0; color: #2d8ee0; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-size:14px; font-weight: bold; text-align: left;">
                                <a href="<?= UserUtils::getUserPublicProfileUrl(\common\models\User::findByPk($product2['author_id']))?>" target="_blank" style="margin: 0 5px 0 0; color: #2d8ee0; display: inline-block; vertical-align: middle; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; line-height: 20px; padding: 0; text-align: left; text-decoration: none; width: 20px; height: 20px; overflow: hidden; border-radius: 50% 50% 50% 50%;">
                                    <img alt="" src="<?php echo $product2['author_avatar']; ?>" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; max-width: 20px; outline: none; width: 20px; height: 20px; min-height: 20px;" width="20px" height="20px">
                                </a>
                                <a href="<?= UserUtils::getUserPublicProfileUrl(\common\models\User::findByPk($product2['author_id']))?>" target="_blank" style="margin: 0; color: #2d8ee0; display: inline-block; vertical-align: middle; font-family: 'Helvetica Neue', 'Arial', sans-serif; line-height: 20px; padding: 0; text-align: left; text-decoration: none;">
                                    <span style="margin: 0; color: #2d8ee0; display: inline-block; vertical-align: middle; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: bold; line-height: 20px; padding: 0; text-align: left; font-size: 14px;">
                                        <?php echo $product2['author']; ?>
                                    </span>
                                </a>
                            </div>

                            <a href="https://www.treatstock.com/3dmodels/<?php echo $product2['id']; ?>" target="_blank" style="display: block; margin: 0 0 40px; background: #e00457; border: none; border-radius: 5px; color: #ffffff; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-size: 16px; font-weight: bold; line-height: 20px; padding: 10px 10px 10px 10px; text-align: center; text-decoration: none; word-wrap: normal; word-break:keep-all; white-space:nowrap;">
                                Print&nbsp;It
                            </a>
                          </td>

                        </tr>
                        </tbody>
                      </table>

                    </td>
                  </tr>
                  </tbody>
                </table>
              <?php endfor; ?>
              </td>
            </tr>
            </tbody>
          </table>
        </td>
      </tr>
      </tbody>
    </table>
  </center>
</div>