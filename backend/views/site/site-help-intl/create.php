<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpIntl */

$this->title = 'Create Site Help Intl';
$this->params['breadcrumbs'][] = ['label' => 'Site Help Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-help-intl-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
