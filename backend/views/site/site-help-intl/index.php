<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteHelpIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Help Translates';
$this->params['breadcrumbs'][] = $this->title;

echo $this->renderFile(Yii::getAlias('@backend/views/site/site-help/_topTabs.php'));
?>
<div class="site-help-intl-index">

    <p>
        <?= Html::a('Create Site Help Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model_id',
            'title',
            'content:ntext',
            'lang_iso',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
