<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpIntl */

$this->title = 'Update Site Help Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Site Help Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-help-intl-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
