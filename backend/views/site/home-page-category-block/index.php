<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\HomePageCategoryBlock;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\HomePageCategoryBlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var \common\models\HomePageCategoryBlock $model
 */

$this->title = 'Home Page Category Blocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-category-block-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Home Page Category Block', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'is_active',
            'product_category_id',
            [
                'attribute' => 'title',
                'value'     => function (HomePageCategoryBlock $model) {
                    return $model->getTitleText();
                }
            ],
            [
                'attribute' => 'url',
                'value'     => function (HomePageCategoryBlock $model) {
                    if (!empty($model->url)) {
                        return $model->url;
                    } elseif ($productCategory = $model->productCategory) {
                        return '/products/'.$productCategory->code;
                    } else {
                        return '';
                    }
                }
            ],
            'position',
            [
                'class'          => \yii\grid\ActionColumn::class,
                'buttons'        => Yii::$app->getModule('translation')->dbI18n->getGridButton(HomePageCategoryBlock::tableName()),
                'template'       => '{translate} &nbsp; {view} &nbsp; {update} &nbsp; {delete} ',
                'contentOptions' => ['style' => 'width: 120px;']
            ]
        ],
    ]); ?>
</div>
