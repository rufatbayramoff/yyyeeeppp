<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryBlock */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Category Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-category-block-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'is_active',
            'product_category_id',
            [
                'attribute' => 'title',
                'value'     => function (\common\models\HomePageCategoryBlock $model) {
                    return $model->getTitleText();
                }
            ],
            [
                'attribute' => 'url',
                'value'     => function (\common\models\HomePageCategoryBlock $model) {
                    if (!empty($model->url)) {
                        return $model->url;
                    } elseif ($productCategory = $model->productCategory) {
                        return '/products/'.$productCategory->code;
                    } else {
                        return '';
                    }
                },
            ],
            'position',
        ],
    ]) ?>

</div>
