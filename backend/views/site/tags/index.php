<?php

use common\models\SiteTag;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \backend\models\search\SiteTagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site tags';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'created_at',
            'text',
            'status' => [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    [
                        SiteTag::STATUS_NEW => 'Active',
                        SiteTag::STATUS_INACTIVE => 'inactive',
                    ],
                    ['class'=>'form-control','prompt' => 'All']),
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>

</div>
