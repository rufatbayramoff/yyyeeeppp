<?php

use common\models\SiteTag;
use common\models\SiteTagLink;
use kartik\select2\Select2;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $siteTag common\models\SiteTag */

$this->title                   = 'Site tag: ' . ' ' . $siteTag->text;
$this->params['breadcrumbs'][] = ['label' => 'Site tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $siteTag->text, 'url' => ['update', 'id' => $siteTag->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="system-lang-update">

    <div class="system-lang-form">
        <div style="max-width: 500px">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($siteTag, 'text')->textInput(['maxlength' => true]) ?>
            <?= $form->field($siteTag, 'status')->dropDownList(['new' => 'Active', 'inactive' => 'Inactive']) ?>
            <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
            </div>
            <h3>Linked site tags</h3>

            <?php
            $linkedTags   = $siteTag->getLikedTags();
            $dataProvider = new ArrayDataProvider(
                [
                    'key'        => 'id',
                    'allModels'  => $linkedTags,
                    'pagination' => [
                        'pageSize' => 10000,
                    ],
                ]
            );
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns'      => [
                     [
                            'attribute' => 'text',
                            'value' => function (SiteTag $siteTag) {
                                return '<a href="/site/site-tag/update?id='.$siteTag->id.'">'.$siteTag->text.'</a>';
                            },
                            'format' => 'raw'
                    ],
                    'status',
                    [
                            'attribute' => 'id',
                            'value' => function (SiteTag $siteTagTow) use ($siteTag) {
                                return '<a href="/site/site-tag/delete-link?site_tag_id='.$siteTag->id.'&site_tag_tow_id='.$siteTagTow->id.'" title="Delete" aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?" data-method="post"><svg aria-hidden="true" style="display:inline-block;font-size:inherit;height:1em;overflow:visible;vertical-align:-.125em;width:.875em" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M32 464a48 48 0 0048 48h288a48 48 0 0048-48V128H32zm272-256a16 16 0 0132 0v224a16 16 0 01-32 0zm-96 0a16 16 0 0132 0v224a16 16 0 01-32 0zm-96 0a16 16 0 0132 0v224a16 16 0 01-32 0zM432 32H312l-9-19a24 24 0 00-22-13H167a24 24 0 00-22 13l-9 19H16A16 16 0 000 48v32a16 16 0 0016 16h416a16 16 0 0016-16V48a16 16 0 00-16-16z"></path></svg></a>';
                            },
                            'format' => 'raw'
                    ]
                ],
            ]); ?>

            <?php ActiveForm::end(); ?>
            <?php $form = ActiveForm::begin(); ?>
            <div>Link with tag</div>
            <div class="form-group">
                <?php echo Select2::widget([
                    'name'          => 'link_tag',
                    'pluginOptions' => [
                        'allowClear'         => true,
                        'minimumInputLength' => 2,
                        'ajax'               => [
                            'url'      => Url::to(['/site/site-tag/search']),
                            'dataType' => 'json',
                            'data'     => new yii\web\JsExpression('function(params) { return {q:params.term}; }')
                        ],
                    ],
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
