<?php

/* @var $this yii\web\View */
/* @var $model common\models\StaticPage */

$this->title = 'Update Static Page: ' . $model->alias;
$this->params['breadcrumbs'][] = ['label' => 'Static Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->alias, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="static-page-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
