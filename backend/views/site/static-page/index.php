<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StaticPageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Static Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="static-page-index">
    <p>
        <?= Html::a('Create Static Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'alias',
            'title',
            [
                'label' => 'Translates',
                'value' => function(\common\models\StaticPage $page)
                {
                    $languages = \common\components\ArrayHelper::getColumn($page->staticPageIntls, 'lang_iso');
                    return implode(', ', $languages);
                }
            ],
            [
                'class' => \yii\grid\ActionColumn::class,
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
