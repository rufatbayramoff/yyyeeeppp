<?php

use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StaticPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="static-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model)?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?php


    /** @var \common\models\SystemLang[] $languages */
    $languages = \common\models\SystemLang::getAllActive();

    ?>

    <div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ($languages as $k => $language):
                $languageId = $language->id;
                ?>
                <li role="presentation" class="<?= ($k == 0 ? 'active' : '')?>"><a href="#<?=$languageId?>" aria-controls="<?=$languageId?>" role="tab" data-toggle="tab"><?= $language->iso_code?></a></li>
            <?php endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php foreach ($languages as $k => $language):
                $languageId = $language->id;
                $isDefaultLang = $language->iso_code == 'en-US';
                ?>
                <div role="tabpanel" class="tab-pane <?= ($k == 0 ? 'active' : '')?>" id="<?=$languageId?>">
                    <br/>
                    <div class="row">
                        <div class="col-md-10">
                            <?=
                                $form->field($model, $isDefaultLang ? 'title' : 'title_'.$languageId)
                                    ->label('Title')
                                    ->textInput(['maxlength' => 255, 'class' => 'title-input form-control']);
                            ?>
                        </div>
                        <div class="col-md-2">
                            <button
                                type="button"
                                class="btn btn-warning btn-sm clear-lang-button"
                                style="margin-top:26px"
                                data-lang="<?= $language->iso_code?>">Clear language</button>
                        </div>
                    </div>


                    <?=
                        $form->field($model, $isDefaultLang ? 'content' : 'content_'.$languageId)
                            ->label('Content')
                            ->widget(\dosamigos\ckeditor\CKEditor::class, \backend\components\CKEditorDefault::getDefaults());
                    ?>
                </div>
            <?php endforeach; ?>
        </div>

    </div>

    <br/>
    <br/>
    <br/>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<style type="text/css">

.tab-pane{
    background: white;
    padding: 20px;
    border-radius: 0 0 6px 6px;
    border: solid #ddd;
    border-width: 0 1px 1px 1px;
}
</style>


<script type="text/javascript">

$(function () {
    $('.clear-lang-button').on('click', function () {

        if(!confirm('Are you sure you want to delete the translation? It will be removed even without pressing Save button.')){
            return;
        }

        var $this = $(this);

        // clear form data
        $this.closest('.tab-pane').find('.title-input').val('');
        CKEDITOR.instances[$this.closest('.tab-pane').find('.content-input').attr('id')].setData('');

        <?php if(!$model->isNewRecord):?>
            // delete translates from server
            var lang = $(this).data('lang');
            $.post('/site/static-page/clear-language?id=<?= $model->id?>&lang=' + lang);
        <?php endif; ?>


    })
})




</script>