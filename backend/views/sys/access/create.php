<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Access */

$this->title = Yii::t('app', 'Create Access');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accesses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="access-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
