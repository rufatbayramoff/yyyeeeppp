<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Access */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Access',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accesses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="access-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
