<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url; 
use yii\bootstrap\Modal;  

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AccessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Accesses');
$this->params['breadcrumbs'][] = $this->title;

\kartik\select2\Select2Asset::register($this);
 
 

?>
<div class="access-index">
    <p>
        <?= Html::a(Yii::t('app', 'Create Access'), ['create'], ['class' => 'btn btn-success']) ?>
            <?php
                echo Html::button('Create group', [
                    'class' => 'btn btn-default btn-ajax-modal',
                    'value' => Url::to('@web/access/create-group'),
                    'data-target' => '#modal_addgroups',
                ]); 
            ?>
        
            <?php
                echo Html::button('Add User Access', [
                    'class' => 'btn btn-default btn-ajax-modal btn-modal-autoclose',
                    'value' => Url::to('@web/user-access/create'),
                    'data-target' => '#modal_createua', 
                ]); 
                 
            ?>
    </p>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'groupTitle',
                'value' => 'group.title',
                'filter' => Html::activeDropDownList(
                    $searchModel, 
                    'group_id', 
                    yii\helpers\ArrayHelper::map(common\models\AccessGroup::find()->asArray()->all(), 'id', 'title' ),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'code',
            'title',
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    
    
    ?> 
</div>
