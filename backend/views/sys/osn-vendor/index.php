<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OsnVendorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Osn Vendors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="osn-vendor-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Osn Vendor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
 <?php \yii\widgets\Pjax::begin(['options' => ['class' => 'pjax-wraper']]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
 
            'title',
            'code',
            'icon',
             [
                'attribute' => 'is_active',
                'format' => 'raw',
                'value' => function($model) {
                    $url = \yii\helpers\Url::toRoute(['/osn-vendor/change-active', 'id' => $model->id]);
                    $val = $model->is_active ? 'Yes' : 'No';
                    $kVal = $model->is_active ? 'ok' : 'remove';
                    return Html::a(sprintf('<span class="glyphicon glyphicon-%s"></span> %s',$kVal, $val), $url, [
                            'title' => 'Change',
                            'data-pjax' => '0',
                            'class' => 'grid-action'
                    ]);
                }
            ], 
            'app_id',
            // 'app_key',
            // 'app_config',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>
</div>
