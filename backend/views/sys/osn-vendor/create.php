<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OsnVendor */

$this->title = 'Create Osn Vendor';
$this->params['breadcrumbs'][] = ['label' => 'Osn Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="osn-vendor-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
