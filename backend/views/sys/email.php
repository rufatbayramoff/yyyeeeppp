
<?php
if (!empty($email)) {
    echo '<pre>' . $email . '</pre>';
}

?>

 <p>
        <?= \yii\helpers\Html::a('Clear emails', ['clear'], ['class' => 'btn btn-danger']) ?>
    </p>
    
<?php
$provider = new yii\data\ArrayDataProvider([
    'allModels' => $emails,
    'sort' => [
        'attributes' => ['id', 'size', 'file', 'date'],
        'defaultOrder' => ['date' => SORT_DESC],
    ],
    'pagination' => [
        'pageSize' => 30,
    ],
]);

echo yii\grid\GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        [
            'attribute' => 'file',
            'format' => 'raw',
            'value' => function ($data) {
                $url = yii\helpers\Url::to('@web/site/email-reader/read/?file=' . $data['file']);
                return yii\helpers\Html::a($data['file'], $url);
            },
        ],
        'date:datetime',
        'size',
    ],
]);
