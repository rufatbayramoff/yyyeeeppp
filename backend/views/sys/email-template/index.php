<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EmailTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Email Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-template-index">
   
    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'code',
            'group',
            [
                'attribute' => 'language_id',
                'value' => 'language_id',
                'filter' => Html::activeDropDownList(
                    $searchModel, 
                    'language_id', 
                        \yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->asArray()->all(), 'iso_code', 'title'),
                        ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value'=> function($data){
                    return '<b>' . $data->title . "</b><br/> <small>". $data->description . " </small>";
                }
            ],
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    [1 => 'Active', 0 => 'Inactive'],
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            // 'updated_at',
            // 'template_html:ntext',
            'template_text:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
