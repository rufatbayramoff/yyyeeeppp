<?php

use common\components\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\EmailTemplate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-template-form">

    <?php $form = ActiveForm::begin(['layout'=>'horizontal']); ?>
    <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
    <?= $form->field($model, 'group')->dropDownList(ArrayHelper::merge(['system' => 'System'], lib\message\Constants::getGroupLabels()))?>
    <?php echo $form->field($model, 'language_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->asArray()->all(), 'iso_code', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'template_html')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'template_text')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'template_sms')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        
        <label class="control-label col-sm-4"></label>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
