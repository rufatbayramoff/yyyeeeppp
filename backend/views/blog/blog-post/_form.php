<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BlogPost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-post-form">


    <pre style="border-color:red;">
    <?php
    echo htmlspecialchars('
All the paragraphs must be within <p> tags. They are automatically created, but they should be checked before publishing in “Source” mode.
Use “Paste plane text” button for inserting texts
All external links (to other sites) should be like: <a href="link" rel="nofollow">Name of link</a>

Tiles: Use only tags <h2>, <h3>

Images

    Preview - images for blog articles should be 340х191px. Proportion of 16:9.
    Big image for the top of article - 1600х900px. Proportion of 16:9.
    In the article, before publishing in “source” mode, all the images should have the following parameters: width:100%; height: auto; style="width: 100%; height: auto;"
    The quality of the image for Photoshop: JPG quality 71. Not more than 1000px wide and 600px height.

Video

    HTML code from "youtube.com" should be like that:
    
    <div class="video-container">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/CYijJ4rrktw?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>'); ?>
    </pre>
    <?php $form = ActiveForm::begin(); ?>


    <?= $form->errorSummary($model)?>


    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
        ],
    ]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'alias')->hint("Must contains \"A-Z, a-z, 0-9, -\" characters")->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>



    <?= $form->field($model, 'imagePreviewFileUploaded')->fileInput() ?>
    <?= $form->field($model, 'imageFileUploaded')->fileInput() ?>
    <?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>



    <?php ActiveForm::end(); ?>

</div>
