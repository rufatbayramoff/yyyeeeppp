<?php

use backend\components\columns\PsColumn;
use common\interfaces\Model3dBaseInterface;
use common\models\AffiliateAward;
use common\models\AffiliateSource;
use common\models\StoreOrder;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AffiliateAwardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Affiliate awards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-user-source-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            [
                'attribute' => 'created_at',
                'value'     => function (AffiliateAward $affiliateAward) {
                    return app('formatter')->asDate($affiliateAward->created_at);
                }
            ],
            [
                'attribute' => 'affiliateSourceText',
                'header'    => 'Affiliate source',
                'format'    => 'raw',
                'value'     => function (AffiliateAward $affiliateAward) {
                    return '<a href="/affiliate/affiliate-source/view?id=' . $affiliateAward->affiliateSource->uuid . '">' . H($affiliateAward->affiliateSource->user->username . ' - ' . $affiliateAward->affiliateSource->uuid) . "</a>";
                }
            ],
            'price',
            'currency',
            [
                'attribute' => 'invoice_uuid',
                'header'    => 'Order/Invoice',
                'format'    => 'raw',
                'label'     => 'Invoice(Order)',
                'value'  => function (AffiliateAward $affiliateAward) {
                    $order = $affiliateAward->invoice->storeOrder;
                    $text = $order->getTitle() . ' - ' . $order->getPaymentStatus();
                    return Html::a($text, '/store/store-order/view?id=' . $order->id);
                }
            ],
            [
                'attribute' => 'status',
                'header'    => 'Status',
                'value'     => function (AffiliateAward $affiliateAward) {
                    return $affiliateAward->getStatusText();
                },
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    $searchModel->getStatusList(),
                    ['class' => 'form-control', 'prompt' => 'All']),
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
        'tableOptions' => [
            'class' => 'table table-bordered m-b0 company-block-index__view-table'
        ],
    ]); ?>

</div>
