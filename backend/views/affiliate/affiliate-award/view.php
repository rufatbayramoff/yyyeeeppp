<?php

use common\models\AffiliateAward;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AffiliateAward */

$this->title                   = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Affiliate awards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-user-award-view">
    <?= DetailView::widget([
            'model'      => $model,
            'attributes' => [
                'id',
                'created_at',
                [
                    'label'  => 'Affiliate source',
                    'format' => 'raw',
                    'value'  => '<a href="/affiliate/affiliate-source/view?id=' . $model->affiliateSource->uuid . '">' . H($model->affiliateSource->user->username . ' - ' . $model->affiliateSource->uuid) . "</a>"
                ],
                'price',
                'currency',
                [
                    'label'  => 'Оrder',
                    'format' => 'raw',
                    'value'  => '<a href="/store/store-order/view?id=' . $model->invoice->store_order_id . '">' . H($model->invoice->storeOrder->getTitle()) . "</a>"
                ],
                'accrued_payment_detail_id',
                [
                        'label' => 'Status',
                        'value' => $model->getStatusText()
                ]
            ]
        ]
    ) ?>

</div>
