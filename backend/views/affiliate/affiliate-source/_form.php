<?php

use backend\models\site\SiteSettingsTemplateManager;
use backend\models\site\siteSettingsTemplates\SchemeAffiliateSourceAward;
use beowulfenator\JsonEditor\JsonEditorWidget;
use common\models\Product;
use frontend\assets\JsonEditorAssetJsTemplate;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AffiliateSource */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="affiliate-user-source-form">

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'uuid',
            'created_at',
            'formattedType',
            'formattedAwardType',
            'url',
            'followersCount',
            'ordersCount',
            'award'
        ],
    ]) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList($model::TYPE_TEXTS) ?>

    <?= $form->field($model, 'award_type')->dropDownList($model::AWARD_TYPE_TEXTS) ?>

    <?= $form->field($model, 'awardConfigString', ['options' => ['class' => 'form-group jsonEditorField']])->widget(
        JsonEditorWidget::className(),
        [
            'schema'          => SiteSettingsTemplateManager::getSchema('Affiliate', 'SourceAward'),
            'enableSelectize' => true,
            'clientOptions'   => [
                'theme' => 'bootstrap3'
            ]
        ]
    )->label('');
    ?>

    <?= $form->field($model, 'utm')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
