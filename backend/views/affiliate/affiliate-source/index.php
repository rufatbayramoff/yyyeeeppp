<?php

use backend\components\columns\PsColumn;
use common\interfaces\Model3dBaseInterface;
use common\models\AffiliateSource;
use common\models\StoreOrder;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AffiliateSourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Affiliate sources';
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="text-align: right">
    <?= (is_null($searchModel->inactive)) ? '<b>' : '' ?>
    <a href="/affiliate/affiliate-source/?inactive=null">All</a>
    <?= (is_null($searchModel->inactive)) ? '</b>' : '' ?>
    /
    <?= ($searchModel->inactive === 0) ? '<b>' : '' ?>
    <a href="/affiliate/affiliate-source/?inactive=0">Active</a>
    <?= ($searchModel->inactive === 0) ? '</b>' : '' ?>
    /
    <?= ($searchModel->inactive === 1) ? '<b>' : '' ?>
    <a href="/affiliate/affiliate-source/?inactive=1">Inactive</a>
    <?= ($searchModel->inactive === 1) ? '</b>' : '' ?>
</div>
<div class="affiliate-user-source-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'uuid',
            [
                'attribute' => 'created_at',
                'value'     => function (AffiliateSource $affiliateSource) {
                    return app('formatter')->asDate($affiliateSource->created_at);
                }
            ],
            [
                'attribute' => 'user',
                'header'    => 'Affiliate',
                'class'     => \backend\components\columns\UserColumn::class,
            ],
            [
                'attribute' => 'api_external_system_id',
                'header'    => 'Api external system',
                'format'    => 'raw',
                'value'     => function (AffiliateSource $model) {
                    return $model->apiExternalSystem ? '<a href="/site/api-external-system/view?id=' . $model->apiExternalSystem->id . '">' . H($model->apiExternalSystem->name) . "</a>" : '';
                },
            ],
            [
                'attribute' => 'type',
                'value'     => function (AffiliateSource $affiliateSource) {
                    return $affiliateSource->getFormattedType();
                },
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'type',
                    $searchModel::TYPE_TEXTS,
                    ['class' => 'form-control', 'prompt' => 'All'])
            ],
            'utm',
            [
                'header' => _t('site.common', 'Followers'),
                'value'  => function (AffiliateSource $affiliateSource) {
                    return $affiliateSource->getFollowersCount();
                }
            ],
            [
                'header' => _t('site.common', 'Bind users'),
                'value'  => function (AffiliateSource $affiliateSource) {
                    return $affiliateSource->getBindUsersCount();
                }
            ],
            [
                'header' => _t('site.common', 'Orders'),
                'value'  => function (AffiliateSource $affiliateSource) {
                    return $affiliateSource->getOrdersCount();
                }
            ],
            [
                'header' => _t('site.common', 'Award'),
                'value'  => function (AffiliateSource $affiliateSource) {
                    $retVal = '';
                    foreach ($affiliateSource->getAwards() as $award) {
                        $retVal .= displayAsMoney($award) . ' ';
                    }
                    return $retVal;
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
        'tableOptions' => [
            'class' => 'table table-bordered m-b0 company-block-index__view-table'
        ],
    ]); ?>

</div>
