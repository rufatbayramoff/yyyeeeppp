<?php

use backend\components\columns\PsColumn;
use backend\components\columns\UserColumn;
use common\models\AffiliateSource;
use common\models\AffiliateSourceClient;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AffiliateSource */
/* @var $bindUsersProvider \yii\data\ActiveDataProvider */
/* @var $referrersProvider \yii\data\ActiveDataProvider */

$this->title                   = $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-user-source-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'uuid',
            [
                'attribute' => 'company',
                'format'    => 'raw',
                'value'     => UserColumn::getMenu($model->user),
            ],
            'created_at',
            'formattedType',
            'formattedAwardType',
            [
                'attribute' => 'api_external_system',
                'format'    => 'raw',
                'value'     => $model->apiExternalSystem ? '<a href="/site/api-external-system/view?id=' . $model->apiExternalSystem->id . '">' . $model->apiExternalSystem->name . "</a>" : ''
            ],
            'utm',
            'url',
            'followersCount',
            'ordersCount',
            'award',
            'awardConfigString',
            'inactive',
            [
                'label'  => 'Awards list',
                'format' => 'raw',
                'value'  => '<a href="/affiliate/affiliate-award?AffiliateAwardSearch[affiliate_source_uuid]=' . $model->uuid . '">view</a>'
            ]
        ]
    ]) ?>

    <div class="row">
        <div class="col-md-5">
            <h3>Bind Users</h3>
            <?= GridView::widget(['dataProvider' => $bindUsersProvider,
                                  'columns'      => ['created_at',
                                      ['label'     => 'User',
                                       'attribute' => 'user',
                                       'class'     => UserColumn::class]]]
            );
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <h3>Referrers</h3>
            <?= GridView::widget(['dataProvider' => $referrersProvider,
                                  'columns'      => [
                                      'date',
                                      'referrer'
                                  ]
            ]);
            ?>
        </div>
    </div>
