<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AffiliateSource */

$this->title = 'Update Affiliate User Source: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Affiliate User Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="affiliate-user-source-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
