<?php
use yii\helpers\Html;


echo $this->renderFile(Yii::getAlias('@backend/views/moderator/tabs.php'));

if(backend\components\AdminAccess::can('root.createUser')){
    $url = yii\helpers\Url::toRoute(['moderator/user-admin/create']);
    echo yii\helpers\Html::a('Add Moderator', $url, ['class'=>'btn btn-primary']);
 echo " ";
    $url = yii\helpers\Url::toRoute(['moderator/user-admin/create-users']);
    echo yii\helpers\Html::a('Create Moderator Site users', $url, ['class'=>'btn btn-warning']);
}
echo yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [ 
        [
            'attribute' => 'id',
            'contentOptions'=>['style'=>'width: 70px;']
        ],
        'username',
        'email:email', 
         [
            'attribute' => 'status',
            'filter' => Html::activeDropDownList(
                $searchModel, 'status',  
                $searchModel->getStatuses(),
                ['class'=>'form-control','prompt' => 'All']
            ), 
             'value' => function($model){
                $st = $model->getStatuses();
                return isset($st[$model->status])?$st[$model->status]:$model->status;
             }
        ], 
        [
            'label' => 'Group',
            'attribute' => 'group.title',
            'filter' => Html::activeDropDownList(
                $searchModel, 'group_id',  
                yii\helpers\ArrayHelper::map(\common\models\UserAdminGroup::find()->all(), 'id', 'title'),
                ['class'=>'form-control','prompt' => 'All']
            ), 
        ], 
        'created_at:datetime',
        'updated_at:datetime',
        [  
            'format' => 'raw',
            'value' => function($model){
                $url = yii\helpers\Url::toRoute(['moderator/user-admin/update', 'id'=>$model->id]);
                $txt1 = yii\helpers\Html::a('Edit User', $url, ['class'=>'btn btn-info ']); 
                return $txt1;                   
            }
        ],
        [  
            'format' => 'raw',
            'value' => function($model){
                $url = yii\helpers\Url::toRoute(['moderator/access/view-access', 'userId'=>$model->id]);
                $txt1 = yii\helpers\Html::a('Edit accesses', $url, ['class'=>'btn btn-warning ']); 
                return $txt1;                   
            }
        ] 
    ],
]);
