<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ModerLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Moder Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moder-log-index">
 
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [ 
            'id',
            'user_id',
            'created_at',
            'started_at',
            'finished_at',
             'action',
            'result:ntext',
             'object_type',
             'object_id',
        ],
    ]); ?>

</div>
