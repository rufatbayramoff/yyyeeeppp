<?php
$links = [
   '@web/moderator/access' => 'Moderator List',
   '@web/moderator/user-admin-group' => 'Moderator Groups'
];
$items = [];
$curUrl = (app('request')->getPathInfo());
$curUrl = str_replace("/index", "", $curUrl);
foreach($links as $k=>$v){
    $item = ['label'=>$v, 'url'=>$k];
    
    if('@web/'.$curUrl==$k){
        $item['active'] = true;
    }
    $items[] = $item;
    
}
echo \yii\bootstrap\Tabs::widget([
    'items' => $items,
    'options' => ['style'=>'margin-bottom:20px;']
]); 