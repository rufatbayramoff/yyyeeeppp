<?php
use yii\helpers\Html;
$accessList = backend\components\AdminAccess::getAccessData();

$form = \yii\widgets\ActiveForm::begin(['action'=>['moderator/access/update-access', 'userId'=>$user->id]]);

$accessForm = new backend\models\access\AccessForm();
$accessForm->initUserAccessList($user);

$this->title = 'Update user access: ' . $user->username;

$this->params['breadcrumbs'][] = ['label' => 'Moderator list', 'url' => ['moderator/access']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="access-list">
<?php
foreach ($accessList as $accessGroup) {    
    echo '<div class="row" style="border-bottom: 1px dotted #afafaf; padding-top: 10px;">';
    $group = $accessGroup['group'];
    $title = !empty($accessGroup['title'])? $accessGroup['title']  : ucwords(str_replace("_", " ", $group));
    echo '<div class="col-md-2 text-right">' . \yii\helpers\Html::tag('h4', $title) . '</div>';
    $accessListInner = $accessForm->formatGroupAccess($accessGroup);
    
    echo '<div class="col-md-6">';
    //echo $form->field($accessForm, 'accessList')->checkboxlist($accessList)->label(false);
    foreach($accessListInner as $k=>$v){
        $checked = in_array($k, $accessForm->accessList);
    ?>
   <label <?php echo $checked?"style='color:#229911;'":"style='color:#888;'"; ?>><input type="checkbox" name="AccessForm[accessList][]" <?php echo $checked?'checked':''; ?> value="<?php echo $k; ?>">
         <?php echo $v; ?> <code>[<?php echo $k; ?>]</code></label>
    <?php
    }
    echo '</div>';
    echo '</div>';
}
?>
    <br />
    <div class="col-md-2"></div>
      <div class="form-group" >
        <?= Html::submitButton( '  Update  ', ['class' =>  'btn btn-primary']) ?>
    </div>
    <?php 

\yii\widgets\ActiveForm::end();

?>
    </div>
<style>
    
    .access-list label{
        display: block;
    }
</style>