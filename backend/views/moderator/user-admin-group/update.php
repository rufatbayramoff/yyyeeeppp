<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserAdminGroup */

$this->title = 'Update User Admin Group: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'User Admin Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-admin-group-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
