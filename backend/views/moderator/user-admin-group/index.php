<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserAdminGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Moderator Groups';
$this->params['breadcrumbs'][] = $this->title;


echo $this->renderFile(Yii::getAlias('@backend/views/moderator/tabs.php'));

?>
<div class="user-admin-group-index">


    <p>
        <?= Html::a('Create Group', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'updated_at',

            [  
                'format' => 'raw',
                'value' => function($model){
                    $url = yii\helpers\Url::toRoute(['moderator/user-admin-group/view-access', 'groupId'=>$model->id]);
                    $txt1 = yii\helpers\Html::a('Edit Group Accesses', $url, ['class'=>'btn btn-warning ']); 
                    return $txt1;                   
                }
            ] ,
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
