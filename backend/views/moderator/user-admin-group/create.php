<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserAdminGroup */

$this->title = 'Create User Admin Group';
$this->params['breadcrumbs'][] = ['label' => 'User Admin Groups', 'url' => ['moderator/user-admin-group']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-admin-group-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
