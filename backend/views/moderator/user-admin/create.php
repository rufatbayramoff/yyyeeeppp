<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserAdmin */

$this->title = 'Create User Admin';
$this->params['breadcrumbs'][] = ['label' => 'User Admins', 'url' => ['moderator/access']];
$this->params['breadcrumbs'][] = $this->title;

$model->status = 10;
$model->email = '@treatstock.com';
?>
<div class="user-admin-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
