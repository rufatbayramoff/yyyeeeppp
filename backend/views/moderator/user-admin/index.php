<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Admins';
$this->params['breadcrumbs'][] = $this->title;


echo $this->renderFile(Yii::getAlias('@backend/views/moderator/tabs.php'));
?>
<div class="user-admin-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
             'email:email',
              'status',
            // 'created_at',
            'updated_at',
             'group.title',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
