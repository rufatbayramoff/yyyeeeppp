<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserAdmin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-admin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

   <?= $form->field($model, 'status')->dropDownList(
            $model->getStatuses(),           // Flat array ('id'=>'label')
            ['prompt'=>'']    // options
        )
        ?>
    
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?php if(!$model->isNewRecord): ?>
    <p>Specify password only if you want to change it.</p>
    
    <?php endif; ?>
     <?php echo $form->field($model, 'group_id')->widget(\kartik\select2\Select2::classname(), [
        'data' =>  yii\helpers\ArrayHelper::map(\common\models\UserAdminGroup::find()->all(), 'id', 'title'),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?> 
    <div class="callout-info  callout">
    If group is changed, all access rights will be updated with group access rights.
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : ' Update ', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
