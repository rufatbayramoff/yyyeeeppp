<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserAdmin */

$this->title = 'Update User Admin: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Admins', 'url' => ['moderator/access']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-admin-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
