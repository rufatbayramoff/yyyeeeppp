<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SystemRejectIntl */

$this->title = 'Update System Reject Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'System Reject Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="system-reject-intl-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
