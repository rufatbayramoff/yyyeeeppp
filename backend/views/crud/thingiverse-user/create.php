<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseUser */

$this->title = 'Create Thingiverse User';
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thingiverse-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
