<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\MsgTopicSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="msg-topic-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'creator_id') ?>

    <?= $form->field($model, 'bind_to') ?>

    <?= $form->field($model, 'bind_id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'last_message_time') ?>

    <?php // echo $form->field($model, 'last_support_user_id') ?>

    <?php // echo $form->field($model, 'hold_support_user_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
