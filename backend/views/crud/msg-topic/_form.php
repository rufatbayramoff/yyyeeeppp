<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MsgTopic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="msg-topic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'creator_id')->textInput() ?>

    <?= $form->field($model, 'bind_to')->dropDownList([ 'order' => 'Order', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'bind_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_message_time')->textInput() ?>

    <?= $form->field($model, 'last_support_user_id')->textInput() ?>

    <?= $form->field($model, 'hold_support_user_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
