<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MsgTopic */

$this->title = 'Update Msg Topic: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Msg Topics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="msg-topic-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
