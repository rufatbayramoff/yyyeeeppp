<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MsgTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Msg Topics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-topic-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Msg Topic', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'creator_id',
            'bind_to',
            'bind_id',
            'created_at',
            // 'title',
            // 'last_message_time',
            // 'last_support_user_id',
            // 'hold_support_user_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
