<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MsgTopic */

$this->title = 'Create Msg Topic';
$this->params['breadcrumbs'][] = ['label' => 'Msg Topics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-topic-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
