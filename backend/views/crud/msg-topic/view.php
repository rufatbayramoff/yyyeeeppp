<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MsgTopic */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Msg Topics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-topic-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'creator_id',
            'bind_to',
            'bind_id',
            'created_at',
            'title',
            'last_message_time',
            'last_support_user_id',
            'hold_support_user_id',
        ],
    ]) ?>

</div>
