<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Preorder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="preorder-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList([ 'wait_confirm' => 'Wait confirm', 'new' => 'New', 'accepted' => 'Accepted', 'rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'ps_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'budget')->textInput() ?>

    <?= $form->field($model, 'estimate_time')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'decline_reason')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'decline_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'decline_initiator')->textInput() ?>

    <?= $form->field($model, 'offer_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'offer_estimate_time')->textInput() ?>

    <?= $form->field($model, 'offered')->textInput() ?>

    <?= $form->field($model, 'confirm_hash')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
