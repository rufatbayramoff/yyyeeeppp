<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Preorder */

$this->title = 'Create Preorder';
$this->params['breadcrumbs'][] = ['label' => 'Preorders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
