<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PreorderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="preorder-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'ps_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'message') ?>

    <?php // echo $form->field($model, 'budget') ?>

    <?php // echo $form->field($model, 'estimate_time') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'decline_reason') ?>

    <?php // echo $form->field($model, 'decline_comment') ?>

    <?php // echo $form->field($model, 'decline_initiator') ?>

    <?php // echo $form->field($model, 'offer_description') ?>

    <?php // echo $form->field($model, 'offer_estimate_time') ?>

    <?php // echo $form->field($model, 'offered') ?>

    <?php // echo $form->field($model, 'confirm_hash') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
