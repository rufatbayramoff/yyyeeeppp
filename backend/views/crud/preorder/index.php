<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PreorderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Preorders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Preorder', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'status',
            'created_at',
            'ps_id',
            'user_id',
            // 'name',
            // 'description:ntext',
            // 'message:ntext',
            // 'budget',
            // 'estimate_time:datetime',
            // 'email:email',
            // 'decline_reason',
            // 'decline_comment',
            // 'decline_initiator',
            // 'offer_description:ntext',
            // 'offer_estimate_time:datetime',
            // 'offered',
            // 'confirm_hash',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
