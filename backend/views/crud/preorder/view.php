<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Preorder */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Preorders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'status',
            'created_at',
            'ps_id',
            'user_id',
            'name',
            'description:ntext',
            'message:ntext',
            'budget',
            'estimate_time:datetime',
            'email:email',
            'decline_reason',
            'decline_comment',
            'decline_initiator',
            'offer_description:ntext',
            'offer_estimate_time:datetime',
            'offered',
            'confirm_hash',
        ],
    ]) ?>

</div>
