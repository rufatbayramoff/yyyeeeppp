<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CncPreorderFile */

$this->title = 'Create Cnc Preorder File';
$this->params['breadcrumbs'][] = ['label' => 'Cnc Preorder Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnc-preorder-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
