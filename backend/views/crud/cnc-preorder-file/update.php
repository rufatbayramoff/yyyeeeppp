<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CncPreorderFile */

$this->title = 'Update Cnc Preorder File: ' . $model->preorder_id;
$this->params['breadcrumbs'][] = ['label' => 'Cnc Preorder Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->preorder_id, 'url' => ['view', 'preorder_id' => $model->preorder_id, 'file_id' => $model->file_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cnc-preorder-file-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
