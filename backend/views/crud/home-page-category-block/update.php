<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryBlock */

$this->title = 'Update Home Page Category Block: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Category Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-page-category-block-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
