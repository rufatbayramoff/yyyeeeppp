<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryBlock */

$this->title = 'Create Home Page Category Block';
$this->params['breadcrumbs'][] = ['label' => 'Home Page Category Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-category-block-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
