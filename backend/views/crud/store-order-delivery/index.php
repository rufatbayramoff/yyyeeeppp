<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderDeliverySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Deliveries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-delivery-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order Delivery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'order_id',
            'file_id',
            'delivery_type_id',
            'tracking_number',
            // 'status',
            // 'created_at',
            // 'send_message_at',
            // 'ps_delivery_details',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
