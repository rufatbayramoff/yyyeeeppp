<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductMainSlider */

$this->title = 'Create Product Main Slider';
$this->params['breadcrumbs'][] = ['label' => 'Product Main Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-main-slider-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
