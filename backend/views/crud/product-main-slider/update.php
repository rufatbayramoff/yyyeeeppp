<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductMainSlider */

$this->title = 'Update Product Main Slider: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Product Main Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-main-slider-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
