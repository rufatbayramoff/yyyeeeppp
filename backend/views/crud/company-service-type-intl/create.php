<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceTypeIntl */

$this->title = 'Create Company Service Type Intl';
$this->params['breadcrumbs'][] = ['label' => 'Company Service Type Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-type-intl-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
