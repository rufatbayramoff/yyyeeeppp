<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceTypeIntl */

$this->title = 'Update Company Service Type Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company Service Type Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-service-type-intl-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
