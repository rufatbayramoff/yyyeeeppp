<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseThing */

$this->title = 'Create Thingiverse Thing';
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Things', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thingiverse-thing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
