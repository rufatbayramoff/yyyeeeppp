<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseThing */

$this->title = 'Update Thingiverse Thing: ' . $model->thing_id;
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Things', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->thing_id, 'url' => ['view', 'id' => $model->thing_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="thingiverse-thing-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
