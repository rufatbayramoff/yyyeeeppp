<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseThing */

$this->title = $model->thing_id;
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Things', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thingiverse-thing-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->thing_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->thing_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'thing_id',
            'model3d_id',
            'created_at',
        ],
    ]) ?>

</div>
