<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-request-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Request', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'created_at',
            'updated_at',
            'status',
            // 'moderator_id',
            // 'request_type',
            // 'code',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
