<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpRelated */

$this->title = 'Create Site Help Related';
$this->params['breadcrumbs'][] = ['label' => 'Site Help Relateds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-help-related-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
