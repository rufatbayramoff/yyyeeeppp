<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteHelpRelatedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Help Relateds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-help-related-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Site Help Related', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'help_id',
            'related_help_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
