<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpRelated */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-help-related-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'help_id')->textInput() ?>

    <?= $form->field($model, 'related_help_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
