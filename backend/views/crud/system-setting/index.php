<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SystemSettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'System Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-setting-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create System Setting', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'group_id',
            'key',
            'value',
            'created_at',
            // 'updated_at',
            // 'created_user_id',
            // 'updated_user_id',
            // 'description',
            // 'json:ntext',
            // 'is_active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
