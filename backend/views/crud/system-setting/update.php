<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SystemSetting */

$this->title = 'Update System Setting: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'System Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="system-setting-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
