<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AffiliateHistory */

$this->title = 'Update Affiliate History: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="affiliate-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
