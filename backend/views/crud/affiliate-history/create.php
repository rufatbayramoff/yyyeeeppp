<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AffiliateHistory */

$this->title = 'Create Affiliate History';
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
