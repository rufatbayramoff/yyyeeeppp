<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CncPreorderPreset */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cnc Preorder Presets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnc-preorder-preset-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'preorder_id',
            'part_replica_id',
            'material',
            'qty',
            'postprocessing:ntext',
        ],
    ]) ?>

</div>
