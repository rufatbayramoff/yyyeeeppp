<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CncPreorderPresetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cnc Preorder Presets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnc-preorder-preset-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cnc Preorder Preset', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'preorder_id',
            'part_replica_id',
            'material',
            'qty',
            // 'postprocessing:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
