<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CncPreorderPreset */

$this->title = 'Update Cnc Preorder Preset: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cnc Preorder Presets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cnc-preorder-preset-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
