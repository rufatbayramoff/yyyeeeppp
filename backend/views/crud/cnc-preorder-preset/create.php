<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CncPreorderPreset */

$this->title = 'Create Cnc Preorder Preset';
$this->params['breadcrumbs'][] = ['label' => 'Cnc Preorder Presets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnc-preorder-preset-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
