<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CncPreorderPresetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cnc-preorder-preset-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'preorder_id') ?>

    <?= $form->field($model, 'part_replica_id') ?>

    <?= $form->field($model, 'material') ?>

    <?= $form->field($model, 'qty') ?>

    <?php // echo $form->field($model, 'postprocessing') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
