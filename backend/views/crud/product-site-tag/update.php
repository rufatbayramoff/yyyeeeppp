<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSiteTag */

$this->title = 'Update Product Site Tag: ' . $model->product_uuid;
$this->params['breadcrumbs'][] = ['label' => 'Product Site Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_uuid, 'url' => ['view', 'product_uuid' => $model->product_uuid, 'site_tag_id' => $model->site_tag_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-site-tag-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
