<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSiteTag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-site-tag-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'site_tag_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
