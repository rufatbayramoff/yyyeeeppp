<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSiteTag */

$this->title = $model->product_uuid;
$this->params['breadcrumbs'][] = ['label' => 'Product Site Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-site-tag-view">

    <p>
        <?= Html::a('Update', ['update', 'product_uuid' => $model->product_uuid, 'site_tag_id' => $model->site_tag_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'product_uuid' => $model->product_uuid, 'site_tag_id' => $model->site_tag_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_uuid',
            'site_tag_id',
        ],
    ]) ?>

</div>
