<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductSiteTag */

$this->title = 'Create Product Site Tag';
$this->params['breadcrumbs'][] = ['label' => 'Product Site Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-site-tag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
