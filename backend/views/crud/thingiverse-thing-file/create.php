<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseThingFile */

$this->title = 'Create Thingiverse Thing File';
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Thing Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thingiverse-thing-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
