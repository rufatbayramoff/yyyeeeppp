<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ThingiverseThingFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Thingiverse Thing Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thingiverse-thing-file-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Thingiverse Thing File', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'thingfile_id',
            'thing_id',
            'name',
            'size',
            'url:url',
            // 'download_url:url',
            // 'thumb',
            // 'render',
            // 'created_at',
            // 'file_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
