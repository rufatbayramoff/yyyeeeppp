<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseThingFile */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Thing Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thingiverse-thing-file-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->thingfile_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->thingfile_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'thingfile_id',
            'thing_id',
            'name',
            'size',
            'url:url',
            'download_url:url',
            'thumb',
            'render',
            'created_at',
            'file_id',
        ],
    ]) ?>

</div>
