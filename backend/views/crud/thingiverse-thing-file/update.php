<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseThingFile */

$this->title = 'Update Thingiverse Thing File: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Thing Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->thingfile_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="thingiverse-thing-file-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
