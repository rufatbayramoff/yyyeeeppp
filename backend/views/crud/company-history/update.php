<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyHistory */

$this->title = 'Update Company History: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-history-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
