<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyHistory */

$this->title = 'Create Company History';
$this->params['breadcrumbs'][] = ['label' => 'Company Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-history-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
