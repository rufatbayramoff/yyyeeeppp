<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dViewedStateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3d Viewed States';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-viewed-state-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model3d Viewed State', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model3d_id',
            'user_session_id',
            'date',
            'model3dTextureInfo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
