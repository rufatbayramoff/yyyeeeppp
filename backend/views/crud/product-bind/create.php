<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductBind */

$this->title = 'Create Product Bind';
$this->params['breadcrumbs'][] = ['label' => 'Product Binds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-bind-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
