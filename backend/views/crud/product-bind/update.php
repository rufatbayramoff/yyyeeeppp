<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductBind */

$this->title = 'Update Product Bind: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Binds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-bind-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
