<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AffiliateAwardRule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="affiliate-award-rule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'visitor_days_valid')->textInput() ?>

    <?= $form->field($model, 'max_orders_per_user')->textInput() ?>

    <?= $form->field($model, 'order_type_api')->checkbox() ?>

    <?= $form->field($model, 'order_type_upload')->checkbox() ?>

    <?= $form->field($model, 'order_type_any')->checkbox() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'reward_type')->dropDownList([ 'api' => 'Api', 'fixed' => 'Fixed', 'percent' => 'Percent', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'reward_amount')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
