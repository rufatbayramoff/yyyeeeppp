<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AffiliateAwardRuleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Affiliate Award Rules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-award-rule-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Affiliate Award Rule', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'visitor_days_valid',
            'max_orders_per_user',
            'order_type_api:boolean',
            // 'order_type_upload:boolean',
            // 'order_type_any:boolean',
            // 'updated_at',
            // 'reward_type',
            // 'reward_amount',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
