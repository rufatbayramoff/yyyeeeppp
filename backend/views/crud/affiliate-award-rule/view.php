<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AffiliateAwardRule */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Award Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-award-rule-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'visitor_days_valid',
            'max_orders_per_user',
            'order_type_api:boolean',
            'order_type_upload:boolean',
            'order_type_any:boolean',
            'updated_at',
            'reward_type',
            'reward_amount',
        ],
    ]) ?>

</div>
