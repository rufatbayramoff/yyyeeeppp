<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceHistory */

$this->title = 'Update Company Service History: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Service Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-service-history-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
