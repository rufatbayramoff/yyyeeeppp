<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceHistory */

$this->title = 'Create Company Service History';
$this->params['breadcrumbs'][] = ['label' => 'Company Service Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-history-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
