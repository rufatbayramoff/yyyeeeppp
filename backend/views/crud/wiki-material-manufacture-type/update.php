<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialManufactureType */

$this->title = 'Update Wiki Material Manufacture Type: ' . $model->wiki_material_id;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Material Manufacture Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->wiki_material_id, 'url' => ['view', 'wiki_material_id' => $model->wiki_material_id, 'manufacture_type_id' => $model->manufacture_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wiki-material-manufacture-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
