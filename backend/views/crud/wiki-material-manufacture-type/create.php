<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialManufactureType */

$this->title = 'Create Wiki Material Manufacture Type';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Material Manufacture Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-material-manufacture-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
