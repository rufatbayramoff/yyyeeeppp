<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TsInternalPurchase */

$this->title = 'Update Ts Internal Purchase: ' . $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Ts Internal Purchases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uid, 'url' => ['view', 'id' => $model->uid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ts-internal-purchase-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
