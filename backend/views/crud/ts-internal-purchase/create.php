<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TsInternalPurchase */

$this->title = 'Create Ts Internal Purchase';
$this->params['breadcrumbs'][] = ['label' => 'Ts Internal Purchases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ts-internal-purchase-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
