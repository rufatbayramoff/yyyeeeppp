<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TsInternalPurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ts Internal Purchases';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ts-internal-purchase-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ts Internal Purchase', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uid',
            'created_at',
            'user_id',
            'primary_invoice_uuid',
            'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
