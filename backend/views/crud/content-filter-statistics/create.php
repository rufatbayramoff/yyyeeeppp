<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContentFilterStatistics */

$this->title = 'Create Content Filter Statistics';
$this->params['breadcrumbs'][] = ['label' => 'Content Filter Statistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-filter-statistics-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
