<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineFile */

$this->title = $model->file_uuid;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-file-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->file_uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->file_uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'wiki_machine_id',
            'file_uuid',
        ],
    ]) ?>

</div>
