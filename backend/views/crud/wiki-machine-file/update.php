<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineFile */

$this->title = 'Update Wiki Machine File: ' . $model->file_uuid;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->file_uuid, 'url' => ['view', 'id' => $model->file_uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wiki-machine-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
