<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineFile */

$this->title = 'Create Wiki Machine File';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-file-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
