<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InvalidEmail */

$this->title = 'Update Invalid Email: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Invalid Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="invalid-email-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
