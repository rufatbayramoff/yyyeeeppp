<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvalidEmail */

$this->title = 'Create Invalid Email';
$this->params['breadcrumbs'][] = ['label' => 'Invalid Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invalid-email-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
