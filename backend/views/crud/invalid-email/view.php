<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\InvalidEmail */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Invalid Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invalid-email-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'email:email',
            'type',
            'ignored',
        ],
    ]) ?>

</div>
