<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BrowserPushSubscriptionUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Browser Push Subscription Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="browser-push-subscription-user-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Browser Push Subscription User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uid',
            'created_at',
            'browser_push_subscription_uid',
            'user_id',
            'ip',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
