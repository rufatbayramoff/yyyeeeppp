<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BrowserPushSubscriptionUser */

$this->title = 'Create Browser Push Subscription User';
$this->params['breadcrumbs'][] = ['label' => 'Browser Push Subscription Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="browser-push-subscription-user-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
