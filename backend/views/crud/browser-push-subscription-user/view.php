<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BrowserPushSubscriptionUser */

$this->title = $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Browser Push Subscription Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="browser-push-subscription-user-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uid',
            'created_at',
            'browser_push_subscription_uid',
            'user_id',
            'ip',
        ],
    ]) ?>

</div>
