<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BrowserPushSubscriptionUser */

$this->title = 'Update Browser Push Subscription User: ' . $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Browser Push Subscription Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uid, 'url' => ['view', 'id' => $model->uid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="browser-push-subscription-user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
