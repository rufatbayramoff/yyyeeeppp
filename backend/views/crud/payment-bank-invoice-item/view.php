<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentBankInvoiceItem */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Payment Bank Invoice Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-bank-invoice-item-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'invoice_id',
            'title',
            'description',
            'measure',
            'qty',
            'unit_price',
            'tax',
            'discount',
            'currency',
            'created_at',
            'item_type',
        ],
    ]) ?>

</div>
