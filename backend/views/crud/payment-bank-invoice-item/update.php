<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentBankInvoiceItem */

$this->title = 'Update Payment Bank Invoice Item: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Payment Bank Invoice Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-bank-invoice-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
