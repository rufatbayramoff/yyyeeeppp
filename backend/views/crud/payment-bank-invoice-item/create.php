<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentBankInvoiceItem */

$this->title = 'Create Payment Bank Invoice Item';
$this->params['breadcrumbs'][] = ['label' => 'Payment Bank Invoice Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-bank-invoice-item-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
