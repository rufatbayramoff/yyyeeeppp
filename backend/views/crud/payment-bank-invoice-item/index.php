<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentBankInvoiceItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Bank Invoice Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-bank-invoice-item-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Bank Invoice Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'invoice_id',
            'title',
            'description',
            'measure',
            // 'qty',
            // 'unit_price',
            // 'tax',
            // 'discount',
            // 'currency',
            // 'created_at',
            // 'item_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
