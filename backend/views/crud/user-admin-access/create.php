<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserAdminAccess */

$this->title = 'Create User Admin Access';
$this->params['breadcrumbs'][] = ['label' => 'User Admin Accesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-admin-access-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
