<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserAdminAccessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Admin Accesses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-admin-access-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Admin Access', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'admin_id',
            'access',
            'is_active',
            'created_at',
            // 'updated_at',
            // 'by_group',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
