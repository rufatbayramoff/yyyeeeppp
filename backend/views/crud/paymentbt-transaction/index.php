<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentbtTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Paymentbt Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paymentbt-transaction-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Paymentbt Transaction', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'transaction_id',
            'created_at',
            'updated_at',
            'status',
            // 'type',
            // 'currency',
            // 'amount',
            // 'merchant_acount_id',
            // 'store_unit_id',
            // 'channel',
            // 'full_response:ntext',
            // 'success',
            // 'customer_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
