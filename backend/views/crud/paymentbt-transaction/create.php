<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentbtTransaction */

$this->title = 'Create Paymentbt Transaction';
$this->params['breadcrumbs'][] = ['label' => 'Paymentbt Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paymentbt-transaction-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
