<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentbtTransaction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Paymentbt Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paymentbt-transaction-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'transaction_id',
            'created_at',
            'updated_at',
            'status',
            'type',
            'currency',
            'amount',
            'merchant_acount_id',
            'store_unit_id',
            'channel',
            'full_response:ntext',
            'success',
            'customer_id',
        ],
    ]) ?>

</div>
