<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StorePricerType */

$this->title = 'Create Store Pricer Type';
$this->params['breadcrumbs'][] = ['label' => 'Store Pricer Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-pricer-type-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
