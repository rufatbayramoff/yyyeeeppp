<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SeoPageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seo Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-page-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Seo Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'url:url',
            'title',
            'header',
            'meta_description',
            // 'meta_keywords',
            // 'header_text:ntext',
            // 'footer_text:ntext',
            // 'is_active:boolean',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
