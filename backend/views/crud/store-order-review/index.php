<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-review-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order Review', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'order_id',
            'store_unit_id',
            'ps_id',
            'rating_speed',
            // 'rating_quality',
            // 'rating_communication',
            // 'comment:ntext',
            // 'file_ids:ntext',
            // 'status',
            // 'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
