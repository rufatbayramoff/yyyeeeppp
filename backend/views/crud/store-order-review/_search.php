<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\StoreOrderReviewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-order-review-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'order_id') ?>

    <?= $form->field($model, 'store_unit_id') ?>

    <?= $form->field($model, 'ps_id') ?>

    <?= $form->field($model, 'rating_speed') ?>

    <?php // echo $form->field($model, 'rating_quality') ?>

    <?php // echo $form->field($model, 'rating_communication') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'file_ids') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
