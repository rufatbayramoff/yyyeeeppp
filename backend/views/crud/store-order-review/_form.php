<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderReview */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-order-review-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'order_id')->textInput() ?>

    <?= $form->field($model, 'store_unit_id')->textInput() ?>

    <?= $form->field($model, 'ps_id')->textInput() ?>

    <?= $form->field($model, 'rating_speed')->textInput() ?>

    <?= $form->field($model, 'rating_quality')->textInput() ?>

    <?= $form->field($model, 'rating_communication')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_ids')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'new' => 'New', 'moderated' => 'Moderated', 'rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
