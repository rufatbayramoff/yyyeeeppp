<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderModel3dPartSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Model3d Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-model3d-file-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order Model3d File', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'order_item_id',
            'model3d_part_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
