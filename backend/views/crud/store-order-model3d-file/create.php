<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderModel3dPart */

$this->title = 'Create Store Order Model3d File';
$this->params['breadcrumbs'][] = ['label' => 'Store Order Model3d Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-model3d-file-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
