<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialGroup */

$this->title = 'Update Printer Material Group: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Material Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-material-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
