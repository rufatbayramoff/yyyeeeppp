<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialGroup */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Material Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'is_active',
            'photo_file_id',
            'density',
            'short_description',
            'long_description:ntext',
        ],
    ]) ?>

</div>
