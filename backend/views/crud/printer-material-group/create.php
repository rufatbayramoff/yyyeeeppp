<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialGroup */

$this->title = 'Create Printer Material Group';
$this->params['breadcrumbs'][] = ['label' => 'Printer Material Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
