<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterToPropertySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer To Properties';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-to-property-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Printer To Property', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'printer_id',
            'property_id',
            'value',
            'is_editable:boolean',
            // 'created_at',
            // 'updated_at',
            // 'is_active:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
