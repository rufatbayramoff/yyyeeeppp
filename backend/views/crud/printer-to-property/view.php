<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterToProperty */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Printer To Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-to-property-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'printer_id',
            'property_id',
            'value',
            'is_editable:boolean',
            'created_at',
            'updated_at',
            'is_active:boolean',
        ],
    ]) ?>

</div>
