<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductFile */

$this->title = 'Update Product File: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Product Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
