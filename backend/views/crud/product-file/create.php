<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductFile */

$this->title = 'Create Product File';
$this->params['breadcrumbs'][] = ['label' => 'Product Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-file-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
