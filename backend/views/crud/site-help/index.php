<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteHelpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Helps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-help-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Site Help', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'updated_at',
            'user_id',
            'alias',
            // 'title',
            // 'content:ntext',
            // 'is_active',
            // 'clicks',
            // 'is_ok',
            // 'is_bad',
            // 'category_id',
            // 'priority',
            // 'is_popular:boolean',
            // 'views',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
