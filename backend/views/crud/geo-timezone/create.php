<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GeoTimezone */

$this->title = Yii::t('app', 'Create Geo Timezone');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Geo Timezones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-timezone-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
