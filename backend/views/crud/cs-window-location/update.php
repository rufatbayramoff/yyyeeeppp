<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowLocation */

$this->title = 'Update Cs Window Location: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cs-window-location-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
