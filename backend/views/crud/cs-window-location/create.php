<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CsWindowLocation */

$this->title = 'Create Cs Window Location';
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-location-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
