<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterColor */

$this->title = 'Update Ps Printer Color: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-printer-color-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
