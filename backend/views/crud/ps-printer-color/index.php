<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsPrinterColorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Printer Colors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-color-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ps Printer Color', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ps_material_id',
            'title',
            'created_at',
            'updated_at',
            // 'color_id',
            // 'price',
            // 'price_measure',
            // 'is_active',
            // 'price_usd',
            // 'price_usd_updated',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
