<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterColor */

$this->title = 'Create Ps Printer Color';
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-color-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
