<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HttpRequestLog */

$this->title = 'Create Http Request Log';
$this->params['breadcrumbs'][] = ['label' => 'Http Request Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="http-request-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
