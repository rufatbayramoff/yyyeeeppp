<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HttpRequestLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="http-request-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remote_addr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'params_get')->textInput() ?>

    <?= $form->field($model, 'params_post')->textInput() ?>

    <?= $form->field($model, 'params_files')->textInput() ?>

    <?= $form->field($model, 'session')->textInput() ?>

    <?= $form->field($model, 'cookie')->textInput() ?>

    <?= $form->field($model, 'answer')->textInput() ?>

    <?= $form->field($model, 'create_date')->textInput() ?>

    <?= $form->field($model, 'run_time_milisec')->textInput() ?>

    <?= $form->field($model, 'request_type')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
