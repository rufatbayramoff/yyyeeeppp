<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HttpRequestLog */

$this->title = 'Update Http Request Log: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Http Request Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="http-request-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
