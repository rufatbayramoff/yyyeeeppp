<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\HttpRequestLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="http-request-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'url') ?>

    <?= $form->field($model, 'remote_addr') ?>

    <?= $form->field($model, 'params_get') ?>

    <?= $form->field($model, 'params_post') ?>

    <?php // echo $form->field($model, 'params_files') ?>

    <?php // echo $form->field($model, 'session') ?>

    <?php // echo $form->field($model, 'cookie') ?>

    <?php // echo $form->field($model, 'answer') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'run_time_milisec') ?>

    <?php // echo $form->field($model, 'request_type') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
