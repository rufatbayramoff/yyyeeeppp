<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialPrinterMaterial */

$this->title = 'Create Wiki Material Printer Material';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Material Printer Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-material-printer-material-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
