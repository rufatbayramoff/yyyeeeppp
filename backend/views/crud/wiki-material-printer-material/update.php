<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialPrinterMaterial */

$this->title = 'Update Wiki Material Printer Material: ' . $model->wiki_material_id;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Material Printer Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->wiki_material_id, 'url' => ['view', 'wiki_material_id' => $model->wiki_material_id, 'printer_material_id' => $model->printer_material_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wiki-material-printer-material-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
