<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialPrinterMaterial */

$this->title = $model->wiki_material_id;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Material Printer Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-material-printer-material-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'wiki_material_id' => $model->wiki_material_id, 'printer_material_id' => $model->printer_material_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'wiki_material_id' => $model->wiki_material_id, 'printer_material_id' => $model->printer_material_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'wiki_material_id',
            'printer_material_id',
        ],
    ]) ?>

</div>
