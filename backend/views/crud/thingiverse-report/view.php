<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseReport */

$this->title = $model->order_id;
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thingiverse-report-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->order_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->order_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'order_id',
            'date',
            'thing_id',
            'status',
            'earnings',
            'platform_fee',
            'transaction_fee',
            'app_transaction_code',
            'printing_fee',
            'shipping',
            'refund',
            'treatstock_transaction_fee',
        ],
    ]) ?>

</div>
