<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\ThingiverseReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="thingiverse-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'order_id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'thing_id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'earnings') ?>

    <?php // echo $form->field($model, 'platform_fee') ?>

    <?php // echo $form->field($model, 'transaction_fee') ?>

    <?php // echo $form->field($model, 'app_transaction_code') ?>

    <?php // echo $form->field($model, 'printing_fee') ?>

    <?php // echo $form->field($model, 'shipping') ?>

    <?php // echo $form->field($model, 'refund') ?>

    <?php // echo $form->field($model, 'treatstock_transaction_fee') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
