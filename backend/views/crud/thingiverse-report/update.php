<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseReport */

$this->title = 'Update Thingiverse Report: ' . $model->order_id;
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->order_id, 'url' => ['view', 'id' => $model->order_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="thingiverse-report-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
