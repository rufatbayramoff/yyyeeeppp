<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ThingiverseReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Thingiverse Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thingiverse-report-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Thingiverse Report', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'order_id',
            'date',
            'thing_id',
            'status',
            'earnings',
            // 'platform_fee',
            // 'transaction_fee',
            // 'app_transaction_code',
            // 'printing_fee',
            // 'shipping',
            // 'refund',
            // 'treatstock_transaction_fee',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
