<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseReport */

$this->title = 'Create Thingiverse Report';
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thingiverse-report-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
