<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseReport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="thingiverse-report-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'order_id')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'thing_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'earnings')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'platform_fee')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transaction_fee')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'app_transaction_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'printing_fee')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'refund')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'treatstock_transaction_fee')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
