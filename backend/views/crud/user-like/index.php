<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserLikeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Likes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-like-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Like', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'created_at',
            'object_type',
            'object_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
