<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserLike */

$this->title = 'Update User Like: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Likes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-like-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
