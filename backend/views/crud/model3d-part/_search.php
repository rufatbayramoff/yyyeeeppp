<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\Model3dPartSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model3d-part-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'model3d_id') ?>

    <?= $form->field($model, 'format') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'antivirus_checked_at') ?>

    <?php // echo $form->field($model, 'moderator_status') ?>

    <?php // echo $form->field($model, 'moderated_at') ?>

    <?php // echo $form->field($model, 'user_status') ?>

    <?php // echo $form->field($model, 'file_id') ?>

    <?php // echo $form->field($model, 'file_src_id') ?>

    <?php // echo $form->field($model, 'rotated_x') ?>

    <?php // echo $form->field($model, 'rotated_y') ?>

    <?php // echo $form->field($model, 'rotated_z') ?>

    <?php // echo $form->field($model, 'qty') ?>

    <?php // echo $form->field($model, 'model3d_part_properties_id') ?>

    <?php // echo $form->field($model, 'model3d_texture_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
