<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dPart */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Model3d Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-part-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'model3d_id',
            'format',
            'name',
            'title',
            'antivirus_checked_at',
            'moderator_status',
            'moderated_at',
            'user_status',
            'file_id',
            'file_src_id',
            'rotated_x',
            'rotated_y',
            'rotated_z',
            'qty',
            'model3d_part_properties_id',
            'model3d_texture_id',
        ],
    ]) ?>

</div>
