<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dPartSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3d Parts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-part-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model3d Part', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model3d_id',
            'format',
            'name',
            'title',
            // 'antivirus_checked_at',
            // 'moderator_status',
            // 'moderated_at',
            // 'user_status',
            // 'file_id',
            // 'file_src_id',
            // 'rotated_x',
            // 'rotated_y',
            // 'rotated_z',
            // 'qty',
            // 'model3d_part_properties_id',
            // 'model3d_texture_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
