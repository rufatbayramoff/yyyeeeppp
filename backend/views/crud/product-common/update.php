<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductCommon */

$this->title = 'Update Product Common: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Product Commons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->uid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-common-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
