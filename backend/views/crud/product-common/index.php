<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductCommonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Commons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-common-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product Common', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uid',
            'category_id',
            'company_id',
            'user_id',
            'product_status',
            // 'created_at',
            // 'updated_at',
            // 'is_active',
            // 'single_price',
            // 'price_currency',
            // 'title',
            // 'description:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
