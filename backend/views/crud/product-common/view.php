<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductCommon */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Product Commons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-common-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uid',
            'category_id',
            'company_id',
            'user_id',
            'product_status',
            'created_at',
            'updated_at',
            'is_active',
            'single_price',
            'price_currency',
            'title',
            'description:ntext',
        ],
    ]) ?>

</div>
