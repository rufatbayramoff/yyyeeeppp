<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductCommon */

$this->title = 'Create Product Common';
$this->params['breadcrumbs'][] = ['label' => 'Product Commons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-common-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
