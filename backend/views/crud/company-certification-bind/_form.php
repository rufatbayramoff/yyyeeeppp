<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyCertificationBind */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-certification-bind-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_service_id')->textInput() ?>

    <?= $form->field($model, 'certification_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
