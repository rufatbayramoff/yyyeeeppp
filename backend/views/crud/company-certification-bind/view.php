<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyCertificationBind */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Certification Binds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-certification-bind-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'product_uuid',
            'company_service_id',
            'certification_id',
        ],
    ]) ?>

</div>
