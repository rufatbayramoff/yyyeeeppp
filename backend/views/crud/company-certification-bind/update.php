<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyCertificationBind */

$this->title = 'Update Company Certification Bind: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Certification Binds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-certification-bind-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
