<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyCertificationBind */

$this->title = 'Create Company Certification Bind';
$this->params['breadcrumbs'][] = ['label' => 'Company Certification Binds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-certification-bind-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
