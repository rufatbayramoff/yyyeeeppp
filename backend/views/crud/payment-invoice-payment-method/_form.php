<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentInvoicePaymentMethod */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-invoice-payment-method-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'payment_invoice_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendor')->dropDownList([ 'braintree' => 'Braintree', 'ts' => 'Ts', 'thingiverse' => 'Thingiverse', 'invoice' => 'Invoice', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
