<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentInvoicePaymentMethod */

$this->title = 'Update Payment Invoice Payment Method: ' . $model->payment_invoice_uuid;
$this->params['breadcrumbs'][] = ['label' => 'Payment Invoice Payment Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->payment_invoice_uuid, 'url' => ['view', 'payment_invoice_uuid' => $model->payment_invoice_uuid, 'vendor' => $model->vendor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-invoice-payment-method-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
