<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentInvoicePaymentMethod */

$this->title = $model->payment_invoice_uuid;
$this->params['breadcrumbs'][] = ['label' => 'Payment Invoice Payment Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-invoice-payment-method-view">

    <p>
        <?= Html::a('Update', ['update', 'payment_invoice_uuid' => $model->payment_invoice_uuid, 'vendor' => $model->vendor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'payment_invoice_uuid' => $model->payment_invoice_uuid, 'vendor' => $model->vendor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'payment_invoice_uuid',
            'vendor',
        ],
    ]) ?>

</div>
