<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentInvoicePaymentMethod */

$this->title = 'Create Payment Invoice Payment Method';
$this->params['breadcrumbs'][] = ['label' => 'Payment Invoice Payment Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-invoice-payment-method-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
