<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WidgetStatitistics */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="widget-statitistics-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList([ 'psFacebook' => 'PsFacebook', 'psSite' => 'PsSite', 'printerSite' => 'PrinterSite', 'designerSite' => 'DesignerSite', '' => '', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'hosting_page')->textInput() ?>

    <?= $form->field($model, 'ps_id')->textInput() ?>

    <?= $form->field($model, 'ps_printer_id')->textInput() ?>

    <?= $form->field($model, 'designer_id')->textInput() ?>

    <?= $form->field($model, 'views_count')->textInput() ?>

    <?= $form->field($model, 'first_visit')->textInput() ?>

    <?= $form->field($model, 'last_visit')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
