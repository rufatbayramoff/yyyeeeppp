<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PreorderWork */

$this->title = 'Create Preorder Work';
$this->params['breadcrumbs'][] = ['label' => 'Preorder Works', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-work-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
