<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PreorderWork */

$this->title = 'Update Preorder Work: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Preorder Works', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="preorder-work-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
