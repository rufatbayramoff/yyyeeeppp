<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PreorderHistory */

$this->title = 'Create Preorder History';
$this->params['breadcrumbs'][] = ['label' => 'Preorder Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
