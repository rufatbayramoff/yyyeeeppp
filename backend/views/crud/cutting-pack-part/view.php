<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingPackPart */

$this->title = $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Pack Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-part-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uuid',
            'cutting_pack_file_uuid',
            'qty',
            'image',
            'width',
            'height',
            'cutting_length',
            'engraving_length',
            'material_id',
            'thickness',
            'color_id',
            'is_active',
        ],
    ]) ?>

</div>
