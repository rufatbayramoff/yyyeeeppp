<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingPackPartSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Pack Parts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-part-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Pack Part', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'cutting_pack_file_uuid',
            'qty',
            'image',
            'width',
            // 'height',
            // 'cutting_length',
            // 'engraving_length',
            // 'material_id',
            // 'thickness',
            // 'color_id',
            // 'is_active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
