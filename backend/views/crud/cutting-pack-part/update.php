<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingPackPart */

$this->title = 'Update Cutting Pack Part: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Pack Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-pack-part-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
