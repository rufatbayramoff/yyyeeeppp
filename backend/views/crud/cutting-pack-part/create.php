<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CuttingPackPart */

$this->title = 'Create Cutting Pack Part';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Pack Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-part-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
