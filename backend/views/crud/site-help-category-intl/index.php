<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteHelpCategoryIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Help Category Intls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-help-category-intl-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Site Help Category Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'category_id',
            'title',
            'info:ntext',
            'lang_iso',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
