<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StaticPageIntl */

$this->title = 'Create Static Page Intl';
$this->params['breadcrumbs'][] = ['label' => 'Static Page Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="static-page-intl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
