<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FailedDeliveryCheckMailbox */

$this->title = 'Create Failed Delivery Check Mailbox';
$this->params['breadcrumbs'][] = ['label' => 'Failed Delivery Check Mailboxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="failed-delivery-check-mailbox-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
