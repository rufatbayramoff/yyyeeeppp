<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FailedDeliveryCheckMailboxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Failed Delivery Check Mailboxes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="failed-delivery-check-mailbox-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Failed Delivery Check Mailbox', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'email:email',
            'message_uid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
