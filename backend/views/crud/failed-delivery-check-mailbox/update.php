<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FailedDeliveryCheckMailbox */

$this->title = 'Update Failed Delivery Check Mailbox: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Failed Delivery Check Mailboxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="failed-delivery-check-mailbox-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
