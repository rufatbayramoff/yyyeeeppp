<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Receipts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-receipt-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Receipt', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'order_id',
            'comment',
            'receipt_date',
            // 'pdf_file_id',
            // 'html_file_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
