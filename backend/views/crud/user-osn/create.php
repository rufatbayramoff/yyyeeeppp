<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserOsn */

$this->title = 'Create User Osn';
$this->params['breadcrumbs'][] = ['label' => 'User Osns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-osn-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
