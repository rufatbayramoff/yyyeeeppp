<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserOsn */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Osns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-osn-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'osn_code',
            'uid',
            'username',
            'email:email',
            'updated_at',
            'status',
            'auth_response',
            'link',
            'created_at',
        ],
    ]) ?>

</div>
