<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserOsnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Osns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-osn-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Osn', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'osn_code',
            'uid',
            'username',
            // 'email:email',
            // 'updated_at',
            // 'status',
            // 'auth_response',
            // 'link',
            // 'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
