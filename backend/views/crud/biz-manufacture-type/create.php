<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BizManufactureType */

$this->title = 'Create Biz Manufacture Type';
$this->params['breadcrumbs'][] = ['label' => 'Biz Manufacture Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biz-manufacture-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
