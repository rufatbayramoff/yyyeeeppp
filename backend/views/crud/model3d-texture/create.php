<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Model3dTexture */

$this->title = 'Create Model3d Texture';
$this->params['breadcrumbs'][] = ['label' => 'Model3d Textures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-texture-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
