<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dTextureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3d Textures';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-texture-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model3d Texture', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model3d_id',
            'model3d_file_id',
            'printer_material_id',
            'printer_color_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
