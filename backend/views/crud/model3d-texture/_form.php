<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dTexture */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model3d-texture-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'model3d_id')->textInput() ?>

    <?= $form->field($model, 'model3d_part_id')->textInput() ?>

    <?= $form->field($model, 'printer_material_id')->textInput() ?>

    <?= $form->field($model, 'printer_color_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
