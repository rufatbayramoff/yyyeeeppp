<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AffiliateSource */

$this->title = 'Create Affiliate Source';
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-source-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
