<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialPropertiesIntl */

$this->title = 'Create Wiki Material Properties Intl';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Material Properties Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-material-properties-intl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
