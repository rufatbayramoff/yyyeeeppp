<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserLocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Locations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-location-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Location', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'country_id',
            'region_id',
            'city_id',
            // 'address',
            // 'lat',
            // 'lon',
            // 'zip_code',
            // 'location_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
