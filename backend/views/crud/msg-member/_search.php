<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\MsgMemberSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="msg-member-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'topic_id') ?>

    <?= $form->field($model, 'folder_id') ?>

    <?= $form->field($model, 'have_unreaded_messages') ?>

    <?= $form->field($model, 'is_deleted') ?>

    <?php // echo $form->field($model, 'hide_message_before') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
