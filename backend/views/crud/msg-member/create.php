<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MsgMember */

$this->title = 'Create Msg Member';
$this->params['breadcrumbs'][] = ['label' => 'Msg Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-member-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
