<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MsgMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Msg Members';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-member-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Msg Member', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_id',
            'topic_id',
            'folder_id',
            'have_unreaded_messages',
            'is_deleted',
            // 'hide_message_before',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
