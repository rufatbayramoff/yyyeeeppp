<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MsgMember */

$this->title = $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'Msg Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-member-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'user_id' => $model->user_id, 'topic_id' => $model->topic_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'user_id' => $model->user_id, 'topic_id' => $model->topic_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'topic_id',
            'folder_id',
            'have_unreaded_messages',
            'is_deleted',
            'hide_message_before',
        ],
    ]) ?>

</div>
