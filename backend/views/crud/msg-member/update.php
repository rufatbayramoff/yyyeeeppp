<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MsgMember */

$this->title = 'Update Msg Member: ' . ' ' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'Msg Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_id, 'url' => ['view', 'user_id' => $model->user_id, 'topic_id' => $model->topic_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="msg-member-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
