<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterial */

$this->title = 'Update Wiki Material: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wiki-material-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
