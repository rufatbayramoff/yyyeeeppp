<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterColorIntl */

$this->title = 'Create Printer Color Intl';
$this->params['breadcrumbs'][] = ['label' => 'Printer Color Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-color-intl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
