<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-transaction-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Transaction', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'order_id',
            'transaction_id',
            'vendor',
            // 'created_at',
            // 'updated_at',
            // 'status',
            // 'type',
            // 'amount',
            // 'currency',
            // 'comment',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
