<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dTextureCatalogCostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3d Texture Catalog Costs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-texture-catalog-cost-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model3d Texture Catalog Cost', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'printer_material_id',
            'printer_color_id',
            'printer_material_group_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
