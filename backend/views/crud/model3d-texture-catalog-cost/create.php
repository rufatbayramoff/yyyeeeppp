<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Model3dTextureCatalogCost */

$this->title = 'Create Model3d Texture Catalog Cost';
$this->params['breadcrumbs'][] = ['label' => 'Model3d Texture Catalog Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-texture-catalog-cost-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
