<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dTextureCatalogCost */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Model3d Texture Catalog Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-texture-catalog-cost-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'printer_material_id',
            'printer_color_id',
            'printer_material_group_id',
        ],
    ]) ?>

</div>
