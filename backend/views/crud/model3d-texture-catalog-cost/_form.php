<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dTextureCatalogCost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model3d-texture-catalog-cost-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'printer_material_id')->textInput() ?>

    <?= $form->field($model, 'printer_color_id')->textInput() ?>

    <?= $form->field($model, 'printer_material_group_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
