<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ConnectedAppIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Connected App Intls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connected-app-intl-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Connected App Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model_id',
            'title',
            'short_descr:ntext',
            'info:ntext',
            // 'instructions:ntext',
            // 'lang_iso',
            // 'is_active:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
