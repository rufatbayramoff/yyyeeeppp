<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ConnectedAppIntl */

$this->title = 'Create Connected App Intl';
$this->params['breadcrumbs'][] = ['label' => 'Connected App Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connected-app-intl-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
