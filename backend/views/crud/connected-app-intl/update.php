<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ConnectedAppIntl */

$this->title = 'Update Connected App Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Connected App Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="connected-app-intl-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
