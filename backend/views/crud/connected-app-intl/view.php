<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ConnectedAppIntl */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Connected App Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connected-app-intl-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'model_id',
            'title',
            'short_descr:ntext',
            'info:ntext',
            'instructions:ntext',
            'lang_iso',
            'is_active:boolean',
        ],
    ]) ?>

</div>
