<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingMaterial */

$this->title = 'Update Cutting Material: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-material-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
