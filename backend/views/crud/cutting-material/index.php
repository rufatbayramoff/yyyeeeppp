<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingMaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Materials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-material-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Material', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            'title',
            'default_thickness',
            'default_width',
            // 'default_length',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
