<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BizIndustry */

$this->title = 'Create Biz Industry';
$this->params['breadcrumbs'][] = ['label' => 'Biz Industries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biz-industry-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
