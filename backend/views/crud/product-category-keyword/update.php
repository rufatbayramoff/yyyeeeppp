<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductCategoryKeyword */

$this->title = 'Update Product Category Keyword: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Category Keywords', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-category-keyword-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
