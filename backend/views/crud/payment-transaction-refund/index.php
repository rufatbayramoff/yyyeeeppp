<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentTransactionRefundSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Transaction Refunds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-transaction-refund-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Transaction Refund', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'transaction_id',
            'created_at',
            'user_id',
            'amount',
            // 'currency',
            // 'status',
            // 'refund_type',
            // 'from_user_id',
            // 'comment',
            // 'transaction_refund_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
