<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentTransactionRefund */

$this->title = 'Create Payment Transaction Refund';
$this->params['breadcrumbs'][] = ['label' => 'Payment Transaction Refunds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-transaction-refund-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
