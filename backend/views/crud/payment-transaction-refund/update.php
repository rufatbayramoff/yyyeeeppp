<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentTransactionRefund */

$this->title = 'Update Payment Transaction Refund: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Transaction Refunds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-transaction-refund-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
