<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlockBind */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Block Binds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-block-bind-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'product_uuid',
            'company_service_id',
            'block_id',
        ],
    ]) ?>

</div>
