<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlockBind */

$this->title = 'Create Company Block Bind';
$this->params['breadcrumbs'][] = ['label' => 'Company Block Binds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-block-bind-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
