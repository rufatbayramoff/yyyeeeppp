<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CompanyBlockBindSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Company Block Binds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-block-bind-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Company Block Bind', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'product_uuid',
            'company_service_id',
            'block_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
