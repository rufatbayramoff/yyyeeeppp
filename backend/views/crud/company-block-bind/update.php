<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlockBind */

$this->title = 'Update Company Block Bind: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Block Binds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-block-bind-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
