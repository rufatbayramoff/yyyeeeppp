<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMachinePhoto */

$this->title = 'Update Wiki Machine Photo: ' . $model->photo_file_uuid;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Photos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->photo_file_uuid, 'url' => ['view', 'id' => $model->photo_file_uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wiki-machine-photo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
