<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMachinePhoto */

$this->title = 'Create Wiki Machine Photo';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Photos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-photo-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
