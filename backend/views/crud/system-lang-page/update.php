<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SystemLangPage */

$this->title = 'Update System Lang Page: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'System Lang Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="system-lang-page-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
