<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SystemLangPage */

$this->title = 'Create System Lang Page';
$this->params['breadcrumbs'][] = ['label' => 'System Lang Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-page-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
