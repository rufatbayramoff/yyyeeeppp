<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\SystemLangPageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="system-lang-page-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'system_lang_source_id') ?>

    <?= $form->field($model, 'url') ?>

    <?= $form->field($model, 'referer') ?>

    <?= $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'lang_iso') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'isPost') ?>

    <?php // echo $form->field($model, 'isJs') ?>

    <?php // echo $form->field($model, 'getParams') ?>

    <?php // echo $form->field($model, 'postParams') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
