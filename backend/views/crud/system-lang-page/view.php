<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SystemLangPage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'System Lang Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-page-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'system_lang_source_id',
            'url:url',
            'referer',
            'created_at',
            'lang_iso',
            'user_id',
            'isPost',
            'isJs',
            'getParams',
            'postParams',
        ],
    ]) ?>

</div>
