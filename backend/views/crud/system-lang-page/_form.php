<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SystemLangPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="system-lang-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'system_lang_source_id')->textInput() ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'referer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'lang_iso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'isPost')->textInput() ?>

    <?= $form->field($model, 'isJs')->textInput() ?>

    <?= $form->field($model, 'getParams')->textInput() ?>

    <?= $form->field($model, 'postParams')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
