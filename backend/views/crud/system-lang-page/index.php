<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SystemLangPageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'System Lang Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-page-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create System Lang Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'system_lang_source_id',
            'url:url',
            'referer',
            'created_at',
            // 'lang_iso',
            // 'user_id',
            // 'isPost',
            // 'isJs',
            // 'getParams',
            // 'postParams',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
