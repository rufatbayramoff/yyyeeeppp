<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingPackPageImage */

$this->title = 'Update Cutting Pack Page Image: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Pack Page Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-pack-page-image-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
