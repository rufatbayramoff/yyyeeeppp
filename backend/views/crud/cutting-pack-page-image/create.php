<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CuttingPackPageImage */

$this->title = 'Create Cutting Pack Page Image';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Pack Page Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-page-image-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
