<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingPackPageImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Pack Page Images';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-page-image-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Pack Page Image', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'cutting_pack_page_uuid',
            'file_uuid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
