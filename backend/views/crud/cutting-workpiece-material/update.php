<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingWorkpieceMaterial */

$this->title = 'Update Cutting Workpiece Material: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Workpiece Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-workpiece-material-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
