<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CuttingWorkpieceMaterial */

$this->title = 'Create Cutting Workpiece Material';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Workpiece Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-workpiece-material-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
