<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingWorkpieceMaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Workpiece Materials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-workpiece-material-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Workpiece Material', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'company_id',
            'material_id',
            'width',
            'height',
            // 'thickness',
            // 'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
