<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentInvoiceAnalytic */

$this->title = 'Create Payment Invoice Analytic';
$this->params['breadcrumbs'][] = ['label' => 'Payment Invoice Analytics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-invoice-analytic-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
