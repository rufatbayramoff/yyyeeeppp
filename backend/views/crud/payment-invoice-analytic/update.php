<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentInvoiceAnalytic */

$this->title = 'Update Payment Invoice Analytic: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Invoice Analytics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-invoice-analytic-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
