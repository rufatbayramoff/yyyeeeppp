<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentInvoiceAnalytic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-invoice-analytic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sort')->dropDownList([ 'default' => 'Default', 'low_price' => 'Low price', 'price' => 'Price', 'rating' => 'Rating', 'distance' => 'Distance', 'default_2021' => 'Default 2021', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'payment_invoice_uuid')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
