<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttemptModerationFile */

$this->title = 'Create Store Order Attempt Moderation File';
$this->params['breadcrumbs'][] = ['label' => 'Store Order Attempt Moderation Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-attempt-moderation-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
