<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttemptModerationFile */

$this->title = 'Update Store Order Attempt Moderation File: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Attempt Moderation Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-order-attempt-moderation-file-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
