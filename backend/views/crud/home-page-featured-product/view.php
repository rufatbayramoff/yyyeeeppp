<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageFeaturedProduct */

$this->title = $model->featured_category_id;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Featured Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-featured-product-view">

    <p>
        <?= Html::a('Update', ['update', 'featured_category_id' => $model->featured_category_id, 'product_uuid' => $model->product_uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'featured_category_id' => $model->featured_category_id, 'product_uuid' => $model->product_uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'featured_category_id',
            'product_uuid',
        ],
    ]) ?>

</div>
