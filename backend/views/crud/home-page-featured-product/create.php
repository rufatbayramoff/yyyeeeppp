<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HomePageFeaturedProduct */

$this->title = 'Create Home Page Featured Product';
$this->params['breadcrumbs'][] = ['label' => 'Home Page Featured Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-featured-product-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
