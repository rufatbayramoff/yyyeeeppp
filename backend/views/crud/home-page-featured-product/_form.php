<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageFeaturedProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="home-page-featured-product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'featured_category_id')->textInput() ?>

    <?= $form->field($model, 'product_uuid')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
