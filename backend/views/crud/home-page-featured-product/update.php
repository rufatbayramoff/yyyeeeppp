<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageFeaturedProduct */

$this->title = 'Update Home Page Featured Product: ' . $model->featured_category_id;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Featured Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->featured_category_id, 'url' => ['view', 'featured_category_id' => $model->featured_category_id, 'product_uuid' => $model->product_uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-page-featured-product-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
