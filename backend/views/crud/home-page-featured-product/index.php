<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\HomePageFeaturedProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Home Page Featured Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-featured-product-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Home Page Featured Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'featured_category_id',
            'product_uuid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
