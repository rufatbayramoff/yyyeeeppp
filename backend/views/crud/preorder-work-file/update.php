<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PreorderWorkFile */

$this->title = 'Update Preorder Work File: ' . $model->work_id;
$this->params['breadcrumbs'][] = ['label' => 'Preorder Work Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->work_id, 'url' => ['view', 'work_id' => $model->work_id, 'file_id' => $model->file_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="preorder-work-file-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
