<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PreorderWorkFile */

$this->title = 'Create Preorder Work File';
$this->params['breadcrumbs'][] = ['label' => 'Preorder Work Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-work-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
