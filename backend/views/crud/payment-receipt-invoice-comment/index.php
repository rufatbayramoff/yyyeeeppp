<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentReceiptInvoiceCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Receipt Invoice Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-receipt-invoice-comment-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Receipt Invoice Comment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'receipt_id',
            'payment_invoice_uuid',
            'comment',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
