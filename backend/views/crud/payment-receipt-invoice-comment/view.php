<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentReceiptInvoiceComment */

$this->title = $model->payment_invoice_uuid;
$this->params['breadcrumbs'][] = ['label' => 'Payment Receipt Invoice Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-receipt-invoice-comment-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->payment_invoice_uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->payment_invoice_uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'receipt_id',
            'payment_invoice_uuid',
            'comment',
        ],
    ]) ?>

</div>
