<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentReceiptInvoiceComment */

$this->title = 'Create Payment Receipt Invoice Comment';
$this->params['breadcrumbs'][] = ['label' => 'Payment Receipt Invoice Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-receipt-invoice-comment-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
