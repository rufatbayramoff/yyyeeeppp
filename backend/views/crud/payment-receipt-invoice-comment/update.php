<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentReceiptInvoiceComment */

$this->title = 'Update Payment Receipt Invoice Comment: ' . $model->payment_invoice_uuid;
$this->params['breadcrumbs'][] = ['label' => 'Payment Receipt Invoice Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->payment_invoice_uuid, 'url' => ['view', 'id' => $model->payment_invoice_uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-receipt-invoice-comment-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
