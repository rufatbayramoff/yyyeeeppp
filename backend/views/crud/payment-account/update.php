<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentAccount */

$this->title = 'Update Payment Account: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-account-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
