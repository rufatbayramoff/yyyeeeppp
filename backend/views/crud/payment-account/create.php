<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentAccount */

$this->title = 'Create Payment Account';
$this->params['breadcrumbs'][] = ['label' => 'Payment Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-account-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
