<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\StoreOrderAttempDatesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-order-attemp-dates-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'attemp_id') ?>

    <?= $form->field($model, 'request_reason') ?>

    <?= $form->field($model, 'accepted_date') ?>

    <?= $form->field($model, 'fact_printed_at') ?>

    <?= $form->field($model, 'plan_printed_at') ?>

    <?php // echo $form->field($model, 'start_print_at') ?>

    <?php // echo $form->field($model, 'finish_print_at') ?>

    <?php // echo $form->field($model, 'expired_at') ?>

    <?php // echo $form->field($model, 'delivered_at') ?>

    <?php // echo $form->field($model, 'shipped_at') ?>

    <?php // echo $form->field($model, 'received_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
