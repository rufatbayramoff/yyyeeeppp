<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttempDates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-order-attemp-dates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'attemp_id')->textInput() ?>

    <?= $form->field($model, 'request_reason')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accepted_date')->textInput() ?>

    <?= $form->field($model, 'fact_printed_at')->textInput() ?>

    <?= $form->field($model, 'plan_printed_at')->textInput() ?>

    <?= $form->field($model, 'start_print_at')->textInput() ?>

    <?= $form->field($model, 'finish_print_at')->textInput() ?>

    <?= $form->field($model, 'expired_at')->textInput() ?>

    <?= $form->field($model, 'delivered_at')->textInput() ?>

    <?= $form->field($model, 'shipped_at')->textInput() ?>

    <?= $form->field($model, 'received_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
