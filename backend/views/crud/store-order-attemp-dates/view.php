<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttempDates */

$this->title = $model->attemp_id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Attemp Dates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-attemp-dates-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->attemp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->attemp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'attemp_id',
            'request_reason',
            'accepted_date',
            'fact_printed_at',
            'plan_printed_at',
            'start_print_at',
            'finish_print_at',
            'expired_at',
            'delivered_at',
            'shipped_at',
            'received_at',
        ],
    ]) ?>

</div>
