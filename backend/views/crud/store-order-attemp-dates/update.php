<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttempDates */

$this->title = 'Update Store Order Attemp Dates: ' . $model->attemp_id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Attemp Dates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->attemp_id, 'url' => ['view', 'id' => $model->attemp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-order-attemp-dates-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
