<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderAttempDatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Attemp Dates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-attemp-dates-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order Attemp Dates', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'attemp_id',
            'request_reason',
            'accepted_date',
            'fact_printed_at',
            'plan_printed_at',
            // 'start_print_at',
            // 'finish_print_at',
            // 'expired_at',
            // 'delivered_at',
            // 'shipped_at',
            // 'received_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
