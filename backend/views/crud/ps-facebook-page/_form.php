<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PsFacebookPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-facebook-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ps_id')->textInput() ?>

    <?= $form->field($model, 'facebook_page_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
