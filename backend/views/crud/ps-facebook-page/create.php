<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsFacebookPage */

$this->title = 'Create Ps Facebook Page';
$this->params['breadcrumbs'][] = ['label' => 'Ps Facebook Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-facebook-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
