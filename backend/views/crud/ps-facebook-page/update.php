<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsFacebookPage */

$this->title = 'Update Ps Facebook Page: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Facebook Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-facebook-page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
