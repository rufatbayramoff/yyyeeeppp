<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsMachineDeliveryCountrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Machine Delivery Countries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-machine-delivery-country-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ps Machine Delivery Country', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ps_machine_delivery_id',
            'geo_country_id',
            'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
