<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsMachineDeliveryCountry */

$this->title = 'Update Ps Machine Delivery Country: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Machine Delivery Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-machine-delivery-country-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
