<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsMachineDeliveryCountry */

$this->title = 'Create Ps Machine Delivery Country';
$this->params['breadcrumbs'][] = ['label' => 'Ps Machine Delivery Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-machine-delivery-country-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
