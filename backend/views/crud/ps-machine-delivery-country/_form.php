<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PsMachineDeliveryCountry */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-machine-delivery-country-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ps_machine_delivery_id')->textInput() ?>

    <?= $form->field($model, 'geo_country_id')->textInput() ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
