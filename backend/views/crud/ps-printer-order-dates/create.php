<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterOrderDates */

$this->title = 'Create Ps Printer Order Dates';
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Order Dates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-order-dates-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
