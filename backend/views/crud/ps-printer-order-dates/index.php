<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsPrinterOrderDatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Printer Order Dates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-order-dates-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ps Printer Order Dates', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ps_printer_id',
            'order_id',
            'accepted_date',
            'fact_printed_at',
            // 'plan_printed_at',
            // 'start_print_at',
            // 'finish_print_at',
            // 'expired_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
