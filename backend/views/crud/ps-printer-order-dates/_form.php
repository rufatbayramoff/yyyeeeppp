<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterOrderDates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-printer-order-dates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ps_printer_id')->textInput() ?>

    <?= $form->field($model, 'order_id')->textInput() ?>

    <?= $form->field($model, 'accepted_date')->textInput() ?>

    <?= $form->field($model, 'fact_printed_at')->textInput() ?>

    <?= $form->field($model, 'plan_printed_at')->textInput() ?>

    <?= $form->field($model, 'start_print_at')->textInput() ?>

    <?= $form->field($model, 'finish_print_at')->textInput() ?>

    <?= $form->field($model, 'expired_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
