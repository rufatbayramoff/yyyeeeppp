<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterOrderDates */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Order Dates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-order-dates-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ps_printer_id',
            'order_id',
            'accepted_date',
            'fact_printed_at',
            'plan_printed_at',
            'start_print_at',
            'finish_print_at',
            'expired_at',
        ],
    ]) ?>

</div>
