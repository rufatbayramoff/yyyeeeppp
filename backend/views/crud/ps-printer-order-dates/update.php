<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterOrderDates */

$this->title = 'Update Ps Printer Order Dates: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Order Dates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-printer-order-dates-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
