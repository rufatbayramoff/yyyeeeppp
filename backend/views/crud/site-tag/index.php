<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteTagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Tags';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-tag-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Site Tag', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'text',
            'status',
            'created_at',
            'updated_at',
            // 'qty_used',
            // 'object_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
