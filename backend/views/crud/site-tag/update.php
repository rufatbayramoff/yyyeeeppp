<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteTag */

$this->title = 'Update Site Tag: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Site Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-tag-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
