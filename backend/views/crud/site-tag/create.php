<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteTag */

$this->title = 'Create Site Tag';
$this->params['breadcrumbs'][] = ['label' => 'Site Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-tag-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
