<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductSnapshotFile */

$this->title = 'Create Product Snapshot File';
$this->params['breadcrumbs'][] = ['label' => 'Product Snapshot Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-snapshot-file-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
