<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSnapshotFile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-snapshot-file-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_snapshot_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_uuid')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
