<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSnapshotFile */

$this->title = 'Update Product Snapshot File: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Snapshot Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-snapshot-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
