<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dImgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3d Imgs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-img-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model3d Img', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model3d_id',
            'size',
            'title',
            'created_at',
            // 'updated_at',
            // 'deleted_at',
            // 'moderated_at',
            // 'is_moderated:boolean',
            // 'basename',
            // 'extension',
            // 'user_id',
            // 'type',
            // 'review_id',
            // 'file_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
