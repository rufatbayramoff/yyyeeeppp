<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductBlockImage */

$this->title = 'Update Product Block Image: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Block Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-block-image-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
