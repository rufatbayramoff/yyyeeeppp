<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductBlockImage */

$this->title = 'Create Product Block Image';
$this->params['breadcrumbs'][] = ['label' => 'Product Block Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-block-image-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
