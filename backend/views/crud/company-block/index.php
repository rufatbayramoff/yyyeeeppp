<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CompanyBlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Company Blocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-block-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Company Block', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'content:ntext',
            'created_at',
            'is_visible',
            // 'position',
            // 'company_id',
            // 'videos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
