<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MsgReport */

$this->title = 'Create Msg Report';
$this->params['breadcrumbs'][] = ['label' => 'Msg Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-report-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
