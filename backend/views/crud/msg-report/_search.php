<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\MsgReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="msg-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'message_id') ?>

    <?= $form->field($model, 'reason_id') ?>

    <?= $form->field($model, 'create_user_id') ?>

    <?= $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'moderated') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
