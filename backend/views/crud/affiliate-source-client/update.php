<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AffiliateSourceClient */

$this->title = 'Update Affiliate Source Client: ' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Source Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_id, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="affiliate-source-client-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
