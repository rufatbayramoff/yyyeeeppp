<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AffiliateSourceClient */

$this->title = 'Create Affiliate Source Client';
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Source Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-source-client-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
