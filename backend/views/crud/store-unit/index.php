<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Units';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-unit-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Unit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model3d_id',
            'price',
            'price_per_print',
            'price_currency',
            // 'created_at',
            // 'updated_at',
            // 'deleted_at',
            // 'moderated_at',
            // 'status',
            // 'license_id',
            // 'is_active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
