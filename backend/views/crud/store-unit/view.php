<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoreUnit */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-unit-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'model3d_id',
            'price',
            'price_per_print',
            'price_currency',
            'created_at',
            'updated_at',
            'deleted_at',
            'moderated_at',
            'status',
            'license_id',
            'is_active',
        ],
    ]) ?>

</div>
