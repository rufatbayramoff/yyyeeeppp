<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterTest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-printer-test-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ps_printer_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'common' => 'Common', 'professional' => 'Professional', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'new' => 'New', 'checked' => 'Checked', 'failed' => 'Failed', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'image_file_ids')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
