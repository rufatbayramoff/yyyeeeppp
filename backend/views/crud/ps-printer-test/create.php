<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterTest */

$this->title = 'Create Ps Printer Test';
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-test-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
