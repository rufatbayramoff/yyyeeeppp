<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterTest */

$this->title = $model->ps_printer_id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-test-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ps_printer_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ps_printer_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ps_printer_id',
            'type',
            'status',
            'image_file_ids:ntext',
            'comment:ntext',
            'created_at',
        ],
    ]) ?>

</div>
