<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterTest */

$this->title = 'Update Ps Printer Test: ' . $model->ps_printer_id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ps_printer_id, 'url' => ['view', 'id' => $model->ps_printer_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-printer-test-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
