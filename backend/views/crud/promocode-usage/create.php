<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PromocodeUsage */

$this->title = 'Create Promocode Usage';
$this->params['breadcrumbs'][] = ['label' => 'Promocode Usages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-usage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
