<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PromocodeUsageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promocode Usages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-usage-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Promocode Usage', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'promocode_id',
            'used_at',
            'order_id',
            // 'amount',
            // 'amount_currency',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
