<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PromocodeUsage */

$this->title = 'Update Promocode Usage: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Promocode Usages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promocode-usage-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
