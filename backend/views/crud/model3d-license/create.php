<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Model3dLicense */

$this->title = 'Create Model3d License';
$this->params['breadcrumbs'][] = ['label' => 'Model3d Licenses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-license-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
