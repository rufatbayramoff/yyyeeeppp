<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductDelivery */

$this->title = 'Create Product Delivery';
$this->params['breadcrumbs'][] = ['label' => 'Product Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-delivery-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
