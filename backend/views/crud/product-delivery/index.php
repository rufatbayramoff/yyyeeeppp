<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductDeliverySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Deliveries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-delivery-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product Delivery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'express_delivery_country_id',
            'express_delivery_first_item',
            'express_delivery_following_item',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
