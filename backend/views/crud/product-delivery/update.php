<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductDelivery */

$this->title = 'Update Product Delivery: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Product Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-delivery-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
