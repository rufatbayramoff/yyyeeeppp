<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductDelivery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-delivery-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'express_delivery_country_id')->textInput() ?>

    <?= $form->field($model, 'express_delivery_first_item')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'express_delivery_following_item')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
