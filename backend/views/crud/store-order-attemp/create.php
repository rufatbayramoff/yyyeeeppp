<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttemp */

$this->title = 'Create Store Order Attemp';
$this->params['breadcrumbs'][] = ['label' => 'Store Order Attemps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-attemp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
