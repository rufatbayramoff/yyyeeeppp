<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderAttempSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Attemps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-attemp-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order Attemp', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'order_id',
            'printer_id',
            'dates_id',
            'status',
            // 'created_at',
            // 'is_offer',
            // 'cancelled_at',
            // 'cancel_initiator',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
