<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttemp */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Attemps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-attemp-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'printer_id',
            'dates_id',
            'status',
            'created_at',
            'is_offer',
            'cancelled_at',
            'cancel_initiator',
        ],
    ]) ?>

</div>
