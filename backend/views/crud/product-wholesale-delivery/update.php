<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductWholesaleDelivery */

$this->title = 'Update Product Wholesale Delivery: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Product Wholesale Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-wholesale-delivery-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
