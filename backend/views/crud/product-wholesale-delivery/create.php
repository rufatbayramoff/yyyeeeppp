<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductWholesaleDelivery */

$this->title = 'Create Product Wholesale Delivery';
$this->params['breadcrumbs'][] = ['label' => 'Product Wholesale Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-wholesale-delivery-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
