<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TsInternalPurchaseCertification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ts-internal-purchase-certification-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ts_internal_purchase_uid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_service_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'verified' => 'Verified', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'expire_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
