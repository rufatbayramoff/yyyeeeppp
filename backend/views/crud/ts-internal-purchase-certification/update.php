<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TsInternalPurchaseCertification */

$this->title = 'Update Ts Internal Purchase Certification: ' . $model->ts_internal_purchase_uid;
$this->params['breadcrumbs'][] = ['label' => 'Ts Internal Purchase Certifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ts_internal_purchase_uid, 'url' => ['view', 'id' => $model->ts_internal_purchase_uid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ts-internal-purchase-certification-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
