<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\TsInternalPurchaseCertificationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ts-internal-purchase-certification-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ts_internal_purchase_uid') ?>

    <?= $form->field($model, 'company_service_id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'expire_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
