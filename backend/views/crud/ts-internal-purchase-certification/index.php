<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TsInternalPurchaseCertificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ts Internal Purchase Certifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ts-internal-purchase-certification-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ts Internal Purchase Certification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ts_internal_purchase_uid',
            'company_service_id',
            'type',
            'expire_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
