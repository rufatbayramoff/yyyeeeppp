<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TsInternalPurchaseCertification */

$this->title = 'Create Ts Internal Purchase Certification';
$this->params['breadcrumbs'][] = ['label' => 'Ts Internal Purchase Certifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ts-internal-purchase-certification-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
