<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TsInternalPurchaseCertification */

$this->title = $model->ts_internal_purchase_uid;
$this->params['breadcrumbs'][] = ['label' => 'Ts Internal Purchase Certifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ts-internal-purchase-certification-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ts_internal_purchase_uid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ts_internal_purchase_uid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ts_internal_purchase_uid',
            'company_service_id',
            'type',
            'expire_date',
        ],
    ]) ?>

</div>
