<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-printer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ps_id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'printer_id')->textInput() ?>

    <?= $form->field($model, 'price_per_hour')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_iso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_per_volume')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'min_order_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location_id')->textInput() ?>

    <?= $form->field($model, 'user_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_online')->textInput() ?>

    <?= $form->field($model, 'moderator_status')->dropDownList([ 'new' => 'New', 'updated' => 'Updated', 'checking' => 'Checking', 'checked' => 'Checked', 'banned' => 'Banned', 'rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'is_deleted')->textInput() ?>

    <?= $form->field($model, 'is_location_change')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
