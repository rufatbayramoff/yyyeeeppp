<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinter */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ps Printers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ps_id',
            'title',
            'description',
            'image',
            'printer_id',
            'price_per_hour',
            'currency_iso',
            'price_per_volume',
            'min_order_price',
            'location_id',
            'user_status',
            'is_online',
            'moderator_status',
            'created_at',
            'updated_at',
            'is_deleted',
            'is_location_change',
        ],
    ]) ?>

</div>
