<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsPrinterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Printers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ps Printer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ps_id',
            'title',
            'description',
            'image',
            // 'printer_id',
            // 'price_per_hour',
            // 'currency_iso',
            // 'price_per_volume',
            // 'min_order_price',
            // 'location_id',
            // 'user_status',
            // 'is_online',
            // 'moderator_status',
            // 'created_at',
            // 'updated_at',
            // 'is_deleted',
            // 'is_location_change',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
