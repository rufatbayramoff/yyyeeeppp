<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceBlock */

$this->title = 'Update Company Service Block: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company Service Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-service-block-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
