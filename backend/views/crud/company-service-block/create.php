<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceBlock */

$this->title = 'Create Company Service Block';
$this->params['breadcrumbs'][] = ['label' => 'Company Service Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-block-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
