<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentWarning */

$this->title = 'Create Payment Warning';
$this->params['breadcrumbs'][] = ['label' => 'Payment Warnings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-warning-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
