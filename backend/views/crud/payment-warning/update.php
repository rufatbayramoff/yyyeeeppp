<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentWarning */

$this->title = 'Update Payment Warning: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Warnings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-warning-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
