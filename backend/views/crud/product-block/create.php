<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductBlock */

$this->title = 'Create Product Block';
$this->params['breadcrumbs'][] = ['label' => 'Product Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-block-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
