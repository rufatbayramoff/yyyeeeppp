<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductBlock */

$this->title = 'Update Product Block: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Product Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-block-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
