<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BrowserPushSubscription */

$this->title = 'Update Browser Push Subscription: ' . $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Browser Push Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uid, 'url' => ['view', 'id' => $model->uid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="browser-push-subscription-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
