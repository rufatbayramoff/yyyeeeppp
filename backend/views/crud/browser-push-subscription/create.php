<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BrowserPushSubscription */

$this->title = 'Create Browser Push Subscription';
$this->params['breadcrumbs'][] = ['label' => 'Browser Push Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="browser-push-subscription-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
