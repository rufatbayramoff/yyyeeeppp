<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BrowserPushSubscriptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Browser Push Subscriptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="browser-push-subscription-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Browser Push Subscription', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uid',
            'created_at',
            'user_agent',
            'endpoint',
            'expiration_time',
            // 'key_auth',
            // 'key_p',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
