<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\BrowserPushSubscriptionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="browser-push-subscription-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'uid') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'user_agent') ?>

    <?= $form->field($model, 'endpoint') ?>

    <?= $form->field($model, 'expiration_time') ?>

    <?php // echo $form->field($model, 'key_auth') ?>

    <?php // echo $form->field($model, 'key_p') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
