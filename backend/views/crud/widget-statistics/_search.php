<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\WidgetStatisticsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="widget-statistics-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'hosting_page') ?>

    <?= $form->field($model, 'ps_id') ?>

    <?= $form->field($model, 'ps_printer_id') ?>

    <?php // echo $form->field($model, 'designer_id') ?>

    <?php // echo $form->field($model, 'views_count') ?>

    <?php // echo $form->field($model, 'first_visit') ?>

    <?php // echo $form->field($model, 'last_visit') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
