<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WidgetStatisticsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Widget Statistics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-statistics-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Widget Statistics', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',
            'hosting_page',
            'ps_id',
            'ps_printer_id',
            // 'designer_id',
            // 'views_count',
            // 'first_visit',
            // 'last_visit',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
