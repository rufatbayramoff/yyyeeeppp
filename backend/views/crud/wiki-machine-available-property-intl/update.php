<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineAvailablePropertyIntl */

$this->title = 'Update Wiki Machine Available Property Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Available Property Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wiki-machine-available-property-intl-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
