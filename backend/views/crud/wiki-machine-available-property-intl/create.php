<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineAvailablePropertyIntl */

$this->title = 'Create Wiki Machine Available Property Intl';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Available Property Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-available-property-intl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
