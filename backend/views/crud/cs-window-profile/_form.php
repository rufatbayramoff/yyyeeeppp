<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cs-window-profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cs_window_uid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'thickness')->textInput() ?>

    <?= $form->field($model, 'chambers')->textInput() ?>

    <?= $form->field($model, 'max_glass')->textInput() ?>

    <?= $form->field($model, 'max_width')->textInput() ?>

    <?= $form->field($model, 'max_height')->textInput() ?>

    <?= $form->field($model, 'noise_reduction')->textInput() ?>

    <?= $form->field($model, 'thermal_resistance')->textInput() ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
