<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CsWindowProfile */

$this->title = 'Create Cs Window Profile';
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-profile-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
