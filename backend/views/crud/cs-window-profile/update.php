<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowProfile */

$this->title = 'Update Cs Window Profile: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cs-window-profile-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
