<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialPhotoIntl */

$this->title = 'Create Wiki Material Photo Intl';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Material Photo Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-material-photo-intl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
