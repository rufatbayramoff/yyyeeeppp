<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceType */

$this->title = 'Create Company Service Type';
$this->params['breadcrumbs'][] = ['label' => 'Company Service Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
