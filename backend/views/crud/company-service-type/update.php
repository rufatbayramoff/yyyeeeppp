<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceType */

$this->title = 'Update Company Service Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Company Service Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-service-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
