<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderReviewFile */

$this->title = $model->review_id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Review Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-review-file-view">

    <p>
        <?= Html::a('Update', ['update', 'review_id' => $model->review_id, 'file_uuid' => $model->file_uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'review_id' => $model->review_id, 'file_uuid' => $model->file_uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'review_id',
            'file_uuid',
            'is_main',
            'created_at',
            'sort_index',
        ],
    ]) ?>

</div>
