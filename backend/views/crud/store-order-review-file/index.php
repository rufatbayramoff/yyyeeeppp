<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderReviewFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Review Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-review-file-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order Review File', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'review_id',
            'file_uuid',
            'is_main',
            'created_at',
            'sort_index',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
