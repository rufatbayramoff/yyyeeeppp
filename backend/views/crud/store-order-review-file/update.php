<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderReviewFile */

$this->title = 'Update Store Order Review File: ' . $model->review_id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Review Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->review_id, 'url' => ['view', 'review_id' => $model->review_id, 'file_uuid' => $model->file_uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-order-review-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
