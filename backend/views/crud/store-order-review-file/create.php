<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderReviewFile */

$this->title = 'Create Store Order Review File';
$this->params['breadcrumbs'][] = ['label' => 'Store Order Review Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-review-file-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
