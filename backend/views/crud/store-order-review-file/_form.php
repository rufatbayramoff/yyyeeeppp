<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderReviewFile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-order-review-file-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'review_id')->textInput() ?>

    <?= $form->field($model, 'file_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_main')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'sort_index')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
