<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SystemLang */

$this->title = 'Create System Lang';
$this->params['breadcrumbs'][] = ['label' => 'System Langs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
