<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowQuote */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Quotes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-quote-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'uid',
            'company_service_id',
            'cs_window_snapshot_uid',
            'user_session_id',
            'user_id',
            'created_at',
            'updated_at',
            'viewing_at',
            'phone',
            'email:email',
            'contact_name',
            'status',
            'address',
            'notes:ntext',
            'total_price',
            'currency',
            'measurement',
        ],
    ]) ?>

</div>
