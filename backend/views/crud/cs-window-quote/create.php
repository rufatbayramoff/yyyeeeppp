<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CsWindowQuote */

$this->title = 'Create Cs Window Quote';
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Quotes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-quote-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
