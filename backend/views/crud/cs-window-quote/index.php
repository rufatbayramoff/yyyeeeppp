<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CsWindowQuoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cs Window Quotes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-quote-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cs Window Quote', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'uid',
            'company_service_id',
            'cs_window_snapshot_uid',
            'user_session_id',
            // 'user_id',
            // 'created_at',
            // 'updated_at',
            // 'viewing_at',
            // 'phone',
            // 'email:email',
            // 'contact_name',
            // 'status',
            // 'address',
            // 'notes:ntext',
            // 'total_price',
            // 'currency',
            // 'measurement',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
