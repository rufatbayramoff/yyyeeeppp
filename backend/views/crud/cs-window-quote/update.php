<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowQuote */

$this->title = 'Update Cs Window Quote: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Quotes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cs-window-quote-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
