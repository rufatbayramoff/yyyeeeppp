<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContentFilterCheckedTopics */

$this->title = 'Create Content Filter Checked Topics';
$this->params['breadcrumbs'][] = ['label' => 'Content Filter Checked Topics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-filter-checked-topics-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
