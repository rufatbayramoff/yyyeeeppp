<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContentFilterCheckedTopics */

$this->title = 'Update Content Filter Checked Topics: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Content Filter Checked Topics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="content-filter-checked-topics-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
