<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ContentFilterCheckedTopicsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Content Filter Checked Topics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-filter-checked-topics-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Content Filter Checked Topics', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'topic_id',
            'isBanned',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
