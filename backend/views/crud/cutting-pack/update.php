<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingPack */

$this->title = 'Update Cutting Pack: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Packs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-pack-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
