<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingPackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Packs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Pack', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'title',
            'user_session_id',
            'user_id',
            'created_at',
            // 'updated_at',
            // 'is_active',
            // 'source',
            // 'source_details',
            // 'is_same_material_for_all_parts',
            // 'material_id',
            // 'thickness',
            // 'color_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
