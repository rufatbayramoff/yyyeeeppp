<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CuttingPack */

$this->title = 'Create Cutting Pack';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Packs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
