<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingPack */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Packs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uuid',
            'title',
            'user_session_id',
            'user_id',
            'created_at',
            'updated_at',
            'is_active',
            'source',
            'source_details',
            'is_same_material_for_all_parts',
            'material_id',
            'thickness',
            'color_id',
        ],
    ]) ?>

</div>
