<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderPositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Positions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-position-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order Position', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'order_id',
            'title',
            'file_ids',
            // 'created_at',
            // 'updated_at',
            // 'status',
            // 'amount',
            // 'currency_iso',
            // 'fee',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
