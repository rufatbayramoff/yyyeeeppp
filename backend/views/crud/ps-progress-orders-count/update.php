<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsProgressOrdersCount */

$this->title = 'Update Ps Progress Orders Count: ' . $model->ps_id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Progress Orders Counts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ps_id, 'url' => ['view', 'id' => $model->ps_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-progress-orders-count-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
