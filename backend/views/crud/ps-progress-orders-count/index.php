<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsProgressOrdersCountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Progress Orders Counts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-progress-orders-count-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ps Progress Orders Count', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ps_id',
            'progress_orders_count',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
