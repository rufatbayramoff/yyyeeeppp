<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PsProgressOrdersCount */

$this->title = $model->ps_id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Progress Orders Counts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-progress-orders-count-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ps_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ps_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ps_id',
            'progress_orders_count',
        ],
    ]) ?>

</div>
