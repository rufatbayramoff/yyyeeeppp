<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsProgressOrdersCount */

$this->title = 'Create Ps Progress Orders Count';
$this->params['breadcrumbs'][] = ['label' => 'Ps Progress Orders Counts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-progress-orders-count-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
