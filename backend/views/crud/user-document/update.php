<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserDocument */

$this->title = 'Update User Document: ' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'User Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_id, 'url' => ['view', 'user_id' => $model->user_id, 'file_id' => $model->file_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-document-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
