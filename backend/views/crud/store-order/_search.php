<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\StoreOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'ship_address_id') ?>

    <?= $form->field($model, 'bill_address_id') ?>

    <?php // echo $form->field($model, 'price_total') ?>

    <?php // echo $form->field($model, 'price_currency') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'billed_at') ?>

    <?php // echo $form->field($model, 'delivery_type_id') ?>

    <?php // echo $form->field($model, 'payment_status') ?>

    <?php // echo $form->field($model, 'order_state') ?>

    <?php // echo $form->field($model, 'payment_id') ?>

    <?php // echo $form->field($model, 'messages') ?>

    <?php // echo $form->field($model, 'can_change_ps') ?>

    <?php // echo $form->field($model, 'current_attemp_id') ?>

    <?php // echo $form->field($model, 'previous_attemp_id') ?>

    <?php // echo $form->field($model, 'confirm_hash') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
