<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'created_at',
            'ship_address_id',
            'bill_address_id',
            'price_total',
            'price_currency',
            'updated_at',
            'billed_at',
            'delivery_type_id',
            'payment_status',
            'order_state',
            'payment_id',
            'messages:ntext',
            'can_change_ps',
            'current_attemp_id',
            'previous_attemp_id',
            'confirm_hash',
        ],
    ]) ?>

</div>
