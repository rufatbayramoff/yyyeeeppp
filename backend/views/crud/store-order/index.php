<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'created_at',
            'ship_address_id',
            'bill_address_id',
            // 'price_total',
            // 'price_currency',
            // 'updated_at',
            // 'billed_at',
            // 'delivery_type_id',
            // 'payment_status',
            // 'order_state',
            // 'payment_id',
            // 'messages:ntext',
            // 'can_change_ps',
            // 'current_attemp_id',
            // 'previous_attemp_id',
            // 'confirm_hash',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
