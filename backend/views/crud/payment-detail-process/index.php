<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentDetailProcessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Detail Processes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-detail-process-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Detail Process', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'processor',
            'created_at',
            'status',
            'amount',
            // 'currency',
            // 'transaction_id',
            // 'payment_detail_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
