<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingMachine */

$this->title = 'Update Cutting Machine: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Machines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-machine-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
