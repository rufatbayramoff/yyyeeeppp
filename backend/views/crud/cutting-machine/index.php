<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingMachineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Machines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-machine-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Machine', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'max_with',
            'max_length',
            'min_order_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
