<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingMachine */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cutting-machine-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'max_with')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'max_length')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'min_order_price')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
