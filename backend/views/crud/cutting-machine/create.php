<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CuttingMachine */

$this->title = 'Create Cutting Machine';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Machines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-machine-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
