<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsMachineHistory */

$this->title = 'Update Ps Machine History: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Machine Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-machine-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
