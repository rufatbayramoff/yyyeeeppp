<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsMachineHistory */

$this->title = 'Create Ps Machine History';
$this->params['breadcrumbs'][] = ['label' => 'Ps Machine Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-machine-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
