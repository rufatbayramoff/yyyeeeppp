<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AccessGroup */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Access Group',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Access Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="access-group-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
