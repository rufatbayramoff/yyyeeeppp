<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dPartProperties */

$this->title = 'Update Model3d Part Properties: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Model3d Part Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="model3d-part-properties-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
