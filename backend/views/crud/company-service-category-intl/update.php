<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceCategoryIntl */

$this->title = 'Update Company Service Category Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company Service Category Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-service-category-intl-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
