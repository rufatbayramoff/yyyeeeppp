<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceCategoryIntl */

$this->title = 'Create Company Service Category Intl';
$this->params['breadcrumbs'][] = ['label' => 'Company Service Category Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-category-intl-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
