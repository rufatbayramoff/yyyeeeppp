<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingMachineWorkpieceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Machine Workpieces';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-machine-workpiece-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Machine Workpiece', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'cutting_machine_id',
            'cuting_workpiece_material_id',
            'cutting_price',
            'engraving_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
