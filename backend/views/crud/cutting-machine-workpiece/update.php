<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingMachineWorkpiece */

$this->title = 'Update Cutting Machine Workpiece: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Machine Workpieces', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-machine-workpiece-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
