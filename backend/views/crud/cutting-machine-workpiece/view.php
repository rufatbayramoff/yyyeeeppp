<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingMachineWorkpiece */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Machine Workpieces', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-machine-workpiece-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cutting_machine_id',
            'cuting_workpiece_material_id',
            'cutting_price',
            'engraving_price',
        ],
    ]) ?>

</div>
