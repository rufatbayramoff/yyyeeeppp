<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CuttingMachineWorkpieceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cutting-machine-workpiece-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'cutting_machine_id') ?>

    <?= $form->field($model, 'cuting_workpiece_material_id') ?>

    <?= $form->field($model, 'cutting_price') ?>

    <?= $form->field($model, 'engraving_price') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
