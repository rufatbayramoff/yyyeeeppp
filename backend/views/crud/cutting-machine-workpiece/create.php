<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CuttingMachineWorkpiece */

$this->title = 'Create Cutting Machine Workpiece';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Machine Workpieces', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-machine-workpiece-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
