<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsPrinterMaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Printer Materials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-material-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ps Printer Material', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'printer_id',
            'title',
            'created_at',
            'updated_at',
            // 'material_id',
            // 'is_active:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
