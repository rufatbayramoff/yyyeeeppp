<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterMaterial */

$this->title = 'Create Ps Printer Material';
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-material-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
