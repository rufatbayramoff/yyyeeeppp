<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MsgFile */

$this->title = 'Create Msg File';
$this->params['breadcrumbs'][] = ['label' => 'Msg Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
