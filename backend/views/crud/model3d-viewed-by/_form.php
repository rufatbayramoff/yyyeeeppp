<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dViewedState */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model3d-viewed-by-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'model3d_id')->textInput() ?>

    <?= $form->field($model, 'user_session_id')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
