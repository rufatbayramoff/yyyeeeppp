<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dViewedState */

$this->title = 'Update Model3d Viewed By: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Model3d Viewed Bies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="model3d-viewed-by-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
