<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MsgFolderIntl */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Msg Folder Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-folder-intl-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'model_id',
            'lang_iso',
            'title',
        ],
    ]) ?>

</div>
