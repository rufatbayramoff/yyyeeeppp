<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserInformer */

$this->title = 'Update User Informer: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Informers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-informer-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
