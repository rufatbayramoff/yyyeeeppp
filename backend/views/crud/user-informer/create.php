<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserInformer */

$this->title = 'Create User Informer';
$this->params['breadcrumbs'][] = ['label' => 'User Informers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-informer-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
