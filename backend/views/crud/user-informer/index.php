<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserInformerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Informers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-informer-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Informer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'key',
            'user_id',
            'created_at',
            'count',
            // 'config',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
