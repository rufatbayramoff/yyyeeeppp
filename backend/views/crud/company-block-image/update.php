<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlockImage */

$this->title = 'Update Company Block Image: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Block Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-block-image-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
