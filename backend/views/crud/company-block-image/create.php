<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlockImage */

$this->title = 'Create Company Block Image';
$this->params['breadcrumbs'][] = ['label' => 'Company Block Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-block-image-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
