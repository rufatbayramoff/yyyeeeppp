<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InformerCustomerOrder */

$this->title = 'Create Informer Customer Order';
$this->params['breadcrumbs'][] = ['label' => 'Informer Customer Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="informer-customer-order-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
