<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InformerCustomerOrder */

$this->title = 'Update Informer Customer Order: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Informer Customer Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="informer-customer-order-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
