<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'title',
            'description:ntext',
            'cover_image_file_uuid',
            'category_id',
            // 'product_status',
            // 'updated_at',
            // 'moderated_at',
            // 'published_at',
            // 'is_active',
            // 'user_id',
            // 'single_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
