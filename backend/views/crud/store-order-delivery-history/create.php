<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderDeliveryHistory */

$this->title = 'Create Store Order Delivery History';
$this->params['breadcrumbs'][] = ['label' => 'Store Order Delivery Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-delivery-history-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
