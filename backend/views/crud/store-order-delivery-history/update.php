<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderDeliveryHistory */

$this->title = 'Update Store Order Delivery History: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Delivery Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-order-delivery-history-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
