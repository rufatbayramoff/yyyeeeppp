<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\InformerCustomerPreorderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Informer Customer Preorders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="informer-customer-preorder-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Informer Customer Preorder', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'key',
            'user_id',
            'created_at',
            'count',
            // 'part_key',
            // 'part_info',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
