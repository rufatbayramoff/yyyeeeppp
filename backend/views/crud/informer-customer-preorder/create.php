<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InformerCustomerPreorder */

$this->title = 'Create Informer Customer Preorder';
$this->params['breadcrumbs'][] = ['label' => 'Informer Customer Preorders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="informer-customer-preorder-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
