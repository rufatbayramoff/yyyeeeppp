<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CsWindowFurniture */

$this->title = 'Create Cs Window Furniture';
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Furnitures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-furniture-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
