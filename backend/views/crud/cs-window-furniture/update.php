<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowFurniture */

$this->title = 'Update Cs Window Furniture: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Furnitures', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cs-window-furniture-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
