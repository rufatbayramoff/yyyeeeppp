<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CsWindowFurnitureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cs Window Furnitures';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-furniture-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cs Window Furniture', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'cs_window_uid',
            'price',
            'price_swivel',
            // 'price_folding',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
