<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyVerify */

$this->title = 'Update Company Verify: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Verifies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-verify-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
