<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyVerify */

$this->title = 'Create Company Verify';
$this->params['breadcrumbs'][] = ['label' => 'Company Verifies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-verify-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
