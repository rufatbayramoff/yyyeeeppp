<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialGeoCity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wiki-material-geo-city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wiki_material_id')->textInput() ?>

    <?= $form->field($model, 'geo_city_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
