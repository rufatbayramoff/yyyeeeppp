<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AffiliateResourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Affiliate Resources';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-resource-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Affiliate Resource', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'affiliate_id',
            'url_referrer:url',
            'params',
            'status',
            // 'award_rule_id',
            // 'priority',
            // 'created_at',
            // 'api_external_system_id',
            // 'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
