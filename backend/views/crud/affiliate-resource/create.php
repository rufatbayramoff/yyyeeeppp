<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AffiliateResource */

$this->title = 'Create Affiliate Resource';
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-resource-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
