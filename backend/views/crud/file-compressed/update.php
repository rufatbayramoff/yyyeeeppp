<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FileCompressed */

$this->title = 'Update File Compressed: ' . $model->file_id;
$this->params['breadcrumbs'][] = ['label' => 'File Compresseds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->file_id, 'url' => ['view', 'id' => $model->file_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="file-compressed-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
