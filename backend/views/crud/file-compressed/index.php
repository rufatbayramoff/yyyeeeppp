<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FileCompressedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'File Compresseds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-compressed-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create File Compressed', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'file_id',
            'date',
            'unpacked',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
