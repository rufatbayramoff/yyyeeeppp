<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FileCompressed */

$this->title = 'Create File Compressed';
$this->params['breadcrumbs'][] = ['label' => 'File Compresseds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-compressed-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
