<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserTaxInfo */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'User Tax Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-tax-info-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'created_at',
            'updated_at',
            'is_usa',
            'address_id',
            'identification_type',
            'identification',
            'classification',
            'name',
            'business_name',
            'mailing_address_id',
            'status',
            'place_country',
            'signature',
            'sign_date',
            'sign_type',
            'encrypted',
            'email_notify:email',
            'rsa_tax_rate',
        ],
    ]) ?>

</div>
