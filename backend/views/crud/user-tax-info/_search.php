<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\UserTaxInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-tax-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'updated_at') ?>

    <?= $form->field($model, 'is_usa') ?>

    <?php // echo $form->field($model, 'address_id') ?>

    <?php // echo $form->field($model, 'identification_type') ?>

    <?php // echo $form->field($model, 'identification') ?>

    <?php // echo $form->field($model, 'classification') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'business_name') ?>

    <?php // echo $form->field($model, 'mailing_address_id') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'place_country') ?>

    <?php // echo $form->field($model, 'signature') ?>

    <?php // echo $form->field($model, 'sign_date') ?>

    <?php // echo $form->field($model, 'sign_type') ?>

    <?php // echo $form->field($model, 'encrypted') ?>

    <?php // echo $form->field($model, 'email_notify') ?>

    <?php // echo $form->field($model, 'rsa_tax_rate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
