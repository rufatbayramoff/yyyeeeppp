<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserTaxInfo */

$this->title = 'Update User Tax Info: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'User Tax Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-tax-info-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
