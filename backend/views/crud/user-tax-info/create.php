<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserTaxInfo */

$this->title = 'Create User Tax Info';
$this->params['breadcrumbs'][] = ['label' => 'User Tax Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-tax-info-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
