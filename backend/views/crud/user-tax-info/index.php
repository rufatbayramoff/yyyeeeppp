<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserTaxInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Tax Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-tax-info-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Tax Info', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'created_at',
            'updated_at',
            'is_usa',
            // 'address_id',
            // 'identification_type',
            // 'identification',
            // 'classification',
            // 'name',
            // 'business_name',
            // 'mailing_address_id',
            // 'status',
            // 'place_country',
            // 'signature',
            // 'sign_date',
            // 'sign_type',
            // 'encrypted',
            // 'email_notify:email',
            // 'rsa_tax_rate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
