<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GeoRegion */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Geo Region',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Geo Regions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="geo-region-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
