<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GeoRegion */

$this->title = Yii::t('app', 'Create Geo Region');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Geo Regions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-region-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
