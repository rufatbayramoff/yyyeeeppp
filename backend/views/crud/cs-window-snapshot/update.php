<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowSnapshot */

$this->title = 'Update Cs Window Snapshot: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Snapshots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cs-window-snapshot-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
