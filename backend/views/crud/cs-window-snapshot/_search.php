<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CsWindowSnapshotSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cs-window-snapshot-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'uid') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'cs_window_uid') ?>

    <?= $form->field($model, 'furniture') ?>

    <?php // echo $form->field($model, 'glass') ?>

    <?php // echo $form->field($model, 'profile') ?>

    <?php // echo $form->field($model, 'service') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
