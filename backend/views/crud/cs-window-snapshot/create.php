<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CsWindowSnapshot */

$this->title = 'Create Cs Window Snapshot';
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Snapshots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-snapshot-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
