<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CsWindowSnapshotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cs Window Snapshots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-snapshot-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cs Window Snapshot', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'uid',
            'created_at',
            'cs_window_uid',
            'furniture',
            // 'glass',
            // 'profile',
            // 'service',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
