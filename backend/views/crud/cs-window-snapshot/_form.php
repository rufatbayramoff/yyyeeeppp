<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowSnapshot */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cs-window-snapshot-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'cs_window_uid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'furniture')->textInput() ?>

    <?= $form->field($model, 'glass')->textInput() ?>

    <?= $form->field($model, 'profile')->textInput() ?>

    <?= $form->field($model, 'service')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
