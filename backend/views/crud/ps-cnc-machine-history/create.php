<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsCncMachineHistory */

$this->title = 'Create Ps Cnc Machine History';
$this->params['breadcrumbs'][] = ['label' => 'Ps Cnc Machine Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-cnc-machine-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
