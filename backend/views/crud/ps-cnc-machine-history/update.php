<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsCncMachineHistory */

$this->title = 'Update Ps Cnc Machine History: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Cnc Machine Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-cnc-machine-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
