<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsCncMachineHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Cnc Machine Histories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-cnc-machine-history-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ps Cnc Machine History', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ps_cnc_machine_id',
            'created_at',
            'user_id',
            'action_id',
            // 'source',
            // 'result',
            // 'comment:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
