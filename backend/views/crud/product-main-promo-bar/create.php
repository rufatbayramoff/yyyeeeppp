<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductMainPromoBar */

$this->title = 'Create Product Main Promo Bar';
$this->params['breadcrumbs'][] = ['label' => 'Product Main Promo Bars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-main-promo-bar-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
