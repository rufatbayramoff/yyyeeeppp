<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductMainPromoBar */

$this->title = 'Update Product Main Promo Bar: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Main Promo Bars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-main-promo-bar-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
