<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\FileAdminSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-admin-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'uuid') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'path') ?>

    <?= $form->field($model, 'stored_name') ?>

    <?= $form->field($model, 'extension') ?>

    <?php // echo $form->field($model, 'size') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'server') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'md5sum') ?>

    <?php // echo $form->field($model, 'last_access_at') ?>

    <?php // echo $form->field($model, 'ownerClass') ?>

    <?php // echo $form->field($model, 'ownerField') ?>

    <?php // echo $form->field($model, 'is_public') ?>

    <?php // echo $form->field($model, 'path_version') ?>

    <?php // echo $form->field($model, 'expire') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
