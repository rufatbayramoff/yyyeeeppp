<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FileAdmin */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'File Admins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-admin-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uuid',
            'name',
            'path',
            'stored_name',
            'extension',
            'size',
            'created_at',
            'updated_at',
            'deleted_at',
            'user_id',
            'server',
            'status',
            'md5sum',
            'last_access_at',
            'ownerClass',
            'ownerField',
            'is_public',
            'path_version',
            'expire',
        ],
    ]) ?>

</div>
