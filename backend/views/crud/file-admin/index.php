<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FileAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'File Admins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-admin-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create File Admin', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'name',
            'path',
            'stored_name',
            'extension',
            // 'size',
            // 'created_at',
            // 'updated_at',
            // 'deleted_at',
            // 'user_id',
            // 'server',
            // 'status',
            // 'md5sum',
            // 'last_access_at',
            // 'ownerClass',
            // 'ownerField',
            // 'is_public',
            // 'path_version',
            // 'expire',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
