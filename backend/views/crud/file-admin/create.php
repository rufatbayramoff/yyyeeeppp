<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FileAdmin */

$this->title = 'Create File Admin';
$this->params['breadcrumbs'][] = ['label' => 'File Admins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-admin-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
