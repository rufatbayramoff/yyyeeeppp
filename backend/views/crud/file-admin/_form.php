<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FileAdmin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-admin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stored_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'extension')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'size')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'deleted_at')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'server')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'new' => 'New', 'active' => 'Active', 'inactive' => 'Inactive', 'danger' => 'Danger', 'deleted' => 'Deleted', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'md5sum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_access_at')->textInput() ?>

    <?= $form->field($model, 'ownerClass')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ownerField')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_public')->textInput() ?>

    <?= $form->field($model, 'path_version')->dropDownList([ 'v1' => 'V1', 'v2' => 'V2', 'v3' => 'V3', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'expire')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
