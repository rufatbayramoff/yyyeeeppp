<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FileAdmin */

$this->title = 'Update File Admin: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'File Admins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="file-admin-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
