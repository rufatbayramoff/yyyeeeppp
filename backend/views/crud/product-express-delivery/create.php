<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductExpressDelivery */

$this->title = 'Create Product Express Delivery';
$this->params['breadcrumbs'][] = ['label' => 'Product Express Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-express-delivery-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
