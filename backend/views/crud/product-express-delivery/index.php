<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductExpressDeliverySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Express Deliveries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-express-delivery-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product Express Delivery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'product_uuid',
            'country_id',
            'first_item',
            'following_item',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
