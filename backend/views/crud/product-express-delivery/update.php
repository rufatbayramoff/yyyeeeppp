<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductExpressDelivery */

$this->title = 'Update Product Express Delivery: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Product Express Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-express-delivery-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
