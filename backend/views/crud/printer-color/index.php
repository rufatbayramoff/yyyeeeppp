<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterColorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Colors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-color-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Printer Color', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'created_at',
            'updated_at',
            'is_active',
            // 'rgb',
            // 'cmyk',
            // 'image',
            // 'render_color',
            // 'ambient',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
