<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PrinterColorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printer-color-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'updated_at') ?>

    <?= $form->field($model, 'is_active') ?>

    <?php // echo $form->field($model, 'rgb') ?>

    <?php // echo $form->field($model, 'cmyk') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'render_color') ?>

    <?php // echo $form->field($model, 'ambient') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
