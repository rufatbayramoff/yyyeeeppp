<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dReplicaPart */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model3d-replica-part-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'model3d_replica_id')->textInput() ?>

    <?= $form->field($model, 'format')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'antivirus_checked_at')->textInput() ?>

    <?= $form->field($model, 'moderator_status')->dropDownList([ 'new' => 'New', 'checking' => 'Checking', 'ok' => 'Ok', 'banned' => 'Banned', 'review' => 'Review', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'moderated_at')->textInput() ?>

    <?= $form->field($model, 'user_status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', 'published' => 'Published', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'file_id')->textInput() ?>

    <?= $form->field($model, 'file_src_id')->textInput() ?>

    <?= $form->field($model, 'rotated_x')->textInput() ?>

    <?= $form->field($model, 'rotated_y')->textInput() ?>

    <?= $form->field($model, 'rotated_z')->textInput() ?>

    <?= $form->field($model, 'scaled')->textInput() ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <?= $form->field($model, 'model3d_part_properties_id')->textInput() ?>

    <?= $form->field($model, 'model3d_texture_id')->textInput() ?>

    <?= $form->field($model, 'original_model3d_part_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
