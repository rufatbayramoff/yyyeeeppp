<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Model3dReplicaPart */

$this->title = 'Update Model3d Replica Part: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Model3d Replica Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="model3d-replica-part-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
