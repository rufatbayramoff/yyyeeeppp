<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Model3dReplicaPart */

$this->title = 'Create Model3d Replica Part';
$this->params['breadcrumbs'][] = ['label' => 'Model3d Replica Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-replica-part-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
