<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NotifyPopup */

$this->title = 'Create Notify Popup';
$this->params['breadcrumbs'][] = ['label' => 'Notify Popups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notify-popup-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
