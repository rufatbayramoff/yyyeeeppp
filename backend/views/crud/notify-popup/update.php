<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NotifyPopup */

$this->title = 'Update Notify Popup: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Notify Popups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->uid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="notify-popup-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
