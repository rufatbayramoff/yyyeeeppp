<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NotifyPopup */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Notify Popups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notify-popup-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uid',
            'created_at',
            'user_id',
            'title',
            'text',
            'url:url',
            'expire_at',
            'code',
            'params',
        ],
    ]) ?>

</div>
