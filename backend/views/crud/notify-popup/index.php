<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\NotifyPopupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notify Popups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notify-popup-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Notify Popup', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uid',
            'created_at',
            'user_id',
            'title',
            'text',
            // 'url:url',
            // 'expire_at',
            // 'code',
            // 'params',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
