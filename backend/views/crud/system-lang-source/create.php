<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SystemLangSource */

$this->title = Yii::t('app', 'Create System Lang Source');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Lang Sources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-source-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
