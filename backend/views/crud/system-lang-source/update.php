<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SystemLangSource */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'System Lang Source',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Lang Sources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="system-lang-source-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
