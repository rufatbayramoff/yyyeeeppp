<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpVote */

$this->title = 'Update Site Help Vote: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Site Help Votes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-help-vote-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
