<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpVote */

$this->title = 'Create Site Help Vote';
$this->params['breadcrumbs'][] = ['label' => 'Site Help Votes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-help-vote-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
