<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SiteHelpVote */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-help-vote-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'site_help_id')->textInput() ?>

    <?= $form->field($model, 'vote_hash')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vote_answer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_added')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
