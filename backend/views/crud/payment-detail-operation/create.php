<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentDetailOperation */

$this->title = 'Create Payment Detail Operation';
$this->params['breadcrumbs'][] = ['label' => 'Payment Detail Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-detail-operation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
