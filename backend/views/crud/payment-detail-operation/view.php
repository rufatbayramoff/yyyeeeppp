<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentDetailOperation */

$this->title = $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Payment Detail Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-detail-operation-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uuid',
            'payment_id',
            'created_at',
        ],
    ]) ?>

</div>
