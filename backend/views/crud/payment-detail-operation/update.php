<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentDetailOperation */

$this->title = 'Update Payment Detail Operation: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Payment Detail Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-detail-operation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
