<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMachineProperty */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wiki-machine-property-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wiki_machine_id')->textInput() ?>

    <?= $form->field($model, 'wiki_machine_available_property_id')->textInput() ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
