<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductCategoryIntl */

$this->title = 'Create Product Category Intl';
$this->params['breadcrumbs'][] = ['label' => 'Product Category Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-category-intl-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
