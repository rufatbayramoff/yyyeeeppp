<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductCategoryIntl */

$this->title = 'Update Product Category Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Product Category Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-category-intl-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
