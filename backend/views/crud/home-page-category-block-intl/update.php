<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryBlockIntl */

$this->title = 'Update Home Page Category Block Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Category Block Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-page-category-block-intl-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
