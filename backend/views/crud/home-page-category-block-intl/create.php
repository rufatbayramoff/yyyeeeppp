<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryBlockIntl */

$this->title = 'Create Home Page Category Block Intl';
$this->params['breadcrumbs'][] = ['label' => 'Home Page Category Block Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-category-block-intl-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
