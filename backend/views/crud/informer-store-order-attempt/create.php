<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InformerStoreOrderAttempt */

$this->title = 'Create Informer Store Order Attempt';
$this->params['breadcrumbs'][] = ['label' => 'Informer Store Order Attempts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="informer-store-order-attempt-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
