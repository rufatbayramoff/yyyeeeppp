<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InformerStoreOrderAttempt */

$this->title = 'Update Informer Store Order Attempt: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Informer Store Order Attempts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="informer-store-order-attempt-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
