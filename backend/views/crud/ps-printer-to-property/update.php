<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterToProperty */

$this->title = 'Update Ps Printer To Property: ' . ' ' . $model->printer_id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer To Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->printer_id, 'url' => ['view', 'printer_id' => $model->printer_id, 'property_id' => $model->property_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-printer-to-property-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
