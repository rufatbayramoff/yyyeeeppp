<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSnapshot */

$this->title = 'Update Product Snapshot: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Product Snapshots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-snapshot-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
