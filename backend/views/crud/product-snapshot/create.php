<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductSnapshot */

$this->title = 'Create Product Snapshot';
$this->params['breadcrumbs'][] = ['label' => 'Product Snapshots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-snapshot-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
