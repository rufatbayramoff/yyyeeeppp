<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceTag */

$this->title = 'Create Company Service Tag';
$this->params['breadcrumbs'][] = ['label' => 'Company Service Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-tag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
