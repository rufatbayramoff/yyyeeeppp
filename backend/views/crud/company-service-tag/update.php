<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceTag */

$this->title = 'Update Company Service Tag: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Service Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-service-tag-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
