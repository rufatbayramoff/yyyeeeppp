<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ConnectedAppSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promo Apps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-app-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Promo App', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            'title',
            'logo_file_id',
            'is_beta',
            // 'url:url',
            // 'short_descr:ntext',
            // 'info:ntext',
            // 'instructions:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
