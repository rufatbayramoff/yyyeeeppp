<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PromoApp */

$this->title = 'Update Promo App: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Promo Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promo-app-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
