<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PromoApp */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Promo Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-app-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'title',
            'logo_file_id',
            'is_beta',
            'url:url',
            'short_descr:ntext',
            'info:ntext',
            'instructions:ntext',
        ],
    ]) ?>

</div>
