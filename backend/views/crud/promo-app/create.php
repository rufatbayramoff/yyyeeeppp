<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PromoApp */

$this->title = 'Create Promo App';
$this->params['breadcrumbs'][] = ['label' => 'Promo Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-app-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
