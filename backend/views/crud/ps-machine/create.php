<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyService */

$this->title = 'Create Ps Machine';
$this->params['breadcrumbs'][] = ['label' => 'Ps Machines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-machine-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
