<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-machine-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ps_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'printer' => 'Printer', 'cnc' => 'Cnc', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'ps_printer_id')->textInput() ?>

    <?= $form->field($model, 'ps_cnc_machine_id')->textInput() ?>

    <?= $form->field($model, 'location_id')->textInput() ?>

    <?= $form->field($model, 'is_deleted')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'is_location_change')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
