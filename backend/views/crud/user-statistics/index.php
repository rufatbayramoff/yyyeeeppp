<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserStatisticsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Statistics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-statistics-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Statistics', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'downloads',
            'prints',
            'favorites',
            // 'views',
            // 'updated_at',
            // 'lastonline_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
