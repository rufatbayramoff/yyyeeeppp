<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserPaypalHistory */

$this->title = 'Create User Paypal History';
$this->params['breadcrumbs'][] = ['label' => 'User Paypal Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-paypal-history-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
