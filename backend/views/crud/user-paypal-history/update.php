<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserPaypalHistory */

$this->title = 'Update User Paypal History: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Paypal Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-paypal-history-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
