<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TsCertificationClassSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ts Certification Classes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ts-certification-class-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ts Certification Class', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            'service_type',
            'price',
            // 'currency',
            // 'max_orders_count',
            // 'max_order_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
