<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TsCertificationClass */

$this->title = 'Create Ts Certification Class';
$this->params['breadcrumbs'][] = ['label' => 'Ts Certification Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ts-certification-class-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
