<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TsCertificationClass */

$this->title = 'Update Ts Certification Class: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ts Certification Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ts-certification-class-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
