<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CatalogCost */

$this->title = 'Update Catalog Cost: ' . $model->store_unit_id;
$this->params['breadcrumbs'][] = ['label' => 'Catalog Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->store_unit_id, 'url' => ['view', 'store_unit_id' => $model->store_unit_id, 'country_id' => $model->country_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="catalog-cost-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
