<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BizIndustryIntl */

$this->title = 'Update Biz Industry Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Biz Industry Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="biz-industry-intl-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
