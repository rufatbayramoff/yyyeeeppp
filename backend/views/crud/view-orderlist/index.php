<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ViewOrderlistSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'View Orderlists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-orderlist-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create View Orderlist', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'order_id',
            'preorder_id',
            'buyer_id',
            'billed_at',
            'designer_id',
            // 'created_at',
            // 'accepted_at',
            // 'ready_sent_at',
            // 'delivered_at',
            // 'shipped_at',
            // 'received_at',
            // 'to_country',
            // 'to_city',
            // 'comment:ntext',
            // 'printer_id',
            // 'printer_title',
            // 'ps_country',
            // 'delivery_type',
            // 'delivery_opts',
            // 'total_amount',
            // 'currency',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
