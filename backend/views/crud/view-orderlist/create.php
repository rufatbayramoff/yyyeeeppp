<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ViewOrderlist */

$this->title = 'Create View Orderlist';
$this->params['breadcrumbs'][] = ['label' => 'View Orderlists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-orderlist-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
