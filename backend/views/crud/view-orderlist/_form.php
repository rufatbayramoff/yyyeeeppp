<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ViewOrderlist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="view-orderlist-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'order_id')->textInput() ?>

    <?= $form->field($model, 'preorder_id')->textInput() ?>

    <?= $form->field($model, 'buyer_id')->textInput() ?>

    <?= $form->field($model, 'billed_at')->textInput() ?>

    <?= $form->field($model, 'designer_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'accepted_at')->textInput() ?>

    <?= $form->field($model, 'ready_sent_at')->textInput() ?>

    <?= $form->field($model, 'delivered_at')->textInput() ?>

    <?= $form->field($model, 'shipped_at')->textInput() ?>

    <?= $form->field($model, 'received_at')->textInput() ?>

    <?= $form->field($model, 'to_country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'printer_id')->textInput() ?>

    <?= $form->field($model, 'printer_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ps_country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'delivery_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'delivery_opts')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
