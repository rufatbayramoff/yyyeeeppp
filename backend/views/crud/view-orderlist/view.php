<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ViewOrderlist */

$this->title = $model->order_id;
$this->params['breadcrumbs'][] = ['label' => 'View Orderlists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-orderlist-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->order_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->order_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'order_id',
            'preorder_id',
            'buyer_id',
            'billed_at',
            'designer_id',
            'created_at',
            'accepted_at',
            'ready_sent_at',
            'delivered_at',
            'shipped_at',
            'received_at',
            'to_country',
            'to_city',
            'comment:ntext',
            'printer_id',
            'printer_title',
            'ps_country',
            'delivery_type',
            'delivery_opts',
            'total_amount',
            'currency',
        ],
    ]) ?>

</div>
