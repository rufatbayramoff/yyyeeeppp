<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ViewOrderlist */

$this->title = 'Update View Orderlist: ' . $model->order_id;
$this->params['breadcrumbs'][] = ['label' => 'View Orderlists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->order_id, 'url' => ['view', 'id' => $model->order_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="view-orderlist-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
