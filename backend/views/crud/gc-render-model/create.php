<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GcRenderModel */

$this->title = 'Create Gc Render Model';
$this->params['breadcrumbs'][] = ['label' => 'Gc Render Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gc-render-model-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
