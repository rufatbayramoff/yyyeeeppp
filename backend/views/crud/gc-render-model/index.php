<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\GcRenderModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gc Render Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gc-render-model-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Gc Render Model', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'webdavUrl:url',
            'path',
            'loaded_at',
            'processed_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
