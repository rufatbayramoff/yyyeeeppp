<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GcRenderModel */

$this->title = 'Update Gc Render Model: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gc Render Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gc-render-model-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
