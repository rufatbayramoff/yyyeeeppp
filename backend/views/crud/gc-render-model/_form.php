<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GcRenderModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gc-render-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'webdavUrl')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loaded_at')->textInput() ?>

    <?= $form->field($model, 'processed_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
