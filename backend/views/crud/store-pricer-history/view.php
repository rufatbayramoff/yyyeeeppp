<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StorePricerHistory */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Pricer Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-pricer-history-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pricer_id',
            'created_at',
            'updated_at',
            'old_price',
            'new_price',
            'user_id',
            'comment',
            'action_id',
        ],
    ]) ?>

</div>
