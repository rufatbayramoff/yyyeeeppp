<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StorePricerHistory */

$this->title = 'Update Store Pricer History: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Pricer Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-pricer-history-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
