<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductMainCategory */

$this->title = 'Update Product Main Category: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Main Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-main-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
