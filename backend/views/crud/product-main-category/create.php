<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductMainCategory */

$this->title = 'Create Product Main Category';
$this->params['breadcrumbs'][] = ['label' => 'Product Main Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-main-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
