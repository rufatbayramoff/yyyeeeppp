<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowHistory */

$this->title = 'Update Cs Window History: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cs-window-history-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
