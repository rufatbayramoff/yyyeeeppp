<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CsWindowHistory */

$this->title = 'Create Cs Window History';
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-history-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
