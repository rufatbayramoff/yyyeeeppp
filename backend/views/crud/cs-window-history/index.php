<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CsWindowHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cs Window Histories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-history-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cs Window History', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'cs_window_uid',
            'user_id',
            'created_at',
            'action_id',
            // 'comment:ntext',
            // 'data:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
