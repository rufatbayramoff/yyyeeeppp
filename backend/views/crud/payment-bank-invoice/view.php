<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentBankInvoice */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Bank Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-bank-invoice-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'invoice_id',
            'created_at',
            'date_due',
            'currency',
            'total_amount',
            'status',
            'user_id',
            'ship_to',
            'bill_to',
            'bank_details',
            'order_id',
            'payment_details',
            'comment:ntext',
            'transaction_id',
        ],
    ]) ?>

</div>
