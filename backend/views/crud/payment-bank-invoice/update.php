<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentBankInvoice */

$this->title = 'Update Payment Bank Invoice: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Bank Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-bank-invoice-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
