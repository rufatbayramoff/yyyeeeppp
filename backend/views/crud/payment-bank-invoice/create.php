<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentBankInvoice */

$this->title = 'Create Payment Bank Invoice';
$this->params['breadcrumbs'][] = ['label' => 'Payment Bank Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-bank-invoice-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
