<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentBankInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Bank Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-bank-invoice-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Bank Invoice', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'invoice_id',
            'created_at',
            'date_due',
            'currency',
            // 'total_amount',
            // 'status',
            // 'user_id',
            // 'ship_to',
            // 'bill_to',
            // 'bank_details',
            // 'order_id',
            // 'payment_details',
            // 'comment:ntext',
            // 'transaction_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
