<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StatisticsCache */

$this->title = 'Update Statistics Cache: ' . $model->type;
$this->params['breadcrumbs'][] = ['label' => 'Statistics Caches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->type, 'url' => ['view', 'id' => $model->type]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="statistics-cache-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
