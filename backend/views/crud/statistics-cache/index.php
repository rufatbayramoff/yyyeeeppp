<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StatisticsCacheSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Statistics Caches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="statistics-cache-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Statistics Cache', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'type',
            'cache_hit_count',
            'cache_miss_count',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
