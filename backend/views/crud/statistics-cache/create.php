<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StatisticsCache */

$this->title = 'Create Statistics Cache';
$this->params['breadcrumbs'][] = ['label' => 'Statistics Caches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="statistics-cache-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
