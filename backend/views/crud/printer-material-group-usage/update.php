<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialGroupUsage */

$this->title = 'Update Printer Material Group Usage: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Material Group Usages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-material-group-usage-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
