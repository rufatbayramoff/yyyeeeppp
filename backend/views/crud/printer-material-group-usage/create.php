<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialGroupUsage */

$this->title = 'Create Printer Material Group Usage';
$this->params['breadcrumbs'][] = ['label' => 'Printer Material Group Usages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-group-usage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
