<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserLoginLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Login Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-login-log-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Login Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'login_type',
            'result',
            'remote_addr',
            // 'http_user_agent',
            // 'created_at',
            // 'logout_at',
            // 'http_referer',
            // 'post_data:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
