<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserLoginLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-login-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'login_type')->dropDownList([ 'form' => 'Form', 'osn' => 'Osn', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'result')->dropDownList([ 'failed' => 'Failed', 'ok' => 'Ok', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'remote_addr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'http_user_agent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'logout_at')->textInput() ?>

    <?= $form->field($model, 'http_referer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'post_data')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
