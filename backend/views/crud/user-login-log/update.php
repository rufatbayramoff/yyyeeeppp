<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserLoginLog */

$this->title = 'Update User Login Log: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Login Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-login-log-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
