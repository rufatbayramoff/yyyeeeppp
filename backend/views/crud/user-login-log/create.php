<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserLoginLog */

$this->title = 'Create User Login Log';
$this->params['breadcrumbs'][] = ['label' => 'User Login Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-login-log-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
