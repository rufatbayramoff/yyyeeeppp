<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\UserLoginLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-login-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'login_type') ?>

    <?= $form->field($model, 'result') ?>

    <?= $form->field($model, 'remote_addr') ?>

    <?php // echo $form->field($model, 'http_user_agent') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'logout_at') ?>

    <?php // echo $form->field($model, 'http_referer') ?>

    <?php // echo $form->field($model, 'post_data') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
