<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyService */

$this->title = 'Update Company Service: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-service-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
