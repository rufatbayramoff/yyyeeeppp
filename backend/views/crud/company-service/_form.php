<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-service-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ps_id')->textInput() ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ps_printer_id')->textInput() ?>

    <?= $form->field($model, 'ps_cnc_machine_id')->textInput() ?>

    <?= $form->field($model, 'location_id')->textInput() ?>

    <?= $form->field($model, 'is_deleted')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'is_location_change')->textInput() ?>

    <?= $form->field($model, 'visibility')->dropDownList([ 'everywhere' => 'Everywhere', 'widgetOnly' => 'WidgetOnly', 'nowhere' => 'Nowhere', '' => '', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'moderator_status')->dropDownList([ 'new' => 'New', 'approved' => 'Approved', 'approvedReviewing' => 'ApprovedReviewing', 'rejected' => 'Rejected', 'rejectedReviewing' => 'RejectedReviewing', 'unavailableReviewing' => 'UnavailableReviewing', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'primary_image_id')->textInput() ?>

    <?= $form->field($model, 'single_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'supply_ability')->textInput() ?>

    <?= $form->field($model, 'supply_ability_time')->dropDownList([ 'day' => 'Day', 'week' => 'Week', 'month' => 'Month', 'year' => 'Year', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'unit_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'avg_production')->textInput() ?>

    <?= $form->field($model, 'avg_production_time')->dropDownList([ 'day' => 'Day', 'week' => 'Week', 'month' => 'Month', 'year' => 'Year', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'incoterms')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'custom_properties')->textInput() ?>

    <?= $form->field($model, 'videos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
