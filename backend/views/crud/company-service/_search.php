<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CompanyServiceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-service-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ps_id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'ps_printer_id') ?>

    <?= $form->field($model, 'ps_cnc_machine_id') ?>

    <?php // echo $form->field($model, 'location_id') ?>

    <?php // echo $form->field($model, 'is_deleted') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'is_location_change') ?>

    <?php // echo $form->field($model, 'visibility') ?>

    <?php // echo $form->field($model, 'moderator_status') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'category_id') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'primary_image_id') ?>

    <?php // echo $form->field($model, 'single_price') ?>

    <?php // echo $form->field($model, 'supply_ability') ?>

    <?php // echo $form->field($model, 'supply_ability_time') ?>

    <?php // echo $form->field($model, 'unit_type') ?>

    <?php // echo $form->field($model, 'avg_production') ?>

    <?php // echo $form->field($model, 'avg_production_time') ?>

    <?php // echo $form->field($model, 'incoterms') ?>

    <?php // echo $form->field($model, 'custom_properties') ?>

    <?php // echo $form->field($model, 'videos') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
