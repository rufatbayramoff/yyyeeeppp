<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CompanyServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Company Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Company Service', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ps_id',
            'type',
            'ps_printer_id',
            'ps_cnc_machine_id',
            // 'location_id',
            // 'is_deleted',
            // 'created_at',
            // 'updated_at',
            // 'is_location_change',
            // 'visibility',
            // 'moderator_status',
            // 'title',
            // 'category_id',
            // 'description:ntext',
            // 'primary_image_id',
            // 'single_price',
            // 'supply_ability',
            // 'supply_ability_time',
            // 'unit_type',
            // 'avg_production',
            // 'avg_production_time',
            // 'incoterms',
            // 'custom_properties',
            // 'videos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
