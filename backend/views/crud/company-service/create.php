<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyService */

$this->title = 'Create Company Service';
$this->params['breadcrumbs'][] = ['label' => 'Company Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
