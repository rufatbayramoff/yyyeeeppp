<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyService */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ps_id',
            'type',
            'ps_printer_id',
            'ps_cnc_machine_id',
            'location_id',
            'is_deleted',
            'created_at',
            'updated_at',
            'is_location_change',
            'visibility',
            'moderator_status',
            'title',
            'category_id',
            'description:ntext',
            'primary_image_id',
            'single_price',
            'supply_ability',
            'supply_ability_time',
            'unit_type',
            'avg_production',
            'avg_production_time',
            'incoterms',
            'custom_properties',
            'videos',
        ],
    ]) ?>

</div>
