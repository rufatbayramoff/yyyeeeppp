<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialGroupIntl */

$this->title = 'Create Printer Material Group Intl';
$this->params['breadcrumbs'][] = ['label' => 'Printer Material Group Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-group-intl-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
