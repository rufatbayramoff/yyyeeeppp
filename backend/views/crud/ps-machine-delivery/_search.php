<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PsMachineDeliverySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-machine-delivery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ps_machine_id') ?>

    <?= $form->field($model, 'delivery_type_id') ?>

    <?= $form->field($model, 'comment') ?>

    <?= $form->field($model, 'carrier') ?>

    <?php // echo $form->field($model, 'carrier_price') ?>

    <?php // echo $form->field($model, 'free_delivery') ?>

    <?php // echo $form->field($model, 'packing_price') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
