<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PsMachineDelivery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-machine-delivery-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ps_machine_id')->textInput() ?>

    <?= $form->field($model, 'delivery_type_id')->textInput() ?>

    <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carrier')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carrier_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'free_delivery')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'packing_price')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
