<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsMachineDeliverySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Machine Deliveries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-machine-delivery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ps Machine Delivery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ps_machine_id',
            'delivery_type_id',
            'comment',
            'carrier',
            // 'carrier_price',
            // 'free_delivery',
            // 'packing_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
