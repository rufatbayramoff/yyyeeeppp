<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsMachineDelivery */

$this->title = 'Create Ps Machine Delivery';
$this->params['breadcrumbs'][] = ['label' => 'Ps Machine Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-machine-delivery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
