<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialFile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wiki-material-file-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wiki_material_id')->textInput() ?>

    <?= $form->field($model, 'file_uuid')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
