<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PromocodeHistory */

$this->title = 'Create Promocode History';
$this->params['breadcrumbs'][] = ['label' => 'Promocode Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
