<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PromocodeHistory */

$this->title = 'Update Promocode History: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Promocode Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promocode-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
