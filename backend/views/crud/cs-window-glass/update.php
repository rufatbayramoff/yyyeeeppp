<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowGlass */

$this->title = 'Update Cs Window Glass: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Glasses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cs-window-glass-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
