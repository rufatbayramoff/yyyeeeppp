<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CsWindowGlassSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cs Window Glasses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-glass-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cs Window Glass', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'updated_at',
            'title',
            'cs_window_uid',
            'thickness',
            // 'chambers',
            // 'noise_reduction',
            // 'thermal_resistance',
            // 'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
