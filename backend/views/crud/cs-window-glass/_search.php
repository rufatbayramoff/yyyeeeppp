<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CsWindowGlassSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cs-window-glass-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'updated_at') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'cs_window_uid') ?>

    <?= $form->field($model, 'thickness') ?>

    <?php // echo $form->field($model, 'chambers') ?>

    <?php // echo $form->field($model, 'noise_reduction') ?>

    <?php // echo $form->field($model, 'thermal_resistance') ?>

    <?php // echo $form->field($model, 'price') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
