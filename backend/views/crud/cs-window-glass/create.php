<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CsWindowGlass */

$this->title = 'Create Cs Window Glass';
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Glasses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-glass-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
