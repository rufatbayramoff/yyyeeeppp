<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowGlass */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Glasses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-glass-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'updated_at',
            'title',
            'cs_window_uid',
            'thickness',
            'chambers',
            'noise_reduction',
            'thermal_resistance',
            'price',
        ],
    ]) ?>

</div>
