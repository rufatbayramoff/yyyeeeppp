<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dReplicaImgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3d Replica Imgs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-replica-img-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model3d Replica Img', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model3d_id',
            'size',
            'title',
            'created_at',
            // 'updated_at',
            // 'deleted_at',
            // 'moderated_at',
            // 'is_moderated:boolean',
            // 'basename',
            // 'extension',
            // 'user_id',
            // 'type',
            // 'review_id',
            // 'file_id',
            // 'original_model3d_img_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
