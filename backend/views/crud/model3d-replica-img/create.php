<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Model3dReplicaImg */

$this->title = 'Create Model3d Replica Img';
$this->params['breadcrumbs'][] = ['label' => 'Model3d Replica Imgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-replica-img-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
