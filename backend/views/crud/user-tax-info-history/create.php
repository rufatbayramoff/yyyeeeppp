<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserTaxInfoHistory */

$this->title = 'Create User Tax Info History';
$this->params['breadcrumbs'][] = ['label' => 'User Tax Info Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-tax-info-history-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
