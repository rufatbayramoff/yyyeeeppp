<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserTaxInfoHistory */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Tax Info Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-tax-info-history-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_tax_info_id',
            'user_id',
            'created_at',
            'action_id',
            'comment:ntext',
        ],
    ]) ?>

</div>
