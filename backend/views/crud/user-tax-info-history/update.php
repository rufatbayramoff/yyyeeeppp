<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserTaxInfoHistory */

$this->title = 'Update User Tax Info History: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Tax Info Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-tax-info-history-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
