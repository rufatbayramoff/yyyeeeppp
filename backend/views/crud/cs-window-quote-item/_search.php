<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CsWindowQuoteItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cs-window-quote-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'uid') ?>

    <?= $form->field($model, 'cs_window_quote_uid') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'quote_parameters') ?>

    <?php // echo $form->field($model, 'qty') ?>

    <?php // echo $form->field($model, 'cost') ?>

    <?php // echo $form->field($model, 'frame_id') ?>

    <?php // echo $form->field($model, 'glass_id') ?>

    <?php // echo $form->field($model, 'profile_id') ?>

    <?php // echo $form->field($model, 'furniture_id') ?>

    <?php // echo $form->field($model, 'windowsill') ?>

    <?php // echo $form->field($model, 'lamination') ?>

    <?php // echo $form->field($model, 'slopes') ?>

    <?php // echo $form->field($model, 'tinting') ?>

    <?php // echo $form->field($model, 'energy_saver') ?>

    <?php // echo $form->field($model, 'installation') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
