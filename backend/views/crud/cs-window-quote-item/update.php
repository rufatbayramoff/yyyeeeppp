<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowQuoteItem */

$this->title = 'Update Cs Window Quote Item: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Quote Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cs-window-quote-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
