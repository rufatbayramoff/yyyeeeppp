<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowQuoteItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cs-window-quote-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cs_window_quote_uid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quote_parameters')->textInput() ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <?= $form->field($model, 'cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'frame_id')->textInput() ?>

    <?= $form->field($model, 'glass_id')->textInput() ?>

    <?= $form->field($model, 'profile_id')->textInput() ?>

    <?= $form->field($model, 'furniture_id')->textInput() ?>

    <?= $form->field($model, 'windowsill')->textInput() ?>

    <?= $form->field($model, 'lamination')->textInput() ?>

    <?= $form->field($model, 'slopes')->textInput() ?>

    <?= $form->field($model, 'tinting')->textInput() ?>

    <?= $form->field($model, 'energy_saver')->textInput() ?>

    <?= $form->field($model, 'installation')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
