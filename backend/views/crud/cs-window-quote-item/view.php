<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindowQuoteItem */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Quote Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-quote-item-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'uid',
            'cs_window_quote_uid',
            'title',
            'quote_parameters',
            'qty',
            'cost',
            'frame_id',
            'glass_id',
            'profile_id',
            'furniture_id',
            'windowsill',
            'lamination',
            'slopes',
            'tinting',
            'energy_saver',
            'installation',
        ],
    ]) ?>

</div>
