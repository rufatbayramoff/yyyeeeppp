<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CsWindowQuoteItem */

$this->title = 'Create Cs Window Quote Item';
$this->params['breadcrumbs'][] = ['label' => 'Cs Window Quote Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-quote-item-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
