<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CsWindowQuoteItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cs Window Quote Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-quote-item-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cs Window Quote Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'uid',
            'cs_window_quote_uid',
            'title',
            'quote_parameters',
            // 'qty',
            // 'cost',
            // 'frame_id',
            // 'glass_id',
            // 'profile_id',
            // 'furniture_id',
            // 'windowsill',
            // 'lamination',
            // 'slopes',
            // 'tinting',
            // 'energy_saver',
            // 'installation',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
