<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserEmailChangeRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Email Change Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-email-change-request-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Email Change Request', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'email:email',
            'confirm_key',
            'status',
            // 'create_at',
            // 'confirm_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
