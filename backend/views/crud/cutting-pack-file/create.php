<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CuttingPackFile */

$this->title = 'Create Cutting Pack File';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Pack Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-file-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
