<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CuttingPackFileSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cutting-pack-file-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'uuid') ?>

    <?= $form->field($model, 'cutting_pack_uuid') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'file_uuid') ?>

    <?= $form->field($model, 'svg_file_uuid') ?>

    <?php // echo $form->field($model, 'qty') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <?php // echo $form->field($model, 'selections') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
