<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingPackFile */

$this->title = 'Update Cutting Pack File: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Pack Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-pack-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
