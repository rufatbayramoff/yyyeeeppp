<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingPackFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Pack Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-file-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Pack File', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'cutting_pack_uuid',
            'type',
            'file_uuid',
            'svg_file_uuid',
            // 'qty',
            // 'is_active',
            // 'selections',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
