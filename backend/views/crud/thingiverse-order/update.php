<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseOrder */

$this->title = 'Update Thingiverse Order: ' . $model->thingiverse_order_id;
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->thingiverse_order_id, 'url' => ['view', 'id' => $model->thingiverse_order_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="thingiverse-order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
