<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ThingiverseOrder */

$this->title = $model->thingiverse_order_id;
$this->params['breadcrumbs'][] = ['label' => 'Thingiverse Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thingiverse-order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->thingiverse_order_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->thingiverse_order_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'thingiverse_order_id',
            'order_id',
            'created_at',
            'updated_at',
            'is_shipped',
            'is_cancelled',
            'refund_note',
            'note',
            'status',
            'api_json',
        ],
    ]) ?>

</div>
