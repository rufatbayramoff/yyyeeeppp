<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CncMaterialSame */

$this->title = 'Create Cnc Material Same';
$this->params['breadcrumbs'][] = ['label' => 'Cnc Material Sames', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnc-material-same-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
