<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ApiRequest */

$this->title = 'Create Api Request';
$this->params['breadcrumbs'][] = ['label' => 'Api Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
