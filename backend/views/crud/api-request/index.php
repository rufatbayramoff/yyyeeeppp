<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ApiRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Api Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-request-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Api Request', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'finished_at',
            'status',
            'in',
            // 'out',
            // 'api_key',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
