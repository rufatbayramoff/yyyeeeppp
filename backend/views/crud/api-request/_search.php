<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\ApiRequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="api-request-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'finished_at') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'in') ?>

    <?php // echo $form->field($model, 'out') ?>

    <?php // echo $form->field($model, 'api_key') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
