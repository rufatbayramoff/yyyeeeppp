<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ApiRequest */

$this->title = 'Update Api Request: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Api Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="api-request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
