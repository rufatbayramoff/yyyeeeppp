<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Model3dPartCncParams */

$this->title = 'Create Model3d Part Cnc Params';
$this->params['breadcrumbs'][] = ['label' => 'Model3d Part Cnc Params', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-part-cnc-params-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
