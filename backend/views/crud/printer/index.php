<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Printer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'firm',
            'title',
            'description:ntext',
            'created_at',
            // 'updated_at',
            // 'is_active:boolean',
            // 'technology_id',
            // 'weight',
            // 'price',
            // 'website',
            // 'build_volume',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
