<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserAccess */

$this->title = 'Create User Access';
$this->params['breadcrumbs'][] = ['label' => 'User Accesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-access-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
