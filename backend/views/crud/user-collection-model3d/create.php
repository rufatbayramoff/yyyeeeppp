<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserCollectionModel3d */

$this->title = 'Create User Collection Model3d';
$this->params['breadcrumbs'][] = ['label' => 'User Collection Model3ds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-collection-model3d-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
