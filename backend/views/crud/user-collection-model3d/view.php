<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserCollectionModel3d */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Collection Model3ds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-collection-model3d-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'collection_id',
            'model3d_id',
            'created_at',
        ],
    ]) ?>

</div>
