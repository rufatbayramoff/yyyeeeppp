<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContentFilterBannedPhrase */

$this->title = 'Update Content Filter Banned Phrase: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Content Filter Banned Phrases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="content-filter-banned-phrase-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
