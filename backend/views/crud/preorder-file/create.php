<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PreorderFile */

$this->title = 'Create Preorder File';
$this->params['breadcrumbs'][] = ['label' => 'Preorder Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
