<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PreorderFile */

$this->title = $model->preorder_id;
$this->params['breadcrumbs'][] = ['label' => 'Preorder Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-file-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'preorder_id' => $model->preorder_id, 'file_id' => $model->file_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'preorder_id' => $model->preorder_id, 'file_id' => $model->file_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'preorder_id',
            'file_id',
        ],
    ]) ?>

</div>
