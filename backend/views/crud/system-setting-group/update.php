<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SystemSettingGroup */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'System Setting Group',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Setting Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="system-setting-group-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
