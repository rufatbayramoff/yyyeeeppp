<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SystemSettingGroup */

$this->title = Yii::t('app', 'Create System Setting Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Setting Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-setting-group-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
