<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsPrinterJob */

$this->title = 'Create Ps Printer Job';
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-job-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
