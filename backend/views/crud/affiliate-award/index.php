<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AffiliateAwardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Affiliate Awards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-award-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Affiliate Award', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'resource_id',
            'price',
            'price_currency',
            'type',
            // 'status',
            // 'created_at',
            // 'order_id',
            // 'award_rule_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
