<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Comment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model',
            'model_id',
            'user_id',
            'username',
            // 'email:email',
            // 'parent_id',
            // 'super_parent_id',
            // 'content:ntext',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'updated_by',
            // 'user_ip',
            // 'url:url',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
