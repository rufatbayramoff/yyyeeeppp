<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderReviewShare */

$this->title = 'Create Store Order Review Share';
$this->params['breadcrumbs'][] = ['label' => 'Store Order Review Shares', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-review-share-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
