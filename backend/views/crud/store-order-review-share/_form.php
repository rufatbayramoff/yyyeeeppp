<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderReviewShare */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-order-review-share-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'review_id')->textInput() ?>

    <?= $form->field($model, 'social_type')->dropDownList([ 'link' => 'Link', 'google' => 'Google', 'facebook' => 'Facebook', 'facebook_im' => 'Facebook im', 'reddit' => 'Reddit', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'photo_id')->textInput() ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'new' => 'New', 'approved' => 'Approved', 'rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
