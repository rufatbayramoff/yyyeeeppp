<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderReviewShareSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Review Shares';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-review-share-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order Review Share', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'review_id',
            'social_type',
            'photo_id',
            // 'text:ntext',
            // 'created_at',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
