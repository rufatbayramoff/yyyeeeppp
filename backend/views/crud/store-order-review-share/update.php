<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderReviewShare */

$this->title = 'Update Store Order Review Share: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Review Shares', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-order-review-share-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
