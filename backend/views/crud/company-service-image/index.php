<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CompanyServiceImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Company Service Images';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-image-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Company Service Image', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'company_service_id',
            'file_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
