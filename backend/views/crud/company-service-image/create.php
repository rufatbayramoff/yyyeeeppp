<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceImage */

$this->title = 'Create Company Service Image';
$this->params['breadcrumbs'][] = ['label' => 'Company Service Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-image-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
