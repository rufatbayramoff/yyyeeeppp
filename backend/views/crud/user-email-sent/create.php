<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserEmailSent */

$this->title = 'Create User Email Sent';
$this->params['breadcrumbs'][] = ['label' => 'User Email Sents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-email-sent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
