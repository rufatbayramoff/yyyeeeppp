<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserEmailSentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Email Sents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-email-sent-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Email Sent', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'email_code:email',
            'created_at',
            'hash',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
