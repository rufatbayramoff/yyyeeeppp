<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserEmailSent */

$this->title = 'Update User Email Sent: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Email Sents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-email-sent-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
