<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SeoPageAutofillTemplate */

$this->title = 'Create Seo Page Autofill Template';
$this->params['breadcrumbs'][] = ['label' => 'Seo Page Autofill Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-page-autofill-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
