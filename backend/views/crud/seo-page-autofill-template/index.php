<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SeoPageAutofillTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seo Page Autofill Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-page-autofill-template-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Seo Page Autofill Template', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',
            'template_name',
            'title',
            'meta_description',
            // 'meta_keywords',
            // 'header',
            // 'created_at',
            // 'lang_iso',
            // 'is_default:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
