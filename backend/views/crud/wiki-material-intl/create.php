<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialIntl */

$this->title = 'Create Wiki Material Intl';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Material Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-material-intl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
