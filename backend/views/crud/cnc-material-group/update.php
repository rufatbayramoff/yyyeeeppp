<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CncMaterialGroup */

$this->title = 'Update Cnc Material Group: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cnc Material Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cnc-material-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
