<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SystemReject */

$this->title = 'Create System Reject';
$this->params['breadcrumbs'][] = ['label' => 'System Rejects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-reject-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
