<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SystemReject */

$this->title = 'Update System Reject: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'System Rejects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="system-reject-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
