<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingPackPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cutting-pack-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cutting_pack_file_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'selections')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
