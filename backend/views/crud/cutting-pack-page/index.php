<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingPackPageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Pack Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-page-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Pack Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'cutting_pack_file_uuid',
            'file_uuid',
            'selections',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
