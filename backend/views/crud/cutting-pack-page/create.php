<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CuttingPackPage */

$this->title = 'Create Cutting Pack Page';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Pack Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-pack-page-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
