<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingPackPage */

$this->title = 'Update Cutting Pack Page: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Pack Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-pack-page-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
