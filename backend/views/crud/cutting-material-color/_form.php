<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingMaterialColor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cutting-material-color-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cutting_material_id')->textInput() ?>

    <?= $form->field($model, 'cutting_color_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
