<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingMaterialColorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Material Colors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-material-color-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Material Color', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'cutting_material_id',
            'cutting_color_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
