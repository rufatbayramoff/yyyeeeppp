<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CuttingMaterialColor */

$this->title = 'Create Cutting Material Color';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Material Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-material-color-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
