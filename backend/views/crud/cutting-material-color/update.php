<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingMaterialColor */

$this->title = 'Update Cutting Material Color: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Material Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-material-color-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
