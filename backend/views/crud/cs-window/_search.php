<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CsWindowSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cs-window-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'uid') ?>

    <?= $form->field($model, 'company_service_id') ?>

    <?= $form->field($model, 'measurement') ?>

    <?= $form->field($model, 'windowsill') ?>

    <?php // echo $form->field($model, 'lamination') ?>

    <?php // echo $form->field($model, 'slopes') ?>

    <?php // echo $form->field($model, 'tinting') ?>

    <?php // echo $form->field($model, 'energy_saver') ?>

    <?php // echo $form->field($model, 'installation') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'price_currency') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
