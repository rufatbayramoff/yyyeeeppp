<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CsWindowSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cs Windows';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cs Window', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'uid',
            'company_service_id',
            'measurement',
            'windowsill',
            // 'lamination',
            // 'slopes',
            // 'tinting',
            // 'energy_saver',
            // 'installation',
            // 'status',
            // 'is_active',
            // 'created_at',
            // 'updated_at',
            // 'price_currency',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
