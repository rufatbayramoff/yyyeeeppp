<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindow */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cs-window-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_service_id')->textInput() ?>

    <?= $form->field($model, 'measurement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'windowsill')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lamination')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slopes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tinting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'energy_saver')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'installation')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'price_currency')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
