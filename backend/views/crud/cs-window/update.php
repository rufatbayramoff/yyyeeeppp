<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindow */

$this->title = 'Update Cs Window: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cs Windows', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cs-window-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
