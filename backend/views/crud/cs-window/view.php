<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CsWindow */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cs Windows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'uid',
            'company_service_id',
            'measurement',
            'windowsill',
            'lamination',
            'slopes',
            'tinting',
            'energy_saver',
            'installation',
            'status',
            'is_active',
            'created_at',
            'updated_at',
            'price_currency',
        ],
    ]) ?>

</div>
