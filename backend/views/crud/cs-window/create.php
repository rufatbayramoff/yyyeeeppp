<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CsWindow */

$this->title = 'Create Cs Window';
$this->params['breadcrumbs'][] = ['label' => 'Cs Windows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cs-window-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
