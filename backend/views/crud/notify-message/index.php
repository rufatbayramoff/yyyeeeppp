<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\NotifyMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notify Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notify-message-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Notify Message', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'message_type',
            'params:ntext',
            'sender',
            // 'sent_datetime',
            // 'is_sended',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
