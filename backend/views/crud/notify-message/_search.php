<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\NotifyMessageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notify-message-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'message_type') ?>

    <?= $form->field($model, 'params') ?>

    <?= $form->field($model, 'sender') ?>

    <?php // echo $form->field($model, 'sent_datetime') ?>

    <?php // echo $form->field($model, 'is_sended') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
