<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NotifyMessage */

$this->title = 'Create Notify Message';
$this->params['breadcrumbs'][] = ['label' => 'Notify Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notify-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
