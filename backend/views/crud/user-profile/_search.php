<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\UserProfileSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profile-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'full_name') ?>

    <?= $form->field($model, 'dob_date') ?>

    <?= $form->field($model, 'address_id') ?>

    <?= $form->field($model, 'website') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'phone_confirmed')->checkbox() ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'current_lang') ?>

    <?php // echo $form->field($model, 'current_currency_iso') ?>

    <?php // echo $form->field($model, 'current_metrics') ?>

    <?php // echo $form->field($model, 'timezone_id') ?>

    <?php // echo $form->field($model, 'firstname') ?>

    <?php // echo $form->field($model, 'lastname') ?>

    <?php // echo $form->field($model, 'is_printservice') ?>

    <?php // echo $form->field($model, 'is_modelshop') ?>

    <?php // echo $form->field($model, 'avatar_url') ?>

    <?php // echo $form->field($model, 'background_url') ?>

    <?php // echo $form->field($model, 'gender') ?>

    <?php // echo $form->field($model, 'printservice_title') ?>

    <?php // echo $form->field($model, 'modelshop_title') ?>

    <?php // echo $form->field($model, 'info_about') ?>

    <?php // echo $form->field($model, 'info_skills') ?>

    <?php // echo $form->field($model, 'default_license_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
