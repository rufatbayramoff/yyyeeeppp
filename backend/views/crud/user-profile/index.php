<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Profile', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_id',
            'full_name',
            'dob_date',
            'address_id',
            'website',
            // 'phone',
            // 'phone_confirmed:boolean',
            // 'updated_at',
            // 'current_lang',
            // 'current_currency_iso',
            // 'current_metrics',
            // 'timezone_id',
            // 'firstname',
            // 'lastname',
            // 'is_printservice',
            // 'is_modelshop',
            // 'avatar_url:url',
            // 'background_url:url',
            // 'gender',
            // 'printservice_title',
            // 'modelshop_title',
            // 'info_about:ntext',
            // 'info_skills:ntext',
            // 'default_license_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
