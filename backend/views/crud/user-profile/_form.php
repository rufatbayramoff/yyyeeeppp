<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dob_date')->textInput() ?>

    <?= $form->field($model, 'address_id')->textInput() ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_confirmed')->checkbox() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'current_lang')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'current_currency_iso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'current_metrics')->dropDownList([ 'cm' => 'Cm', 'in' => 'In', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'timezone_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_printservice')->textInput() ?>

    <?= $form->field($model, 'is_modelshop')->textInput() ?>

    <?= $form->field($model, 'avatar_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'background_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gender')->dropDownList([ 'male' => 'Male', 'female' => 'Female', 'none' => 'None', 'multi' => 'Multi', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'printservice_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modelshop_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'info_about')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'info_skills')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'default_license_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
