<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */

$this->title = $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'User Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'full_name',
            'dob_date',
            'address_id',
            'website',
            'phone',
            'phone_confirmed:boolean',
            'updated_at',
            'current_lang',
            'current_currency_iso',
            'current_metrics',
            'timezone_id',
            'firstname',
            'lastname',
            'is_printservice',
            'is_modelshop',
            'avatar_url:url',
            'background_url:url',
            'gender',
            'printservice_title',
            'modelshop_title',
            'info_about:ntext',
            'info_skills:ntext',
            'default_license_id',
        ],
    ]) ?>

</div>
