<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ApiFile */

$this->title = 'Update Api File: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Api Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="api-file-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
