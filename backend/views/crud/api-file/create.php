<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ApiFile */

$this->title = 'Create Api File';
$this->params['breadcrumbs'][] = ['label' => 'Api Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
