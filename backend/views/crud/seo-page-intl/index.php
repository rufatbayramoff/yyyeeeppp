<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SeoPageIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seo Page Intls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-page-intl-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Seo Page Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'seo_page_id',
            'lang_iso',
            'title',
            'header',
            // 'meta_description',
            // 'meta_keywords',
            // 'header_text:ntext',
            // 'footer_text:ntext',
            // 'is_active:boolean',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
