<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SeoPageIntl */

$this->title = 'Create Seo Page Intl';
$this->params['breadcrumbs'][] = ['label' => 'Seo Page Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-page-intl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
