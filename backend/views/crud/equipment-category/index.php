<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EquipmentCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Equipment Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipment-category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Equipment Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'lft',
            'rgt',
            'depth',
            'code',
            // 'title',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
