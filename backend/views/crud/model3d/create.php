<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Model3d */

$this->title = 'Create Model3d';
$this->params['breadcrumbs'][] = ['label' => 'Model3ds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
