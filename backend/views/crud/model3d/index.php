<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3ds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model3d', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'user_id',
            'description:ntext',
            'created_at',
            // 'updated_at',
            // 'published_at',
            // 'is_published',
            // 'is_printer_ready',
            // 'dimensions',
            // 'status',
            // 'cover_file_id',
            // 'stat_views',
            // 'model_units',
            // 'category_id',
            // 'source',
            // 'model3d_texture_id',
            // 'cae',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
