<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Model3d */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Model3ds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'user_id',
            'description:ntext',
            'created_at',
            'updated_at',
            'published_at',
            'is_published',
            'is_printer_ready',
            'dimensions',
            'status',
            'cover_file_id',
            'stat_views',
            'model_units',
            'category_id',
            'source',
            'model3d_texture_id',
            'cae',
        ],
    ]) ?>

</div>
