<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Model3d */

$this->title = 'Update Model3d: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Model3ds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="model3d-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
