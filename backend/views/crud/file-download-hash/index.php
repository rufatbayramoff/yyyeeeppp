<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FileDownloadHashSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'File Download Hashes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-download-hash-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create File Download Hash', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'file_id',
            'start_date',
            'end_date',
            'hash',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
