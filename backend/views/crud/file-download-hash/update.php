<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FileDownloadHash */

$this->title = 'Update File Download Hash: ' . $model->hash;
$this->params['breadcrumbs'][] = ['label' => 'File Download Hashes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->hash, 'url' => ['view', 'id' => $model->hash]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="file-download-hash-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
