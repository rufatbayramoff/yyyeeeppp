<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FileDownloadHash */

$this->title = 'Create File Download Hash';
$this->params['breadcrumbs'][] = ['label' => 'File Download Hashes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-download-hash-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
