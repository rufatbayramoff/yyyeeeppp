<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MsgMessage */

$this->title = 'Update Msg Message: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Msg Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="msg-message-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
