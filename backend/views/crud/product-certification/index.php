<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductCertificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Certifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-certification-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product Certification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'file_id',
            'certifier',
            'application',
            // 'created_at',
            // 'issue_date',
            // 'expire_date',
            // 'product_uuid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
