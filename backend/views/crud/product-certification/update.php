<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductCertification */

$this->title = 'Update Product Certification: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Product Certifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-certification-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
