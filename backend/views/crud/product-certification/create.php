<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductCertification */

$this->title = 'Create Product Certification';
$this->params['breadcrumbs'][] = ['label' => 'Product Certifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-certification-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
