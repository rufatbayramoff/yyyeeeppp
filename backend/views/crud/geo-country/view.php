<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\GeoCountry */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Geo Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-country-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'iso_code',
            'phone_code',
            'iso_alpha3',
            'currency_code',
            'currency_name',
            'currency_symbol',
            'is_easypost_intl:boolean',
            'is_easypost_domestic:boolean',
            'capital_id',
            'region_required:boolean',
            'paypal_payout:boolean',
        ],
    ]) ?>

</div>
