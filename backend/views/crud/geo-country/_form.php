<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GeoCountry */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geo-country-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iso_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iso_alpha3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_symbol')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_easypost_intl')->checkbox() ?>

    <?= $form->field($model, 'is_easypost_domestic')->checkbox() ?>

    <?= $form->field($model, 'capital_id')->textInput() ?>

    <?= $form->field($model, 'region_required')->checkbox() ?>

    <?= $form->field($model, 'paypal_payout')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
