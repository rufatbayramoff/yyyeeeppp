<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\GeoCountrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Geo Countries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-country-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Geo Country', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'iso_code',
            'phone_code',
            'iso_alpha3',
            // 'currency_code',
            // 'currency_name',
            // 'currency_symbol',
            // 'is_easypost_intl:boolean',
            // 'is_easypost_domestic:boolean',
            // 'capital_id',
            // 'region_required:boolean',
            // 'paypal_payout:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
