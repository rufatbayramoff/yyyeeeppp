<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GeoCountry */

$this->title = 'Create Geo Country';
$this->params['breadcrumbs'][] = ['label' => 'Geo Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-country-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
