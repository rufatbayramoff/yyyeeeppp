<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\GeoCountrySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geo-country-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'iso_code') ?>

    <?= $form->field($model, 'phone_code') ?>

    <?= $form->field($model, 'iso_alpha3') ?>

    <?php // echo $form->field($model, 'currency_code') ?>

    <?php // echo $form->field($model, 'currency_name') ?>

    <?php // echo $form->field($model, 'currency_symbol') ?>

    <?php // echo $form->field($model, 'is_easypost_intl')->checkbox() ?>

    <?php // echo $form->field($model, 'is_easypost_domestic')->checkbox() ?>

    <?php // echo $form->field($model, 'capital_id') ?>

    <?php // echo $form->field($model, 'region_required')->checkbox() ?>

    <?php // echo $form->field($model, 'paypal_payout')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
