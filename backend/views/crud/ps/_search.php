<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'logo') ?>

    <?php // echo $form->field($model, 'phone_code') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <?php // echo $form->field($model, 'moderator_status') ?>

    <?php // echo $form->field($model, 'moderated_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'phone_status') ?>

    <?php // echo $form->field($model, 'phone_country_iso') ?>

    <?php // echo $form->field($model, 'dont_show_download_policy_modal') ?>

    <?php // echo $form->field($model, 'is_excluded_from_printing') ?>

    <?php // echo $form->field($model, 'sms_gateway') ?>

    <?php // echo $form->field($model, 'logo_circle') ?>

    <?php // echo $form->field($model, 'picture_file_ids') ?>

    <?php // echo $form->field($model, 'designer_picture_file_ids') ?>

    <?php // echo $form->field($model, 'url') ?>

    <?php // echo $form->field($model, 'url_changes_count') ?>

    <?php // echo $form->field($model, 'url_old') ?>

    <?php // echo $form->field($model, 'is_test_order_offer_showed') ?>

    <?php // echo $form->field($model, 'is_cnc_allowed') ?>

    <?php // echo $form->field($model, 'max_progress_orders_count') ?>

    <?php // echo $form->field($model, 'is_cnc_hints_readed') ?>

    <?php // echo $form->field($model, 'website') ?>

    <?php // echo $form->field($model, 'facebook') ?>

    <?php // echo $form->field($model, 'instagram') ?>

    <?php // echo $form->field($model, 'twitter') ?>

    <?php // echo $form->field($model, 'is_designer') ?>

    <?php // echo $form->field($model, 'country_id') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'cover_file_id') ?>

    <?php // echo $form->field($model, 'ownership') ?>

    <?php // echo $form->field($model, 'total_employees') ?>

    <?php // echo $form->field($model, 'year_established') ?>

    <?php // echo $form->field($model, 'annual_turnover') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
