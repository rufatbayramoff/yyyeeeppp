<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Ps */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'logo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->textInput() ?>

    <?= $form->field($model, 'moderator_status')->dropDownList([ 'new' => 'New', 'updated' => 'Updated', 'checking' => 'Checking', 'checked' => 'Checked', 'banned' => 'Banned', 'rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'moderated_at')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'phone_status')->dropDownList([ 'new' => 'New', 'checking' => 'Checking', 'checked' => 'Checked', 'failed' => 'Failed', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'phone_country_iso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dont_show_download_policy_modal')->textInput() ?>

    <?= $form->field($model, 'is_excluded_from_printing')->textInput() ?>

    <?= $form->field($model, 'sms_gateway')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'logo_circle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picture_file_ids')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'designer_picture_file_ids')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url_changes_count')->textInput() ?>

    <?= $form->field($model, 'url_old')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_test_order_offer_showed')->textInput() ?>

    <?= $form->field($model, 'is_cnc_allowed')->textInput() ?>

    <?= $form->field($model, 'max_progress_orders_count')->textInput() ?>

    <?= $form->field($model, 'is_cnc_hints_readed')->textInput() ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_designer')->textInput() ?>

    <?= $form->field($model, 'country_id')->textInput() ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cover_file_id')->textInput() ?>

    <?= $form->field($model, 'ownership')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_employees')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'year_established')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'annual_turnover')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
