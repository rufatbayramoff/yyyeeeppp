<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ps */

$this->title = 'Update Ps: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
