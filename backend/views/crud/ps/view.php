<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Ps */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'title',
            'description:ntext',
            'logo',
            'phone_code',
            'phone',
            'is_active',
            'moderator_status',
            'moderated_at',
            'created_at',
            'updated_at',
            'phone_status',
            'phone_country_iso',
            'dont_show_download_policy_modal',
            'is_excluded_from_printing',
            'sms_gateway',
            'logo_circle',
            'picture_file_ids:ntext',
            'designer_picture_file_ids:ntext',
            'url:url',
            'url_changes_count:url',
            'url_old:url',
            'is_test_order_offer_showed',
            'is_cnc_allowed',
            'max_progress_orders_count',
            'is_cnc_hints_readed',
            'website',
            'facebook',
            'instagram',
            'twitter',
            'is_designer',
            'country_id',
            'currency',
            'cover_file_id',
            'ownership',
            'total_employees',
            'year_established',
            'annual_turnover',
        ],
    ]) ?>

</div>
