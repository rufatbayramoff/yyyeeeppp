<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsCatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Catalogs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-catalog-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ps Catalog', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ps_id',
            'rating_avg',
            'rating_med',
            'rating_count',
            'price_low',
            'rating',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
