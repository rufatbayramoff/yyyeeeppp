<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsCatalog */

$this->title = 'Create Ps Catalog';
$this->params['breadcrumbs'][] = ['label' => 'Ps Catalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-catalog-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
