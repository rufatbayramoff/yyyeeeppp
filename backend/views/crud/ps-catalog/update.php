<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsCatalog */

$this->title = 'Update Ps Catalog: ' . $model->ps_id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Catalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ps_id, 'url' => ['view', 'id' => $model->ps_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-catalog-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
