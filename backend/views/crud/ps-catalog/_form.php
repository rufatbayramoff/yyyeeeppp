<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PsCatalog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-catalog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ps_id')->textInput() ?>

    <?= $form->field($model, 'rating_avg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rating_med')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rating_count')->textInput() ?>

    <?= $form->field($model, 'price_low')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
