<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PsCatalog */

$this->title = $model->ps_id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Catalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-catalog-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ps_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ps_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ps_id',
            'rating_avg',
            'rating_med',
            'rating_count',
            'price_low',
            'rating',
            'rating_log'
        ],
    ]) ?>

</div>
