<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PsCatalogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-catalog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ps_id') ?>

    <?= $form->field($model, 'rating_avg') ?>

    <?= $form->field($model, 'rating_med') ?>

    <?= $form->field($model, 'rating_count') ?>

    <?= $form->field($model, 'price_low') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
