<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryCardIntl */

$this->title = 'Create Home Page Category Card Intl';
$this->params['breadcrumbs'][] = ['label' => 'Home Page Category Card Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-category-card-intl-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
