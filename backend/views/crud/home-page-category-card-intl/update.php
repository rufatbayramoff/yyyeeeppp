<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryCardIntl */

$this->title = 'Update Home Page Category Card Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Category Card Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-page-category-card-intl-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
