<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SeoRedirSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seo Redirs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-redir-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Seo Redir', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'from_url:url',
            'to_url:url',
            'created_at',
            'user_admin_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
