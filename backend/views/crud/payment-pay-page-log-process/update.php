<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentPayPageLogProcess */

$this->title = 'Update Payment Pay Page Log Process: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Payment Pay Page Log Processes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-pay-page-log-process-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
