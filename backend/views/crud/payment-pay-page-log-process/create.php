<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentPayPageLogProcess */

$this->title = 'Create Payment Pay Page Log Process';
$this->params['breadcrumbs'][] = ['label' => 'Payment Pay Page Log Processes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-pay-page-log-process-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
