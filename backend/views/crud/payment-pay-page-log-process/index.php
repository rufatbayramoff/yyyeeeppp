<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentPayPageLogProcessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Pay Page Log Processes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-pay-page-log-process-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Pay Page Log Process', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'process_err',
            'process_payload',
            'process_message',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
