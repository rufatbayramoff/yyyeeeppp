<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DynamicField */

$this->title = 'Update Dynamic Field: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dynamic-field-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
