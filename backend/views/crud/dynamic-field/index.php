<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DynamicFieldSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dynamic Fields';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dynamic-field-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dynamic Field', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            'binded_model',
            'title',
            'type',
            // 'type_params',
            // 'default_value',
            // 'description:ntext',
            // 'is_active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
