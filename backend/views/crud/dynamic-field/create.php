<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DynamicField */

$this->title = 'Create Dynamic Field';
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dynamic-field-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
