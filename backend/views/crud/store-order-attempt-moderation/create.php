<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttemptModeration */

$this->title = 'Create Store Order Attempt Moderation';
$this->params['breadcrumbs'][] = ['label' => 'Store Order Attempt Moderations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-attempt-moderation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
