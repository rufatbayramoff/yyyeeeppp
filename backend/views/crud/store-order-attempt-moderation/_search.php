<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\StoreOrderAttemptModerationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-order-attempt-moderation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'attempt_id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'reject_reason_id') ?>

    <?= $form->field($model, 'reject_comment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
