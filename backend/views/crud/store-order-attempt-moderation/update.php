<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttemptModeration */

$this->title = 'Update Store Order Attempt Moderation: ' . $model->attempt_id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Attempt Moderations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->attempt_id, 'url' => ['view', 'id' => $model->attempt_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-order-attempt-moderation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
