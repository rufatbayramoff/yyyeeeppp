<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderAttemptModerationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Attempt Moderations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-attempt-moderation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order Attempt Moderation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'attempt_id',
            'status',
            'reject_reason_id',
            'reject_comment:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
