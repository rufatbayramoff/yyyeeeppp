<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttemptModeration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-order-attempt-moderation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'attempt_id')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'new' => 'New', 'accepted' => 'Accepted', 'rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'reject_reason_id')->textInput() ?>

    <?= $form->field($model, 'reject_comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
