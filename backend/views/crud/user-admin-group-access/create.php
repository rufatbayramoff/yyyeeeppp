<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserAdminGroupAccess */

$this->title = 'Create User Admin Group Access';
$this->params['breadcrumbs'][] = ['label' => 'User Admin Group Accesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-admin-group-access-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
