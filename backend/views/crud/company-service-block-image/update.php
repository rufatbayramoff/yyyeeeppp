<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceBlockImage */

$this->title = 'Update Company Service Block Image: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Service Block Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-service-block-image-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
