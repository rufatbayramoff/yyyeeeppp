<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceBlockImage */

$this->title = 'Create Company Service Block Image';
$this->params['breadcrumbs'][] = ['label' => 'Company Service Block Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-block-image-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
