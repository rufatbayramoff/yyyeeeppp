<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MsgFolderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Msg Folders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-folder-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Msg Folder', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'is_active',
            'sort',
            'alias',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
