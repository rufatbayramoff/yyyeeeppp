<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreUnitShoppingCandidate */

$this->title = 'Create Store Unit Shopping Candidate';
$this->params['breadcrumbs'][] = ['label' => 'Store Unit Shopping Candidates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-unit-shopping-candidate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
