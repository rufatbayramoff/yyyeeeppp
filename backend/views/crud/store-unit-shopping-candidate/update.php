<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreUnitShoppingCandidate */

$this->title = 'Update Store Unit Shopping Candidate: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Unit Shopping Candidates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-unit-shopping-candidate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
