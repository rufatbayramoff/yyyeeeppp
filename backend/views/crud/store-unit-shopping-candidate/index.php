<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreUnitShoppingCandidateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Unit Shopping Candidates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-unit-shopping-candidate-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Unit Shopping Candidate', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'store_unit_id',
            'type',
            'user_session_id',
            'create_date',
            // 'view_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
