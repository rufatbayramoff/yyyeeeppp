<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\StoreUnitShoppingCandidateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-unit-shopping-candidate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'store_unit_id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'user_session_id') ?>

    <?= $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'view_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
