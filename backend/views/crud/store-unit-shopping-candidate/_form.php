<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreUnitShoppingCandidate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-unit-shopping-candidate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'store_unit_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'api' => 'Api', 'utm' => 'Utm', 'print_here' => 'Print here', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'user_session_id')->textInput() ?>

    <?= $form->field($model, 'create_date')->textInput() ?>

    <?= $form->field($model, 'view_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
