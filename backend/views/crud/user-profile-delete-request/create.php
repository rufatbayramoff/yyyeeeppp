<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserProfileDeleteRequest */

$this->title = 'Create User Profile Delete Request';
$this->params['breadcrumbs'][] = ['label' => 'User Profile Delete Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-delete-request-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
