<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfileDeleteRequest */

$this->title = 'Update User Profile Delete Request: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Profile Delete Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-profile-delete-request-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
