<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AffiliateUserSourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Affiliate User Sources';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-user-source-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Affiliate User Source', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'user_id',
            'resource_id',
            'type',
            // 'status',
            // 'first_url:url',
            // 'referrer',
            // 'user_agent',
            // 'ip',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
