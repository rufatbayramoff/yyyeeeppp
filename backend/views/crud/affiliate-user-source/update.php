<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AffiliateUserSource */

$this->title = 'Update Affiliate User Source: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Affiliate User Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="affiliate-user-source-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
