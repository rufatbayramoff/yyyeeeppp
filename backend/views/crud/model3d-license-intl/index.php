<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\Model3dLicenseIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model3d License Intls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model3d-license-intl-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model3d License Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'license_id',
            'title',
            'description:ntext',
            'info:ntext',
            // 'lang_iso',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
