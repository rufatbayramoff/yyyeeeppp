<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentbtAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Paymentbt Addresses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paymentbt-address-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Paymentbt Address', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'updated_at',
            'address_id',
            'customer_id',
            // 'firstname',
            // 'lastname',
            // 'company',
            // 'street_address',
            // 'extended_address',
            // 'city',
            // 'region',
            // 'postal_code',
            // 'country',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
