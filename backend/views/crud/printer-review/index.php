<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-review-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Printer Review', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'printer_id',
            'user_id',
            'created_at',
            'print_quality',
            // 'ease_of_use',
            // 'failure_rate',
            // 'software',
            // 'community',
            // 'build_quality',
            // 'reliability',
            // 'running_expenses',
            // 'customer_service',
            // 'value',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
