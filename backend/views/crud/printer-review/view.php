<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterReview */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Printer Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-review-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'printer_id',
            'user_id',
            'created_at',
            'print_quality',
            'ease_of_use',
            'failure_rate',
            'software',
            'community',
            'build_quality',
            'reliability',
            'running_expenses',
            'customer_service',
            'value',
            'status',
        ],
    ]) ?>

</div>
