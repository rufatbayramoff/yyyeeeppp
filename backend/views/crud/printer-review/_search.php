<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PrinterReviewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printer-review-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'printer_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'print_quality') ?>

    <?php // echo $form->field($model, 'ease_of_use') ?>

    <?php // echo $form->field($model, 'failure_rate') ?>

    <?php // echo $form->field($model, 'software') ?>

    <?php // echo $form->field($model, 'community') ?>

    <?php // echo $form->field($model, 'build_quality') ?>

    <?php // echo $form->field($model, 'reliability') ?>

    <?php // echo $form->field($model, 'running_expenses') ?>

    <?php // echo $form->field($model, 'customer_service') ?>

    <?php // echo $form->field($model, 'value') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
