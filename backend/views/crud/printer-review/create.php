<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterReview */

$this->title = 'Create Printer Review';
$this->params['breadcrumbs'][] = ['label' => 'Printer Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-review-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
