<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterReview */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printer-review-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'printer_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'print_quality')->textInput() ?>

    <?= $form->field($model, 'ease_of_use')->textInput() ?>

    <?= $form->field($model, 'failure_rate')->textInput() ?>

    <?= $form->field($model, 'software')->textInput() ?>

    <?= $form->field($model, 'community')->textInput() ?>

    <?= $form->field($model, 'build_quality')->textInput() ?>

    <?= $form->field($model, 'reliability')->textInput() ?>

    <?= $form->field($model, 'running_expenses')->textInput() ?>

    <?= $form->field($model, 'customer_service')->textInput() ?>

    <?= $form->field($model, 'value')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
