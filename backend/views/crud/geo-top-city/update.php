<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GeoTopCity */

$this->title = 'Update Geo Top City: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Geo Top Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="geo-top-city-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
