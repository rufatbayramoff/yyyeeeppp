<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GeoTopCity */

$this->title = 'Create Geo Top City';
$this->params['breadcrumbs'][] = ['label' => 'Geo Top Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-top-city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
