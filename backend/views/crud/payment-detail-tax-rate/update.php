<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentDetailTaxRate */

$this->title = 'Update Payment Detail Tax Rate: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Detail Tax Rates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-detail-tax-rate-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
