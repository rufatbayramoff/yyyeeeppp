<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentDetailTaxRate */

$this->title = 'Create Payment Detail Tax Rate';
$this->params['breadcrumbs'][] = ['label' => 'Payment Detail Tax Rates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-detail-tax-rate-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
