<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialIntl */

$this->title = 'Update Printer Material Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Material Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-material-intl-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
