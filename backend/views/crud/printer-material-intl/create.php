<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialIntl */

$this->title = 'Create Printer Material Intl';
$this->params['breadcrumbs'][] = ['label' => 'Printer Material Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-intl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
