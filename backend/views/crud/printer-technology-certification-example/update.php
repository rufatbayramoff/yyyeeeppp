<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterTechnologyCertificationExample */

$this->title = 'Update Printer Technology Certification Example: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Printer Technology Certification Examples', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-technology-certification-example-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
