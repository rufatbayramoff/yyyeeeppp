<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterTechnologyCertificationExampleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Technology Certification Examples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-technology-certification-example-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Printer Technology Certification Example', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'technology_id',
            'file_uuid',
            'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
