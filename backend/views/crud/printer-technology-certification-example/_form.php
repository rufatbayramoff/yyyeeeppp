<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterTechnologyCertificationExample */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printer-technology-certification-example-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'technology_id')->textInput() ?>

    <?= $form->field($model, 'file_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'common' => 'Common', 'professional' => 'Professional', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
