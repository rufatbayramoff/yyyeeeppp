<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterTechnologyCertificationExample */

$this->title = 'Create Printer Technology Certification Example';
$this->params['breadcrumbs'][] = ['label' => 'Printer Technology Certification Examples', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-technology-certification-example-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
