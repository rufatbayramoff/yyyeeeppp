<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserSession */

$this->title = 'Update User Session: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Sessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-session-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
