<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserSession */

$this->title = 'Create User Session';
$this->params['breadcrumbs'][] = ['label' => 'User Sessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-session-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
