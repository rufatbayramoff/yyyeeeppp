<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AffiliateSourceReferrer */

$this->title = 'Create Affiliate Source Referrer';
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Source Referrers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-source-referrer-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
