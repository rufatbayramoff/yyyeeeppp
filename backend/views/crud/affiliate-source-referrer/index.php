<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AffiliateSourceReferrerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Affiliate Source Referrers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-source-referrer-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Affiliate Source Referrer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'affiliate_source_uuid',
            'referrer',
            'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
