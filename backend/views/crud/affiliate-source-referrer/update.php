<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AffiliateSourceReferrer */

$this->title = 'Update Affiliate Source Referrer: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Source Referrers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="affiliate-source-referrer-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
