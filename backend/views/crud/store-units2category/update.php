<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreUnits2category */

$this->title = 'Update Store Units2category: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Units2categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-units2category-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
