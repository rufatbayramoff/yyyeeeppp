<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\HomePageCategoryCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Home Page Category Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-page-category-card-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Home Page Category Card', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'is_active',
            'product_category_id',
            'title',
            // 'url:url',
            // 'cover_file_uuid',
            // 'position',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
