<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HomePageCategoryCard */

$this->title = 'Update Home Page Category Card: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Category Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-page-category-card-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
