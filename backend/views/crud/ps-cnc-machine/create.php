<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PsCncMachine */

$this->title = 'Create Ps Cnc Machine';
$this->params['breadcrumbs'][] = ['label' => 'Ps Cnc Machines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-cnc-machine-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
