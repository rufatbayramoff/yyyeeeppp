<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsCncMachine */

$this->title = 'Update Ps Cnc Machine: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Cnc Machines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-cnc-machine-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
