<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserPaypal */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Paypals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-paypal-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'updated_at',
            'user_id',
            'fullname',
            'email:email',
            'address',
            'country_iso',
            'check_status',
        ],
    ]) ?>

</div>
