<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserPaypal */

$this->title = 'Update User Paypal: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Paypals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-paypal-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
