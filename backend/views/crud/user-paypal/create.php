<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserPaypal */

$this->title = 'Create User Paypal';
$this->params['breadcrumbs'][] = ['label' => 'User Paypals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-paypal-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
