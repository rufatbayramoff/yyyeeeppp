<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OsnVendorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Osn Vendors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="osn-vendor-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Osn Vendor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'code',
            'icon',
            'is_active',
            // 'app_id',
            // 'app_key',
            // 'app_config',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
