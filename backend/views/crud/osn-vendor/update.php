<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OsnVendor */

$this->title = 'Update Osn Vendor: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Osn Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="osn-vendor-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
