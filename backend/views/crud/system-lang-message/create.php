<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SystemLangMessage */

$this->title = Yii::t('app', 'Create System Lang Message');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Lang Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-lang-message-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
