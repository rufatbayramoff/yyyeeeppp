<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\DynamicFieldCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dynamic-field-category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'field_code') ?>

    <?= $form->field($model, 'category_id') ?>

    <?= $form->field($model, 'is_required') ?>

    <?= $form->field($model, 'default_value') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <?php // echo $form->field($model, 'has_filter') ?>

    <?php // echo $form->field($model, 'filter_params') ?>

    <?php // echo $form->field($model, 'position') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
