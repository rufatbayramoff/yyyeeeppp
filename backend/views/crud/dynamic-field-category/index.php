<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DynamicFieldCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dynamic Field Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dynamic-field-category-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dynamic Field Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'field_code',
            'category_id',
            'is_required',
            'default_value',
            // 'is_active',
            // 'has_filter',
            // 'filter_params',
            // 'position',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
