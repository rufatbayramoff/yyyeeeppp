<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DynamicFieldCategory */

$this->title = 'Update Dynamic Field Category: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Field Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dynamic-field-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
