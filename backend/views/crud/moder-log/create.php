<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ModerLog */

$this->title = 'Create Moder Log';
$this->params['breadcrumbs'][] = ['label' => 'Moder Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moder-log-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
