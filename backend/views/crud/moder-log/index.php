<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ModerLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Moder Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moder-log-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Moder Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'created_at',
            'started_at',
            'finished_at',
            // 'action',
            // 'result:ntext',
            // 'object_type',
            // 'object_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
