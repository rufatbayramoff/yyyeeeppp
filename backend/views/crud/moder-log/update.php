<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ModerLog */

$this->title = 'Update Moder Log: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Moder Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="moder-log-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
