<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserDeactivatingLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-deactivating-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reason_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'reason_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reason_desc')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
