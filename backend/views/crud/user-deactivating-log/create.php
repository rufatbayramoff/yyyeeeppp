<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserDeactivatingLog */

$this->title = 'Create User Deactivating Log';
$this->params['breadcrumbs'][] = ['label' => 'User Deactivating Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-deactivating-log-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
