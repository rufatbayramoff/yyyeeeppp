<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserDeactivatingLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Deactivating Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-deactivating-log-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Deactivating Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'reason_id',
            'user_id',
            'created_at',
            'reason_title',
            // 'reason_desc',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
