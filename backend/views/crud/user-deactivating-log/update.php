<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserDeactivatingLog */

$this->title = 'Update User Deactivating Log: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Deactivating Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-deactivating-log-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
