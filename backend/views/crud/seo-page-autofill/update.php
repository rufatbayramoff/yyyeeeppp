<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SeoPageAutofill */

$this->title = 'Update Seo Page Autofill: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Seo Page Autofills', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="seo-page-autofill-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
