<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SeoPageAutofillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seo Page Autofills';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-page-autofill-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Seo Page Autofill', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'object_type',
            'object_id',
            'created_at',
            'seo_page_id',
            // 'template_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
