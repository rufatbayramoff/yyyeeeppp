<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingMachineProcessingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cutting Machine Processings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-machine-processing-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cutting Machine Processing', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'cutting_machine_id',
            'cutting_material_id',
            'thickness',
            'cutting_price',
            // 'engraving_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
