<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CuttingMachineProcessing */

$this->title = 'Create Cutting Machine Processing';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Machine Processings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cutting-machine-processing-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
