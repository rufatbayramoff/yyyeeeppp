<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CuttingMachineProcessingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cutting-machine-processing-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'uuid') ?>

    <?= $form->field($model, 'cutting_machine_id') ?>

    <?= $form->field($model, 'cutting_material_id') ?>

    <?= $form->field($model, 'thickness') ?>

    <?= $form->field($model, 'cutting_price') ?>

    <?php // echo $form->field($model, 'engraving_price') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
