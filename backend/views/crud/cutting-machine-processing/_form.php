<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingMachineProcessing */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cutting-machine-processing-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cutting_machine_id')->textInput() ?>

    <?= $form->field($model, 'cutting_material_id')->textInput() ?>

    <?= $form->field($model, 'thickness')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cutting_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'engraving_price')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
