<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingMachineProcessing */

$this->title = 'Update Cutting Machine Processing: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Machine Processings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cutting-machine-processing-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
