<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductMainCard */

$this->title = 'Create Product Main Card';
$this->params['breadcrumbs'][] = ['label' => 'Product Main Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-main-card-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
