<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductMainCard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-main-card-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_common_uid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'feature' => 'Feature', 'machine' => 'Machine', 'material' => 'Material', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
