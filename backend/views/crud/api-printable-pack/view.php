<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ApiPrintablePack */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Api Printable Packs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-printable-pack-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'model3d_id',
            'api_external_system_id',
            'created_at',
            'affiliate_price',
            'affiliate_currency_id',
            'status',
        ],
    ]) ?>

</div>
