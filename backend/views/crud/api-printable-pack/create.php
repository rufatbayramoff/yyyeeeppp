<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ApiPrintablePack */

$this->title = 'Create Api Printable Pack';
$this->params['breadcrumbs'][] = ['label' => 'Api Printable Packs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-printable-pack-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
