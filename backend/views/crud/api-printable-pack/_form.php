<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ApiPrintablePack */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="api-printable-pack-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'model3d_id')->textInput() ?>

    <?= $form->field($model, 'api_external_system_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'affiliate_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'affiliate_currency_id')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'expired' => 'Expired', 'active' => 'Active', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
