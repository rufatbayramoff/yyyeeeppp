<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentbtCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Paymentbt Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paymentbt-customer-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Paymentbt Customer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'created_at',
            'updated_at',
            'firstname',
            // 'lastname',
            // 'company',
            // 'email:email',
            // 'phone',
            // 'fax',
            // 'customer_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
