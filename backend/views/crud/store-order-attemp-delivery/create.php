<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttempDelivery */

$this->title = 'Create Store Order Attemp Delivery';
$this->params['breadcrumbs'][] = ['label' => 'Store Order Attemp Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-attemp-delivery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
