<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderAttempDelivery */

$this->title = 'Update Store Order Attemp Delivery: ' . $model->order_attemp_id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Attemp Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->order_attemp_id, 'url' => ['view', 'id' => $model->order_attemp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-order-attemp-delivery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
