<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\StoreOrderAttempDeliverySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-order-attemp-delivery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'order_attemp_id') ?>

    <?= $form->field($model, 'file_id') ?>

    <?= $form->field($model, 'tracking_number') ?>

    <?= $form->field($model, 'tracking_shipper') ?>

    <?= $form->field($model, 'send_message_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
