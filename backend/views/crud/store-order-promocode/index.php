<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderPromocodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Promocodes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-promocode-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Order Promocode', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'order_id',
            'promocode_id',
            'created_at',
            'amount',
            // 'amount_currency',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
