<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderPromocode */

$this->title = 'Create Store Order Promocode';
$this->params['breadcrumbs'][] = ['label' => 'Store Order Promocodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-promocode-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
