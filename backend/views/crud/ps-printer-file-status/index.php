<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsPrinterFileStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Printer File Statuses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-printer-file-status-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ps Printer File Status', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'order_id',
            'model3d_part_id',
            'file_id',
            'status',
            // 'details',
            // 'created_at',
            // 'check_status_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
