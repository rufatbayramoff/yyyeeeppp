<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TaxUs */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tax uses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tax-us-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'zip_code',
            'state_abbrevation',
            'county_name',
            'city_name',
            'state_sales_tax',
            'county_sales_tax',
            'city_sales_tax',
            'state_use_tax',
            'county_use_tax',
            'city_use_tax',
            'total_sales_tax',
            'total_use_tax',
            'is_tax_shipping_alone',
            'is_tax_ship_handling',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
