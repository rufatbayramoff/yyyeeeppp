<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\TaxUsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tax-us-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'zip_code') ?>

    <?= $form->field($model, 'state_abbrevation') ?>

    <?= $form->field($model, 'county_name') ?>

    <?= $form->field($model, 'city_name') ?>

    <?php // echo $form->field($model, 'state_sales_tax') ?>

    <?php // echo $form->field($model, 'county_sales_tax') ?>

    <?php // echo $form->field($model, 'city_sales_tax') ?>

    <?php // echo $form->field($model, 'state_use_tax') ?>

    <?php // echo $form->field($model, 'county_use_tax') ?>

    <?php // echo $form->field($model, 'city_use_tax') ?>

    <?php // echo $form->field($model, 'total_sales_tax') ?>

    <?php // echo $form->field($model, 'total_use_tax') ?>

    <?php // echo $form->field($model, 'is_tax_shipping_alone') ?>

    <?php // echo $form->field($model, 'is_tax_ship_handling') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
