<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TaxUsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tax uses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tax-us-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tax Us', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'zip_code',
            'state_abbrevation',
            'county_name',
            'city_name',
            // 'state_sales_tax',
            // 'county_sales_tax',
            // 'city_sales_tax',
            // 'state_use_tax',
            // 'county_use_tax',
            // 'city_use_tax',
            // 'total_sales_tax',
            // 'total_use_tax',
            // 'is_tax_shipping_alone',
            // 'is_tax_ship_handling',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
