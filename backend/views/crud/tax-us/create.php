<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaxUs */

$this->title = 'Create Tax Us';
$this->params['breadcrumbs'][] = ['label' => 'Tax uses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tax-us-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
