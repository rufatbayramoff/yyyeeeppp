<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaxUs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tax-us-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'zip_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state_abbrevation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'county_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state_sales_tax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'county_sales_tax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_sales_tax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state_use_tax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'county_use_tax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_use_tax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_sales_tax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_use_tax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_tax_shipping_alone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_tax_ship_handling')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
