<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteBlock */

$this->title = 'Create Site Block';
$this->params['breadcrumbs'][] = ['label' => 'Site Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-block-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
