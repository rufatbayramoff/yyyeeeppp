<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CncPreorderWorkSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cnc-preorder-work-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'preorder_id') ?>

    <?= $form->field($model, 'part_replica_id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'cost') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'per') ?>

    <?php // echo $form->field($model, 'source') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
