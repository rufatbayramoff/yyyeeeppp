<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CncPreorderWork */

$this->title = 'Create Cnc Preorder Work';
$this->params['breadcrumbs'][] = ['label' => 'Cnc Preorder Works', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnc-preorder-work-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
