<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CncPreorderWork */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cnc-preorder-work-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'preorder_id')->textInput() ?>

    <?= $form->field($model, 'part_replica_id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'per')->dropDownList([ 'part' => 'Part', 'batch' => 'Batch', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'source')->dropDownList([ 'api' => 'Api', 'manual' => 'Manual', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
