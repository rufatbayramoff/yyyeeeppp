<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InstantPayment */

$this->title = 'Update Instant Payment: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Instant Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="instant-payment-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
