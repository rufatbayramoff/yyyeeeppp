<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\InstantPayment */

$this->title = $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Instant Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instant-payment-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uuid',
            'created_at',
            'descr',
            'from_user_id',
            'to_user_id',
            'sum',
            'currency',
            'status',
            'payed_at',
            'finished_at',
        ],
    ]) ?>

</div>
