<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InstantPayment */

$this->title = 'Create Instant Payment';
$this->params['breadcrumbs'][] = ['label' => 'Instant Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instant-payment-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
