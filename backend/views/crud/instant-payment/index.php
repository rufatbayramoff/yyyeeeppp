<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\InstantPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Instant Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instant-payment-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Instant Payment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'created_at',
            'descr',
            'from_user_id',
            'to_user_id',
            // 'sum',
            // 'currency',
            // 'status',
            // 'payed_at',
            // 'finished_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
