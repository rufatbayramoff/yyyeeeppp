<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContentFilterCheckedMessages */

$this->title = 'Create Content Filter Checked Messages';
$this->params['breadcrumbs'][] = ['label' => 'Content Filter Checked Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-filter-checked-messages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
