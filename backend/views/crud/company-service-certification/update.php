<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceCertification */

$this->title = 'Update Company Service Certification: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company Service Certifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-service-certification-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
