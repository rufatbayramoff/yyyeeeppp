<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceCertification */

$this->title = 'Create Company Service Certification';
$this->params['breadcrumbs'][] = ['label' => 'Company Service Certifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-certification-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
