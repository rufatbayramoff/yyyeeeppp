<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteSearchQuery */

$this->title = 'Update Site Search Query: ' . ' ' . $model->query;
$this->params['breadcrumbs'][] = ['label' => 'Site Search Queries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->query, 'url' => ['view', 'id' => $model->query]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-search-query-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
