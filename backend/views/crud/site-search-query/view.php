<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SiteSearchQuery */

$this->title = $model->query;
$this->params['breadcrumbs'][] = ['label' => 'Site Search Queries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-search-query-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->query], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->query], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'query',
            'qty',
        ],
    ]) ?>

</div>
