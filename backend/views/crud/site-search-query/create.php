<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteSearchQuery */

$this->title = 'Create Site Search Query';
$this->params['breadcrumbs'][] = ['label' => 'Site Search Queries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-search-query-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
