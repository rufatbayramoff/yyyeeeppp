<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SiteSearchQuerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Site Search Queries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-search-query-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Site Search Query', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'query',
            'qty',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
