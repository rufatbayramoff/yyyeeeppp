<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Sms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sms-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Sms', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'created_at',
            'phone',
            'message',
            // 'status',
            // 'type',
            // 'updated_at',
            // 'response',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
