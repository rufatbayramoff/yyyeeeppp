<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserSms */

$this->title = 'Create User Sms';
$this->params['breadcrumbs'][] = ['label' => 'User Sms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sms-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
