<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PaymentLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'level') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'info') ?>

    <?php // echo $form->field($model, 'search_markers') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
