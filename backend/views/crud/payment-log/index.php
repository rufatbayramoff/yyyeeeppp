<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-log-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'created_at',
            'level',
            'type',
            'comment:ntext',
            // 'info',
            // 'search_markers',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
