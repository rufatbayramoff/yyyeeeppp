<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ApiExternalSystem */

$this->title = 'Create Api External System';
$this->params['breadcrumbs'][] = ['label' => 'Api External Systems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-external-system-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
