<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ApiExternalSystem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="api-external-system-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'private_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'public_upload_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'binded_user_id')->textInput() ?>

    <?= $form->field($model, 'json_config')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
