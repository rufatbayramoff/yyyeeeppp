<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ApiExternalSystemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Api External Systems';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-external-system-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Api External System', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'private_key',
            'public_upload_key',
            'binded_user_id',
            // 'json_config',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
