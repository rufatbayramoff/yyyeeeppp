<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterMaterialGroupUsageIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Material Group Usage Intls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-group-usage-intl-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Printer Material Group Usage Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'lang_iso',
            'title',
            'description',
            'is_active:boolean',
            // 'usage_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
