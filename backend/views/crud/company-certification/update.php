<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyCertification */

$this->title = 'Update Company Certification: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company Certifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-certification-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
