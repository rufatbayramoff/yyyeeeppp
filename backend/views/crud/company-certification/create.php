<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyCertification */

$this->title = 'Create Company Certification';
$this->params['breadcrumbs'][] = ['label' => 'Company Certifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-certification-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
