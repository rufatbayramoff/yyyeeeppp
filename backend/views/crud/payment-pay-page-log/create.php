<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentPayPageLog */

$this->title = 'Create Payment Pay Page Log';
$this->params['breadcrumbs'][] = ['label' => 'Payment Pay Page Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-pay-page-log-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
