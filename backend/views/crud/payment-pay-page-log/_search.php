<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PaymentPayPageLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-pay-page-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'uuid') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'ip') ?>

    <?= $form->field($model, 'user_agent') ?>

    <?= $form->field($model, 'invoice_uuid') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'process_err') ?>

    <?php // echo $form->field($model, 'process_payload') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
