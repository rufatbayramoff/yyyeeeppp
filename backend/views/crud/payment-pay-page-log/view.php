<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentPayPageLog */

$this->title = $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Payment Pay Page Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-pay-page-log-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uuid',
            'created_at',
            'ip',
            'user_agent',
            'invoice_uuid',
            'status',
            'process_err',
            'process_payload',
        ],
    ]) ?>

</div>
