<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentPayPageLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Pay Page Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-pay-page-log-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Pay Page Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uuid',
            'created_at',
            'ip',
            'user_agent',
            'invoice_uuid',
            // 'status',
            // 'process_err',
            // 'process_payload',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
