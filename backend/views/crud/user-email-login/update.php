<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserEmailLogin */

$this->title = 'Update User Email Login: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Email Logins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-email-login-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
