<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserEmailLoginSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Email Logins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-email-login-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Email Login', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'created_at',
            'visited_at',
            'expired_at',
            // 'hash',
            // 'email:email',
            // 'requested_url:url',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
