<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserEmailLogin */

$this->title = 'Create User Email Login';
$this->params['breadcrumbs'][] = ['label' => 'User Email Logins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-email-login-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
