<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TranslationDb */

$this->title = 'Create Translation Db';
$this->params['breadcrumbs'][] = ['label' => 'Translation Dbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translation-db-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
