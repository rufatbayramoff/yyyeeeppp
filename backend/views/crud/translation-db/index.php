<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TranslationDbSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Translation Dbs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translation-db-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Translation Db', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'table_name',
            'column_name',
            'line_id',
            'updated_at',
            // 'translated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
