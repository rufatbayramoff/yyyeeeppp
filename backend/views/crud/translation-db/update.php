<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TranslationDb */

$this->title = 'Update Translation Db: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Translation Dbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="translation-db-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
