<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMaterialPhoto */

$this->title = 'Create Wiki Material Photo';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Material Photos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-material-photo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
