<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WikiMachinePropertyIntl */

$this->title = 'Create Wiki Machine Property Intl';
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Property Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-property-intl-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
