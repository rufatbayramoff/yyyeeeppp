<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WikiMachinePropertyIntl */

$this->title = 'Update Wiki Machine Property Intl: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Wiki Machine Property Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wiki-machine-property-intl-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
