<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WikiMachinePropertyIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wiki Machine Property Intls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wiki-machine-property-intl-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Wiki Machine Property Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model_id',
            'value',
            'lang_iso',
            'is_active:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
