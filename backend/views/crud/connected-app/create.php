<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ConnectedApp */

$this->title = 'Create Connected App';
$this->params['breadcrumbs'][] = ['label' => 'Connected Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connected-app-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
