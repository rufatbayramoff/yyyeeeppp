<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ConnectedApp */

$this->title = 'Update Connected App: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Connected Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="connected-app-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
