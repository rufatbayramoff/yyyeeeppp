<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterMaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Materials';
$this->params['breadcrumbs'][] = $this->title;
$langs = common\models\SystemLang::find()->all();
echo $this->renderFile(Yii::getAlias('@backend/views/ps/printer-material/_topTabs.php'));
?>
<div class="printer-material-index">

    <p>
        <?= Html::a('Create Printer Material', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'caption' => new \backend\components\GridViewDataExporter($searchModel),

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'group_id',
                'value' => function($model){
                    if($model->group){
                        return $model->group->title;
                    }else{
                        return 'No group';
                    }
                },
                    'filter' => Html::activeDropDownList(
                    $searchModel,
                    'group_id',
                    yii\helpers\ArrayHelper::map(common\models\PrinterMaterialGroup::find()->asArray()->all(), 'id', 'title'),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(\common\models\PrinterMaterial $model) use($langs){
                    $result = [];
                    $map = \lib\collection\CollectionDb::getMap($model->printerMaterialIntls, 'lang_iso');
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->title : false;
                        if($val){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['ps/printer-material-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['ps/printer-material-intl/create',
                                'PrinterMaterialIntl[model_id]'=>$model->id, 'PrinterMaterialIntl[lang_iso]'=>$v->iso_code]);
                        }
                    }
                    $result = Html::tag("h5", $model->title) .  implode("<br />", $result);
                    return $result;
                }
            ],
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'filament_title',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
