<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printer-material-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]); ?>
    
     <?php echo $form->field($model, 'group_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\PrinterMaterialGroup::find()->asArray()->all(), 'id', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    
    <?= $form->field($model, 'filament_title')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'density')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'short_description'); ?>
    <?= $form->field($model, 'long_description')->textarea(); ?>

    <?= $form->field($model, 'recomend_price_per_gramm_min')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'recomend_price_per_gramm_max')->textInput(['maxlength' => true]); ?>

    <?= $form->field($model, 'is_active')->checkbox(); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
