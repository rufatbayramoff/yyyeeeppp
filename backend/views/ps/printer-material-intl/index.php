<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterMaterialIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Material Intls';
$this->params['breadcrumbs'][] = $this->title;

echo $this->renderFile(Yii::getAlias('@backend/views/ps/printer-material/_topTabs.php'));
?>
<div class="printer-material-intl-index">

    <p>
        <?= Html::a('Create Printer Material Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model_id',
            'lang_iso',
            'title',
            'short_description',
            // 'long_description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
