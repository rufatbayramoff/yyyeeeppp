<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialIntl */

$this->title = 'Update Printer Material Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Material Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-material-intl-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
