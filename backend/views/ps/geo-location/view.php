<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\GeoLocation */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Geo Locations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-location-view">
  

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'country.title',
            'region.title',
            'city.title',
            'title',
            'lat',
            'lon',
            'zip_code',
        ],
    ]) ?>

</div>
