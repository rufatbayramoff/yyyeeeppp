<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GeoLocation */

$this->title = Yii::t('app', 'Create Geo Location');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Geo Locations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-location-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
