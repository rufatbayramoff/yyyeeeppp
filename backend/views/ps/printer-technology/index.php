<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterTechnologySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Technologies';
$this->params['breadcrumbs'][] = $this->title;
$langs = common\models\SystemLang::find()->all();
echo $this->renderFile(Yii::getAlias('@backend/views/ps/printer-technology/_topTabs.php'));
?>
<div class="printer-technology-index">

    <p>
        <?= Html::a('Create Printer Technology', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'caption' => new \backend\components\GridViewDataExporter($searchModel),

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(\common\models\PrinterTechnology $model) use($langs){
                    $result = [];
                    $map = \lib\collection\CollectionDb::getMap($model->printerTechnologyIntls, 'lang_iso');
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->title : false;
                        if($val){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['ps/printer-technology-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['ps/printer-technology-intl/create',
                                'PrinterTechnologyIntl[model_id]'=>$model->id,
                                'PrinterTechnologyIntl[lang_iso]'=>$v->iso_code,
                                ]);
                        }
                    }
                    $result = Html::tag("h5", $model->title) .  implode("<br />", $result);
                    return $result;
                }
            ],
            'print_speed_min',
            'print_speed_max',
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
