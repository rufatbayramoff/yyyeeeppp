<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterTechnology */

$this->title = 'Create Printer Technology';
$this->params['breadcrumbs'][] = ['label' => 'Printer Technologies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-technology-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
