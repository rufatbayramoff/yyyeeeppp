<?php

use backend\widgets\FilesListWidget;
use common\components\ArrayHelper;
use common\components\FileTypesHelper;
use common\models\PsPrinterTest;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterTechnology */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printer-technology-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(); ?>
    <?= $form->field($model, 'print_speed_min')->textInput() ?>

    <?= $form->field($model, 'print_speed_max')->textInput() ?>
    <?= $form->field($model, 'code')->textInput() ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>



    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">Common Certification Examples</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'formPrefix'             => $model->formName(),
                    'formAttribute'          => 'commonCertificationFiles',
                    'filesList'              => ArrayHelper::getColumn($model->getCertificationExmplesForType(PsPrinterTest::TYPE_COMMON), 'file'),
                    'rights'                 => [
                        FilesListWidget::ALLOW_DELETE,
                        FilesListWidget::ALLOW_ADD,
                        FilesListWidget::ALLOW_MULTIPLE,
                        FilesListWidget::ALLOW_ROTATE
                    ],
                    'filesAllowedExtensions' => FileTypesHelper::ALLOW_IMAGES_EXTENSIONS
                ]
            ) ?>
        </div>
    </div>




    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">Professional Certification Examples</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'formPrefix'             => $model->formName(),
                    'formAttribute'          => 'professionalCertificationFiles',
                    'filesList'              => ArrayHelper::getColumn($model->getCertificationExmplesForType(PsPrinterTest::TYPE_PROFESSIONAL), 'file'),
                    'rights'                 => [
                        FilesListWidget::ALLOW_DELETE,
                        FilesListWidget::ALLOW_ADD,
                        FilesListWidget::ALLOW_MULTIPLE,
                        FilesListWidget::ALLOW_ROTATE
                    ],
                    'filesAllowedExtensions' => FileTypesHelper::ALLOW_IMAGES_EXTENSIONS
                ]
            ) ?>
        </div>
    </div>





    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
