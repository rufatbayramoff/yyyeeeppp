<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialGroup */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Material groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(
            'Delete',
            ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ]
        ) ?>
    </p>

    <?= DetailView::widget(
        [
            'model'      => $model,
            'attributes' => [
                'id',
                'priority',
                'code',
                'title',
                'density',
                'wall_thickness',
                'fill_percent',
                'short_description',
                'long_description',
                'is_active:boolean',
                'photoFile' => [
                    'label' => _t('site', 'Photo file'),
                    'value' => $model->photoFile?Html::img($model->photoFile->getFileUrl(), [
                        'class' => 'img-view'
                    ]):'',
                    'format' => 'raw'
                ]
            ]
        ]
    ); ?>

</div>
