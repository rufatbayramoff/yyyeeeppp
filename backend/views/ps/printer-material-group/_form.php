<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printer-material-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'priority')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'density')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'wall_thickness')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'fill_percent')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'fill_percent_max')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'short_description'); ?>
    <?= $form->field($model, 'long_description')->textarea(); ?>
    <?= $form->field($model, 'photoFile')->fileInput(); ?>

    <?= $form->field($model, 'is_active')->checkbox(); ?>
    <?= $form->field($model, 'need_supports')->checkbox(); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
