<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterMaterialGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Material Groups';
$this->params['breadcrumbs'][] = $this->title;
$langs = common\models\SystemLang::find()->all();
echo $this->renderFile(Yii::getAlias('@backend/views/ps/printer-material-group/_topTabs.php'));
?>
<div class="printer-material-index">

    <p>
        <?= Html::a('Create Printer Material Group', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'priority',
            'code',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(\common\models\PrinterMaterialGroup $model) use($langs){
                    $result = [];
                    $map = \lib\collection\CollectionDb::getMap($model->printerMaterialGroupIntls, 'lang_iso');
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->title : false;
                        if($val){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['ps/printer-material-group-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['ps/printer-material-group-intl/create']);
                        }
                    }
                    $result = Html::tag("h5", $model->title) .  implode("<br />", $result);
                    return $result;
                }
            ],
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'need_supports',
                'value' => function ($model) {
                    return ($model->need_supports) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'need_supports',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'density',
            'wall_thickness',
            'fill_percent',
            [
                'attribute' => 'short_description',
                'format' => 'raw',
                'value' => function(\common\models\PrinterMaterialGroup $model) use($langs){
                    $result = [];
                    $map = \lib\collection\CollectionDb::getMap($model->printerMaterialGroupIntls, 'lang_iso');
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->short_description : false;
                        if($val){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['ps/printer-material-group-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['ps/printer-material-group-intl/create']);
                        }
                    }
                    $result = Html::tag("h5", $model->short_description) .  implode("<br />", $result);
                    return $result;
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
