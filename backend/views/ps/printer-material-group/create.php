<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterial */

$this->title = 'Create Printer Material group';
$this->params['breadcrumbs'][] = ['label' => 'Printer Material groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
