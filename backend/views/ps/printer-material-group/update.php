<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialGroup */

$this->title = 'Update Printer Material: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Material groupss', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-material-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
