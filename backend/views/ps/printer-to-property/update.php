<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterToProperty */

$this->title = 'Update Printer To Property: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Printer To Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-to-property-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
