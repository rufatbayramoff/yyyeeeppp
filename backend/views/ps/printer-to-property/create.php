<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterToProperty */

$this->title = 'Create Printer To Property';
$this->params['breadcrumbs'][] = ['label' => 'Printer To Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-to-property-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
