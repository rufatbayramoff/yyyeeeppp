<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterToPropertySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer To Properties';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-to-property-index">

    <p>
        <?= Html::a('Create Printer To Property', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    $printers = yii\helpers\ArrayHelper::map(common\models\Printer::find()->asArray()->all(), 'id', 'title');
    $properties = yii\helpers\ArrayHelper::map(common\models\PrinterProperties::find()->asArray()->all(), 'id', 'title');
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'printer_id',
                'value' => function ($model) use ($printers) {
                    return $printers[$model->printer_id];
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'printer_id',
                    $printers,
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'property_id',
                'value' => function ($model) use ($properties) {
                    return $properties[$model->property_id];
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'property_id',
                    $properties,
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'value',
            'is_editable:boolean',
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
