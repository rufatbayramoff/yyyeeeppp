<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-index">

    <p>
        <?= Html::a('Create Printer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    $technologies = yii\helpers\ArrayHelper::map(common\models\PrinterTechnology::find()->asArray()->all(), 'id', 'title');
    ?>

    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute'=>'firm',
                'label' => 'Brand'
            ],

            'title',
            [
                'attribute' => 'description',
                'value' => function ($model) {
                    return \yii\helpers\StringHelper::truncateWords(strip_tags($model->description), 10);
                }
            ],
            [
                'attribute' => 'technology_id',
                'value' => function ($model) use ($technologies) {
                    return $technologies[$model->technology_id];
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'technology_id',
                    $technologies,
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'weight',
            'price',
            'website',
            'build_volume',
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
