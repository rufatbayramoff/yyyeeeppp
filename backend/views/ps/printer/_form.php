<?php

use backend\widgets\FilesListWidget;
use common\components\ArrayHelper;
use common\models\EquipmentCategory;
use common\models\TsCertificationClass;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Printer */
/* @var $model_include common\models\PrinterProperties */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printer-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'firm')->textInput(['maxlength' => true])->label('Brand') ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>


    <?php echo $form->field($model, 'technology_id')->widget(
        \kartik\select2\Select2::classname(),
        [
            'data'          => yii\helpers\ArrayHelper::map(common\models\PrinterTechnology::find()->asArray()->all(), 'id', 'title'),
            'options'       => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]
    );
    ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'build_volume')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'positional_accuracy')->textInput(['type' => 'number', 'step' => 0.001]) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>


    <?= $form->field($model, 'have_unfixed_size')->checkbox() ?>
    <label class="control-label" for="printer-category">Category</label>
    <select name="Printer[equipment_category_id]" class="form-control">
        <?php
        $data = EquipmentCategory::findByPk(EquipmentCategory::PRINTERS_CATEGORY_ID)->children()->orWhere(['id'=>EquipmentCategory::PRINTERS_CATEGORY_ID])->orderBy('lft')->all();
        function printOption($model, $data)
        {
            foreach ($data as $k => $equipmentCategory) {
                $level = $equipmentCategory->depth-2;
                $space = str_repeat('&nbsp; ', $level * 2);
                $selected = 'selected="selected"';
                if ($equipmentCategory->id != $model->equipment_category_id) {
                    $selected = '';
                }
                echo sprintf("<option value='%d' %s>%s</option>", $equipmentCategory->id, $selected, $space . $equipmentCategory->title);
            }
        }
        printOption($model, $data);
        ?>
    </select>

    <?= $form->field($model, 'ts_certification_class_id')->dropDownList(TsCertificationClass::getDropDownList()); ?>

    <?php echo $form->field($model, 'moreImages[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label('Additional Images') ?>
    <div>
        <?php
        $images = $model->getImages();
        $imagesJson = [];
        if (!empty($model->images_json)) {
            $imagesJson = $model->images_json;
        }
        foreach ($images as $image) {
            if (is_string($image)) {
                continue;
            }
            echo Html::img($image->getStaticUrl(), ['width' => 100]);

            if (in_array($image->url, $imagesJson)) {
                echo Html::a('[x]', ['/ps/printer/delete-image', 'id' => $model->id, 'fileId' => $image->url], ['data-confirm' => 'Are you sure?']);
            }
        }
        ?>
    </div>
    <?php echo $form->field($model, 'printerFiles[]')->fileInput(['multiple' => true])->label('Printer Files') ?>
    <div>

        <?php
        $printerFiles = $model->getPrinterFiles();
        foreach ($printerFiles as $printerFile) {
            echo '<br /> <b>' . Html::a($printerFile->filename, $printerFile->getStaticUrl()) . '</b> ';
            echo Html::a('[x]', ['/ps/printer/delete-file', 'id' => $model->id, 'fileId' => $printerFile->url], ['data-confirm' => 'Are you sure?']);
        }
        ?>
    </div>

    <h3>Printer Properties</h3>
    <?php foreach ($properties AS $propertyId => $propertyValue) { ?>
        <div class="form-group">
            <label for="" class="control-label"><?php echo $propertyValue['title']; ?></label>
            <input type="text" id="" class="form-control"
                   name="Property[<?php echo $propertyId; ?>]"
                   value="<?php echo (isset($propertyValue['value'])) ? $propertyValue['value'] : ''; ?>">
            <div>
                <label>
                    <?php if (isset($propertyValue['is_editable'])) { ?>
                        <input type="checkbox" <?php echo ($propertyValue['is_editable'] == 1) ? 'checked' : ''; ?>
                               name="Editable[<?php echo $propertyId; ?>]" id=""> Is Editable
                    <?php } else { ?>
                        <input type="checkbox" name="Editable[<?php echo $propertyId; ?>]" id=""> Is Editable
                    <?php } ?>
                </label>
            </div>
        </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">


    $(function () {
        $('#printer-have_unfixed_size').change(function () {
            var $this = $(this);
            $("[name='Editable[1]']").prop('checked', $this.prop('checked'));
        });

        $("[name='Editable[1]']").change(function () {
            var $this = $(this);
            $('#printer-have_unfixed_size').prop('checked', $this.prop('checked'));
        });
    });

</script>