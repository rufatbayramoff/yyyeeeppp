<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Printer */
/* @var $properties common\models\PrinterProperties */

$this->title = 'Create Printer';
$this->params['breadcrumbs'][] = ['label' => 'Printers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-create">

    <?= $this->render('_form', [
        'model' => $model, 'properties' => $properties
    ]) ?>

</div>
