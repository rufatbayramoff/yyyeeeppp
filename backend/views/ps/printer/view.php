<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Printer */

$this->title                   = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            ['attribute' => 'firm', 'label' => 'Brand'],
            'title',
            'description:ntext',
            'created_at',
            'updated_at',
            'is_active:boolean',
            'technology_id',
            'weight',
            'price',
            'website',
            'build_volume',
            [
                'label' => 'Ts certification  class',
                'value' => $model->tsCertificationClass->title ?? ''
            ]

        ],
    ]) ?>

</div>
