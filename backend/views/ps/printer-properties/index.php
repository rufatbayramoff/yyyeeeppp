<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterPropertiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Properties';
$this->params['breadcrumbs'][] = $this->title;

$langs = common\models\SystemLang::find()->all();
echo $this->renderFile(Yii::getAlias('@backend/views/ps/printer-properties/_topTabs.php'));
?>
<div class="printer-properties-index">

    <p>
        <?= Html::a('Create Printer Properties', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(\common\models\PrinterProperties $model) use($langs){
                    $result = [];
                    $map = \lib\collection\CollectionDb::getMap($model->printerPropertiesIntls, 'lang_iso');
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->title : false;
                        if($val){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['ps/printer-properties-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['ps/printer-properties-intl/create']);
                        }
                    }
                    $result = Html::tag("h5", $model->title) .  implode("<br />", $result);
                    return $result;
                }
            ],
            'description',
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
