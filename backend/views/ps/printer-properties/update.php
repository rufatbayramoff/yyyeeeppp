<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterProperties */

$this->title = 'Update Printer Properties: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-properties-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
