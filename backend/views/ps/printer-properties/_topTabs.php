<?php

$links = [
    'ps/printer-properties' => 'Printer properties',
    'ps/printer-properties-intl' => 'Translates'
];
$items = [];
$curUrl = (\Yii::$app->request->getPathInfo());
foreach($links as $k=>$v){
    $item = ['label'=>$v, 'url'=> [$k]];

    if($curUrl==$k || $curUrl==$k.'/index'){
        $item['active'] = true;
    }
    $items[] = $item;

}
echo \yii\bootstrap\Tabs::widget([
    'items' => $items,
    'options' => ['style'=>'margin-bottom:20px;']
]);