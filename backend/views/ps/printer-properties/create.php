<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterProperties */

$this->title = 'Create Printer Properties';
$this->params['breadcrumbs'][] = ['label' => 'Printer Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-properties-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
