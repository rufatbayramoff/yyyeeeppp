<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserLocation */

$this->title = 'Update User Location: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-location-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
