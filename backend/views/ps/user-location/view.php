<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserLocation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-location-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'format' => 'raw',
                'label' => 'User',
                'value' => \backend\models\Backend::displayUser($model->user_id)
            ],
            [
                'label' => 'Country',
                'value' => \lib\geo\GeoNames::getCountryById($model->country_id)->title
            ],
            [
                'label' => 'Region',
                'value' => \lib\geo\GeoNames::getRegionById($model->region_id)->title
            ],
            [
                'label' => 'City',
                'value' => \lib\geo\GeoNames::getCityById($model->city_id)->title
            ],
            'address',
            'lat',
            'lon',
            'zip_code',
            'location_type',
        ],
    ]) ?>

</div>
