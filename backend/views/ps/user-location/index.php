<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserLocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Locations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-location-index">

    <p>
        <?= Html::a('Create User Location', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'User',
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function($model) {
                    $user  = \backend\models\Backend::displayUser($model->user_id);
                    return $user;
                }
            ],
            [
                'label' => 'Country',
                'attribute' => 'country_id',
                'value' => function($model) {
                    return \lib\geo\GeoNames::getCountryById($model->country_id)->title;
                }
            ],
            [
                'label' => 'Region',
                'attribute' => 'region_id',
                'value' => function($model) {
                    return \lib\geo\GeoNames::getRegionById($model->region_id)->title;
                }
            ],
            [
                'label' => 'City',
                'attribute' => 'city_id',
                'value' => function($model) {
                    return \lib\geo\GeoNames::getCityById($model->city_id)->title;
                }
            ],
            'address',
            'lat',
            'lon',
            'zip_code',
            'location_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
