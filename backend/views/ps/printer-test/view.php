<?php

use backend\widgets\FilesListWidget;
use common\models\PsPrinterTest;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model PsPrinterTest */

$this->title = $model->ps_printer_id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Printer Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ps-printer-test-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ps_printer_id',
            'type',
            'status',
            [
                'format'    => 'raw',
                'attribute' => 'Images',
                'value'     => function (PsPrinterTest $model) {
                    return FilesListWidget::widget(
                        [
                            'filesList'              => $model->getFiles(),
                            'rights'                 => [
                                FilesListWidget::ALLOW_ROTATE
                            ],
                        ]
                    );
                }
            ],
            'comment:ntext',
            'created_at',
        ],
    ]) ?>

</div>
