<?php

use backend\components\AdminAccess;
use common\models\PsPrinterTest;
use common\models\TsCertificationClass;
use frontend\components\image\ImageHtmlHelper;
use yii\helpers\Html;
use yii\grid\GridView;


/* @var $filter \backend\models\search\PsPrinterTestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Printers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-index">


    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $filter,
            'columns'      => [
                [
                    'header'    => 'User',
                    'attribute' => 'user',
                    'format'    => 'raw',
                    'value'     => function (\common\models\base\PsPrinterTest $test) {
                        return Html::a(
                            "{$test->psPrinter->ps->user->username} [{$test->psPrinter->ps->user->id}]",
                            ['/user/user/view', 'id' => $test->psPrinter->ps->user->id],
                            ['target' => '_blank']
                        );
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'format'    => 'datetime',
                    'label'     => 'First Requested At'
                ],
                'updated_at:datetime'
                ,
                [
                    'format' => 'raw',
                    'header' => 'Document',
                    'value'  => function (common\models\PsPrinterTest $test) {
                        if (!\backend\components\AdminAccess::can('psprintertest.docs')) {
                            return ' - ';
                        }

                        $user = $test->psPrinter->ps->user;

                        return $user->hasDocument()
                            ? Html::a(
                                Html::img(ImageHtmlHelper::getThumbUrlForFile($user->getLastDocument()->file, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL)),
                                $user->getLastDocument()->file->getFileUrl()
                            )
                            : '[not set]';

                    }
                ],
                [
                    'format'    => 'html',
                    'header'    => 'PS',
                    'attribute' => 'ps',
                    'value'     => function (common\models\PsPrinterTest $test) {
                        return Html::a($test->psPrinter->ps->title, ['/ps/ps/view', 'id' => $test->psPrinter->ps->id]);
                    }
                ],
                [
                    'format'    => 'html',
                    'attribute' => 'printer',
                    'header'    => 'Printer',
                    'value'     => function (common\models\PsPrinterTest $test) {
                        return Html::a($test->psPrinter->title, ['/ps/ps-printer/view', 'id' => $test->psPrinter->id]);
                    }
                ],
                [
                    'attribute' => 'certClassId',
                    'header'    => 'Cert class',
                    'format'    => 'raw',
                    'filter'    => TsCertificationClass::getDropDownList(),
                    'value'     => function (common\models\PsPrinterTest $test) {
                        $tsInternalPurchaseCertification = $test->tsInternalPurchase->tsInternalPurchaseCertification ?? null;
                        if ($tsInternalPurchaseCertification) {
                            $url = '<a href="/store/ts-internal-purchase/view?id=' . $tsInternalPurchaseCertification->tsInternalPurchase->uid . '">' .
                                H($tsInternalPurchaseCertification->tsCertificationClass->title) .
                                '</a>';
                            return $url;
                        }
                    }
                ],
                [
                    'attribute' => 'type',
                    'header'    => 'Test type',
                    'filter'    => $filter->getTypesList(),
                    'value'     => function (common\models\PsPrinterTest $test) {
                        return ucfirst($test->type);
                    }
                ],
                [
                    'format' => 'raw',
                    'header' => 'Images',
                    'value'  => function (common\models\PsPrinterTest $test) {
                        $html = '';
                        foreach ($test->getFiles() as $file) {
                            $html .= Html::a(
                                Html::img(ImageHtmlHelper::getThumbUrlForFile($file, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL),
                                    ['width' => 85, 'height' => 85]),
                                $file->getFileUrl()
                            );
                        }
                        $html = $html . Html::a('[Edit Test]', ['/ps/ps-printer-test/update', 'id' => $test->psPrinter->id]);
                        return $html;
                    }
                ],
                [
                    'format'    => 'raw',
                    'attribute' => 'status',
                    'filter'    => $filter->getStatusList(),
                    'value'     => function (PsPrinterTest $test) use ($filter) {
                        return $filter->getStatusList()[$test->status];
                    }
                ],

                [
                    'format' => 'raw',
                    'label'  => 'Actions',
                    'value'  => function (PsPrinterTest $test) {

                        $html = "";

                        if ($test->isNew() && AdminAccess::can('psprintertest.approve')) {
                            $html = Html::a('Approve', ['approve', 'psPrinterId' => $test->ps_printer_id], ['class' => 'btn btn-sm btn-success', 'data-method' => 'post']) . ' ';
                        }
                        if ($test->isNew() || $test->isChecked()) {
                            $html .= Html::a('Repeat', ['repeat', 'psPrinterId' => $test->ps_printer_id], ['class' => 'btn btn-sm btn-warning', 'data-method' => 'post']) . ' ';
                        }

                        if (($test->isNew() || $test->isChecked()) && AdminAccess::can('psprintertest.reject')) {

                            $html .= Html::button('Reject', ['class' => 'btn btn-sm btn-danger js-show-reject-form', 'type' => 'button']);

                            $html .= Html::beginForm(['reject', 'psPrinterId' => $test->ps_printer_id], 'post', ['class' => 'reject-form', 'style' => 'display : none']);
                            $html .= '<hr/>';
                            $html .= '<div style="color:red">Attention Money will be not refunded to ps. Please contact with ps before reject.</div>';
                            $html .= Html::textarea('comment', '', ['style' => 'width:200px']);
                            $html .= '<br/>';
                            $html .= Html::submitButton('Submit Reject', ['class' => 'btn btn-sm btn-danger']) . ' ';
                            $html .= Html::button('Cancel', ['class' => 'btn btn-sm btn-default js-hide-reject-form', 'type' => 'button']);

                            $html .= Html::endForm();
                        }

                        return $html;
                    }
                ],
                'decision_at:datetime',
                [
                    'format' => 'raw',
                    'value'  => function (PsPrinterTest $model) {
                        return Html::a('VIEW', ['/ps/ps-printer-test/view', 'id' => $model->ps_printer_id], ['class' => 'btn btn-ghost btn-info', 'target' => '_blank']);
                    }
                ]

            ],
        ]
    ); ?>

</div>

<script type="text/javascript">

    $(function () {

        $('.js-show-reject-form').click(function () {
            $(this)
                .hide()
                .next('.reject-form')
                .show();
        });

        $('.js-hide-reject-form').click(function () {
            $(this)
                .closest('.reject-form')
                .hide()
                .prev('.js-show-reject-form')
                .show();
        });
    });

</script>