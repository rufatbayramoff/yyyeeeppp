<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SystemRejectIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'System Reject Intls';
$this->params['breadcrumbs'][] = $this->title;

echo $this->renderFile(Yii::getAlias('@backend/views/ps/system-reject/_topTabs.php'));
?>
<div class="system-reject-intl-index">


    <p>
        <?= Html::a('Create System Reject Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model_id',
            'lang_iso',
            'title',
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
