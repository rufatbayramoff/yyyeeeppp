<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SystemRejectIntl */

$this->title = 'Create System Reject Intl';
$this->params['breadcrumbs'][] = ['label' => 'System Reject Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-reject-intl-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
