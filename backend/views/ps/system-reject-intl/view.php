<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SystemRejectIntl */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'System Reject Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-reject-intl-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'model_id',
            'lang_iso',
            'title',
            'description',
        ],
    ]) ?>

</div>
