<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SystemRejectIntl */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord){
    $systemRejectRows  = common\models\SystemReject::find()->joinWith('systemRejectIntls')->andWhere(['system_reject_intl.id'=>null])->asArray()->all();
}else{
}
$systemRejectRows  = common\models\SystemReject::find()->asArray()->all();
?>

<div class="system-reject-intl-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'model_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map($systemRejectRows, 'id', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'lang_iso')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->where(['!=', 'iso_code', 'en-US'])->asArray()->all(), 'iso_code', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
