<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterPropertiesIntl */

$this->title = 'Create Printer Properties Intl';
$this->params['breadcrumbs'][] = ['label' => 'Printer Properties Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-properties-intl-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
