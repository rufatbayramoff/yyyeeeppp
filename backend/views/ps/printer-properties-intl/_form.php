<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterPropertiesIntl */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord){
    $sourceRows  = common\models\PrinterProperties::find()
                    ->joinWith('printerPropertiesIntls')
                    ->andWhere(['printer_properties_intl.id'=>null])->asArray()->all();
    $model->load(app('request')->getQueryParams());
}else{
}
$sourceRows  = common\models\PrinterProperties::find()->asArray()->all();
?>

<div class="printer-properties-intl-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php echo $form->field($model, 'model_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map($sourceRows, 'id', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'lang_iso')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\SystemLang::find()->where(['!=', 'iso_code', 'en-US'])->asArray()->all(), 'iso_code', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
