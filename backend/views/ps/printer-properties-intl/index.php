<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterPropertiesIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Properties Intls';
$this->params['breadcrumbs'][] = $this->title;
echo $this->renderFile(Yii::getAlias('@backend/views/ps/printer-properties/_topTabs.php'));
?>
<div class="printer-properties-intl-index">


    <p>
        <?= Html::a('Create Printer Properties Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model_id',
            'lang_iso',
            'title',
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
