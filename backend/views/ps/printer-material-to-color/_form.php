<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialToColor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printer-material-to-color-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'material_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\PrinterMaterial::find()->asArray()->all(), 'id', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?php echo $form->field($model, 'color_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\PrinterColor::find()->asArray()->all(), 'id', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'multiple' => $model->isNewRecord,
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>
    <?= $form->field($model, 'is_default')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
