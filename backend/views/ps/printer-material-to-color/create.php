<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialToColor */

$this->title = 'Create Printer Material To Color';
$this->params['breadcrumbs'][] = ['label' => 'Printer Material To Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-to-color-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
