<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialToColor */

$this->title = 'Update Printer Material To Color: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Printer Material To Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-material-to-color-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
