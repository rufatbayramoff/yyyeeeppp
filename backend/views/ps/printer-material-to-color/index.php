<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterMaterialToColorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Material To Colors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-to-color-index">

    <p>
        <?= Html::a('Create Printer Material To Color', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    $materials = yii\helpers\ArrayHelper::map(common\models\PrinterMaterial::find()->asArray()->all(), 'id', 'title');
    $colors = yii\helpers\ArrayHelper::map(common\models\PrinterColor::find()->asArray()->all(), 'id', 'title');
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'material_id',
                'value' => function ($model) use ($materials) {
                    return $materials[$model->material_id];
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'material_id',
                    $materials,
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'color_id',
                'value' => function ($model) use ($colors) {
                    return $colors[$model->color_id];
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'color_id',
                    $colors,
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'is_active',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->is_active) ? '<b>Yes</b>' : '-';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'is_default',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->is_default) ? '<b>Yes</b>' : '-';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_default',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
