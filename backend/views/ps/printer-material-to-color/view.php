<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialToColor */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Printer Material To Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-to-color-view">

    <?php
    $materials = yii\helpers\ArrayHelper::map(common\models\PrinterMaterial::find()->asArray()->all(), 'id', 'title');
    $colors = yii\helpers\ArrayHelper::map(common\models\PrinterColor::find()->asArray()->all(), 'id', 'title');
    $model->material_id = $materials[$model->material_id];
    $model->color_id = $colors[$model->color_id];
    ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'material_id',
            'color_id',
            'is_active:boolean',
        ],
    ]) ?>

</div>
