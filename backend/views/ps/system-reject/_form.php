<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SystemReject */
/* @var $form yii\widgets\ActiveForm */


if ($model->isNewRecord) {
    $model->priority = 0;
}
?>

<div class="system-reject-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'group')->widget(\kartik\select2\Select2::classname(), [
        'data' => \common\models\SystemReject::getGroups(),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'priority')->textInput(['type' => 'number', 'min' => 0]) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
