<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SystemRejectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reject Templates';
$this->params['breadcrumbs'][] = $this->title;
$langs = common\models\SystemLang::find()->all();
echo $this->renderFile(Yii::getAlias('@backend/views/ps/system-reject/_topTabs.php'));
?>
<div class="system-reject-index">

    <p>
        <?= Html::a('Create Reject Template', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php $systemRejectGroups = \common\models\SystemReject::getGroups(); ?>

    <?= GridView::widget([
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'group',
                'value' => function ($model) use ($systemRejectGroups) {
                    return array_key_exists($model->group, $systemRejectGroups) ? $systemRejectGroups[$model->group] : $model->group;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'group',
                    $systemRejectGroups,
                    ['class'=>'form-control','prompt' => _t('site.common' , 'All')]),
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(\common\models\SystemReject $model) use($langs){
                    $result = [];
                    $map = \lib\collection\CollectionDb::getMap($model->systemRejectIntls, 'lang_iso');
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->title : false;
                        if($val){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['ps/system-reject-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['ps/system-reject-intl/create']);
                        }
                    }
                    $result = Html::tag("h5", $model->title) .  implode("<br />", $result);
                    return $result;
                }
            ],
            'description',
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'priority',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
