<?php

use backend\components\columns\PsColumn;
use backend\models\Backend;
use backend\widgets\OrderReviewList;
use common\components\ArrayHelper;
use common\components\FileTypesHelper;
use common\models\CompanyBlock;
use common\models\CompanyCategory;
use common\models\CompanyHistory;
use common\models\CompanyService;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\PsPrinter;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderReview;
use common\models\UserAddress;
use common\models\UserSms;
use common\modules\comments\widgets\Comments;
use common\modules\company\repositories\OrderRepository;
use frontend\components\image\ImageHtmlHelper;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Ps */
/* @var $log mixed */
/* @var $already_moderated mixed */
/* @var $moderate mixed */

$this->title = $model->title;
if ($model->isDeleted()) {
    $this->title .= '(deleted)';
}
$this->params['breadcrumbs'][] = ['label' => 'Ps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$psLogo                        = $model->getLogoUrl();
$orderRepository               = Yii::createObject(OrderRepository::class);
$preOrderCount                 = $orderRepository->preorder($model);
$allPreorder                   = $orderRepository->allPreorder($model);
$payOrderCount                 = $orderRepository->payOrder($model);
$payApproved                   = $orderRepository->payApproved($model);
$completedOrderCount           = $orderRepository->completedOrder($model);
$canceledOrderCount            = $orderRepository->canceledPs($model);
$preorderCancel                = $orderRepository->preorderCancel($model);
?>
<div class="ps-view">

    <div class="row">
        <div class="col-md-5">
            <?php if ($already_moderated) { ?>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign"></span>
                    This PS is already moderated by another moderator
                </div>
            <?php } ?>
            <?php if ($model->user->isUnconfirmed()): ?>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign"></span>
                    This Company belongs to unconfirmed user
                </div>
            <?php endif; ?>
            <p>
                <?php if ($moderate) { ?>
                    <?php if ($model->moderator_status !== common\models\Ps::MSTATUS_CHECKED): ?>
                        <?= Html::a('Approve', ['approve', 'id' => $model->id], ['class' => 'btn btn-success', 'data-method' => 'post']) ?>
                    <?php endif; ?>

                    <?php
                    $urlReject    = Url::toRoute(['ps/ps/reject', 'id' => $model->id]);
                    $rejectButton = Html::button(
                        'Reject',
                        [
                            'title'       => 'Reject reason',
                            'class'       => 'btn btn-warning btn-ajax-modal',
                            'data-pjax'   => 0,
                            'value'       => $urlReject,
                            'data-target' => '#ps_reject',
                        ]
                    );

                    $urlBanned    = Url::toRoute(['ps/ps/banned', 'id' => $model->id]);
                    $bannedButton = Html::button(
                        'Ban',
                        [
                            'title'       => 'Ban reason',
                            'class'       => 'btn btn-danger btn-ajax-modal',
                            'data-pjax'   => 0,
                            'value'       => $urlBanned,
                            'data-target' => '#ps_banned',
                        ]
                    );

                    $excludedFromPrintingButton = Html::a(
                        $model->is_excluded_from_printing ? 'Include to printing' : 'Exclude from printing',
                        ['set-exclude-from-printing', 'id' => $model->id, 'isExclude' => (int)(!$model->getIsExcludedFromPrinting())],
                        ['data-method' => 'post', 'class' => 'btn btn-warning']
                    );

                    $cncFunctionalAvaliableButtn = Html::a(
                        $model->is_cnc_allowed ? 'Deny CNC' : 'Allow CNC',
                        ['set-allow-cnc', 'id' => $model->id, 'isExclude' => (int)(!$model->is_cnc_allowed)],
                        ['data-method' => 'post', 'class' => 'btn btn-warning']
                    );

                    echo "$rejectButton $bannedButton $excludedFromPrintingButton $cncFunctionalAvaliableButtn";
                    ?>

                <?php } else { ?>
                    <?= Html::a(Yii::t('app', 'Moderate'), ['/ps/ps/view', 'id' => $model->id, 'moderate' => 1], ['class' => 'btn btn-warning']) ?>
                <?php } ?>

                <?php if ($model->isDeleted()): ?>
                    <?= Html::a(Yii::t('app', 'Restore'), ['/ps/ps/restore-ps', 'id' => $model->id],
                        [
                            'class'        => 'btn btn-danger',
                            'data-confirm' => _t('site.ps', "Are you sure you want to restored Business?"),
                            'data-method'  => 'post',
                        ]) ?>
                <?php else: ?>
                    <?= Html::a(Yii::t('app', 'Delete'), ['/ps/ps/delete-ps', 'id' => $model->id],
                        [
                            'class'        => 'btn btn-danger',
                            'data-confirm' => _t('site.ps', "Are you sure you want to delete this Business?"),
                            'data-method'  => 'delete',
                        ]) ?>
                <?php endif; ?>
                <?= Html::a(Yii::t('app', 'Send message'), ['/support/active-topics/create-topic-form', 'withUserId' => $model->user_id], ['class' => 'btn btn-info']) ?>
            </p>

            <div class="box box-solid">
                <div class="box-body">
                    <b>Moderator Comments</b>
                    <?php
                    echo Comments::widget(['model' => 'backend_ps', 'model_id' => $model->id]);
                    ?>
                </div>
            </div>
            <?php

            echo DetailView::widget(
                [
                    'model'      => $model,
                    'attributes' => [
                        [
                            'attribute' => 'id',
                            'format'    => 'raw',
                            'value'     => PsColumn::getMenu($model),
                        ],
                        [
                            'format' => 'raw',
                            'label'  => 'User',
                            'value'  => Backend::displayUser($model->user_id)
                        ],
                        'user.email',
                        'title',
                        'description:ntext',
                        'business_type',
                        'logo',
                        'phone_code',
                        'phone',
                        'phone_status',
                        'sms_gateway',
                        'currency',
                        'moderator_status',
                        'is_excluded_from_printing',
                        'moderated_at',
                        'created_at',
                        'updated_at',
                        [
                            'format' => 'raw',
                            'label'  => 'Orders in progress',
                            'value'  => '<a href=\'/store/store-order?' .
                                'StoreOrderSearch[payment_status][]=authorized&' .
                                'StoreOrderSearch[payment_status][]=paid&' .
                                'StoreOrderSearch[ps_user]=' . $model->user_id . '&' .
                                'StoreOrderSearch[order_status][]=new&' .
                                'StoreOrderSearch[order_status][]=accepted&' .
                                'StoreOrderSearch[order_status][]=printing&' .
                                'StoreOrderSearch[notTest]=1\'>' . ($model->psProgressOrdersCount->progress_orders_count ?? 0) .
                                '</a>'
                        ],
                        [
                            'format' => 'raw',
                            'label'  => 'Public url',
                            'value'  => '<a target=_blank href=\'' . param('siteUrl') . '/c/' . $model->url . '\'>' . param('siteUrl') . '/c/' . $model->url . '</a>'
                        ],
                        'website',
                        'twitter',
                        'facebook',
                        'instagram',
                        'cashback_percent',
                        [
                            'format' => 'raw',
                            'label'  => 'Rating',
                            'value'  => function (\common\models\Company $company) {
                                return $company->psCatalog->rating ?? 0;
                            }
                        ],
                        [
                            'format' => 'raw',
                            'label'  => 'Conversion %',
                            'value'  => ($preOrderCount + $payOrderCount) > 0 ? round(($completedOrderCount / ($preOrderCount + $payOrderCount)) * 100, 2) : 0
                        ],
                        [
                            'format' => 'raw',
                            'label'  => 'Orders from preorders',
                            'value'  => $preOrderCount
                        ],
                        [
                            'format' => 'raw',
                            'label'  => ' Count of orders and quotes rejects ',
                            'value'  => $canceledOrderCount + $preorderCancel
                        ],
                        [
                            'format' => 'raw',
                            'label'  => 'Paid  orders count',
                            'value'  => $payOrderCount
                        ],
                        [
                            'format' => 'raw',
                            'label'  => 'Completed orders count',
                            'value'  => $completedOrderCount
                        ],
                        [
                            'format' => 'raw',
                            'label'  => 'Count of Quotes and Instant Payments',
                            'value'  => $allPreorder + $payApproved
                        ]
                    ],
                ]
            ) ?>

            <?php $form = ActiveForm::begin(['action' => '/ps/ps/update?id=' . $model->id, 'layout' => 'inline']); ?>
            <div>
                Max Progress Orders Count
                <?= $form->field($model, 'max_progress_orders_count')->textInput(['maxlength' => true]) ?>
            </div>
            <div style="padding-top: 10px;">
                Max outgoing quotes
                <?= $form->field($model, 'max_outgoing_quotes')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>

            <div style="padding-top: 10px;">
                <?php $form = ActiveForm::begin(['action' => '/ps/ps/update-fee?id=' . $model->id, 'layout' => 'inline',]); ?>
                Personal Fee (for example 0.01)
                <?= $form->field($model, 'fee_percent')->textInput(['maxlength' => true]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <h3>Moderation history</h3>
            <?= GridView::widget(
                [
                    'dataProvider' => $log['dataProvider'],
                    'filterModel'  => $log['searchModel'],
                    'columns'      => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'started_at:datetime',
                        'finished_at:datetime',
                        'action',
                        'result:ntext'
                    ],
                ]
            ); ?>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-lg-2">
                    PS Logo: <br/>
                    <?php

                    if (empty($model->logo)):
                        echo 'no logo';
                    else:
                        echo Html::a(Html::img($psLogo, ['width' => 100]), $psLogo, ['class' => 'jspopup']);
                    endif;
                    ?>
                    <?php if ($model->coverFile): ?>

                        <div style="margin-top: 20px">
                            PS Cover: <br/>
                            <?= Html::a(Html::img(ImageHtmlHelper::getThumbUrlForFile($model->coverFile, 100, 100), ['width' => 100]), $model->coverFile->getFileUrl(), ['class' => 'jspopup']); ?>
                        </div>


                    <?php endif; ?>
                </div>
                <div class="col-lg-10">
                    PS Pictures: <br/>
                    <?php $pictures = $model->getPicturesFiles(); ?>
                    <?php
                    #  dd($pictures);
                    foreach ($pictures as $picture):
                        if (FileTypesHelper::isVideo($picture)): ?>
                            <video width="160" height="140" controls preload="metadata">
                                <source src="<?= $picture->getFileUrl(); ?>#t=0.1" type="video/mp4">
                            </video>
                        <?php else: ?>
                            <a href="<?= $picture->getFileUrl(); ?>" target="_blank"><img style="vertical-align: top"
                                                                                          src="<?= ImageHtmlHelper::getThumbUrl(
                                                                                              $picture->getFileUrl(),
                                                                                              ImageHtmlHelper::THUMB_SMALL,
                                                                                              ImageHtmlHelper::THUMB_SMALL
                                                                                          ); ?>"></a>
                        <?php endif;
                    endforeach;
                    ?>
                </div>

            </div>
            <hr/>
            <H3>Printers list</h3>

            <?php
            $printerIds = ArrayHelper::getColumn($model->companyServices, 'ps_printer_id');
            $machineIds = ArrayHelper::getColumn($model->companyServices, 'id');
            $printers   = PsPrinter::getDataProvider(['id' => $printerIds], 10, [
                'pagination' => [
                    'pageParam'     => 'printerPage',
                    'pageSizeParam' => 'printer-per-page'
                ]
            ]);
            ?>

            <?php
            echo GridView::widget(
                [
                    'dataProvider' => $printers,
                    'columns'      => [
                        'id',
                        [
                            'attribute' => 'title',
                            'format'    => 'raw',
                            'value'     => function (PsPrinter $model) {
                                return "<a href='/ps/ps-printer/view?id={$model->id}'>" . H($model->title) . "</a>";
                            }
                        ],
                        'description',
                        'printer_id',
                        'min_order_price',
                        [
                            'header' => 'location',
                            'value'  => function (PsPrinter $model) {
                                $location = $model?->companyService?->location;
                                if ($location) {
                                    return UserAddress::formatPrinterAddress($model);
                                }
                                return '';
                            }
                        ],
                        [
                            'header' => 'Is deleted',
                            'value'  => function (PsPrinter $model) {
                                return $model->companyService->is_deleted;
                            }
                        ],
                        [
                            'header' => 'Visibility',
                            'value'  => function (PsPrinter $model) {
                                return $model->companyService->visibility;
                            }
                        ],
                        [
                            'header' => 'Moderator status',
                            'value'  => function (PsPrinter $model) {
                                return $model->companyService->moderator_status;
                            }
                        ],
                    ],
                ]
            ); ?>
            <h3>Services</h3>

            <?php
            $serviceDataProvider = CompanyService::getDataProvider(
                [
                    'ps_id' => $model->id,
                    'type'  => [CompanyService::TYPE_CNC, CompanyService::TYPE_SERVICE, CompanyService::TYPE_DESIGNER, CompanyService::TYPE_CUTTING]
                ],
                20,
                [
                    'pagination' => [
                        'pageParam'     => 'companyServicePage',
                        'pageSizeParam' => 'companyservice-per-page'
                    ]
                ]
            );

            echo GridView::widget(
                [
                    'dataProvider' => $serviceDataProvider,
                    'rowOptions'   => function ($model, $index, $widget, $grid) {
                        if ($model->is_deleted) {
                            return [
                                'style' => 'background: #dfdfdf; color: #8a8a8a!important;'
                            ];
                        }
                        if ($model->moderator_status == CompanyService::MODERATOR_STATUS_PENDING) {
                            return [
                                'style' => 'background: #abdfff'
                            ];
                        }
                    },
                    'columns'      => [
                        [
                            'attribute' => 'id',
                            'format'    => 'raw',
                            'value'     => function ($model) {
                                $url        = Url::toRoute(['/company/company-service/view', 'id' => $model->id]);
                                $viewButton = Html::a($model->id, $url, ['class' => 'btn btn-primary', 'data-pjax' => 0]);
                                return $viewButton;
                            }
                        ],
                        [
                            'attribute' => 'type',
                        ],
                        [
                            'attribute' => 'category_id',
                            'value'     => function (CompanyService $psMachine) {
                                if (!$psMachine->category_id) {
                                    return null;
                                }
                                return $psMachine->category->title;
                            },
                        ],
                        [
                            'attribute' => 'title',
                            'value'     => function (CompanyService $psMachine) {
                                return $psMachine->getTitleLabel();
                            }
                        ],
                        [
                            'attribute' => 'moderator_status',
                        ],
                        [
                            'attribute' => 'visibility'
                        ],
                        [
                            'attribute' => 'is_deleted',
                            'value'     => function ($model) {
                                return ($model->is_deleted) ? 'Yes' : 'No';
                            }
                        ],
                        'updated_at'
                    ],
                ]
            ); ?>


            <h3>Verify SMS</h3>
            <?=
            yii\grid\GridView::widget(
                [
                    'sorter'       => false,
                    'showFooter'   => false,
                    'layout'       => '{items}',
                    'dataProvider' => new ActiveDataProvider(
                        [
                            'query'      => UserSms::find()->where(['user_id' => $model->user_id, 'type' => UserSms::TYPE_CONFIRM]),
                            'pagination' => [
                                'pageSize' => 10000,
                            ]
                        ]
                    ),
                    'columns'      => [
                        'created_at',
                        'phone',
                        'message',
                        'status',
                        'gateway',
                    ],
                ]
            );

            ?>

            <h3>Orders History</h3>

            <?= GridView::widget(
                [
                    'dataProvider' => StoreOrderAttemp::getDataProvider(
                        ['machine_id' => $machineIds],
                        20,
                        [
                            'sort'       => ['defaultOrder' => ['id' => SORT_DESC]],
                            'pagination' => [
                                'pageParam'     => 'orderPage',
                                'pageSizeParam' => 'order-per-page'
                            ]
                        ]
                    ),
                    'columns'      => [
                        'id',
                        'order_id',
                        'machine_id',
                        'status',
                        'created_at:datetime',
                        'is_offer:boolean',
                        'cancelled_at:datetime',
                        [
                            'attribute' => 'cancel_initiator',
                            'value'     => function ($model) {
                                return ChangeOrderStatusEvent::getInitiatorLabel($model->cancel_initiator);
                            },
                        ]
                    ],
                ]
            ); ?>

            <h3>Company History</h3>
            <?= GridView::widget(
                [
                    'dataProvider' => CompanyHistory::getDataProvider(
                        ['company_id' => $model->id],
                        10,
                        [
                            'sort'       => ['defaultOrder' => ['id' => SORT_DESC]],
                            'pagination' => [
                                'pageParam'     => 'companyPage',
                                'pageSizeParam' => 'company-per-page'
                            ]
                        ]
                    ),
                    'columns'      => [
                        'id',
                        'created_at:datetime',
                        'action_id',
                        'comment',
                        'user.username',
                    ],
                ]
            ); ?>
            <h3>Company blocks</h3>
            <?= GridView::widget(
                [
                    'dataProvider' => CompanyBlock::getDataProvider(['company_id' => $model->id], 10, ['sort' => ['defaultOrder' => ['id' => SORT_DESC]]]),
                    'columns'      => [
                        'id',
                        [
                            'attribute' => 'title',
                            'format'    => 'raw',
                            'value'     => function ($model) {
                                return Html::a($model->title, ['company/company-block/view', 'id' => $model->id]);
                            }
                        ],
                        'created_at',
                        'is_visible:boolean',
                        // 'videos',
                    ],
                ]
            ); ?>
            <h3>Company categories</h3>
            <?= GridView::widget(
                [
                    'dataProvider' => CompanyCategory::getDataProvider(['company_id' => $model->id], 10, ['sort' => ['defaultOrder' => ['id' => SORT_DESC]]]),
                    'columns'      => [
                        [
                            'attribute' => 'title',
                            'value'     => function (CompanyCategory $model) {
                                return $model->companyServiceCategory->title;
                            },
                        ]
                    ],
                ]
            ); ?>

        </div>
    </div>

</div>


<div class="box box-solid box-default">
    <div class="box-header">Review
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <?= OrderReviewList::widget(['query' => StoreOrderReview::find()->forPs($model)]); ?>
    </div>
</div>