<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Ps */
/* @var $form yii\widgets\ActiveForm */
/* @var $bannedForm backend\models\ps\PsBannedForm */
?>

<div class="ps-banned-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php
    echo $form->field($bannedForm, 'reasonId')->widget(kartik\widgets\Select2::classname(), [
        'data' => $bannedForm->getSuggestList(),
        'options' => ['placeholder' => 'Select', 'style' => 'width:100%'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    echo $form->field($bannedForm, 'reasonDescription')->textarea(['maxlength' => true]);
    ?>
    <div class="form-group">
        <?= Html::submitButton('Ban', ['class' =>   'btn btn-danger ajax-submit']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
