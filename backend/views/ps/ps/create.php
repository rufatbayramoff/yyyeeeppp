<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Ps */

$this->title = 'Create Company';
$this->params['breadcrumbs'][] = ['label' => 'Ps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
