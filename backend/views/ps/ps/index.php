<?php

use lib\money\Currency;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-index">

    <?php $form = ActiveForm::begin(
        [
            'action' => ['index'],
            'method' => 'get',
            'layout' => 'inline'
        ]
    ); ?>
    <div class="row" style="padding-bottom: 20px;">
            <div class="col-md-4">
                <b>Currency: </b>
                <?= $form->field($searchModel, 'currency')->dropDownList([Currency::USD => 'USD', Currency::EUR => 'EUR']) ?>
                <input type="submit" value="search" class="btn btn-primary"/>
            </div>
            <div class="col-md-8 text-right">
                <a class="btn btn-default" href="/system/export?tableName=ps"><span class="fa fa-file-excel-o text-success"></span> Export</a>
            </div>
    </div>
    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'rowOptions'   => function ($model, $index, $widget, $grid) {
            $res = true;
            if ($model->country) {
                $res = \frontend\models\site\DenyCountry::checkCountry($model->country->iso_code, false);
            }
            if (!$res) {
                return [
                    'style' => 'background: #ffdfaf;'
                ];
            }
        },
        'columns'      => [
            [
                'attribute' => 'Moderate',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $url        = \yii\helpers\Url::toRoute(['/ps/ps/view', 'id' => $model->id]);
                    $viewButton = Html::a('View', $url, ['class' => 'btn btn-primary', 'data-pjax' => 0, 'target' => '_blank']);
                    $html       = $viewButton;

                    $url            = \yii\helpers\Url::toRoute(['/ps/ps/view', 'id' => $model->id, 'moderate' => 1]);
                    $moderateButton = Html::a('Moderate', $url, ['class' => 'btn btn-warning', 'data-pjax' => 0, 'target' => '_blank']);
                    if (\backend\components\AdminAccess::can('ps.moderate')) {
                        $html .= ' '.$moderateButton;
                    }

                    return $html;
                }
            ],

            'id',
            [
                'label'     => 'User',
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $user = \backend\components\columns\PsColumn::getMenu($model);
                    return $user;
                }
            ],
            'title',
            [
                'label'     => 'Is excluded',
                'attribute' => 'is_excluded_from_printing',
                'value'     => function ($model) {
                    return $model->is_excluded_from_printing ? 'Yes' : 'No';
                },
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'is_excluded_from_printing',
                    [0 => 'No', 1 => 'Yes'],
                    ['class' => 'form-control', 'prompt' => 'All']),
            ],
            [
                'label'     => 'email',
                'attribute' => 'user_email',
                'value'     => function ($model) {
                    return $model->user->email;
                }
            ],
            'phone',
            [
                'label'     => 'Status',
                'attribute' => 'moderator_status',
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'moderator_status',
                    $searchModel->getStatusesOptions(),
                    ['class' => 'form-control', 'prompt' => 'All']),
            ],
            'created_at',
            'updated_at',
            ['class' => \backend\components\columns\UserLocationColumn::class, 'attribute' => 'location'],
        ],
    ]); ?>

</div>
