<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterColorIntl */

$this->title = 'Update Printer Color Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Color Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-color-intl-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
