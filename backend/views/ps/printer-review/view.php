<?php

use common\models\CompanyService;
use common\models\ModerLog;
use common\models\PrinterReview;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterReview */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Printer Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-review-view">

    <?php
    if ($model->status != PrinterReview::STATUS_REJECTED) {
        echo Html::button(
            'Reject',
            [
                'title'       => 'Reject reason',
                'class'       => 'btn btn-warning btn-ajax-modal',
                'data-pjax'   => 0,
                'value'       => \yii\helpers\Url::toRoute(['/ps/printer-review/reject', 'id' => $model->id]),
                'data-target' => '#model_storereject'
            ]
        );
    }
    echo ' &nbsp; ';
    if ($model->status != PrinterReview::STATUS_PUBLISHED) {
        echo Html::a(
            'Approve',
            ['/ps/printer-review/publish', 'id' => $model->id],
            [
                'title' => 'Approved',
                'class' => 'btn btn-success m-r20',
            ]
        );
    }
    ?></div>
<?php
if (in_array($model->status, [PrinterReview::STATUS_REJECTED])) {
    echo '<br />
        <div class="m-t20 m-b10"><p class="callout callout-danger">REJECTED: ' . ModerLog::getLastRejectModerLog(ModerLog::TYPE_PRINT_REVIEW, $model->id) . '</p></div>';
}
?>

<div class="row">
    <div class="col-lg-7">
        <?= DetailView::widget(
            [
                'model'      => $model,
                'attributes' => [
                    'id',
                    'printer_id',
                    [
                        'attribute' => 'id',
                        'format'    => 'raw',
                        'value'     => \backend\components\columns\UserColumn::getMenu($model->user),
                    ],
                    'created_at:datetime',
                    'status',
                    'comments:ntext',
                ],
            ]
        ) ?>

    </div>
    <div class="col-lg-3">
        <?= DetailView::widget(
            [
                'model'      => $model,
                'attributes' => [
                    'print_quality',
                    'ease_of_use',
                    'failure_rate',
                    'software',
                    'community',
                    'build_quality',
                    'reliability',
                    'running_expenses',
                    'customer_service',
                    'value',
                ]
            ]
        ); ?>
    </div>

</div>
