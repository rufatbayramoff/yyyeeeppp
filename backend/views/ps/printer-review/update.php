<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterReview */

$this->title = 'Update Printer Review: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Printer Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-review-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
