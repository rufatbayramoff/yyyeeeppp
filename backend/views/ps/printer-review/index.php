<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-review-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [

            'id',
            [
                'attribute' => 'status',
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    \common\models\PrinterReview::getModeratorStatusLabels(),
                    ['class' => 'form-control', 'prompt' => 'All']
                ),
            ],
            [
                'attribute' => 'printer_id',
                'value' => function($model){
                    return $model->printer->title;
                },
            ],

            [
                'class' => \backend\components\columns\UserColumn::class,
                'attribute' =>  'user',
            ],
            'created_at:datetime',
           /* [
               'attribute' => 'reviews',
                'format' => 'raw',
               'value' => function (\common\models\PrinterReview $model) {
                   $data = $model->getReviewData();
                   unset($data['comments']);
                   $result = [];
                   foreach ($data as $k => $v) {
                       $result[] = "$k : $v";
                   }
                   return implode("<br />", $result);
               }
            ],*/
            // 'failure_rate',
            // 'software',
            // 'community',
            // 'build_quality',
            // 'reliability',
            // 'running_expenses',
            // 'customer_service',
            //'comments:ntext',
            [
                'attribute' => 'Moderate',
                'format' => 'raw',
                'value' => function ($model) {
                    $url = \yii\helpers\Url::toRoute(['/ps/printer-review/view', 'id' => $model->id, 'moderate' => 1]);
                    $moderateButton = Html::a('Moderate', $url, ['class'=>'btn btn-warning', 'data-pjax'=>0, ]);

                    return $moderateButton;
                }
            ],
        ],
    ]); ?>
</div>
