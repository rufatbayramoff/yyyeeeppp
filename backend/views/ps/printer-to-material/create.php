<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterToMaterial */

$this->title = 'Create Printer To Material';
$this->params['breadcrumbs'][] = ['label' => 'Printer To Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-to-material-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
