<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterToMaterial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="printer-to-material-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'printer_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\Printer::find()->asArray()->all(), 'id', 'title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?php echo $form->field($model, 'material_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(common\models\PrinterMaterial::find()->asArray()->all(), 'id', 'filament_title' ),
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'multiple' => $model->isNewRecord,
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
