<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterToMaterial */

$this->title = 'Update Printer To Material: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Printer To Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-to-material-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
