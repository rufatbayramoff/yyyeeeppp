<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterToMaterial */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Printer To Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-to-material-view">

    <?php
    $printers = yii\helpers\ArrayHelper::map(common\models\Printer::find()->asArray()->all(), 'id', 'title');
    $materials = yii\helpers\ArrayHelper::map(common\models\PrinterMaterial::find()->asArray()->all(), 'id', 'title');
    $model->printer_id = $printers[$model->printer_id];
    $model->material_id = $materials[$model->material_id];
    ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'printer_id',
            'material_id',
            'is_active:boolean',
        ],
    ]) ?>

</div>
