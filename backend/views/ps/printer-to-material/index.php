<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterToMaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer To Materials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-to-material-index">

    <p>
        <?= Html::a('Create Printer To Material', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    $printers = yii\helpers\ArrayHelper::map(common\models\Printer::find()->asArray()->all(), 'id', 'title');
    $materials = yii\helpers\ArrayHelper::map(common\models\PrinterMaterial::find()->asArray()->all(), 'id', 'filament_title');
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'printer_id',
                'value' => function ($model) use ($printers) {
                    return $printers[$model->printer_id];
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'printer_id',
                    $printers,
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'material_id',
                'value' => function ($model) use ($materials) {
                    return !empty($materials[$model->material_id]) ? $materials[$model->material_id] : $model->material_id;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'material_id',
                    $materials,
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
