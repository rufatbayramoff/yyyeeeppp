<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterColor */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-color-view">

    <h1><?= \H($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'created_at',
            'updated_at',
            'is_active',
            'rgb',
            'ambient',
            'cmyk',
            'image',
            'render_color',
        ],
    ]) ?>

</div>
