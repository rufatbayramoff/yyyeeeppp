<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterColor */

$this->title = 'Create Printer Color';
$this->params['breadcrumbs'][] = ['label' => 'Printer Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-color-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
