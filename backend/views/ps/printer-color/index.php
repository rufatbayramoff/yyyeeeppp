<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterColorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Colors';
$this->params['breadcrumbs'][] = $this->title;
$langs = common\models\SystemLang::find()->all();
echo $this->renderFile(Yii::getAlias('@backend/views/ps/printer-color/_topTabs.php'));
?>
<div class="printer-color-index"> 
    <p>
        <?= Html::a('Create Printer Color', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'caption' => new \backend\components\GridViewDataExporter($searchModel),

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(\common\models\PrinterColor $model) use($langs){
                    $result = [];
                    $map = \lib\collection\CollectionDb::getMap($model->printerColorIntls, 'lang_iso');
                    foreach($langs as $k=>$v){
                        if($v->iso_code=='en-US') continue;
                        $val = isset($map[$v->iso_code]) ? $map[$v->iso_code]->title : false;
                        if($val){
                            $result[] = Html::a($v->iso_code . " : " . $val, ['ps/printer-color-intl/update', 'id'=>$map[$v->iso_code]->id]);
                        }else{
                            $result[] = Html::a($v->iso_code . " : " . 'Empty', ['ps/printer-color-intl/create']);
                        }
                    }
                    $result = Html::tag("h5", $model->title) .  implode("<br />", $result);
                    return $result;
                }
            ],
            [
                'attribute' => 'rgb',
                'format' => 'raw',
                'value' => function($model){
                    return "<span style='background: rgb(".$model->rgb.") '>" . $model->rgb . "</span>";
                }
            ],
            'ambient',
            'cmyk',
            'image',
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? 'Yes' : 'No';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_active',
                    $searchModel->getActiveInactive(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
'render_color',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
