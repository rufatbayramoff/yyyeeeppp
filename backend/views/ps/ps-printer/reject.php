<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinter */
/* @var $form yii\widgets\ActiveForm */
/* @var $rejectForm backend\models\ps\PsPrinterRejectForm */
?>

<div class="ps-printer-reject-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php
    echo $form->field($rejectForm, 'reasonId')->widget(kartik\widgets\Select2::classname(), [
        'data' => $rejectForm->getSuggestList(),
        'options' => ['placeholder' => 'Select', 'style' => 'width:100%'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    echo $form->field($rejectForm, 'reasonDescription')->textarea(['maxlength' => true]);
    ?>
    <div class="form-group">
        <?= Html::submitButton('Reject', ['class' =>   'btn btn-warning ajax-submit']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
