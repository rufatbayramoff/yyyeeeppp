<?php

use backend\models\search\PsPrinterSearch;
use common\models\PsPrinter;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsPrinterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printers Moderation';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-6" style="font-size:16px;">
        Filter:
        <a href="/ps/ps-printer?PsPrinterSearch[is_deleted]=0" >All Printers</a> |
        <a href="/ps/ps-printer?PsPrinterSearch[is_deleted]=0&PsPrinterSearch[user_status]=active&PsPrinterSearch[moderator_status]=active" >All Active Printers</a> |
        <a href="/ps/ps-printer?PsPrinterSearch[moderator_status]=waiting" >Waiting Moderation</a> |
        <a href="/ps/ps-printer?PsPrinterSearch[moderator_status]=inactive" >Inactive printers</a> |
        <a href="/ps/ps-printer?PsPrinterSearch[is_deleted]=1" >Deleted printers</a> |
    </div>
    <div class="col-lg-6">
        <div class="pull-right">
            <a href="/ps/ps-printer/download-report" class="btn btn-default btn-sm" target="_blank"><i class="fa fa-download"></i> Download Printers</a>
        </div>
    </div>
</div>
<div class="ps-printer-index">
    <?= GridView::widget([
//        'caption' => new \backend\components\GridViewDataExporter($searchModel),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'header' => 'User',
                'attribute' => 'ps.user',
                'format' => 'raw',
                'filter' => Html::activeTextInput($searchModel, 'userId', ['class' => 'form-control']),
                'class' => \backend\components\columns\UserColumn::class,
            ],
            [
                'header' => 'Company',
                'attribute' => 'ps_id',
                'format' => 'raw',
                'class' => \backend\components\columns\PsColumn::class,
            ],
            [
                'attribute'=>'country',
                'value'=>'psMachine.location.country.iso_code',
            ],
            'title',
            [
                'attribute' => 'description',
                'value' => function ($model) {
                    return \yii\helpers\StringHelper::truncateWords(strip_tags($model->description), 10);
                }
            ],
            [
                'attribute' => 'printer_title',
                'value' => 'printer.title'
            ],
            [
                'attribute' => 'companyService.visibility',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'visibility',
                    [
                            \common\models\CompanyService::VISIBILITY_EVERYWHERE => 'yes',
                            \common\models\CompanyService::VISIBILITY_NOWHERE => 'no',
                    ],
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            [
                'attribute' => 'companyService.moderator_status',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'moderator_status',
                    \common\models\CompanyService::getModeratorStatusLabels(),
                    ['class'=>'form-control','prompt' => 'All']),
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'header' => 'Test Order',
                'format' => 'raw',
                'value' => function (PsPrinter $printer)
                {
                    if ($printer->is_test_order_resolved) {
                        $testOrder = $printer->testOrder;
                        $a = $testOrder
                            ? ' '.Html::a("[{$printer->testOrder->id}]", ['/store/store-order/view', 'id' => $printer->testOrder->id], ['target' => '_blank'])
                            : '';

                        return 'Resolved'.$a;
                    }

                    if (!$printer->is_test_order_resolved && $printer->testOrder) {
                        return 'In process '.Html::a("[{$printer->testOrder->id}]", ['/store/store-order/view', 'id' => $printer->testOrder->id], ['target' => '_blank']);
                    }

                    return 'No';
                },
                'attribute' => 'testOrderStatus',
                'filter' => PsPrinterSearch::getTestOrderFilter()
            ],
            [
                'attribute' => 'Moderate',
                'format' => 'raw',
                'value' => function (PsPrinter $model) {

                    $buttons = [];

                    $url = \yii\helpers\Url::toRoute(['/ps/ps-printer/view', 'id' => $model->id]);
                    $buttons[] = Html::a('View', $url, ['class'=>'btn btn-primary', 'data-pjax'=>0, 'target'=>'_blank']);

                    if(\backend\components\AdminAccess::can('psprinter.moderate') && !$model->is_deleted){
                        $url = \yii\helpers\Url::toRoute(['/ps/ps-printer/view', 'id' => $model->id, 'moderate' => 1, 'target'=>'_blank']);
                        $moderateButton = Html::a('Moderate', $url, ['class'=>'btn btn-warning', 'data-pjax'=>0, 'target'=>'_blank']);
                        $buttons[] = $moderateButton;
                    }
                    return implode('&nbsp;&nbsp;', $buttons);
                }
            ],
            [
                'attribute' => 'is_deleted','filter' => [0=>'No',1=>'Yes'],
                'value' => function (PsPrinter $psPrinter) {
                    return ($psPrinter->companyService->is_deleted) ? 'Yes' : '';
                },
            ],
        ],
    ]); ?>

</div>
