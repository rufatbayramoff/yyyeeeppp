<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinter */

$this->title = 'Update Ps Printer: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ps Printers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-printer-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
