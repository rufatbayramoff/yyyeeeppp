<?php

use common\models\base\PsPrinterHistory;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\PsPrinter;
use frontend\models\ps\PsFacade;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinter */
/* @var $orders common\models\StoreOrder[] */
/* @var $moderate bool */
/* @var $log array */
/* @var $modelHistory array */

$this->title                   = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ps Printers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$testOrderFn = function (PsPrinter $printer) {
    if ($printer->is_test_order_resolved) {
        if (!$printer->testOrder) {
            return 'Resolved';
        }
        return 'Resolved : ' . Html::a("[{$printer->testOrder->id}]", ['/store/store-order/view', 'id' => $printer->testOrder->id], ['target' => '_blank']);
    }

    if (!$printer->testOrder) {
        return Html::a(Yii::t('app', 'Send test order'), ['send-test-order', 'id' => $printer->id], ['class' => 'btn btn-warning']);
    }

    return 'In process : ' . Html::a("[{$printer->testOrder->id}]", ['/store/store-order/view', 'id' => $printer->testOrder->id], ['target' => '_blank']);
}


?>
<div class="ps-printer-view">

    <div class="row">
        <div class="col-md-6">


            <?php if ($model->companyService->is_location_change && PsFacade::isPrinterHaveUnclosedOrders($model)): ?>

                <div class="alert alert-danger" role="alert">
                    This printer has open orders address of the printer was changed. <br/>Do you approve to publish printer with new address?
                </div>

            <?php endif ?>

            <p>
                <?php if ($model->companyService->visibility === \common\models\CompanyService::VISIBILITY_NOWHERE): ?>
                    <?= Html::a('ACTIVATE', ['visibility', 'status' => \common\models\CompanyService::VISIBILITY_EVERYWHERE, 'id' => $model->id], ['class' => 'btn btn-success', 'data-method' => 'post']) ?>
                <?php else: ?>
                    <?= Html::a('Inactivate', ['visibility', 'status' => \common\models\CompanyService::VISIBILITY_NOWHERE, 'id' => $model->id], ['class' => 'btn btn-warning', 'data-method' => 'post']) ?>
                <?php endif; ?>

                <?php if ($model->companyService->moderator_status != \common\models\CompanyService::MODERATOR_STATUS_APPROVED): ?>
                    <?= Html::a('Approve', ['approve', 'id' => $model->id], ['class' => 'btn btn-success', 'data-method' => 'post']) ?>
                <?php endif; ?>

                <?php
                $urlReject    = \yii\helpers\Url::toRoute(['ps/ps-printer/reject', 'id' => $model->id]);
                $rejectButton = Html::button('Reject',
                    [
                        'title'       => 'Reject reason',
                        'class'       => 'btn btn-warning btn-ajax-modal',
                        'data-pjax'   => 0,
                        'value'       => $urlReject,
                        'data-target' => '#ps_printer_reject'
                    ]
                );

                echo $rejectButton;
                ?>
            </p>


            <p><?= Html::a(Yii::t('app', 'Send message'), ['/support/active-topics/create-topic-form', 'withUserId' => $model->ps->user_id], ['class' => 'btn btn-warning']) ?></p>


            <?php

            $address = '';
            try {
                $address = [
                    $model->companyService->location->country->title,
                    $model->companyService->location->region ? $model->companyService->location->region->title : '',
                    $model->companyService->location->city->title,
                    $model->companyService->location->zip_code,
                    $model->companyService->location->lat,
                    $model->companyService->location->lon,
                ];
                $address = implode(", ", array_filter($address));
            } catch (\Exception $e) {
                //Yii::error($e);
                $address = $e->getMessage();
            }
            $lat = $model?->companyService?->location?->lat;
            $lon = $model?->companyService?->location?->lon;
            if ($lat && $lon) {
                $address .= " " . "<a target='_blank' href='https://maps.google.com/maps?q=loc:{$model->companyService->location->lat},{$model->companyService->location->lon}&z=11'>On map</a>";
            }
            ?>

            <?= DetailView::widget([
                'model'      => $model,
                'attributes' => [
                    [
                        'format' => 'raw',
                        'label'  => 'PS machine id',
                        'value'  => $model->companyService->id
                    ],
                    [
                        'format' => 'raw',
                        'label'  => 'PS',
                        'value'  => "<a href='/ps/ps/view?id={$model->ps->id}'>" . \H($model->ps->title) . "</a>"
                    ],
                    [
                        'format' => 'raw',
                        'label'  => 'Title',
                        'value'  => "<a href='/ps/printer/view?id={$model->printer_id}'>" . \H($model->title) . ' (' . \H($model->printer->title) . ')</a>'
                    ],
                    'description',
                    //'image',
                    'price_per_hour:currency',
                    //'currency_iso',
                    'price_per_volume:currency',
                    [
                        'format' => 'raw',
                        'label'  => 'Location',
                        'value'  => $address
                    ],
                    [
                        'label' => 'Enabled',
                        'value' => $model->companyService->visibility == CompanyService::VISIBILITY_EVERYWHERE ? 'Yes' : 'No',
                    ],
                    'is_online:boolean',
                    'visibility'       => 'companyService.visibility',
                    'moderator_status' => 'companyService.moderator_status',
                    [
                        'attribute' => 'Certification',
                        'format'    => 'html',
                        'value'     => ($model->psPrinterTest ? '<a href="/ps/ps-printer-test?printer=' . $model->id . '">' . $model->psPrinterTest->status . '</a>'
                                : $model->companyService->certification) . ' Expire: ' . $model->companyService->certification_expire
                    ],

                    [
                        'label'  => 'Test order',
                        'format' => 'raw',
                        'value'  => $testOrderFn($model),
                    ],
                    [
                        'attribute' => 'build_volume',
                        'value'     => implode(" x ", $model->getBuildVolume()) . ' mm.'
                    ],
                    [
                        'attribute' => 'min_order_price',
                        'value'     => ($model->min_order_price ?: 0) . ' ' . $model->companyService->company->currency,
                    ],
                    'created_at',
                    'updated_at'
                ],
            ]) ?>

            <h3>Moderation history</h3>
            <?= yii\grid\GridView::widget([
                'dataProvider' => $log['dataProvider'],
                'filterModel'  => $log['searchModel'],
                'columns'      => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'started_at:datetime',
                    'finished_at:datetime',
                    'action',
                    [
                        'attribute' => 'result',
                        'format'    => 'html',
                        'value'     => function (\common\models\ModerLog $data) {
                            $result = \yii\helpers\Json::decode($data->result);
                            $texts  = [];

                            if (isset($result['reason']) && $result['reason']) {
                                /** @var \common\models\SystemReject $reason */
                                $reason  = \common\models\SystemReject::findByPk($result['reason']);
                                $texts[] = "Reason : {$reason->title}";
                            }

                            if (isset($result['descr'])) {
                                $texts[] = "Comment : {$result['descr']}";
                            }

                            return implode('<br/>', $texts);
                        }
                    ]
                ],
            ]); ?>

            <h3>Printer history</h3>
            <?= yii\grid\GridView::widget([
                'dataProvider' => $modelHistory['dataProvider'],
                'filterModel'  => $modelHistory['searchModel'],
                'columns'      => [
                    'id',
                    'user.username',
                    'created_at:datetime',
                    'action_id',
                    'comment:ntext',
                    [
                        'format' => 'html',
                        'value'  => function (PsPrinterHistory $historyItem) {
                            if ($historyItem->data) {
                                return '<pre>' . print_r(\yii\helpers\Json::decode($historyItem->data), true) . '</pre>';
                            }
                            return null;
                        }
                    ]
                ],
            ]); ?>

            <h3>Orders tokens</h3>
            <?php if (isset($orders[0])) { ?>
                <table class="table table-striped table-bordered detail-view">
                    <?php foreach ($orders as $oneOrder) { ?>
                        <tr>
                            <td>
                                Order <?php echo $oneOrder->id; ?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            <?php } ?>
        </div>


        <div class="col-md-6">
            <h3>Materials and colors</h3>
            <table class="table table-striped table-bordered detail-view">
                <?php foreach ($model->materials as $psMaterial): ?>
                    <tr>
                        <td width="25%">
                            <?= "{$psMaterial->material->filament_title} ({$psMaterial->material->title})" ?>
                            <?php if ($psMaterial->material->group): ?>
                                <?= $psMaterial->material->group->title; ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php foreach ($psMaterial->colors as $psColor): ?>
                                <div class="row oneColorRow">
                                    <div class="col-lg-4" style="padding-left: 0;">
                                        <div class="color-one-block__color-title"><?= $psColor->id; ?>. <?= $psColor->color->title; ?></div>
                                    </div>
                                    <div class="col-lg-8" style="padding-left: 0;">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="padding: 0;border: 0;">
                                                <div class="color-one-block">
                                                    <div class="added-colors-div"></div>
                                                    <div style="background-color: rgb(<?= $psColor->color->rgb; ?>);padding: 15px;" class="form-control"></div>
                                                </div>
                                            </span>
                                            <div class="form-control" style="padding-left: 6px;padding-right: 6px;">
                                                <?= H($model->company->currency) ?> <?= H($psColor->price) ?> per <?= H($psColor->price_measure) ?> /
                                                <?= displayAsMoney($psColor->getPriceUsdPerMl()); ?> per ml

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <h3>Delivery Types</h3>
            <table class="table table-bordered">
                <?php foreach ($model->companyService->deliveryTypess as $psDeliveryType): ?>
                    <tr>
                        <td>
                            <div><b><?= H($psDeliveryType->deliveryType->title); ?></b></div>
                            <?=
                            DetailView::widget([
                                'model'      => $psDeliveryType,
                                'attributes' => [
                                    'carrier',
                                    [
                                        'label'   => 'Shipping price',
                                        'value'   => $psDeliveryType->carrier_price . ' ' . $model->companyService->company->currency,
                                        'visible' => $psDeliveryType->carrier != DeliveryType::CARRIER_TS,
                                    ],
                                    [
                                        'label'   => 'Free Delivery from',
                                        'value'   => $psDeliveryType->free_delivery . ' ' . $model->companyService->company->currency,
                                        'visible' => $psDeliveryType->free_delivery > 0,
                                    ],
                                    [
                                        'label'   => 'Package Fee',
                                        'value'   => $psDeliveryType->packing_price . ' ' . $model->companyService->company->currency,
                                        'visible' => (bool)$psDeliveryType->packing_price && $psDeliveryType->carrier === DeliveryType::CARRIER_TS
                                    ],
                                    [
                                        'label'   => 'Comment',
                                        'value'   => $psDeliveryType->comment,
                                        'visible' => (bool)$psDeliveryType->comment
                                    ],
                                ],
                            ]);

                            ?>
                            <?php if ($psDeliveryType->psMachineDeliveryCountries): ?>
                                <?php foreach ($psDeliveryType->psMachineDeliveryCountries as $country): ?>
                                    <?php echo
                                    DetailView::widget([
                                        'model'      => $country,
                                        'attributes' => [
                                            [
                                                'label'     => 'Country',
                                                'attribute' => 'geoCountry.title',
                                            ],
                                            'price',
                                        ],
                                    ]);
                                    ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach ?>

            </table>
        </div>
    </div>

</div>
