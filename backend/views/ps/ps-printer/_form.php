<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PsPrinter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ps-printer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'ps_id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'printer_id')->textInput() ?>

    <?= $form->field($model, 'price_per_hour')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_iso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_per_volume')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location_id')->textInput() ?>

    <?= $form->field($model, 'user_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_online')->checkbox() ?>

    <?= $form->field($model, 'moderator_status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
