<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 02.08.17
 * Time: 16:07
 */

use backend\assets\AngularAppAsset;
use backend\components\AdminAccess;
use common\components\ArrayHelper;
use frontend\assets\JsonEditorAssetJsTemplate;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyService */


if (isset($companyService)) {
    $psMachine = $companyService;
}

$psCncMachineRepository = Yii::createObject(\common\modules\cnc\repositories\PsCncMachineRepository::class);
$psCncMachine = $psCncMachineRepository->getByPsMachine($psMachine);
$canEdit = AdminAccess::can('psmachine.moderate');
?>

<?= $commonDetails ?>
<b>Cnc code:</b><br>
<?= $psCncMachine->code ?><br><br>
<?= $commonForm ?>
<?php
$json = Yii::$app->getModule('cnc')->psCncJsonGenerator->generateJsonServiceInfo($psCncMachine->psCnc);
$cncSchema = Yii::$app->getModule('cnc')->schemaManager->getInputFormSchema([
    '__CURRENCY__' => $psCncMachine->companyService->company->paymentCurrency->title_original
]);


if ($canEdit) {
    AngularAppAsset::register($this);
    JsonEditorAssetJsTemplate::register($this);

    Yii::$app->angular
        ->service(['maps', 'geo', 'notify', 'router', 'user', 'ps/baseSteps'])
        ->directive(['google-map', 'google-address-input'])
        ->resource(['PsCnc', 'CncSchema'])
        ->controllerBackend('ps/psMachine/ps-cnc-machine')
        ->controllerParam('psMachineId', $psMachine->id)
        ->controllerParam('psCncJson', $json)
        ->controllerParam('cncSchema', $cncSchema)
        ->registerScripts($this);
}
?>
<div>
    <div class ng-controller="PsCncMachineController">
        <?php
        if (!$canEdit) {
            ?>
            <pre class="jsonHighlight"><?= json_encode($json, JSON_PRETTY_PRINT); ?></pre>
            <?php
        } else {
            ?>
            <div class="form-group" id="psCncJsonContainer">
            </div>
            <button value="SaveSchema" ng-click="saveCnc()">Save schema</button>
            <?php
        }
        ?>
    </div>
</div>
<div>
</div>
<br>
