<?php

use backend\components\AdminAccess;
use common\interfaces\ModelHistoryInterface;
use common\models\CompanyService;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyService */

$psMachine = $model;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ps Machines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ps-machine-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    $attributes = [
        'model'      => $model,
        'attributes' => [
            'id',
            'ps_id',
            'type',
            'ps_printer_id',
            'ps_cnc_machine_id',
            [
                'label'  => 'Location',
                'format' => 'raw',
                'value'  => \common\models\UserAddress::formatAddress($psMachine->location->toUserAddress()),
            ],
            'created_at',
            'updated_at',
            'is_location_change',
        ],
    ];
    if (!AdminAccess::can('psmachine.moderate')) {
        $attributes['attributes'][]='is_deleted';
        $attributes['attributes'][]='visibility';
        $attributes['attributes'][]='moderator_status';
    }

    if($psMachine->isCnc() && AdminAccess::can('psmachine.moderate') && $psMachine->moderator_status != CompanyService::MODERATOR_STATUS_APPROVED){
        echo Html::a('Approve', Url::toRoute(['approve-machine', 'psMachineId' => $psMachine->id]), ['class' => 'btn btn-success']);
    }

    $commonDetails = DetailView::widget(
            $attributes
    );

    if (AdminAccess::can('psmachine.moderate')) {
        $commonForm = $this->render('commonForm', ['companyService' => $psMachine]);
    } else {
        $commonForm = '';
    }

    $viewFilePath = __DIR__ . '/' . 'viewPsMachine' . ucfirst($psMachine->type) . '.php';
    if (file_exists($viewFilePath)) {
        echo $this->renderFile($viewFilePath, ['commonDetails' => $commonDetails, 'commonForm' => $commonForm, 'companyService' => $psMachine]);
    }

    if ($historyProvider) {
        echo GridView::widget(
            [
                'dataProvider' => $historyProvider,
                'columns'      => [
                    'id',
                    'created_at',
                    [
                        'header'    => 'User',
                        'attribute' => 'user_id',
                        'format'    => 'raw',
                        'value'     => function (ModelHistoryInterface $history) {
                            return \backend\models\Backend::displayUser($history->getUserId());
                        }
                    ],
                    'action_id',
                    'diffRemove' => [
                        'header' => 'Remove',
                        'format' => 'raw',
                        'value'  => function (ModelHistoryInterface $history) {
                            return '<pre class="jsonHighlight small-text">' . json_encode($history->getSourceResultDiffRemove(), JSON_PRETTY_PRINT) . '</pre>';
                        },
                    ],
                    'diffAdd'    => [
                        'header' => 'Add',
                        'format' => 'raw',
                        'value'  => function (ModelHistoryInterface $history) {
                            return '<pre class="jsonHighlight small-text">' . json_encode($history->getSourceResultDiffAdd(), JSON_PRETTY_PRINT) . '</pre>';
                        },
                    ],
                    'comment'
                ],
            ]
        );
    }
    ?>
</div>

