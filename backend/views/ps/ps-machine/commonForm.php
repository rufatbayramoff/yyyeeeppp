<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.08.17
 * Time: 11:22
 */

use common\models\CompanyService;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $psMachine common\models\CompanyService */

if (isset($companyService)) {
    $psMachine = $companyService;
}

$form = ActiveForm::begin(['action' => '/ps/ps-machine/save-ps-machine?id=' . $psMachine->id]);
echo $form->field($psMachine, 'is_deleted')->dropDownList([0 => 'notDeleted', 1 => 'deleted']);
echo $form->field($psMachine, 'visibility')->dropDownList(CompanyService::getVisibilityLabels());
echo $form->field($psMachine, 'moderator_status')->dropDownList(CompanyService::getModeratorStatusLabels());
?>
<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>
<?php
ActiveForm::end();
?>
