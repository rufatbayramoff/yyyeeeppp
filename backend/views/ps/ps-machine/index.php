<?php

use common\models\CompanyService;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CompanyServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ps Machines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-machine-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                [
                    'header'    => 'User',
                    'attribute' => 'ps.user_id',
                    'format'    => 'raw',
                    'filter'    => Html::activeTextInput($searchModel, 'userId', ['class' => 'form-control']),
                    'value'     => function (CompanyService $psMachine) {
                        return \backend\models\Backend::displayUser($psMachine->ps->user_id);
                    }
                ],
                [
                    'attribute' => 'ps_id',
                    'header'    => 'Ps',
                    'format'    => 'raw',
                    'value'     => function ($model) {
                        return "<a href='/ps/ps/view?id={$model->ps->id}'>" . \H($model->ps->title) . " [{$model->ps->id}] </a>";
                    }
                ],
                [
                    'attribute' => 'country',
                    'header'    => 'Cntry',
                    'value'     => 'location.country.iso_code',
                ],
                [
                    'attribute' => 'title',
                    'value'     => function (CompanyService $psMachine) {
                        return $psMachine->type == CompanyService::TYPE_PRINTER ? $psMachine->psPrinter->title : $psMachine->psCncMachine->code;
                    }
                ],
                [
                    'attribute' => 'type',
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'type',
                        CompanyService::ALLOWED_TYPES,
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                ],
                'updated_at',
                [
                    'class'    => ActionColumn::class,
                    'template' => '{view}'
                ],
            ],
        ]
    ); ?>
</div>
