<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PrinterMaterialIntl */

$this->title = 'Create Printer Material group Intl';
$this->params['breadcrumbs'][] = ['label' => 'Printer Material group Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-material-intl-group-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
