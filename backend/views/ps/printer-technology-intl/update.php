<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrinterTechnologyIntl */

$this->title = 'Update Printer Technology Intl: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Printer Technology Intls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="printer-technology-intl-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
