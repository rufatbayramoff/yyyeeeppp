<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PrinterTechnologyIntlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Printer Technology Intls';
$this->params['breadcrumbs'][] = $this->title;
echo $this->renderFile(Yii::getAlias('@backend/views/ps/printer-technology/_topTabs.php'));
?>
<div class="printer-technology-intl-index">


    <p>
        <?= Html::a('Create Printer Technology Intl', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'caption' => new \backend\components\GridViewDataExporter($searchModel),

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'model_id',
            'lang_iso',
            'title',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
