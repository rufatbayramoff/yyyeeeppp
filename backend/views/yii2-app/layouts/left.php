<?php
use yii\bootstrap\Nav;
$user = app('user')->getIdentity();
?>
<aside class="main-sidebar" style="padding-top: 0px;">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php 
                                  echo \cebe\gravatar\Gravatar::widget([
                                        'secure' => true,
                                        'email' => $user->email,
                                        'defaultImage' => 'monsterid',
                                        'options' => ['class'=> 'img-circle'],
                                        'size' => 160,
                                ]); 
                                  ?> 
            </div>
            <div class="pull-left info">
                <p><?php echo $user->username; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <form action="#" method="get" class="sidebar-form ">
            <div class="input-group">
                <input type="text" name="q" class="menusearch form-control" autocomplete="off" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>

        <?=
            \dmstr\widgets\Menu::widget([
                'items' => (new \backend\models\Backend())->getMenu(),
                'encodeLabels' => false,
                'options' => [
                    'class' => 'sidebar-menu'
                ]
            ]);
        ?>

    </section>
    <?=$this->registerJs("$(function(){
    $('input.menusearch').keyup(function(){
        var searchText = $(this).val().toLowerCase();
        $('.sidebar ul > li').each(function(){
            var currentLiText = $(this).text().toLowerCase(),
                showCurrentLi = currentLiText.indexOf(searchText) !== -1;
            $(this).toggle(showCurrentLi);
            $(this).find('ul').show();
        });     
    });
});");?>

</aside>
