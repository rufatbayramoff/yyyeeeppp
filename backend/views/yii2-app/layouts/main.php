<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$skin = YII_ENV === 'prod' ? 'skin-blue' : 'skin-purple-light';
if (Yii::$app->controller->action->id === 'login') {
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {
    if (is_guest()) {
        header("Location: /site/login");
        exit;
    }
    backend\assets\AppAsset::register($this);
    \backend\assets\AdminLteAsset::register($this);
    // dmstr\web\AdminLteAsset::register($this);, fucking RKN - blocking font from googleapis

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= \H($this->title) ?></title>
        <link href='/css/AdminLTE.min.css' rel="stylesheet"/>
        <?php $this->head() ?>
        <style>
            .grid-view .table thead th {
                background: #8aa4af;
                color: white;
            }

            .grid-view .table thead th a {
                color: white;
            }
        </style>
    </head>
    <body class="<?= $skin; ?>">
    <?php $this->beginBody() ?>
    <div class="wrapper" ng-app="app">


        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?= $this->render(
                'left.php',
                ['directoryAsset' => $directoryAsset]
            )
            ?>
            <?php
            echo $this->render(
                'content.php',
                ['content' => $content, 'directoryAsset' => $directoryAsset]
            ) ?>

        </div>
    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
