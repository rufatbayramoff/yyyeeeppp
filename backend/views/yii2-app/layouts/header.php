<?php
use backend\models\notify\NotifyItem;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

$user = app('user')->getIdentity();
/* @var $this \yii\web\View */
/* @var $content string */

$messages = 0;
$notifiesData = \backend\models\notify\NotifyFacade::getNotifies();
$soundObjectsCount = 0;
foreach ($notifiesData as $k => $v) {
    if (\backend\models\notify\NotifyItem::isTranslate($v)) {
        continue;
    }
    if (($v->type != NotifyItem::TYPE_TO_TRANSLATE) && ($v->type != NotifyItem::TYPE_TRANSLATE_CHECK)) {
        $soundObjectsCount += (int)$v->count;
    }
}
$tasks = 0;

// version check - remove from here later
$version = app('cache')->get('git_version');
if (!$version) {
    // TS -  HEAD detached at R1.92.p15
    exec("git branch | grep \*", $a, $r);
    if ($a) {
        $r = $a[0];
        $version = str_replace(["detached from", "*", "(", ")"], ["", "", ""], $r);
        app('cache')->set('git_version', $version, 30);
    } else {
        app('cache')->set('git_version', '', 3);
    }
}
$playSound = false;

?>
<audio id="notify_play_sound" src="/audio/ring.mp3" preload="auto"></audio>
<?php
if (Yii::$app->session->get('lastNotifiesObject') != $soundObjectsCount) {
    Yii::$app->session->set('lastNotifiesObject', $soundObjectsCount);
    ?>
    <script>document.getElementById('notify_play_sound').play();</script>
    <?php
}
?>
<header class="main-header">

    <?= Html::a("TS - " . $version, Yii::$app->homeUrl, ['class' => 'logo', 'style' => 'font-size: 14px;']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <?php if ($messages > 0): ?>
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success"><?php echo $messages; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have <?php echo $messages; ?> messages</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li><!-- start message -->
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="" class="img-circle"
                                                     alt="User Image"/>
                                            </div>
                                            <h4>
                                                Support Team
                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                    <!-- end message -->
                                </ul>
                            </li>
                            <li class="footer"><a href="#">See All Messages</a></li>
                        </ul>
                    </li>

                <?php endif; ?>

                <li class="dropdown notifications-menu" id="js-top-notify-list">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-danger" style="font-size:12px;"><?php echo 0; ?></span>
                        </a>

                    <!-- doubles in top_notify.php -->
                    <ul class="dropdown-menu">
                        <li class="header">You have <span class="notify-count"><?php echo '-'; ?></span> notifications</li>
                        <li>

                        </li>
                        <!-- <li class="footer"><a href="#">View all</a></li> -->
                    </ul>
                </li>
                <!-- Tasks: style can be found in dropdown.less -->

                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php
                        echo \cebe\gravatar\Gravatar::widget(
                            [
                                'secure'       => true,
                                'email'        => $user->email,
                                'defaultImage' => 'monsterid',
                                'options'      => ['class' => 'user-image'],
                                'size'         => 160,
                            ]
                        );
                        ?>
                        <span class="hidden-xs"><?php echo $user->username; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php
                            echo \cebe\gravatar\Gravatar::widget(
                                [
                                    'secure'       => true,
                                    'email'        => $user->email,
                                    'defaultImage' => 'monsterid',
                                    'options'      => ['class' => 'img-circle'],
                                    'size'         => 160,
                                ]
                            );
                            ?>

                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#"> </a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#"> </a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#"> </a>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->

            </ul>
        </div>
    </nav>
</header>
