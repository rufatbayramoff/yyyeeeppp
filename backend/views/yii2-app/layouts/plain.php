<?php

use dmstr\widgets\Alert;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

backend\assets\AppAsset::register($this);
dmstr\web\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <style>
        .grid-view .table thead th {
            background: #8aa4af;
            color: white;
        }

        .grid-view .table thead th a {
            color: white;
        }
    </style>
</head>
<?php $this->beginBody();?>
<section class="content">
    <?= Alert::widget() ?>
    <?php
    $content = str_replace("grid-view", "grid-view table-responsive", $content);
    echo $content; ?>
</section>
<?=$this->endBody(); ?>
</html>
<?php $this->endPage() ?>
