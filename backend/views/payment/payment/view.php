<?php

use backend\models\Backend;
use common\models\Payment;
use common\models\PaymentDetail;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Payment */

$this->title                   = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
if ($model->paymentInvoice && $model->paymentInvoice->store_order_id) {
    $orderId       = $model->paymentInvoice->store_order_id;
    $orderViewLink = '<a href="/store/store-order/view?id=' . $orderId . '">' . $orderId . '</a>';
} else {
    $orderViewLink = null;
}


?>
<div class="payment-view">

    <p>
        <?php if ($model->id > 1) {
            echo Html::a('Create Payment correction #' . $model->id, ['/store/payment-correction/create', 'paymentId' => $model->id], ['class' => 'btn btn-success']);
        } ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'created_at',
            'description',
            'status',
            'payment_invoice_uuid',
            'store_order'          => [
                'label'  => 'Order',
                'format' => 'raw',
                'value'  => function (Payment $model) {
                    if ($model->paymentInvoice && $model->paymentInvoice->store_order_id) {
                        $orderId = $model->paymentInvoice->store_order_id;
                        return '<a href="/store/store-order/view?id=' . $orderId . '">' . $orderId . '</a>';
                    }
                }
            ],
            'instant_payment'      => [
                'label'  => 'Instant payment',
                'format' => 'raw',
                'value'  => function (Payment $model) {
                    if ($model->paymentInvoice && $model->paymentInvoice->instantPayment) {
                        $instantPaymentId = $model->paymentInvoice->instantPayment->uuid;
                        return '<a href="/store/instant-payment/update?id=' . $instantPaymentId . '">' . $instantPaymentId . '</a>';
                    }
                }
            ],
            'ts_internal_purchase' => [
                'label'  => 'Ts Internal Purchase',
                'format' => 'raw',
                'value'  => function (Payment $model) {
                    if ($model->paymentInvoice && $model->paymentInvoice->tsInternalPurchase) {
                        $tsInternalPurchaseId = $model->paymentInvoice->tsInternalPurchase->uid;
                        return '<a href="/store/ts-internal-purchase/view?id=' . $tsInternalPurchaseId . '">' . $tsInternalPurchaseId . '</a>';
                    }
                }
            ],


        ],
    ]) ?>

    <hr/>

    <?= yii\grid\GridView::widget([
        'dataProvider' => $dataProviderDetail,
        'columns'      => [
            'id',
            'payment_user' => [
                'format'    => 'raw',
                'attribute' => 'payment_account_id',
                'label'     => 'User',
                'value'     => function (PaymentDetail $detail) {
                    return Backend::displayUser($detail->paymentAccount->user->id);
                }
            ],
            'payment_type' => [
                'format' => 'raw',
                'label'  => 'Type',
                'value'  => function (PaymentDetail $detail) {
                    return $detail->paymentAccount->type;
                }
            ],
            'updated_at',
            'amount',
            'original_currency',
            'rate_id',
            'description',
            'type',
            'payment_detail_operation_uuid'
        ],
    ]); ?>
</div>
