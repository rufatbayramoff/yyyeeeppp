<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id', 
                'format' => 'raw',
                'value' => function($model){
                    $url = yii\helpers\Url::toRoute(['store/payment/view', 'id'=>$model->id]);
                    return yii\helpers\Html::a('#' .$model->id, $url, ['class'=>'btn btn-lg btn-ghost btn-info']);
                }
            ],
            'created_at:datetime',
            'description',
            'status',
        ],
    ]); ?>

</div>
