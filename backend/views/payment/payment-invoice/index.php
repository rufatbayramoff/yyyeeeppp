<?php

use backend\components\columns\UserColumn;
use common\models\PaymentBankInvoice;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentBankInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Bank Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-invoice-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'uuid',
            'payment_invoice_uuid',
            [
                'format'    => 'raw',
                'attribute' => 'user_id',
                'label'     => 'User',
                'value'     => static function (PaymentBankInvoice $paymentBankInvoice) {
                    return UserColumn::getMenu($paymentBankInvoice->paymentInvoice->user);
                }
            ],
            [
                'format'    => 'raw',
                'label'     => 'Order',
                'attribute' => 'order_id',
                'value'     => static function (PaymentBankInvoice $paymentBankInvoice) {
                    if ($orderId = $paymentBankInvoice->storeOrderId) {
                        return '<a href="/store/store-order/view?id=' . $orderId . '">' . H($orderId) . "</a>";
                    }
                    return null;
                }
            ],
            'created_at',
            'date_due',
            'status',
            'ship_to:raw',
            'bank_details',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>


<!--uuid                 varchar(32)                         not null-->
<!--primary key,-->
<!--payment_invoice_uuid varchar(6)                          not null,-->
<!--created_at           timestamp default CURRENT_TIMESTAMP not null,-->
<!--date_due             date                                not null,-->
<!--ship_to              varchar(245)                        null,-->
<!--bill_to              varchar(245)                        null,-->
<!--bank_details         varchar(245)                        null,-->
<!--payment_details      varchar(245)                        null,-->
<!--comment              text                                null,-->
<!--transaction_id       in-->