<?php

use backend\components\columns\UserColumn;
use common\models\PaymentBankInvoice;
use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentBankInvoice */

$this->title                   = $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Payment Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$paymentService = Yii::createObject(\common\modules\payment\services\PaymentService::class);

?>
<div class="payment-invoice-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?php if (!$model->isPaid()): ?>
            <?php echo Html::a('Extend date', ['extend', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        <?php if ($model->isNew()): ?>
            <button type="button" class="btn btn-success btn-ajax-modal" value="/store/payment-bank-invoice/pay-amount-popup?id=<?= $model->uuid ?>" title="Set Pay" data-target="#set_pay">Set Pay
            </button>
            <?php echo Html::a('Cancel', ['void', 'id' => $model->uuid], ['class' => 'btn btn-danger']) ?>
        <?php endif; ?>
    </p>

    <div class="row">
        <div class="col-lg-5">
            <h3>Bank Invoice #<?= $model->uuid; ?></h3>
            <?php echo DetailView::widget(
                [
                    'model'      => $model,
                    'attributes' => [
                        [
                            'attribute' => 'order_id',
                            'format'    => 'raw',
                            'value'     => function (PaymentBankInvoice $model) {
                                if ($model->paymentInvoice->storeOrder) {
                                    return Html::a($model->paymentInvoice->storeOrder->id, ['store/store-order/view', 'id' => $model->paymentInvoice->storeOrder->id]);
                                }

                                return null;
                            }
                        ],
                        'uuid',
                        'payment_invoice_uuid',
                        'created_at',
                        'date_due',
                        'status',
                        'ship_to:html',
                        'bill_to:html',
                        'bank_details:html',
                        'payment_details:html',
                    ],
                ]
            ) ?>

            <?php
            if ($model->paymentInvoice->payments) {
                echo $this->renderFile(Yii::getAlias('@backend/views/store/payment-invoice/payments.php'), ['payments' => $model->paymentInvoice->payments]);
            }
            ?>

        </div>
        <div class="col-lg-7">
            <h3>Invoice #<?php echo $model->payment_invoice_uuid; ?></h3>
            <?php echo DetailView::widget(
                [
                    'model'      => $model->paymentInvoice,
                    'attributes' => [
                        'created_at',
                        'date_expire',
                        'total_amount',
                        'total_payed' => [
                            'label' => 'Total payed',
                            'value' => function (PaymentInvoice $model) use ($paymentService) {
                                return $paymentService->getTotalPayed($model);
                            }],
                        'currency',
                        'status',
                        'company'     => [
                            'format' => 'raw',
                            'label'  => 'Company',
                            'value'  => $model->paymentInvoice->company?\backend\components\columns\PsColumn::getMenu($model->paymentInvoice->company):''
                        ],
                        'user'        => [
                            'format' => 'raw',
                            'label'  => 'User',
                            'value'  => UserColumn::getMenu($model->paymentInvoice->user)
                        ],
                        'store_order' => [
                            'format' => 'raw',
                            'label'  => 'Store order',
                            'value'  => '<a href="/store/store-order/view?id=' . $model->getStoreOrderId() . '">' . $model->getStoreOrderId() . '</a>'
                        ],
                        'order_position_id',
                        'preorder_id',
                        'details',
                        'is_settle_submit'
                    ],
                ]
            ) ?>

            <h3>Invoice items</h3>
            <?php echo \yii\grid\GridView::widget(
                [
                    'dataProvider' => PaymentInvoiceItem::getDataProvider(['payment_invoice_uuid' => $model->payment_invoice_uuid]),
                    'columns'      => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'pos',
                        'title',
                        'description',
                        'measure',
                        [
                            'attribute' => 'qty',
                            'format'    => 'raw',
                            'value'     => function (PaymentInvoiceItem $model) {
                                return app()->formatter->asDecimal($model->qty);
                            }
                        ],
                        [
                            'attribute' => 'total_line',
                            'value'     => function (PaymentInvoiceItem $model) {
                                return displayAsMoney($model->getLineTotal());
                            }
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>

</div>
