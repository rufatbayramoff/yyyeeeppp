<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentBankInvoice */

$this->title = 'Update Payment Invoice: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Payment Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-invoice-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
