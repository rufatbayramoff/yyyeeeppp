<?php
/**
 *
 * @var $refundModel \common\models\PaymentTransactionRefund
 * @var $transaction \common\models\PaymentTransaction
 *
 */

use backend\modules\payment\assets\RefundPopupAsset;
use common\components\JsObjectFactory;
use common\models\PaymentBankInvoice;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var $paymentBankInvoice PaymentBankInvoice */

$this->title = 'Bank invoice set payed';
?>

<div class="pay-amount-form">
    <div class="form-message"></div>
    <?php $form = ActiveForm::begin([
        'action' => ['settle', 'id'=>$paymentBankInvoice->uuid],
        'method' => 'post',
    ]); ?>
    <div class="form-group field-paymenttransactionrefund-amount required">
        <label class="control-label" for="paymenttransactionrefund-amount">Amount (<?=$paymentBankInvoice->paymentInvoice->currency?>)</label>
        <input type="text" id="paymenttransactionrefund-amount" class="form-control" name="amount" value="<?=$paymentBankInvoice->paymentInvoice->getAmountTotal()->getAmount() ?>" aria-required="true">
        <div class="help-block"></div>
    </div>
    <?php echo Html::submitButton('Submit', ['class' => 'btn btn-primary ajax-submit']) ?>
    <?php ActiveForm::end(); ?>
</div>

