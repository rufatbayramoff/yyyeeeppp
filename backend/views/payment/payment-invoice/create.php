<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentBankInvoice */

$this->title = 'Create Payment Invoice';
$this->params['breadcrumbs'][] = ['label' => 'Payment Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$model->uuid = \common\models\PaymentBankInvoice::generateUuid();

?>
<div class="payment-invoice-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
