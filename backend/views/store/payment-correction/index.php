<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 15.11.18
 * Time: 11:06
 *
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProviderDetail
 */

use common\models\Payment;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Payment correction';
?>

<h1><?php echo \H($this->title) ?></h1>

<p>
    <?php echo Html::a('Create Payment correction', ['create'], ['class' => 'btn btn-success']);?>
</p>

<?php echo GridView::widget([
    'dataProvider' => $dataProviderDetail,
    'columns' => [
        [
            'label' => 'payment',
            'format' => 'raw',
            'attribute' => 'id',
            'value' => function (Payment $payment) {
                return Html::a($payment->id, ['create', 'paymentId' => $payment->id], ['class' => 'btn btn-info']);
            }
        ],
        'created_at',
        'description',
        'status',
        'parent_id',
    ],
]) ?>