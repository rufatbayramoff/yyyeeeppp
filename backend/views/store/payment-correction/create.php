<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 15.11.18
 * Time: 11:06
 *
 * @var \yii\web\View $this
 * @var array $userAccountTypes
 * @var \common\models\Payment $payment
 */

use backend\models\Backend;
use common\models\PaymentDetail;
use common\models\PaymentTransaction;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

$this->title = 'Create Payment correction';
$this->params['breadcrumbs'][] = ['label' => 'Payment correction', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-6">
        <?php if ($payment): ?>
            <?php ActiveForm::begin([
                'action' => ['transfer-money']
            ]); ?>
            <div class="form-group">
                <label for="exampleInputPassword1">Payment ID</label>
                <?php echo Html::textInput('payment_id', $payment->id ?? null, ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label>User ID</label>
                <div class="row">
                    <div class="col-md-8">
                        <?php echo Select2::widget([
                            'name'          => 'user_id',
                            'value'         => $currentUser->id ?? null,
                            'initValueText' => $currentUser->username ?? null,
                            'pluginOptions' => [
                                'allowClear'         => true,
                                'minimumInputLength' => 2,
                                'ajax'               => [
                                    'url'      => \yii\helpers\Url::to(['/user/user/list']),
                                    'dataType' => 'json',
                                    'data'     => new yii\web\JsExpression('function(params) { return {q:params.term, page:params.page, k:"id,username"}; }')
                                ],
                            ],
                        ]); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo Html::dropDownList('account_type', null, $userAccountTypes, ['class' => 'form-control']); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo Html::dropDownList('currency', $payment->paymentInvoice->currency, \lib\money\Currency::ALLOW_SELECT_CURRENCIES, ['class' => 'form-control']); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Direction</label>
                <?php echo Html::dropDownList('direction', null, [
                    'from_t_to_u' => 'From Treatstock correct to User',
                    'from_u_to_t' => 'From user to Treatstock correct'
                ], ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Amount</label>
                <?php echo Html::textInput('amount', null, ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Comment</label>
                <?php echo Html::textarea('comment', null, ['class' => 'form-control']); ?>
            </div>
            <?php echo Html::submitButton('Submit', ['class' => 'btn btn-success']); ?>
            <?php ActiveForm::end(); ?>
        <?php else: ?>
            <?php ActiveForm::begin([
                'method' => 'GET',
                'action' => ['create']
            ]); ?>
            <div class="form-group">
                <label for="exampleInputPassword1">Payment ID</label>
                <?php echo Html::textInput('paymentId', null, ['class' => 'form-control']); ?>
            </div>

            <?php echo Html::submitButton('Submit', ['class' => 'btn btn-success']); ?>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>

    <div class="col-md-6">
        <?php if ($payment): ?>
            <h3>Payment ID: <?php echo $payment->id; ?></h3>
            <?php echo DetailView::widget([
                'model'      => $payment,
                'attributes' => [
                    'created_at',
                    'description',
                    'status',
                    'parent_id',
                ],
            ]) ?>
        <?php endif; ?>
    </div>

    <div class="col-md-12">
        <?php if ($payment): ?>
            <h3>Payment detail</h3>
            <?php echo GridView::widget([
                'dataProvider' => $dataProviderDetail,
                'columns'      => [
                    'id',
                    'payment_account_id',
                    [
                        'label'  => 'User',
                        'format' => 'raw',
                        'value'  => function (PaymentDetail $detail) {
                            return Backend::displayUser($detail->paymentAccount->user->id);
                        }
                    ],
                    'updated_at',
                    'amount',
                    'original_currency',
                    'rate_id',
                    'description',
                    'type'
                ],
            ]); ?>
        <?php endif; ?>
    </div>
</div>


