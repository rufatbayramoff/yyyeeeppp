<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TaxCountrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tax Countries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tax-country-index">

    <p>
        <?= Html::a('Create Tax Country', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'country',            
            ['attribute' => 'title', 'value' => 'country0.title'],
            'rate_ps',
            'rate_model',
            'updated_at',
            'is_default:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
