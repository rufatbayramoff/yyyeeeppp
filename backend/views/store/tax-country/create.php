<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaxCountry */

$this->title = 'Create Tax Country';
$this->params['breadcrumbs'][] = ['label' => 'Tax Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tax-country-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
