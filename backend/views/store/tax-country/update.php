<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaxCountry */

$this->title = 'Update Tax Country: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tax Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tax-country-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
