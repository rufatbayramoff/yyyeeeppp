<?php

/* @var $this yii\web\View */

use backend\components\columns\UserColumn;
use common\models\CuttingPack;
use common\models\CuttingPackFile;
use common\models\CuttingPackPart;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $cuttingPack CuttingPack */

echo DetailView::widget([
    'model' => $cuttingPack,
    'attributes' => [
        'created_at',
        [
          'label' => 'User',
          'format' => 'raw',
          'value' => $cuttingPack->user?UserColumn::getMenu($cuttingPack->user):'',
        ],
        'uuid',
        'source',
        [
            'label' => 'Material',
            'format' => 'raw',
            'value' => $cuttingPack->material ? '<a href="/cutting/cutting-material/update?id=' . $cuttingPack->material->id . '">' . $cuttingPack->material->title . '</a>' : '',
        ],
        [
            'label' => 'Thickness',
            'value' => $cuttingPack->thickness
        ],
        [
            'label' => 'Color',
            'format' => 'raw',
            'value' => $cuttingPack->color ? '<a href="/ps/printer-color/view?id=' . $cuttingPack->color->id . '">' . $cuttingPack->color->title . '</a>' : ''
        ]
    ]
]);
?>
<div class="row">
    <div class="col-md-12"><h3>Files</h3></div>
    <?php
    foreach ($cuttingPack->activeCuttingPackFiles as $cuttingPackFile) { ?>
        <div class="col-md-12">
            <?php
            $link = ['/store/cutting-pack/download-pack-file', 'uuid' => $cuttingPackFile->uuid];
            echo '&nbsp; &nbsp; ' . Html::a(' Download', $link, ['class' => 'fa fa-download']);
            echo DetailView::widget(
                [
                    'model' => $cuttingPackFile,
                    'options' => ['class' => 'table no-margin no-first-row-top-border'],
                    'attributes' => [
                        'type',
                        'title',
                        'previewUrl' => [
                            'attribute' => 'previewUrl',
                            'format' => 'raw',
                            'value' => function (CuttingPackFile $cuttingPackFile) {
                                if ($cuttingPackFile->isImage()) {
                                    $url = $cuttingPackFile->file->getFileUrl();
                                    return '<a href="' . $url . '"><img src="' . $url . '" style="max-width:150px"></a>';
                                }
                                return '';
                            }
                        ],
                        [
                            'attribute' => 'jobs',
                            'format' => 'raw',
                            'value'  => function(CuttingPackFile $cuttingPackFile) {
                                $links = '';
                                $jobs = $cuttingPackFile->file->fileJobs ?? [];
                                foreach($jobs as $job) {
                                    $links .= Html::a($job->id,['model3d/jobs/view','id' => $job->id]).'<br>';
                                }
                                return $links;
                            }
                        ],
                        'qty',
                        'is_active',
                    ]
                ]
            );
            ?>
        </div>
        <?php
        if ($cuttingPackFile->cuttingPackParts) { ?>
            <div class="col-md-12"><h4>Parts</h4></div>
            <?php
            foreach ($cuttingPackFile->cuttingPackParts as $cuttingPackPart) {
                if (!$cuttingPackPart->is_active) {
                    continue;
                }
                echo '<div class="col-md-12">';
                echo DetailView::widget(
                    [
                        'model' => $cuttingPackPart,
                        'options' => ['class' => 'table no-margin no-first-row-top-border'],
                        'attributes' => [
                            [
                                'label' => 'Image',
                                'format' => 'raw',
                                'value' => function (CuttingPackPart $cuttingPackPart) {
                                    $url = '/store/cutting-pack/download-part-image?uuid=' . $cuttingPackPart->uuid;
                                    return $cuttingPackPart->image ? '<a href="' . $url . '" target=_blank><img src="' . $url . '" style="width:100px;height:100px;background-color:white;border: solid 1px #bdbdbd;"></>' : '';
                                }
                            ],
                            'qty',
                            'width',
                            'height',
                            'cutting_length',
                            'engraving_length',
                            [
                                'label' => 'Material',
                                'format' => 'raw',
                                'value' => $cuttingPackPart->material ? '<a href="/cutting/cutting-material/update?id=' . $cuttingPackPart->material->id . '">' . $cuttingPackPart->material->title . '</a>' : '',
                            ],
                            [
                                'label' => 'Thickness',
                                'value' => $cuttingPackPart->thickness
                            ],
                            [
                                'label' => 'Color',
                                'format' => 'raw',
                                'value' => $cuttingPackPart->color ? '<a href="/ps/printer-color/view?id=' . $cuttingPackPart->color->id . '">' . $cuttingPackPart->color->title . '</a>' : ''
                            ]
                        ]
                    ]);
                echo '</div>';
            }
            ?>
            <?php
        } ?>
        <?php
    } ?>
</div>
