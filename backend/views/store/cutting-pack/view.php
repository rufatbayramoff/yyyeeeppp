<?php

use backend\models\Backend;
use backend\widgets\FilesListWidget;
use common\models\CuttingPack;
use common\models\PreorderHistory;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $cuttingPack CuttingPack */

$this->title                   = 'Cutting pack  #'.$cuttingPack->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Preorders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('viewModel', ['cuttingPack' => $cuttingPack]) ?>
</div>
