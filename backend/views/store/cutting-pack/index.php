<?php

use backend\components\columns\PsColumn;
use backend\components\columns\UserColumn;
use common\interfaces\Model3dBaseInterface;
use common\models\CuttingPack;
use common\models\DeliveryType;
use common\models\StoreOrder;
use common\models\UserAddress;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Cutting packs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-index">
    <?= GridView::widget(
        ['dataProvider' => $dataProvider,
         'filterModel'  => $searchModel,
         'columns'      => [
             'created_at',
             'uuid',
             [
                 'label'     => 'User',
                 'format'    => 'raw',
                 'filter'    => Html::activeTextInput($searchModel, 'user_id', ['class' => 'form-control']),
                 'class'     => \backend\components\columns\UserColumn::class,
                 'attribute' => 'user',
             ],
             [
                 'label'  => 'User location',
                 'format' => 'raw',
                 'value'  => function (CuttingPack $cuttingPack) {
                     return $cuttingPack->userSession ? Yii::$app->geo->getLocationStringByIp($cuttingPack->userSession->ip) : '';
                 }
             ],
             [
                 'label' => 'User ip',
                 'value' => function (CuttingPack $cuttingPack) {
                     return $cuttingPack->userSession ? $cuttingPack->userSession->ip : '';
                 }
             ],
             [
                 'label' => 'Referrer',
                 'value' => function (CuttingPack $cuttingPack) {
                     return $cuttingPack->userSession ? $cuttingPack->userSession->referrer : '';
                 }
             ],
             [
                 'label' => 'File formats',
                 'value' => function (CuttingPack $cuttingPack) {
                     $ext = [];
                     foreach ($cuttingPack->activeCuttingPackFiles as $file) {
                         $ext[$file->file->getFileExtension()] = $file->file->getFileExtension();
                     }
                     return implode(', ', $ext);
                 }
             ],
             [
                 'label' => 'Material',
                 'value' => function (CuttingPack $cuttingPack) {
                     return ($cuttingPack->is_same_material_for_all_parts && $cuttingPack->material) ? $cuttingPack->material->title : '';
                 }
             ],
             'thickness',
             [
                 'label' => 'Color',
                 'value' => function (CuttingPack $cuttingPack) {
                     return ($cuttingPack->is_same_material_for_all_parts && $cuttingPack->color) ? $cuttingPack->color->title : '';
                 }
             ],
             [
                 'format' => 'raw',
                 'value'  => function ($model) {
                     $url  = yii\helpers\Url::toRoute(['/store/cutting-pack/view', 'uuid' => $model->uuid]);
                     $txt1 = yii\helpers\Html::a('View', $url, ['class' => 'btn btn-primary', 'target' => '_blank']);
                     return $txt1;
                 }
             ]
         ],
        ]
    ); ?>

</div>
