<?php

use common\models\PaymentTransaction;
use common\modules\payment\gateways\vendors\BankPayoutGateway;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Payment bank payout';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-transaction-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            [
                'label'     => 'User',
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $user = \backend\models\Backend::displayUser($model->user);
                    return $user;
                }
            ],
            [
                'attribute' => 'transaction_id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $url = yii\helpers\Url::toRoute(['store/payment-transaction/view', 'id' => $model->id]);
                    return yii\helpers\Html::a($model->transaction_id, $url, ['style' => 'font-size: 15px;']);
                }
            ],
            'updated_at:datetime',
            'status',
            [
                'format'    => 'raw',
                'attribute' => 'amount',
                'value'     => function ($model) {
                    $html = app('formatter')->asCurrency($model->amount, $model->currency);
                    return $html;
                }
            ],
            [
                'attribute' => 'details',
                'value'     => function (PaymentTransaction $model) {
                    return $model->details[BankPayoutGateway::ACCOUNT_DETAILS];
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}']

        ],
    ]); ?>

</div>
