<?php
/**
 *
 * @var $refundModel \common\models\PaymentTransactionRefund
 * @var $transaction \common\models\PaymentTransaction
 *
 */

use backend\modules\payment\assets\RefundPopupAsset;
use common\components\JsObjectFactory;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Request refund';
[$accountingList, $moneyMax] = $availableTypes;

JsObjectFactory::createJsObject(
    'refundPopupClass',
    'refundPopup',
    ['moneyMax' => $moneyMax]
    ,
    $this
);
?>

<div class="payment-transaction-refund-form">
    <div class="form-message"></div>
    <?php $form = ActiveForm::begin(); ?>
        <b>Total amount:</b> <?= displayAsMoney($refundModel->getMoneyAmount()); ?>
        <?php echo $form->field($refundModel, 'comment')->textarea(['maxlength' => true]) ?>
        <?php echo $form->field($refundModel, 'transaction_id')->hiddenInput()->label(false) ?>
        <?php echo $form->field($refundModel, 'currency')->hiddenInput()->label(false) ?>
        <?php echo $form->field($refundModel, 'user_id')->hiddenInput()->label(false) ?>

        <?php if ($refundModel->paymentInvoice->baseByPrintOrder() || !$refundModel->paymentInvoice->storeOrder->isCompleted()):?>
            <?php if ($refundModel->isAllowedFullRefund()): ?>
                <?php echo $form->field($refundModel,
                    'isFullRefund')->checkbox(['onclick' => 'if (this.checked) {$("#fromAccount").addClass("hidden")} else {$("#fromAccount").removeClass("hidden")}']);
                ?>
            <?php endif; ?>

            <div id="fromAccount" class="hidden">
                <?php echo $form->field($refundModel, 'refund_type')->dropDownList($accountingList, ['onchange' => 'refundPopup.updateMax(this.value);'])->label('Refund From'); ?>
                <?php echo $form->field($refundModel, 'amount')->textInput(['maxlength' => true])->label(sprintf('Amount (%s)', $refundModel->currency)) ?>
            </div>
        <?php endif; ?>
        <?php echo Html::submitButton($refundModel->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary ajax-submit']) ?>
    <?php ActiveForm::end(); ?>
</div>
