<?php

use backend\models\search\PaymentTransactionHistorySearch;
use common\models\PaymentTransactionHistory;
use common\modules\payment\gateways\vendors\BankPayoutGateway;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var PaymentTransactionHistorySearch $searchModelHistory
 * @var $paymentTransaction common\models\PaymentTransaction
 */

$this->title                   = $paymentTransaction->id;
$this->params['breadcrumbs'][] = ['label' => 'Payout bank Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


if ($paymentTransaction->isBankPayoutAuthorized()) {
    ?>
    <p>
        <?= Html::a('Approve', ['approve', 'id' => $paymentTransaction->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Decline', ['decline', 'id' => $paymentTransaction->id], ['class' => 'btn btn-danger']) ?>
    </p>
<?php } ?>

<div class="payment-transaction-view">
    <?php echo DetailView::widget([
        'model'      => $paymentTransaction,
        'attributes' => [
            'id',
            'user_id',
            'transaction_id',
            'first_payment_detail_id',
            'vendor',
            'created_at',
            'updated_at',
            'status',
            'type',
            'amount',
            'currency',
            'comment',
            [
                'attribute' => 'details',
                'format'    => 'raw',
                'value'     => '<pre>'.htmlspecialchars($paymentTransaction->details[BankPayoutGateway::ACCOUNT_DETAILS]).'</pre>',
            ],
            'is_notified_need_settled',
            'refund_id',
            'last_original_transaction'
        ],
    ]) ?>

    <?php echo \yii\grid\GridView::widget([
        'dataProvider' => $dataProviderHistory,
        'filterModel'  => $searchModelHistory,
        'columns'      => [
            'id',
            'payment_detail_id',
            'created_at',
            'action_id',
            [
                'attribute' => 'comment',
                'format'    => 'raw',
                'value'     => function (PaymentTransactionHistory $model) {
                    return $model->comment
                        ? VarDumper::dumpAsString($model->comment, 10, true)
                        : null;
                }
            ],
            'user_id',
        ],
    ]); ?>
</div>
