<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustomCodesCountry */

$this->title = 'Create Custom Codes Country';
$this->params['breadcrumbs'][] = ['label' => 'Custom Codes Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-codes-country-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
