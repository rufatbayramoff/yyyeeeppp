<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustomCodesCountry */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Custom Codes Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-codes-country-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code_id',
            'title',
            'iso_code',
            'custom_code',
            'is_active',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
