<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustomCodesCountry */

$this->title = 'Update Custom Codes Country: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Custom Codes Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="custom-codes-country-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
