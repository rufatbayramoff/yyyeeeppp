<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CustomCodesCountrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Custom Codes Countries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-codes-country-index">

    <p>
        <?= Html::a('Create Custom Codes Country', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code_id',
            'title',
            'iso_code',
            'custom_code',
            // 'is_active',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
