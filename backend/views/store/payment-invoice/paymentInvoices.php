<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.08.18
 * Time: 16:29
 */

use common\models\StoreOrder;


/** @var $primaryInvoice \common\models\PaymentInvoice */
/** @var $invoices \common\models\PaymentInvoice[] */

foreach ($invoices as $invoice) {
    echo $this->render('paymentInvoice', ['primaryInvoice'=>$primaryInvoice, 'paymentInvoice' => $invoice]);
}
?>
