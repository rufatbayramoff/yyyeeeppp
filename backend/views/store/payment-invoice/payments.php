<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.10.18
 * Time: 17:18
 */

use common\models\Payment;

/** @var $payments Payment[] */
?>
<?php
foreach ($payments as $payment) {
    echo $this->render('payment', ['payment' => $payment]);
}
?>
