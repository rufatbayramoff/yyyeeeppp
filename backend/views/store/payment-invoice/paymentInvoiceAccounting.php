<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.08.18
 * Time: 17:07
 */

$collapseId = 'accounting-' . random_int(0, 99999);
?>
<div id="parent-<?= $collapseId ?>">
    <div class="card">
        <div class="card-header" id="headingTwo">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#<?= $collapseId ?>" aria-expanded="false" aria-controls="<?= $collapseId ?>">
                    Collapse accounting
                </button>
            </h5>
        </div>
        <div id="<?= $collapseId ?>" class="collapse" aria-labelledby="headingTwo" data-parent="#parent-<?= $collapseId ?>">
            <div class="card-body">
                <pre class="jsonHighlight small-text"><?= json_encode($accounting, JSON_PRETTY_PRINT) ?></pre>
            </div>
        </div>
    </div>
</div>

