<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.10.18
 * Time: 13:22
 */

/** @var \common\models\PaymentTransactionHistory $paymentTransactionHistory */
$paymentTransaction = $paymentTransactionHistory->transaction;
?>

<?= \yii\widgets\DetailView::widget(
    [
        'model'      => $paymentTransaction,
        'attributes' => [
            'vendor',
            'transaction_id',
            'created_at',
            'updated_at',
            'type',
            'amount'  => [
                'label' => 'Amount',
                'value' => $paymentTransaction['amount'] . ' ' . $paymentTransaction['currency']
            ],
            'refund_id',
            'user'    => [
                'label' => 'User',
                'value' => $paymentTransaction->user->username
            ],
            'history' => [
                'label' => 'History state',
                'value' => $paymentTransactionHistory->action_id . ': ' . json_encode($paymentTransactionHistory->comment)
            ],
        ]
    ]);
?>
