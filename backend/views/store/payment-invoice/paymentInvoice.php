<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.08.18
 * Time: 16:35
 */

use backend\models\Backend;
use common\models\PaymentInvoice;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var $primaryInvoice \common\models\PaymentInvoice */
/** @var $paymentInvoice \common\models\PaymentInvoice */

?>
<div class="box box-solid<?php echo($primaryInvoice->uuid !== $paymentInvoice->uuid ? ' collapsed-box' : ''); ?>">
    <div class="box-header with-border">
        <h5 class="box-title">
            Invoice: <?= $paymentInvoice->uuid ?>, <?= $paymentInvoice->status ?> (<?= $paymentInvoice->details ?>)

            <?php if ($paymentInvoice->paymentBankInvoice) { ?>
                (<?php echo Html::a('Bank invoice', [
                    '/store/payment-bank-invoice/view', 'id' => $paymentInvoice->paymentBankInvoice->uuid
                ], ['target' => '_blank']); ?>)
            <?php } ?>
        </h5>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i
                        class="fa <?php echo($primaryInvoice->uuid === $paymentInvoice->uuid ? 'fa-minus' : 'fa-plus'); ?>"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <?php echo \yii\widgets\DetailView::widget(
            [
                'model'      => $paymentInvoice,
                'attributes' => [
                    'created_at',
                    'date_expire',
                    [
                        'label'  => 'Discount',
                        'format' => 'raw',
                        'value'  => function (PaymentInvoice $invoice) {
                            if ($invoice->hasUsedPromoCode()) {
                                $discount = displayAsMoney($invoice->promocodeUsage->getAmountTotal());

                                $code = Html::a($invoice->promocodeUsage->promocode->code, [
                                    '/promocode/promocode/view',
                                    'id' => $invoice->promocodeUsage->promocode->id
                                ], [
                                    'target' => '_blank'
                                ]);

                                return $discount . ' (' . $code . ')';
                            }

                            return null;
                        }
                    ],
                    [
                        'label'  => 'Affiliate award',
                        'format' => 'raw',
                        'value'  => function (PaymentInvoice $invoice) {
                            if ($invoice->getLinkedAffiliateAward()) {
                                return Html::a(H($invoice->getLinkedAffiliateAward()->getTextDescription()), [
                                    '/affiliate/affiliate-award/view',
                                    'id' => $invoice->getLinkedAffiliateAward()->id
                                ], [
                                    'target' => '_blank'
                                ]);
                            }
                            return null;
                        }
                    ],
                    [
                        'attribute' => 'total_amount',
                        'format'    => 'raw',
                        'value'     => function (PaymentInvoice $invoice) {
                            $totalAmount = displayAsMoney($invoice->getAmountTotal());
                            if ($invoice->hasUsedPromoCode()) {
                                return $totalAmount . ' (without promo code: ' . displayAsMoney($invoice->getAmountTotalWithoutPromoCode()) . ')';
                            }

                            if ($invoice->getAmountBonus()) {
                                return $totalAmount . ' <span style="background-color:yellow">(BONUS PAYED)</span>';
                            }
                            return $totalAmount;
                        }
                    ],
                    [
                        'attribute' => 'currency',
                    ],
                    [
                        'attribute' => 'bonus_accrued',
                        'format'    => 'raw',
                        'value'     => function (PaymentInvoice $invoice) {
                            return 'From Ts: ' . $invoice->getAmountBonusAccruedFromTs()?->getAmount() . ' / From Ps: ' . $invoice->getAmountBonusAccruedFromPs()?->getAmount();
                        }
                    ],
                    [
                        'label'  => 'Amount Total With Refund',
                        'format' => 'raw',
                        'value'  => function (PaymentInvoice $invoice) {
                            if ($invoice->hasUsedPromoCode()) {
                                return displayAsMoney($invoice->getAmountTotalWithRefund()) . ' (without promo code: ' . displayAsMoney($invoice->getAmountTotalWithoutPromoCodeWithRefund()) . ')';
                            }

                            return displayAsMoney($invoice->getAmountTotalWithRefund());
                        }
                    ],
                    [
                        'label'  => 'Status',
                        'format' => 'raw',
                        'value'  => function (PaymentInvoice $invoice) {
                            $stext = H($invoice->status);
                            if ($invoice->allowManualCancel() && 0) {
                                $stext .= ' <a class="btn btn-primary" title="Change only invoice status!" href="/store/store-order/manual-cancel-invoice?uuid=' . $invoice->uuid . '">Manual cancel</a>';
                            }
                            return $stext;
                        }
                    ],
                    'details',
                    [
                        'label'  => 'Accounting',
                        'value'  => $this->render('paymentInvoiceAccounting', ['accounting' => $paymentInvoice->accounting]),
                        'format' => 'raw'
                    ],
                    [
                        'label'  => 'Client receipt (pdf)',
                        'format' => 'raw',
                        'value'  => function (PaymentInvoice $invoice) {
                            return ' <a title="View" href="/store/payment/view-invoice?uuid=' . $invoice->uuid . '" target="_blank">View</a>';
                        }
                    ],
                    [
                        'label'  => 'Pay via braintree card',
                        'format' => 'raw',
                        'value'  => function (PaymentInvoice $invoice) {
                            if (!$invoice->allowPay()) {
                                return '';
                            }
                            return ' <a title="View" href="'.param('siteUrl').'/store/payment/pay-invoice?invoiceUuid=' . $invoice->uuid . '&defaultPayCardVendor=braintree" target="_blank">Braintree payment</a>';
                        }
                    ],
                ]
            ]);
        ?>
        <br>
        <?php
        if ($paymentInvoice->payments) {
            echo $this->render('payments', ['payments' => $paymentInvoice->payments]);
        }
        ?>
    </div>
</div>
