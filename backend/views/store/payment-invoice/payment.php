<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.10.18
 * Time: 17:20
 */

use backend\models\Backend;
use backend\modules\payment\assets\RefundPopupAsset;
use common\models\PaymentDetailOperation;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use yii\bootstrap\Html;

/** @var $payment \common\models\Payment */

\yii\widgets\DetailView::widget(
    [
        'model'      => $payment,
        'attributes' => [
            'id' => [
                'label' => 'Payment id',
                'value' => $payment->id
            ],
            'created_at',
            'description',
            'status'
        ]
    ]);

$this->registerAssetBundle(RefundPopupAsset::class);


$paymentCorrectUrl = '/store/payment-correction/create?paymentId=' . $payment->id . '&userId=' . $payment->paymentInvoice->user_id;

?>
<h4 style="padding-top: 30px;">Payment details
    <small><a href="<?= $paymentCorrectUrl ?>" target="_blank">correct <?= $payment->id ?></a></small>
</h4>
<div style="padding: 0 20px 0 20px;">
    <?php
    /** @var PaymentDetailOperation $paymentDetailOperation */
    foreach ($payment->getPaymentDetailOperations()->sort()->all() as $paymentDetailOperation) {
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="headerBlock">
                    <?= $paymentDetailOperation->created_at ?>: <b><?= $paymentDetailOperation->uuid ?></b>
                </div>
            </div>
            <div class="col-md-12">
                <?php
                foreach ($paymentDetailOperation->paymentDetails as $paymentDetail) {
                    ?>
                    <div class="row underlineRow">
                        <div class="col-md-1"><?= $paymentDetail->id ?></div>
                        <div class="col-md-3"><?= Backend::displayUser($paymentDetail->paymentAccount->user) ?> (<b><?= $paymentDetail->paymentAccount->type ?></b>)</div>
                        <div class="col-md-2"><?= displayAsMoney($paymentDetail->getMoneyAmount()) ?></div>
                        <div class="col-md-6"><?= $paymentDetail->description ?></div>

                        <?php if ($paymentTransactionHistory = $paymentDetail->getVendorTransactionHistory()) { ?>
                            <div class="col-md-12">
                                <b>Transaction history:</b>
                                <div class="row">
                                    <div class="col-md-2">
                                        <b>ID:</b> <?= $paymentTransactionHistory->id ?>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Vendor:</b> <?= $paymentTransactionHistory->transaction->vendor ?> / <?= $paymentTransactionHistory->transaction->transaction_id ?>
                                    </div>
                                    <div class="col-md-2">
                                        <b>Action: </b>
                                        <?= $paymentTransactionHistory->action_id ?>
                                    </div>
                                    <div class="col-md-5">
                                        <b>Comment: </b>
                                        <?php
                                        foreach ($paymentTransactionHistory->comment as $key => $value) {
                                            if (is_array($value) && array_key_exists('from', $value)) {
                                                echo $key . ': ' . json_encode($value['from']) . ' => ' . json_encode($value['to']) . '<br>';
                                            } else {
                                                echo $key . ': ' . json_encode($value);
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<h4 style="padding-top: 30px;">Vendor transactions</h4>
<div style="padding: 0 20px 0 20px;">
    <div class="row underlineRow hidden-xs hidden-sm">
        <div class="col-md-2 col-sm-6"><b>ID</b></div>
        <div class="col-md-1 col-sm-6"><b>Vendor</b></div>
        <div class="col-md-1 col-sm-6"><b>Type</b></div>
        <div class="col-md-3 col-sm-6"><b>VendorID</b></div>
        <div class="col-md-1 col-sm-6"><b>Created</b></div>
        <div class="col-md-1 col-sm-6"><b>Amount</b></div>
        <div class="col-md-2 col-sm-6"><b>Status</b></div>
        <div class="col-md-1 col-sm-6"></div>
    </div>

    <?php
    /** @var PaymentTransaction $paymentTransaction */
    foreach ($payment->getVendorTransactions()->sort()->all() as $paymentTransaction) {
        ?>
        <div class="row underlineRow" style="min-width: 950px;">
            <div class="col-md-2 col-sm-6">
                <b class="visible-xs-inline visible-sm-inline">ID: </b>
                <?= $paymentTransaction->id ?>
            </div>
            <div class="col-md-1 col-sm-6">
                <b class="visible-xs-inline visible-sm-inline">Vendor: </b><?= $paymentTransaction->vendor ?>
            </div>
            <div class="col-md-1 col-sm-6">
                <b class="visible-xs-inline visible-sm-inline">Type: </b><?= $paymentTransaction->type ?>
            </div>
            <div class="col-md-3 col-sm-6">
                <b class="visible-xs-inline visible-sm-inline">VendorID: </b><?= $paymentTransaction->transaction_id ?>
            </div>
            <div class="col-md-1 col-sm-6">
                <b class="visible-xs-inline visible-sm-inline">Created: </b>
                <?= $paymentTransaction->created_at ?>
            </div>
            <div class="col-md-1 col-sm-6">
                <b class="visible-xs-inline visible-sm-inline">Amount: </b>
                <?= displayAsMoney($paymentTransaction->getMoneyAmount()) ?>
            </div>
            <div class="col-md-2 col-sm-6">
                <b class="visible-xs-inline visible-sm-inline">Status: </b>
                <?= H($paymentTransaction->status) ?>
            </div>
            <div class="col-md-1 col-sm-6">
                <a href="/store/payment-transaction/refresh?id=<?= $paymentTransaction->id ?>">Refresh</a><br>
                <?php if ($paymentTransaction->canBeCanceled()) { ?>
                    <a href="/store/payment-transaction/void?id=<?= $paymentTransaction->id ?>" data-message='Are you sure?'>Void</a><br>
                <?php } ?>
                <?php if ($paymentTransaction->canBeSettle()) { ?>
                    <a href="/store/payment-transaction/settle?id=<?= $paymentTransaction->id ?>" data-message='Are you sure?'>Settle</a><br>
                <?php } ?>
                <a href="/payment/log/index?PaymentLogSearch%5Bsearch_markers%5D=<?= $paymentTransaction->transaction_id ?>">Log</a>
                <?php if ($paymentTransaction->canBeRefunded()) {
                    echo '<br>';
                    echo Html::button('Refund', [
                        'class'       => 'btn btn-sm btn-warning btn-ajax-modal',
                        'title'       => 'Refund Request',
                        'value'       => yii\helpers\Url::toRoute(['store/payment-transaction/request-refund', 'id' => $paymentTransaction->id]),
                        'data-target' => '#model_refund',
                    ]);

                } ?>
            </div>
            <div class="col-md-12">
                <b>History:</b>
                <div style="padding: 0 20px 0 20px;">
                    <div class="row underlineRow hidden-xs hidden-sm">
                        <div class="col-md-2"><b>ID</b></div>
                        <div class="col-md-2"><b>PaymentDetail</b></div>
                        <div class="col-md-2"><b>Date</b></div>
                        <div class="col-md-1"><b>Action</b></div>
                        <div class="col-md-3"><b>Comment</b></div>
                        <div class="col-md-2"><b>User</b></div>
                    </div>
                    <?php
                    /** @var PaymentTransactionHistory $paymentTransactionHistory */
                    foreach ($paymentTransaction->getPaymentTransactionHistories()->sort()->all() as $paymentTransactionHistory) { ?>
                        <div class="row underlineRow">
                            <div class="col-md-2">
                                <b class="visible-xs-inline visible-sm-inline">ID:</b>
                                <?= $paymentTransactionHistory->id ?>
                            </div>
                            <div class="col-md-2">
                                <b class="visible-xs-inline visible-sm-inline">PaymentDetail: </b>
                                <?= $paymentTransactionHistory->payment_detail_id ?>
                            </div>
                            <div class="col-md-2">
                                <b class="visible-xs-inline visible-sm-inline">Date: </b>
                                <?= $paymentTransactionHistory->created_at ?>
                            </div>
                            <div class="col-md-1">
                                <b class="visible-xs-inline visible-sm-inline">Action: </b>
                                <?= $paymentTransactionHistory->action_id ?>
                            </div>
                            <div class="col-md-3">
                                <b class="visible-xs-inline visible-sm-inline">Comment: </b>
                                <?php
                                foreach ($paymentTransactionHistory->comment as $key => $value) {
                                    if (is_array($value) && array_key_exists('from', $value)) {
                                        echo $key . ': ' . json_encode($value['from']) . ' => ' . json_encode($value['to']) . '<br>';
                                    } else {
                                        echo $key . ': ' . json_encode($value);
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-2">
                                <b class="visible-xs-inline visible-sm-inline">User: </b>
                                <?= $paymentTransactionHistory->user->username ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>

</div>
