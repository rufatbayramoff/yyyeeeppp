<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreUnit */
/* @var $form yii\widgets\ActiveForm */
/* @var $rejectForm backend\models\store\StoreUnitRejectForm */
?>

<div class="store-unit-rejectform">
    <?php $form = ActiveForm::begin(); ?> 
    <?php
    echo $form->field($rejectForm, 'reasonSuggest')->widget(kartik\widgets\Select2::classname(), [
        'data' => $rejectForm->getSuggestList(),
        'options' => ['placeholder' => 'Select', 'style' => 'width:100%'], 
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    echo $form->field($rejectForm, 'reasonDescription')->textarea(['maxlength' => true]);
    ?>
    <div class="form-group">
        <?= Html::submitButton('Reject publish', ['class' =>   'btn btn-warning ajax-submit']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
