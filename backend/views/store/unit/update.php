<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreUnit */

$this->title = 'Update Store Unit: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-unit-update">

    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
