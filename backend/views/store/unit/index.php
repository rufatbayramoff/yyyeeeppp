<?php

use common\interfaces\Model3dBaseInterface;
use common\modules\product\models\ProductBase;
use frontend\models\model3d\Model3dFacade;
use lib\money\Currency;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Units';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-unit-index">
    <p>
        <?= Html::a('Create Store Unit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget(
        [
            'export'       => false,
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                [
                    'attribute'      => 'id',
                    'value'          => 'id',
                    'contentOptions' => ['style' => 'width: 80px;']
                ],
                [
                    'attribute'      => 'model3d_id',
                    'contentOptions' => ['style' => 'width: 80px;']
                ],
                [
                    'format' => 'raw',
                    'value'  => function ($model) {
                        $model3dObj = $model->model3d;
                        $cover = Model3dFacade::getCover($model3dObj);
                        return Html::img($cover['image'], ['width' => 90]);
                    }
                ],
                [
                    'label'     => 'User',
                    'attribute' => 'user_id',
                    'format'    => 'raw',
                    'value'     => function ($model) {
                        $user = \backend\models\Backend::displayUser($model->model3d->user);
                        return $user;
                    }
                ],
                [
                    'format'    => 'raw',
                    'attribute' => 'title',
                    'value'     => function ($model) {
                        $url = \yii\helpers\Url::toRoute(['store/store-unit/view', 'id' => $model->id]);
                        $tags = $model->model3d->model3dTags;
                        $tagList = [];
                        foreach ($tags as $tag) {
                            $tagList[] = yii\helpers\Html::a(\H($tag->tag->text), '#' . $tag->tag->id, ['class' => 'label label-default']);
                        }
                        $tagListHtml = implode(", ", $tagList);
                        $modelTitle = \H($model->model3d->title);
                        $link = Html::a($modelTitle, $url, ['data-pjax' => 0, 'class' => 'btn-lg']);
                        return $link . '<br />Tags: ' . $tagListHtml;
                    }
                ],
                /*
            [
                'attribute' => 'custom_code_id',
                'value' => function($model) {
                    $customCodeId = $model->custom_code_id;

                    if(is_null($customCodeId)) {
                        return $customCodeId;
                    }
                    else {
                        return \common\models\CustomCode::findByPk($customCodeId)->title;
                    }
                }
            ], */
                [
                    'format'    => 'raw',
                    'attribute' => 'price',
                    'value'     => function (\common\models\StoreUnit $model) {
                        $perPrint = displayAsMoney($model->model3d->getPriceMoneyByQty(1));
                        return sprintf("Per print: %s", $perPrint);
                    }
                ],
                'rating',
                [
                    'label' => 'Updated at',
                    'value' => function ($model) {
                        return $model->model3d->updated_at;
                    }
                ],
                [
                    'label'  => 'Status',
                    'value'  => function ($model) {
                        return $model->model3d->getProductStatusLabel();
                    },
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'product_status',
                        ProductBase::getProductStatusLabels(),
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                ],
                [
                    'label'     => 'Source',
                    'attribute' => 'source',
                    'value'     => function ($model) {
                        return $model->model3d->source;
                    },
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'source',
                        array_merge([''], array_combine(Model3dBaseInterface::POSSIBLE_SOURCES, Model3dBaseInterface::POSSIBLE_SOURCES)),
                        ['class' => 'form-control']
                    ),
                ],
                [
                    'attribute' => 'published_at',
                    'value'     => 'model3d.published_at',
                    'format'    => ['datetime'],
                ],
                [
                    'attribute' => 'moderated_at',
                    'value'     => 'model3d.moderated_at',
                    'format'    => ['datetime'],
                ],
                [
                    'format' => 'raw',
                    'value'  => function ($model) {
                        $url = yii\helpers\Url::toRoute(['store/store-unit/view', 'id' => $model->id]);
                        $txt1 = yii\helpers\Html::a('View', $url, ['class' => 'btn btn-primary', 'target' => '_blank']);
                        $txt2 = '';

                        $url = yii\helpers\Url::toRoute(['store/store-unit/view', 'id' => $model->id, 'target' => '_blank', 'moderate' => 1]);
                        $txt2 = yii\helpers\Html::a('Moderate', $url, ['class' => 'btn btn-ghost btn-warning', 'target' => '_blank']);

                        return $txt1 . $txt2;
                    }
                ]
            ],
        ]
    ); ?>

</div>
