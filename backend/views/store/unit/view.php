<?php

use backend\components\AdminAccess;
use backend\components\columns\PsColumn;
use backend\models\Backend;
use backend\models\store\StoreUnitForm;
use common\components\ArrayHelper;
use common\components\ps\locator\materials\MaterialColorsItem;
use common\models\ProductCategory;
use common\modules\product\interfaces\ProductInterface;
use common\models\FileJob;
use common\models\PrinterColor;
use common\models\PrinterMaterialGroup;
use common\models\StoreOrderReview;
use common\models\StoreUnit;
use common\modules\product\repositories\ProductCategoryRepository;
use common\services\MeasureService;
use common\services\Model3dPartService;
use common\services\Model3dService;
use console\jobs\model3d\AntivirusJob;
use frontend\models\model3d\Model3dFacade;
use kartik\select2\Select2;
use lib\collection\CollectionDb;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoreUnit */
/* @var $model3d common\models\Model3d */
/* @var $moderate bool */
/* @var $log array */
/* @var $modelHistory array */

$model3d                       = \common\models\Model3d::tryFindByPk($model->model3d_id);
$this->title                   = $model3d->title;
$this->params['breadcrumbs'][] = ['label' => 'Store Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$files       = Model3dService::getModel3dActivePartsAndImagesList($model3d);
$modelParts  = $model3d->getActiveModel3dParts();
$tags        = $model3d->tags;
$renders     = frontend\models\model3d\Model3dFacade::getRenderedImages($modelParts, $model3d);
$renderedMap = \lib\collection\CollectionDb::getMap($renders, 'fileId');

$fileIds = array_keys($files);
/** @var \common\models\FileJob[] $fileJobs */
$fileJobs = \common\models\FileJob::find()->where(['file_id' => $fileIds])->orderBy('id desc')->all();

/** @var FileJob $fileJobAntivirus */
$fileJobAntivirus = CollectionDb::getRow($fileJobs, ['operation' => 'antivirus']);
$canDownload      = \backend\components\AdminAccess::can('model3d.download');

$changeTextureFormModel = Yii::createObject(\backend\models\store\ChangeStoreUnitTextureForm::class, [$model]);
$groupsAndColors        = ArrayHelper::toArray(
    $changeTextureFormModel->getAllowedMaterialsGroupsAndColors(),
    [
        MaterialColorsItem::class   => ['materialGroup', 'colors'],
        PrinterMaterialGroup::class => ['id', 'title'],
        PrinterColor::class         => ['id', 'title']
    ]
);

$findSameFilesByMd5 = function (\common\interfaces\Model3dBasePartInterface $model3dPart, $userId) {
    $file         = $model3dPart->file;
    $otherFiles   = \common\models\File::find()
        ->joinWith('model3dParts')
        ->joinWith('model3dParts.model3d')
        ->md5($file->md5sum)
        ->andWhere(['!=', 'model3d_part.id', $model3dPart->id])
        ->andWhere(['!=', 'file.user_id', $userId]);
    $dataProvider = new \yii\data\ActiveDataProvider([
        'query' => $otherFiles,
    ]);
    return $dataProvider;
}
?>
<div style="padding-bottom: 10px;">
    <?php
    $html       = "<br />";
    $canPublish = true;
    try {
        $canPublish = \common\models\StoreUnit::getCanBePublished($model);
    } catch (\Exception $e) {
        $canPublish = false;
        echo "<span style='color:red'>" . $e->getMessage() . ' - cannot be published.</span>';
    }
    $notPublished = $model3d->product_status !== ProductInterface::STATUS_PUBLISHED_PUBLIC;
    if ($canPublish && $notPublished && AdminAccess::can('storeunit.publish')) {
        $url           = \yii\helpers\Url::toRoute(['store/store-unit/publish', 'id' => $model->id]);
        $publishButton = Html::a('Publish', $url, ['class' => 'btn btn-primary', 'style' => 'margin-right:10px', 'data-pjax' => 0, 'data-method' => 'post']);
        $html          .= $publishButton;
    }
    if ($model3d->is_active && $model3d->product_status !== \common\modules\product\interfaces\ProductInterface::STATUS_REJECTED) {
        $urlReject    = \yii\helpers\Url::toRoute(['store/store-unit/publish-reject', 'id' => $model->id]);
        $rejectButton = Html::button(
            'Reject',
            [
                'title'       => 'Reject reason',
                'class'       => 'btn btn-warning btn-ajax-modal',
                'data-pjax'   => 0,
                'value'       => $urlReject,
                'data-target' => '#model_storereject'
            ]
        );
        if (AdminAccess::can('storeunit.reject')) {
            $html .= $rejectButton;
        }
    }


    if (!$model3d->is_active) {
        $urlReject     = \yii\helpers\Url::toRoute(['store/store-unit/restore-model', 'id' => $model->id]);
        $restoreButton = Html::a('Restore', $urlReject, ['class' => 'btn btn-success']);

        if (AdminAccess::can('storeunit.reject')) {
            $html .= '&nbsp;' . $restoreButton;
        }
    }


    if ($moderate) {
        echo $html;
    }
    ?>
</div>

<div class="store-unit-view">

    <div class="row">
        <div class="col-xs-6">


            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <h3 class="box-title">3D Model Information:</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">

                            <?php
                            echo DetailView::widget(
                                [
                                    'model'      => $model3d,
                                    'attributes' => [
                                        [
                                            'format'  => 'raw',
                                            'label'   => 'Website',
                                            'value'   => Html::a($model3d->id . " (Website)", $model3d->getPublicPageUrl([], true), ['target' => '_blank']),
                                            'visible' => $model3d->isPublished(),
                                        ],
                                        'id',
                                        [
                                            'label' => 'Store unit Id',
                                            'value' => $model3d->storeUnit->id
                                        ],
                                        'title',
                                        [
                                            'format' => 'raw',
                                            'label'  => 'User',
                                            'value'  => \backend\models\Backend::displayUser($model3d->user_id),
                                        ],
                                        'description',
                                        'product_status',
                                        'is_active',
                                        'stat_views',
                                        'source',
                                        [
                                            'label' => 'Source details',
                                            'value' => json_encode($model3d->source_details)
                                        ],
                                        'source',
                                        [
                                            'label'  => 'Api external system',
                                            'format' => 'raw',
                                            'value'  => function ($model3d) {
                                                $printablePacks   = $model3d->apiPrintablePacks;
                                                $apiPrintablePack = reset($printablePacks);
                                                $label            = '';
                                                if ($apiPrintablePack) {
                                                    $label = 'Api external system: ' . $apiPrintablePack->apiExternalSystem->id . ' ' . PsColumn::getMenu($apiPrintablePack->apiExternalSystem->bindedUser->company);
                                                }
                                                return $label;
                                            }
                                        ],
                                        [
                                            'format' => 'raw',
                                            'label'  => 'CAE',
                                            'value'  => $model3d->cae . Html::a(' Change ', '/store/store-unit/change-cae?id=' . $model->id),
                                        ],
                                        'created_at:datetime',
                                        'moderated_at:datetime',
                                        [
                                            'label' => 'Published at',
                                            'value' => $model3d->isPublished() ? $model3d->published_at : ''
                                        ],
                                        [
                                            'label' => 'Rating',
                                            'value' => $model3d->storeUnit->rating,
                                        ],
                                        [
                                            'label' => 'Price Per Print',
                                            'value' => displayAsMoney($model3d->getPriceMoneyByQty(1)),
                                        ],
                                        [
                                            'label' => 'Tags',
                                            'value' => function ($model) {
                                                $retVal = '';
                                                foreach ($model->getProductTags() as $tag) {
                                                    $retVal .= Html::a(H($tag->text), '#' . $tag->id) . ', ';
                                                }
                                                return $retVal;
                                            }

                                        ],
                                    ]
                                ]
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-solid box-default">
                <div class="box-header">Attributes
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?php
                    $form = \yii\widgets\ActiveForm::begin(
                        [
                            'action' => ['change', 'id' => $model->id]
                        ]
                    );
                    ?>

                    <?php
                    $productCategory = $model->model3d->productCategory;
                    if (!$productCategory) {
                        $productCategory = new ProductCategory();
                    }
                    $productCategoryRepository = Yii::createObject(ProductCategoryRepository::class);
                    $categoriesMap             = $productCategoryRepository->getFinalMap();
                    echo $form->field($productCategory, 'id')->widget(\kartik\select2\Select2::classname(), [
                        'data'          => $categoriesMap,
                        'options'       => ['placeholder' => 'Select'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Product category');
                    ?>


                    <div class="text-right" style="padding: 10px; border-top: 1px solid #eeeeee">
                        <button type="submit" class="btn btn-success btn-sm">Save</button>
                    </div>
                    <?php \yii\widgets\ActiveForm::end(); ?>
                </div>
            </div>
            <div class="box box-solid box-default">
                <div class="box-header">Colors and materials
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body" style="padding: 0">

                    <?php
                    $form = \yii\widgets\ActiveForm::begin(
                        [
                            'action' => ['change-texture', 'id' => $model->id]
                        ]
                    );
                    ?>

                    <table class="table table-hover" id="design-params">

                        <thead>
                        <tr>
                            <th>File</th>
                            <th>Material group</th>
                            <th>Color</th>
                        </tr>
                        </thead>

                        <tbody>

                        <tr id="for-all-model">
                            <td class="model3d-area">
                                <?= $changeTextureFormModel->canModifyParts() ? $form->field($changeTextureFormModel, 'forAllFiles')->checkbox() : 'For all model' ?>
                            </td>
                            <td>
                                <?=
                                $form->field($changeTextureFormModel, 'allFilesTexture[groupId]')
                                    ->label(false)
                                    ->dropDownList($changeTextureFormModel->getMaterialGropusOptions(), ['class' => 'material-group-select'])
                                ?>
                            </td>
                            <td>
                                <?=
                                $form->field($changeTextureFormModel, 'allFilesTexture[colorId]')
                                    ->label(false)
                                    ->dropDownList(
                                        $changeTextureFormModel->getColorOptions($changeTextureFormModel->getModelOneTexture()),
                                        ['class' => 'material-color-select']
                                    );
                                ?>
                            </td>
                        </tr>

                        <?php if ($changeTextureFormModel->canModifyParts()): ?>

                            <?php
                            foreach ($modelParts as $modelPart):
                                /** @var \common\models\model3d\RenderInfo $render */
                                $render = isset($renderedMap[$modelPart->file->id]) ? $renderedMap[$modelPart->file->id] : '';
                                ?>
                                <tr class="model-paret-row">
                                    <td>
                                        <?= Html::a(Html::img($render->url, ['width' => 100]), $render->url, ['class' => 'jspopup']); ?>
                                    </td>
                                    <td>
                                        <?=
                                        $form->field($changeTextureFormModel, "filesTextures[$modelPart->id][groupId]")
                                            ->label(false)
                                            ->dropDownList($changeTextureFormModel->getMaterialGropusOptions(), ['class' => 'material-group-select'])
                                        ?>
                                    </td>
                                    <td>
                                        <?=
                                        $form->field($changeTextureFormModel, "filesTextures[$modelPart->id][colorId]")
                                            ->label(false)
                                            ->dropDownList($changeTextureFormModel->getColorOptions($modelPart->model3dTexture), ['class' => 'material-color-select'])
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>


                        <?php endif; ?>


                        </tbody>
                    </table>

                    <div class="text-right" style="padding: 10px; border-top: 1px solid #eeeeee">
                        <button type="submit" class="btn btn-success btn-sm">Save</button>
                    </div>

                    <?php \yii\widgets\ActiveForm::end(); ?>
                </div>
            </div>
            <div class="box box-solid box-default">
                <div class="box-header">Review
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= \backend\widgets\OrderReviewList::widget(['query' => StoreOrderReview::find()->forStoreUnit($model)]); ?>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">3D Model files:</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <table class="table table-hover panel-body">
                        <thead>
                        <tr>
                            <th> Click to rotate</th>
                            <th>Model&File</th>
                            <th>Title</th>
                            <th>Qty</th>
                            <th>Size</th>
                            <th>Format</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        /** @var \common\models\Model3dPart $modelPart */
                        foreach (ArrayHelper::index($model3d->model3dParts, 'file_id') as $modelPart):
                            if (!($props = $modelPart->model3dPartProperties)) {
                                $props = Model3dPartService::getParserInfo($modelPart);
                            }
                            /** @var \common\models\model3d\RenderInfo $render */
                            $render       = isset($renderedMap[$modelPart->file->id]) ? $renderedMap[$modelPart->file->id] : false;
                            $render360    = param('server') . '/catalog/model3d/preview360?model3dPartId=' . $modelPart->id;
                            $fileDoublsDp = $findSameFilesByMd5($modelPart, $model3d->user_id);
                            ?>
                            <tr class="<?= $modelPart->isActive() ? '' : 'unactive-mode-part' ?>">

                                <td class="model3d-area">

                                    <?php if ($modelPart->isActive()): ?>

                                        <?php
                                        if (!$modelPart->isMulticolorFormat()) {
                                            ?>
                                            <table>
                                                <tr align="center">
                                                    <td>

                                                    </td>
                                                    <td>
                                                        <a href="<?php
                                                        echo Url::toRoute(
                                                            [
                                                                'store/store-unit/view',
                                                                'id'          => $model->id,
                                                                'moderate'    => $moderate,
                                                                'model3dPart' => $modelPart->id,
                                                                'rotate'      => 'up'
                                                            ]
                                                        );
                                                        ?>" data-method="post"><span class="fa fa-arrow-up"></span></a>
                                                    </td>
                                                    <td>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td>

                                                        <a href="<?php
                                                        echo Url::toRoute(
                                                            [
                                                                'store/store-unit/view',
                                                                'id'          => $model->id,
                                                                'moderate'    => $moderate,
                                                                'model3dPart' => $modelPart->id,
                                                                'rotate'      => 'right'
                                                            ]
                                                        );
                                                        ?>" data-method="post"><span class="fa fa-repeat"></span></a>
                                                    </td>
                                                    <td>
                                                        <?= $render ? Html::a(Html::img($render->url, ['width' => 80]), $render->url, ['class' => 'jspopup']) : '' ?>
                                                    </td>
                                                    <td>
                                                        <a href="<?php
                                                        echo Url::toRoute(
                                                            [
                                                                'store/store-unit/view',
                                                                'id'          => $model->id,
                                                                'moderate'    => $moderate,
                                                                'model3dPart' => $modelPart->id,
                                                                'rotate'      => 'left'
                                                            ]
                                                        );
                                                        ?>" data-method="post"><span class="fa fa-undo"></span></a>
                                                    </td>
                                                </tr>
                                                <tr align="center">
                                                    <td>

                                                    </td>
                                                    <td>
                                                        <a href="<?php
                                                        echo Url::toRoute(
                                                            [
                                                                'store/store-unit/view',
                                                                'id'          => $model->id,
                                                                'moderate'    => $moderate,
                                                                'model3dPart' => $modelPart->id,
                                                                'rotate'      => 'down'
                                                            ]
                                                        );
                                                        ?>" data-method="post"><span
                                                                    class="fa fa-arrow-down"></span></a>
                                                    </td>
                                                    <td>
                                                    </td>

                                                </tr>
                                            </table>
                                            <form method="get" action="<?php
                                            echo \yii\helpers\Url::toRoute(['store/store-unit/view']);
                                            ?>">
                                                <input type="hidden" name="id" value="<?= $model->id ?>"/>
                                                <input type="hidden" name="moderate" value="<?= $moderate ?>"/>
                                                <input type="hidden" name="model3dPart"
                                                       value="<?php echo $modelPart->id; ?>"/>
                                                Scale by:
                                                <input class="rangeslideroff" type="text" size="2" name="zoom"
                                                       value="1"/>
                                                <button>
                                                    <i class="fa fa-search-plus"> </i>
                                                </button>
                                            </form>
                                            <?php
                                        } else {
                                            echo Html::a(Html::img($render->url, ['width' => 100]), $render->url, ['class' => 'jspopup']);
                                        }
                                        ?>


                                    <?php /* if file deleted */ else : ?>

                                        <?php
                                        $render = Model3dFacade::getRenderedImages([$modelPart], $model3d);
                                        $render = $render ? reset($render) : null;
                                        echo $render ? Html::a(Html::img($render->url, ['width' => 100]), $render->url, ['class' => 'jspopup']) : '';
                                        ?>

                                    <?php endif; ?>

                                </td>

                                <td><?php echo $modelPart->id; ?>
                                    <br/><?php echo $modelPart->file->id;

                                    if ($modelPart->isActive()) {
                                        echo '&nbsp; <br />', Html::a('View 3D', $render360, ['class' => 'jspopup']);
                                    }

                                    ?>

                                </td>
                                <td>
                                    <?php
                                    if ($modelPart->fileSrc): ?>

                                        <a href="<?php
                                        echo \yii\helpers\Url::toRoute(
                                            [
                                                'store/store-unit/view',
                                                'id'          => $model->id,
                                                'moderate'    => $moderate,
                                                'model3dPart' => $modelPart->id,
                                                'revert'      => 'true'
                                            ]
                                        );
                                        ?>" data-method="post">Revert to original <span
                                                    class="fa fa-retweet"></span></a> <br/>

                                    <?php endif; ?>

                                    <?php if (!empty($render['model3dUrl'])): ?>
                                        <b><?php echo $file['name']; ?></b>
                                    <?php endif;
                                    if ($canDownload) {
                                        $link = ['store/store-unit/download-model', 'id' => $modelPart->file->id];
                                        echo '&nbsp; &nbsp; ' . Html::a(' Download', $link, ['class' => 'fa fa-download']);
                                    }
                                    ?>

                                    <br/>
                                    <span class="small">
                              <?php echo \frontend\components\Icon::get('resize'); ?>
                                        Size : <?= $modelPart->size ? $modelPart->size->toStringWithMeasurement('mm') : '' ?><br/>
                                        Original Size : <?= $modelPart->originalSize ? $modelPart->originalSize->toStringWithMeasurement('mm') : '' ?>
                          </span>
                                    <br/>
                                    <span class="small">
                              <?php echo \frontend\components\Icon::get('play'); ?>
                                        Faces : <?php echo $props['faces']; ?>
                                        &nbsp;,
                               Weight : <?= MeasureService::getWeightFormatted($modelPart->getWeight(), 'gr') ?>
                          </span>
                                </td>
                                <td>
                                    <form method="post"
                                          action="<?= Url::toRoute(['change-part-qty', 'id' => $model->id, 'modelPartId' => $modelPart->id]) ?>">
                                        <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>
                                        <input class="rangeslideroff" type="text" size="2" name="qty"
                                               value="<?= $modelPart->qty; ?>"/>
                                        <button type="submit">
                                            <i class="fa fa-check"> </i>
                                        </button>
                                    </form>
                                </td>
                                <td><?= Yii::$app->formatter->asShortSize($modelPart->file->size, 2); ?></td>
                                <td><?= $modelPart->file->extension ?></td>
                                <td>
                                    <?= $modelPart->isActive() ? Html::a('Delete', ['delete-part', 'id' => $model->id, 'modelPartId' => $modelPart->id]) : '' ?>
                                    <?= !$modelPart->isActive() ? Html::a('Restore', ['restore-part', 'id' => $model->id, 'modelPartId' => $modelPart->id]) : '' ?>
                                </td>
                            </tr>
                            <?php if (!empty($fileDoublsDp->getCount())): ?>
                            <tr>
                                <td colspan="8">
                                    <div class="box box-warning box-solid">
                                        <div class="box-header">
                                            <h3 class="box-title">File duplicates found</h3>
                                            <div class="box-tools pull-right">
                                                <button class="btn btn-box-tool" data-widget="collapse"><i
                                                            class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body no-padding"> <?= yii\grid\GridView::widget(
                                                [
                                                    'dataProvider' => $fileDoublsDp,
                                                    'columns'      => [
                                                        [
                                                            'attribute' => 'name',
                                                            'format'    => 'raw',
                                                            'value'     => function (\common\models\File $file) {
                                                                $parts = $file->model3dParts;
                                                                $part  = reset($parts);
                                                                if ($part && $part->model3d && $part->model3d->storeUnit) {
                                                                    $storeUnitId = reset($parts)->model3d->storeUnit->id;
                                                                    return Html::a($file->name, ['/store/store-unit/view', 'id' => $storeUnitId]);
                                                                }
                                                                return '';
                                                            }
                                                        ],
                                                        'size:size',
                                                        'created_at:date',
                                                        'deleted_at:date',
                                                        'user_id',
                                                        'status',
                                                    ],
                                                ]
                                            ); ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>
            <?php if (!empty($modelScreens)): ?>
                <div>Click to crop <span class="fa fa-crop"></span></div>
            <?php endif; ?>
            <?php
            foreach ($model3d->model3dImgs as $model3dImg):
                if (!empty($model3dImg->deleted_at)) {
                    continue;
                }
                echo '<div class="pull-left">';
                $imgUrl  = $model3dImg->file->getFileUrl();
                $imgCrop = '/store/store-unit/crop-image/?id=' . $model3dImg->id . '&fileid=' . $model3dImg->file_id;
                $style   = '';
                if ($model3dImg->file_id === $model3d->cover_file_id) {
                    $style = 'border: 2px solid red;';
                }
                $img = yii\helpers\Html::img(
                    $imgUrl,
                    [
                        'width'  => 100,
                        'alt'    => H($model3dImg->file->getFileName()),
                        'hspace' => 3,
                        'style'  => $style
                    ]
                );

                echo Html::a($img, $imgCrop, ['class' => 'jspopup']);
                echo "</div>\n";
            endforeach;

            ?>

            <br clear="all"/>
            <?php //@TODO - replace with Grid ?>
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">File jobs</h3>
                    <div class="box-tools pull-right">
                        <?= Html::a('Restart All', ['repeat-all-jobs', 'model3dId' => $model3d->id], ['class' => 'btn btn-xs btn-success', 'data-method' => 'post']) ?>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-hover panel-body">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>File ID</th>
                            <th>Operation</th>
                            <th>Status</th>
                            <th>Started</th>
                            <th>Finished</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($fileJobs as $k => $v): ?>
                            <tr>
                                <td><a href="/model3d/jobs/view?id=<?= $v['id']; ?>"><?= $v['id']; ?></a></td>
                                <td><?php echo $v['file_id']; ?> </td>
                                <td><?php echo $v['operation']; ?></td>
                                <td><?php echo $v['status']; ?></td>
                                <td><?php echo $v['created_at']; ?></td>
                                <td><?php echo $v['finished_at']; ?></td>
                                <td><?= Html::a('Repeat', ['repeat-job', 'jobId' => $v->id], ['class' => 'btn btn-xs btn-success', 'data-method' => 'post']) ?></td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <div class="result-wrapper">
                                        <a href="#"
                                           onclick="$(this).parent().find('.result-txt').toggle();return false;">Show/hide
                                            args and result</a>

                                        <div class="result-txt" style="display:none;">
                                            Arguments:<br>
                                            <?php pre(yii\helpers\Json::decode($v['args'])); ?>
                                            <br>Result:<br>
                                            <?php pre(yii\helpers\Json::decode($v['result'])); ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php if ($log['dataProvider']->getCount()) { ?>
            <div class="col-md-6">
                <h3>Moderation history</h3>
                <?= yii\grid\GridView::widget(
                    [
                        'dataProvider' => $log['dataProvider'],
                        'columns'      => [
                            'user_id',
                            'created_at:datetime',
                            'action',
                            'result:ntext'
                        ],
                    ]
                ); ?>
            </div>
        <?php } ?>
    </div>
</div>

<h3>3D Model history</h3>
<?= yii\grid\GridView::widget(
    [
        'dataProvider' => $modelHistory['dataProvider'],
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'user.username',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return Backend::displayUser($model->user);
                }
            ],
            'created_at:datetime',
            'action_id',
            'comment:ntext',
            [
                'attribute' => 'source',
                'value'     => function ($model) {
                    return is_array($model->source) ? json_encode($model->source) : $model->source;
                }
            ],
            [
                'attribute' => 'result',
                'value'     => function ($model) {
                    return is_array($model->result) ? json_encode($model->result) : $model->result;
                }
            ]
        ],
    ]
); ?>


<style type="text/css">

    select:disabled {
        opacity: .5;
    }

</style>


<script type="text/javascript">


    $(function () {
        var forAllModel = $('#for-all-model');
        var modelPartRows = $('.model-paret-row');

        var groupsAndColors = <?= \yii\helpers\Json::htmlEncode($groupsAndColors)?>;

        var updateForm = function () {
            var isForAllModel = forAllModel.find('input:checkbox').length == 0 || forAllModel.find('input:checkbox')[0].checked;
            if (isForAllModel) {
                modelPartRows.find('select').attr('disabled', 'disabled');
                forAllModel.find('select').removeAttr('disabled');
            } else {
                modelPartRows.find('select').removeAttr('disabled');
                forAllModel.find('select').attr('disabled', 'disabled');
            }
        };

        forAllModel.find('input:checkbox').on('change', updateForm);
        updateForm();

        $('.material-group-select').on('change', function () {
            var $this = $(this);
            var value = $this.val();

            var colorSelect = $this.closest('tr').find('.material-color-select');
            colorSelect.find('option').remove();
            colorSelect.val('');


            var data = groupsAndColors[value];

            if (data) {
                for (var k in data.colors) {
                    var color = data.colors[k];
                    $('<option>').attr('value', color.id).text(color.title).appendTo(colorSelect);
                }
            }
        });
    });

</script>

<style type="text/css">
    .unactive-mode-part {
        opacity: .5;
    }
</style>