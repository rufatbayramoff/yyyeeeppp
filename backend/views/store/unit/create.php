<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreUnit */

$this->title = 'Create Store Unit';
$this->params['breadcrumbs'][] = ['label' => 'Store Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-unit-create">
 
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
