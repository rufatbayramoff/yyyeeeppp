<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-transaction-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
             [
                'label' => 'User',
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function($model){
                    $user  = \backend\models\Backend::displayUser($model->user);
                    return $user;
                }
            ],
            [
                'label' => 'Order Id',
                'format' => 'raw',
                'value' => function(\common\models\PaymentTransaction $model){
                    if (
                        $model->firstPaymentDetail &&
                        $model->firstPaymentDetail->paymentDetailOperation &&
                        $model->firstPaymentDetail->paymentDetailOperation->payment &&
                        $model->firstPaymentDetail->paymentDetailOperation->payment->paymentInvoice
                    ) {
                        return $model->firstPaymentDetail->paymentDetailOperation->payment->paymentInvoice->store_order_id;
                    }
                    return null;
                }
            ],
            'vendor',
            [
                'attribute' => 'transaction_id',
                'format' => 'raw',
                'value' => function($model) {
                    $url = yii\helpers\Url::toRoute(['store/payment-transaction/view', 'id' => $model->id]);
                    return yii\helpers\Html::a($model->transaction_id, $url, ['style' => 'font-size: 15px;']);
                }
            ],
            'updated_at:datetime',
             'status',
             'type',
            [ 
                'format' => 'raw',
                'attribute' => 'amount',
                'value' => function($model){
                    $html = app('formatter')->asCurrency($model->amount, $model->currency);
                    return $html;
                }
            ],  
             'comment',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}']
 
        ],
    ]); ?>

</div>
