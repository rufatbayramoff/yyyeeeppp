<?php

use backend\models\search\PaymentTransactionHistorySearch;
use common\models\PaymentTransactionHistory;
use yii\helpers\VarDumper;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var PaymentTransactionHistorySearch $searchModelHistory
 * @var $model common\models\PaymentTransaction
 */

$this->title                   = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="payment-transaction-view">
    <?php echo DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'user_id',
            'transaction_id',
            'first_payment_detail_id',
            'vendor',
            'created_at',
            'updated_at',
            'status',
            'type',
            'amount',
            'currency',
            'comment',
            'is_notified_need_settled',
            'refund_id',
            'last_original_transaction',
            'payment' => [
                'format'  => 'raw',
                'label' => 'Payment',
                'value' => '<a href="/store/payment/view?id=' . $model->firstPaymentDetail->payment->id . '">' . $model->firstPaymentDetail->payment->id . '</a>'
            ]
        ],
    ]) ?>

    <?php echo \yii\grid\GridView::widget([
        'dataProvider' => $dataProviderHistory,
        'filterModel'  => $searchModelHistory,
        'columns'      => [
            'id',
            'payment_detail_id',
            'created_at',
            'action_id',
            [
                'attribute' => 'comment',
                'format'    => 'raw',
                'value'     => function (PaymentTransactionHistory $model) {
                    return $model->comment
                        ? VarDumper::dumpAsString($model->comment, 10, true)
                        : null;
                }
            ],
            'user_id',
        ],
    ]); ?>
</div>
