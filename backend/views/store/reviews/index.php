<?php

use common\models\StoreOrderReview;
use frontend\components\image\ImageHtmlHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-review-index">

    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                'order_id',
                [
                    'label'     => 'Creator',
                    'class'     => \backend\components\columns\UserColumn::class,
                    'attribute' => 'order.user'
                ],
                'created_at',
                [
                    'attribute' => 'comment',
                    'format' => 'raw',
                    'value'  => function (StoreOrderReview $review) {
                        $html =  Html::encode($review->comment);

                        if (!empty($review->answer)) {
                            $html .= '<br><br><b>Answer:</b> '. Html::encode($review->answer);
                        }

                        return $html;
                    }
                ],
                [
                    'attribute' => 'file_ids',
                    'format'    => 'raw',
                    'label'     => 'Images',
                    'filter'    => false,
                    'value'     => function (StoreOrderReview $review) {
                        $html = '';
                        foreach ($review->reviewFilesSort as $file) {
                            $previewUrl = ImageHtmlHelper::getThumbUrlForFile($file, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL);
                            $html .= Html::a(Html::img($previewUrl . '?time='.md5($file->updated_at), ['style' => 'max-width: 50px']), $file->getFileUrl(), ['target' => '_blank']) . ' ';
                        }
                        return $html;
                    }
                ],
                [
                    'label'  => 'Ratings',
                    'format' => 'raw',
                    'value'  => function (StoreOrderReview $review) {
                        return \yii\widgets\DetailView::widget(
                            [
                                'model'      => $review,
                                'template'   => '<tr><td>{label}</td><td>{value}</td></tr>',
                                'options'    => ['class' => 'table table-condensed'],
                                'attributes' => [
                                    [
                                        'attribute' => 'rating_speed',
                                        'label'     => 'Speed'
                                    ],
                                    [
                                        'attribute' => 'rating_quality',
                                        'label'     => 'Quality'
                                    ],
                                    [
                                        'attribute' => 'rating_communication',
                                        'label'     => 'Communication'
                                    ],
                                ],
                            ]
                        );
                    }
                ],
                [
                    'class'     => \backend\components\columns\UserColumn::class,
                    'attribute' => 'ps.user'
                ],
                [
                    'attribute' => 'status',
                    'filter'    => [
                        StoreOrderReview::STATUS_NEW       => StoreOrderReview::STATUS_NEW,
                        StoreOrderReview::STATUS_MODERATED => StoreOrderReview::STATUS_MODERATED,
                        StoreOrderReview::STATUS_REJECTED  => StoreOrderReview::STATUS_REJECTED
                    ],
                ],
                [
                    'attribute' => 'verified_answer_status',
                    'filter'    => [
                        StoreOrderReview::ANSWER_STATUS_NEW  => StoreOrderReview::ANSWER_STATUS_NEW,
                        StoreOrderReview::ANSWER_STATUS_MODERATED => StoreOrderReview::ANSWER_STATUS_MODERATED,
                        StoreOrderReview::ANSWER_STATUS_REJECTED  => StoreOrderReview::ANSWER_STATUS_REJECTED
                    ],
                ],
                [
                    'format' => 'raw',
                    'value'  => function (StoreOrderReview $review) {
                        return Html::a('Update', ['update', 'id' => $review->id], ['class' => 'btn btn-primary']);
                    }
                ],
                [
                    'format' => 'raw',
                    'value'  => function (StoreOrderReview $review) {
                        if ($review->status != StoreOrderReview::STATUS_NEW) {
                            return "";
                        }

                        return Html::a('Approve', ['approve', 'id' => $review->id], ['class' => 'btn btn-success '])
                        . ' <br /><br /> ' . Html::a('Reject', ['reject', 'id' => $review->id], ['class' => 'btn btn-danger btn-xs']);
                    }
                ]
            ]
        ]
    ); ?>
</div>