<?php

use backend\widgets\FilesListWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrderReview */

$this->title = 'Store order review ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Order Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-review-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="form-group field-storeorderreview-comment">
        <?php if ($model->isAnswerRejected()): ?>
            <a href="<?php echo Url::toRoute(['/store/reviews/answer-approve', 'id' => $model->id]); ?>"
               class="btn btn-success">Answer accept
            </a>
        <?php else: ?>
            <?php if ($model->isAnswerNew()): ?>
                <a href="<?php echo Url::toRoute(['/store/reviews/answer-approve', 'id' => $model->id]); ?>"
                   class="btn btn-success">Answer accept
                </a>
            <?php endif; ?>
            <button type="button"
                    class="btn btn-warning btn-ajax-modal"
                    value="<?php echo Url::toRoute(['/store/reviews/answer-reject', 'id' => $model->id]); ?>"
                    data-pjax="0"
                    data-target="#rejprint">
                Answer reject
            </button>
        <?php endif; ?>
    </div>


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= $form->errorSummary($model); ?>
    <?= DetailView::widget(
        [
            'model'      => $model,
            'attributes' => [
                'id',
                'created_at',
                'createdBy'  => [
                    'label'  => 'Creator',
                    'format' => 'raw',
                    'value'  => Html::a(H($model->order->user->username), ['/user/user/view', 'id' => $model->order->user->id])
                ],
                'order'      => [
                    'label'  => 'Order',
                    'format' => 'raw',
                    'value'  => Html::a(H($model->order->getTitle()), ['/store/store-order/view', 'id' => $model->order->id])
                ],
                'store_unit' => [
                    'label'  => 'Store unit',
                    'format' => 'raw',
                    'value'  => $model->storeUnit
                        ? Html::a($model->storeUnit->id . ' ' . H($model->storeUnit->model3d->title), ['/store/store-unit/view', 'id' => $model->storeUnit->id])
                        : null,
                ],
                'ps'         => [
                    'label'  => 'Print service',
                    'format' => 'raw',
                    'value'  => Html::a($model->ps->id . ' ' . H($model->ps->title), ['/ps/ps/view', 'id' => $model->ps->id])
                ],
            ],
        ]
    ) ?>

    <?= $form->field($model, 'public_to_print')->checkbox(); ?>

    <?= $form->field($model, 'rating_speed')->textInput() ?>

    <?= $form->field($model, 'rating_quality')->textInput() ?>

    <?= $form->field($model, 'rating_communication')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'answer')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'answer_reject_comment')->textarea(['rows' => 6]) ?>

    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">Photos</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'formPrefix'             => $model->formName(),
                    'formAttribute'          => 'files',
                    'filesList'              => $model->reviewFilesSort,
                    'rights'                 => [
                        FilesListWidget::ALLOW_DELETE,
                        FilesListWidget::ALLOW_ADD,
                        FilesListWidget::ALLOW_ROTATE,
                        FilesListWidget::ALLOW_SET_MAIN
                    ],
                    'setMainFile' => [
                        'attribute'  => 'setMainImageUuid',
                        'activeUuid' =>  $model->reviewCoverFile->uuid ?? null
                    ],
                    'filesAllowedExtensions' => $model::ALLOW_EXTENSIONS
                ]
            ) ?>
        </div>
    </div>

    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">Order result photos</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'filesList' => Yii::$app->reviewService->getAllowedAttachFiles($model->order->currentAttemp),
                    'rights'                 => [
                        FilesListWidget::ALLOW_ROTATE
                    ],
                ]
            ) ?>
        </div>
    </div>

    <?= $form->field($model, 'status')->dropDownList(['new' => 'New', 'moderated' => 'Moderated', 'rejected' => 'Rejected',], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
