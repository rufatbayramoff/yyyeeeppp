<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\StoreOrderReview */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="store-order-cancel-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php
    echo $form->field($model, 'answer_reject_comment')->textarea(['maxlength' => true]);
    ?>
    <div class="form-group">
        <?= Html::submitButton('Answer reject', ['class' => 'btn btn-danger ajax-submit']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
