<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreUnits2category */

$this->title = 'Create Store Units2category';
$this->params['breadcrumbs'][] = ['label' => 'Store Units2categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-units2category-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
