<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustomCode */

$this->title = 'Update Custom Code: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Custom Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="custom-code-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
