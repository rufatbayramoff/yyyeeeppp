<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustomCode */

$this->title = 'Create Custom Code';
$this->params['breadcrumbs'][] = ['label' => 'Custom Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-code-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
