<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.06.19
 * Time: 14:29
 */

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model \common\models\UserAddress */


$this->title = 'Update User Address: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-address-update">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
