<?php

use backend\models\search\PreorderSearch;
use backend\models\search\StoreOrderSearch;
use common\components\DateHelper;
use common\models\Preorder;
use common\models\PsPrinter;
use common\models\StoreOrder;
use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Orders Notify';
$this->params['breadcrumbs'][] = $this->title;

$search2                   = new StoreOrderSearch(['change_printer_expire' => 1, 'notTest' => true, 'order_status' => 'change_printer']);
$dataProviderChangePrinter = $search2->search(Yii::$app->request->queryParams);

// printing expired
$search3    = new StoreOrderSearch(['printing_expire' => 1, 'notTest' => true, 'order_state' => ['completed', 'processing']]);
$dpPrinting = $search3->search(Yii::$app->request->queryParams);

$search4   = new StoreOrderSearch(['shipped_expire' => 1, 'notTest' => true, 'order_state' => ['completed', 'processing']]);
$dpShipped = $search4->search(Yii::$app->request->queryParams);

$search5      = new StoreOrderSearch(['authorized_expire' => 1, 'notTest' => true]);
$dpAuthorized = $search5->search(Yii::$app->request->queryParams);

$searchCanceledWithoutRefund = new StoreOrderSearch(['canceled_without_refund' => 1, 'notTest' => true]);
$dpCanceledWithoutRefund     = $searchCanceledWithoutRefund->search(Yii::$app->request->queryParams);

$search6    = new StoreOrderSearch(['payment_status' => 'settlement_declined', 'notTest' => true, 'order_state' => ['processing']]);
$dpDeclined = $search6->search(Yii::$app->request->queryParams);

//  Ready to send more that 24 hours
$search7     = new StoreOrderSearch(['notTest' => true, 'readyToSent_expire' => true]);
$dpNotSended = $search7->search(Yii::$app->request->queryParams);

$search8   = new StoreOrderSearch(['is_dispute_open' => 1]);
$dpDispute = $search8->search(Yii::$app->request->queryParams);

$search9     = new StoreOrderSearch(['user_id' => User::USER_ANONIM, 'notTest' => true, 'created_at' => DateHelper::subNowSec(60 * 60 * 24 * 14)]);
$dpAnonymous = $search9->search(Yii::$app->request->queryParams);


function getGridNotify($dataProvider, $searchModel = null, $showCancel = false)
{
    return GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'format'    => 'raw',
                'value'     => function (StoreOrder $model) {
                    $url = yii\helpers\Url::toRoute(['store/store-order/view', 'id' => $model->id]);
                    return yii\helpers\Html::a('#' . $model->id, $url, ['style' => 'font-size: 22px;']);
                }
            ],
            [
                'label'     => 'User',
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function (StoreOrder $model) {
                    $user = \backend\models\Backend::displayUser($model->user);
                    return $user;
                }
            ],
            'currentAttemp.status',
            [
                'attribute' => 'printer_id',
                'format'    => 'raw',
                'value'     => function (StoreOrder $model) {
                    return $model->currentAttemp && $model->currentAttemp->machine
                        ? PsPrinter::getLink($model->currentAttemp->machine)
                        : '';
                }
            ],
            [
                'format'         => 'raw',
                'contentOptions' => ['style' => 'min-width: 200px;'],
                'value'          => function (StoreOrder $model) {
                    $dates  = [
                        'Billed: '   => $model->billed_at,
                        'Shipped:  ' => isset($model->currentAttemp) ? $model->currentAttemp->dates->shipped_at : null,
                        'Received: ' => isset($model->currentAttemp) ? $model->currentAttemp->dates->received_at : null,
                    ];
                    $result = [];
                    foreach ($dates as $k => $v) {
                        $v        = app('formatter')->asDatetime($v);
                        $result[] = $k . $v;
                    }
                    $html = implode("<br />", $result);
                    return "<small>" . $html . "</small>";
                }
            ],
            'updated_at:datetime',
            [
                'attribute' => 'Actions',
                'visible'   => $showCancel,
                'format'    => 'raw',
                'value'     => function (StoreOrder $model) {
                    $urlCancel    = \yii\helpers\Url::toRoute(['/store/store-notify/cancel', 'id' => $model->id]);
                    $cancelButton = Html::button('Cancel',
                        [
                            'title'       => 'Cancel reason',
                            'class'       => 'btn btn-warning btn-ajax-modal',
                            'data-pjax'   => 0,
                            'value'       => $urlCancel,
                            'data-target' => '#store_order_cancel'
                        ]
                    );

                    return $cancelButton;
                }
            ],

        ],
    ]);
}

?>
<div class="store-order-index">

    <div class="row">
        <div class="col-lg-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Dispute Orders</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= getGridNotify($dpDispute, null, true); ?>
                </div>
            </div>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Authorized Over 5 days</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= getGridNotify($dpAuthorized, null); ?>
                </div>
            </div>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Canceled orders without refund</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= getGridNotify($dpCanceledWithoutRefund, null); ?>
                </div>
            </div>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Payment Declined Orders</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= getGridNotify($dpDeclined, null); ?>
                </div>
            </div>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Anonymous Orders</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= getGridNotify($dpAnonymous, null); ?>
                </div>
            </div>

        </div>
        <div class="col-lg-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Change Printer Over 24 hours</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= getGridNotify($dataProviderChangePrinter, null); ?>
                </div>
            </div>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Printing Over limit</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= getGridNotify($dpPrinting, null); ?>
                </div>
            </div>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Ready to sent more than 24 hours</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= getGridNotify($dpNotSended, null); ?>
                </div>
            </div>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Shipped Over 14 days</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= getGridNotify($dpShipped, null); ?>
                </div>
            </div>


        </div>


    </div>
</div>
