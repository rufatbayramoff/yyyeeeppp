<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StorePricer */

$this->title = 'Create Store Pricer';
$this->params['breadcrumbs'][] = ['label' => 'Store Pricers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-pricer-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
