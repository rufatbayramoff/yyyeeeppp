<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StorePricerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Pricers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-pricer-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Pricer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'order_id',
            'pricer_type',
            'created_at',
            'updated_at',
            // 'price',
            // 'currency',
            // 'rate_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
