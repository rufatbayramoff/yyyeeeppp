<?php

use yii\bootstrap\ActiveForm;
use \yii\helpers\Html;
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 07.08.18
 * Time: 16:16
 */
?>
<div class="store-order-attemp-delivery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="store-order-attemp-delivery-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'tracking_number')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'tracking_shipper')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>