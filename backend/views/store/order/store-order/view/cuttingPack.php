<?php

use common\models\CuttingPack;
use yii\widgets\DetailView;

/** @var CuttingPack $cuttingPack */

?>

<div class="col-lg-5">
    <div class="box box-solid collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Cutting Item</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body" style="display: none;">
            <?= $this->renderFile(Yii::getAlias('@backend/views/store/cutting-pack/viewModel.php'), ['cuttingPack' => $cuttingPack]); ?>
        </div>
    </div>
</div>

