<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \frontend\models\ps\TrackingNumberForm $form */
/** @var \common\models\StoreOrderAttemp $attempt */

$aForm = ActiveForm::begin([
    'action' => ['set-as-sent', 'attemptId' => $attempt->id],
    'id' => 'tracking-number-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
<?= $aForm->field($form, 'trackingNumber') ?>
<?= $aForm->field($form, 'shipper') ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>