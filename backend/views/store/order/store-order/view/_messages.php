<?php
/* @var $this \yii\web\View */
/* @var $order common\models\StoreOrder */
/* @var $topics common\models\MsgTopic[] */

use backend\modules\support\assets\TopicEditAssets;
use common\models\message\builders\TopicBuilder;


TopicEditAssets::register($this);
$supportUser = TopicBuilder::resolveSupportUser();
?>

<div>

    <?php foreach ($topics as $topic) : ?>

        <h1><?= $topic->title ?></h1>

        <div class="topic-view">

            <div class="chat-list">

                <?php foreach ($topic->messages as $message) : ?>

                    <?= $this->render('@backend/modules/support/views/active-topics/message-item.php', ['user' => $supportUser, 'model' => $message])?>
                <?php endforeach; ?>

            </div>
        </div>

    <?php endforeach; ?>

</div>

<script type="text/javascript">

    // dirty hack for prevent messages update
    support = {isResolved : true};

</script>