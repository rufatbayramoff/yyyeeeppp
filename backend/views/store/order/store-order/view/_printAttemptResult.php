<?php
use common\models\Model3dReplicaPart;
use common\models\StoreOrderAttemp;
use frontend\models\ps\AddResultImageForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var AddResultImageForm $model */
/** @var StoreOrderAttemp $attempt */
/** @var Model3dReplicaPart $model3dPart */

$form = ActiveForm::begin([
    'action' => ['print-attempt-result', 'attemptId' => $attempt->id],
    'id' => 'print-attempt-result-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
<?= $form->field($model, 'file')->fileInput() ?>
    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>