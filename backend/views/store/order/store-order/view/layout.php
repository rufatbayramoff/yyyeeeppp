<?php

/* @var $this yii\web\View */
use backend\assets\AngularAppAsset;
use common\models\message\builders\TopicBuilder;
use common\models\MsgTopic;

/* @var $model common\models\StoreOrder */
/* @var $customerIp string */
/* @var $pricer array */
/* @var $paymentTransaction array */
/* @var $paymentbtTransaction array */
/* @var $history array */
/* @var $userPsIp int */



$this->title = 'Order #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerAssetBundle(AngularAppAsset::class);


/** @var MsgTopic[] $topics */
$topics = MsgTopic::find()
    ->forOrder($model)
    ->forUser(TopicBuilder::resolveSupportUser())
    ->all();

?>

<style type="text/css">
    <?= $this->render('styles.css')?>
</style>


<div class="so">


    <div class="so-tabs">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Main</a></li>
            <?php if($topics) : ?>
                <li role="presentation"><a href="#messages" aria-controls="main" role="tab" data-toggle="tab">Messages</a></li>
            <?php endif; ?>
            <li role="presentation"><a href="#history" aria-controls="settings" role="tab" data-toggle="tab">History</a></li>
        </ul>
    </div>

    <div>
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane active" id="main">
                <?= $this->render('_main', compact('model', 'psEarning','customerIp', 'userPsIp', 'history', 'refundService'))?>
            </div>

            <?php if($topics) : ?>
                <div role="tabpanel" class="tab-pane" id="messages">
                    <?= $this->render('_messages', ['order' => $model, 'topics' => $topics])?>
                </div>
            <?php endif; ?>

            <div role="tabpanel" class="tab-pane" id="history">
                <?= $this->render('history/history', ['order' => $model])?>
            </div>

        </div>
    </div>

</div>

