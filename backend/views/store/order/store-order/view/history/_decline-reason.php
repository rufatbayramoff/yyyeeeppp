<?php
use common\models\base\StoreOrderAttemp;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $historyItem \common\models\StoreOrderHistory */

$data = $historyItem->getDecodedData();
/** @var StoreOrderAttemp $attemp */
$attemp = StoreOrderAttemp::tryFindByPk($data['attemptId']);
$str = '<i>Attemp to printer</i> '
    .Html::a($attemp->machine->getTitleLabel().' ['.$attemp->machine->id.']', ['/company/company-service/view', 'id' => $attemp->machine->id],
        ['target' => '_blank'])
    .' <i>ps</i> '
    .Html::a($attemp->ps->title.' ['.$attemp->ps->id.']', ['/ps/ps/view', 'id' => $attemp->ps->id], ['target' => '_blank'])
    .' <i>user</i> '
    .Html::a($attemp->ps->user->username.' ['.$attemp->ps->user->id.']', ['/user/user/view', 'id' => $attemp->ps->user_id], ['target' => '_blank'])
    .' <i>was declined. <br/> Reason: </i> '
    . \H($data['reason']). '<i> and comment </i> '. nl2br(\H($data['comment'])).'.';
?>



<div class="timeline-body">
    <?= $str;?>
</div>
