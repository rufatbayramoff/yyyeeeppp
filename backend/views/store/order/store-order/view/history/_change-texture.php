<?php

use common\models\Model3dReplicaPart;
use common\models\PrinterColor;
use common\models\PrinterMaterial;

/* @var $this yii\web\View */
/* @var $historyItem \common\models\StoreOrderHistory */

$data = $historyItem->getDecodedData();

$textureLabel = function($item){
    return $item['kitTexture'] ? 'For all order' : 'Per models';
};

$allOrderTextureFn = function ($item) {
    $material = PrinterMaterial::findByPk($item['kitTexture']['materialId']);
    $color = PrinterColor::findByPk($item['kitTexture']['colorId']);
    $infill = $item['kitTexture']['infill'] ?? '';
?>

    <div class="col-md-3">

        <table class="table no-first-row-top-border table-condensed texture-table">
            <tr>
                <td><i>Material:</i></td>
                <td><?= $material->title ?? ''?></td>
            </tr>
            <tr>
                <td><i>Color:</i</td>
                <td><?= $color->title ?? ''?></td>
            </tr>
            <tr>
                <td><i>Infill:</i</td>
                <td><?php echo $infill?></td>
            </tr>
        </table>

    </div>

    <?php
};


$perModelsTextureFn = function ($item) {

    foreach ($item['partsTextures'] as $partId => $part) {
        $replicaPart = Model3dReplicaPart::findByPk($partId);
        $material = PrinterMaterial::findByPk($part['materialId']);
        $color = PrinterColor::findByPk($part['colorId']);
        $infill = $part['infill'] ?? '';

        ?>

        <div class="col-md-3">

            <table class="table no-first-row-top-border table-condensed texture-table">
                <tr>
                    <td><i>Part:</i></td>
                    <td><b><?= $replicaPart->name ?? ''?></b></td>
                </tr>
                <tr>
                    <td><i>Material:</i></td>
                    <td><?= $material->title ?? ''?></td>
                </tr>
                <tr>
                    <td><i>Color:</i</td>
                    <td><?= $color->title ?? '' ?></td>
                </tr>
                <tr>
                    <td><i>Infill:</i</td>
                    <td><?php echo $infill ?></td>
                </tr>
            </table>

        </div>

<?php
    }
};
?>



<div class="timeline-body  with-border">


    <div class="row">

        <div class="col-md-12">
            <div style="margin-bottom: 20px">
                <b><i>Old texture:</i> <?= $textureLabel($data['oldTexture'])?></b>
            </div>
        </div>

        <?= $data['oldTexture']['kitTexture'] ? $allOrderTextureFn($data['oldTexture']): $perModelsTextureFn($data['oldTexture']) ?>

    </div>
</div>

<div class="timeline-body">


    <div class="row">

        <div class="col-md-12">

            <div style="margin-bottom: 20px">
                <b><i>New texture:</i> <?= $textureLabel($data['newTexture'])?></b>
            </div>

        </div>

        <?= $data['newTexture']['kitTexture'] ? $allOrderTextureFn($data['newTexture']): $perModelsTextureFn($data['newTexture']) ?>

    </div>
</div>
