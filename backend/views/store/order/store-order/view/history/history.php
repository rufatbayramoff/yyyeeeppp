<?php

/* @var $order common\models\StoreOrder */


use common\components\ArrayHelper;
use common\models\StoreOrderHistory;

$items = StoreOrderHistory::find()
    ->where(['order_id' => $order->id])
    ->orderBy('id DESC')
    ->all();

$dp = new \yii\data\ArrayDataProvider(
    [
        'allModels' => $items
    ]
);

$datesMap = ArrayHelper::group(
    $items,
    function (StoreOrderHistory $item) {
        return Yii::$app->formatter->asDate($item->created_at);
    }
);

$getIconFn = function (StoreOrderHistory $item) {

    switch ($item->action_id) {

        case StoreOrderHistory::PAYMENT_STATUS :
            return ['fa-usd', 'bg-blue'];

        case StoreOrderHistory::ITEM_ADDED :
            return ['fa-plus', 'bg-green'];

        case StoreOrderHistory::USER_CANCEL_ORDER :
        case StoreOrderHistory::MODERATOR_CANCEL_ORDER :
            return ['fa-times', 'bg-red'];

        case StoreOrderHistory::ORDER_ATTEMPT_DECLINE:
            return ['fa-times', 'bg-orange'];

        case StoreOrderHistory::ORDER_ATTEMPT_ACCEPT :
            return ['fa-check ', 'bg-green'];
    }
    return ['fa-dot-circle-o', 'bg-blue'];
}


?>

<ul class="timeline">


    <?php foreach ($datesMap as $date => $items): ?>


        <li class="time-label">
        <span class="bg-green">
            <?= $date ?>
        </span>
        </li>

        <?php /** @var StoreOrderHistory $item */
        foreach ($items as $item):
            list($icon, $bgColor) = $getIconFn($item);

            ?>

            <li>
                <!-- timeline icon -->
                <i class="fa <?= $icon ?> <?= $bgColor ?>"></i>
                <div class="timeline-item">
                    <span class="time">
                        <?php
                        if ($userId = $item->admin_user_id??$item->user_id) {
                            $user = \common\models\User::findByPk($userId);
                            echo $user->username . ': ';
                        }
                        echo \Yii::$app->formatter->asTime($item->created_at)
                        ?>
                    </span>
                    <h3 class="timeline-header"><?= H($item->comment) ?></h3>


                    <?php if ($item->action_id == StoreOrderHistory::TEXTURE_UPDATE) : ?>

                        <?= $this->render('_change-texture', ['historyItem' => $item]) ?>

                    <?php elseif ($item->action_id == StoreOrderHistory::ORDER_ATTEMPT_DECLINE) : ?>

                        <?= $this->render('_decline-reason', ['historyItem' => $item]) ?>

                    <?php else : ?>
                        <div class="timeline-body">
                            <small style="color: darkgray"><?= $item->data ?></small>
                        </div>
                    <?php endif ?>

                    <!--                    <div class="timeline-footer">
                                            <a class="btn btn-primary btn-xs">...</a>
                                        </div>-->
                </div>
            </li>

        <?php endforeach ?>


    <?php endforeach; ?>

    <li>
        <i class="fa fa-clock-o bg-gray"></i>
    </li>

</ul>