<?php

use backend\assets\AngularAppAsset;
use backend\components\GridAttributeHelper;
use backend\models\Backend;
use backend\widgets\OrderReviewList;
use common\components\ArrayHelper;
use common\components\FileTypesHelper;
use common\components\Model3dWeightCalculator;
use common\components\order\predicates\NeedSetAsSettledPredicate;
use common\components\ps\locator\Size;
use common\models\ModerLog;
use common\models\MsgMessage;
use common\models\MsgTopic;
use common\models\PaymentTransactionRefund;
use common\models\PsPrinter;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderPosition;
use common\models\StoreOrderReview;
use common\models\ThingiverseOrder;
use common\models\UserAddress;
use common\models\UserLike;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\payment\models\StoreOrderPositionForm;
use common\modules\storeOrder\helpers\ParcelHelper;
use common\services\MeasureService;
use common\services\Model3dPartService;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\model3d\Model3dFacade;
use frontend\models\order\StoreOrderAttemptDeadline;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\modules\preorder\components\PreorderService;
use lib\delivery\delivery\DeliveryFacade;
use lib\delivery\delivery\models\DeliveryPostalLabel;
use lib\delivery\parcel\ParcelFactory;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrder */
/* @var $refundService \common\modules\payment\services\RefundService */
/* @var $customerIp string */
/* @var $paymentTransaction array */
/* @var $paymentbtTransaction array */
/* @var $history array */
/* @var $userPsIp int */

$order       = $model;
$this->title = 'Order #' . $model->id;

$attempt     = $model->currentAttemp;
$replica     = $model3d = $model->orderItem ? $model->orderItem->model3dReplica : null;
$machine     = $attempt->machine ?? null;
$canDownload = \backend\components\AdminAccess::can('model3d.download');

/**
 * @param StoreOrder $order
 * @return \common\models\model3d\RenderInfo[]
 */
$getUnitModelRendersFn = function (StoreOrder $order) {
    $renderedMap = [];
    foreach ($order->storeOrderItems as $storeItem) {
        $model3d = $storeItem->model3dReplica;

        if ($model3d) {
            $renders     = Model3dFacade::getRenderedImages($model3d->getActiveModel3dParts(), $model3d, true);
            $renderedMap = ArrayHelper::merge($renderedMap, ArrayHelper::index($renders, 'model3dPartId'));
        }
    }
    return $renderedMap;
};


$orderStatusFn = function (StoreOrder $order) {
    if (!$order->currentAttemp) {
        return null;
    }
    return $order->currentAttemp->status;
};

$transactionIds     = ArrayHelper::getColumn($model->paymentTransactions, 'id');
$refundDataProvider = PaymentTransactionRefund::getDataProvider(['transaction_id' => $transactionIds]);


/**
 * @param StoreOrder $order
 * @return string
 */
$changeOfferButtonFn = function (StoreOrder $order) use ($attempt, $refundDataProvider) {
    if (!$order->can_change_ps) {
        return 'No';
    }

    if ($order->isCancelled()) {
        return 'Yes';
    }

    $class = !$attempt || $attempt->isCancelled() ? 'btn btn-warning' : 'btn btn-link text-yellow no-padding';

    $btn = '<div class="btn-group">
            <a class="' . $class . '" href="/store/store-order/change-printer?id=' . $order->id . '" target="_blank" ">Change Printer</a>
            </div>';

    if ($refundDataProvider->getCount() > 0) {
        $btn = $btn . "<br /><b class='label label-danger'>REFUNDS WILL BE TAKEN FROM NEW PRINTER</b>";
    }
    return $btn;
};

/** @var \common\models\model3d\RenderInfo[] $renderedMap */
$renderedMap = $getUnitModelRendersFn($model);

AngularAppAsset::register($this);

$haveCancelledAttemptsFn = function (StoreOrder $order) {
    foreach ($order->attemps as $attempt) {
        if ($attempt->isCancelled()) {
            return true;
        }
    }
    return false;
}

?>

<div ng-controller="StoreOrderController">

    <div class="so-control">

        <?php if ($order->currentAttemp):

            $printRequest = StoreOrderAttemptProcess::create($order->currentAttemp);
            ?>

            <?php if ($printRequest->canSetAsPrinted()): ?>

            <?= Html::a('Set as Printed', ['set-as-printed', 'attemptId' => $order->currentAttemp->id], ['class' => 'btn btn-default']) ?>

        <?php endif; ?>

            <?php if ($printRequest->canSetAsSent()): ?>


            <?php if ($printRequest->canSetAsSentWithTrackingNumber()) : ?>

                <button
                        type="button"
                        data-pjax="0"
                        data-target="#trackingNumberModal"
                        value="<?= Url::toRoute(['store/store-order/tracking-number-form', 'attemptId' => $model->currentAttemp->id]) ?>"
                        class="btn btn-default btn-ajax-modal">
                    Set as sent
                </button>

            <?php elseif($order->isPickup()):?>
                <a
                        href="<?= Url::toRoute(['store/store-order/set-as-sent', 'attemptId' => $model->currentAttemp->id])?>"
                        class="btn btn-default">
                    Set as sent
                </a>
            <?php else : ?>
                <?php echo Html::a('Set as sent',['sent','id' => $model->id],['class' => 'btn btn-default'])?>
            <?php endif; ?>

        <?php endif; ?>

            <?php if ($printRequest->canSetAsDelivered()): ?>

            <a
                    href="<?= Url::toRoute(['store/store-order/set-as-delivered', 'attemptId' => $model->currentAttemp->id]) ?>"
                    class="btn btn-default">
                Set as Delivered
            </a>

        <?php endif; ?>

            <?php if ($printRequest->canReceive()): ?>

            <a
                    href="<?= Url::toRoute(['store/store-order/set-as-received', 'attemptId' => $model->currentAttemp->id]) ?>"
                    class="btn btn-default">
                Set as Received
            </a>

        <?php endif; ?>

        <?php endif; ?>

        <a href="<?= Url::toRoute(['/store/store-order/dispute-switch', 'id' => $model->id]) ?>"
           class="btn btn-default t-store-order-open-dispute">
            <?= ($model->is_dispute_open ? 'Close' : 'Open') . ' dispute' ?>
        </a>

        <?php
        if (!$model->isResolved() && !$model->isCancelled()) {
            echo Html::button(
                'Cancel Order',
                [
                    'type'        => 'button',
                    'title'       => 'Cancel order',
                    'class'       => 'btn btn-link btn-ajax-modal text-red t-store-order-cancel',
                    'data-pjax'   => 0,
                    'value'       => Url::toRoute(['store/store-order/cancel', 'id' => $model->id]),
                    'data-target' => '#cancel_order'
                ]
            );
        }


        ?>
    </div>

    <?php if ($model->isCancelled() && !empty($model->getPayedNotCanceledAdditionalServices())): ?>
        <div class="callout callout-danger">
            <h4>The order is canceled and has paid "additional services".</h4>
            <p>You need to refund for "additional services".</p>
        </div>
    <?php endif; ?>

    <div class="store-order-view">
        <div class="row">
            <div class="col-lg-4">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Print Offers</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body no-padding">

                        <?= GridView::widget(
                            [
                                'tableOptions' => ['class' => 'table no-margin'],
                                'options'      => ['class' => ''],
                                'rowOptions'   => function (StoreOrderAttemp $attemp) {
                                    if ($attemp->isCancelled()) {
                                        return ['ng-if' => 'showCanceledAttempts'];
                                    }
                                    return [];
                                },
                                'layout'       => "{items}",
                                'dataProvider' => StoreOrderAttemp::getDataProvider(['order_id' => $model->id], 1000),
                                //StoreOrderOffer::getDataProvider(['order_id' => $model->id], 10),
                                'columns'      => [
                                    [
                                        'attribute'     => 'Printer',
                                        'headerOptions' => ['style' => 'width:270px'],
                                        'format'        => 'raw',
                                        'value'         => function (StoreOrderAttemp $model) {

                                            if ($model->hasMachine()) {
                                                return sprintf('%d &nbsp  %s <br/> <b>PS</b>: %s<br /><b>Layer Resolution</b>: %s',
                                                    $model->id,
                                                    GridAttributeHelper::getPrinterLink($model->machine),
                                                    GridAttributeHelper::getPsLink($model->machine->ps),
                                                    \frontend\models\ps\PsFacade::getPrinterLayerResolution($model->machine));
                                            } else {
                                                return 'Attempt id: ' . $model->id;
                                            }
                                            return null;
                                        }
                                    ],
                                    'created_at:datetime',
                                    [
                                        'attribute'     => 'status',
                                        'headerOptions' => ['style' => 'width:20px'],
                                        'value'         => function (StoreOrderAttemp $model) {
                                            if ($model->isCancelled() && $model->dates->expired_at) {
                                                return 'expired';
                                            }
                                            return $model->status;
                                        }
                                    ],
                                    [
                                        'attribute'     => 'id',
                                        'label'         => '',
                                        'headerOptions' => ['style' => 'width:20px'],
                                        'format'        => 'raw',
                                        'value'         => function (StoreOrderAttemp $attemp) use ($model) {
                                            if (!empty($model->current_attemp_id) || $attemp->status !== StoreOrderAttemp::STATUS_CANCELED) {
                                                return '';
                                            }
                                            return Html::a('Re-Offer', ['/store/store-order/reoffer-attempt', 'attempId' => $attemp->id, 'orderId' => $model->id]);
                                        }
                                    ],
                                    [
                                        'attribute'     => 'id',
                                        'label'         => '',
                                        'format'        => 'raw',
                                        'headerOptions' => ['style' => 'width:20px'],
                                        'value'         => function (StoreOrderAttemp $attemp) use ($model) {
                                            if ($attemp->isCancelled() || ($attemp->dates && $attemp->dates->expired_at)) {
                                                return '';
                                            }
                                            return Html::a('Cancel', ['/store/store-order/cancel-attempt', 'attempId' => $attemp->id, 'orderId' => $model->id]);
                                        }
                                    ]
                                ],
                            ]
                        ); ?>
                    </div>

                    <?php if ($haveCancelledAttemptsFn($order)): ?>

                        <div class="box-footer text-center">
                            <button
                                    class="btn btn-link no-padding"
                                    type="button"
                                    ng-click="showCanceledAttempts = !showCanceledAttempts">{{showCanceledAttempts ?
                                'Hide cancelled' : 'Show cancelled'}}
                            </button>
                        </div>

                    <?php endif; ?>

                </div>

                <div class="box box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Moderator Comments</h3>
                    </div>
                    <div class="box-body">
                        <?php $form = \yii\widgets\ActiveForm::begin(['action' => '/store/store-order/save-comment']);
                        $moderLog   = ModerLog::findOne(['object_id' => $model->id, 'action' => 'order_cmt', 'object_type' => ModerLog::TYPE_ORDER]);

                        ?>
                        <input type="hidden" name="order_id" value="<?php echo $model->id; ?>"/>
                        <textarea name="comment"
                                  style="width:100%;height:50px;"><?= $moderLog ? $moderLog->result : ''; ?></textarea>
                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-default']) ?>
                        </div>

                        <?php \yii\widgets\ActiveForm::end(); ?>
                        <?php
                        echo \common\modules\comments\widgets\Comments::widget(['model' => 'backend_store_order', 'model_id' => $model->id]);

                        ?>
                    </div>

                </div>

                <?php if (!empty($model->storeOrderPromocodes)): ?>
                    <div class="box box-solid box-info">
                        <div class="box-header">
                            <h3 class="box-title">Promocodes Used</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <?= GridView::widget(
                                [
                                    'class'        => 'box-body',
                                    'layout'       => "{items}",
                                    'dataProvider' => \common\models\StoreOrderPromocode::getDataProvider(['order_id' => $model->id]),
                                    'columns'      => [
                                        'promocode.code',
                                        'created_at:datetime',
                                        'amount:currency',
                                        'amount_currency'
                                    ],
                                ]
                            ); ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ((new NeedSetAsSettledPredicate())->test($model)): ?>

                    <div class="callout callout-danger">
                        <h4>Set payment as SETTLED</h4>
                        <p>You must set payment transaction status to SETTLED</p>
                    </div>

                <?php endif; ?>


                <?php if ($model->isAnonim()): ?>

                    <div class="box box-solid box-success">
                        <div class="box-body">
                            <div>
                                <p>
                                    This order is anonim
                                    <button
                                            type="button"
                                            class="btn btn-warning btn-xs text-left"
                                            onclick="$('#changeClientForm').show();">Change client
                                    </button>
                                </p>
                            </div>

                            <form
                                    method="post"
                                    id="changeClientForm"
                                    style="display: none"
                                    action="<?= Url::toRoute(['/store/store-order/change-client', 'id' => $model->id]); ?>"
                                    class="form-inline">

                                <div class="form-group">
                                    <input type="number"
                                           name="clientId"
                                           class="form-control input-sm"
                                           placeholder="Client User ID"/>
                                </div>

                                <button class="btn btn-default btn-sm">Change</button>
                                <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>
                            </form>

                        </div>
                    </div>

                <?php endif; ?>

                <!-- 1 -->
                <div class="box box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Order information</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body no-padding">

                        <?php $form = \yii\widgets\ActiveForm::begin(['action' => '/store/store-order/save-user-comment']);
                        ?>
                        <input type="hidden" name="order_id" value="<?php echo $model->id; ?>"/>
                        <textarea name="comment" style="width:100%;height:50px;"><?= H($model->comment); ?></textarea>
                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-default']) ?>
                        </div>

                        <?php \yii\widgets\ActiveForm::end(); ?>

                        <?php
                        if (!empty($model->comment)) {
                            # echo "<p style='padding: 0 10px 10px 10px;font-size:10px;'>Comment: <br/>" . nl2br(H($model->comment)) . "</p>";
                        }


                        $shipAddress = $deliveryTypeInfo = $deliveryTypeDetails = "";
                        if ($model->hasDelivery()) {
                            $shipAddress = common\models\UserAddress::formatAddressWithFields($model->shipAddress);
                            if (!empty($model->shipAddress->phone)) {
                                $shipAddress .= "<div><b>Phone</b>: " . \H($model->shipAddress->phone)."</div>";
                            }
                            if (!empty($model->shipAddress->email)) {
                                $shipAddress .= "<div><b>Email</b>: " . \H($model->shipAddress->email)."</div>";
                            }
                            if (!empty($model->shipAddress->is_forced_invalid)) {
                                $shipAddress .= "<div><b>Forced Invalid Address</b></div>";
                            }

                            $shipAddress = '<div class="pull-left">' . $shipAddress . '</div>';
                            $shipAddress .= Html::a(
                                '<i class="fa fa-pencil"></i>',
                                ['/store/user-address/update', 'id' => $model->ship_address_id],
                                ['class' => 'btn btn-box-tool pull-right', 'target' => '_blank']
                            );

                            $deliveryTypeInfo    = $model->deliveryType ? $model->deliveryType->title : 'None';
                            $deliveryTypeDetails = $model->storeOrderDelivery;
                            if (!empty($deliveryTypeDetails) && !empty($deliveryTypeDetails->ps_delivery_details)) {
                                $deliveryTypeInfo .= VarDumper::dumpAsString($deliveryTypeDetails->ps_delivery_details, 10, true);
                            }
                        }

                        $ipDetails    = sprintf(" , IP : <b> %s </b>", $customerIp);
                        $orderUser    = $model->user;
                        $menuUser     = '<div class="btn-group">
                        <button class="btn btn-sm btn-default">' . Backend::displayUser($model->user) . '</button>
                        <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <li>' . Html::a(
                                Yii::t('app', 'Send message'),
                                [
                                    '/support/active-topics/create-topic-form',
                                    'withUserId'       => $orderUser ? $orderUser->id : 0,
                                    'orderId'          => $order->id,
                                    'TopicForm[title]' => 'Order #' . $model->id
                                ]
                            ) . '</li> 
                        </ul>
                      </div>
                    </div>';
                        $bindAnonimus = '';
                        if ($model->user_id === \common\models\User::USER_ANONIM && $model->shipAddress->email) {
                            $bindUser = $model->getPossibleUser();
                            if ($bindUser) {
                                $bindAnonimus = ' ' . Html::a('Bind to ' . $bindUser->username,
                                        Url::toRoute(['bind-anonymous-order', 'id' => $model->id]),
                                        ['class' => 'btn btn-success', 'data-confirm' => 'Are you sure?', 'data-method' => 'post']
                                    );
                            }
                        }
                        echo DetailView::widget(
                            [
                                'model'      => $model,
                                'options'    => ['class' => 'table  no-margin'],
                                'attributes' => [
                                    [
                                        'format' => 'raw',
                                        'label'  => 'PS Earning',
                                        'value'  => $psEarning ? displayAsMoney($psEarning) : ''
                                    ],
                                    [
                                        'format' => 'raw',
                                        'label'  => 'PS',
                                        'value'  => ($order->currentAttemp && $order->currentAttemp->ps) ? \backend\components\columns\PsColumn::getMenu($order->currentAttemp->ps) : ''
                                    ],
                                    [
                                        'format' => 'raw',
                                        'label'  => 'Customer',
                                        'value'  => $menuUser . $bindAnonimus,
                                    ],
                                    //'user.email',
                                    'created_at:datetime',
                                    // 'can_change_ps:boolean',
                                    [
                                        'attribute' => 'can_change_ps',
                                        'format'    => 'raw',
                                        'value'     => $changeOfferButtonFn($model)
                                    ],
                                    [
                                        'attribute' => 'order_status',
                                        'label'     => 'Current attempt status',
                                        'format'    => 'raw',
                                        'value'     => $orderStatusFn($model),
                                    ],
                                    'order_state',
                                    [
                                        'attribute' => 'payment_status',
                                        'format'    => 'raw',
                                        'value'     => $model->getPaymentStatus() . (PaymentInfoHelper::isPaypalPaid($model) ? '- Paypal' : ''),
                                    ],
                                    [
                                        'label'  => 'Printer',
                                        'format' => 'raw',
                                        'value'  => !$model->currentAttemp || !$model->currentAttemp->machine || $model->currentAttemp->machine->isCnc() ? '' : PsPrinter::getLink($model->currentAttemp->machine)
                                    ],
                                    [
                                        'format' => 'raw',
                                        'label'  => 'Ship address',
                                        'value'  => $shipAddress
                                    ],
                                    [
                                        'format' => 'raw',
                                        'label'  => 'Shipping adress from preoder',
                                        'value'  => $model->preorder && $model->preorder->shipTo ? common\models\UserAddress::formatAddress($model->preorder->shipTo) : null
                                    ],
                                    /*[
                                        'format' => 'raw',
                                        'label' => 'Bill address',
                                        'value' => common\models\UserAddress::formatAddress($model->billAddress)
                                    ], */
                                    'updated_at:datetime',
                                    'billed_at:datetime',
                                    [
                                        'attribute' => 'currentAttemp.dates.shipped_at',
                                        'format'    => 'datetime',
                                        'label'     => 'Shipped At'
                                    ],
                                    [
                                        'attribute' => 'currentAttemp.dates.received_at',
                                        'format'    => 'datetime',
                                        'label'     => 'Received At'
                                    ],
                                    /*[
                                        'label'  => 'Delivery type',
                                        'format' => 'raw',
                                        'value'  => $deliveryTypeInfo
                                    ],*/

                                    [
                                        'label'  => 'Tracking ',
                                        'format' => 'raw',
                                        'value'  => $model->hasDelivery() || $model->hasDeliveryTrackingData() ?
                                            sprintf(
                                                "%s (%s) %s",
                                                $model->currentAttemp ? $model->currentAttemp->delivery->tracking_number : null,
                                                $model->currentAttemp ? $model->currentAttemp->delivery->tracking_shipper : null,
                                                $model->currentAttemp ? Html::a('Edit', '/store/store-order/tracknumber-change?id=' . $model->currentAttemp->id) : null
                                            ) : ""
                                    ],
                                    [
                                        'label'  => 'Ps Delivery Details',
                                        'format' => 'raw',
                                        'value'  => function (StoreOrder $model) {
                                            if ($model->hasDelivery() && ($lastDelivery = $model->storeOrderDelivery)) {
                                                $deliveryTypeText = $lastDelivery->deliveryType->getTitle();
                                                if ($lastDelivery->is_express) {
                                                    $deliveryTypeText .= "<br> Express delivery";
                                                }
                                                $dump = VarDumper::dumpAsString($lastDelivery->ps_delivery_details, 10, true);
                                                $edit = '<a href="#" ng-click="changeCarrier()" >Edit</a>';
                                                return $deliveryTypeText . '<br>' . $dump . $edit;
                                            }
                                        }
                                    ],
                                    'source',
                                    [
                                        'label' => 'IP',
                                        'value' => $model->userSession ? $model->userSession->ip : ''
                                    ],
                                    [
                                        'label' => 'sortBy',
                                        'value' => $model?->primaryPaymentInvoice?->paymentInvoiceAnalytic?->sort
                                    ]
                                ],
                            ]
                        );
                        ?>
                    </div>
                </div>

                <?php
                $orderTopics = MsgTopic::find()->where(['bind_id' => $model->id, 'bind_to' => 'order'])->all();

                if ($orderTopics) {
                    $order        = $model;
                    $dataProvider = MsgMessage::getDataProvider(
                        ['topic_id' => \yii\helpers\ArrayHelper::getColumn($orderTopics, 'id')],
                        150
                    );
                    ?>
                    <div class="box box-solid box-default">
                        <div class="box-header">Order messages
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <?php
                            echo GridView::widget(
                                [
                                    'sorter'       => false,
                                    'showFooter'   => false,
                                    'layout'       => '{items}',
                                    'dataProvider' => $dataProvider,
                                    'columns'      => [
                                        [
                                            'attribute' => 'user.username',
                                            'format'    => 'raw',
                                            'value'     => function ($model) {
                                                $username = Backend::displayUser($model->user);
                                                return $username;
                                            }
                                        ],
                                        'created_at:datetime',
                                        [
                                            'attribute' => 'text',
                                            'value'     => function ($model) use ($order) {
                                                if ($order->is_dispute_open) {
                                                    return $model->text;
                                                }
                                            }
                                        ]
                                    ],
                                ]
                            ); ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Printer information</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>


                    <?php if ($machine) : ?>

                        <div class="box-body no-padding">

                            <?php
                            $menuUserPs = '<div class="btn-group">
                        <button class="btn btn-default btn-sm">' . Backend::displayUser($machine->ps->user) . '</button>
                        <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <li>' . Html::a(
                                    Yii::t('app', 'Send message'),
                                    [
                                        '/support/active-topics/create-topic-form',
                                        'withUserId'       => $machine->ps->user ? $machine->ps->user_id : 0,
                                        'orderId'          => $order->id,
                                        'TopicForm[title]' => 'Order #' . $model->id
                                    ]
                                ) . '</li>
                           <li><a >' . sprintf("<br /> IP : <b> %s </b>", $userPsIp) . '</a></li>
                        </ul>
                      </div>
                    </div>';
                            echo
                            DetailView::widget(
                                [
                                    'model'      => $machine,
                                    'options'    => ['class' => 'table no-first-row-top-border'],
                                    'attributes' => [
                                        [
                                            'format' => 'raw',
                                            'label'  => 'User',
                                            'value'  => $menuUserPs
                                        ],
                                        [
                                            'format' => 'raw',
                                            'label'  => 'PS',
                                            'value'  => Html::a("PS", Url::toRoute(['ps/ps/view', 'id' => $machine->ps->id]))
                                        ],
                                        [
                                            'format' => 'raw',
                                            'label'  => 'Title',
                                            'value'  => GridAttributeHelper::getPrinterLink($machine),
                                        ],
                                        [
                                            'format' => 'raw',
                                            'label'  => 'Location',
                                            'value'  => UserAddress::formatAddress(UserAddress::getAddressFromLocation($machine->location))
                                        ],
                                        [
                                            'format' => 'raw',
                                            'label'  => 'timezone',
                                            'value'  => date("H:i:s d/m/Y", strtotime(\common\components\DateHelper::now($machine->ps->user->userProfile->timezone_id)))
                                        ],
                                        'psPrinter.user_status',
                                        'moderator_status',
                                    ],
                                ]
                            );
                            ?>
                        </div>

                    <?php else : ?>

                        <div class="box-body text-center">
                            <i>No printer info</i>
                        </div>
                    <?php endif ?>
                </div>

                <?php if ($model->delivery_type_id && DeliveryFacade::getIsPostDelivery($model)): ?>

                    <div class="box box-solid">
                        <div class="box-header">
                            <h3 class="box-title">Parcel information</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <?php
                                $model3dPartsExist = $model->getFirstReplicaItem() && ParcelFactory::getModel3dPartsCount($model->getFirstReplicaItem());
                            ?>
                            <?php if ($model->currentAttemp && ($model->hasCuttingPack() || $model3dPartsExist)): ?>

                                <?php
                                $uploadDelivery = $model->storeOrderDeliveryHistory;
                                $attemptDelivery = $model->currentAttemp->delivery;
                                $calculatedParcel = ParcelFactory::createFromOrder($model);
                                echo DetailView::widget(
                                    [
                                        'model'      => $attemptDelivery,
                                        'options'    => ['class' => 'table'],
                                        'attributes' => [
                                            [
                                                'label'     => "Service's calculated",
                                                'format'    => 'raw',
                                                'value'     =>  $uploadDelivery ? ParcelHelper::format($uploadDelivery->getData()) : ''
                                            ],
                                            [
                                                'attribute' => 'real_parcel',
                                                'format'    => 'raw',
                                                'value'     => ParcelHelper::format($attemptDelivery->getData())
                                            ],
                                            [
                                                'attribute' => 'calculated_parcel',
                                                'format'    => 'raw',
                                                'value'     => ParcelHelper::formatMeasure($calculatedParcel)
                                            ],
                                        ],
                                    ]
                                );
                                ?>
                            <?php endif; ?>

                            <?php
                            $postalLabelUrl = $model->currentAttemp ? DeliveryPostalLabel::checkPostalLabelExists($model->currentAttemp) : null;
                            if ($postalLabelUrl) {
                                $postalLabelUrl = $model->currentAttemp->delivery->file->getFileUrl();
                                echo Html::img($postalLabelUrl, ['width' => '100%']);
                            }
                            ?>
                        </div>
                    </div>

                <?php endif; ?>


            </div>

            <div class="col-lg-8">

                <div class="row">

                    <?php
                    if ($order->getCuttingPack()) {
                        echo $this->render('cuttingPack', ['cuttingPack' => $order->getCuttingPack()]);
                    }
                    ?>

                    <?php if ($replica) : ?>


                        <div class="col-lg-5">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Order Item</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <?php
                                            $currentAttemp = $order->currentAttemp;
                                            $deadlineTimer = $currentAttemp ? StoreOrderAttemptDeadline::create($currentAttemp, false) : null;
                                            $this->registerJsFile(param('server') . '/js/jquery.countdown.min.js');
                                            ?>
                                            <?php
                                            if ($deadlineTimer) {
                                                echo '<div class="row " style="margin:-5px 0 10px;"><strong>';
                                                echo $deadlineTimer->getTitle() . ':</strong> ';
                                                echo \frontend\widgets\Countdown::widget(
                                                    [
                                                        'id'              => 'countdownattemp-' . $order->currentAttemp->id,
                                                        'options'         => ['class' => 'label label-warning', 'style' => 'font-size:14px;'],
                                                        'datetime'        => $deadlineTimer->getDate('Y-m-d H:i:s'),
                                                        'timeout'         => $deadlineTimer->getTimeout(),
                                                        'finishedMessage' => _t('site.ps', '00:00:00')
                                                    ]
                                                );
                                                echo '</div>';
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="row">


                                        <div class="col-md-3">
                                            <a target="_blank"
                                               href="<?= Url::to(['/store/store-unit/view', 'id' => $replica->store_unit_id]) ?>">
                                                <img src="<?= Model3dFacade::getCover($replica)['image'] ?>"
                                                     class="img-rounded" width="100%"/>
                                            </a>
                                        </div>

                                        <div class="col-md-9">

                                            <?php
                                            $totalPartQty = array_sum(\yii\helpers\ArrayHelper::getColumn($replica->getActiveModel3dParts(), 'qty')) * $order->orderItem->qty;
                                            echo DetailView::widget(
                                                [
                                                    'model'      => $order->orderItem,
                                                    'options'    => ['class' => 'table  no-margin no-first-row-top-border'],
                                                    'attributes' => [
                                                        'unit_id',
                                                        [
                                                            'attribute' => 'qty',
                                                            'label'     => 'Total Qty',
                                                            'value'     => $totalPartQty
                                                        ],
                                                        [
                                                            'label'  => 'Source',
                                                            'format' => 'raw',
                                                            'value'  => $model->isThingiverseOrder() ? '<b class="badge bg-light-blue">Thingiverse Order</b>' : $replica->source
                                                        ],
                                                    ],
                                                ]
                                            );

                                            ?>
                                        </div>


                                    </div>

                                </div>
                                <div class="box-footer box-comments no-padding">
                                    <div class="box-header">
                                        <h3 class="box-title">Files</h3>
                                        <div class="box-tools pull-right">
                                            <button
                                                    ng-click="openChangeMaterialAndColorModal()"
                                                    class="btn btn-box-tool"><i class="fa fa-pencil"></i></button>
                                        </div>
                                    </div>
                                </div>


                                <?php foreach ($replica->getActiveModel3dParts() as $part): ?>

                                    <div class="box-footer">

                                        <div class="row">

                                            <div class="col-md-2">
                                                <a target="_blank"
                                                   href="<?= Url::to(['/store/store-unit/view', 'id' => $replica->store_unit_id]) ?>">
                                                    <img src="<?= Model3dPartService::getModel3dPartRenderUrl($part); ?>"
                                                         class="img-rounded" width="90"/>
                                                </a>
                                            </div>

                                            <div class="col-md-10">
                                                <?php
                                                if ($canDownload) {
                                                    $link = ['/store/store-unit/download-model', 'id' => $part->file->id];
                                                    echo '&nbsp; &nbsp; ' . Html::a(' Download', $link, ['class' => 'fa fa-download']);
                                                }
                                                ?>
                                                <?=
                                                DetailView::widget(
                                                    [
                                                        'model'      => $part,
                                                        'options'    => ['class' => 'table no-margin no-first-row-top-border'],
                                                        'attributes' => [
                                                            [
                                                                'label'  => 'File',
                                                                'format' => 'raw',
                                                                'value'  => "<a class='jspopup  ' style='padding : 0 5px' href='" . param(
                                                                        'server'
                                                                    ) . '/catalog/model3d/preview360?model3dPartId=' . $part->original_model3d_part_id . "'><span class='fa fa-eye'></span></a>"
                                                                    . sprintf(' %s ( x %s )', $part->name, $part->qty * $order->orderItem->qty)
                                                            ],
                                                            [
                                                                'label'  => 'Material group',
                                                                'format' => 'raw',
                                                                'value'  => $part->getCalculatedTexture()->calculatePrinterMaterialGroup()->title ?? ''
                                                            ],
                                                            [
                                                                'label' => 'Material',
                                                                'value' => $part->getCalculatedTexture()->printerMaterial->filament_title ?? ''
                                                            ],
                                                            [
                                                                'label' => 'Color',
                                                                'value' => $part->getCalculatedTexture()->printerColor->title ?? ''

                                                            ],
                                                            [
                                                                'label'  => 'Infill',
                                                                'format' => 'raw',
                                                                'value'  => $part->getCalculatedTexture()->infillText()
                                                            ],
                                                            [
                                                                'label' => 'Weight, Size',
                                                                'value' =>
                                                                    sprintf(
                                                                        '%s - %s',
                                                                        ($part->getWeight() ?
                                                                            MeasureService::getWeightFormatted($part->getWeight(), \lib\MeasurementUtil::GRAM)
                                                                            : ''
                                                                        ),
                                                                        ($part->getSize() ? $part->getSize()->toStringWithMeasurement(Size::TYPE_MM) : '')
                                                                    )
                                                            ],
                                                            [
                                                                'label' => 'Support weight',
                                                                'value' => ($part->getWeight() ?
                                                                    MeasureService::getWeightFormatted(
                                                                        Model3dWeightCalculator::calculateModel3dPartSupportWeight($part),
                                                                        \lib\MeasurementUtil::GRAM
                                                                    )
                                                                    : ''
                                                                )
                                                            ],
                                                            [
                                                                'label' => 'Volume',
                                                                'value' => $part->getVolume()
                                                            ]
                                                        ],
                                                    ]
                                                );
                                                ?>
                                            </div>


                                        </div>

                                    </div>

                                <?php endforeach; ?>
                                <?php if ($order): ?>
                                    <a href="<?php echo Url::toRoute(['/store/store-order/parts-list', 'orderId' => $order->id]); ?>"
                                       target="_blank"
                                       class="btn btn-default btn-sm service-order-parts">
                                        <span class="tsi tsi-doc"></span>
                                        <?= _t('user.order', 'Parts list'); ?>
                                    </a>
                                <?php endif; ?>
                            </div>


                            <?php if ($model->isThingiverseOrder()): ?>
                                <!-- thingiverse order block -->
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h3 class="box-title">Thingiverse Order Details</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i
                                                        class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body no-padding">
                                        <?php
                                        $thingId = $model3d->originalModel3d->thingiverseThing ? $model3d->originalModel3d->thingiverseThing->thing_id : '';
                                        echo DetailView::widget(
                                            [
                                                'model'      => $model->thingiverseOrder,
                                                'options'    => ['class' => 'table'],
                                                'attributes' => [
                                                    [
                                                        'label'  => 'Thingiverse Model',
                                                        'format' => 'raw',
                                                        'value'  => Html::a(
                                                            $thingId,
                                                            'http://www.thingiverse.com/thing:' . $thingId,
                                                            ['target' => '_blank']
                                                        )
                                                    ],
                                                    [
                                                        'attribute' => 'thingiverse_order_id',
                                                        'format'    => 'raw',
                                                        'value'     => Html::a(
                                                                $model->thingiverseOrder->thingiverse_order_id,
                                                                param('thingiverse_url') . '/order:' . $model->thingiverseOrder->thingiverse_order_id,
                                                                ['target' => '_blank']
                                                            ) . ' / ' . Html::a(
                                                                'local',
                                                                '/payment/thingiverse/thingiverse-report?ThingiverseReportSearch%5Border_id%5D=' . $model->thingiverseOrder->thingiverse_order_id,
                                                                ['target' => '_blank']
                                                            ),
                                                    ],
                                                    'updated_at:datetime',
                                                    'is_shipped',
                                                    'is_cancelled',
                                                    'refund_note',
                                                    'note',
                                                    'status',
                                                    [
                                                        'label' => 'Thingiverse total fee',
                                                        'value' => $model->thingiverseOrder->getTotalThingiverseFee()
                                                    ],
                                                    [
                                                        'label' => 'Access Token',
                                                        'value' => $model->user->thingiverseUser->access_token
                                                    ],
                                                    [
                                                        'label' => 'Email',
                                                        'value' => $model->user->thingiverseUser->email
                                                    ],
                                                ]
                                            ]
                                        );
                                        ?>


                                        <span>

                                            Email login link:
https://www.treatstock.com/user/email-login?redirectTo=/workbench/order/view/<?= $order->id; ?>

                                        </span> <br/><br/>
                                        <span>Review link:
                                            <?php
                                            $order = $model;
                                            $hash  = \common\modules\thingPrint\components\ThingiverseOrderReview::getOrderAccessHash($order);
                                            echo param('server') . sprintf('/workbench/order/add-anonymouse-review/?orderId=%d&hash=%s', $order->id, $hash);
                                            ?>
                                        </span>
                                    </div>


                                    <div class="box-footer">
                                        <a href="/store/store-order/view?id=<?= $model->id; ?>&thingiverseOrderFetch=1"
                                           class="btn btn-default">API Refresh</a>
                                        <?php
                                        if ((!$model->thingiverseOrder->is_cancelled) && ($model->thingiverseOrder->status === ThingiverseOrder::STATUS_PLACED) && ($model->getPrimaryInvoice() && !$model->getPrimaryInvoice()->getActivePayment())) {
                                            ?>
                                            <a href="/store/store-order/fix-payed?id=<?= $model->id; ?>"
                                               class="btn btn-default">Thingiverse PAYED</a>
                                            <?php
                                        }
                                        ?>
                                    </div>


                                </div>

                                <!-- thingiverse order block end -->
                            <?php endif; ?>
                        </div>

                    <?php endif; ?>


                    <div class="col-lg-7">
                        <?php if (true or !$model->isThingiverseOrder()): ?>
                            <div class="box box-solid ">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Additional Services
                                        (<?= count($model->storeOrderPositions); ?>)</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body no-padding">
                                    <?php if (!empty($model->storeOrderPositions)): ?>
                                        <?= GridView::widget(
                                            [
                                                'showHeader'   => true,
                                                'layout'       => '{items}',
                                                'options'      => ['class' => ''],
                                                'showFooter'   => false,
                                                'dataProvider' => StoreOrderPosition::getDataProvider(['order_id' => $model->id]),
                                                'columns'      => [
                                                    [
                                                        'attribute' => 'user.username',
                                                        'format'    => 'raw',
                                                        'value'     => function ($model) {
                                                            $username = Backend::displayUser($model->user);
                                                            return $username;
                                                        }
                                                    ],
                                                    'title',
                                                    [
                                                        'attribute' => 'file_ids',
                                                        'format'    => 'raw',
                                                        'value'     => function (StoreOrderPosition $model) {
                                                            if (empty($model->file_ids)) {
                                                                return '';
                                                            }
                                                            $files  = \common\models\File::findAll($model->file_ids);
                                                            $result = [];
                                                            foreach ($files as $k => $v) {
                                                                $result[] = Html::a($v->id, '/store/store-unit/download-model?id=' . $v->id, ['title' => $v->name]);
                                                            }
                                                            return implode(", ", $result);

                                                        }
                                                    ],
                                                    'created_at:datetime',
                                                    'status',
                                                    [
                                                        'attribute' => 'amount',

                                                        'format' => 'raw',
                                                        'value'  => function (StoreOrderPosition $model) {
                                                            $currency = $model->order->getCurrency();
                                                            $amount     = displayAsCurrency($model->amount, $currency);
                                                            $feeExclude = $model->fee ? ' + fee ' . displayAsCurrency($model->fee, $currency) : '';
                                                            $feeInclude = $model->fee_include ? ' - fee ' . displayAsCurrency($model->fee_include, $currency) : '';
                                                            return $amount . $feeExclude . $feeInclude;
                                                        }
                                                    ],
                                                    [
                                                        'format' => 'raw',
                                                        'value'  => function (StoreOrderPosition $model) {
                                                            return $model->status !== StoreOrderPosition::STATUS_CANCELED ? Html::a('<i class="fa fa-trash"></i>',
                                                                '/store/store-order/delete-position?id=' . $model->id,
                                                                ['data-confirm' => 'Are you sure?']) : '';
                                                        }
                                                    ],
                                                ],
                                            ]
                                        ); ?>
                                    <?php endif; ?>
                                    <div style="padding: 5px;">
                                        <?php
                                        $orderPosition           = new StoreOrderPositionForm();
                                        $orderPosition->order_id = $model->id;
                                        $orderPositionForm       = \yii\bootstrap\ActiveForm::begin(
                                            [
                                                'action'      => ['/store/store-order/add-position'],
                                                'layout'      => 'inline',
                                                'options'     => ['style' => 'border: 1px solid #dfdfdf;padding:3px;', 'enctype' => 'multipart/form-data'],
                                                'fieldConfig' => [
                                                    'labelOptions' => ['class' => ''],
                                                    'template'     => "<span>{label}</span><br /> {input} ",
                                                ]
                                            ]
                                        ); ?>
                                        <?= $orderPositionForm->field($orderPosition, 'order_id')->hiddenInput()->label(false); ?>
                                        <?= $orderPositionForm->field($orderPosition, 'title')->textInput(['maxlength' => true, 'size' => 20]) ?>
                                        <?= $orderPositionForm->field($orderPosition, 'files[]')->fileInput(['multiple' => true, 'size' => 3]) ?>
                                        <?= $orderPositionForm->field($orderPosition, 'amount')->textInput(['size' => 2])->label('Amount ($)') ?>
                                        <div class="form-group"><br/>
                                            <?= \yii\bootstrap\Html::submitButton('Add', ['class' => 'btn btn-primary']); ?>
                                        </div>
                                        <?php \yii\bootstrap\ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($attempt && $attempt->moderation): ?>

                            <div class="box box-solid print-results">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Order manufactured files</h3>
                                </div>
                                <div class="box-body">
                                    <?php
                                    /** @var $userLike UserLike */
                                    $userLike = UserLike::findOne(['object_id' => $attempt->id, 'object_type' => UserLike::TYPE_ORDER_ATTEMPT]);
                                    if ($userLike && $userLike->is_dislike): ?>
                                        <div class="bg-danger" style="padding: 10px;">Customer disliked these results.
                                            - <?= Yii::$app->formatter->asDatetime($userLike->created_at); ?></div>
                                    <?php elseif ($userLike): ?>
                                        <div class="bg-success" style="padding: 10px;">Customer liked these results.
                                            - <?= Yii::$app->formatter->asDatetime($userLike->created_at); ?></div>
                                    <?php endif; ?>
                                    <?php if (!$attempt->moderation->files) : ?>

                                        <div class="text-center">
                                            <i>No printed files</i>
                                        </div>
                                    <?php else: ?>
                                        <a href="/store/store-order/moderation-download-all?attemptId=<?= $attempt->id; ?>"
                                           class="btn btn-info">Download all (ZIP)</a> <br/>

                                    <?php endif ?>



                                    <?php foreach ($attempt->moderation->files as $moderationFile): ?>
                                        <?php if (FileTypesHelper::isVideo($moderationFile->file)): ?>
                                            <video width="160" height="140" controls preload="metadata">
                                                <source src="<?= $moderationFile->file->getFileUrl(); ?>#t=0.1"
                                                        type="video/mp4">
                                            </video>
                                        <?php else: ?>
                                            <div style="border: 1px solid #dfdfdf; padding: 4px; margin: 2px; display:inline-block;">
                                                <a href="<?= $moderationFile->file->getFileUrl() ?>" class="jspopup">
                                                    <img src="<?= ImageHtmlHelper::getThumbUrl(
                                                        $moderationFile->file->getFileUrl(),
                                                        ImageHtmlHelper::THUMB_SMALL,
                                                        ImageHtmlHelper::THUMB_SMALL
                                                    ); ?>" alt="..." class="img-rounded"
                                                         width="<?= ImageHtmlHelper::THUMB_SMALL; ?>">
                                                </a><br/>
                                                <a href="/site/image/rotate?direction=right&fileUuid=<?= $moderationFile->file->uuid; ?>"
                                                   title="Rotate right">
                                                    <i class="fa fa fa-rotate-right"></i>
                                                </a>
                                                <a href="/store/store-order/delete-printed-image?id=<?= $moderationFile->id; ?>"
                                                   title="Delete" class="ts-confirm"
                                                   data-confirm="Are you sure you want to delete this item?">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                                <a href="/site/image/rotate?direction=left&fileUuid=<?= $moderationFile->file->uuid; ?>"
                                                   title="Rotate left">
                                                    <i class="fa fa-rotate-left"></i>
                                                </a>
                                                <div>
                                                    <a href="/store/store-order/set-main-review-image?id=<?= $moderationFile->id; ?>"
                                                       title="Main image" class="ts-confirm"
                                                       data-confirm="Set as the main image for a letter with a review?">
                                                        <?php if ($moderationFile->isMainReviewFile()): ?>
                                                            <i class="fa fa-circle"></i>
                                                        <?php else: ?>
                                                            <i class="fa fa-circle-o"></i>
                                                        <?php endif; ?>
                                                        Set main file
                                                    </a>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <div style="margin:20px" class="text-right">
                                        <?php
                                        $form                                = \yii\bootstrap\ActiveForm::begin([
                                            'options' => ['enctype' => 'multipart/form-data'],
                                            'layout'  => 'inline',
                                            'action'  => '/store/store-order/upload-printed-images?attempt_id=' . $attempt->id
                                        ]);
                                        $uploadPrintedFilesModel             = \yii\base\DynamicModel::validateData(
                                            ['attempt_id', 'moreImages'],
                                            [['attempt_id', 'safe'], [['moreImages'], 'image', 'skipOnEmpty' => true, 'maxFiles' => 15]]
                                        );
                                        $uploadPrintedFilesModel->attempt_id = $attempt->id;
                                        #echo $form->field($uploadPrintedFilesModel, 'attempt_id')->hiddenInput()->label(false);
                                        ?>
                                        <?php echo $form->field($uploadPrintedFilesModel, 'moreImages[]')->fileInput(['multiple' => true])->label('Printed Files') ?>
                                        <?= Html::submitButton('Upload', ['class' => 'btn btn-primary']) ?>
                                        <?php \yii\bootstrap\ActiveForm::end(); ?>
                                    </div>
                                </div>

                                <?php if (!$attempt->moderation->isNew()) : ?>

                                    <div class="box-footer">

                                        <?php if ($attempt->moderation->isAccepted()) : ?>
                                            Moderation is <span class="text-green">accepted</span>.
                                        <?php elseif ($attempt->moderation->isRejected()): ?>
                                            Moderation is <span
                                                    class="text-red">rejected</span> : <?= $attempt->moderation->reject_comment ?>.
                                        <?php endif ?>
                                    </div>

                                <?php endif; ?>


                                <?php if (in_array($attempt->status, [StoreOrderAttemp::STATUS_PRINTED])) : ?>

                                    <div class="box-footer">

                                        <button type="button" data-toggle="tooltip" title=""
                                                data-original-title="Add result image"
                                                class="btn btn-box-tool btn-ajax-modal"
                                                data-pjax="0"
                                                data-target="#printAttemptResultModal"
                                                value="<?= Url::toRoute(['store/store-order/print-attempt-result', 'attemptId' => $model->currentAttemp->id]) ?>">
                                            <i class="fa fa-plus"></i> &nbsp;Add result
                                        </button>

                                        <?php if ($attempt->moderation->isNew() && $attempt->moderation->files): ?>

                                            <div class="pull-right">
                                                <a href="<?= Url::toRoute(['/store/store-order/print-review-ok', 'orderId' => $model->id]); ?>"
                                                   class="btn btn-success">Accept</a>
                                                <button type="button"
                                                        class="btn btn-warning btn-ajax-modal"
                                                        value="<?= Url::toRoute(['/store/store-order/print-review-reject', 'orderId' => $model->id]) ?>"
                                                        data-pjax="0"
                                                        data-target="#rejprint">Reject
                                                </button>
                                            </div>

                                        <?php endif ?>
                                    </div>

                                <?php endif ?>

                            </div>

                        <?php endif; ?>


                        <?php if ($order->preorder): $preorder = $order->preorder; ?>
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Preorder Details</h3>
                                </div>
                                <div class="box-body">
                                    <?= $this->render('@backend/views/store/preorder/viewModel.php', [
                                        'preorder'                 => $preorder,
                                        'preorderWorkDataProvider' => PreorderService::getWorksDataProvider($preorder)
                                    ]);
                                    ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Order History</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body no-padding">
                                <?= GridView::widget(
                                    [
                                        'options'      => ['class' => ''],
                                        'layout'       => '{items}',
                                        'tableOptions' => ['class' => 'table no-margin',],
                                        'showFooter'   => false,
                                        'dataProvider' => $history['dataProvider'],
                                        //'filterModel' => $history['searchModel'],
                                        'columns'      => [
                                            'action_id',
                                            [
                                                'attribute' => 'admin_user_id',
                                                'value'     => function ($model) {
                                                    $userAdmin = \common\models\UserAdmin::findOne($model->admin_user_id);
                                                    if ($userAdmin) {
                                                        return $userAdmin->username;
                                                    }
                                                    return $model->admin_user_id;
                                                }
                                            ],
                                            [
                                                'attribute' => 'created_at',
                                                'value'     => 'created_at'
                                            ],
                                            'comment',
                                        ],
                                    ]
                                ); ?>
                            </div>
                        </div>

                        <!-- payment  transactions -->

                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-12">
                        <?php
                        $transactionIds     = ArrayHelper::getColumn($model->paymentTransactions, 'id');
                        $refundDataProvider = PaymentTransactionRefund::getDataProvider(['transaction_id' => $transactionIds]);
                        if ($refundDataProvider->getCount() > 0): ?>
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Transaction Refunds</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse">
                                            <i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body no-padding">
                                    <div class="table-responsive">
                                        <?php
                                        echo GridView::widget(
                                            [
                                                'options'      => ['class' => ''],
                                                'layout'       => '{items}',
                                                'tableOptions' => ['class' => 'table'],
                                                'dataProvider' => $refundDataProvider,
                                                'columns'      => [
                                                    'id',
                                                    'transaction_id',
                                                    'created_at',
                                                    [
                                                        'label'     => 'User',
                                                        'attribute' => 'user_id',
                                                        'format'    => 'raw',
                                                        'value'     => function ($model) {
                                                            return \backend\models\Backend::displayUser($model->user_id);
                                                        }
                                                    ],
                                                    [
                                                        'label'     => 'Amount',
                                                        'attribute' => 'amount',
                                                        'format'    => 'raw',
                                                        'value'     => function (PaymentTransactionRefund $model) {
                                                            if ($model->isFullPrintRefund()) {
                                                                return '<div class="alert-warning">Total refund</div>' . displayAsMoney($model->getMoneyAmount());
                                                            } else {
                                                                return displayAsMoney($model->getMoneyAmount());
                                                            }
                                                        }
                                                    ],
                                                    [
                                                        'attribute' => 'comment',
                                                        'format'    => 'raw',
                                                        'value'     => function ($m) use ($model) {
                                                            $comment = \H($m->comment);
                                                            if ($model->getFirstReplicaItem()) {
                                                                //                                                                if ($m->amount >= StorePricer::getPrintPrice($model->id)->price) {
                                                                //                                                                    $comment = "<b class='label label-danger'>FULL REFUND FOR PRINT FEE</b><br/> " . $m->comment;
                                                                //                                                                }
                                                            }
                                                            return $comment;
                                                        }
                                                    ],
                                                    'status',
                                                    'item',
                                                    'refund_type',
                                                    [
                                                        'contentOptions' => ['style' => 'width: 150px;'],
                                                        'format'         => 'raw',
                                                        'value'          => function ($model) use ($refundService, $order) {
                                                            $btn = [];

                                                            if ($model->status != PaymentTransactionRefund::STATUS_APPROVED) {
                                                                if ($refundService->canRefund($model->transaction)) {
                                                                    $btn[] = Html::a(
                                                                        'Approve',
                                                                        ['store/payment-transaction/approve-refund', 'id' => $model->id],
                                                                        [
                                                                            'class'        => 'btn btn-sm btn-warning',
                                                                            'data-confirm' => "Are you sure you want to approve this refund?",
                                                                        ]
                                                                    );
                                                                } else {
                                                                    //  $btn[] = $refundService->lastErrorMessage;
                                                                }
                                                            }
                                                            if ($model->status == 'new') {
                                                                $btn[] = Html::a(
                                                                    'Delete',
                                                                    ['store/payment-transaction/refund-delete', 'id' => $model->id],
                                                                    [
                                                                        'class'        => 'btn btn-sm btn-default',
                                                                        'data-confirm' => "Are you sure you want to delete this refund request?",
                                                                    ]
                                                                );
                                                            }
                                                            return implode(' ', $btn);
                                                        }
                                                    ]
                                                ]
                                            ]

                                        ); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($model->primaryPaymentInvoice && $model->primaryPaymentInvoice->paymentBankInvoice): ?>
                            <?php $bankInvoiceUuid = $model->primaryPaymentInvoice->paymentBankInvoice->uuid; ?>
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Bank Invoice</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <p>
                                        <?php echo Html::a('View', ['/store/payment-bank-invoice/view', 'id' => $bankInvoiceUuid], ['class' => 'btn btn-primary']) ?>
                                        <?php echo Html::a('Update', ['/store/payment-bank-invoice/update', 'id' => $bankInvoiceUuid], ['class' => 'btn btn-primary']) ?>
                                        <?php if (!$model->primaryPaymentInvoice->paymentBankInvoice->isPaid()): ?>
                                            <?php echo Html::a('Extend date', ['/store/payment-bank-invoice/extend', 'id' => $bankInvoiceUuid],
                                                ['class' => 'btn btn-primary']) ?>
                                        <?php endif; ?>
                                        <?php if ($model->primaryPaymentInvoice->paymentBankInvoice->isNew()): ?>
                                            <?php echo Html::a('Set Pay', ['/store/payment-bank-invoice/settle', 'id' => $bankInvoiceUuid], ['class' => 'btn btn-success']) ?>
                                            <?php echo Html::a('Cancel', ['/store/payment-bank-invoice/void', 'id' => $bankInvoiceUuid], ['class' => 'btn btn-danger']) ?>
                                        <?php endif; ?>
                                    </p>

                                    <?php echo DetailView::widget(
                                        [
                                            'model'      => $model->primaryPaymentInvoice->paymentBankInvoice,
                                            'attributes' => [
                                                'uuid',
                                                'created_at',
                                                'date_due',
                                                'status',
                                                'ship_to:html',
                                                'bill_to:html',
                                                'bank_details:html',
                                                'payment_details:html',
                                            ],
                                        ]
                                    ) ?>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Invoices</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <?= $this->render('@backend/views/store/payment-invoice/paymentInvoices.php', [
                                    'primaryInvoice' => $order->primaryInvoice,
                                    'invoices'       => $order->paymentInvoices
                                ]) ?>
                            </div>
                        </div>
                        <?php if ($model->currentAttemp): ?>

                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Review</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body no-padding">
                                    <?= OrderReviewList::widget(['query' => StoreOrderReview::find()->forAttemp($model->currentAttemp)]); ?>
                                </div>
                            </div>

                        <?php endif ?>


                    </div>
                </div>

            </div>

        </div>

    </div>
</div>

<style type="text/css">

    .table th {
        white-space: nowrap;
    }

    .table.no-first-row-top-border tr:first-child th,
    .table.no-first-row-top-border tr:first-child td {
        border-top: none;
    }

    .print-results img {
        filter: grayscale(80%);
        transition: filter 0.15s ease-out;
    }

    .print-results img:hover {
        filter: none;
    }

    .not-set {
        color: #d8d8d8;
    }

    code {
        background: none;
    }

    .with-border {
        border-bottom: 1px solid #f4f4f4;
    }

    .with-border-top {
        border-top: 1px solid #f4f4f4;
    }

    .with-padding {
        padding: 15px;
    }

    .texture-table tr td:first-child {
        width: 90px;
    }

    .change-offer-modal .offers-table tbody tr td:first-child {
        padding-left: 15px;
    }

    .change-offer-modal .offers-table tbody tr.highlighted {
        background-color: #fcf8e3;
    }

    #changeps .modal-dialog, .change-offer-modal {
        width: 100%;
    }

</style>
<script type="text/javascript">
    orderId = <?= $order->id?>;
</script>