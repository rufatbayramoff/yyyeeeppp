<?php

use backend\assets\AdminLteAsset;
use backend\assets\AngularAppAsset;
use backend\assets\AppAsset;
use backend\serializers\StoreOrderSerializer;
use common\models\StoreOrder;
use lib\money\Money;

/** @var StoreOrder $storeOrder */
/** @var Money|null $producePrice */
/** @var Money|null $shippingPrice */
/** @var Money|null $treatstockFee */

AppAsset::register($this);
AdminLteAsset::register($this);
AngularAppAsset::register($this);

$orderSerialized                  = StoreOrderSerializer::serialize($storeOrder);
$orderSerialized['totalPrice']    = round($totalPrice->getAmount(), 2);
$orderSerialized['producePrice']  = round($producePrice->getAmount(), 2);
$orderSerialized['shippingPrice'] = round($shippingPrice->getAmount(), 2);
$orderSerialized['treatstockFee'] = round($treatstockFee->getAmount(), 2);

Yii::$app->angular
    ->service(['router', 'user', 'camelizer', 'notify'])
    ->controllerBackend('store-order/change-printer')
    ->controllerBackend('store-order/change-printer-models')
    ->controllerParam('order', $orderSerialized)
    ->registerScripts($this);

/** @var $storeOrder StoreOrder */
?>
<div class="wrapper" ng-app="app">
    <div ng-controller="ChangePrinterController">
        <div class="row">
            <div class="col-md-12">
                <div style="font-size: 20px;">
                    Change Printer for order
                    <a target="_blank"
                       href="/store/store-order/view?id=<?= $storeOrder->id ?>">#<?= $storeOrder->id ?></a>
                </div>
            </div>
        </div>
        <hr>
        <div class="row" ng-cloak>
            <div ng-if="warnings.length > 0" class="with-border with-padding">
                <div class="callout callout-danger no-margin">
                    <h4>Warnings</h4>
                    <p ng-repeat="warning in warnings" class="no-margin">{{warning}}</p>
                </div>
            </div>
            <div class="box-comments with-border with-padding">
                <div class="pull-left " style="margin-left: 20px;">
                    <input ng-model="hideCancelled" ng-change="changeFilters()" type="checkbox"
                           name="hideCancelled" ng-disabled="loading"> Hide cancelled by PS
                    <div class="form-inline">
                        <div class="form-group">
                            <input ng-model="exactMaterialColor" ng-change="changeFilters()" type="checkbox"
                                   name="exactMaterialColor" ng-disabled="loading"> Exact material and color
                        </div>
                        <div class="form-group" style="padding-left: 20px;">
                            <input ng-model="sameCurrency" ng-change="changeFilters()" type="checkbox"
                                   name="sameCurrency" ng-disabled="loading"> Same currency
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-right: 20px;">
                    <div>
                        <span>Total manufacture : <b>{{order.totalPrice}}</b>&nbsp;&nbsp;</span>
                        <span>Produce price : <b>{{order.producePrice}}</b>&nbsp;&nbsp;</span>
                        <span>Shipping price : <b>{{order.shippingPrice}}</b></span>
                    </div>
                    <div style="text-align: right">
                        <span ng-if="order.currency">Currency : <b>{{order.currency}}</b>&nbsp;&nbsp;</span>
                        <span ng-if="order.treatstockFee">Treatstock fee : <b>{{order.treatstockFee}}</b>&nbsp;&nbsp;</span>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div style="padding-left: 20px;padding-top: 10px;">
                <div class="pull-left">
                    <button type="button" class="btn btn-default" ng-click="close()">Close</button>
                    <button type="button" class="btn btn-primary" loader-click="makeOffers()" ng-disabled="loading">Make
                        offers
                    </button>
                </div>
                <div class="pull-right" style="margin-right: 20px;">
                    <div class="changePrinterHighlighted"
                         style="display: inline-block; width: 12px; height: 12px;border: solid black 1px"></div>
                    - Current order attempt
                    <div class="changePrinterSafe"
                         style="margin-left: 15px; display: inline-block; width: 12px; height: 12px;border: solid black 1px"></div>
                    - Total price is lower than original order
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="table-responsive">
                <table class="table offers-table" style="margin-bottom: 0">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Printer</th>
                        <th>Ps</th>
                        <th class="text-right" style="min-width: 220px">Total / Produce / Shipping</th>
                        <th>Contacts</th>
                        <th>Delivery Details</th>
                        <th>Layer Resolution</th>
                        <th>Location</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr ng-if="loading">
                        <td colspan="8" align="center">
                            <div class="preloader"><h3>Loading...</h3></div>
                        </td>
                    </tr>
                    <tr ng-repeat="offer in offersSorted()"
                        ng-class="{changePrinterHighlighted : offer.isHighlighted, changePrinterSafe: isSafeOffer(offer)}">
                        <td style="padding-left:20px">
                            <input type="checkbox" ng-model="selectesOffers[offer.printer.id]"/>
                        </td>
                        <td>
                            <printer-link printer="offer.printer"></printer-link>
                            <br/>
                            <span
                                    ng-if="offer.printer.isCertificated"
                                    class="label label-success"
                                    style="opacity: .5">certificated</span>
                        </td>
                        <td>
                            <ps-link ps="offer.printer.ps"></ps-link>
                        </td>
                        <td class="text-right">
                            <b>{{offer.getTotalPrice()}} {{offer.producePrice.currency}}</b> /
                            {{offer.getProducePriceAmount()}} /
                            <span ng-if="offer.shippingPrice=='TBC'"><button class="btn btn-link"
                                                                             title="Click here to calculate Delivery price. We pay for each price calculation."
                                                                             ng-click="calcDeliveryTBC(offer.printer.id)">TBC</button></span>
                            <span ng-if="offer.shippingPrice!='TBC'">{{offer.getShippingPriceAmount()}}</span>
                            <span ng-if="offer.loadingDeliveryPrice"> - updating...</span>
                            <span ng-if="offer.deliveryError"> (<span class="changePrinterTBCError">{{offer.deliveryError}}</span>)</span>
                        </td>
                        <td>
                            <i>{{offer.printer.ps.phone}}</i><br/>
                            {{offer.printer.ps.email}}
                        </td>
                        <td>
                            <span style="white-space : nowrap">{{offer.printer.deliveryDetails.title}}</span>
                            <span class="small" ng-if="offer.printer.deliveryDetails.carrier">
                                            <br/> Carrier : {{offer.printer.deliveryDetails.carrier}}
                                        </span>
                            <span class="small" ng-if="offer.printer.deliveryDetails.carrier_price">
                                            <br/> Carrier price : {{offer.printer.deliveryDetails.carrier_price}}
                            </span>
                            <span class="small" ng-if="offer.printer.deliveryDetails.international_carrier_price">
                                            <br/> International price : {{offer.printer.deliveryDetails.international_carrier_price}}
                            </span>
                            <span class="small" ng-if="offer.printer.deliveryDetails.packing_price">
                                            <br/> Packaging fee: {{offer.printer.deliveryDetails.packing_price}}
                                        </span>
                            <span class="small" ng-if="offer.printer.deliveryDetails.free_delivery">
                                            <br/> Free Delivery : {{offer.printer.deliveryDetails.free_delivery}}
                                        </span>
                        </td>
                        <td ng-bind-html="offer.printer.layerResolution | html"></td>
                        <td>{{offer.printer.location}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>




