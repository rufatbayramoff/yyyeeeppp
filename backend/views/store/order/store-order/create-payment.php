<?php


/** @var \backend\models\store\CreatePaymentForm $form */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var \yii\web\View $this */

$this->title = 'Create payment for order #'.$form->getOrder()->id;
?>

<?php $editform = ActiveForm::begin([]) ?>

<?=
$editform->field($form, 'fromUserId')->widget(kartik\select2\Select2::classname(), [
    'initValueText' => '',
    'options' => ['placeholder' => _t('front.messages', 'Select user')],
    'pluginOptions' => [
        'allowClear' => false,
        'minimumInputLength' => 2,
        'ajax' => [
            'url' => \yii\helpers\Url::to(['user-list']),
            'dataType' => 'json',
            'data' => new yii\web\JsExpression('function(params) { return {query:params.term}; }')
        ],
    ],
]);
?>

<?=
$editform->field($form, 'toUserId')->widget(kartik\select2\Select2::classname(), [
    'initValueText' => '',
    'options' => ['placeholder' => _t('front.messages', 'Select user')],
    'pluginOptions' => [
        'allowClear' => false,
        'minimumInputLength' => 2,
        'ajax' => [
            'url' => \yii\helpers\Url::to(['user-list']),
            'dataType' => 'json',
            'data' => new yii\web\JsExpression('function(params) { return {query:params.term}; }')
        ],
    ],
]);
?>

<?= $editform->field($form, 'amount')->textInput();?>
<?= $editform->field($form, 'comment')->textInput();?>

<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>


<?php ActiveForm::end(); ?>
