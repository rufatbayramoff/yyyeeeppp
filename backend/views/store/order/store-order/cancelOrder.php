<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrder */
/* @var $form yii\widgets\ActiveForm */
/* @var $cancelForm frontend\models\order\CancelOrderForm */
?>

<div class="store-order-cancel-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php
    echo $form->field($cancelForm, 'reasonId')->widget(kartik\widgets\Select2::classname(), [
        'data' => $cancelForm->getSuggestList(),
        'options' => ['placeholder' => 'Select', 'style' => 'width:100%; display: block;'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    echo $form->field($cancelForm, 'reasonDescription')->textarea(['maxlength' => true]);
    ?>
    <div class="form-group">
        <?= Html::submitButton('Cancel order', ['class' => 'btn btn-danger ajax-submit']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
