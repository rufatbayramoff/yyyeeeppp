<?php

use backend\components\columns\PsColumn;
use backend\components\columns\UserColumn;
use backend\models\search\StoreOrderSearch;
use common\components\ArrayHelper;
use common\interfaces\Model3dBaseInterface;
use common\models\DeliveryType;
use common\models\StoreOrder;
use common\models\UserAddress;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-9">
            <?php $form = ActiveForm::begin(
                [
                    'action' => ['index'],
                    'method' => 'get',
                ]
            ); ?>
            <div class="col-md-7 col-sm-7 col-lg-5">
                <?= $form->field($searchModel, 'search') ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'notTest')->dropDownList([0 => 'All', 1 => 'notTest']) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'orderItemType')->dropDownList(
                    ['' => 'All',
                        StoreOrderSearch::ORDER_ITEM_TYPE_MODEL3D => '3D Models',
                        StoreOrderSearch::ORDER_ITEM_TYPE_CUTTING => 'Cutting',
                        StoreOrderSearch::ORDER_ITEM_TYPE_PREORDER => 'Preorder',
                    ]) ?>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-3">
                <input type="submit" value="Search Orders" class="btn btn-primary searchModel-btn"/>
                <style>
                    @media (min-width: 1200px) {
                        .searchModel-btn {
                            margin-top: 25px;
                        }
                    }
                </style>
            </div>
            <?php ActiveForm::end(); ?>

    </div>
    <div class="col-sm-3">
        <?php
        $url = yii\helpers\Url::toRoute(['store/store-order/download-report']);
        echo yii\helpers\Html::a('<i class="fa fa-shopping-cart"></i> Download Report', $url, ['class' => 'btn btn-default btn-sm pull-right']);
        ?>
    </div>
</div>
<br>
<div style="margin:10px;">
    <span style="background: #ddffdd;padding:5px;margin:5px;">Delivered</span>
    <span style="background: #bbffbb;padding:5px;margin:5px;">Received</span>
    <span style="background: #ffd3d3;padding:5px;margin:5px;">Change Printer</span>
    <span style="background: #ffdfdf;padding:5px;margin:5px;">Printed</span>
    <span style="background: #efefef;padding:5px;margin:5px;">Removed</span>
</div>
<div class="store-order-index">
    <?= GridView::widget(
        ['dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions' => function (StoreOrder $model, $index, $widget, $grid) {
                if ($model->order_state == StoreOrder::STATE_REMOVED) {
                    return [
                        'style' => 'background: #efefef; color: #888!important'
                    ];
                } else {
                    if (empty($model->current_attemp_id)) {
                        return [
                            'style' => 'background: #ffd3d3;'
                        ];
                    } else {
                        if ($model->currentAttemp->status == 'delivered' || $model->currentAttemp->status == 'sent') {
                            return [
                                'style' => 'background: #ddffdd;'
                            ];
                        }
                        if ($model->currentAttemp->status == 'received') {
                            return [
                                'style' => 'background: #bbffbb;'
                            ];
                        }
                        if ($model->currentAttemp->status == 'printed') {
                            return [
                                'style' => 'background: #ffdfdf;'
                            ];
                        }
                        if ($model->currentAttemp->status == 'cancelled') {
                            return [
                                'style' => 'background: #efefef; color: #777!important'
                            ];
                        }
                    }
                }
            }, 'columns' => [
            [
                'attribute' => 'id',
                'format' => 'raw',
                'value' => function (StoreOrder $model) {
                    $url = Url::toRoute(['store/store-order/view', 'id' => $model->id]);
                    return Html::a('#' . $model->id, $url,
                        ['class' => 'btn  btn-ghost btn-info', 'target' => '_blank', 'style' => '']
                    );
                }
            ],
            [
                'label' => 'Invoice',
                'attribute' => 'primary_payment_invoice_uuid',
                'format' => 'raw',
                'value' => function (StoreOrder $model) {
                    return $model->primary_payment_invoice_uuid;
                }
            ],
            [
                'label' => 'User',
                'attribute' => 'user_id',
                'relation' => 'user',
                'class' => UserColumn::class,
            ],
            [
                //'label' => 'Model author',
                'format' => 'raw',
                'attribute' => 'modelAuthor',
                'value' => function (StoreOrder $model) {
                    $firstItem = $model->getFirstItem();

                    if ($firstItem && $firstItem->model3dReplica) {
                        $modelAuthor = $firstItem->model3dReplica->originalModel3d->user;

                        if (!$modelAuthor) {
                            return '';
                        }

                        return UserColumn::getMenu($modelAuthor);
                    }

                    return null;
                }
            ],
            [
                'label' => 'Order Items',
                'attribute' => 'itemTitle',
                'format' => 'raw',
                'value' => function (StoreOrder $model) {
                    $storeOrderItems = $model->storeOrderItems;
                    $result = [];
                    foreach ($storeOrderItems as $orderItem) {
                        if ($orderItem->hasModel()) {
                            $itemTitle = \H($orderItem->unit->model3d->title);
                            $link = \yii\helpers\Url::toRoute(['store/store-unit/view', 'id' => $orderItem->unit->id]);
                            $href = Html::a($itemTitle, $link);
                            $result[] = $href;
                        }
                        if ($orderItem->hasCuttingPack()) {
                            $itemTitle = $orderItem->cuttingPack->getTitle();
                            $link = \yii\helpers\Url::toRoute(['/store/cutting-pack/view', 'uuid' => $orderItem->cuttingPack->uuid]);
                            $href = Html::a($itemTitle, $link);
                            $result[] = $href;
                        }
                    }
                    return implode("<br />", $result);
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'price_total',
                'value' => function (StoreOrder $model) {
                    $html = app('formatter')->asCurrency($model->getTotalPrice(), $model->getCurrency());
                    return $html;
                }
            ],
            [
                'attribute' => 'payment_status',
                'label' => 'Payment',
                'value' => function (StoreOrder $model) {
                    return $model->getPaymentStatus();
                },
                'filter' => Html::textInput('StoreOrderSearch[payment_status]', is_array($searchModel->payment_status) ? reset($searchModel->payment_status) : $searchModel->payment_status)
            ],
            [
                'attribute' => 'source',
                'value' => function (StoreOrder $model) {
                    $item = $model->getFirstItem();
                    return $item->model3dReplica->source ?? null;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'source',
                    array_combine(Model3dBaseInterface::POSSIBLE_SOURCES, Model3dBaseInterface::POSSIBLE_SOURCES),
                    ['class' => 'form-control', 'prompt' => 'All']),
            ],
            [
                'attribute' => 'order_status',
                'value' => function (StoreOrder $model) {

                    if ($model->currentAttemp) {
                        return $model->currentAttemp->status;
                    }

                    if ($model->inChangePrinter()) {
                        return 'change_printer';
                    }
                    return null;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'order_status',
                    $searchModel->getStatueseList(),
                    ['class' => 'form-control', 'prompt' => 'All']
                ),
            ],
            [
                'attribute' => 'carrier',
                'value' => function (StoreOrder $model) {
                    $details = $model->getOrderDeliveryDetails();
                    if ($details) {
                        return $details['carrier'];
                    }
                    return null;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'carrier',
                    [DeliveryType::CARRIER_MYSELF => 'Myself', DeliveryType::CARRIER_TS => 'Treatstock'],
                    ['class' => 'form-control', 'prompt' => 'All']
                ),
            ],
            [
                'attribute' => 'is_interception',
                'format' => 'raw',
                'value' => function (StoreOrder $model) {
                    return $model->isInterception()?'yes':'no';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'is_interception',
                    [1 => 'yes', 0 => 'no'],
                    ['class' => 'form-control', 'prompt' => 'All']
                ),
            ],
            [
                'attribute' => 'ps_user',
                'format' => 'raw',
                'value' => function (StoreOrder $model) {
                    return $model->currentAttemp ? PsColumn::getMenu($model->currentAttemp->ps) : null;
                }
            ],
            [
                'attribute' => 'ship_address_id',
                'format' => 'raw',
                'value' => function (StoreOrder $model) {
                    if (!$model->deliveryType) {
                        return '-';
                    }
                    $delivery = $model->deliveryType->title;
                    return $model->shipAddress ? '<small>' . UserAddress::formatAddress($model->shipAddress, false) . '<br>' . $delivery . '</small>' : 'No information';
                }
            ],
            /*[
                'attribute'=>'bill_address_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->billAddress ? common\models\UserAddress::formatAddress($model->billAddress) : 'No information';
                }
            ], */
            [
                'format' => 'raw',
                'attribute' => 'sort_date',
                'filter' => Html::activeDropDownList($searchModel, 'sort_date', $searchModel->getSortDates(), ['class' => 'form-control', 'prompt' => '-']),
                'contentOptions' => ['style' => 'min-width: 200px;'],
                'value' => function (StoreOrder $model) {
                    $dates = [
                        'Billed: ' => $model->billed_at,
                        'Shipped:  ' => isset($model->currentAttemp) && $model->currentAttemp->dates  ? $model->currentAttemp->dates->shipped_at : null,
                        'Delivered: ' => isset($model->currentAttemp)&& $model->currentAttemp->dates ? $model->currentAttemp->dates->delivered_at : null,
                        'Received: ' => isset($model->currentAttemp) && $model->currentAttemp->dates ? $model->currentAttemp->dates->received_at : null,
                        'Updated: ' => $model->updated_at,
                    ];
                    $result = [];
                    foreach ($dates as $k => $v) {
                        $v = app('formatter')->asDatetime($v);
                        $result[] = $k . $v;
                    }
                    $html = implode("<br />", $result);
                    return "<small>" . $html . "</small>";
                }
            ]
        ],
        ]
    ); ?>

</div>