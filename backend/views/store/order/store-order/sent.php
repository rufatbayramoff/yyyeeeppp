<?php
/** @var \common\models\StoreOrder $order */

/** @var \common\modules\storeOrder\forms\ParcelForm $model */

use backend\assets\AngularAppAsset;
use common\modules\storeOrder\helpers\ParcelHelper;
use lib\delivery\parcel\ParcelFactory;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\DetailView;

$this->title = 'Set as sent';
$this->params['breadcrumbs'][] = ['label' => 'Store Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Order # {$order->id}", 'url' => ['view', 'id' => $order->id]];
$this->params['breadcrumbs'][] = $this->title;
AngularAppAsset::register($this);

?>
<div class="preorder-view">
    <?php
    $uploadDelivery = $order->storeOrderDeliveryHistory;
    $attemptDelivery = $order->currentAttemp->delivery;
    $calculatedParcel = ParcelFactory::createFromOrder($order);
    echo DetailView::widget(
        [
            'model'      => $attemptDelivery,
            'attributes' => [
                [
                    'label'  => "Service's calculated",
                    'format' => 'raw',
                    'value'  => $uploadDelivery ? ParcelHelper::format($uploadDelivery->getData()) : ''
                ],
                [
                    'attribute' => 'real_parcel',
                    'format'    => 'raw',
                    'value'     => ParcelHelper::format($attemptDelivery->getData())
                ],
                [
                    'attribute' => 'calculated_parcel',
                    'format'    => 'raw',
                    'value'     => ParcelHelper::formatMeasure($calculatedParcel)
                ],
                [
                    'attribute' => 'Amount shipping',
                    'format'    => 'raw',
                    'value'     => $order->primaryPaymentInvoice->storeOrderAmount->getAmountShipping()
                ],
            ],
        ]
    );
    ?>

    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model, 'weight')->textInput()->label("Weight ({$model->weightGr} gr)") ?>

    <?php echo $form->field($model, 'length')->textInput()->label("Length ({$model->lengthMM} mm)") ?>

    <?php echo $form->field($model, 'width')->textInput()->label("Width ({$model->widthMM} mm)") ?>

    <?php echo $form->field($model, 'height')->textInput()->label("Height ({$model->heightMM} mm)") ?>

    <?php echo $form->field($model, 'measure')->textInput(['readonly' => true]) ?>

    <?php if ($model->rate): ?>
        <?php echo $form->field($model, 'rate')->textInput(['readonly' => true]) ?>
        <?php echo $form->field($model, 'currency')->textInput(['readonly' => true]) ?>

    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Calculate', ['class' => 'btn btn-primary']) ?>
        <?php if ($model->rate): ?>
            <?php echo Html::a('Set as send',
                ['set-as-sent', 'attemptId' => $order->current_attemp_id],
                [
                    'class'       => 'btn btn-primary',
                    'data-method' => 'POST',
                    'data-params' => [
                        'weight'  => $model->weight,
                        'length'  => $model->length,
                        'width'   => $model->width,
                        'height'  => $model->height,
                        'measure' => $model->measure,
                    ],
                ]) ?>
        <?php endif; ?>
    </div>


    <?php ActiveForm::end(); ?>
</div>
