<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrder */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Download report';
$this->params['breadcrumbs'][] = ['label' => 'Store Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$pluginOptions = [
    'placeholder' => 'Select day',
    'class'=> 'form-control',
    'autoclose'=>true,
    'format' => 'yyyy-mm-dd',
    'weekStart' => 1
];
?>

<div class="store-order-download-report">
    <?php
        $form = ActiveForm::begin();
    ?>
    <?=$form->field($model, 'from_date')->widget(DatePicker::class,[
            'pluginOptions' => $pluginOptions
    ])?>
    <?=$form->field($model, 'to_date')->widget(DatePicker::class,[
        'pluginOptions' => $pluginOptions
    ])?>
    <div class="form-group">
        <?= Html::submitButton('Download', ['class' =>  'btn btn-danger']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
