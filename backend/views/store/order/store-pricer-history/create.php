<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StorePricerHistory */

$this->title = 'Create Store Pricer History';
$this->params['breadcrumbs'][] = ['label' => 'Store Pricer Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-pricer-history-create">

    <h1><?= \H($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
