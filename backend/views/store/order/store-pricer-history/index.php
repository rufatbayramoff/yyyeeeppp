<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StorePricerHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Pricer Histories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-pricer-history-index">

    <h1><?= \H($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Store Pricer History', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'pricer_id',
            'created_at',
            'updated_at',
            'old_price',
            // 'new_price',
            // 'user_id',
            // 'comment',
            // 'action_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
