<?php

use backend\components\GridAttributeHelper;
use backend\models\Backend;
use backend\models\search\PreorderSearch;
use common\models\Preorder;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel PreorderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Preorders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row preorder-index">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin(
            [
                'action' => ['index'],
                'method' => 'get',
            ]
        ); ?>

        <div class="col-md-5 col-sm-5 col-lg-4">
            <?= $form->field($searchModel, 'serviceType')->dropDownList(['' => 'All', \common\models\CompanyService::TYPE_CNC => 'Only cnc', \common\models\CompanyService::TYPE_PRINTER => '3D Printing']) ?>
        </div>
        <div class="col-md-12 col-sm-12 col-lg-3">
            <input type="submit" value="Search" class="btn btn-primary searchModel-btn"/>
            <style>
                @media (min-width: 1200px) {
                    .searchModel-btn {
                        margin-top: 25px;
                    }
                }
            </style>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-12">
        <?= GridView::widget([
            'caption'      => sprintf('<div class="row"><div class="col-lg-1">%s</div></div>',
                new \backend\components\GridViewDataExporter($searchModel)),
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                'created_at:datetime',
                [
                    'label'     => 'Status',
                    'attribute' => 'status',
                    'filter'    => Html::activeDropDownList(
                        $searchModel, 'status',
                        $searchModel->getStatusLabelsForAdmin(),
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                    'value'     => function (Preorder $model) {
                        return $model->getLabeledStatusForAdmin();
                    }
                ],
                'user'           => [
                    'label'  => 'User',
                    'attribute' => 'user_id',
                    'format' => 'raw',
                    'value'  => function (Preorder $preorder) {
                        $retval = '';
                        if ($preorder->confirm_hash) {
                            $retval .= "<span style='background-color: #ffabab'>unconfirmed</span> ";
                        }
                        $retval.= GridAttributeHelper::getUserLink($preorder->user);
                        return $retval;
                    }
                ],
                [
                    'label'     => 'Ps',
                    'attribute' => 'ps_id',
                    'format'    => 'raw',
                    'value'     => function (Preorder $model) {
                        return GridAttributeHelper::getPsLink($model->ps);
                    }
                ],
                'name',
                'description',
                'budget',
                'estimate_time',

                [
                    'label'     => 'Offered',
                    'attribute' => 'offered',
                    'format'    => 'raw',
                    'value'     => function (Preorder $model) {
                        return $model->offered ? "Yes" : 'No';
                    },
                    'filter'    => [
                        0 => 'No',
                        1 => 'Yes'
                    ],

                ],

                [
                    'label'  => 'Cost',
                    'format' => 'raw',
                    'value'  => function (Preorder $model) {
                        return $model->offered ? displayAsMoney($model->primaryPaymentInvoice->getAmountTotal() ) : null;
                    }
                ],

                [
                    'label'  => 'Order',
                    'format' => 'raw',
                    'value'  => function (Preorder $model) {
                        return $model->storeOrders
                            ? GridAttributeHelper::getOrderLink(\common\components\ArrayHelper::first($model->storeOrders))
                            : null;
                    }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
