<?php

use backend\models\Backend;
use backend\widgets\FilesListWidget;
use common\models\Preorder;
use common\models\PreorderHistory;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $preorder common\models\Preorder */
/* @var $preorderWorkDataProvider \yii\data\ActiveDataProvider */

$totalCost = $preorder->primaryPaymentInvoice ? $preorder->primaryPaymentInvoice->getAmountTotalWithoutPaymentMethodFee() : '';
$psWillGet = $preorder->primaryPaymentInvoice ? $preorder->primaryPaymentInvoice->preorderAmount->getAmountManufacturer() : '';
?>

<?= DetailView::widget([
    'model'      => $preorder,
    'attributes' => [
        'id',
        'status',
        'status_text'     => [
            'label' => 'Status text',
            'value' => $preorder->getLabeledStatusForAdmin()
        ],
        'created_at',
        'created_by',
        'ps'              => [
            'label'  => 'Ps',
            'format' => 'raw',
            'value'  => \backend\components\columns\PsColumn::getMenu($preorder->ps),
        ],
        'service'         => [
            'label'  => 'Service',
            'format' => 'raw',
            'value'  => $preorder->service ? '<a href="/company/company-service/view?id=' . $preorder->service_id . '">' . H($preorder->service->getTitleLabel()) . '</a>' : ''
        ],
        'product'         => [
            'label'  => 'Product',
            'format' => 'raw',
            'value'  => $preorder->productSnapshot ? '<a href="/product/product/update?uuid=' . $preorder->productSnapshot->product_uuid . '">' . H($preorder->productSnapshot->product->title) . '</a>' : ''
        ],
        'user'            => [
            'label'  => 'User',
            'format' => 'raw',
            'value'  => function (Preorder $preorder) {
                $retval = '';
                if ($preorder->confirm_hash) {
                    $retval .= "<span style='background-color: #ffabab'>unconfirmed</span> ";
                }
                $retval .= Backend::displayUser($preorder->user);
                return $retval;
            }
        ],
        'name',
        'description:ntext',
        'message:ntext',
        'budget',
        'is_interception' => [
            'label'  => 'Is interception',
            'format' => 'raw',
            'value'  => function (Preorder $preorder) {
                return $preorder->is_interception ? "<span style='background-color: #ffabab'>yes</span>" : 'no';
            }
        ],
        'estimate_time',
        'email:email',
        'decline_reason',
        'decline_comment',
        'decline_initiator',
        'offer_description',
        'offer_accepted_at',
        'offer_estimate_time',
        'admin_comment',
        'confirm_hash',
        'offer_deadline'  => [
            'label'  => 'Deadline',
            'format' => 'raw',
            'value'  => '<span class="alert-warning">' . $preorder->getDeadline() . '</span>'
        ],
        'offered',
        'files'           => [
            'label'  => 'Files',
            'format' => 'raw',
            'value'  => FilesListWidget::widget(
                [
                    'filesList' => $preorder->files,
                ]
            )
        ],
        'total_cost'      => [
            'label'  => 'Total cost',
            'format' => 'raw',
            'value'  => $totalCost
        ],
        'ps_will_get'     => [
            'label'  => 'Ps will get',
            'format' => 'raw',
            'value'  => $psWillGet
        ],
        'ship_to_id'      => [
            'label'  => 'Ship address',
            'format' => 'raw',
            'value'  => $preorder->shipTo ? \common\models\UserAddress::formatAddress($preorder->shipTo) : null
        ],
        'order'           => [
            'label'  => 'Order',
            'format' => 'raw',
            'value'  => $preorder->storeOrder ? '<a href="/store/store-order/view?id=' . $preorder->storeOrder->id . '">' . $preorder->storeOrder->id . '</a>' : ''
        ]
    ],
]) ?>

<h3>Works</h3>
<?=
GridView::widget(
    [
        'tableOptions' => ['class' => 'table no-margin'],
        'options'      => ['class' => ''],
        'dataProvider' => $preorderWorkDataProvider,
        'columns'      => [
            'id',
            'title',
            [
                'label'  => 'Product',
                'format' => 'raw',
                'value'  => function (\common\models\PreorderWork $preorderWork) {
                    return $preorderWork->product ? '<a href="/product/product/update?uuid=' . $preorderWork->product_uuid . '">' . H($preorderWork->product->title) . '</a>' : '';
                }
            ],
            [
                'label'  => 'Service',
                'format' => 'raw',
                'value'  => function (\common\models\PreorderWork $preorderWork) {
                    return $preorderWork->companyService ? '<a href="/company/company-service/view?id=' . $preorderWork->company_service_id . '">' . H($preorderWork->companyService->getTitleLabel()) . '</a>' : '';
                }
            ],
            'qty',
            'cost'
        ]
    ]
); ?>

<?php
if ($preorder->preorderCncWorks) {
    ?>
    <h3>CNC items</h3>
    <?=
    GridView::widget(
        [
            'tableOptions' => ['class' => 'table no-margin'],
            'options'      => ['class' => ''],
            'dataProvider' => new ActiveDataProvider([
                'query' => $preorder->getPreorderCncWorks(),
            ]),
            'columns'      => [
                'preorder_work_id',
                'title',
                'qty',
                'price',
            ]
        ]
    ); ?>
    <?php
}
?>

<h3>History</h3>
<?php
echo GridView::widget(
    [
        'dataProvider' => PreorderHistory::getDataProvider(['model_id' => $preorder->id], 1000),
        'columns'      => [
            'id',
            'created_at',
            [
                'header'    => 'User',
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => function (PreorderHistory $history) {
                    return \backend\models\Backend::displayUser($history->user_id);
                }
            ],
            'action_id',
            'diffRemove' => [
                'header' => 'Remove',
                'format' => 'raw',
                'value'  => function (PreorderHistory $history) {
                    return '<pre class="jsonHighlight small-text">' . json_encode($history->getSourceResultDiffRemove(), JSON_PRETTY_PRINT) . '</pre>';
                },
            ],
            'diffAdd'    => [
                'header' => 'Add',
                'format' => 'raw',
                'value'  => function (PreorderHistory $history) {
                    return '<pre class="jsonHighlight small-text">' . json_encode($history->getSourceResultDiffAdd(), JSON_PRETTY_PRINT) . '</pre>';
                },
            ],
            'comment'
        ],
    ]
);

?>
