<?php

use backend\models\Backend;
use backend\widgets\FilesListWidget;
use common\models\PreorderHistory;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $preorder common\models\Preorder */
/* @var $preorderWorkDataProvider \yii\data\ActiveDataProvider */

$this->title                   = 'Preorder  #'.$preorder->id;
$this->params['breadcrumbs'][] = ['label' => 'Preorders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

        <?= Html::a('Update', ['update', 'id' => $preorder->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $preorder->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>

        <?php if ($preorder->status == \common\models\Preorder::STATUS_NEW) : ?>

            <?= Html::a('Decline', ['decline', 'id' => $preorder->id], [
                'class' => 'btn btn-warning',
                'data'  => [
                    'confirm' => 'Are you sure you want to decline this item?',
                    'method'  => 'post',
                ],
            ]) ?>

        <?php endif; ?>
    </p>

    <?= $this->render('viewModel', ['preorder' => $preorder, 'preorderWorkDataProvider' => $preorderWorkDataProvider]) ?>
    <?= $this->render('viewMessages', ['preorder' => $preorder]) ?>

    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Invoices</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <?= $this->render('@backend/views/store/payment-invoice/paymentInvoices.php', [
                'primaryInvoice' => $preorder->primaryPaymentInvoice,
                'invoices'       => $preorder->paymentInvoices
            ]) ?>
        </div>
    </div>

</div>
