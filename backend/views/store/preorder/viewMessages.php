<?php

use backend\models\Backend;
use common\models\MsgMessage;
use common\models\MsgTopic;
use common\models\Preorder;
use yii\grid\GridView;

/** @var Preorder preorder */
$preorderTopics = MsgTopic::find()->where(['bind_id' => $preorder->id, 'bind_to' => 'preorder'])->all();

if ($preorderTopics) {
    $dataProvider = MsgMessage::getDataProvider(
        ['topic_id' => \yii\helpers\ArrayHelper::getColumn($preorderTopics, 'id')],
        150
    );
    ?>
    <div class="box box-solid box-default">
        <div class="box-header">Preorder messages
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <?php
            echo GridView::widget(
                [
                    'sorter'       => false,
                    'showFooter'   => false,
                    'layout'       => '{items}',
                    'dataProvider' => $dataProvider,
                    'columns'      => [
                        [
                            'attribute' => 'user.username',
                            'format'    => 'raw',
                            'value'     => function ($model) {
                                $username = Backend::displayUser($model->user);
                                return $username;
                            }
                        ],
                        'created_at:datetime',
                    ],
                ]
            ); ?>
        </div>
    </div>
<?php } ?>
