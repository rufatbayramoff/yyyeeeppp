<?php

use backend\components\columns\PsColumn;
use backend\components\columns\UserColumn;
use backend\components\GridAttributeHelper;
use backend\models\search\InstantPaymentSearch;
use common\models\InstantPayment;
use common\models\Preorder;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel InstantPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Instant payment';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'uuid',
            'created_at:datetime',
            [
                'label'     => 'From',
                'attribute' => 'from_user_id',
                'relation'  => 'fromUser',
                'class'     => UserColumn::class,
            ],
            [
                'label'     => 'To',
                'attribute' => 'to_user_id',
                'format'    => 'raw',
                'value'     => function (InstantPayment $model) {
                    return PsColumn::getMenu($model->toUser->company);
                }
            ],
            'descr',
            'sum',
            'payed_at',
            'finished_at',
            [
                'attribute' => 'filterStatus',
                'filter'    => Html::activeDropDownList($searchModel, 'filterStatus', $searchModel->getFilterStatusList(), ['class' => 'form-control']),
                'value' => function (InstantPayment $model) {
                    return $model->status;
                },
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>


</div>
