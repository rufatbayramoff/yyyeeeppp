<?php

use backend\components\columns\PsColumn;
use backend\components\columns\UserColumn;
use backend\models\Backend;
use common\models\PreorderHistory;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $instantPayment \common\models\InstantPayment */

$this->title                   = 'Instant payment: ' . $instantPayment->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Instant payment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if ($instantPayment->allowCancel()) {
            ?>
            <?= Html::a('Refund', ['refund', 'id' => $instantPayment->uuid], ['class' => 'btn btn-primary']) ?>
            <?php
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model'      => $instantPayment,
        'attributes' => [
            'uuid',
            'created_at:datetime',
            [
                'label'     => 'From',
                'attribute' => 'from_user_id',
                'format'    => 'raw',
                'value'     => Backend::displayUser($instantPayment->fromUser)
            ],
            [
                'label'     => 'To',
                'attribute' => 'from_user_id',
                'format'    => 'raw',
                'value'     => PsColumn::getMenu($instantPayment->toUser->company)
            ],
            'descr',
            'sum',
            'payed_at',
            'finished_at',
            'status',
        ],
    ]) ?>
    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Invoices</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <?= $this->render('@backend/views/store/payment-invoice/paymentInvoices.php', [
                'primaryInvoice' => $instantPayment->primaryInvoice,
                'invoices'       => $instantPayment->paymentInvoices
            ]) ?>
        </div>
    </div>
</div>
