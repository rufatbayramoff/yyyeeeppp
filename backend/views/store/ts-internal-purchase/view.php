<?php

use backend\components\columns\PsColumn;
use backend\components\columns\UserColumn;
use backend\models\Backend;
use common\models\TsInternalPurchase;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $tsInternalPurchase TsInternalPurchase */

$this->title                   = 'Ts internal purchase: ' . $tsInternalPurchase->uid;
$this->params['breadcrumbs'][] = ['label' => 'Ts internal purchase', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-view">
    <?= DetailView::widget(['model'      => $tsInternalPurchase,
                            'attributes' => [
                                'uid',
                                'type',
                                [
                                    'label'  => 'User',
                                    'format' => 'raw',
                                    'value'  => UserColumn::getMenu($tsInternalPurchase->user)
                                ],
                                'companyService' => [
                                    'attribute' => 'companyServiceTitle',
                                    'label'     => 'Company service',
                                    'format'    => 'raw',
                                    'value'     => function (TsInternalPurchase $tsInternalPurchase) {
                                        $companyService = $tsInternalPurchase->getCompanyService();
                                        if (!$companyService) {
                                            return '';
                                        }
                                        $title = $tsInternalPurchase->getCompanyService()->ps->title . ' - ' . $tsInternalPurchase->getCompanyService()->getTitleLabel();
                                        $url   = '<a href="/company/company-service/view?id=' . $tsInternalPurchase->getCompanyService()->id . '">' . H($title) . '</a>';
                                        return $url;
                                    }],
                                [
                                    'label'  => 'Certification class',
                                    'format' => 'raw',
                                    'value'  => function (TsInternalPurchase $tsInternalPurchase) {
                                        if (!$tsInternalPurchase->tsInternalPurchaseCertification) {
                                            return '';
                                        }
                                        $url = '<a href="/tsCertification/certification-class/update?id=' . $tsInternalPurchase->tsInternalPurchaseCertification->tsCertificationClass->id . '">' . H($tsInternalPurchase->tsInternalPurchaseCertification->tsCertificationClass->title) . '</a>';
                                        return $url;
                                    }
                                ],
                                [
                                    'label'  => 'Printer certification',
                                    'format' => 'raw',
                                    'value'  => function (TsInternalPurchase $tsInternalPurchase) {
                                        $url = '';
                                        foreach ($tsInternalPurchase->psPrinterTests as $psPrinterTest) {
                                            $url .= '<a href="/ps/ps-printer-test/index?printer=' . $psPrinterTest->ps_printer_id . '">' . H($psPrinterTest->psPrinter->title) . '</>';
                                        }
                                        return $url;
                                    }
                                ],
                                'created_at',
                                'price',
                                'status'
                            ],
    ]) ?>
    <?= $this->renderFile(Yii::getAlias('@backend/views/store/payment-invoice/paymentInvoice.php'), ['primaryInvoice' => $tsInternalPurchase->primaryInvoice, 'paymentInvoice' => $tsInternalPurchase->primaryInvoice]); ?>
</div>
