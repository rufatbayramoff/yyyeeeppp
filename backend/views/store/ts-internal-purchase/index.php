<?php

use backend\components\columns\PsColumn;
use backend\components\columns\UserColumn;
use backend\components\GridAttributeHelper;
use backend\models\search\InstantPaymentSearch;
use common\models\InstantPayment;
use common\models\Preorder;
use common\models\TsInternalPurchase;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel \common\models\TsInternalPurchase */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Ts internal purchase';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preorder-index">

    <p>
        <?php echo Html::a('Create Deposit', ['create-deposit'], ['class' => 'btn btn-success']);?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'uid',
            'created_at:datetime',
            'type',
            'companyService' => [
                'attribute' => 'companyServiceTitle',
                'label'     => 'Company service',
                'format'    => 'raw',
                'value'     => function (TsInternalPurchase $tsInternalPurchase) {
                    $companyService = $tsInternalPurchase->getCompanyService();
                    if (!$companyService) {
                        return '';
                    }
                    $title = $tsInternalPurchase->getCompanyService()->ps->title . ' - ' . $tsInternalPurchase->getCompanyService()->getTitleLabel();
                    $url   = '<a href="/company/company-service/view?id=' . $tsInternalPurchase->getCompanyService()->id . '">' . H($title) . '</a>';
                    return $url;
                }],
            'user' => [
                'label'     => 'User',
                'format'    => 'raw',
                'value'     => function(TsInternalPurchase $tsInternalPurchase){
                    return Html::a(
                        $tsInternalPurchase->user->username,
                        ['user/user/view','id' => $tsInternalPurchase->user->id]
                    );
                }
            ],
            'price',
            'status',
            [
                'class'          => 'yii\grid\ActionColumn',
                'template'       => '{view}',
                'buttons' => [
                    'view' => function ($url,TsInternalPurchase $model, $key) {
                        $isCertification = $model->tsInternalPurchaseCertification->tsCertificationClass->id ?? null;
                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-eye-open"]);
                        if($isCertification) {
                            return Html::a($icon, $url);
                        }
                        if($model->isDeposit()) {
                            return Html::a($icon, ['deposit','id' => $model->uid]);
                        }
                    }
                ]
            ],
        ],
    ]); ?>


</div>
