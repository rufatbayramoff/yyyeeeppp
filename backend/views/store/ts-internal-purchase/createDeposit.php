<?php

use frontend\modules\tsInternalPurchase\forms\DepositForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/** @var $depositForm DepositForm */
?>
<h4>Create Deposit</h4>
<div class="cutting-pack-file-form">

    <?php
    $form = ActiveForm::begin(); ?>

    <div class="form-group field-depositform-currency has-success">
        <label class="control-label">User</label>
        <?php echo Select2::widget([
            'name'          => 'DepositForm[userId]',
            'value'         => null,
            'initValueText' => null,
            'pluginOptions' => [
                'allowClear'         => true,
                'minimumInputLength' => 2,
                'ajax'               => [
                    'url'      => \yii\helpers\Url::to(['/user/user/list']),
                    'dataType' => 'json',
                    'data'     => new yii\web\JsExpression('function(params) { return {q:params.term, page:params.page, k:"id,username"}; }')
                ],
            ],
        ]); ?>
    </div>



    <?= $form->field($depositForm, 'balance')->textInput(['maxlength' => true]) ?>
    <?= $form->field($depositForm, 'currency')->dropDownList(\lib\money\Currency::ALLOWED_CURRENCIES, ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

