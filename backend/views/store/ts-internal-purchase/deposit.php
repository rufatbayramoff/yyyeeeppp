<?php
/* @var $this yii\web\View */

/* @var $tsInternalPurchase TsInternalPurchase */

use common\models\TsInternalPurchase;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Ts internal purchase: ' . $tsInternalPurchase->uid;
$this->params['breadcrumbs'][] = ['label' => 'Ts internal purchase', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="preorder-view">
    <?php echo DetailView::widget([
        'model'      => $tsInternalPurchase,
        'attributes' => [
            'uid',
            'type',
            'user' => [
                'label'     => 'User',
                'format'    => 'raw',
                'value'     => function(TsInternalPurchase $tsInternalPurchase){
                    return Html::a(
                        $tsInternalPurchase->user->username,
                        ['user/user/view','id' => $tsInternalPurchase->user->id]
                    );
                }
            ],
            'bank' => [
                'label'     => 'Bank Invoice',
                'format'    => 'raw',
                'value'     => function(TsInternalPurchase $tsInternalPurchase){
                    if($tsInternalPurchase->primaryInvoice && $tsInternalPurchase->primaryInvoice->paymentBankInvoice) {
                        $uuid = $tsInternalPurchase->primaryInvoice->paymentBankInvoice->uuid;
                        return Html::a(
                            $uuid,
                            ['store/payment-bank-invoice/view','id' => $uuid]
                        );
                    }
                }
            ],
            'created_at',
            'price',
            'status'
        ],
    ]) ?>
    <?php if($tsInternalPurchase->primaryInvoice):?>
        <?= $this->renderFile(Yii::getAlias('@backend/views/store/payment-invoice/paymentInvoice.php'),
            ['primaryInvoice' => $tsInternalPurchase->primaryInvoice, 'paymentInvoice' => $tsInternalPurchase->primaryInvoice]); ?>
    <?php endif;?>
</div>
