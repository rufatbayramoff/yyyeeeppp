<?php

use backend\components\columns\UserColumn;
use common\models\StoreOrderReviewShare;
use frontend\components\image\ImageHtmlHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StoreOrderReviewShareSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Order Review Shares';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-order-review-share-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'caption' => sprintf('<div class="row"><div class="col-lg-1">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel)),
        'columns' => [

            'id',
            [
                'label'     => 'User',
                'class'     => UserColumn::class,
                'attribute' => 'user',
            ],
            [
                'attribute' => 'social_type',
                'filter' => StoreOrderReviewShare::SOCIAL_TYPES_LABELS,
                'format' => 'raw',
                'value' => function($share) {
                    $type = $share->social_type;
                    $link = Html::a('View', param('server') . '/reviews/' . $share->review_id . '/share/'. $share->id, ['target'=>'_blank']);
                    return $type . "<br />" . $link;
                }
            ],
             'text:ntext',
             'created_at',
             [
                 'format'    => 'raw',
                 'attribute' => 'status',
                 'filter' => StoreOrderReviewShare::STATUS_LABELS,
                 'value' => function (StoreOrderReviewShare $share) {

                    $html = $share->status.'</br>';

                    if ($share->status == StoreOrderReviewShare::STATUS_NEW) {
                        $html.= Html::a('Approve', ['approve', 'id' => $share->id], ['class' => 'btn btn-success '])
                            . ' <br /><br /> ' . Html::a('Reject', ['reject', 'id' => $share->id], ['class' => 'btn btn-danger btn-xs']);
                    }

                    return $html;
                 }
             ],
            [
                'format'    => 'raw',
                'label'     => 'Images',
                'filter'    => false,
                'value'     => function (StoreOrderReviewShare $share) {
                    $html = '';
                    foreach ($share->getAllPhotos() as $file) {
                        $previewUrl = ImageHtmlHelper::getThumbUrlForFile($file, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL);
                        $html .= Html::a(Html::img($previewUrl . '?time='.md5($file->updated_at), ['style' => 'max-width: 50px; padding : 5px;']), $file->getFileUrl(), ['target' => '_blank']) . ' ';
                    }
                    return $html;
                }
            ],
        ],
    ]); ?>
</div>
