<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.11.18
 * Time: 13:26
 */

namespace backend\modules\payment\assets;

use yii\web\AssetBundle;

class RefundPopupAsset extends AssetBundle
{
    public $baseUrl = '@web';
    public $js = [
        'js/refundPopup.js'
    ];
}