<?php

namespace backend\modules\payment;

/**
 * Payment module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\payment\controllers';

    /**
     *
     */
    public $defaultRoute = 'balance-report';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
