<?php
namespace backend\modules\payment\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\AccountBalanceSearch;
use backend\modules\payment\models\AccountBalanceForm;
use backend\modules\payment\models\BalanceReportViewModel;
use common\modules\payment\models\AccountBalance;
use common\modules\payment\services\BonusService;


class BonusController extends AdminController
{
    /**
     * @var BonusService
     */
    protected $bonusService;

    public function injectDependencies(BonusService $bonusService)
    {
        $this->bonusService        = $bonusService;
    }


    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('payment.view');
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $searchModel = new AccountBalanceSearch();
        $searchModel->load(\Yii::$app->request->get());
        $dataProvider = $this->bonusService->getBonusUsersProvider($searchModel);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $accountBalanceForm = new AccountBalanceForm();
        if (\Yii::$app->request->isPost && $accountBalanceForm->load(\Yii::$app->request->post())) {
            if ($accountBalanceForm->validate()) {
                $accountBalance = \Yii::createObject(AccountBalance::class);
                $accountBalance->loadByForm($accountBalanceForm);
                $this->bonusService->addBalance($accountBalance, $accountBalanceForm->comment);
                $this->setFlashMsg(true, _t('site.common', 'Created'));
                return $this->redirect('/payment/bonus/index');
            }
        }
        return $this->render('create', [
            'model' =>$accountBalanceForm
        ]);
    }
}