<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.10.18
 * Time: 12:36
 */

namespace backend\modules\payment\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\PaymentLogSearch;
use backend\models\search\PaymentWarningSearch;
use backend\modules\payment\models\AlarmPaymentsForm;
use common\components\DateHelper;
use common\components\exceptions\OnlyPostRequestException;
use common\models\base\PaymentWarning;
use common\models\PaymentLog;
use common\modules\payment\components\PaymentLogger;
use common\modules\payment\services\PaymentWarningService;
use frontend\models\user\UserFacade;
use Psr\Log\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

class WarningController extends AdminController
{
    /**
     * @var PaymentWarningService
     */
    public $paymentWarningService;

    /**
     * @param PaymentWarningService $paymentWarningService
     */
    public function injectDependencies(
        PaymentWarningService $paymentWarningService
    ) {
        $this->paymentWarningService = $paymentWarningService;
    }


    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('payment.warnings');
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $alarmPaymentsForm = new AlarmPaymentsForm();
        $alarmPaymentsForm->init();
        if (\Yii::$app->request->isPost) {
            $alarmPaymentsForm->load(\Yii::$app->request->post());
        }

        $alarms = [];
        $alarms += $this->paymentWarningService->formAlarmsInvoices($alarmPaymentsForm->dateFrom, $alarmPaymentsForm->dateEnd);
        $alarms += $this->paymentWarningService->formAlarmsDetailsInvoiceDate($alarmPaymentsForm->dateFrom, $alarmPaymentsForm->dateEnd);
        $alarms += $this->paymentWarningService->formAlarmsIncomeWithoutInvoice($alarmPaymentsForm->dateFrom, $alarmPaymentsForm->dateEnd);
        $alarms += $this->paymentWarningService->formAlarmsReservedOutWithoutIn($alarmPaymentsForm->dateFrom, $alarmPaymentsForm->dateEnd);
        $alarms += $this->paymentWarningService->formAlarmsNotZeroTechnicalAccounts($alarmPaymentsForm->dateFrom, $alarmPaymentsForm->dateEnd);
        $alarms += $this->paymentWarningService->formAlarmsDifferentCurrency($alarmPaymentsForm->dateFrom, $alarmPaymentsForm->dateEnd);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $alarms,
        ]);

        return $this->render('index', [
            'alarmPaymentsForm' => $alarmPaymentsForm,
            'dataProvider'      => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionCheckedList()
    {
        $searchModel = new PaymentWarningSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('checkedList', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     * @throws \yii\base\UserException
     */
    public function actionChecked()
    {
        if (!\Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $paymentDetailId = \Yii::$app->request->get('paymentDetailId');
        $uuid = \Yii::$app->request->get('uuid');
        $date = \Yii::$app->request->get('date');
        $description = \Yii::$app->request->get('description');

        $paymentWarning = PaymentWarning::findOne(['uuid' => $uuid]);
        if ($paymentWarning) {
            \Yii::$app->session->setFlash('success', 'Already marked checked.');
            return $this->redirect('/payment/warning/index');
        }

        $paymentWarning = new PaymentWarning();
        $paymentWarning->created_at = DateHelper::now();
        $paymentWarning->user_id = UserFacade::getCurrentUserId();
        $paymentWarning->uuid = $uuid;
        $paymentWarning->payment_detail_id = $paymentDetailId;
        $paymentWarning->date = $date;
        $paymentWarning->description = strip_tags($description);


        $paymentWarning->safeSave();
        \Yii::$app->session->setFlash('success', 'Warning marked is checked.');
        $this->redirect('/payment/warning/index');
    }
}
