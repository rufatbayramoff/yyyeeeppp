<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.09.18
 * Time: 16:03
 */


namespace backend\modules\payment\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\modules\payment\exports\DetailsBalanceReport;
use backend\modules\payment\models\BalanceDetailsViewModel;
use backend\modules\payment\models\BalanceReportViewModel;
use backend\modules\statistic\reports\ReportExcelWriter;
use common\components\exceptions\BusinessException;
use common\models\PaymentAccount;
use common\models\User;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use lib\money\Currency;
use lib\money\Money;
use Yii;
use yii\base\UserException;
use yii\helpers\ArrayHelper;

class BalanceReportController extends AdminController
{
    /**
     * @var PaymentService
     */
    protected $paymentService;

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /**
     * @param PaymentService $paymentService
     * @param PaymentAccountService $paymentAccountService
     */
    public function injectDependencies(PaymentService $paymentService, PaymentAccountService $paymentAccountService)
    {
        $this->paymentService = $paymentService;
        $this->paymentAccountService = $paymentAccountService;
    }


    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('payment.reports');
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $balanceReportViewModel = new BalanceReportViewModel();
        $balanceReportViewModel->dateFrom = Yii::$app->request->get('dateFrom', date('Y-m-d', strtotime('-1 months')));
        $balanceReportViewModel->dateTo = Yii::$app->request->get('dateTo', '');

        $balanceReportViewModel->systemBalances = $this->paymentAccountService->calculateAccountsBalanceEx(
            PaymentAccount::find()->system(),
            $balanceReportViewModel->dateFrom,
            $balanceReportViewModel->dateTo
        );
        $balanceReportViewModel->totalUsersBalance = $this->paymentService->calculateTotalUsersBalanceEx(
            $balanceReportViewModel->dateFrom,
            $balanceReportViewModel->dateTo
        );
        $totalSystem = 0;
        foreach ($balanceReportViewModel->systemBalances as $systemBalance) {
            $totalSystem += $systemBalance->balanceCurrent->getAmount();
        }
        $totalSystem += $balanceReportViewModel->totalUsersBalance->getCurrentTotalAmount();
        $balanceReportViewModel->totalCheck = Money::create($totalSystem, Currency::USD);

        return $this->render('index', ['balanceReportViewModel' => $balanceReportViewModel]);
    }


    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\RangeNotSatisfiableHttpException
     * @throws \PHPExcel_Exception
     * @throws UserException
     */
    public function actionDetails()
    {
        $viewModel = new BalanceDetailsViewModel();
        $viewModel->load(Yii::$app->request->get());
        $viewModel->paymentDetailOperations = $this->paymentService->getUserPaymentDetailOperations(
            $viewModel->user,
            $viewModel->accountType,
            $viewModel->dateFrom,
            $viewModel->dateTo,
            $viewModel->sign
        );

        $viewModel->paymentAccountTypes = $viewModel->user
            ? $this->paymentService->getUsedPaymentTypesByUser($viewModel->user)
            : $this->paymentService->getUsedPaymentTypesAll();

        $usersList = User::find()
            ->select('user.id, username, email')
            ->leftJoin('payment_account', 'payment_account.user_id=user.id')->where('payment_account.id is not null')
            ->groupBy('user.id')
            ->asArray()
            ->all();

        $viewModel->usersSelectList = ArrayHelper::map($usersList, 'username', 'username');

        if ($viewModel->user) {
            $viewModel->balanceEx = $this->paymentService->calculateTotalUserBalanceEx(
                $viewModel->user,
                $viewModel->dateFrom,
                $viewModel->dateTo
            );
        } else {
            $viewModel->balanceEx = $this->paymentService->calculateTotalUsersBalanceEx(
                $viewModel->dateFrom,
                $viewModel->dateTo
            );
        }

        if (Yii::$app->request->get('export')) {
            if (!$viewModel->user) {
                throw new BusinessException('Please select User');
            }
            $report = DetailsBalanceReport::create();
            $report->setParams(['viewModel' => $viewModel]);
            $reportWriter = new ReportExcelWriter('DetailsBalance');
            $filename = 'balance-' .
                $viewModel->user->username . '-' .
                ($viewModel->dateFrom ? $viewModel->dateFrom : '2000-01-01') .
                ($viewModel->dateTo ? '-' . $viewModel->dateTo : '') .
                '.xlsx';
            return Yii::$app->response->sendContentAsFile($reportWriter->write($report), $filename, ['inline' => true]);
        }

        return $this->render('operationDetails', [
            'balanceDetailsViewModel' => $viewModel
        ]);
    }
}
