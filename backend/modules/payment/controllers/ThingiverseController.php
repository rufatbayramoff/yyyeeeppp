<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.10.18
 * Time: 12:36
 */

namespace backend\modules\payment\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\ThingiverseReportSearch;
use Yii;
use yii\base\Action;
use yii\base\Exception;

class ThingiverseController extends AdminController
{
    /**
     * @param Action $action
     * @return bool
     * @throws Exception
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('payment.view');
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     *
     * @return string
     */
    public function actionThingiverseReport()
    {
        /** @var ThingiverseReportSearch $searchModel */
        $searchModel = new ThingiverseReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('thingiverseReport', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
