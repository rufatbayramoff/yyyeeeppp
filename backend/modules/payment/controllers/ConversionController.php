<?php
namespace backend\modules\payment\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\AccountBalanceSearch;
use backend\modules\payment\models\AccountBalanceForm;
use backend\modules\payment\models\BalanceReportViewModel;
use common\modules\payment\models\AccountBalance;
use common\modules\payment\services\BonusService;
use common\modules\payment\services\CurrencyConversionService;
use yii\base\BaseObject;


class ConversionController extends AdminController
{
    /**
     * @var CurrencyConversionService
     */
    protected $currencyConversionService;

    public function injectDependencies(CurrencyConversionService $currencyConversionService)
    {
        $this->currencyConversionService        = $currencyConversionService;
    }


    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('payment.view');
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $dataProvider = $this->currencyConversionService->getConversionBalancesProvider();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}