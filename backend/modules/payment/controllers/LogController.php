<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.10.18
 * Time: 12:36
 */

namespace backend\modules\payment\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\PaymentLogSearch;
use common\models\PaymentLog;
use common\modules\payment\components\PaymentLogger;
use yii\web\NotFoundHttpException;

class LogController extends AdminController
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('payment.log');
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        /** @var PaymentLogSearch $searchModel */
        $searchModel = new PaymentLogSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaymentLog model.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = PaymentLog::findByPk($id);
        return $this->render('view', ['paymentLog' => $model]);
    }
}
