<?php

namespace backend\modules\payment\exports;

use backend\modules\payment\models\BalanceDetailsViewModel;
use backend\modules\statistic\reports\BaseReportInterface;
use DateTime;

class DetailsBalanceReport implements BaseReportInterface
{
    /**
     * @var BalanceDetailsViewModel
     */
    public $viewModel;

    public static function create()
    {
        return new self;
    }

    public function getCreatedTime()
    {
        return new DateTime();
    }

    public function getColumnsNames()
    {
        return [
            'operationUuid'      => 'Operation uuid',
            'date'               => 'Date',
            'paymentId'          => 'Payment id',
            'invoiceUuid'        => 'Invoice uuid',
            'orderId'            => 'Order Id',
            'detailId'           => 'Detail Id',
            'detailType'         => 'Detail Type',
            'accountType'        => 'Account Type',
            'plus'               => 'Plus',
            'minus'              => 'Minus',
            'to'                 => 'To account',
            'transactions'       => 'Transactions',
            'thingiverseOrderId' => 'Thingiverse order id'
        ];
    }

    /**
     * @return array
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function getItems()
    {
        $rows = [];
        foreach ($this->viewModel->paymentDetailOperations as $paymentDetailOperation) {
            $storeOrder = $paymentDetailOperation->payment->paymentInvoice->storeOrder ?? null;
            $row = [
                'operationUuid'      => $paymentDetailOperation->uuid,
                'date'               => $paymentDetailOperation->created_at,
                'paymentId'          => $paymentDetailOperation->payment->id,
                'invoiceUuid'        => $paymentDetailOperation->payment->paymentInvoice->uuid,
                'orderId'            => $storeOrder->id ?? null,
                'detailId'           => '',
                'detailType'         => '',
                'accountType'        => '',
                'plus'               => '',
                'minus'              => '',
                'to'                 => '',
                'transactions'       => [],
                'thingiverseOrderId' => '',
            ];
            if ($storeOrder && $storeOrder->thingiverseOrder) {
                $row['thingiverseOrderId'] = $storeOrder->thingiverseOrder->thingiverse_order_id;
            };


            foreach ($paymentDetailOperation->paymentDetails as $paymentDetail) {
                if ($paymentDetail->paymentAccount->user_id !== $this->viewModel->user->id) {
                    if ($row['to']) {
                        $row['to'] .= ',';
                    }
                    $row['to'] .= $paymentDetail->paymentAccount->user->username . ' (' . $paymentDetail->paymentAccount->type . ')';
                }
                if ($vendorTransactionHistory = $paymentDetail->getVendorTransactionHistory()) {
                    $row['transactions'][$vendorTransactionHistory->transaction->id] = $vendorTransactionHistory->transaction->id;
                }
            }
            foreach ($paymentDetailOperation->paymentDetails as $paymentDetail) {
                if ($paymentDetail->paymentAccount->user_id == $this->viewModel->user->id) {
                    $row['detailId'] = $paymentDetail->id;
                    $row['detailType'] = $paymentDetail->type;
                    $row['accountType'] = $paymentDetail->paymentAccount->type;
                    if ($paymentDetail->amount > 0) {
                        $row['plus'] = $paymentDetail->amount;
                    } else {
                        $row['minus'] = $paymentDetail->amount;
                    }
                    if ($vendorTransactionHistory = $paymentDetail->getVendorTransactionHistory()) {
                        $row['transactions'][$vendorTransactionHistory->transaction->id] = $vendorTransactionHistory->transaction->id;
                    }
                }
            };
            $row['transactions'] = implode(',', $row['transactions']);
            $rowObject = (object)$row;
            $rows[] = $rowObject;
        }
        return $rows;
    }

    public function setParams(array $params)
    {
        $this->viewModel = $params['viewModel'];
    }
}