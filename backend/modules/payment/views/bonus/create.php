<?php

use backend\modules\payment\models\AccountBalanceForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model AccountBalanceForm */
/* @var $accountBalance \common\modules\payment\models\AccountBalance */

$this->title                   = 'Add bonus balance';
$this->params['breadcrumbs'][] = ['label' => 'Payment Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-log-create">

    <div class="payment-log-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group">
            <label>User ID</label>
            <?php echo Select2::widget([
                'name'          => 'AccountBalanceForm[userId]',
                'value'         => $model->userId,
                'initValueText' => $model->getUserName(),
                'pluginOptions' => [
                    'allowClear'         => true,
                    'minimumInputLength' => 2,
                    'ajax'               => [
                        'url'      => Url::to(['/user/user/list']),
                        'dataType' => 'json',
                        'data'     => new yii\web\JsExpression('function(params) { return {q:params.term, page:params.page, k:"id,username"}; }')
                    ],
                ],
            ]); ?>
        </div>
        <?= $form->field($model, 'amount')->textInput() ?>
        <?= $form->field($model, 'comment')->textInput() ?>


        <div class="form-group">
            <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

</div>
