<?php

use backend\components\columns\UserColumn;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\data\BaseDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel \backend\models\search\AccountBalanceSearch */
/* @var $dataProvider yii\data\BaseDataProvider */

$this->title                   = 'Bonus list';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-log-index">
    <p>
        <?= Html::a('Add bonus balance', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns'      => [
            [
                'attribute' => 'user',
                'format'    => 'raw',
                'value'     => function (\common\modules\payment\models\AccountBalance $accountBalance) {
                    return UserColumn::getMenu($accountBalance->paymentAccount->user) . ' ' . $accountBalance->paymentAccount->type;
                }
            ],
            [
                'attribute' => 'Balance',
                'value'     => function (\common\modules\payment\models\AccountBalance $accountBalance) {
                    return $accountBalance->money->getAmount();
                }
            ],
        ],
    ]); ?>
</div>