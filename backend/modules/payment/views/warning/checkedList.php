<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.12.18
 * Time: 17:34
 */

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Checked payment alarms';
$this->params['breadcrumbs'][] = ['label' => 'Payment alarms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-log-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'uuid',
            'date',
            'description',
            'created_at',
            'user' => [
                'label'  => 'User',
                'format' => 'raw',
                'value'  => function (\common\models\PaymentWarning $paymentWarning) {
                    return \backend\models\Backend::displayUser($paymentWarning->user->id);
                }
            ]
        ],
    ]); ?>
</div>
