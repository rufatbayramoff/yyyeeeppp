<?php

use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment alarms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-log-index">

    <div class="ps-banned-form">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($alarmPaymentsForm, 'dateFrom')->widget(DatePicker::class, [
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ],
        ]) ?>
        <?= $form->field($alarmPaymentsForm, 'dateEnd')->widget(DatePicker::class, [
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ],
        ]) ?>
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-danger ajax-submit']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'date',
            'description' => [
                'attribute' => 'description',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'checked',
                'format'    => 'raw',
                'value'     => /**
                 * @param \backend\modules\payment\models\PaymentAlarm $paymentAlarm
                 * @return string
                 */
                    function (\backend\modules\payment\models\PaymentAlarm $paymentAlarm) {
                        if (!$paymentAlarm->allowPressChecked) {
                            return 'Contact with Admin';
                        }
                        $url = Url::toRoute([
                            '/payment/warning/checked',
                            'uuid'            => $paymentAlarm->uuid,
                            'paymentDetailId' => $paymentAlarm->paymentDetailId,
                            'date'            => $paymentAlarm->date,
                            'description'     => strip_tags($paymentAlarm->description)
                        ]);

                        return Html::a('Checked', $url,
                            ['class' => 'btn  btn-ghost btn-info', 'data-method' => 'post']
                        );
                    }
            ],
        ],
    ]); ?>

    <a href="/payment/warning/checked-list">Checked List</a>
</div>
