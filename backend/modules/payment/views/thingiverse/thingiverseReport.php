<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Thingiverse reports';
$this->params['breadcrumbs'][] = $this->title;
?>

<p>
    <?= Html::a('Export', ['/statistic/reports/download?reportId=thingiverse_report'], ['class' => 'btn btn-primary']) ?>
</p>

<div class="payment-log-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'order_id',
            'date',
            'thing_id',
            'status',
            'earnings',
            'platform_fee',
            'transaction_fee',
            'app_transaction_code',
            'printing_fee',
            'shipping',
            'refund',
            'treatstock_transaction_fee'
        ],
    ]); ?>
</div>
