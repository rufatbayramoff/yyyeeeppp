<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.09.18
 * Time: 16:06
 */

use backend\components\columns\UserColumn;
use kartik\form\ActiveForm;
use lib\money\Money;
use yii\helpers\Html;
use yii\web\View;


/** @var $this View */

/** @var $balanceReportViewModel \backend\modules\payment\models\BalanceReportViewModel */

$comments = [
    'treatstock'  => 'Common treatstock user balance. We make payments throw this balance.',
    'thingiverse' => 'Thingiverse balance. Show how match money thingiverse own us. Should be minus or zero, thingiverse owe us.',
    'easypost'    => 'How match money we should pay for delivery.',
    'taxagent'    => 'Transitional balance witch used for pay taxes.',
    'paypal'      => 'Users pay using paypal. This is total users payments balance. Should be minus or zero, paypal owe us.',
    'braintree'   => 'Users pay using braintree. This is total users payments balance. Should be minus or zero, braintree owe us.'
];
$backgroundColor = [
    ''
]
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($balanceReportViewModel, 'dateFrom')->textInput(['maxlength' => true])->label('Date from') ?>
<?= $form->field($balanceReportViewModel, 'dateTo')->textInput(['maxlength' => true])->label('Date to') ?>
<?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>

    <div>
        <div style="padding: 15px 5px 10px 5px;">
            System Balances:
        </div>
        <?php
        $i = 0;
        foreach ($balanceReportViewModel->systemBalances as $userBalance) {
            $i++;
            if ($i % 2) {
                $trBackgroundColorOdd = 'trBackgroundColorOdd';
            } else {
                $trBackgroundColorOdd = '';
            }
            ?>
            <div class="row <?= $trBackgroundColorOdd ?>" style="padding: 10px;">
                <div class="col-xs-4 col-sm-3"><?= ucfirst($userBalance->user->username) ?></div>
                <div class="col-xs-4 col-sm-2" style="text-align: right;"><?= displayAsMoney($userBalance->money); ?></div>
                <div class="col-xs-2 col-sm-1"><a href="/payment/balance-report/details?user_id=<?=$userBalance->user->id?>">details report</a></div>
                <div class="col-xs-2 col-sm-1"><a>correct</a></div>
                <div class="col-sm-5" style="text-align: left"><?= $comments[$userBalance->user->username] ?? '' ?></div>
            </div>
        <?php }
        $i++;
        if ($i % 2) {
            $trBackgroundColorOdd = 'trBackgroundColorOdd';
        } else {
            $trBackgroundColorOdd = '';
        }
        ?>
        <div class="row <?= $trBackgroundColorOdd ?>" style="padding: 10px; border-bottom: 1px solid #ddd;">
            <div class="col-xs-6 col-sm-3">Total Users Balance</div>
            <div class="col-xs-6 col-sm-2" style="text-align: right;"><?= displayAsMoney($balanceReportViewModel->totalUsersBalance); ?></div>
            <div class="col-sm-1"><a>details report</a></div>
            <div class="col-sm-6" style="text-align: left">This is total users count sum</div>
        </div>
        <div class="row" style="padding: 15px 5px 10px 5px;border-bottom: 1px solid #ddd;">
            <div class="col-xs-6 col-sm-3">Total check</div>
            <div class="col-xs-6 col-sm-2" style="text-align: right;"><b><?= displayAsMoney($balanceReportViewModel->totalCheck); ?></b></div>
            <div class="col-sm-1"><a>details report</a></div>
            <div class="col-sm-7" style="text-align: left">This field should be zerro. If it`s not zero, balance invalid.</div>
        </div>
    </div>
    <br>
    <div>
        If balance more than zero - we owe user money.
    </div>
    <div>
        If balance less than zero - user owe us money.
    </div>
<?php
ActiveForm::end();