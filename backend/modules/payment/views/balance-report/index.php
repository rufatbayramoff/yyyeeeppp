<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.09.18
 * Time: 16:06
 */

use backend\components\columns\UserColumn;
use common\modules\payment\components\PaymentInfoHelper;
use kartik\form\ActiveForm;
use lib\money\Money;
use lib\money\MoneyMath;
use yii\helpers\Html;
use yii\web\View;


/** @var $this View */

/** @var $balanceReportViewModel \backend\modules\payment\models\BalanceReportViewModel */

$comments = [
    'treatstock'  => 'Common treatstock user balance. We make payments throw this balance.',
    'thingiverse' => 'Thingiverse balance. Show how match money thingiverse own us. Should be minus or zero, thingiverse owe us.',
    'easypost'    => 'How match money we should pay for delivery.',
    'taxagent'    => 'Transitional balance witch used for pay taxes.',
    'paypal'      => 'Users pay using paypal. This is total users payments balance. Should be minus or zero, paypal owe us.',
    'braintree'   => 'Users pay using braintree. This is total users payments balance. Should be minus or zero, braintree owe us.'
];
$backgroundColor = [
    ''
]
?>
    <form id="w0" class="form-vertical" action="/payment/balance-report/index" method="get" role="form">
        <div class="form-group">
            <label class="control-label" for="datefrom">Date from</label>
            <input type="date" id="datefrom" class="form-control" name="dateFrom" value="<?= $balanceReportViewModel->dateFrom ?>" style="max-width:200px;">
        </div>
        <div class="form-group">
            <label class="control-label" for="dateto">Date to</label>
            <input type="date" id="dateto" class="form-control" name="dateTo" value="<?= $balanceReportViewModel->dateTo ?>" style="max-width:200px;">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>

    <div>
        <div style="padding: 15px 5px 10px 5px;">
            System Balances:
        </div>
        <div class="row" style="padding: 10px;">
            <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div>
            <div class="hidden-xs hidden-sm col-md-2 col-lg-2" style="text-align: right;">
                <b>Diff</b>
            </div>
            <div class="hidden-xs hidden-sm col-md-2 col-lg-2" style="text-align: right;">
                <b>Current</b>
            </div>
            <div class="hidden-xs hidden-sm col-md-2 col-lg-2" style="text-align: right;">
                <b>Start period</b>
            </div>
            <div class="hidden-xs hidden-sm col-md-2 col-lg-2" style="text-align: right;">
                <b>Minus</b>
            </div>
            <div class="hidden-xs hidden-sm col-md-2 col-lg-2" style="text-align: right; white-space: nowrap">
                <b>Plus</b>
            </div>
            <div class="hidden-xs hidden-sm col-md-12 col-lg-12"></div>
        </div>
        <?php
        $i = 0;
        foreach ($balanceReportViewModel->systemBalances as $userBalanceEx) {
            $i++;
            if ($i % 2) {
                $trBackgroundColorOdd = 'trBackgroundColorOdd';
            } else {
                $trBackgroundColorOdd = '';
            }
            ?>
            <div class="row <?= $trBackgroundColorOdd ?>" style="padding: 10px;">
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2"><b><?= ucfirst($userBalanceEx->paymentAccount->user->username) ?></b>
                    (<?= $userBalanceEx->paymentAccount->type ?>)
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2" style="text-align: right;">
                    <a href="<?= PaymentInfoHelper::getDetailsUrl($userBalanceEx->paymentAccount, $balanceReportViewModel->dateFrom, $balanceReportViewModel->dateTo); ?>">
                        <?= $userBalanceEx->getDiff()->getAmount() > 0 ? '+' : '' ?> <?= displayAsMoney($userBalanceEx->getDiff()); ?>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2" style="text-align: right;">
                    <?= displayAsMoney($userBalanceEx->balanceCurrent); ?>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2" style="text-align: right;">
                    <?= displayAsMoney($userBalanceEx->balanceStart); ?>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2" style="text-align: right;">
                    <a href="<?= PaymentInfoHelper::getDetailsUrl($userBalanceEx->paymentAccount, $balanceReportViewModel->dateFrom, $balanceReportViewModel->dateTo,
                        '-'); ?>"><?= displayAsMoney($userBalanceEx->balanceMinus); ?></a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2" style="text-align: right; white-space: nowrap">
                    <a href="<?= PaymentInfoHelper::getDetailsUrl($userBalanceEx->paymentAccount, $balanceReportViewModel->dateFrom, $balanceReportViewModel->dateTo,
                        '+'); ?>"> + <?= displayAsMoney($userBalanceEx->balancePlus); ?></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: left"><?= $comments[$userBalanceEx->paymentAccount->id] ?? '' ?></div>
            </div>
        <?php }
        foreach (\common\models\PaymentAccount::getUserAccountRealTypes() as $type) {
            $i++;
            if ($i % 2) {
                $trBackgroundColorOdd = 'trBackgroundColorOdd';
            } else {
                $trBackgroundColorOdd = '';
            }
            $current = $balanceReportViewModel->totalUsersBalance->balanceCurrent[$type] ?? Money::zero();
            $start = $balanceReportViewModel->totalUsersBalance->balanceStart[$type] ?? Money::zero();
            $moneyMinus = $balanceReportViewModel->totalUsersBalance->balanceMinus[$type] ?? Money::zero();
            $moneyPlus = $balanceReportViewModel->totalUsersBalance->balancePlus[$type] ?? Money::zero();
            $moneyDiff = MoneyMath::sum($moneyMinus, $moneyPlus);
            ?>
            <div class="row <?= $trBackgroundColorOdd ?>" style="padding: 10px; border-bottom: 1px solid #ddd;">
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2"><b>Total Users</b> (<?= $type ?>)</div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2" style="text-align: right;">
                    <a href="<?= PaymentInfoHelper::getDetailsUrl(null, $balanceReportViewModel->dateFrom, $balanceReportViewModel->dateTo); ?>">
                        <?= displayAsMoney($moneyDiff); ?>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2" style="text-align: right;">
                    <?= displayAsMoney($current); ?>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2" style="text-align: right;">
                    <?= displayAsMoney($start); ?>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2" style="text-align: right;">
                    <a href="<?= PaymentInfoHelper::getDetailsUrl(null, $balanceReportViewModel->dateFrom, $balanceReportViewModel->dateTo, '-'); ?>">
                       <?= displayAsMoney($moneyMinus); ?>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2" style="text-align: right;">
                    <a href="<?= PaymentInfoHelper::getDetailsUrl(null, $balanceReportViewModel->dateFrom, $balanceReportViewModel->dateTo, '-'); ?>">
                   + <?= displayAsMoney($moneyPlus); ?>
                    </a>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="row" style="padding: 15px 5px 10px 5px;border-bottom: 1px solid #ddd;">
            <div class="col-xs-6 col-sm-3"><b>Total check</b></div>
            <div class="col-xs-6 col-sm-2" style="text-align: right;"><b><?= displayAsMoney($balanceReportViewModel->totalCheck); ?></b></div>
            <div class="col-sm-7" style="color:red;text-align: left">This field should be zerro. If it`s not zero, balance invalid.</div>
        </div>
    </div>
    <br>
    <div>
        If balance more than zero - we owe user money.
    </div>
    <div>
        If balance less than zero - user owe us money.
    </div>
<?php
