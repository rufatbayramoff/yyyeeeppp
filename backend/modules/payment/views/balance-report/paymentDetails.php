<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.09.18
 * Time: 16:06
 */

use backend\components\columns\UserColumn;
use common\models\PaymentDetail;
use kartik\form\ActiveForm;
use lib\money\Money;
use yii\helpers\Html;
use yii\web\View;


/** @var $this View */
/** @var $paymentDetails PaymentDetail[] */

foreach ($paymentDetails as $paymentDetail) {
    ?>
    <div class="row" style="padding-bottom: 5px;">
        <div class="col-sm-1"><?= $paymentDetail->id ?></div>
        <div class="col-sm-2"><?= $paymentDetail->type ?></div>
        <div class="col-sm-3" style="word-break: break-all"><?= H($paymentDetail->paymentAccount->user->username).' ('.$paymentDetail->paymentAccount->type.')' ?></div>
        <div class="col-sm-2"><?= $paymentDetail->amount ?></div>
        <div class="col-sm-4"><?= $paymentDetail->description ?></div>
    </div>
    <?php
    if ($vendorTransactionHistory = $paymentDetail->getVendorTransactionHistory()) {
        $transaction = $vendorTransactionHistory->transaction;
        ?>
        <div class="row" style="padding: 0 20px 5px 20px; border-bottom: 1px solid #f4f4f4"">
        <div class="col-sm-6 col-md-3">
            <label>Transaction:</label>
            <?= $vendorTransactionHistory->transaction->id ?>
        </div>
        <div class="col-sm-6 col-md-3">
            <label>Date:</label>
            <?= $vendorTransactionHistory->created_at ?>
        </div>
        <div class="col-sm-6 col-md-3">
            <label>Vendor:</label>
            <?= H($transaction->vendor) ?>
        </div>
        <div class="col-sm-6 col-md-3">
            <label>User:</label>
            <?= H($transaction->user->username) ?>
        </div>
        <div class="col-sm-6 col-md-3">
            <label>Status:</label>
            <?= H($transaction->status) ?>
        </div>
        <div class="col-sm-6 col-md-3">
            <label>Type:</label>
            <?= H($transaction->type) ?>
        </div>
        <div class="col-sm-6 col-md-3">
            <label>Amount:</label>
            <?= H($transaction->amount) ?>
        </div>
        <div class="col-sm-6 col-md-3">
            <label>Comment:</label>
            <?= json_encode($vendorTransactionHistory->comment) ?>
        </div>
        </div>
        <?php
    }
}
