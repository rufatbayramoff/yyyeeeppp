<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.09.18
 * Time: 14:16
 */

use kartik\select2\Select2;
use lib\money\Money;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var $balanceDetailsViewModel \backend\modules\payment\models\BalanceDetailsViewModel */
?>
<div class="row">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'method' => 'get', 'action' => '/payment/balance-report/details']); ?>
    <div class="col-md-6">
        <div class="form-group field-type">
            <label class="control-label col-sm-3" for="type">User</label>
            <div class="col-sm-6">
                <?php echo Select2::widget([
                    'name'          => 'user_id',
                    'value'         => $balanceDetailsViewModel->user->id ?? null,
                    'initValueText' => $balanceDetailsViewModel->user->username ?? null,
                    'pluginOptions' => [
                        'allowClear'         => true,
                        'minimumInputLength' => 2,
                        'ajax'               => [
                            'url'      => Url::to(['/user/user/list']),
                            'dataType' => 'json',
                            'data'     => new yii\web\JsExpression('function(params) { return {q:params.term, page:params.page, k:"id,username"}; }')
                        ],
                    ],
                ]); ?>
            </div>
        </div>
        <div class="form-group field-type">
            <label class="control-label col-sm-3" for="type">Type</label>
            <div class="col-sm-6">
                <?php echo Html::dropDownList('account_type', $balanceDetailsViewModel->accountType,
                    array_merge(['' => 'Any'], array_combine($balanceDetailsViewModel->paymentAccountTypes, $balanceDetailsViewModel->paymentAccountTypes)),
                    ['class' => 'form-control']); ?>
            </div>
        </div>
        <?= $form->field($balanceDetailsViewModel, 'sign')->dropDownList(['' => 'Any', '+' => '+', '-' => '-']) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($balanceDetailsViewModel, 'dateFrom')->textInput(['type' => 'date']) ?>
        <?= $form->field($balanceDetailsViewModel, 'dateTo')->textInput(['type' => 'date']) ?>
    </div>
    <div class="col-md-12" style="text-align: center;">
        <?= Html::submitButton('View', ['name' => 'export', 'value' => '0', 'class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('Export', ['name' => 'export', 'value' => '1', 'class' => 'btn btn-info']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<h4>Balance status:</h4>
<div class="row" style="padding: 20px;">
    <?php
    foreach ($balanceDetailsViewModel->paymentAccountTypes as $paymentAccountType) {
        $diff = $balanceDetailsViewModel->balanceEx->getDiff()[$paymentAccountType] ?? Money::zero();
        $plus = $balanceDetailsViewModel->balanceEx->balancePlus[$paymentAccountType] ?? Money::zero();
        $minus = $balanceDetailsViewModel->balanceEx->balanceMinus[$paymentAccountType] ?? Money::zero();
        $current = $balanceDetailsViewModel->balanceEx->balanceCurrent[$paymentAccountType] ?? Money::zero();
        $startPeriod = $balanceDetailsViewModel->balanceEx->balanceStart[$paymentAccountType] ?? Money::zero();
        ?>
        <div class="col-md-2">
            <b>Type</b>: <?= $paymentAccountType ?>
        </div>
        <div class="col-md-2">
            <b>Diff</b>: <?= displayAsMoney($diff) ?>
        </div>
        <div class="col-md-2">
            <b>Plus</b>: <?= displayAsMoney($plus) ?>
        </div>
        <div class="col-md-2">
            <b>Minus</b>: <?= displayAsMoney($minus) ?>
        </div>
        <div class="col-md-2">
            <b>Current</b>: <?= displayAsMoney($current) ?>
        </div>
        <div class="col-md-2">
            <b>Start period</b>: <?= displayAsMoney($startPeriod) ?>
        </div>
        <?php
    }
    ?>
</div>
<h4>Operaions list:</h4>
<?php
$i = 0;
foreach ($balanceDetailsViewModel->paymentDetailOperations as $paymentDetailOperation) {
    $i++;
    if ($i % 2) {
        $trBackgroundColorOdd = 'trBackgroundColorOdd';
    } else {
        $trBackgroundColorOdd = '';
    }
    ?>
    <div class="row <?= $trBackgroundColorOdd ?>" style="padding: 15px 10px 10px;">
        <div class="col-sm-12 col-md-3">
            <b>Operation uuid: </b><?= $paymentDetailOperation->uuid ?>
            <br>
            <b>Date: </b><?= ucfirst($paymentDetailOperation->created_at) ?>
            <br>
            <b>Payment Id:</b> <?= $paymentDetailOperation->payment->id; ?>
            <br>
            <b>Invoice uuid:</b> <?= $paymentDetailOperation->payment->paymentInvoice->uuid ?? ''; ?>
            <br>
            <b>Order Id:</b> <?= $paymentDetailOperation->payment->paymentInvoice->store_order_id ?? ''; ?>
            <br>
            <b>Description:</b>
            <?= $paymentDetailOperation->payment->description; ?></div>
        <div class="col-sm-12 col-md-9">
            <div style="padding-bottom: 10px;"><b>Payment details:</b></div>
            <?= $this->render('paymentDetails', ['paymentDetails' => $paymentDetailOperation->paymentDetails]); ?>
        </div>
    </div>
    <?php
}
?>
