<?php

use backend\components\columns\UserColumn;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\data\BaseDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\BaseDataProvider */

$this->title                   = 'Conversation balance';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-log-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            [
                'attribute' => 'type',
                'format'    => 'raw',
                'value'     => function (\common\modules\payment\models\ConversationBalance $conversationBalance) {
                    return $conversationBalance->type;
                }
            ],
            [
                'attribute' => 'balances',
                'value'     => function (\common\modules\payment\models\ConversationBalance $conversationBalance) {
                    foreach ($conversationBalance->balances as $currencyBalance) {
                        return displayAsMoney($currencyBalance);
                    }
                }
            ],
        ],
    ]); ?>
</div>