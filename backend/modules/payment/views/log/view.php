<?php

use common\models\PaymentTransaction;
use common\modules\payment\components\PaymentLogger;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $paymentLog common\models\PaymentLog */

$this->title = $paymentLog->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-log-view">
    <?= DetailView::widget([
        'model'      => $paymentLog,
        'attributes' => [
            'id',
            'created_at',
            'level',
            'type',
            'comment:ntext',
            'info'           => [
                'attribute' => 'info',
                'format'    => 'raw',
                'value'     => function (\common\models\PaymentLog $paymentLog) {
                    return '<pre class="jsonHighlight small-text">' . json_encode($paymentLog->info, JSON_PRETTY_PRINT) . '</pre>';
                }
            ],
            'search_markers' => [
                'attribute' => 'search_markers',
                'format'    => 'raw',
                'value'     => function (\common\models\PaymentLog $paymentLog) {
                    $stripeVendorView = '';
                    if ($paymentLog->type===PaymentTransaction::VENDOR_STRIPE) {
                        $stripeVendorView = '<a href="https://dashboard.stripe.com/payments/'.$paymentLog->search_markers[PaymentLogger::MARKER_VENDOR_TRANSACTION_ID].'">View stripe transaction</a>';
                    }

                    return '<pre class="jsonHighlight small-text">' . json_encode($paymentLog->search_markers, JSON_PRETTY_PRINT) . '</pre>'.$stripeVendorView;
                }
            ],
        ],
    ]) ?>
</div>
