<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentLog */

$this->title = 'Update Payment Log: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-log-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
