<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Logs';
$this->params['breadcrumbs'][] = $this->title;
$searchMarkerView = function (\common\models\PaymentLog $paymentLog) use ($searchModel) {
    $markers = H(json_encode($paymentLog->search_markers, JSON_PRETTY_PRINT));
    if ($searchModel->search_markers) {
        $markers = str_replace($searchModel->search_markers, '<b>' . $searchModel->search_markers . '</b>', $markers);
    }
    return '<p style="font-size:10px;">' . $markers . '</p>';
};

$searchInfoView = function (\common\models\PaymentLog $paymentLog) use ($searchModel) {
    $info = H(json_encode($paymentLog->info, JSON_PRETTY_PRINT));
    if ($searchModel->search_markers && $searchPos = strpos($info, $searchModel->search_markers)) {
        $sT = $searchPos - 500;
        if ($sT < 0) {
            $sT = 0;
        }
        $info = substr($info, $sT, $searchPos - $sT) . '<b>' . $searchModel->search_markers . '</b>' . substr($info, $searchPos + strlen($searchModel->search_markers), 500);
    } else {
        $info = substr($info, 0, 500);
    }
    return '<p style="font-size:10px;">' . $info . '</p>';
};

?>
<div class="payment-log-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'level',
            'type',
            'comment:ntext',
            [
                'attribute' => 'search_markers',
                'format'    => 'raw',
                'value'     => $searchMarkerView
            ],
            [
                'attribute' => 'info',
                'format'    => 'raw',
                'value'     => $searchInfoView
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
