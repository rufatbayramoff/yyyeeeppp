<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.11.18
 * Time: 14:36
 */

namespace backend\modules\payment\models;

use Faker\Provider\Uuid;
use yii\base\Model;

class PaymentAlarm extends Model
{
    /**
     * @var Uuid
     */
    public $uuid;

    /**
     * Payment Detail id
     *
     * @var int
     */
    public $paymentDetailId;

    /**
     * Event date
     *
     * @var string
     */
    public $date;

    /**
     * Description
     *
     * @var string
     */
    public $description;

    /**
     * @var bool
     */
    public $checked = false;

    /**
     * Is allow to be checked by admin
     *
     * @var bool
     */
    public $allowPressChecked = true;
}