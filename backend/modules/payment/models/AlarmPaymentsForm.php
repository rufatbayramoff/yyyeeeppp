<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.11.18
 * Time: 14:00
 */

namespace backend\modules\payment\models;

use common\components\DateHelper;
use yii\base\Model;

class AlarmPaymentsForm extends Model
{
    public $dateFrom;
    public $dateEnd;

    public function init()
    {
        $this->dateFrom = DateHelper::subNow('P60D');
        $this->dateEnd = DateHelper::now();
    }
}