<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.09.18
 * Time: 10:33
 */
namespace backend\modules\payment\models;

use common\modules\payment\models\BalanceEx;
use common\modules\payment\models\PaymentAccountBalancePeriod;
use lib\money\Money;
use yii\base\Model;

class BalanceReportViewModel extends Model
{
    /** @var string */
    public $dateFrom;

    /** @var string */
    public $dateTo;

    /** @var PaymentAccountBalancePeriod[] */
    public $systemBalances = [];

    /** @var BalanceEx */
    public $totalUsersBalance;

    /** @var Money */
    public $totalCheck;
}