<?php

namespace backend\modules\payment\models;

use common\components\DateHelper;
use common\models\User;
use yii\base\Model;

class AccountBalanceForm extends Model
{
    public $userId;
    public $amount;
    public $comment;

    public function getUserName(): string
    {
        return $this->userId?User::findByPk($this->userId)->username:'';
    }

    public function rules()
    {
        return [
            [['userId', 'amount'], 'integer'],
            [['comment'], 'string']
        ];
    }
}