<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.09.18
 * Time: 10:55
 */

namespace backend\modules\payment\models;

use common\components\DateHelper;
use common\models\PaymentAccount;
use common\models\PaymentDetailOperation;
use common\models\User;
use common\modules\payment\models\BalanceEx;
use common\modules\payment\models\PaymentAccountBalancePeriod;
use lib\money\Money;
use yii\base\Model;

class BalanceDetailsViewModel extends Model
{
    /** @var string */
    public $dateFrom;

    /** @var string */
    public $dateTo;

    /** @var string */
    public $sign;

    /** @var User|null */
    public $user;

    /** @var string */
    public $accountType;

    /** @var PaymentDetailOperation[] */
    public $paymentDetailOperations;

    /** @var User */
    public $usersSelectList;

    /** @var BalanceEx */
    public $balanceEx;

    /** @var array */
    public $paymentAccountTypes;

    public function formName()
    {
        return '';
    }

    /**
     * @param array $data
     * @return bool|void
     */
    public function load($data, $formName = null)
    {
        $userId = $data['user_id'] ?? '';
        $this->user = User::findOne([
            'id' => $userId
        ]);
        $this->dateFrom = '';
        if (array_key_exists('dateFrom', $data) && $data['dateFrom']) {
            $this->dateFrom = DateHelper::filterDate($data['dateFrom']);
        }
        $this->dateTo = '';
        if (array_key_exists('dateTo', $data) && $data['dateTo']) {
            $this->dateTo = DateHelper::filterDate($data['dateTo']);
        }
        $this->sign = $data['sign'] ?? '';
        $this->accountType = $data['account_type']??'';
    }

    public function getUsername()
    {
        return $this->user->username ?? '';
    }
}