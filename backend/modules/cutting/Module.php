<?php
namespace backend\modules\cutting;

/**
 * Payment module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\cutting\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
