<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.04.18
 * Time: 16:29
 */


use yii\grid\ActionColumn;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Cutting materials';
$this->params['breadcrumbs'][] = $this->title;
?>
<p>
    <?= Html::a('Create Cutting Material', ['create'], ['class' => 'btn btn-success']) ?>
</p>
<div class="store-unit-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            'code',
            'title',
            'default_thickness',
            'default_width',
            'default_length',
            'is_active',
            [
                'class'          => ActionColumn::class,
                'template'       => '{update}',
            ]
        ],
    ]); ?>
</div>
