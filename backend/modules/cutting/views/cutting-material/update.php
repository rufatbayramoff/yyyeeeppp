<?php

use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CuttingMaterial */

$this->title                   = 'Update Cutting Material: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cutting Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cnc-material-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <h4>Colors</h4>
    <?php
    $colorsProvider = new ActiveDataProvider(['query' => $model->getCuttingMaterialColors()]);
    echo GridView::widget([
        'dataProvider' => $colorsProvider,
        'columns'      => [
            'title' => [
                'label' => 'title',
                'value' =>
                    function (\common\models\CuttingMaterialColor $color) {
                        return $color->cuttingColor->title;
                    }
            ],

            [
                'label'  => '',
                'format' => 'raw',
                'value'  =>
                    function (\common\models\CuttingMaterialColor $color) {
                        $edit   = '<a href="/cutting/cutting-material/update-color?id=' . $color->id . '" title="Update" aria-label="Update"><span class="glyphicon glyphicon-pencil"></span></a>';
                        $delete = '<a href="/cutting/cutting-material/delete-color?id=' . $color->id . '" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post"><span class="glyphicon glyphicon-trash"></span></a>';
                        return $edit . ' ' . $delete;
                    }
            ]
        ],
    ]); ?>
    <p>
        <?= Html::a('Add color', ['create-color', 'cuttingMaterialId' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
</div>
