<?php

use common\components\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $cuttingMaterial common\models\CuttingMaterial */

$this->title                   = 'Edit Cutting Material Color';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$selected      = ArrayHelper::getColumn($model->cuttingMaterial->cuttingMaterialColors, 'id');
$allowedColors = ArrayHelper::map(\common\models\PrinterColor::find()->active()->andWhere(
    ['or',
     ['not in', 'id', $selected],
     ['id' => $model->id]
    ])->all(), 'id', 'title')
?>
<div class="cutting-material-create">

    <div class="cnc-material-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'id')->hiddenInput()->label('') ?>

        <?= $form->field($model, 'cutting_color_id')->dropDownList($allowedColors) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
