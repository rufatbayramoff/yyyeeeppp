<?php

use common\components\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $cuttingMaterial common\models\CuttingMaterial */

$this->title                   = 'Create Cutting Material Color';
$this->params['breadcrumbs'][] = ['label' => 'Cutting Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$selected = ArrayHelper::getColumn($cuttingMaterial->cuttingMaterialColors, 'cutting_color_id');
$allowedColors = ArrayHelper::map(\common\models\PrinterColor::find()->active()->andWhere(['not in', 'id', $selected])->orderBy('title')->all(), 'id', 'title')
?>
<div class="cutting-material-create">

    <div class="cnc-material-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'cutting_material_id')->hiddenInput()->label('') ?>

        <?= $form->field($model, 'cutting_color_id')->dropDownList($allowedColors) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
