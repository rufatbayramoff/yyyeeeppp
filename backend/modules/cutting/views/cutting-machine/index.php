<?php

use backend\components\columns\PsColumn;
use common\models\CuttingMachine;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CuttingMachineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Cutting machine';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-unit-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'format' => 'raw',
                'value'     => function (CuttingMachine $cuttingMachine) {
                    $url        = \yii\helpers\Url::toRoute(['/company/company-service/view', 'id' => $cuttingMachine->companyService->id]);
                    $viewButton = Html::a($cuttingMachine->id, $url, ['class' => 'btn btn-primary', 'data-pjax' => 0]);
                    return $viewButton;
                }
            ],
            [
                'label'     => 'Title',
                'attribute' => 'serviceTitle',
                'value'     => function (CuttingMachine $cuttingMachine) {
                    return $cuttingMachine->companyService->title;
                }
            ],
            [
                'attribute' => 'psUser',
                'label'     => 'Company',
                'format'    => 'raw',
                'value'     => function (CuttingMachine $cuttingMachine) {
                    return PsColumn::getMenu($cuttingMachine->companyService->company);
                }
            ],
            [
                'attribute' => 'serviceStatus',
                'label'     => 'Status',
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'serviceStatus',
                    \common\models\CompanyService::getModeratorStatusLabels(),
                    ['class' => 'form-control', 'prompt' => 'Any']
                ),
                'value'     => function (CuttingMachine $cuttingMachine) {
                    return $cuttingMachine->companyService->moderator_status;
                }
            ],
            [
                'attribute' => 'serviceDescription',
                'label'     => 'Description',
                'value'     => function (CuttingMachine $cuttingMachine) {
                    return $cuttingMachine->companyService->description;
                }
            ],
            [
                'label'     => 'Is deleted',
                'attribute' => 'serviceIsDeleted',
                'value'     => function (CuttingMachine $cuttingMachine) {
                    return ($cuttingMachine->companyService->is_deleted) ? 'Yes' : 'No';
                },
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'serviceIsDeleted',
                    [1 => 'Yes', 0 => 'No'],
                    ['class' => 'form-control', 'prompt' => 'All']),
            ]
        ]
    ]); ?>
</div>
