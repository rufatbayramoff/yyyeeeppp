<?php

namespace backend\modules\cutting\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\CuttingMaterialSearch;
use common\models\CncMaterialSame;
use common\models\CuttingMaterial;
use common\models\CuttingMaterialColor;
use Yii;

class CuttingMaterialController extends AdminController
{

    /**
     * @param $action
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('printer.view');
        return parent::beforeAction($action);
    }

    /**
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new CuttingMaterialSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model            = new CuttingMaterial();
        $model->is_active = 1;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->setFlashMsg(true, 'Created');
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateColor($cuttingMaterialId)
    {
        $cuttingMaterial            = CuttingMaterial::findByPk($cuttingMaterialId);
        $model                      = new CuttingMaterialColor();
        $model->cutting_material_id = $cuttingMaterial->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->setFlashMsg(true, 'Created');
            return $this->redirect(['update', 'id' => $model->cuttingMaterial->id]);
        } else {
            return $this->render('createColor', [
                'model'           => $model,
                'cuttingMaterial' => $cuttingMaterial
            ]);
        }
    }

    public function actionUpdateColor($id)
    {
        $cuttingMaterialColor = CuttingMaterialColor::findByPk($id);
        if ($cuttingMaterialColor->load(Yii::$app->request->post()) && $cuttingMaterialColor->save()) {
            $this->setFlashMsg(true, 'saved');
            return $this->redirect(['update', 'id' => $cuttingMaterialColor->cuttingMaterial->id]);
        } else {
            return $this->render('editColor', [
                'model' => $cuttingMaterialColor,
            ]);
        }
    }

    public function actionDeleteColor($id)
    {
        $cuttingMaterialColor = CuttingMaterialColor::findByPk($id);
        $cuttingMaterialColor->delete();
        $cuttingMaterial      = $cuttingMaterialColor->cuttingMaterial;
        return $this->redirect('/cutting/cutting-material/update?id=' . $cuttingMaterial->id);
    }

    public function actionView($id)
    {
        return $this->redirect('/cutting/cutting-material/update?id=' . $id);
    }


    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = CuttingMaterial::findByPk($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->setFlashMsg(true, 'Updated');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}