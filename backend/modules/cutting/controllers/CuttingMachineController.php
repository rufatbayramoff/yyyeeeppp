<?php

namespace backend\modules\cutting\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\CuttingMachineSearch;
use backend\models\search\CuttingMaterialSearch;
use common\models\CncMaterialSame;
use common\models\CuttingMachine;
use common\models\CuttingMaterial;
use common\models\CuttingMaterialColor;
use Yii;

class CuttingMachineController extends AdminController
{

    /**
     * @param $action
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('printer.view');
        return parent::beforeAction($action);
    }

    /**
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new CuttingMachineSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $cuttingMachine = CuttingMachine::findByPk($id);
        if ($cuttingMachine->load(Yii::$app->request->post()) && $cuttingMachine->save()) {
            $this->setFlashMsg(true, 'Updated');
            return $this->redirect(['update', 'id' => $cuttingMachine->id]);
        } else {
            return $this->render('update', [
                'cuttingMachine' => $cuttingMachine,
            ]);
        }
    }
}