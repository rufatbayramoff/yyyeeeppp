<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-index table-responsive"">

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<p>
    <?= Html::a('Add company', ['create'], ['class' => 'btn btn-success']) ?>
</p>
<?= GridView::widget(
    [
        'caption'      => sprintf(
            '<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel, 'Company'),
            \backend\widgets\ExcelImportWidget::widget(['tableName' => $searchModel->tableName()])
        ),
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [

            ['class' => 'yii\grid\ActionColumn'],
            'id',
            [
                'label' => 'Company',
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function($model) {
                    $user  = \backend\components\columns\PsColumn::getMenu($model);
                    return $user;
                }
            ],
           /* [
                'attribute' => 'title',
                'format' => 'raw',
                'value'     => function ($data) {
                    return Html::a($data->title, param('server').'/c/'.$data->url, ['target'=>'_blank']);
                }
            ],*/
            /*[
               'attribute' => 'description',
               'value'     => function ($data) {
                   return strlen($data->description) > 100 ? Html::encode(substr($data->description, 0, 100)) . "..." : $data->description;
               }
           ],
          [
               'attribute' => 'logo_file_id',
               'label'     => 'Logo',
               'format'    => 'raw',
               'value'     => function (\common\models\Ps $ps) {
                   return \frontend\components\image\ImageHtmlHelper::getClickableThumb($ps->logoFile);
               }
           ],*/
            // 'phone_code',
            'phone',
            // 'moderator_status',
            // 'moderated_at',
            'created_at',
            'updated_at',
            // 'phone_status',
            // 'phone_country_iso',
            // 'dont_show_download_policy_modal',
            // 'is_excluded_from_printing',
            // 'sms_gateway',
            // 'logo_circle',
            // 'picture_file_ids:ntext',
            // 'designer_picture_file_ids:ntext',
          //   'url',
            // 'url_changes_count:url',
            // 'url_old:url',
            // 'is_test_order_offer_showed',
            // 'is_cnc_allowed',
            // 'max_progress_orders_count',
            // 'is_cnc_hints_readed',
             'website',
            // 'facebook',
            // 'instagram',
            // 'twitter',
            // 'is_designer',
            ['class' => \backend\components\columns\UserLocationColumn::class, 'attribute' => 'location'],
            'country_id',
            // 'currency',
            // 'cover_file_id',
            'ownership',
            'total_employees',
            'year_established',
            'annual_turnover',

        ],
    ]
); ?>
</div>
