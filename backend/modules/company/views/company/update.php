<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ps */
/* @var $user \common\models\User */

$this->title = 'Update Company: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-update">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
    ]) ?>

</div>
