<?php

use frontend\components\image\ImageHtmlHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ps */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord){
    $model->country_id = \common\models\GeoCountry::tryFind(['iso_code'=>'US'])->id;
}
?>

<div class="row">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
    <div class="col-lg-8">
        <div class="ps-form">
            <?= $form->field($model, 'is_active')->checkbox() ?>
            <?= $form->field($currentUser, 'email')->input('email') ?>
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

           <?php
            if ($model->logoFile) {


                ?>
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                <div class="form-group field-product-deletedmainphotofile">
                    <?php echo ImageHtmlHelper::getClickableThumb($model->logoFile); ?>
                    <label><input type="checkbox" id="company-deletelogofile" name="CompanyEditForm[deleteLogoFile]"> Delete Logo File</label>
                    <div class="help-block"></div>
                </div>
                </div>
                </div>
            <?php
            } ?>

            <?= $form->field($model, 'logoFileUpload')->fileInput(['accept' => 'image/*'])->label('Logo') ?>


            <?php
            echo $form->field($model, 'country_id')->widget(
                \kartik\select2\Select2::classname(), [
                'data' => yii\helpers\ArrayHelper::map(common\models\GeoCountry::find()->asArray()->all(), 'id', 'title' ),
                'options' => ['placeholder' => 'Select', 'style' => 'width:100%'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
            <?= $form->field($model, 'ownership')->dropDownList(
                array_combine($model->ownershipList, $model->ownershipList)
            ) ?>


            <?= $form->field($model, 'total_employees')->dropDownList(
                array_combine($model->totalEmployeesList, $model->totalEmployeesList)
            ) ?>

            <?= $form->field($model, 'year_established')->widget(
                    \kartik\widgets\DatePicker::class, [
                    'size' => 'sm',
                    'pluginOptions' => [
                        'minViewMode' => 2,
                        'autoclose'=>true,
                        'format' => 'yyyy'
                    ]
                ]
            )?>

            <?= $form->field($model, 'annual_turnover')->input('number') ?>

            <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>

            <?php /**
            $form->field($model, 'phone_country_iso')->widget(
            \kartik\select2\Select2::classname(), [
            'data' => $model->getPhoneCodesIso(),
            'options' => ['placeholder' => 'Select', 'style' => 'width:100%'],
            'pluginOptions' => [
            'allowClear' => true
            ],
            ]); **/
            ?>




        </div>
    </div>
    <div class="col-lg-4">

            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

