<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Ps */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-view">


    <p>
        <div class="row">
        <div class="col-lg-8">
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= Html::a('ADD NEW', ['create', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
        </div>
    </div>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'title',
            'description:ntext',
            'logo',
            'phone_code',
            'phone',
            'is_active',
            'moderator_status',
            'moderated_at',
            'created_at',
            'updated_at',
            'phone_status',
        ],
    ]) ?>

</div>
    <h3>History</h3>
<?= \yii\grid\GridView::widget(
    [
        'dataProvider' => \common\models\CompanyHistory::getDataProvider(['company_id' => $model->id], 10, ['sort' => ['defaultOrder' => ['id'=>SORT_DESC]]]),
        'columns'      => [
            'id',
            'created_at:datetime',
            'action_id',
            'comment',
            'user.username',
        ],
    ]
); ?>