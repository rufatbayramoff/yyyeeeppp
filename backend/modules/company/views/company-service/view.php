<?php

use backend\components\AdminAccess;
use backend\components\columns\PsColumn;
use common\interfaces\ModelHistoryInterface;
use common\models\CompanyService;
use common\models\ModerLog;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyService */
/* @var $historySearch \backend\models\search\PsCncMachineHistorySearch|null */
/* @var $historyProvider null|\yii\data\ActiveDataProvider */

$psMachine = $model;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ps-machine-view">
    <div class="m-b10">

    <?php
    if ($model->moderator_status != CompanyService::MODERATOR_STATUS_REJECTED) {
        echo Html::button(
            'Reject',
            [
                'title'       => 'Reject reason',
                'class'       => 'btn btn-warning btn-ajax-modal',
                'data-pjax'   => 0,
                'value'       => \yii\helpers\Url::toRoute(['/company/company-service/reject', 'id' => $model->id]),
                'data-target' => '#model_storereject'
            ]
        );
    }
    echo ' &nbsp; ';
    if ($model->moderator_status != CompanyService::MODERATOR_STATUS_APPROVED) {
        echo Html::a(
            'Approve',
            ['/company/company-service/approve', 'id' => $model->id],
            [
                'title'       => 'Approved',
                'class'       => 'btn btn-success m-r20',
            ]
        );
    }
     ?></div>
    <?php
    if(in_array($model->moderator_status, [CompanyService::MODERATOR_STATUS_REJECTED, CompanyService::MODERATOR_STATUS_REJECTED_REVIEWING])){
    echo '<br />
    <div class="m-t20 m-b10"><p class="callout callout-danger">REJECTED: ' . ModerLog::getLastRejectModerLog(ModerLog::TYPE_SERVICE, $model->id) . '</p></div>';
    }
    ?>

    <?php
    $attributes = [
        'model'      => $model,
        'attributes' => [
            'id',
            'category_id',
            'title',
            'ps_id',
            'type',
            'ps_printer_id',
            'ps_cnc_machine_id',
            [
                'attribute' => 'ps_id',
                'format' => 'raw',
                'value' => PsColumn::getMenu($model->ps),
            ],
            [
                'label'  => 'Location',
                'format' => 'raw',
                'value'  => $psMachine->location ? \common\models\UserAddress::formatAddress($psMachine->location->toUserAddress()) : '-',
            ],
            'created_at',
            'updated_at',
            'is_location_change',
        ],
    ];
    if (!AdminAccess::can('psmachine.moderate')) {
        $attributes['attributes'][]='is_deleted';
        $attributes['attributes'][]='visibility';
        $attributes['attributes'][]='moderator_status';
    }

    $commonDetails = DetailView::widget(
        $attributes
    );

    if (AdminAccess::can('psmachine.moderate')) {
        $commonForm = $this->render('commonForm', ['psMachine' => $psMachine]);
    } else {
        $commonForm = '';
    }

    $viewFilePath = __DIR__ . '/' . 'viewPsMachine' . ucfirst($psMachine->type) . '.php';
    if (file_exists($viewFilePath)) {
        echo $this->renderFile($viewFilePath, ['commonDetails' => $commonDetails, 'commonForm' => $commonForm, 'psMachine' => $psMachine]);
    }

    if ($historyProvider) {
        echo GridView::widget(
            [
                'dataProvider' => $historyProvider,
                'columns'      => [
                    'id',
                    'created_at',
                    [
                        'header'    => 'User',
                        'attribute' => 'user_id',
                        'format'    => 'raw',
                        'value'     => function (ModelHistoryInterface $history) {
                            return \backend\models\Backend::displayUser($history->getUserId());
                        }
                    ],
                    'action_id',
                    'diffRemove' => [
                        'header' => 'Remove',
                        'format' => 'raw',
                        'value'  => function (ModelHistoryInterface $history) {
                            return '<pre class="jsonHighlight small-text">' . json_encode($history->getSourceResultDiffRemove(), JSON_PRETTY_PRINT) . '</pre>';
                        },
                    ],
                    'diffAdd'    => [
                        'header' => 'Add',
                        'format' => 'raw',
                        'value'  => function (ModelHistoryInterface $history) {
                            return '<pre class="jsonHighlight small-text">' . json_encode($history->getSourceResultDiffAdd(), JSON_PRETTY_PRINT) . '</pre>';
                        },
                    ],
                    'comment'
                ],
            ]
        );
    }
    ?>
</div>

