<?php

use common\models\CompanyService;
use common\models\CompanyServiceCategory;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CompanyServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $serviceTypes \common\models\CompanyServiceType[] */

$this->title = 'Company Services';
$this->params['breadcrumbs'][] = $this->title;

$serviceCategory = new CompanyServiceCategory();
$serviceCategoryList = $serviceCategory->getCategoryTree(CompanyServiceCategory::find()->withoutRoot()->orderBy('lft')->all(), '-');

?>
<div class="text-left">
    <a href="/company/company-service" class="btn btn-default">Clear filters</a>
</div>
<div class="text-right">
    <a href="/company/company-service-category" class="btn btn-primary">Service Categories</a>
</div>

<div class="ps-machine-index">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'rowOptions'   => function ($model, $index, $widget, $grid) {
                if ($model->is_deleted) {
                    return [
                        'style' => 'background: #dfdfdf; color: #8a8a8a!important;'
                    ];
                }

                if ($model->moderator_status == CompanyService::MODERATOR_STATUS_PENDING) {
                    return [
                        'style' => 'background: #abdfff'
                    ];
                }
            },
            'columns'      => [
                [
                    'attribute' => 'id',
                    'format'    => 'raw',
                    'value'     => function ($model) {
                        $url = \yii\helpers\Url::toRoute(['/company/company-service/view', 'id' => $model->id]);
                        $viewButton = Html::a($model->id, $url, ['class' => 'btn btn-primary', 'data-pjax' => 0]);
                        return $viewButton;
                    }
                ],
                'attribute'           => 'created_at',
                [
                    'attribute' => 'type',
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'type',
                        \common\components\ArrayHelper::map($serviceTypes, 'name', 'title'),
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                ],
                [
                    'attribute' => 'category_id',
                    'value'     => function (CompanyService $psMachine) {
                        if (!$psMachine->category_id) {
                            return null;
                        }
                        return $psMachine->category->title;
                    },
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'category_id',
                        \common\components\ArrayHelper::map($serviceCategoryList, 'id', 'title'),
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                ],
                [
                    'attribute' => 'title',
                    'value'     => function (CompanyService $psMachine) {
                        return $psMachine->getTitleLabel();
                    }
                ],

                [
                    'attribute' => 'moderator_status',
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'moderator_status',
                        $searchModel->getModeratorStatusLabels(),
                        ['class' => 'form-control', 'prompt' => 'Any']
                    ),
                ],
                [
                    'attribute' => 'visibility',
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'visibility',
                        $searchModel->getVisibilityLabels(),
                        ['class' => 'form-control', 'prompt' => 'Any']
                    ),
                ],
                [
                    'label'     => 'User',
                    'format'    => 'raw',
                    'filter'    => Html::activeTextInput($searchModel, 'userId', ['class' => 'form-control']),
                    'class'     => \backend\components\columns\UserColumn::class,
                    'attribute' => 'ps.user',
                ],
                [
                    'attribute' => 'ps_id',
                    'header'    => 'Ps',
                    'class'     => \backend\components\columns\PsColumn::class,
                ],
                [
                    'attribute' => 'description',
                    'value'     => function ($model) {
                        return \yii\helpers\StringHelper::truncate(strip_tags($model->description), 100);
                    }
                ],
                [
                    'attribute' => 'country',
                    'header'    => 'Country',
                    'value'     => 'location.country.iso_code',
                ],

                [
                    'attribute' => 'is_deleted',
                    'value'     => function ($model) {
                        return ($model->is_deleted) ? 'Yes' : 'No';
                    },
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'is_deleted',
                        [1 => 'YES', 0 => 'No'],
                        ['class' => 'form-control', 'prompt' => 'All']),
                ],

                'updated_at',
                [
                    'class'    => ActionColumn::class,
                    'template' => '{view}'
                ],
            ],
        ]
    ); ?>
</div>
