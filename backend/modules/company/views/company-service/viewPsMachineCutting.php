<?php

/**
 * @var $model \common\models\CompanyService
 */

use backend\widgets\FilesListWidget;
use common\models\CompanyService;
use common\models\CuttingMachineProcessing;
use common\models\CuttingWorkpieceMaterial;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

$model = $psMachine;
$images = $model->getImages();


$this->params['breadcrumbs']   = [

    ['label' => 'Cutting machines', 'url' => ['/cutting/cutting-machine']]
];
$this->params['breadcrumbs'][] = $this->title;

$workpieceMaterialsProvider = new ActiveDataProvider([
    'query' => $model->company->getCuttingWorkpieceMaterials(),
]);

$processingProvider = new ActiveDataProvider([
    'query' => $model->cuttingMachine->getCuttingMachineProcessings(),
]);

?>
<div class="row">
    <div class="col-md-4">
        <?= $commonDetails ?>
        <?php
        $form = ActiveForm::begin(['action' => '/company/company-service/save-ps-machine?id=' . $psMachine->id]);
        echo $form->field($psMachine, 'is_deleted')->dropDownList([0 => 'notDeleted', 1 => 'deleted']);
        echo $form->field($psMachine, 'visibility')->dropDownList(CompanyService::getVisibilityLabels());
        echo $form->field($psMachine, 'moderator_status')->dropDownList(CompanyService::getModeratorStatusLabels());
        ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-6">
        <h3>Cutting Machine Service details</h3>
        <?= DetailView::widget([
            'model' => $model->cuttingMachine,
            'attributes' => [
                'id',
                'companyService.description',
                [
                    'label' => 'Images',
                    'format' => 'raw',
                    'value' => FilesListWidget::widget(
                        [
                            'filesList' => $images,
                            'rights' => [
                                FilesListWidget::ALLOW_ROTATE
                            ],
                        ]
                    )
                ],
                'max_width',
                'max_length',
                'min_order_price'
            ]]); ?>
    </div>
    <div class="col-md-6">
        <h3>Workpiece materials</h3>
        <?php
        echo GridView::widget(
            [
                'dataProvider' => new ActiveDataProvider(['query' => $model->company->getCuttingWorkpieceMaterials()]),
                'columns'      => [
                    'uuid',
                    [
                        'label' => 'Material',
                        'value' => 'material.title'
                    ],
                    [
                        'label' => 'Color',
                        'value' => 'color.title'
                    ],
                    'width',
                    'height',
                    'thickness',
                    'price',
                ]]); ?>
    </div>
    <div class="col-md-6">
        <h3>Processing rates</h3>
        <?php
        echo GridView::widget(
            [
                'dataProvider' => new ActiveDataProvider(['query' => $model->cuttingMachine->getCuttingMachineProcessings()]),
                'columns'      => [
                    'uuid',
                    [
                        'label' => 'Material',
                        'value' => 'cuttingMaterial.title'
                    ],
                    'thickness',
                    'cutting_price',
                    'engraving_price'
                ]]); ?>
    </div>
    <div class="col-md-6">
        <?=$this->render('deliveryTypes', ['companyService' => $model])?>
    </div>
</div>

