<?php
?>

<h3>Delivery Types</h3>
<table class="table table-bordered">
    <?php use common\models\DeliveryType;
    use yii\widgets\DetailView;

    foreach ($companyService->deliveryTypess as $psDeliveryType): ?>
        <tr>
            <td>
                <div><b><?= H($psDeliveryType->deliveryType->title); ?></b></div>
                <?=
                DetailView::widget([
                    'model'      => $psDeliveryType,
                    'attributes' => [
                        'carrier',
                        [
                            'label'   => 'Shipping price',
                            'value'   => $psDeliveryType->carrier_price . ' ' . $companyService->company->currency,
                            'visible' => $psDeliveryType->carrier != DeliveryType::CARRIER_TS,
                        ],
                        [
                            'label'   => 'Free Delivery from',
                            'value'   => $psDeliveryType->free_delivery . ' ' . $companyService->company->currency,
                            'visible' => $psDeliveryType->free_delivery > 0,
                        ],
                        [
                            'label'   => 'Package Fee',
                            'value'   => $psDeliveryType->packing_price . ' ' . $companyService->company->currency,
                            'visible' => (bool)$psDeliveryType->packing_price && $psDeliveryType->carrier === DeliveryType::CARRIER_TS
                        ],
                        [
                            'label'   => 'Comment',
                            'value'   => $psDeliveryType->comment,
                            'visible' => (bool)$psDeliveryType->comment
                        ],
                    ],
                ]);

                ?>
            </td>
        </tr>
    <?php endforeach ?>
</table>