<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 02.08.17
 * Time: 16:08
 */

use common\models\DeliveryType;
use yii\widgets\DetailView;

$model = $psMachine->psPrinter;
?>
<div class="row">
    <div class="col-md-5">

        <?= $commonDetails ?>

    </div>
    <div class="col-md-7"><a href="/ps/ps-printer/view?id=<?=$psMachine->ps_printer_id;?>&moderate=1" class="btn btn-warning">Moderate Printer</a>

        <h3>3D Print Service details</h3>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'format' => 'raw',
                    'label' => 'PS machine id',
                    'value' => $model->companyService->id
                ],
                [
                    'format' => 'raw',
                    'label' => 'PS',
                    'value' => "<a href='/ps/ps/view?id={$model->ps->id}'>". \H($model->ps->title) . "</a>"
                ],
                [
                    'format' => 'raw',
                    'label' => 'Title',
                    'value' => "<a href='/ps/printer/view?id={$model->printer_id}'>" . \H($model->title) . ' (' . \H($model->printer->title) . ')</a>'
                ],
                'description',
                //'image',
                'price_per_hour:currency',
                //'currency_iso',
                'price_per_volume:currency',
                [
                    'label' => 'Enabled',
                    'value' => $model->companyService->visibility == \common\models\CompanyService::VISIBILITY_EVERYWHERE ? 'Yes' : 'No',
                ],
                'is_online:boolean',
                'user_status',
                'moderator_status',
                [
                    'attribute' => 'test_status',
                    'format' => 'html',
                    'value' => $model->getTestStatusLabel().($model->test ? "<br/><small>{$model->test->decision_at}</small>" : null)
                ],
                [
                    'attribute' => 'build_volume',
                    'value' => implode(" x ", $model->getBuildVolume()) . ' mm.'
                ],
                [
                    'attribute' => 'min_order_price',
                    'value' => ($model->min_order_price ?: 0).' '.$model->companyService->company->currency,
                ],
                'created_at',
                'updated_at'
            ],
        ]) ?>

        <h3>Materials and colors</h3>
        <table class="table table-striped table-bordered detail-view">
            <?php foreach($model->materials as $psMaterial): ?>
                <tr>
                    <td width="30%">
                        <?= "{$psMaterial->material->filament_title} ({$psMaterial->material->title})" ?>
                    </td>
                    <td>
                        <?php foreach($psMaterial->colors as $psColor):?>
                            <div class="row oneColorRow">
                                <div class="col-md-3">
                                    <div class="color-one-block__color-title"><?=$psColor->id;?>. <?= $psColor->color->title; ?></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="color-one-block">
                                        <div class="added-colors-div"></div>
                                        <div style="background-color: rgb(<?= $psColor->color->rgb; ?>);" class="form-control"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-control">
                                        <?= displayAsMoney($psColor->getPriceUsdPerGr()); ?> per gr. / $
                                        <?= displayAsMoney($psColor->getPriceUsdPerMl()); ?> per ml. / $
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <?=$this->render('deliveryTypes', ['companyService' => $model->companyService]) ?>
    </div>
</div>

