<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 06.09.2018
 * Time: 12:26
 */
use yii\grid\GridView;
use yii\widgets\DetailView;
$csWindow = $psMachine->csWindow;
?>
<h2>Window Service Manufacturing</h2>

<div class="row">

    <div class="col-lg-3">

        <?= $csWindow ? DetailView::widget([
            'model' => $csWindow,
            'attributes' => [
                'uid',
                'measurement',
                'windowsill',
                'lamination',
                'slopes',
                'tinting',
                'energy_saver',
                'installation',
                'created_at:datetime',
                'updated_at:datetime',
            ],
        ]): ''; ?>

        <h3>Common service information</h3>
        <?= $commonForm ?>
        <?= DetailView::widget([
            'model' => $psMachine,
            'attributes' => [
                'id',
                'moderator_status',
                'ps_id',
                'is_deleted',
                'visibility',
                'moderator_status',
            ],
        ]) ?>

        <hr />
        <?php if($csWindow): ?>
        <h4>Location</h4>
        <?= GridView::widget([
            'dataProvider' => \common\models\CsWindowLocation::getDataProvider(['cs_window_uid'=>$csWindow->uid]),
            'columns' => [
                'id',
                'location_id',
                'radius',
            ],
        ]); ?>
        <?php endif; ?>
    </div>

    <div class="col-lg-9">
        <?php if($csWindow): ?>
        <h4>Profile</h4>
        <?= GridView::widget([
            'dataProvider' => \common\models\CsWindowProfile::getDataProvider(['cs_window_uid'=>$csWindow->uid]),
            'columns' => [
                'id',
                'updated_at:datetime',
                'title',
                'thickness',
                 'chambers',
                 'max_glass',
                 'max_width',
                 'max_height',
                 'noise_reduction',
                 'thermal_resistance',
                 'price',

            ],
        ]); ?>

        <h4>Glass</h4>
        <?= GridView::widget([
            'dataProvider' => \common\models\CsWindowGlass::getDataProvider(['cs_window_uid'=>$csWindow->uid]),
            'columns' => [
                'id',
                'updated_at:datetime',
                'title',
                'thickness',
                 'chambers',
                 'noise_reduction',
                 'thermal_resistance',
                 'price',
            ],
        ]); ?>
        <h4>Furniture</h4>
        <?= GridView::widget([
            'dataProvider' => \common\models\CsWindowFurniture::getDataProvider(['cs_window_uid'=>$csWindow->uid], 10, ['sort' => ['defaultOrder' => ['id'=>SORT_DESC]]]),
            'columns' => [
                'id',
                'updated_at:datetime',
                'title',
                'price',
                'price_swivel',
                 'price_folding',
            ],
        ]); ?>

        <h4>History</h4>
            <?= GridView::widget([
                'dataProvider' => \common\models\CsWindowHistory::getDataProvider(['cs_window_id'=>$csWindow->id], 30, ['sort' => ['defaultOrder' => ['id'=>SORT_DESC]]]),
                'columns' => [
                    'id',
                    [
                      'attribute'  =>     'user',
                        'class'     => \backend\components\columns\UserColumn::class,
                    ],
                    'created_at:datetime',
                    'action_id',
                     'comment:ntext',
                     'data:ntext',
                ],
            ]); ?>
        <?php endif; ?>
    </div>
</div>