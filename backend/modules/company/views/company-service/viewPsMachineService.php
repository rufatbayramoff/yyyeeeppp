<?php

/**
 * @var $model \common\models\CompanyService
 */

use backend\widgets\FilesListWidget;

$model = $psMachine;
$images = $model->getImages();
?>
<div class="row">
    <div class="col-md-5">
        <?= $commonDetails ?>
        <?= $commonForm ?>
    </div>
    <div class="col-md-5">
<h2>Description</h2>
        <?=$model->description; ?>

        <h2>Portfolio images</h2>

        <?php echo FilesListWidget::widget(
            [
                'filesList'              => $images,
                'rights'                 => [
                    FilesListWidget::ALLOW_ROTATE
                ],
            ]
        ) ?>


    </div>
</div>

