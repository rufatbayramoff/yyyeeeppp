<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlock */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-block-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'content:ntext',
            'created_at',
            'is_visible',
            'position',
            'company_id',
        ],
    ]) ?>

    <div class="m-t10">
        <?=$model->getImages()?
            \frontend\widgets\SwipeGalleryWidget::widget([
                'files' => $model->getImages(),
                'thumbSize' => [230, 180],
                'containerOptions' => ['class' => 'picture-slider'],
                'itemOptions' => ['class' => 'picture-slider__item'],
                'scrollbarOptions' => ['class' => 'picture-slider__scrollbar'],
            ]):'';
        ?>
    </div>

    <?php if($model->videos): ?>
        Videos: <?=\yii\helpers\Json::encode($model->videos); ?>
    <?php endif; ?>

</div>
