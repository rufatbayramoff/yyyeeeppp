<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlock */

$this->title = 'Update Company Block: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-block-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
