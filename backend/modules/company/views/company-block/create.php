<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlock */

$this->title = 'Create Company Block';
$this->params['breadcrumbs'][] = ['label' => 'Company Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-block-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
