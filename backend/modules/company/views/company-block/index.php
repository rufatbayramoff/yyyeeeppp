<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CompanyBlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Company Blocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-block-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'options' => ['style'=>'min-height:500px!important;', 'class'=>'grid-view table-responsive'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'header' => 'Company',
                'attribute' => 'company_id',
                'format' => 'raw',
                'class' => \backend\components\columns\PsColumn::class,
            ],
            'title',
            'created_at',
            'is_visible:boolean',
            // 'videos',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}'],
        ],
    ]); ?>
</div>
