<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlock */
/* @var $form yii\widgets\ActiveForm */

$model->videos = \yii\helpers\Json::encode($model->videos);
?>

<div class="company-block-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(),
        \backend\components\CKEditorDefault::getDefaults()) ?>

    <?= $form->field($model, 'is_visible')->checkbox() ?>

    <?php if (false): ?>
        <?= $form->field($model, 'position')->textInput() ?>
        <?= $form->field($model, 'company_id')->textInput() ?>
    <?php endif; ?>
 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <h3>Images</h3>
    <div class="m-t10">
        <?=$model->getImages()?
            \frontend\widgets\SwipeGalleryWidget::widget([
                'files' => $model->getImages(),
                'thumbSize' => [230, 180],
                'containerOptions' => ['class' => 'picture-slider'],
                'itemOptions' => ['class' => 'picture-slider__item'],
                'scrollbarOptions' => ['class' => 'picture-slider__scrollbar'],
            ]):'';
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
