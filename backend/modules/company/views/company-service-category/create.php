<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceCategory */

$this->title = 'Create  Category';
$this->params['breadcrumbs'][] = ['label' => ' Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
