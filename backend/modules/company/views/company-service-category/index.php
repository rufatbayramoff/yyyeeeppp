<?php

use common\models\CompanyServiceCategory;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
$categories = CompanyServiceCategory::find()->withoutRoot()->orderBy('lft')->all();

function printOption($data)
{
    $model =  new CompanyServiceCategory();
    $result = $model->getCategoryTree($data);
    foreach ($result as $k => $category) {
        echo sprintf("<p>%s</p>", $category['space'] . $category['title']);
    }
}

?>
<div class="company-service-category-index">
    <p>
        <?= Html::a('Create  Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                [
                    'label'  => 'Parent',
                    'format' => 'raw',
                    'value'  => function (CompanyServiceCategory $category) {
                        return $category->parentCategory?$category->parentCategory->getTitleWithCode():'';
                    }
                ],
                'code',
                'slug',
                'title',
                'is_visible:boolean',
                'is_active:boolean',
                'position',
                [
                    'class'          => ActionColumn::class,
                    'buttons'        => Yii::$app->getModule('translation')->dbI18n->getGridButton('company_service_category'),
                    'template'       => '{translate} &nbsp; {view} &nbsp; {update} &nbsp; {delete} ',
                    'contentOptions' => ['style' => 'width: 100px;']
                ]
            ],
        ]
    ); ?>
</div>
<hr />
<div class="">
    <?=printOption($categories);?>
</div>

