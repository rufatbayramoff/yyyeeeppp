<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $companyServiceCategory common\models\CompanyServiceCategory */

$this->title = $companyServiceCategory->title;
$this->params['breadcrumbs'][] = ['label' => ' Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $companyServiceCategory->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(
            'Delete',
            ['delete', 'id' => $companyServiceCategory->id],
            [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ]
        ) ?>
    </p>

    <?= DetailView::widget(
        [
            'model'      => $companyServiceCategory,
            'attributes' => [
                'id',
                'lft',
                'rgt',
                'depth',
                [
                    'label'  => 'Parent',
                    'format' => 'raw',
                    'value'  => $companyServiceCategory->parentCategory->getTitleWithCode()
                ],
                'code',
                'slug',
                'title',
                'description',
            ],
        ]
    ) ?>

</div>
