<?php

use common\components\ArrayHelper;
use common\models\CompanyServiceCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceCategory */
/* @var $form yii\widgets\ActiveForm */

$categories = CompanyServiceCategory::find()->withoutRoot()->orderBy('lft')->all();


function printOption($form, $data)
{
    $model =  new CompanyServiceCategory();
    $model->id = $form->parentCategoryId;
    $result = $model->getCategoryTree($data);
    foreach ($result as $k => $category) {
        echo sprintf("<option value='%d' %s>%s</option>", $category['id'], $category['selected'], $category['space'] . $category['title']);
    }
}

?>

<div class="company-service-category-form">

    <?php $form = ActiveForm::begin(); ?>
<div clss="form-group">
    <label>Parent category:</label>
    <select class="form-control" name="CompanyServiceCategory[parentCategoryId]">
        <option value="1">Root</option>
        <?= printOption($model, $categories); ?>
    </select>
</div>
    <?php # $form->field($model, 'parentCategoryId')->dropDownList(ArrayHelper::map(CompanyServiceCategory::find()->all(), 'id', 'titleWithCode')) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'is_active')->checkbox() ?>
    <?= $form->field($model, 'is_visible')->checkbox() ?>
    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
