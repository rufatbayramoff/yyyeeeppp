<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceCategory */

$this->title = 'Update  Category: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => ' Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-service-category-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
