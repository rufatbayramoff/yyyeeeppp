<?php
/**
 * User: nabi
 */

namespace backend\modules\company;

use yii\base\Module;

class CompanyModule extends Module
{
    public $controllerNamespace = 'backend\modules\company\controllers';

}