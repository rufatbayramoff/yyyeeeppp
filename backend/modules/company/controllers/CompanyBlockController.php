<?php
/**
 * User: nabi
 */

namespace backend\modules\company\controllers;


use backend\components\CrudController;
use yii\helpers\Json;

class CompanyBlockController extends CrudController
{
    public $accessGroup = 'company';
    public function init()
    {
        parent::init();
        $this->searchModel = new \backend\models\search\CompanyBlockSearch();
        $this->mainModel = new \common\models\CompanyBlock();
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdate']);

    }

    public function beforeUpdate($event)
    {
        if($event->sender->videos){
            $event->sender->videos = Json::decode($event->sender->videos);
        }

    }
}