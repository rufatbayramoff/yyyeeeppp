<?php
/**
 * User: nabi
 */

namespace backend\modules\company\controllers;


use backend\components\AdminController;
use backend\models\search\PsSearch;
use backend\modules\company\models\CompanyEditForm;
use backend\modules\company\models\UserCompanyForm;
use backend\modules\company\services\CompanyService;
use backend\modules\company\services\CompanyUserService;
use common\components\exceptions\BusinessException;
use common\models\Company;
use common\models\Ps;
use common\models\repositories\CompanyRepository;
use Yii;
use yii\base\UserException;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

class CompanyController extends AdminController
{

    /**
     * @var CompanyService
     */
    public $companyService;

    /**
     * @var  CompanyRepository
     */
    public $companyRepository;


    /**
     * @var companyUserService
     */
    private $companyUserService;

    public $searchModel;

    public function injectDependencies(CompanyService $companyService, CompanyUserService $companyUserService, CompanyRepository $companyRepository)
    {
        $this->companyService = $companyService;
        $this->companyUserService = $companyUserService;
        $this->companyRepository = $companyRepository;
        $searchModel = new PsSearch();
        $this->searchModel = $searchModel;
    }

    /**
     * Lists all Ps models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = $this->searchModel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['is_backend' => 1]);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single Ps model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new Ps model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\UserException
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new CompanyEditForm();
        $user = new UserCompanyForm();
        if (app('request')->isPost) {
            $model->load(Yii::$app->request->post());
            $user->load(Yii::$app->request->post());
            try {
                $model = $this->companyService->createCompany($model);
                $user = $this->companyUserService->createUserForCompany($user, $model);
                if ($user->validate(null, false) && $model->validate(null, false)) {
                    $user = $this->companyUserService->saveUserForCompany($user);
                    $this->companyService->saveCompany($model, $user);
                } else {
                    throw new BusinessException(Html::errorSummary([$model, $user]));
                }
            } catch (\Exception $e) {
                $this->setFlashMsg(false, $e->getMessage());
                return $this->render(
                    'create',
                    [
                        'model' => $model,
                        'user'  => $user,
                    ]
                );
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $user = new UserCompanyForm();
        return $this->render(
            'create',
            [
                'model' => $model,
                'user'  => $user,
            ]
        );

    }

    /**
     * Updates an existing Ps model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws UserException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = UserCompanyForm::tryFindByPk($model->user_id);
        if ($model->load(Yii::$app->request->post()) ) {
            try {
                $user->load(Yii::$app->request->post());
                if ($user->validate(null, false) && $model->validate(null, false)) {
                    $this->companyUserService->saveUserForCompany($user);
                    $this->companyService->saveCompany($model, $user);
                    $this->setFlashMsg(true, 'Saved');
                } else {
                    throw new BusinessException(Html::errorSummary([$model, $user]));
                }
            } catch (\Exception $e) {
                $this->setFlashMsg(false, $e->getMessage());
            }
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render(
            'update',
            [
                'model' => $model,
                'user' => $user,
            ]
        );
    }

    /**
     * Deletes an existing Ps model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $company = Company::tryFindByPk($id);
        if (!$company->is_backend) {
            throw new BusinessException('Delete only unconfirmed companies');
        }
        $this->companyRepository->delete($company);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Ps model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Ps the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyEditForm::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionList($q = null, $id = null, $k = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $kCol = 'id';
        $vCol = 'name';
        $columns = [];
        if (!empty($k)) {
            [$kCol, $vCol] = explode(",", $k);
            $columns = [$kCol, $vCol];
        }
        $tableName = $this->searchModel->tableName();
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query;
            $search = $vCol . ' LIKE "' . $q . '%"';
            if (is_numeric($q)) {
                $search = $kCol . "=" . intval($q);
            }
            $query->select(implode(",", $columns) . " AS text")
                ->from($tableName)
                ->where($search)
                ->limit(30);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }
}