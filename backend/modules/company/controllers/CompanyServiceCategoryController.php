<?php


namespace backend\modules\company\controllers;

use backend\components\AdminAccess;
use backend\models\search\CompanyServiceCategorySearch;
use common\models\CompanyServiceCategory;
use common\models\repositories\CompanyServiceCategoryRepository;
use Yii;
use backend\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * User: nabi
 */
class CompanyServiceCategoryController extends AdminController
{
    /**
     * @var CompanyServiceCategoryRepository
     */
    public $companyServiceCategoryRepository;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        AdminAccess::validateAccess('company.category');
        return parent::beforeAction($action);
    }


    /**
     * @param CompanyServiceCategoryRepository $companyServiceCategoryRepository
     */
    public function injectDependencies(CompanyServiceCategoryRepository $companyServiceCategoryRepository)
    {
        $this->companyServiceCategoryRepository = $companyServiceCategoryRepository;
    }

    /**
     * Lists all  models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyServiceCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single  model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'companyServiceCategory' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new  model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $category = new CompanyServiceCategory();

        if ($category->load(Yii::$app->request->post()) && $category->validate()) {
            try {
                $this->companyServiceCategoryRepository->save($category);

            } catch (\Exception $e) {
                $this->setFlashMsg(false, $e->getMessage());
            }
            return $this->redirect(['/company/company-service-category']);
        } else {
            return $this->render(
                'create',
                [
                    'model' => $category,
                ]
            );
        }
    }

    /**
     * Updates an existing  model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $category = $this->findModel($id);

        if ($category->load(Yii::$app->request->post()) && $category->validate()) {

            try {
                $this->companyServiceCategoryRepository->save($category);
                $this->setFlashMsg(true, 'Updated');
            } catch (\Exception $e) {
                $this->setFlashMsg(false, $e->getMessage());
            }
            return $this->redirect(\Yii::$app->request->referrer);
        } else {
            return $this->render(
                'update',
                [
                    'model' => $category,
                ]
            );
        }
    }

    /**
     * Deletes an existing   model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the   model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return CompanyServiceCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyServiceCategory::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
