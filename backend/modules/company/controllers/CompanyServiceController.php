<?php


namespace backend\modules\company\controllers;


use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\company\CompanyServiceRejectForm;
use backend\models\search\PsCncMachineHistorySearch;
use backend\models\search\CompanyServiceSearch;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\models\populators\PsCncMachinePopulator;
use common\models\CompanyService;
use common\modules\cnc\repositories\PsCncMachineRepository;
use common\modules\company\repositories\CompanyServiceTypesRepository;
use common\modules\company\services\CompanyServicesService;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyInformer;
use common\modules\informer\models\CompanyServiceInformer;
use common\traits\TransactedControllerTrait;
use kartik\base\TranslationTrait;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * User: nabi
 */

class CompanyServiceController extends AdminController
{
    use TransactedControllerTrait;
    /**
     * @var PsCncMachineRepository
     */
    protected $psCncMachineRepository;

    /**
     * @var PsCncMachinePopulator
     */
    protected $psCncMachinePopulator;

    /**
     * @var CompanyServiceTypesRepository
     */
    private $serviceTypesRepository;

    /**
     * @var CompanyServicesService
     */
    private $companyServicesService;

    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @param PsCncMachineRepository $psCncMachineRepository
     * @param PsCncMachinePopulator $psCncMachinePopulator
     * @param CompanyServiceTypesRepository $serviceTypesRepository
     * @param CompanyServicesService $companyServicesService
     * @param Emailer $emailer
     */
    public function injectDependencies(
        PsCncMachineRepository $psCncMachineRepository,
        PsCncMachinePopulator $psCncMachinePopulator,
        CompanyServiceTypesRepository $serviceTypesRepository,
        CompanyServicesService $companyServicesService,
        Emailer $emailer
    ) {
        $this->psCncMachineRepository = $psCncMachineRepository;
        $this->psCncMachinePopulator = $psCncMachinePopulator;
        $this->serviceTypesRepository = $serviceTypesRepository;
        $this->companyServicesService = $companyServicesService;
        $this->emailer = $emailer;
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete'          => ['POST'],
                    'save-cnc-schema' => ['POST'],
                    'save-ps-machine' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all PsMachine models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('psmachine.view');
        $searchModel = new CompanyServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      #  $dataProvider->query->andWhere([CompanyService::column('type')=>CompanyService::MODERATOR_SERVER_TYPES]);
        $serviceTypes = $this->serviceTypesRepository->getAll();
        return $this->render(
            'index',
            [
                'serviceTypes' => $serviceTypes,
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\UserException
     */
    public function actionApprove($id)
    {
        AdminAccess::validateAccess('company_service.approve');
        $companyService = $this->findModel($id);
        $this->companyServicesService->moderatorServiceApprove($companyService);
        $this->setFlashMsg(true, 'Service approved!');
        InformerModule::addInformer($companyService->getOwner(), CompanyServiceInformer::class);

        return $this->redirect(app('request')->referrer);
    }

    /**
     *
     * @transacted
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\UserException
     */
    public function actionReject($id)
    {
        AdminAccess::validateAccess('company_service.reject');
        $model = $this->findModel($id);
        $rejectForm = new CompanyServiceRejectForm();
        $currentUser = $this->getCurrentUser();

        $post = app('request')->post();
        if ($post) {
            $rejectForm->load($post);
            $rejectForm->setUser($currentUser);
            $this->companyServicesService->moderatorServiceReject($model, $rejectForm);
            app('cache')->set('back.notify.data', '');
            InformerModule::addInformer($model->getOwner(), CompanyServiceInformer::class);
            return $this->jsonSuccess(['reload' => true]);
        }

        return $this->renderAdaptive(
            '@backend/views/ps/ps/reject.php',
            [
                'model'      => $model,
                'rejectForm' => $rejectForm
            ]
        );
    }


    /**
     * Displays a single PsMachine model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess('psmachine.view');
        $psMachine = $this->findModel($id);

        $historySearch = new PsCncMachineHistorySearch();
        $historySearch->ps_machine_id = $psMachine->id;
        if ($psMachine->type == CompanyService::TYPE_CNC) {
            $historySearch->ps_cnc_machine_id = $psMachine->ps_cnc_machine_id;
            $historyProvider = $historySearch->search(Yii::$app->request->queryParams);
        } else {
            $historyProvider = $historySearch->search(Yii::$app->request->queryParams);
        }

        return $this->render(
            'view',
            [
                'model'           => $psMachine,
                'historySearch'   => $historySearch,
                'historyProvider' => $historyProvider,
            ]
        );
    }

    public function actionSavePsMachine($id)
    {
        AdminAccess::validateAccess('psmachine.moderate');
        $psMachine = $this->findModel($id);
        $postParams = Yii::$app->request->post();
        $psMachine->load($postParams);
        $psMachine->safeSave();
        $this->setFlashMsg(true, 'Ps machine info saved.');
        $this->redirect('view?id=' . $psMachine->id);
    }


    /**
     * Updates an existing PsMachine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionSaveCncSchema($psMachineId)
    {
        AdminAccess::validateAccess('psmachine.moderate');
        $post = Yii::$app->request->getBodyParams();
        $psMachine = CompanyService::tryFindByPk($psMachineId);
        $psCncMachine = $this->psCncMachineRepository->getByPsMachine($psMachine);
        $this->psCncMachinePopulator->populate($psCncMachine, $post);
        $this->psCncMachineRepository->save($psCncMachine);

        $resultJson = Yii::$app->getModule('cnc')->psCncJsonGenerator->generateJsonServiceInfo($psCncMachine->psCnc);
        $this->setFlashMsg(true, 'Schema saved.');
        return $this->jsonReturn($resultJson);
    }

    /**
     * @param $psMachineId
     * @throws NotFoundHttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\UserException
     */
    public function actionApproveMachine($psMachineId)
    {
        AdminAccess::validateAccess('psmachine.moderate');

        $psMachine = CompanyService::tryFindByPk($psMachineId);
        $psMachine->moderator_status = CompanyService::MODERATOR_STATUS_APPROVED;
        AssertHelper::assertSave($psMachine);
        InformerModule::addInformer($psMachine->getOwner(), CompanyServiceInformer::class);

        $this->emailer->sendPsServiceApproved($psMachine);
        $this->setFlashMsg(true, 'Machine successful approved.');
        $this->redirect('view?id=' . $psMachine->id);
    }

    /**
     * Finds the PsMachine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return CompanyService the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyService::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
