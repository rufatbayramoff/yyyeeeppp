<?php
/**
 * User: nabi
 */

namespace backend\modules\company\services;


use backend\modules\company\models\CompanyEditForm;
use common\components\DateHelper;
use common\models\factories\FileFactory;
use common\models\Ps;
use common\models\repositories\FileRepository;
use common\models\User;
use libphonenumber\PhoneNumberUtil;
use yii\base\BaseObject;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

class CompanyService extends BaseObject
{

    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @var FileRepository
     */
    private $fileRepository;

    public function injectDependencies(FileFactory $fileFactory, FileRepository $fileRepo)
    {
        $this->fileFactory = $fileFactory;
        $this->fileRepository = $fileRepo;
    }

    /**
     * @param Ps $ps
     * @return Ps
     * @throws \libphonenumber\NumberParseException
     */
    public function createCompany(Ps $ps)
    {
        $ps->user_id = 0;
        $ps->moderator_status = Ps::MSTATUS_DRAFT;
        $ps->is_backend = 1;
        $ps->created_at = DateHelper::now();
        $ps->updated_at = DateHelper::now();
        $ps->phone_status = Ps::PHONE_STATUS_NEW;
        self::processUrl($ps);
        if (empty($ps->country_id)) {
            $ps->addError('country_id', 'Country is required');
            return $ps;
        }
        if(!empty($ps->phone)){
            $this->processPhone($ps);
        }
        return $ps;
    }

    /**
     * @param Ps $model
     * @param User $user
     * @return Ps
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function saveCompany(CompanyEditForm $model, User $user)
    {
        if ($model->deleteLogoFile) {
            $logoFile = $model->logoFile;
            $model->logo_file_id = null;
            $model->safeSave();
            $this->fileRepository->delete($logoFile);
        }
        $uploadFile = UploadedFile::getInstance($model, 'logoFileUpload');
        if ($uploadFile) {
            $file = $this->fileFactory->createFileFromUploadedFile($uploadFile);
            $file->setPublicMode(1);
            $this->fileRepository->save($file);
            $model->logo_file_id = $file->id;
        }
        $model->user_id = $user->id;
        $model->safeSave();
        return $model;
    }

    /**
     * @param Ps $ps
     * @return Ps
     */
    public static function processUrl(Ps $ps)
    {
        $url = Inflector::slug($ps->title);
        $i = 0;
        while (Ps::find()->where(['url' => $url])->exists()) {
            $url .= $i;
        }
        $ps->url = $url;
        $ps->url_changes_count = 0;
        return $ps;
    }

    /**
     * @param Ps $ps
     * @throws \libphonenumber\NumberParseException
     */
    public function processPhone(Ps $ps)
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        if (!$ps->country) {
            $ps->addError('phone', 'Country iso required if phone number specified');
            return;
        }
        $numberObj = $phoneUtil->parse($ps->phone, $ps->country->iso_code);
        if (!$phoneUtil->isValidNumber($numberObj)) {
            $ps->addError('phone', 'Invalid phone number');
        }
        $ps->phone_country_iso = $ps->country->iso_code;
        $ps->phone_code = (string)$numberObj->getCountryCode();
        $ps->phone = $numberObj->getNationalNumber();
    }
}