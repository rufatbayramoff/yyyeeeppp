<?php
/**
 * User: nabi
 */

namespace backend\modules\company\services;


use common\models\Ps;
use common\models\User;
use common\models\UserProfile;
use lib\money\Currency;
use yii\base\UserException;
use yii\helpers\Html;

class CompanyUserService
{

    /**
     * @param User $user
     * @return User
     * @throws \yii\base\Exception
     */
    public function saveUserForCompany(User $user)
    {
        $user->safeSave();
        if (!$user->userProfile) {
            $userProfile = new UserProfile();
            $userProfile->user_id = $user->id;
            $userProfile->current_lang = 'en-US';
            $userProfile->current_currency_iso = Currency::USD;
            $userProfile->timezone_id = 'America/Los_Angeles';
            if (!$userProfile->validate()) {
                $user->addError('email', Html::errorSummary($userProfile));
            }
            $userProfile->safeSave();
        }
        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function createUserForCompany(User $user, Ps $company)
    {
        if (empty($user->email)) {
            $user->addError('email', 'Email is required');
        }
        if ($userExists = User::findOne(['email' => $user->email])) {
            if ($user->status === User::STATUS_DRAFT_COMPANY) {
                return $userExists;
            }
            $user->addError('email', 'User with such email already exists');
        }
        $user->status = User::STATUS_DRAFT_COMPANY;
        $user->username = $company->url . '-' . $user->uid;
        $user->auth_key = '-';
        $user->password_hash = '-';
        $user->created_at = time();
        $user->updated_at = time();
        $user->lastlogin_at = 1;
        return $user;
    }
}