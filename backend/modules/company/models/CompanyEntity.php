<?php
/**
 * User: nabi
 */

namespace backend\modules\company\models;


class CompanyEntity
{
    public $id;
    public $url;
    public $logo;
    public $images;
    public $services;
    public $products;
    public $email;
}