<?php

use common\models\ProductCategory;
use common\models\ProductCategoryKeyword;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \common\models\search\ProductCategoryKeywordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Product Category keywords';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-category-index">

    <p>
        <?= Html::a('Create Category Keyword', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            'category' => [
                'label' => 'Category',
                'attribute' => 'category',
                'format' => 'raw',
                'value' => function (ProductCategoryKeyword $productCategoryKeyword) {
                    return $productCategoryKeyword->productCategory->getFullTitle(true);
                }
            ],
            'keyword',
            'is_active',
            [
                'class'          => ActionColumn::class,
                'template'       => '{update} &nbsp; {delete} ',
                'contentOptions' => ['style' => 'width: 80px;']
            ]
        ],
    ]); ?>
</div>
