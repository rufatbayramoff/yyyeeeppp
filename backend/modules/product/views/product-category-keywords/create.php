<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $productCategoryKeyword common\models\ProductCategory */

$this->title = 'Create Product Category Keyword';
$this->params['breadcrumbs'][] = ['label' => 'Product Category Keywords', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-category-create">

    <?= $this->render('_form', [
        'model' => $productCategoryKeyword,
    ]) ?>

</div>
