<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $productCategoryKeyword common\models\ProductCategoryKeyword */

$this->title = 'Update Product Category Keyword: ' . $productCategoryKeyword->keyword;
$this->params['breadcrumbs'][] = ['label' => 'Product Category Keywords', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-category-update">

    <?= $this->render('_form', [
        'model' => $productCategoryKeyword,
    ]) ?>

</div>
