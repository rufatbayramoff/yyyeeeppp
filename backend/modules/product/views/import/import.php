<?php

/* @var $this \yii\web\View */
/* @var $model ImportProduct */

use backend\modules\product\import\ImportProduct;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$model->importPolicy = ImportProduct::IMPORT_POLICY_ADD;
?>


<div class="product-category-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action'=>'/product/import/upload']) ?>


    <div class="row">


        <div class="col-lg-6">
    <?= $form->field($model, 'importFile')->fileInput() ?>
    <?php
    echo $form->field($model, 'company_id')->widget(kartik\select2\Select2::classname(), [
        'options' => ['placeholder' => 'Search'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/company/company/list']),
                'dataType' => 'json',
                'data' => new yii\web\JsExpression('function(params) { return {q:params.term, page:params.page, k:"id,title"}; }')
            ],
        ],
    ]);
    ?>
    <?= $form->field($model, 'importPolicy')->dropDownList([ImportProduct::IMPORT_POLICY_ADD => 'Add', ImportProduct::IMPORT_POLICY_UPDATE => 'update'], ['prompt' => '']) ?>


            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <a href="<?=\yii\helpers\Url::toRoute(['download-sample']);?>" class="btn btn-info">Download Sample file</a>
        </div>
        <div class="col-lg-6">
            <?= Html::submitButton('IMPORT', ['class' => 'btn btn-primary']) ?>

        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
