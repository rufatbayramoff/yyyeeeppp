<?php
/**
 * User: nabi
 */

use yii\bootstrap\ActiveForm;use yii\helpers\Html;

/* @var $importProduct \backend\modules\product\import\ImportProduct */
/* @var $this \yii\web\View */

$this->registerJsFile('/js/jquery-sortable-min.js');
$this->title = 'Import';

$product = new \common\models\Product();
$noImportFields = ['id', 'created_at', 'updated_at', 'company_id'];

echo $importProduct->filePath;


?>
<p>
    <?= Html::a('Return To Import', ['/product/import'], ['class' => 'btn btn-success']) ?>
</p>
<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action'=>'/product/import/update-step?session='.$session, 'layout' => 'inline']) ?>
        Column Index <?= $form->field($importProduct, 'columnIndex')->textInput(['maxlength' => true]) ?>
        <?= Html::submitButton('Set', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <h3>Map Columns to import</h3>
        <div class="row">
            <div class="col-lg-6">
                <h4>Columns from Excel</h4>
                <ol class="simple_with_drop vertical">
                    <?php foreach($importColumns as $col): ?>
                    <li>
                        <i class="icon-move"></i>
                        <?=$col;?>
                    </li>
                    <?php endforeach; ?>
                </ol>
            </div>
            <div class="col-lg-6">
                <h4>In Database</h4>
                <ol class="simple_with_drop vertical">
                    <?php foreach ($importProduct->getImportAttributes() as $k => $attributeLabel):
                                if(in_array($k, $noImportFields)){
                                    continue;
                                }
                        ?>
                        <li>
                            <i class="icon-move"></i>
                            <?=$attributeLabel;?>
                        </li>
                    <?php endforeach; ?>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs('$("ol.simple_with_drop").sortable({
});');
?>
<style>
    body.dragging, body.dragging * {
        cursor: move !important; }

    /* line 4, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    .dragged {
        position: absolute;
        top: 0;
        opacity: 0.5;
        z-index: 2000; }

    /* line 10, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical {
        margin: 0 0 9px 0;
        min-height: 10px; }
    /* line 13, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical li {
        display: block;
        margin: 5px;
        padding: 5px;
        border: 1px solid #cccccc;
        color: #0088cc;
        background: #eeeeee; }
    /* line 20, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical li.placeholder {
        position: relative;
        margin: 0;
        padding: 0;
        border: none; }
    /* line 25, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical li.placeholder:before {
        position: absolute;
        content: "";
        width: 0;
        height: 0;
        margin-top: -5px;
        left: -5px;
        top: -4px;
        border: 5px solid transparent;
        border-left-color: red;
        border-right: none; }
</style>
