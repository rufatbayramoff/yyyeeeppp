<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductMainSliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Main Sliders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-main-slider-index">

    <p>
        <?= Html::a('Create Product Main Slider', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'url',
            'file_uuid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
