<?php

use backend\widgets\FilesListWidget;
use common\components\FileTypesHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\base\ProductMainSlider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-main-slider-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">File</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'formPrefix' => $model->formName(),
                    'formAttribute' => 'image',
                    'emptyText' => 'add file',
                    'filesList' => $model->file ? [$model->file] : [],
                    'rights' => [
                        FilesListWidget::ALLOW_DELETE,
                        FilesListWidget::ALLOW_ADD,
                        FilesListWidget::ALLOW_MULTIPLE,
                        FilesListWidget::ALLOW_ROTATE
                    ],
                    'filesAllowedExtensions' => FileTypesHelper::ALLOW_IMAGES_EXTENSIONS
                ]
            ) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
