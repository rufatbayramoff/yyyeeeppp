<?php

use common\models\ProductMainSlider;
use frontend\components\image\ImageHtmlHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\base\ProductMainSlider */

$this->title                   = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Product Main Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-main-slider-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'title',
            'url',
            'file' => [
                'label'  => 'File',
                'format' => 'raw',
                'value'  => function (ProductMainSlider $model) {
                    return '<img src="' . ImageHtmlHelper::getThumbUrlForFile($model->file) . '">';
                }
            ],
        ],
    ]) ?>

</div>
