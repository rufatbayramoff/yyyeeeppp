<?php

use common\models\ProductCategory;
use common\models\ProductCategoryKeyword;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $productCategory ProductCategory */


?>
<div class="form-group field-category-keywords-comment">
    <label class="control-label">Keywords</label>
    <div>
        <?= GridView::widget(
            [
                'dataProvider' => $dataProvider = new ActiveDataProvider(['query' => $productCategory->getProductCategoryKeywords()]),
                'columns'      => [
                    'keyword',
                    [
                        'class'   => ActionColumn::class,
                        'buttons' =>
                                [
                                    'update' => function ($url, ProductCategoryKeyword $categoryKeyword) {
                                        $urlRes = Html::a(
                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                            '/product/product-category/keyword-update-popup?id=' . $categoryKeyword->id,
                                            [
                                                'class'       => 'btn-ajax-modal',
                                                'title'       => 'Update',
                                                'data-target' => '#addKeywordPopup',
                                                'value'       => '/product/product-category/keyword-form-popup?id=' . $categoryKeyword->id,
                                            ]
                                        );
                                        return $urlRes;
                                    },
                                    'delete' => function ($url, $model) {
                                        $urlRes = Html::a(
                                            '<span class="glyphicon glyphicon-trash"></span>',
                                            '/product/product-category/keyword-delete?id=' . $model->id,
                                            [
                                                'title'        => 'Delete',
                                                'data-confirm' => 'Are you sure you want to delete this item?',
                                                'data-method'  => 'post'
                                            ]
                                        );
                                        return $urlRes;
                                    }
                                ],
                        'template' => '{update} &nbsp; {delete} ',
                    ]
                ]
            ]
        );
        ?>
        <?= Html::button(
            'Add Keyword',
            [
                'class'       => 'btn btn-success btn-ajax-modal',
                'title'       => 'Add keyword',
                'data-target' => '#addKeywordPopup',
                'data-pjax'   => 0,
                'value'       => '/product/product-category/keyword-form-popup?categoryId=' . $productCategory->id
            ]
        ) ?>
    </div>
</div>
<a id="addKeyword" name="addKeyword"></a>
<script>
    if (window.location.href.indexOf('addKeyword') !== -1) {
        $(document).ready(function () {
            setTimeout(function () {
                window.location.href = window.location.href;
            }, 1000);

        });
    }
</script>