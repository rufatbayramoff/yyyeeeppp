<?php

use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Product Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-category-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product Category', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Migrate', ['/system/migrate-categories-products'], ['class' => 'btn btn-default']) ?>
    </p>
    <?= GridView::widget([
        'caption'      => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel),
            \backend\widgets\ExcelImportWidget::widget(['tableName' => $searchModel->tableName()])),
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [

            'id',
            'code',
            'title',
            'is_active:boolean',
            'is_visible:boolean',
            'parent_id',
            'need_premoderation:boolean',
            'position',
            [
                'class'          => ActionColumn::class,
                'buttons'        => Yii::$app->getModule('translation')->dbI18n->getGridButton('product_category'),
                'template'       => '{translate} &nbsp; {update} &nbsp; {delete} ',
                'contentOptions' => ['style' => 'min-width: 100px;']
            ]
        ],
    ]); ?>
</div>
