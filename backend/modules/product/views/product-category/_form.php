<?php

use backend\widgets\FilesListWidget;
use common\components\FileTypesHelper;
use common\modules\product\repositories\ProductCategoryRepository;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-category-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php echo ' &nbsp; ';
        if (!$model->isNewRecord && !$model?->productMainCategory) {
            echo Html::a(
                'Main',
                ['product-category/main', 'id' => $model->id],
                [
                    'title' => 'Main',
                    'class' => 'btn btn-danger m-r20',
                ]
            );
        } ?>
    </div>
    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'need_premoderation')->checkbox() ?>
    <?= $form->field($model, 'is_active')->checkbox() ?>
    <?= $form->field($model, 'is_visible')->checkbox() ?>

    <div class="form-group field-storeorderreview-comment">
        <label class="control-label" for="storeorderreview-comment">File</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'formPrefix' => $model->formName(),
                    'formAttribute' => 'image',
                    'emptyText' => 'add file',
                    'filesList' => $model->file ? [$model->file] : [],
                    'rights' => [
                        FilesListWidget::ALLOW_DELETE,
                        FilesListWidget::ALLOW_ADD,
                        FilesListWidget::ALLOW_MULTIPLE,
                        FilesListWidget::ALLOW_ROTATE
                    ],
                    'filesAllowedExtensions' => FileTypesHelper::ALLOW_IMAGES_EXTENSIONS
                ]
            ) ?>
        </div>
    </div>

    <?php
    $productCategoryRepository = Yii::createObject(ProductCategoryRepository::class);
    $categoriesMap = $productCategoryRepository->getAllMap();

    echo $form->field($model, 'parent_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => $categoriesMap,
        'options' => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'description')->widget(\dosamigos\ckeditor\CKEditor::className(), [
        'options' => ['rows' => 2],
        'preset' => 'full'
    ]) ?>

    <?= $form->field($model, 'full_description')->widget(\dosamigos\ckeditor\CKEditor::className(), [
        'options' => ['rows' => 3],
        'preset' => 'full'
    ]) ?>


    <?php
    if ($model->id) {
        echo $this->render('keywords', ['productCategory' => $model]);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<script>
    function camelize(text) {
        return text.toString()
            .replace(/\s+/g, ' ')           // Replace spaces with single space
            .replace(/[^\w\-]+/g, ' ')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '')             // Trim - from end of text
            .replace(/(?:^\w|[A-Z]|\b\w)/g, function (letter, index) {
                return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
            })
            .replace(/\s+/g, '');
    }

    function slugify(text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }

    var e = document.getElementById('productcategory-title');
    e.oninput = function (a, b, c) {
        console.log(e.value);
        document.getElementById('productcategory-code').value = slugify(e.value);
    };
</script>