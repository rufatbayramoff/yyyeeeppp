<?php

/** @var ProductCategoryKeyword $productCategoryKeyword */

use common\models\ProductCategoryKeyword;
use yii\helpers\Html;

?>

<form method="post" action="/product/product-category/keyword-update-ajax?categoryId=<?= $productCategoryKeyword->product_category_id ?>">
    <?php
    if (!$productCategoryKeyword->isNewRecord) {
        ?>
        <input type="hidden" class="form-control" name="id" value="<?= H($productCategoryKeyword->id) ?>">
        <?php
    }
    ?>
    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-3">Keyword</div>
            <div class="col-md-7">
                <input type="text" id="productcategory-keyword" class="form-control" name="ProductCategoryKeyword[keyword]" maxlength="65" aria-required="true" aria-invalid="false" value="<?=H($productCategoryKeyword->keyword)?>">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>

