<?php

use backend\widgets\FilesListWidget;
use common\components\FileTypesHelper;
use common\models\ProductMainPromoBar;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductMainPromoBar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-bar-form">

    <?php $form = ActiveForm::begin([
        'method'  => 'post',
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'url')->textInput() ?>

    <div class="form-group field-storeorderreview-comment">
        <label class="control-label">File</label>
        <div>
            <?= FilesListWidget::widget(
                [
                    'formPrefix'             => $model->formName(),
                    'formAttribute'          => 'file',
                    'emptyText'              => 'add file',
                    'filesList'              => $model->file ? [$model->file] : [],
                    'rights'                 => [
                        FilesListWidget::ALLOW_DELETE,
                        FilesListWidget::ALLOW_ADD,
                    ],
                    'filesAllowedExtensions' => FileTypesHelper::ALLOW_IMAGES_EXTENSIONS
                ]
            ) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
