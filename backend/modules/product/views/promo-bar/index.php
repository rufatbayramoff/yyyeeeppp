<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PromoBarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promo Bars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-bar-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'value',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
