<?php

use common\models\ProductMainPromoBar;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductMainPromoBar */

$this->title = 'Update Promo Bar: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Promo Bars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promo-bar-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
