<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PromoBar */

$this->title = 'Create Promo Bar';
$this->params['breadcrumbs'][] = ['label' => 'Promo Bars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-bar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
