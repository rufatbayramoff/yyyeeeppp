<?php

use frontend\components\image\ImageHtmlHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductMainPromoBar */

$this->title                   = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Promo Bars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="promo-bar-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'url',
            'file' => [
                'label'  => 'Image',
                'format' => 'raw',
                'value'  => function (\common\models\ProductMainPromoBar $promoBar) {
                    return '<img src="'.ImageHtmlHelper::getThumbUrlForFile($promoBar->file).'">';
                }
            ],
        ],
    ]) ?>

</div>
