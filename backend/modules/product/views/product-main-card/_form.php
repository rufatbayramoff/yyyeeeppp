<?php

use common\models\ProductCommon;
use common\modules\product\interfaces\ProductInterface;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\base\ProductMainCard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-main-card-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $products = \common\models\ProductCommon::find()->statusPublished()->active()->all();
    echo $form->field($model, 'product_common_uid')->label('Product')->widget(\kartik\select2\Select2::class, [
        'data'          => yii\helpers\ArrayHelper::map($products, 'uid', function (ProductCommon $productCommon) {
            return $productCommon->getType() . ': '
                . ($productCommon->getType() == ProductInterface::TYPE_PRODUCT ? $productCommon->getExtObject()->uuid : $productCommon->getExtObject()->id)
                . ' - ' . $productCommon->getExtObject()->title;
        }),
        'options'       => ['placeholder' => 'Select'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
