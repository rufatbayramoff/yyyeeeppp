<?php

use common\models\ProductMainCard;
use common\modules\product\interfaces\ProductInterface;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductMainCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Product Main Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-main-card-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns'      => [
            'id',
            'position',
            'productCommon.title',
            'type',
            [
                'label'  => 'Product',
                'format' => 'raw',
                'value'  => function (ProductMainCard $card) {
                    if (!$card->productCommon) {
                        return '';
                    }
                    if ($card?->productCommon?->getType() == ProductInterface::TYPE_MODEL3D) {
                        $url = '/store/store-unit/view?id='.$card->productCommon?->model3d->storeUnit->id;
                    } else {
                        $url = '/product/product/update?uuid=' . $card?->productCommon?->product?->uuid;
                    }
                    return '<a target="_blank" href="' . $url . '">' . $card?->productCommon?->getType().': '.$card?->productCommon?->title . '</a>';
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>


</div>
