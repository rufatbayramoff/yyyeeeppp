<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\base\ProductMainCard */

$this->title                   = 'Update Product Main Card: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Main Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-main-card-update">

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'type',
            'position'
        ]
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
