<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\base\ProductMainCard */

$this->title = 'Create Product Main Card';
$this->params['breadcrumbs'][] = ['label' => 'Product Main Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-main-card-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
