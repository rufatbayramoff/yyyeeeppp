<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.04.18
 * Time: 16:29
 */


use common\interfaces\Model3dBaseInterface;
use common\modules\product\models\ProductBase;
use frontend\models\model3d\Model3dFacade;
use lib\money\Currency;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-unit-index">


    <?= GridView::widget([
        'caption'      => sprintf('<div class="row"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel),
            Html::a('Import products', ['/product/import'], ['class' => 'btn btn-success'])),
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'uuid',
            [
                'label'     => 'User', 'format' => 'raw',
                'filter'    => Html::activeTextInput($searchModel, 'user_id', ['class' => 'form-control']),
                'class'     => \backend\components\columns\UserColumn::class,
                'attribute' => 'user',
            ],
            [
                'format' => 'raw',
                'value'  => function (\common\models\Product $model) {
                    $url = $model->getCoverUrl();
                    return $url ? Html::img($url, ['width' => 90]) : '';
                }
            ],
            'title',
            [
                'attribute' => 'description',
                'value'     => function (\common\models\Product $model) {
                    return substr(strip_tags($model->description), 0, 500);
                }
            ],
            [
                'attribute' => 'category_id',
                'value'     => function ($model) {
                    return $model->category ? $model->category->title : '';
                },
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'category_id',
                    \common\models\ProductCategory::getActiveSelectCategories(),
                    ['class' => 'form-control', 'prompt' => 'All']
                ),
            ],
            [
                'attribute' => 'product_status',
                'value'     => function ($model) {
                    return ProductBase::getProductStatusLabels()[$model->product_status];
                },
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'product_status',
                    ProductBase::getProductStatusLabels(),
                    ['class' => 'form-control', 'prompt' => 'All']
                ),
            ],
           // 'updated_at',
            'moderated_at',
            'published_at',
            'is_active:boolean',
            [
                'format' => 'raw',
                'value'  => function (\common\models\Product $model) {
                    $url1 = param('siteUrl') . '/product/' . $model->uuid . '-1?pvc=' . $model->generatePrivateViewCode();
                    $txt1 = yii\helpers\Html::a('VIEW', $url1, ['class' => 'btn btn-ghost btn-info', 'target' => '_blank']);
                    $url  = yii\helpers\Url::toRoute(['product/update', 'uuid' => $model->uuid]);
                    $txt2 = yii\helpers\Html::a('Moderate', $url, ['class' => 'btn btn-ghost btn-warning', 'target' => '_blank']);
                    return $txt1 . $txt2;
                }
            ]
        ],
    ]); ?>
</div>
