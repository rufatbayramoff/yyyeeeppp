<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 10:25
 */

use backend\assets\AngularAppAsset;
use backend\models\Backend;
use backend\widgets\FilesListWidget;
use beowulfenator\JsonEditor\JsonEditorAsset;
use beowulfenator\JsonEditor\JsonEditorWidget;
use common\components\ArrayHelper;
use common\components\FileTypesHelper;
use common\models\CompanyBlock;
use common\models\ModerLog;
use common\models\Product;
use common\models\ProductCategory;
use common\models\ProductFile;
use common\models\ProductHistory;
use common\models\ProductMainCard;
use common\modules\product\repositories\ProductCategoryRepository;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $product common\models\Product */

$this->title                   = 'Update Product: ' . $product->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $product->title, 'url' => ['view', 'id' => $product->uuid]];
$this->params['breadcrumbs'][] = 'Update';

$product->updateFormAttributes();
AngularAppAsset::register($this);
JsonEditorAsset::register($this);

?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<div class="product-update">

    <div class="product-form row">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="col-md-12" style="padding-bottom: 20px;">
            <div class="m-b10">
                <?php
                if (in_array($product->product_status, [Product::STATUS_PUBLISHED_UPDATED, Product::STATUS_REJECTED])) {
                    echo '<br />
    <div class="m-t20 m-b10"><p class="callout callout-danger">REJECTED: ' . ModerLog::getLastRejectModerLog(ModerLog::TYPE_PRODUCT, $product->uuid) . '</p></div>';
                }
                ?>
                <?php
                if ($product->product_status != Product::STATUS_REJECTED) {
                    echo Html::button(
                        'Reject',
                        [
                            'title'       => 'Reject reason',
                            'class'       => 'btn btn-warning btn-ajax-modal',
                            'data-pjax'   => 0,
                            'value'       => \yii\helpers\Url::toRoute(['product/reject', 'uuid' => $product->uuid]),
                            'data-target' => '#model_storereject'
                        ]
                    );
                }
                echo ' &nbsp; ';
                if ($product->product_status != Product::STATUS_PUBLISHED_PUBLIC) {
                    echo Html::a(
                        'Publish',
                        ['product/approve', 'uuid' => $product->uuid],
                        [
                            'title' => 'Publish',
                            'class' => 'btn btn-success m-r20',
                        ]
                    );
                }
                echo ' &nbsp; ';
                $url1 = param('siteUrl') . '/product/' . $product->uuid . '-1?pvc=' . $product->generatePrivateViewCode();
                echo ' &nbsp; ' . yii\helpers\Html::a('VIEW', $url1, ['class' => 'btn btn-ghost btn-info', 'target' => '_blank']);
                echo ' &nbsp; ';
                echo Html::submitButton($product->isNewRecord ? 'Create' : 'Save', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                ?>
            </div>

            <div style="color: red; padding-top: 10px;">
                <?= $product->getErrors() ? \yii\helpers\Html::errorSummary($product) : '' ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>Uuid</label>: <?= $product->uuid ?>
            </div>
            <div class="form-group">
                <label>User</label>: <a href="/user/user/update?id=<?= $product->user->id ?>" target="_blank"><?= $product->user->email ?></a>
            </div>
            <div class="form-group">
                <label>Company</label>: <a href="/ps/ps/view?id=<?= $product->company->id ?>" target="_blank"><?= $product->company->title ?></a>
            </div>
            <div class="form-group">
                <label>Updated at</label>: <?= $product->updated_at ?>
            </div>
            <div class="form-group">
                <label>Moderated at</label>: <?= $product->moderated_at ?>
            </div>
            <div class="form-group">
                <label>Published at</label>: <?= $product->published_at ?>
            </div>
            <div class="form-group">
                <label>Ship from</label>: <?= $product->getShipFromLocation(true)->formatted_address; ?>
            </div>
            <div class="form-group">
                <label>Avg. production time</label>: <?= H($product->avg_production . ' ' . $product->avg_production_time); ?>
            </div>
            <div class="form-group">
                <label>Supply ability</label>: <?= H($product->supply_ability . ' ' . $product->supply_ability_time); ?>
            </div>


            <?= $form->field($product, 'formTitle')->textInput(['maxlength' => true]) ?>
            <div class="form-group">
                <div class="">
                    <?php
                    $productCategory = $product->productCategory;
                    if (!$productCategory) {
                        $productCategory = new ProductCategory();
                    }
                    $productCategoryRepository = Yii::createObject(ProductCategoryRepository::class);
                    $categoriesMap             = $productCategoryRepository->getFinalMap();
                    echo $form->field($productCategory, 'id')->widget(\kartik\select2\Select2::classname(), [
                        'data'          => $categoriesMap,
                        'options'       => ['placeholder' => 'Select'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Product category');
                    ?>
                </div>
            </div>

            <?= $form->field($product, 'product_status')->dropDownList([
                'draft'              => 'Draft',
                'published_public'   => 'Published public',
                'published_updated'  => 'Published updated',
                'published_directly' => 'Published directly',
                'publish_pending'    => 'Publish pending',
                'rejected'           => 'Rejected',
                'private'            => 'Private',
            ], ['prompt' => '']) ?>


            <?= $form->field($product, 'is_active')->textInput() ?>

            <?= $form->field($product, 'formSinglePrice')->textInput(['maxlength' => true]) ?>

            <?= $form->field($product, 'unit_type')->dropDownList(Product::getUnitTypesComboList()); ?>

            <?= $form->field($product, 'incoterms')->dropDownList(Product::getIncotermsList()); ?>

            <?= $form->field($product, 'product_package')->textInput(['maxlength' => true, 'list' => 'datalist_product_package']); ?>
            <datalist id="datalist_product_package">
                <?php foreach (Product::getProductPackageList() as $k => $v): ?>
                    <option value="<?= $v; ?>"><?= $v; ?></option>
                <?php endforeach; ?>
            </datalist>

            <?= $form->field($product, 'outer_package')->textInput(['maxlength' => true, 'list' => 'datalist_outer_package']); ?>
            <datalist id="datalist_outer_package">
                <?php foreach (Product::getOuterPackageList() as $v): ?>
                    <option value="<?= $v; ?>"><?= $v; ?></option>
                <?php endforeach; ?>
            </datalist>

            <?php if ($product->getProductDelivery()): ?>
                <div class="form-group">
                    <?= $form->field($product->getProductDelivery(), 'country_id')->widget(\kartik\select2\Select2::classname(), [
                        'data'          => yii\helpers\ArrayHelper::map(common\models\GeoCountry::find()->asArray()->all(), 'id', 'title'),
                        'options'       => ['placeholder' => 'Select'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= $form->field($product->getProductDelivery(), 'first_item')->textInput(['maxlength' => true]); ?>
                </div>
                <div class="form-group">
                    <?= $form->field($product->getProductDelivery(), 'following_item')->textInput(['maxlength' => true]); ?>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <label>Tags</label>
                <?php
                $productTagsTexts = \yii\helpers\ArrayHelper::map($product->getProductTags(), 'id', 'text');
                echo $form->field($product, 'productTags')->widget(
                    Select2::class,
                    common\components\SimpleSelect2::getAjax(
                        ['product/tags'],
                        'id,text',
                        $productTagsTexts,
                        [
                            'options'       => ['placeholder' => _t('front.model3d', 'Add tags') . '... ', 'multiple' => true],
                            'pluginOptions' => [
                                'tags'               => true,
                                //'allowClear' => true,
                                'minimumInputLength' => 2,
                                'maximumInputLength' => 45
                            ]
                        ]
                    )
                )->label(false); ?>
            </div>
            <div class="form-group">
                <div class="col-12" id="productDynamicValues">
                    <?= $this->render('@frontend/modules/mybusiness/modules/product/views/product/productDynamicValues', [
                        'dynamicValues' => $product->getDynamicValues(),
                        'formName'      => $product->formName()
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-12">
                    <?= $form->field($product, 'moqPricesJson', ['options' => ['class' => 'form-group jsonEditorField']])->widget(
                        JsonEditorWidget::class,
                        [
                            'schema'          => [
                                'title'  => 'Moq prices',
                                'type'   => 'array',
                                'format' => 'table',
                                'items'  => [
                                    'type'       => 'object',
                                    'properties' => [
                                        'moq'          => [
                                            'type' => 'number',
                                        ],
                                        'price'        => [
                                            'type'     => 'number',
                                            'readonly' => 1,
                                        ],
                                        'priceWithFee' => [
                                            'type' => 'number',
                                        ]
                                    ]
                                ]
                            ],
                            'enableSelectize' => true,
                            'clientOptions'   => [
                                'theme' => 'bootstrap3'
                            ]
                        ]
                    )->label(''); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-12">
                    <?= $form->field($product, 'customPropertiesJson', ['options' => ['class' => 'form-group jsonEditorField']])->widget(
                        JsonEditorWidget::class,
                        [
                            'schema'          => [
                                'title'  => 'Specifications',
                                'type'   => 'array',
                                'format' => 'table',
                                'items'  => [
                                    'type'       => 'object',
                                    'properties' => [
                                        'title'    => [
                                            'type' => 'string',
                                        ],
                                        'value'    => [
                                            'type' => 'string',
                                        ],
                                        'position' => [
                                            'type' => 'number',
                                        ]
                                    ]
                                ]
                            ],
                            'enableSelectize' => true,
                            'clientOptions'   => [
                                'theme' => 'bootstrap3'
                            ]
                        ]
                    )->label(''); ?>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <?= $form->field($product, 'formDescription')->widget(\dosamigos\ckeditor\CKEditor::className(), \backend\components\CKEditorDefault::getDefaults()) ?>
            <div class="form-group field-storeorderreview-comment">
                <label class="control-label" for="storeorderreview-comment">Images</label>
                <div>
                    <?= FilesListWidget::widget(
                        [
                            'formPrefix'             => $product->formName(),
                            'formAttribute'          => 'images',
                            'filesList'              => $product->imageFiles,
                            'rights'                 => [
                                FilesListWidget::ALLOW_DELETE,
                                FilesListWidget::ALLOW_ADD,
                                FilesListWidget::ALLOW_MULTIPLE,
                                FilesListWidget::ALLOW_ROTATE
                            ],
                            'filesAllowedExtensions' => FileTypesHelper::ALLOW_IMAGES_EXTENSIONS,
                            'compatible'             => FilesListWidget::COMPATIBLE_DROPZONE
                        ]
                    ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">3D Models</label>
                <div>
                    <?= GridView::widget([
                        'dataProvider' => new ActiveDataProvider(['query' => $product->getCadFiles()]),
                        'columns'      => [
                            'title',
                            [
                                'label'  => 'Preview',
                                'format' => 'raw',
                                'value'  => function (ProductFile $productFile) {
                                    [$html, $url] = $productFile->getRenderUrl();
                                    return '<a href="' . param('server') . $url . '" class="preview_model3d">' . $html . '</a>';
                                }
                            ],
                            [
                                'label'  => '',
                                'format' => 'raw',
                                'value'  => function (ProductFile $productFile) {
                                    $renderButton = '<a href="/product/product/render-cad-file?id=' . $productFile->id . '">Render</a>';
                                    $deleteButton = '<a href="/product/product/delete-file?id=' . $productFile->id . '">Delete</a>';
                                    return $renderButton . ' &nbsp; ' . $deleteButton;
                                }
                            ]
                        ]
                    ]); ?>

                </div>
            </div>
            <div class="form-group">
                <div>
                    <?= $form->field($product, 'videosJson', ['options' => ['class' => 'form-group jsonEditorField']])->widget(
                        JsonEditorWidget::class,
                        [
                            'schema'          => [
                                'title'  => 'Videos',
                                'type'   => 'array',
                                'format' => 'table',
                                'items'  => [
                                    'type'       => 'object',
                                    'properties' => [
                                        'url'     => [
                                            'type' => 'string',
                                        ],
                                        'thumb'   => [
                                            'type'    => 'string',
                                            'options' => [
                                                'hidden' => true
                                            ],
                                        ],
                                        'videoId' => [
                                            'type'    => 'string',
                                            'options' => [
                                                'hidden' => true
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            'enableSelectize' => true,
                            'clientOptions'   => [
                                'theme' => 'bootstrap3'
                            ]
                        ]
                    )->label(''); ?>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <?php
            $certDataProvider = $product->getCertificationsDataProvider();
            if ($certDataProvider->getTotalCount() > 0) {
                ?>
                <div class="product-page__section">
                    <h3 class="product-page__section-title"><?= _t('company.public', 'Product Certifications'); ?></h3>
                    <div class="table-responsive">
                        <?php echo \yii\grid\GridView::widget(
                            [
                                'dataProvider' => $certDataProvider,
                                'columns'      => [
                                    [
                                        'format' => 'raw',
                                        'value'  => function ($cert) {
                                            if (\common\components\FileTypesHelper::isImage($cert->file)) {
                                                $downloadLink = \frontend\components\image\ImageHtmlHelper::getClickableThumb($cert->file);
                                            } else {
                                                $imgHtml      = "<img width='85' src='" . param('server') . "/static/images/certificate.svg' style='margin-bottom: -5px;'/> ";
                                                $downloadLink = \yii\helpers\Html::a($imgHtml, $cert->file->getFileUrl());
                                            }
                                            return $downloadLink;
                                        }
                                    ],
                                    [
                                        'attribute' => 'title',
                                        'format'    => 'raw',
                                        'label'     => _t('mybusiness.public', 'Certification'),
                                        'value'     => function ($cert) {
                                            $downloadLink = \yii\helpers\Html::a(_t('site.product', 'Download'), $cert->file->getFileUrl());
                                            if (\common\components\FileTypesHelper::isImage($cert->file)) {
                                                $downloadLink = '<br />' . \frontend\components\image\ImageHtmlHelper::getClickableThumb($cert->file);
                                            }

                                            return '<b>' . H($cert->title) . '</b>';
                                        }
                                    ],
                                    'certifier',
                                    'application',
                                    'issue_date:date',
                                    'expire_date:date',
                                ],
                                'tableOptions' => [
                                    'class' => 'table table-bordered product__cert-table'
                                ],
                            ]
                        ); ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-md-6">
            <?php foreach ($product->getPublicProductBlocks() as $productBlock): ?>
                <div class="product-page__section ugc-content">
                    <h3 class="product-page__section-title"><?= H($productBlock->title); ?></h3>

                    <?= $productBlock->content; ?>

                    <div class="m-t10">
                        <?= $productBlock->imageFiles ?
                            \frontend\widgets\SwipeGalleryWidget::widget([
                                'files'            => $productBlock->imageFiles,
                                'thumbSize'        => [230, 180],
                                'containerOptions' => ['class' => 'picture-slider'],
                                'itemOptions'      => ['class' => 'picture-slider__item'],
                                'scrollbarOptions' => ['class' => 'picture-slider__scrollbar'],
                            ]) : '';
                        ?>
                    </div>
                    <?php if ($productBlock instanceof CompanyBlock && $videos = $productBlock->getCompanyBlockVideos()): ?>
                        <div class="row">
                            <?php foreach ($videos as $video): ?>
                                <div class="col-sm-4">
                                    <div class="video-container">
                                        <object data="https://www.youtube.com/embed/<?= $video['videoId']; ?>"></object>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col-md-12">
            <h3>History</h3>
            <?= GridView::widget(
                [
                    'dataProvider' => ProductHistory::getDataProvider(['model_uuid' => $product->uuid], 1000, ['sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]]),
                    'columns'      => [
                        'id',
                        'created_at',
                        [
                            'header'    => 'User',
                            'attribute' => 'user_id',
                            'format'    => 'raw',
                            'value'     => function (ProductHistory $history) {
                                return \backend\models\Backend::displayUser($history->user_id);
                            }
                        ],
                        'action_id',
                        'diffRemove' => [
                            'header' => 'Remove',
                            'format' => 'raw',
                            'value'  => function (ProductHistory $history) {
                                return '<pre class="jsonHighlight small-text">' . json_encode($history->getSourceResultDiffRemove(), JSON_PRETTY_PRINT) . '</pre>';
                            },
                        ],
                        'diffAdd'    => [
                            'header' => 'Add',
                            'format' => 'raw',
                            'value'  => function (ProductHistory $history) {
                                return '<pre class="jsonHighlight small-text">' . json_encode($history->getSourceResultDiffAdd(), JSON_PRETTY_PRINT) . '</pre>';
                            },
                        ],
                    ],
                ]
            );
            ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
