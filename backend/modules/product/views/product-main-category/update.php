<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\base\ProductMainCategory */

$this->title = 'Update Product Main Category: ' . $model->productCategory?->title;
$this->params['breadcrumbs'][] = ['label' => 'Product Main Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->productCategory?->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-main-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
