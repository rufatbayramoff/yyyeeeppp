<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductMainCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Product Main Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-main-category-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            'productCategory.title' => [
                'attribute' => 'productCategory.title',
                'format' => 'raw',
                'value'     => function (\common\models\ProductMainCategory $productMainCategory) {
                    return "<a href='/product/product-category/update?id=".$productMainCategory->product_category_id."'>".H($productMainCategory->productCategory?->title).'</a>';
                },
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>


</div>
