<?php
/**
 * User: nabi
 */

namespace backend\modules\product\import;

use backend\modules\importer\etl\EtlSchema;
use backend\modules\importer\etl\extractors\ArrayExtractor;
use backend\modules\importer\etl\ImportReporter;
use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\loaders\BaseLoader;
use backend\modules\importer\etl\Runner;
use backend\modules\importer\etl\transformers\CallbackTransformer;
use backend\modules\importer\etl\transformers\ColumnProperties;
use backend\modules\importer\etl\transformers\ColumnValueSplitter;
use backend\modules\importer\etl\transformers\ImageUrlToFile;
use backend\modules\importer\etl\transformers\TextToTagTransformer;
use backend\modules\importer\etl\transformers\VideoTransformer;
use common\components\ArrayHelper;
use common\models\Company;
use common\models\factories\UploadedFileListFactory;
use common\models\Product;
use common\models\User;
use common\modules\product\factories\ProductFactory;
use common\modules\product\interfaces\ProductInterface;
use common\modules\product\populators\ProductPopulator;
use common\modules\product\repositories\ProductCategoryRepository;
use common\modules\product\repositories\ProductRepository;
use Yii;
use yii\helpers\Json;

/**
 * Class ImportService
 *
 * @package backend\modules\product\import
 */
class ImportService
{

    /**
     * @param ImportProduct $importModel
     * @throws \yii\base\Exception
     */
    public function saveToSession(ImportProduct $importModel)
    {
        $session = Yii::$app->session;
        $key     = Yii::$app->security->generateRandomString(10);
        $session->set('upload_' . $key, serialize($importModel));
        return $key;
    }

    public function updateToSession($key, ImportProduct $importModel)
    {
        $session = Yii::$app->session;
        $session->set('upload_' . $key, serialize($importModel));
        return $key;
    }

    /**
     * @param $key
     * @return ImportProduct
     */
    public function loadFromSession($key)
    {
        $session = Yii::$app->session;
        $r       = unserialize($session->get('upload_' . $key));
        return $r;
    }

    /**
     * @param ImportProduct $importProduct
     * @return int
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function getColumns(ImportProduct $importProduct)
    {
        $objPHPExcel  = \PHPExcel_IOFactory::load($importProduct->filePath);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $row          = [];
        foreach ($objWorksheet->getRowIterator() as $i => $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $row = [];
            foreach ($cellIterator as $cell) {
                $row[] = $cell->getValue();
            }
            if ($i == $importProduct->columnIndex) {
                break;
            }
        }
        return array_filter($row);
    }

    /**
     * @param ImportProduct $importModel
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function getExcelRows(ImportProduct $importModel)
    {
        $objPHPExcel  = \PhpOffice\PhpSpreadsheet\IOFactory::load($importModel->filePath);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $imagesLocation = [];
        $i              = 0;
        foreach ($objPHPExcel->getActiveSheet()->getDrawingCollection()->getIterator() as $image) {
            /** @var  \PHPExcel_Worksheet_Drawing $image */
            $coords = $image->getCoordinates();
            if (is_a($image, \PHPExcel_Worksheet_MemoryDrawing::class)) {
                /** @var $image \PHPExcel_Worksheet_MemoryDrawing */
                ob_start();
                call_user_func($image->getRenderingFunction(), $image->getImageResource());
                $imageContents = ob_get_contents();
                ob_end_clean();
                $extension  = 'png';
                $myFileName = Yii::getAlias('@runtime') . '/uploads/product_image_' . ++$i . '.' . $extension;
                file_put_contents($myFileName, $imageContents);

                $imagesLocation[$coords] = $myFileName;
            } else {
                $imagesLocation[$coords] = ($image->getPath());
            }
            #$name = $image->getName();
            #$desc = $image->getDescription();
        }
        $imported = $skipped = 0;
        $logs     = [];
        $result   = [];
        $columns  = [];
        foreach ($objWorksheet->getRowIterator() as $i => $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $row    = [];
            $coords = '';
            foreach ($cellIterator as $k => $cell) {
                if (!empty($columns) && count($row) === count($columns)) {
                    continue;
                }
                $rowValue = (string)$cell->getValue();
                $coords   = $cell->getColumn() . $i;
                if (!empty($imagesLocation[$coords])) {
                    $rowValue = $imagesLocation[$coords];
                }
                $row[] = $rowValue;
            }
            if ($i == $importModel->columnIndex) {
                $row     = array_filter($row);
                $columns = array_map('strtolower', $row);
                continue;
            }
            if (!$columns) {
                continue;
            }
            $result[$i] = array_combine($columns, $row);
        }

        return $result;
    }

    /**
     * @param ImportProduct $importModel
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function import(ImportProduct $importModel)
    {
        set_time_limit(600); // 10 minutes

        $rows      = $this->getExcelRows($importModel);
        $loader    = new BaseLoader();
        $etlSchema = (new EtlSchema())
            ->extract(['class' => ArrayExtractor::class])
            ->transform($this->getImportTransformers())
            ->load($loader);

        $etlRunner    = new Runner();
        $parsedRows   = $etlRunner->run($etlSchema, $rows);
        $importReport = $etlRunner->getReport();

        $importReport = $this->populateProducts($importModel, $importReport, $parsedRows);
        return $importReport;
    }

    /**
     * @return array
     */
    private function getImportTransformers()
    {
        return [
            [
                'class'    => CallbackTransformer::class,
                'callback' => function (ElementInterface $el) {
                    $data = $el->getData();
                    foreach ($data as $k => $v) {
                        $k = trim($k);
                        if (!is_array($v)) {
                            $data[$k] = trim($v);
                        }
                    }
                    $el->setData($data);
                    return $el;
                }
            ],
            [
                'fields'    => ['video', 'tags', 'images', 'image'],
                'class'     => ColumnValueSplitter::class,
                'delimeter' => ',',
            ],/*
            [
                'field'   => 'tags',
                'class'   => TextToTagTransformer::class,
                'columns' => ['title']
            ],*/
            [
                'fields' => ['primary_image', 'images', 'image'],
                'class'  => ImageUrlToFile::class,
            ],
            [
                'field'   => 'properties',
                'class'   => ColumnProperties::class,
                'columns' => ['properties', 'specifications']
            ],
            [
                'field' => 'video',
                'class' => VideoTransformer::class,
            ]
        ];
    }


    /**
     * @param ImportProduct $importModel
     * @param BaseLoader[] $parsedRows
     * @param $productFactory
     * @param $companyUser
     * @param $productPopulator
     * @param $productRepository
     * @param $importReport
     * @return ImportReporter
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    private function populateProducts(ImportProduct $importModel, ImportReporter $importReport, $parsedRows)
    {
        $productCategoryRepository = Yii::createObject(ProductCategoryRepository::class);
        $categoriesMap             = $productCategoryRepository->getFinalMap();
        $categoriesMap             = array_flip($categoriesMap);

        // find current products
        $currentProducts    = Product::find()
            ->leftJoin('product_common', 'product_common.uid=product.product_common_uid')
            ->where(['company_id' => $importModel->company_id, 'is_active' => 1])
            ->select(['product.uuid as uuid', 'product_common.title as title'])
            ->asArray()->all();
        $oldProductsByTitle = ArrayHelper::index($currentProducts, 'title');
        $company            = Company::tryFindByPk($importModel->company_id);
        $companyUser        = $company->user;

        $productPopulator  = Yii::createObject(ProductPopulator::class);
        $productRepository = Yii::createObject(ProductRepository::class);
        /** @var ProductFactory $productFactory */
        $productFactory         = Yii::createObject(['class' => ProductFactory::class, 'user' => $companyUser]);
        $importReport->imported = 0;
        foreach ($parsedRows as $baseLoader) {
            $data = $baseLoader->getRawData();
            /** @var BaseLoader $baseLoader */
            $product = $productFactory->create();
            if (in_array($data['title'], array_keys($oldProductsByTitle))) {
                $oldProduct = $oldProductsByTitle[$data['title']];
                if ($importModel->importPolicy == ImportProduct::IMPORT_POLICY_ADD) {
                    $importReport->errors[$baseLoader->getId()] = 'Old product. Skipped adding';
                    $importReport->skipped++;
                    continue;
                } else if ($importModel->importPolicy === ImportProduct::IMPORT_POLICY_UPDATE) {
                    $product = Product::find()
                        ->joinWith('productCommon')
                        ->where([
                            'uuid'                      => $oldProduct['uuid'],
                            'product_common.company_id' => $company->id
                        ])->one();
                }
            }

            $props = !empty($data['properties']) ? Json::encode($data['properties']) : null;

            $moq  = [['moq' => 1, 'priceWithFee' => (float)$data['price'], 'price' => (float)$data['price']]];
            $post = [
                $product->formName() => [
                    'title'             => $data['title'],
                    'description'       => $data['description'],
                    'price'             => (float)$data['price'],
                    'moq_prices'        => Json::encode($moq),
                    'user_id'           => $companyUser->id,
                    'category_id'       => $importModel->category_id,
                    'company_id'        => $importModel->company_id,
                    'custom_properties' => $props,
                    'productTags'       => array_key_exists('tags', $data) ? $data['tags'] : ''
                ]
            ];
            if (array_key_exists('category', $data)) {
                $categoryName = $data['category'];
                if (array_key_exists($categoryName, $categoriesMap)) {
                    $post['ProductCategory']['id'] = $categoriesMap[$categoryName];
                }
            }

            $productPopulator->populate($product, $post);
            if (!empty($data['image'])) {
                $images       = $data['image'];
                $primaryImage = array_shift($images);
                $productPopulator->populateFile($product, 'coverImageFile', $primaryImage);
                if ($images) {
                    $productPopulator->populateFiles($product, 'imageFiles', $images);
                }
            }
            $product->productCommon->product_status = ProductInterface::STATUS_PUBLISH_PENDING;
            if ($product->validate()) {
                $productRepository->saveProduct($product);
                $importReport->imported++;
            } else {
                $importReport->errors[$baseLoader->getId()] = $product->getErrors();
            }
        }
        return $importReport;
    }
}