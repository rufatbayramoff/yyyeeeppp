<?php
/**
 * User: nabi
 */

namespace backend\modules\product\import;


use common\models\Product;
use yii\base\Model;
use yii\web\UploadedFile;

class ImportProduct extends Model
{
    public $company_id;
    public $category_id;
    public $importFrom;
    public $importTo;
    public $importPolicy;

    public $columnIndex = 1;


    public $filePath;
    /**
     * @var UploadedFile
     */
    public $importFile;

    const IMPORT_POLICY_ADD = 'add';
    const IMPORT_POLICY_UPDATE = 'update';

    public function rules()
    {
        return [
            [['importFile'], 'file', 'skipOnEmpty' => true, 'checkExtensionByMimeType' => false, 'extensions' => 'csv, xls, xlsx', 'maxFiles' => 1],
            [['company_id'], 'required'],
            [['importFrom', 'importTo'], 'number'],
            [['columnIndex'], 'number'],
            [['importPolicy'], 'string']
        ];
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        if (!empty($this->filePath)) {
            return $this->filePath;
        }
        $uploadDir = \Yii::getAlias('@runtime/uploads');
        if (!is_dir($uploadDir)) {
            mkdir($uploadDir);
        }
        return $uploadDir . '/'. $this->importFile->baseName . '.' . $this->importFile->extension;
    }

    public function getImportAttributes()
    {
        $product = new Product();
        $productAttributes = $product->attributeLabels();
        $result = [];

        $unset = [
            'uuid',
            'cover_image_file_uuid',
            'category_id',
            'product_status',
            'created_at',
            'updated_at',
            'moderated_at',
            'published_at',
            'is_active',
            'user_id',
            'company_id',
            'single_price',
            'custom_properties',
        ];
        foreach ($productAttributes as $k => $v) {
            if (in_array($k, $unset)) {
                unset($productAttributes[$k]);
            }
        }
        return $productAttributes;
    }

    public function __sleep()
    {
        $this->filePath = $this->getFilePath();
        return ['filePath', 'company_id', 'category_id', 'importPolicy', 'importFrom', 'importTo', 'columnIndex'];
    }
}