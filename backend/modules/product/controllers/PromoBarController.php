<?php

namespace backend\modules\product\controllers;

use backend\components\CrudController;
use backend\models\search\ProductMainPromoBarSearch;
use common\interfaces\FileBaseInterface;
use common\models\factories\FileFactory;
use common\models\ProductMainPromoBar;
use common\models\ProductMainSlider;
use common\models\repositories\FileRepository;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * PromobarController implements the CRUD actions for PromoBar model.
 */
class PromoBarController extends CrudController
{

    /** @var FileFactory */
    protected $fileFactory;

    public function injectDependencies(FileFactory $fileFactory)
    {
        $this->fileFactory = $fileFactory;
    }


    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all PromoBar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductMainPromoBarSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PromoBar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing PromoBar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost) {
            $this->addImage($model);
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the PromoBar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductMainPromoBar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductMainPromoBar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param ProductMainSlider $model
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\ErrorException
     */
    protected function addImage(ProductMainPromoBar $model): void
    {
        $files = UploadedFile::getInstances($model, 'file');
        if ($files) {
            $image = $this->fileFactory->createFileFromUploadedFile($files[0], FileBaseInterface::TYPE_FILE_ADMIN);
            if ($image) {
                $model->addImage($image);
            }
        }
    }

}
