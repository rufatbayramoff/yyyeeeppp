<?php

namespace backend\modules\product\controllers;

use backend\components\AdminAccess;
use backend\components\CrudController;
use backend\models\search\ProductCategorySearch;
use common\components\exceptions\OnlyPostRequestException;
use common\interfaces\FileBaseInterface;
use common\models\factories\FileFactory;
use common\models\ProductCategory;
use common\models\ProductCategoryKeyword;
use common\models\ProductMainCategory;
use Yii;
use yii\base\InvalidArgumentException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProductCategoryController implements the CRUD actions for ProductCategory model.
 */
class ProductCategoryController extends CrudController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /** @var  FileFactory */
    public $fileFactory;

    public function injectDependencies(FileFactory $fileFactory)
    {
        $this->fileFactory = $fileFactory;
    }

    public function init()
    {
        parent::init();
        $this->searchModel = new ProductCategorySearch();
        $this->mainModel   = new \common\models\ProductCategory();
    }

    /**
     * Lists all ProductCategory models.
     *
     * @return mixed
     * @throws \yii\base\UserException
     */
    public function actionIndex()
    {
        AdminAccess::validateAccess('product.product_category');
        $searchModel  = new ProductCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductCategory model.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\base\UserException
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess('product.product_category');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        AdminAccess::validateAccess('product.product_category');
        $model = new ProductCategory();

        if ($model->load(Yii::$app->request->post())) {
            $this->addImage($model);
            $model->total_count = 0;
            $model->safeSave();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\base\UserException
     */
    public function actionUpdate($id)
    {
        AdminAccess::validateAccess('product.product_category');
        $model = $this->findModel($id);
        $this->addImage($model);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\base\UserException
     */
    public function actionDelete($id)
    {
        AdminAccess::validateAccess('product.product_category');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionKeywordUpdateAjax($categoryId)
    {
        AdminAccess::validateAccess('product.product_category');
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }

        if ($keywordId = Yii::$app->request->post('id')) {
            $productCategoryKeyword = ProductCategoryKeyword::tryFindByPk($keywordId);
        } else {
            $productCategoryKeyword                      = new ProductCategoryKeyword();
            $productCategoryKeyword->product_category_id = $categoryId;
        }


        $productCategoryKeyword->load(Yii::$app->request->post());
        if ($productCategoryKeyword->save()) {
            Yii::$app->getSession()->setFlash('success', 'Keyword added', false);
            $this->redirect('/product/product-category/update?id=' . $productCategoryKeyword->product_category_id.'#addKeyword');
            Yii::$app->end();
        }
    }

    public function actionKeywordFormPopup()
    {
        AdminAccess::validateAccess('product.product_category');
        if ($id = Yii::$app->request->get('id')) {
            $productCategoryKeyword = ProductCategoryKeyword::tryFindByPk($id);
        } else {
            $categoryId                                  = Yii::$app->request->get('categoryId');
            $productCategoryKeyword                      = new ProductCategoryKeyword();
            $productCategoryKeyword->product_category_id = $categoryId;
        }
        return $this->renderAjax('keywordFormPopup', ['productCategoryKeyword' => $productCategoryKeyword]);
    }

    public function actionKeywordDelete($id)
    {
        AdminAccess::validateAccess('product.product_category');
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $productCategoryKeyword = ProductCategoryKeyword::tryFindByPk($id);
        $productCategoryId = $productCategoryKeyword->product_category_id;
        $productCategoryKeyword->delete();
        Yii::$app->getSession()->setFlash('success', 'Keyword added', false);
        $this->redirect('/product/product-category/update?id=' . $productCategoryId.'#addKeyword');

    }

    public function actionMain($id)
    {
        AdminAccess::validateAccess('product.product_category');
        $model       = $this->findModel($id);
        $productMainCategory = new ProductMainCategory();
        $productMainCategory->product_category_id = $model->id;
        $productMainCategory->safeSave();
        return $this->redirect('/product/product-category/update?id=' . $model->id);
    }

    /**
     * Finds the ProductCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductCategory::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param ProductCategory $model
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\ErrorException
     */
    private function addImage(ProductCategory $model): void
    {
        $files = UploadedFile::getInstances($model, 'image');
        if ($files) {
            $image = $this->fileFactory->createFileFromUploadedFile($files[0], FileBaseInterface::TYPE_FILE_ADMIN);
            if ($image) {
                $model->addImage($image);
            }
        }
    }
}
