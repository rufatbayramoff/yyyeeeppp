<?php

namespace backend\modules\product\controllers;

use backend\components\CrudController;
use backend\models\search\ProductMainSliderSearch;
use common\interfaces\FileBaseInterface;
use common\models\factories\FileFactory;
use common\models\ProductMainSlider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProductMainSliderController implements the CRUD actions for ProductMainSlider model.
 */
class ProductMainSliderController extends CrudController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /** @var  FileFactory */
    public $fileFactory;

    public function injectDependencies(FileFactory $fileFactory)
    {
        $this->fileFactory = $fileFactory;
    }

    /**
     * Lists all ProductMainSlider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductMainSliderSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductMainSlider model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductMainSlider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductMainSlider();

        if ($this->request->isPost) {
            $this->addImage($model);
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductMainSlider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $this->addImage($model);
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductMainSlider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductMainSlider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductMainSlider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductMainSlider::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param ProductMainSlider $model
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\ErrorException
     */
    private function addImage(ProductMainSlider $model): void
    {
        $files = UploadedFile::getInstances($model, 'image');
        if ($files) {
            $image = $this->fileFactory->createFileFromUploadedFile($files[0], FileBaseInterface::TYPE_FILE_ADMIN);
            if ($image) {
                $model->addImage($image);
            }
        }
    }
}
