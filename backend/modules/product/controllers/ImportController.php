<?php
/**
 * User: nabi
 */

namespace backend\modules\product\controllers;


use backend\components\AdminController;
use backend\modules\product\import\ImportProduct;
use backend\modules\product\import\ImportService;
use backend\modules\statistic\reports\ReportExcelWriter;
use common\components\ArrayHelper;
use common\components\FileDirHelper;
use common\modules\product\repositories\ProductCategoryRepository;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use yii\helpers\Html;
use yii\web\UploadedFile;
use Yii;

class ImportController extends AdminController
{

    /**
     * @var ImportService
     */
    public $importService;

    public function injectDependencies(ImportService $importService)
    {
        $this->importService = $importService;
    }

    public function actionIndex()
    {
        $model = new ImportProduct();
        return $this->render('import', ['model' => $model]);
    }

    public function actionDownloadSample()
    {
        $page1                     = [
            [
                'title',
                'category',
                'image',
                'min_qty',
                'price',
                'description',
            ],
            [
                'Best cup',
                'Shoes & Accessories \ Other Footwear \ Boots',
                '',
                1,
                10.18,
                'A cup is an open-top container used to hold or cold liquids for pouring or drinking; while mainly used for drinking, it also can be used to store solids for pouring (e.g., sugar, flour, grains, salt).[1][2] Cups may be made of glass, metal, china,[3] clay, wood, stone, polystyrene, plastic, aluminium or other materials, and are usually fixed with a stem, handles, or other adornments.'
            ]
        ];
        $productCategoryRepository = Yii::createObject(ProductCategoryRepository::class);
        $categoriesMap             = $productCategoryRepository->getFinalMap();
        $page2                     = [
            [
                'Category name'
            ],
        ];
        foreach ($categoriesMap as $category) {
            $page2[] = [
                $category
            ];
        }

        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('Paid');
        $drawing->setDescription('Paid');
        $drawing->setPath(Yii::getAlias('@frontend/web/static/images/cat2.png'));
        $drawing->setCoordinates('C2');
        $drawing->getShadow()->setVisible(true);


        $spreadsheet = new Spreadsheet();
        $sheet       = $spreadsheet->getActiveSheet();
        $sheet->fromArray($page1);
        $drawing->setWorksheet($sheet);
        $sheet->getColumnDimension('A')->setWidth(120, 'px');
        $sheet->getColumnDimension('B')->setWidth(320, 'px');
        $sheet->getColumnDimension('C')->setWidth(320, 'px');
        $sheet->getColumnDimension('F')->setWidth(320, 'px');
        $spreadsheet->getActiveSheet()->getRowDimension('2')->setRowHeight(300);

        $categoriesSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'Categories');
        $spreadsheet->addSheet($categoriesSheet, 1);
        $categoriesSheet->fromArray($page2);


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="testImportProduct.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = new Xlsx($spreadsheet);
        $writer->setPreCalculateFormulas(false);
        $dir = \Yii::getAlias('@frontend/runtime/export/');
        FileDirHelper::createDir($dir);
        $path = $dir . 'excel_' . date('Y_m_d_H_i_s_') . random_int(0, 100000) . '.xls';
        if (file_exists($path)) {
            unlink($path);
        }
        $writer->save($path);
        return file_get_contents($path);
    }

    public function actionUpload()
    {
        $importModel = new ImportProduct();
        $importModel->load(app('request')->post());
        $importModel->importFile = UploadedFile::getInstance($importModel, 'importFile');
        if ($importModel->importFile && $importModel->validate()) {
            $importModel->importFile->saveAs($importModel->getFilePath());
            $key          = $this->importService->saveToSession($importModel);
            $importReport = $this->importService->import($importModel);
            $this->setFlashMsg(true, $importReport->getString());
            return $this->redirect('/product/import');
            //return $this->redirect('/product/import/step2?session=' . $key);
        } else {
            $this->setFlashMsg(false, Html::errorSummary($importModel));
            return $this->redirect('/product/import');
        }

    }

    /**
     * @param $session
     * @return \yii\web\Response
     */
    public function actionUpdateStep($session)
    {
        $importModel = $this->importService->loadFromSession($session);
        $importModel->load(app('request')->post());
        if (!$importModel->validate()) {
            $this->setFlashMsg(false, Html::errorSummary($importModel));
        } else {
            $session = $this->importService->updateToSession($session, $importModel);
        }
        // start import without step2 :)

        $this->importService->import($importModel);

        return $this->redirect('/product/import');
        //return $this->redirect('/product/import/step2?session=' . $session);
    }

    public function actionStep2($session)
    {
        $importProduct = $this->importService->loadFromSession($session);
        $importColumns = $this->importService->getColumns($importProduct);
        return $this->render(
            'import2',
            [
                'session'       => $session,
                'importProduct' => $importProduct,
                'importColumns' => $importColumns
            ]
        );
    }
}