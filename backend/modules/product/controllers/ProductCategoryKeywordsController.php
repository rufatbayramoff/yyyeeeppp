<?php

namespace backend\modules\product\controllers;
use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\ProductCategoryKeywordSearch;
use common\models\ProductCategoryKeyword;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\services\ProductService;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Class ProductController
 *
 * @package backend\controllers\store
 */
class ProductCategoryKeywordsController extends AdminController
{
    /** @var ProductRepository */
    public $productRepository;

    /**
     * @var ProductService
     */
    public $productService;

    public function injectDependencies(ProductRepository $productRepository, ProductService $productService)
    {
        $this->productRepository = $productRepository;
        $this->productService = $productService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    //                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('storeunit.view');
        return parent::beforeAction($action);
    }

    /**
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductCategoryKeywordSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    protected function savePost($productCategoryKeyword)
    {
        if (Yii::$app->request->isPost) {
            $productCategoryKeyword->load(Yii::$app->request->post());
            if ($productCategoryKeyword->save()) {
                Yii::$app->getSession()->setFlash('success', 'Product saved', false);
                $this->redirect('/product/product-category-keywords');
                Yii::$app->end();
            }
        }
    }

    public function actionCreate()
    {
        $productCategoryKeyword = new ProductCategoryKeyword();
        $productCategoryKeyword->is_active = 1;
        $this->savePost($productCategoryKeyword);

        return $this->render(
            'create',
            [
                'productCategoryKeyword' => $productCategoryKeyword,
            ]
        );
    }

    /**
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $productCategoryKeyword = ProductCategoryKeyword::tryFindByPk($id);

        $this->savePost($productCategoryKeyword);

        return $this->render(
            'update',
            [
                'productCategoryKeyword' => $productCategoryKeyword,
            ]
        );

    }

    public function actionDelete($id)
    {
        $productCategoryKeyword = ProductCategoryKeyword::tryFindByPk($id);
        $productCategoryKeyword->delete();
        $this->setFlashMsg(true,'Was deleted');
        $this->redirect('/product/product-category-keywords');
    }
}
