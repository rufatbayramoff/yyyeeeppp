<?php

namespace backend\modules\product\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\Model3dHistorySearch;
use backend\models\search\ProductSearch;
use common\components\ArrayHelper;
use common\components\exceptions\BusinessException;
use common\components\FileTypesHelper;
use common\models\factories\FileFactory;
use common\models\Product;
use common\models\ProductFile;
use common\models\ProductMainCard;
use common\modules\product\components\ProductRejectForm;
use common\modules\product\populators\ProductPopulator;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\services\ProductService;
use console\jobs\model3d\RenderJob;
use console\jobs\QueueGateway;
use frontend\controllers\actions\TagsListAction;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Class ProductController
 *
 * @package backend\controllers\store
 */
class ProductController extends AdminController
{
    /** @var ProductRepository */
    public $productRepository;

    /** @var FileFactory */
    public $fileFactory;

    /** @var ProductPopulator */
    public $productPopulator;

    /**
     * @var ProductService
     */
    public $productService;

    public function injectDependencies(ProductRepository $productRepository, FileFactory $fileFactory, ProductPopulator $productPopulator, ProductService $productService)
    {
        $this->productRepository = $productRepository;
        $this->fileFactory       = $fileFactory;
        $this->productPopulator  = $productPopulator;
        $this->productService    = $productService;
    }

    public function actions()
    {
        return array_merge(parent::actions(),
            [
                'tags' => [
                    'class' => TagsListAction::class
                ]
            ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    //                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('storeunit.view');
        return parent::beforeAction($action);
    }

    /**
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @param Product $product
     * @param array $post
     * @throws \Exception
     */
    public function saveProduct(Product $product, array $post)
    {
        $this->productPopulator
            ->populate($product, $post)
            ->populateFiles($product, 'images')
            ->populateAdminForm($product, $post);
        if ($product->validate()) {
            $this->productRepository->saveProduct($product);
            return true;
        }
        return false;
    }


    /**
     * @param string $uuid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($uuid)
    {
        $product = $this->findModel($uuid);

        if (Yii::$app->request->isPost) {
            if ($this->saveProduct($product, Yii::$app->request->post())) {
                Yii::$app->getSession()->setFlash('success', 'Product saved', false);
                return $this->redirect('/product/product/update?uuid='.$product->uuid);
            }
        }
        return $this->render(
            'update',
            [
                'product' => $product,
            ]
        );

    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @throws \yii\base\UserException
     */
    public function actionApprove($uuid)
    {
        AdminAccess::validateAccess('product.approve');
        $product = $this->findModel($uuid);
        $this->productService->moderatorApprove($product);
        $this->setFlashMsg(true, 'Product approved!');
        return $this->redirect(app('request')->referrer);
    }

    /**
     *
     * @transacted
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\UserException
     */
    public function actionReject($uuid)
    {
        AdminAccess::validateAccess('product.reject');
        $model       = $this->findModel($uuid);
        $rejectForm  = new ProductRejectForm();
        $currentUser = $this->getCurrentUser();

        $post = app('request')->post();
        if ($post) {
            $rejectForm->load($post);
            $rejectForm->setUser($currentUser);
            $this->productService->moderatorReject($model, $rejectForm);
            app('cache')->set('back.notify.data', '');
            return $this->jsonSuccess(['reload' => true]);
        }

        return $this->renderAdaptive(
            '@backend/views/ps/ps/reject.php',
            [
                'model'      => $model,
                'rejectForm' => $rejectForm
            ]
        );
    }

    public function actionRenderCadFile($id)
    {
        AdminAccess::validateAccess('product.approve');
        $productFile = ProductFile::tryFindByPk($id);
        if (!FileTypesHelper::isRenderable($productFile->file)) {
            throw new BusinessException('Is not renderable');
        }
        $rJob = RenderJob::create($productFile->file);
        $fileJob = QueueGateway::addFileJob($rJob);
        return $this->redirect('/model3d/jobs/view?id='.$fileJob->id);
    }

    public function actionDeleteFile($id)
    {
        AdminAccess::validateAccess('product.approve');
        $productFile = ProductFile::tryFindByPk($id);
        $product = $productFile->product;
        $this->productRepository->deleteProductFile($productFile);
        return $this->redirect('/product/product/update?uuid='.$product->uuid);
    }

    public function actionMain($uuid, $type)
    {
        AdminAccess::validateAccess('product.approve');
        $model       = $this->findModel($uuid);
        $productMainCard = ProductMainCard::createProduct(
            $model->productCommon->uid,
            $type
        );
        $productMainCard->safeSave();
        return $this->redirect('/product/product/update?uuid='.$model->uuid);
    }
    

    /**
     * Finds the WikiMachine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param Product $uuid
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($uuid)
    {
        if (($model = Product::tryFind(['uuid' => $uuid])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
