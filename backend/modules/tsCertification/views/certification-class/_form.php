<?php

use common\modules\company\repositories\CompanyServiceTypesRepository;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Payment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput([]) ?>
    <?= $form->field($model, 'description')->textarea() ?>
    <?= $form->field($model, 'service_type')->dropDownList(CompanyServiceTypesRepository::getLabels(), ['readonly' => true]) ?>
    <?= $form->field($model, 'price')->textInput() ?>
    <?= $form->field($model, 'currency')->textInput(['readonly' => true]) ?>
    <?= $form->field($model, 'max_orders_count')->textInput() ?>
    <?= $form->field($model, 'max_order_price')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
