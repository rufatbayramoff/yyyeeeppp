<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $tsCertificationClass \common\models\TsCertificationClass */

$this->title = 'Create certification class';
$this->params['breadcrumbs'][] = ['label' => 'Certification class list', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="certification-class-create">
    <?= $this->render('_form', [
        'model' => $tsCertificationClass,
    ]) ?>
</div>
