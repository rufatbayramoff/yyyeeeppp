<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $tsCertificationClass \common\models\TsCertificationClass */

$this->title                   = 'Update certification class';
$this->params['breadcrumbs'][] = ['label' => 'Certification class list', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="certification-class-update">
    <?= $this->render('_form', [
        'model' => $tsCertificationClass,
    ]) ?>
    <?php
     if ($tsCertificationClass->isForPrinters()) {
         echo $this->render('printers-select', ['tsCertificationClass' => $tsCertificationClass]);
     }
    ?>
</div>
