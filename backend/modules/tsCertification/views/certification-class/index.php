<?php

use common\models\MsgReport;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = "Certification  class";
?>
    <p>
        <?= Html::a('Create class', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns'      => [
        'id',
        'title',
        'description',
        'service_type',
        'price',
        'currency',
        'max_orders_count',
        'max_order_price',
        ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
    ]
]); ?>