<?php

use common\modules\company\repositories\CompanyServiceTypesRepository;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \common\models\TsCertificationClass $tsCertificationClass */
?>

<div class="row">
    <div class="col-md-12">
        Printers:
    </div>
    <?php
    if ($tsCertificationClass->printers) {
        foreach ($tsCertificationClass->printers as $printer) {
            ?>
            <div class="col-md-3">
                <a href="/ps/printer/view?id=<?= $printer->id ?>"><?= H($printer->id . ' ' . $printer->firm . ' ' . $printer->title . ' ' . $printer->technology->code) ?></a>
                <a href="printers-list-delete?id=<?= $tsCertificationClass->id ?>&printer_id=<?= $printer->id ?>" style="color:red" title="delete from list">x</a>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="col-md-12">Empty list</div>
        <?php
    }
    ?>
    <div class="col-md-12">
        <a href="printers-list-delete?id=<?= $tsCertificationClass->id ?>?>" style="color:red" title="delete all list">
            clear list
        </a>
    </div>
</div>


<br><br>
<?php $form = ActiveForm::begin(['action' => 'import-printers-list?id=' . $tsCertificationClass->id]); ?>
<div class="form-group field-tscertificationclass-description required has-success">
    <label class="control-label" for="tscertificationclass-description">Printers ids list for import</label>
    <textarea id="tscertificationclass-description" class="form-control" name="printers-import-list" aria-required="true" aria-invalid="false"></textarea>
</div>

<div class="form-group">
    <?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


