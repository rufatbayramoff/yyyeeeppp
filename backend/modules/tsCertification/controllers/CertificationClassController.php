<?php
/**
 * Created by mitaichik
 */

namespace backend\modules\tsCertification\controllers;


use backend\components\AdminAccess;
use backend\models\search\TsCertificationClassSearch;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\models\MsgMessage;
use common\models\MsgReport;
use common\models\MsgTopic;
use common\models\Printer;
use common\models\TsCertificationClass;
use common\modules\company\components\CompanyServiceInterface;
use common\modules\company\repositories\CompanyServiceTypesRepository;
use common\modules\message\services\MessageService;
use common\modules\payment\processors\PayPalPayoutProcessor;
use common\modules\payment\services\PaymentService;
use common\modules\tsCertification\services\TsCertificationService;
use lib\money\Currency;
use Yii;
use yii\data\ActiveDataProvider;

class CertificationClassController extends \backend\components\AdminController
{

    /** @var TsCertificationService */
    protected $tsCertificationService;

    public function injectDependencies(TsCertificationService $tsCertificationService)
    {
        $this->tsCertificationService = $tsCertificationService;
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('psmachine.view');
        return parent::beforeAction($action);
    }

    protected function tryUpdate($tsCertificationClass)
    {
        if (Yii::$app->request->isPost) {
            $tsCertificationClass->load(Yii::$app->request->post());
            if ($tsCertificationClass->validate()) {
                $tsCertificationClass->safeSave();
                $this->setFlashMsg(true, 'Saved');
                $this->redirect('index');
                Yii::$app->end();
            }
        }
    }


    public function actionCreate()
    {
        $tsCertificationClass               = Yii::createObject(TsCertificationClass::class);
        $tsCertificationClass->currency     = Currency::USD;
        $tsCertificationClass->service_type = CompanyServiceInterface::TYPE_PRINTER;
        $this->tryUpdate($tsCertificationClass);
        return $this->render('create', ['tsCertificationClass' => $tsCertificationClass]);
    }

    public function actionUpdate($id)
    {
        $tsCertificationClass = TsCertificationClass::tryFindByPk($id);
        $this->tryUpdate($tsCertificationClass);
        return $this->render('update', ['tsCertificationClass' => $tsCertificationClass]);
    }

    public function actionDelete($id)
    {
        $tsCertificationClass = TsCertificationClass::tryFindByPk($id);
        if ($tsCertificationClass->id <= TsCertificationClass::MAX_SYSTEM_ID) {
            $this->setFlashMsg(false, 'Is system');
            return $this->redirect('index');
        }
        $tsCertificationClass->delete();
        $this->setFlashMsg(true, 'Deleted');

        return $this->redirect('index');
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $tsCertificationClass = Yii::createObject(TsCertificationClassSearch::class);
        $dataProvider         = $tsCertificationClass->search(Yii::$app->request->queryParams);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionPrintersListDelete($id)
    {
        $tsCertificationClass = TsCertificationClass::tryFindByPk($id);
        if ($printerId = Yii::$app->request->get('printer_id')) {
            $printer = Printer::tryFindByPk($printerId);
            $this->tsCertificationService->deleteClassPrintersList($printer);
            $this->setFlashMsg(true, 'Deleted from list');
        } else {
            $this->tsCertificationService->clearClassPrintersList($tsCertificationClass);
            $this->setFlashMsg(true, 'Printers list is empty');
        }
        $this->redirect('update?id=' . $tsCertificationClass->id);
    }

    public function actionImportPrintersList($id)
    {
        $tsCertificationClass = TsCertificationClass::tryFindByPk($id);
        $printersList         = Yii::$app->request->post('printers-import-list');
        $this->tsCertificationService->importClassPrintersList($tsCertificationClass, $printersList);
        $this->setFlashMsg(true, 'Imported');
        $this->redirect('update?id=' . $tsCertificationClass->id);
    }
}