<?php

namespace backend\modules\tsCertification;

/**
 * Class Module
 * @package app\modules\support
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'backend\modules\tsCertification\controllers';

    /**
     *
     */
    public function init()
    {
        parent::init();
    }
}
