<?php

namespace backend\modules\statistic\components;
/**
 * Date: 06.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class CsvToExcelConverter
{
    /**
     * Read given csv file and write all rows to given xls file
     *
     * @param string $csvFile Resource path of the csv file
     * @param string $xlsFile Resource path of the excel file
     * @param string $encoding Encoding of the csv file, use utf8 if null
     * @throws Exception
     */
    public static function convert($csvFile, $xlsFile, $encoding=null) {
        //set cache
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod);

        //open csv file
        $objReader = new PHPExcel_Reader_CSV();
        $objReader->setDelimiter("\t");

        if ($encoding !== null)
            $objReader->setInputEncoding($encoding);
        $objPHPExcel = $objReader->load($csvFile);
        $inSheet = $objPHPExcel->getActiveSheet();

        $objPHPExcel = new PHPExcel();
        $outSheet = $objPHPExcel->getActiveSheet();

        $rowIndex = 0;
        foreach ($inSheet->getRowIterator() as $row) {
            $rowIndex++;
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            //column index start from 0
            $colIndex = -1;
            foreach ($cellIterator as $cell) {
                $colIndex++;
                $outSheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $cell->getValue());
            }
        }

        //write excel file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($xlsFile);
    }
}