<?php

namespace backend\modules\statistic\components;

use common\components\FileDirHelper;
use common\interfaces\Model3dBaseInterface;
use common\models\StoreOrder;
use yii\base\BaseObject;
use yii\base\UserException;
use ZipArchive;

class OrderFilesDownloader extends BaseObject
{
    public function formArchive($ordersList)
    {
        $orderIds = explode("\n", $ordersList);
        $orders = [];
        foreach ($orderIds as $orderId) {
            $order    = StoreOrder::find()->where(['id' => $orderId])->one();
            if ($order) {
                $orders[] = $order;
            }
        }
        if (!$orders) {
            throw new UserException('Empty orders  list');
        }

        $path = \Yii::getAlias('@runtime').'/orderFiles';
        FileDirHelper::createDir($path);
        $filePath = $path.'/'.date('Y-m-d_H-i-s').'.zip';
        $zip     = new ZipArchive();
        $zip->open($filePath, ZIPARCHIVE::CREATE);
        $filesExists = false;
        foreach ($orders as $order) {
            /** @var $model3d Model3dBaseInterface */
            if ($order->hasModel() && $model3d = $order->getFirstReplicaItem()) {
                foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
                    $path = $model3dPart->file->getLocalTmpFilePath();
                    if (file_exists($path)) {
                        $zip->addFile($path, $order->id.'/'.$model3dPart->file->getFileName());
                        $filesExists = true;
                    }
                }
            }
        }
        if (!$filesExists) {
            throw new UserException('No files exists');
        }
        $zip->close();
        return $filePath;
    }
}