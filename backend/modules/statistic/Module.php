<?php

namespace backend\modules\statistic;

/**
 * statistic module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\statistic\controllers';

    /**
     *
     */
    public $defaultRoute = 'reports';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
