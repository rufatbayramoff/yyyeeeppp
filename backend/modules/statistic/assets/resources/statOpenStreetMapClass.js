/**
 * Orders statistics on street map
 */
var statOpenStreetMapClass = {
    config: {
        printersInfo: {},
        mapElementId: null,
        basePosition: {
            'lon': -97.734,
            'lat': 37.962
        },
        minLengthKm: 200,
    },

    map: null,

    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);
        self.initializeMap();
        self.initializePrinters();
    },

    initializeMap: function () {
        var self = this;
        var height = $('.content-wrapper').height();
        var otherInfoHeight = $('#otherPrinterInfo').height();

        $('#' + self.config.mapElementId).height(height - otherInfoHeight);

        var map = new OpenLayers.Map(self.config.mapElementId);
        self.map = map;

        var mapnik = new OpenLayers.Layer.OSM();
        var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
        var toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection

        var position = new OpenLayers.LonLat(self.config.basePosition.lon, self.config.basePosition.lat).transform(fromProjection, toProjection);
        var zoom = 5;
        map.addLayer(mapnik);
        map.setCenter(position, zoom);
    },

    initializePrinters: function () {
        var self = this;
        var map = self.map;
        var markers = new OpenLayers.Layer.Markers("Markers");
        var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
        var toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection

        var vectorLayer = new OpenLayers.Layer.Vector("Overlay");
        map.addLayer(vectorLayer);
        map.addLayer(markers);


        var size = new OpenLayers.Size(20, 31);
        var size2 = new OpenLayers.Size(30, 31);
        var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
        for (var key in self.config.printersInfo) {
            var printerInfo = self.config.printersInfo[key];
            var icon = new OpenLayers.Icon('/img/3d-printer-icon-2.png', size, offset);
            var position = new OpenLayers.LonLat(printerInfo['lon'], printerInfo['lat']).transform(fromProjection, toProjection);
            var marker = new OpenLayers.Marker(position, icon);
            marker.printerInfo = printerInfo;
            marker.events.register('mousedown', marker, function (evt) {
                self.showPrinterInfo(this.printerInfo);
                OpenLayers.Event.stop(evt);
            });
            markers.addMarker(marker);

            if (printerInfo.orders) {
                for (var ordersKey in printerInfo.orders) {
                    var orderInfo = printerInfo.orders[ordersKey];
                    var icon = new OpenLayers.Icon('/img/3d-printer-icon-3.png', size2, offset);
                    var position2 = new OpenLayers.LonLat(orderInfo['lon'], orderInfo['lat']).transform(fromProjection, toProjection);
                    var marker = new OpenLayers.Marker(position2, icon);
                    marker.printerInfo = printerInfo;
                    orderInfo['printerTitle'] = printerInfo['title'];
                    marker.orderInfo = orderInfo;
                    marker.events.register('mousedown', marker, function (evt) {
                        self.showPrinterInfo(this.orderInfo);
                        OpenLayers.Event.stop(evt);
                    });
                    markers.addMarker(marker);

                    var points = [
                        new OpenLayers.Geometry.Point(printerInfo['lon'], printerInfo['lat']).transform(fromProjection, toProjection),
                        new OpenLayers.Geometry.Point(orderInfo['lon'], orderInfo['lat']).transform(fromProjection, toProjection)
                    ];
                    var line = new OpenLayers.Geometry.LineString(points);
                    var distance = self.getDistanceFromLatLonInKm(printerInfo['lat'], printerInfo['lon'], orderInfo['lat'], orderInfo['lon']);
                    var lineColor = '#fbff24';
                    if (distance < self.config.minLengthKm) {
                        lineColor = '#ff0200';
                    }
                    var style = {strokeColor: lineColor, strokeWidth: 3};
                    var feature = new OpenLayers.Feature.Vector(line, {}, style);
                    vectorLayer.addFeatures(feature);
                }
            }

        }
        map.addLayer(new OpenLayers.Layer.OSM());
    },

    showPrinterInfo: function (currentPrinterInfo) {
        var self = this;
        var json = commonJs.jsonSyntaxHighlight(currentPrinterInfo);
        $('#currentPrinterInfo').html('<pre>'+json+'</pre>');
    },

    getDistanceFromLatLonInKm: function (lat1, lon1, lat2, lon2) {
        var self = this;

        var R = 6371; // Radius of the earth in km
        var dLat = self.deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = self.deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(self.deg2rad(lat1)) * Math.cos(self.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    },

    deg2rad: function (deg) {
        return deg * (Math.PI / 180)
    }
}
