<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.04.17
 * Time: 11:11
 */

namespace backend\modules\statistic\assets;


use yii\web\AssetBundle;

/**
 * @package app\modules\support
 */
class StatOpenStreetMapAssets extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@backend/modules/statistic/assets/resources';

    /**
     * @var array
     */
    public $js = [
        'statOpenStreetMapClass.js',
    ];
}