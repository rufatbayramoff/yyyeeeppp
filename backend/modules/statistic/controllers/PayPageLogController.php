<?php

namespace backend\modules\statistic\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\PaymentPayPageLogSearch;
use common\components\ps\locator\PrinterRepository;
use common\models\PaymentPayPageLog;
use common\models\PsPrinter;
use common\models\StoreOrderAttemp;
use common\models\UserAddress;
use common\modules\psPrinterListMaps\components\ColorsMapCalculator;
use common\modules\psPrinterListMaps\components\PsPrinterSizesMapCalculator;
use Yii;

class PayPageLogController extends AdminController
{

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('statistic.pay_page_log');
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $searchModel  = new PaymentPayPageLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function actionView($id)
    {
        $payLog = PaymentPayPageLog::tryFind(['uuid' => $id]);
        return $this->render(
            'view',
            [
                'payLog' => $payLog,
            ]
        );
    }
}
