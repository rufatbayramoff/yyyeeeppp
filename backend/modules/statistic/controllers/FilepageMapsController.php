<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.17
 * Time: 11:30
 */

namespace backend\modules\statistic\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use common\components\ps\locator\PrinterRepository;
use common\models\PsPrinter;
use common\models\StoreOrderAttemp;
use common\models\UserAddress;
use common\modules\psPrinterListMaps\components\ColorsMapCalculator;
use common\modules\psPrinterListMaps\components\PsPrinterSizesMapCalculator;
use Yii;
use yii\db\Expression;

class FilepageMapsController extends AdminController
{

    public $defaultAction = 'location';

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        if(!is_guest()) AdminAccess::validateAccess('statistic.view_filepage_maps');
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function actionLocation()
    {
        $printers = [];
        /** @var PsPrinter[] $allActivePsPrinters */
        $allActivePsPrinters = PrinterRepository::getCommonQuery()->innerJoinWith(['companyService.storeOrderAttemps'])->all();
        foreach ($allActivePsPrinters as $activePsPrinter) {
            $orders = [];
            foreach ($activePsPrinter->companyService->storeOrderAttemps as $attemp) {
                if (in_array(
                    $attemp->status,
                    [
                        StoreOrderAttemp::STATUS_PRINTED,
                        StoreOrderAttemp::STATUS_PRINTING,
                        StoreOrderAttemp::STATUS_SENT,
                        StoreOrderAttemp::STATUS_DELIVERED,
                        StoreOrderAttemp::STATUS_RECEIVED
                    ],
                    true
                )) {
                    if ($attemp->order->shipAddress->lat) {
                        $addressKey = $attemp->order->shipAddress->country->title . ', ' . $attemp->order->shipAddress->region . ', ' . $attemp->order->shipAddress->city . ', ' . $attemp->order->shipAddress->address . ', ' . $attemp->order->shipAddress->zip_code;
                        $orders[$addressKey] = [
                            'id'      => $attemp->order->id,
                            'address' => UserAddress::prepareAddressData($attemp->order->shipAddress),
                            'lat'     => $attemp->order->shipAddress->lat,
                            'lon'     => $attemp->order->shipAddress->lon,
                            'date'    => $attemp->created_at
                        ];
                    }
                }
            }
            $printers[$activePsPrinter->id] = [
                'lat'    => $activePsPrinter->companyService->location->lat,
                'lon'    => $activePsPrinter->companyService->location->lon,
                'title'  => $activePsPrinter->ps->title . ' - ' . $activePsPrinter->title,
                'orders' => $orders
            ];
        }
        return $this->render(
            'location',
            [
                'printersInfo' => $printers
            ]
        );
    }

    /**
     * @return string
     */
    public function actionSizes()
    {
        return $this->render('sizes');
    }

    /**
     * @return string
     */
    public function actionWeight()
    {
        return $this->render('weight');
    }

    public function actionCalculate()
    {
        $colors = include Yii::getAlias('@common/config/filepageMaps/last-colors-map.php');
        $locations = include Yii::getAlias('@common/config/filepageMaps/last-locations-map.php');
        $sizes = include Yii::getAlias('@common/config/filepageMaps/last-sizes-map.php');
        $weight = include Yii::getAlias('@common/config/filepageMaps/last-weight-map.php');
        $countWeight = count($weight);
        $isPublicCnt = 2;
        $commonInfo = [];
        foreach ($colors as $groupName => $colorsGroup) {
            $countColors = count($colors[$groupName]);
            $countLocations = count($locations[$groupName]);
            $countSizes = count($sizes[$groupName]);
            $commonInfo[$groupName] = $countColors * $countLocations * $countSizes * $countWeight * $isPublicCnt;
        }
        arsort($commonInfo);
        return $this->render(
            'calculate',
            [
                'commonInfo' => $commonInfo
            ]
        );
    }

    /**
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function actionColors()
    {
        /** @var ColorsMapCalculator $colorMapCalculator */
        $colorMapCalculator = \Yii::createObject(ColorsMapCalculator::class);
        $orderColors = $colorMapCalculator->formOrdersColors();
        return $this->render(
            'colors',
            [
                'orderColors' => $orderColors,
            ]
        );
    }
}