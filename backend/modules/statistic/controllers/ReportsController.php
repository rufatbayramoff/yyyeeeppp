<?php

namespace backend\modules\statistic\controllers;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\report\AffiliateAwardReport;
use backend\models\report\AttemptDeclineReason;
use backend\models\report\BraintreeTransactionsReport;
use backend\models\report\CompaniesReport;
use backend\models\report\FeeReport;
use backend\models\report\Model3dReport;
use backend\models\report\NewTestOrdersUsers;
use backend\models\report\OrdersDeliveryReport;
use backend\models\report\OrdersFiles;
use backend\models\report\OrdersWithBlockMessages;
use backend\models\report\OrderWeekReport;
use backend\models\report\ProfitSourcesReport;
use backend\models\report\PsPrinterLocationReport;
use backend\models\report\PsPrinterWeekReport;
use backend\models\report\ThingBalanceReport;
use backend\models\report\ThingUsersReport;
use backend\models\report\UserEmailsReport;
use backend\models\report\UsersOrdersReport;
use backend\models\report\UserWeekReport;
use backend\modules\statistic\components\OrderFilesDownloader;
use backend\modules\statistic\reports\BaseReport;
use backend\modules\statistic\reports\impl\TaxStatisticReport;
use backend\modules\statistic\reports\mailchimp\Report;
use backend\modules\statistic\reports\ReportCsvWriter;
use backend\modules\statistic\reports\ReportDescription;
use backend\modules\statistic\reports\ReportExcelWriter;
use lib\geo\GeoService;
use Yii;

/**
 * Default controller for the `statistic` module
 */
class ReportsController extends AdminController
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (!is_guest()) AdminAccess::validateAccess('statistic.view');
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['reports' => $this->getAllowedReports()]);
    }

    public function actionFeeReport()
    {
        return $this->render('fee-report', []);
    }

    public function actionFilesReport()
    {
        return $this->render('files-report', []);
    }

    public function actionFilesReportDownload()
    {
        $orderFilesDownloader   = Yii::createObject(OrderFilesDownloader::class);
        $path                   = $orderFilesDownloader->formArchive(Yii::$app->request->get('orders-list'));
        $this->response->format = \yii\web\Response::FORMAT_RAW;
        $this->response->sendFile($path, 'order_files_' . date('Y-m-d_H-i-s') . '.zip', ['inline' => 1]);
    }

    public function actionFeeReportDownload(GeoService $geoService)
    {
        $report = FeeReport::create();
        $report->setGeoService($geoService);
        $report->setOrdersList(\Yii::$app->request->get('orders-list'));
        $report->setDate(\Yii::$app->request->get('date'));
        $report->setStatus(\Yii::$app->request->get('status'));
        $items = $report->getItems();
        if ($items === 'RELOAD') {
            $url = \yii\helpers\Url::toRoute([
                '/statistic/reports/fee-report-download',
                'orders-list' => \Yii::$app->request->get('orders-list'),
                'date'        => \Yii::$app->request->get('date'),
                'status'      => Yii::$app->request->get('status'),
                'p'           => date('Y-m-d_h-i-s')
            ]);
            return $this->redirect($url);
        }

        header('Content-Disposition: attachment;filename="FeeReport' . date('Y-m-d_H-i-s') . '.csv"');
        $report->formCsv();
        exit;
    }


    public function ordersFilesReport()
    {
        $report = OrdersFiles::create();
        header('Content-Disposition: attachment;filename="FilesReports' . date('Y-m-d_H-i-s') . '.csv"');
        $report->formCsv();
        exit;
    }

    /**
     * @param $reportId
     * @return string
     */
    public function actionDownload($reportId)
    {
        $this->checkAccess($reportId);

        if ($reportId == 'orders_files') {
            return $this->ordersFilesReport();
        }

        $reportDescription = $this->getAllowedReports()[$reportId];
        $reportClass       = $reportDescription->class;
        /** @var BaseReport $report */
        $report = $reportClass::create();

        $reportWriter = new ReportCsvWriter($reportDescription->name);
        return $reportWriter->write($report);
    }

    /**
     * @return ReportDescription[]
     */
    private function getAllowedReports()
    {
        $reports = [
            'mailchimp'                     => new ReportDescription('mailchimp', 'Mailchimp report', Report::class),
            'user_emails_report'            => new ReportDescription('user_emails_report', 'All users emails', UserEmailsReport::class),
            'ps_printer_week'               => new ReportDescription('ps_printer_week', 'Ps Printer Week report', PsPrinterWeekReport::class),
            'model3d'                       => new ReportDescription('model3d', 'Model3d report', Model3dReport::class),
            'ps_printer_location'           => new ReportDescription('ps_printer_location', 'Ps Printer Location report', PsPrinterLocationReport::class),
            'users_week'                    => new ReportDescription('users_week', 'Users Week report', UserWeekReport::class),
            'orders_week'                   => new ReportDescription('orders_week', 'Orders Week report', OrderWeekReport::class),
            'new_test_orders_users'         => new ReportDescription('new_test_orders_users', "New test orders users", NewTestOrdersUsers::class),
            'orders_block_messages'         => new ReportDescription('orders_block_messages', "Orders with block messages", OrdersWithBlockMessages::class),
            'attrmpt_decline_reason'        => new ReportDescription('attrmpt_decline_reason', "Attemps delcline reasons statistic", AttemptDeclineReason::class),
            'users_orders'                  => new ReportDescription('users_orders', "List of users with orders info", UsersOrdersReport::class),
            'orders_delivery'               => new ReportDescription('orders_delivery', "List of orders with delivery info", OrdersDeliveryReport::class),
            'thingiverse_report'            => new ReportDescription('thingiverse_report', "Thingiverse orders", ThingBalanceReport::class),
            'thingiverse_users_report'      => new ReportDescription('thingiverse_users_report', "Thingiverse users", ThingUsersReport::class),
            'profit_sources_report'         => new ReportDescription('profit_sources_report', "Profit sources", ProfitSourcesReport::class),
            'braintree_transactions_report' => new ReportDescription('braintree_transactions_report', 'Braintree transactions report', BraintreeTransactionsReport::class),
            'orders_files'                  => new ReportDescription('orders_files', 'Orders files', OrdersFiles::class),
            'week_affiliates_report'        => new ReportDescription('week_affiliates_report', 'Week Affiliates report', AffiliateAwardReport::class),
            'companies_report'        => new ReportDescription('companies_report', 'Companies report', CompaniesReport::class)
        ];

        if (AdminAccess::can('statistic.view.tax')) {
            $reports['tax_statistic'] = new ReportDescription('tax_statistic', "Tax statistic", TaxStatisticReport::class);
        }

        return $reports;
    }


    private function checkAccess(string $reportId)
    {
        if ($reportId == 'tax_statistic') {
            AdminAccess::validateAccess('statistic.view.tax');
        }
    }

}
