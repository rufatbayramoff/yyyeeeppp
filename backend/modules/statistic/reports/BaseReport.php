<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace backend\modules\statistic\reports;


use common\components\exceptions\AssertHelper;
use ReflectionClass;
use yii\helpers\Inflector;

/**
 * Class BaseReport
 * @package backend\modules\statistic\reports
 */
abstract class BaseReport implements BaseReportInterface
{
    /**
     * @var BaseReportItem[]
     */
    private $items;

    /**
     * @var \DateTime
     */
    private $createdTime;

    protected $getParams;

    /**
     * BaseReport constructor.
     * @param \DateTime $createdTime
     * @param array     $items
     */
    public function __construct(\DateTime $createdTime, array $items)
    {
        AssertHelper::assertInstances($items, BaseReportItem::class);

        $this->createdTime = $createdTime;
        $this->items = $items;
    }

    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return string[]
     */
    public function getColumnsNames()
    {
        $class = new ReflectionClass($this->getItemClass());
        $columnNames = [];
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property)
        {
            if (!$property->isStatic())
            {
                $column = $property->getName();
                $columnNames[$column] =  Inflector::camel2words($column);
            }
        }

        return $columnNames;
    }

    public function setParams(array $params)
    {
        $this->getParams = $params;
    }

    /**
     * Return item class
     * @return string
     */
    abstract public function getItemClass();
}