<?php
/**
 * Date: 06.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\modules\statistic\reports;


use common\components\FileDirHelper;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use yii\helpers\Json;

class ReportExcelWriter implements ReportWriterInterface
{

    protected $filename;

    public function __construct($filename = 'report')
    {
        $date = new \DateTime('now');
        $this->filename = $filename . '_' . $date->format('Y-m-d H:i');
    }

    /**
     * @param $filename
     */
    public function setFileName($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @param BaseReportInterface $report
     * @return string
     * @throws \PHPExcel_Exception
     */
    public function write(BaseReportInterface $report)
    {
        $columnsNames = $report->getColumnsNames();
        $columns = array_keys($columnsNames);

        $data = [];
        $data[] = array_values($columnsNames);
        foreach ($report->getItems() as $item) {
            $itemData = [];
            foreach ($columns as $column) {
                $val = $item->$column;
                if (is_array($val)) {
                    $val = Json::encode($val);
                }else{
                    $val = (string)$val;
                }
                $itemData[] = $val;
            }
            $data[] = $itemData;
        }

        return $this->writeExcel($data);
    }

    /**
     * $data - first row are columns, others - data
     *
     * @param array $data
     * @throws \yii\base\ErrorException
     */
    public function writeExcel(array $data)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray($data);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $this->filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = new Xlsx($spreadsheet);
        $writer->setPreCalculateFormulas(false);
        $dir = \Yii::getAlias('@frontend/runtime/export/');
        FileDirHelper::createDir($dir);
        $path = $dir . 'excel_' . date('Y_m_d_H_i_s_') . random_int(0, 100000) . '.xls';
        if (file_exists($path)) {
            unlink($path);
        }
        $writer->save($path);
        return file_get_contents($path);
    }
}