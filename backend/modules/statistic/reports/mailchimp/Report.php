<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace backend\modules\statistic\reports\mailchimp;

use backend\modules\statistic\reports\BaseReport;
use common\components\ArrayHelper;
use common\models\base\Ps;
use common\models\Model3d;
use common\models\Payment;
use common\models\PaymentDetail;
use common\models\PsPrinter;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreUnit;
use common\models\User;
use common\components\PaymentExchangeRateFacade;
use common\models\UserTaxInfo;
use common\modules\payment\services\PaymentService;
use lib\money\Currency;

/**
 * Class Report
 * @package backend\modules\statistic\reports\mailchimp
 */
class Report extends BaseReport
{
    /**
     * @return BaseReport
     */
    public static function create()
    {
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);
        $paymentService = \Yii::createObject(PaymentService::class);

        /** @var User[] $users */
        $usersQ = User::find()
            ->notSystem()
            ->andWhere(['status' => [User::STATUS_ACTIVE]])
            ->andWhere(['NOT IN', 'id', $userIds])
            ->with('ps', 'userProfile.address.country', 'userTaxInfo', 'userPaypal');
        debugSql($usersQ);exit;

        $users =$usersQ->all();

        $activePrintersCounts = ArrayHelper::map(PsPrinter::find()
            ->select('any_value(ps_printer.id) as id, ps.user_id userId, count(*) `count`')
            ->notDeleted(false)
            ->visible()
            ->moderated()
            ->groupBy('userId')
            ->asArray()
            ->all(), 'userId', 'count');

        $acceptedOrdersCounts = ArrayHelper::map(StoreOrderAttemp::find()
            ->select('ps.user_id userId, count(*) `count`')
            ->inStatus([
                StoreOrderAttemp:: STATUS_ACCEPTED,
                StoreOrderAttemp:: STATUS_PRINTING,
                StoreOrderAttemp:: STATUS_PRINTED ,
                StoreOrderAttemp:: STATUS_SENT,
                StoreOrderAttemp:: STATUS_DELIVERED,
                StoreOrderAttemp:: STATUS_RECEIVED,
            ])
            ->joinWith('machine.ps ps', false)
            ->groupBy('userId')
            ->asArray()
            ->all()
            , 'userId', 'count');

        $publishModelsCounts = ArrayHelper::index(Model3d::find()
            ->select('model3d.user_id userId, count(*) `count`, min(product_common.created_at) as `first_model`')
            ->joinWith('productCommon')
            ->published()
            ->groupBy('userId')
            ->asArray()
            ->all(), 'userId');

        $modelsInPayedOrdersCounts = ArrayHelper::map(StoreOrder::find()
            ->select('model.user_id userId, count(DISTINCT(model.id)) `count`')
            ->payed()
            ->joinWith('orderItem.unit.model3d model', false)
            ->groupBy('model.user_id')
            ->asArray()
            ->all(), 'userId', 'count');


        $reportItems = [];

        foreach($users as $user)
        {
            $reportItem = new ReportItem();

            /** @var Ps $ps */
            $ps = $user->ps ? ArrayHelper::first($user->ps) : null;

            /** @var StoreOrder[] $payedOrders */
            $payedOrders = StoreOrder::find()
                ->forUser($user)
                ->payed()
                ->all();

            $reportItem->userId = $user->id;
            $reportItem->userName = $user->username;
            $reportItem->userEmail = $user->email;
            if($user->userProfile->address)
            {
                $reportItem->userCountry = $user->userProfile->address->country->iso_code;
            }

            $reportItem->userTimezone = $user->userProfile->timezone_id ?: null;
            $reportItem->userRegDate = date('Y-m-d', $user->created_at);

            $reportItem->payedOrdersCount = count($payedOrders);
            $reportItem->totalPayedSumInUSD = self::getTotalPayedSumInUSD($payedOrders);

            $reportItem->havePs = (bool)$ps;
            if($ps)
            {
                $reportItem->psId  = $ps->id;
                $reportItem->psCreatedDate =  $ps->created_at;
                $reportItem->psPhone = '+'.$ps->phone_code.' '.$ps->phone;
                $reportItem->isPsPhoneVerifyed = $ps->phone_status == \common\models\Ps::PHONE_STATUS_CHECKED;
            }

            $reportItem->earnedAsPrintserviceMoneyInUSD = 0;//Payment::getAwardsAmount($user, [PaymentDetail::TYPE_AWARD_PRINT]);
            $reportItem->activePrintersCount = isset($activePrintersCounts[$user->id]) ? $activePrintersCounts[$user->id] : 0;
            $reportItem->acceptedOrdersCount = isset($acceptedOrdersCounts[$user->id]) ? $acceptedOrdersCounts[$user->id] : 0;

            $reportItem->earnedAsDesignerMoneyInUSD = 0;//Payment::getAwardsAmount($user, [PaymentDetail::TYPE_AWARD_MODEL]);
            $reportItem->publishedModelsCount = isset($publishModelsCounts[$user->id]) ? $publishModelsCounts[$user->id]['count'] : 0;
            if($reportItem->publishedModelsCount){
                $reportItem->userFirstModelDate = $publishModelsCounts[$user->id]['first_model'];
            }
            $reportItem->modelsInPayedOrdersCount = isset($modelsInPayedOrdersCounts[$user->id]) ? $modelsInPayedOrdersCounts[$user->id] : 0;
            $reportItem->incomBalanceInUSD = 0; //$paymentAccountService->getUserMainAmount($user);
            $reportItem->isPaypalAuthorized = (bool)$user->userPaypal;
            $reportItem->isFilledTaxes = $user->userTaxInfo && $user->userTaxInfo->status != UserTaxInfo::STATUS_DECLINE;

            $reportItems[] = $reportItem;
        }

        return new Report(new \DateTime(), $reportItems);
    }

    /**
     * @param StoreOrder[] $payedOrders
     * @return int
     */
    private static function getTotalPayedSumInUSD($payedOrders)
    {
        $sum = 0;
        foreach($payedOrders as $order)
        {
            $sum+= $order->getTotalPrice();
        }
        return $sum;
    }

    /**
     * Return item class
     * @return string
     */
    public function getItemClass()
    {
        return ReportItem::class;
    }
}