<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace backend\modules\statistic\reports\mailchimp;


use backend\modules\statistic\reports\BaseReportItem;
use common\models\User;

/**
 * Class ReportItem
 * @package backend\modules\statistic\reports\mailchimp
 *          
 *          Для рассылки писем нашим пользователям с сервиса mailchimp, а также для первичной отчетности, необходимо сделать экспорт списка пользователей.

Формат списка CSV, колонки:
ОБЩЕЕ/
- Имя пользователя
- Емайл пользователя
- Страна пользователя
- Часовой пояс пользователя
- дата его регистрации
ЗАКАЗИК/
- сколько сделал оплаченных заказов в штуках (включая обработанные и т.п.)
- общая сумма поступивших от него оплат
ПОСТАВЩИК/
- ФЛАГ есть ли принтсервис
- телефон принтсервиса в международном формате
- ФЛАГ подтвержден ли телефон
- сколько есть сейчас активных принтеров
- сколько принял заказов как принтер (за исключением тех которых отказался)
- сколько итого ему начислено как принтсервису
- сколько опубликовано сейчас активных моделей
- сколько его моделей попало в оплаченные заказы
- сколько ему начислено как автору
- общая сумма невыплачено на балансе (income)
- ФЛАГ авторизован ли через пайпал
- ФЛАГ заполнена ли налоговая форма
 *          
 */
class ReportItem extends BaseReportItem
{
    public $userId;
    /**
     * @var string
     */
    public $userName;

    /**
     * @var string
     */
    public $userEmail;

    /**
     * @var string
     */
    public $userCountry;

    /**
     * @var string
     */
    public $userTimezone;

    /**
     * @var string
     */
    public $userRegDate;

    // -- AS BUYER

    /**
     * @var int
     */
    public $payedOrdersCount;

    /**
     * @var float
     */
    public $totalPayedSumInUSD;

    // - AS PS

    /**
     * @var bool
     */
    public $havePs;

    public $psId;

    public $psCreatedDate;

    /**
     * @var string
     */
    public $psPhone;

    /**
     * @var bool
     */
    public $isPsPhoneVerifyed;

    /**
     * @var int
     */
    public $activePrintersCount;

    /**
     * @var int
     */
    public $acceptedOrdersCount;

    /**
     * @var float
     */
    public $earnedAsPrintserviceMoneyInUSD;

    // -- AS DESIGNER

    /**
     * @var int
     */
    public $publishedModelsCount;

    /**
     * @var int
     */
    public $modelsInPayedOrdersCount;
    
    /**
     * @var float
     */
    public $earnedAsDesignerMoneyInUSD;

    public $userFirstModelDate;

    // -- COMMON

    /**
     * @var float
     */
    public $incomBalanceInUSD;

    /**
     * @var bool
     */
    public $isPaypalAuthorized;

    /**
     * @var bool
     */
    public $isFilledTaxes;
}