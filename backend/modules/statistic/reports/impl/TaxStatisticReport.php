<?php
/**
 * Created by mitaichik
 */

namespace backend\modules\statistic\reports\impl;


use backend\modules\statistic\reports\BaseReportInterface;

/**
 * Class TaxStatisticReport
 *
 * @package backend\modules\statistic\reports\impl
 */
class TaxStatisticReport implements BaseReportInterface
{
    /**
     * @return TaxStatisticReport
     */
    public static function create()
    {
        return new self;
    }

    /**
     * @return array
     */
    public function getColumnsNames()
    {
        return [
            'user_id'               => 'User Id',
            'username'              => 'Username',
            'paypal_id'             => 'Paypal Id',
            'transaction_datetime'  => 'Transaction Datetime',
            'payout_amount'         => 'Payout Amount',
            'tax_amount'            => 'Tax Amount',
            'paypal_transaction_id' => 'Paypal Transaction ID',
            'transaction_status'    => 'Transaction Status',
            'tax_status'            => 'Tax Status',
        ];
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $data = \Yii::$app->db->createCommand(
            " SELECT 
                user.id as user_id,
                user.username,
                user_paypal.email as paypal_id,
                d1.updated_at as transaction_datetime,
                ABS(d1.amount) as payout_amount,
                ABS(d2.amount) AS tax_amount,
                d1.transaction_id,
                trans.transaction_id as paypal_transaction_id,
                trans.status as transaction_status,
                ut.status as tax_status 
            FROM payment_detail AS d1
            LEFT JOIN payment_transaction AS trans ON trans.id=d1.transaction_id
            LEFT JOIN payment_detail AS d2 ON d1.payment_id = d2.payment_id
            LEFT JOIN `user` ON `user`.id = d1.user_id
            LEFT JOIN user_paypal ON user_paypal.user_id = user.id
            LEFT JOIN user_tax_info ut ON ut.user_id=user.id
            WHERE d1.type = 'payout' AND d2.type = 'tax' AND d1.amount < 0 AND  d2.amount < 0;"
        )->queryAll(\PDO::FETCH_CLASS);

        return $data;
    }

    public function setParams(array $params)
    {
        // TODO: Implement setParams() method.
    }
}