<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace backend\modules\statistic\reports;

use common\components\FileDirHelper;

/**
 * Class ReportCsvWriter
 * @package backend\modules\statistic\reports
 */
class ReportCsvWriter implements ReportWriterInterface
{
    protected $filename;

    public function __construct($filename = 'report')
    {
        $date = new \DateTime('now');
        $this->filename = $filename . '_' . $date->format('Y-m-d H:i');
    }

    /**
     * @param BaseReportInterface $report
     * @return string
     */
    public function write(BaseReportInterface $report)
    {
        $columnsNames = $report->getColumnsNames();
        $columns = array_keys($columnsNames);

        $data = [];
        $data[] = array_values($columnsNames);

        foreach($report->getItems() as $item)
        {
            $itemData = [];

            foreach($columns as $column)
            {
                $itemData[] = $item->$column;
            }

            $data[] = $itemData;
        }

        return $this->arrayToCsv($data);
    }

    /**
     * @param array $data
     * @return string
     */
    private function arrayToCsv(array $data)
    {
        $delimiter = ',';
        $enclosure = '"';

        $dir = \Yii::getAlias('@frontend/runtime/export/');
        FileDirHelper::createDir($dir);
        $path = $dir . 'excel_' . date('Y_m_d_H_i_s_') . rand(0, 100000) . '.csv';

        $fp = fopen($path, 'w');
        foreach($data as $item)
        {
            fputcsv($fp, $item, $delimiter, $enclosure);
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $this->filename . '.csv"');
        header('Cache-Control: max-age=0');

        return file_get_contents($path);
    }
}