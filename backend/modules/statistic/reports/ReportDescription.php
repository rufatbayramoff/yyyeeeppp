<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace backend\modules\statistic\reports;

/**
 * Class ReportDescription
 * @package backend\modules\statistic\reports
 */
class ReportDescription
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $class;

    /**
     * ReportDescription constructor.
     * @param $id
     * @param $name
     * @param $class
     */
    public function __construct($id, $name, $class)
    {
        $this->name = $name;
        $this->id = $id;
        $this->class = $class;
    }
}