<?php
/**
 * Date: 26.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\modules\statistic\reports;


interface BaseReportInterface
{

    public static function create();


    public function getColumnsNames();

    /**
     * @return array
     */
    public function getItems();

    public function setParams(array $params);
}