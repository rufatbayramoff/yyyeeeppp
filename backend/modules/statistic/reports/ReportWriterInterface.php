<?php
/**
 * Date: 06.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\modules\statistic\reports;


interface ReportWriterInterface
{

    /**
     * @param BaseReportInterface $report
     * @return string
     */
    public function write(BaseReportInterface $report);
}