<?php
/** @var \backend\modules\statistic\reports\ReportDescription[] $reports */
use yii\helpers\Html;

?>

<table class="table">
    <?php foreach($reports as $report):?>
        <tr>
            <td><?= $report->name?></td>
            <td><?= Html::a('Download', ['/statistic/reports/download', 'reportId' => $report->id])?></td>
        </tr>
    <?php endforeach;?>
</table>

<a href="/statistic/reports/fee-report">Fee report for id orders list</a>
<br>
<a href="/statistic/reports/files-report">Files report for id orders list</a>