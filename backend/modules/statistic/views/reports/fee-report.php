<?php

use backend\models\report\FeeReport;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html; ?>

<?php $form = ActiveForm::begin([
    'action' => ['fee-report-download'],
    'method' => 'get'
]); ?>

Input here list of id orders:
<div class="form-group">
    <textarea name="orders-list"></textarea>
</div>
Date:
<div class="form-group">
    <select name="date">
        <option value="">All</option>
        <option value="2019">2019</option>
        <option value="2020">2020</option>
        <option value="2021">2021</option>
    </select>
</div>
Status:
<div class="form-group">
    <select name="status">
        <option value="">All</option>
        <option value="<?=FeeReport::STATUS_RECEIVED?>">Receivied</option>
    </select>
</div>
<div class="form-group">
    <?= Html::submitButton('Report', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


