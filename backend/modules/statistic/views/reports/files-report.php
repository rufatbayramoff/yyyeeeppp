<?php

use backend\models\report\FeeReport;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html; ?>

<?php $form = ActiveForm::begin([
    'action' => ['files-report-download'],
    'method' => 'get'
]); ?>

Input here list of id orders:
<div class="form-group">
    <textarea name="orders-list"></textarea>
</div>
<div class="form-group">
    <?= Html::submitButton('Report', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


