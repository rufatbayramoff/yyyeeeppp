<?php

use common\models\PaymentPayPageLog;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var \backend\models\search\PaymentPayPageLogSearch $searchModel */

$this->title                   = 'Pay page log';
$this->params['breadcrumbs'][] = $this->title;

?>
<div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'uuid',
            'created_at',
            'ip',
            'user_agent',
            'invoice_uuid',
            [
                'attribute' => 'order_id',
                'label'     => 'Order id',
                'format'    => 'raw',
                'value'     => function (PaymentPayPageLog $payLog) {
                    return '<a href="/store/store-order/view?id=' . $payLog->invoice->store_order_id . '">' . $payLog->invoice->store_order_id . '</a>';
                },
            ],
            [
                'attribute' => 'summ',
                'label'     => 'Summ',
                'value'     => function (PaymentPayPageLog $payLog) {
                    return $payLog->invoice->getAmountTotal();
                },
            ],
            [
                'label'     => 'Vendor',
                'value'     => function (PaymentPayPageLog $payLog) {
                    return $payLog->getLastVendor();
                },
            ],
            [
                'attribute' => 'status',
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    PaymentPayPageLog::STATUS_TEXTS,
                    ['class' => 'form-control', 'prompt' => 'All']),
                'value' => function (PaymentPayPageLog $pageLog) {
                    return $pageLog->getLastStatus();
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>
</div>

