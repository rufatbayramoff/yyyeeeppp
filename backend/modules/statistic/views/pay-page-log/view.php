<?php


use common\models\PaymentPayPageLog;
use common\models\PaymentTransaction;
use common\modules\payment\components\PaymentLogger;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $payLog \common\models\PaymentPayPageLog */

$this->title                   = $payLog->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Pay log', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-view">

    <h3>Pay page log</h3>

    <?= DetailView::widget([
        'model'      => $payLog,
        'attributes' => [
            'uuid',
            'created_at',
            'ip',
            'user_agent',
            'invoice_uuid',
            [
                'label'  => 'Order',
                'format' => 'raw',
                'value'  => '<a href="/store/store-order/view?id=' . $payLog->invoice->store_order_id . '">' . $payLog->invoice->store_order_id . '</a>'
            ],
            [
                'label' => 'Summ',
                'value' => $payLog->invoice->getAmountTotal()
            ],
            'vendor',
            [
                'label'  => 'Status',
                'format' => 'raw',
                'value'  => $payLog->getStatusHistory("<br>\n")
            ],
            [
                'label'  => 'Paylog',
                'format' => 'raw',
                'value'  => function (PaymentPayPageLog $payLog) {
                    $retVal = '';
                    foreach ($payLog->processes as $process) {
                        $retVal .= $process->date;
                        if ($process->process_err) {
                            $retVal .= '<pre>' . json_encode($process->process_err ?? '', JSON_PRETTY_PRINT) . '</pre>';
                        }
                        if ($process->process_payload) {
                            $retVal .= '<pre>' . json_encode($process->process_payload ?? '', JSON_PRETTY_PRINT) . '</pre>';
                        }
                    }
                    return $retVal;
                }
            ],
            [
                'label' => 'Process message',
                'value' => $payLog->process->process_message ?? ''
            ],
            [
                'label' => '3d secure',
                'value' => $payLog->is3dSecureFailed() ? 'Failed' : ''
            ]
        ],
    ]) ?>
    <h3>Internal payment log</h3>
    <?= GridView::widget([
        'dataProvider' => \common\models\PaymentLog::getDataProvider(
            ['like', 'search_markers', '"page_log_uuid": "' . $payLog->uuid . '"'],
            10000,
            ['sort' => ['defaultOrder' => ['id' => SORT_DESC]]]),
        'columns'      => [
            'id',
            'created_at',
            'level',
            'type',
            'comment:ntext',
            'info'           => [
                'attribute' => 'info',
                'format'    => 'raw',
                'value'     => function (\common\models\PaymentLog $paymentLog) {
                    return '<pre class="jsonHighlight small-text" style="max-height:350px;">' . json_encode($paymentLog->info, JSON_PRETTY_PRINT) . '</pre>';
                }
            ],
            'search_markers' => [
                'attribute' => 'search_markers',
                'format'    => 'raw',
                'value'     => function (\common\models\PaymentLog $paymentLog) {
                    $stripeVendorView = '';
                    if ($paymentLog->type===PaymentTransaction::VENDOR_STRIPE) {
                        $stripeVendorView = '<a href="https://dashboard.stripe.com/payments/'.$paymentLog->search_markers[PaymentLogger::MARKER_VENDOR_TRANSACTION_ID].'">View stripe transaction</a>';
                    }

                    return '<pre class="jsonHighlight small-text">' . json_encode($paymentLog->search_markers, JSON_PRETTY_PRINT) . '</pre>'.$stripeVendorView;
                }
            ],
        ],
    ]); ?>

</div>