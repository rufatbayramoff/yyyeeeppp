<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.17
 * Time: 13:55
 */

/** @var \yii\web\View $this */
/** @var \backend\modules\statistic\reports\ReportDescription[] $reports */

use backend\modules\statistic\assets\StatOpenStreetMapAssets;
use common\components\JsObjectFactory;
use frontend\assets\OpenLayersAsset;

echo $this->render('topPanel');

$this->registerAssetBundle(OpenLayersAsset::class);
$this->registerAssetBundle(StatOpenStreetMapAssets::class);
JsObjectFactory::createJsObject(
    'statOpenStreetMapClass',
    'statOpenStreetMapObj',
    [
        'mapElementId' => 'demoMap',
        'printersInfo' => $printersInfo
    ]
    ,
    $this
)
?>
<div id="demoMap" style="height:700px;"></div>
<style>
    .jsonHighlight { outline: 1px solid #ccc; padding: 5px; margin: 5px; }

    .jsonHighlight .string { color: green; }

    .jsonHighlight .number { color: darkorange; }

    .jsonHighlight .boolean { color: blue; }

    .jsonHighlight .null { color: magenta; }

    .jsonHighlight .key { color: red; }
</style>
<div id="currentPrinterInfo" class="jsonHighlight"></div>
<div id="otherPrinterInfo">
    <?php
    $path = Yii::getAlias('@common/config/filepageMaps/last-locations-map.php');
    if (!file_exists($path)) {
        echo "\n<br><br>\n" . $path . ' file not exists';
        return;
    }
    $colors = include $path;
    ?>
    <br><br>
    Last colors:
    <br>
    <pre class="jsonHighlight">
        <?= json_encode($colors, JSON_PRETTY_PRINT); ?>
    </pre>
</div>