<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.17
 * Time: 14:00
 */

/** @var \yii\web\View $this */

$elements = [
    '/statistic/filepage-maps/location'  => 'Location Maps',
    '/statistic/filepage-maps/sizes'     => 'Sizes Maps',
    '/statistic/filepage-maps/colors'    => 'Colors Maps',
    '/statistic/filepage-maps/weight'    => 'Weight Maps',
    '/statistic/filepage-maps/calculate' => 'Calculated combinations',
];


$actionId = Yii::$app->controller->action->id;

foreach ($elements as $key => $value) {
    if (strpos($key, $actionId)) {
        echo '<b>';
    }
    echo '<a href="' . $key . '">' . $value . '</a>';
    if (strpos($key, $actionId)) {
        echo '</b>';
    }
    if (next($elements) !== false) {
        echo ' / ';
    }
};
?>
