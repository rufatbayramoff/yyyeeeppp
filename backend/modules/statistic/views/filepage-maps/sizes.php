<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.17
 * Time: 13:55
 */

/** @var \backend\modules\statistic\reports\ReportDescription[] $reports */
use yii\helpers\Html;

echo $this->render('topPanel');
$path = Yii::getAlias('@common/config/filepageMaps/last-sizes-map.php');
if (!file_exists($path)) {
    echo "\n<br><br>\n" . $path . ' file not exists';
    return;
}
$sizes = include Yii::getAlias('@common/config/filepageMaps/last-sizes-map.php');
?>
<br><br>
Last sizes:
<br>
<pre class="jsonHighlight">
<?= json_encode($sizes, JSON_PRETTY_PRINT); ?>
</pre>


