<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.17
 * Time: 13:55
 */
/** @var array $orderColors */

use yii\helpers\Html;

/** @var array $commonInfo */

echo $this->render('topPanel');
?>
<br><br>
Result combinations: <?= number_format(array_sum($commonInfo), 0, '.', ' '); ?>
<?php
foreach ($commonInfo as $groupName => $info) {
    echo "\n<br>" . $groupName . ': ' . number_format($info, 0,'.', ' ') . "\n";
}
?>

