<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.17
 * Time: 13:55
 */

/** @var array $orderColors */


echo $this->render('topPanel');
?>
<br><br>
MulticolorOrders
<pre class="jsonHighlight">
<?= json_encode($orderColors['multicolorOrders'], JSON_PRETTY_PRINT); ?>
</pre>

<br><br>
Order colors:
<pre class="jsonHighlight">
<?= json_encode($orderColors['orderColors'], JSON_PRETTY_PRINT); ?>
</pre>

<?php
$path = Yii::getAlias('@common/config/filepageMaps/last-colors-map.php');
if (!file_exists($path)) {
    echo "\n<br><br>\n" . $path . ' file not exists';
    return;
}
$colors = include $path;
?>
<br><br>
Last colors:
<br>
<pre class="jsonHighlight">
<?= json_encode($colors, JSON_PRETTY_PRINT); ?>
</pre>





