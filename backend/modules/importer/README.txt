Usage example:

- Extractor - just a class which converts data into Element class
- transformers - different classes to transform Element object.
- loader - class which will be used  to keep final elements;

$etlSchema = (new EtlSchema())->extract(
            [
                'class' => ArrayExtractor::class,
            ]
        )->transform(
            [
                [
                    'field'   => 'description',
                    'glue'    => ' ',
                    'class'   => ColumnJoiner::class,
                    'columns' => ['description', 'info', 'details']
                ],
                [
                    'fields'    => ['video', 'tags', 'images'],
                    'class'     => ColumnValueSplitter::class,
                    'delimeter' => ',',
                ],
                [
                    'field'   => 'tags',
                    'class'   => TextToTagTransformer::class,
                    'columns' => ['title']
                ],
                [
                    'fields' => ['images', 'primary_image'],
                    'class' => ImageUrlToFile::class,
                ],
                [
                    'field' => 'properties',
                    'class' => ColumnProperties::class,
                    'columns' => ['properties', 'specifications']
                ],
                [
                    'field' => 'video',
                    'class' => VideoTransformer::class,
                ],
                [
                    'class'    => CallbackTransformer::class,
                    'callback' => function (ElementInterface $el) {
                        $data = $el->getData();
                        $data['callbackValue'] = 'some value';
                        $el->setData($data);
                        return $el;
                    }
                ],
                [
                    'class' => LocationTransformer::class,
                    'field' => 'location',
                    'columns' => [
                        'country' => 'country',
                        'location' => 'address'
                    ]
                ]
            ]
        )->load(new BaseLoader());

        $etlRunner = new Runner();
        $result = $etlRunner->run($etlSchema, $items);