<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\extractors;


use backend\modules\importer\etl\interfaces\BaseElement;
use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\ExtractInterface;

class ArrayExtractor implements ExtractInterface
{

    /**
     * @var iterable
     */
    public $items;


    public function extractRow($id, $data): ElementInterface
    {
        $element = new BaseElement();
        $element->setId($id);
        $element->setData($data);
        return $element;
    }
    public function init(): void
    {
    }
}