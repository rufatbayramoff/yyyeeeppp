<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\extractors;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\ExtractInterface;

class ExcelRowExtractor implements ExtractInterface
{
    public $file;

    /**
     * @var \PHPExcel
     */
    public $phpExcelObject;


    private $excelActiveSheet;
    private $excelRowIterator;
    /**
     * @param $key
     * @param $value
     * @return ElementInterface
     * @throws \PHPExcel_Reader_Exception
     */
    public function init(): void
    {
        $this->phpExcelObject = \PHPExcel_IOFactory::load($this->file);
    }

    /**
     * @return ElementInterface
     * @throws \PHPExcel_Exception
     */
    public function extractRow($id, $data) : ElementInterface
    {
        $this->excelActiveSheet = $this->excelActiveSheet ? : $this->phpExcelObject->getActiveSheet();
        $this->excelRowIterator = $this->excelRowIterator ? : $this->excelActiveSheet->getRowIterator();
        $row = $this->excelRowIterator->next();
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false);
        $row = [];
        foreach ($cellIterator as $cell) {
            $row[] = $cell->getValue();
        }
        return $row;
    }
}