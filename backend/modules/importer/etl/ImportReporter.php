<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl;


use yii\helpers\VarDumper;

class ImportReporter
{
    public $errors = [];
    public $imported = 0;
    public $skipped = 0;

    public function getString()
    {
        $tpl = "Imported: %d Skipped: %d";
        $dump = VarDumper::export($this->errors);
        if (!empty($this->errors)) {
            $tpl = $tpl . " <br>Errors: <pre>%s</pre>";
        }
        return sprintf($tpl, $this->imported, $this->skipped, $dump);
    }
}