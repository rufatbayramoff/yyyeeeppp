<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\interfaces;


interface ExtractInterface
{
    public function extractRow($id, $data) : ElementInterface;
    public function init() : void;
}