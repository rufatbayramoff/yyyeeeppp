<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\interfaces;


interface TransformInterface
{
    public function transform(ElementInterface $el) : ElementInterface;
}