<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\interfaces;


interface LoadInterface
{
    /**
     * @param ElementInterface $el
     * @return LoadInterface
     */
    public function load(ElementInterface $el) : LoadInterface;

    /**
     * @return mixed
     */
    public function validate() : bool;

    /**
     * @return bool
     */
    public function needSkip() : bool ;

    /**
     * error if validation or something failed
     * @return string
     */
    public function getError() : string;
}