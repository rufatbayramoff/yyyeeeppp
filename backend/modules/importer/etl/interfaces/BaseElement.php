<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\interfaces;


class BaseElement implements ElementInterface
{

    public $id;
    public $data;
    public $needSkip = false;

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data): void
    {
        $this->data = $data;
    }

    public function needSkip(): bool
    {
        return $this->needSkip;
    }

    public function setSkip($skipFlag)
    {
        $this->needSkip = $skipFlag;
    }
}