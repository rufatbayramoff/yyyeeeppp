<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\interfaces;


interface ElementInterface
{

    public function setId($id): void;

    public function getId();

    public function getData();

    public function setData($data): void;

    public function needSkip() : bool ;
    public function setSkip($skipFlag);
}