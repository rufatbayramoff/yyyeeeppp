<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\loaders;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\LoadInterface;
use common\modules\product\factories\ProductCategoryFactory;
use common\modules\product\repositories\ProductCategoryRepository;

class CategoriesMigrateLoader extends BaseLoader
{

    public $rootCategory;
    public $subCategory;
    public $subSubCategory;

    public $data;

    public $repo;
    public $factory;

    public function __construct()
    {
        $this->repo = new ProductCategoryRepository();
        $this->factory = new ProductCategoryFactory();
    }

    /**
     * @param ElementInterface $el
     * @return LoadInterface
     */
    public function load(ElementInterface $el): LoadInterface
    {
        $self = clone $this;
        $self->data = ['id' => $el->getId(), 'raw' => $el->getData()];
        $self->rootCategory = $self->data['raw']['root categories'];
        $self->subCategory = $self->data['raw']['subcategories'];
        $self->subSubCategory = $self->data['raw']['sub-subcategories'];
        if (empty($self->subSubCategory)) {
            $self->error = 'Empty title';
            $self->isValid = false;
            return $self;
        }
        $productCategory = $self->factory->createByLoader($self);
        try {
            $res = $self->repo->add($productCategory);
            if ($res->position === 1) {
                $res->position = $res->id;
                $res->save();
            }

        } catch (\Exception $e) {
            $self->isValid = false;
            $self->error = $e->getTraceAsString();
        }
        return $self;
    }

    /**
     * @return mixed
     */
    public function needSkip(): bool
    {
        if (empty($this->getRawData()['sub-subcategories'])) {
            return true;
        }
        return false;
    }
}