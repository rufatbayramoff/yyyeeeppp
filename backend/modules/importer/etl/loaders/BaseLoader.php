<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\loaders;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\LoadInterface;

class BaseLoader implements LoadInterface
{

    public $data;

    /**
     * @var string
     */
    protected $error;

    /**
     * @var bool
     */
    protected $isValid = true;

    /**
     * @param ElementInterface $el
     * @return LoadInterface
     */
    public function load(ElementInterface $el): LoadInterface
    {
        $self = clone $this;
        $self->data = ['id' => $el->getId(), 'raw' => $el->getData()];
        return $self;
    }

    public function getId()
    {
        return $this->data['id'];
    }

    public function getRawData()
    {
        return $this->data['raw'];
    }

    /**
     * @return mixed
     */
    public function validate(): bool
    {
        return $this->isValid;
    }

    /**
     * @return bool
     */
    public function needSkip(): bool
    {
        return false;
    }

    /**
     * error if validation or something failed
     *
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }
}