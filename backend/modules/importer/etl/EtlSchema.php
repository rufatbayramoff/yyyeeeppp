<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\ExtractInterface;
use backend\modules\importer\etl\interfaces\LoadInterface;
use backend\modules\importer\etl\interfaces\TransformInterface;

class EtlSchema
{
    /**
     * @var ElementInterface
     */
    public $currentElement;

    /**
     * @var ExtractInterface
     */
    public $extractor;

    /**
     * @var TransformInterface[]
     */
    public $transformers;

    /**
     * @var LoadInterface
     */
    public $loader;

    /**
     * @param $data
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function extract($data)
    {
        if (is_object($data) && $data instanceof ExtractInterface) {
            $this->extractor = $data;
            return $this;
        }
        $extractor = \Yii::createObject($data);
        if (!($extractor instanceof ExtractInterface)) {
            throw new \Exception('Not valid extractor');
        }
        $this->extractor = $extractor;
        return $this;
    }

    /**
     * @param array $data
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function transform(array $data)
    {
        $transformers = [];
        $data = array_filter($data);
        foreach ($data as $row) {
            if (is_object($row) && $row instanceof TransformInterface) {
                $transformers[] = $row;
                continue;
            }
            $transformer = \Yii::createObject($row);
            if (!($transformer instanceof TransformInterface)) {
                throw new \Exception('Not valid transformer : ' . $row['class']);
            }
            $transformers[] = $transformer;
        }
        $this->transformers = $transformers;
        return $this;
    }

    public function load(LoadInterface $loader)
    {
        $this->loader = $loader;
        return $this;
    }
}