<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\transformers;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\TransformInterface;

class CallbackTransformer implements TransformInterface
{
    /**
     * @var callable
     */
    public $callback;

    public function transform(ElementInterface $el): ElementInterface
    {
        $callback = $this->callback;
        $newEl = $callback($el);
        if (is_null($newEl)) {
            $el->setSkip(true);
        }
        return $newEl ? $newEl : $el;
    }
}