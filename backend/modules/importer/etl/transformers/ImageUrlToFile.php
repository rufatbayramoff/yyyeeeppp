<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\transformers;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\TransformInterface;
use common\models\factories\FileFactory;
use common\models\factories\UploadedFileFactory;
use common\models\factories\UploadedFileListFactory;
use yii\web\UploadedFile;

/**
 * Class ImageUrlToFile
 *
 * converts string image url to UploadFile object with saved in local runtime folder file.
 *
 * @package backend\modules\importer\etl\transformers
 */
class ImageUrlToFile implements TransformInterface
{
    public $fields = [];

    public function transform(ElementInterface $el): ElementInterface
    {
        $data = $el->getData();
        #$data['image'] = null; $el->setData($data); return $el; // no images updates
        $toProcess = [];
        foreach ($data as $k => $v) {
            if (!in_array($k, $this->fields)) {
                continue;
            }
            $toProcess[$k] = [];
            if (is_array($v)) {
                foreach ($v as $k1 => $v1) {
                    $toProcess[$k][] = $v1;
                }
            } else {
                $toProcess[$k] = $v;
            }
        }
        $fileFactory = \Yii::createObject(FileFactory::class);
        foreach ($toProcess as $k => $v) {
            $files = null;
            $v = (array)$v;
            $uploadFiles = [];
            foreach ($v as $fileOne) {
                if (!$fileOne) {
                    continue;
                }
                if (parse_url($fileOne)) {
                    $uploadFiles[] = UploadedFileFactory::createUploadedFile($fileOne);
                } else {
                    $content = file_get_contents($fileOne);
                    $parsedFile = pathinfo($fileOne);
                    $uploadedFile = new UploadedFile();
                    $uploadedFile->name = $parsedFile['basename'];
                    $uploadedFile->size = mb_strlen($content);
                    $uploadedFile->tempName = $fileOne;

                    $uploadFiles[] = $uploadedFile;
                }
            }
            $data[$k] = $uploadFiles;
        }
        $el->setData($data);
        return $el;
    }
}