<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\transformers;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\TransformInterface;

class ColumnValueSplitter implements TransformInterface
{
    public $field;
    public $fields = [];
    public $delimeter = ', ';

    public function transform(ElementInterface $el): ElementInterface
    {
        $data = $el->getData();

        foreach ($this->fields as $column) {
            if (empty($data[$column])) {
                continue;
            }
            if ($this->field) {
                $data[$this->field] = array_map('trim', explode($this->delimeter, $data[$column]));
            } else {
                $data[$column] = array_map('trim', explode($this->delimeter, $data[$column]));
            }
        }
        $el->setData($data); // or create new element?
        return $el;
    }
}