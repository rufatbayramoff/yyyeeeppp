<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\transformers;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\TransformInterface;
use yii\helpers\Json;

class VideoTransformer implements TransformInterface
{

    public $field;
    public $columns;

    public function transform(ElementInterface $el): ElementInterface
    {
        //[{"url": "https://www.youtube.com/watch?v=0z0i8GzfyXs",
        // "thumb": "http://img.youtube.com/vi/0z0i8GzfyXs/default.jpg",
        // "videoId": "0z0i8GzfyXs"}, {"url": "https://www.youtube.com/watch?v=RBHGCIa07PM", "thumb": "http://img.youtube.com/vi/RBHGCIa07PM/default.jpg", "videoId": "RBHGCIa07PM"}, {"url": "https://www.youtube.com/watch?v=bgHoQ_5dT2M", "thumb": "http://img.youtube.com/vi/bgHoQ_5dT2M/default.jpg", "videoId": "bgHoQ_5dT2M"}, {"url": "https://www.youtube.com/watch?v=-NnM6x2fBiA&t=85s", "thumb": "http://img.youtube.com/vi/-NnM6x2fBiA/default.jpg", "videoId": "-NnM6x2fBiA"}]
        $data = $el->getData();
        if (!empty($data[$this->field])) {
            $videos = (array)$data[$this->field];
            $result = [];
            foreach ($videos as $video) {
                $videoInf = $this->getInfoByLink($video);
                if ($videoInf) {
                    $result[] = $videoInf;
                }
            }
            $data[$this->field] = Json::encode($result);
        }
        $el->setData($data);
        return $el;
    }

    private function getInfoByLink($video)
    {
        $url = parse_url($video);
        $videoId = null;
        if (!empty($url['query'])) {
            parse_str($url['query'], $res);
            $videoId = $res['v'];
        } else {

        }
        if(empty($videoId)){
            return null;
        }

        $result = ['url' => $video, 'thumb' => 'http://img.youtube.com/vi/' . $videoId . '/default.jpg', 'videoId' => $videoId];

        return $result;
    }
}