<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\transformers;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\TransformInterface;

class LocationTransformer implements TransformInterface
{
    public $field;
    public $columns;

    public function transform(ElementInterface $el): ElementInterface
    {
        $data = $el->getData();

        $el->setData($data);
        return $el;
    }
}