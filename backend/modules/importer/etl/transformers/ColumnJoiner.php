<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\transformers;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\TransformInterface;

class ColumnJoiner implements TransformInterface
{
    public $field;
    public $columns;
    public $glue = '';

    public function transform(ElementInterface $el): ElementInterface
    {
        $data = $el->getData();
        $toJoin = [];
        foreach ($data as $k => $v) {
            if (in_array($k, $this->columns)) {
                $toJoin[] = $v;
            }
        }
        $data[$this->field] = implode($this->glue, $toJoin);

        $el->setData($data); // or create new element?
        return $el;
    }
}