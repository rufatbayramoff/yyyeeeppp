<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\transformers;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\TransformInterface;

class TextToTagTransformer implements TransformInterface
{
    public $field;
    public $columns;
    public $separator = ' ';

    public function transform(ElementInterface $el): ElementInterface
    {
        $data = $el->getData();
        $keywords = [];
        foreach ($data as $k => $v) {
            if (in_array($k, $this->columns)) {
                $keywords = array_merge($keywords, explode(" ", $v));
            }
        }
        if (!empty($data[$this->field])) {
            $keywords = array_merge((array)$data[$this->field], $keywords);
        }
        $tags = array_filter(array_map('strtolower', array_unique($keywords)));
        $tagsFiltered = [];
        foreach ($tags as $k => $tag) {
            if (strlen($tag) < 2 || ctype_digit($tag)) {
                continue;
            }
            $tagsFiltered[$tag] = $tag;
        }
        $data[$this->field] = array_values($tagsFiltered);
        $el->setData($data); // or create new element?
        return $el;
    }
}