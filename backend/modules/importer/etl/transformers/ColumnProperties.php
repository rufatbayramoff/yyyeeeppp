<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\transformers;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\TransformInterface;
use common\modules\product\components\CustomPropertiesImporter;

class ColumnProperties implements TransformInterface
{
    public $field;
    public $columns;

    public function transform(ElementInterface $el): ElementInterface
    {
        $data = $el->getData();
        $importer = new CustomPropertiesImporter();
        // grap information from columns and save all to field
        $result = [];

        $this->columns = $this->columns ? $this->columns : (array)$this->field;

        foreach ($this->columns as $column) {
            if (empty($data[$column])) {
                continue;
            }
            $columnResult = $importer->importProperties($data[$column]);
            $result = array_merge($result, $columnResult);
        }

        $resultProperties = [];
        $i = 0;
        foreach ($result as $k => $v) {
            $i++;
            $resultProperties[] = [
                'title'    => $k,
                'value'    => $v,
                'position' => $i
            ];
        }
        $data[$this->field] = $resultProperties;

        $el->setData($data);
        return $el;
    }
}