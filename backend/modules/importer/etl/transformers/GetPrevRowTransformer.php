<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl\transformers;


use backend\modules\importer\etl\interfaces\ElementInterface;
use backend\modules\importer\etl\interfaces\TransformInterface;

class GetPrevRowTransformer implements TransformInterface
{
    public $prevRow = null;

    /**
     * @var callable
     */
    public $callback;

    public function transform(ElementInterface $el): ElementInterface
    {
        $data = $el->getData();
        $callback = $this->callback;
        if ($updatedData = $callback($el, $this->prevRow)) {
            $data = $updatedData;
            $el->setData($data);
        }
        $this->prevRow = $el;
        return $el;
    }
}