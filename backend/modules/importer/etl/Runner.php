<?php
/**
 * User: nabi
 */

namespace backend\modules\importer\etl;


use backend\modules\importer\etl\interfaces\LoadInterface;
use yii\base\InvalidArgumentException;
use yii\helpers\Html;

class Runner
{
    /**
     * @var ImportReporter
     */
    private $importReporter;

    /**
     * is result needed after run?
     *
     * @var bool
     */
    private $needResult = true;

    public function __construct($needResult = true)
    {
        $this->needResult = $needResult;
    }

    /**
     * @param EtlSchema $etl
     * @return LoadInterface[]
     */
    public function run(EtlSchema $etl, $items)
    {
        $importReporter = new ImportReporter();
        $extractor = $etl->extractor;
        $extractor->init();
        $result = [];
        $imported = $skipped = 0;
        foreach ($items as $i => $row) {
            $row = $extractor->extractRow($i, $row);
            $id = $row->getId();
            if ($row->needSkip()) {
                $skipped++;
                continue;
            }
            // Transform element
            foreach ($etl->transformers as $transformer) {
                try {
                    $row = $transformer->transform($row);
                } catch (InvalidArgumentException $e) {
                    $importReporter->errors[$id] = $e->getMessage();
                }
            }
            if ($row->needSkip()) {
                $skipped++;
                continue;
            }
            $model = $etl->loader->load($row);
            if ($model->needSkip()) {
                $skipped++;
                continue;
            }
            if ($model->validate()) {
                if ($this->needResult) {
                    $result[] = $model;
                }
                $imported++;
            } else {
                $importReporter->errors[$id] = $model->getError();
                $skipped++;
            }
        }
        $importReporter->imported = $imported;
        $importReporter->skipped = $skipped;
        $this->importReporter = $importReporter;
        return $result;
    }

    /**
     * @return ImportReporter
     */
    public function getReport()
    {
        return $this->importReporter;
    }
}