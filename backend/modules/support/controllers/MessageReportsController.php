<?php
/**
 * Created by mitaichik
 */

namespace backend\modules\support\controllers;


use backend\components\AdminAccess;
use common\components\exceptions\AssertHelper;
use common\models\MsgReport;
use yii\data\ActiveDataProvider;

class MessageReportsController extends \backend\components\AdminController
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(!is_guest()) AdminAccess::validateAccess('support.message_reports');
        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => MsgReport::find()->orderBy('id DESC'),
        ]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $report = MsgReport::tryFindByPk($id);
        $report->moderated = 1;
        AssertHelper::assertSave($report);
        return $this->render('view', ['report' => $report]);
    }
}