<?php
/**
 * Created by mitaichik
 */

namespace backend\modules\support\controllers;


use backend\components\AdminAccess;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\models\MsgMessage;
use common\models\MsgReport;
use common\models\MsgTopic;
use common\modules\message\services\MessageService;
use yii\data\ActiveDataProvider;

class MessageBotController extends \backend\components\AdminController
{
    /**
     * @var MessageService
     */
    protected $messageService;

    /**
     * @param MessageService $messageService
     */
    public function injectDependencies(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('support.view');
        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => MsgMessage::find()->where(['topic_id' => [MsgTopic::BOT_MESSAGE_ALL, MsgTopic::BOT_MESSAGE_COMPANIES]])->orderBy('created_at DESC'),
        ]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionCreate()
    {
        if (\Yii::$app->request->isPost) {
            $this->messageService->createBotMessage(\Yii::$app->request->post('MsgMessage'));
            $this->redirect('index');
        }
        $messageBot = new MsgMessage();
        return $this->render('create', ['model' => $messageBot]);
    }

}