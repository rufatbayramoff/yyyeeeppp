<?php

namespace backend\modules\support\controllers;

use backend\components\AdminAccess;
use backend\modules\support\components\ResolveTrait;
use backend\modules\support\components\SupportTopicFacade;
use backend\modules\support\exceptions\TopicAlreadyOpenedException;
use backend\modules\support\models\TopicFilter;
use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\components\exceptions\AssertHelper;
use common\models\base\UserAdmin;
use common\models\message\forms\MessageForm;
use common\models\message\forms\TopicForm;
use common\models\message\builders\TopicBuilder;
use common\models\MsgFile;
use common\models\MsgMessage;
use common\models\MsgTopic;
use common\models\StoreOrder;
use common\models\User;
use frontend\models\community\MessageFacade;
use frontend\models\community\TopicMerger;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Controller for active topics
 * @package app\modules\support\controllers
 */
class ActiveTopicsController extends \backend\components\AdminController
{
    use ResolveTrait;

    /**
     * @var TopicMerger
     */
    private $topicMerger;

    /**
     * @param TopicMerger $topicMerger
     */
    public function injectDependencies(TopicMerger $topicMerger)
    {
        $this->topicMerger = $topicMerger;
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(!is_guest()){
            AdminAccess::validateAccess('support.view');
        }
        return parent::beforeAction($action);
    }
    
    /**
     * @return string
     */
    public function actionIndex()
    {
        $user = $this->resolveSupportUser();

        $filter = new TopicFilter();
        $filter->load($_GET);
        $filter->excludeBotMessages = true;

        return $this->render('index', [
            'user' => $user,
            'adminUser' => $this->resolveAdminUser(),
            'dataProvider' => SupportTopicFacade::getTopicsDataProvider($this->resolveAdminUser(), $filter),
            'filter' => $filter
        ]);
    }

    /**
     * Create topic from support to user
     * @param int $withUserId User id
     * @param null $orderId
     * @return string|\yii\web\Response
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \common\components\exceptions\AssertException
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     * @throws \common\models\message\exceptions\InvalidBindedObject
     */
    public function actionCreateTopicForm($withUserId, $orderId = null)
    {
        // try to find if such topic already exists
        $withUser = User::findOne($withUserId);
        $msgTopic = MsgTopic::find()->forUser($withUser)
            ->andWhere(['bind_to' => MsgTopic::BIND_OBJECT_ORDER, 'bind_id' => $orderId, 'creator_id'=>User::USER_ID_SUPPORT])
            ->latest();
  #      debugSql($msgTopic); exit;
        if ($msgTopic && !SupportTopicFacade::getIsResolved($msgTopic)) {

            return $this->redirect(['view-topic', 'topicId' => $msgTopic->id]);
        }

        $topicForm = TopicForm::createPersonal($this->resolveSupportUser(), User::tryFindByPk($withUserId));

        if ($orderId) {
            $order = StoreOrder::tryFind($orderId);
            $topicForm->setBindedObject($order);
        }

        if(\Yii::$app->request->isPost){
            AssertHelper::assert($topicForm->load(\Yii::$app->request->post()));

            if($topicForm->validate()){
                $adminUser = $this->resolveAdminUser();
                $topic = TopicBuilder::builder()->fromForm($topicForm)->create();
                SupportTopicFacade::holdTopic($topic, $adminUser);
                return $this->redirect(['view-topic', 'topicId' => $topic->id]);
            }
        }
        return $this->render('new-topic-with-user', ['topicForm' => $topicForm]);
    }

    /**
     * Bind topic to order
     * @param int $topicId
     * @param int $orderId
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionBindOrder(int $topicId, int $orderId)
    {
        $topic = MsgTopic::tryFind($topicId);

        $anotherTopics = MsgTopic::find()
            ->joinWith(['members' => function (BaseActiveQuery $query) {$query->from('msg_member m1');}])
            ->joinWith(['members' => function (BaseActiveQuery $query) {$query->from('msg_member m2');}])
            ->andWhere([
                'bind_to' => MsgTopic::BIND_OBJECT_ORDER,
                'bind_id' => $orderId,
            ])
            ->andWhere([
                'm1.user_id' => $topic->members[0]->user_id,
                'm2.user_id' => $topic->members[1]->user_id
            ])
            ->andWhere(['!=', 'msg_topic.id', $topic->id])
            ->orderBy('msg_topic.id ASC')
            ->all();

        if ($anotherTopics) {
            $anotherTopics[] = $topic;
            $firstTopic = array_shift($anotherTopics);
            foreach ($anotherTopics as $topic) {
                $this->topicMerger->mergeTopics($topic, $firstTopic);
            }
            $this->redirect(['view-topic', 'topicId' => $firstTopic->id]);
        }
        else {
            $topic->bind_to = MsgTopic::BIND_OBJECT_ORDER;
            $topic->bind_id = $orderId;
            $topic->safeSave();
            $this->redirect(['view-topic', 'topicId' => $topic->id]);
        }
    }

    /**
     * @return string
     */
    public function actionResolved()
    {
        $user = $this->resolveSupportUser();

        return $this->render('index', [
            'user' => $user,
            'adminUser' => $this->resolveAdminUser(),
            'dataProvider' => SupportTopicFacade::getTopicsDataProvider($this->resolveAdminUser(), true)
        ]);
    }

    /**
     * View topic for dialog
     * @param $topicId
     * @return string
     * @throws Exception
     */
    public function actionViewTopic($topicId)
    {
        $topic = $this->resolveTopic($topicId);
        $user = TopicBuilder::resolveSupportUser();
        $adminUser = $this->resolveAdminUser();

        $this->checkForNotOpenInAnotherUser($topic, $adminUser);

        SupportTopicFacade::setIsOpened($topic, $adminUser);

        MessageFacade::markTopicAsReaded($topic, $user);
        SupportTopicFacade::holdTopic($topic, $adminUser);

        $messagesDataProvider = MessageFacade::getMessagesDataProvider($user, $topic, false);

        {
            //TODO crutch for detect that topic have new message, delete after refactor like frontend messages
            /** @var MsgMessage $lastMessage */
            $lastMessage = ArrayHelper::last($messagesDataProvider->getModels());
            $lastMessageId = ($lastMessage ? $lastMessage->id : 0);
            header("LastMessageId: ".$lastMessageId);
        }

        return $this->render('topic-view', [
            'topic' => $topic,
            'messagesDataProvider' => $messagesDataProvider,
            'user' => $user,
            'adminUser' => $adminUser,
            'lastMessageId' => $lastMessageId
        ]);
    }

    /**
     * Resolve message
     * @param $topicId
     * @return \yii\web\Response
     */
    public function actionResolveTopic($topicId)
    {
        $topic = $this->resolveTopic($topicId);
        $adminUser = $this->resolveAdminUser();

        $this->checkForNotOpenInAnotherUser($topic, $adminUser);

        SupportTopicFacade::resolveTopic($topic, $adminUser);
        return $this->redirect(['index']);
    }

    /**
     * Forward topic to another user
     * @param $topicId
     * @param $newSupportUserId
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionForwardTopic($topicId, $newSupportUserId)
    {
        $topic = $this->resolveTopic($topicId);
        $newSupportUser = $this->resolveAdminUser($newSupportUserId);

        $this->checkForNotOpenInAnotherUser($topic, $this->resolveAdminUser());

        SupportTopicFacade::forwardTopic($topic, $newSupportUser);
        return $this->redirect(['index']);
    }

    /**
     * Add message to topic
     * @param $topicId
     * @throws HttpException
     */
    public function actionAddMessage($topicId)
    {
        $topic = $this->resolveTopic($topicId);
        $user = $this->resolveSupportUser();

        $this->checkForNotOpenInAnotherUser($topic, $this->resolveAdminUser());

        $messageForm = new MessageForm();
        if($messageForm->load(\Yii::$app->request->post()) && $messageForm->validate())
        {
            if(SupportTopicFacade::getIsResolved($topic))
            {
                SupportTopicFacade::restoreTopic($topic);
            }

            MessageFacade::addMessage($topic, $user, $messageForm, $this->resolveAdminUser());
            return;
        }

        throw new HttpException(500, 'Bad request');
    }

    /**
     * Refresh that topic is open
     * @param $topicId
     */
    public function actionTopickOpened($topicId)
    {
        $topic = $this->resolveTopic($topicId);
        if(!$topic) {
            throw new NotFoundHttpException();
        }
        SupportTopicFacade::setIsOpened($topic, $this->resolveAdminUser());
    }

    /**
     * Topic is closed
     * @param $topicId
     */
    public function actionTopicClosed($topicId)
    {
        $topic = $this->resolveTopic($topicId);
        if(!$topic) {
            throw new NotFoundHttpException();
        }
        SupportTopicFacade::setIsClosed($topic, $this->resolveAdminUser());
    }

    /**
     * Check that this topic not open on another user
     * @param MsgTopic $topic
     * @param UserAdmin $user
     * @throws TopicAlreadyOpenedException
     */
    public function checkForNotOpenInAnotherUser(MsgTopic $topic, UserAdmin $user)
    {
        if(SupportTopicFacade::getIsOpenedByOtherUser($topic, $user))
        {
            throw new TopicAlreadyOpenedException($topic);
        }

    }

    public function actionDownloadMessageFile($fileId)
    {
        $user = $this->resolveSupportUser();

        /** @var MsgFile $file */
        $file = MsgFile::find()
            ->forId($fileId)
            ->active()
            ->one();

        if (!$file) {
            throw new NotFoundHttpException();
        }
        AdminAccess::validateAccess('message.downloadfile');
        if (!$file->isHaveAccess($user)){
            #throw new ForbiddenHttpException();
        }

        return \Yii::$app->response->sendFile($file->file->getLocalTmpFilePath(), $file->file->getFileName());
    }
}
