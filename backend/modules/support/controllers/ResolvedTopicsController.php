<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 22.10.15
 * Time: 15:41
 */

namespace backend\modules\support\controllers;


use backend\components\AdminAccess;
use backend\modules\support\components\ResolveTrait;
use backend\modules\support\components\SupportTopicFacade;
use backend\modules\support\models\TopicFilter;
use common\models\message\builders\TopicBuilder;

/**
 * Controller for view resolved topic
 * @package backend\modules\support\controllers
 */
class ResolvedTopicsController extends \backend\components\AdminController
{
    use ResolveTrait;

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(!is_guest()) AdminAccess::validateAccess('support.view');
        return parent::beforeAction($action);
    }
    
    /**
     * @return string
     * @todo merge with \backend\modules\support\controllers\ActiveTopicsController::actionIndex
     */
    public function actionIndex()
    {
        $user = $this->resolveSupportUser();

        $filter = new TopicFilter();
        $filter->load($_GET);

        return $this->render('../active-topics/index', [
            'user' => $user,
            'adminUser' => $this->resolveAdminUser(),
            'dataProvider' => SupportTopicFacade::getTopicsDataProvider($this->resolveAdminUser(), $filter, true),
            'filter' => $filter
        ]);
    }

    /**
     * View topic for dialog
     * @param $topicId
     * @return string
     */
    public function actionViewTopic($topicId)
    {
        $topic = $this->resolveTopic($topicId);
        $user = TopicBuilder::resolveSupportUser();
        $adminUser = $this->resolveAdminUser();

        return $this->render('../active-topics/topic-view', [
            'topic' => $topic,
            'user' => $user,
            'adminUser' => $adminUser
        ]);
    }
}