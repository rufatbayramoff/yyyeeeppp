<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 22.10.15
 * Time: 13:00
 */

namespace backend\modules\support\components;


use common\models\base\UserAdmin;

class ViewHelper
{
    /**
     * @param UserAdmin $excludeUser
     * @return array
     */
    public static function supportUsersList(UserAdmin $excludeUser = null)
    {
        $query = UserAdmin::find();

        if($excludeUser)
        {
            $query->where(['!=', 'id', $excludeUser->id]);
        }

        return $query->all();
    }

}