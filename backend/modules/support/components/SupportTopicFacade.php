<?php

namespace backend\modules\support\components;


use backend\modules\support\models\TopicFilter;
use common\components\exceptions\AssertHelper;
use common\models\base\UserAdmin;
use common\models\message\builders\TopicBuilder;
use common\models\message\forms\MessageForm;
use common\models\MsgFolder;
use common\models\MsgMember;
use common\models\MsgTopic;
use common\models\User;
use frontend\models\community\MessageFacade;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use common\components\ActiveQuery;

/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 21.10.15
 * Time: 16:04
 */
class SupportTopicFacade
{
    /**
     * Return Topic dataProvider witch latest dialogs
     * @param UserAdmin $adminUser
     * @param TopicFilter|null $filter
     * @param bool $archive
     * @return ActiveDataProvider
     */
    public static function getTopicsDataProvider(UserAdmin $adminUser, $filter = null, $archive = false)
    {
        $query = MsgTopic::find()
            ->with(
                'lastMessage',
                'users',
                'lastMessage.user',
                'lastMessage.user.userProfile',
                'holdSupportUser',
                'lastSupportUser',
                'members',
                'members.user'
            )
            ->joinWith('members', false)
            ->andWhere([
                'msg_member.folder_id' => MsgFolder::FOLDER_ID_SUPPORT,
                'msg_member.user_id' => User::USER_ID_SUPPORT,
                'msg_member.is_deleted' => (int) $archive,
            ])
            ->orderBy('last_message_time DESC');

        if(!$archive)
        {
            $query->andWhere(['not in', MsgTopic::column('id'), self::getTopicsIdsOpenedByOtherSupportUsers($adminUser)]);
        }

        if($filter)
        {
            $filter->addToQuery($query);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => MessageFacade::LIMIT_HISTORY,
            ],
        ]);
    }

    /**
     * Return topic's ids that opened by other users
     * @param UserAdmin $user
     * @return array
     */
    private static function getTopicsIdsOpenedByOtherSupportUsers(UserAdmin $user)
    {
        $keys = \Yii::$app->redis->keys('backend.support.topic-open.*');

        if(empty($keys))
        {
            return [];
        }

        $values = \Yii::$app->redis->executeCommand('mget', $keys);
        $keys = array_map(function($item){ return substr($item, strrpos($item, '.') + 1); }, $keys);
        $values = array_map(function($item){return (int)$item;}, $values);
        $openedIds = array_filter(array_combine($keys, $values));
        $openedByOtherUsers = array_filter($openedIds, function($item) use ($user) { return $user->id != $item; });
        return array_keys($openedByOtherUsers);
    }

    /**
     * Check that topic is opened other support user
     * @param MsgTopic $topic
     * @param UserAdmin $adminUser
     * @return bool
     * @throws Exception
     */
    public static function getIsOpenedByOtherUser(MsgTopic $topic, UserAdmin $adminUser)
    {
        $openedUserId = self::getOpenTopicUserId($topic);
        return $openedUserId && ($openedUserId != $adminUser->id);
    }

    /**
     * Return admin user id who now open topic, or null of topic not opened
     * @param MsgTopic $topic
     * @return int|null
     */
    public static function getOpenTopicUserId(MsgTopic $topic)
    {
        self::checkIsSupportTopic($topic);
        return \Yii::$app->redis->get('backend.support.topic-open.'.$topic->id);
    }

    /**
     * Set thet topic opened for user
     * @param MsgTopic $topic
     * @param UserAdmin $adminUser
     */
    public static function setIsOpened(MsgTopic $topic, UserAdmin $adminUser)
    {
        self::checkIsSupportTopic($topic);
        self::checkIsTopicNotDeleted($topic);
        \Yii::$app->redis->setex('backend.support.topic-open.'.$topic->id, 10, $adminUser->id);
    }

    /**
     * Set thet topic opened for user
     * @param MsgTopic $topic
     * @param UserAdmin $adminUser
     */
    public static function setIsClosed(MsgTopic $topic, UserAdmin $adminUser)
    {
        self::checkIsSupportTopic($topic);
        self::checkIsTopicNotDeleted($topic);
        $openedUserId = \Yii::$app->redis->get('backend.support.topic-open.'.$topic->id);
        if($openedUserId == $adminUser->id)
        {
            \Yii::$app->redis->del('backend.support.topic-open.'.$topic->id);
        }
    }

    /**
     * @param MsgTopic $topic
     * @param UserAdmin $supportUser
     */
    public static function holdTopic(MsgTopic $topic, UserAdmin $supportUser)
    {
        self::checkIsSupportTopic($topic);
        self::checkIsTopicNotDeleted($topic);
        $topic->hold_support_user_id = $supportUser->id;
        AssertHelper::assertSave($topic);
    }

    /**
     * Forwar topic to $newSupportUser
     * @param MsgTopic $topic
     * @param UserAdmin $newSupportUser
     * @throws Exception
     */
    public static function forwardTopic(MsgTopic $topic, UserAdmin $newSupportUser)
    {
        self::checkIsSupportTopic($topic);
        self::checkIsTopicNotDeleted($topic);
        MessageFacade::markTopicAsUnreaded($topic, TopicBuilder::resolveSupportUser());
        SupportTopicFacade::holdTopic($topic, $newSupportUser);
    }

    /**
     * Resolve topic
     *
     * @param MsgTopic $topic
     * @param UserAdmin $adminUser
     * @throws \yii\base\Exception
     */
    public static function resolveTopic(MsgTopic $topic, UserAdmin $adminUser)
    {
        self::checkIsSupportTopic($topic);
        self::checkIsTopicNotDeleted($topic);
        $supportUser = TopicBuilder::resolveSupportUser();
        MessageFacade::addMessage(
            $topic,
            $supportUser,
            new MessageForm(['message' => _t('backend.support', 'Issue is resolved by {author}', ['author' => $adminUser->username])]),
            $adminUser
        );
        MessageFacade::deleteTopic($topic, $supportUser);
    }

    /**
     * Restore topic.
     * @param MsgTopic $topic
     */
    public static function restoreTopic(MsgTopic $topic)
    {
        $member = $topic->getMemberForUser(TopicBuilder::resolveSupportUser());
        $member->is_deleted = 0;
        AssertHelper::assertSave($member);
    }

    /**
     * Return count of opened support topics with new messages
     * @param UserAdmin|null $adminUser If set - return count only for this user, else - for all users
     * @return int
     */
    public static function getOpenedTopicsCount($adminUser = null)
    {
        $query = MsgMember::find()
            ->where([
                'folder_id' => MsgFolder::FOLDER_ID_SUPPORT,
                'user_id' => User::USER_ID_SUPPORT,
                'is_deleted' => 0,
                'have_unreaded_messages' => 1,
            ]);

        if($adminUser)
        {
            $query->joinWith(['topic' => function(ActiveQuery $query) use ($adminUser){
                $query->andWhere(['hold_support_user_id' => $adminUser->id]);
            }], false);
        }

        return (int)$query->count();
    }


    /**
     * Return count of unholded support topics
     * @return int
     */
    public static function getUnholdTopicsCount()
    {
        $query = MsgMember::find()
            ->where([
                'folder_id' => MsgFolder::FOLDER_ID_SUPPORT,
                'user_id' => User::USER_ID_SUPPORT,
                'is_deleted' => 0,
                'have_unreaded_messages' => 1
            ])
            ->joinWith(['topic' => function(ActiveQuery $query)
            {
                $query->andWhere(['hold_support_user_id' => null]);
            }], false);

        return (int)$query->count();
    }

    /**
     * Check that topic is resolved
     * @param MsgTopic $topic
     * @return int
     * @throws Exception
     */
    public static function getIsResolved(MsgTopic $topic)
    {
        return $topic->getMemberForUser(TopicBuilder::resolveSupportUser())->is_deleted;
    }

    /**
     * Check that topic is support.
     * It need becouse topic can be other message dialog, but this facade work only with support topics.
     * @param MsgTopic $topic
     * @throws Exception
     */
    private static function checkIsSupportTopic(MsgTopic $topic)
    {
        if(!self::isSupportTopic($topic))
            throw new Exception('Topic is not support');
    }

    public static function isSupportTopic(MsgTopic $topic)
    {
        foreach($topic->members as $member)
        {
            if($member->user_id == User::USER_ID_SUPPORT){
                return true;
            }
        }
        return false;
    }
    /**
     * Chect that this topic not deleted for support user
     * @param MsgTopic $topic
     * @throws Exception
     */
    private static function checkIsTopicNotDeleted(MsgTopic $topic)
    {
        AssertHelper::assert(!self::getIsResolved($topic), 'Topic already deleted');
    }

    /**
     * Get user topics with support
     *
     * @param User $user
     * @param bool $archive
     *
     * @return ActiveDataProvider
     */
    public static function getUserSupportTopicsDataProvider(User $user, $archive = false): ?ActiveDataProvider
    {
        $query = MsgTopic::find()
            ->from(['t' => MsgTopic::tableName()])
                ->innerJoin(['mm' => MsgMember::tableName()], 't.id = mm.topic_id and mm.folder_id = :folder_id and mm.user_id = :user_id')
                ->innerJoin(['mm2' => MsgMember::tableName()], 't.id = mm2.topic_id and mm2.folder_id = :folder_id and mm2.user_id = :support_user_id')
            ->where([
                'mm2.is_deleted' => (int) $archive,
            ])
            ->orderBy(['t.last_message_time' => SORT_DESC])
            ->addParams([
                'folder_id' => MsgFolder::FOLDER_ID_SUPPORT,
                'support_user_id' => User::USER_ID_SUPPORT,
                'user_id' => $user->id
            ]);

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }
}