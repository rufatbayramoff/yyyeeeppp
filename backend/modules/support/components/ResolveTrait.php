<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 22.10.15
 * Time: 15:16
 */

namespace backend\modules\support\components;


use common\models\message\builders\TopicBuilder;
use common\models\MsgTopic;
use common\models\UserAdmin;
use yii\web\NotFoundHttpException;

/**
 * Trait for resolve some objects in controllers
 * @package backend\modules\support\components
 */
trait ResolveTrait
{
    /**
     * @param $topicId
     * @return null|MsgTopic
     */
    private function resolveTopic($topicId)
    {
        return MsgTopic::findByPk($topicId);
    }

    /**
     * @return \common\models\User
     */
    private function resolveSupportUser()
    {
        return TopicBuilder::resolveSupportUser();
    }

    /**
     * @param null|int $id if null - return current user
     * @return UserAdmin
     * @throws NotFoundHttpException
     */
    private function resolveAdminUser($id = null)
    {
        if($id === null)
        {
            return \Yii::$app->user->getIdentity();
        }

        $user = UserAdmin::findByPk($id);

        if(!$user)
        {
            throw new NotFoundHttpException('Support user not found');
        }

        return $user;
    }
}