<?php

namespace backend\modules\support;

/**
 * Class Module
 * @package app\modules\support
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'backend\modules\support\controllers';

    /**
     *
     */
    public function init()
    {
        parent::init();
    }
}
