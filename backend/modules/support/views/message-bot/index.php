<?php

use common\models\MsgReport;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = "Bot message list";
?>

    <p>
        <?= Html::a('Create message', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,

    'columns' => [
        [
            'attribute' => 'created_at',
        ],
        [
            'attribute' => 'topic_id',
            'value'     => function (\common\models\MsgMessage $message) {
                return $message->topic_id === \common\models\MsgTopic::BOT_MESSAGE_ALL ? 'All' : 'Companies';
            }
        ],
        ['attribute' => 'text',]
    ]
]); ?>