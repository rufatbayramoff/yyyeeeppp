<?php

use backend\modules\support\assets\TopicViewAssets;
use common\models\User;
use frontend\models\community\MessageFacade;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/** @var \common\models\MsgMessage $model */
/** @var \yii\web\View $this */

$this->title                   = 'Create bot message';
$this->params['breadcrumbs'][] = ['label' => 'Bot messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-bot-create">

    <div class="message-bot-form">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'topic_id')->label('Message for')->dropDownList(['1' => 'All', '2' => 'Only for companies'])?>

        <?= $form->field($model, 'text')->textarea(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
