<?php
use backend\modules\support\assets\TopicViewAssets;
use common\models\User;
use frontend\models\community\MessageFacade;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var \common\models\MsgReport $report */
/** @var \yii\web\View $this */

TopicViewAssets::register($this);

$this->title  = 'Message report';

$userLinkFn = function(User $user)
{
    return Html::a("{$user->username}", ['/user/user/view', 'id' => $user->id], ['target' => '_blank']);
};

$memberLinksFn = function() use ($report, $userLinkFn)
{
    $memberLinks = [];
    foreach ($report->topic->getOtherUsers($report->createUser) as $member){
        $memberLinks[] = $userLinkFn($member);
    }
    return implode(' , ', $memberLinks);
};
?>

<div class="topic-view">
    <div class="header">

        <?= DetailView::widget([
            'model' => $report,
            'attributes' => [
                //'id',
                [
                    'format' => 'raw',
                    'label' => 'Reported user',
                    'value' => $userLinkFn($report->createUser)
                ],
                [
                    'format' => 'raw',
                    'label' => 'Dialog With',
                    'value' => $memberLinksFn()
                ],
                [
                    'format' => 'raw',
                    'label' => 'Topic theme',
                    'value' => H($report->topic->title),
                ],
                [
                    'label' => 'Reason',
                    'value' => $report->reason->title
                ],
                [
                    'label' => 'Comment',
                    'value' => $report->comment
                ],

                'created_at',
            ],
        ]) ?>

    </div>
    <?=
        \yii\widgets\ListView::widget([
            'id' => 'chat-list',
            'dataProvider' => MessageFacade::getMessagesDataProvider($report->createUser, $report->topic, false),
            'itemView' => '../active-topics/message-item',
            'summary' => false,
            'viewParams' => [
                'user' => $report->createUser,
            ],
        ]);
    ?>
</div>