<?php

use common\models\MsgReport;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = "Message reports list";
?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
   // 'filterModel' => $filter,
    'columns' => [
        [
            'label' => 'User',
            'attribute' => 'user',
            'format' => 'raw',
            'value' => function(MsgReport $report)
            {
                return Html::a("{$report->createUser->username}", ['/user/user/view', 'id' => $report->createUser->id], ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'created_at',
        ],
        [
            'label' => 'Topic theme',
            'attribute' => 'topic.title'
        ],
        [
            'label' => 'Reason',
            'attribute' => 'reason.title',
        ],
        [
            'attribute' => 'comment',
        ],
        [
            'attribute' => 'moderated',
            'format' => 'boolean'
        ],
        [
            'format' => 'html',
            'value' => function(MsgReport $report)
            {
                return Html::a('View', ['view', 'id' => $report->id]);
            }
        ],
    ],
]); ?>