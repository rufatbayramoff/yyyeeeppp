<?php
use frontend\components\UserUtils;

/** @var \common\models\User $user */
/** @var \common\models\MsgMessage $model */


$dir = $model->getIsAuthor($user)? 'right' : 'left';
$dir2 = $dir=='right' ? 'left' : 'right';
$avatar = UserUtils::getAvatarForUser($model->user, 'chat');

?>

<div class="message" id="message-<?=$model->id?>">
    <div class="pull-left avatar">
        <?= $avatar; ?>
    </div>
    <div>
        <div>
            <b><?= $model->user->username; ?></b>
            <small class="message-date"><i><?=app('formatter')->asDatetime($model->created_at);?></i></small>
        </div>

        <div class="message-text">
            <?= nl2br(\H($model->text));?>
        </div>


        <?php if($model->msgFiles) : ?>

            <div class="message-files">
                <?=_t('frontend.message', 'Attached files:')?>
                <?php foreach ($model->msgFiles as $file) :?>
                    <span class="label">
                        <a href="<?= \yii\helpers\Url::to(['/support/active-topics/download-message-file', 'fileId' => $file->id])?>" target="_blank"><?= $file->file->name?></a>
                    </span>
                <?php endforeach; ?>
            </div>

        <?php endif; ?>

    </div>
    <div class="clearfix"></div>
</div>