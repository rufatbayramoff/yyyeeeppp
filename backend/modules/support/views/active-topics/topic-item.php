<?php
use backend\modules\support\components\SupportTopicFacade;
use frontend\components\UserUtils;
use yii\helpers\Html;

/** @var \common\models\MsgTopic $model */
/** @var \common\models\User $user */
/** @var \common\models\UserAdmin $adminUser */

$isResolved = SupportTopicFacade::getIsResolved($model);

?>

<div class="row topic-item <?= $model->getMemberForUser($user)->have_unreaded_messages ? 'unreaded' : null?>">
    <div class="col-md-3">
        <b><?=H($model->title)?></b> <small class="created-date"><i><?=app('formatter')->asDatetime($model->created_at);?></i></small><br/>
        <small>
            Dialog users:

            <?php
                $otherUsersLinks = [];
                foreach($model->getOtherUsers($user) as $otherUser)
                {
                    $otherUsersLinks[] = Html::a("<i>{$otherUser->username}</i>", ['/user/user/view', 'id' => $otherUser->id], ['target' => '_blank']);
                }
                echo implode(', ', $otherUsersLinks);
            ?>

        </small><br/>
        <?php if($model->lastSupportUser):?>
            <small>Last support user: <i><?= $model->lastSupportUser->username?></i> on <?=app('formatter')->asDatetime($model->last_message_time); ?></small><br/>
        <?php endif; ?>
        <?php if($model->holdSupportUser):?>
            <small>Holded for <i style="color: red"><?= $model->holdSupportUser->username?></i></small>
        <?php endif; ?>
    </div>

    <!-- Bind object -->

    <div class="col-md-1">
        <?php if($model->getBindedObject()): ?>
            <?= \yii\helpers\Html::a('#'.$model->getBindedObject()->id, \common\models\message\helpers\UrlHelper::bindedObjectBackendRoute($model->getBindedObject()), ['target' => '_blank']) ?>
        <?php endif; ?>
    </div>

    <div class="col-md-6">
        <div class="row">

            <!-- Avatars -->

            <div class="col-md-2">
                    <div class="avatar">
                        <?= $model->lastMessage?UserUtils::getAvatarForUser($model->lastMessage->user, 'message'):''; ?>
                    </div>
            </div>

            <!-- Message -->

            <div class="col-md-10">
                <div>
                    <small><?= $model->created_at?></small>
                </div>
                <div class="clearfix"></div>
                <div>
                    <?=$model->lastMessage?\H($model->lastMessage->text):'';?>
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-2">

        <?php if($isResolved): ?>

            <?= Html::a('View', ['view-topic', 'topicId' => $model->id], ['class' => 'btn btn-default btn-sm', 'target' => '_blank']); ?>

        <?php else: ?>

            <?= Html::a('Hold', ['view-topic', 'topicId' => $model->id], ['class' => 'btn btn-default btn-sm', 'target' => '_blank']); ?>

        <?php endif ?>

    </div>
</div>
