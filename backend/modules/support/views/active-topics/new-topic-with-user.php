<?php

/** @var \yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \common\models\message\forms\TopicForm $topicForm */
$this->title = "Create dialog with ".$topicForm->getMemberUser()->username;

if(!empty(app('request')->get())){
    $topicForm->load(app('request')->queryParams);
}
$form = ActiveForm::begin();

echo $form->field($topicForm, 'title')->textInput();

echo $form->field($topicForm, 'message')->textarea();

echo Html::submitButton('Create topic', ['class' => 'btn btn-primary']);

ActiveForm::end();