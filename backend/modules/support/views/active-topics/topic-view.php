<?php


use backend\modules\support\assets\TopicEditAssets;
use backend\modules\support\components\SupportTopicFacade;
use backend\modules\support\components\ViewHelper;
use common\models\base\UserAdmin;
use common\models\message\helpers\UrlHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\User $user */
/** @var \common\models\UserAdmin $adminUser */
/** @var \common\models\MsgTopic $topic */
/** @var \yii\data\ActiveDataProvider $messagesDataProvider */
/** @var int $lastMessageId */

TopicEditAssets::register($this);
$otherUsersNames = $topic->getChatUsersList($user);
$isResolved = SupportTopicFacade::getIsResolved($topic);
?>
<div class="topic-view">

    <div class="header">
        <div class="row">
            <div class="col-md-4">

                <?=
                    _t('front.chat', 'Dialogue with <i>{usernames}</i> on theme <b>{theme}</b>', [
                        'usernames' => implode(', ', $otherUsersNames),
                        'theme' => H($topic->title)
                    ]);
                ?>

                <?php if($topic->getBindedObject()): ?>
                    For object <?=$topic->bind_to?> <?= Html::a('#'.H($topic->getBindedObject()->id), UrlHelper::bindedObjectBackendRoute($topic->getBindedObject()), ['target' => '_blank']) ?>
                <?php endif; ?>

            </div>
            <div class="col-md-8">
                <div class="pull-right">

                    <?php if(!$isResolved) : ?>

                        <?php if(!$topic->isBinded()) :

                            preg_match('/\#(\d+)/', $topic->title, $mathes);
                            $possibleOrderId = $mathes[1] ?? "";
                            ?>

                            <form method="get"
                                  action="<?= Url::toRoute(['/support/active-topics/bind-order'])?>"
                                  class="form-inline pull-left"
                                  style="margin-right: 10px; <?= $possibleOrderId ? 'border : 3px solid red; padding : 3px  ' : ''?>">
                                <div class="form-group">
                                    <input type="number" step="1" class="form-control input-sm" name="orderId" value="<?= $possibleOrderId?>">
                                    <input type="hidden" name="topicId" value="<?= $topic->id?>">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-sm">Bind to Order</button>
                                </div>
                            </form>

                        <?php endif ?>

                        <?= yii\bootstrap\ButtonDropdown::widget([
                            'label' => 'Forward',
                            'options' => [
                                'class' => 'btn-default btn-sm'
                            ],
                            'dropdown' => [
                                 'items' => array_map(function(UserAdmin $user) use ($topic)
                                 {
                                     return ['label' => H($user->username), 'url' => ['forward-topic', 'topicId' => $topic->id, 'newSupportUserId' => $user->id]];
                                 }, ViewHelper::supportUsersList($adminUser))
                              ],
                        ]) ?>

                        <?= Html::a('Resolved', ['resolve-topic', 'topicId' => $topic->id], ['class' => 'btn btn-default btn-sm resolve-btn'])?>

                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>

    <?php
        \yii\widgets\Pjax::begin([
            'enablePushState' => false,
            'id' => 'support-dialog-chat',
            'linkSelector' => false,
            'timeout' => 10000
        ]);

        echo \yii\widgets\ListView::widget([
            'id' => 'chat-list',
            'dataProvider' => $messagesDataProvider,
            'itemView' => 'message-item',
            'summary' => false,
            'viewParams' => [
                'user' => $user,
            ],
        ]);

        \yii\widgets\Pjax::end();
    ?>

    <div class="add-message">


        <div class="row">

            <div class="col-md-10">
                <textarea name="message" placeholder="Typegg Message ..." class="form-control chat-send-message"></textarea>
            </div>

            <div class="col-md-2">
                <div style="margin-bottom: 20px">
                    <?php echo Html::fileInput('files', null, [
                        'multiple' => true,
                        'class' => 'js-chat-send-files'
                    ]);?>
                </div>
                <div>
                    <button type="button" class="btn btn-warning btn-flat chat-send">Send</button>
                </div>
            </div>
        </div>

    </div>

</div>


<script type="text/javascript">
support = {
    topicId : <?=$topic->id?>,
    isResolved : <?=(int)$isResolved?>,
    lastMessageId : <?= $lastMessageId?>
};
</script>
