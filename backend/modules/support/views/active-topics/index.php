<?php
/** @var \common\models\User $user */
use backend\modules\support\components\ViewHelper;
use common\models\MsgMember;
use kartik\form\ActiveForm;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/** @var \common\models\UserAdmin $adminUser */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \yii\web\View $this */
/** @var \backend\modules\support\models\TopicFilter $filter */

\backend\modules\support\assets\TopicListAssets::register($this);

?>

<div class="topic-filter">

    <div class="">
        <?php
            $form = ActiveForm::begin([
                'id' => 'login-form-inline',
                'type' => ActiveForm::TYPE_INLINE,
                'method' => 'get'
            ]);
        ?>
        <div class="pull-left">
            <?= $form->field($filter, 'order_id')->textInput(['class' => 'input-sm']) ?>
            <?= $form->field($filter, 'text')->textInput(['class' => 'input-sm']) ?>
        </div>
        <div class="pull-right">
            <?= $form->field($filter, 'supportUserId')->dropDownList(['' => ''] + ArrayHelper::map(ViewHelper::supportUsersList(), 'id', 'username'), ['class' => 'input-sm']) ?>
            <?= $form->field($filter, 'username')->textInput(['class' => 'input-sm']) ?>
            <?php echo $form->field($filter, 'type')->dropDownList(
                ['' => 'All',
                    MsgMember::TYPE_UNREAD => MsgMember::TYPE_UNREAD,
                    MsgMember::TYPE_WITHOUT_ANSWER => MsgMember::TYPE_WITHOUT_ANSWER
                ],['class' => 'input-sm']) ?>
            <?=
                DatePicker::widget([
                    'model' => $filter,
                    'attribute' => 'createdDate',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'size' => 'sm',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
            ?>
            <?= Html::submitButton('Find', ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
    <div class="clearfix"></div>
</div>


<div class="topic-list">

    <?php \yii\widgets\Pjax::begin(['id' => 'topic-list', 'linkSelector' => false, 'timeout' => 5000]); ?>

    <?=
        \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'topic-item',
            'layout' => '{items} <div class="pager-wrapper">{pager}</div>',
            'viewParams' => [
                'user' => $user,
                'adminUser' => $adminUser
            ],
        ]);
    ?>

    <?php \yii\widgets\Pjax::end(); ?>

</div>







