<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 22.10.15
 * Time: 16:28
 */

namespace backend\modules\support\models;


use common\components\exceptions\AssertHelper;
use common\models\MsgMember;
use common\models\MsgTopic;
use common\models\User;
use yii\base\Model;
use common\components\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;


/**
 * Filter for topics
 * @package backend\modules\support\models
 */
class TopicFilter extends Model
{
    /**
     * Id of support user (which is holded - field hold_support_user_id)
     * @var string
     */
    public $supportUserId;

    public $order_id;

    public $text;

    /**
     * Part of username
     * @var string
     */
    public $username;

    /**
     * Created date
     * @var string
     */
    public $createdDate;

    public $type;

    /** @var bool */
    public $excludeBotMessages = false;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['supportUserId','order_id'], 'integer'],
            [['type','text'], 'string'],
            ['username', 'safe'],
            ['createdDate', 'date', 'format' => 'yyyy-MM-dd'],
        ];
    }



    /**
     * Add filter criteria to $query
     * @param ActiveQuery $query
     */
    public function addToQuery(ActiveQuery $query)
    {
        AssertHelper::assertValidate($this);

        $query->andFilterWhere([
            MsgTopic::column('hold_support_user_id') => $this->supportUserId,
        ]);

        if ($this->excludeBotMessages) {
            $query->andWhere(['not in', 'msg_topic.id', [MsgTopic::BOT_MESSAGE_ALL, MsgTopic::BOT_MESSAGE_COMPANIES]]);
        }

        if($this->username)
        {
            $query->joinWith(['members' => function(ActiveQuery $query)
            {
                $query
                    ->from(['m2' => 'msg_member'])
                    ->joinWith('user uu')
                    ->andWhere(['!=', 'm2.user_id', User::USER_ID_SUPPORT]);
            }], false);

            $query->andWhere(['like', 'uu.username', $this->username]);
        }

        if($this->createdDate)
        {
            $query->andWhere('date('.MsgTopic::column('created_at').') = :date', ['date' => $this->createdDate]);
        }

        if($this->type === MsgMember::TYPE_UNREAD) {
            $query->andWhere(['<>', 'msg_member.have_unreaded_messages', 0]);
        }

        if($this->type === MsgMember::TYPE_WITHOUT_ANSWER) {
            $query->innerJoinWith('msgMessages');
            $msgQuery = (new Query)->from(['mms' => 'msg_message']);
            $msgQuery->select(['id' => new Expression("max(mms.id)")]);
            $msgQuery->groupBy('mms.topic_id');
            $query->innerJoin(['msg' => $msgQuery], 'msg_message.id=msg.id and msg_message.user_id <>' . User::USER_ID_SUPPORT);
        }

        $query->andFilterWhere(['msg_topic.bind_id' => $this->order_id]);
        if($this->text) {
            $query->innerJoinWith('msgMessages');
            $query->andWhere(['like','msg_message.text',$this->text]);
        }
    }
}