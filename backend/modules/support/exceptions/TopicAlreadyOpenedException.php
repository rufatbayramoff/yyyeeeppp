<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 21.10.15
 * Time: 17:53
 */

namespace backend\modules\support\exceptions;


use backend\modules\support\components\SupportTopicFacade;
use common\models\base\UserAdmin;
use common\models\MsgTopic;
use yii\base\UserException;

/**
 * Exception then user wont make anything with topic, but topic already opened by other user
 * @package backend\modules\support\exceptions
 */
class TopicAlreadyOpenedException extends UserException
{
    /**
     * @param MsgTopic $topic
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function __construct(MsgTopic $topic)
    {
        $openTopicUserId = SupportTopicFacade::getOpenTopicUserId($topic);

        if(!$openTopicUserId)
        {
            throw new \yii\base\Exception('Bad logic');
        }

        /** @var UserAdmin $userAdmin */
        $userAdmin = UserAdmin::tryFindByPk($openTopicUserId);

        \Exception::__construct("Topic already opened by {$userAdmin->username}");
    }

}