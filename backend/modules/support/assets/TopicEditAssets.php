<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 21.10.15
 * Time: 15:27
 */

namespace backend\modules\support\assets;


use yii\web\AssetBundle;

/**
 * AssetBundle for module support
 * @package app\modules\support
 */
class TopicEditAssets extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/modules/support/assets/assets';

    /**
     * @var array
     */
    public $css = [
        'topic-view.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'topic-view.js',
    ];
}