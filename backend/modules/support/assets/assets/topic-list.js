/**
 * Created by mitaichik on 21.10.15.
 */

$(function()
{
    /**
     * Periodically update topic list
     */
    window.setInterval(function()
    {
        $.pjax.reload('#topic-list', {replace : false, timeout : 5000});
    }, 5000);
});
