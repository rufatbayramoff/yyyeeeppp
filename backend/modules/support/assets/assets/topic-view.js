/**
 * Created by mitaichik on 21.10.15.
 */

/**
 * Open topic handler
 */
$(function()
{
    /**
     * Periodically send messages to server that topic is opended
     */
    window.setInterval(function()
    {
        if(!support.isResolved)
        {
            $.post('/support/active-topics/topick-opened?topicId=' + support.topicId);
        }
    }, 5000);


    /**
     * Send message to server that tab is closed.
     * It does not guarantee that request will be sent
     */

    $(window).on("unload", function() {
        if(!support.isResolved) {
            $.post('/support/active-topics/topic-closed?topicId=' + support.topicId);
        }
    });
});


/**
 * Elements positions
 */
$(function()
{
    var contentWraper = $('.content-wrapper'),
        contentHeader = $('.content-header'),
        topicView = $('.topic-view');

    function resize()
    {
        var wrapHeight = parseInt(contentWraper.css('min-height'));
        topicView.css('height', (wrapHeight - (contentHeader.height() + 30)) + 'px');
    }

    $(window).resize(resize);
    window.setTimeout(resize, 100);

});

/**
 * Chat handlers
 */
jQuery(function($)
{
    var messageInput = $('.chat-send-message'),
        filesInput = $('.js-chat-send-files'),
        chatUrl = '/support/active-topics/add-message?topicId=' + support.topicId;


    /**
     * Update chat
     */
    var updateChatFn = function()
    {
        if($('#support-dialog-chat').length > 0)
        {
            var dialogUrl = support.isResolved
                ? '/support/resolved-topics/view-topic?topicId=' + support.topicId
                : '/support/active-topics/view-topic?topicId=' + support.topicId;

            $.pjax.reload('#support-dialog-chat', {url : dialogUrl, replace : false, timeout : 5000});
        }
    };

    /**
     * Prevent sending form on message input
     */
    messageInput.keypress(function(e)
    {
        if((e.keyCode === 10 || e.keyCode === 13) && e.ctrlKey){
            sendMessage();
            e.preventDefault();
            return false;
        }
    });

    /**
     * Send message button handler
     */
    $('.chat-send').on('click', function(e)
    {
        e.preventDefault();
        sendMessage();
        return false;
    });

    /**
     * Send message to server
     * @returns {boolean}
     */
    function sendMessage()
    {
        var msg = messageInput.val(),
            files = filesInput[0].files;

        if(msg === '' && files.length === 0) {
            return false;
        }

        var data = new FormData();

        $.each(files, function(i, file) {
           data.append('files[]', file);
        });

        data.append('MessageForm[message]', msg);

        $.ajax({
            url: chatUrl,
            type: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            data: data,
            success : function (){
                messageInput.val('');
                filesInput.val('');
                messageInput.focus();
                support.isResolved = false;
                updateChatFn();
            }
        });
    }

    /**
     * Scroll chat to bottom
     */
    var scrollToChatBottomFn = function()
    {
        var chatArea = $("#support-dialog-chat")[0];

        if (chatArea) {
            chatArea.scrollTop = chatArea.scrollHeight;
        }
    };

    /**
     * Autoupdate messages
     */


    $('#support-dialog-chat').on('pjax:success', function(x,y,z,response)
    {
        var lastMessageId = parseInt(response.getResponseHeader('LastMessageId'));

        if(lastMessageId > support.lastMessageId){
            support.lastMessageId = lastMessageId;
            scrollToChatBottomFn();
        }
        $('.chat-send-message').focus();
        window.setTimeout(updateChatFn, 10000);
    });
    window.setTimeout(updateChatFn, 10000);
    window.setTimeout(scrollToChatBottomFn, 200);


});