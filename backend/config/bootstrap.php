<?php

function checkWhitelistIp()
{
    $ips = file_get_contents('../runtime/ips.txt');
    $ips .= '
allow 94.180.253.137; # office ip
allow 74.208.155.35; # Server
allow 92.255.198.33; # gchannel
allow 5.9.199.107; # zabbix server';
    $ipRows = explode("\n", $ips);
    $ipList = [];
    foreach ($ipRows as $ip) {
        $ip = str_replace('allow', '', $ip);
        if (strpos($ip, '#') > 0) {
            $ip = substr($ip, 0, strpos($ip, '#'));
        }
        $ipList[] = trim($ip, " \t\n\r\0\x0B;");
    }
    $ipList = array_filter($ipList);
    $currentIp = $_SERVER['REMOTE_ADDR'] ?? null;
    if (!in_array($currentIp, $ipList, true)) {
        header('HTTP/1.1  403 Forbidden');
        echo '<html><head><meta http-equiv="content-type" content="text/html; charset=windows-1252"><title>403 Forbidden</title></head>' .
            '<body bgcolor="white"><center><h1>403 Forbidden</h1></center><hr><center>nginx</center></body></html>';
        exit;
    }
}

if (defined('BACKEND_FILTER') && BACKEND_FILTER === true) {
    checkWhitelistIp();
}
