<?php
return [
    [
        'group'  => 'system',
        'title'  => 'System',
        'access' => [
            'view'                 => 'View system tab',
            'view_access'          => 'View user access',
            'edit_access'          => 'Edit user access',
            'access.view'          => 'View group/user access',
            'access.update'        => 'Update group/user access',
            'access.delete'        => 'Delete group/user access',
            'moderlog.view'        => 'View moderator logs',
            'api_systems.settings' => 'Api system settings access',
            'httplog.view'         => 'View http logs',
            'content_filter'       => 'Content filter system',
            'watermark.view'       => 'Watermark Decoder'
        ]
    ],
    [
        'group'  => 'website',
        'title'  => 'Website',
        'access' => [
            'view'                   => 'View website tab',
            'view_lang'              => 'View lang',
            'view_currency'          => 'View currency',
            'view_translaction'      => 'View transaction',
            'translation_statistics' => 'Transaction statistics',
            'siteblock.view'         => 'View site blocks',
            'siteblock.update'       => 'Update site blocks',
            'siteblock.add'          => 'Create site blocks',
            'siteblock.delete'       => 'Delete site block',

            'sitehelp.view'   => 'View site help',
            'sitehelp.update' => 'Update site help',
            'sitehelp.add'    => 'Create site help',
            'sitehelp.delete' => 'Delete site help',

            'geocountry.view'   => 'Geo country view',
            'geocountry.update' => 'Geo country update',

            'static_page.view'   => 'View Static Pages',
            'static_page.add'    => 'Add Static Pages',
            'static_page.update' => 'Update Static Pages',
            'static_page.delete' => 'Delete Static Pages',
            'hiredesigner.view'  => 'Hire Designer',
            'widget.statistics'  => 'View external widget statistics',
            'site.cache'         => 'Site caches',
            'wiki_material'      => 'Wiki materials',
            'wiki_machine'       => 'Wiki machines',
            'connected_app'      => 'Apps',
            'home_page.edit'     => 'Home page edit'
        ]
    ],
    [
        'group'  => 'seo_smm',
        'title'  => 'SEO & SMM',
        'access' => [
            'siteblock.smm' => 'View SMM Block',

            'seo_page.view'   => 'View SEO Page',
            'seo_page.update' => 'Update SEO Page',
            'seo_page.add'    => 'Create SEO Page',
            'seo_page.delete' => 'Delete SEO Page',

            'seo_redir.view'   => 'View SEO Redir',
            'seo_redir.update' => 'Update SEO Redir',
            'seo_redir.add'    => 'Create SEO Redir',
            'seo_redir.delete' => 'Delete SEO Redir',

        ]
    ],
    [
        'group'  => 'setting',
        'title'  => 'Setting tab',
        'access' => [
            'add'    => 'Add setting',
            'delete' => 'Delete setting',
            'update' => 'Update setting',
            'view'   => 'View setting'
        ]
    ],
    [
        'group'  => 'model3d',
        'title'  => 'Model 3D',
        'access' => [
            'download'        => 'Download Model3d Files',  // model3d.download
            'add'             => 'Add model3d',
            'delete'          => 'Delete model3d',
            'update'          => 'Update model3d',
            'view'            => 'View model3d',
            'file_job.view'   => 'View File Jobs',
            'file_job.update' => 'Update File Jobs',
            'file_job.delete' => 'Delete File Jobs',
        ]
    ],
    [
        'group'  => 'model3d_image',
        'title'  => 'Model 3d Images',
        'access' => [
            'add'    => 'Add model3d_image',
            'delete' => 'Delete model3d_image',
            'update' => 'Update model3d_image',
            'view'   => 'View model3d_image'
        ]
    ],
    [
        'group'  => 'user',
        'title'  => 'User',
        'access' => [
            'delete'                       => 'Delete user account',
            //'restore' => 'Restore user account',
            'update'                       => 'Update user',
            'view'                         => 'View users',
            'login_as'                     => 'Login as user',
            'wipe'                         => 'Wipe user',
            'user_request.view'            => 'View user requests',
            'user_request.cancel'          => 'Cancel user requests',
            'user_request.account_delete'  => 'Delete user',
            'user_request.account_restore' => 'Restore user',

            'user_comment.view'   => 'View user comments',
            // 'user_comment.add' => 'Add user comments',
            'user_comment.delete' => 'Delete user comments',
            'user_comment.update' => 'Update user comments',

        ]
    ],
    [
        'group'  => 'storeunit',
        'title'  => 'Store Units',
        'access' => [
            'view'     => 'View store items',
            'publish'  => 'Publish new store unit',
            'moderate' => 'Moderate',
            'reject'   => 'Can reject store unit'
        ]
    ],
    [
        'group'  => 'store_categories',
        'title'  => 'Store categories',
        'access' => [
            'view'   => 'Can view',
            'add'    => 'Can add store categories',
            'delete' => 'Can delete store categories',
            'update' => 'Can update store categories',
        ]
    ],
    [
        'group'  => 'store_order',
        'title'  => 'Store Order',
        'access' => [
            'view'                      => 'Can view orders',
            'moderate'                  => 'Can moderate orders',
            'reject'                    => 'Can reject order',
            'payment.view'              => 'View payments',
            'payment.update'            => 'Payment updates/Refunds',
            'paymentbankinvoice.view'   => 'View invoices',
            'paymentbankinvoice.add'    => 'Add invoices',
            'paymentbankinvoice.delete' => 'Delete invoices',
            'paymentbankinvoice.update' => 'Update invoices',
        ]
    ],
    [
        'group'  => 'support',
        'access' => [
            'view'                 => 'View support block',
            'message_reports'      => 'Messages Report',
            'message.downloadfile' => 'Download message files'
        ]
    ],
    [
        'group'  => 'crud',
        'access' => [
            'view' => 'View CRUD block',
        ]
    ],

    [
        'group'  => 'printer',
        'access' => [
            'view'   => 'can view system printer information',
            'add'    => 'can add printer information',
            'update' => 'can update printer information',
            'delete' => 'can delete printer information',

            'printer_review.view'   => 'can view printer review information',
            'printer_review.add'    => 'can add printer review information',
            'printer_review.update' => 'can update printer review information',
            'printer_review.delete' => 'can delete printer review information',
        ]
    ],

    [
        'group'  => 'ps',
        'title'  => 'Print service ',
        'access' => [
            'view'     => 'can view ps moderation',
            'moderate' => 'can moderate ps printer'
        ]
    ],
    [
        'group'  => 'psmachine',
        'title'  => 'Company services ',
        'access' => [
            'view'                    => 'can view company services',
            'moderate'                => 'can moderate company services',
            'company.category'        => 'Company service categories',
            'company_service.reject'  => 'Company service reject',
            'company_service.approve' => 'Company service approve',
            'company.view'            => 'Companies (Catalog)'
        ]
    ],
    [
        'group'  => 'product',
        'title'  => 'Product',
        'access' => [
            'view'             => 'View',
            'update'           => 'Update',
            'reject'           => 'reject',
            'approve'          => 'approve',
            'product_category' => 'Product Categories'

        ]
    ],
    [
        'group'  => 'dynamic_field',
        'title'  => 'Dynamic fields',
        'access' => [
            'view' => 'View',
        ]
    ],
    [
        'group'  => 'psprinter',
        'title'  => 'Print Service printer ',
        'access' => [
            'view'     => 'can view ps printer moderation',
            'moderate' => 'can moderate ps printer'
        ]
    ],
    [
        'group'  => 'psprinter.testorder',
        'title'  => 'Print Service Test Orders ',
        'access' => [
            'view' => 'can view ps printer test orders moderation',
        ]
    ],
    [
        'group'  => 'psprintertest',
        'title'  => 'Print Service printer Test',
        'access' => [
            'view'    => 'can view',
            'docs'    => 'can view document',
            'approve' => 'can approve',
            'reject'  => 'can reject'
        ]
    ],
    [
        'group'  => 'systemreject',
        'access' => [
            'view'   => 'can view reject templates',
            'add'    => 'can add reject templates',
            'update' => 'can update reject templates',
            'delete' => 'can delete reject templates'
        ]
    ],
    [
        'title'  => 'Custom and taxes',
        'group'  => 'custom',
        'access' => [
            'view'   => 'can view general tax data',
            'add'    => 'can add',
            'update' => 'can update',
            'delete' => 'can delete'
        ]
    ],
    [
        'group'  => 'usertaxinfo',
        'title'  => 'User Taxes Information',
        'access' => [
            'payout'   => 'Can request user payout',
            'view'     => 'can view user tax information',
            'moderate' => 'Can moderate user tax information',
            'reject'   => 'Can reject tax information',
            'approve'  => 'Can approve tax information'
        ]
    ],
    [
        'group'  => 'emailtemplate',
        'title'  => 'Email templates',
        'access' => [
            'view'   => 'Can view email templates',
            'delete' => 'Can delete email templates',
            'update' => 'Can update email templates',
            'add'    => 'Can add email templates'
        ]
    ],


    [
        'group'  => 'license',
        'title'  => 'License',
        'access' => [
            'view'   => 'can view licenses',
            'update' => 'can update licenses',
            'delete' => 'can delete licenses',
            'add'    => 'can add licenses'
        ]
    ],
    [
        'group'  => 'payment',
        'title'  => 'Payment',
        'access' => [
            'reports'  => 'Balance reports',
            'log'      => 'Payment logs',
            'warnings' => 'Payment alarms',
        ]
    ],
    [
        'group'  => 'statistic',
        'title'  => 'Statistic',
        'access' => [
            'view'               => 'Can view statistic',
            'view_filepage_maps' => 'Can view filepage maps statistics',
            'statistic.view.tax' => 'Can view tax report',
            'pay_page_log'       => 'Pay page log'
        ]
    ],
    [
        'group'  => 'blog',
        'title'  => 'Blog',
        'access' => [
            'view' => 'Can view blog',
        ]
    ],
    [
        'group'  => 'site',
        'title'  => 'Site tag',
        'access' => [
            'sitetag.view' => 'Can update site tags',
        ]
    ],
    [
        'group'  => 'affiliate',
        'title'  => 'Affiliates',
        'access' => [
            'affiliate.view' => 'Affiliate view',
        ]
    ],
    [
        'group'  => 'promocode',
        'title'  => 'Promocodes',
        'access' => [
            'promocode.view'   => 'Promocode view',
            'promocode.add'    => 'Promocode add',
            'promocode.update' => 'Promocode update',
            'promocode.delete' => 'Promocode delete',

            'promocodehistory.view' => 'Promocode History view',

            'promocodeusage.view' => 'Promocode Usage view',
        ]
    ],
    [
        'group'  => 'store_order_reviews',
        'title'  => 'Order review',
        'access' => [
            'view'   => 'Can view reviews',
            'update' => 'Can manage reviews',
        ]
    ],
    [
        'group'  => 'hiredesigner',
        'title'  => 'Hire Designer',
        'access' => [
            'view' => 'Can manage hire designer',
        ]
    ],
];