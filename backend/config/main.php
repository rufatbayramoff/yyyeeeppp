<?php

use common\bootstrap\Bootstrap;
use yii\web\Response;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => [
        'log',
        'sentry',
        'comments',
        [
            'class'   => 'yii\filters\ContentNegotiator',
            'formats' => [
                'text/plain'       => Response::FORMAT_RAW,
                'text/html'        => Response::FORMAT_HTML,
                'application/json' => Response::FORMAT_JSON
            ]
        ]
    ],
    'on beforeRequest'    => function () {
        if (param('siteMode', '') === 'down') {
            $letMeIn = Yii::$app->session['ts_skip'] || isset($_GET['ts_skip']);
            if (!$letMeIn) {
                Yii::$app->catchAll = [
                    'site/down',
                ];
                header('HTTP/1.1 503 Service Temporarily Unavailable');
                header('Status: 503 Service Temporarily Unavailable');
                header('Retry-After: 30');//30 seconds
                die("Site is upgrading. Please wait... <script>setTimeout(function(){ location.reload(); }, 15000);</script>");
            } else {
                Yii::$app->session['ts_skip'] = 1;
            }
        }
    },
    'modules'             => [
        'gridview'        => [
            'class' => '\kartik\grid\Module'
        ],
        'support'         => [
            'class' => \backend\modules\support\Module::class
        ],
        'statistic'       => [
            'class' => \backend\modules\statistic\Module::class,
        ],
        'company'         => [
            'class' => \backend\modules\company\CompanyModule::class,
        ],
        'comments'        => [
            'displayAvatar' => false,
            'class'         => 'common\modules\comments\Comments',
        ],
        'product'         => [
            'class'   => \backend\modules\product\ProductModule::class,
            'modules' => [
                'fields' => [
                    'class'               => \common\modules\dynamicField\DynamicFieldModule::class,
                    'controllerNamespace' => '\common\modules\dynamicField\controllers\backend'
                ]
            ]
        ],
        'payment'         => [
            'class' => \backend\modules\payment\Module::class,
        ],
        'cutting'         => [
            'class' => \backend\modules\cutting\Module::class,
        ],
        'tsCertification' => [
            'class' => \backend\modules\tsCertification\Module::class,
        ],

    ],
    'components'          => [

        'db'           => [
            'enableQueryCache' => false
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                'site/logout'              => 'site/site/logout',
                'site/login'               => 'site/site/login',
                'site/notify/get-notifies' => 'site/notify/get-notifies',
                'site/download-file'       => 'site/site/download-file',
                ''                         => 'site/site'
            ]
        ],
        'angular'      => [
            'class' => \frontend\components\angular\AngularService::class
        ],
        'user'         => [
            'identityClass'   => 'backend\models\user\UserAdmin',
            'enableAutoLogin' => false,
            'identityCookie'  => [
                'name' => '_backendUser', // unique for backend
            ]
        ],
        'session'      => [
            'name' => 'PHPBACKSESSID',
        ],
        'request'      => [
            'cookieValidationKey' => 'cfghbrf4sd3zxcpq',
            'csrfParam'           => '_backendCSRF',
            'parsers'             => [
                'application/json' => 'yii\web\JsonParser'
            ]
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => common\components\FileTarget::class,
                    'levels'  => ['error', 'warning'],
                    'logVars' => ['_GET'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/site/error',
        ],
        'view'         => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/views/yii2-app'
                ],
            ],
        ],
        'sentry'       => [
            'class'     => '\lib\sentry\Sentry',
            'enabled'   => true,
            'phpLogger' => [
                'dsn'       => 'https://ed7d6b5df3ed4f63a2034be01b9200d4@o4504125362798592.ingest.sentry.io/4504210450939904',
                'dsnTrash' => 'https://ed7d6b5df3ed4f63a2034be01b9200d4@o4504125362798592.ingest.sentry.io/4504210450939904',
                'logger'    => 'backend-php',
                'levels'    => ['error'],
            ],
            'jsLogger'  => [
                'dsn'       => 'https://ed7d6b5df3ed4f63a2034be01b9200d4@o4504125362798592.ingest.sentry.io/4504210450939904',
                'dsnTrash' => 'https://ed7d6b5df3ed4f63a2034be01b9200d4@o4504125362798592.ingest.sentry.io/4504210450939904',
                'logger'    => 'backend-js',
            ],
        ],
    ],
    'params'              => $params,
];
