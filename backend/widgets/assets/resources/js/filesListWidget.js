var filesListWidgetClass = {
    config : {
        'formPrefix': '',
        'formAttribute': '',
        'uid': ''
    },
    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
    },
    removeFile: function (fileInfo) {
        var self = this;
        var block = $('#' + fileInfo.blockId);
        block.hide();

        $("input[name*='"+self.config.formPrefix + '[' + self.config.formAttribute + "'][value='"+fileInfo.fileId+"']").remove();
        var html = '<input type=hidden name=' + self.config.formPrefix + '[' + self.config.formAttribute + '][' + fileInfo.fileId + '] value="deleted">';
        block.append(html);
    }
}




