<?php
/**
 * Created by mitaichik
 */

namespace backend\widgets;


use common\models\StoreOrderReview;
use yii\base\Widget;
use yii\grid\GridView;
use yii\helpers\Html;

class OrderReviewList extends Widget
{
    public $query;

    /**
     * @return string
     */
    public function run()
    {
        return GridView::widget(
            [
                'options' => ['class' => ''],
                'layout'       => '{items}',
                'tableOptions' => ['class' => 'table no-margin', ],
                'sorter'       => false,
                'showFooter'   => false,
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => $this->query,
                ]),
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'rating_speed',
                    'rating_quality',
                    'rating_communication',
                    'comment',
                    'status',
                    /*[
                        'attribute' => 'file_ids',
                        'format' => 'raw',
                        'label' => 'Images',
                        'value' => function(StoreOrderReview $review)
                        {
                            $html = '';
                            foreach($review->getFiles() as $file)
                            {

                                $previewUrl = \frontend\components\image\ImageHtmlHelper::getThumbUrl($file->getFileUrl(), 64, 64);
                                $html .= Html::a(Html::img($previewUrl, ['style' => 'max-width: 50px']), $file->getFileUrl(), ['target' => '_blank']).' ';
                            }
                            return $html;
                        }
                    ]*/
                ]
            ]
        );
    }
}