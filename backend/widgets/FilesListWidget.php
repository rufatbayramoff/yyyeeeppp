<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.12.16
 * Time: 12:02
 */

namespace backend\widgets;

use common\components\JsObjectFactory;
use backend\widgets\assets\FilesListWidgetAssets;
use kartik\base\Widget;
use yii\base\InvalidParamException;

class FilesListWidget extends Widget
{
    public const ALLOW_DELETE = 'allow_delete';
    public const ALLOW_ADD = 'allow_add';
    public const ALLOW_MULTIPLE = 'allow_multiple';
    public const ALLOW_ROTATE = 'allow_rotate';
    public const ALLOW_SET_MAIN = 'allow_set_main';

    public const VIEW_MODE_GALLERY = 'view_gallery';
    public const VIEW_MODE_LIST = 'view_list';

    public const COMPATIBLE_DROPZONE = 'compatible_dropzone';

    public $uid;
    public $filesList;
    public $rights = [];
    public $viewMode = self::VIEW_MODE_GALLERY;
    public $formPrefix = '';
    public $formAttribute = '';
    public $filesAllowedExtensions = [];
    public $emptyText = null;
    public $compatible = '';

    /**
     * @var array ['attribute' => '', 'activeUuid' => '']
     */
    public $setMainFile = [
        'attribute'  => false,
        'activeUuid' => false
    ];

    /**
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        if (in_array(self::ALLOW_ADD, $this->rights, true)) {
            if (!$this->formAttribute) {
                throw new InvalidParamException('FilesListWidget exception. Please specify formPrefix, formAttribute params, for add file.');
            }
        }

        if (in_array(self::ALLOW_SET_MAIN, $this->rights, true)) {
            if (!isset($this->setMainFile['attribute']) || !$this->setMainFile['attribute']) {
                throw new InvalidParamException('FilesListWidget exception. Please specify formPrefix, setMainFile[\'attribute\'] params, for set main file.');
            }
        }

        if (!$this->uid) {
            $this->uid = mt_rand(0, 9999) . '_' . mt_rand(0, 9999);
        }
        if ($this->emptyText === null) {
            $this->emptyText = _t('back.filesList', 'Empty, add photos');
        }
        $this->registerAssets();
        return $this->render(
            'FilesList',
            [
                'emptyText'              => $this->emptyText,
                'uid'                    => $this->uid,
                'files'                  => $this->filesList,
                'rights'                 => $this->rights,
                'viewMode'               => $this->viewMode,
                'formPrefix'             => $this->formPrefix,
                'formAttribute'          => $this->formAttribute,
                'filesAllowedExtensions' => $this->filesAllowedExtensions,
                'dropzoneMode'           => $this->compatible == self::COMPATIBLE_DROPZONE,
                'setMainFile'            => $this->setMainFile
            ]
        );
    }

    public function registerAssets()
    {
        $this->getView()->registerAssetBundle(FilesListWidgetAssets::class);
        JsObjectFactory::createJsObject(
            'filesListWidgetClass',
            'filesListWidgetObj' . $this->uid,
            [
                'uid'           => $this->uid,
                'formPrefix'    => $this->formPrefix,
                'formAttribute' => $this->formAttribute
            ],
            $this->getView()
        );
    }


}