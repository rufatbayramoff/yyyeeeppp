<?php
/**
 * User: nabi
 */

namespace backend\widgets;


use backend\models\system\ExcelImportForm;
use yii\base\Widget;

class ExcelImportWidget extends Widget
{
    public $tableName;

    public function run()
    {
        $model = ExcelImportForm::create($this->tableName);
        $model->tableName = $this->tableName;
        return $this->render('excelImport', [
            'model' => $model
        ]);
    }
}