<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$form = ActiveForm::begin([
    'action'  => '/system/import',
    'options' => ['enctype' => "multipart/form-data",],
    'layout'  => 'inline'
]);
?>

<?= $form->field($model, 'tableName')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'file')->fileInput() ?>
<?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>
<?php
if ($model->getTemplateFields()) {
    ?>
    <a href="/system/import/template?tableName=<?= $model->tableName ?>">Download Template</a>
    <?php
}

?>
<?php ActiveForm::end(); ?>