<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.12.16
 * Time: 12:14
 */

use backend\widgets\FilesListWidget;
use common\models\File;
use frontend\components\image\ImageHtmlHelper;
use yii\helpers\Html;

/** @var File[] $files */
/** @var array $rights */
/** @var string $formPrefix */
/** @var string $formAttribute */
/** @var array $filesAllowedExtensions */
/** @var bool $dropzoneMode */
/** @var array $setMainFile */


if (!$files) {
    echo $emptyText;
} else {

    foreach ($files as $index => $file) {
        if ($file->isNewRecord) continue;
        ?>
        <div id='file-image-block-<?= $uid ?>-<?= $file->uuid ?>' class="files-list-wiget-thrumb-element">
            <table>
                <tr>
                    <td>
                        <?php if (in_array(FilesListWidget::ALLOW_ROTATE, $rights, true)): ?>
                            <a href="<?php
                            echo \yii\helpers\Url::toRoute(
                                [
                                    '/site/image/rotate',
                                    'direction' => 'right',
                                    'fileUuid'  => $file->uuid
                                ]
                            );
                            ?>" data-method="post"><span class="fa fa-rotate-right"></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <a href="<?= $file->getFileUrl() ?>" target='_blank' class="jspopup">
                            <?php if (\common\components\FileTypesHelper::isImage($file)) { ?>
                                <img src="<?= ImageHtmlHelper::getThumbUrlForFile($file, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL) ?>"
                                     title="<?= $file->name ?>">
                            <?php } else { ?>
                                <?= $file->name ?>
                            <?php } ?>
                        </a>
                    </td>
                    <td>
                        <?php if (in_array(FilesListWidget::ALLOW_ROTATE, $rights, true)): ?>
                            <a href="<?php
                            echo \yii\helpers\Url::toRoute(
                                [
                                    '/site/image/rotate',
                                    'direction' => 'left',
                                    'fileUuid'  => $file->uuid
                                ]
                            );
                            ?>" data-method="post"><span class="fa fa-rotate-left"></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php
                        if (in_array(FilesListWidget::ALLOW_ADD, $rights, true)) {
                            ?>
                            <a href="#" onclick='filesListWidgetObj<?= $uid ?>.removeFile($(this).data());return false;'
                               data-block-id="file-image-block-<?= $uid ?>-<?= $file->uuid ?>"
                               data-file-id="<?= $file->uuid ?>"
                               class="tsi tsi-bin files-list-widget-delete-button"
                               title="<?= _t('back.filesList', 'Delete') ?>"
                            ></a>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php if (in_array(FilesListWidget::ALLOW_SET_MAIN, $rights, true)): ?>
                    <tr>
                        <td colspan="4">
                            <label>
                                <?php echo Html::radio($formPrefix . '[' . $setMainFile['attribute'] . ']', $file->uuid === $setMainFile['activeUuid'], [
                                    'value' => $file->uuid
                                ]); ?> Set main file
                            </label>
                        </td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
        <?php
        if ($dropzoneMode) {
            ?>
            <input type="hidden" name="<?= $formPrefix ?>[<?= $formAttribute ?>][<?= $index ?>][uuid]"
                   value="<?= $file->uuid ?>">
            <?php
        }
    }
}
if (in_array(FilesListWidget::ALLOW_ADD, $rights, true)) {
    echo '<br> <br>';
    $allowExtStr = $filesAllowedExtensions ? '.' . implode(', .', $filesAllowedExtensions) : '';
    if (in_array(FilesListWidget::ALLOW_MULTIPLE, $rights, true)) {
        ?>
        <div class="form-group">
            <input type="hidden" name="<?= $formPrefix ?>[<?= $formAttribute ?>][]" value="">
            <input
                    type="file"
                    id="<?= $formPrefix ?>[<?= $formAttribute ?>][]"
                    class="inputfile inputfile--wide inputfile--sm"
                    name="<?= $formPrefix ?>[<?= $formAttribute ?>][]"
                    multiple=""
                    data-multiple-caption="{count} files selected"
                    value=""
                    accept="<?= $allowExtStr ?>"
                    aria-required="true">
            <label class="uploadlabel" for="<?= $formPrefix ?>[<?= $formAttribute ?>][]">
                <span>0 files selected</span>
                <strong>
                    <i class="tsi tsi-upload-l"></i> <?= _t('back.filesList', 'Add photo') ?>
                </strong>
            </label>
        </div>
        <?php
    } else {
        ?>
        <div class="form-group">
            <input type="hidden" name="<?= $formPrefix ?>[<?= $formAttribute ?>][]" value="">
            <input
                    type="file"
                    id="<?= $formPrefix ?>[<?= $formAttribute ?>][]"
                    class="inputfile inputfile--sm"
                    name="<?= $formPrefix ?>[<?= $formAttribute ?>][]"
                    value=""
                    accept="<?= $allowExtStr ?>"
                    aria-required="true">
            <label class="uploadlabel" for="<?= $formPrefix ?>[<?= $formAttribute ?>][]">
                <span>
                    <i class="tsi tsi-upload-l"></i> <?= _t('back.filesList', 'Add photo') ?>
                </span>
            </label>
        </div>
        <?php
    }
}

