<?php

namespace backend\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\PsPrinter;
use common\models\StoreOrder;

class StoreOrderSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            StoreOrder::class => [
                'id',
                'title',
                'currency'
            ]
        ];
    }
}