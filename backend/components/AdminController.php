<?php namespace backend\components;

use common\components\BaseActiveQuery;
use common\components\BaseAR;
use common\components\HttpRequestLogger;
use common\models\HttpRequestLog;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;

/**
 * Base controller for controllers in admin part
 * add general functions and behaviors
 */
class AdminController extends \common\components\BaseController
{
    protected $enableSeo = false;
    /**
     * main model for controller actions
     *
     * @var \common\components\BaseAR
     */
    public $searchModel;

    /**
     *
     * @var \common\components\BaseAR
     */
    public $mainModel;

    protected $isBackend = true;

    public static $user;

    /**
     * init default language for backend
     */
    public function init()
    {
        BaseActiveQuery::$staticCacheGlobalEnable = false;
        parent::init();

        // in backend use only en-US
        $this->detectLanguage = 0;
        $this->enableSeo = false;

        $cookie = \Yii::$app->getResponse()->getCookies()->getValue('tsbackend_admin', false);
        if ($cookie === false) {
            $cookie = new \yii\web\Cookie([
                'name'   => 'tsbackend_admin',
                'value'  => '1',
                'expire' => time() + 86400 * 365,
                'domain' => '.treatstock.com'
            ]);
            \Yii::$app->getResponse()->getCookies()->add($cookie);
        }
    }

    /**
     * get current admin user
     *
     * @return \common\models\User|boolean
     */
    public function getCurrentUser($redir = null)
    {
        if (self::$user) {
            return self::$user;
        }
        if (is_guest()) {
            return false;
        }
        self::$user = app('user')->getIdentity();
        return self::$user;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge([
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow'   => true
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    '*'      => ['get', 'post', 'put', 'delete'],
                    'logout' => ['post']
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * @param $action
     *
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        app()->httpRequestLogger->initRequestLog(HttpRequestLog::REQUEST_TYPE_BACKEND);
        BaseAR::$enableLog = true;

        return parent::beforeAction($action);
    }
}
