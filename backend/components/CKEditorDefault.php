<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 31.05.2017
 * Time: 9:52
 */

namespace backend\components;


use dosamigos\ckeditor\CKEditor;

class CKEditorDefault extends CKEditor
{
    public static function getDefaults()
    {
        return [
            'options' => ['rows' => 6],
            'preset' => 'full',
            'clientOptions' => [
                'allowedContent' => true, //(bool)app('request')->get('allowcontent', false),
                'contentsCss' => param('server').'/css/site.css',
                'filebrowserUploadUrl' =>  \Yii::$app->getUrlManager()->createUrl('site/site-help/upload'),
                'removePlugins' => 'flash'
            ]
        ];
    }
}