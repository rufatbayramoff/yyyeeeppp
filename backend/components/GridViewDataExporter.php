<?php
/**
 * User: nabi
 */

namespace backend\components;


use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\Html;

class GridViewDataExporter
{
    /** @var Model */
    public $search;
    protected $exportClass = '';
    protected $writerClass = '';

    public function __construct(?Model $search, $exportClass = '', $writerClass = '')
    {
        $this->search      = $search;
        $this->exportClass = $exportClass;
        $this->writerClass = $writerClass;
    }

    public function __toString()
    {
        $searchParams = [];
        if ($this->search instanceof ActiveRecord) {
            $tableName    = $this->search->tableName();
            $attr         = array_filter($this->search->getAttributes());
            $searchParams = array_merge($searchParams, array_filter(get_object_vars($this->search)), $attr);
        } elseif ($this->search) {
            $tableName    = '';
            $searchParams = array_merge($searchParams, array_filter(get_object_vars($this->search)), []);
        } else {
            $tableName = null;
        }
        if ($this->writerClass) {
            $searchParams['writerClass'] = $this->writerClass;
        }
        $searchParams['tableName']   = $tableName;
        $searchParams['exportClass'] = $this->exportClass;

        return Html::a('<span class="fa fa-file-excel-o text-success"></span> Export', array_merge(['/system/export'], $searchParams), ['class' => 'btn btn-default']);
    }
}