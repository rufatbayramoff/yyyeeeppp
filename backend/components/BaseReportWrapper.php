<?php
/**
 * User: nabi
 */

namespace backend\components;


use backend\modules\statistic\reports\BaseReportInterface;
use common\components\BaseAR;
use yii\data\ActiveDataProvider;

/**
 * Class BaseReportWrapper
 * basic wrapper for report to be exported.
 *
 * @package backend\components
 */
class BaseReportWrapper implements BaseReportInterface
{
    /**
     * @var array
     */
    public $columns;

    /**
     * @var array
     */
    public $items;

    public $getParams;

    public function __construct($columns = [], $items = [])
    {
        $this->columns = $columns;
        $this->items = $items;
    }

    /**
     * @param array $cols
     */
    public function setColumns(array $cols)
    {
        $this->columns = $cols;
    }

    public function setItems(array $items)
    {
        $result = [];
        foreach($items as $k=>$v){
            $row = [];
            foreach($this->columns as $key=>$col){
                if(strpos($key, 'is_')!==false){
                    $v[$key] = (string)(int)$v[$key];
                }
                if(is_array($v[$key])){
                    $v[$key] = json_encode($v[$key]);
                }
                $row[$key] = (string)$v[$key];
            }
            $result[$k] = $row;
        }
        $this->items = $items;
    }

    public function getColumnsNames()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * factory method
     *
     * @param ActiveDataProvider $provider
     * @param BaseAR $model
     * @return BaseReportWrapper
     */
    public static function createFromDataProvider(ActiveDataProvider $provider, BaseAR $model)
    {
        $report = new BaseReportWrapper();
        $provider->pagination->setPageSize($provider->getTotalCount());
        $report->columns = $model->attributeLabels();
        $report->setItems($provider->getModels());
        return $report;
    }

    public function setParams(array $params)
    {
        $this->getParams = $params;
    }

    public static function create()
    {

    }
}