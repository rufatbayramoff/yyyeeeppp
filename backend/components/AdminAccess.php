<?php namespace backend\components;

/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class AdminAccess
{

    /**
     * 
     * @param string $accessKey
     * @return boolean
     */
    public static function can($accessKey)
    { 
        /** @var $user \common\models\UserAdmin $user * */
        $user = app('user')->getIdentity();
        
        if(!$user){
            return false;
        }
        if ($user->username == 'admin') { // only admin has full access
            return true;
        }
        
        $accesses = $user->userAdminAccesses;
        foreach ($accesses as $ac) {
            if ($accessKey == $ac->access && !empty($ac->is_active)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @param string $accessKey
     * @return boolean
     * @throws \yii\base\UserException
     */
    public static function validateAccess($accessKey)
    {
        if(self::can($accessKey)){
            return true;
        }
        throw new NoAccessException(sprintf("You don't have access to perform this action! [%s]", $accessKey));
    }
    
    /**
     * 
     * @return array
     */
    public static function getAccessData()
    {
        $accessList = require_once (\Yii::getAlias('@backend/config/access.php'));
        // @TODo can create config/access - with group.php file name, and combine all configs in one array
        return $accessList;
    } 
}
