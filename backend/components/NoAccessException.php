<?php namespace backend\components;

/** 
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class NoAccessException extends \yii\base\UserException
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'No Access';
    }
}
