<?php namespace backend\components;

use common\components\BaseActiveQuery;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\db\ActiveRecord;

/**
 * CrudController - give basic support for crud operations
 * create, update, delete
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class CrudController extends AdminController
{
    /**
     * events
     */
    const EVENT_BEFORE_UPDATE  = 'admincrud_beforeUpdate';
    const EVENT_BEFORE_CREATE = 'admincrud_beforeCreate';
    const EVENT_BEFORE_DELETE = 'admincrud_beforeDelete';
    const EVENT_BEFORE_MODEL_LOAD = 'admincrud_beforeModelLoad';
    const EVENT_AFTER_SAVE = 'admincrud_afterModelSave';
    const EVENT_AFTER_DELETE = 'admincrud_afterModelDelete';

    protected $accessGroup;

    protected $redirectAfterCreate = ['index'];
    public function init()
    {
        BaseActiveQuery::$staticCacheGlobalEnable = false;
        $this->detectLanguage = 0;
        $this->enableSeo = false;

        parent::init();
        if(empty($this->accessGroup)){
            $shortName = (new \ReflectionClass($this))->getShortName();
            $this->accessGroup = strtolower(str_replace("Controller", "", $shortName));
        }
        
    }
    /**
     * list records
     * 
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = $this->searchModel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        AdminAccess::validateAccess($this->accessGroup . '.view');
            
        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Action used for combo list - ajax requests
     * 
     * 
     * @param string $q - query to search
     * @param string $id
     * @param string $k - format keyCol,valueCol; for example _id,username_
     * @return array
     */
    public function actionList($q = null, $id = null, $k = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $kCol = 'id';
        $vCol = 'name';
        $columns = [];
        if (!empty($k)) {
            list($kCol, $vCol) = explode(",", $k);
            $columns = [$kCol, $vCol];
        }

        $tableName = $this->searchModel->tableName();
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query;
            $search = $vCol . ' LIKE "' . $q . '%"';
            if (is_numeric($q)) {
                $search = $kCol . "=" . intval($q);
            }
            $query->select(implode(",", $columns) . " AS text")
                ->from($tableName)
                ->where($search)
                ->limit(30);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

    /**
     * Displays a single row.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.view');
        
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * trigger events
     * 
     * @return mixed
     */
    public function actionCreate()
    {
        $model = $this->mainModel;
        $post = Yii::$app->request->post();
        AdminAccess::validateAccess($this->accessGroup . '.add');
        $formConfig = ['isAjax' => $this->isAjax];
        $this->trigger(
            self::EVENT_BEFORE_MODEL_LOAD, new Event(['sender' => $model, 'data' => ['postData'=>$post]])
        );
        if ($model->load($post)) {
            $this->trigger(
                self::EVENT_BEFORE_CREATE, new Event(['sender' => $model])
            );
            if ($this->isAjax) {
                $formConfig['enableAjaxValidation'] = true;
                if ($model->save()) {
                    $this->trigger(
                        self::EVENT_AFTER_SAVE, new Event(['sender' => $model])
                    );
                    return $this->jsonSuccess(
                            ['message' => 'Successfully created']
                    );
                } else {
                    if (count($model->getErrors()) > 0) {
                        $msg = \yii\helpers\Html::errorSummary($model);
                        return $this->jsonError($msg);
                    }
                }
            } else {
                if ($model->save()) {
                    $this->trigger(
                        self::EVENT_AFTER_SAVE, new Event(['sender' => $model])
                    );
                    Yii::$app->getSession()->setFlash('success', 'Successfully created');
                    $this->redirectAfterCreate = str_replace('%id%', $model->getPrimaryKey(), $this->redirectAfterCreate);
                    return $this->redirect($this->redirectAfterCreate);
                }else{
                    $msg = \yii\helpers\Html::errorSummary($model);
                    Yii::$app->getSession()->setFlash('error', $msg);
                }
            }
        } 
        app('cache')->set('back.notify.data', '');
        if ($this->isAjax) {
            return $this->renderPartial('create', [
                    'model' => $model, 'formConfig' => $formConfig
            ]);
        }
        return $this->render('create', [
                'model' => $model, 'formConfig' => $formConfig
        ]);
    }

    /**
     * Forming model load data
     *
     * @param $model
     * @return mixed
     */
    public function formModelLoadData($model)
    {
        return Yii::$app->request->post();
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.update');
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        app('cache')->set('back.notify.data', '');
        $this->trigger(
            self::EVENT_BEFORE_MODEL_LOAD, new Event(['sender' => ['model' => $model, 'postData'=>$post]])
        );
        $postData = $this->formModelLoadData($model);
        if ($model->load($postData)) {

            $this->trigger(
                self::EVENT_BEFORE_UPDATE, new Event(['sender' => $model])
            );
            if ($model->save()) {
                $this->trigger(
                    self::EVENT_AFTER_SAVE, new Event(['sender' => $model])
                );
                BaseActiveQuery::resetCacheDependency();
                return $this->redirect(['index']);
            }
        }
        return $this->render('update', [
                'model' => $model,
        ]);
    }

    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NoAccessException
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        app('cache')->set('back.notify.data', '');
        $this->trigger(
            self::EVENT_BEFORE_DELETE, new Event(['sender' => $this, 'data' => ['id' => $id]])
        );
        AdminAccess::validateAccess($this->accessGroup . '.delete');
        // we cannot delete!
        $model = $this->findModel($id);
        $model->delete();
        $this->trigger(
            self::EVENT_AFTER_DELETE, new Event(['sender' => $model])
        );
        return $this->redirect(['index']);
    }
    
    

    /**
     * Finds the model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ActiveRecord loaded model
     * @throws \yii\web\NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = $this->mainModel;
        if (($model = $model::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * if is_active column exists, updates it's value
     * 
     * @param mixed $id
     * @return boolean
     * @throws \Exception
     */
    public function actionChangeActive($id)
    {
        AdminAccess::validateAccess($this->accessGroup . '.update');
        app('cache')->set('back.notify.data', '');
        $hasActiveColumn = $this->mainModel->hasAttribute('is_active');
        if (!$hasActiveColumn) {
            throw new \Exception("is_active column not defined for this model");
        }
        $model = $this->findModel($id);
        $model->is_active = $model->is_active == 1 ? 0 : 1;
        
        if ($model->validate() && $model->save()) {
            $result = true;
        }else{
            return \yii\helpers\Html::errorSummary($model);
        }

        \Yii::$app->session->setFlash('message', $result ? 'Done' : 'Error');
        return true;
    }
}
