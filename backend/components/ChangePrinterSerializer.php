<?php
/**
 * Created by mitaichik
 */

namespace backend\components;


use common\components\ArrayHelper;
use common\components\serizaliators\AbstractProperties;
use common\models\DeliveryType;
use common\models\PsPrinter;
use common\models\Ps;
use common\models\PsMachineDelivery;
use common\models\StoreOrder;
use common\models\UserAddress;

class ChangePrinterSerializer extends AbstractProperties
{
    /**
     * @var StoreOrder
     */
    public $order;

    /**
     * ChangePrinterSerializer constructor.
     *
     * @param StoreOrder $order
     */
    public function __construct(StoreOrder $order)
    {
        $this->order = $order;
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return [

            PsPrinter::class => [
                'id',
                'title',
                'isCertificated' => function (PsPrinter $model) {
                    return $model->isCertificated();
                },
                'ps',

                'location' => function (PsPrinter $model) {
                    return UserAddress::formatPrinterAddress($model, '%city%, %region%, %country%');
                },

                'deliveryDetails' => function (PsPrinter $printer) {
                    $psDelivery = $printer->companyService->asDeliveryParams()->getPsDeliveryTypeByCode($this->order->deliveryType->code);
                    $data = array_filter($psDelivery->getAttributes(PsMachineDelivery::DELIVERY_DETAILS));
                    if ($data['carrier'] === DeliveryType::CARRIER_TS) { // Don`t show price, if delivery by treatstock
                        unset($data['carrier_price']);
                    }

                    $data['title'] = $this->order->deliveryType->title;
                    $data['packing_price'] = (float)$psDelivery->packing_price;
                    return $data;
                },

                'layerResolution' => function (PsPrinter $printer) {
                    $res = ArrayHelper::map($printer->getProperties([
                        'code' => [PsPrinter::PROPERTY_LAYER_RESOLUTION_LOW, PsPrinter::PROPERTY_LAYER_RESOLUTION_HIGH]
                    ]), 'code', 'value');
                    return sprintf("High: %s <br/>Low: %s", $res[PsPrinter::PROPERTY_LAYER_RESOLUTION_LOW] ?? '-',
                        $res[PsPrinter::PROPERTY_LAYER_RESOLUTION_HIGH] ?? '-');
                }
            ],

            Ps::class => [
                'id',
                'title',
                'email' => 'user.email',
                'phone' => function (Ps $ps) {
                    return "+" . $ps->phone_code . $ps->phone;
                }
            ]


        ];
    }
}