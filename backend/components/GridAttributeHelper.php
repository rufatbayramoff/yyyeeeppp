<?php
/**
 * Date: 15.06.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace backend\components;

use common\models\Ps;
use common\models\CompanyService;
use common\models\StoreOrder;
use common\models\StoreUnit;
use common\models\User;
use yii\helpers\Html;

/**
 * Class GridAttributeHelper
 * grid links to common objects (ps, printer, order, storeUnit)
 *
 * @package backend\components
 */
class GridAttributeHelper
{
    /**
     * @param Ps $ps
     * @return string
     */
    public static function getPsLink(Ps $ps)
    {
        return sprintf('%s [%s]', H($ps->title), Html::a($ps->id, ['ps/ps/view', 'id'=>$ps->id], ['target'=>'_blank']));
    }

    /**
     * @param CompanyService $psMachine
     * @return string
     */
    public static function getPrinterLink(CompanyService $psMachine)
    {
        return sprintf('%s [%s]', H($psMachine->getTitleLabel()), Html::a($psMachine->ps_printer_id, ['ps/ps-printer/view', 'id' => $psMachine->ps_printer_id], ['target' =>'_blank']));
    }

    /**
     * @param CompanyService $psMachine
     * @return string
     */
    public static function getMachineLink(CompanyService $psMachine)
    {
        return sprintf('%s [%s]', H($psMachine->getTitleLabel()), Html::a($psMachine->id, ['ps/ps-machine/view', 'id' => $psMachine->id], ['target' =>'_blank']));
    }

    /**
     * get user link
     * 
     * @param User $user
     * @return string
     */
    public static function getUserLink(User $user)
    {
        return sprintf('%s [%s]', H($user->username), Html::a($user->id, ['user/user/view', 'id'=>$user->id], ['target'=>'_blank']));
    }

    /**
     * @param StoreUnit $storeUnit
     * @return int
     */
    public static function getStoreUnitLink(StoreUnit $storeUnit)
    {
        return $storeUnit->id;
    }

    /**
     * @param StoreOrder $order
     * @return string
     */
    public static function getOrderLink(StoreOrder $order)
    {
        return Html::a($order->id, ['/store/store-order/view', 'id' => $order->id], ['target' => '_blank']);
    }

    public static function getExportImportCaption($dataProvider, $searchModel)
    {
        return sprintf('<div class="row" style="margin:0;"><div class="col-lg-1">%s</div><div class="col-lg-4">%s</div></div>',
            new \backend\components\GridViewDataExporter($searchModel) ,
            \backend\widgets\ExcelImportWidget::widget(['tableName'=> $searchModel->tableName()]));
    }

}