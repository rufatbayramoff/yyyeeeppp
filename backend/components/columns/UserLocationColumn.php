<?php
/**
 * User: nabi
 */

namespace backend\components\columns;


use common\models\UserLocation;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Inflector;

class UserLocationColumn extends DataColumn
{
    public $format = 'raw';
    /**
     * @inheritdoc
     */
    protected function renderFilterCellContent()
    {
        if ($this->filter !== null) {
            return parent::renderFilterCellContent();
        }
        $filterModel = $this->grid->filterModel;
        $filterAttribute = lcfirst(Inflector::id2camel($this->attribute, '.'));
        return Html::activeTextInput($filterModel, $filterAttribute, $this->filterInputOptions);
    }

    /**
     * @inheritdoc
     */
    public function getDataCellValue($model, $key, $index)
    {
        return $model->{$this->attribute} ? UserLocation::formatLocation($model->{$this->attribute}, '%city%, %region%, %country%') : '-';
    }
}