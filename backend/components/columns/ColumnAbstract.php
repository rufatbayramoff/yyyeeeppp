<?php

namespace backend\components\columns;


use common\components\ArrayHelper;
use common\models\User;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Inflector;

class ColumnAbstract extends DataColumn
{
    public $format = 'raw';
    protected static $menuId = 1;
    protected static $cachedMenus = [];

    public static function drawMenu($menuLiItems, $asSub = false)
    {
        $menuLi = [];
        if ($asSub) {
            $menuLi[] = $asSub . ' <ul  class="dropdown-menu"  aria-labelledby="dropdownMenu_' . self::$menuId . '">';
        }
        foreach ($menuLiItems as $item) {
            if (empty($item['link'])) {
                $element = sprintf('<li class="divider"></li>');
            } else {
                if ($item['link'] === '#') {
                    $element = sprintf('<li><span style="padding: 14px;font-size:12px;"><nobr>%s</nobr></span></li>', $item['title']);
                } else {
                    $element = sprintf('<li><a href="%s" target="_blank">%s</a></li>', $item['link'], $item['title']);
                }
            }
            if (!empty($item['submenu'])) {
                $element = sprintf(
                    '<li class="dropdown-submenu">
                        <a href="%s" data-toggle="dropdown" class="dropdownMenuUser_%s" >%s</a>',
                    $item['link'],
                    self::$menuId,
                    $item['title']
                );
                $menuLi[] = implode("", self::drawMenu($item['submenu'], $element));
                continue;
            }

            $menuLi[] = $element;
        }
        if ($asSub) {
            $menuLi[] = '</ul></li>';
        }
        return $menuLi;
    }

    /**
     * @inheritdoc
     */
    protected function renderFilterCellContent()
    {
        if ($this->filter !== null) {
            return parent::renderFilterCellContent();
        }
        $filterModel = $this->grid->filterModel;
        $filterAttribute = lcfirst(Inflector::id2camel($this->attribute, '.'));
        return Html::activeTextInput($filterModel, $filterAttribute, $this->filterInputOptions);
    }

    /**
     * @inheritdoc
     */
    public function getDataCellValue($model, $key, $index)
    {
        $user = ArrayHelper::getValue($model, $this->attribute);
        if (!$user) {
            return null;
        }
        return self::getMenu($user);
    }


    /**
     * get menu for ps
     *
     * @param $user
     * @return null|string
     */
    public static function getMenu($user)
    {
        if (!empty(self::$cachedMenus[$user->id])) {
            return self::$cachedMenus[$user->id];
        }
        /**
         * @var
         */
        self::$menuId = $user->id;
        $menuLiItems = self::getMenuItems();

        $menuLi = self::drawMenu($menuLiItems);
        $menu = $user ? sprintf(
            "<div class=\"btn-group\" style='border:1px #ddd;'><a class='btn btn-xs' href='/ps/ps/view?id=%d' title='%s - %d'>%s  </a> 
            <button class='btn btn-xs btn-default'  dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"caret\"></span></button>
            <ul  class=\"dropdown-menu  multi-level\" >%s</ul>
            </div></div>",
            $user->id,
            \H($user->username),
            $user->id,
            \H($user->username),
            implode('', $menuLi)
        ) : null;

        self::$cachedMenus[$user->id] = $menu;
        return self::$cachedMenus[$user->id];
    }
}