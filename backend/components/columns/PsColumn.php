<?php
/**
 * User: nabi
 */

namespace backend\components\columns;

use common\components\ArrayHelper;
use common\models\Ps;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

class PsColumn extends DataColumn
{
    public $format = 'raw';
    private static $cachedMenus = [];

    /**
     * @inheritdoc
     */
    public function getDataCellValue($model, $key, $index)
    {
        $ps = !empty($model->ps) ? $model->ps : $model->company;
        if (!$ps) {
            return null;
        }
        return self::getMenu($ps);
    }

    /**
     * get menu for ps
     *
     * @param $ps
     * @return null|string
     */
    public static function getMenu($ps)
    {
        if (!empty(self::$cachedMenus[$ps->id])) {
            return self::$cachedMenus[$ps->id];
        }
        $user = $ps->user;
        /**
         * @var $ps Ps
         */
        $menuLiItems = [
            [
                'link' => '#',
                'title' => '<b>COMPANY MENU</b>',
            ],
            ['link' => '/ps/ps/view?id=' . $ps->id, 'title' => '' . \H($ps->title) . ' [' . $ps->id.']'],
            ['link' => '/store/store-order?StoreOrderSearch[ps_user]=' . $ps->user_id, 'title' => 'Company Orders'],
            ['link' => '/company/company-service?CompanyServiceSearch[ps_id]=' . $ps->id, 'title' => 'Company Services'],
            ['link' => '/ps/ps-printer?PsPrinterSearch[ps_id]=' . $ps->id, 'title' => 'Company Printers'],
            ['link' => '/product/product?productSearch[user_id]=' . $ps->user_id, 'title' => 'Company Products'],
            ['link' => '/company/company-block?CompanyBlockSearch[company_id]=' . $ps->id, 'title' => 'Company blocks'],
            [
                'link'    => '/user/user/view?id=' . $user->id,
                'title'   => 'User: ' . $user->getFullNameOrUsername(),
                'submenu' => UserColumn::getUserMenuItems($user)
            ],
        ];
        $menuLi = ColumnAbstract::drawMenu($menuLiItems);
        $menu = sprintf(
            "<div class=\"btn-group\"><a class='btn btn-xs' href='/ps/ps/view?id=%d' title='%s - %d'>%s  </a> 
            <button class='btn btn-xs btn-default'  dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"caret\"></span></button>
            <ul  class=\"dropdown-menu\">%s</ul>
            </div></div>",
            $ps->id,
            \H($ps->title),
            $ps->id,
            \H(StringHelper::truncateWords($ps->title, 3)),
            implode('', $menuLi)
        );

        self::$cachedMenus[$ps->id] = $menu;
        return self::$cachedMenus[$ps->id];
    }
}