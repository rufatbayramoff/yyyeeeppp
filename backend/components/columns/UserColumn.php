<?php
/**
 * Created by mitaichik
 */

namespace backend\components\columns;


use common\components\ArrayHelper;
use common\models\User;
use frontend\components\UserUtils;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Inflector;

/**
 * Class UserColumn
 * @package backend\components\columns
 */
class UserColumn extends DataColumn
{
    public $format = 'raw';

    /**
     * Relation name for retrive user object.
     * If setted - user object will retrive from this relation, otherwise - from attribute property.
     *
     * @var string|null
     */
    public $relation;

    private static $cachedMenus = [];

    /**
     * @param User $user
     * @return array
     */
    public static function getUserMenuItems(User $user): array
    {

        $link =  UserUtils::getUserPublicProfileUrl($user);

        $menuLiItems = [
            [
                'link' => '#',
                'title' => '<b>USER MENU</b>',
            ],
            [
                'link'  => '/user/user/view?id=' . $user->id,
                'title' => $user->getFullNameOrUsername() . ' [' . $user->id . ']'
            ],
            [
                'link' => $link,
                'title' => 'Public profile',
            ],
            ['link' => '/user/user-tax-info?UserTaxInfoSearch[user_id]=' . $user->id, 'title' => 'Tax information'],
            ['link' => '/store/store-order?StoreOrderSearch[user_id]=' . $user->id, 'title' => 'User Orders'],
            ['link' => '/store/store-order?StoreOrderSearch[modelAuthor]=' . $user->id, 'title' => 'Model Orders'],
            [
                'link' => '', 'title' => 'User company',
                'submenu' => [
                    ['link' => '/store/store-order?StoreOrderSearch[ps_user]=' . $user->id, 'title' => 'Company Orders'],
                    ['link' => '/company/company-service?CompanyServiceSearch[userId]=' . $user->id, 'title' => 'Company Services'],
                    ['link' => '/ps/ps-printer?PsPrinterSearch[userId]=' . $user->id, 'title' => 'Company Printers'],
                    ['link' => '/product/product?productSearch[user_id]=' . $user->id, 'title' => 'Company Products']
                ]

            ],
            [
                'link'  => '#',
                'title' => 'Email: ' . Html::mailto($user->email, $user->email)
            ],
            [
                'link'  => '#',
                'title' => 'Last login: ' . date("m/d/Y H:i", $user->lastlogin_at)
            ]

        ];
        return $menuLiItems;
    }

    /**
     * @inheritdoc
     */
    protected function renderFilterCellContent()
    {
        if ($this->filter !== null) {
            return parent::renderFilterCellContent();
        }
        $filterModel = $this->grid->filterModel;
        $filterAttribute = lcfirst(Inflector::id2camel($this->attribute, '.'));
        return Html::activeTextInput($filterModel, $filterAttribute, $this->filterInputOptions);
    }

    /**
     * @inheritdoc
     */
    public function getDataCellValue($model, $key, $index)
    {
        $user = $this->retriveUser($model);
        if (!$user) {
            return null;
        }
        return self::getMenu($user);
    }

    /**
     * get menu for ps
     *
     * @param $user
     * @return null|string
     */
    public static function getMenu(User $user)
    {
        if (!empty(self::$cachedMenus[$user->id])) {
            return self::$cachedMenus[$user->id];
        }

        $menuLiItems = self::getUserMenuItems($user);

        $menuLi = ColumnAbstract::drawMenu($menuLiItems);
        $menu = $user ? sprintf(
            "<div class=\"btn-group\" style='border:1px #ddd;'><a class='btn btn-xs' href='/user/user/view?id=%d' title='%s - %d'>%s  </a> 
            <button class='btn btn-xs btn-default'  dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"caret\"></span></button>
            <ul  class=\"dropdown-menu  multi-level\" >%s</ul>
            </div>",
            $user->id,
            \H($user->username),
            $user->id,
            \H($user->username),
            implode('', $menuLi)
        ) : null;

        self::$cachedMenus[$user->id] = $menu;
        return self::$cachedMenus[$user->id];
    }

    /**
     * @param $model
     * @return User|null
     */
    private function retriveUser($model) : ?User
    {
        $relationName = $this->relation ?? $this->attribute;
        return ArrayHelper::getValue($model, $relationName);
    }
}