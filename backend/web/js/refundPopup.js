var refundPopupClass = {
    config : {
        'uid': '',
        'moneyMax': ''
    },
    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
        refundPopup.updateMax($('#paymenttransactionrefund-refund_type').val());

    },
    updateMax: function (refundType) {
        var self = this;
        $('#paymenttransactionrefund-amount').val(self.config.moneyMax[refundType].price);
    }
}


