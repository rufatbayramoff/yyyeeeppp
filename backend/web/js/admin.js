/**
 * 
 * Admin core js functions and event handlers
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
$(document).ready(function () {
    initAjaxClicks();    
    initAjaxSubmits();
    initNotifyUpdates();
    
    jQuery('.jspopup').click(function(a,b,c){
        var link = a.currentTarget.href;
       var newWin = window.open(link, " ", "width=880,height=740,resizable=yes,scrollbars=yes,status=yes");
         newWin.focus();

        return false;
    });
}); 

// re-init after pjax refresh
$(document).on('ready pjax:success', function(){
    initAjaxClicks();    
});

/**
 * generate html markup for modal window if not exists
 * 
 * @param {String} target
 * @param {String} header
 * @returns {String}
 */
function generateModal(target, header){
    header = header || 'Add';
    target = target.replace('#', '');
    var html = '<div id="'+target+'" class="fade modal" role="dialog">\n\
        <div class="modal-dialog "><div class="modal-content">\n\
            <div class="modal-header">\n\
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n\
                <h4>' + header + '</h4></div>\n\
                <div class="modal-body"></div></div></div>\n\
        </div>';
    return html;
}



var btnAjaxModalClickListenerBinded  = [];

function initAjaxClicks()
{
    // ajax modals
    $('.btn-ajax-modal').each(function(index, elem)
    {
        if(btnAjaxModalClickListenerBinded.indexOf(elem) === -1)
        {
            btnAjaxModalClickListenerBinded.push(elem);
            $(elem).click(function()
            {
                var elm = $(this),
                    target = elm.attr('data-target'),
                    title = elm.attr('title'),
                    ajaxBody = elm.attr('value');

                var $target = $(target);

                if($target.length===0)
                {
                    //$(generateModal(target, title)).insertAfter('.btn-ajax-modal');
                    $(generateModal(target, title)).appendTo('body');
                }
                else
                {
                    $target.find('.modal-body').html('');
                }

                $(target).modal('show').find('.modal-body').load(ajaxBody);
                return false;
            });
        }
    });
    
    // grid ajax click actions
    $('body').on('click', '.grid-action', function (e) {
        var href = $(this).attr('href');
        var self = this;
        $.get(href, function () {
            var pjId = $(self).closest('.pjax-wraper').attr('id');
            $.pjax.reload('#' + pjId);
        });
        return false;
    });

}
/**
 * ajax form submit
 * 
 * @returns {undefined}
 */
function initAjaxSubmits()
{
    $('body').on('click', '.ajax-submit', function (e) {
        e.preventDefault();
        var me = this;
        me.disabled = true;
        var $form = $(this).parents('form').first();
        var $modal = $form.parents('.modal').first();
        var isModalMode = $modal.length>0;
        
        var modalId = ''; //
        if(isModalMode){
            modalId = '#' + ($modal.attr('id'));
        }else{            
        }
        $.ajax({
            url: $form.attr('action'),
            type: "POST",
            data: $form.serialize(),
            dataType: 'json', 
            success: function (result) {
                var modalContainer = modalBody = $(me).parent();
                if(isModalMode){
                    modalContainer = $(modalId);
                    modalBody = modalContainer.find('.modal-body'); 
                }
                var msgContainer = modalContainer.find('.form-message');
                if(msgContainer.length===0){
                    modalBody.prepend('<div class="form-message"></div>');
                    msgContainer = modalContainer.find('.form-message');
                }
                if (result.message === 'reload') {
                    window.location.reload();
                    return;
                }else if(result.redir){
                    document.location = result.redir;
                }
                if (result.success === true) {
                    var timeout = 1500;
                    if (result.warning) {
                        msgContainer.removeClass('alert-success');
                        msgContainer.addClass('alert-warning');
                        msgContainer.html(result.warning);
                        timeout = 3000;
                    } else {
                        msgContainer.removeClass('alert-warning');
                        msgContainer.addClass('alert-success');
                        msgContainer.html(result.message || 'Done');
                    }
                    $form[0].reset();
                   
                    setTimeout(function(){
                        $(modalId).modal('hide');
                        
                        if($('.refresh-pjax').length!==0){
                            $('.refresh-pjax').trigger('click');
                        } 
                        $('.modal-backdrop').remove();
                        $('body').removeClass('modal-open');
                    }, timeout);
                }
                else {
                    msgContainer.html(result.message || 'Done');                    
                    msgContainer.fadeOut().fadeIn();
                    me.disabled = false;
                }
                if (result.reload) {
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                }
            },
            error : function(result){
                var modalContainer = modalBody = $(me).parent().parent();
                var msgContainer = modalContainer.find('.form-message');
                if(msgContainer.length===0){
                    modalBody.prepend('<div class="form-message"></div>');
                    msgContainer = modalContainer.find('.form-message');
                }
                me.disabled = false;
                result = result.responseJSON || {};
                msgContainer.html(result.message || 'Done');
                msgContainer.fadeOut().fadeIn();
                if (result.reload) {
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                }
            }
        });
        return false;
    });
}

function initNotifyUpdates()
{
    // js-top-notify-count
    // js-top-notify-list
    var lastResponse = null;
    jQuery.ajax({
        type: "GET",
        url: "/site/notify/get-notifies",
        dataType: "html",
        success: function (response) {
            lastResponse =  response;
            $('#js-top-notify-list').html(response);
        }
    });
    setInterval(function () {
        jQuery.ajax({
            type: "GET",
            url: "/site/notify/get-notifies",
            dataType: "html",
            success: function (response) {
                if(lastResponse!=response){
                    console.log('ring', lastResponse, response);
                    document.getElementById('notify_play_sound').play();
                }
                lastResponse =  response;
                $('#js-top-notify-list').html(response);
            }
        });
    }, 25000);
}