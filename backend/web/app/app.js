"use strict";

var app = angular.module('app', [])

.config(['$locationProvider', '$provide', function($locationProvider, $provide)
{
    $locationProvider
        .html5Mode(false);

    $provide.decorator('$browser', ['$delegate', function ($delegate)
    {
        $delegate.onUrlChange = function () {};
        $delegate.url = function () { return ""};
        return $delegate;
    }]);
}])


/**
 *
 */
.config(['$httpProvider', function($httpProvider)
{
    // configure yii csrf token
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = function() {
        return yii.getCsrfToken();
    };
}])

/**
 * Handler for show http errors
 */
.factory('$notifyHttpErrors', ['$notify', '$q', function($notify, $q)
{
    return function(data)
    {
        $notify.error(data.data.errors || data.data.message || (_.isString(data.data) && data.data) || 'Error. Please, try again.');
        return $q.reject(data);
    }
}])


/**
 * Handler for show notifiers
 */
.factory('$notifyHttp', ['$notify', '$q', function ($notify, $q) {
      return function (data) {
          $notify.success(data.data.message || (_.isString(data.data) && data.data) || 'Successfully saved.');
          return $q.reject(data);
      }
}])


.directive('checkListObject', function() {
    return {
        scope: {
            list: '=checkListObject',
            value: '='
        },
        link: function(scope, elem) {
            var handler = function(setup) {
                var checked = elem.prop('checked');
                var index = scope.list.indexOf(scope.value);

                if (checked && index == -1) {
                    if (setup) elem.prop('checked', false);
                    else scope.list.push(scope.value);
                } else if (!checked && index != -1) {
                    if (setup) elem.prop('checked', true);
                    else scope.list.splice(index, 1);
                }
            };

            var setupHandler = handler.bind(null, true);
            var changeHandler = handler.bind(null, false);

            elem.bind('change', function() {
                scope.$apply(changeHandler);
            });
            scope.$watch('list', setupHandler, true);
        }
    };
})
/**
 * Directive for loader button.
 * It disabled button while not resolved returned from click promise.
 *
 */
.directive('loaderClick', ['$parse', '$q', '$timeout', function($parse, $q, $timeout)
{
    var inLoadText = 'Please wait...';

    return {
        restrict: 'A',
        compile: function($element, attr)
        {
            var fn = $parse(attr['loaderClick'], null, true);
            return function ngEventHandler(scope, element)
            {
                element.on('click', function(event)
                {
                    var promise = scope.$apply(function() {
                        return fn(scope, {$event:event});
                    });

                    // if it not promise - do nothing
                    if (!promise || !angular.isFunction(promise.then)){
                        return;
                    }

                    var content = element.html();
                    element.prop('disabled', true);
                    element.html(inLoadText);

                    var restoreFn = function () {

                        var disabled = false;

                        if(attr['ngDisabled']) {
                            disabled = !! $parse(attr['ngDisabled'])(scope);
                        }

                        $timeout(function () {
                            element.prop('disabled', disabled);
                            element.html(content);
                        });
                    };

                    promise.then(function ()
                    {
                        var needRestore = !$parse((attr['loaderClickUnrestored']))(scope);
                        if (needRestore){
                            restoreFn();
                        }
                    })
                        .catch(restoreFn);
                });
            };
        }
    };
}])