"use strict";

app.controller('PsCncMachineController', function ($scope, $modal, $http, $q, $routerBackend, $notifyHttpErrors, controllerParams) {

    var element = document.getElementById("psCncJsonContainer");

    var editor = new JSONEditor(element, {
        startval: controllerParams.psCncJson,
        theme: 'bootstrap3',
        iconlib: 'bootstrap3',
        disable_collapse: true,
        disable_edit_json: true,
        disable_properties: true,
        disable_array_delete_all_rows: true,
        disable_array_delete_last_row: true,
        no_additional_properties: true,
        schema: controllerParams.cncSchema,
        template: 'jsSupport'
    });

    $scope.saveCnc = function () {
       return $http.post('/ps/ps-machine/save-cnc-schema?psMachineId=' + controllerParams.psMachineId, {
            psCnc: editor.getValue()
        })
            .then(function () {
                $routerBackend.to('view?id=' + controllerParams.psMachineId);
            })
            .catch(function (data) {
                $notifyHttpErrors(data);
                return $q.reject(data);
            });
    }

});

