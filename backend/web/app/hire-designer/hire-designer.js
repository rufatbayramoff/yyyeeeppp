"use strict";

const MAX_MODELS_COUNT_PER_DESIGNER = 3;

app.controller('HireDesignerListController', function($scope, $modal, $http)
{
    var items = $scope.items = [];

    $http.get('/site/hire-designer/load').then(function (response)
    {
        items = $scope.items = response.data.items;
    });


    /**
     *
     */
    $scope.openAddDesignerModal = function ()
    {
        var addedDesignersIds = [];
        angular.forEach(items, function (item) {
            addedDesignersIds.push(item.designer.id);
        });

        $modal.open({
            template: '/app/hire-designer/select-designer-modal.html',
            scope: {
                addedDesignersIds : addedDesignersIds,
                addDesigner : function (designer) {
                    $scope.items.push({
                        designer : designer,
                        models : []
                    });
                }
            }
        });
    };


    /**
     *
     * @param item
     */
    $scope.openAddModelModal = function (item)
    {
        $modal.open({
            template: '/app/hire-designer/select-model-modal.html',
            scope: {
                item : item
            }
        });
    };

    /**
     *
     * @param item
     */
    $scope.moveUp = function (item)
    {
        if (!$scope.canMoveUp(item)){
            throw new Error('Cant move up');
        }

        var currentIndex = items.indexOf(item);
        items[currentIndex] = items[currentIndex - 1];
        items[currentIndex - 1] = item;
    };

    /**
     *
     * @param item
     * @returns {boolean}
     */
    $scope.canMoveUp = function (item)
    {

        return items.indexOf(item) > 0;
    };

    /**
     *
     * @param item
     */
    $scope.moveDown = function (item)
    {
        if (!$scope.canMoveDown(item)){
            throw new Error('Cant move up');
        }

        var currentIndex = items.indexOf(item);
        items[currentIndex] = items[currentIndex + 1];
        items[currentIndex + 1] = item;
    };

    /**
     *
     * @param item
     * @returns {boolean}
     */
    $scope.canMoveDown = function (item)
    {
        return items.indexOf(item) < items.length - 1;
    };

    /**
     *
     * @param item
     */
    $scope.remove = function (item)
    {
        if (confirm("You realy want delete designer named " + item.designer.username)){
            var index = items.indexOf(item);
            items.splice(index, 1);
        }
    };

    /**
     *
     * @param item
     * @returns {boolean}
     */
    $scope.canAddModel = function (item)
    {
        return item.models.length < MAX_MODELS_COUNT_PER_DESIGNER;
    };

    /**
     *
     * @param item
     * @param model
     */
    $scope.removeModel = function (item, model)
    {
        var index = item.models.indexOf(model);
        item.models.splice(index, 1);
    };

    /**
     *
     */
    $scope.save = function ()
    {
        $http.post('/site/hire-designer/save', {items : items})
            .then(function ()
            {
                alert('Saved');
            })
            .catch(function ()
            {
                alert('Error');
            });
    }
})



.controller('HireDesignerSelectDesignerModalController', function ($scope, $http)
{
    $scope.designerQuery = '';
    $scope.designers = [];

    $scope.$watch('designerQuery', function (newValue)
    {
        if (newValue.length < 2) {
            $scope.designers = [];
            return;
        }



        $http.get('/site/hire-designer/designers', {params : {query : newValue, 'excludedIds[]' : $scope.addedDesignersIds}})
            .then(function (response) {
                $scope.designers = response.data;
            });
    });

    /**
     *
     * @param designer
     */
    $scope.addDesigner = function (designer)
    {
        $scope.$parent.addDesigner(designer);
        $scope.$dismiss();
    }
})

.controller('HireDesignerSelectModelModalController', function ($scope, $http)
{
    /**
     *
     */
    var item = $scope.$parent.item;

    /**
     *
     * @type {Array}
     */
    $scope.selectedModels = [];

    /**
     *
     * @type {boolean}
     */
    $scope.loading = true;


    var addedModelsIds = [];
    angular.forEach(item.models, function (model) {
        addedModelsIds.push(model.id);
    });

    $http.get('/site/hire-designer/models', {params : {designerId : item.designer.id, 'excludedIds[]' : addedModelsIds}})
        .then(function (response) {
            $scope.models = response.data;
            $scope.loading = false;
        });

    /**
     *
     * @param model
     * @returns {boolean}
     */
    $scope.isSelected = function(model)
    {
        return $scope.selectedModels.indexOf(model) !== -1;
    };

    /**
     *
     * @returns {boolean}
     */
    $scope.canSelectMore = function ()
    {
        return item.models.length + $scope.selectedModels.length < MAX_MODELS_COUNT_PER_DESIGNER;
    };

    /**
     *
     */
    $scope.addSelectedModels = function ()
    {
        item.models.push.apply(item.models, $scope.selectedModels);
        $scope.$dismiss();
    };
});