"use strict";

app.factory('$modal', function($templateRequest, $compile, $rootScope, $controller)
{
    /**
     * Default config for modal
     * @type {{scope: undefined, template: undefined}}
     */
    var defaultModalConfig = {
        scope : undefined,
        template : undefined,
        controller : undefined,
        onShown : angular.noop
    };


    /**
     * Prepare config : merge with default config and resolve them
     * @param config
     */
    var prepareConfig = function (config)
    {
        if(!config.template){
            throw new Error('Bad modal configuration');
        }

        if(!config.scope){
            config.scope = $rootScope.$new(true);
        }

        if(config.scope.constructor != $rootScope.constructor){
            config.scope = angular.extend($rootScope.$new(true), config.scope);
        }

        config = angular.extend({}, defaultModalConfig, config);
        return config;
    };


    var $modal = {};

    /**
     * Open modal dialog
     * @param config
     */
    $modal.open = function(config)
    {
        config = prepareConfig(config);
        $templateRequest(config.template).then(function(templateHtml)
        {
            var modal = $compile(templateHtml)(config.scope);

            if(modal.length == 0){
                throw new Error("Undefined modal template " + config.template);
            }

            modal.modal({})
                .on('hidden.bs.modal', function()
                {
                    $(this).remove();
                })
                .on('shown.bs.modal', config.onShown);

            config.scope.$dismiss = function()
            {
                modal.modal('hide');
            };

            if(config.controller){
                $controller(config.controller, {$scope: config.scope});
            }
        });
    };


    return $modal;
});
