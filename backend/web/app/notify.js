"use strict";

/**
 * Notifi service.
 * Now it just wrapper for TS.Notify functions
 */
app.factory('$notify', function()
{
    var $notify = {};

    /**
     * Prepare message to string
     * @param {String|String[]}message
     * @returns {String}
     */
    var prepareMessage = function(message)
    {
        if(_.isString(message)){
            return message;
        }

        if(_.isObject(message) && message.message){
            return message.message;
        }

        if(_.isArray(message) || _.isObject(message)){
            return _.uniq(_.values(message)).join('<br/>')
        }

        throw "Bad type for message";
    };

    /**
     * Show error notify
     * @param {String|String[]} message
     * @param {boolean} [autoClose]
     */
    $notify.error = function(message, autoClose)
    {
        alert(prepareMessage(message));
    };

    /**
     * Show success notify
     * @param {String|String[]} message
     * @param {boolean} [autoClose]
     */
    $notify.success = function(message, autoClose)
    {
        alert(prepareMessage(message));
    };

    /**
     * Show success notify
     * @param {String|String[]} message
     * @param {boolean} [autoClose]
     */
    $notify.info = function(message, autoClose)
    {
        alert(prepareMessage(message));
    };

    /**
     * Show success notify
     * @param {String|String[]} message
     * @param {boolean} [autoClose]
     */
    $notify.warning = function(message, autoClose)
    {
        alert(prepareMessage(message));
    };

    return $notify;
});
