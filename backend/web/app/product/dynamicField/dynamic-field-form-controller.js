"use strict";

app.controller('DynamicFieldFormController', function ($scope, $http, $q, $camelizer, $jsonEditorHelper, $routerBackend, $notifyHttpErrors, controllerParams) {

    $scope.dynamicField = new DynamicField(controllerParams.dynamicField);
    var dynamicFieldSchema = controllerParams.dynamicFieldSchema;
    var editorMaterials;
    var dynamicFieldTitle = $('#dynamicfield-title');

    function initTitle() {
        dynamicFieldTitle.on('focusin', function (event) {
            dynamicFieldTitle.data('prevValue', dynamicFieldTitle.val());
        }).on('input', function (event) {
            var prevValue = dynamicFieldTitle.data('prevValue');
            var value = dynamicFieldTitle.val();
            dynamicFieldTitle.data('prevValue', value);

            if ($('#dynamicfield-code').attr('readonly')) {
                return;
            }

            //console.log('Prev value: ' + prevValue);
            //console.log('Value: ' + value);

            var prevCodeCamelize = $camelizer.camelize(prevValue);
            if (prevCodeCamelize === $('#dynamicfield-code').val() || ($('#dynamicfield-code').val() === '')) {
                $('#dynamicfield-code').val($camelizer.camelize(value));
            }
        });
    }

    function jsonEditorChange() {
        var jsonTypeParams = editorMaterials.getValue();
        document.getElementById('typeParamsJson').value = JSON.stringify(jsonTypeParams);
        //console.log(jsonTypeParams);
    }

    function reinitJsonEditor(schema, startVal) {
        if ((schema.length === 0) || (typeof schema === 'undefined')) {
            editorMaterials = null;
            return;
        }
        var element = document.getElementById("typeParamsJsonContainer");
        editorMaterials = new JSONEditor(element, {
            theme: 'bootstrap3',
            iconlib: 'bootstrap3',
            disable_collapse: true,
            disable_edit_json: true,
            disable_properties: true,
            disable_array_delete_all_rows: true,
            disable_array_delete_last_row: true,
            no_additional_properties: true,
            schema: schema,
            show_errors: 'always'
        });
        var valueEmpty = editorMaterials.getValue();
        if (valueEmpty === null) {
            valueEmpty = {};
        }
        startVal = Object.assign(valueEmpty, startVal);
        editorMaterials.setValue(startVal);
        editorMaterials.on('change', jsonEditorChange);
    }

    function initParams() {

        var type = $scope.dynamicField.type;
        var schema = dynamicFieldSchema[type];
        var startVal = $scope.dynamicField.typeParams;

        window.jQuery.fn.select2 = false;

        reinitJsonEditor(schema, startVal);
    }

    function initForm() {
        initTitle();
        initParams();
    }

    $scope.updateTypeParams = function () {
        editorMaterials.getValue();
    };

    $scope.onChangeType = function () {
        var newType = $scope.dynamicField.type;
        var newSchema = dynamicFieldSchema[newType];
        if (editorMaterials) {
            editorMaterials.destroy();
        }
        reinitJsonEditor(newSchema);
    };

    initForm();
});

