"use strict";


app.controller('StoreOrderController', function ($scope, $modal, $http) {
    /**
     *
     * @type {{id: *}}
     */
    $scope.order = {id: orderId};

    /**
     *
     * @type {boolean}
     */
    $scope.showCanceledAttempts = false;

    $scope.changeCarrier = function () {
        $modal.open({
            template: '/app/store-order/change-carrier.html',
            controller: 'StoreOrderChangeCarrierController',
            scope: {order: $scope.order}
        });

    };

    /**
     *
     */
    $scope.openChangeMaterialAndColorModal = function () {
        $http.get('/store/store-order/change-texture', {params: {orderId: $scope.order.id}})
            .then(function (response) {
                $modal.open({
                    template: '/app/store-order/change-material-and-color-modal.html',
                    controller: 'StoreOrderChangeMaterialAndColorDialog',
                    scope: angular.extend({}, response.data, {order: $scope.order})
                });
            });
    };


    /**
     *
     * @param {bool} [exactMaterialAndColor]
     */
    $scope.openChangeOfferModal = function (exactMaterialAndColor) {
        return $http.get('/store/store-order/change-printer', {
            params: {
                id: $scope.order.id,
                exactMaterialAndColor: exactMaterialAndColor ? 1 : undefined
            }
        })
            .then(function (response) {
                $modal.open({
                    template: '/app/store-order/change-offer-modal.html',
                    controller: 'ChangeOfferController',
                    scope: angular.extend({}, response.data)
                });
            });
    }
})


    /**
     *
     */
    .controller('StoreOrderChangeCarrierController', function ($scope) {

        $scope.getCsrfToken = function () {
            return yii.getCsrfToken();
        }
    })

    .controller('ChangeOfferController', function ($scope) {
        /**
         *
         * @type {boolean}
         */
        $scope.hideCanclled = false;

        /**
         *
         * @returns {*}
         */
        $scope.getFilteredOffers = function () {
            var result = [];

            /** @namespace offer.isCancelled */
            /** @namespace $scope.offers */

            $scope.offers.forEach(function (offer) {
                if ($scope.hideCanclled && offer.isCancelled) {
                    return;
                }
                result.push(offer);
            });

            return result;
        }
    })

    /**
     *
     */
    .directive('printerLink', function () {

        return {
            restict: 'E',
            scope: {
                printer: '='
            },
            link: function (scope, element) {
                element.html(scope.printer.title + ' [<a target="_blank" href="/ps/ps-printer/view?id=' + scope.printer.id + '">' + scope.printer.id + "</a>]");
            }
        }
    })

    /**
     *
     */
    .directive('psLink', function () {

        return {
            restict: 'E',
            scope: {
                ps: '='
            },
            link: function (scope, element) {
                element.html(scope.ps.title + ' [<a target="_blank" href="/ps/ps/view?id=' + scope.ps.id + '">' + scope.ps.id + "</a>]");
            }
        }
    })

    /**
     *
     */
    .filter('html', ['$sce', function ($sce) {
        return function (text) {
            return $sce.trustAsHtml(text);
        };
    }])

    /** @namespace form.forAllFiles */
    /** @namespace $scope.replica */
    /** @namespace form.filesTextures */
    /** @namespace form.allFilesTexture.materialId */
    /** @namespace form.allFilesTexture.colorId */
    .controller('StoreOrderChangeMaterialAndColorDialog', function ($scope, $notify, $routerBackend, $http, $notifyHttpErrorsXxx) {
        /**
         *
         */
        $scope.materialByIdMap = _.indexBy($scope.materials, 'id');

        /**
         *
         * @returns {boolean}
         */
        $scope.save = function () {
            var errors = $scope.validate();

            if (!_.isEmpty(errors)) {
                $notify.error(errors);
                return false;
            }

            $http.post('/store/store-order/change-texture', $scope.form, {params: {orderId: $scope.order.id}})
                .then($routerBackend.reload)
                .catch($notifyHttpErrorsXxx);
        };

        /**
         * Valdate form
         * @returns {Array}
         */
        $scope.validate = function () {

            var errors = [];
            var form = $scope.form;

            if (form.forAllFiles) {
                if (!form.allFilesTexture.materialId || !form.allFilesTexture.colorId) {
                    errors.push("Please fill all fields");
                }
            }

            if (!form.forAllFiles) {

                angular.forEach($scope.replica.parts, function (part) {
                    if (!form.filesTextures[part.id] || !form.filesTextures[part.id].materialId || !form.filesTextures[part.id].colorId) {
                        errors.push("Please fill all fields");
                    }
                });
            }
            return errors;
        }
    })
    /**
     * Handler for show http errors
     */
    .factory('$notifyHttpErrorsXxx', ['$notify', '$q', function ($notify, $q) {
        return function (data) {
            $notify.error(data.data.errors || data.data.message || (_.isString(data.data) && data.data) || 'Error. Please, try again.');
            return $q.reject(data);
        }
    }])
