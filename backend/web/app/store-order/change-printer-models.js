"use strict";

function OffersList() {
    this.list = [];
}

OffersList.prototype.clear = function () {
    this.list = [];
}

OffersList.prototype.load = function (data) {
    for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
        var dataElement = data[key];
        var element = new Offer(dataElement);
        this.list.push(element);
    }
};

OffersList.prototype.findOffer = function (printerId) {
    for (var key in this.list) {
        if (!this.list.hasOwnProperty(key)) continue;
        let offer = this.list[key];
        if (offer.printer.id == printerId) {
            return offer;
        }
    }
    return null;
};

/*
* @property bool isHighlighted
* @property bool isCancelled
* @property string shippingPrice
* @property string producePrice
* @property bool loadingDeliveryPrice
* @param data
* @constructor
*/
function Offer(data) {
    angular.extend(this, data);
}

Offer.prototype.getTotalPrice = function () {
    if (this.shippingPrice === 'TBC') {
        return 'TBC';
    }
    return Math.round((this.producePrice.amount + this.shippingPrice.amount) * 100) / 100;
};

Offer.prototype.getProducePriceAmount = function () {
    return Math.round(this.producePrice.amount * 100) / 100;
};

Offer.prototype.getShippingPriceAmount = function () {

    return Math.round(this.shippingPrice.amount * 100) / 100;
};

Offer.prototype.getTotalPrice = function () {
    if (this.shippingPrice === 'TBC') {
        return 'TBC';
    }
    return Math.round((this.producePrice.amount + this.shippingPrice.amount) * 100) / 100;
};


Offer.prototype.updateTBCDelivery = function (data) {
    this.shippingPrice = Object.assign({},this.shippingPrice,{'amount': data.shippingPrice});
    this.loadingDeliveryPrice = false;
};

Offer.prototype.updateTBCDeliveryError = function (data) {
    this.deliveryPrice = null;
    this.deliveryError = data.data.message;
    this.loadingDeliveryPrice = false;
};

