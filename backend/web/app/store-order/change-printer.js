"use strict";


app.controller('ChangePrinterController', function ($scope, $modal, $http, $q, $routerBackend, $notifyHttpErrors, controllerParams) {
    $scope.hideCancelled = false;
    $scope.exactMaterialColor = true;
    $scope.sameCurrency = true;

    $scope.order = controllerParams['order'];
    $scope.selectesOffers = {};
    $scope.offersList = new OffersList();
    $scope.loading = 1;

    $scope.close = function () {
        window.close();
    };

    $scope.changeFilters = function () {
        setTimeout(function () {
            $scope.updateOffers();
        }, 100);
    };

    $scope.makeOffers = function () {
        let printerIds = [];
        for (var key in $scope.selectesOffers) {
            if ((!$scope.selectesOffers.hasOwnProperty(key)) || (!$scope.selectesOffers[key])) {
                continue;
            }
            let offer = $scope.offersList.findOffer(key);
            if (!offer) {
                continue;
            }
            if (offer.deliveryPrice == 'TBC') {
                alert('Please press on TBC to calculate delivery Price. Printer ' + offer.printer.id);
                return;
            }
            printerIds.push(key);
        }

        if (!printerIds.length) {
            alert('No offers selected');
            return;
        }

        return $http.post('/store/store-order/send-print-requests', {
            orderId: $scope.order.id,
            printerIds: printerIds
        })
            .finally(function () {
                $routerBackend.to('/store/store-order/view?id=' + $scope.order.id);
            })
    };

    $scope.calcDeliveryTBC = function (printerId) {
        let offer = $scope.offersList.findOffer(printerId)
        offer.deliveryError = '';
        offer.loadingDeliveryPrice = true;
        $http.get('/store/store-order/calc-delivery-tbc-ajax', {
            params: {
                orderId: $scope.order.id,
                printerId: printerId
            }
        })
            .then(function (response) {
                offer.updateTBCDelivery(response.data);
            })
            .catch(function (data) {
                offer.updateTBCDeliveryError(data);
                return $q.reject(data);
            });
    };

    $scope.updateOffers = function () {
        $scope.loading = 1;
        $scope.offersList.clear();
        $scope.warnings = [];
        $http.get('/store/store-order/change-printer-ajax', {
            params: {
                orderId: $scope.order.id,
                hideCancelled: $scope.hideCancelled ? 1 : 0,
                exactMaterialAndColor: $scope.exactMaterialColor ? 1 : 0,
                sameCurrency: $scope.sameCurrency ? 1 : 0
            }
        })
            .then(function (response) {
                $scope.offersList.load(response.data.offers);
                $scope.warnings = response.data.warnings;
            })
            .finally(function () {
                $scope.loading = 0;
            })
    };

    $scope.offersSorted = function () {
        let firstPart = [];
        let secondPart = [];
        for (var key in $scope.offersList.list) {
            if (!$scope.offersList.list.hasOwnProperty(key)) continue;
            let offer = $scope.offersList.list[key];
            if ($scope.selectesOffers[offer.printer.id]) {
                firstPart.push(offer);
            } else {
                secondPart.push(offer);
            }
        }
        let result = firstPart.concat(secondPart);
        return result;
    };

    $scope.isSafeOffer = function (offer) {
        let totalPrice = offer.getTotalPrice();
        if ((offer.deliveryPrice !== 'TBC') && (totalPrice < $scope.order.totalPrice)) {
            return true;
        }
        return false;
    };

    $scope.updateOffers();
});
