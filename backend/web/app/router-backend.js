"use strict";

/**
 * Notifi service.
 * Now it just wrapper for TS.Notify functions
 */
app.factory('$routerBackend', function()
{
    var $routerBackend = {};

    /**
     * Set url
     * @param {String} url
     */
    var setUrl = function (url)
    {
        window.location.href = url;
    };

    /**
     * Reload current page
     */
    $routerBackend.reload = function()
    {
        window.location.reload();
    };

    /**
     * Go to url
     */
    $routerBackend.to = function(url)
    {
        setUrl(url);
    };

    /**
     * Retrun current location
     * @returns {string}
     */
    $routerBackend.getCurrentLocation = function ()
    {
        return location.pathname;    
    };

    $routerBackend.toPsMachinesList = function()
    {
        setUrl('/ps/ps-machine');
    };

    /**
     * Add/replace hash to url
     * @param hash
     */
    $routerBackend.setHash = function(hash)
    {
        window.location.hash = '#' + hash;
    };

    return $routerBackend;
});
